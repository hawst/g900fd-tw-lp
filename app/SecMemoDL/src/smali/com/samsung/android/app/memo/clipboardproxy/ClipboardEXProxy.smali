.class public Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;
.super Ljava/lang/Object;
.source "ClipboardEXProxy.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy$ClipboardDataPasteHandler;,
        Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy$IClipboardDataPasteEventStub;,
        Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy$IClipboardEXProxyPateListener;
    }
.end annotation


# static fields
.field private static final CLIPBOARDEX_PACKAGE:Ljava/lang/String; = "android.sec.clipboard"

.field public static final CLIPBOARDEX_SERVICE:Ljava/lang/String; = "clipboardEx"

.field public static final CLIPBOARD_SERVICE:Ljava/lang/String; = "clipboard"

.field private static ClipboardData:Ljava/lang/Class; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static final ClipboardDataBItmapURI_CLASS:Ljava/lang/String; = "android.sec.clipboard.data.list.ClipboardDataUri"

.field private static final ClipboardDataBItmap_CLASS:Ljava/lang/String; = "android.sec.clipboard.data.list.ClipboardDataBitmap"

.field private static ClipboardDataHTMLFragment:Ljava/lang/Class; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static final ClipboardDataHTMLFragment_CLASS:Ljava/lang/String; = "android.sec.clipboard.data.list.ClipboardDataHTMLFragment"

.field private static ClipboardDataText:Ljava/lang/Class; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static final ClipboardDataText_CLASS:Ljava/lang/String; = "android.sec.clipboard.data.list.ClipboardDataText"

.field private static final ClipboardData_CLASS:Ljava/lang/String; = "android.sec.clipboard.data.ClipboardData"

.field private static IClipboardDataPasteEvent:Ljava/lang/Class; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static final IClipboardDataPasteEvent_CLASS:Ljava/lang/String; = "android.sec.clipboard.IClipboardDataPasteEvent"

.field private static SetHTMLFragment:Ljava/lang/reflect/Method;

.field private static clipBoardClass:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<+",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private static dismissUIDataDialog:Ljava/lang/reflect/Method;

.field private static getFrozenState:Ljava/lang/reflect/Method;

.field private static instance:Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;

.field private static isShowing:Ljava/lang/reflect/Method;

.field private static setData:Ljava/lang/reflect/Method;

.field private static setPasteFrozenMethod:Ljava/lang/reflect/Method;

.field private static setThawPaste:Ljava/lang/reflect/Method;

.field private static showUIDataDialog:Ljava/lang/reflect/Method;


# instance fields
.field private TAG:Ljava/lang/String;

.field private clipEx:Ljava/lang/Object;

.field private clipboardData:Ljava/lang/Object;

.field private isClipBoardExServiceEnabled:Z

.field private isPastedFromTheClipboard:Z

.field private mBinder:Ljava/lang/Object;

.field private mPasteEvent:Ljava/lang/Object;

.field private pasteListener:Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy$IClipboardEXProxyPateListener;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 82
    sput-object v0, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->clipBoardClass:Ljava/lang/Class;

    .line 84
    sput-object v0, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->IClipboardDataPasteEvent:Ljava/lang/Class;

    .line 86
    sput-object v0, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->setPasteFrozenMethod:Ljava/lang/reflect/Method;

    .line 88
    sput-object v0, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->setThawPaste:Ljava/lang/reflect/Method;

    .line 90
    sput-object v0, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->ClipboardData:Ljava/lang/Class;

    .line 92
    sput-object v0, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->ClipboardDataHTMLFragment:Ljava/lang/Class;

    .line 94
    sput-object v0, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->SetHTMLFragment:Ljava/lang/reflect/Method;

    .line 96
    sput-object v0, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->setData:Ljava/lang/reflect/Method;

    .line 98
    sput-object v0, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->ClipboardDataText:Ljava/lang/Class;

    .line 100
    sput-object v0, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->getFrozenState:Ljava/lang/reflect/Method;

    .line 102
    sput-object v0, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->isShowing:Ljava/lang/reflect/Method;

    .line 104
    sput-object v0, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->dismissUIDataDialog:Ljava/lang/reflect/Method;

    .line 106
    sput-object v0, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->showUIDataDialog:Ljava/lang/reflect/Method;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 4
    .param p1, "mContext"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    const/4 v3, 0x0

    .line 108
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    const-string v1, "ClipboardEXProxy"

    iput-object v1, p0, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->TAG:Ljava/lang/String;

    .line 68
    iput-boolean v3, p0, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->isClipBoardExServiceEnabled:Z

    .line 70
    iput-boolean v3, p0, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->isPastedFromTheClipboard:Z

    .line 74
    iput-object v2, p0, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->clipEx:Ljava/lang/Object;

    .line 76
    iput-object v2, p0, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->mPasteEvent:Ljava/lang/Object;

    .line 78
    iput-object v2, p0, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->mBinder:Ljava/lang/Object;

    .line 80
    iput-object v2, p0, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->clipboardData:Ljava/lang/Object;

    .line 110
    :try_start_0
    const-string v1, "clipboardEx"

    invoke-virtual {p1, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->clipEx:Ljava/lang/Object;

    .line 111
    iget-object v1, p0, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->clipEx:Ljava/lang/Object;

    if-eqz v1, :cond_0

    .line 112
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->isClipBoardExServiceEnabled:Z
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/NoSuchMethodError; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/NoClassDefFoundError; {:try_start_0 .. :try_end_0} :catch_3

    .line 127
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->clipEx:Ljava/lang/Object;

    if-nez v1, :cond_1

    .line 128
    iput-boolean v3, p0, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->isClipBoardExServiceEnabled:Z

    .line 129
    :cond_1
    return-void

    .line 113
    :catch_0
    move-exception v0

    .line 114
    .local v0, "e":Ljava/lang/SecurityException;
    iput-boolean v3, p0, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->isClipBoardExServiceEnabled:Z

    .line 115
    iget-object v1, p0, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->TAG:Ljava/lang/String;

    const-string v2, "ClipboardEXProxy"

    invoke-static {v1, v2, v0}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 116
    .end local v0    # "e":Ljava/lang/SecurityException;
    :catch_1
    move-exception v0

    .line 117
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    iput-boolean v3, p0, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->isClipBoardExServiceEnabled:Z

    .line 118
    iget-object v1, p0, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->TAG:Ljava/lang/String;

    const-string v2, "ClipboardEXProxy"

    invoke-static {v1, v2, v0}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 119
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :catch_2
    move-exception v0

    .line 120
    .local v0, "e":Ljava/lang/NoSuchMethodError;
    iput-boolean v3, p0, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->isClipBoardExServiceEnabled:Z

    .line 121
    iget-object v1, p0, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->TAG:Ljava/lang/String;

    const-string v2, "ClipboardEXProxy"

    invoke-static {v1, v2, v0}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 122
    .end local v0    # "e":Ljava/lang/NoSuchMethodError;
    :catch_3
    move-exception v0

    .line 123
    .local v0, "e":Ljava/lang/NoClassDefFoundError;
    iput-boolean v3, p0, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->isClipBoardExServiceEnabled:Z

    .line 124
    iget-object v1, p0, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->TAG:Ljava/lang/String;

    const-string v2, "ClipboardEXProxy"

    invoke-static {v1, v2, v0}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method static synthetic access$0(Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 78
    iput-object p1, p0, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->mBinder:Ljava/lang/Object;

    return-void
.end method

.method static synthetic access$1(Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->mBinder:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$2(Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$3(Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;)Z
    .locals 1

    .prologue
    .line 68
    iget-boolean v0, p0, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->isClipBoardExServiceEnabled:Z

    return v0
.end method

.method static synthetic access$4(Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;Z)V
    .locals 0

    .prologue
    .line 70
    iput-boolean p1, p0, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->isPastedFromTheClipboard:Z

    return-void
.end method

.method static synthetic access$5(Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;)Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy$IClipboardEXProxyPateListener;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->pasteListener:Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy$IClipboardEXProxyPateListener;

    return-object v0
.end method

.method public static newInstance(Landroid/content/Context;)Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;
    .locals 1
    .param p0, "mContext"    # Landroid/content/Context;

    .prologue
    .line 132
    sget-object v0, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->instance:Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;

    if-nez v0, :cond_0

    .line 133
    new-instance v0, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;

    invoke-direct {v0, p0}, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->instance:Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;

    .line 136
    :cond_0
    sget-object v0, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->instance:Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;

    return-object v0
.end method


# virtual methods
.method public dismissUIDataDialog()V
    .locals 4

    .prologue
    .line 380
    :try_start_0
    sget-object v1, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->clipBoardClass:Ljava/lang/Class;

    if-eqz v1, :cond_0

    sget-object v1, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->dismissUIDataDialog:Ljava/lang/reflect/Method;

    if-nez v1, :cond_1

    .line 381
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->clipEx:Ljava/lang/Object;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    sput-object v1, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->clipBoardClass:Ljava/lang/Class;

    .line 382
    sget-object v1, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->clipBoardClass:Ljava/lang/Class;

    const-string v2, "dismissUIDataDialog"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    sput-object v1, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->dismissUIDataDialog:Ljava/lang/reflect/Method;

    .line 384
    :cond_1
    sget-object v1, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->dismissUIDataDialog:Ljava/lang/reflect/Method;

    iget-object v2, p0, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->clipEx:Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_3

    .line 394
    :goto_0
    return-void

    .line 385
    :catch_0
    move-exception v0

    .line 386
    .local v0, "e":Ljava/lang/NoSuchMethodException;
    iget-object v1, p0, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->TAG:Ljava/lang/String;

    const-string v2, "dismissUIDataDialog"

    invoke-static {v1, v2, v0}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 387
    .end local v0    # "e":Ljava/lang/NoSuchMethodException;
    :catch_1
    move-exception v0

    .line 388
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    iget-object v1, p0, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->TAG:Ljava/lang/String;

    const-string v2, "dismissUIDataDialog"

    invoke-static {v1, v2, v0}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 389
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :catch_2
    move-exception v0

    .line 390
    .local v0, "e":Ljava/lang/IllegalAccessException;
    iget-object v1, p0, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->TAG:Ljava/lang/String;

    const-string v2, "dismissUIDataDialog"

    invoke-static {v1, v2, v0}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 391
    .end local v0    # "e":Ljava/lang/IllegalAccessException;
    :catch_3
    move-exception v0

    .line 392
    .local v0, "e":Ljava/lang/reflect/InvocationTargetException;
    iget-object v1, p0, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->TAG:Ljava/lang/String;

    const-string v2, "dismissUIDataDialog"

    invoke-static {v1, v2, v0}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public getData(Landroid/content/Context;I)Lcom/samsung/android/app/memo/clipboardproxy/ClipProxyData;
    .locals 23
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "formatid"    # I

    .prologue
    .line 270
    const/16 v17, 0x0

    .line 271
    .local v17, "seqText":Ljava/lang/CharSequence;
    new-instance v16, Lcom/samsung/android/app/memo/clipboardproxy/ClipProxyData;

    invoke-direct/range {v16 .. v16}, Lcom/samsung/android/app/memo/clipboardproxy/ClipProxyData;-><init>()V

    .line 272
    .local v16, "proxyData":Lcom/samsung/android/app/memo/clipboardproxy/ClipProxyData;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->clipEx:Ljava/lang/Object;

    move-object/from16 v18, v0

    if-eqz v18, :cond_4

    .line 273
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->clipEx:Ljava/lang/Object;

    move-object/from16 v18, v0

    check-cast v18, Landroid/sec/clipboard/ClipboardExManager;

    const/16 v19, 0x1

    invoke-virtual/range {v18 .. v19}, Landroid/sec/clipboard/ClipboardExManager;->hasDataOf(I)Z

    move-result v18

    if-eqz v18, :cond_4

    .line 275
    :try_start_0
    sget-object v18, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->clipBoardClass:Ljava/lang/Class;

    if-nez v18, :cond_0

    .line 276
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->clipEx:Ljava/lang/Object;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v18

    sput-object v18, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->clipBoardClass:Ljava/lang/Class;

    .line 277
    :cond_0
    sget-object v18, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->clipBoardClass:Ljava/lang/Class;

    const-string v19, "getData"

    const/16 v20, 0x2

    move/from16 v0, v20

    new-array v0, v0, [Ljava/lang/Class;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    const-class v22, Landroid/content/Context;

    aput-object v22, v20, v21

    const/16 v21, 0x1

    sget-object v22, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v22, v20, v21

    invoke-virtual/range {v18 .. v20}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v13

    .line 278
    .local v13, "getData":Ljava/lang/reflect/Method;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->clipEx:Ljava/lang/Object;

    move-object/from16 v18, v0

    const/16 v19, 0x2

    move/from16 v0, v19

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    aput-object p1, v19, v20

    const/16 v20, 0x1

    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    aput-object v21, v19, v20

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v13, v0, v1}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    .line 279
    .local v10, "clipdata":Ljava/lang/Object;
    invoke-virtual {v10}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v18

    const-string v19, "android.sec.clipboard.data.list.ClipboardDataHTMLFragment"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v18

    if-eqz v18, :cond_1

    .line 280
    invoke-virtual {v10}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    .line 281
    .local v3, "ClipboardDataHTMLFragment":Ljava/lang/Class;, "Ljava/lang/Class<+Ljava/lang/Object;>;"
    const-string v18, "GetHTMLFragment"

    .line 282
    const/16 v19, 0x0

    .line 281
    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v3, v0, v1}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v7

    .line 283
    .local v7, "GetHTMLFragment":Ljava/lang/reflect/Method;
    const/16 v18, 0x0

    move-object/from16 v0, v18

    invoke-virtual {v7, v10, v0}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v18

    move-object/from16 v0, v18

    check-cast v0, Ljava/lang/CharSequence;

    move-object/from16 v17, v0

    .line 284
    const/16 v18, 0x4

    move-object/from16 v0, v16

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/memo/clipboardproxy/ClipProxyData;->setFormat(I)V

    .line 304
    .end local v3    # "ClipboardDataHTMLFragment":Ljava/lang/Class;, "Ljava/lang/Class<+Ljava/lang/Object;>;"
    .end local v7    # "GetHTMLFragment":Ljava/lang/reflect/Method;
    :goto_0
    invoke-virtual/range {v16 .. v17}, Lcom/samsung/android/app/memo/clipboardproxy/ClipProxyData;->setData(Ljava/lang/CharSequence;)V

    .line 335
    .end local v10    # "clipdata":Ljava/lang/Object;
    .end local v13    # "getData":Ljava/lang/reflect/Method;
    :goto_1
    return-object v16

    .line 285
    .restart local v10    # "clipdata":Ljava/lang/Object;
    .restart local v13    # "getData":Ljava/lang/reflect/Method;
    :cond_1
    invoke-virtual {v10}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v18

    .line 286
    const-string v19, "android.sec.clipboard.data.list.ClipboardDataBitmap"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v18

    if-eqz v18, :cond_2

    .line 287
    invoke-virtual {v10}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    .line 288
    .local v2, "ClipboardDataBitmap":Ljava/lang/Class;, "Ljava/lang/Class<+Ljava/lang/Object;>;"
    const-string v18, "GetBitmapPath"

    const/16 v19, 0x0

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v2, v0, v1}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v5

    .line 289
    .local v5, "GetBitmapPath":Ljava/lang/reflect/Method;
    const/16 v18, 0x0

    move-object/from16 v0, v18

    invoke-virtual {v5, v10, v0}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v18

    move-object/from16 v0, v18

    check-cast v0, Ljava/lang/CharSequence;

    move-object/from16 v17, v0

    .line 290
    const/16 v18, 0x3

    move-object/from16 v0, v16

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/memo/clipboardproxy/ClipProxyData;->setFormat(I)V
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_3

    goto :goto_0

    .line 305
    .end local v2    # "ClipboardDataBitmap":Ljava/lang/Class;, "Ljava/lang/Class<+Ljava/lang/Object;>;"
    .end local v5    # "GetBitmapPath":Ljava/lang/reflect/Method;
    .end local v10    # "clipdata":Ljava/lang/Object;
    .end local v13    # "getData":Ljava/lang/reflect/Method;
    :catch_0
    move-exception v12

    .line 306
    .local v12, "e":Ljava/lang/NoSuchMethodException;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->TAG:Ljava/lang/String;

    move-object/from16 v18, v0

    const-string v19, "getData"

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-static {v0, v1, v12}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 291
    .end local v12    # "e":Ljava/lang/NoSuchMethodException;
    .restart local v10    # "clipdata":Ljava/lang/Object;
    .restart local v13    # "getData":Ljava/lang/reflect/Method;
    :cond_2
    :try_start_1
    invoke-virtual {v10}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v18

    .line 292
    const-string v19, "android.sec.clipboard.data.list.ClipboardDataUri"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v18

    if-eqz v18, :cond_3

    .line 293
    invoke-virtual {v10}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    .line 294
    .restart local v2    # "ClipboardDataBitmap":Ljava/lang/Class;, "Ljava/lang/Class<+Ljava/lang/Object;>;"
    const-string v18, "GetUri"

    const/16 v19, 0x0

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v2, v0, v1}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v6

    .line 295
    .local v6, "GetClipdataUri":Ljava/lang/reflect/Method;
    const/16 v18, 0x0

    move-object/from16 v0, v18

    invoke-virtual {v6, v10, v0}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Landroid/net/Uri;

    .line 296
    .local v15, "mUri":Landroid/net/Uri;
    invoke-virtual {v15}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v17

    .line 297
    const/16 v18, 0x5

    move-object/from16 v0, v16

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/memo/clipboardproxy/ClipProxyData;->setFormat(I)V
    :try_end_1
    .catch Ljava/lang/NoSuchMethodException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_1 .. :try_end_1} :catch_3

    goto/16 :goto_0

    .line 307
    .end local v2    # "ClipboardDataBitmap":Ljava/lang/Class;, "Ljava/lang/Class<+Ljava/lang/Object;>;"
    .end local v6    # "GetClipdataUri":Ljava/lang/reflect/Method;
    .end local v10    # "clipdata":Ljava/lang/Object;
    .end local v13    # "getData":Ljava/lang/reflect/Method;
    .end local v15    # "mUri":Landroid/net/Uri;
    :catch_1
    move-exception v12

    .line 308
    .local v12, "e":Ljava/lang/IllegalArgumentException;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->TAG:Ljava/lang/String;

    move-object/from16 v18, v0

    const-string v19, "getData"

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-static {v0, v1, v12}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_1

    .line 299
    .end local v12    # "e":Ljava/lang/IllegalArgumentException;
    .restart local v10    # "clipdata":Ljava/lang/Object;
    .restart local v13    # "getData":Ljava/lang/reflect/Method;
    :cond_3
    :try_start_2
    invoke-virtual {v10}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    .line 300
    .local v4, "ClipboardDataText":Ljava/lang/Class;, "Ljava/lang/Class<+Ljava/lang/Object;>;"
    const-string v18, "GetText"

    const/16 v19, 0x0

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v4, v0, v1}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v8

    .line 301
    .local v8, "GetText":Ljava/lang/reflect/Method;
    const/16 v18, 0x0

    move-object/from16 v0, v18

    invoke-virtual {v8, v10, v0}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v18

    move-object/from16 v0, v18

    check-cast v0, Ljava/lang/CharSequence;

    move-object/from16 v17, v0

    .line 302
    const/16 v18, 0x2

    move-object/from16 v0, v16

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/memo/clipboardproxy/ClipProxyData;->setFormat(I)V
    :try_end_2
    .catch Ljava/lang/NoSuchMethodException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_2 .. :try_end_2} :catch_3

    goto/16 :goto_0

    .line 309
    .end local v4    # "ClipboardDataText":Ljava/lang/Class;, "Ljava/lang/Class<+Ljava/lang/Object;>;"
    .end local v8    # "GetText":Ljava/lang/reflect/Method;
    .end local v10    # "clipdata":Ljava/lang/Object;
    .end local v13    # "getData":Ljava/lang/reflect/Method;
    :catch_2
    move-exception v12

    .line 310
    .local v12, "e":Ljava/lang/IllegalAccessException;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->TAG:Ljava/lang/String;

    move-object/from16 v18, v0

    const-string v19, "getData"

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-static {v0, v1, v12}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_1

    .line 311
    .end local v12    # "e":Ljava/lang/IllegalAccessException;
    :catch_3
    move-exception v12

    .line 312
    .local v12, "e":Ljava/lang/reflect/InvocationTargetException;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->TAG:Ljava/lang/String;

    move-object/from16 v18, v0

    const-string v19, "getData"

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-static {v0, v1, v12}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_1

    .line 317
    .end local v12    # "e":Ljava/lang/reflect/InvocationTargetException;
    :cond_4
    const-string v18, "clipboard"

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v9

    .line 316
    check-cast v9, Landroid/content/ClipboardManager;

    .line 318
    .local v9, "clipboardManager":Landroid/content/ClipboardManager;
    if-eqz v9, :cond_6

    .line 319
    invoke-virtual {v9}, Landroid/content/ClipboardManager;->getPrimaryClip()Landroid/content/ClipData;

    move-result-object v11

    .line 320
    .local v11, "clipdata1":Landroid/content/ClipData;
    const/4 v14, 0x0

    .line 322
    .local v14, "item":Landroid/content/ClipData$Item;
    if-eqz v11, :cond_5

    .line 323
    const/16 v18, 0x0

    move/from16 v0, v18

    invoke-virtual {v11, v0}, Landroid/content/ClipData;->getItemAt(I)Landroid/content/ClipData$Item;

    move-result-object v14

    .line 326
    :cond_5
    if-eqz v14, :cond_6

    .line 327
    invoke-virtual {v14}, Landroid/content/ClipData$Item;->getText()Ljava/lang/CharSequence;

    move-result-object v17

    .end local v17    # "seqText":Ljava/lang/CharSequence;
    check-cast v17, Ljava/lang/String;

    .line 328
    .restart local v17    # "seqText":Ljava/lang/CharSequence;
    const/16 v18, 0x4

    move-object/from16 v0, v16

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/memo/clipboardproxy/ClipProxyData;->setFormat(I)V

    .line 331
    .end local v11    # "clipdata1":Landroid/content/ClipData;
    .end local v14    # "item":Landroid/content/ClipData$Item;
    :cond_6
    invoke-virtual/range {v16 .. v17}, Lcom/samsung/android/app/memo/clipboardproxy/ClipProxyData;->setData(Ljava/lang/CharSequence;)V

    goto/16 :goto_1
.end method

.method public getFrozenState()Z
    .locals 6

    .prologue
    .line 339
    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    .line 341
    .local v1, "bool":Ljava/lang/Boolean;
    :try_start_0
    sget-object v3, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->clipBoardClass:Ljava/lang/Class;

    if-eqz v3, :cond_0

    sget-object v3, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->getFrozenState:Ljava/lang/reflect/Method;

    if-nez v3, :cond_1

    .line 342
    :cond_0
    iget-object v3, p0, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->clipEx:Ljava/lang/Object;

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    sput-object v3, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->clipBoardClass:Ljava/lang/Class;

    .line 343
    sget-object v3, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->clipBoardClass:Ljava/lang/Class;

    const-string v4, "getFrozenState"

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v3

    sput-object v3, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->getFrozenState:Ljava/lang/reflect/Method;

    .line 345
    :cond_1
    sget-object v3, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->getFrozenState:Ljava/lang/reflect/Method;

    iget-object v4, p0, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->clipEx:Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Ljava/lang/Boolean;

    move-object v1, v0
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_3

    .line 355
    :goto_0
    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    return v3

    .line 346
    :catch_0
    move-exception v2

    .line 347
    .local v2, "e":Ljava/lang/NoSuchMethodException;
    iget-object v3, p0, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->TAG:Ljava/lang/String;

    const-string v4, "getFrozenState"

    invoke-static {v3, v4, v2}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 348
    .end local v2    # "e":Ljava/lang/NoSuchMethodException;
    :catch_1
    move-exception v2

    .line 349
    .local v2, "e":Ljava/lang/IllegalArgumentException;
    iget-object v3, p0, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->TAG:Ljava/lang/String;

    const-string v4, "getFrozenState"

    invoke-static {v3, v4, v2}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 350
    .end local v2    # "e":Ljava/lang/IllegalArgumentException;
    :catch_2
    move-exception v2

    .line 351
    .local v2, "e":Ljava/lang/IllegalAccessException;
    iget-object v3, p0, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->TAG:Ljava/lang/String;

    const-string v4, "getFrozenState"

    invoke-static {v3, v4, v2}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 352
    .end local v2    # "e":Ljava/lang/IllegalAccessException;
    :catch_3
    move-exception v2

    .line 353
    .local v2, "e":Ljava/lang/reflect/InvocationTargetException;
    iget-object v3, p0, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->TAG:Ljava/lang/String;

    const-string v4, "getFrozenState"

    invoke-static {v3, v4, v2}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public isCalledPasteFromClipboard()Z
    .locals 1

    .prologue
    .line 540
    iget-boolean v0, p0, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->isPastedFromTheClipboard:Z

    return v0
.end method

.method public isClipBoardExServiceEnabled()Z
    .locals 1

    .prologue
    .line 140
    iget-boolean v0, p0, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->isClipBoardExServiceEnabled:Z

    return v0
.end method

.method public isShowing()Z
    .locals 6

    .prologue
    .line 359
    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    .line 361
    .local v1, "bool":Ljava/lang/Boolean;
    :try_start_0
    sget-object v3, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->clipBoardClass:Ljava/lang/Class;

    if-eqz v3, :cond_0

    sget-object v3, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->isShowing:Ljava/lang/reflect/Method;

    if-nez v3, :cond_1

    .line 362
    :cond_0
    iget-object v3, p0, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->clipEx:Ljava/lang/Object;

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    sput-object v3, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->clipBoardClass:Ljava/lang/Class;

    .line 363
    sget-object v3, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->clipBoardClass:Ljava/lang/Class;

    const-string v4, "isShowing"

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v3

    sput-object v3, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->isShowing:Ljava/lang/reflect/Method;

    .line 365
    :cond_1
    sget-object v3, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->isShowing:Ljava/lang/reflect/Method;

    iget-object v4, p0, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->clipEx:Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Ljava/lang/Boolean;

    move-object v1, v0
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_3

    .line 375
    :goto_0
    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    return v3

    .line 366
    :catch_0
    move-exception v2

    .line 367
    .local v2, "e":Ljava/lang/NoSuchMethodException;
    iget-object v3, p0, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->TAG:Ljava/lang/String;

    const-string v4, "isShowing"

    invoke-static {v3, v4, v2}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 368
    .end local v2    # "e":Ljava/lang/NoSuchMethodException;
    :catch_1
    move-exception v2

    .line 369
    .local v2, "e":Ljava/lang/IllegalArgumentException;
    iget-object v3, p0, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->TAG:Ljava/lang/String;

    const-string v4, "isShowing"

    invoke-static {v3, v4, v2}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 370
    .end local v2    # "e":Ljava/lang/IllegalArgumentException;
    :catch_2
    move-exception v2

    .line 371
    .local v2, "e":Ljava/lang/IllegalAccessException;
    iget-object v3, p0, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->TAG:Ljava/lang/String;

    const-string v4, "isShowing"

    invoke-static {v3, v4, v2}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 372
    .end local v2    # "e":Ljava/lang/IllegalAccessException;
    :catch_3
    move-exception v2

    .line 373
    .local v2, "e":Ljava/lang/reflect/InvocationTargetException;
    iget-object v3, p0, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->TAG:Ljava/lang/String;

    const-string v4, "isShowing"

    invoke-static {v3, v4, v2}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public registerClipboardPasteService(Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy$IClipboardEXProxyPateListener;)V
    .locals 6
    .param p1, "listener"    # Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy$IClipboardEXProxyPateListener;

    .prologue
    .line 145
    iget-object v1, p0, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->clipEx:Ljava/lang/Object;

    if-nez v1, :cond_1

    .line 146
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->unregisterClipboardPasteService()V

    .line 178
    :cond_0
    :goto_0
    return-void

    .line 149
    :cond_1
    iget-boolean v1, p0, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->isClipBoardExServiceEnabled:Z

    if-eqz v1, :cond_0

    .line 150
    iput-object p1, p0, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->pasteListener:Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy$IClipboardEXProxyPateListener;

    .line 152
    :try_start_0
    sget-object v1, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->clipBoardClass:Ljava/lang/Class;

    if-eqz v1, :cond_2

    sget-object v1, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->IClipboardDataPasteEvent:Ljava/lang/Class;

    if-eqz v1, :cond_2

    .line 153
    sget-object v1, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->setPasteFrozenMethod:Ljava/lang/reflect/Method;

    if-nez v1, :cond_3

    .line 154
    :cond_2
    iget-object v1, p0, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->clipEx:Ljava/lang/Object;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    sput-object v1, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->clipBoardClass:Ljava/lang/Class;

    .line 155
    const-string v1, "android.sec.clipboard.IClipboardDataPasteEvent"

    invoke-static {v1}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    sput-object v1, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->IClipboardDataPasteEvent:Ljava/lang/Class;

    .line 156
    sget-object v1, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->clipBoardClass:Ljava/lang/Class;

    const-string v2, "setPasteFrozen"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Class;

    const/4 v4, 0x0

    sget-object v5, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v5, v3, v4

    const/4 v4, 0x1

    .line 157
    sget-object v5, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->IClipboardDataPasteEvent:Ljava/lang/Class;

    aput-object v5, v3, v4

    .line 156
    invoke-virtual {v1, v2, v3}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    sput-object v1, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->setPasteFrozenMethod:Ljava/lang/reflect/Method;

    .line 159
    :cond_3
    sget-object v1, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->IClipboardDataPasteEvent:Ljava/lang/Class;

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    .line 160
    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Class;

    const/4 v3, 0x0

    .line 161
    sget-object v4, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->IClipboardDataPasteEvent:Ljava/lang/Class;

    aput-object v4, v2, v3

    .line 162
    new-instance v3, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy$ClipboardDataPasteHandler;

    const/4 v4, 0x0

    invoke-direct {v3, p0, v4}, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy$ClipboardDataPasteHandler;-><init>(Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy$ClipboardDataPasteHandler;)V

    .line 159
    invoke-static {v1, v2, v3}, Ljava/lang/reflect/Proxy;->newProxyInstance(Ljava/lang/ClassLoader;[Ljava/lang/Class;Ljava/lang/reflect/InvocationHandler;)Ljava/lang/Object;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->mPasteEvent:Ljava/lang/Object;

    .line 163
    sget-object v1, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->setPasteFrozenMethod:Ljava/lang/reflect/Method;

    iget-object v2, p0, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->clipEx:Ljava/lang/Object;

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    .line 164
    const/4 v5, 0x1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    iget-object v5, p0, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->mPasteEvent:Ljava/lang/Object;

    aput-object v5, v3, v4

    .line 163
    invoke-virtual {v1, v2, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_4

    goto :goto_0

    .line 166
    :catch_0
    move-exception v0

    .line 167
    .local v0, "e":Ljava/lang/NoSuchMethodException;
    iget-object v1, p0, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->TAG:Ljava/lang/String;

    const-string v2, "registerClipboardPasteService"

    invoke-static {v1, v2, v0}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 168
    .end local v0    # "e":Ljava/lang/NoSuchMethodException;
    :catch_1
    move-exception v0

    .line 169
    .local v0, "e":Ljava/lang/ClassNotFoundException;
    iget-object v1, p0, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->TAG:Ljava/lang/String;

    const-string v2, "registerClipboardPasteService"

    invoke-static {v1, v2, v0}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 170
    .end local v0    # "e":Ljava/lang/ClassNotFoundException;
    :catch_2
    move-exception v0

    .line 171
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    iget-object v1, p0, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->TAG:Ljava/lang/String;

    const-string v2, "registerClipboardPasteService"

    invoke-static {v1, v2, v0}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_0

    .line 172
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :catch_3
    move-exception v0

    .line 173
    .local v0, "e":Ljava/lang/IllegalAccessException;
    iget-object v1, p0, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->TAG:Ljava/lang/String;

    const-string v2, "registerClipboardPasteService"

    invoke-static {v1, v2, v0}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_0

    .line 174
    .end local v0    # "e":Ljava/lang/IllegalAccessException;
    :catch_4
    move-exception v0

    .line 175
    .local v0, "e":Ljava/lang/reflect/InvocationTargetException;
    iget-object v1, p0, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->TAG:Ljava/lang/String;

    const-string v2, "registerClipboardPasteService"

    invoke-static {v1, v2, v0}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_0
.end method

.method public resetIsPastedFromTheClipboard()V
    .locals 1

    .prologue
    .line 544
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->isPastedFromTheClipboard:Z

    .line 545
    return-void
.end method

.method public setData(Landroid/content/Context;Lcom/samsung/android/app/memo/clipboardproxy/ClipProxyData;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "data"    # Lcom/samsung/android/app/memo/clipboardproxy/ClipProxyData;

    .prologue
    .line 204
    invoke-virtual {p2}, Lcom/samsung/android/app/memo/clipboardproxy/ClipProxyData;->getFormat()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 209
    invoke-virtual {p2}, Lcom/samsung/android/app/memo/clipboardproxy/ClipProxyData;->getData()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->setTextData(Landroid/content/Context;Ljava/lang/CharSequence;)V

    .line 211
    :goto_0
    return-void

    .line 206
    :pswitch_0
    invoke-virtual {p2}, Lcom/samsung/android/app/memo/clipboardproxy/ClipProxyData;->getData()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->setHtmlFragmentData(Landroid/content/Context;Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 204
    nop

    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_0
    .end packed-switch
.end method

.method public setHtmlFragmentData(Landroid/content/Context;Ljava/lang/CharSequence;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "htmlStr"    # Ljava/lang/CharSequence;

    .prologue
    .line 215
    :try_start_0
    sget-object v1, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->clipBoardClass:Ljava/lang/Class;

    if-eqz v1, :cond_0

    sget-object v1, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->ClipboardData:Ljava/lang/Class;

    if-eqz v1, :cond_0

    .line 216
    sget-object v1, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->ClipboardDataHTMLFragment:Ljava/lang/Class;

    if-nez v1, :cond_1

    .line 217
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->clipEx:Ljava/lang/Object;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    sput-object v1, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->clipBoardClass:Ljava/lang/Class;

    .line 218
    const-string v1, "android.sec.clipboard.data.ClipboardData"

    invoke-static {v1}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    sput-object v1, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->ClipboardData:Ljava/lang/Class;

    .line 219
    const-string v1, "android.sec.clipboard.data.list.ClipboardDataHTMLFragment"

    invoke-static {v1}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    sput-object v1, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->ClipboardDataHTMLFragment:Ljava/lang/Class;

    .line 221
    :cond_1
    sget-object v1, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->ClipboardDataHTMLFragment:Ljava/lang/Class;

    invoke-virtual {v1}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->clipboardData:Ljava/lang/Object;

    .line 222
    sget-object v1, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->ClipboardDataHTMLFragment:Ljava/lang/Class;

    const-string v2, "SetHTMLFragment"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Class;

    const/4 v4, 0x0

    .line 223
    const-class v5, Ljava/lang/CharSequence;

    aput-object v5, v3, v4

    .line 222
    invoke-virtual {v1, v2, v3}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    sput-object v1, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->SetHTMLFragment:Ljava/lang/reflect/Method;

    .line 224
    sget-object v1, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->SetHTMLFragment:Ljava/lang/reflect/Method;

    iget-object v2, p0, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->clipboardData:Ljava/lang/Object;

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p2, v3, v4

    invoke-virtual {v1, v2, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    .line 225
    sget-object v1, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->clipBoardClass:Ljava/lang/Class;

    const-string v2, "setData"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Class;

    const/4 v4, 0x0

    const-class v5, Landroid/content/Context;

    aput-object v5, v3, v4

    const/4 v4, 0x1

    sget-object v5, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->ClipboardData:Ljava/lang/Class;

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    sput-object v1, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->setData:Ljava/lang/reflect/Method;

    .line 226
    sget-object v1, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->setData:Ljava/lang/reflect/Method;

    iget-object v2, p0, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->clipEx:Ljava/lang/Object;

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    const/4 v4, 0x1

    iget-object v5, p0, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->clipboardData:Ljava/lang/Object;

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_5

    .line 240
    :goto_0
    return-void

    .line 227
    :catch_0
    move-exception v0

    .line 228
    .local v0, "e":Ljava/lang/NoSuchMethodException;
    iget-object v1, p0, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->TAG:Ljava/lang/String;

    const-string v2, "setHtmlFragmentData"

    invoke-static {v1, v2, v0}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 229
    .end local v0    # "e":Ljava/lang/NoSuchMethodException;
    :catch_1
    move-exception v0

    .line 230
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    iget-object v1, p0, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->TAG:Ljava/lang/String;

    const-string v2, "setHtmlFragmentData"

    invoke-static {v1, v2, v0}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 231
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :catch_2
    move-exception v0

    .line 232
    .local v0, "e":Ljava/lang/IllegalAccessException;
    iget-object v1, p0, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->TAG:Ljava/lang/String;

    const-string v2, "setHtmlFragmentData"

    invoke-static {v1, v2, v0}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 233
    .end local v0    # "e":Ljava/lang/IllegalAccessException;
    :catch_3
    move-exception v0

    .line 234
    .local v0, "e":Ljava/lang/reflect/InvocationTargetException;
    iget-object v1, p0, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->TAG:Ljava/lang/String;

    const-string v2, "setHtmlFragmentData"

    invoke-static {v1, v2, v0}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 235
    .end local v0    # "e":Ljava/lang/reflect/InvocationTargetException;
    :catch_4
    move-exception v0

    .line 236
    .local v0, "e":Ljava/lang/ClassNotFoundException;
    iget-object v1, p0, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->TAG:Ljava/lang/String;

    const-string v2, "setHtmlFragmentData"

    invoke-static {v1, v2, v0}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 237
    .end local v0    # "e":Ljava/lang/ClassNotFoundException;
    :catch_5
    move-exception v0

    .line 238
    .local v0, "e":Ljava/lang/InstantiationException;
    iget-object v1, p0, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->TAG:Ljava/lang/String;

    const-string v2, "setHtmlFragmentData"

    invoke-static {v1, v2, v0}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public setTextData(Landroid/content/Context;Ljava/lang/CharSequence;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "plainText"    # Ljava/lang/CharSequence;

    .prologue
    .line 244
    :try_start_0
    sget-object v1, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->clipBoardClass:Ljava/lang/Class;

    if-eqz v1, :cond_0

    sget-object v1, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->ClipboardData:Ljava/lang/Class;

    if-eqz v1, :cond_0

    sget-object v1, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->ClipboardDataText:Ljava/lang/Class;

    if-nez v1, :cond_1

    .line 245
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->clipEx:Ljava/lang/Object;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    sput-object v1, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->clipBoardClass:Ljava/lang/Class;

    .line 246
    const-string v1, "android.sec.clipboard.data.ClipboardData"

    invoke-static {v1}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    sput-object v1, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->ClipboardData:Ljava/lang/Class;

    .line 247
    const-string v1, "android.sec.clipboard.data.list.ClipboardDataText"

    invoke-static {v1}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    sput-object v1, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->ClipboardDataText:Ljava/lang/Class;

    .line 249
    :cond_1
    sget-object v1, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->ClipboardDataText:Ljava/lang/Class;

    invoke-virtual {v1}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->clipboardData:Ljava/lang/Object;

    .line 250
    sget-object v1, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->ClipboardDataText:Ljava/lang/Class;

    const-string v2, "SetText"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Class;

    const/4 v4, 0x0

    const-class v5, Ljava/lang/CharSequence;

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    sput-object v1, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->SetHTMLFragment:Ljava/lang/reflect/Method;

    .line 251
    sget-object v1, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->SetHTMLFragment:Ljava/lang/reflect/Method;

    iget-object v2, p0, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->clipboardData:Ljava/lang/Object;

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p2, v3, v4

    invoke-virtual {v1, v2, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    .line 252
    sget-object v1, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->clipBoardClass:Ljava/lang/Class;

    const-string v2, "setData"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Class;

    const/4 v4, 0x0

    const-class v5, Landroid/content/Context;

    aput-object v5, v3, v4

    const/4 v4, 0x1

    sget-object v5, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->ClipboardData:Ljava/lang/Class;

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    sput-object v1, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->setData:Ljava/lang/reflect/Method;

    .line 253
    sget-object v1, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->setData:Ljava/lang/reflect/Method;

    iget-object v2, p0, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->clipEx:Ljava/lang/Object;

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    const/4 v4, 0x1

    iget-object v5, p0, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->clipboardData:Ljava/lang/Object;

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_5

    .line 267
    :goto_0
    return-void

    .line 254
    :catch_0
    move-exception v0

    .line 255
    .local v0, "e":Ljava/lang/NoSuchMethodException;
    iget-object v1, p0, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->TAG:Ljava/lang/String;

    const-string v2, "setTextData"

    invoke-static {v1, v2, v0}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 256
    .end local v0    # "e":Ljava/lang/NoSuchMethodException;
    :catch_1
    move-exception v0

    .line 257
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    iget-object v1, p0, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->TAG:Ljava/lang/String;

    const-string v2, "setTextData"

    invoke-static {v1, v2, v0}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 258
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :catch_2
    move-exception v0

    .line 259
    .local v0, "e":Ljava/lang/IllegalAccessException;
    iget-object v1, p0, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->TAG:Ljava/lang/String;

    const-string v2, "setTextData"

    invoke-static {v1, v2, v0}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 260
    .end local v0    # "e":Ljava/lang/IllegalAccessException;
    :catch_3
    move-exception v0

    .line 261
    .local v0, "e":Ljava/lang/reflect/InvocationTargetException;
    iget-object v1, p0, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->TAG:Ljava/lang/String;

    const-string v2, "setTextData"

    invoke-static {v1, v2, v0}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 262
    .end local v0    # "e":Ljava/lang/reflect/InvocationTargetException;
    :catch_4
    move-exception v0

    .line 263
    .local v0, "e":Ljava/lang/ClassNotFoundException;
    iget-object v1, p0, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->TAG:Ljava/lang/String;

    const-string v2, "setTextData"

    invoke-static {v1, v2, v0}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 264
    .end local v0    # "e":Ljava/lang/ClassNotFoundException;
    :catch_5
    move-exception v0

    .line 265
    .local v0, "e":Ljava/lang/InstantiationException;
    iget-object v1, p0, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->TAG:Ljava/lang/String;

    const-string v2, "setTextData"

    invoke-static {v1, v2, v0}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public showUIDataDialog()V
    .locals 4

    .prologue
    .line 398
    :try_start_0
    sget-object v1, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->clipBoardClass:Ljava/lang/Class;

    if-eqz v1, :cond_0

    sget-object v1, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->showUIDataDialog:Ljava/lang/reflect/Method;

    if-nez v1, :cond_1

    .line 399
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->clipEx:Ljava/lang/Object;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    sput-object v1, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->clipBoardClass:Ljava/lang/Class;

    .line 400
    sget-object v1, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->clipBoardClass:Ljava/lang/Class;

    const-string v2, "showUIDataDialog"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    sput-object v1, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->showUIDataDialog:Ljava/lang/reflect/Method;

    .line 402
    :cond_1
    sget-object v1, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->showUIDataDialog:Ljava/lang/reflect/Method;

    iget-object v2, p0, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->clipEx:Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_3

    .line 412
    :goto_0
    return-void

    .line 403
    :catch_0
    move-exception v0

    .line 404
    .local v0, "e":Ljava/lang/NoSuchMethodException;
    iget-object v1, p0, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->TAG:Ljava/lang/String;

    const-string v2, "showUIDataDialog"

    invoke-static {v1, v2, v0}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 405
    .end local v0    # "e":Ljava/lang/NoSuchMethodException;
    :catch_1
    move-exception v0

    .line 406
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    iget-object v1, p0, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->TAG:Ljava/lang/String;

    const-string v2, "showUIDataDialog"

    invoke-static {v1, v2, v0}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 407
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :catch_2
    move-exception v0

    .line 408
    .local v0, "e":Ljava/lang/IllegalAccessException;
    iget-object v1, p0, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->TAG:Ljava/lang/String;

    const-string v2, "showUIDataDialog"

    invoke-static {v1, v2, v0}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 409
    .end local v0    # "e":Ljava/lang/IllegalAccessException;
    :catch_3
    move-exception v0

    .line 410
    .local v0, "e":Ljava/lang/reflect/InvocationTargetException;
    iget-object v1, p0, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->TAG:Ljava/lang/String;

    const-string v2, "showUIDataDialog"

    invoke-static {v1, v2, v0}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public unregisterClipboardPasteService()V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 181
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->isClipBoardExServiceEnabled:Z

    .line 182
    sput-object v2, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->instance:Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;

    .line 184
    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->clipEx:Ljava/lang/Object;

    if-eqz v1, :cond_2

    .line 185
    sget-object v1, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->clipBoardClass:Ljava/lang/Class;

    if-eqz v1, :cond_0

    sget-object v1, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->setThawPaste:Ljava/lang/reflect/Method;

    if-nez v1, :cond_1

    .line 186
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->clipEx:Ljava/lang/Object;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    sput-object v1, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->clipBoardClass:Ljava/lang/Class;

    .line 187
    sget-object v1, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->clipBoardClass:Ljava/lang/Class;

    const-string v2, "setThawPaste"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    sput-object v1, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->setThawPaste:Ljava/lang/reflect/Method;

    .line 189
    :cond_1
    sget-object v1, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->setThawPaste:Ljava/lang/reflect/Method;

    iget-object v2, p0, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->clipEx:Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_3

    .line 200
    :cond_2
    :goto_0
    return-void

    .line 191
    :catch_0
    move-exception v0

    .line 192
    .local v0, "e":Ljava/lang/NoSuchMethodException;
    iget-object v1, p0, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->TAG:Ljava/lang/String;

    const-string v2, "unregisterClipboardPasteService"

    invoke-static {v1, v2, v0}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 193
    .end local v0    # "e":Ljava/lang/NoSuchMethodException;
    :catch_1
    move-exception v0

    .line 194
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    iget-object v1, p0, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->TAG:Ljava/lang/String;

    const-string v2, "unregisterClipboardPasteService"

    invoke-static {v1, v2, v0}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 195
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :catch_2
    move-exception v0

    .line 196
    .local v0, "e":Ljava/lang/IllegalAccessException;
    iget-object v1, p0, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->TAG:Ljava/lang/String;

    const-string v2, "unregisterClipboardPasteService"

    invoke-static {v1, v2, v0}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 197
    .end local v0    # "e":Ljava/lang/IllegalAccessException;
    :catch_3
    move-exception v0

    .line 198
    .local v0, "e":Ljava/lang/reflect/InvocationTargetException;
    iget-object v1, p0, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->TAG:Ljava/lang/String;

    const-string v2, "unregisterClipboardPasteService"

    invoke-static {v1, v2, v0}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method
