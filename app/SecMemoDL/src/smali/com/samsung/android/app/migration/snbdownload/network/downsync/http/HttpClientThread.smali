.class public Lcom/samsung/android/app/migration/snbdownload/network/downsync/http/HttpClientThread;
.super Ljava/lang/Thread;
.source "HttpClientThread.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/app/migration/snbdownload/network/downsync/http/HttpClientThread$RESPONSE;
    }
.end annotation


# instance fields
.field private mHttpClient:Lorg/apache/http/client/HttpClient;

.field private mHttpClientResponseListener:Lcom/samsung/android/app/migration/snbdownload/network/downsync/http/HttpClientResponseListener;

.field private mHttpResponse:Lorg/apache/http/HttpResponse;

.field private mHttpUriRequest:Lorg/apache/http/client/methods/HttpUriRequest;

.field private mRequestID:Ljava/lang/String;

.field private final mThreadLock:Ljava/lang/Object;

.field private mbClosed:Z


# direct methods
.method public constructor <init>(Lorg/apache/http/client/methods/HttpUriRequest;Lorg/apache/http/client/HttpClient;Lcom/samsung/android/app/migration/snbdownload/network/downsync/http/HttpClientResponseListener;Ljava/lang/String;)V
    .locals 1
    .param p1, "httpUriRequest"    # Lorg/apache/http/client/methods/HttpUriRequest;
    .param p2, "httpClient"    # Lorg/apache/http/client/HttpClient;
    .param p3, "listener"    # Lcom/samsung/android/app/migration/snbdownload/network/downsync/http/HttpClientResponseListener;
    .param p4, "requestID"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 46
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 32
    iput-object v0, p0, Lcom/samsung/android/app/migration/snbdownload/network/downsync/http/HttpClientThread;->mHttpClientResponseListener:Lcom/samsung/android/app/migration/snbdownload/network/downsync/http/HttpClientResponseListener;

    .line 33
    iput-object v0, p0, Lcom/samsung/android/app/migration/snbdownload/network/downsync/http/HttpClientThread;->mHttpUriRequest:Lorg/apache/http/client/methods/HttpUriRequest;

    .line 34
    iput-object v0, p0, Lcom/samsung/android/app/migration/snbdownload/network/downsync/http/HttpClientThread;->mHttpClient:Lorg/apache/http/client/HttpClient;

    .line 35
    iput-object v0, p0, Lcom/samsung/android/app/migration/snbdownload/network/downsync/http/HttpClientThread;->mRequestID:Ljava/lang/String;

    .line 36
    iput-object v0, p0, Lcom/samsung/android/app/migration/snbdownload/network/downsync/http/HttpClientThread;->mHttpResponse:Lorg/apache/http/HttpResponse;

    .line 37
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/app/migration/snbdownload/network/downsync/http/HttpClientThread;->mbClosed:Z

    .line 38
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/app/migration/snbdownload/network/downsync/http/HttpClientThread;->mThreadLock:Ljava/lang/Object;

    .line 48
    iput-object p3, p0, Lcom/samsung/android/app/migration/snbdownload/network/downsync/http/HttpClientThread;->mHttpClientResponseListener:Lcom/samsung/android/app/migration/snbdownload/network/downsync/http/HttpClientResponseListener;

    .line 49
    iput-object p1, p0, Lcom/samsung/android/app/migration/snbdownload/network/downsync/http/HttpClientThread;->mHttpUriRequest:Lorg/apache/http/client/methods/HttpUriRequest;

    .line 50
    iput-object p2, p0, Lcom/samsung/android/app/migration/snbdownload/network/downsync/http/HttpClientThread;->mHttpClient:Lorg/apache/http/client/HttpClient;

    .line 51
    iput-object p4, p0, Lcom/samsung/android/app/migration/snbdownload/network/downsync/http/HttpClientThread;->mRequestID:Ljava/lang/String;

    .line 53
    return-void
.end method

.method private removeThis(I)V
    .locals 2
    .param p1, "reason"    # I

    .prologue
    .line 92
    iget-object v1, p0, Lcom/samsung/android/app/migration/snbdownload/network/downsync/http/HttpClientThread;->mHttpClientResponseListener:Lcom/samsung/android/app/migration/snbdownload/network/downsync/http/HttpClientResponseListener;

    if-eqz v1, :cond_0

    .line 93
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    .line 94
    .local v0, "msg":Landroid/os/Message;
    iput p1, v0, Landroid/os/Message;->arg1:I

    .line 95
    iput-object p0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 96
    iget-object v1, p0, Lcom/samsung/android/app/migration/snbdownload/network/downsync/http/HttpClientThread;->mHttpClientResponseListener:Lcom/samsung/android/app/migration/snbdownload/network/downsync/http/HttpClientResponseListener;

    invoke-interface {v1, v0}, Lcom/samsung/android/app/migration/snbdownload/network/downsync/http/HttpClientResponseListener;->onResponse(Landroid/os/Message;)V

    .line 98
    .end local v0    # "msg":Landroid/os/Message;
    :cond_0
    return-void
.end method

.method private responseOK()V
    .locals 2

    .prologue
    .line 101
    iget-object v1, p0, Lcom/samsung/android/app/migration/snbdownload/network/downsync/http/HttpClientThread;->mHttpClientResponseListener:Lcom/samsung/android/app/migration/snbdownload/network/downsync/http/HttpClientResponseListener;

    if-eqz v1, :cond_0

    .line 102
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    .line 103
    .local v0, "msg":Landroid/os/Message;
    const/4 v1, 0x1

    iput v1, v0, Landroid/os/Message;->arg1:I

    .line 104
    iput-object p0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 105
    iget-object v1, p0, Lcom/samsung/android/app/migration/snbdownload/network/downsync/http/HttpClientThread;->mHttpClientResponseListener:Lcom/samsung/android/app/migration/snbdownload/network/downsync/http/HttpClientResponseListener;

    invoke-interface {v1, v0}, Lcom/samsung/android/app/migration/snbdownload/network/downsync/http/HttpClientResponseListener;->onResponse(Landroid/os/Message;)V

    .line 107
    .end local v0    # "msg":Landroid/os/Message;
    :cond_0
    return-void
.end method


# virtual methods
.method public close()V
    .locals 1

    .prologue
    .line 110
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/app/migration/snbdownload/network/downsync/http/HttpClientThread;->mbClosed:Z

    .line 111
    return-void
.end method

.method public getHttpResponse()Lorg/apache/http/HttpResponse;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/samsung/android/app/migration/snbdownload/network/downsync/http/HttpClientThread;->mHttpResponse:Lorg/apache/http/HttpResponse;

    return-object v0
.end method

.method public getRequestID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/samsung/android/app/migration/snbdownload/network/downsync/http/HttpClientThread;->mRequestID:Ljava/lang/String;

    return-object v0
.end method

.method public getThreadLock()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 118
    iget-object v0, p0, Lcom/samsung/android/app/migration/snbdownload/network/downsync/http/HttpClientThread;->mThreadLock:Ljava/lang/Object;

    return-object v0
.end method

.method public isClosed()Z
    .locals 1

    .prologue
    .line 114
    iget-boolean v0, p0, Lcom/samsung/android/app/migration/snbdownload/network/downsync/http/HttpClientThread;->mbClosed:Z

    return v0
.end method

.method public run()V
    .locals 5

    .prologue
    const/4 v4, 0x3

    .line 65
    iget-object v1, p0, Lcom/samsung/android/app/migration/snbdownload/network/downsync/http/HttpClientThread;->mHttpUriRequest:Lorg/apache/http/client/methods/HttpUriRequest;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/android/app/migration/snbdownload/network/downsync/http/HttpClientThread;->mHttpClient:Lorg/apache/http/client/HttpClient;

    if-eqz v1, :cond_0

    .line 67
    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/app/migration/snbdownload/network/downsync/http/HttpClientThread;->mHttpClient:Lorg/apache/http/client/HttpClient;

    iget-object v2, p0, Lcom/samsung/android/app/migration/snbdownload/network/downsync/http/HttpClientThread;->mHttpUriRequest:Lorg/apache/http/client/methods/HttpUriRequest;

    invoke-interface {v1, v2}, Lorg/apache/http/client/HttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/app/migration/snbdownload/network/downsync/http/HttpClientThread;->mHttpResponse:Lorg/apache/http/HttpResponse;
    :try_end_0
    .catch Lorg/apache/http/client/ClientProtocolException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2

    .line 69
    const-wide/16 v2, 0xa

    :try_start_1
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lorg/apache/http/client/ClientProtocolException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    .line 75
    :try_start_2
    iget-boolean v1, p0, Lcom/samsung/android/app/migration/snbdownload/network/downsync/http/HttpClientThread;->mbClosed:Z

    if-nez v1, :cond_1

    .line 76
    invoke-direct {p0}, Lcom/samsung/android/app/migration/snbdownload/network/downsync/http/HttpClientThread;->responseOK()V

    .line 89
    :cond_0
    :goto_0
    return-void

    .line 70
    :catch_0
    move-exception v0

    .line 71
    .local v0, "e":Ljava/lang/InterruptedException;
    iget-object v1, p0, Lcom/samsung/android/app/migration/snbdownload/network/downsync/http/HttpClientThread;->mHttpUriRequest:Lorg/apache/http/client/methods/HttpUriRequest;

    invoke-interface {v1}, Lorg/apache/http/client/methods/HttpUriRequest;->abort()V

    .line 72
    const/4 v1, 0x2

    invoke-direct {p0, v1}, Lcom/samsung/android/app/migration/snbdownload/network/downsync/http/HttpClientThread;->removeThis(I)V
    :try_end_2
    .catch Lorg/apache/http/client/ClientProtocolException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_0

    .line 81
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :catch_1
    move-exception v0

    .line 82
    .local v0, "e":Lorg/apache/http/client/ClientProtocolException;
    iget-object v1, p0, Lcom/samsung/android/app/migration/snbdownload/network/downsync/http/HttpClientThread;->mHttpUriRequest:Lorg/apache/http/client/methods/HttpUriRequest;

    invoke-interface {v1}, Lorg/apache/http/client/methods/HttpUriRequest;->abort()V

    .line 83
    invoke-direct {p0, v4}, Lcom/samsung/android/app/migration/snbdownload/network/downsync/http/HttpClientThread;->removeThis(I)V

    goto :goto_0

    .line 78
    .end local v0    # "e":Lorg/apache/http/client/ClientProtocolException;
    :cond_1
    const/4 v1, 0x2

    :try_start_3
    invoke-direct {p0, v1}, Lcom/samsung/android/app/migration/snbdownload/network/downsync/http/HttpClientThread;->removeThis(I)V
    :try_end_3
    .catch Lorg/apache/http/client/ClientProtocolException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    goto :goto_0

    .line 84
    :catch_2
    move-exception v0

    .line 85
    .local v0, "e":Ljava/io/IOException;
    iget-object v1, p0, Lcom/samsung/android/app/migration/snbdownload/network/downsync/http/HttpClientThread;->mHttpUriRequest:Lorg/apache/http/client/methods/HttpUriRequest;

    invoke-interface {v1}, Lorg/apache/http/client/methods/HttpUriRequest;->abort()V

    .line 86
    invoke-direct {p0, v4}, Lcom/samsung/android/app/migration/snbdownload/network/downsync/http/HttpClientThread;->removeThis(I)V

    goto :goto_0
.end method
