.class Lcom/samsung/android/app/memo/voice/VoiceViewLayout$7;
.super Ljava/lang/Object;
.source "VoiceViewLayout.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/memo/voice/VoiceViewLayout;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$7;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    .line 1529
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 1532
    iget-object v1, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$7;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    # getter for: Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mVoiceManger:Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;
    invoke-static {v1}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->access$9(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;

    move-result-object v1

    if-nez v1, :cond_1

    .line 1544
    :cond_0
    :goto_0
    return-void

    .line 1534
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$7;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    const/4 v2, 0x1

    iput-boolean v2, v1, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mIsNotificationStart:Z

    .line 1535
    iget-object v1, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$7;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    # getter for: Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mVoiceinfo:Lcom/samsung/android/app/memo/voice/VoiceInfo;
    invoke-static {v1}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->access$0(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)Lcom/samsung/android/app/memo/voice/VoiceInfo;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1536
    iget-object v1, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$7;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    # getter for: Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mVoiceManger:Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;
    invoke-static {v1}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->access$9(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$7;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    # getter for: Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mVoiceinfo:Lcom/samsung/android/app/memo/voice/VoiceInfo;
    invoke-static {v2}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->access$0(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)Lcom/samsung/android/app/memo/voice/VoiceInfo;

    move-result-object v2

    .line 1537
    iget-object v3, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$7;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    iget-object v3, v3, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mRecordListener:Lcom/samsung/android/app/memo/voice/VoiceRecorderBase$RecordListener;

    .line 1536
    invoke-virtual {v1, v2, v3}, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->startRecording(Lcom/samsung/android/app/memo/voice/VoiceInfo;Lcom/samsung/android/app/memo/voice/VoiceRecorderBase$RecordListener;)Z

    move-result v0

    .line 1538
    .local v0, "res":Z
    iget-object v1, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$7;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    invoke-virtual {v1}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->setRecordingLayout()V

    .line 1540
    if-nez v0, :cond_0

    .line 1541
    iget-object v1, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$7;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    invoke-virtual {v1}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->cleanUp()V

    goto :goto_0
.end method
