.class final enum Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MIME_TYPE;
.super Ljava/lang/Enum;
.source "MemoListFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/memo/uifragment/MemoListFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "MIME_TYPE"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MIME_TYPE;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum APPLICATION_MEMO:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MIME_TYPE;

.field public static final enum AUDIO_ALL:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MIME_TYPE;

.field public static final enum AUDIO_MP4:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MIME_TYPE;

.field private static final synthetic ENUM$VALUES:[Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MIME_TYPE;

.field public static final enum IMAGE_ALL:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MIME_TYPE;

.field public static final enum INVALID:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MIME_TYPE;

.field public static final enum TEXT_IMAGE_AUDIO:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MIME_TYPE;

.field public static final enum TEXT_PLAIN:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MIME_TYPE;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 167
    new-instance v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MIME_TYPE;

    const-string v1, "IMAGE_ALL"

    invoke-direct {v0, v1, v3}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MIME_TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MIME_TYPE;->IMAGE_ALL:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MIME_TYPE;

    new-instance v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MIME_TYPE;

    const-string v1, "TEXT_PLAIN"

    invoke-direct {v0, v1, v4}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MIME_TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MIME_TYPE;->TEXT_PLAIN:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MIME_TYPE;

    new-instance v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MIME_TYPE;

    const-string v1, "AUDIO_ALL"

    invoke-direct {v0, v1, v5}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MIME_TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MIME_TYPE;->AUDIO_ALL:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MIME_TYPE;

    new-instance v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MIME_TYPE;

    const-string v1, "AUDIO_MP4"

    invoke-direct {v0, v1, v6}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MIME_TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MIME_TYPE;->AUDIO_MP4:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MIME_TYPE;

    new-instance v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MIME_TYPE;

    const-string v1, "TEXT_IMAGE_AUDIO"

    invoke-direct {v0, v1, v7}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MIME_TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MIME_TYPE;->TEXT_IMAGE_AUDIO:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MIME_TYPE;

    new-instance v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MIME_TYPE;

    const-string v1, "APPLICATION_MEMO"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MIME_TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MIME_TYPE;->APPLICATION_MEMO:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MIME_TYPE;

    new-instance v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MIME_TYPE;

    const-string v1, "INVALID"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MIME_TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MIME_TYPE;->INVALID:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MIME_TYPE;

    .line 166
    const/4 v0, 0x7

    new-array v0, v0, [Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MIME_TYPE;

    sget-object v1, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MIME_TYPE;->IMAGE_ALL:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MIME_TYPE;

    aput-object v1, v0, v3

    sget-object v1, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MIME_TYPE;->TEXT_PLAIN:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MIME_TYPE;

    aput-object v1, v0, v4

    sget-object v1, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MIME_TYPE;->AUDIO_ALL:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MIME_TYPE;

    aput-object v1, v0, v5

    sget-object v1, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MIME_TYPE;->AUDIO_MP4:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MIME_TYPE;

    aput-object v1, v0, v6

    sget-object v1, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MIME_TYPE;->TEXT_IMAGE_AUDIO:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MIME_TYPE;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MIME_TYPE;->APPLICATION_MEMO:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MIME_TYPE;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MIME_TYPE;->INVALID:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MIME_TYPE;

    aput-object v2, v0, v1

    sput-object v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MIME_TYPE;->ENUM$VALUES:[Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MIME_TYPE;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 166
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MIME_TYPE;
    .locals 1

    .prologue
    .line 1
    const-class v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MIME_TYPE;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MIME_TYPE;

    return-object v0
.end method

.method public static values()[Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MIME_TYPE;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MIME_TYPE;->ENUM$VALUES:[Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MIME_TYPE;

    array-length v1, v0

    new-array v2, v1, [Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MIME_TYPE;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method
