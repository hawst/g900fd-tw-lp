.class Lcom/samsung/android/app/memo/voice/VoiceViewLayout$2;
.super Ljava/lang/Object;
.source "VoiceViewLayout.java"

# interfaces
.implements Lcom/samsung/android/app/memo/voice/VoiceRecorderBase$SoundListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/memo/voice/VoiceViewLayout;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$2;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    .line 1044
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFailPlaying()V
    .locals 2

    .prologue
    .line 1072
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$2;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    sget-object v1, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$State;->STATE_PLAY_STOP:Lcom/samsung/android/app/memo/voice/VoiceViewLayout$State;

    invoke-static {v0, v1}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->access$2(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;Lcom/samsung/android/app/memo/voice/VoiceViewLayout$State;)V

    .line 1073
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$2;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    invoke-virtual {v0}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->hideNotification()V

    .line 1074
    return-void
.end method

.method public onPausePlaying()V
    .locals 3

    .prologue
    .line 1078
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$2;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    sget-object v1, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$State;->STATE_PLAY_PAUSE:Lcom/samsung/android/app/memo/voice/VoiceViewLayout$State;

    invoke-static {v0, v1}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->access$2(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;Lcom/samsung/android/app/memo/voice/VoiceViewLayout$State;)V

    .line 1079
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$2;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    # getter for: Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mRemoteViews:Landroid/widget/RemoteViews;
    invoke-static {v0}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->access$8(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)Landroid/widget/RemoteViews;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/samsung/android/app/memo/MemoApp;->isShowNotificationEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1080
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$2;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    # getter for: Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mRemoteViews:Landroid/widget/RemoteViews;
    invoke-static {v0}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->access$8(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)Landroid/widget/RemoteViews;

    move-result-object v0

    const v1, 0x7f0e006a

    const/16 v2, 0x8

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 1081
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$2;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    # getter for: Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mRemoteViews:Landroid/widget/RemoteViews;
    invoke-static {v0}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->access$8(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)Landroid/widget/RemoteViews;

    move-result-object v0

    const v1, 0x7f0e0069

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 1082
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$2;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->showVoiceNotification(Z)V

    .line 1084
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$2;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    # getter for: Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mVoiceManger:Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;
    invoke-static {v0}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->access$9(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->pauseSoundPlay()V

    .line 1085
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$2;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    # getter for: Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mPlayingButton:Landroid/widget/ImageButton;
    invoke-static {v0}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->access$7(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)Landroid/widget/ImageButton;

    move-result-object v0

    const v1, 0x7f0200ad

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 1086
    return-void
.end method

.method public onStartPlaying()V
    .locals 3

    .prologue
    .line 1060
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$2;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    # getter for: Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mPlayingButton:Landroid/widget/ImageButton;
    invoke-static {v0}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->access$7(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)Landroid/widget/ImageButton;

    move-result-object v0

    const v1, 0x7f0200ac

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 1061
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$2;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    sget-object v1, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$State;->STATE_PLAY:Lcom/samsung/android/app/memo/voice/VoiceViewLayout$State;

    invoke-static {v0, v1}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->access$2(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;Lcom/samsung/android/app/memo/voice/VoiceViewLayout$State;)V

    .line 1062
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$2;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    # getter for: Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mRemoteViews:Landroid/widget/RemoteViews;
    invoke-static {v0}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->access$8(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)Landroid/widget/RemoteViews;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/samsung/android/app/memo/MemoApp;->isShowNotificationEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1063
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$2;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    # getter for: Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mRemoteViews:Landroid/widget/RemoteViews;
    invoke-static {v0}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->access$8(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)Landroid/widget/RemoteViews;

    move-result-object v0

    const v1, 0x7f0e0069

    const/16 v2, 0x8

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 1064
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$2;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    # getter for: Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mRemoteViews:Landroid/widget/RemoteViews;
    invoke-static {v0}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->access$8(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)Landroid/widget/RemoteViews;

    move-result-object v0

    const v1, 0x7f0e006a

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 1065
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$2;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->showVoiceNotification(Z)V

    .line 1067
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$2;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    invoke-virtual {v0}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->updateTimerView()V

    .line 1068
    return-void
.end method

.method public onStopPlaying()V
    .locals 4

    .prologue
    .line 1048
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$2;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    sget-object v1, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$State;->STATE_PLAY_STOP:Lcom/samsung/android/app/memo/voice/VoiceViewLayout$State;

    invoke-static {v0, v1}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->access$2(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;Lcom/samsung/android/app/memo/voice/VoiceViewLayout$State;)V

    .line 1049
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$2;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    # getter for: Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->isCompletioning:Z
    invoke-static {v0}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->access$4(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1050
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$2;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    invoke-virtual {v0}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->setReadyToPlayLayout()V

    .line 1051
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$2;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    const-wide/16 v2, 0x0

    invoke-static {v0, v2, v3}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->access$5(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;J)V

    .line 1052
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$2;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    invoke-virtual {v0}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->updateTimerView()V

    .line 1053
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$2;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    # getter for: Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mPlayingSeekbar:Landroid/widget/SeekBar;
    invoke-static {v0}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->access$6(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)Landroid/widget/SeekBar;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1054
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$2;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    # getter for: Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mPlayingSeekbar:Landroid/widget/SeekBar;
    invoke-static {v0}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->access$6(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)Landroid/widget/SeekBar;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 1055
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$2;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    invoke-virtual {v0}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->hideNotification()V

    .line 1056
    return-void
.end method
