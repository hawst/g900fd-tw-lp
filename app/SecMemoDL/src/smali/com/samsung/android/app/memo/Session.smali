.class public final Lcom/samsung/android/app/memo/Session;
.super Ljava/lang/Object;
.source "Session.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/app/memo/Session$SessionException;
    }
.end annotation


# static fields
.field private static final CID_CATEGORY_UUID:I = 0x5

.field private static final CID_CONTENT:I = 0x7

.field private static final CID_CREATED_AT:I = 0x3

.field private static final CID_DATA:I = 0x9

.field private static final CID_ID:I = 0x0

.field private static final CID_IS_DELETED:I = 0x2

.field private static final CID_LAST_MODIFIED_AT:I = 0x4

.field private static final CID_TITLE:I = 0x6

.field private static final CID_UUID:I = 0x1

.field private static final CID_VR_FILE_UUID:I = 0x8

.field public static final EMPTY_CONTENT:Ljava/lang/String; = ""

.field private static final KEY_CONTENT:Ljava/lang/String;

.field private static final KEY_CREATEDAT:Ljava/lang/String;

.field private static final KEY_CTGR_UUID:Ljava/lang/String;

.field private static final KEY_DATA:Ljava/lang/String;

.field private static final KEY_FILESNEWLYADDED:Ljava/lang/String;

.field private static final KEY_FILESTOBEDELETED:Ljava/lang/String;

.field private static final KEY_HAS_INSTANCE:Ljava/lang/String;

.field private static final KEY_ID:Ljava/lang/String;

.field private static final KEY_INITIALFILES:Ljava/lang/String;

.field private static final KEY_ISDIRTY:Ljava/lang/String;

.field private static final KEY_LASTMODAT:Ljava/lang/String;

.field private static final KEY_PHONENUM:Ljava/lang/String;

.field private static final KEY_TITLE:Ljava/lang/String;

.field private static final KEY_UUID:Ljava/lang/String;

.field private static final KEY_VRFILE_UUID:Ljava/lang/String;

.field private static final PROJECTION:[Ljava/lang/String;

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mCategoryUUID:Ljava/lang/String;

.field private mContent:Ljava/lang/String;

.field private mCreatedAt:J

.field private mData:Ljava/lang/String;

.field private mFileUUIDsNewlyAdded:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mFileUUIDsToBeRemoved:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mFinalFileUUIDsList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mId:J

.field private mInitialFileUUIDsList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mIsDirty:Z

.field private mLastModifiedAt:J

.field private mPhoneNum:Ljava/lang/String;

.field private mTitle:Ljava/lang/String;

.field private mUUID:Ljava/lang/String;

.field private mVrFileUUID:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 63
    const-class v0, Lcom/samsung/android/app/memo/Session;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/app/memo/Session;->TAG:Ljava/lang/String;

    .line 65
    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/samsung/android/app/memo/Session;->TAG:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "_instance"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/app/memo/Session;->KEY_HAS_INSTANCE:Ljava/lang/String;

    .line 67
    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/samsung/android/app/memo/Session;->TAG:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "_fileUUIDsNewlyAdded"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/app/memo/Session;->KEY_FILESNEWLYADDED:Ljava/lang/String;

    .line 69
    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/samsung/android/app/memo/Session;->TAG:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "_InitialFileUUIDsList"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/app/memo/Session;->KEY_INITIALFILES:Ljava/lang/String;

    .line 70
    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/samsung/android/app/memo/Session;->TAG:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "_fileUUIDsToBeRemoved"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/app/memo/Session;->KEY_FILESTOBEDELETED:Ljava/lang/String;

    .line 72
    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/samsung/android/app/memo/Session;->TAG:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "_Id"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/app/memo/Session;->KEY_ID:Ljava/lang/String;

    .line 73
    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/samsung/android/app/memo/Session;->TAG:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "_UUID"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/app/memo/Session;->KEY_UUID:Ljava/lang/String;

    .line 74
    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/samsung/android/app/memo/Session;->TAG:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "_isDirty"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/app/memo/Session;->KEY_ISDIRTY:Ljava/lang/String;

    .line 75
    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/samsung/android/app/memo/Session;->TAG:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "_createdAt"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/app/memo/Session;->KEY_CREATEDAT:Ljava/lang/String;

    .line 76
    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/samsung/android/app/memo/Session;->TAG:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "_lastModifiedAt"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/app/memo/Session;->KEY_LASTMODAT:Ljava/lang/String;

    .line 77
    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/samsung/android/app/memo/Session;->TAG:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "_categoryUUID"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/app/memo/Session;->KEY_CTGR_UUID:Ljava/lang/String;

    .line 78
    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/samsung/android/app/memo/Session;->TAG:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "_title"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/app/memo/Session;->KEY_TITLE:Ljava/lang/String;

    .line 79
    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/samsung/android/app/memo/Session;->TAG:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "_content"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/app/memo/Session;->KEY_CONTENT:Ljava/lang/String;

    .line 80
    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/samsung/android/app/memo/Session;->TAG:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "_vrFileUUID"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/app/memo/Session;->KEY_VRFILE_UUID:Ljava/lang/String;

    .line 81
    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/samsung/android/app/memo/Session;->TAG:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "_data"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/app/memo/Session;->KEY_DATA:Ljava/lang/String;

    .line 82
    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/samsung/android/app/memo/Session;->TAG:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "_phoneNum"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/app/memo/Session;->KEY_PHONENUM:Ljava/lang/String;

    .line 85
    const/16 v0, 0xa

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    .line 86
    const-string v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    .line 87
    const-string v2, "UUID"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    .line 88
    const-string v2, "isDeleted"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    .line 89
    const-string v2, "createdAt"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    .line 90
    const-string v2, "lastModifiedAt"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    .line 91
    const-string v2, "categoryUUID"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    .line 92
    const-string v2, "title"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    .line 93
    const-string v2, "content"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    .line 94
    const-string v2, "vrfileUUID"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    .line 95
    const-string v2, "_data"

    aput-object v2, v0, v1

    .line 85
    sput-object v0, Lcom/samsung/android/app/memo/Session;->PROJECTION:[Ljava/lang/String;

    .line 112
    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    .line 188
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 189
    sget-object v0, Lcom/samsung/android/app/memo/Session;->TAG:Ljava/lang/String;

    sget-object v1, Lcom/samsung/android/app/memo/Session;->TAG:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 191
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/app/memo/Session;->mFileUUIDsNewlyAdded:Ljava/util/ArrayList;

    .line 192
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/app/memo/Session;->mFileUUIDsToBeRemoved:Ljava/util/ArrayList;

    .line 193
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/app/memo/Session;->mInitialFileUUIDsList:Ljava/util/ArrayList;

    .line 194
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/app/memo/Session;->mFinalFileUUIDsList:Ljava/util/ArrayList;

    .line 196
    return-void
.end method

.method public constructor <init>(J)V
    .locals 3
    .param p1, "id"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/android/app/memo/Session$SessionException;
        }
    .end annotation

    .prologue
    .line 205
    invoke-direct {p0}, Lcom/samsung/android/app/memo/Session;-><init>()V

    .line 206
    sget-object v0, Lcom/samsung/android/app/memo/Session;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Session() id: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 207
    const-wide/16 v0, -0x1

    cmp-long v0, p1, v0

    if-nez v0, :cond_0

    .line 208
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/Session;->startNew()V

    .line 212
    :goto_0
    return-void

    .line 210
    :cond_0
    const/4 v0, 0x0

    invoke-direct {p0, v0, p1, p2}, Lcom/samsung/android/app/memo/Session;->loadMemo(Ljava/lang/String;J)V

    goto :goto_0
.end method

.method public constructor <init>(Landroid/net/Uri;)V
    .locals 10
    .param p1, "uri"    # Landroid/net/Uri;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/android/app/memo/Session$SessionException;
        }
    .end annotation

    .prologue
    const-wide/16 v8, -0x1

    .line 215
    invoke-direct {p0}, Lcom/samsung/android/app/memo/Session;-><init>()V

    .line 217
    sget-object v4, Lcom/samsung/android/app/memo/Session;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Session() uri: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 218
    if-nez p1, :cond_0

    .line 219
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/Session;->startNew()V

    .line 246
    :goto_0
    return-void

    .line 223
    :cond_0
    const-string v4, "content"

    invoke-virtual {p1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    const-string v4, "com.samsung.android.memo"

    invoke-virtual {p1}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 224
    :cond_1
    new-instance v4, Lcom/samsung/android/app/memo/Session$SessionException;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "unknown uri: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Lcom/samsung/android/app/memo/Session$SessionException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 227
    :cond_2
    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v1

    .line 229
    .local v1, "lastPathSeg":Ljava/lang/String;
    :try_start_0
    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 230
    .local v2, "id":J
    cmp-long v4, v2, v8

    if-nez v4, :cond_3

    .line 231
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/Session;->startNew()V
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 236
    .end local v2    # "id":J
    :catch_0
    move-exception v4

    .line 241
    :try_start_1
    invoke-static {v1}, Lcom/samsung/android/app/memo/util/UUIDHelper;->verify(Ljava/lang/String;)V

    .line 242
    const-wide/16 v4, -0x1

    invoke-direct {p0, v1, v4, v5}, Lcom/samsung/android/app/memo/Session;->loadMemo(Ljava/lang/String;J)V
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 243
    :catch_1
    move-exception v0

    .line 244
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    new-instance v4, Lcom/samsung/android/app/memo/Session$SessionException;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "unknown uri: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5, v0}, Lcom/samsung/android/app/memo/Session$SessionException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v4

    .line 233
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    .restart local v2    # "id":J
    :cond_3
    const/4 v4, 0x0

    :try_start_2
    invoke-direct {p0, v4, v2, v3}, Lcom/samsung/android/app/memo/Session;->loadMemo(Ljava/lang/String;J)V
    :try_end_2
    .catch Ljava/lang/NumberFormatException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 3
    .param p1, "uuid"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/android/app/memo/Session$SessionException;
        }
    .end annotation

    .prologue
    .line 199
    invoke-direct {p0}, Lcom/samsung/android/app/memo/Session;-><init>()V

    .line 200
    sget-object v0, Lcom/samsung/android/app/memo/Session;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Session() uuid: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 201
    const-wide/16 v0, -0x1

    invoke-direct {p0, p1, v0, v1}, Lcom/samsung/android/app/memo/Session;->loadMemo(Ljava/lang/String;J)V

    .line 202
    return-void
.end method

.method private clearDirty()V
    .locals 1

    .prologue
    .line 404
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/app/memo/Session;->mIsDirty:Z

    .line 405
    return-void
.end method

.method public static createInstanceFrom(Landroid/os/Bundle;)Lcom/samsung/android/app/memo/Session;
    .locals 3
    .param p0, "b"    # Landroid/os/Bundle;

    .prologue
    .line 139
    sget-object v1, Lcom/samsung/android/app/memo/Session;->KEY_HAS_INSTANCE:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-nez v1, :cond_0

    .line 140
    const/4 v0, 0x0

    .line 144
    :goto_0
    return-object v0

    .line 142
    :cond_0
    new-instance v0, Lcom/samsung/android/app/memo/Session;

    invoke-direct {v0}, Lcom/samsung/android/app/memo/Session;-><init>()V

    .line 143
    .local v0, "s":Lcom/samsung/android/app/memo/Session;
    invoke-virtual {v0, p0}, Lcom/samsung/android/app/memo/Session;->restoreInstanceFrom(Landroid/os/Bundle;)V

    goto :goto_0
.end method

.method private deleteFiles(Ljava/util/List;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/android/app/memo/Session$SessionException;
        }
    .end annotation

    .prologue
    .line 638
    .local p1, "uuids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v3

    if-nez v3, :cond_0

    .line 653
    :goto_0
    return-void

    .line 641
    :cond_0
    new-instance v1, Ljava/lang/StringBuffer;

    const-string v3, "UUID"

    invoke-direct {v1, v3}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 642
    .local v1, "sb":Ljava/lang/StringBuffer;
    invoke-direct {p0, v1, p1}, Lcom/samsung/android/app/memo/Session;->toSQLInState(Ljava/lang/StringBuffer;Ljava/util/List;)V

    .line 644
    const/4 v0, 0x0

    .line 646
    .local v0, "ret":I
    :try_start_0
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/Session;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    sget-object v4, Lcom/samsung/android/provider/memo/MemoContract;->BASE_URI_FILE:Landroid/net/Uri;

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v3, v4, v5, v6}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 652
    sget-object v3, Lcom/samsung/android/app/memo/Session;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "removeFiles() "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 647
    :catch_0
    move-exception v2

    .line 648
    .local v2, "sqle":Landroid/database/SQLException;
    sget-object v3, Lcom/samsung/android/app/memo/Session;->TAG:Ljava/lang/String;

    const-string v4, "removeFiles"

    invoke-static {v3, v4, v2}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 649
    new-instance v3, Lcom/samsung/android/app/memo/Session$SessionException;

    const-string v4, "fail to removeFiles"

    invoke-direct {v3, v4, v2}, Lcom/samsung/android/app/memo/Session$SessionException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v3
.end method

.method private fromCursor(Landroid/database/Cursor;)V
    .locals 3
    .param p1, "c"    # Landroid/database/Cursor;

    .prologue
    const/4 v2, 0x0

    .line 471
    invoke-interface {p1, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/samsung/android/app/memo/Session;->mId:J

    .line 472
    const/4 v0, 0x1

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/memo/Session;->mUUID:Ljava/lang/String;

    .line 473
    iput-boolean v2, p0, Lcom/samsung/android/app/memo/Session;->mIsDirty:Z

    .line 474
    const/4 v0, 0x3

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/samsung/android/app/memo/Session;->mCreatedAt:J

    .line 475
    const/4 v0, 0x4

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/samsung/android/app/memo/Session;->mLastModifiedAt:J

    .line 476
    const/4 v0, 0x5

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/memo/Session;->mCategoryUUID:Ljava/lang/String;

    .line 477
    const/4 v0, 0x6

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/memo/Session;->mTitle:Ljava/lang/String;

    .line 478
    const/4 v0, 0x7

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/memo/Session;->mContent:Ljava/lang/String;

    .line 479
    const/16 v0, 0x8

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/memo/Session;->mVrFileUUID:Ljava/lang/String;

    .line 480
    const/16 v0, 0x9

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/memo/Session;->mData:Ljava/lang/String;

    .line 481
    iget-object v0, p0, Lcom/samsung/android/app/memo/Session;->mContent:Ljava/lang/String;

    invoke-static {v0}, Lcom/samsung/android/app/memo/util/ImageUtil;->getImageUUIDList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/memo/Session;->mInitialFileUUIDsList:Ljava/util/ArrayList;

    .line 482
    return-void
.end method

.method private insertNew()J
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/android/app/memo/Session$SessionException;
        }
    .end annotation

    .prologue
    .line 424
    const-wide/16 v0, -0x1

    .line 426
    .local v0, "ret":J
    :try_start_0
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/Session;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    sget-object v5, Lcom/samsung/android/provider/memo/MemoContract;->BASE_URI_MEMO:Landroid/net/Uri;

    invoke-direct {p0}, Lcom/samsung/android/app/memo/Session;->toContentValues()Landroid/content/ContentValues;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v3

    .line 427
    .local v3, "uri":Landroid/net/Uri;
    invoke-virtual {v3}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v0

    .line 432
    return-wide v0

    .line 428
    .end local v3    # "uri":Landroid/net/Uri;
    :catch_0
    move-exception v2

    .line 429
    .local v2, "sqle":Landroid/database/SQLException;
    sget-object v4, Lcom/samsung/android/app/memo/Session;->TAG:Ljava/lang/String;

    const-string v5, "insertNew()"

    invoke-static {v4, v5, v2}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 430
    new-instance v4, Lcom/samsung/android/app/memo/Session$SessionException;

    const-string v5, "fail to insert"

    invoke-direct {v4, v5, v2}, Lcom/samsung/android/app/memo/Session$SessionException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v4
.end method

.method private loadMemo(Ljava/lang/String;J)V
    .locals 8
    .param p1, "uuid"    # Ljava/lang/String;
    .param p2, "id"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/android/app/memo/Session$SessionException;
        }
    .end annotation

    .prologue
    .line 450
    if-nez p1, :cond_2

    .line 451
    sget-object v0, Lcom/samsung/android/provider/memo/MemoContract;->BASE_URI_MEMO:Landroid/net/Uri;

    invoke-static {v0, p2, p3}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v1

    .line 456
    .local v1, "uri":Landroid/net/Uri;
    :goto_0
    const/4 v6, 0x0

    .line 458
    .local v6, "c":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/Session;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v2, Lcom/samsung/android/app/memo/Session;->PROJECTION:[Ljava/lang/String;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 459
    if-eqz v6, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-nez v0, :cond_3

    .line 460
    :cond_0
    new-instance v0, Landroid/database/SQLException;

    const-string v2, "empty"

    invoke-direct {v0, v2}, Landroid/database/SQLException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 462
    :catch_0
    move-exception v7

    .line 463
    .local v7, "sqle":Landroid/database/SQLException;
    :try_start_1
    sget-object v0, Lcom/samsung/android/app/memo/Session;->TAG:Ljava/lang/String;

    const-string v2, "insertNew()"

    invoke-static {v0, v2, v7}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 464
    new-instance v0, Lcom/samsung/android/app/memo/Session$SessionException;

    const-string v2, "fail to load"

    invoke-direct {v0, v2, v7}, Lcom/samsung/android/app/memo/Session$SessionException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 465
    .end local v7    # "sqle":Landroid/database/SQLException;
    :catchall_0
    move-exception v0

    .line 466
    if-eqz v6, :cond_1

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 467
    :cond_1
    throw v0

    .line 453
    .end local v1    # "uri":Landroid/net/Uri;
    .end local v6    # "c":Landroid/database/Cursor;
    :cond_2
    sget-object v0, Lcom/samsung/android/provider/memo/MemoContract;->BASE_URI_MEMO:Landroid/net/Uri;

    invoke-static {v0, p1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .restart local v1    # "uri":Landroid/net/Uri;
    goto :goto_0

    .line 461
    .restart local v6    # "c":Landroid/database/Cursor;
    :cond_3
    :try_start_2
    invoke-direct {p0, v6}, Lcom/samsung/android/app/memo/Session;->fromCursor(Landroid/database/Cursor;)V
    :try_end_2
    .catch Landroid/database/SQLException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 466
    if-eqz v6, :cond_4

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 468
    :cond_4
    return-void
.end method

.method private setDirty()V
    .locals 2

    .prologue
    .line 399
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/app/memo/Session;->mIsDirty:Z

    .line 400
    invoke-static {}, Lcom/samsung/android/app/memo/util/Utils;->getCurrentUTCTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/samsung/android/app/memo/Session;->mLastModifiedAt:J

    .line 401
    return-void
.end method

.method private setFileUUIDsToBeRemoved()V
    .locals 3

    .prologue
    .line 684
    iget-object v1, p0, Lcom/samsung/android/app/memo/Session;->mInitialFileUUIDsList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_1

    .line 690
    return-void

    .line 684
    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 685
    .local v0, "uuid":Ljava/lang/String;
    iget-object v2, p0, Lcom/samsung/android/app/memo/Session;->mFinalFileUUIDsList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_2

    .line 686
    iget-object v2, p0, Lcom/samsung/android/app/memo/Session;->mFinalFileUUIDsList:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 687
    :cond_2
    iget-object v2, p0, Lcom/samsung/android/app/memo/Session;->mFileUUIDsToBeRemoved:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method private toContentValues()Landroid/content/ContentValues;
    .locals 4

    .prologue
    .line 408
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 409
    .local v0, "values":Landroid/content/ContentValues;
    const-string v1, "UUID"

    iget-object v2, p0, Lcom/samsung/android/app/memo/Session;->mUUID:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 410
    const-string v1, "createdAt"

    iget-wide v2, p0, Lcom/samsung/android/app/memo/Session;->mCreatedAt:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 411
    const-string v1, "lastModifiedAt"

    iget-wide v2, p0, Lcom/samsung/android/app/memo/Session;->mLastModifiedAt:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 412
    const-string v1, "categoryUUID"

    iget-object v2, p0, Lcom/samsung/android/app/memo/Session;->mCategoryUUID:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 413
    const-string v1, "title"

    iget-object v2, p0, Lcom/samsung/android/app/memo/Session;->mTitle:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 414
    const-string v1, "content"

    iget-object v2, p0, Lcom/samsung/android/app/memo/Session;->mContent:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 415
    const-string v1, "vrfileUUID"

    iget-object v2, p0, Lcom/samsung/android/app/memo/Session;->mVrFileUUID:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 416
    const-string v1, "_data"

    iget-object v2, p0, Lcom/samsung/android/app/memo/Session;->mData:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 417
    iget-object v1, p0, Lcom/samsung/android/app/memo/Session;->mPhoneNum:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 418
    const-string v1, "_phoneNum"

    iget-object v2, p0, Lcom/samsung/android/app/memo/Session;->mPhoneNum:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 419
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/app/memo/Session;->mContent:Ljava/lang/String;

    invoke-static {v1}, Lcom/samsung/android/app/memo/util/ImageUtil;->getImageUUIDList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/app/memo/Session;->mFinalFileUUIDsList:Ljava/util/ArrayList;

    .line 420
    return-object v0
.end method

.method private toSQLInState(Ljava/lang/StringBuffer;Ljava/util/List;)V
    .locals 5
    .param p1, "sb"    # Ljava/lang/StringBuffer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/StringBuffer;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p2, "uuids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/16 v4, 0x27

    .line 676
    const-string v1, " IN ("

    invoke-virtual {p1, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 677
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_0

    .line 680
    invoke-virtual {p1}, Ljava/lang/StringBuffer;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    const/16 v2, 0x29

    invoke-virtual {p1, v1, v2}, Ljava/lang/StringBuffer;->setCharAt(IC)V

    .line 681
    return-void

    .line 677
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 678
    .local v0, "uuid":Ljava/lang/String;
    invoke-virtual {p1, v4}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2, v4}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    move-result-object v2

    const/16 v3, 0x2c

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    goto :goto_0
.end method

.method private updateCurrent()J
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/android/app/memo/Session$SessionException;
        }
    .end annotation

    .prologue
    .line 437
    :try_start_0
    sget-object v3, Lcom/samsung/android/provider/memo/MemoContract;->BASE_URI_MEMO:Landroid/net/Uri;

    iget-wide v4, p0, Lcom/samsung/android/app/memo/Session;->mId:J

    invoke-static {v3, v4, v5}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v2

    .line 438
    .local v2, "uri":Landroid/net/Uri;
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/Session;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    invoke-direct {p0}, Lcom/samsung/android/app/memo/Session;->toContentValues()Landroid/content/ContentValues;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual {v3, v2, v4, v5, v6}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 439
    .local v0, "count":I
    const/4 v3, 0x1

    if-eq v0, v3, :cond_0

    .line 440
    new-instance v3, Landroid/database/SQLException;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "fail to update id: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v6, p0, Lcom/samsung/android/app/memo/Session;->mId:J

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Landroid/database/SQLException;-><init>(Ljava/lang/String;)V

    throw v3
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    .line 441
    .end local v0    # "count":I
    .end local v2    # "uri":Landroid/net/Uri;
    :catch_0
    move-exception v1

    .line 442
    .local v1, "sqle":Landroid/database/SQLException;
    sget-object v3, Lcom/samsung/android/app/memo/Session;->TAG:Ljava/lang/String;

    const-string v4, "insertNew()"

    invoke-static {v3, v4, v1}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 443
    new-instance v3, Lcom/samsung/android/app/memo/Session$SessionException;

    const-string v4, "fail to update"

    invoke-direct {v3, v4, v1}, Lcom/samsung/android/app/memo/Session$SessionException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v3

    .line 445
    .end local v1    # "sqle":Landroid/database/SQLException;
    .restart local v0    # "count":I
    .restart local v2    # "uri":Landroid/net/Uri;
    :cond_0
    iget-wide v4, p0, Lcom/samsung/android/app/memo/Session;->mId:J

    return-wide v4
.end method


# virtual methods
.method public addFile(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;I)Landroid/net/Uri;
    .locals 15
    .param p1, "sourceUri"    # Landroid/net/Uri;
    .param p2, "displayName"    # Ljava/lang/String;
    .param p3, "mimeType"    # Ljava/lang/String;
    .param p4, "orientation"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/android/app/memo/Session$SessionException;
        }
    .end annotation

    .prologue
    .line 536
    invoke-static {}, Lcom/samsung/android/app/memo/util/UUIDHelper;->newUUID()Ljava/lang/String;

    move-result-object v10

    .line 537
    .local v10, "uuid":Ljava/lang/String;
    new-instance v11, Landroid/content/ContentValues;

    invoke-direct {v11}, Landroid/content/ContentValues;-><init>()V

    .line 538
    .local v11, "values":Landroid/content/ContentValues;
    const-string v12, "UUID"

    invoke-virtual {v11, v12, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 539
    const-string v12, "isDirty"

    const/4 v13, 0x1

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    invoke-virtual {v11, v12, v13}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 540
    const-string v12, "isDeleted"

    const/4 v13, 0x0

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    invoke-virtual {v11, v12, v13}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 541
    const-string v12, "memoUUID"

    iget-object v13, p0, Lcom/samsung/android/app/memo/Session;->mUUID:Ljava/lang/String;

    invoke-virtual {v11, v12, v13}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 542
    const-string v12, "mime_type"

    move-object/from16 v0, p3

    invoke-virtual {v11, v12, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 543
    const-string v12, "orientation"

    invoke-static/range {p4 .. p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    invoke-virtual {v11, v12, v13}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 544
    if-eqz p1, :cond_0

    .line 545
    const-string v12, "_srcUri"

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v11, v12, v13}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 547
    :cond_0
    if-eqz p2, :cond_1

    const-string v12, ".m4a"

    move-object/from16 v0, p2

    invoke-virtual {v0, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_1

    .line 548
    const-string v12, "_display_name"

    new-instance v13, Ljava/lang/StringBuilder;

    const-string v14, "memo_voice"

    invoke-direct {v13, v14}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v14

    add-int/lit8 v14, v14, -0x2

    invoke-virtual {v10, v14}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    .line 549
    const-string v14, ".m4a"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    .line 548
    invoke-virtual {v11, v12, v13}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 569
    :goto_0
    :try_start_0
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/Session;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v12

    sget-object v13, Lcom/samsung/android/provider/memo/MemoContract;->BASE_URI_FILE:Landroid/net/Uri;

    invoke-virtual {v12, v13, v11}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v9

    .line 570
    .local v9, "uri":Landroid/net/Uri;
    if-nez v9, :cond_5

    .line 571
    new-instance v12, Landroid/database/SQLException;

    const-string v13, "fail to add file"

    invoke-direct {v12, v13}, Landroid/database/SQLException;-><init>(Ljava/lang/String;)V

    throw v12
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    .line 573
    .end local v9    # "uri":Landroid/net/Uri;
    :catch_0
    move-exception v8

    .line 574
    .local v8, "sqle":Landroid/database/SQLException;
    sget-object v12, Lcom/samsung/android/app/memo/Session;->TAG:Ljava/lang/String;

    const-string v13, "newAttachment-insert"

    invoke-static {v12, v13, v8}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 575
    new-instance v12, Lcom/samsung/android/app/memo/Session$SessionException;

    const-string v13, "fail to addImageFile"

    invoke-direct {v12, v13, v8}, Lcom/samsung/android/app/memo/Session$SessionException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v12

    .line 552
    .end local v8    # "sqle":Landroid/database/SQLException;
    :cond_1
    if-eqz p1, :cond_3

    .line 553
    invoke-static/range {p2 .. p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v12

    if-nez v12, :cond_2

    const-string v12, "null"

    move-object/from16 v0, p2

    invoke-virtual {v0, v12}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v12

    if-eqz v12, :cond_3

    .line 554
    :cond_2
    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object p2

    .line 555
    const-string v5, ".*\\.(png|jpg|bmp|gif|jpeg|wbmp|PNG|JPG|GIF|BMP|JPEG|WBMP)"

    .line 556
    .local v5, "re":Ljava/lang/String;
    invoke-static/range {p2 .. p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v12

    if-nez v12, :cond_4

    .line 557
    move-object/from16 v0, p2

    invoke-virtual {v0, v5}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v12

    if-nez v12, :cond_3

    .line 558
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v13

    invoke-static {v13}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v13

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v13, "."

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    .line 559
    const-string v13, "/"

    move-object/from16 v0, p3

    invoke-virtual {v0, v13}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v13

    add-int/lit8 v13, v13, 0x1

    move-object/from16 v0, p3

    invoke-virtual {v0, v13}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    .line 558
    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    .line 566
    .end local v5    # "re":Ljava/lang/String;
    :cond_3
    :goto_1
    const-string v12, "_display_name"

    move-object/from16 v0, p2

    invoke-virtual {v11, v12, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 561
    .restart local v5    # "re":Ljava/lang/String;
    :cond_4
    new-instance v12, Ljava/lang/StringBuilder;

    const-string v13, "memo_"

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "."

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    .line 562
    const-string v13, "/"

    move-object/from16 v0, p3

    invoke-virtual {v0, v13}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v13

    add-int/lit8 v13, v13, 0x1

    move-object/from16 v0, p3

    invoke-virtual {v0, v13}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    .line 561
    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    goto :goto_1

    .line 572
    .end local v5    # "re":Ljava/lang/String;
    .restart local v9    # "uri":Landroid/net/Uri;
    :cond_5
    :try_start_1
    sget-object v12, Lcom/samsung/android/app/memo/Session;->TAG:Ljava/lang/String;

    new-instance v13, Ljava/lang/StringBuilder;

    const-string v14, "add file id: "

    invoke-direct {v13, v14}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, ", uuid: "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Landroid/database/SQLException; {:try_start_1 .. :try_end_1} :catch_0

    .line 578
    iget-object v12, p0, Lcom/samsung/android/app/memo/Session;->mFileUUIDsNewlyAdded:Ljava/util/ArrayList;

    invoke-virtual {v12, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 580
    sget-object v12, Lcom/samsung/android/provider/memo/MemoContract;->BASE_URI_FILE:Landroid/net/Uri;

    invoke-static {v12, v10}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    .line 581
    .local v6, "returnUri":Landroid/net/Uri;
    if-eqz p2, :cond_7

    const-string v12, ".m4a"

    move-object/from16 v0, p2

    invoke-virtual {v0, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-nez v12, :cond_7

    .line 582
    const/4 v1, 0x0

    .line 583
    .local v1, "bm":Landroid/graphics/Bitmap;
    const/4 v3, 0x0

    .line 586
    .local v3, "is":Ljava/io/InputStream;
    :try_start_2
    new-instance v4, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v4}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 587
    .local v4, "opts":Landroid/graphics/BitmapFactory$Options;
    sget-object v12, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    iput-object v12, v4, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    .line 588
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/Session;->getContext()Landroid/content/Context;

    move-result-object v12

    const/4 v13, 0x0

    invoke-static {v12, v6, v13}, Lcom/samsung/android/app/memo/util/ImageUtil;->getActualImageWidth(Landroid/content/Context;Landroid/net/Uri;I)I

    move-result v7

    .line 589
    .local v7, "sourcewidth":I
    const/16 v12, 0xc8

    invoke-static {v7, v12}, Lcom/samsung/android/app/memo/util/ImageUtil;->calculateInSampleSize(II)I

    move-result v12

    iput v12, v4, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 590
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/Session;->getContext()Landroid/content/Context;

    move-result-object v12

    invoke-virtual {v12}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v12

    invoke-virtual {v12, v6}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;

    move-result-object v3

    .line 591
    const/4 v12, 0x0

    invoke-static {v3, v12, v4}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_3
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v1

    .line 597
    if-eqz v3, :cond_6

    .line 599
    :try_start_3
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_6

    .line 603
    .end local v4    # "opts":Landroid/graphics/BitmapFactory$Options;
    .end local v7    # "sourcewidth":I
    :cond_6
    :goto_2
    if-nez v1, :cond_9

    .line 604
    const/4 v6, 0x0

    .line 612
    .end local v1    # "bm":Landroid/graphics/Bitmap;
    .end local v3    # "is":Ljava/io/InputStream;
    .end local v6    # "returnUri":Landroid/net/Uri;
    :cond_7
    :goto_3
    return-object v6

    .line 592
    .restart local v1    # "bm":Landroid/graphics/Bitmap;
    .restart local v3    # "is":Ljava/io/InputStream;
    .restart local v6    # "returnUri":Landroid/net/Uri;
    :catch_1
    move-exception v2

    .line 593
    .local v2, "e":Ljava/io/FileNotFoundException;
    :try_start_4
    sget-object v12, Lcom/samsung/android/app/memo/Session;->TAG:Ljava/lang/String;

    const-string v13, "add file "

    invoke-static {v12, v13, v2}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 597
    if-eqz v3, :cond_6

    .line 599
    :try_start_5
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2

    goto :goto_2

    .line 600
    :catch_2
    move-exception v12

    goto :goto_2

    .line 594
    .end local v2    # "e":Ljava/io/FileNotFoundException;
    :catch_3
    move-exception v2

    .line 595
    .local v2, "e":Ljava/io/IOException;
    :try_start_6
    sget-object v12, Lcom/samsung/android/app/memo/Session;->TAG:Ljava/lang/String;

    const-string v13, "add file "

    invoke-static {v12, v13, v2}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 597
    if-eqz v3, :cond_6

    .line 599
    :try_start_7
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_4

    goto :goto_2

    .line 600
    :catch_4
    move-exception v12

    goto :goto_2

    .line 596
    .end local v2    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v12

    .line 597
    if-eqz v3, :cond_8

    .line 599
    :try_start_8
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_5

    .line 602
    :cond_8
    :goto_4
    throw v12

    .line 606
    :cond_9
    move/from16 v0, p4

    invoke-static {v10, v0}, Lcom/samsung/android/app/memo/util/ImageUtil;->updateExifInterface(Ljava/lang/String;I)V

    .line 607
    invoke-virtual {v6}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {p0, v12}, Lcom/samsung/android/app/memo/Session;->removeFile(Ljava/lang/String;)Z

    .line 608
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V

    goto :goto_3

    .line 600
    :catch_5
    move-exception v13

    goto :goto_4

    .restart local v4    # "opts":Landroid/graphics/BitmapFactory$Options;
    .restart local v7    # "sourcewidth":I
    :catch_6
    move-exception v12

    goto :goto_2
.end method

.method public declared-synchronized cancelEditing()V
    .locals 4

    .prologue
    .line 501
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/app/memo/Session;->mFileUUIDsNewlyAdded:Ljava/util/ArrayList;

    invoke-direct {p0, v1}, Lcom/samsung/android/app/memo/Session;->deleteFiles(Ljava/util/List;)V

    .line 502
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/Session;->isDraft()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 503
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/Session;->startNew()V
    :try_end_0
    .catch Lcom/samsung/android/app/memo/Session$SessionException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 510
    :goto_0
    monitor-exit p0

    return-void

    .line 505
    :cond_0
    const/4 v1, 0x0

    :try_start_1
    iget-wide v2, p0, Lcom/samsung/android/app/memo/Session;->mId:J

    invoke-direct {p0, v1, v2, v3}, Lcom/samsung/android/app/memo/Session;->loadMemo(Ljava/lang/String;J)V
    :try_end_1
    .catch Lcom/samsung/android/app/memo/Session$SessionException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 507
    :catch_0
    move-exception v0

    .line 508
    .local v0, "se":Lcom/samsung/android/app/memo/Session$SessionException;
    :try_start_2
    sget-object v1, Lcom/samsung/android/app/memo/Session;->TAG:Ljava/lang/String;

    const-string v2, "cancelEditing"

    invoke-static {v1, v2, v0}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 501
    .end local v0    # "se":Lcom/samsung/android/app/memo/Session$SessionException;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public deletevoiceFile(Ljava/lang/String;)V
    .locals 8
    .param p1, "uuids"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/android/app/memo/Session$SessionException;
        }
    .end annotation

    .prologue
    .line 656
    new-instance v1, Ljava/lang/StringBuffer;

    const-string v4, "UUID"

    invoke-direct {v1, v4}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 657
    .local v1, "sb":Ljava/lang/StringBuffer;
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 658
    .local v3, "vUUID":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual {v3, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 659
    invoke-direct {p0, v1, v3}, Lcom/samsung/android/app/memo/Session;->toSQLInState(Ljava/lang/StringBuffer;Ljava/util/List;)V

    .line 660
    const/4 v0, 0x0

    .line 662
    .local v0, "ret":I
    :try_start_0
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/Session;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    sget-object v5, Lcom/samsung/android/provider/memo/MemoContract;->BASE_URI_FILE:Landroid/net/Uri;

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    invoke-virtual {v4, v5, v6, v7}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 667
    const-string v4, ""

    iput-object v4, p0, Lcom/samsung/android/app/memo/Session;->mVrFileUUID:Ljava/lang/String;

    .line 668
    iget-wide v4, p0, Lcom/samsung/android/app/memo/Session;->mId:J

    const-wide/16 v6, 0x0

    cmp-long v4, v4, v6

    if-lez v4, :cond_0

    .line 669
    invoke-direct {p0}, Lcom/samsung/android/app/memo/Session;->setDirty()V

    .line 672
    :goto_0
    sget-object v4, Lcom/samsung/android/app/memo/Session;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "removeVoiceFiles() "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 673
    return-void

    .line 663
    :catch_0
    move-exception v2

    .line 664
    .local v2, "sqle":Landroid/database/SQLException;
    sget-object v4, Lcom/samsung/android/app/memo/Session;->TAG:Ljava/lang/String;

    const-string v5, "removeVoiceFiles"

    invoke-static {v4, v5, v2}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 665
    new-instance v4, Lcom/samsung/android/app/memo/Session$SessionException;

    const-string v5, "fail to removeVoiceFiles"

    invoke-direct {v4, v5, v2}, Lcom/samsung/android/app/memo/Session$SessionException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v4

    .line 671
    .end local v2    # "sqle":Landroid/database/SQLException;
    :cond_0
    invoke-direct {p0}, Lcom/samsung/android/app/memo/Session;->clearDirty()V

    goto :goto_0
.end method

.method public getCategoryUUID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 297
    iget-object v0, p0, Lcom/samsung/android/app/memo/Session;->mCategoryUUID:Ljava/lang/String;

    return-object v0
.end method

.method public getContent()Ljava/lang/String;
    .locals 1

    .prologue
    .line 306
    iget-object v0, p0, Lcom/samsung/android/app/memo/Session;->mContent:Ljava/lang/String;

    return-object v0
.end method

.method public getContentResolver()Landroid/content/ContentResolver;
    .locals 1

    .prologue
    .line 253
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/Session;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    return-object v0
.end method

.method public getContext()Landroid/content/Context;
    .locals 1

    .prologue
    .line 249
    invoke-static {}, Lcom/samsung/android/app/memo/util/Utils;->getAppContext()Landroid/content/Context;

    move-result-object v0

    return-object v0
.end method

.method public getCreatedAt()J
    .locals 2

    .prologue
    .line 282
    iget-wide v0, p0, Lcom/samsung/android/app/memo/Session;->mCreatedAt:J

    return-wide v0
.end method

.method public getData()Ljava/lang/String;
    .locals 1

    .prologue
    .line 314
    iget-object v0, p0, Lcom/samsung/android/app/memo/Session;->mData:Ljava/lang/String;

    return-object v0
.end method

.method public getId()J
    .locals 2

    .prologue
    .line 274
    iget-wide v0, p0, Lcom/samsung/android/app/memo/Session;->mId:J

    return-wide v0
.end method

.method public getLastModifiedAt()J
    .locals 2

    .prologue
    .line 286
    iget-wide v0, p0, Lcom/samsung/android/app/memo/Session;->mLastModifiedAt:J

    return-wide v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 302
    iget-object v0, p0, Lcom/samsung/android/app/memo/Session;->mTitle:Ljava/lang/String;

    return-object v0
.end method

.method public getUUID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 278
    iget-object v0, p0, Lcom/samsung/android/app/memo/Session;->mUUID:Ljava/lang/String;

    return-object v0
.end method

.method public getVrFileUUID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 310
    iget-object v0, p0, Lcom/samsung/android/app/memo/Session;->mVrFileUUID:Ljava/lang/String;

    return-object v0
.end method

.method public isDirty()Z
    .locals 1

    .prologue
    .line 327
    iget-boolean v0, p0, Lcom/samsung/android/app/memo/Session;->mIsDirty:Z

    return v0
.end method

.method public isDraft()Z
    .locals 4

    .prologue
    .line 270
    iget-wide v0, p0, Lcom/samsung/android/app/memo/Session;->mId:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "b"    # Landroid/os/Bundle;

    .prologue
    .line 169
    sget-object v0, Lcom/samsung/android/app/memo/Session;->KEY_HAS_INSTANCE:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 171
    sget-object v0, Lcom/samsung/android/app/memo/Session;->KEY_FILESNEWLYADDED:Ljava/lang/String;

    iget-object v1, p0, Lcom/samsung/android/app/memo/Session;->mFileUUIDsNewlyAdded:Ljava/util/ArrayList;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 172
    sget-object v0, Lcom/samsung/android/app/memo/Session;->KEY_FILESTOBEDELETED:Ljava/lang/String;

    iget-object v1, p0, Lcom/samsung/android/app/memo/Session;->mFileUUIDsToBeRemoved:Ljava/util/ArrayList;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 173
    sget-object v0, Lcom/samsung/android/app/memo/Session;->KEY_INITIALFILES:Ljava/lang/String;

    iget-object v1, p0, Lcom/samsung/android/app/memo/Session;->mInitialFileUUIDsList:Ljava/util/ArrayList;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 175
    sget-object v0, Lcom/samsung/android/app/memo/Session;->KEY_ID:Ljava/lang/String;

    iget-wide v2, p0, Lcom/samsung/android/app/memo/Session;->mId:J

    invoke-virtual {p1, v0, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 176
    sget-object v0, Lcom/samsung/android/app/memo/Session;->KEY_UUID:Ljava/lang/String;

    iget-object v1, p0, Lcom/samsung/android/app/memo/Session;->mUUID:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 177
    sget-object v0, Lcom/samsung/android/app/memo/Session;->KEY_ISDIRTY:Ljava/lang/String;

    iget-boolean v1, p0, Lcom/samsung/android/app/memo/Session;->mIsDirty:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 178
    sget-object v0, Lcom/samsung/android/app/memo/Session;->KEY_CREATEDAT:Ljava/lang/String;

    iget-wide v2, p0, Lcom/samsung/android/app/memo/Session;->mCreatedAt:J

    invoke-virtual {p1, v0, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 179
    sget-object v0, Lcom/samsung/android/app/memo/Session;->KEY_LASTMODAT:Ljava/lang/String;

    iget-wide v2, p0, Lcom/samsung/android/app/memo/Session;->mLastModifiedAt:J

    invoke-virtual {p1, v0, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 180
    sget-object v0, Lcom/samsung/android/app/memo/Session;->KEY_CTGR_UUID:Ljava/lang/String;

    iget-object v1, p0, Lcom/samsung/android/app/memo/Session;->mCategoryUUID:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 181
    sget-object v0, Lcom/samsung/android/app/memo/Session;->KEY_TITLE:Ljava/lang/String;

    iget-object v1, p0, Lcom/samsung/android/app/memo/Session;->mTitle:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 182
    sget-object v0, Lcom/samsung/android/app/memo/Session;->KEY_CONTENT:Ljava/lang/String;

    iget-object v1, p0, Lcom/samsung/android/app/memo/Session;->mContent:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 183
    sget-object v0, Lcom/samsung/android/app/memo/Session;->KEY_VRFILE_UUID:Ljava/lang/String;

    iget-object v1, p0, Lcom/samsung/android/app/memo/Session;->mVrFileUUID:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 184
    sget-object v0, Lcom/samsung/android/app/memo/Session;->KEY_DATA:Ljava/lang/String;

    iget-object v1, p0, Lcom/samsung/android/app/memo/Session;->mData:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 185
    sget-object v0, Lcom/samsung/android/app/memo/Session;->KEY_PHONENUM:Ljava/lang/String;

    iget-object v1, p0, Lcom/samsung/android/app/memo/Session;->mPhoneNum:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 186
    return-void
.end method

.method public overrideDT(JJ)V
    .locals 5
    .param p1, "createdAt"    # J
    .param p3, "lastModifiedAt"    # J

    .prologue
    const-wide/16 v2, 0x0

    .line 290
    cmp-long v0, p1, v2

    if-eqz v0, :cond_0

    .line 291
    iput-wide p1, p0, Lcom/samsung/android/app/memo/Session;->mCreatedAt:J

    .line 292
    :cond_0
    cmp-long v0, p3, v2

    if-eqz v0, :cond_1

    .line 293
    iput-wide p3, p0, Lcom/samsung/android/app/memo/Session;->mLastModifiedAt:J

    .line 294
    :cond_1
    return-void
.end method

.method public removeFile(Ljava/lang/String;)Z
    .locals 2
    .param p1, "uuid"    # Ljava/lang/String;

    .prologue
    .line 617
    :try_start_0
    invoke-static {p1}, Lcom/samsung/android/app/memo/util/UUIDHelper;->verify(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 621
    iget-object v1, p0, Lcom/samsung/android/app/memo/Session;->mFileUUIDsToBeRemoved:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 622
    invoke-direct {p0}, Lcom/samsung/android/app/memo/Session;->setDirty()V

    .line 624
    const/4 v1, 0x1

    :goto_0
    return v1

    .line 618
    :catch_0
    move-exception v0

    .line 619
    .local v0, "iae":Ljava/lang/IllegalArgumentException;
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public removeVoiceFile(Ljava/lang/String;)Z
    .locals 2
    .param p1, "uuid"    # Ljava/lang/String;

    .prologue
    .line 628
    :try_start_0
    invoke-static {p1}, Lcom/samsung/android/app/memo/util/UUIDHelper;->verify(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 632
    iget-object v1, p0, Lcom/samsung/android/app/memo/Session;->mFileUUIDsToBeRemoved:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 633
    invoke-direct {p0}, Lcom/samsung/android/app/memo/Session;->setDirty()V

    .line 634
    const-string v1, ""

    iput-object v1, p0, Lcom/samsung/android/app/memo/Session;->mVrFileUUID:Ljava/lang/String;

    .line 635
    const/4 v1, 0x1

    :goto_0
    return v1

    .line 629
    :catch_0
    move-exception v0

    .line 630
    .local v0, "iae":Ljava/lang/IllegalArgumentException;
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public restoreInstanceFrom(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "b"    # Landroid/os/Bundle;

    .prologue
    .line 148
    sget-object v0, Lcom/samsung/android/app/memo/Session;->KEY_FILESNEWLYADDED:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/memo/Session;->mFileUUIDsNewlyAdded:Ljava/util/ArrayList;

    .line 149
    sget-object v0, Lcom/samsung/android/app/memo/Session;->KEY_FILESTOBEDELETED:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/memo/Session;->mFileUUIDsToBeRemoved:Ljava/util/ArrayList;

    .line 150
    sget-object v0, Lcom/samsung/android/app/memo/Session;->KEY_INITIALFILES:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/memo/Session;->mInitialFileUUIDsList:Ljava/util/ArrayList;

    .line 152
    sget-object v0, Lcom/samsung/android/app/memo/Session;->KEY_ID:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/samsung/android/app/memo/Session;->mId:J

    .line 153
    sget-object v0, Lcom/samsung/android/app/memo/Session;->KEY_UUID:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/memo/Session;->mUUID:Ljava/lang/String;

    .line 154
    sget-object v0, Lcom/samsung/android/app/memo/Session;->KEY_ISDIRTY:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/samsung/android/app/memo/Session;->mIsDirty:Z

    .line 155
    sget-object v0, Lcom/samsung/android/app/memo/Session;->KEY_CREATEDAT:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/samsung/android/app/memo/Session;->mCreatedAt:J

    .line 156
    sget-object v0, Lcom/samsung/android/app/memo/Session;->KEY_LASTMODAT:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/samsung/android/app/memo/Session;->mLastModifiedAt:J

    .line 157
    sget-object v0, Lcom/samsung/android/app/memo/Session;->KEY_CTGR_UUID:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/memo/Session;->mCategoryUUID:Ljava/lang/String;

    .line 158
    sget-object v0, Lcom/samsung/android/app/memo/Session;->KEY_TITLE:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/memo/Session;->mTitle:Ljava/lang/String;

    .line 159
    sget-object v0, Lcom/samsung/android/app/memo/Session;->KEY_CONTENT:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/memo/Session;->mContent:Ljava/lang/String;

    .line 160
    sget-object v0, Lcom/samsung/android/app/memo/Session;->KEY_VRFILE_UUID:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/memo/Session;->mVrFileUUID:Ljava/lang/String;

    .line 161
    sget-object v0, Lcom/samsung/android/app/memo/Session;->KEY_DATA:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/memo/Session;->mData:Ljava/lang/String;

    .line 162
    sget-object v0, Lcom/samsung/android/app/memo/Session;->KEY_PHONENUM:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/memo/Session;->mPhoneNum:Ljava/lang/String;

    .line 163
    iget-object v0, p0, Lcom/samsung/android/app/memo/Session;->mUUID:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 164
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "can\'t restored from savedInstacneState"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 166
    :cond_0
    return-void
.end method

.method public declared-synchronized saveMemo()J
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/android/app/memo/Session$SessionException;
        }
    .end annotation

    .prologue
    .line 485
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/Session;->isDraft()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 486
    invoke-direct {p0}, Lcom/samsung/android/app/memo/Session;->insertNew()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/samsung/android/app/memo/Session;->mId:J

    .line 487
    iget-object v0, p0, Lcom/samsung/android/app/memo/Session;->mFileUUIDsNewlyAdded:Ljava/util/ArrayList;

    iput-object v0, p0, Lcom/samsung/android/app/memo/Session;->mInitialFileUUIDsList:Ljava/util/ArrayList;

    .line 488
    iget-object v0, p0, Lcom/samsung/android/app/memo/Session;->mVrFileUUID:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/app/memo/Session;->mFileUUIDsToBeRemoved:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/samsung/android/app/memo/Session;->mVrFileUUID:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 489
    iget-object v0, p0, Lcom/samsung/android/app/memo/Session;->mFinalFileUUIDsList:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/samsung/android/app/memo/Session;->mVrFileUUID:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 493
    :cond_0
    :goto_0
    invoke-direct {p0}, Lcom/samsung/android/app/memo/Session;->setFileUUIDsToBeRemoved()V

    .line 494
    iget-object v0, p0, Lcom/samsung/android/app/memo/Session;->mFileUUIDsToBeRemoved:Ljava/util/ArrayList;

    invoke-direct {p0, v0}, Lcom/samsung/android/app/memo/Session;->deleteFiles(Ljava/util/List;)V

    .line 495
    invoke-direct {p0}, Lcom/samsung/android/app/memo/Session;->clearDirty()V

    .line 496
    iget-wide v0, p0, Lcom/samsung/android/app/memo/Session;->mId:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-wide v0

    .line 491
    :cond_1
    :try_start_1
    invoke-direct {p0}, Lcom/samsung/android/app/memo/Session;->updateCurrent()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/samsung/android/app/memo/Session;->mId:J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 485
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized saveOrCancelEditing()V
    .locals 3

    .prologue
    .line 514
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/Session;->isDirty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 515
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/Session;->saveMemo()J
    :try_end_0
    .catch Lcom/samsung/android/app/memo/Session$SessionException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 522
    :goto_0
    monitor-exit p0

    return-void

    .line 517
    :cond_0
    :try_start_1
    iget-object v1, p0, Lcom/samsung/android/app/memo/Session;->mFileUUIDsNewlyAdded:Ljava/util/ArrayList;

    invoke-direct {p0, v1}, Lcom/samsung/android/app/memo/Session;->deleteFiles(Ljava/util/List;)V
    :try_end_1
    .catch Lcom/samsung/android/app/memo/Session$SessionException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 519
    :catch_0
    move-exception v0

    .line 520
    .local v0, "se":Lcom/samsung/android/app/memo/Session$SessionException;
    :try_start_2
    sget-object v1, Lcom/samsung/android/app/memo/Session;->TAG:Ljava/lang/String;

    const-string v2, "cancelEditing"

    invoke-static {v1, v2, v0}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 514
    .end local v0    # "se":Lcom/samsung/android/app/memo/Session$SessionException;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public setCategory(Ljava/lang/String;)V
    .locals 10
    .param p1, "category"    # Ljava/lang/String;

    .prologue
    .line 364
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 392
    :cond_0
    :goto_0
    return-void

    .line 366
    :cond_1
    const/4 v7, 0x0

    .line 368
    .local v7, "c":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/Session;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/provider/memo/MemoContract;->BASE_URI_CATEGORY:Landroid/net/Uri;

    const/4 v2, 0x0

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "_display_name=\'"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 369
    if-eqz v7, :cond_2

    invoke-interface {v7}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_2

    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 370
    const-string v0, "UUID"

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/memo/Session;->mCategoryUUID:Ljava/lang/String;
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 389
    :goto_1
    if-eqz v7, :cond_0

    .line 390
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 372
    :cond_2
    :try_start_1
    invoke-static {}, Lcom/samsung/android/app/memo/util/UUIDHelper;->newUUID()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/memo/Session;->mCategoryUUID:Ljava/lang/String;

    .line 373
    new-instance v8, Landroid/content/ContentValues;

    invoke-direct {v8}, Landroid/content/ContentValues;-><init>()V

    .line 374
    .local v8, "cv":Landroid/content/ContentValues;
    const-string v0, "UUID"

    iget-object v1, p0, Lcom/samsung/android/app/memo/Session;->mCategoryUUID:Ljava/lang/String;

    invoke-virtual {v8, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 375
    const-string v0, "_display_name"

    invoke-virtual {v8, v0, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 376
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/Session;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/android/app/memo/util/AccountHelper;->getRegisteredSamsungAccount(Landroid/content/Context;)Landroid/accounts/Account;

    move-result-object v6

    .line 377
    .local v6, "account":Landroid/accounts/Account;
    if-nez v6, :cond_3

    .line 378
    const-string v0, "accountType"

    const-string v1, "local"

    invoke-virtual {v8, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 379
    const-string v0, "accountName"

    const-string v1, ""

    invoke-virtual {v8, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 384
    :goto_2
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/Session;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/provider/memo/MemoContract;->BASE_URI_CATEGORY:Landroid/net/Uri;

    invoke-virtual {v0, v1, v8}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    :try_end_1
    .catch Landroid/database/SQLException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 386
    .end local v6    # "account":Landroid/accounts/Account;
    .end local v8    # "cv":Landroid/content/ContentValues;
    :catch_0
    move-exception v9

    .line 387
    .local v9, "sqle":Landroid/database/SQLException;
    :try_start_2
    sget-object v0, Lcom/samsung/android/app/memo/Session;->TAG:Ljava/lang/String;

    const-string v1, "setCategory()"

    invoke-static {v0, v1, v9}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 389
    if-eqz v7, :cond_0

    .line 390
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0

    .line 381
    .end local v9    # "sqle":Landroid/database/SQLException;
    .restart local v6    # "account":Landroid/accounts/Account;
    .restart local v8    # "cv":Landroid/content/ContentValues;
    :cond_3
    :try_start_3
    const-string v0, "accountType"

    const-string v1, "com.osp.app.signin"

    invoke-virtual {v8, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 382
    const-string v0, "accountName"

    iget-object v1, v6, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v8, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catch Landroid/database/SQLException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_2

    .line 388
    .end local v6    # "account":Landroid/accounts/Account;
    .end local v8    # "cv":Landroid/content/ContentValues;
    :catchall_0
    move-exception v0

    .line 389
    if-eqz v7, :cond_4

    .line 390
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 391
    :cond_4
    throw v0
.end method

.method public setCategoryUUID(Ljava/lang/String;)V
    .locals 1
    .param p1, "categoryUUID"    # Ljava/lang/String;

    .prologue
    .line 331
    iget-object v0, p0, Lcom/samsung/android/app/memo/Session;->mCategoryUUID:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 339
    :cond_0
    :goto_0
    return-void

    .line 334
    :cond_1
    iput-object p1, p0, Lcom/samsung/android/app/memo/Session;->mCategoryUUID:Ljava/lang/String;

    .line 337
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/Session;->isDraft()Z

    move-result v0

    if-nez v0, :cond_0

    .line 338
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/app/memo/Session;->mIsDirty:Z

    goto :goto_0
.end method

.method public setContent(Ljava/lang/String;)V
    .locals 4
    .param p1, "newContent"    # Ljava/lang/String;

    .prologue
    const-wide/16 v2, 0x0

    .line 349
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-wide v0, p0, Lcom/samsung/android/app/memo/Session;->mId:J

    cmp-long v0, v0, v2

    if-gez v0, :cond_1

    iget-object v0, p0, Lcom/samsung/android/app/memo/Session;->mVrFileUUID:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/samsung/android/app/memo/Session;->mTitle:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/samsung/android/app/memo/Session;->mTitle:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 350
    invoke-direct {p0}, Lcom/samsung/android/app/memo/Session;->clearDirty()V

    .line 361
    :cond_0
    :goto_0
    return-void

    .line 353
    :cond_1
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 354
    const-string v0, "\u00a0"

    const-string v1, "&nbsp;"

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p1

    .line 355
    const-string v0, "</p>\n<p>"

    const-string v1, "</p><p>"

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p1

    .line 357
    :cond_2
    iget-wide v0, p0, Lcom/samsung/android/app/memo/Session;->mId:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_3

    iget-object v0, p0, Lcom/samsung/android/app/memo/Session;->mContent:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 359
    :cond_3
    iput-object p1, p0, Lcom/samsung/android/app/memo/Session;->mContent:Ljava/lang/String;

    .line 360
    invoke-direct {p0}, Lcom/samsung/android/app/memo/Session;->setDirty()V

    goto :goto_0
.end method

.method public setData(Z)Ljava/lang/String;
    .locals 1
    .param p1, "clear"    # Z

    .prologue
    .line 318
    if-eqz p1, :cond_0

    .line 319
    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/android/app/memo/Session;->mData:Ljava/lang/String;

    .line 323
    :goto_0
    iget-object v0, p0, Lcom/samsung/android/app/memo/Session;->mData:Ljava/lang/String;

    return-object v0

    .line 321
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/app/memo/Session;->mUUID:Ljava/lang/String;

    invoke-static {v0}, Lcom/samsung/android/app/memo/util/FileHelper;->getPrivateFilePath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/android/app/memo/util/Utils;->getRelativePath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/memo/Session;->mData:Ljava/lang/String;

    goto :goto_0
.end method

.method public setPhoneNumer(Ljava/lang/String;)V
    .locals 0
    .param p1, "phoneNum"    # Ljava/lang/String;

    .prologue
    .line 395
    iput-object p1, p0, Lcom/samsung/android/app/memo/Session;->mPhoneNum:Ljava/lang/String;

    .line 396
    return-void
.end method

.method public setTitle(Ljava/lang/String;)V
    .locals 1
    .param p1, "title"    # Ljava/lang/String;

    .prologue
    .line 341
    iget-object v0, p0, Lcom/samsung/android/app/memo/Session;->mTitle:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/app/memo/Session;->mTitle:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 346
    :goto_0
    return-void

    .line 344
    :cond_0
    iput-object p1, p0, Lcom/samsung/android/app/memo/Session;->mTitle:Ljava/lang/String;

    .line 345
    invoke-direct {p0}, Lcom/samsung/android/app/memo/Session;->setDirty()V

    goto :goto_0
.end method

.method public setVoiceRecordedFile(Landroid/net/Uri;)Landroid/net/Uri;
    .locals 4
    .param p1, "sourceUri"    # Landroid/net/Uri;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/android/app/memo/Session$SessionException;
        }
    .end annotation

    .prologue
    .line 525
    const-string v1, ".m4a"

    const-string v2, "audio/mp4"

    const/4 v3, 0x0

    invoke-virtual {p0, p1, v1, v2, v3}, Lcom/samsung/android/app/memo/Session;->addFile(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;I)Landroid/net/Uri;

    move-result-object v0

    .line 526
    .local v0, "uri":Landroid/net/Uri;
    const-string v1, ""

    iget-object v2, p0, Lcom/samsung/android/app/memo/Session;->mVrFileUUID:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 527
    iget-object v1, p0, Lcom/samsung/android/app/memo/Session;->mFileUUIDsToBeRemoved:Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/samsung/android/app/memo/Session;->mVrFileUUID:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 529
    :cond_0
    invoke-virtual {v0}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/app/memo/Session;->mVrFileUUID:Ljava/lang/String;

    .line 530
    invoke-direct {p0}, Lcom/samsung/android/app/memo/Session;->setDirty()V

    .line 531
    return-object v0
.end method

.method public startNew()V
    .locals 2

    .prologue
    .line 257
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/samsung/android/app/memo/Session;->mId:J

    .line 258
    invoke-static {}, Lcom/samsung/android/app/memo/util/UUIDHelper;->newUUID()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/memo/Session;->mUUID:Ljava/lang/String;

    .line 259
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/app/memo/Session;->mIsDirty:Z

    .line 261
    invoke-static {}, Lcom/samsung/android/app/memo/util/Utils;->getCurrentUTCTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/samsung/android/app/memo/Session;->mLastModifiedAt:J

    .line 260
    iput-wide v0, p0, Lcom/samsung/android/app/memo/Session;->mCreatedAt:J

    .line 262
    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/android/app/memo/Session;->mCategoryUUID:Ljava/lang/String;

    .line 263
    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/android/app/memo/Session;->mTitle:Ljava/lang/String;

    .line 264
    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/android/app/memo/Session;->mContent:Ljava/lang/String;

    .line 265
    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/android/app/memo/Session;->mVrFileUUID:Ljava/lang/String;

    .line 266
    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/android/app/memo/Session;->mData:Ljava/lang/String;

    .line 267
    return-void
.end method
