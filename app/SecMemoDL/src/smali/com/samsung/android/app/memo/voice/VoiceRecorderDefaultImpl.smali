.class public Lcom/samsung/android/app/memo/voice/VoiceRecorderDefaultImpl;
.super Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;
.source "VoiceRecorderDefaultImpl.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/app/memo/voice/VoiceRecorderDefaultImpl$TimeCounterTask;
    }
.end annotation


# static fields
.field private static TAG:Ljava/lang/String;


# instance fields
.field private mCurVoiceInfoDefault:Lcom/samsung/android/app/memo/voice/VoiceInfo;

.field private mNowRecording:Z

.field private mRecordStartTime:J

.field private mRecorder:Landroid/media/MediaRecorder;

.field private mRecording:Z

.field private mTimer:Lcom/samsung/android/app/memo/voice/VoiceRecorderDefaultImpl$TimeCounterTask;

.field private recordTime:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 36
    const-string v0, "VoiceRecorderSecMediaImpl"

    sput-object v0, Lcom/samsung/android/app/memo/voice/VoiceRecorderDefaultImpl;->TAG:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 53
    invoke-direct {p0, p1}, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;-><init>(Landroid/content/Context;)V

    .line 50
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderDefaultImpl;->recordTime:J

    .line 54
    return-void
.end method

.method static synthetic access$0(Lcom/samsung/android/app/memo/voice/VoiceRecorderDefaultImpl;J)V
    .locals 1

    .prologue
    .line 50
    iput-wide p1, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderDefaultImpl;->recordTime:J

    return-void
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/samsung/android/app/memo/voice/VoiceRecorderDefaultImpl;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 57
    new-instance v0, Lcom/samsung/android/app/memo/voice/VoiceRecorderDefaultImpl;

    invoke-direct {v0, p0}, Lcom/samsung/android/app/memo/voice/VoiceRecorderDefaultImpl;-><init>(Landroid/content/Context;)V

    return-object v0
.end method


# virtual methods
.method public RecordStartFail(Lcom/samsung/android/app/memo/voice/VoiceRecorderBase$RecordListener;)V
    .locals 3
    .param p1, "mListner"    # Lcom/samsung/android/app/memo/voice/VoiceRecorderBase$RecordListener;

    .prologue
    const/4 v2, 0x0

    .line 77
    if-eqz p1, :cond_0

    .line 78
    invoke-interface {p1}, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase$RecordListener;->onStopRecording()V

    .line 81
    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderDefaultImpl;->mRecorder:Landroid/media/MediaRecorder;

    invoke-virtual {v0}, Landroid/media/MediaRecorder;->reset()V

    .line 82
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderDefaultImpl;->mRecorder:Landroid/media/MediaRecorder;

    invoke-virtual {v0}, Landroid/media/MediaRecorder;->release()V

    .line 83
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderDefaultImpl;->mTimer:Lcom/samsung/android/app/memo/voice/VoiceRecorderDefaultImpl$TimeCounterTask;

    if-eqz v0, :cond_1

    .line 84
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderDefaultImpl;->mTimer:Lcom/samsung/android/app/memo/voice/VoiceRecorderDefaultImpl$TimeCounterTask;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/memo/voice/VoiceRecorderDefaultImpl$TimeCounterTask;->cancel(Z)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 89
    :cond_1
    iput-object v2, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderDefaultImpl;->mRecorder:Landroid/media/MediaRecorder;

    .line 91
    :goto_0
    if-eqz p1, :cond_2

    .line 92
    invoke-interface {p1}, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase$RecordListener;->onFailRecording()V

    .line 93
    :cond_2
    return-void

    .line 87
    :catch_0
    move-exception v0

    .line 89
    iput-object v2, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderDefaultImpl;->mRecorder:Landroid/media/MediaRecorder;

    goto :goto_0

    .line 88
    :catchall_0
    move-exception v0

    .line 89
    iput-object v2, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderDefaultImpl;->mRecorder:Landroid/media/MediaRecorder;

    .line 90
    throw v0
.end method

.method public getCurRecordingTime()J
    .locals 2

    .prologue
    .line 73
    iget-wide v0, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderDefaultImpl;->recordTime:J

    return-wide v0
.end method

.method public getVoiceRecordingTime(Landroid/net/Uri;)J
    .locals 7
    .param p1, "vUri"    # Landroid/net/Uri;

    .prologue
    const-wide/16 v2, 0x0

    const/4 v6, 0x0

    .line 295
    const/4 v1, 0x0

    .line 296
    .local v1, "recording_time":I
    iget-object v4, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderDefaultImpl;->mPlayer:Landroid/media/MediaPlayer;

    if-eqz v4, :cond_0

    .line 297
    iget-object v4, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderDefaultImpl;->mPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v4}, Landroid/media/MediaPlayer;->release()V

    .line 298
    :cond_0
    new-instance v4, Landroid/media/MediaPlayer;

    invoke-direct {v4}, Landroid/media/MediaPlayer;-><init>()V

    iput-object v4, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderDefaultImpl;->mPlayer:Landroid/media/MediaPlayer;

    .line 299
    iget-object v4, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderDefaultImpl;->mPlayer:Landroid/media/MediaPlayer;

    if-nez v4, :cond_1

    .line 318
    :goto_0
    return-wide v2

    .line 303
    :cond_1
    :try_start_0
    iget-object v4, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderDefaultImpl;->mPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {p0, p1}, Lcom/samsung/android/app/memo/voice/VoiceRecorderDefaultImpl;->getRealPathFromURI(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/media/MediaPlayer;->setDataSource(Ljava/lang/String;)V

    .line 304
    iget-object v4, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderDefaultImpl;->mPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v4}, Landroid/media/MediaPlayer;->prepare()V

    .line 305
    iget-object v4, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderDefaultImpl;->mPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v4}, Landroid/media/MediaPlayer;->getDuration()I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_2

    move-result v1

    .line 317
    iput-object v6, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderDefaultImpl;->mPlayer:Landroid/media/MediaPlayer;

    .line 318
    int-to-long v2, v1

    goto :goto_0

    .line 306
    :catch_0
    move-exception v0

    .line 307
    .local v0, "e":Ljava/io/IOException;
    iput-object v6, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderDefaultImpl;->mPlayer:Landroid/media/MediaPlayer;

    goto :goto_0

    .line 309
    .end local v0    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v0

    .line 310
    .local v0, "e":Ljava/lang/IllegalStateException;
    iput-object v6, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderDefaultImpl;->mPlayer:Landroid/media/MediaPlayer;

    goto :goto_0

    .line 312
    .end local v0    # "e":Ljava/lang/IllegalStateException;
    :catch_2
    move-exception v0

    .line 313
    .local v0, "e":Ljava/lang/NullPointerException;
    iput-object v6, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderDefaultImpl;->mPlayer:Landroid/media/MediaPlayer;

    goto :goto_0
.end method

.method public isNowRecording()Z
    .locals 1

    .prologue
    .line 62
    iget-boolean v0, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderDefaultImpl;->mNowRecording:Z

    return v0
.end method

.method public isNowStopped()Z
    .locals 1

    .prologue
    .line 98
    const/4 v0, 0x0

    return v0
.end method

.method public isRecordActive(Landroid/content/Context;)Z
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 212
    const/4 v0, 0x0

    .line 220
    .local v0, "bRecording":Z
    return v0
.end method

.method public isRecording()Z
    .locals 1

    .prologue
    .line 67
    iget-boolean v0, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderDefaultImpl;->mRecording:Z

    return v0
.end method

.method public pauseRecording()V
    .locals 0

    .prologue
    .line 202
    return-void
.end method

.method public resumeRecording()V
    .locals 0

    .prologue
    .line 208
    return-void
.end method

.method public startRecording(Lcom/samsung/android/app/memo/voice/VoiceInfo;Lcom/samsung/android/app/memo/voice/VoiceRecorderBase$RecordListener;)Z
    .locals 6
    .param p1, "voiceFile"    # Lcom/samsung/android/app/memo/voice/VoiceInfo;
    .param p2, "mListner"    # Lcom/samsung/android/app/memo/voice/VoiceRecorderBase$RecordListener;

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 103
    iget-boolean v4, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderDefaultImpl;->mNowRecording:Z

    if-eqz v4, :cond_0

    .line 168
    :goto_0
    return v2

    .line 106
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/voice/VoiceRecorderDefaultImpl;->requestAudioFocus()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 107
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/voice/VoiceRecorderDefaultImpl;->stopRecording()Lcom/samsung/android/app/memo/voice/VoiceInfo;

    goto :goto_0

    .line 111
    :cond_1
    new-instance v1, Landroid/content/Intent;

    const-string v4, "com.android.music.musicservicecommand"

    invoke-direct {v1, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 112
    .local v1, "i":Landroid/content/Intent;
    const-string v4, "command"

    const-string v5, "pause"

    invoke-virtual {v1, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 113
    iget-object v4, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderDefaultImpl;->mActivity:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 114
    new-instance v4, Landroid/media/MediaRecorder;

    invoke-direct {v4}, Landroid/media/MediaRecorder;-><init>()V

    iput-object v4, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderDefaultImpl;->mRecorder:Landroid/media/MediaRecorder;

    .line 115
    iget-object v4, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderDefaultImpl;->mRecorder:Landroid/media/MediaRecorder;

    invoke-virtual {v4, v3}, Landroid/media/MediaRecorder;->setAudioSource(I)V

    .line 116
    iget-object v4, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderDefaultImpl;->mRecorder:Landroid/media/MediaRecorder;

    const/4 v5, 0x3

    invoke-virtual {v4, v5}, Landroid/media/MediaRecorder;->setOutputFormat(I)V

    .line 117
    iget-object v4, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderDefaultImpl;->mRecorder:Landroid/media/MediaRecorder;

    invoke-virtual {v4, v3}, Landroid/media/MediaRecorder;->setAudioEncoder(I)V

    .line 118
    iget-object v4, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderDefaultImpl;->mRecorder:Landroid/media/MediaRecorder;

    iget-object v5, p1, Lcom/samsung/android/app/memo/voice/VoiceInfo;->mUri:Landroid/net/Uri;

    invoke-virtual {p0, v5}, Lcom/samsung/android/app/memo/voice/VoiceRecorderDefaultImpl;->getRealPathFromURI(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/media/MediaRecorder;->setOutputFile(Ljava/lang/String;)V

    .line 119
    iget-object v4, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderDefaultImpl;->mRecorder:Landroid/media/MediaRecorder;

    new-instance v5, Lcom/samsung/android/app/memo/voice/VoiceRecorderDefaultImpl$1;

    invoke-direct {v5, p0}, Lcom/samsung/android/app/memo/voice/VoiceRecorderDefaultImpl$1;-><init>(Lcom/samsung/android/app/memo/voice/VoiceRecorderDefaultImpl;)V

    invoke-virtual {v4, v5}, Landroid/media/MediaRecorder;->setOnInfoListener(Landroid/media/MediaRecorder$OnInfoListener;)V

    .line 137
    :try_start_0
    iget-object v4, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderDefaultImpl;->mRecorder:Landroid/media/MediaRecorder;

    invoke-virtual {v4}, Landroid/media/MediaRecorder;->prepare()V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 150
    :try_start_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderDefaultImpl;->mRecordStartTime:J

    .line 151
    const-wide/16 v4, 0x0

    iput-wide v4, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderDefaultImpl;->recordTime:J

    .line 152
    iget-object v4, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderDefaultImpl;->mRecorder:Landroid/media/MediaRecorder;

    invoke-virtual {v4}, Landroid/media/MediaRecorder;->start()V

    .line 153
    new-instance v4, Lcom/samsung/android/app/memo/voice/VoiceRecorderDefaultImpl$TimeCounterTask;

    const/4 v5, 0x0

    invoke-direct {v4, p0, v5}, Lcom/samsung/android/app/memo/voice/VoiceRecorderDefaultImpl$TimeCounterTask;-><init>(Lcom/samsung/android/app/memo/voice/VoiceRecorderDefaultImpl;Lcom/samsung/android/app/memo/voice/VoiceRecorderDefaultImpl$TimeCounterTask;)V

    iput-object v4, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderDefaultImpl;->mTimer:Lcom/samsung/android/app/memo/voice/VoiceRecorderDefaultImpl$TimeCounterTask;

    .line 154
    iget-object v4, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderDefaultImpl;->mTimer:Lcom/samsung/android/app/memo/voice/VoiceRecorderDefaultImpl$TimeCounterTask;

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Void;

    invoke-virtual {v4, v5}, Lcom/samsung/android/app/memo/voice/VoiceRecorderDefaultImpl$TimeCounterTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2

    .line 161
    iput-boolean v3, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderDefaultImpl;->mNowRecording:Z

    .line 162
    iput-boolean v3, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderDefaultImpl;->mRecording:Z

    .line 163
    iput-object p1, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderDefaultImpl;->mCurVoiceInfoDefault:Lcom/samsung/android/app/memo/voice/VoiceInfo;

    .line 164
    iput-object p2, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderDefaultImpl;->mRecordListener:Lcom/samsung/android/app/memo/voice/VoiceRecorderBase$RecordListener;

    .line 165
    iget-object v2, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderDefaultImpl;->mRecordListener:Lcom/samsung/android/app/memo/voice/VoiceRecorderBase$RecordListener;

    if-eqz v2, :cond_2

    .line 166
    iget-object v2, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderDefaultImpl;->mRecordListener:Lcom/samsung/android/app/memo/voice/VoiceRecorderBase$RecordListener;

    iget-object v4, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderDefaultImpl;->mCurVoiceInfoDefault:Lcom/samsung/android/app/memo/voice/VoiceInfo;

    invoke-interface {v2, v4}, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase$RecordListener;->onStartRecording(Lcom/samsung/android/app/memo/voice/VoiceInfo;)V

    :cond_2
    move v2, v3

    .line 168
    goto/16 :goto_0

    .line 138
    :catch_0
    move-exception v0

    .line 139
    .local v0, "e":Ljava/lang/IllegalStateException;
    sget-object v3, Lcom/samsung/android/app/memo/voice/VoiceRecorderDefaultImpl;->TAG:Ljava/lang/String;

    const-string v4, "startRecording"

    invoke-static {v3, v4, v0}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 140
    invoke-virtual {p0, p2}, Lcom/samsung/android/app/memo/voice/VoiceRecorderDefaultImpl;->RecordStartFail(Lcom/samsung/android/app/memo/voice/VoiceRecorderBase$RecordListener;)V

    goto/16 :goto_0

    .line 142
    .end local v0    # "e":Ljava/lang/IllegalStateException;
    :catch_1
    move-exception v0

    .line 143
    .local v0, "e":Ljava/io/IOException;
    sget-object v3, Lcom/samsung/android/app/memo/voice/VoiceRecorderDefaultImpl;->TAG:Ljava/lang/String;

    const-string v4, "startRecording"

    invoke-static {v3, v4, v0}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 144
    invoke-virtual {p0, p2}, Lcom/samsung/android/app/memo/voice/VoiceRecorderDefaultImpl;->RecordStartFail(Lcom/samsung/android/app/memo/voice/VoiceRecorderBase$RecordListener;)V

    goto/16 :goto_0

    .line 155
    .end local v0    # "e":Ljava/io/IOException;
    :catch_2
    move-exception v0

    .line 156
    .local v0, "e":Ljava/lang/Exception;
    sget-object v3, Lcom/samsung/android/app/memo/voice/VoiceRecorderDefaultImpl;->TAG:Ljava/lang/String;

    const-string v4, "startRecording"

    invoke-static {v3, v4, v0}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 157
    invoke-virtual {p0, p2}, Lcom/samsung/android/app/memo/voice/VoiceRecorderDefaultImpl;->RecordStartFail(Lcom/samsung/android/app/memo/voice/VoiceRecorderBase$RecordListener;)V

    goto/16 :goto_0
.end method

.method public stopRecording()Lcom/samsung/android/app/memo/voice/VoiceInfo;
    .locals 9

    .prologue
    const/4 v8, 0x0

    const/4 v2, 0x0

    .line 225
    iget-object v3, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderDefaultImpl;->mCurVoiceInfoDefault:Lcom/samsung/android/app/memo/voice/VoiceInfo;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderDefaultImpl;->mRecorder:Landroid/media/MediaRecorder;

    if-eqz v3, :cond_0

    iget-boolean v3, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderDefaultImpl;->mNowRecording:Z

    if-nez v3, :cond_1

    :cond_0
    move-object v1, v2

    .line 256
    :goto_0
    return-object v1

    .line 229
    :cond_1
    :try_start_0
    iget-object v3, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderDefaultImpl;->mRecorder:Landroid/media/MediaRecorder;

    invoke-virtual {v3}, Landroid/media/MediaRecorder;->stop()V

    .line 230
    iget-object v3, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderDefaultImpl;->mTimer:Lcom/samsung/android/app/memo/voice/VoiceRecorderDefaultImpl$TimeCounterTask;

    if-eqz v3, :cond_2

    .line 231
    iget-object v3, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderDefaultImpl;->mTimer:Lcom/samsung/android/app/memo/voice/VoiceRecorderDefaultImpl$TimeCounterTask;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Lcom/samsung/android/app/memo/voice/VoiceRecorderDefaultImpl$TimeCounterTask;->cancel(Z)Z
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 235
    :cond_2
    :goto_1
    iget-object v3, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderDefaultImpl;->mCurVoiceInfoDefault:Lcom/samsung/android/app/memo/voice/VoiceInfo;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    .line 236
    iget-wide v6, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderDefaultImpl;->mRecordStartTime:J

    sub-long/2addr v4, v6

    .line 235
    iput-wide v4, v3, Lcom/samsung/android/app/memo/voice/VoiceInfo;->mTotalTime:J

    .line 237
    iput-boolean v8, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderDefaultImpl;->mNowRecording:Z

    .line 238
    iput-boolean v8, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderDefaultImpl;->mRecording:Z

    .line 239
    iget-object v3, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderDefaultImpl;->mCurVoiceInfoDefault:Lcom/samsung/android/app/memo/voice/VoiceInfo;

    iget-wide v4, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderDefaultImpl;->mRecordStartTime:J

    iput-wide v4, v3, Lcom/samsung/android/app/memo/voice/VoiceInfo;->mRecordStartTime:J

    .line 241
    :try_start_1
    iget-object v3, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderDefaultImpl;->mRecorder:Landroid/media/MediaRecorder;

    invoke-virtual {v3}, Landroid/media/MediaRecorder;->release()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 245
    iput-object v2, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderDefaultImpl;->mRecorder:Landroid/media/MediaRecorder;

    .line 248
    :goto_2
    const/4 v3, -0x1

    iput v3, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderDefaultImpl;->mPlayerState:I

    .line 249
    iget-object v3, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderDefaultImpl;->mRecordListener:Lcom/samsung/android/app/memo/voice/VoiceRecorderBase$RecordListener;

    if-eqz v3, :cond_3

    .line 250
    iget-object v3, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderDefaultImpl;->mRecordListener:Lcom/samsung/android/app/memo/voice/VoiceRecorderBase$RecordListener;

    invoke-interface {v3}, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase$RecordListener;->onStopRecording()V

    .line 252
    :cond_3
    iget-object v1, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderDefaultImpl;->mCurVoiceInfoDefault:Lcom/samsung/android/app/memo/voice/VoiceInfo;

    .line 253
    .local v1, "vInfo":Lcom/samsung/android/app/memo/voice/VoiceInfo;
    iput-object v2, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderDefaultImpl;->mCurVoiceInfoDefault:Lcom/samsung/android/app/memo/voice/VoiceInfo;

    .line 254
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/voice/VoiceRecorderDefaultImpl;->abandonAudioFocus()V

    goto :goto_0

    .line 232
    .end local v1    # "vInfo":Lcom/samsung/android/app/memo/voice/VoiceInfo;
    :catch_0
    move-exception v0

    .line 233
    .local v0, "e":Ljava/lang/RuntimeException;
    sget-object v3, Lcom/samsung/android/app/memo/voice/VoiceRecorderDefaultImpl;->TAG:Ljava/lang/String;

    const-string v4, "stopRecording"

    invoke-static {v3, v4, v0}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 242
    .end local v0    # "e":Ljava/lang/RuntimeException;
    :catch_1
    move-exception v0

    .line 243
    .local v0, "e":Ljava/lang/Exception;
    :try_start_2
    sget-object v3, Lcom/samsung/android/app/memo/voice/VoiceRecorderDefaultImpl;->TAG:Ljava/lang/String;

    const-string v4, "stopRecording"

    invoke-static {v3, v4, v0}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 245
    iput-object v2, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderDefaultImpl;->mRecorder:Landroid/media/MediaRecorder;

    goto :goto_2

    .line 244
    .end local v0    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v3

    .line 245
    iput-object v2, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderDefaultImpl;->mRecorder:Landroid/media/MediaRecorder;

    .line 246
    throw v3
.end method
