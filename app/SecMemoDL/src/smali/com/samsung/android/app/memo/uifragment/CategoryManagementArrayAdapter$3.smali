.class Lcom/samsung/android/app/memo/uifragment/CategoryManagementArrayAdapter$3;
.super Ljava/lang/Object;
.source "CategoryManagementFragment.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/app/memo/uifragment/CategoryManagementArrayAdapter;->delete(I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/memo/uifragment/CategoryManagementArrayAdapter;

.field private final synthetic val$pos:I


# direct methods
.method constructor <init>(Lcom/samsung/android/app/memo/uifragment/CategoryManagementArrayAdapter;I)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsung/android/app/memo/uifragment/CategoryManagementArrayAdapter$3;->this$0:Lcom/samsung/android/app/memo/uifragment/CategoryManagementArrayAdapter;

    iput p2, p0, Lcom/samsung/android/app/memo/uifragment/CategoryManagementArrayAdapter$3;->val$pos:I

    .line 727
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 10
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 731
    iget-object v4, p0, Lcom/samsung/android/app/memo/uifragment/CategoryManagementArrayAdapter$3;->this$0:Lcom/samsung/android/app/memo/uifragment/CategoryManagementArrayAdapter;

    iget-object v4, v4, Lcom/samsung/android/app/memo/uifragment/CategoryManagementArrayAdapter;->mCategoryManagementFragment:Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;

    .line 732
    invoke-virtual {v4}, Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;->getCategoryManagementListViewItems()Ljava/util/ArrayList;

    move-result-object v1

    .line 734
    .local v1, "listViewItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/app/memo/uifragment/CategoryListItem;>;"
    iget v4, p0, Lcom/samsung/android/app/memo/uifragment/CategoryManagementArrayAdapter$3;->val$pos:I

    if-ltz v4, :cond_0

    iget v4, p0, Lcom/samsung/android/app/memo/uifragment/CategoryManagementArrayAdapter$3;->val$pos:I

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v7

    if-lt v4, v7, :cond_1

    .line 775
    :cond_0
    :goto_0
    return-void

    .line 737
    :cond_1
    iget-object v4, p0, Lcom/samsung/android/app/memo/uifragment/CategoryManagementArrayAdapter$3;->this$0:Lcom/samsung/android/app/memo/uifragment/CategoryManagementArrayAdapter;

    iget-object v4, v4, Lcom/samsung/android/app/memo/uifragment/CategoryManagementArrayAdapter;->mCategoryManagementFragment:Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;

    iget v7, p0, Lcom/samsung/android/app/memo/uifragment/CategoryManagementArrayAdapter$3;->val$pos:I

    .line 738
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v8

    add-int/lit8 v8, v8, -0x1

    .line 739
    iget-object v9, p0, Lcom/samsung/android/app/memo/uifragment/CategoryManagementArrayAdapter$3;->this$0:Lcom/samsung/android/app/memo/uifragment/CategoryManagementArrayAdapter;

    iget-object v9, v9, Lcom/samsung/android/app/memo/uifragment/CategoryManagementArrayAdapter;->mCategoryManagementFragment:Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;

    invoke-virtual {v9}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 740
    iget-object v9, p0, Lcom/samsung/android/app/memo/uifragment/CategoryManagementArrayAdapter$3;->this$0:Lcom/samsung/android/app/memo/uifragment/CategoryManagementArrayAdapter;

    iget-object v9, v9, Lcom/samsung/android/app/memo/uifragment/CategoryManagementArrayAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v9}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v9

    .line 737
    invoke-virtual {v4, v7, v8, v6, v9}, Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;->doReorderItems(IIILandroid/content/ContentResolver;)V

    .line 742
    iget-object v4, p0, Lcom/samsung/android/app/memo/uifragment/CategoryManagementArrayAdapter$3;->this$0:Lcom/samsung/android/app/memo/uifragment/CategoryManagementArrayAdapter;

    iget-object v7, v4, Lcom/samsung/android/app/memo/uifragment/CategoryManagementArrayAdapter;->mContext:Landroid/content/Context;

    .line 743
    iget v4, p0, Lcom/samsung/android/app/memo/uifragment/CategoryManagementArrayAdapter$3;->val$pos:I

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/android/app/memo/uifragment/CategoryListItem;

    invoke-virtual {v4}, Lcom/samsung/android/app/memo/uifragment/CategoryListItem;->getCategoryUUID()Ljava/lang/String;

    move-result-object v4

    .line 742
    invoke-static {v7, v4}, Lcom/samsung/android/app/memo/MemoDataHandler;->updateCategoryUUIDToNone(Landroid/content/Context;Ljava/lang/String;)V

    .line 745
    iget-object v4, p0, Lcom/samsung/android/app/memo/uifragment/CategoryManagementArrayAdapter$3;->this$0:Lcom/samsung/android/app/memo/uifragment/CategoryManagementArrayAdapter;

    iget-object v4, v4, Lcom/samsung/android/app/memo/uifragment/CategoryManagementArrayAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    sget-object v7, Lcom/samsung/android/provider/memo/MemoContract;->BASE_URI_CATEGORY:Landroid/net/Uri;

    .line 746
    iget-object v8, p0, Lcom/samsung/android/app/memo/uifragment/CategoryManagementArrayAdapter$3;->this$0:Lcom/samsung/android/app/memo/uifragment/CategoryManagementArrayAdapter;

    iget-object v8, v8, Lcom/samsung/android/app/memo/uifragment/CategoryManagementArrayAdapter;->mCategoryManagementFragment:Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;

    iget v9, p0, Lcom/samsung/android/app/memo/uifragment/CategoryManagementArrayAdapter$3;->val$pos:I

    invoke-virtual {v8, v9}, Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;->getSelectionByUUID(I)Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x0

    .line 745
    invoke-virtual {v4, v7, v8, v9}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 749
    iget-object v4, p0, Lcom/samsung/android/app/memo/uifragment/CategoryManagementArrayAdapter$3;->this$0:Lcom/samsung/android/app/memo/uifragment/CategoryManagementArrayAdapter;

    iget-object v4, v4, Lcom/samsung/android/app/memo/uifragment/CategoryManagementArrayAdapter;->mContext:Landroid/content/Context;

    .line 750
    sget-object v7, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->PREF_CATEGORY:Ljava/lang/String;

    invoke-virtual {v4, v7, v6}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    .line 751
    .local v2, "pref":Landroid/content/SharedPreferences;
    sget-object v4, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->KEY_CATEGORY_UUID:Ljava/lang/String;

    const-string v7, ""

    invoke-interface {v2, v4, v7}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 752
    .local v3, "selecteUUID":Ljava/lang/String;
    iget v4, p0, Lcom/samsung/android/app/memo/uifragment/CategoryManagementArrayAdapter$3;->val$pos:I

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/android/app/memo/uifragment/CategoryListItem;

    invoke-virtual {v4}, Lcom/samsung/android/app/memo/uifragment/CategoryListItem;->getCategoryUUID()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_2

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-ne v4, v5, :cond_3

    .line 753
    :cond_2
    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 754
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    sget-object v4, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->KEY_CATEGORY_UUID:Ljava/lang/String;

    const-string v7, ""

    invoke-interface {v0, v4, v7}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 755
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-le v4, v5, :cond_4

    .line 756
    sget-object v4, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->KEY_CATEGORY_TITLE:Ljava/lang/String;

    .line 757
    iget-object v7, p0, Lcom/samsung/android/app/memo/uifragment/CategoryManagementArrayAdapter$3;->this$0:Lcom/samsung/android/app/memo/uifragment/CategoryManagementArrayAdapter;

    iget-object v7, v7, Lcom/samsung/android/app/memo/uifragment/CategoryManagementArrayAdapter;->mContext:Landroid/content/Context;

    const v8, 0x7f0b0017

    invoke-virtual {v7, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 756
    invoke-interface {v0, v4, v7}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 758
    sget-object v4, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->KEY_CATEGORY_TYPE:Ljava/lang/String;

    invoke-interface {v0, v4, v5}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 767
    :goto_1
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 770
    .end local v0    # "editor":Landroid/content/SharedPreferences$Editor;
    :cond_3
    iget v4, p0, Lcom/samsung/android/app/memo/uifragment/CategoryManagementArrayAdapter$3;->val$pos:I

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 771
    iget-object v4, p0, Lcom/samsung/android/app/memo/uifragment/CategoryManagementArrayAdapter$3;->this$0:Lcom/samsung/android/app/memo/uifragment/CategoryManagementArrayAdapter;

    invoke-virtual {v4}, Lcom/samsung/android/app/memo/uifragment/CategoryManagementArrayAdapter;->notifyDataSetChanged()V

    .line 772
    iget-object v4, p0, Lcom/samsung/android/app/memo/uifragment/CategoryManagementArrayAdapter$3;->this$0:Lcom/samsung/android/app/memo/uifragment/CategoryManagementArrayAdapter;

    invoke-static {v4, v6}, Lcom/samsung/android/app/memo/uifragment/CategoryManagementArrayAdapter;->access$3(Lcom/samsung/android/app/memo/uifragment/CategoryManagementArrayAdapter;Z)V

    .line 773
    invoke-static {}, Lcom/samsung/android/app/memo/util/Utils;->isTablet()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 774
    iget-object v4, p0, Lcom/samsung/android/app/memo/uifragment/CategoryManagementArrayAdapter$3;->this$0:Lcom/samsung/android/app/memo/uifragment/CategoryManagementArrayAdapter;

    iget-object v7, v4, Lcom/samsung/android/app/memo/uifragment/CategoryManagementArrayAdapter;->mCategoryManagementFragment:Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-nez v4, :cond_5

    move v4, v5

    :goto_2
    invoke-virtual {v7, v4}, Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;->showNoCategory(Z)V

    goto/16 :goto_0

    .line 761
    .restart local v0    # "editor":Landroid/content/SharedPreferences$Editor;
    :cond_4
    sget-object v4, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->KEY_CATEGORY_TITLE:Ljava/lang/String;

    .line 762
    iget-object v7, p0, Lcom/samsung/android/app/memo/uifragment/CategoryManagementArrayAdapter$3;->this$0:Lcom/samsung/android/app/memo/uifragment/CategoryManagementArrayAdapter;

    iget-object v7, v7, Lcom/samsung/android/app/memo/uifragment/CategoryManagementArrayAdapter;->mContext:Landroid/content/Context;

    const v8, 0x7f0b000d

    invoke-virtual {v7, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 761
    invoke-interface {v0, v4, v7}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 763
    sget-object v4, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->KEY_CATEGORY_TYPE:Ljava/lang/String;

    invoke-interface {v0, v4, v6}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    goto :goto_1

    .end local v0    # "editor":Landroid/content/SharedPreferences$Editor;
    :cond_5
    move v4, v6

    .line 774
    goto :goto_2
.end method
