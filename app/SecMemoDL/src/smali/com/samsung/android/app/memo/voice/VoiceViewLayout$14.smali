.class Lcom/samsung/android/app/memo/voice/VoiceViewLayout$14;
.super Landroid/content/BroadcastReceiver;
.source "VoiceViewLayout.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->updateEarphoneReceiver(Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$14;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    .line 738
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3
    .param p1, "ctx"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 743
    if-eqz p2, :cond_1

    .line 744
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "android.intent.action.HEADSET_PLUG"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 745
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$14;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    const-string v1, "state"

    const/4 v2, 0x0

    invoke-virtual {p2, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    invoke-static {v0, v1}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->access$36(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;I)V

    .line 746
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$14;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    # getter for: Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->headSetStateOld:I
    invoke-static {v0}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->access$37(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$14;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    # getter for: Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->headSetStateNew:I
    invoke-static {v0}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->access$38(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)I

    move-result v0

    if-nez v0, :cond_0

    .line 747
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$14;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    # getter for: Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mVoiceManger:Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;
    invoke-static {v0}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->access$9(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$14;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    # getter for: Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mVoiceManger:Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;
    invoke-static {v0}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->access$9(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->isNowPlaying()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 748
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$14;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    # invokes: Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->pauseDuringAlarm()V
    invoke-static {v0}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->access$39(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)V

    .line 751
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$14;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    iget-object v1, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$14;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    # getter for: Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->headSetStateNew:I
    invoke-static {v1}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->access$38(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)I

    move-result v1

    invoke-static {v0, v1}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->access$40(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;I)V

    .line 756
    :cond_1
    return-void
.end method
