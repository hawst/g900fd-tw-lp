.class public Lcom/samsung/android/app/memo/util/ImageLoader$UpdateThumbImageReceiver;
.super Landroid/content/BroadcastReceiver;
.source "ImageLoader.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/memo/util/ImageLoader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "UpdateThumbImageReceiver"
.end annotation


# instance fields
.field private data:Ljava/lang/String;

.field final synthetic this$0:Lcom/samsung/android/app/memo/util/ImageLoader;


# direct methods
.method public constructor <init>(Lcom/samsung/android/app/memo/util/ImageLoader;)V
    .locals 0

    .prologue
    .line 73
    iput-object p1, p0, Lcom/samsung/android/app/memo/util/ImageLoader$UpdateThumbImageReceiver;->this$0:Lcom/samsung/android/app/memo/util/ImageLoader;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 79
    const-string v0, "ImageLoader"

    const-string v1, "UpdateThumbImageReceiver onReceive()"

    invoke-static {v0, v1}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 80
    const-string v0, "_data"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 81
    const-string v0, "_data"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/memo/util/ImageLoader$UpdateThumbImageReceiver;->data:Ljava/lang/String;

    .line 82
    iget-object v0, p0, Lcom/samsung/android/app/memo/util/ImageLoader$UpdateThumbImageReceiver;->data:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 83
    iget-object v0, p0, Lcom/samsung/android/app/memo/util/ImageLoader$UpdateThumbImageReceiver;->this$0:Lcom/samsung/android/app/memo/util/ImageLoader;

    iget-object v1, p0, Lcom/samsung/android/app/memo/util/ImageLoader$UpdateThumbImageReceiver;->data:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/memo/util/ImageLoader;->removeCache(Ljava/lang/String;)V

    .line 85
    :cond_0
    return-void
.end method
