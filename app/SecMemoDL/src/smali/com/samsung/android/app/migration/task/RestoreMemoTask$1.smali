.class Lcom/samsung/android/app/migration/task/RestoreMemoTask$1;
.super Ljava/lang/Object;
.source "RestoreMemoTask.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/app/migration/task/RestoreMemoTask;->showDialog(Landroid/app/Activity;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/migration/task/RestoreMemoTask;

.field private final synthetic val$context:Landroid/app/Activity;

.field private final synthetic val$mutex:Ljava/util/concurrent/Semaphore;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/migration/task/RestoreMemoTask;Landroid/app/Activity;Ljava/util/concurrent/Semaphore;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsung/android/app/migration/task/RestoreMemoTask$1;->this$0:Lcom/samsung/android/app/migration/task/RestoreMemoTask;

    iput-object p2, p0, Lcom/samsung/android/app/migration/task/RestoreMemoTask$1;->val$context:Landroid/app/Activity;

    iput-object p3, p0, Lcom/samsung/android/app/migration/task/RestoreMemoTask$1;->val$mutex:Ljava/util/concurrent/Semaphore;

    .line 483
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 486
    iget-object v0, p0, Lcom/samsung/android/app/migration/task/RestoreMemoTask$1;->this$0:Lcom/samsung/android/app/migration/task/RestoreMemoTask;

    # getter for: Lcom/samsung/android/app/migration/task/RestoreMemoTask;->progressDialog:Lcom/samsung/android/app/migration/dialog/ProgressBarDialog;
    invoke-static {v0}, Lcom/samsung/android/app/migration/task/RestoreMemoTask;->access$0(Lcom/samsung/android/app/migration/task/RestoreMemoTask;)Lcom/samsung/android/app/migration/dialog/ProgressBarDialog;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/app/migration/task/RestoreMemoTask$1;->this$0:Lcom/samsung/android/app/migration/task/RestoreMemoTask;

    # getter for: Lcom/samsung/android/app/migration/task/RestoreMemoTask;->progressDialog:Lcom/samsung/android/app/migration/dialog/ProgressBarDialog;
    invoke-static {v0}, Lcom/samsung/android/app/migration/task/RestoreMemoTask;->access$0(Lcom/samsung/android/app/migration/task/RestoreMemoTask;)Lcom/samsung/android/app/migration/dialog/ProgressBarDialog;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/app/migration/dialog/ProgressBarDialog;->isAdded()Z

    move-result v0

    if-nez v0, :cond_0

    .line 487
    iget-object v0, p0, Lcom/samsung/android/app/migration/task/RestoreMemoTask$1;->this$0:Lcom/samsung/android/app/migration/task/RestoreMemoTask;

    # getter for: Lcom/samsung/android/app/migration/task/RestoreMemoTask;->progressDialog:Lcom/samsung/android/app/migration/dialog/ProgressBarDialog;
    invoke-static {v0}, Lcom/samsung/android/app/migration/task/RestoreMemoTask;->access$0(Lcom/samsung/android/app/migration/task/RestoreMemoTask;)Lcom/samsung/android/app/migration/dialog/ProgressBarDialog;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/app/migration/task/RestoreMemoTask$1;->val$context:Landroid/app/Activity;

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/migration/dialog/ProgressBarDialog;->show(Landroid/app/Activity;)V

    .line 488
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/app/migration/task/RestoreMemoTask$1;->val$mutex:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v0}, Ljava/util/concurrent/Semaphore;->release()V

    .line 489
    return-void
.end method
