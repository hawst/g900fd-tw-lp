.class Lcom/samsung/android/app/memo/share/SharePopupList$2;
.super Ljava/lang/Object;
.source "SharePopupList.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/app/memo/share/SharePopupList;->openPopupList()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/widget/AdapterView$OnItemClickListener;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/memo/share/SharePopupList;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/memo/share/SharePopupList;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsung/android/app/memo/share/SharePopupList$2;->this$0:Lcom/samsung/android/app/memo/share/SharePopupList;

    .line 146
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 4
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 151
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    iget-object v0, p0, Lcom/samsung/android/app/memo/share/SharePopupList$2;->this$0:Lcom/samsung/android/app/memo/share/SharePopupList;

    iget-object v1, v0, Lcom/samsung/android/app/memo/share/SharePopupList;->target:Landroid/content/Intent;

    new-instance v2, Landroid/content/ComponentName;

    .line 152
    iget-object v0, p0, Lcom/samsung/android/app/memo/share/SharePopupList$2;->this$0:Lcom/samsung/android/app/memo/share/SharePopupList;

    # getter for: Lcom/samsung/android/app/memo/share/SharePopupList;->rList:Ljava/util/List;
    invoke-static {v0}, Lcom/samsung/android/app/memo/share/SharePopupList;->access$1(Lcom/samsung/android/app/memo/share/SharePopupList;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/ResolveInfo;

    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v3, v0, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    .line 153
    iget-object v0, p0, Lcom/samsung/android/app/memo/share/SharePopupList$2;->this$0:Lcom/samsung/android/app/memo/share/SharePopupList;

    # getter for: Lcom/samsung/android/app/memo/share/SharePopupList;->rList:Ljava/util/List;
    invoke-static {v0}, Lcom/samsung/android/app/memo/share/SharePopupList;->access$1(Lcom/samsung/android/app/memo/share/SharePopupList;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/ResolveInfo;

    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v0, v0, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    invoke-direct {v2, v3, v0}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 151
    invoke-virtual {v1, v2}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 155
    if-eqz p2, :cond_1

    .line 156
    invoke-virtual {p2}, Landroid/view/View;->performClick()Z

    .line 157
    iget-object v0, p0, Lcom/samsung/android/app/memo/share/SharePopupList$2;->this$0:Lcom/samsung/android/app/memo/share/SharePopupList;

    iget-object v0, v0, Lcom/samsung/android/app/memo/share/SharePopupList;->mSharePopupMenu:Landroid/widget/ListPopupWindow;

    invoke-virtual {v0}, Landroid/widget/ListPopupWindow;->dismiss()V

    .line 159
    iget-object v0, p0, Lcom/samsung/android/app/memo/share/SharePopupList$2;->this$0:Lcom/samsung/android/app/memo/share/SharePopupList;

    iget-object v0, v0, Lcom/samsung/android/app/memo/share/SharePopupList;->mcontext:Landroid/content/Context;

    instance-of v0, v0, Lcom/samsung/android/app/memo/Main;

    if-eqz v0, :cond_0

    .line 160
    iget-object v0, p0, Lcom/samsung/android/app/memo/share/SharePopupList$2;->this$0:Lcom/samsung/android/app/memo/share/SharePopupList;

    iget-object v0, v0, Lcom/samsung/android/app/memo/share/SharePopupList;->mActivity:Landroid/app/Activity;

    check-cast v0, Lcom/samsung/android/app/memo/Main;

    invoke-virtual {v0}, Lcom/samsung/android/app/memo/Main;->finishActionMode()V

    .line 162
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/app/memo/share/SharePopupList$2;->this$0:Lcom/samsung/android/app/memo/share/SharePopupList;

    iget-object v0, v0, Lcom/samsung/android/app/memo/share/SharePopupList;->mcontext:Landroid/content/Context;

    iget-object v1, p0, Lcom/samsung/android/app/memo/share/SharePopupList$2;->this$0:Lcom/samsung/android/app/memo/share/SharePopupList;

    iget-object v1, v1, Lcom/samsung/android/app/memo/share/SharePopupList;->target:Landroid/content/Intent;

    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 164
    :cond_1
    return-void
.end method
