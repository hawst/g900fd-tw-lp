.class Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment$4;
.super Ljava/lang/Object;
.source "CategoryManagementFragment.java"

# interfaces
.implements Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog$OnDialogResultListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;->onItemLongClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;

.field private final synthetic val$pos:I


# direct methods
.method constructor <init>(Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;I)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment$4;->this$0:Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;

    iput p2, p0, Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment$4;->val$pos:I

    .line 505
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPositiveBtnClick(Ljava/lang/String;Ljava/lang/String;)V
    .locals 6
    .param p1, "uuid"    # Ljava/lang/String;
    .param p2, "categoryName"    # Ljava/lang/String;

    .prologue
    const/4 v5, 0x0

    .line 510
    if-nez p1, :cond_1

    .line 530
    :cond_0
    :goto_0
    return-void

    .line 512
    :cond_1
    # getter for: Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;->mListViewItems:Ljava/util/ArrayList;
    invoke-static {}, Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;->access$2()Ljava/util/ArrayList;

    move-result-object v3

    if-eqz v3, :cond_2

    # getter for: Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;->mListViewItems:Ljava/util/ArrayList;
    invoke-static {}, Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;->access$2()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-lez v3, :cond_2

    iget v3, p0, Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment$4;->val$pos:I

    # getter for: Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;->mListViewItems:Ljava/util/ArrayList;
    invoke-static {}, Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;->access$2()Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-ge v3, v4, :cond_2

    .line 513
    # getter for: Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;->mListViewItems:Ljava/util/ArrayList;
    invoke-static {}, Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;->access$2()Ljava/util/ArrayList;

    move-result-object v3

    iget v4, p0, Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment$4;->val$pos:I

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/android/app/memo/uifragment/CategoryListItem;

    invoke-virtual {v3, p2}, Lcom/samsung/android/app/memo/uifragment/CategoryListItem;->setCategoryName(Ljava/lang/String;)V

    .line 514
    :cond_2
    iget-object v3, p0, Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment$4;->this$0:Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;

    # getter for: Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;->mCategoryManagementArrayAdapter:Lcom/samsung/android/app/memo/uifragment/CategoryManagementArrayAdapter;
    invoke-static {v3}, Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;->access$1(Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;)Lcom/samsung/android/app/memo/uifragment/CategoryManagementArrayAdapter;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/android/app/memo/uifragment/CategoryManagementArrayAdapter;->notifyDataSetChanged()V

    .line 517
    iget-object v3, p0, Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment$4;->this$0:Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;

    invoke-virtual {v3}, Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 518
    iget-object v3, p0, Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment$4;->this$0:Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;

    invoke-virtual {v3}, Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    sget-object v4, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->PREF_CATEGORY:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, Landroid/app/Activity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 519
    .local v1, "pref":Landroid/content/SharedPreferences;
    sget-object v3, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->KEY_CATEGORY_UUID:Ljava/lang/String;

    const-string v4, ""

    invoke-interface {v1, v3, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 520
    .local v2, "selecteUUID":Ljava/lang/String;
    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 521
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 522
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    sget-object v3, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->KEY_CATEGORY_UUID:Ljava/lang/String;

    invoke-interface {v0, v3, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 523
    sget-object v3, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->KEY_CATEGORY_TITLE:Ljava/lang/String;

    invoke-interface {v0, v3, p2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 524
    sget-object v3, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->KEY_IS_CATEGORY_CHANGE:Ljava/lang/String;

    invoke-interface {v0, v3, v5}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 525
    sget-object v3, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->KEY_CATEGORY_TYPE:Ljava/lang/String;

    .line 526
    iget-object v4, p0, Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment$4;->this$0:Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;

    invoke-virtual {v4}, Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-static {v4, p1, p2}, Lcom/samsung/android/app/memo/util/Utils;->getCategoryType(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v4

    .line 525
    invoke-interface {v0, v3, v4}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 527
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto :goto_0
.end method
