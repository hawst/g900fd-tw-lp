.class public final Lcom/samsung/android/app/migration/snbdownload/network/downsync/records/RecordItem;
.super Lcom/samsung/android/app/migration/snbdownload/network/downsync/records/RecordBaseKeyItem;
.source "RecordItem.java"


# static fields
.field private static final VALUE:Ljava/lang/String; = "value"


# instance fields
.field mValue:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/Long;Ljava/lang/String;Z)V
    .locals 0
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "timeStamp"    # Ljava/lang/Long;
    .param p3, "value"    # Ljava/lang/String;
    .param p4, "deleted"    # Z

    .prologue
    .line 36
    invoke-direct {p0, p1, p2, p4}, Lcom/samsung/android/app/migration/snbdownload/network/downsync/records/RecordBaseKeyItem;-><init>(Ljava/lang/String;Ljava/lang/Long;Z)V

    .line 37
    iput-object p3, p0, Lcom/samsung/android/app/migration/snbdownload/network/downsync/records/RecordItem;->mValue:Ljava/lang/String;

    .line 38
    return-void
.end method


# virtual methods
.method public fromJSON(Ljava/lang/Object;)V
    .locals 2
    .param p1, "obj"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 41
    invoke-super {p0, p1}, Lcom/samsung/android/app/migration/snbdownload/network/downsync/records/RecordBaseKeyItem;->fromJSON(Ljava/lang/Object;)V

    move-object v0, p1

    .line 42
    check-cast v0, Lorg/json/JSONObject;

    .line 43
    .local v0, "json":Lorg/json/JSONObject;
    const-string v1, "value"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 44
    const-string v1, "value"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/app/migration/snbdownload/network/downsync/records/RecordItem;->mValue:Ljava/lang/String;

    .line 45
    :cond_0
    return-void
.end method

.method public getValue()Ljava/lang/String;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcom/samsung/android/app/migration/snbdownload/network/downsync/records/RecordItem;->mValue:Ljava/lang/String;

    return-object v0
.end method

.method public toJSON()Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 48
    invoke-super {p0}, Lcom/samsung/android/app/migration/snbdownload/network/downsync/records/RecordBaseKeyItem;->toJSON()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/json/JSONObject;

    .line 49
    .local v0, "json":Lorg/json/JSONObject;
    const-string v1, "value"

    iget-object v2, p0, Lcom/samsung/android/app/migration/snbdownload/network/downsync/records/RecordItem;->mValue:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 50
    return-object v0
.end method
