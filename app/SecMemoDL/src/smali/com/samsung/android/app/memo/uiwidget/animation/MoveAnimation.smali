.class public Lcom/samsung/android/app/memo/uiwidget/animation/MoveAnimation;
.super Landroid/view/animation/Animation;
.source "MoveAnimation.java"


# instance fields
.field private mFromAlpha:F

.field private mFromScaleX:F

.field private mFromScaleY:F

.field private mFromXDelta:F

.field private mFromYDelta:F

.field private mMoveDuration:J

.field private mPivotX:F

.field private mPivotY:F

.field private mToAlpha:F

.field private mToScaleX:F

.field private mToScaleY:F

.field private mToXDelta:F

.field private mToYDelta:F


# direct methods
.method public constructor <init>(FFFF)V
    .locals 5
    .param p1, "fromXDelta"    # F
    .param p2, "toXDelta"    # F
    .param p3, "fromYDelta"    # F
    .param p4, "toYDelta"    # F

    .prologue
    const/4 v4, 0x0

    const/16 v3, 0xa

    const/high16 v2, 0x3f800000    # 1.0f

    .line 54
    invoke-direct {p0}, Landroid/view/animation/Animation;-><init>()V

    .line 28
    const-wide/16 v0, 0x12c

    iput-wide v0, p0, Lcom/samsung/android/app/memo/uiwidget/animation/MoveAnimation;->mMoveDuration:J

    .line 38
    iput v2, p0, Lcom/samsung/android/app/memo/uiwidget/animation/MoveAnimation;->mFromScaleX:F

    .line 40
    iput v2, p0, Lcom/samsung/android/app/memo/uiwidget/animation/MoveAnimation;->mToScaleX:F

    .line 42
    iput v2, p0, Lcom/samsung/android/app/memo/uiwidget/animation/MoveAnimation;->mFromScaleY:F

    .line 44
    iput v2, p0, Lcom/samsung/android/app/memo/uiwidget/animation/MoveAnimation;->mToScaleY:F

    .line 46
    iput v4, p0, Lcom/samsung/android/app/memo/uiwidget/animation/MoveAnimation;->mPivotX:F

    .line 48
    iput v4, p0, Lcom/samsung/android/app/memo/uiwidget/animation/MoveAnimation;->mPivotY:F

    .line 50
    iput v2, p0, Lcom/samsung/android/app/memo/uiwidget/animation/MoveAnimation;->mFromAlpha:F

    .line 52
    iput v2, p0, Lcom/samsung/android/app/memo/uiwidget/animation/MoveAnimation;->mToAlpha:F

    .line 55
    iput p1, p0, Lcom/samsung/android/app/memo/uiwidget/animation/MoveAnimation;->mFromXDelta:F

    .line 56
    iput p2, p0, Lcom/samsung/android/app/memo/uiwidget/animation/MoveAnimation;->mToXDelta:F

    .line 57
    iput p3, p0, Lcom/samsung/android/app/memo/uiwidget/animation/MoveAnimation;->mFromYDelta:F

    .line 58
    iput p4, p0, Lcom/samsung/android/app/memo/uiwidget/animation/MoveAnimation;->mToYDelta:F

    .line 59
    iget-wide v0, p0, Lcom/samsung/android/app/memo/uiwidget/animation/MoveAnimation;->mMoveDuration:J

    invoke-virtual {p0, v0, v1}, Lcom/samsung/android/app/memo/uiwidget/animation/MoveAnimation;->setDuration(J)V

    .line 60
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/memo/uiwidget/animation/MoveAnimation;->setFillAfter(Z)V

    .line 61
    invoke-virtual {p0, v3, v3, v3, v3}, Lcom/samsung/android/app/memo/uiwidget/animation/MoveAnimation;->initialize(IIII)V

    .line 67
    return-void
.end method

.method private swapMoveXY()V
    .locals 3

    .prologue
    .line 147
    iget v0, p0, Lcom/samsung/android/app/memo/uiwidget/animation/MoveAnimation;->mFromXDelta:F

    .line 148
    .local v0, "tempFromX":F
    iget v1, p0, Lcom/samsung/android/app/memo/uiwidget/animation/MoveAnimation;->mFromYDelta:F

    .line 149
    .local v1, "tempFromY":F
    iget v2, p0, Lcom/samsung/android/app/memo/uiwidget/animation/MoveAnimation;->mToXDelta:F

    iput v2, p0, Lcom/samsung/android/app/memo/uiwidget/animation/MoveAnimation;->mFromXDelta:F

    .line 150
    iput v0, p0, Lcom/samsung/android/app/memo/uiwidget/animation/MoveAnimation;->mToXDelta:F

    .line 151
    iget v2, p0, Lcom/samsung/android/app/memo/uiwidget/animation/MoveAnimation;->mToYDelta:F

    iput v2, p0, Lcom/samsung/android/app/memo/uiwidget/animation/MoveAnimation;->mFromYDelta:F

    .line 152
    iput v1, p0, Lcom/samsung/android/app/memo/uiwidget/animation/MoveAnimation;->mToYDelta:F

    .line 153
    return-void
.end method


# virtual methods
.method protected applyTransformation(FLandroid/view/animation/Transformation;)V
    .locals 11
    .param p1, "interpolatedTime"    # F
    .param p2, "t"    # Landroid/view/animation/Transformation;

    .prologue
    const/4 v9, 0x0

    const/high16 v10, 0x3f800000    # 1.0f

    .line 100
    const/high16 v3, 0x3f800000    # 1.0f

    .local v3, "scaleX":F
    const/high16 v4, 0x3f800000    # 1.0f

    .line 101
    .local v4, "scaleY":F
    iget v1, p0, Lcom/samsung/android/app/memo/uiwidget/animation/MoveAnimation;->mFromXDelta:F

    .line 102
    .local v1, "f1":F
    iget v2, p0, Lcom/samsung/android/app/memo/uiwidget/animation/MoveAnimation;->mFromYDelta:F

    .line 103
    .local v2, "f2":F
    invoke-virtual {p2}, Landroid/view/animation/Transformation;->clear()V

    .line 104
    new-instance v5, Landroid/view/animation/Transformation;

    invoke-direct {v5}, Landroid/view/animation/Transformation;-><init>()V

    .line 106
    .local v5, "transformation":Landroid/view/animation/Transformation;
    iget v6, p0, Lcom/samsung/android/app/memo/uiwidget/animation/MoveAnimation;->mFromXDelta:F

    iget v7, p0, Lcom/samsung/android/app/memo/uiwidget/animation/MoveAnimation;->mToXDelta:F

    cmpl-float v6, v6, v7

    if-eqz v6, :cond_0

    .line 107
    iget v6, p0, Lcom/samsung/android/app/memo/uiwidget/animation/MoveAnimation;->mFromXDelta:F

    iget v7, p0, Lcom/samsung/android/app/memo/uiwidget/animation/MoveAnimation;->mToXDelta:F

    iget v8, p0, Lcom/samsung/android/app/memo/uiwidget/animation/MoveAnimation;->mFromXDelta:F

    sub-float/2addr v7, v8

    mul-float/2addr v7, p1

    add-float v1, v6, v7

    .line 109
    :cond_0
    iget v6, p0, Lcom/samsung/android/app/memo/uiwidget/animation/MoveAnimation;->mFromYDelta:F

    iget v7, p0, Lcom/samsung/android/app/memo/uiwidget/animation/MoveAnimation;->mToYDelta:F

    cmpl-float v6, v6, v7

    if-eqz v6, :cond_1

    .line 110
    iget v6, p0, Lcom/samsung/android/app/memo/uiwidget/animation/MoveAnimation;->mFromYDelta:F

    iget v7, p0, Lcom/samsung/android/app/memo/uiwidget/animation/MoveAnimation;->mToYDelta:F

    iget v8, p0, Lcom/samsung/android/app/memo/uiwidget/animation/MoveAnimation;->mFromYDelta:F

    sub-float/2addr v7, v8

    mul-float/2addr v7, p1

    add-float v2, v6, v7

    .line 112
    :cond_1
    invoke-static {v1, v2}, Ljava/lang/Float;->compare(FF)I

    move-result v6

    if-eqz v6, :cond_2

    .line 113
    invoke-virtual {v5}, Landroid/view/animation/Transformation;->getMatrix()Landroid/graphics/Matrix;

    move-result-object v6

    invoke-virtual {v6, v1, v2}, Landroid/graphics/Matrix;->setTranslate(FF)V

    .line 114
    invoke-virtual {p2, v5}, Landroid/view/animation/Transformation;->compose(Landroid/view/animation/Transformation;)V

    .line 115
    invoke-virtual {v5}, Landroid/view/animation/Transformation;->clear()V

    .line 118
    :cond_2
    iget v6, p0, Lcom/samsung/android/app/memo/uiwidget/animation/MoveAnimation;->mFromScaleX:F

    cmpl-float v6, v6, v10

    if-nez v6, :cond_3

    iget v6, p0, Lcom/samsung/android/app/memo/uiwidget/animation/MoveAnimation;->mToScaleX:F

    cmpl-float v6, v6, v10

    if-eqz v6, :cond_4

    .line 119
    :cond_3
    iget v6, p0, Lcom/samsung/android/app/memo/uiwidget/animation/MoveAnimation;->mFromScaleX:F

    iget v7, p0, Lcom/samsung/android/app/memo/uiwidget/animation/MoveAnimation;->mToScaleX:F

    iget v8, p0, Lcom/samsung/android/app/memo/uiwidget/animation/MoveAnimation;->mFromScaleX:F

    sub-float/2addr v7, v8

    mul-float/2addr v7, p1

    add-float v3, v6, v7

    .line 121
    :cond_4
    iget v6, p0, Lcom/samsung/android/app/memo/uiwidget/animation/MoveAnimation;->mFromScaleY:F

    cmpl-float v6, v6, v10

    if-nez v6, :cond_5

    iget v6, p0, Lcom/samsung/android/app/memo/uiwidget/animation/MoveAnimation;->mToScaleY:F

    cmpl-float v6, v6, v10

    if-eqz v6, :cond_6

    .line 122
    :cond_5
    iget v6, p0, Lcom/samsung/android/app/memo/uiwidget/animation/MoveAnimation;->mFromScaleY:F

    iget v7, p0, Lcom/samsung/android/app/memo/uiwidget/animation/MoveAnimation;->mToScaleY:F

    iget v8, p0, Lcom/samsung/android/app/memo/uiwidget/animation/MoveAnimation;->mFromScaleY:F

    sub-float/2addr v7, v8

    mul-float/2addr v7, p1

    add-float v4, v6, v7

    .line 124
    :cond_6
    cmpl-float v6, v3, v10

    if-nez v6, :cond_7

    cmpl-float v6, v4, v10

    if-eqz v6, :cond_8

    .line 125
    :cond_7
    const/16 v6, 0x9

    new-array v0, v6, [F

    .line 126
    .local v0, "arrayOfFloat":[F
    invoke-virtual {p2}, Landroid/view/animation/Transformation;->getMatrix()Landroid/graphics/Matrix;

    move-result-object v6

    invoke-virtual {v6, v0}, Landroid/graphics/Matrix;->getValues([F)V

    .line 130
    iget v6, p0, Lcom/samsung/android/app/memo/uiwidget/animation/MoveAnimation;->mPivotX:F

    cmpl-float v6, v6, v9

    if-nez v6, :cond_b

    iget v6, p0, Lcom/samsung/android/app/memo/uiwidget/animation/MoveAnimation;->mPivotY:F

    cmpl-float v6, v6, v9

    if-nez v6, :cond_b

    .line 131
    invoke-virtual {v5}, Landroid/view/animation/Transformation;->getMatrix()Landroid/graphics/Matrix;

    move-result-object v6

    invoke-virtual {v6, v3, v4, v9, v9}, Landroid/graphics/Matrix;->setScale(FFFF)V

    .line 136
    :goto_0
    invoke-virtual {p2, v5}, Landroid/view/animation/Transformation;->compose(Landroid/view/animation/Transformation;)V

    .line 137
    invoke-virtual {v5}, Landroid/view/animation/Transformation;->clear()V

    .line 140
    .end local v0    # "arrayOfFloat":[F
    :cond_8
    iget v6, p0, Lcom/samsung/android/app/memo/uiwidget/animation/MoveAnimation;->mFromAlpha:F

    cmpl-float v6, v6, v10

    if-nez v6, :cond_9

    iget v6, p0, Lcom/samsung/android/app/memo/uiwidget/animation/MoveAnimation;->mToAlpha:F

    cmpl-float v6, v6, v10

    if-eqz v6, :cond_a

    .line 141
    :cond_9
    iget v6, p0, Lcom/samsung/android/app/memo/uiwidget/animation/MoveAnimation;->mFromAlpha:F

    iget v7, p0, Lcom/samsung/android/app/memo/uiwidget/animation/MoveAnimation;->mToAlpha:F

    iget v8, p0, Lcom/samsung/android/app/memo/uiwidget/animation/MoveAnimation;->mFromAlpha:F

    sub-float/2addr v7, v8

    mul-float/2addr v7, p1

    add-float/2addr v6, v7

    invoke-virtual {v5, v6}, Landroid/view/animation/Transformation;->setAlpha(F)V

    .line 142
    invoke-virtual {p2, v5}, Landroid/view/animation/Transformation;->compose(Landroid/view/animation/Transformation;)V

    .line 144
    :cond_a
    return-void

    .line 133
    .restart local v0    # "arrayOfFloat":[F
    :cond_b
    invoke-virtual {v5}, Landroid/view/animation/Transformation;->getMatrix()Landroid/graphics/Matrix;

    move-result-object v6

    const/4 v7, 0x2

    aget v7, v0, v7

    iget v8, p0, Lcom/samsung/android/app/memo/uiwidget/animation/MoveAnimation;->mPivotX:F

    add-float/2addr v7, v8

    .line 134
    const/4 v8, 0x5

    aget v8, v0, v8

    iget v9, p0, Lcom/samsung/android/app/memo/uiwidget/animation/MoveAnimation;->mPivotY:F

    add-float/2addr v8, v9

    .line 133
    invoke-virtual {v6, v3, v4, v7, v8}, Landroid/graphics/Matrix;->setScale(FFFF)V

    goto :goto_0
.end method

.method public reverseAnimation()V
    .locals 8

    .prologue
    .line 156
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    .line 157
    .local v0, "curTime":J
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uiwidget/animation/MoveAnimation;->getDuration()J

    move-result-wide v4

    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uiwidget/animation/MoveAnimation;->getStartTime()J

    move-result-wide v6

    sub-long v6, v0, v6

    sub-long v2, v4, v6

    .line 158
    .local v2, "remainDurationTime":J
    invoke-direct {p0}, Lcom/samsung/android/app/memo/uiwidget/animation/MoveAnimation;->swapMoveXY()V

    .line 159
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uiwidget/animation/MoveAnimation;->hasEnded()Z

    move-result v4

    if-nez v4, :cond_0

    .line 160
    sub-long v4, v0, v2

    invoke-virtual {p0, v4, v5}, Lcom/samsung/android/app/memo/uiwidget/animation/MoveAnimation;->setStartTime(J)V

    .line 164
    :goto_0
    return-void

    .line 162
    :cond_0
    const-wide/16 v4, -0x1

    invoke-virtual {p0, v4, v5}, Lcom/samsung/android/app/memo/uiwidget/animation/MoveAnimation;->setStartTime(J)V

    goto :goto_0
.end method

.method public setAlpha(FF)V
    .locals 0
    .param p1, "fromAlpha"    # F
    .param p2, "toAlpha"    # F

    .prologue
    .line 87
    iput p1, p0, Lcom/samsung/android/app/memo/uiwidget/animation/MoveAnimation;->mFromAlpha:F

    .line 88
    iput p2, p0, Lcom/samsung/android/app/memo/uiwidget/animation/MoveAnimation;->mToAlpha:F

    .line 90
    return-void
.end method

.method public final setDuration(J)V
    .locals 1
    .param p1, "duration"    # J

    .prologue
    .line 94
    iput-wide p1, p0, Lcom/samsung/android/app/memo/uiwidget/animation/MoveAnimation;->mMoveDuration:J

    .line 95
    invoke-super {p0, p1, p2}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 96
    return-void
.end method

.method public setFillAfter(Z)V
    .locals 0
    .param p1, "fillAfter"    # Z

    .prologue
    .line 71
    invoke-super {p0, p1}, Landroid/view/animation/Animation;->setFillAfter(Z)V

    .line 72
    return-void
.end method

.method public setPivot(FF)V
    .locals 0
    .param p1, "pivotX"    # F
    .param p2, "pivotY"    # F

    .prologue
    .line 82
    iput p1, p0, Lcom/samsung/android/app/memo/uiwidget/animation/MoveAnimation;->mPivotX:F

    .line 83
    iput p2, p0, Lcom/samsung/android/app/memo/uiwidget/animation/MoveAnimation;->mPivotY:F

    .line 84
    return-void
.end method

.method public setScale(FFFF)V
    .locals 0
    .param p1, "fromScaleX"    # F
    .param p2, "toScaleX"    # F
    .param p3, "fromScaleY"    # F
    .param p4, "toScaleY"    # F

    .prologue
    .line 75
    iput p1, p0, Lcom/samsung/android/app/memo/uiwidget/animation/MoveAnimation;->mFromScaleX:F

    .line 76
    iput p2, p0, Lcom/samsung/android/app/memo/uiwidget/animation/MoveAnimation;->mToScaleX:F

    .line 77
    iput p3, p0, Lcom/samsung/android/app/memo/uiwidget/animation/MoveAnimation;->mFromScaleY:F

    .line 78
    iput p4, p0, Lcom/samsung/android/app/memo/uiwidget/animation/MoveAnimation;->mToScaleY:F

    .line 79
    return-void
.end method
