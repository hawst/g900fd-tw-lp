.class public final Lcom/samsung/android/app/memo/R$color;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/memo/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "color"
.end annotation


# static fields
.field public static final action_mode_spinner_color:I = 0x7f080023

.field public static final action_mode_spinner_pressd_color:I = 0x7f080024

.field public static final action_mode_spinner_text_color:I = 0x7f080022

.field public static final category_edit_color:I = 0x7f080011

.field public static final category_edit_line_color:I = 0x7f080010

.field public static final category_edit_ripple_color:I = 0x7f080012

.field public static final category_edit_text_color:I = 0x7f08000b

.field public static final category_list_edit_text_color:I = 0x7f08000a

.field public static final category_list_selected_text_color:I = 0x7f08000c

.field public static final category_list_text_color:I = 0x7f080009

.field public static final category_management_color:I = 0x7f080013

.field public static final category_management_list_text_color:I = 0x7f08000f

.field public static final category_management_ripple_color:I = 0x7f080014

.field public static final chooser_image_text_color:I = 0x7f080029

.field public static final drawer_bg:I = 0x7f080007

.field public static final edit_background_color:I = 0x7f080025

.field public static final edit_font_color:I = 0x7f080028

.field public static final edit_hint_color:I = 0x7f080026

.field public static final editor_background_color:I = 0x7f080042

.field public static final fab_btn_bg_color:I = 0x7f080037

.field public static final fab_btn_ripple_color:I = 0x7f080038

.field public static final grid_list_bg_color:I = 0x7f08003e

.field public static final image_viewer_bg_color:I = 0x7f080034

.field public static final image_viewr_actionbar_bg_color:I = 0x7f080035

.field public static final import_dialog_text_color:I = 0x7f080021

.field public static final memo_bg_color:I = 0x7f080002

.field public static final memo_list_background_color:I = 0x7f08003a

.field public static final memo_list_bg:I = 0x7f080041

.field public static final memo_list_boundray_color:I = 0x7f08001c

.field public static final memo_list_item_contents_color:I = 0x7f080015

.field public static final memo_list_item_contents_color_pressed:I = 0x7f08003c

.field public static final memo_list_item_contents_display_name_color:I = 0x7f080016

.field public static final memo_list_item_contents_display_name_color_pressed:I = 0x7f080017

.field public static final memo_list_item_date_time_color:I = 0x7f080018

.field public static final memo_list_item_date_time_color_pressed:I = 0x7f08003d

.field public static final memo_list_item_line_color:I = 0x7f080019

.field public static final memo_list_no_memos_popup_text_color:I = 0x7f08001b

.field public static final memo_list_no_memos_text_color:I = 0x7f08001a

.field public static final memo_primary_color:I = 0x7f08001d

.field public static final memo_primary_dark_color:I = 0x7f08001e

.field public static final menu_item_text_color:I = 0x7f08000d

.field public static final menu_item_text_color_disabled:I = 0x7f08000e

.field public static final quick_panel_memo_bg_color:I = 0x7f080001

.field public static final quick_panel_memo_time_text_color:I = 0x7f080000

.field public static final samsung_account_dialog_text_color:I = 0x7f080030

.field public static final selectall_text_color:I = 0x7f080020

.field public static final selectall_text_shadow:I = 0x7f08001f

.field public static final share_text_color:I = 0x7f080033

.field public static final sync_memo_dialog_text_color:I = 0x7f080032

.field public static final theme_action_bar_background_color:I = 0x7f080003

.field public static final theme_action_bar_text_shadow_color:I = 0x7f080005

.field public static final theme_action_bar_text_shadow_color_categories:I = 0x7f080039

.field public static final theme_action_bar_text_style_color:I = 0x7f080004

.field public static final theme_action_bar_text_style_color_fonblet:I = 0x7f08003b

.field public static final theme_edit_action_bar_menu_text_style_color:I = 0x7f080006

.field public static final title_font_color:I = 0x7f080027

.field public static final transparent:I = 0x7f080008

.field public static final undo_button_color:I = 0x7f080036

.field public static final voice_delete_btn_separator_color:I = 0x7f08003f

.field public static final voice_record_text_color:I = 0x7f080040

.field public static final voicememo_quickpanel_bg_color:I = 0x7f080031

.field public static final widget_bg_color:I = 0x7f08002c

.field public static final widget_empty_list_view_tablet_text_color:I = 0x7f08002e

.field public static final widget_no_memos_text_color:I = 0x7f08002d

.field public static final widget_text_color:I = 0x7f08002b

.field public static final widget_title_color:I = 0x7f08002a

.field public static final widget_voice_memo_list_view_text_color:I = 0x7f08002f


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 187
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
