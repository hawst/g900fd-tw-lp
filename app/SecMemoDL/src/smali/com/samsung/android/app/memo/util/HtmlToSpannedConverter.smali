.class Lcom/samsung/android/app/memo/util/HtmlToSpannedConverter;
.super Ljava/lang/Object;
.source "HtmlUtil.java"

# interfaces
.implements Lorg/xml/sax/ContentHandler;


# static fields
.field private static ctx:Landroid/content/Context;

.field private static onClickListener:Lcom/samsung/android/app/memo/uiwidget/ClickableSpanType$OnClickListener;


# instance fields
.field private mImageGetter:Lcom/samsung/android/app/memo/util/HtmlUtil$ImageGetter;

.field private mReader:Lorg/xml/sax/XMLReader;

.field private mSource:Ljava/lang/String;

.field private mSpannableStringBuilder:Landroid/text/SpannableStringBuilder;

.field private mysteryTagContent:Ljava/lang/String;

.field private mysteryTagFound:Z

.field private mysteryTagName:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/samsung/android/app/memo/util/HtmlUtil$ImageGetter;Lcom/samsung/android/app/memo/util/HtmlUtil$TagHandler;Lorg/ccil/cowan/tagsoup/Parser;Landroid/content/Context;Lcom/samsung/android/app/memo/uiwidget/ClickableSpanType$OnClickListener;J)V
    .locals 1
    .param p1, "source"    # Ljava/lang/String;
    .param p2, "imageGetter"    # Lcom/samsung/android/app/memo/util/HtmlUtil$ImageGetter;
    .param p3, "tagHandler"    # Lcom/samsung/android/app/memo/util/HtmlUtil$TagHandler;
    .param p4, "parser"    # Lorg/ccil/cowan/tagsoup/Parser;
    .param p5, "context"    # Landroid/content/Context;
    .param p6, "listener"    # Lcom/samsung/android/app/memo/uiwidget/ClickableSpanType$OnClickListener;
    .param p7, "_ID"    # J

    .prologue
    .line 431
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 434
    iput-object p1, p0, Lcom/samsung/android/app/memo/util/HtmlToSpannedConverter;->mSource:Ljava/lang/String;

    .line 435
    new-instance v0, Landroid/text/SpannableStringBuilder;

    invoke-direct {v0}, Landroid/text/SpannableStringBuilder;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/app/memo/util/HtmlToSpannedConverter;->mSpannableStringBuilder:Landroid/text/SpannableStringBuilder;

    .line 436
    iput-object p2, p0, Lcom/samsung/android/app/memo/util/HtmlToSpannedConverter;->mImageGetter:Lcom/samsung/android/app/memo/util/HtmlUtil$ImageGetter;

    .line 437
    iput-object p4, p0, Lcom/samsung/android/app/memo/util/HtmlToSpannedConverter;->mReader:Lorg/xml/sax/XMLReader;

    .line 438
    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/android/app/memo/util/HtmlToSpannedConverter;->mysteryTagContent:Ljava/lang/String;

    .line 439
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/app/memo/util/HtmlToSpannedConverter;->mysteryTagName:Ljava/lang/String;

    .line 440
    sput-object p5, Lcom/samsung/android/app/memo/util/HtmlToSpannedConverter;->ctx:Landroid/content/Context;

    .line 441
    sput-object p6, Lcom/samsung/android/app/memo/util/HtmlToSpannedConverter;->onClickListener:Lcom/samsung/android/app/memo/uiwidget/ClickableSpanType$OnClickListener;

    .line 443
    return-void
.end method

.method public static final convertValueToInt(Ljava/lang/CharSequence;I)I
    .locals 9
    .param p0, "charSeq"    # Ljava/lang/CharSequence;
    .param p1, "defaultValue"    # I

    .prologue
    const/4 v6, 0x0

    .line 992
    if-nez p0, :cond_0

    .line 1029
    .end local p1    # "defaultValue":I
    :goto_0
    return p1

    .line 995
    .restart local p1    # "defaultValue":I
    :cond_0
    invoke-interface {p0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v4

    .line 1000
    .local v4, "nm":Ljava/lang/String;
    const/4 v5, 0x1

    .line 1001
    .local v5, "sign":I
    const/4 v2, 0x0

    .line 1002
    .local v2, "index":I
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v3

    .line 1003
    .local v3, "len":I
    const/16 v0, 0xa

    .line 1005
    .local v0, "base":I
    const/16 v7, 0x2d

    invoke-virtual {v4, v6}, Ljava/lang/String;->charAt(I)C

    move-result v8

    if-ne v7, v8, :cond_1

    .line 1006
    const/4 v5, -0x1

    .line 1007
    add-int/lit8 v2, v2, 0x1

    .line 1010
    :cond_1
    const/16 v7, 0x30

    invoke-virtual {v4, v2}, Ljava/lang/String;->charAt(I)C

    move-result v8

    if-ne v7, v8, :cond_6

    .line 1012
    add-int/lit8 v7, v3, -0x1

    if-ne v2, v7, :cond_2

    move p1, v6

    .line 1013
    goto :goto_0

    .line 1015
    :cond_2
    add-int/lit8 v6, v2, 0x1

    invoke-virtual {v4, v6}, Ljava/lang/String;->charAt(I)C

    move-result v1

    .line 1017
    .local v1, "c":C
    const/16 v6, 0x78

    if-eq v6, v1, :cond_3

    const/16 v6, 0x58

    if-ne v6, v1, :cond_5

    .line 1018
    :cond_3
    add-int/lit8 v2, v2, 0x2

    .line 1019
    const/16 v0, 0x10

    .line 1029
    .end local v1    # "c":C
    :cond_4
    :goto_1
    invoke-virtual {v4, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6, v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v6

    mul-int p1, v6, v5

    goto :goto_0

    .line 1021
    .restart local v1    # "c":C
    :cond_5
    add-int/lit8 v2, v2, 0x1

    .line 1022
    const/16 v0, 0x8

    .line 1024
    goto :goto_1

    .end local v1    # "c":C
    :cond_6
    const/16 v6, 0x23

    invoke-virtual {v4, v2}, Ljava/lang/String;->charAt(I)C

    move-result v7

    if-ne v6, v7, :cond_4

    .line 1025
    add-int/lit8 v2, v2, 0x1

    .line 1026
    const/16 v0, 0x10

    goto :goto_1
.end method

.method private static handleBr(Landroid/text/SpannableStringBuilder;)V
    .locals 1
    .param p0, "text"    # Landroid/text/SpannableStringBuilder;

    .prologue
    .line 630
    const/16 v0, 0xa

    invoke-virtual {p0, v0}, Landroid/text/SpannableStringBuilder;->append(C)Landroid/text/SpannableStringBuilder;

    .line 631
    return-void
.end method

.method private handleEndTag(Ljava/lang/String;)V
    .locals 2
    .param p1, "tag"    # Ljava/lang/String;

    .prologue
    .line 552
    iget-boolean v0, p0, Lcom/samsung/android/app/memo/util/HtmlToSpannedConverter;->mysteryTagFound:Z

    if-nez v0, :cond_3

    .line 553
    const-string v0, "br"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 554
    iget-object v0, p0, Lcom/samsung/android/app/memo/util/HtmlToSpannedConverter;->mSpannableStringBuilder:Landroid/text/SpannableStringBuilder;

    invoke-static {v0}, Lcom/samsung/android/app/memo/util/HtmlToSpannedConverter;->handleBr(Landroid/text/SpannableStringBuilder;)V

    .line 611
    :cond_0
    :goto_0
    return-void

    .line 555
    :cond_1
    const-string v0, "p"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 556
    iget-object v0, p0, Lcom/samsung/android/app/memo/util/HtmlToSpannedConverter;->mSpannableStringBuilder:Landroid/text/SpannableStringBuilder;

    invoke-static {v0}, Lcom/samsung/android/app/memo/util/HtmlToSpannedConverter;->handleP(Landroid/text/SpannableStringBuilder;)V

    goto :goto_0

    .line 557
    :cond_2
    const-string v0, "div"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 558
    iget-object v0, p0, Lcom/samsung/android/app/memo/util/HtmlToSpannedConverter;->mSpannableStringBuilder:Landroid/text/SpannableStringBuilder;

    invoke-static {v0}, Lcom/samsung/android/app/memo/util/HtmlToSpannedConverter;->handleP(Landroid/text/SpannableStringBuilder;)V

    goto :goto_0

    .line 599
    :cond_3
    const-string v0, "html"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "body"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 603
    iget-object v0, p0, Lcom/samsung/android/app/memo/util/HtmlToSpannedConverter;->mysteryTagName:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 604
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/app/memo/util/HtmlToSpannedConverter;->mysteryTagFound:Z

    .line 605
    iget-object v0, p0, Lcom/samsung/android/app/memo/util/HtmlToSpannedConverter;->mSpannableStringBuilder:Landroid/text/SpannableStringBuilder;

    iget-object v1, p0, Lcom/samsung/android/app/memo/util/HtmlToSpannedConverter;->mysteryTagContent:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    goto :goto_0
.end method

.method private static handleP(Landroid/text/SpannableStringBuilder;)V
    .locals 1
    .param p0, "text"    # Landroid/text/SpannableStringBuilder;

    .prologue
    .line 625
    const/16 v0, 0xa

    invoke-virtual {p0, v0}, Landroid/text/SpannableStringBuilder;->append(C)Landroid/text/SpannableStringBuilder;

    .line 627
    return-void
.end method

.method private handleStartTag(Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 2
    .param p1, "tag"    # Ljava/lang/String;
    .param p2, "attributes"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 489
    iget-boolean v0, p0, Lcom/samsung/android/app/memo/util/HtmlToSpannedConverter;->mysteryTagFound:Z

    if-nez v0, :cond_0

    .line 491
    const-string v0, "br"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 498
    const-string v0, "p"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 500
    const-string v0, "div"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 501
    iget-object v0, p0, Lcom/samsung/android/app/memo/util/HtmlToSpannedConverter;->mSpannableStringBuilder:Landroid/text/SpannableStringBuilder;

    invoke-static {v0}, Lcom/samsung/android/app/memo/util/HtmlToSpannedConverter;->handleP(Landroid/text/SpannableStringBuilder;)V

    .line 549
    :cond_0
    :goto_0
    return-void

    .line 535
    :cond_1
    const-string v0, "img"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 536
    iget-object v0, p0, Lcom/samsung/android/app/memo/util/HtmlToSpannedConverter;->mSpannableStringBuilder:Landroid/text/SpannableStringBuilder;

    iget-object v1, p0, Lcom/samsung/android/app/memo/util/HtmlToSpannedConverter;->mImageGetter:Lcom/samsung/android/app/memo/util/HtmlUtil$ImageGetter;

    invoke-static {v0, p2, v1}, Lcom/samsung/android/app/memo/util/HtmlToSpannedConverter;->startImg(Landroid/text/SpannableStringBuilder;Lorg/xml/sax/Attributes;Lcom/samsung/android/app/memo/util/HtmlUtil$ImageGetter;)V

    goto :goto_0

    .line 539
    :cond_2
    const-string v0, "html"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "body"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 543
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/app/memo/util/HtmlToSpannedConverter;->mysteryTagFound:Z

    .line 544
    iput-object p1, p0, Lcom/samsung/android/app/memo/util/HtmlToSpannedConverter;->mysteryTagName:Ljava/lang/String;

    goto :goto_0
.end method

.method private static startImg(Landroid/text/SpannableStringBuilder;Lorg/xml/sax/Attributes;Lcom/samsung/android/app/memo/util/HtmlUtil$ImageGetter;)V
    .locals 10
    .param p0, "text"    # Landroid/text/SpannableStringBuilder;
    .param p1, "attributes"    # Lorg/xml/sax/Attributes;
    .param p2, "img"    # Lcom/samsung/android/app/memo/util/HtmlUtil$ImageGetter;

    .prologue
    const/16 v9, 0x21

    .line 670
    if-nez p1, :cond_0

    .line 690
    :goto_0
    return-void

    .line 672
    :cond_0
    const-string v7, "src"

    invoke-interface {p1, v7}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 673
    .local v6, "src":Ljava/lang/String;
    const-string v7, "orientation"

    invoke-interface {p1, v7}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 674
    .local v5, "orientation":Ljava/lang/String;
    const-string v7, "alttext"

    invoke-interface {p1, v7}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 675
    .local v0, "altText":Ljava/lang/String;
    if-nez v0, :cond_1

    .line 676
    const-string v0, ""

    .line 677
    :cond_1
    invoke-static {v6}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 678
    .local v2, "attachmentUri":Landroid/net/Uri;
    invoke-virtual {p0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v4

    .line 679
    .local v4, "len":I
    const v7, 0xfffc

    invoke-virtual {p0, v7}, Landroid/text/SpannableStringBuilder;->append(C)Landroid/text/SpannableStringBuilder;

    .line 680
    new-instance v3, Lcom/samsung/android/app/memo/uiwidget/HtmlImageSpan;

    sget-object v7, Lcom/samsung/android/app/memo/util/HtmlToSpannedConverter;->ctx:Landroid/content/Context;

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v8

    invoke-direct {v3, v7, v2, v8, v0}, Lcom/samsung/android/app/memo/uiwidget/HtmlImageSpan;-><init>(Landroid/content/Context;Landroid/net/Uri;ILjava/lang/String;)V

    .line 681
    .local v3, "imageSpan":Lcom/samsung/android/app/memo/uiwidget/HtmlImageSpan;
    sget-object v7, Lcom/samsung/android/app/memo/util/HtmlToSpannedConverter;->onClickListener:Lcom/samsung/android/app/memo/uiwidget/ClickableSpanType$OnClickListener;

    invoke-virtual {v3, v7}, Lcom/samsung/android/app/memo/uiwidget/HtmlImageSpan;->setOnClickListener(Lcom/samsung/android/app/memo/uiwidget/ClickableSpanType$OnClickListener;)V

    .line 682
    invoke-virtual {p0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v7

    invoke-virtual {p0, v3, v4, v7, v9}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 684
    new-instance v1, Landroid/text/style/AlignmentSpan$Standard;

    sget-object v7, Landroid/text/Layout$Alignment;->ALIGN_LEFT:Landroid/text/Layout$Alignment;

    invoke-direct {v1, v7}, Landroid/text/style/AlignmentSpan$Standard;-><init>(Landroid/text/Layout$Alignment;)V

    .line 686
    .local v1, "as":Landroid/text/style/AlignmentSpan$Standard;
    invoke-static {}, Lcom/samsung/android/app/memo/util/Utils;->isTablet()Z

    move-result v7

    if-eqz v7, :cond_2

    .line 687
    new-instance v1, Landroid/text/style/AlignmentSpan$Standard;

    .end local v1    # "as":Landroid/text/style/AlignmentSpan$Standard;
    sget-object v7, Landroid/text/Layout$Alignment;->ALIGN_CENTER:Landroid/text/Layout$Alignment;

    invoke-direct {v1, v7}, Landroid/text/style/AlignmentSpan$Standard;-><init>(Landroid/text/Layout$Alignment;)V

    .line 689
    .restart local v1    # "as":Landroid/text/style/AlignmentSpan$Standard;
    :cond_2
    invoke-virtual {p0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v7

    invoke-virtual {p0, v1, v4, v7, v9}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    goto :goto_0
.end method


# virtual methods
.method public characters([CII)V
    .locals 10
    .param p1, "ch"    # [C
    .param p2, "start"    # I
    .param p3, "length"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xml/sax/SAXException;
        }
    .end annotation

    .prologue
    const/16 v9, 0xa

    const/16 v8, 0x20

    .line 834
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .line 841
    .local v5, "sb":Ljava/lang/StringBuilder;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-lt v2, p3, :cond_0

    .line 869
    :try_start_0
    iget-boolean v6, p0, Lcom/samsung/android/app/memo/util/HtmlToSpannedConverter;->mysteryTagFound:Z

    if-eqz v6, :cond_7

    .line 870
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->length()I

    move-result v6

    if-ge v6, p3, :cond_6

    .line 871
    iget-object v6, p0, Lcom/samsung/android/app/memo/util/HtmlToSpannedConverter;->mysteryTagContent:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v7, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    add-int/lit8 v8, p3, -0x1

    invoke-virtual {v6, p2, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/samsung/android/app/memo/util/HtmlToSpannedConverter;->mysteryTagContent:Ljava/lang/String;

    .line 876
    :goto_1
    const-string v6, "HELL"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "sb.length()"

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->length()I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 880
    :goto_2
    return-void

    .line 842
    :cond_0
    add-int v6, v2, p2

    aget-char v0, p1, v6

    .line 844
    .local v0, "c":C
    if-eq v0, v8, :cond_1

    if-ne v0, v9, :cond_5

    .line 846
    :cond_1
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->length()I

    move-result v3

    .line 848
    .local v3, "len":I
    if-nez v3, :cond_4

    .line 849
    iget-object v6, p0, Lcom/samsung/android/app/memo/util/HtmlToSpannedConverter;->mSpannableStringBuilder:Landroid/text/SpannableStringBuilder;

    invoke-virtual {v6}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v3

    .line 851
    if-nez v3, :cond_3

    .line 852
    const/16 v4, 0xa

    .line 860
    .local v4, "pred":C
    :goto_3
    if-eq v4, v8, :cond_2

    if-eq v4, v9, :cond_2

    .line 861
    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 841
    .end local v3    # "len":I
    .end local v4    # "pred":C
    :cond_2
    :goto_4
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 854
    .restart local v3    # "len":I
    :cond_3
    iget-object v6, p0, Lcom/samsung/android/app/memo/util/HtmlToSpannedConverter;->mSpannableStringBuilder:Landroid/text/SpannableStringBuilder;

    add-int/lit8 v7, v3, -0x1

    invoke-virtual {v6, v7}, Landroid/text/SpannableStringBuilder;->charAt(I)C

    move-result v4

    .line 856
    .restart local v4    # "pred":C
    goto :goto_3

    .line 857
    .end local v4    # "pred":C
    :cond_4
    add-int/lit8 v6, v3, -0x1

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v4

    .restart local v4    # "pred":C
    goto :goto_3

    .line 864
    .end local v3    # "len":I
    .end local v4    # "pred":C
    :cond_5
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_4

    .line 873
    .end local v0    # "c":C
    :cond_6
    :try_start_1
    iget-object v6, p0, Lcom/samsung/android/app/memo/util/HtmlToSpannedConverter;->mysteryTagContent:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v7, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6, p2, p3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/samsung/android/app/memo/util/HtmlToSpannedConverter;->mysteryTagContent:Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    .line 877
    :catch_0
    move-exception v1

    .line 878
    .local v1, "e":Ljava/lang/Exception;
    const-string v6, "HtmlUtil"

    const-string v7, "setOverFlowMenu exception"

    invoke-static {v6, v7, v1}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_2

    .line 875
    .end local v1    # "e":Ljava/lang/Exception;
    :cond_7
    :try_start_2
    iget-object v6, p0, Lcom/samsung/android/app/memo/util/HtmlToSpannedConverter;->mSpannableStringBuilder:Landroid/text/SpannableStringBuilder;

    invoke-virtual {v6, v5}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_1
.end method

.method public convert()Landroid/text/Spanned;
    .locals 11

    .prologue
    const/16 v10, 0xa

    .line 447
    iget-object v6, p0, Lcom/samsung/android/app/memo/util/HtmlToSpannedConverter;->mReader:Lorg/xml/sax/XMLReader;

    invoke-interface {v6, p0}, Lorg/xml/sax/XMLReader;->setContentHandler(Lorg/xml/sax/ContentHandler;)V

    .line 449
    :try_start_0
    iget-object v6, p0, Lcom/samsung/android/app/memo/util/HtmlToSpannedConverter;->mReader:Lorg/xml/sax/XMLReader;

    new-instance v7, Lorg/xml/sax/InputSource;

    new-instance v8, Ljava/io/StringReader;

    iget-object v9, p0, Lcom/samsung/android/app/memo/util/HtmlToSpannedConverter;->mSource:Ljava/lang/String;

    invoke-direct {v8, v9}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V

    invoke-direct {v7, v8}, Lorg/xml/sax/InputSource;-><init>(Ljava/io/Reader;)V

    invoke-interface {v6, v7}, Lorg/xml/sax/XMLReader;->parse(Lorg/xml/sax/InputSource;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lorg/xml/sax/SAXException; {:try_start_0 .. :try_end_0} :catch_1

    .line 459
    iget-object v6, p0, Lcom/samsung/android/app/memo/util/HtmlToSpannedConverter;->mSpannableStringBuilder:Landroid/text/SpannableStringBuilder;

    const/4 v7, 0x0

    iget-object v8, p0, Lcom/samsung/android/app/memo/util/HtmlToSpannedConverter;->mSpannableStringBuilder:Landroid/text/SpannableStringBuilder;

    invoke-virtual {v8}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v8

    .line 460
    const-class v9, Landroid/text/style/ParagraphStyle;

    .line 459
    invoke-virtual {v6, v7, v8, v9}, Landroid/text/SpannableStringBuilder;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v4

    .line 461
    .local v4, "obj":[Ljava/lang/Object;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    array-length v6, v4

    if-lt v2, v6, :cond_1

    .line 482
    iget-object v6, p0, Lcom/samsung/android/app/memo/util/HtmlToSpannedConverter;->mSpannableStringBuilder:Landroid/text/SpannableStringBuilder;

    invoke-virtual {v6}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v6

    add-int/lit8 v3, v6, -0x1

    .line 483
    .local v3, "length":I
    if-lez v3, :cond_0

    iget-object v6, p0, Lcom/samsung/android/app/memo/util/HtmlToSpannedConverter;->mSpannableStringBuilder:Landroid/text/SpannableStringBuilder;

    invoke-virtual {v6, v3}, Landroid/text/SpannableStringBuilder;->charAt(I)C

    move-result v6

    if-ne v6, v10, :cond_0

    .line 484
    iget-object v6, p0, Lcom/samsung/android/app/memo/util/HtmlToSpannedConverter;->mSpannableStringBuilder:Landroid/text/SpannableStringBuilder;

    add-int/lit8 v7, v3, 0x1

    const-string v8, ""

    invoke-virtual {v6, v3, v7, v8}, Landroid/text/SpannableStringBuilder;->replace(IILjava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 485
    :cond_0
    iget-object v6, p0, Lcom/samsung/android/app/memo/util/HtmlToSpannedConverter;->mSpannableStringBuilder:Landroid/text/SpannableStringBuilder;

    return-object v6

    .line 450
    .end local v2    # "i":I
    .end local v3    # "length":I
    .end local v4    # "obj":[Ljava/lang/Object;
    :catch_0
    move-exception v0

    .line 452
    .local v0, "e":Ljava/io/IOException;
    new-instance v6, Ljava/lang/RuntimeException;

    invoke-direct {v6, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v6

    .line 453
    .end local v0    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v0

    .line 455
    .local v0, "e":Lorg/xml/sax/SAXException;
    new-instance v6, Ljava/lang/RuntimeException;

    invoke-direct {v6, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v6

    .line 462
    .end local v0    # "e":Lorg/xml/sax/SAXException;
    .restart local v2    # "i":I
    .restart local v4    # "obj":[Ljava/lang/Object;
    :cond_1
    iget-object v6, p0, Lcom/samsung/android/app/memo/util/HtmlToSpannedConverter;->mSpannableStringBuilder:Landroid/text/SpannableStringBuilder;

    aget-object v7, v4, v2

    invoke-virtual {v6, v7}, Landroid/text/SpannableStringBuilder;->getSpanStart(Ljava/lang/Object;)I

    move-result v5

    .line 463
    .local v5, "start":I
    iget-object v6, p0, Lcom/samsung/android/app/memo/util/HtmlToSpannedConverter;->mSpannableStringBuilder:Landroid/text/SpannableStringBuilder;

    aget-object v7, v4, v2

    invoke-virtual {v6, v7}, Landroid/text/SpannableStringBuilder;->getSpanEnd(Ljava/lang/Object;)I

    move-result v1

    .line 466
    .local v1, "end":I
    add-int/lit8 v6, v1, -0x2

    if-ltz v6, :cond_2

    .line 467
    iget-object v6, p0, Lcom/samsung/android/app/memo/util/HtmlToSpannedConverter;->mSpannableStringBuilder:Landroid/text/SpannableStringBuilder;

    add-int/lit8 v7, v1, -0x1

    invoke-virtual {v6, v7}, Landroid/text/SpannableStringBuilder;->charAt(I)C

    move-result v6

    if-ne v6, v10, :cond_2

    .line 468
    iget-object v6, p0, Lcom/samsung/android/app/memo/util/HtmlToSpannedConverter;->mSpannableStringBuilder:Landroid/text/SpannableStringBuilder;

    add-int/lit8 v7, v1, -0x2

    invoke-virtual {v6, v7}, Landroid/text/SpannableStringBuilder;->charAt(I)C

    move-result v6

    if-ne v6, v10, :cond_2

    .line 469
    add-int/lit8 v1, v1, -0x1

    .line 473
    :cond_2
    if-ne v1, v5, :cond_3

    .line 474
    iget-object v6, p0, Lcom/samsung/android/app/memo/util/HtmlToSpannedConverter;->mSpannableStringBuilder:Landroid/text/SpannableStringBuilder;

    aget-object v7, v4, v2

    invoke-virtual {v6, v7}, Landroid/text/SpannableStringBuilder;->removeSpan(Ljava/lang/Object;)V

    .line 461
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 477
    :cond_3
    :try_start_1
    iget-object v6, p0, Lcom/samsung/android/app/memo/util/HtmlToSpannedConverter;->mSpannableStringBuilder:Landroid/text/SpannableStringBuilder;

    aget-object v7, v4, v2

    const/16 v8, 0x33

    invoke-virtual {v6, v7, v5, v1, v8}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2

    goto :goto_1

    .line 478
    :catch_2
    move-exception v6

    goto :goto_1
.end method

.method public endDocument()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xml/sax/SAXException;
        }
    .end annotation

    .prologue
    .line 792
    return-void
.end method

.method public endElement(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "uri"    # Ljava/lang/String;
    .param p2, "localName"    # Ljava/lang/String;
    .param p3, "qName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xml/sax/SAXException;
        }
    .end annotation

    .prologue
    .line 827
    iget-boolean v0, p0, Lcom/samsung/android/app/memo/util/HtmlToSpannedConverter;->mysteryTagFound:Z

    if-eqz v0, :cond_0

    .line 828
    iget-object v0, p0, Lcom/samsung/android/app/memo/util/HtmlToSpannedConverter;->mysteryTagContent:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v0, "</"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ">"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/memo/util/HtmlToSpannedConverter;->mysteryTagContent:Ljava/lang/String;

    .line 830
    :cond_0
    invoke-direct {p0, p2}, Lcom/samsung/android/app/memo/util/HtmlToSpannedConverter;->handleEndTag(Ljava/lang/String;)V

    .line 831
    return-void
.end method

.method public endPrefixMapping(Ljava/lang/String;)V
    .locals 0
    .param p1, "prefix"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xml/sax/SAXException;
        }
    .end annotation

    .prologue
    .line 798
    return-void
.end method

.method public ignorableWhitespace([CII)V
    .locals 0
    .param p1, "ch"    # [C
    .param p2, "start"    # I
    .param p3, "length"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xml/sax/SAXException;
        }
    .end annotation

    .prologue
    .line 883
    return-void
.end method

.method public processingInstruction(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "target"    # Ljava/lang/String;
    .param p2, "data"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xml/sax/SAXException;
        }
    .end annotation

    .prologue
    .line 886
    return-void
.end method

.method public setDocumentLocator(Lorg/xml/sax/Locator;)V
    .locals 0
    .param p1, "locator"    # Lorg/xml/sax/Locator;

    .prologue
    .line 786
    return-void
.end method

.method public skippedEntity(Ljava/lang/String;)V
    .locals 0
    .param p1, "name"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xml/sax/SAXException;
        }
    .end annotation

    .prologue
    .line 889
    return-void
.end method

.method public startDocument()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xml/sax/SAXException;
        }
    .end annotation

    .prologue
    .line 789
    return-void
.end method

.method public startElement(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 6
    .param p1, "uri"    # Ljava/lang/String;
    .param p2, "localName"    # Ljava/lang/String;
    .param p3, "qName"    # Ljava/lang/String;
    .param p4, "attributes"    # Lorg/xml/sax/Attributes;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xml/sax/SAXException;
        }
    .end annotation

    .prologue
    .line 803
    iget-boolean v4, p0, Lcom/samsung/android/app/memo/util/HtmlToSpannedConverter;->mysteryTagFound:Z

    if-nez v4, :cond_0

    .line 804
    const-string v4, ""

    iput-object v4, p0, Lcom/samsung/android/app/memo/util/HtmlToSpannedConverter;->mysteryTagContent:Ljava/lang/String;

    .line 807
    :cond_0
    move-object v2, p2

    .line 808
    .local v2, "eName":Ljava/lang/String;
    const-string v4, ""

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 809
    move-object v2, p3

    .line 810
    :cond_1
    iget-object v4, p0, Lcom/samsung/android/app/memo/util/HtmlToSpannedConverter;->mysteryTagContent:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v5, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, "<"

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/samsung/android/app/memo/util/HtmlToSpannedConverter;->mysteryTagContent:Ljava/lang/String;

    .line 811
    if-eqz p4, :cond_2

    .line 812
    invoke-interface {p4}, Lorg/xml/sax/Attributes;->getLength()I

    move-result v1

    .line 813
    .local v1, "attributesLength":I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    if-lt v3, v1, :cond_3

    .line 821
    .end local v1    # "attributesLength":I
    .end local v3    # "i":I
    :cond_2
    iget-object v4, p0, Lcom/samsung/android/app/memo/util/HtmlToSpannedConverter;->mysteryTagContent:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v5, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, ">"

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/samsung/android/app/memo/util/HtmlToSpannedConverter;->mysteryTagContent:Ljava/lang/String;

    .line 823
    invoke-direct {p0, p2, p4}, Lcom/samsung/android/app/memo/util/HtmlToSpannedConverter;->handleStartTag(Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    .line 824
    return-void

    .line 814
    .restart local v1    # "attributesLength":I
    .restart local v3    # "i":I
    :cond_3
    invoke-interface {p4, v3}, Lorg/xml/sax/Attributes;->getLocalName(I)Ljava/lang/String;

    move-result-object v0

    .line 815
    .local v0, "aName":Ljava/lang/String;
    const-string v4, ""

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 816
    invoke-interface {p4, v3}, Lorg/xml/sax/Attributes;->getQName(I)Ljava/lang/String;

    move-result-object v0

    .line 817
    :cond_4
    iget-object v4, p0, Lcom/samsung/android/app/memo/util/HtmlToSpannedConverter;->mysteryTagContent:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v5, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, " "

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/samsung/android/app/memo/util/HtmlToSpannedConverter;->mysteryTagContent:Ljava/lang/String;

    .line 818
    iget-object v4, p0, Lcom/samsung/android/app/memo/util/HtmlToSpannedConverter;->mysteryTagContent:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v5, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "=\""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-interface {p4, v3}, Lorg/xml/sax/Attributes;->getValue(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/samsung/android/app/memo/util/HtmlToSpannedConverter;->mysteryTagContent:Ljava/lang/String;

    .line 813
    add-int/lit8 v3, v3, 0x1

    goto :goto_0
.end method

.method public startPrefixMapping(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "prefix"    # Ljava/lang/String;
    .param p2, "uri"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xml/sax/SAXException;
        }
    .end annotation

    .prologue
    .line 795
    return-void
.end method
