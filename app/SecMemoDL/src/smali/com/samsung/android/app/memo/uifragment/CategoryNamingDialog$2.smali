.class Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog$2;
.super Ljava/lang/Object;
.source "CategoryNamingDialog.java"

# interfaces
.implements Landroid/text/TextWatcher;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog$2;->this$0:Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;

    .line 179
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 0
    .param p1, "s"    # Landroid/text/Editable;

    .prologue
    .line 194
    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "count"    # I
    .param p4, "after"    # I

    .prologue
    .line 191
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 2
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "before"    # I
    .param p4, "count"    # I

    .prologue
    .line 183
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 185
    .local v0, "CategoryName":Ljava/lang/String;
    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog$2;->this$0:Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;

    # invokes: Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;->setPositiveButtonEnabled(Ljava/lang/String;)V
    invoke-static {v1, v0}, Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;->access$2(Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;Ljava/lang/String;)V

    .line 186
    return-void
.end method
