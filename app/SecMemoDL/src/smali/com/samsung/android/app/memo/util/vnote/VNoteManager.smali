.class public Lcom/samsung/android/app/memo/util/vnote/VNoteManager;
.super Ljava/lang/Object;
.source "VNoteManager.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mVn:Lcom/samsung/android/app/memo/util/vnote/pim/VNote;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 49
    const-class v0, Lcom/samsung/android/app/memo/util/vnote/VNoteManager;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/app/memo/util/vnote/VNoteManager;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "mContext"    # Landroid/content/Context;

    .prologue
    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    new-instance v0, Lcom/samsung/android/app/memo/util/vnote/pim/VNote;

    invoke-direct {v0}, Lcom/samsung/android/app/memo/util/vnote/pim/VNote;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/app/memo/util/vnote/VNoteManager;->mVn:Lcom/samsung/android/app/memo/util/vnote/pim/VNote;

    .line 54
    return-void
.end method

.method private decodeText(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 16
    .param p1, "value"    # Ljava/lang/String;
    .param p2, "targetCharset"    # Ljava/lang/String;
    .param p3, "encoding"    # Ljava/lang/String;

    .prologue
    .line 156
    const/4 v10, 0x0

    .line 157
    .local v10, "nextCh":C
    if-eqz p3, :cond_6

    .line 158
    const-string v12, "QUOTED-PRINTABLE"

    move-object/from16 v0, p3

    invoke-virtual {v12, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_6

    .line 159
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 160
    .local v1, "builder":Ljava/lang/StringBuilder;
    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->length()I

    move-result v7

    .line 161
    .local v7, "length":I
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_0
    if-lt v6, v7, :cond_0

    .line 175
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    .line 178
    .local v11, "quotedPrintable":Ljava/lang/String;
    const/4 v12, 0x0

    invoke-virtual {v1, v12}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 179
    move-object/from16 v0, p0

    invoke-direct {v0, v11, v1}, Lcom/samsung/android/app/memo/util/vnote/VNoteManager;->getLines(Ljava/lang/String;Ljava/lang/StringBuilder;)[Ljava/lang/String;

    move-result-object v9

    .line 181
    .local v9, "lines":[Ljava/lang/String;
    const/4 v12, 0x0

    invoke-virtual {v1, v12}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 182
    array-length v13, v9

    const/4 v12, 0x0

    :goto_1
    if-lt v12, v13, :cond_4

    .line 190
    :try_start_0
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    const-string v13, "UTF-8"

    invoke-virtual {v12, v13}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 196
    .local v2, "bytes":[B
    :goto_2
    :try_start_1
    invoke-static {v2}, Lorg/apache/commons/codec/net/QuotedPrintableCodec;->decodeQuotedPrintable([B)[B
    :try_end_1
    .catch Lorg/apache/commons/codec/DecoderException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v2

    .line 203
    :try_start_2
    new-instance v12, Ljava/lang/String;

    move-object/from16 v0, p2

    invoke-direct {v12, v2, v0}, Ljava/lang/String;-><init>([BLjava/lang/String;)V
    :try_end_2
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_2 .. :try_end_2} :catch_2

    .line 209
    .end local v1    # "builder":Ljava/lang/StringBuilder;
    .end local v2    # "bytes":[B
    .end local v6    # "i":I
    .end local v7    # "length":I
    .end local v9    # "lines":[Ljava/lang/String;
    .end local v11    # "quotedPrintable":Ljava/lang/String;
    :goto_3
    return-object v12

    .line 162
    .restart local v1    # "builder":Ljava/lang/StringBuilder;
    .restart local v6    # "i":I
    .restart local v7    # "length":I
    :cond_0
    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, Ljava/lang/String;->charAt(I)C

    move-result v3

    .line 163
    .local v3, "ch":C
    const/16 v12, 0x3d

    if-ne v3, v12, :cond_3

    add-int/lit8 v12, v7, -0x1

    if-ge v6, v12, :cond_3

    .line 164
    add-int/lit8 v12, v6, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, Ljava/lang/String;->charAt(I)C

    move-result v10

    .line 165
    const/16 v12, 0x20

    if-eq v10, v12, :cond_1

    const/16 v12, 0x9

    if-ne v10, v12, :cond_3

    .line 167
    :cond_1
    invoke-virtual {v1, v10}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 168
    add-int/lit8 v6, v6, 0x1

    .line 161
    :cond_2
    :goto_4
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 172
    :cond_3
    const/16 v12, 0x3d

    if-eq v10, v12, :cond_2

    .line 173
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_4

    .line 182
    .end local v3    # "ch":C
    .restart local v9    # "lines":[Ljava/lang/String;
    .restart local v11    # "quotedPrintable":Ljava/lang/String;
    :cond_4
    aget-object v8, v9, v12

    .line 183
    .local v8, "line":Ljava/lang/String;
    const-string v14, "="

    invoke-virtual {v8, v14}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v14

    if-eqz v14, :cond_5

    .line 184
    const/4 v14, 0x0

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v15

    add-int/lit8 v15, v15, -0x1

    invoke-virtual {v8, v14, v15}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v8

    .line 186
    :cond_5
    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 182
    add-int/lit8 v12, v12, 0x1

    goto :goto_1

    .line 191
    .end local v8    # "line":Ljava/lang/String;
    :catch_0
    move-exception v5

    .line 192
    .local v5, "e1":Ljava/io/UnsupportedEncodingException;
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/String;->getBytes()[B

    move-result-object v2

    .restart local v2    # "bytes":[B
    goto :goto_2

    .line 197
    .end local v5    # "e1":Ljava/io/UnsupportedEncodingException;
    :catch_1
    move-exception v4

    .line 198
    .local v4, "e":Lorg/apache/commons/codec/DecoderException;
    sget-object v12, Lcom/samsung/android/app/memo/util/vnote/VNoteManager;->TAG:Ljava/lang/String;

    const-string v13, " decodeText "

    invoke-static {v12, v13, v4}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 199
    const-string v12, ""

    goto :goto_3

    .line 204
    .end local v4    # "e":Lorg/apache/commons/codec/DecoderException;
    :catch_2
    move-exception v4

    .line 205
    .local v4, "e":Ljava/io/UnsupportedEncodingException;
    new-instance v12, Ljava/lang/String;

    invoke-direct {v12, v2}, Ljava/lang/String;-><init>([B)V

    goto :goto_3

    .line 209
    .end local v1    # "builder":Ljava/lang/StringBuilder;
    .end local v2    # "bytes":[B
    .end local v4    # "e":Ljava/io/UnsupportedEncodingException;
    .end local v6    # "i":I
    .end local v7    # "length":I
    .end local v9    # "lines":[Ljava/lang/String;
    .end local v11    # "quotedPrintable":Ljava/lang/String;
    :cond_6
    invoke-direct/range {p0 .. p2}, Lcom/samsung/android/app/memo/util/vnote/VNoteManager;->encodeString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    goto :goto_3
.end method

.method private encodeString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p1, "originalString"    # Ljava/lang/String;
    .param p2, "targetCharset"    # Ljava/lang/String;

    .prologue
    .line 245
    invoke-static {p2}, Ljava/nio/charset/Charset;->forName(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v2

    .line 246
    .local v2, "charset":Ljava/nio/charset/Charset;
    invoke-virtual {v2, p1}, Ljava/nio/charset/Charset;->encode(Ljava/lang/String;)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 247
    .local v0, "byteBuffer":Ljava/nio/ByteBuffer;
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v4

    new-array v1, v4, [B

    .line 248
    .local v1, "bytes":[B
    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->get([B)Ljava/nio/ByteBuffer;

    .line 250
    :try_start_0
    new-instance v4, Ljava/lang/String;

    invoke-direct {v4, v1, p2}, Ljava/lang/String;-><init>([BLjava/lang/String;)V
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    .line 253
    :goto_0
    return-object v4

    .line 251
    :catch_0
    move-exception v3

    .line 253
    .local v3, "e":Ljava/io/UnsupportedEncodingException;
    const/4 v4, 0x0

    goto :goto_0
.end method

.method private getLines(Ljava/lang/String;Ljava/lang/StringBuilder;)[Ljava/lang/String;
    .locals 10
    .param p1, "quotedPrintable"    # Ljava/lang/String;
    .param p2, "builder"    # Ljava/lang/StringBuilder;

    .prologue
    const/16 v9, 0xa

    const/4 v8, 0x0

    .line 216
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    .line 217
    .local v3, "length":I
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 218
    .local v5, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-lt v2, v3, :cond_1

    .line 236
    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 237
    .local v1, "finalLine":Ljava/lang/String;
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v7

    if-lez v7, :cond_0

    .line 238
    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 240
    :cond_0
    new-array v7, v8, [Ljava/lang/String;

    invoke-virtual {v5, v7}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [Ljava/lang/String;

    .line 241
    .local v4, "lines":[Ljava/lang/String;
    return-object v4

    .line 219
    .end local v1    # "finalLine":Ljava/lang/String;
    .end local v4    # "lines":[Ljava/lang/String;
    :cond_1
    invoke-virtual {p1, v2}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 220
    .local v0, "ch":C
    if-ne v0, v9, :cond_3

    .line 221
    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 222
    invoke-virtual {p2, v8}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 218
    :cond_2
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 223
    :cond_3
    const/16 v7, 0xd

    if-ne v0, v7, :cond_4

    .line 224
    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 225
    invoke-virtual {p2, v8}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 226
    add-int/lit8 v7, v3, -0x1

    if-ge v2, v7, :cond_2

    .line 227
    add-int/lit8 v7, v2, 0x1

    invoke-virtual {p1, v7}, Ljava/lang/String;->charAt(I)C

    move-result v6

    .line 228
    .local v6, "nextCh":C
    if-ne v6, v9, :cond_2

    .line 229
    add-int/lit8 v2, v2, 0x1

    .line 232
    goto :goto_1

    .line 233
    .end local v6    # "nextCh":C
    :cond_4
    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_1
.end method

.method private getUri(Landroid/content/Context;Landroid/content/Intent;)Landroid/net/Uri;
    .locals 5
    .param p1, "ctx"    # Landroid/content/Context;
    .param p2, "i"    # Landroid/content/Intent;

    .prologue
    .line 61
    const/4 v2, 0x0

    .line 62
    .local v2, "uri":Landroid/net/Uri;
    if-eqz p2, :cond_0

    .line 63
    invoke-virtual {p2}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v2

    .line 64
    if-eqz v2, :cond_2

    .line 65
    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v4, "com.google.android.apps.docs"

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 66
    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/samsung/android/app/migration/utils/Utils;->getTitle(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 67
    .local v0, "fileName":Ljava/lang/String;
    new-instance v3, Ljava/lang/StringBuilder;

    sget-object v4, Lcom/samsung/android/app/migration/utils/Utils;->SNB_TMP_FOLDER_PATH:Ljava/lang/String;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ".vnt"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 68
    .local v1, "filePath":Ljava/lang/String;
    invoke-static {p1, v2, v1}, Lcom/samsung/android/app/memo/util/FileHelper;->writeFileFromURI(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;)V

    .line 69
    new-instance v3, Ljava/io/File;

    invoke-direct {v3, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v3}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v2

    .line 77
    .end local v0    # "fileName":Ljava/lang/String;
    .end local v1    # "filePath":Ljava/lang/String;
    :cond_0
    :goto_0
    return-object v2

    .line 70
    :cond_1
    const-string v3, "android.intent.extra.STREAM"

    invoke-virtual {p2, v3}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 71
    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v3

    const-string v4, "android.intent.extra.STREAM"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    .end local v2    # "uri":Landroid/net/Uri;
    check-cast v2, Landroid/net/Uri;

    .line 73
    .restart local v2    # "uri":Landroid/net/Uri;
    goto :goto_0

    :cond_2
    const-string v3, "android.intent.extra.STREAM"

    invoke-virtual {p2, v3}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 74
    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v3

    const-string v4, "android.intent.extra.STREAM"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    .end local v2    # "uri":Landroid/net/Uri;
    check-cast v2, Landroid/net/Uri;

    .restart local v2    # "uri":Landroid/net/Uri;
    goto :goto_0
.end method


# virtual methods
.method public decodeVNote(Landroid/net/Uri;)Ljava/util/ArrayList;
    .locals 1
    .param p1, "uri"    # Landroid/net/Uri;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/net/Uri;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 57
    iget-object v0, p0, Lcom/samsung/android/app/memo/util/vnote/VNoteManager;->mVn:Lcom/samsung/android/app/memo/util/vnote/pim/VNote;

    invoke-virtual {v0, p1}, Lcom/samsung/android/app/memo/util/vnote/pim/VNote;->readFile(Landroid/net/Uri;)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public getItemCount(Landroid/content/Context;Landroid/content/Intent;)I
    .locals 4
    .param p1, "ctx"    # Landroid/content/Context;
    .param p2, "i"    # Landroid/content/Intent;

    .prologue
    .line 81
    if-eqz p2, :cond_0

    .line 82
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/app/memo/util/vnote/VNoteManager;->getUri(Landroid/content/Context;Landroid/content/Intent;)Landroid/net/Uri;

    move-result-object v1

    .line 83
    .local v1, "uri":Landroid/net/Uri;
    if-eqz v1, :cond_0

    .line 85
    :try_start_0
    iget-object v2, p0, Lcom/samsung/android/app/memo/util/vnote/VNoteManager;->mVn:Lcom/samsung/android/app/memo/util/vnote/pim/VNote;

    invoke-virtual {v2, v1}, Lcom/samsung/android/app/memo/util/vnote/pim/VNote;->getFileLineCount(Landroid/net/Uri;)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 91
    .end local v1    # "uri":Landroid/net/Uri;
    :goto_0
    return v2

    .line 86
    .restart local v1    # "uri":Landroid/net/Uri;
    :catch_0
    move-exception v0

    .line 87
    .local v0, "e":Ljava/io/IOException;
    sget-object v2, Lcom/samsung/android/app/memo/util/vnote/VNoteManager;->TAG:Ljava/lang/String;

    const-string v3, "getItemCount() "

    invoke-static {v2, v3, v0}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 91
    .end local v0    # "e":Ljava/io/IOException;
    .end local v1    # "uri":Landroid/net/Uri;
    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public pasreVNT(Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 13
    .param p1, "data"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 122
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    .line 123
    .local v10, "vntList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :goto_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v11

    if-gtz v11, :cond_1

    .line 152
    :cond_0
    return-object v10

    .line 124
    :cond_1
    const-string v11, "BEGIN:VNOTE"

    invoke-virtual {p1, v11}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v8

    .line 125
    .local v8, "startIndex":I
    const-string v11, "END:VNOTE"

    invoke-virtual {p1, v11}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v5

    .line 126
    .local v5, "endIndex":I
    const-string v11, "END:VNOTE"

    invoke-virtual {v11}, Ljava/lang/String;->length()I

    move-result v11

    add-int/2addr v11, v5

    invoke-virtual {p1, v8, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v9

    .line 127
    .local v9, "vntData":Ljava/lang/String;
    const-string v11, "BODY;CHARSET"

    invoke-virtual {v9, v11}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v11

    invoke-virtual {v9, v11}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    .line 128
    .local v1, "bodyText":Ljava/lang/String;
    const-string v11, "LAST-MODIFIED:"

    invoke-virtual {v1, v11}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v7

    .line 129
    .local v7, "indexOfLM":I
    if-lez v7, :cond_2

    .line 130
    const/4 v11, 0x0

    invoke-virtual {v1, v11, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 131
    :cond_2
    const-string v11, "DCREATED:"

    invoke-virtual {v1, v11}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v7

    .line 132
    if-lez v7, :cond_3

    .line 133
    const/4 v11, 0x0

    invoke-virtual {v1, v11, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 134
    :cond_3
    const-string v11, ";"

    invoke-virtual {v1, v11}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 135
    .local v0, "arrStr":[Ljava/lang/String;
    const-string v2, "UTF-8"

    .line 136
    .local v2, "charSet":Ljava/lang/String;
    const-string v4, ""

    .line 137
    .local v4, "encodedStr":Ljava/lang/String;
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_1
    array-length v11, v0

    if-lt v6, v11, :cond_4

    .line 144
    const-string v11, "QUOTED-PRINTABLE"

    invoke-direct {p0, v4, v2, v11}, Lcom/samsung/android/app/memo/util/vnote/VNoteManager;->decodeText(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 145
    .local v3, "decodedStr":Ljava/lang/String;
    invoke-virtual {v10, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 146
    const-string v11, "END:VNOTE"

    invoke-virtual {v11}, Ljava/lang/String;->length()I

    move-result v11

    add-int/2addr v11, v5

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v12

    if-ge v11, v12, :cond_0

    .line 147
    const-string v11, "END:VNOTE"

    invoke-virtual {v11}, Ljava/lang/String;->length()I

    move-result v11

    add-int/2addr v11, v5

    invoke-virtual {p1, v11}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    .line 138
    .end local v3    # "decodedStr":Ljava/lang/String;
    :cond_4
    aget-object v11, v0, v6

    const-string v12, "CHARSET"

    invoke-virtual {v11, v12}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_6

    .line 139
    aget-object v11, v0, v6

    const-string v12, "="

    invoke-virtual {v11, v12}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v11

    const/4 v12, 0x1

    aget-object v2, v11, v12

    .line 137
    :cond_5
    :goto_2
    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    .line 140
    :cond_6
    aget-object v11, v0, v6

    const-string v12, "ENCODING=QUOTED-PRINTABLE"

    invoke-virtual {v11, v12}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_5

    .line 141
    aget-object v11, v0, v6

    const-string v12, ":"

    invoke-virtual {v11, v12}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v11

    const/4 v12, 0x1

    aget-object v4, v11, v12

    goto :goto_2
.end method

.method public readFile(Landroid/content/Context;Landroid/content/Intent;)Ljava/lang/StringBuilder;
    .locals 9
    .param p1, "ctx"    # Landroid/content/Context;
    .param p2, "i"    # Landroid/content/Intent;

    .prologue
    const/4 v7, 0x0

    .line 95
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/app/memo/util/vnote/VNoteManager;->getUri(Landroid/content/Context;Landroid/content/Intent;)Landroid/net/Uri;

    move-result-object v3

    .line 96
    .local v3, "uri":Landroid/net/Uri;
    if-nez v3, :cond_1

    move-object v2, v7

    .line 118
    :cond_0
    :goto_0
    return-object v2

    .line 99
    :cond_1
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 102
    .local v2, "strBulider":Ljava/lang/StringBuilder;
    :try_start_0
    invoke-virtual {p0, v3}, Lcom/samsung/android/app/memo/util/vnote/VNoteManager;->decodeVNote(Landroid/net/Uri;)Ljava/util/ArrayList;

    move-result-object v4

    .line 103
    .local v4, "vNoteArray":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v5

    .line 104
    .local v5, "vNoteArraySize":I
    const/4 v1, 0x0

    .local v1, "j":I
    :goto_1
    if-lt v1, v5, :cond_2

    .line 114
    .end local v1    # "j":I
    .end local v4    # "vNoteArray":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v5    # "vNoteArraySize":I
    :goto_2
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    if-nez v6, :cond_0

    move-object v2, v7

    .line 115
    goto :goto_0

    .line 105
    .restart local v1    # "j":I
    .restart local v4    # "vNoteArray":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .restart local v5    # "vNoteArraySize":I
    :cond_2
    :try_start_1
    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 106
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v6

    add-int/lit8 v6, v6, -0x1

    if-ge v1, v6, :cond_3

    .line 107
    const/16 v6, 0xa

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 104
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 110
    .end local v1    # "j":I
    .end local v4    # "vNoteArray":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v5    # "vNoteArraySize":I
    :catch_0
    move-exception v0

    .line 111
    .local v0, "e":Ljava/io/IOException;
    sget-object v6, Lcom/samsung/android/app/memo/util/vnote/VNoteManager;->TAG:Ljava/lang/String;

    const-string v8, " readFile() "

    invoke-static {v6, v8, v0}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_2
.end method
