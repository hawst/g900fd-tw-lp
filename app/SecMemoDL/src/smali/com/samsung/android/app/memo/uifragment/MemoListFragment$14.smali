.class Lcom/samsung/android/app/memo/uifragment/MemoListFragment$14;
.super Ljava/lang/Object;
.source "MemoListFragment.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->handlePickOne()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/widget/AdapterView$OnItemClickListener;"
    }
.end annotation


# static fields
.field private static synthetic $SWITCH_TABLE$com$samsung$android$app$memo$uifragment$MemoListFragment$MIME_TYPE:[I


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;


# direct methods
.method static synthetic $SWITCH_TABLE$com$samsung$android$app$memo$uifragment$MemoListFragment$MIME_TYPE()[I
    .locals 3

    .prologue
    .line 2805
    sget-object v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$14;->$SWITCH_TABLE$com$samsung$android$app$memo$uifragment$MemoListFragment$MIME_TYPE:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MIME_TYPE;->values()[Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MIME_TYPE;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MIME_TYPE;->APPLICATION_MEMO:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MIME_TYPE;

    invoke-virtual {v1}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MIME_TYPE;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_6

    :goto_1
    :try_start_1
    sget-object v1, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MIME_TYPE;->AUDIO_ALL:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MIME_TYPE;

    invoke-virtual {v1}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MIME_TYPE;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_5

    :goto_2
    :try_start_2
    sget-object v1, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MIME_TYPE;->AUDIO_MP4:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MIME_TYPE;

    invoke-virtual {v1}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MIME_TYPE;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_4

    :goto_3
    :try_start_3
    sget-object v1, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MIME_TYPE;->IMAGE_ALL:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MIME_TYPE;

    invoke-virtual {v1}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MIME_TYPE;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_3

    :goto_4
    :try_start_4
    sget-object v1, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MIME_TYPE;->INVALID:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MIME_TYPE;

    invoke-virtual {v1}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MIME_TYPE;->ordinal()I

    move-result v1

    const/4 v2, 0x7

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_2

    :goto_5
    :try_start_5
    sget-object v1, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MIME_TYPE;->TEXT_IMAGE_AUDIO:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MIME_TYPE;

    invoke-virtual {v1}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MIME_TYPE;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_1

    :goto_6
    :try_start_6
    sget-object v1, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MIME_TYPE;->TEXT_PLAIN:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MIME_TYPE;

    invoke-virtual {v1}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MIME_TYPE;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_0

    :goto_7
    sput-object v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$14;->$SWITCH_TABLE$com$samsung$android$app$memo$uifragment$MemoListFragment$MIME_TYPE:[I

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_7

    :catch_1
    move-exception v1

    goto :goto_6

    :catch_2
    move-exception v1

    goto :goto_5

    :catch_3
    move-exception v1

    goto :goto_4

    :catch_4
    move-exception v1

    goto :goto_3

    :catch_5
    move-exception v1

    goto :goto_2

    :catch_6
    move-exception v1

    goto :goto_1
.end method

.method constructor <init>(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$14;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    .line 2805
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 22
    .param p2, "arg1"    # Landroid/view/View;
    .param p3, "arg2"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 2808
    .local p1, "arg0":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$14;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    .line 2809
    .local v4, "activity":Landroid/app/Activity;
    invoke-virtual {v4}, Landroid/app/Activity;->getCallingPackage()Ljava/lang/String;

    move-result-object v6

    .line 2810
    .local v6, "callingPkg":Ljava/lang/String;
    new-instance v11, Landroid/content/Intent;

    invoke-direct {v11}, Landroid/content/Intent;-><init>()V

    .line 2811
    .local v11, "returnIntent":Landroid/content/Intent;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$14;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    move-object/from16 v18, v0

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mMenuSearch:Landroid/view/MenuItem;
    invoke-static/range {v18 .. v18}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->access$41(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;)Landroid/view/MenuItem;

    move-result-object v18

    if-eqz v18, :cond_0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$14;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    move-object/from16 v18, v0

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mMenuSearch:Landroid/view/MenuItem;
    invoke-static/range {v18 .. v18}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->access$41(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;)Landroid/view/MenuItem;

    move-result-object v18

    invoke-interface/range {v18 .. v18}, Landroid/view/MenuItem;->isActionViewExpanded()Z

    move-result v18

    if-eqz v18, :cond_0

    .line 2812
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$14;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    move-object/from16 v18, v0

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mMenuSearch:Landroid/view/MenuItem;
    invoke-static/range {v18 .. v18}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->access$41(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;)Landroid/view/MenuItem;

    move-result-object v18

    invoke-interface/range {v18 .. v18}, Landroid/view/MenuItem;->collapseActionView()Z

    .line 2814
    :cond_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$14;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    move-object/from16 v18, v0

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mIsWidgetPick:Z
    invoke-static/range {v18 .. v18}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->access$49(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;)Z

    move-result v18

    if-eqz v18, :cond_3

    .line 2815
    new-instance v17, Landroid/content/Intent;

    const-string v18, "com.sec.android.widgetapp.MEMO_SAVE"

    invoke-direct/range {v17 .. v18}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2816
    .local v17, "widgetIntent":Landroid/content/Intent;
    const-string v18, "memo_id"

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    move-wide/from16 v2, p4

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 2817
    const-string v18, "appWidgetId"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$14;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    move-object/from16 v19, v0

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mWidgetID:J
    invoke-static/range {v19 .. v19}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->access$61(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;)J

    move-result-wide v20

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    move-wide/from16 v2, v20

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 2818
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$14;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v18

    invoke-static/range {v18 .. v18}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v18

    move-object/from16 v0, v18

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Landroid/support/v4/content/LocalBroadcastManager;->sendBroadcast(Landroid/content/Intent;)Z

    .line 2897
    .end local v17    # "widgetIntent":Landroid/content/Intent;
    :cond_1
    :goto_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$14;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    move-object/from16 v18, v0

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mimeType:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MIME_TYPE;
    invoke-static/range {v18 .. v18}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->access$62(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;)Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MIME_TYPE;

    move-result-object v18

    sget-object v19, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MIME_TYPE;->APPLICATION_MEMO:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MIME_TYPE;

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    if-eq v0, v1, :cond_2

    .line 2898
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$14;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    move-object/from16 v18, v0

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mFragmentAction:Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;
    invoke-static/range {v18 .. v18}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->access$36(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;)Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;

    move-result-object v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$14;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->getTag()Ljava/lang/String;

    move-result-object v19

    invoke-interface/range {v18 .. v19}, Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;->onFragmentSuicide(Ljava/lang/String;)V

    .line 2899
    :cond_2
    return-void

    .line 2820
    :cond_3
    const/16 v16, 0x0

    .line 2821
    .local v16, "voiceUri":Landroid/net/Uri;
    invoke-static {}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$14;->$SWITCH_TABLE$com$samsung$android$app$memo$uifragment$MemoListFragment$MIME_TYPE()[I

    move-result-object v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$14;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    move-object/from16 v19, v0

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mimeType:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MIME_TYPE;
    invoke-static/range {v19 .. v19}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->access$62(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;)Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MIME_TYPE;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MIME_TYPE;->ordinal()I

    move-result v19

    aget v18, v18, v19

    packed-switch v18, :pswitch_data_0

    .line 2893
    :cond_4
    :goto_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$14;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    move-object/from16 v18, v0

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mimeType:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MIME_TYPE;
    invoke-static/range {v18 .. v18}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->access$62(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;)Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MIME_TYPE;

    move-result-object v18

    sget-object v19, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MIME_TYPE;->APPLICATION_MEMO:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MIME_TYPE;

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    if-eq v0, v1, :cond_1

    .line 2894
    const/16 v18, -0x1

    move/from16 v0, v18

    invoke-virtual {v4, v0, v11}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    goto :goto_0

    .line 2823
    :pswitch_0
    const-string v18, "text/plain"

    move-object/from16 v0, v18

    invoke-virtual {v11, v0}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 2824
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$14;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    move-wide/from16 v1, p4

    # invokes: Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->getTitle(J)Ljava/lang/String;
    invoke-static {v0, v1, v2}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->access$22(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;J)Ljava/lang/String;

    move-result-object v14

    .line 2825
    .local v14, "titleForTexTPlain":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$14;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    move-wide/from16 v1, p4

    # invokes: Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->getText(J)Ljava/lang/String;
    invoke-static {v0, v1, v2}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->access$23(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;J)Ljava/lang/String;

    move-result-object v7

    .line 2826
    .local v7, "content":Ljava/lang/String;
    invoke-virtual {v7}, Ljava/lang/String;->isEmpty()Z

    move-result v18

    if-eqz v18, :cond_5

    .line 2827
    move-object v7, v14

    .line 2828
    :cond_5
    const-string v18, "android.intent.extra.TEXT"

    move-object/from16 v0, v18

    invoke-virtual {v11, v0, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2829
    const-string v18, "android.intent.extra.SUBJECT"

    move-object/from16 v0, v18

    invoke-virtual {v11, v0, v14}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2830
    const-string v18, "result"

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-static {v14}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v20

    invoke-direct/range {v19 .. v20}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v19

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v11, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_1

    .line 2833
    .end local v7    # "content":Ljava/lang/String;
    .end local v14    # "titleForTexTPlain":Ljava/lang/String;
    :pswitch_1
    const-string v18, "text/plain"

    move-object/from16 v0, v18

    invoke-virtual {v11, v0}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 2834
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$14;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    move-wide/from16 v1, p4

    # invokes: Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->getTitle(J)Ljava/lang/String;
    invoke-static {v0, v1, v2}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->access$22(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;J)Ljava/lang/String;

    move-result-object v13

    .line 2835
    .local v13, "titleForTexTImage":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$14;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    move-wide/from16 v1, p4

    # invokes: Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->getText(J)Ljava/lang/String;
    invoke-static {v0, v1, v2}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->access$23(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;J)Ljava/lang/String;

    move-result-object v8

    .line 2836
    .local v8, "contents":Ljava/lang/String;
    const-string v18, "android.intent.extra.TEXT"

    move-object/from16 v0, v18

    invoke-virtual {v11, v0, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2837
    const-string v18, "android.intent.extra.SUBJECT"

    move-object/from16 v0, v18

    invoke-virtual {v11, v0, v13}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2838
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$14;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    move-wide/from16 v1, p4

    # invokes: Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->getImageUriList(J)Ljava/util/ArrayList;
    invoke-static {v0, v1, v2}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->access$25(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;J)Ljava/util/ArrayList;

    move-result-object v5

    .line 2839
    .local v5, "attachmentList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v18

    if-lez v18, :cond_6

    .line 2840
    const-string v18, "image/*"

    move-object/from16 v0, v18

    invoke-virtual {v11, v0}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 2842
    :cond_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$14;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    move-wide/from16 v1, p4

    # invokes: Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->getVoiceUri(J)Landroid/net/Uri;
    invoke-static {v0, v1, v2}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->access$26(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;J)Landroid/net/Uri;

    move-result-object v16

    .line 2843
    sget-object v18, Lcom/samsung/android/provider/memo/MemoContract;->BASE_URI_FILE:Landroid/net/Uri;

    invoke-virtual/range {v16 .. v16}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v12

    .line 2844
    .local v12, "returnUri":Landroid/net/Uri;
    if-eqz v16, :cond_7

    sget-object v18, Landroid/net/Uri;->EMPTY:Landroid/net/Uri;

    move-object/from16 v0, v16

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-nez v18, :cond_7

    .line 2845
    invoke-virtual {v5, v12}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2846
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v18

    const/16 v19, 0x1

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_7

    .line 2847
    const-string v18, "audio/mp4"

    move-object/from16 v0, v18

    invoke-virtual {v11, v0}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 2850
    :cond_7
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v18

    if-lez v18, :cond_4

    .line 2851
    const/16 v18, 0x0

    move/from16 v0, v18

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Landroid/net/Uri;

    move-object/from16 v0, v18

    invoke-virtual {v11, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 2852
    const-string v18, "image/*"

    move-object/from16 v0, v18

    invoke-virtual {v11, v0}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 2853
    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v18

    :cond_8
    :goto_2
    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->hasNext()Z

    move-result v19

    if-nez v19, :cond_9

    .line 2859
    const-string v18, "android.intent.extra.STREAM"

    move-object/from16 v0, v18

    invoke-virtual {v11, v0, v5}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    goto/16 :goto_1

    .line 2853
    :cond_9
    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Landroid/net/Uri;

    .line 2854
    .local v15, "uri":Landroid/net/Uri;
    if-eqz v6, :cond_8

    .line 2856
    const/16 v19, 0x1

    .line 2855
    move/from16 v0, v19

    invoke-virtual {v4, v6, v15, v0}, Landroid/app/Activity;->grantUriPermission(Ljava/lang/String;Landroid/net/Uri;I)V

    goto :goto_2

    .line 2864
    .end local v5    # "attachmentList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    .end local v8    # "contents":Ljava/lang/String;
    .end local v12    # "returnUri":Landroid/net/Uri;
    .end local v13    # "titleForTexTImage":Ljava/lang/String;
    .end local v15    # "uri":Landroid/net/Uri;
    :pswitch_2
    const-string v18, "image/*"

    move-object/from16 v0, v18

    invoke-virtual {v11, v0}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 2865
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$14;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    move-wide/from16 v1, p4

    # invokes: Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->getImageUriList(J)Ljava/util/ArrayList;
    invoke-static {v0, v1, v2}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->access$25(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;J)Ljava/util/ArrayList;

    move-result-object v9

    .line 2866
    .local v9, "imageUriList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    const/16 v18, 0x0

    move/from16 v0, v18

    invoke-virtual {v9, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Landroid/net/Uri;

    move-object/from16 v0, v18

    invoke-virtual {v11, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 2867
    invoke-virtual {v9}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v18

    :cond_a
    :goto_3
    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->hasNext()Z

    move-result v19

    if-nez v19, :cond_b

    .line 2873
    const-string v18, "android.intent.extra.STREAM"

    move-object/from16 v0, v18

    invoke-virtual {v11, v0, v9}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    goto/16 :goto_1

    .line 2867
    :cond_b
    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Landroid/net/Uri;

    .line 2868
    .restart local v15    # "uri":Landroid/net/Uri;
    if-eqz v6, :cond_a

    .line 2870
    const/16 v19, 0x1

    .line 2869
    move/from16 v0, v19

    invoke-virtual {v4, v6, v15, v0}, Landroid/app/Activity;->grantUriPermission(Ljava/lang/String;Landroid/net/Uri;I)V

    goto :goto_3

    .line 2878
    .end local v9    # "imageUriList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    .end local v15    # "uri":Landroid/net/Uri;
    :pswitch_3
    const-string v18, "audio/mp4"

    move-object/from16 v0, v18

    invoke-virtual {v11, v0}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 2879
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$14;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    move-wide/from16 v1, p4

    # invokes: Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->getVoiceUri(J)Landroid/net/Uri;
    invoke-static {v0, v1, v2}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->access$26(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;J)Landroid/net/Uri;

    move-result-object v16

    .line 2881
    const/16 v18, 0x1

    .line 2880
    move-object/from16 v0, v16

    move/from16 v1, v18

    invoke-virtual {v4, v6, v0, v1}, Landroid/app/Activity;->grantUriPermission(Ljava/lang/String;Landroid/net/Uri;I)V

    .line 2882
    move-object/from16 v0, v16

    invoke-virtual {v11, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    goto/16 :goto_1

    .line 2885
    :pswitch_4
    const-string v18, "application/vnd.samsung.memo"

    move-object/from16 v0, v18

    invoke-virtual {v11, v0}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 2886
    new-instance v10, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ShareQuickConnectTask;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$14;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    move-wide/from16 v1, p4

    invoke-direct {v10, v0, v11, v1, v2}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ShareQuickConnectTask;-><init>(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;Landroid/content/Intent;J)V

    .line 2887
    .local v10, "quickTask":Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ShareQuickConnectTask;
    sget-object v18, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/16 v19, 0x0

    move/from16 v0, v19

    new-array v0, v0, [Ljava/lang/Void;

    move-object/from16 v19, v0

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v10, v0, v1}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ShareQuickConnectTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto/16 :goto_1

    .line 2821
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_3
        :pswitch_1
        :pswitch_4
    .end packed-switch
.end method
