.class public Lcom/samsung/android/app/migration/dialog/SamsungAccountDialog;
.super Landroid/app/DialogFragment;
.source "SamsungAccountDialog.java"


# instance fields
.field private mDialog:Landroid/app/AlertDialog;

.field private mDoNotShowAgain:Landroid/widget/CheckBox;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    return-void
.end method

.method static synthetic access$0(Lcom/samsung/android/app/migration/dialog/SamsungAccountDialog;)Landroid/widget/CheckBox;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/samsung/android/app/migration/dialog/SamsungAccountDialog;->mDoNotShowAgain:Landroid/widget/CheckBox;

    return-object v0
.end method

.method private isContextAlive(Landroid/app/Activity;)Z
    .locals 1
    .param p1, "context"    # Landroid/app/Activity;

    .prologue
    .line 49
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/app/Activity;->isDestroyed()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Landroid/app/Activity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 50
    const/4 v0, 0x1

    .line 52
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static newInstance()Lcom/samsung/android/app/migration/dialog/SamsungAccountDialog;
    .locals 1

    .prologue
    .line 45
    new-instance v0, Lcom/samsung/android/app/migration/dialog/SamsungAccountDialog;

    invoke-direct {v0}, Lcom/samsung/android/app/migration/dialog/SamsungAccountDialog;-><init>()V

    return-object v0
.end method


# virtual methods
.method public hide()V
    .locals 0

    .prologue
    .line 62
    invoke-virtual {p0}, Lcom/samsung/android/app/migration/dialog/SamsungAccountDialog;->dismiss()V

    .line 63
    return-void
.end method

.method public onCancel(Landroid/content/DialogInterface;)V
    .locals 2
    .param p1, "dialog"    # Landroid/content/DialogInterface;

    .prologue
    .line 67
    invoke-virtual {p0}, Lcom/samsung/android/app/migration/dialog/SamsungAccountDialog;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/samsung/android/app/migration/utils/SharedPref;->setSADoNotShowFlag(Landroid/content/Context;Z)V

    .line 68
    return-void
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 72
    invoke-virtual {p0}, Lcom/samsung/android/app/migration/dialog/SamsungAccountDialog;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f040027

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 73
    .local v0, "v":Landroid/view/View;
    const v1, 0x7f0e0021

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/CheckBox;

    iput-object v1, p0, Lcom/samsung/android/app/migration/dialog/SamsungAccountDialog;->mDoNotShowAgain:Landroid/widget/CheckBox;

    .line 74
    iget-object v1, p0, Lcom/samsung/android/app/migration/dialog/SamsungAccountDialog;->mDoNotShowAgain:Landroid/widget/CheckBox;

    new-instance v2, Lcom/samsung/android/app/migration/dialog/SamsungAccountDialog$1;

    invoke-direct {v2, p0}, Lcom/samsung/android/app/migration/dialog/SamsungAccountDialog$1;-><init>(Lcom/samsung/android/app/migration/dialog/SamsungAccountDialog;)V

    invoke-virtual {v1, v2}, Landroid/widget/CheckBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 80
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/samsung/android/app/migration/dialog/SamsungAccountDialog;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    .line 81
    const v2, 0x7f0b0005

    new-instance v3, Lcom/samsung/android/app/migration/dialog/SamsungAccountDialog$2;

    invoke-direct {v3, p0}, Lcom/samsung/android/app/migration/dialog/SamsungAccountDialog$2;-><init>(Lcom/samsung/android/app/migration/dialog/SamsungAccountDialog;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    .line 86
    const v2, 0x7f0b0047

    new-instance v3, Lcom/samsung/android/app/migration/dialog/SamsungAccountDialog$3;

    invoke-direct {v3, p0}, Lcom/samsung/android/app/migration/dialog/SamsungAccountDialog$3;-><init>(Lcom/samsung/android/app/migration/dialog/SamsungAccountDialog;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    .line 107
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    .line 80
    iput-object v1, p0, Lcom/samsung/android/app/migration/dialog/SamsungAccountDialog;->mDialog:Landroid/app/AlertDialog;

    .line 108
    iget-object v1, p0, Lcom/samsung/android/app/migration/dialog/SamsungAccountDialog;->mDialog:Landroid/app/AlertDialog;

    const v2, 0x7f0b0031

    invoke-virtual {p0, v2}, Lcom/samsung/android/app/migration/dialog/SamsungAccountDialog;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog;->setTitle(Ljava/lang/CharSequence;)V

    .line 109
    iget-object v1, p0, Lcom/samsung/android/app/migration/dialog/SamsungAccountDialog;->mDialog:Landroid/app/AlertDialog;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog;->setCanceledOnTouchOutside(Z)V

    .line 110
    iget-object v1, p0, Lcom/samsung/android/app/migration/dialog/SamsungAccountDialog;->mDialog:Landroid/app/AlertDialog;

    return-object v1
.end method

.method public show(Landroid/app/Activity;)V
    .locals 2
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 56
    invoke-direct {p0, p1}, Lcom/samsung/android/app/migration/dialog/SamsungAccountDialog;->isContextAlive(Landroid/app/Activity;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 57
    invoke-virtual {p1}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    const-string v1, "samsungAccountDialog"

    invoke-virtual {p0, v0, v1}, Lcom/samsung/android/app/migration/dialog/SamsungAccountDialog;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 59
    :cond_0
    return-void
.end method
