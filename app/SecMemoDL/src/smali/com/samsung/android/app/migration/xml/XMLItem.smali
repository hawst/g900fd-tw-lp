.class public Lcom/samsung/android/app/migration/xml/XMLItem;
.super Ljava/lang/Object;
.source "XMLItem.java"


# instance fields
.field private content:Ljava/lang/String;

.field private createdTime:J

.field private modifiedTime:J

.field private templateType:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    iput-object v0, p0, Lcom/samsung/android/app/migration/xml/XMLItem;->templateType:Ljava/lang/String;

    .line 30
    iput-object v0, p0, Lcom/samsung/android/app/migration/xml/XMLItem;->content:Ljava/lang/String;

    .line 22
    return-void
.end method


# virtual methods
.method public getContent()Ljava/lang/String;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/samsung/android/app/migration/xml/XMLItem;->content:Ljava/lang/String;

    return-object v0
.end method

.method public getCreatedTime()J
    .locals 2

    .prologue
    .line 49
    iget-wide v0, p0, Lcom/samsung/android/app/migration/xml/XMLItem;->createdTime:J

    return-wide v0
.end method

.method public getModifiedTime()J
    .locals 2

    .prologue
    .line 57
    iget-wide v0, p0, Lcom/samsung/android/app/migration/xml/XMLItem;->modifiedTime:J

    return-wide v0
.end method

.method public getTemplateType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/samsung/android/app/migration/xml/XMLItem;->templateType:Ljava/lang/String;

    return-object v0
.end method

.method public setContent(Ljava/lang/String;)V
    .locals 0
    .param p1, "content"    # Ljava/lang/String;

    .prologue
    .line 37
    iput-object p1, p0, Lcom/samsung/android/app/migration/xml/XMLItem;->content:Ljava/lang/String;

    .line 38
    return-void
.end method

.method public setCreatedTime(J)V
    .locals 1
    .param p1, "createdTime"    # J

    .prologue
    .line 53
    iput-wide p1, p0, Lcom/samsung/android/app/migration/xml/XMLItem;->createdTime:J

    .line 54
    return-void
.end method

.method public setModifiedTime(J)V
    .locals 1
    .param p1, "modifiedTime"    # J

    .prologue
    .line 61
    iput-wide p1, p0, Lcom/samsung/android/app/migration/xml/XMLItem;->modifiedTime:J

    .line 62
    return-void
.end method

.method public setTemplateType(Ljava/lang/String;)V
    .locals 0
    .param p1, "templateType"    # Ljava/lang/String;

    .prologue
    .line 45
    iput-object p1, p0, Lcom/samsung/android/app/migration/xml/XMLItem;->templateType:Ljava/lang/String;

    .line 46
    return-void
.end method
