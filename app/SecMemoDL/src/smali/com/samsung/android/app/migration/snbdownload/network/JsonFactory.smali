.class public Lcom/samsung/android/app/migration/snbdownload/network/JsonFactory;
.super Ljava/lang/Object;
.source "JsonFactory.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "JsonFactory"

.field private static urlMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 40
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/samsung/android/app/migration/snbdownload/network/JsonFactory;->urlMap:Ljava/util/HashMap;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    return-void
.end method

.method public static getBaseURLfromResp(Ljava/lang/String;)V
    .locals 10
    .param p0, "response"    # Ljava/lang/String;

    .prologue
    .line 76
    sget-object v6, Lcom/samsung/android/app/migration/snbdownload/network/JsonFactory;->urlMap:Ljava/util/HashMap;

    invoke-virtual {v6}, Ljava/util/HashMap;->clear()V

    .line 78
    new-instance v5, Lorg/json/JSONTokener;

    invoke-direct {v5, p0}, Lorg/json/JSONTokener;-><init>(Ljava/lang/String;)V

    .line 80
    .local v5, "tokener":Lorg/json/JSONTokener;
    :try_start_0
    new-instance v3, Lorg/json/JSONObject;

    invoke-direct {v3, v5}, Lorg/json/JSONObject;-><init>(Lorg/json/JSONTokener;)V

    .line 82
    .local v3, "regions":Lorg/json/JSONObject;
    const-string v6, "gld_user_region"

    invoke-virtual {v3, v6}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    .line 86
    .local v0, "defaultRegion":Lorg/json/JSONObject;
    if-eqz v0, :cond_1

    .line 87
    const-string v6, "primary"

    invoke-virtual {v0, v6}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v2

    .line 88
    .local v2, "primaryRegion":Lorg/json/JSONArray;
    const-string v6, "secondary"

    invoke-virtual {v0, v6}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v4

    .line 89
    .local v4, "secondaryRegion":Lorg/json/JSONArray;
    invoke-virtual {v2}, Lorg/json/JSONArray;->length()I

    move-result v6

    if-lez v6, :cond_0

    .line 91
    sget-object v6, Lcom/samsung/android/app/migration/snbdownload/network/JsonFactory;->urlMap:Ljava/util/HashMap;

    const-string v7, "gld_user_region_1"

    const/4 v8, 0x0

    invoke-virtual {v2, v8}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v8

    const-string v9, "address"

    invoke-virtual {v8, v9}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 93
    :cond_0
    invoke-virtual {v4}, Lorg/json/JSONArray;->length()I

    move-result v6

    if-lez v6, :cond_1

    .line 95
    sget-object v6, Lcom/samsung/android/app/migration/snbdownload/network/JsonFactory;->urlMap:Ljava/util/HashMap;

    const-string v7, "gld_user_region_2"

    const/4 v8, 0x0

    invoke-virtual {v4, v8}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v8

    const-string v9, "address"

    invoke-virtual {v8, v9}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 99
    .end local v2    # "primaryRegion":Lorg/json/JSONArray;
    .end local v4    # "secondaryRegion":Lorg/json/JSONArray;
    :cond_1
    const-string v6, "gld_mcc_region"

    invoke-virtual {v3, v6}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    .line 100
    if-eqz v0, :cond_3

    .line 101
    const-string v6, "primary"

    invoke-virtual {v0, v6}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v2

    .line 102
    .restart local v2    # "primaryRegion":Lorg/json/JSONArray;
    const-string v6, "secondary"

    invoke-virtual {v0, v6}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v4

    .line 103
    .restart local v4    # "secondaryRegion":Lorg/json/JSONArray;
    invoke-virtual {v2}, Lorg/json/JSONArray;->length()I

    move-result v6

    if-lez v6, :cond_2

    .line 106
    sget-object v6, Lcom/samsung/android/app/migration/snbdownload/network/JsonFactory;->urlMap:Ljava/util/HashMap;

    const-string v7, "gld_mcc_region_1"

    const/4 v8, 0x0

    invoke-virtual {v2, v8}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v8

    const-string v9, "address"

    invoke-virtual {v8, v9}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 108
    :cond_2
    invoke-virtual {v4}, Lorg/json/JSONArray;->length()I

    move-result v6

    if-lez v6, :cond_3

    .line 110
    sget-object v6, Lcom/samsung/android/app/migration/snbdownload/network/JsonFactory;->urlMap:Ljava/util/HashMap;

    const-string v7, "gld_mcc_region_2"

    const/4 v8, 0x0

    invoke-virtual {v4, v8}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v8

    const-string v9, "address"

    invoke-virtual {v8, v9}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 116
    .end local v0    # "defaultRegion":Lorg/json/JSONObject;
    .end local v2    # "primaryRegion":Lorg/json/JSONArray;
    .end local v3    # "regions":Lorg/json/JSONObject;
    .end local v4    # "secondaryRegion":Lorg/json/JSONArray;
    :cond_3
    :goto_0
    return-void

    .line 113
    :catch_0
    move-exception v1

    .line 114
    .local v1, "e":Lorg/json/JSONException;
    const-string v6, "JsonFactory"

    const-string v7, "Exception in getBaseURLfromResp. "

    invoke-static {v6, v7, v1}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public static smartServerFilter()Ljava/util/ArrayList;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v9, 0x1

    .line 119
    const/4 v2, 0x0

    .line 120
    .local v2, "imsiMap_p":Ljava/lang/String;
    const/4 v3, 0x0

    .line 121
    .local v3, "imsiMap_s":Ljava/lang/String;
    const/4 v0, 0x0

    .line 122
    .local v0, "defaultMap_p":Ljava/lang/String;
    const/4 v4, 0x0

    .line 123
    .local v4, "ipMap_p":Ljava/lang/String;
    const/4 v1, 0x0

    .line 124
    .local v1, "defaultMap_s":Ljava/lang/String;
    const/4 v5, 0x0

    .line 125
    .local v5, "ipMap_s":Ljava/lang/String;
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 126
    .local v6, "resultList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    sget-object v7, Lcom/samsung/android/app/migration/snbdownload/network/JsonFactory;->urlMap:Ljava/util/HashMap;

    invoke-virtual {v7}, Ljava/util/HashMap;->size()I

    move-result v7

    if-lez v7, :cond_2

    .line 127
    sget-object v7, Lcom/samsung/android/app/migration/snbdownload/network/JsonFactory;->urlMap:Ljava/util/HashMap;

    const-string v8, "gld_mcc_region_1"

    invoke-virtual {v7, v8}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    .end local v2    # "imsiMap_p":Ljava/lang/String;
    check-cast v2, Ljava/lang/String;

    .line 128
    .restart local v2    # "imsiMap_p":Ljava/lang/String;
    sget-object v7, Lcom/samsung/android/app/migration/snbdownload/network/JsonFactory;->urlMap:Ljava/util/HashMap;

    const-string v8, "gld_mcc_region_2"

    invoke-virtual {v7, v8}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    .end local v3    # "imsiMap_s":Ljava/lang/String;
    check-cast v3, Ljava/lang/String;

    .line 129
    .restart local v3    # "imsiMap_s":Ljava/lang/String;
    sget-object v7, Lcom/samsung/android/app/migration/snbdownload/network/JsonFactory;->urlMap:Ljava/util/HashMap;

    const-string v8, "gld_user_region_1"

    invoke-virtual {v7, v8}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "defaultMap_p":Ljava/lang/String;
    check-cast v0, Ljava/lang/String;

    .line 130
    .restart local v0    # "defaultMap_p":Ljava/lang/String;
    sget-object v7, Lcom/samsung/android/app/migration/snbdownload/network/JsonFactory;->urlMap:Ljava/util/HashMap;

    const-string v8, "gld_user_region_2"

    invoke-virtual {v7, v8}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "defaultMap_s":Ljava/lang/String;
    check-cast v1, Ljava/lang/String;

    .line 131
    .restart local v1    # "defaultMap_s":Ljava/lang/String;
    sget-object v7, Lcom/samsung/android/app/migration/snbdownload/network/JsonFactory;->urlMap:Ljava/util/HashMap;

    const-string v8, "gld_ip_region_1"

    invoke-virtual {v7, v8}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    .end local v4    # "ipMap_p":Ljava/lang/String;
    check-cast v4, Ljava/lang/String;

    .line 132
    .restart local v4    # "ipMap_p":Ljava/lang/String;
    sget-object v7, Lcom/samsung/android/app/migration/snbdownload/network/JsonFactory;->urlMap:Ljava/util/HashMap;

    const-string v8, "gld_ip_region_2"

    invoke-virtual {v7, v8}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    .end local v5    # "ipMap_s":Ljava/lang/String;
    check-cast v5, Ljava/lang/String;

    .line 138
    .restart local v5    # "ipMap_s":Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/app/migration/snbdownload/network/util/RelayUtil;->getNetworkServiceInstance()Lcom/samsung/android/app/migration/snbdownload/network/NetworkServiceApi;

    move-result-object v7

    iget-object v7, v7, Lcom/samsung/android/app/migration/snbdownload/network/NetworkServiceApi;->mRegisteredNetwork:Ljava/lang/String;

    if-eqz v7, :cond_6

    .line 139
    invoke-static {}, Lcom/samsung/android/app/migration/snbdownload/network/util/RelayUtil;->getNetworkServiceInstance()Lcom/samsung/android/app/migration/snbdownload/network/NetworkServiceApi;

    move-result-object v7

    iget-object v7, v7, Lcom/samsung/android/app/migration/snbdownload/network/NetworkServiceApi;->mRegisteredNetwork:Ljava/lang/String;

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    if-ge v7, v9, :cond_6

    .line 140
    invoke-static {}, Lcom/samsung/android/app/migration/snbdownload/network/util/RelayUtil;->getNetworkServiceInstance()Lcom/samsung/android/app/migration/snbdownload/network/NetworkServiceApi;

    move-result-object v7

    iget v7, v7, Lcom/samsung/android/app/migration/snbdownload/network/NetworkServiceApi;->mNetworktype:I

    if-ne v7, v9, :cond_5

    .line 141
    invoke-static {}, Lcom/samsung/android/app/migration/snbdownload/network/util/RelayUtil;->getNetworkServiceInstance()Lcom/samsung/android/app/migration/snbdownload/network/NetworkServiceApi;

    move-result-object v7

    iget-object v7, v7, Lcom/samsung/android/app/migration/snbdownload/network/NetworkServiceApi;->mDeviceIMSI:Ljava/lang/String;

    if-eqz v7, :cond_0

    .line 142
    invoke-static {}, Lcom/samsung/android/app/migration/snbdownload/network/util/RelayUtil;->getNetworkServiceInstance()Lcom/samsung/android/app/migration/snbdownload/network/NetworkServiceApi;

    move-result-object v7

    iget-object v7, v7, Lcom/samsung/android/app/migration/snbdownload/network/NetworkServiceApi;->mDeviceIMSI:Ljava/lang/String;

    invoke-virtual {v7}, Ljava/lang/String;->isEmpty()Z

    move-result v7

    if-eqz v7, :cond_4

    .line 143
    :cond_0
    if-eqz v4, :cond_1

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 144
    :cond_1
    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 145
    invoke-virtual {v6, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 165
    :goto_0
    return-object v6

    .line 134
    :cond_2
    const-string v7, "https://eu.sc-proxy.samsungosp.com"

    invoke-virtual {v6, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 135
    const-string v7, "https://eu1.sc-proxy.samsungosp.com"

    invoke-virtual {v6, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 148
    :cond_3
    invoke-virtual {v6, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 149
    invoke-virtual {v6, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 153
    :cond_4
    invoke-virtual {v6, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 154
    invoke-virtual {v6, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 158
    :cond_5
    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 159
    invoke-virtual {v6, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 163
    :cond_6
    invoke-virtual {v6, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 164
    invoke-virtual {v6, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method


# virtual methods
.method public fromJSONString(Ljava/lang/String;ILcom/samsung/android/app/migration/snbdownload/network/downsync/util/SharedInfo;)V
    .locals 5
    .param p1, "response"    # Ljava/lang/String;
    .param p2, "req_type"    # I
    .param p3, "authinfo"    # Lcom/samsung/android/app/migration/snbdownload/network/downsync/util/SharedInfo;

    .prologue
    .line 48
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 50
    .local v0, "base_url":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :try_start_0
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 51
    .local v2, "myJSON":Lorg/json/JSONObject;
    packed-switch p2, :pswitch_data_0

    .line 73
    .end local v2    # "myJSON":Lorg/json/JSONObject;
    :cond_0
    :goto_0
    return-void

    .line 54
    .restart local v2    # "myJSON":Lorg/json/JSONObject;
    :pswitch_0
    const-string v3, "uid"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p3, Lcom/samsung/android/app/migration/snbdownload/network/downsync/util/SharedInfo;->userId:Ljava/lang/String;

    .line 56
    invoke-static {p1}, Lcom/samsung/android/app/migration/snbdownload/network/JsonFactory;->getBaseURLfromResp(Ljava/lang/String;)V

    .line 57
    invoke-static {}, Lcom/samsung/android/app/migration/snbdownload/network/JsonFactory;->smartServerFilter()Ljava/util/ArrayList;

    move-result-object v0

    .line 58
    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_0

    .line 59
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    const/4 v4, 0x2

    if-ge v3, v4, :cond_1

    .line 60
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v3, "https://"

    invoke-direct {v4, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p3, Lcom/samsung/android/app/migration/snbdownload/network/downsync/util/SharedInfo;->baseUrl:Ljava/lang/String;

    .line 61
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v3, "https://"

    invoke-direct {v4, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p3, Lcom/samsung/android/app/migration/snbdownload/network/downsync/util/SharedInfo;->baseUrl2:Ljava/lang/String;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 70
    .end local v2    # "myJSON":Lorg/json/JSONObject;
    :catch_0
    move-exception v1

    .line 71
    .local v1, "e":Lorg/json/JSONException;
    const-string v3, "JsonFactory"

    const-string v4, "Exception in fromJSONString. "

    invoke-static {v3, v4, v1}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 63
    .end local v1    # "e":Lorg/json/JSONException;
    .restart local v2    # "myJSON":Lorg/json/JSONObject;
    :cond_1
    :try_start_1
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v3, "https://"

    invoke-direct {v4, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p3, Lcom/samsung/android/app/migration/snbdownload/network/downsync/util/SharedInfo;->baseUrl:Ljava/lang/String;

    .line 64
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v3, "https://"

    invoke-direct {v4, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p3, Lcom/samsung/android/app/migration/snbdownload/network/downsync/util/SharedInfo;->baseUrl2:Ljava/lang/String;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0

    .line 51
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method
