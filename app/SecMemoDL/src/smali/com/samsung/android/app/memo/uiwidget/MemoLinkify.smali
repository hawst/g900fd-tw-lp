.class public Lcom/samsung/android/app/memo/uiwidget/MemoLinkify;
.super Ljava/lang/Object;
.source "MemoLinkify.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$InternalURLSpan;,
        Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$ResourceString;
    }
.end annotation


# static fields
.field public static final DATE_TIME:I = 0x10

.field private static final ENGLISH_DATE_KEYWORD_FORMAT:Ljava/lang/String; = "([tT](onight|oday|omorrow))"

.field public static final ENGLISH_DATE_KEYWORD_PATTERN:Ljava/util/regex/Pattern;

.field private static final ENGLISH_DATE_TIME_1_SCHEME:Ljava/lang/String; = "english_date_time_1://"

.field private static final ENGLISH_DATE_TIME_2_SCHEME:Ljava/lang/String; = "english_date_time_2://"

.field private static final ENGLISH_DATE_TIME_3_SCHEME:Ljava/lang/String; = "english_date_time_3://"

.field private static final ENGLISH_DATE_TIME_4_SCHEME:Ljava/lang/String; = "english_date_time_4://"

.field private static final ENGLISH_DATE_TIME_5_SCHEME:Ljava/lang/String; = "english_date_time_5://"

.field public static final ENGLISH_DATE_TIME_PATTERN:Ljava/util/regex/Pattern;

.field private static final ENGLISH_DAY_KEYWORD_FORMAT:Ljava/lang/String; = "(([sS](unday|aterday))|([mM]onday)|([tT](uesday|hursday))|([wW]ednesday)|([fF]riday))"

.field private static final ENGLISH_DAY_MONTH_FORMAT:Ljava/lang/String; = "((0[1-9]|([1](st)?|[2](nd)?|[3](rd)?|[4-90](th)?)|[1][0-9](th)?|[2]([1](st)?|[2](nd)?|[3](rd)?|[4-90](th)?)|3(0(th)?|1(st)?)) ((J(anuary|uly)|Ma(rch|y)|August|(Octo|Decem)ber)|((J(an|ul)|Ma(r|y)|Aug|Oct|Dec)[\\.\\,]?))|(0[1-9]|([1](st)?|[2](nd)?|[3](rd)?|[4-90](th)?)|[1][0-9](th)?|[2]([1](st)?|[2](nd)?|[3](rd)?|[4-90](th)?)|30(th)?) ((April|June|(Sept|Nov)ember)|((Apr|Jun|Sep|Nov)[\\.\\,]?))|(0[1-9]|([1](st)?|[2](nd)?|[3](rd)?|[4-90](th)?)|[1][0-9](th)?|2([1](st)?|[2](nd)?|[3](rd)?|[4-80](th)?)) (February|Feb[\\.\\,]?)|29(th)? (February|Feb[\\.\\,]?) ((0[48]|[2468][048]|[13579][26])00|[0-9]{2}(0[48]|[2468][048]|[13579][26])))"

.field private static final ENGLISH_MONTH_DAY_FORMAT:Ljava/lang/String; = "(((J(anuary|uly)|Ma(rch|y)|August|(Octo|Decem)ber)|((J(an|ul)|Ma(r|y)|Aug|Oct|Dec)[\\.\\,]?)) (0[1-9]|([1](st)?|[2](nd)?|[3](rd)?|[4-90](th)?)|[1][0-9](th)?|[2]([1](st)?|[2](nd)?|[3](rd)?|[4-90](th)?)|3(0(th)?|1(st)?))|((April|June|(Sept|Nov)ember)|((Apr|Jun|Sep|Nov)[\\.\\,]?)) (0[1-9]|([1](st)?|[2](nd)?|[3](rd)?|[4-90](th)?)|[1][0-9](th)?|[2]([1](st)?|[2](nd)?|[3](rd)?|[4-90](th)?)|30(th)?)|(February|Feb[\\.\\,]?) (0[1-9]|([1](st)?|[2](nd)?|[3](rd)?|[4-90](th)?)|[1][0-9](th)?|2([1](st)?|[2](nd)?|[3](rd)?|[4-80](th)?))|(February|Feb[\\.\\,]?)29(th)? ((0[48]|[2468][048]|[13579][26])00|[0-9]{2}(0[48]|[2468][048]|[13579][26])))"

.field public static final ENGLISH_ONLY_DATE_PATTERN:Ljava/util/regex/Pattern;

.field public static final ENGLISH_TIME_DATE_PATTERN:Ljava/util/regex/Pattern;

.field private static final ENGLISH_TIME_KEYWORD_FORMAT:Ljava/lang/String; = "((([mM]orning) ([0][6-9]|[1][0-1]|[6-9])(\\:)([0-5][0-9]|[1-9]))|(([aA]fternoon) ([0][1-5]|[1][2]|[1-5])(\\:)([0-5][0-9]|[1-9]))|(([eE]vening) ([0][6-9]|[1][0]|[6-9])(\\:)([0-5][0-9]|[1-9]))|(([nN]ight) ([0][8-9]|[1][0-2]|[8-9])(\\:)([0-5][0-9]|[1-9])))"

.field public static final ENGLISH_TIME_KEYWORD_PATTERN:Ljava/util/regex/Pattern;

.field private static final KOREAN_DATE_FORMAT:Ljava/lang/String; = "(18|19|20|21)\\d{2}\ub144 ([1][0-2]|[1-9]|[0][1-9])\uc6d4 ([1-2][0-9]|[3][0-1]|[1-9]|[0][1-9])\uc77c"

.field private static final KOREAN_DATE_KEYWORD_FORMAT:Ljava/lang/String; = "((\u00bf\u00c0\u00b4\u00c3|\u00b3\u00bb\u00c0\u00cf)[ \\,][ ]?(((([0-1][0-9]|[1-9]|([2][0-4]))\uc2dc)[ ]?(([0-5][0-9]|[1-9])\ubd84)?)|(((([1][3-9])|([2][0-3]))[\\:]([0-5][0-9]|[1-9]))|(([0-1][0-9]|[1-9])[ ]?[aApP][mM])|(([0-1][0-9]|[1-9])[\\:]([0-5][0-9]|[1-9])([ ]?[aApP][mM])?))))"

.field public static final KOREAN_DATE_KEYWORD_PATTERN:Ljava/util/regex/Pattern;

.field private static final KOREAN_DATE_TIME_1_SCHEME:Ljava/lang/String; = "korean_date_time_1://"

.field private static final KOREAN_DATE_TIME_2_SCHEME:Ljava/lang/String; = "korean_date_time_2://"

.field private static final KOREAN_DATE_TIME_3_SCHEME:Ljava/lang/String; = "korean_date_time_3://"

.field private static final KOREAN_DATE_TIME_4_SCHEME:Ljava/lang/String; = "korean_date_time_4://"

.field private static final KOREAN_DATE_TIME_5_SCHEME:Ljava/lang/String; = "korean_date_time_5://"

.field private static final KOREAN_DATE_TIME_6_SCHEME:Ljava/lang/String; = "korean_date_time_6://"

.field public static final KOREAN_DATE_TIME_PATTERN:Ljava/util/regex/Pattern;

.field private static final KOREAN_DAY_KEYWORD_FORMAT:Ljava/lang/String; = "(([\u00c0\u00cf\u00bf\u00f9\u00c8\u00ad\u00bc\u00f6\u00b8\u00f1\u00b1\u00dd\u00c5\u00e4]\u00bf\u00e4\u00c0\u00cf))"

.field public static final KOREAN_ONLY_DATE_PATTERN:Ljava/util/regex/Pattern;

.field public static final KOREAN_ONLY_TIME_PATTERN:Ljava/util/regex/Pattern;

.field public static final KOREAN_TIME_DATE_PATTERN:Ljava/util/regex/Pattern;

.field private static final KOREAN_TIME_FORMAT:Ljava/lang/String; = "((([0-1][0-9]|[1-9]|([2][0-4]))\uc2dc)[ ]?(([0-5][0-9]|[1-9])\ubd84)?)"

.field private static final KOREAN_TIME_KEYWORD_FORMAT:Ljava/lang/String; = "(((\u00bb\u00f5\u00ba\u00ae) (([0][1-5]|[1-5])\u00bd\u00c3([ ]([0-5][0-9]|[1-9])\u00ba\u00d0)?|([0][1-5]|[1-5])(\\:)([0-5][0-9]|[1-9])))|((\u00be\u00c6\u00c4\u00a7) (([0][6-9]|[1][0-1]|[6-9])\u00bd\u00c3([ ]([0-5][0-9]|[1-9])\u00ba\u00d0)?|([0][6-9]|[1][0-1]|[6-9])(\\:)([0-5][0-9]|[1-9])))|((\u00bf\u00c0\u00c0\u00fc) (([0][6-9]|[1][0-1]|[6-9])\u00bd\u00c3([ ]([0-5][0-9]|[1-9])\u00ba\u00d0)?|([0][6-9]|[1][0-1]|[6-9])(\\:)([0-5][0-9]|[1-9])))|((\u00bf\u00c0\u00c8\u00c4) (([0][1-5]|[1][2]|[1-5])\u00bd\u00c3([ ]([0-5][0-9]|[1-9])\u00ba\u00d0)?|([0][1-5]|[1][2]|[1-5])(\\:)([0-5][0-9]|[1-9])))|((\u00c0\u00fa\u00b3\u00e1) (([0][6-9]|[1][0]|[6-9])\u00bd\u00c3([ ]([0-5][0-9]|[1-9])\u00ba\u00d0)?|([0][6-9]|[1][0]|[6-9])(\\:)([0-5][0-9]|[1-9])))|((\u00b9\u00e3) (([0][8-9]|[1][0-2]|[8-9])\u00bd\u00c3([ ]([0-5][0-9]|[1-9])\u00ba\u00d0)?|([0][8-9]|[1][0-2]|[8-9])(\\:)([0-5][0-9]|[1-9]))))"

.field public static final KOREAN_TIME_KEYWORD_PATTERN:Ljava/util/regex/Pattern;

.field private static final SEPERATOR_FORMAT:Ljava/lang/String; = "[ \\,][ ]?"

.field private static final STANDARD_DATE_TIME_1_SCHEME:Ljava/lang/String; = "standard_date_time_1://"

.field private static final STANDARD_DATE_TIME_2_SCHEME:Ljava/lang/String; = "standard_data_time_2://"

.field private static final STANDARD_DATE_TIME_3_SCHEME:Ljava/lang/String; = "standard_date_time_3://"

.field private static final STANDARD_DATE_TIME_4_SCHEME:Ljava/lang/String; = "standard_date_time_4://"

.field public static final STANDARD_DATE_TIME_PATTERN:Ljava/util/regex/Pattern;

.field private static final STANDARD_DAY_FORMAT:Ljava/lang/String; = "([1-2][0-9]|[3][0-1]|[1-9]|[0][1-9])"

.field private static final STANDARD_MONTH_FORMAT:Ljava/lang/String; = "([1][0-2]|[1-9]|[0][1-9])"

.field public static final STANDARD_ONLY_DATE_PATTERN:Ljava/util/regex/Pattern;

.field public static final STANDARD_ONLY_TIME_PATTERN:Ljava/util/regex/Pattern;

.field public static final STANDARD_TIME_DATE_PATTERN:Ljava/util/regex/Pattern;

.field private static final STANDARD_TIME_FORMAT:Ljava/lang/String; = "(((([1][3-9])|([2][0-3]))[\\:]([0-5][0-9]|[1-9]))|(([0-1][0-9]|[1-9])[ ]?[aApP][mM])|(([0-1][0-9]|[1-9])[\\:]([0-5][0-9]|[1-9])([ ]?[aApP][mM])?))"

.field private static final STANDARD_YEAR_FORMAT:Ljava/lang/String; = "(18|19|20|21)\\d{2}"

.field private static final STRING_COMMA:Ljava/lang/String; = ","

.field private static final STRING_EMPTY:Ljava/lang/String; = ""

.field private static final STRING_FORWARD_SLASH:Ljava/lang/String; = "/"

.field private static final STRING_OR:Ljava/lang/String; = "|"

.field private static final STRING_PARANTHESIS_CLOSE:Ljava/lang/String; = ")"

.field private static final STRING_PARANTHESIS_OPEN:Ljava/lang/String; = "("

.field private static final STRING_PERIOD:Ljava/lang/String; = "."

.field private static WESTERN_DATE_KEYWORD_FORMAT:Ljava/lang/String; = null

.field public static WESTERN_DATE_KEYWORD_PATTERN:Ljava/util/regex/Pattern; = null

.field private static final WESTERN_DATE_TIME_1_SCHEME:Ljava/lang/String; = "western_date_time_1://"

.field private static final WESTERN_DATE_TIME_2_SCHEME:Ljava/lang/String; = "western_date_time_2://"

.field private static final WESTERN_DATE_TIME_3_SCHEME:Ljava/lang/String; = "western_date_time_3://"

.field private static final WESTERN_DATE_TIME_4_SCHEME:Ljava/lang/String; = "western_date_time_4://"

.field private static final WESTERN_DATE_TIME_5_SCHEME:Ljava/lang/String; = "western_date_time_5://"

.field public static WESTERN_DATE_TIME_PATTERN:Ljava/util/regex/Pattern;

.field private static WESTERN_DAY_KEYWORD_FORMAT:Ljava/lang/String;

.field private static WESTERN_DAY_MONTH_FORMAT:Ljava/lang/String;

.field private static WESTERN_MONTH_DAY_FORMAT:Ljava/lang/String;

.field public static WESTERN_ONLY_DATE_PATTERN:Ljava/util/regex/Pattern;

.field public static WESTERN_TIME_DATE_PATTERN:Ljava/util/regex/Pattern;

.field private static WESTERN_TIME_KEYWORD_FORMAT:Ljava/lang/String;

.field public static WESTERN_TIME_KEYWORD_PATTERN:Ljava/util/regex/Pattern;

.field static mArrDayOfWeek:[Ljava/lang/String;

.field private static mContext:Landroid/content/Context;

.field private static mInternalURLSpan:Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$InternalURLSpan;

.field private static mModifiedTime:J


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 1350
    const/4 v0, 0x7

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    .line 1351
    const-string v2, "sunday"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "monday"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "tuesday"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "wednesday"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "thursday"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "friday"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "saturday"

    aput-object v2, v0, v1

    .line 1350
    sput-object v0, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify;->mArrDayOfWeek:[Ljava/lang/String;

    .line 1450
    const-string v0, "(((18|19|20|21)\\d{2}\\/([1][0-2]|[1-9]|[0][1-9])\\/([1-2][0-9]|[3][0-1]|[1-9]|[0][1-9]))|(([1][0-2]|[1-9]|[0][1-9])\\/([1-2][0-9]|[3][0-1]|[1-9]|[0][1-9])\\/(18|19|20|21)\\d{2})|(([1-2][0-9]|[3][0-1]|[1-9]|[0][1-9])\\/([1][0-2]|[1-9]|[0][1-9])\\/(18|19|20|21)\\d{2}))"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify;->STANDARD_ONLY_DATE_PATTERN:Ljava/util/regex/Pattern;

    .line 1460
    const-string v0, "(((([1][3-9])|([2][0-3]))[\\:]([0-5][0-9]|[1-9]))|(([0-1][0-9]|[1-9])[ ]?[aApP][mM])|(([0-1][0-9]|[1-9])[\\:]([0-5][0-9]|[1-9])([ ]?[aApP][mM])?))"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify;->STANDARD_ONLY_TIME_PATTERN:Ljava/util/regex/Pattern;

    .line 1462
    const-string v0, "(((18|19|20|21)\\d{2}\\/([1][0-2]|[1-9]|[0][1-9])\\/([1-2][0-9]|[3][0-1]|[1-9]|[0][1-9])[ \\,][ ]?(((([1][3-9])|([2][0-3]))[\\:]([0-5][0-9]|[1-9]))|(([0-1][0-9]|[1-9])[ ]?[aApP][mM])|(([0-1][0-9]|[1-9])[\\:]([0-5][0-9]|[1-9])([ ]?[aApP][mM])?)))|(([1][0-2]|[1-9]|[0][1-9])\\/([1-2][0-9]|[3][0-1]|[1-9]|[0][1-9])\\/(18|19|20|21)\\d{2}[ \\,][ ]?(((([1][3-9])|([2][0-3]))[\\:]([0-5][0-9]|[1-9]))|(([0-1][0-9]|[1-9])[ ]?[aApP][mM])|(([0-1][0-9]|[1-9])[\\:]([0-5][0-9]|[1-9])([ ]?[aApP][mM])?)))|(([1-2][0-9]|[3][0-1]|[1-9]|[0][1-9])\\/([1][0-2]|[1-9]|[0][1-9])\\/(18|19|20|21)\\d{2}[ \\,][ ]?(((([1][3-9])|([2][0-3]))[\\:]([0-5][0-9]|[1-9]))|(([0-1][0-9]|[1-9])[ ]?[aApP][mM])|(([0-1][0-9]|[1-9])[\\:]([0-5][0-9]|[1-9])([ ]?[aApP][mM])?))))"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify;->STANDARD_DATE_TIME_PATTERN:Ljava/util/regex/Pattern;

    .line 1474
    const-string v0, "(((((([1][3-9])|([2][0-3]))[\\:]([0-5][0-9]|[1-9]))|(([0-1][0-9]|[1-9])[ ]?[aApP][mM])|(([0-1][0-9]|[1-9])[\\:]([0-5][0-9]|[1-9])([ ]?[aApP][mM])?))[ \\,][ ]?(18|19|20|21)\\d{2}\\/([1][0-2]|[1-9]|[0][1-9])\\/([1-2][0-9]|[3][0-1]|[1-9]|[0][1-9]))|((((([1][3-9])|([2][0-3]))[\\:]([0-5][0-9]|[1-9]))|(([0-1][0-9]|[1-9])[ ]?[aApP][mM])|(([0-1][0-9]|[1-9])[\\:]([0-5][0-9]|[1-9])([ ]?[aApP][mM])?))[ \\,][ ]?([1][0-2]|[1-9]|[0][1-9])\\/([1-2][0-9]|[3][0-1]|[1-9]|[0][1-9])\\/(18|19|20|21)\\d{2})|((((([1][3-9])|([2][0-3]))[\\:]([0-5][0-9]|[1-9]))|(([0-1][0-9]|[1-9])[ ]?[aApP][mM])|(([0-1][0-9]|[1-9])[\\:]([0-5][0-9]|[1-9])([ ]?[aApP][mM])?))[ \\,][ ]?([1-2][0-9]|[3][0-1]|[1-9]|[0][1-9])\\/([1][0-2]|[1-9]|[0][1-9])\\/(18|19|20|21)\\d{2}))"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify;->STANDARD_TIME_DATE_PATTERN:Ljava/util/regex/Pattern;

    .line 1486
    const-string v0, "(((((J(anuary|uly)|Ma(rch|y)|August|(Octo|Decem)ber)|((J(an|ul)|Ma(r|y)|Aug|Oct|Dec)[\\.\\,]?)) (0[1-9]|([1](st)?|[2](nd)?|[3](rd)?|[4-90](th)?)|[1][0-9](th)?|[2]([1](st)?|[2](nd)?|[3](rd)?|[4-90](th)?)|3(0(th)?|1(st)?))|((April|June|(Sept|Nov)ember)|((Apr|Jun|Sep|Nov)[\\.\\,]?)) (0[1-9]|([1](st)?|[2](nd)?|[3](rd)?|[4-90](th)?)|[1][0-9](th)?|[2]([1](st)?|[2](nd)?|[3](rd)?|[4-90](th)?)|30(th)?)|(February|Feb[\\.\\,]?) (0[1-9]|([1](st)?|[2](nd)?|[3](rd)?|[4-90](th)?)|[1][0-9](th)?|2([1](st)?|[2](nd)?|[3](rd)?|[4-80](th)?))|(February|Feb[\\.\\,]?)29(th)? ((0[48]|[2468][048]|[13579][26])00|[0-9]{2}(0[48]|[2468][048]|[13579][26])))|((0[1-9]|([1](st)?|[2](nd)?|[3](rd)?|[4-90](th)?)|[1][0-9](th)?|[2]([1](st)?|[2](nd)?|[3](rd)?|[4-90](th)?)|3(0(th)?|1(st)?)) ((J(anuary|uly)|Ma(rch|y)|August|(Octo|Decem)ber)|((J(an|ul)|Ma(r|y)|Aug|Oct|Dec)[\\.\\,]?))|(0[1-9]|([1](st)?|[2](nd)?|[3](rd)?|[4-90](th)?)|[1][0-9](th)?|[2]([1](st)?|[2](nd)?|[3](rd)?|[4-90](th)?)|30(th)?) ((April|June|(Sept|Nov)ember)|((Apr|Jun|Sep|Nov)[\\.\\,]?))|(0[1-9]|([1](st)?|[2](nd)?|[3](rd)?|[4-90](th)?)|[1][0-9](th)?|2([1](st)?|[2](nd)?|[3](rd)?|[4-80](th)?)) (February|Feb[\\.\\,]?)|29(th)? (February|Feb[\\.\\,]?) ((0[48]|[2468][048]|[13579][26])00|[0-9]{2}(0[48]|[2468][048]|[13579][26])))) (18|19|20|21)\\d{2})"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify;->ENGLISH_ONLY_DATE_PATTERN:Ljava/util/regex/Pattern;

    .line 1491
    const-string v0, "(((((J(anuary|uly)|Ma(rch|y)|August|(Octo|Decem)ber)|((J(an|ul)|Ma(r|y)|Aug|Oct|Dec)[\\.\\,]?)) (0[1-9]|([1](st)?|[2](nd)?|[3](rd)?|[4-90](th)?)|[1][0-9](th)?|[2]([1](st)?|[2](nd)?|[3](rd)?|[4-90](th)?)|3(0(th)?|1(st)?))|((April|June|(Sept|Nov)ember)|((Apr|Jun|Sep|Nov)[\\.\\,]?)) (0[1-9]|([1](st)?|[2](nd)?|[3](rd)?|[4-90](th)?)|[1][0-9](th)?|[2]([1](st)?|[2](nd)?|[3](rd)?|[4-90](th)?)|30(th)?)|(February|Feb[\\.\\,]?) (0[1-9]|([1](st)?|[2](nd)?|[3](rd)?|[4-90](th)?)|[1][0-9](th)?|2([1](st)?|[2](nd)?|[3](rd)?|[4-80](th)?))|(February|Feb[\\.\\,]?)29(th)? ((0[48]|[2468][048]|[13579][26])00|[0-9]{2}(0[48]|[2468][048]|[13579][26])))|((0[1-9]|([1](st)?|[2](nd)?|[3](rd)?|[4-90](th)?)|[1][0-9](th)?|[2]([1](st)?|[2](nd)?|[3](rd)?|[4-90](th)?)|3(0(th)?|1(st)?)) ((J(anuary|uly)|Ma(rch|y)|August|(Octo|Decem)ber)|((J(an|ul)|Ma(r|y)|Aug|Oct|Dec)[\\.\\,]?))|(0[1-9]|([1](st)?|[2](nd)?|[3](rd)?|[4-90](th)?)|[1][0-9](th)?|[2]([1](st)?|[2](nd)?|[3](rd)?|[4-90](th)?)|30(th)?) ((April|June|(Sept|Nov)ember)|((Apr|Jun|Sep|Nov)[\\.\\,]?))|(0[1-9]|([1](st)?|[2](nd)?|[3](rd)?|[4-90](th)?)|[1][0-9](th)?|2([1](st)?|[2](nd)?|[3](rd)?|[4-80](th)?)) (February|Feb[\\.\\,]?)|29(th)? (February|Feb[\\.\\,]?) ((0[48]|[2468][048]|[13579][26])00|[0-9]{2}(0[48]|[2468][048]|[13579][26])))) (18|19|20|21)\\d{2}[ \\,][ ]?(((([1][3-9])|([2][0-3]))[\\:]([0-5][0-9]|[1-9]))|(([0-1][0-9]|[1-9])[ ]?[aApP][mM])|(([0-1][0-9]|[1-9])[\\:]([0-5][0-9]|[1-9])([ ]?[aApP][mM])?)))"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify;->ENGLISH_DATE_TIME_PATTERN:Ljava/util/regex/Pattern;

    .line 1496
    const-string v0, "((((([1][3-9])|([2][0-3]))[\\:]([0-5][0-9]|[1-9]))|(([0-1][0-9]|[1-9])[ ]?[aApP][mM])|(([0-1][0-9]|[1-9])[\\:]([0-5][0-9]|[1-9])([ ]?[aApP][mM])?))[ \\,][ ]?((((J(anuary|uly)|Ma(rch|y)|August|(Octo|Decem)ber)|((J(an|ul)|Ma(r|y)|Aug|Oct|Dec)[\\.\\,]?)) (0[1-9]|([1](st)?|[2](nd)?|[3](rd)?|[4-90](th)?)|[1][0-9](th)?|[2]([1](st)?|[2](nd)?|[3](rd)?|[4-90](th)?)|3(0(th)?|1(st)?))|((April|June|(Sept|Nov)ember)|((Apr|Jun|Sep|Nov)[\\.\\,]?)) (0[1-9]|([1](st)?|[2](nd)?|[3](rd)?|[4-90](th)?)|[1][0-9](th)?|[2]([1](st)?|[2](nd)?|[3](rd)?|[4-90](th)?)|30(th)?)|(February|Feb[\\.\\,]?) (0[1-9]|([1](st)?|[2](nd)?|[3](rd)?|[4-90](th)?)|[1][0-9](th)?|2([1](st)?|[2](nd)?|[3](rd)?|[4-80](th)?))|(February|Feb[\\.\\,]?)29(th)? ((0[48]|[2468][048]|[13579][26])00|[0-9]{2}(0[48]|[2468][048]|[13579][26])))|((0[1-9]|([1](st)?|[2](nd)?|[3](rd)?|[4-90](th)?)|[1][0-9](th)?|[2]([1](st)?|[2](nd)?|[3](rd)?|[4-90](th)?)|3(0(th)?|1(st)?)) ((J(anuary|uly)|Ma(rch|y)|August|(Octo|Decem)ber)|((J(an|ul)|Ma(r|y)|Aug|Oct|Dec)[\\.\\,]?))|(0[1-9]|([1](st)?|[2](nd)?|[3](rd)?|[4-90](th)?)|[1][0-9](th)?|[2]([1](st)?|[2](nd)?|[3](rd)?|[4-90](th)?)|30(th)?) ((April|June|(Sept|Nov)ember)|((Apr|Jun|Sep|Nov)[\\.\\,]?))|(0[1-9]|([1](st)?|[2](nd)?|[3](rd)?|[4-90](th)?)|[1][0-9](th)?|2([1](st)?|[2](nd)?|[3](rd)?|[4-80](th)?)) (February|Feb[\\.\\,]?)|29(th)? (February|Feb[\\.\\,]?) ((0[48]|[2468][048]|[13579][26])00|[0-9]{2}(0[48]|[2468][048]|[13579][26])))) (18|19|20|21)\\d{2})"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify;->ENGLISH_TIME_DATE_PATTERN:Ljava/util/regex/Pattern;

    .line 1502
    const-string v0, "(([tT](onight|oday|omorrow))([ \\,][ ]?(((([1][3-9])|([2][0-3]))[\\:]([0-5][0-9]|[1-9]))|(([0-1][0-9]|[1-9])[ ]?[aApP][mM])|(([0-1][0-9]|[1-9])[\\:]([0-5][0-9]|[1-9])([ ]?[aApP][mM])?)))?)"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify;->ENGLISH_DATE_KEYWORD_PATTERN:Ljava/util/regex/Pattern;

    .line 1506
    const-string v0, "((([sS](unday|aterday))|([mM]onday)|([tT](uesday|hursday))|([wW]ednesday)|([fF]riday))[ \\,][ ]?((((([1][3-9])|([2][0-3]))[\\:]([0-5][0-9]|[1-9]))|(([0-1][0-9]|[1-9])[ ]?[aApP][mM])|(([0-1][0-9]|[1-9])[\\:]([0-5][0-9]|[1-9])([ ]?[aApP][mM])?))|((([mM]orning) ([0][6-9]|[1][0-1]|[6-9])(\\:)([0-5][0-9]|[1-9]))|(([aA]fternoon) ([0][1-5]|[1][2]|[1-5])(\\:)([0-5][0-9]|[1-9]))|(([eE]vening) ([0][6-9]|[1][0]|[6-9])(\\:)([0-5][0-9]|[1-9]))|(([nN]ight) ([0][8-9]|[1][0-2]|[8-9])(\\:)([0-5][0-9]|[1-9])))))"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify;->ENGLISH_TIME_KEYWORD_PATTERN:Ljava/util/regex/Pattern;

    .line 1510
    const-string v0, "((18|19|20|21)\\d{2}\ub144 ([1][0-2]|[1-9]|[0][1-9])\uc6d4 ([1-2][0-9]|[3][0-1]|[1-9]|[0][1-9])\uc77c)"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify;->KOREAN_ONLY_DATE_PATTERN:Ljava/util/regex/Pattern;

    .line 1513
    const-string v0, "((18|19|20|21)\\d{2}\ub144 ([1][0-2]|[1-9]|[0][1-9])\uc6d4 ([1-2][0-9]|[3][0-1]|[1-9]|[0][1-9])\uc77c[ \\,][ ]?(((([0-1][0-9]|[1-9]|([2][0-4]))\uc2dc)[ ]?(([0-5][0-9]|[1-9])\ubd84)?)|(((([1][3-9])|([2][0-3]))[\\:]([0-5][0-9]|[1-9]))|(([0-1][0-9]|[1-9])[ ]?[aApP][mM])|(([0-1][0-9]|[1-9])[\\:]([0-5][0-9]|[1-9])([ ]?[aApP][mM])?))))"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify;->KOREAN_DATE_TIME_PATTERN:Ljava/util/regex/Pattern;

    .line 1516
    const-string v0, "((((([0-1][0-9]|[1-9]|([2][0-4]))\uc2dc)[ ]?(([0-5][0-9]|[1-9])\ubd84)?)|(((([1][3-9])|([2][0-3]))[\\:]([0-5][0-9]|[1-9]))|(([0-1][0-9]|[1-9])[ ]?[aApP][mM])|(([0-1][0-9]|[1-9])[\\:]([0-5][0-9]|[1-9])([ ]?[aApP][mM])?)))[ \\,][ ]?(18|19|20|21)\\d{2}\ub144 ([1][0-2]|[1-9]|[0][1-9])\uc6d4 ([1-2][0-9]|[3][0-1]|[1-9]|[0][1-9])\uc77c)"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify;->KOREAN_TIME_DATE_PATTERN:Ljava/util/regex/Pattern;

    .line 1520
    const-string v0, "(((\u00bf\u00c0\u00b4\u00c3|\u00b3\u00bb\u00c0\u00cf)[ \\,][ ]?(((([0-1][0-9]|[1-9]|([2][0-4]))\uc2dc)[ ]?(([0-5][0-9]|[1-9])\ubd84)?)|(((([1][3-9])|([2][0-3]))[\\:]([0-5][0-9]|[1-9]))|(([0-1][0-9]|[1-9])[ ]?[aApP][mM])|(([0-1][0-9]|[1-9])[\\:]([0-5][0-9]|[1-9])([ ]?[aApP][mM])?))))([ \\,][ ]?(((([0-1][0-9]|[1-9]|([2][0-4]))\uc2dc)[ ]?(([0-5][0-9]|[1-9])\ubd84)?)|(((([1][3-9])|([2][0-3]))[\\:]([0-5][0-9]|[1-9]))|(([0-1][0-9]|[1-9])[ ]?[aApP][mM])|(([0-1][0-9]|[1-9])[\\:]([0-5][0-9]|[1-9])([ ]?[aApP][mM])?))))?)"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify;->KOREAN_DATE_KEYWORD_PATTERN:Ljava/util/regex/Pattern;

    .line 1524
    const-string v0, "(((([\u00c0\u00cf\u00bf\u00f9\u00c8\u00ad\u00bc\u00f6\u00b8\u00f1\u00b1\u00dd\u00c5\u00e4]\u00bf\u00e4\u00c0\u00cf))[ \\,][ ]?(((([1][3-9])|([2][0-3]))[\\:]([0-5][0-9]|[1-9]))|(([0-1][0-9]|[1-9])[ ]?[aApP][mM])|(([0-1][0-9]|[1-9])[\\:]([0-5][0-9]|[1-9])([ ]?[aApP][mM])?)))|(((([\u00c0\u00cf\u00bf\u00f9\u00c8\u00ad\u00bc\u00f6\u00b8\u00f1\u00b1\u00dd\u00c5\u00e4]\u00bf\u00e4\u00c0\u00cf))[ \\,][ ]?)?(((\u00bb\u00f5\u00ba\u00ae) (([0][1-5]|[1-5])\u00bd\u00c3([ ]([0-5][0-9]|[1-9])\u00ba\u00d0)?|([0][1-5]|[1-5])(\\:)([0-5][0-9]|[1-9])))|((\u00be\u00c6\u00c4\u00a7) (([0][6-9]|[1][0-1]|[6-9])\u00bd\u00c3([ ]([0-5][0-9]|[1-9])\u00ba\u00d0)?|([0][6-9]|[1][0-1]|[6-9])(\\:)([0-5][0-9]|[1-9])))|((\u00bf\u00c0\u00c0\u00fc) (([0][6-9]|[1][0-1]|[6-9])\u00bd\u00c3([ ]([0-5][0-9]|[1-9])\u00ba\u00d0)?|([0][6-9]|[1][0-1]|[6-9])(\\:)([0-5][0-9]|[1-9])))|((\u00bf\u00c0\u00c8\u00c4) (([0][1-5]|[1][2]|[1-5])\u00bd\u00c3([ ]([0-5][0-9]|[1-9])\u00ba\u00d0)?|([0][1-5]|[1][2]|[1-5])(\\:)([0-5][0-9]|[1-9])))|((\u00c0\u00fa\u00b3\u00e1) (([0][6-9]|[1][0]|[6-9])\u00bd\u00c3([ ]([0-5][0-9]|[1-9])\u00ba\u00d0)?|([0][6-9]|[1][0]|[6-9])(\\:)([0-5][0-9]|[1-9])))|((\u00b9\u00e3) (([0][8-9]|[1][0-2]|[8-9])\u00bd\u00c3([ ]([0-5][0-9]|[1-9])\u00ba\u00d0)?|([0][8-9]|[1][0-2]|[8-9])(\\:)([0-5][0-9]|[1-9])))))|((([\u00c0\u00cf\u00bf\u00f9\u00c8\u00ad\u00bc\u00f6\u00b8\u00f1\u00b1\u00dd\u00c5\u00e4]\u00bf\u00e4\u00c0\u00cf))[ \\,][ ]?((([0-1][0-9]|[1-9]|([2][0-4]))\uc2dc)[ ]?(([0-5][0-9]|[1-9])\ubd84)?)))"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify;->KOREAN_TIME_KEYWORD_PATTERN:Ljava/util/regex/Pattern;

    .line 1530
    const-string v0, "(((([0-1][0-9]|[1-9]|([2][0-4]))\uc2dc)[ ]?(([0-5][0-9]|[1-9])\ubd84)?))"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify;->KOREAN_ONLY_TIME_PATTERN:Ljava/util/regex/Pattern;

    .line 1541
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$0()Landroid/content/Context;
    .locals 1

    .prologue
    .line 113
    sget-object v0, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method public static actionCalendar()Landroid/app/AlertDialog;
    .locals 3

    .prologue
    .line 500
    sget-object v0, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify;->mInternalURLSpan:Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$InternalURLSpan;

    if-nez v0, :cond_0

    .line 503
    const/4 v0, 0x0

    .line 505
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify;->mInternalURLSpan:Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$InternalURLSpan;

    iget-object v0, v0, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$InternalURLSpan;->mTime:Ljava/lang/String;

    sget-object v1, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify;->mInternalURLSpan:Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$InternalURLSpan;

    iget-boolean v1, v1, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$InternalURLSpan;->mIsAllDay:Z

    .line 506
    sget-object v2, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify;->mInternalURLSpan:Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$InternalURLSpan;

    iget-object v2, v2, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$InternalURLSpan;->mDesc:Ljava/lang/String;

    .line 505
    invoke-static {v0, v1, v2}, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify;->showPopup(Ljava/lang/String;ZLjava/lang/String;)Landroid/app/AlertDialog;

    move-result-object v0

    goto :goto_0
.end method

.method public static final addLinks(Landroid/text/Spannable;ILandroid/content/Context;J)Z
    .locals 11
    .param p0, "text"    # Landroid/text/Spannable;
    .param p1, "mask"    # I
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "modifiedTime"    # J

    .prologue
    .line 340
    sput-object p2, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify;->mContext:Landroid/content/Context;

    .line 341
    sput-wide p3, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify;->mModifiedTime:J

    .line 343
    if-nez p1, :cond_0

    .line 344
    const/4 v7, 0x0

    .line 452
    :goto_0
    return v7

    .line 347
    :cond_0
    invoke-static {}, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify;->setWesternDateTimeFormat()V

    .line 349
    const/4 v7, 0x0

    invoke-interface {p0}, Landroid/text/Spannable;->length()I

    move-result v8

    const-class v9, Landroid/text/style/URLSpan;

    invoke-interface {p0, v7, v8, v9}, Landroid/text/Spannable;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v5

    check-cast v5, [Landroid/text/style/URLSpan;

    .line 351
    .local v5, "old":[Landroid/text/style/URLSpan;
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 353
    .local v4, "links":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/app/memo/uiwidget/LinkSpec;>;"
    and-int/lit8 v7, p1, 0x10

    if-eqz v7, :cond_1

    .line 354
    sget-object v7, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify;->STANDARD_ONLY_DATE_PATTERN:Ljava/util/regex/Pattern;

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/String;

    const/4 v9, 0x0

    .line 355
    const-string v10, "standard_date_time_1://"

    aput-object v10, v8, v9

    .line 354
    invoke-static {v4, p0, v7, v8}, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify;->gatherLinks(Ljava/util/ArrayList;Landroid/text/Spannable;Ljava/util/regex/Pattern;[Ljava/lang/String;)V

    .line 357
    sget-object v7, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify;->STANDARD_ONLY_TIME_PATTERN:Ljava/util/regex/Pattern;

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/String;

    const/4 v9, 0x0

    .line 358
    const-string v10, "standard_data_time_2://"

    aput-object v10, v8, v9

    .line 357
    invoke-static {v4, p0, v7, v8}, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify;->gatherLinks(Ljava/util/ArrayList;Landroid/text/Spannable;Ljava/util/regex/Pattern;[Ljava/lang/String;)V

    .line 360
    sget-object v7, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify;->STANDARD_DATE_TIME_PATTERN:Ljava/util/regex/Pattern;

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/String;

    const/4 v9, 0x0

    .line 361
    const-string v10, "standard_date_time_3://"

    aput-object v10, v8, v9

    .line 360
    invoke-static {v4, p0, v7, v8}, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify;->gatherLinks(Ljava/util/ArrayList;Landroid/text/Spannable;Ljava/util/regex/Pattern;[Ljava/lang/String;)V

    .line 363
    sget-object v7, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify;->STANDARD_TIME_DATE_PATTERN:Ljava/util/regex/Pattern;

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/String;

    const/4 v9, 0x0

    .line 364
    const-string v10, "standard_date_time_4://"

    aput-object v10, v8, v9

    .line 363
    invoke-static {v4, p0, v7, v8}, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify;->gatherLinks(Ljava/util/ArrayList;Landroid/text/Spannable;Ljava/util/regex/Pattern;[Ljava/lang/String;)V

    .line 367
    sget-object v7, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify;->ENGLISH_ONLY_DATE_PATTERN:Ljava/util/regex/Pattern;

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/String;

    const/4 v9, 0x0

    .line 368
    const-string v10, "english_date_time_1://"

    aput-object v10, v8, v9

    .line 367
    invoke-static {v4, p0, v7, v8}, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify;->gatherLinks(Ljava/util/ArrayList;Landroid/text/Spannable;Ljava/util/regex/Pattern;[Ljava/lang/String;)V

    .line 370
    sget-object v7, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify;->ENGLISH_DATE_TIME_PATTERN:Ljava/util/regex/Pattern;

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/String;

    const/4 v9, 0x0

    .line 371
    const-string v10, "english_date_time_2://"

    aput-object v10, v8, v9

    .line 370
    invoke-static {v4, p0, v7, v8}, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify;->gatherLinks(Ljava/util/ArrayList;Landroid/text/Spannable;Ljava/util/regex/Pattern;[Ljava/lang/String;)V

    .line 373
    sget-object v7, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify;->ENGLISH_TIME_DATE_PATTERN:Ljava/util/regex/Pattern;

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/String;

    const/4 v9, 0x0

    .line 374
    const-string v10, "english_date_time_3://"

    aput-object v10, v8, v9

    .line 373
    invoke-static {v4, p0, v7, v8}, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify;->gatherLinks(Ljava/util/ArrayList;Landroid/text/Spannable;Ljava/util/regex/Pattern;[Ljava/lang/String;)V

    .line 376
    sget-object v7, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify;->ENGLISH_DATE_KEYWORD_PATTERN:Ljava/util/regex/Pattern;

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/String;

    const/4 v9, 0x0

    .line 377
    const-string v10, "english_date_time_4://"

    aput-object v10, v8, v9

    .line 376
    invoke-static {v4, p0, v7, v8}, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify;->gatherLinks(Ljava/util/ArrayList;Landroid/text/Spannable;Ljava/util/regex/Pattern;[Ljava/lang/String;)V

    .line 379
    sget-object v7, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify;->ENGLISH_TIME_KEYWORD_PATTERN:Ljava/util/regex/Pattern;

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/String;

    const/4 v9, 0x0

    .line 380
    const-string v10, "english_date_time_5://"

    aput-object v10, v8, v9

    .line 379
    invoke-static {v4, p0, v7, v8}, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify;->gatherLinks(Ljava/util/ArrayList;Landroid/text/Spannable;Ljava/util/regex/Pattern;[Ljava/lang/String;)V

    .line 383
    sget-object v7, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify;->mContext:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v7

    iget-object v7, v7, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    .line 384
    invoke-virtual {v7}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v0

    .line 385
    .local v0, "currentLanguage":Ljava/lang/String;
    const-string v7, "fr"

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 387
    sget-object v7, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify;->WESTERN_ONLY_DATE_PATTERN:Ljava/util/regex/Pattern;

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/String;

    const/4 v9, 0x0

    .line 388
    const-string v10, "western_date_time_1://"

    aput-object v10, v8, v9

    .line 387
    invoke-static {v4, p0, v7, v8}, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify;->gatherLinks(Ljava/util/ArrayList;Landroid/text/Spannable;Ljava/util/regex/Pattern;[Ljava/lang/String;)V

    .line 390
    sget-object v7, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify;->WESTERN_DATE_TIME_PATTERN:Ljava/util/regex/Pattern;

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/String;

    const/4 v9, 0x0

    .line 391
    const-string v10, "western_date_time_2://"

    aput-object v10, v8, v9

    .line 390
    invoke-static {v4, p0, v7, v8}, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify;->gatherLinks(Ljava/util/ArrayList;Landroid/text/Spannable;Ljava/util/regex/Pattern;[Ljava/lang/String;)V

    .line 393
    sget-object v7, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify;->WESTERN_TIME_DATE_PATTERN:Ljava/util/regex/Pattern;

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/String;

    const/4 v9, 0x0

    .line 394
    const-string v10, "western_date_time_3://"

    aput-object v10, v8, v9

    .line 393
    invoke-static {v4, p0, v7, v8}, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify;->gatherLinks(Ljava/util/ArrayList;Landroid/text/Spannable;Ljava/util/regex/Pattern;[Ljava/lang/String;)V

    .line 396
    sget-object v7, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify;->WESTERN_DATE_KEYWORD_PATTERN:Ljava/util/regex/Pattern;

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/String;

    const/4 v9, 0x0

    .line 397
    const-string v10, "western_date_time_4://"

    aput-object v10, v8, v9

    .line 396
    invoke-static {v4, p0, v7, v8}, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify;->gatherLinks(Ljava/util/ArrayList;Landroid/text/Spannable;Ljava/util/regex/Pattern;[Ljava/lang/String;)V

    .line 399
    sget-object v7, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify;->WESTERN_TIME_KEYWORD_PATTERN:Ljava/util/regex/Pattern;

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/String;

    const/4 v9, 0x0

    .line 400
    const-string v10, "western_date_time_5://"

    aput-object v10, v8, v9

    .line 399
    invoke-static {v4, p0, v7, v8}, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify;->gatherLinks(Ljava/util/ArrayList;Landroid/text/Spannable;Ljava/util/regex/Pattern;[Ljava/lang/String;)V

    .line 427
    .end local v0    # "currentLanguage":Ljava/lang/String;
    :cond_1
    :goto_1
    invoke-static {v4}, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify;->pruneOverlaps(Ljava/util/ArrayList;)V

    .line 429
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v7

    if-nez v7, :cond_3

    .line 430
    const/4 v7, 0x0

    goto/16 :goto_0

    .line 403
    .restart local v0    # "currentLanguage":Ljava/lang/String;
    :cond_2
    sget-object v7, Ljava/util/Locale;->KOREAN:Ljava/util/Locale;

    invoke-virtual {v7}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 405
    sget-object v7, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify;->KOREAN_ONLY_DATE_PATTERN:Ljava/util/regex/Pattern;

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/String;

    const/4 v9, 0x0

    .line 406
    const-string v10, "korean_date_time_1://"

    aput-object v10, v8, v9

    .line 405
    invoke-static {v4, p0, v7, v8}, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify;->gatherLinks(Ljava/util/ArrayList;Landroid/text/Spannable;Ljava/util/regex/Pattern;[Ljava/lang/String;)V

    .line 408
    sget-object v7, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify;->KOREAN_DATE_TIME_PATTERN:Ljava/util/regex/Pattern;

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/String;

    const/4 v9, 0x0

    .line 409
    const-string v10, "korean_date_time_2://"

    aput-object v10, v8, v9

    .line 408
    invoke-static {v4, p0, v7, v8}, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify;->gatherLinks(Ljava/util/ArrayList;Landroid/text/Spannable;Ljava/util/regex/Pattern;[Ljava/lang/String;)V

    .line 411
    sget-object v7, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify;->KOREAN_TIME_DATE_PATTERN:Ljava/util/regex/Pattern;

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/String;

    const/4 v9, 0x0

    .line 412
    const-string v10, "korean_date_time_3://"

    aput-object v10, v8, v9

    .line 411
    invoke-static {v4, p0, v7, v8}, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify;->gatherLinks(Ljava/util/ArrayList;Landroid/text/Spannable;Ljava/util/regex/Pattern;[Ljava/lang/String;)V

    .line 414
    sget-object v7, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify;->KOREAN_DATE_KEYWORD_PATTERN:Ljava/util/regex/Pattern;

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/String;

    const/4 v9, 0x0

    .line 415
    const-string v10, "korean_date_time_4://"

    aput-object v10, v8, v9

    .line 414
    invoke-static {v4, p0, v7, v8}, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify;->gatherLinks(Ljava/util/ArrayList;Landroid/text/Spannable;Ljava/util/regex/Pattern;[Ljava/lang/String;)V

    .line 417
    sget-object v7, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify;->KOREAN_TIME_KEYWORD_PATTERN:Ljava/util/regex/Pattern;

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/String;

    const/4 v9, 0x0

    .line 418
    const-string v10, "korean_date_time_5://"

    aput-object v10, v8, v9

    .line 417
    invoke-static {v4, p0, v7, v8}, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify;->gatherLinks(Ljava/util/ArrayList;Landroid/text/Spannable;Ljava/util/regex/Pattern;[Ljava/lang/String;)V

    .line 420
    sget-object v7, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify;->KOREAN_ONLY_TIME_PATTERN:Ljava/util/regex/Pattern;

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/String;

    const/4 v9, 0x0

    .line 421
    const-string v10, "korean_date_time_6://"

    aput-object v10, v8, v9

    .line 420
    invoke-static {v4, p0, v7, v8}, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify;->gatherLinks(Ljava/util/ArrayList;Landroid/text/Spannable;Ljava/util/regex/Pattern;[Ljava/lang/String;)V

    goto :goto_1

    .line 434
    .end local v0    # "currentLanguage":Ljava/lang/String;
    :cond_3
    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_4
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-nez v8, :cond_5

    .line 452
    const/4 v7, 0x1

    goto/16 :goto_0

    .line 434
    :cond_5
    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/android/app/memo/uiwidget/LinkSpec;

    .line 435
    .local v3, "link":Lcom/samsung/android/app/memo/uiwidget/LinkSpec;
    sput-wide p3, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify;->mModifiedTime:J

    .line 436
    iget-object v8, v3, Lcom/samsung/android/app/memo/uiwidget/LinkSpec;->mUrl:Ljava/lang/String;

    iget v9, v3, Lcom/samsung/android/app/memo/uiwidget/LinkSpec;->mStart:I

    iget v10, v3, Lcom/samsung/android/app/memo/uiwidget/LinkSpec;->mEnd:I

    invoke-static {v8, v9, v10, p0}, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify;->applyLink(Ljava/lang/String;IILandroid/text/Spannable;)V

    .line 437
    iget-object v8, v3, Lcom/samsung/android/app/memo/uiwidget/LinkSpec;->mUrl:Ljava/lang/String;

    const-string v9, "standard_date_time_3://"

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    .line 438
    .local v1, "dateLink":Ljava/lang/String;
    const-string v8, "\\p{Space}"

    const-string v9, ""

    invoke-virtual {v1, v8, v9}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 441
    array-length v8, v5

    add-int/lit8 v2, v8, -0x1

    .local v2, "i":I
    :goto_2
    if-ltz v2, :cond_4

    .line 442
    aget-object v8, v5, v2

    invoke-virtual {v8}, Landroid/text/style/URLSpan;->getURL()Ljava/lang/String;

    move-result-object v6

    .line 443
    .local v6, "oldUrl":Ljava/lang/String;
    const-string v8, "tel:"

    invoke-virtual {v6, v8}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_6

    .line 444
    const-string v8, "tel:"

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    invoke-virtual {v6, v8}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v6

    .line 445
    invoke-virtual {v1, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_6

    .line 446
    aget-object v8, v5, v2

    invoke-interface {p0, v8}, Landroid/text/Spannable;->removeSpan(Ljava/lang/Object;)V

    .line 441
    :cond_6
    add-int/lit8 v2, v2, -0x1

    goto :goto_2
.end method

.method private static final applyLink(Ljava/lang/String;IILandroid/text/Spannable;)V
    .locals 20
    .param p0, "url"    # Ljava/lang/String;
    .param p1, "start"    # I
    .param p2, "end"    # I
    .param p3, "text"    # Landroid/text/Spannable;

    .prologue
    .line 548
    move-object/from16 v10, p0

    .line 549
    .local v10, "sTime":Ljava/lang/String;
    new-instance v4, Ljava/text/SimpleDateFormat;

    const-string v15, "yyyy/MM/dd"

    invoke-direct {v4, v15}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 550
    .local v4, "dateFormat":Ljava/text/SimpleDateFormat;
    new-instance v6, Ljava/text/SimpleDateFormat;

    const-string v15, "EEE"

    invoke-direct {v6, v15}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 551
    .local v6, "dayFormat":Ljava/text/SimpleDateFormat;
    const/4 v15, 0x0

    invoke-static {v15}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    .line 553
    .local v8, "isAllDay":Ljava/lang/Boolean;
    new-instance v15, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$InternalURLSpan;

    sget-object v16, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify;->mContext:Landroid/content/Context;

    invoke-direct/range {v15 .. v16}, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$InternalURLSpan;-><init>(Landroid/content/Context;)V

    sput-object v15, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify;->mInternalURLSpan:Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$InternalURLSpan;

    .line 554
    sget-object v15, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify;->mInternalURLSpan:Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$InternalURLSpan;

    invoke-interface/range {p3 .. p3}, Landroid/text/Spannable;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$InternalURLSpan;->setDescription(Ljava/lang/String;)V

    .line 558
    const-string v15, "standard_date_time_1://"

    invoke-virtual {v10, v15}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v15

    if-eqz v15, :cond_1

    .line 560
    const/4 v15, 0x1

    invoke-static {v15}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    .line 561
    new-instance v15, Ljava/lang/StringBuilder;

    const-string v16, "standard_date_time_1://"

    invoke-virtual/range {v16 .. v16}, Ljava/lang/String;->length()I

    move-result v16

    move/from16 v0, v16

    invoke-virtual {v10, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v16 .. v16}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v16

    invoke-direct/range {v15 .. v16}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v16, " 00:00"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    .line 778
    :cond_0
    :goto_0
    sget-object v15, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify;->mInternalURLSpan:Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$InternalURLSpan;

    invoke-virtual {v15, v10}, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$InternalURLSpan;->setTimeString(Ljava/lang/String;)V

    .line 779
    sget-object v15, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify;->mInternalURLSpan:Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$InternalURLSpan;

    invoke-virtual {v8}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v16

    invoke-virtual/range {v15 .. v16}, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$InternalURLSpan;->setAllDay(Z)V

    .line 781
    sget-object v15, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify;->mInternalURLSpan:Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$InternalURLSpan;

    const/16 v16, 0x21

    move-object/from16 v0, p3

    move/from16 v1, p1

    move/from16 v2, p2

    move/from16 v3, v16

    invoke-interface {v0, v15, v1, v2, v3}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    .line 783
    return-void

    .line 563
    :cond_1
    const-string v15, "standard_data_time_2://"

    invoke-virtual {v10, v15}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v15

    if-eqz v15, :cond_2

    .line 565
    const-string v15, "standard_data_time_2://"

    invoke-virtual {v15}, Ljava/lang/String;->length()I

    move-result v15

    invoke-virtual {v10, v15}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v10

    .line 566
    new-instance v11, Ljava/lang/StringBuffer;

    sget-wide v16, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify;->mModifiedTime:J

    invoke-static/range {v16 .. v17}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v15

    invoke-virtual {v4, v15}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v15

    invoke-direct {v11, v15}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 567
    .local v11, "sbTime":Ljava/lang/StringBuffer;
    const/16 v15, 0x20

    invoke-virtual {v11, v15}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    move-result-object v11

    .line 568
    invoke-virtual {v11, v10}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v11

    .line 569
    invoke-virtual {v11}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v10

    .line 571
    goto :goto_0

    .end local v11    # "sbTime":Ljava/lang/StringBuffer;
    :cond_2
    const-string v15, "standard_date_time_3://"

    invoke-virtual {v10, v15}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v15

    if-eqz v15, :cond_3

    .line 573
    const-string v15, "standard_date_time_3://"

    invoke-virtual {v15}, Ljava/lang/String;->length()I

    move-result v15

    invoke-virtual {v10, v15}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v10

    .line 575
    goto :goto_0

    :cond_3
    const-string v15, "standard_date_time_4://"

    invoke-virtual {v10, v15}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v15

    if-eqz v15, :cond_4

    .line 577
    const-string v15, "standard_date_time_4://"

    invoke-virtual {v15}, Ljava/lang/String;->length()I

    move-result v15

    invoke-virtual {v10, v15}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v10

    .line 578
    sget-object v15, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v10, v15}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v10

    .line 579
    const-string v15, " am"

    const-string v16, "am"

    move-object/from16 v0, v16

    invoke-virtual {v10, v15, v0}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v10

    .line 580
    const-string v15, " pm"

    const-string v16, "pm"

    move-object/from16 v0, v16

    invoke-virtual {v10, v15, v0}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v10

    .line 581
    const-string v15, ","

    const-string v16, " "

    move-object/from16 v0, v16

    invoke-virtual {v10, v15, v0}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v10

    .line 582
    invoke-virtual {v10}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v15

    const-string v16, "\\s+"

    const-string v17, " "

    invoke-virtual/range {v15 .. v17}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 583
    const-string v15, " "

    invoke-virtual {v10, v15}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v13

    .line 584
    .local v13, "temp":[Ljava/lang/String;
    new-instance v15, Ljava/lang/StringBuilder;

    const/16 v16, 0x1

    aget-object v16, v13, v16

    invoke-static/range {v16 .. v16}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v16

    invoke-direct/range {v15 .. v16}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v16, " "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const/16 v16, 0x0

    aget-object v16, v13, v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    .line 586
    goto/16 :goto_0

    .end local v13    # "temp":[Ljava/lang/String;
    :cond_4
    const-string v15, "english_date_time_1://"

    invoke-virtual {v10, v15}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v15

    if-eqz v15, :cond_8

    .line 588
    const/4 v15, 0x1

    invoke-static {v15}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    .line 590
    const-string v9, ""

    .line 591
    .local v9, "month":Ljava/lang/String;
    const-string v5, ""

    .line 592
    .local v5, "day":Ljava/lang/String;
    const-string v14, ""

    .line 594
    .local v14, "year":Ljava/lang/String;
    const-string v15, "english_date_time_1://"

    invoke-virtual {v15}, Ljava/lang/String;->length()I

    move-result v15

    invoke-virtual {v10, v15}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v10

    .line 595
    const-string v15, " "

    invoke-virtual {v10, v15}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v13

    .line 596
    .restart local v13    # "temp":[Ljava/lang/String;
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_1
    const/4 v15, 0x2

    if-lt v7, v15, :cond_6

    .line 606
    :cond_5
    :goto_2
    const/4 v15, 0x2

    aget-object v14, v13, v15

    .line 608
    new-instance v15, Ljava/lang/StringBuilder;

    invoke-static {v14}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v16

    invoke-direct/range {v15 .. v16}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v16, "/"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, "/"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, " 00:00"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    .line 610
    goto/16 :goto_0

    .line 597
    :cond_6
    aget-object v15, v13, v7

    invoke-static {v15}, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify;->getMonth(Ljava/lang/String;)I

    move-result v15

    if-lez v15, :cond_7

    .line 598
    aget-object v15, v13, v7

    invoke-static {v15}, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify;->getMonth(Ljava/lang/String;)I

    move-result v15

    invoke-static {v15}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v9

    .line 599
    add-int/lit8 v15, v7, -0x1

    invoke-static {v15}, Ljava/lang/Math;->abs(I)I

    move-result v15

    aget-object v5, v13, v15

    .line 600
    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v15

    const/16 v16, 0x3

    move/from16 v0, v16

    if-lt v15, v0, :cond_5

    .line 601
    const/4 v15, 0x0

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v16

    add-int/lit8 v16, v16, -0x2

    move/from16 v0, v16

    invoke-virtual {v5, v15, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    .line 603
    goto :goto_2

    .line 596
    :cond_7
    add-int/lit8 v7, v7, 0x1

    goto :goto_1

    .line 610
    .end local v5    # "day":Ljava/lang/String;
    .end local v7    # "i":I
    .end local v9    # "month":Ljava/lang/String;
    .end local v13    # "temp":[Ljava/lang/String;
    .end local v14    # "year":Ljava/lang/String;
    :cond_8
    const-string v15, "english_date_time_2://"

    invoke-virtual {v10, v15}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v15

    if-eqz v15, :cond_9

    .line 612
    invoke-static {v10}, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify;->englishDateTime2Scheme(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 614
    goto/16 :goto_0

    :cond_9
    const-string v15, "english_date_time_3://"

    invoke-virtual {v10, v15}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v15

    if-eqz v15, :cond_a

    .line 616
    invoke-static {v10}, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify;->englishDateTime3Scheme(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 618
    goto/16 :goto_0

    :cond_a
    const-string v15, "english_date_time_4://"

    invoke-virtual {v10, v15}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v15

    if-eqz v15, :cond_f

    .line 620
    const-string v15, "english_date_time_4://"

    invoke-virtual {v15}, Ljava/lang/String;->length()I

    move-result v15

    invoke-virtual {v10, v15}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v10

    .line 621
    const-string v15, " "

    invoke-virtual {v10, v15}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v13

    .line 622
    .restart local v13    # "temp":[Ljava/lang/String;
    sget-object v15, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v10, v15}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v15

    const-string v16, "tomorrow"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v15

    if-eqz v15, :cond_b

    .line 623
    sget-wide v16, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify;->mModifiedTime:J

    const-wide/32 v18, 0x5265c00

    add-long v16, v16, v18

    sput-wide v16, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify;->mModifiedTime:J

    .line 626
    :cond_b
    array-length v15, v13

    const/16 v16, 0x1

    move/from16 v0, v16

    if-ne v15, v0, :cond_d

    .line 627
    sget-object v15, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v10, v15}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v15

    const-string v16, "tonight"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v15

    if-eqz v15, :cond_c

    .line 628
    new-instance v15, Ljava/lang/StringBuilder;

    sget-wide v16, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify;->mModifiedTime:J

    invoke-static/range {v16 .. v17}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v4, v0}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v16 .. v16}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v16

    invoke-direct/range {v15 .. v16}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v16, " 8pm"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    .line 629
    goto/16 :goto_0

    .line 630
    :cond_c
    new-instance v15, Ljava/lang/StringBuilder;

    sget-wide v16, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify;->mModifiedTime:J

    invoke-static/range {v16 .. v17}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v4, v0}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v16 .. v16}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v16

    invoke-direct/range {v15 .. v16}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v16, " 00:00"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    .line 631
    const/4 v15, 0x1

    invoke-static {v15}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    .line 634
    goto/16 :goto_0

    .line 635
    :cond_d
    sget-wide v16, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify;->mModifiedTime:J

    invoke-static/range {v16 .. v17}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v15

    invoke-virtual {v4, v15}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    .line 636
    new-instance v11, Ljava/lang/StringBuffer;

    invoke-direct {v11, v10}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 637
    .restart local v11    # "sbTime":Ljava/lang/StringBuffer;
    const/4 v7, 0x1

    .restart local v7    # "i":I
    :goto_3
    array-length v15, v13

    if-lt v7, v15, :cond_e

    .line 641
    invoke-virtual {v11}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v10

    .line 644
    goto/16 :goto_0

    .line 638
    :cond_e
    const/16 v15, 0x20

    invoke-virtual {v11, v15}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    move-result-object v11

    .line 639
    aget-object v15, v13, v7

    invoke-virtual {v11, v15}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v11

    .line 637
    add-int/lit8 v7, v7, 0x1

    goto :goto_3

    .line 644
    .end local v7    # "i":I
    .end local v11    # "sbTime":Ljava/lang/StringBuffer;
    .end local v13    # "temp":[Ljava/lang/String;
    :cond_f
    const-string v15, "english_date_time_5://"

    invoke-virtual {v10, v15}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v15

    if-eqz v15, :cond_10

    .line 645
    invoke-static {v10, v4, v6}, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify;->englishDateTime5Scheme(Ljava/lang/String;Ljava/text/SimpleDateFormat;Ljava/text/SimpleDateFormat;)Ljava/lang/String;

    move-result-object v10

    .line 647
    goto/16 :goto_0

    :cond_10
    const-string v15, "korean_date_time_1://"

    invoke-virtual {v10, v15}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v15

    if-eqz v15, :cond_11

    .line 649
    const/4 v15, 0x1

    invoke-static {v15}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    .line 651
    const-string v15, "korean_date_time_1://"

    invoke-virtual {v15}, Ljava/lang/String;->length()I

    move-result v15

    invoke-virtual {v10, v15}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v10

    .line 653
    const-string v15, "\ub144 "

    const-string v16, "/"

    move-object/from16 v0, v16

    invoke-virtual {v10, v15, v0}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v10

    .line 654
    const-string v15, "\uc6d4 "

    const-string v16, "/"

    move-object/from16 v0, v16

    invoke-virtual {v10, v15, v0}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v10

    .line 655
    const-string v15, "\uc77c"

    const-string v16, ""

    move-object/from16 v0, v16

    invoke-virtual {v10, v15, v0}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v10

    .line 656
    new-instance v15, Ljava/lang/StringBuilder;

    invoke-static {v10}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v16

    invoke-direct/range {v15 .. v16}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v16, " 00:00"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    .line 658
    goto/16 :goto_0

    :cond_11
    const-string v15, "korean_date_time_2://"

    invoke-virtual {v10, v15}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v15

    if-eqz v15, :cond_12

    .line 660
    const-string v15, "korean_date_time_2://"

    invoke-virtual {v15}, Ljava/lang/String;->length()I

    move-result v15

    invoke-virtual {v10, v15}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v10

    .line 661
    const-string v15, "\ub144 "

    const-string v16, "/"

    move-object/from16 v0, v16

    invoke-virtual {v10, v15, v0}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v10

    .line 662
    const-string v15, "\uc6d4 "

    const-string v16, "/"

    move-object/from16 v0, v16

    invoke-virtual {v10, v15, v0}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v10

    .line 663
    const-string v15, "\uc77c "

    const-string v16, " "

    move-object/from16 v0, v16

    invoke-virtual {v10, v15, v0}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v10

    .line 664
    const-string v15, "\uc2dc"

    const-string v16, ":"

    move-object/from16 v0, v16

    invoke-virtual {v10, v15, v0}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v10

    .line 665
    const-string v15, "\ubd84"

    const-string v16, ""

    move-object/from16 v0, v16

    invoke-virtual {v10, v15, v0}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v10

    .line 667
    goto/16 :goto_0

    :cond_12
    const-string v15, "korean_date_time_3://"

    invoke-virtual {v10, v15}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v15

    if-eqz v15, :cond_13

    .line 669
    const-string v15, "korean_date_time_3://"

    invoke-virtual {v15}, Ljava/lang/String;->length()I

    move-result v15

    invoke-virtual {v10, v15}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v10

    .line 670
    const-string v15, "\ub144 "

    const-string v16, "/"

    move-object/from16 v0, v16

    invoke-virtual {v10, v15, v0}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v10

    .line 671
    const-string v15, "\uc6d4 "

    const-string v16, "/"

    move-object/from16 v0, v16

    invoke-virtual {v10, v15, v0}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v10

    .line 672
    const-string v15, "\uc77c"

    const-string v16, " "

    move-object/from16 v0, v16

    invoke-virtual {v10, v15, v0}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v10

    .line 673
    const-string v15, "\uc2dc"

    const-string v16, ":"

    move-object/from16 v0, v16

    invoke-virtual {v10, v15, v0}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v10

    .line 674
    const-string v15, "\ubd84"

    const-string v16, ""

    move-object/from16 v0, v16

    invoke-virtual {v10, v15, v0}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v10

    .line 675
    const/16 v15, 0x2f

    invoke-virtual {v10, v15}, Ljava/lang/String;->indexOf(I)I

    move-result v15

    add-int/lit8 v12, v15, -0x4

    .line 676
    .local v12, "sepIndex":I
    new-instance v15, Ljava/lang/StringBuilder;

    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v16

    move/from16 v0, v16

    invoke-virtual {v10, v12, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v16 .. v16}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v16

    invoke-direct/range {v15 .. v16}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v16, " "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    .line 677
    const/16 v16, 0x0

    add-int/lit8 v17, v12, -0x1

    move/from16 v0, v16

    move/from16 v1, v17

    invoke-virtual {v10, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    .line 676
    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    .line 679
    goto/16 :goto_0

    .end local v12    # "sepIndex":I
    :cond_13
    const-string v15, "korean_date_time_4://"

    invoke-virtual {v10, v15}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v15

    if-eqz v15, :cond_16

    .line 681
    const-string v15, "korean_date_time_4://"

    invoke-virtual {v15}, Ljava/lang/String;->length()I

    move-result v15

    invoke-virtual {v10, v15}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v10

    .line 682
    const-string v15, "\uc2dc "

    const-string v16, ":"

    move-object/from16 v0, v16

    invoke-virtual {v10, v15, v0}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v10

    .line 683
    const-string v15, "\ubd84"

    const-string v16, ""

    move-object/from16 v0, v16

    invoke-virtual {v10, v15, v0}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v10

    .line 684
    const-string v15, "\uc2dc"

    const-string v16, ":"

    move-object/from16 v0, v16

    invoke-virtual {v10, v15, v0}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v10

    .line 686
    const-string v15, " "

    invoke-virtual {v10, v15}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v13

    .line 687
    .restart local v13    # "temp":[Ljava/lang/String;
    const-string v15, "\u00b3\u00bb\u00c0\u00cf"

    invoke-virtual {v10, v15}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v15

    if-eqz v15, :cond_14

    .line 688
    sget-wide v16, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify;->mModifiedTime:J

    const-wide/32 v18, 0x5265c00

    add-long v16, v16, v18

    sput-wide v16, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify;->mModifiedTime:J

    .line 691
    :cond_14
    array-length v15, v13

    const/16 v16, 0x1

    move/from16 v0, v16

    if-ne v15, v0, :cond_15

    .line 693
    new-instance v15, Ljava/lang/StringBuilder;

    sget-wide v16, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify;->mModifiedTime:J

    invoke-static/range {v16 .. v17}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v4, v0}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v16 .. v16}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v16

    invoke-direct/range {v15 .. v16}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v16, " 00:00"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    .line 694
    const/4 v15, 0x1

    invoke-static {v15}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    .line 696
    goto/16 :goto_0

    .line 697
    :cond_15
    sget-wide v16, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify;->mModifiedTime:J

    invoke-static/range {v16 .. v17}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v15

    invoke-virtual {v4, v15}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    .line 698
    const/4 v7, 0x1

    .restart local v7    # "i":I
    :goto_4
    array-length v15, v13

    if-ge v7, v15, :cond_0

    .line 699
    new-instance v15, Ljava/lang/StringBuilder;

    invoke-static {v10}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v16

    invoke-direct/range {v15 .. v16}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v16, " "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    aget-object v16, v13, v7

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    .line 698
    add-int/lit8 v7, v7, 0x1

    goto :goto_4

    .line 703
    .end local v7    # "i":I
    .end local v13    # "temp":[Ljava/lang/String;
    :cond_16
    const-string v15, "korean_date_time_5://"

    invoke-virtual {v10, v15}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v15

    if-eqz v15, :cond_17

    .line 705
    invoke-static {v10, v4, v6}, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify;->koreanDateTime5Scheme(Ljava/lang/String;Ljava/text/SimpleDateFormat;Ljava/text/SimpleDateFormat;)Ljava/lang/String;

    move-result-object v10

    .line 707
    goto/16 :goto_0

    :cond_17
    const-string v15, "korean_date_time_6://"

    invoke-virtual {v10, v15}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v15

    if-eqz v15, :cond_18

    .line 709
    const-string v15, "korean_date_time_6://"

    invoke-virtual {v15}, Ljava/lang/String;->length()I

    move-result v15

    invoke-virtual {v10, v15}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v10

    .line 710
    const-string v15, "\uc2dc "

    const-string v16, ":"

    move-object/from16 v0, v16

    invoke-virtual {v10, v15, v0}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v10

    .line 711
    const-string v15, "\ubd84"

    const-string v16, ""

    move-object/from16 v0, v16

    invoke-virtual {v10, v15, v0}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v10

    .line 712
    const-string v15, "\uc2dc"

    const-string v16, ":"

    move-object/from16 v0, v16

    invoke-virtual {v10, v15, v0}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v10

    .line 713
    new-instance v15, Ljava/lang/StringBuilder;

    sget-wide v16, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify;->mModifiedTime:J

    invoke-static/range {v16 .. v17}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v4, v0}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v16 .. v16}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v16

    invoke-direct/range {v15 .. v16}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v16, " "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    .line 715
    goto/16 :goto_0

    :cond_18
    const-string v15, "western_date_time_1://"

    invoke-virtual {v10, v15}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v15

    if-eqz v15, :cond_1c

    .line 717
    const/4 v15, 0x1

    invoke-static {v15}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    .line 719
    const-string v9, ""

    .line 720
    .restart local v9    # "month":Ljava/lang/String;
    const-string v5, ""

    .line 721
    .restart local v5    # "day":Ljava/lang/String;
    const-string v14, ""

    .line 723
    .restart local v14    # "year":Ljava/lang/String;
    const-string v15, "western_date_time_1://"

    invoke-virtual {v15}, Ljava/lang/String;->length()I

    move-result v15

    invoke-virtual {v10, v15}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v10

    .line 724
    const-string v15, " "

    invoke-virtual {v10, v15}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v13

    .line 725
    .restart local v13    # "temp":[Ljava/lang/String;
    const/4 v7, 0x0

    .restart local v7    # "i":I
    :goto_5
    const/4 v15, 0x2

    if-lt v7, v15, :cond_1a

    .line 735
    :cond_19
    :goto_6
    const/4 v15, 0x2

    aget-object v14, v13, v15

    .line 737
    new-instance v15, Ljava/lang/StringBuilder;

    invoke-static {v14}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v16

    invoke-direct/range {v15 .. v16}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v16, "/"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, "/"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, " 00:00"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    .line 739
    goto/16 :goto_0

    .line 726
    :cond_1a
    aget-object v15, v13, v7

    invoke-static {v15}, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$ResourceString;->getMonth(Ljava/lang/String;)I

    move-result v15

    if-lez v15, :cond_1b

    .line 727
    aget-object v15, v13, v7

    invoke-static {v15}, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$ResourceString;->getMonth(Ljava/lang/String;)I

    move-result v15

    invoke-static {v15}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v9

    .line 728
    add-int/lit8 v15, v7, -0x1

    invoke-static {v15}, Ljava/lang/Math;->abs(I)I

    move-result v15

    aget-object v5, v13, v15

    .line 729
    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v15

    const/16 v16, 0x3

    move/from16 v0, v16

    if-lt v15, v0, :cond_19

    .line 730
    const/4 v15, 0x0

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v16

    add-int/lit8 v16, v16, -0x2

    move/from16 v0, v16

    invoke-virtual {v5, v15, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    .line 732
    goto :goto_6

    .line 725
    :cond_1b
    add-int/lit8 v7, v7, 0x1

    goto :goto_5

    .line 739
    .end local v5    # "day":Ljava/lang/String;
    .end local v7    # "i":I
    .end local v9    # "month":Ljava/lang/String;
    .end local v13    # "temp":[Ljava/lang/String;
    .end local v14    # "year":Ljava/lang/String;
    :cond_1c
    const-string v15, "western_date_time_2://"

    invoke-virtual {v10, v15}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v15

    if-eqz v15, :cond_1d

    .line 741
    invoke-static {v10}, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify;->westernDateTime2Scheme(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 743
    goto/16 :goto_0

    :cond_1d
    const-string v15, "western_date_time_3://"

    invoke-virtual {v10, v15}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v15

    if-eqz v15, :cond_1e

    .line 745
    invoke-static {v10}, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify;->westernDateTime3Scheme(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 747
    goto/16 :goto_0

    :cond_1e
    const-string v15, "western_date_time_4://"

    invoke-virtual {v10, v15}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v15

    if-eqz v15, :cond_23

    .line 749
    const-string v15, "western_date_time_4://"

    invoke-virtual {v15}, Ljava/lang/String;->length()I

    move-result v15

    invoke-virtual {v10, v15}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v10

    .line 750
    const-string v15, " "

    invoke-virtual {v10, v15}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v13

    .line 751
    .restart local v13    # "temp":[Ljava/lang/String;
    sget-object v15, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v10, v15}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v15

    sget-object v16, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$ResourceString;->mTomorrow:Ljava/lang/String;

    sget-object v17, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v15

    if-eqz v15, :cond_1f

    .line 752
    sget-wide v16, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify;->mModifiedTime:J

    const-wide/32 v18, 0x5265c00

    add-long v16, v16, v18

    sput-wide v16, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify;->mModifiedTime:J

    .line 755
    :cond_1f
    array-length v15, v13

    const/16 v16, 0x1

    move/from16 v0, v16

    if-ne v15, v0, :cond_21

    .line 756
    sget-object v15, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v10, v15}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v15

    sget-object v16, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$ResourceString;->mTonight:Ljava/lang/String;

    sget-object v17, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v15

    if-eqz v15, :cond_20

    .line 757
    new-instance v15, Ljava/lang/StringBuilder;

    sget-wide v16, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify;->mModifiedTime:J

    invoke-static/range {v16 .. v17}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v4, v0}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v16 .. v16}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v16

    invoke-direct/range {v15 .. v16}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v16, " 8pm"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    .line 758
    goto/16 :goto_0

    .line 759
    :cond_20
    new-instance v15, Ljava/lang/StringBuilder;

    sget-wide v16, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify;->mModifiedTime:J

    invoke-static/range {v16 .. v17}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v4, v0}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v16 .. v16}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v16

    invoke-direct/range {v15 .. v16}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v16, " 00:00"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    .line 760
    const/4 v15, 0x1

    invoke-static {v15}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    .line 763
    goto/16 :goto_0

    .line 764
    :cond_21
    sget-wide v16, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify;->mModifiedTime:J

    invoke-static/range {v16 .. v17}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v15

    invoke-virtual {v4, v15}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    .line 765
    new-instance v11, Ljava/lang/StringBuffer;

    invoke-direct {v11, v10}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 766
    .restart local v11    # "sbTime":Ljava/lang/StringBuffer;
    const/4 v7, 0x1

    .restart local v7    # "i":I
    :goto_7
    array-length v15, v13

    if-lt v7, v15, :cond_22

    .line 770
    invoke-virtual {v11}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v10

    .line 773
    goto/16 :goto_0

    .line 767
    :cond_22
    const/16 v15, 0x20

    invoke-virtual {v11, v15}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    move-result-object v11

    .line 768
    aget-object v15, v13, v7

    invoke-virtual {v11, v15}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v11

    .line 766
    add-int/lit8 v7, v7, 0x1

    goto :goto_7

    .line 773
    .end local v7    # "i":I
    .end local v11    # "sbTime":Ljava/lang/StringBuffer;
    .end local v13    # "temp":[Ljava/lang/String;
    :cond_23
    const-string v15, "western_date_time_5://"

    invoke-virtual {v10, v15}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v15

    if-eqz v15, :cond_0

    .line 774
    invoke-static {v10, v4, v6}, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify;->westernDateTime5Scheme(Ljava/lang/String;Ljava/text/SimpleDateFormat;Ljava/text/SimpleDateFormat;)Ljava/lang/String;

    move-result-object v10

    goto/16 :goto_0
.end method

.method private static englishDateTime2Scheme(Ljava/lang/String;)Ljava/lang/String;
    .locals 9
    .param p0, "sTime"    # Ljava/lang/String;

    .prologue
    const/4 v8, 0x2

    .line 817
    const-string v2, ""

    .line 818
    .local v2, "month":Ljava/lang/String;
    const-string v0, ""

    .line 819
    .local v0, "day":Ljava/lang/String;
    const-string v5, ""

    .line 820
    .local v5, "year":Ljava/lang/String;
    const-string v6, "english_date_time_2://"

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    invoke-virtual {p0, v6}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p0

    .line 821
    const-string v6, " "

    invoke-virtual {p0, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .line 822
    .local v4, "temp":[Ljava/lang/String;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-lt v1, v8, :cond_1

    .line 832
    :cond_0
    :goto_1
    aget-object v5, v4, v8

    .line 834
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v7, "/"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "/"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    .line 835
    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3, p0}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 836
    .local v3, "sbTime":Ljava/lang/StringBuffer;
    const/4 v1, 0x3

    :goto_2
    array-length v6, v4

    if-lt v1, v6, :cond_3

    .line 840
    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object p0

    .line 841
    return-object p0

    .line 823
    .end local v3    # "sbTime":Ljava/lang/StringBuffer;
    :cond_1
    aget-object v6, v4, v1

    invoke-static {v6}, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify;->getMonth(Ljava/lang/String;)I

    move-result v6

    if-lez v6, :cond_2

    .line 824
    aget-object v6, v4, v1

    invoke-static {v6}, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify;->getMonth(Ljava/lang/String;)I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    .line 825
    add-int/lit8 v6, v1, -0x1

    invoke-static {v6}, Ljava/lang/Math;->abs(I)I

    move-result v6

    aget-object v0, v4, v6

    .line 826
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v6

    const/4 v7, 0x3

    if-lt v6, v7, :cond_0

    .line 827
    const/4 v6, 0x0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v7

    add-int/lit8 v7, v7, -0x2

    invoke-virtual {v0, v6, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 829
    goto :goto_1

    .line 822
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 837
    .restart local v3    # "sbTime":Ljava/lang/StringBuffer;
    :cond_3
    const/16 v6, 0x20

    invoke-virtual {v3, v6}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    move-result-object v3

    .line 838
    aget-object v6, v4, v1

    invoke-virtual {v3, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    .line 836
    add-int/lit8 v1, v1, 0x1

    goto :goto_2
.end method

.method private static englishDateTime3Scheme(Ljava/lang/String;)Ljava/lang/String;
    .locals 8
    .param p0, "sTime"    # Ljava/lang/String;

    .prologue
    .line 876
    const-string v2, ""

    .line 877
    .local v2, "month":Ljava/lang/String;
    const-string v0, ""

    .line 878
    .local v0, "day":Ljava/lang/String;
    const-string v5, ""

    .line 880
    .local v5, "year":Ljava/lang/String;
    const-string v6, "english_date_time_3://"

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    invoke-virtual {p0, v6}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p0

    .line 881
    const-string v6, " "

    invoke-virtual {p0, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .line 882
    .local v4, "temp":[Ljava/lang/String;
    array-length v6, v4

    add-int/lit8 v1, v6, -0x2

    .local v1, "i":I
    :goto_0
    if-gtz v1, :cond_1

    .line 892
    :cond_0
    :goto_1
    array-length v6, v4

    add-int/lit8 v6, v6, -0x1

    aget-object v5, v4, v6

    .line 894
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v7, "/"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "/"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    .line 895
    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3, p0}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 896
    .local v3, "sbTime":Ljava/lang/StringBuffer;
    const/4 v1, 0x0

    :goto_2
    array-length v6, v4

    add-int/lit8 v6, v6, -0x3

    if-lt v1, v6, :cond_3

    .line 900
    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object p0

    .line 901
    return-object p0

    .line 883
    .end local v3    # "sbTime":Ljava/lang/StringBuffer;
    :cond_1
    aget-object v6, v4, v1

    invoke-static {v6}, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify;->getMonth(Ljava/lang/String;)I

    move-result v6

    if-lez v6, :cond_2

    .line 884
    aget-object v6, v4, v1

    invoke-static {v6}, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify;->getMonth(Ljava/lang/String;)I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    .line 885
    array-length v6, v4

    sub-int v6, v1, v6

    invoke-static {v6}, Ljava/lang/Math;->abs(I)I

    move-result v6

    aget-object v0, v4, v6

    .line 886
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v6

    const/4 v7, 0x3

    if-lt v6, v7, :cond_0

    .line 887
    const/4 v6, 0x0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v7

    add-int/lit8 v7, v7, -0x2

    invoke-virtual {v0, v6, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 889
    goto :goto_1

    .line 882
    :cond_2
    add-int/lit8 v1, v1, -0x1

    goto :goto_0

    .line 897
    .restart local v3    # "sbTime":Ljava/lang/StringBuffer;
    :cond_3
    const/16 v6, 0x20

    invoke-virtual {v3, v6}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    move-result-object v3

    .line 898
    aget-object v6, v4, v1

    invoke-virtual {v3, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    .line 896
    add-int/lit8 v1, v1, 0x1

    goto :goto_2
.end method

.method private static englishDateTime5Scheme(Ljava/lang/String;Ljava/text/SimpleDateFormat;Ljava/text/SimpleDateFormat;)Ljava/lang/String;
    .locals 12
    .param p0, "sTime"    # Ljava/lang/String;
    .param p1, "dateFormat"    # Ljava/text/SimpleDateFormat;
    .param p2, "dayFormat"    # Ljava/text/SimpleDateFormat;

    .prologue
    .line 1021
    const/4 v0, -0x1

    .line 1022
    .local v0, "curDayIndex":I
    const/4 v3, -0x1

    .line 1024
    .local v3, "newDayIndex":I
    sget-wide v8, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify;->mModifiedTime:J

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {p2, v7}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 1026
    .local v4, "sCurDay":Ljava/lang/String;
    const-string v7, "english_date_time_5://"

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    invoke-virtual {p0, v7}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p0

    .line 1027
    const-string v7, " "

    invoke-virtual {p0, v7}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v6

    .line 1029
    .local v6, "temp":[Ljava/lang/String;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    sget-object v7, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify;->mArrDayOfWeek:[Ljava/lang/String;

    array-length v7, v7

    if-lt v2, v7, :cond_1

    .line 1039
    sub-int v1, v3, v0

    .line 1040
    .local v1, "delta":I
    if-lez v1, :cond_4

    .line 1041
    sget-wide v8, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify;->mModifiedTime:J

    const v7, 0x5265c00

    mul-int/2addr v7, v1

    int-to-long v10, v7

    add-long/2addr v8, v10

    sput-wide v8, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify;->mModifiedTime:J

    .line 1046
    :cond_0
    :goto_1
    sget-wide v8, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify;->mModifiedTime:J

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {p1, v7}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p0

    .line 1048
    new-instance v5, Ljava/lang/StringBuffer;

    invoke-direct {v5, p0}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 1049
    .local v5, "sbTime":Ljava/lang/StringBuffer;
    const/4 v7, 0x1

    aget-object v7, v6, v7

    const-string v8, "morning"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 1050
    const/16 v7, 0x20

    invoke-virtual {v5, v7}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    move-result-object v5

    .line 1051
    const/4 v7, 0x2

    aget-object v7, v6, v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    .line 1052
    invoke-virtual {v5}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object p0

    .line 1066
    :goto_2
    return-object p0

    .line 1030
    .end local v1    # "delta":I
    .end local v5    # "sbTime":Ljava/lang/StringBuffer;
    :cond_1
    const/4 v7, 0x0

    aget-object v7, v6, v7

    sget-object v8, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v7, v8}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v7

    sget-object v8, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify;->mArrDayOfWeek:[Ljava/lang/String;

    aget-object v8, v8, v2

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 1031
    move v3, v2

    .line 1034
    :cond_2
    sget-object v7, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify;->mArrDayOfWeek:[Ljava/lang/String;

    aget-object v7, v7, v2

    sget-object v8, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v4, v8}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 1035
    move v0, v2

    .line 1029
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1042
    .restart local v1    # "delta":I
    :cond_4
    if-gez v1, :cond_0

    .line 1043
    sget-wide v8, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify;->mModifiedTime:J

    const v7, 0x5265c00

    add-int/lit8 v10, v1, 0x7

    mul-int/2addr v7, v10

    int-to-long v10, v7

    add-long/2addr v8, v10

    sput-wide v8, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify;->mModifiedTime:J

    goto :goto_1

    .line 1053
    .restart local v5    # "sbTime":Ljava/lang/StringBuffer;
    :cond_5
    const/4 v7, 0x1

    aget-object v7, v6, v7

    const-string v8, "afternoon"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_6

    const/4 v7, 0x1

    aget-object v7, v6, v7

    const-string v8, "evening"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_6

    .line 1054
    const/4 v7, 0x1

    aget-object v7, v6, v7

    const-string v8, "night"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_7

    .line 1055
    :cond_6
    const/16 v7, 0x20

    invoke-virtual {v5, v7}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    move-result-object v5

    .line 1056
    const/4 v7, 0x2

    aget-object v7, v6, v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    .line 1057
    const-string v7, "pm"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    .line 1058
    invoke-virtual {v5}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object p0

    .line 1059
    goto :goto_2

    .line 1060
    :cond_7
    const/4 v2, 0x1

    :goto_3
    array-length v7, v6

    if-lt v2, v7, :cond_8

    .line 1064
    invoke-virtual {v5}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object p0

    goto :goto_2

    .line 1061
    :cond_8
    const/16 v7, 0x20

    invoke-virtual {v5, v7}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    move-result-object v5

    .line 1062
    aget-object v7, v6, v2

    invoke-virtual {v5, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    .line 1060
    add-int/lit8 v2, v2, 0x1

    goto :goto_3
.end method

.method private static final gatherLinks(Ljava/util/ArrayList;Landroid/text/Spannable;Ljava/util/regex/Pattern;[Ljava/lang/String;)V
    .locals 6
    .param p1, "s"    # Landroid/text/Spannable;
    .param p2, "pattern"    # Ljava/util/regex/Pattern;
    .param p3, "schemes"    # [Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/app/memo/uiwidget/LinkSpec;",
            ">;",
            "Landroid/text/Spannable;",
            "Ljava/util/regex/Pattern;",
            "[",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1211
    .local p0, "links":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/app/memo/uiwidget/LinkSpec;>;"
    invoke-virtual {p2, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v1

    .line 1213
    .local v1, "m":Ljava/util/regex/Matcher;
    :goto_0
    invoke-virtual {v1}, Ljava/util/regex/Matcher;->find()Z

    move-result v5

    if-nez v5, :cond_0

    .line 1226
    return-void

    .line 1214
    :cond_0
    invoke-virtual {v1}, Ljava/util/regex/Matcher;->start()I

    move-result v3

    .line 1215
    .local v3, "start":I
    invoke-virtual {v1}, Ljava/util/regex/Matcher;->end()I

    move-result v0

    .line 1217
    .local v0, "end":I
    new-instance v2, Lcom/samsung/android/app/memo/uiwidget/LinkSpec;

    invoke-direct {v2}, Lcom/samsung/android/app/memo/uiwidget/LinkSpec;-><init>()V

    .line 1218
    .local v2, "spec":Lcom/samsung/android/app/memo/uiwidget/LinkSpec;
    const/4 v5, 0x0

    invoke-virtual {v1, v5}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5, p3, v1}, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify;->makeUrl(Ljava/lang/String;[Ljava/lang/String;Ljava/util/regex/Matcher;)Ljava/lang/String;

    move-result-object v4

    .line 1220
    .local v4, "url":Ljava/lang/String;
    iput-object v4, v2, Lcom/samsung/android/app/memo/uiwidget/LinkSpec;->mUrl:Ljava/lang/String;

    .line 1221
    iput v3, v2, Lcom/samsung/android/app/memo/uiwidget/LinkSpec;->mStart:I

    .line 1222
    iput v0, v2, Lcom/samsung/android/app/memo/uiwidget/LinkSpec;->mEnd:I

    .line 1224
    invoke-virtual {p0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method private static getDateFormat(Ljava/lang/String;)Ljava/text/DateFormat;
    .locals 1
    .param p0, "pattern"    # Ljava/lang/String;

    .prologue
    .line 1070
    new-instance v0, Ljava/text/SimpleDateFormat;

    invoke-direct {v0, p0}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method static getEnglishWeekDay(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "weekday"    # Ljava/lang/String;

    .prologue
    .line 1331
    const-string v0, "\u00c0\u00cf\u00bf\u00e4\u00c0\u00cf"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1332
    sget-object v0, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify;->mArrDayOfWeek:[Ljava/lang/String;

    const/4 v1, 0x0

    aget-object p0, v0, v1

    .line 1347
    .end local p0    # "weekday":Ljava/lang/String;
    :cond_0
    :goto_0
    return-object p0

    .line 1333
    .restart local p0    # "weekday":Ljava/lang/String;
    :cond_1
    const-string v0, "\u00bf\u00f9\u00bf\u00e4\u00c0\u00cf"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1334
    sget-object v0, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify;->mArrDayOfWeek:[Ljava/lang/String;

    const/4 v1, 0x1

    aget-object p0, v0, v1

    goto :goto_0

    .line 1335
    :cond_2
    const-string v0, "\u00c8\u00ad\u00bf\u00e4\u00c0\u00cf"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1336
    sget-object v0, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify;->mArrDayOfWeek:[Ljava/lang/String;

    const/4 v1, 0x2

    aget-object p0, v0, v1

    goto :goto_0

    .line 1337
    :cond_3
    const-string v0, "\u00bc\u00f6\u00bf\u00e4\u00c0\u00cf"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1338
    sget-object v0, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify;->mArrDayOfWeek:[Ljava/lang/String;

    const/4 v1, 0x3

    aget-object p0, v0, v1

    goto :goto_0

    .line 1339
    :cond_4
    const-string v0, "\u00b8\u00f1\u00bf\u00e4\u00c0\u00cf"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1340
    sget-object v0, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify;->mArrDayOfWeek:[Ljava/lang/String;

    const/4 v1, 0x4

    aget-object p0, v0, v1

    goto :goto_0

    .line 1341
    :cond_5
    const-string v0, "\u00b1\u00dd\u00bf\u00e4\u00c0\u00cf"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1342
    sget-object v0, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify;->mArrDayOfWeek:[Ljava/lang/String;

    const/4 v1, 0x5

    aget-object p0, v0, v1

    goto :goto_0

    .line 1343
    :cond_6
    const-string v0, "\u00c5\u00e4\u00bf\u00e4\u00c0\u00cf"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1344
    sget-object v0, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify;->mArrDayOfWeek:[Ljava/lang/String;

    const/4 v1, 0x6

    aget-object p0, v0, v1

    goto :goto_0
.end method

.method static getMonth(Ljava/lang/String;)I
    .locals 2
    .param p0, "sMonth"    # Ljava/lang/String;

    .prologue
    .line 1287
    const/4 v0, -0x1

    .line 1289
    .local v0, "month":I
    const-string v1, "January"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "Jan"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "Jan."

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1290
    const-string v1, "Jan,"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1291
    :cond_0
    const/4 v0, 0x1

    .line 1326
    :cond_1
    :goto_0
    return v0

    .line 1292
    :cond_2
    const-string v1, "February"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    const-string v1, "Feb"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    const-string v1, "Feb."

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 1293
    const-string v1, "Feb,"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 1294
    :cond_3
    const/4 v0, 0x2

    .line 1295
    goto :goto_0

    :cond_4
    const-string v1, "March"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    const-string v1, "Mar"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    const-string v1, "Mar."

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 1296
    const-string v1, "Mar,"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 1297
    :cond_5
    const/4 v0, 0x3

    .line 1298
    goto :goto_0

    :cond_6
    const-string v1, "April"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_7

    const-string v1, "Apr"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_7

    const-string v1, "Apr."

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_7

    .line 1299
    const-string v1, "Apr,"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 1300
    :cond_7
    const/4 v0, 0x4

    .line 1301
    goto :goto_0

    :cond_8
    const-string v1, "May"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 1302
    const/4 v0, 0x5

    .line 1303
    goto :goto_0

    :cond_9
    const-string v1, "June"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_a

    const-string v1, "Jun"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_a

    const-string v1, "Jun."

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_a

    .line 1304
    const-string v1, "Jun,"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_b

    .line 1305
    :cond_a
    const/4 v0, 0x6

    .line 1306
    goto/16 :goto_0

    :cond_b
    const-string v1, "July"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_c

    const-string v1, "Jul"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_c

    const-string v1, "Jul."

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_c

    .line 1307
    const-string v1, "Jul,"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_d

    .line 1308
    :cond_c
    const/4 v0, 0x7

    .line 1309
    goto/16 :goto_0

    :cond_d
    const-string v1, "August"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_e

    const-string v1, "Aug"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_e

    const-string v1, "Aug."

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_e

    .line 1310
    const-string v1, "Aug,"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_f

    .line 1311
    :cond_e
    const/16 v0, 0x8

    .line 1312
    goto/16 :goto_0

    :cond_f
    const-string v1, "September"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_10

    const-string v1, "Sep"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_10

    const-string v1, "Sep."

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_10

    .line 1313
    const-string v1, "Sep,"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_11

    .line 1314
    :cond_10
    const/16 v0, 0x9

    .line 1315
    goto/16 :goto_0

    :cond_11
    const-string v1, "October"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_12

    const-string v1, "Oct"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_12

    const-string v1, "Oct."

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_12

    .line 1316
    const-string v1, "Oct,"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_13

    .line 1317
    :cond_12
    const/16 v0, 0xa

    .line 1318
    goto/16 :goto_0

    :cond_13
    const-string v1, "November"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_14

    const-string v1, "Nov"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_14

    const-string v1, "Nov."

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_14

    .line 1319
    const-string v1, "Nov,"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_15

    .line 1320
    :cond_14
    const/16 v0, 0xb

    .line 1321
    goto/16 :goto_0

    :cond_15
    const-string v1, "December"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_16

    const-string v1, "Dec"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_16

    const-string v1, "Dec."

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_16

    .line 1322
    const-string v1, "Dec,"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1323
    :cond_16
    const/16 v0, 0xc

    goto/16 :goto_0
.end method

.method private static koreanDateTime5Scheme(Ljava/lang/String;Ljava/text/SimpleDateFormat;Ljava/text/SimpleDateFormat;)Ljava/lang/String;
    .locals 16
    .param p0, "sTime"    # Ljava/lang/String;
    .param p1, "dateFormat"    # Ljava/text/SimpleDateFormat;
    .param p2, "dayFormat"    # Ljava/text/SimpleDateFormat;

    .prologue
    .line 957
    const/4 v2, -0x1

    .line 958
    .local v2, "curDayIndex":I
    const/4 v5, -0x1

    .line 960
    .local v5, "newDayIndex":I
    sget-wide v12, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify;->mModifiedTime:J

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    move-object/from16 v0, p2

    invoke-virtual {v0, v11}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    .line 962
    .local v6, "sCurDay":Ljava/lang/String;
    const-string v11, "korean_date_time_5://"

    invoke-virtual {v11}, Ljava/lang/String;->length()I

    move-result v11

    move-object/from16 v0, p0

    invoke-virtual {v0, v11}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p0

    .line 964
    const-string v11, "\u00bd\u00c3 "

    const-string v12, ":"

    move-object/from16 v0, p0

    invoke-virtual {v0, v11, v12}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p0

    .line 965
    const-string v11, "\u00ba\u00d0"

    const-string v12, ""

    move-object/from16 v0, p0

    invoke-virtual {v0, v11, v12}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p0

    .line 966
    const-string v11, "\u00bd\u00c3"

    const-string v12, ""

    move-object/from16 v0, p0

    invoke-virtual {v0, v11, v12}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p0

    .line 968
    const-string v11, " "

    move-object/from16 v0, p0

    invoke-virtual {v0, v11}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    .line 970
    .local v9, "temp":[Ljava/lang/String;
    const/4 v11, 0x0

    aget-object v11, v9, v11

    invoke-static {v11}, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify;->getEnglishWeekDay(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 972
    .local v10, "weekday":Ljava/lang/String;
    const/4 v11, 0x0

    aput-object v10, v9, v11

    .line 973
    const-string v7, ""

    .line 974
    .local v7, "sTimeKeyword":Ljava/lang/String;
    const/4 v11, 0x0

    aget-object v11, v9, v11

    if-eq v11, v10, :cond_7

    .line 976
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    sget-object v11, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify;->mArrDayOfWeek:[Ljava/lang/String;

    array-length v11, v11

    if-lt v4, v11, :cond_3

    .line 986
    sub-int v3, v5, v2

    .line 987
    .local v3, "delta":I
    if-lez v3, :cond_6

    .line 988
    sget-wide v12, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify;->mModifiedTime:J

    const v11, 0x5265c00

    mul-int/2addr v11, v3

    int-to-long v14, v11

    add-long/2addr v12, v14

    sput-wide v12, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify;->mModifiedTime:J

    .line 992
    :cond_0
    :goto_1
    const/4 v11, 0x1

    aget-object v7, v9, v11

    .line 997
    .end local v3    # "delta":I
    .end local v4    # "i":I
    :goto_2
    sget-wide v12, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify;->mModifiedTime:J

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p0

    .line 999
    const-string v11, "\u00be\u00c6\u00c4\u00a7"

    invoke-virtual {v7, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-nez v11, :cond_1

    const-string v11, "\u00bb\u00f5\u00ba\u00ae"

    invoke-virtual {v7, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-nez v11, :cond_1

    const-string v11, "\u00bf\u00c0\u00c0\u00fc"

    invoke-virtual {v7, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_9

    .line 1000
    :cond_1
    new-instance v8, Ljava/lang/StringBuffer;

    move-object/from16 v0, p0

    invoke-direct {v8, v0}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 1001
    .local v8, "sbTime":Ljava/lang/StringBuffer;
    const/4 v4, 0x1

    .restart local v4    # "i":I
    :goto_3
    array-length v11, v9

    if-lt v4, v11, :cond_8

    .line 1005
    invoke-virtual {v8}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object p0

    .line 1015
    .end local v4    # "i":I
    .end local v8    # "sbTime":Ljava/lang/StringBuffer;
    :cond_2
    :goto_4
    return-object p0

    .line 977
    .restart local v4    # "i":I
    :cond_3
    const/4 v11, 0x0

    aget-object v11, v9, v11

    sget-object v12, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v11, v12}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v11

    sget-object v12, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify;->mArrDayOfWeek:[Ljava/lang/String;

    aget-object v12, v12, v4

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_4

    .line 978
    move v5, v4

    .line 981
    :cond_4
    sget-object v11, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify;->mArrDayOfWeek:[Ljava/lang/String;

    aget-object v11, v11, v4

    sget-object v12, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v6, v12}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_5

    .line 982
    move v2, v4

    .line 976
    :cond_5
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 989
    .restart local v3    # "delta":I
    :cond_6
    if-gez v3, :cond_0

    .line 990
    sget-wide v12, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify;->mModifiedTime:J

    const v11, 0x5265c00

    add-int/lit8 v14, v3, 0x7

    mul-int/2addr v11, v14

    int-to-long v14, v11

    add-long/2addr v12, v14

    sput-wide v12, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify;->mModifiedTime:J

    goto :goto_1

    .line 994
    .end local v3    # "delta":I
    .end local v4    # "i":I
    :cond_7
    const/4 v11, 0x0

    aget-object v7, v9, v11

    goto :goto_2

    .line 1002
    .restart local v4    # "i":I
    .restart local v8    # "sbTime":Ljava/lang/StringBuffer;
    :cond_8
    const/16 v11, 0x20

    invoke-virtual {v8, v11}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    move-result-object v8

    .line 1003
    aget-object v11, v9, v4

    invoke-virtual {v8, v11}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v8

    .line 1001
    add-int/lit8 v4, v4, 0x1

    goto :goto_3

    .line 1007
    .end local v4    # "i":I
    .end local v8    # "sbTime":Ljava/lang/StringBuffer;
    :cond_9
    const-string v11, "\u00bf\u00c0\u00c8\u00c4"

    invoke-virtual {v7, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-nez v11, :cond_a

    const-string v11, "\u00c0\u00fa\u00b3\u00e1"

    invoke-virtual {v7, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-nez v11, :cond_a

    .line 1008
    const-string v11, "\u00b9\u00e3"

    invoke-virtual {v7, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_2

    .line 1009
    :cond_a
    const/4 v4, 0x1

    .restart local v4    # "i":I
    :goto_5
    array-length v11, v9

    if-lt v4, v11, :cond_b

    .line 1013
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-static/range {p0 .. p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v12, " pm"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    goto :goto_4

    .line 1010
    :cond_b
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-static/range {p0 .. p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v12, " "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    aget-object v12, v9, v4

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    .line 1009
    add-int/lit8 v4, v4, 0x1

    goto :goto_5
.end method

.method private static final makeUrl(Ljava/lang/String;[Ljava/lang/String;Ljava/util/regex/Matcher;)Ljava/lang/String;
    .locals 9
    .param p0, "url"    # Ljava/lang/String;
    .param p1, "prefixes"    # [Ljava/lang/String;
    .param p2, "m"    # Ljava/util/regex/Matcher;

    .prologue
    const/4 v2, 0x0

    .line 1187
    const/4 v7, 0x0

    .line 1189
    .local v7, "hasPrefix":Z
    const/4 v8, 0x0

    .local v8, "i":I
    :goto_0
    array-length v0, p1

    if-lt v8, v0, :cond_2

    .line 1202
    :cond_0
    :goto_1
    if-nez v7, :cond_1

    .line 1203
    new-instance v0, Ljava/lang/StringBuilder;

    aget-object v1, p1, v2

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    .line 1206
    :cond_1
    return-object p0

    .line 1190
    :cond_2
    const/4 v1, 0x1

    aget-object v3, p1, v8

    aget-object v0, p1, v8

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v5

    move-object v0, p0

    move v4, v2

    invoke-virtual/range {v0 .. v5}, Ljava/lang/String;->regionMatches(ZILjava/lang/String;II)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1191
    const/4 v7, 0x1

    .line 1194
    aget-object v4, p1, v8

    aget-object v0, p1, v8

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v6

    move-object v1, p0

    move v3, v2

    move v5, v2

    invoke-virtual/range {v1 .. v6}, Ljava/lang/String;->regionMatches(ZILjava/lang/String;II)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1195
    new-instance v0, Ljava/lang/StringBuilder;

    aget-object v1, p1, v8

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    aget-object v1, p1, v8

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    invoke-virtual {p0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    .line 1198
    goto :goto_1

    .line 1189
    :cond_3
    add-int/lit8 v8, v8, 0x1

    goto :goto_0
.end method

.method private static final pruneOverlaps(Ljava/util/ArrayList;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/app/memo/uiwidget/LinkSpec;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1229
    .local p0, "links":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/app/memo/uiwidget/LinkSpec;>;"
    new-instance v2, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$4;

    invoke-direct {v2}, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$4;-><init>()V

    .line 1255
    .local v2, "c":Ljava/util/Comparator;, "Ljava/util/Comparator<Lcom/samsung/android/app/memo/uiwidget/LinkSpec;>;"
    invoke-static {p0, v2}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 1257
    invoke-virtual {p0}, Ljava/util/ArrayList;->size()I

    move-result v4

    .line 1258
    .local v4, "len":I
    const/4 v3, 0x0

    .line 1260
    .local v3, "i":I
    :goto_0
    add-int/lit8 v6, v4, -0x1

    if-lt v3, v6, :cond_0

    .line 1284
    return-void

    .line 1261
    :cond_0
    invoke-virtual {p0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/app/memo/uiwidget/LinkSpec;

    .line 1262
    .local v0, "a":Lcom/samsung/android/app/memo/uiwidget/LinkSpec;
    add-int/lit8 v6, v3, 0x1

    invoke-virtual {p0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/app/memo/uiwidget/LinkSpec;

    .line 1263
    .local v1, "b":Lcom/samsung/android/app/memo/uiwidget/LinkSpec;
    const/4 v5, -0x1

    .line 1265
    .local v5, "remove":I
    iget v6, v0, Lcom/samsung/android/app/memo/uiwidget/LinkSpec;->mStart:I

    iget v7, v1, Lcom/samsung/android/app/memo/uiwidget/LinkSpec;->mStart:I

    if-gt v6, v7, :cond_4

    iget v6, v0, Lcom/samsung/android/app/memo/uiwidget/LinkSpec;->mEnd:I

    iget v7, v1, Lcom/samsung/android/app/memo/uiwidget/LinkSpec;->mStart:I

    if-le v6, v7, :cond_4

    .line 1266
    iget v6, v1, Lcom/samsung/android/app/memo/uiwidget/LinkSpec;->mEnd:I

    iget v7, v0, Lcom/samsung/android/app/memo/uiwidget/LinkSpec;->mEnd:I

    if-gt v6, v7, :cond_2

    .line 1267
    add-int/lit8 v5, v3, 0x1

    .line 1274
    :cond_1
    :goto_1
    const/4 v6, -0x1

    if-eq v5, v6, :cond_4

    .line 1275
    invoke-virtual {p0, v5}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 1276
    add-int/lit8 v4, v4, -0x1

    .line 1277
    goto :goto_0

    .line 1268
    :cond_2
    iget v6, v0, Lcom/samsung/android/app/memo/uiwidget/LinkSpec;->mEnd:I

    iget v7, v0, Lcom/samsung/android/app/memo/uiwidget/LinkSpec;->mStart:I

    sub-int/2addr v6, v7

    iget v7, v1, Lcom/samsung/android/app/memo/uiwidget/LinkSpec;->mEnd:I

    iget v8, v1, Lcom/samsung/android/app/memo/uiwidget/LinkSpec;->mStart:I

    sub-int/2addr v7, v8

    if-le v6, v7, :cond_3

    .line 1269
    add-int/lit8 v5, v3, 0x1

    .line 1270
    goto :goto_1

    :cond_3
    iget v6, v0, Lcom/samsung/android/app/memo/uiwidget/LinkSpec;->mEnd:I

    iget v7, v0, Lcom/samsung/android/app/memo/uiwidget/LinkSpec;->mStart:I

    sub-int/2addr v6, v7

    iget v7, v1, Lcom/samsung/android/app/memo/uiwidget/LinkSpec;->mEnd:I

    iget v8, v1, Lcom/samsung/android/app/memo/uiwidget/LinkSpec;->mStart:I

    sub-int/2addr v7, v8

    if-ge v6, v7, :cond_1

    .line 1271
    move v5, v3

    goto :goto_1

    .line 1282
    :cond_4
    add-int/lit8 v3, v3, 0x1

    goto :goto_0
.end method

.method private static setWesternDateTimeFormat()V
    .locals 29

    .prologue
    .line 121
    sget-object v26, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify;->mContext:Landroid/content/Context;

    invoke-static/range {v26 .. v26}, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$ResourceString;->init(Landroid/content/Context;)V

    .line 122
    sget-object v2, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$ResourceString;->mMonth01:Ljava/lang/String;

    .line 123
    .local v2, "month01":Ljava/lang/String;
    sget-object v4, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$ResourceString;->mMonth02:Ljava/lang/String;

    .line 124
    .local v4, "month02":Ljava/lang/String;
    sget-object v6, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$ResourceString;->mMonth03:Ljava/lang/String;

    .line 125
    .local v6, "month03":Ljava/lang/String;
    sget-object v8, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$ResourceString;->mMonth04:Ljava/lang/String;

    .line 126
    .local v8, "month04":Ljava/lang/String;
    sget-object v10, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$ResourceString;->mMonth05:Ljava/lang/String;

    .line 127
    .local v10, "month05":Ljava/lang/String;
    sget-object v12, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$ResourceString;->mMonth06:Ljava/lang/String;

    .line 128
    .local v12, "month06":Ljava/lang/String;
    sget-object v14, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$ResourceString;->mMonth07:Ljava/lang/String;

    .line 129
    .local v14, "month07":Ljava/lang/String;
    sget-object v16, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$ResourceString;->mMonth08:Ljava/lang/String;

    .line 130
    .local v16, "month08":Ljava/lang/String;
    sget-object v18, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$ResourceString;->mMonth09:Ljava/lang/String;

    .line 131
    .local v18, "month09":Ljava/lang/String;
    sget-object v20, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$ResourceString;->mMonth10:Ljava/lang/String;

    .line 132
    .local v20, "month10":Ljava/lang/String;
    sget-object v22, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$ResourceString;->mMonth11:Ljava/lang/String;

    .line 133
    .local v22, "month11":Ljava/lang/String;
    sget-object v24, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$ResourceString;->mMonth12:Ljava/lang/String;

    .line 134
    .local v24, "month12":Ljava/lang/String;
    sget-object v3, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$ResourceString;->mMonth01_abb:Ljava/lang/String;

    .line 135
    .local v3, "month01_abb":Ljava/lang/String;
    sget-object v5, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$ResourceString;->mMonth02_abb:Ljava/lang/String;

    .line 136
    .local v5, "month02_abb":Ljava/lang/String;
    sget-object v7, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$ResourceString;->mMonth03_abb:Ljava/lang/String;

    .line 137
    .local v7, "month03_abb":Ljava/lang/String;
    sget-object v9, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$ResourceString;->mMonth04_abb:Ljava/lang/String;

    .line 138
    .local v9, "month04_abb":Ljava/lang/String;
    sget-object v11, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$ResourceString;->mMonth05_abb:Ljava/lang/String;

    .line 139
    .local v11, "month05_abb":Ljava/lang/String;
    sget-object v13, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$ResourceString;->mMonth06_abb:Ljava/lang/String;

    .line 140
    .local v13, "month06_abb":Ljava/lang/String;
    sget-object v15, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$ResourceString;->mMonth07_abb:Ljava/lang/String;

    .line 141
    .local v15, "month07_abb":Ljava/lang/String;
    sget-object v17, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$ResourceString;->mMonth08_abb:Ljava/lang/String;

    .line 142
    .local v17, "month08_abb":Ljava/lang/String;
    sget-object v19, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$ResourceString;->mMonth09_abb:Ljava/lang/String;

    .line 143
    .local v19, "month09_abb":Ljava/lang/String;
    sget-object v21, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$ResourceString;->mMonth10_abb:Ljava/lang/String;

    .line 144
    .local v21, "month10_abb":Ljava/lang/String;
    sget-object v23, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$ResourceString;->mMonth11_abb:Ljava/lang/String;

    .line 145
    .local v23, "month11_abb":Ljava/lang/String;
    sget-object v25, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$ResourceString;->mMonth12_abb:Ljava/lang/String;

    .line 148
    .local v25, "month12_abb":Ljava/lang/String;
    new-instance v26, Ljava/lang/StringBuilder;

    const-string v27, "((("

    invoke-direct/range {v26 .. v27}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 149
    move-object/from16 v0, v26

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    .line 150
    const-string v27, "|"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    .line 151
    move-object/from16 v0, v26

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    .line 152
    const-string v27, "|"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    .line 153
    move-object/from16 v0, v26

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    .line 154
    const-string v27, "|"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    .line 155
    move-object/from16 v0, v26

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    .line 156
    const-string v27, "|"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    .line 157
    move-object/from16 v0, v26

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    .line 158
    const-string v27, "|"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    .line 159
    move-object/from16 v0, v26

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    .line 160
    const-string v27, "|"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    .line 161
    move-object/from16 v0, v26

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    .line 162
    const-string v27, ")|"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    .line 163
    const-string v27, "(("

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    .line 164
    move-object/from16 v0, v26

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    .line 165
    const-string v27, "|"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    .line 166
    move-object/from16 v0, v26

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    .line 167
    const-string v27, "|"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    .line 168
    move-object/from16 v0, v26

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    .line 169
    const-string v27, "|"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    .line 170
    move-object/from16 v0, v26

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    .line 171
    const-string v27, "|"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    .line 172
    move-object/from16 v0, v26

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    .line 173
    const-string v27, "|"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    .line 174
    move-object/from16 v0, v26

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    .line 175
    const-string v27, "|"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    .line 176
    move-object/from16 v0, v26

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    .line 177
    const-string v27, ")[\\.\\,]?))"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    .line 178
    const-string v27, " "

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    .line 179
    const-string v27, "(0[1-9]|([1](st)?|[2](nd)?|[3](rd)?|[4-90](th)?)|[1][0-9](th)?|[2]([1](st)?|[2](nd)?|[3](rd)?|[4-90](th)?)|3(0(th)?|1(st)?))"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    .line 180
    const-string v27, "|"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    .line 181
    const-string v27, "(("

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    .line 182
    move-object/from16 v0, v26

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    .line 183
    const-string v27, "|"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    .line 184
    move-object/from16 v0, v26

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    .line 185
    const-string v27, "|"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    .line 186
    move-object/from16 v0, v26

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    .line 187
    const-string v27, "|"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    .line 188
    move-object/from16 v0, v26

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    .line 189
    const-string v27, ")|"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    .line 190
    const-string v27, "(("

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    .line 191
    move-object/from16 v0, v26

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    .line 192
    const-string v27, "|"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    .line 193
    move-object/from16 v0, v26

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    .line 194
    const-string v27, "|"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    .line 195
    move-object/from16 v0, v26

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    .line 196
    const-string v27, "|"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    .line 197
    move-object/from16 v0, v26

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    .line 198
    const-string v27, ")[\\.\\,]?))"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    .line 199
    const-string v27, " "

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    .line 200
    const-string v27, "(0[1-9]|([1](st)?|[2](nd)?|[3](rd)?|[4-90](th)?)|[1][0-9](th)?|[2]([1](st)?|[2](nd)?|[3](rd)?|[4-90](th)?)|30(th)?)"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    .line 201
    const-string v27, "|"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    .line 202
    const-string v27, "("

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    .line 203
    move-object/from16 v0, v26

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    .line 204
    const-string v27, "|"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    .line 205
    move-object/from16 v0, v26

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    .line 206
    const-string v27, "[\\.\\,]?)"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    .line 207
    const-string v27, " "

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    .line 208
    const-string v27, "(0[1-9]|([1](st)?|[2](nd)?|[3](rd)?|[4-90](th)?)|[1][0-9](th)?|2([1](st)?|[2](nd)?|[3](rd)?|[4-80](th)?))"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    .line 209
    const-string v27, "|"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    const-string v27, "("

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    move-object/from16 v0, v26

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    const-string v27, "|"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    move-object/from16 v0, v26

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    const-string v27, "[\\.\\,]?)"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    const-string v27, "29(th)?"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    const-string v27, " "

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    .line 210
    const-string v27, "((0[48]|[2468][048]|[13579][26])00|[0-9]{2}(0[48]|[2468][048]|[13579][26]))"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    .line 211
    const-string v27, ")"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    .line 148
    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    sput-object v26, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify;->WESTERN_MONTH_DAY_FORMAT:Ljava/lang/String;

    .line 214
    new-instance v26, Ljava/lang/StringBuilder;

    const-string v27, "((0[1-9]|([1](st)?|[2](nd)?|[3](rd)?|[4-90](th)?)|[1][0-9](th)?|[2]([1](st)?|[2](nd)?|[3](rd)?|[4-90](th)?)|3(0(th)?|1(st)?)) (("

    invoke-direct/range {v26 .. v27}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 217
    move-object/from16 v0, v26

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    .line 218
    const-string v27, "|"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    .line 219
    move-object/from16 v0, v26

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    .line 220
    const-string v27, "|"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    .line 221
    move-object/from16 v0, v26

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    .line 222
    const-string v27, "|"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    .line 223
    move-object/from16 v0, v26

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    .line 224
    const-string v27, "|"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    .line 225
    move-object/from16 v0, v26

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    .line 226
    const-string v27, "|"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    .line 227
    move-object/from16 v0, v26

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    .line 228
    const-string v27, "|"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    .line 229
    move-object/from16 v0, v26

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    .line 230
    const-string v27, ")|"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    .line 231
    const-string v27, "(("

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    .line 232
    move-object/from16 v0, v26

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    .line 233
    const-string v27, "|"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    .line 234
    move-object/from16 v0, v26

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    .line 235
    const-string v27, "|"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    .line 236
    move-object/from16 v0, v26

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    .line 237
    const-string v27, "|"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    .line 238
    move-object/from16 v0, v26

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    .line 239
    const-string v27, "|"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    .line 240
    move-object/from16 v0, v26

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    .line 241
    const-string v27, "|"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    .line 242
    move-object/from16 v0, v26

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    .line 243
    const-string v27, "|"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    .line 244
    move-object/from16 v0, v26

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    .line 245
    const-string v27, ")[\\.\\,]?))"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    .line 246
    const-string v27, "|"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    .line 247
    const-string v27, "(0[1-9]|([1](st)?|[2](nd)?|[3](rd)?|[4-90](th)?)|[1][0-9](th)?|[2]([1](st)?|[2](nd)?|[3](rd)?|[4-90](th)?)|30(th)?)"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    .line 248
    const-string v27, " "

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    .line 249
    const-string v27, "(("

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    .line 250
    move-object/from16 v0, v26

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    .line 251
    const-string v27, "|"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    .line 252
    move-object/from16 v0, v26

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    .line 253
    const-string v27, "|"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    .line 254
    move-object/from16 v0, v26

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    .line 255
    const-string v27, "|"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    .line 256
    move-object/from16 v0, v26

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    .line 257
    const-string v27, ")|"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    .line 258
    const-string v27, "(("

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    .line 259
    move-object/from16 v0, v26

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    .line 260
    const-string v27, "|"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    .line 261
    move-object/from16 v0, v26

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    .line 262
    const-string v27, "|"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    .line 263
    move-object/from16 v0, v26

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    .line 264
    const-string v27, "|"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    .line 265
    move-object/from16 v0, v26

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    .line 266
    const-string v27, ")[\\.\\,]?))"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    .line 267
    const-string v27, "|"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    .line 268
    const-string v27, "(0[1-9]|([1](st)?|[2](nd)?|[3](rd)?|[4-90](th)?)|[1][0-9](th)?|2([1](st)?|[2](nd)?|[3](rd)?|[4-80](th)?))"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    .line 269
    const-string v27, " "

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    .line 270
    const-string v27, "("

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    .line 271
    move-object/from16 v0, v26

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    .line 272
    const-string v27, "|"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    .line 273
    move-object/from16 v0, v26

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    .line 274
    const-string v27, "[\\.\\,]?)"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    .line 275
    const-string v27, "|"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    .line 276
    const-string v27, "29(th)?"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    .line 277
    const-string v27, " "

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    .line 278
    const-string v27, "("

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    .line 279
    move-object/from16 v0, v26

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    .line 280
    const-string v27, "|"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    .line 281
    move-object/from16 v0, v26

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    .line 282
    const-string v27, "[\\.\\,]?)"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    .line 283
    const-string v27, " "

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    .line 284
    const-string v27, "((0[48]|[2468][048]|[13579][26])00|[0-9]{2}(0[48]|[2468][048]|[13579][26]))"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    .line 285
    const-string v27, ")"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    .line 214
    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    sput-object v26, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify;->WESTERN_DAY_MONTH_FORMAT:Ljava/lang/String;

    .line 287
    new-instance v26, Ljava/lang/StringBuilder;

    const-string v27, "(("

    invoke-direct/range {v26 .. v27}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v27, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$ResourceString;->mTonight:Ljava/lang/String;

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    const-string v27, "|"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    .line 288
    sget-object v27, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$ResourceString;->mToday:Ljava/lang/String;

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    const-string v27, "|"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    sget-object v27, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$ResourceString;->mTomorrow:Ljava/lang/String;

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    const-string v27, "|"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    .line 289
    sget-object v27, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$ResourceString;->mTonight:Ljava/lang/String;

    sget-object v28, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v27

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    const-string v27, "|"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    sget-object v27, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$ResourceString;->mToday:Ljava/lang/String;

    sget-object v28, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v27

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    .line 290
    const-string v27, "|"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    sget-object v27, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$ResourceString;->mTomorrow:Ljava/lang/String;

    sget-object v28, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v27

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    const-string v27, ")"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    const-string v27, ")"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    .line 287
    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    sput-object v26, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify;->WESTERN_DATE_KEYWORD_FORMAT:Ljava/lang/String;

    .line 292
    new-instance v26, Ljava/lang/StringBuilder;

    const-string v27, "(("

    invoke-direct/range {v26 .. v27}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v27, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$ResourceString;->mDay01:Ljava/lang/String;

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    const-string v27, "|"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    .line 293
    sget-object v27, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$ResourceString;->mDay02:Ljava/lang/String;

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    const-string v27, "|"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    sget-object v27, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$ResourceString;->mDay03:Ljava/lang/String;

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    const-string v27, "|"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    .line 294
    sget-object v27, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$ResourceString;->mDay04_abb:Ljava/lang/String;

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    const-string v27, "|"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    sget-object v27, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$ResourceString;->mDay05:Ljava/lang/String;

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    const-string v27, "|"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    .line 295
    sget-object v27, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$ResourceString;->mDay06:Ljava/lang/String;

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    const-string v27, "|"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    sget-object v27, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$ResourceString;->mDay07:Ljava/lang/String;

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    const-string v27, "|"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    const-string v27, ")"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    const-string v27, ")"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    .line 292
    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    sput-object v26, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify;->WESTERN_DAY_KEYWORD_FORMAT:Ljava/lang/String;

    .line 297
    new-instance v26, Ljava/lang/StringBuilder;

    const-string v27, "((("

    invoke-direct/range {v26 .. v27}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v27, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$ResourceString;->mMorning:Ljava/lang/String;

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    const-string v27, "|"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    .line 298
    sget-object v27, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$ResourceString;->mMorning:Ljava/lang/String;

    sget-object v28, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v27

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    .line 299
    const-string v27, ") ([0][6-9]|[1][0-1]|[6-9])(\\:)([0-5][0-9]|[1-9]))"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    const-string v27, "|"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    const-string v27, "(("

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    .line 300
    sget-object v27, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$ResourceString;->mAfternoon:Ljava/lang/String;

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    const-string v27, "|"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    sget-object v27, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$ResourceString;->mAfternoon:Ljava/lang/String;

    sget-object v28, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v27

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    .line 301
    const-string v27, ") ([0][1-5]|[1][2]|[1-5])(\\:)([0-5][0-9]|[1-9]))"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    const-string v27, "|"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    const-string v27, "(("

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    .line 302
    sget-object v27, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$ResourceString;->mEvening:Ljava/lang/String;

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    const-string v27, "|"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    sget-object v27, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$ResourceString;->mEvening:Ljava/lang/String;

    sget-object v28, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v27

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    .line 303
    const-string v27, ") ([0][6-9]|[1][0]|[6-9])(\\:)([0-5][0-9]|[1-9]))"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    const-string v27, "|"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    const-string v27, "(("

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    .line 304
    sget-object v27, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$ResourceString;->mNight:Ljava/lang/String;

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    const-string v27, "|"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    sget-object v27, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$ResourceString;->mNight:Ljava/lang/String;

    sget-object v28, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v27

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    .line 305
    const-string v27, ") ([0][8-9]|[1][0-2]|[8-9])(\\:)([0-5][0-9]|[1-9]))"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    const-string v27, ")"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    .line 297
    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    sput-object v26, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify;->WESTERN_TIME_KEYWORD_FORMAT:Ljava/lang/String;

    .line 307
    new-instance v26, Ljava/lang/StringBuilder;

    const-string v27, "(("

    invoke-direct/range {v26 .. v27}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v27, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify;->WESTERN_MONTH_DAY_FORMAT:Ljava/lang/String;

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    .line 309
    const-string v27, "|"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    sget-object v27, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify;->WESTERN_DAY_MONTH_FORMAT:Ljava/lang/String;

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    .line 310
    const-string v27, ")"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    const-string v27, " "

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    const-string v27, "(18|19|20|21)\\d{2}"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    const-string v27, ")"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    .line 307
    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    invoke-static/range {v26 .. v26}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v26

    sput-object v26, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify;->WESTERN_ONLY_DATE_PATTERN:Ljava/util/regex/Pattern;

    .line 312
    new-instance v26, Ljava/lang/StringBuilder;

    const-string v27, "(("

    invoke-direct/range {v26 .. v27}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v27, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify;->WESTERN_MONTH_DAY_FORMAT:Ljava/lang/String;

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    .line 314
    const-string v27, "|"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    sget-object v27, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify;->WESTERN_DAY_MONTH_FORMAT:Ljava/lang/String;

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    .line 315
    const-string v27, ")"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    const-string v27, " "

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    const-string v27, "(18|19|20|21)\\d{2}"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    const-string v27, "[ \\,][ ]?"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    const-string v27, "(((([1][3-9])|([2][0-3]))[\\:]([0-5][0-9]|[1-9]))|(([0-1][0-9]|[1-9])[ ]?[aApP][mM])|(([0-1][0-9]|[1-9])[\\:]([0-5][0-9]|[1-9])([ ]?[aApP][mM])?))"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    const-string v27, ")"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    .line 312
    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    invoke-static/range {v26 .. v26}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v26

    sput-object v26, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify;->WESTERN_DATE_TIME_PATTERN:Ljava/util/regex/Pattern;

    .line 317
    new-instance v26, Ljava/lang/StringBuilder;

    const-string v27, "((((([1][3-9])|([2][0-3]))[\\:]([0-5][0-9]|[1-9]))|(([0-1][0-9]|[1-9])[ ]?[aApP][mM])|(([0-1][0-9]|[1-9])[\\:]([0-5][0-9]|[1-9])([ ]?[aApP][mM])?))[ \\,][ ]?("

    invoke-direct/range {v26 .. v27}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 318
    sget-object v27, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify;->WESTERN_MONTH_DAY_FORMAT:Ljava/lang/String;

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    .line 319
    const-string v27, "|"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    sget-object v27, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify;->WESTERN_DAY_MONTH_FORMAT:Ljava/lang/String;

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    .line 320
    const-string v27, ")"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    const-string v27, " "

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    const-string v27, "(18|19|20|21)\\d{2}"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    const-string v27, ")"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    .line 317
    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    invoke-static/range {v26 .. v26}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v26

    sput-object v26, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify;->WESTERN_TIME_DATE_PATTERN:Ljava/util/regex/Pattern;

    .line 322
    new-instance v26, Ljava/lang/StringBuilder;

    const-string v27, "("

    invoke-direct/range {v26 .. v27}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v27, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify;->WESTERN_DATE_KEYWORD_FORMAT:Ljava/lang/String;

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    const-string v27, "("

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    .line 323
    const-string v27, "[ \\,][ ]?"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    const-string v27, "(((([1][3-9])|([2][0-3]))[\\:]([0-5][0-9]|[1-9]))|(([0-1][0-9]|[1-9])[ ]?[aApP][mM])|(([0-1][0-9]|[1-9])[\\:]([0-5][0-9]|[1-9])([ ]?[aApP][mM])?))"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    const-string v27, ")?"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    const-string v27, ")"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    .line 322
    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    invoke-static/range {v26 .. v26}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v26

    sput-object v26, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify;->WESTERN_DATE_KEYWORD_PATTERN:Ljava/util/regex/Pattern;

    .line 325
    new-instance v26, Ljava/lang/StringBuilder;

    const-string v27, "("

    invoke-direct/range {v26 .. v27}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v27, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify;->WESTERN_DAY_KEYWORD_FORMAT:Ljava/lang/String;

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    .line 326
    const-string v27, "[ \\,][ ]?"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    const-string v27, "("

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    const-string v27, "(((([1][3-9])|([2][0-3]))[\\:]([0-5][0-9]|[1-9]))|(([0-1][0-9]|[1-9])[ ]?[aApP][mM])|(([0-1][0-9]|[1-9])[\\:]([0-5][0-9]|[1-9])([ ]?[aApP][mM])?))"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    const-string v27, "|"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    sget-object v27, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify;->WESTERN_TIME_KEYWORD_FORMAT:Ljava/lang/String;

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    .line 327
    const-string v27, ")"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    const-string v27, ")"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    .line 325
    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    invoke-static/range {v26 .. v26}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v26

    sput-object v26, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify;->WESTERN_TIME_KEYWORD_PATTERN:Ljava/util/regex/Pattern;

    .line 329
    return-void
.end method

.method protected static showPopup(Ljava/lang/String;ZLjava/lang/String;)Landroid/app/AlertDialog;
    .locals 7
    .param p0, "mTime"    # Ljava/lang/String;
    .param p1, "mIsAllDay"    # Z
    .param p2, "mDesc"    # Ljava/lang/String;

    .prologue
    .line 512
    sget-object v4, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify;->mContext:Landroid/content/Context;

    const v5, 0x7f0b0067

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 513
    .local v3, "title":Ljava/lang/String;
    sget-object v4, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify;->mContext:Landroid/content/Context;

    const v5, 0x7f0b0068

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 514
    .local v2, "message":Ljava/lang/String;
    new-instance v1, Landroid/app/AlertDialog$Builder;

    sget-object v4, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify;->mContext:Landroid/content/Context;

    invoke-direct {v1, v4}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 515
    .local v1, "builder":Landroid/app/AlertDialog$Builder;
    invoke-virtual {v1, v3}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 516
    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 519
    new-instance v4, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$1;

    invoke-direct {v4}, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$1;-><init>()V

    invoke-virtual {v1, v4}, Landroid/app/AlertDialog$Builder;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)Landroid/app/AlertDialog$Builder;

    .line 530
    const v4, 0x7f0b0069

    new-instance v5, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$2;

    invoke-direct {v5}, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$2;-><init>()V

    invoke-virtual {v1, v4, v5}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    .line 538
    const v5, 0x7f0b006a

    new-instance v6, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$3;

    invoke-direct {v6}, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$3;-><init>()V

    invoke-virtual {v4, v5, v6}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 542
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 543
    .local v0, "addToCalendarConfirmDialog":Landroid/app/AlertDialog;
    return-object v0
.end method

.method public static toDate(Ljava/lang/String;)Ljava/util/Date;
    .locals 14
    .param p0, "dateStr"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    .line 1092
    const/4 v0, 0x5

    .line 1093
    .local v0, "DATA_ELEMENT_COUNT":I
    const/4 v9, 0x0

    .line 1094
    .local v9, "isPM":Z
    new-instance v5, Ljava/util/Date;

    invoke-direct {v5}, Ljava/util/Date;-><init>()V

    .line 1096
    .local v5, "defalutDate":Ljava/util/Date;
    if-nez p0, :cond_1

    move-object v2, v5

    .line 1182
    :cond_0
    :goto_0
    return-object v2

    .line 1100
    :cond_1
    sget-object v11, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {p0, v11}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object p0

    .line 1101
    const-string v11, "pm"

    invoke-virtual {p0, v11}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v11

    if-eqz v11, :cond_7

    .line 1102
    const/4 v9, 0x1

    .line 1103
    const-string v11, "pm"

    const-string v12, ""

    invoke-virtual {p0, v11, v12}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p0

    .line 1108
    :cond_2
    :goto_1
    invoke-virtual {p0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object p0

    .line 1111
    const/16 v11, 0x2f

    const/16 v12, 0x20

    invoke-virtual {p0, v11, v12}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object p0

    .line 1112
    const-string v11, ":"

    invoke-virtual {p0, v11}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v11

    if-nez v11, :cond_8

    .line 1113
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v12, ":00"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    .line 1117
    :cond_3
    :goto_2
    const/16 v11, 0x3a

    const/16 v12, 0x20

    invoke-virtual {p0, v11, v12}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object p0

    .line 1118
    const/16 v11, 0x2c

    const/16 v12, 0x20

    invoke-virtual {p0, v11, v12}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object p0

    .line 1119
    invoke-virtual {p0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v11

    const-string v12, "\\s+"

    const-string v13, " "

    invoke-virtual {v11, v12, v13}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    .line 1121
    const-string v11, " "

    invoke-virtual {p0, v11}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .line 1122
    .local v4, "dateStrArr":[Ljava/lang/String;
    const/4 v11, 0x5

    new-array v10, v11, [Ljava/lang/String;

    .line 1124
    .local v10, "newDateStrArr":[Ljava/lang/String;
    const/4 v11, 0x0

    const/4 v12, 0x0

    array-length v13, v4

    invoke-static {v4, v11, v10, v12, v13}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1126
    const/4 v11, 0x0

    aget-object v11, v4, v11

    invoke-virtual {v11}, Ljava/lang/String;->length()I

    move-result v11

    const/4 v12, 0x4

    if-ge v11, v12, :cond_4

    .line 1127
    const/4 v11, 0x0

    const/4 v12, 0x2

    aget-object v12, v4, v12

    aput-object v12, v10, v11

    .line 1128
    const/4 v11, 0x1

    aget-object v11, v4, v11

    invoke-static {v11}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v11

    const/16 v12, 0xc

    if-le v11, v12, :cond_9

    .line 1129
    const/4 v11, 0x1

    const/4 v12, 0x0

    aget-object v12, v4, v12

    aput-object v12, v10, v11

    .line 1130
    const/4 v11, 0x2

    const/4 v12, 0x1

    aget-object v12, v4, v12

    aput-object v12, v10, v11

    .line 1151
    :cond_4
    :goto_3
    const/4 v8, 0x1

    .local v8, "i":I
    :goto_4
    array-length v11, v10

    if-lt v8, v11, :cond_b

    .line 1157
    if-eqz v9, :cond_5

    .line 1158
    const/4 v11, 0x3

    aget-object v11, v10, v11

    invoke-static {v11}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v11

    add-int/lit8 v7, v11, 0xc

    .line 1159
    .local v7, "hour":I
    const/4 v11, 0x3

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v12, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    aput-object v12, v10, v11

    .line 1162
    .end local v7    # "hour":I
    :cond_5
    new-instance v11, Ljava/lang/StringBuilder;

    const/4 v12, 0x0

    aget-object v12, v10, v12

    invoke-static {v12}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v12, "/"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const/4 v12, 0x1

    aget-object v12, v10, v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "/"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const/4 v12, 0x2

    aget-object v12, v10, v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    .line 1163
    const/4 v12, 0x3

    aget-object v12, v10, v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ":"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const/4 v12, 0x4

    aget-object v12, v10, v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    .line 1162
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    .line 1165
    invoke-virtual {p0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object p0

    .line 1166
    const/4 v2, 0x0

    .line 1167
    .local v2, "date":Ljava/util/Date;
    const-string v11, "yyyy/MM/dd HH:mm"

    invoke-static {v11}, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify;->getDateFormat(Ljava/lang/String;)Ljava/text/DateFormat;

    move-result-object v3

    .line 1168
    .local v3, "dateFormat":Ljava/text/DateFormat;
    invoke-static {}, Lcom/samsung/android/app/memo/util/Utils;->IsJapanLanguage()Z

    move-result v11

    if-eqz v11, :cond_6

    .line 1169
    const-string v11, "yyyy/MM/dd K:mm"

    invoke-static {v11}, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify;->getDateFormat(Ljava/lang/String;)Ljava/text/DateFormat;

    move-result-object v3

    .line 1172
    :cond_6
    :try_start_0
    invoke-virtual {v3, p0}, Ljava/text/DateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v2

    .line 1173
    const-string v11, "Asia/Shanghai"

    invoke-static {}, Landroid/text/format/Time;->getCurrentTimezone()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-nez v11, :cond_0

    invoke-virtual {v3, v2}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {p0, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v11

    if-nez v11, :cond_0

    .line 1176
    move-object v2, v5

    goto/16 :goto_0

    .line 1104
    .end local v2    # "date":Ljava/util/Date;
    .end local v3    # "dateFormat":Ljava/text/DateFormat;
    .end local v4    # "dateStrArr":[Ljava/lang/String;
    .end local v8    # "i":I
    .end local v10    # "newDateStrArr":[Ljava/lang/String;
    :cond_7
    const-string v11, "am"

    invoke-virtual {p0, v11}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v11

    if-eqz v11, :cond_2

    .line 1105
    const-string v11, "am"

    const-string v12, ""

    invoke-virtual {p0, v11, v12}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p0

    goto/16 :goto_1

    .line 1114
    :cond_8
    const-string v11, ":"

    invoke-virtual {p0, v11}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_3

    .line 1115
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v12, "00"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    goto/16 :goto_2

    .line 1131
    .restart local v4    # "dateStrArr":[Ljava/lang/String;
    .restart local v10    # "newDateStrArr":[Ljava/lang/String;
    :cond_9
    const/4 v11, 0x0

    aget-object v11, v4, v11

    invoke-static {v11}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v11

    const/16 v12, 0xc

    if-le v11, v12, :cond_a

    .line 1132
    const/4 v11, 0x1

    const/4 v12, 0x1

    aget-object v12, v4, v12

    aput-object v12, v10, v11

    .line 1133
    const/4 v11, 0x2

    const/4 v12, 0x0

    aget-object v12, v4, v12

    aput-object v12, v10, v11

    goto/16 :goto_3

    .line 1136
    :cond_a
    const/4 v11, 0x1

    const/4 v12, 0x0

    aget-object v12, v4, v12

    aput-object v12, v10, v11

    .line 1137
    const/4 v11, 0x2

    const/4 v12, 0x1

    aget-object v12, v4, v12

    aput-object v12, v10, v11

    .line 1139
    sget-object v11, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify;->mContext:Landroid/content/Context;

    invoke-virtual {v11}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v11

    .line 1140
    const-string v12, "date_format"

    .line 1139
    invoke-static {v11, v12}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1141
    .local v1, "curDateFormat":Ljava/lang/String;
    if-eqz v1, :cond_4

    .line 1144
    const-string v11, "dd-MM-yyyy"

    invoke-virtual {v1, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_4

    .line 1145
    const/4 v11, 0x1

    const/4 v12, 0x1

    aget-object v12, v4, v12

    aput-object v12, v10, v11

    .line 1146
    const/4 v11, 0x2

    const/4 v12, 0x0

    aget-object v12, v4, v12

    aput-object v12, v10, v11

    goto/16 :goto_3

    .line 1152
    .end local v1    # "curDateFormat":Ljava/lang/String;
    .restart local v8    # "i":I
    :cond_b
    aget-object v11, v10, v8

    invoke-virtual {v11}, Ljava/lang/String;->length()I

    move-result v11

    const/4 v12, 0x1

    if-ne v11, v12, :cond_c

    .line 1153
    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "0"

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    aget-object v12, v10, v8

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    aput-object v11, v10, v8

    .line 1151
    :cond_c
    add-int/lit8 v8, v8, 0x1

    goto/16 :goto_4

    .line 1178
    .restart local v2    # "date":Ljava/util/Date;
    .restart local v3    # "dateFormat":Ljava/text/DateFormat;
    :catch_0
    move-exception v6

    .line 1179
    .local v6, "e":Ljava/text/ParseException;
    move-object v2, v5

    goto/16 :goto_0
.end method

.method private static westernDateTime2Scheme(Ljava/lang/String;)Ljava/lang/String;
    .locals 9
    .param p0, "sTime"    # Ljava/lang/String;

    .prologue
    const/4 v8, 0x2

    .line 787
    const-string v2, ""

    .line 788
    .local v2, "month":Ljava/lang/String;
    const-string v0, ""

    .line 789
    .local v0, "day":Ljava/lang/String;
    const-string v5, ""

    .line 790
    .local v5, "year":Ljava/lang/String;
    const-string v6, "western_date_time_2://"

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    invoke-virtual {p0, v6}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p0

    .line 791
    const-string v6, " "

    invoke-virtual {p0, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .line 792
    .local v4, "temp":[Ljava/lang/String;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-lt v1, v8, :cond_1

    .line 802
    :cond_0
    :goto_1
    aget-object v5, v4, v8

    .line 804
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v7, "/"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "/"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    .line 806
    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3, p0}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 807
    .local v3, "sbTime":Ljava/lang/StringBuffer;
    const/4 v1, 0x3

    :goto_2
    array-length v6, v4

    if-lt v1, v6, :cond_3

    .line 811
    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object p0

    .line 812
    return-object p0

    .line 793
    .end local v3    # "sbTime":Ljava/lang/StringBuffer;
    :cond_1
    aget-object v6, v4, v1

    invoke-static {v6}, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$ResourceString;->getMonth(Ljava/lang/String;)I

    move-result v6

    if-lez v6, :cond_2

    .line 794
    aget-object v6, v4, v1

    invoke-static {v6}, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$ResourceString;->getMonth(Ljava/lang/String;)I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    .line 795
    add-int/lit8 v6, v1, -0x1

    invoke-static {v6}, Ljava/lang/Math;->abs(I)I

    move-result v6

    aget-object v0, v4, v6

    .line 796
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v6

    const/4 v7, 0x3

    if-lt v6, v7, :cond_0

    .line 797
    const/4 v6, 0x0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v7

    add-int/lit8 v7, v7, -0x2

    invoke-virtual {v0, v6, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 799
    goto :goto_1

    .line 792
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 808
    .restart local v3    # "sbTime":Ljava/lang/StringBuffer;
    :cond_3
    const/16 v6, 0x20

    invoke-virtual {v3, v6}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    move-result-object v3

    .line 809
    aget-object v6, v4, v1

    invoke-virtual {v3, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    .line 807
    add-int/lit8 v1, v1, 0x1

    goto :goto_2
.end method

.method private static westernDateTime3Scheme(Ljava/lang/String;)Ljava/lang/String;
    .locals 8
    .param p0, "sTime"    # Ljava/lang/String;

    .prologue
    .line 846
    const-string v2, ""

    .line 847
    .local v2, "month":Ljava/lang/String;
    const-string v0, ""

    .line 848
    .local v0, "day":Ljava/lang/String;
    const-string v5, ""

    .line 850
    .local v5, "year":Ljava/lang/String;
    const-string v6, "western_date_time_3://"

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    invoke-virtual {p0, v6}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p0

    .line 851
    const-string v6, " "

    invoke-virtual {p0, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .line 852
    .local v4, "temp":[Ljava/lang/String;
    array-length v6, v4

    add-int/lit8 v1, v6, -0x2

    .local v1, "i":I
    :goto_0
    if-gtz v1, :cond_1

    .line 862
    :cond_0
    :goto_1
    array-length v6, v4

    add-int/lit8 v6, v6, -0x1

    aget-object v5, v4, v6

    .line 864
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v7, "/"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "/"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    .line 865
    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3, p0}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 866
    .local v3, "sbTime":Ljava/lang/StringBuffer;
    const/4 v1, 0x0

    :goto_2
    array-length v6, v4

    add-int/lit8 v6, v6, -0x3

    if-lt v1, v6, :cond_3

    .line 870
    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object p0

    .line 871
    return-object p0

    .line 853
    .end local v3    # "sbTime":Ljava/lang/StringBuffer;
    :cond_1
    aget-object v6, v4, v1

    invoke-static {v6}, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$ResourceString;->getMonth(Ljava/lang/String;)I

    move-result v6

    if-lez v6, :cond_2

    .line 854
    new-instance v6, Ljava/lang/StringBuilder;

    aget-object v7, v4, v1

    invoke-static {v7}, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$ResourceString;->getMonth(Ljava/lang/String;)I

    move-result v7

    invoke-static {v7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 855
    array-length v6, v4

    sub-int v6, v1, v6

    invoke-static {v6}, Ljava/lang/Math;->abs(I)I

    move-result v6

    aget-object v0, v4, v6

    .line 856
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v6

    const/4 v7, 0x3

    if-lt v6, v7, :cond_0

    .line 857
    const/4 v6, 0x0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v7

    add-int/lit8 v7, v7, -0x2

    invoke-virtual {v0, v6, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 859
    goto :goto_1

    .line 852
    :cond_2
    add-int/lit8 v1, v1, -0x1

    goto :goto_0

    .line 867
    .restart local v3    # "sbTime":Ljava/lang/StringBuffer;
    :cond_3
    const/16 v6, 0x20

    invoke-virtual {v3, v6}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    move-result-object v3

    .line 868
    aget-object v6, v4, v1

    invoke-virtual {v3, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    .line 866
    add-int/lit8 v1, v1, 0x1

    goto :goto_2
.end method

.method private static westernDateTime5Scheme(Ljava/lang/String;Ljava/text/SimpleDateFormat;Ljava/text/SimpleDateFormat;)Ljava/lang/String;
    .locals 12
    .param p0, "sTime"    # Ljava/lang/String;
    .param p1, "dateFormat"    # Ljava/text/SimpleDateFormat;
    .param p2, "dayFormat"    # Ljava/text/SimpleDateFormat;

    .prologue
    .line 907
    const/4 v0, -0x1

    .line 908
    .local v0, "curDayIndex":I
    const/4 v3, -0x1

    .line 910
    .local v3, "newDayIndex":I
    sget-wide v8, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify;->mModifiedTime:J

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {p2, v8}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 912
    .local v4, "sCurDay":Ljava/lang/String;
    const-string v8, "western_date_time_5://"

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    invoke-virtual {p0, v8}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p0

    .line 913
    const-string v8, " "

    invoke-virtual {p0, v8}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v6

    .line 915
    .local v6, "temp":[Ljava/lang/String;
    const/4 v8, 0x0

    aget-object v8, v6, v8

    invoke-static {v8}, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$ResourceString;->getEnglishWeekDay(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 916
    .local v7, "weekday":Ljava/lang/String;
    const/4 v8, 0x0

    aput-object v7, v6, v8

    .line 917
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    sget-object v8, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify;->mArrDayOfWeek:[Ljava/lang/String;

    array-length v8, v8

    if-lt v2, v8, :cond_1

    .line 927
    sub-int v1, v3, v0

    .line 928
    .local v1, "delta":I
    if-lez v1, :cond_4

    .line 929
    sget-wide v8, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify;->mModifiedTime:J

    const v10, 0x5265c00

    mul-int/2addr v10, v1

    int-to-long v10, v10

    add-long/2addr v8, v10

    sput-wide v8, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify;->mModifiedTime:J

    .line 934
    :cond_0
    :goto_1
    sget-wide v8, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify;->mModifiedTime:J

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {p1, v8}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p0

    .line 936
    const/4 v8, 0x1

    aget-object v8, v6, v8

    sget-object v9, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$ResourceString;->mMorning:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_5

    .line 937
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v9, " "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const/4 v9, 0x2

    aget-object v9, v6, v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    .line 951
    :goto_2
    return-object p0

    .line 918
    .end local v1    # "delta":I
    :cond_1
    const/4 v8, 0x0

    aget-object v8, v6, v8

    sget-object v9, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v8, v9}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v8

    sget-object v9, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify;->mArrDayOfWeek:[Ljava/lang/String;

    aget-object v9, v9, v2

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 919
    move v3, v2

    .line 922
    :cond_2
    sget-object v8, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify;->mArrDayOfWeek:[Ljava/lang/String;

    aget-object v8, v8, v2

    sget-object v9, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v4, v9}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 923
    move v0, v2

    .line 917
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 930
    .restart local v1    # "delta":I
    :cond_4
    if-gez v1, :cond_0

    .line 931
    sget-wide v8, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify;->mModifiedTime:J

    const v10, 0x5265c00

    add-int/lit8 v11, v1, 0x7

    mul-int/2addr v10, v11

    int-to-long v10, v10

    add-long/2addr v8, v10

    sput-wide v8, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify;->mModifiedTime:J

    goto :goto_1

    .line 939
    :cond_5
    const/4 v8, 0x1

    aget-object v8, v6, v8

    sget-object v9, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$ResourceString;->mAfternoon:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_6

    .line 940
    const/4 v8, 0x1

    aget-object v8, v6, v8

    sget-object v9, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$ResourceString;->mEvening:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_6

    .line 941
    const/4 v8, 0x1

    aget-object v8, v6, v8

    sget-object v9, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$ResourceString;->mNight:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_7

    .line 942
    :cond_6
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v9, " "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const/4 v9, 0x2

    aget-object v9, v6, v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "pm"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    .line 943
    goto :goto_2

    .line 944
    :cond_7
    new-instance v5, Ljava/lang/StringBuffer;

    invoke-direct {v5, p0}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 945
    .local v5, "sbTime":Ljava/lang/StringBuffer;
    const/4 v2, 0x1

    :goto_3
    array-length v8, v6

    if-lt v2, v8, :cond_8

    .line 949
    invoke-virtual {v5}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object p0

    goto/16 :goto_2

    .line 946
    :cond_8
    const/16 v8, 0x20

    invoke-virtual {v5, v8}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    move-result-object v5

    .line 947
    aget-object v8, v6, v2

    invoke-virtual {v5, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    .line 945
    add-int/lit8 v2, v2, 0x1

    goto :goto_3
.end method
