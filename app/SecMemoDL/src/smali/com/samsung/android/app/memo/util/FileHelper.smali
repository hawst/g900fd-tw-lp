.class public final Lcom/samsung/android/app/memo/util/FileHelper;
.super Ljava/lang/Object;
.source "FileHelper.java"


# static fields
.field private static final COPY_BUF_SIZE:I = 0x100000

.field public static final DEFAULT_BLOB_EXTENSSION:Ljava/lang/String; = ".blob"

.field public static final ERROR_CODE_EXCEPTION:I = -0x1

.field public static final ERROR_CODE_NOT_SUPPORTED:I = -0x2

.field public static final ERROR_CODE_NO_FREE_SPACE:I = -0x4

.field public static final ERROR_CODE_WRONG_PWD:I = -0x3

.field private static final TAG:Ljava/lang/String;

.field private static mFileBuffer:[B


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 51
    const-class v0, Lcom/samsung/android/app/memo/util/FileHelper;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/app/memo/util/FileHelper;->TAG:Ljava/lang/String;

    .line 61
    const/high16 v0, 0x100000

    new-array v0, v0, [B

    sput-object v0, Lcom/samsung/android/app/memo/util/FileHelper;->mFileBuffer:[B

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    .line 63
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 64
    sget-object v0, Lcom/samsung/android/app/memo/util/FileHelper;->TAG:Ljava/lang/String;

    sget-object v1, Lcom/samsung/android/app/memo/util/FileHelper;->TAG:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 65
    return-void
.end method

.method public static copy(Ljava/lang/String;Ljava/lang/String;)J
    .locals 29
    .param p0, "sourceUri"    # Ljava/lang/String;
    .param p1, "destPath"    # Ljava/lang/String;

    .prologue
    .line 124
    const-wide/16 v16, 0x0

    .line 125
    .local v16, "copied":J
    new-instance v18, Ljava/io/File;

    move-object/from16 v0, v18

    move-object/from16 v1, p1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 126
    .local v18, "destFile":Ljava/io/File;
    const/4 v10, 0x0

    .line 127
    .local v10, "bos":Ljava/io/BufferedOutputStream;
    const/4 v7, 0x0

    .line 128
    .local v7, "bis":Ljava/io/BufferedInputStream;
    invoke-static/range {p0 .. p0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v23

    .line 130
    .local v23, "srcURI":Landroid/net/Uri;
    :try_start_0
    invoke-virtual/range {v18 .. v18}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/io/File;->mkdirs()Z

    .line 131
    invoke-virtual/range {v18 .. v18}, Ljava/io/File;->createNewFile()Z

    .line 132
    new-instance v11, Ljava/io/BufferedOutputStream;

    new-instance v25, Ljava/io/FileOutputStream;

    move-object/from16 v0, v25

    move-object/from16 v1, v18

    invoke-direct {v0, v1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    move-object/from16 v0, v25

    invoke-direct {v11, v0}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_4
    .catchall {:try_start_0 .. :try_end_0} :catchall_3

    .line 133
    .end local v10    # "bos":Ljava/io/BufferedOutputStream;
    .local v11, "bos":Ljava/io/BufferedOutputStream;
    :try_start_1
    invoke-static/range {v23 .. v23}, Lcom/samsung/android/app/memo/util/ImageUtil;->isImageFile(Landroid/net/Uri;)Z

    move-result v25

    if-eqz v25, :cond_8

    .line 134
    const-string v25, "content://"

    move-object/from16 v0, p0

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v25

    if-nez v25, :cond_0

    const-string v25, "file://"

    move-object/from16 v0, p0

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v25

    if-nez v25, :cond_0

    const-string v25, "file:/"

    move-object/from16 v0, p0

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v25

    if-eqz v25, :cond_6

    .line 135
    :cond_0
    new-instance v8, Ljava/io/BufferedInputStream;

    invoke-static {}, Lcom/samsung/android/app/memo/util/Utils;->getAppContext()Landroid/content/Context;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v25

    .line 136
    move-object/from16 v0, v25

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;

    move-result-object v25

    .line 135
    move-object/from16 v0, v25

    invoke-direct {v8, v0}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_b
    .catchall {:try_start_1 .. :try_end_1} :catchall_6

    .line 137
    .end local v7    # "bis":Ljava/io/BufferedInputStream;
    .local v8, "bis":Ljava/io/BufferedInputStream;
    :try_start_2
    invoke-static/range {v23 .. v23}, Lcom/samsung/android/app/memo/util/ImageUtil;->lookupImageInfo(Landroid/net/Uri;)Landroid/os/Bundle;

    move-result-object v13

    .line 138
    .local v13, "bundle":Landroid/os/Bundle;
    sget-object v25, Lcom/samsung/android/app/memo/util/ImageUtil;->KEY_ORIENTATION:Ljava/lang/String;

    const/16 v26, 0x0

    move-object/from16 v0, v25

    move/from16 v1, v26

    invoke-virtual {v13, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v20

    .line 139
    .local v20, "orientation":I
    invoke-static {}, Lcom/samsung/android/app/memo/util/Utils;->getAppContext()Landroid/content/Context;

    move-result-object v25

    move-object/from16 v0, v25

    move-object/from16 v1, v23

    move/from16 v2, v20

    invoke-static {v0, v1, v2}, Lcom/samsung/android/app/memo/util/ImageUtil;->getActualImageWidth(Landroid/content/Context;Landroid/net/Uri;I)I

    move-result v22

    .line 140
    .local v22, "sourceWidth":I
    const/16 v25, 0x1000

    move/from16 v0, v22

    move/from16 v1, v25

    if-le v0, v1, :cond_4

    .line 141
    invoke-static {}, Lcom/samsung/android/app/memo/util/Utils;->getAppContext()Landroid/content/Context;

    move-result-object v25

    const/16 v26, -0x1

    const/16 v27, 0x1000

    move-object/from16 v0, v25

    move-object/from16 v1, v23

    move/from16 v2, v26

    move/from16 v3, v20

    move/from16 v4, v27

    invoke-static {v0, v1, v2, v3, v4}, Lcom/samsung/android/app/memo/util/ImageUtil;->decodeImageScaledForDB(Landroid/content/Context;Landroid/net/Uri;III)Landroid/graphics/Bitmap;

    move-result-object v9

    .line 142
    .local v9, "bmp":Landroid/graphics/Bitmap;
    new-instance v6, Ljava/io/FileOutputStream;

    move-object/from16 v0, v18

    invoke-direct {v6, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 143
    .local v6, "bbos":Ljava/io/FileOutputStream;
    sget-object v25, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v26, 0x64

    move-object/from16 v0, v25

    move/from16 v1, v26

    invoke-virtual {v9, v0, v1, v6}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 144
    invoke-virtual {v6}, Ljava/io/FileOutputStream;->flush()V

    .line 145
    invoke-virtual {v6}, Ljava/io/FileOutputStream;->close()V

    .line 146
    invoke-virtual {v9}, Landroid/graphics/Bitmap;->recycle()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_c
    .catchall {:try_start_2 .. :try_end_2} :catchall_7

    move-object v7, v8

    .line 199
    .end local v6    # "bbos":Ljava/io/FileOutputStream;
    .end local v8    # "bis":Ljava/io/BufferedInputStream;
    .end local v9    # "bmp":Landroid/graphics/Bitmap;
    .end local v13    # "bundle":Landroid/os/Bundle;
    .end local v20    # "orientation":I
    .end local v22    # "sourceWidth":I
    .restart local v7    # "bis":Ljava/io/BufferedInputStream;
    :cond_1
    :goto_0
    if-eqz v11, :cond_2

    .line 202
    :try_start_3
    invoke-virtual {v11}, Ljava/io/BufferedOutputStream;->flush()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_5

    .line 204
    :try_start_4
    invoke-virtual {v11}, Ljava/io/BufferedOutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_9

    .line 210
    :cond_2
    :goto_1
    if-eqz v7, :cond_14

    .line 212
    :try_start_5
    invoke-virtual {v7}, Ljava/io/BufferedInputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_a

    move-object v10, v11

    .end local v11    # "bos":Ljava/io/BufferedOutputStream;
    .restart local v10    # "bos":Ljava/io/BufferedOutputStream;
    :cond_3
    :goto_2
    move-wide/from16 v26, v16

    .line 218
    :goto_3
    return-wide v26

    .line 148
    .end local v7    # "bis":Ljava/io/BufferedInputStream;
    .end local v10    # "bos":Ljava/io/BufferedOutputStream;
    .restart local v8    # "bis":Ljava/io/BufferedInputStream;
    .restart local v11    # "bos":Ljava/io/BufferedOutputStream;
    .restart local v13    # "bundle":Landroid/os/Bundle;
    .restart local v20    # "orientation":I
    .restart local v22    # "sourceWidth":I
    :cond_4
    const/high16 v25, 0x100000

    :try_start_6
    move/from16 v0, v25

    new-array v12, v0, [B

    .line 150
    .local v12, "buf":[B
    :goto_4
    invoke-virtual {v8, v12}, Ljava/io/BufferedInputStream;->read([B)I

    move-result v21

    .local v21, "read":I
    const/16 v25, -0x1

    move/from16 v0, v21

    move/from16 v1, v25

    if-ne v0, v1, :cond_5

    move-object v7, v8

    .line 155
    .end local v8    # "bis":Ljava/io/BufferedInputStream;
    .restart local v7    # "bis":Ljava/io/BufferedInputStream;
    goto :goto_0

    .line 151
    .end local v7    # "bis":Ljava/io/BufferedInputStream;
    .restart local v8    # "bis":Ljava/io/BufferedInputStream;
    :cond_5
    const/16 v25, 0x0

    move/from16 v0, v25

    move/from16 v1, v21

    invoke-virtual {v11, v12, v0, v1}, Ljava/io/BufferedOutputStream;->write([BII)V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_c
    .catchall {:try_start_6 .. :try_end_6} :catchall_7

    .line 152
    move/from16 v0, v21

    int-to-long v0, v0

    move-wide/from16 v26, v0

    add-long v16, v16, v26

    goto :goto_4

    .line 156
    .end local v8    # "bis":Ljava/io/BufferedInputStream;
    .end local v12    # "buf":[B
    .end local v13    # "bundle":Landroid/os/Bundle;
    .end local v20    # "orientation":I
    .end local v21    # "read":I
    .end local v22    # "sourceWidth":I
    .restart local v7    # "bis":Ljava/io/BufferedInputStream;
    :cond_6
    :try_start_7
    new-instance v24, Ljava/net/URL;

    move-object/from16 v0, v24

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 157
    .local v24, "url":Ljava/net/URL;
    invoke-virtual/range {v24 .. v24}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v14

    check-cast v14, Ljava/net/HttpURLConnection;

    .line 158
    .local v14, "connection":Ljava/net/HttpURLConnection;
    const/16 v25, 0xbb8

    move/from16 v0, v25

    invoke-virtual {v14, v0}, Ljava/net/HttpURLConnection;->setReadTimeout(I)V

    .line 159
    const/16 v25, 0x7530

    move/from16 v0, v25

    invoke-virtual {v14, v0}, Ljava/net/HttpURLConnection;->setConnectTimeout(I)V

    .line 160
    new-instance v8, Ljava/io/BufferedInputStream;

    invoke-virtual {v14}, Ljava/net/HttpURLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v25

    move-object/from16 v0, v25

    invoke-direct {v8, v0}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_b
    .catchall {:try_start_7 .. :try_end_7} :catchall_6

    .line 161
    .end local v7    # "bis":Ljava/io/BufferedInputStream;
    .restart local v8    # "bis":Ljava/io/BufferedInputStream;
    const/high16 v25, 0x100000

    :try_start_8
    move/from16 v0, v25

    new-array v12, v0, [B

    .line 163
    .restart local v12    # "buf":[B
    :goto_5
    invoke-virtual {v8, v12}, Ljava/io/BufferedInputStream;->read([B)I

    move-result v21

    .restart local v21    # "read":I
    const/16 v25, -0x1

    move/from16 v0, v21

    move/from16 v1, v25

    if-ne v0, v1, :cond_7

    move-object v7, v8

    .line 168
    .end local v8    # "bis":Ljava/io/BufferedInputStream;
    .restart local v7    # "bis":Ljava/io/BufferedInputStream;
    goto :goto_0

    .line 164
    .end local v7    # "bis":Ljava/io/BufferedInputStream;
    .restart local v8    # "bis":Ljava/io/BufferedInputStream;
    :cond_7
    const/16 v25, 0x0

    move/from16 v0, v25

    move/from16 v1, v21

    invoke-virtual {v11, v12, v0, v1}, Ljava/io/BufferedOutputStream;->write([BII)V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_c
    .catchall {:try_start_8 .. :try_end_8} :catchall_7

    .line 165
    move/from16 v0, v21

    int-to-long v0, v0

    move-wide/from16 v26, v0

    add-long v16, v16, v26

    goto :goto_5

    .line 169
    .end local v8    # "bis":Ljava/io/BufferedInputStream;
    .end local v12    # "buf":[B
    .end local v14    # "connection":Ljava/net/HttpURLConnection;
    .end local v21    # "read":I
    .end local v24    # "url":Ljava/net/URL;
    .restart local v7    # "bis":Ljava/io/BufferedInputStream;
    :cond_8
    :try_start_9
    const-string v25, "content://"

    move-object/from16 v0, p0

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v25

    if-nez v25, :cond_9

    const-string v25, "file://"

    move-object/from16 v0, p0

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v25

    if-nez v25, :cond_9

    const-string v25, "file:/"

    move-object/from16 v0, p0

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v25

    if-eqz v25, :cond_a

    .line 170
    :cond_9
    new-instance v8, Ljava/io/BufferedInputStream;

    invoke-static {}, Lcom/samsung/android/app/memo/util/Utils;->getAppContext()Landroid/content/Context;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v25

    .line 171
    move-object/from16 v0, v25

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;

    move-result-object v25

    .line 170
    move-object/from16 v0, v25

    invoke-direct {v8, v0}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V

    .end local v7    # "bis":Ljava/io/BufferedInputStream;
    .restart local v8    # "bis":Ljava/io/BufferedInputStream;
    move-object v7, v8

    .line 189
    .end local v8    # "bis":Ljava/io/BufferedInputStream;
    .restart local v7    # "bis":Ljava/io/BufferedInputStream;
    :goto_6
    const/high16 v25, 0x100000

    move/from16 v0, v25

    new-array v12, v0, [B

    .line 191
    .restart local v12    # "buf":[B
    :goto_7
    invoke-virtual {v7, v12}, Ljava/io/BufferedInputStream;->read([B)I

    move-result v21

    .restart local v21    # "read":I
    const/16 v25, -0x1

    move/from16 v0, v21

    move/from16 v1, v25

    if-eq v0, v1, :cond_1

    .line 192
    const/16 v25, 0x0

    move/from16 v0, v25

    move/from16 v1, v21

    invoke-virtual {v11, v12, v0, v1}, Ljava/io/BufferedOutputStream;->write([BII)V

    .line 193
    move/from16 v0, v21

    int-to-long v0, v0

    move-wide/from16 v26, v0

    add-long v16, v16, v26

    goto :goto_7

    .line 172
    .end local v12    # "buf":[B
    .end local v21    # "read":I
    :cond_a
    const-string v25, "data:image/jpeg;base64,"

    move-object/from16 v0, p0

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v25

    if-eqz v25, :cond_d

    .line 173
    const/16 v25, 0x17

    move-object/from16 v0, p0

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v25

    .line 174
    const/16 v26, 0x0

    .line 173
    invoke-static/range {v25 .. v26}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v15

    .line 175
    .local v15, "decodedString":[B
    invoke-virtual {v11, v15}, Ljava/io/BufferedOutputStream;->write([B)V

    .line 176
    array-length v0, v15

    move/from16 v25, v0
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_b
    .catchall {:try_start_9 .. :try_end_9} :catchall_6

    move/from16 v0, v25

    int-to-long v0, v0

    move-wide/from16 v26, v0

    .line 199
    if-eqz v11, :cond_b

    .line 202
    :try_start_a
    invoke-virtual {v11}, Ljava/io/BufferedOutputStream;->flush()V
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    .line 204
    :try_start_b
    invoke-virtual {v11}, Ljava/io/BufferedOutputStream;->close()V
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_0

    .line 210
    :cond_b
    :goto_8
    if-eqz v7, :cond_c

    .line 212
    :try_start_c
    invoke-virtual {v7}, Ljava/io/BufferedInputStream;->close()V
    :try_end_c
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_1

    :cond_c
    :goto_9
    move-object v10, v11

    .line 176
    .end local v11    # "bos":Ljava/io/BufferedOutputStream;
    .restart local v10    # "bos":Ljava/io/BufferedOutputStream;
    goto/16 :goto_3

    .line 203
    .end local v10    # "bos":Ljava/io/BufferedOutputStream;
    .restart local v11    # "bos":Ljava/io/BufferedOutputStream;
    :catchall_0
    move-exception v25

    .line 204
    :try_start_d
    invoke-virtual {v11}, Ljava/io/BufferedOutputStream;->close()V

    .line 205
    throw v25
    :try_end_d
    .catch Ljava/io/IOException; {:try_start_d .. :try_end_d} :catch_0

    .line 206
    :catch_0
    move-exception v19

    .line 207
    .local v19, "e":Ljava/io/IOException;
    sget-object v25, Lcom/samsung/android/app/memo/util/FileHelper;->TAG:Ljava/lang/String;

    const-string v28, "copy()"

    move-object/from16 v0, v25

    move-object/from16 v1, v28

    move-object/from16 v2, v19

    invoke-static {v0, v1, v2}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_8

    .line 213
    .end local v19    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v19

    .line 214
    .restart local v19    # "e":Ljava/io/IOException;
    sget-object v25, Lcom/samsung/android/app/memo/util/FileHelper;->TAG:Ljava/lang/String;

    const-string v28, "copy()"

    move-object/from16 v0, v25

    move-object/from16 v1, v28

    move-object/from16 v2, v19

    invoke-static {v0, v1, v2}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_9

    .line 177
    .end local v15    # "decodedString":[B
    .end local v19    # "e":Ljava/io/IOException;
    :cond_d
    :try_start_e
    const-string v25, "data:image/png;base64,"

    move-object/from16 v0, p0

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v25

    if-eqz v25, :cond_10

    .line 178
    const/16 v25, 0x16

    move-object/from16 v0, p0

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v25

    .line 179
    const/16 v26, 0x0

    .line 178
    invoke-static/range {v25 .. v26}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v15

    .line 180
    .restart local v15    # "decodedString":[B
    invoke-virtual {v11, v15}, Ljava/io/BufferedOutputStream;->write([B)V

    .line 181
    array-length v0, v15

    move/from16 v25, v0
    :try_end_e
    .catch Ljava/io/IOException; {:try_start_e .. :try_end_e} :catch_b
    .catchall {:try_start_e .. :try_end_e} :catchall_6

    move/from16 v0, v25

    int-to-long v0, v0

    move-wide/from16 v26, v0

    .line 199
    if-eqz v11, :cond_e

    .line 202
    :try_start_f
    invoke-virtual {v11}, Ljava/io/BufferedOutputStream;->flush()V
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_1

    .line 204
    :try_start_10
    invoke-virtual {v11}, Ljava/io/BufferedOutputStream;->close()V
    :try_end_10
    .catch Ljava/io/IOException; {:try_start_10 .. :try_end_10} :catch_2

    .line 210
    :cond_e
    :goto_a
    if-eqz v7, :cond_f

    .line 212
    :try_start_11
    invoke-virtual {v7}, Ljava/io/BufferedInputStream;->close()V
    :try_end_11
    .catch Ljava/io/IOException; {:try_start_11 .. :try_end_11} :catch_3

    :cond_f
    :goto_b
    move-object v10, v11

    .line 181
    .end local v11    # "bos":Ljava/io/BufferedOutputStream;
    .restart local v10    # "bos":Ljava/io/BufferedOutputStream;
    goto/16 :goto_3

    .line 203
    .end local v10    # "bos":Ljava/io/BufferedOutputStream;
    .restart local v11    # "bos":Ljava/io/BufferedOutputStream;
    :catchall_1
    move-exception v25

    .line 204
    :try_start_12
    invoke-virtual {v11}, Ljava/io/BufferedOutputStream;->close()V

    .line 205
    throw v25
    :try_end_12
    .catch Ljava/io/IOException; {:try_start_12 .. :try_end_12} :catch_2

    .line 206
    :catch_2
    move-exception v19

    .line 207
    .restart local v19    # "e":Ljava/io/IOException;
    sget-object v25, Lcom/samsung/android/app/memo/util/FileHelper;->TAG:Ljava/lang/String;

    const-string v28, "copy()"

    move-object/from16 v0, v25

    move-object/from16 v1, v28

    move-object/from16 v2, v19

    invoke-static {v0, v1, v2}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_a

    .line 213
    .end local v19    # "e":Ljava/io/IOException;
    :catch_3
    move-exception v19

    .line 214
    .restart local v19    # "e":Ljava/io/IOException;
    sget-object v25, Lcom/samsung/android/app/memo/util/FileHelper;->TAG:Ljava/lang/String;

    const-string v28, "copy()"

    move-object/from16 v0, v25

    move-object/from16 v1, v28

    move-object/from16 v2, v19

    invoke-static {v0, v1, v2}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_b

    .line 183
    .end local v15    # "decodedString":[B
    .end local v19    # "e":Ljava/io/IOException;
    :cond_10
    :try_start_13
    new-instance v24, Ljava/net/URL;

    move-object/from16 v0, v24

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 184
    .restart local v24    # "url":Ljava/net/URL;
    invoke-virtual/range {v24 .. v24}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v14

    check-cast v14, Ljava/net/HttpURLConnection;

    .line 185
    .restart local v14    # "connection":Ljava/net/HttpURLConnection;
    const/16 v25, 0xbb8

    move/from16 v0, v25

    invoke-virtual {v14, v0}, Ljava/net/HttpURLConnection;->setReadTimeout(I)V

    .line 186
    const/16 v25, 0x7530

    move/from16 v0, v25

    invoke-virtual {v14, v0}, Ljava/net/HttpURLConnection;->setConnectTimeout(I)V

    .line 187
    new-instance v8, Ljava/io/BufferedInputStream;

    invoke-virtual {v14}, Ljava/net/HttpURLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v25

    move-object/from16 v0, v25

    invoke-direct {v8, v0}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_13
    .catch Ljava/io/IOException; {:try_start_13 .. :try_end_13} :catch_b
    .catchall {:try_start_13 .. :try_end_13} :catchall_6

    .end local v7    # "bis":Ljava/io/BufferedInputStream;
    .restart local v8    # "bis":Ljava/io/BufferedInputStream;
    move-object v7, v8

    .end local v8    # "bis":Ljava/io/BufferedInputStream;
    .restart local v7    # "bis":Ljava/io/BufferedInputStream;
    goto/16 :goto_6

    .line 196
    .end local v11    # "bos":Ljava/io/BufferedOutputStream;
    .end local v14    # "connection":Ljava/net/HttpURLConnection;
    .end local v24    # "url":Ljava/net/URL;
    .restart local v10    # "bos":Ljava/io/BufferedOutputStream;
    :catch_4
    move-exception v19

    .line 197
    .restart local v19    # "e":Ljava/io/IOException;
    :goto_c
    :try_start_14
    sget-object v25, Lcom/samsung/android/app/memo/util/FileHelper;->TAG:Ljava/lang/String;

    new-instance v26, Ljava/lang/StringBuilder;

    const-string v27, "copy() from "

    invoke-direct/range {v26 .. v27}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v26

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    const-string v27, " to "

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v18 .. v18}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v27

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    move-object/from16 v0, v25

    move-object/from16 v1, v26

    move-object/from16 v2, v19

    invoke-static {v0, v1, v2}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_14
    .catchall {:try_start_14 .. :try_end_14} :catchall_3

    .line 199
    if-eqz v10, :cond_11

    .line 202
    :try_start_15
    invoke-virtual {v10}, Ljava/io/BufferedOutputStream;->flush()V
    :try_end_15
    .catchall {:try_start_15 .. :try_end_15} :catchall_2

    .line 204
    :try_start_16
    invoke-virtual {v10}, Ljava/io/BufferedOutputStream;->close()V
    :try_end_16
    .catch Ljava/io/IOException; {:try_start_16 .. :try_end_16} :catch_6

    .line 210
    :cond_11
    :goto_d
    if-eqz v7, :cond_3

    .line 212
    :try_start_17
    invoke-virtual {v7}, Ljava/io/BufferedInputStream;->close()V
    :try_end_17
    .catch Ljava/io/IOException; {:try_start_17 .. :try_end_17} :catch_5

    goto/16 :goto_2

    .line 213
    :catch_5
    move-exception v19

    .line 214
    sget-object v25, Lcom/samsung/android/app/memo/util/FileHelper;->TAG:Ljava/lang/String;

    const-string v26, "copy()"

    move-object/from16 v0, v25

    move-object/from16 v1, v26

    move-object/from16 v2, v19

    invoke-static {v0, v1, v2}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_2

    .line 203
    :catchall_2
    move-exception v25

    .line 204
    :try_start_18
    invoke-virtual {v10}, Ljava/io/BufferedOutputStream;->close()V

    .line 205
    throw v25
    :try_end_18
    .catch Ljava/io/IOException; {:try_start_18 .. :try_end_18} :catch_6

    .line 206
    :catch_6
    move-exception v19

    .line 207
    sget-object v25, Lcom/samsung/android/app/memo/util/FileHelper;->TAG:Ljava/lang/String;

    const-string v26, "copy()"

    move-object/from16 v0, v25

    move-object/from16 v1, v26

    move-object/from16 v2, v19

    invoke-static {v0, v1, v2}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_d

    .line 198
    .end local v19    # "e":Ljava/io/IOException;
    :catchall_3
    move-exception v25

    .line 199
    :goto_e
    if-eqz v10, :cond_12

    .line 202
    :try_start_19
    invoke-virtual {v10}, Ljava/io/BufferedOutputStream;->flush()V
    :try_end_19
    .catchall {:try_start_19 .. :try_end_19} :catchall_4

    .line 204
    :try_start_1a
    invoke-virtual {v10}, Ljava/io/BufferedOutputStream;->close()V
    :try_end_1a
    .catch Ljava/io/IOException; {:try_start_1a .. :try_end_1a} :catch_7

    .line 210
    :cond_12
    :goto_f
    if-eqz v7, :cond_13

    .line 212
    :try_start_1b
    invoke-virtual {v7}, Ljava/io/BufferedInputStream;->close()V
    :try_end_1b
    .catch Ljava/io/IOException; {:try_start_1b .. :try_end_1b} :catch_8

    .line 216
    :cond_13
    :goto_10
    throw v25

    .line 203
    :catchall_4
    move-exception v26

    .line 204
    :try_start_1c
    invoke-virtual {v10}, Ljava/io/BufferedOutputStream;->close()V

    .line 205
    throw v26
    :try_end_1c
    .catch Ljava/io/IOException; {:try_start_1c .. :try_end_1c} :catch_7

    .line 206
    :catch_7
    move-exception v19

    .line 207
    .restart local v19    # "e":Ljava/io/IOException;
    sget-object v26, Lcom/samsung/android/app/memo/util/FileHelper;->TAG:Ljava/lang/String;

    const-string v27, "copy()"

    move-object/from16 v0, v26

    move-object/from16 v1, v27

    move-object/from16 v2, v19

    invoke-static {v0, v1, v2}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_f

    .line 213
    .end local v19    # "e":Ljava/io/IOException;
    :catch_8
    move-exception v19

    .line 214
    .restart local v19    # "e":Ljava/io/IOException;
    sget-object v26, Lcom/samsung/android/app/memo/util/FileHelper;->TAG:Ljava/lang/String;

    const-string v27, "copy()"

    move-object/from16 v0, v26

    move-object/from16 v1, v27

    move-object/from16 v2, v19

    invoke-static {v0, v1, v2}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_10

    .line 203
    .end local v10    # "bos":Ljava/io/BufferedOutputStream;
    .end local v19    # "e":Ljava/io/IOException;
    .restart local v11    # "bos":Ljava/io/BufferedOutputStream;
    :catchall_5
    move-exception v25

    .line 204
    :try_start_1d
    invoke-virtual {v11}, Ljava/io/BufferedOutputStream;->close()V

    .line 205
    throw v25
    :try_end_1d
    .catch Ljava/io/IOException; {:try_start_1d .. :try_end_1d} :catch_9

    .line 206
    :catch_9
    move-exception v19

    .line 207
    .restart local v19    # "e":Ljava/io/IOException;
    sget-object v25, Lcom/samsung/android/app/memo/util/FileHelper;->TAG:Ljava/lang/String;

    const-string v26, "copy()"

    move-object/from16 v0, v25

    move-object/from16 v1, v26

    move-object/from16 v2, v19

    invoke-static {v0, v1, v2}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_1

    .line 213
    .end local v19    # "e":Ljava/io/IOException;
    :catch_a
    move-exception v19

    .line 214
    .restart local v19    # "e":Ljava/io/IOException;
    sget-object v25, Lcom/samsung/android/app/memo/util/FileHelper;->TAG:Ljava/lang/String;

    const-string v26, "copy()"

    move-object/from16 v0, v25

    move-object/from16 v1, v26

    move-object/from16 v2, v19

    invoke-static {v0, v1, v2}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .end local v19    # "e":Ljava/io/IOException;
    :cond_14
    move-object v10, v11

    .end local v11    # "bos":Ljava/io/BufferedOutputStream;
    .restart local v10    # "bos":Ljava/io/BufferedOutputStream;
    goto/16 :goto_2

    .line 198
    .end local v10    # "bos":Ljava/io/BufferedOutputStream;
    .restart local v11    # "bos":Ljava/io/BufferedOutputStream;
    :catchall_6
    move-exception v25

    move-object v10, v11

    .end local v11    # "bos":Ljava/io/BufferedOutputStream;
    .restart local v10    # "bos":Ljava/io/BufferedOutputStream;
    goto :goto_e

    .end local v7    # "bis":Ljava/io/BufferedInputStream;
    .end local v10    # "bos":Ljava/io/BufferedOutputStream;
    .restart local v8    # "bis":Ljava/io/BufferedInputStream;
    .restart local v11    # "bos":Ljava/io/BufferedOutputStream;
    :catchall_7
    move-exception v25

    move-object v7, v8

    .end local v8    # "bis":Ljava/io/BufferedInputStream;
    .restart local v7    # "bis":Ljava/io/BufferedInputStream;
    move-object v10, v11

    .end local v11    # "bos":Ljava/io/BufferedOutputStream;
    .restart local v10    # "bos":Ljava/io/BufferedOutputStream;
    goto :goto_e

    .line 196
    .end local v10    # "bos":Ljava/io/BufferedOutputStream;
    .restart local v11    # "bos":Ljava/io/BufferedOutputStream;
    :catch_b
    move-exception v19

    move-object v10, v11

    .end local v11    # "bos":Ljava/io/BufferedOutputStream;
    .restart local v10    # "bos":Ljava/io/BufferedOutputStream;
    goto/16 :goto_c

    .end local v7    # "bis":Ljava/io/BufferedInputStream;
    .end local v10    # "bos":Ljava/io/BufferedOutputStream;
    .restart local v8    # "bis":Ljava/io/BufferedInputStream;
    .restart local v11    # "bos":Ljava/io/BufferedOutputStream;
    :catch_c
    move-exception v19

    move-object v7, v8

    .end local v8    # "bis":Ljava/io/BufferedInputStream;
    .restart local v7    # "bis":Ljava/io/BufferedInputStream;
    move-object v10, v11

    .end local v11    # "bos":Ljava/io/BufferedOutputStream;
    .restart local v10    # "bos":Ljava/io/BufferedOutputStream;
    goto/16 :goto_c
.end method

.method public static copyFileItem(Ljava/io/File;Ljava/io/File;)I
    .locals 14
    .param p0, "sourceLocation"    # Ljava/io/File;
    .param p1, "targetLocation"    # Ljava/io/File;

    .prologue
    .line 286
    const/4 v10, 0x0

    .line 287
    .local v10, "res":I
    invoke-virtual {p0}, Ljava/io/File;->exists()Z

    move-result v11

    if-nez v11, :cond_0

    .line 288
    const/4 v11, 0x0

    .line 332
    :goto_0
    return v11

    .line 289
    :cond_0
    invoke-virtual {p0}, Ljava/io/File;->isDirectory()Z

    move-result v11

    if-eqz v11, :cond_4

    .line 290
    invoke-virtual {p1}, Ljava/io/File;->exists()Z

    move-result v11

    if-nez v11, :cond_1

    invoke-virtual {p1}, Ljava/io/File;->mkdirs()Z

    move-result v11

    if-nez v11, :cond_1

    .line 291
    sget-object v11, Lcom/samsung/android/app/memo/util/FileHelper;->TAG:Ljava/lang/String;

    new-instance v12, Ljava/lang/StringBuilder;

    const-string v13, "Error occurred during creating folders in "

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 292
    const/4 v11, -0x1

    goto :goto_0

    .line 294
    :cond_1
    invoke-virtual {p0}, Ljava/io/File;->list()[Ljava/lang/String;

    move-result-object v1

    .line 295
    .local v1, "children":[Ljava/lang/String;
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_1
    array-length v11, v1

    if-lt v4, v11, :cond_3

    .end local v1    # "children":[Ljava/lang/String;
    .end local v4    # "i":I
    :cond_2
    :goto_2
    move v11, v10

    .line 332
    goto :goto_0

    .line 296
    .restart local v1    # "children":[Ljava/lang/String;
    .restart local v4    # "i":I
    :cond_3
    new-instance v11, Ljava/io/File;

    aget-object v12, v1, v4

    invoke-direct {v11, p0, v12}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    new-instance v12, Ljava/io/File;

    .line 297
    aget-object v13, v1, v4

    invoke-direct {v12, p1, v13}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 296
    invoke-static {v11, v12}, Lcom/samsung/android/app/memo/util/FileHelper;->copyFileItem(Ljava/io/File;Ljava/io/File;)I

    .line 295
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 300
    .end local v1    # "children":[Ljava/lang/String;
    .end local v4    # "i":I
    :cond_4
    invoke-virtual {p1}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v2

    .line 301
    .local v2, "directory":Ljava/io/File;
    if-eqz v2, :cond_5

    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v11

    if-nez v11, :cond_5

    invoke-virtual {v2}, Ljava/io/File;->mkdirs()Z

    move-result v11

    if-nez v11, :cond_5

    .line 302
    sget-object v11, Lcom/samsung/android/app/memo/util/FileHelper;->TAG:Ljava/lang/String;

    new-instance v12, Ljava/lang/StringBuilder;

    const-string v13, "Error occurred during creating folders in "

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 303
    const/4 v11, -0x1

    goto :goto_0

    .line 305
    :cond_5
    const/4 v5, 0x0

    .line 306
    .local v5, "in":Ljava/io/InputStream;
    const/4 v8, 0x0

    .line 308
    .local v8, "out":Ljava/io/OutputStream;
    :try_start_0
    new-instance v6, Ljava/io/FileInputStream;

    invoke-direct {v6, p0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_4
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 309
    .end local v5    # "in":Ljava/io/InputStream;
    .local v6, "in":Ljava/io/InputStream;
    :try_start_1
    new-instance v9, Ljava/io/FileOutputStream;

    invoke-direct {v9, p1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_5
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 310
    .end local v8    # "out":Ljava/io/OutputStream;
    .local v9, "out":Ljava/io/OutputStream;
    const/16 v11, 0x400

    :try_start_2
    new-array v0, v11, [B

    .line 312
    .local v0, "buf":[B
    :goto_3
    invoke-virtual {v6, v0}, Ljava/io/InputStream;->read([B)I

    move-result v7

    .local v7, "len":I
    if-gtz v7, :cond_7

    .line 314
    invoke-virtual {v6}, Ljava/io/InputStream;->close()V

    .line 315
    invoke-virtual {v9}, Ljava/io/OutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 323
    if-eqz v6, :cond_6

    .line 324
    :try_start_3
    invoke-virtual {v6}, Ljava/io/InputStream;->close()V

    .line 325
    :cond_6
    if-eqz v9, :cond_2

    .line 326
    invoke-virtual {v9}, Ljava/io/OutputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    goto :goto_2

    .line 327
    :catch_0
    move-exception v3

    .line 328
    .local v3, "e":Ljava/io/IOException;
    sget-object v11, Lcom/samsung/android/app/memo/util/FileHelper;->TAG:Ljava/lang/String;

    const-string v12, "Error occurred during closing the streams."

    invoke-static {v11, v12, v3}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_2

    .line 313
    .end local v3    # "e":Ljava/io/IOException;
    :cond_7
    const/4 v11, 0x0

    :try_start_4
    invoke-virtual {v9, v0, v11, v7}, Ljava/io/OutputStream;->write([BII)V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    goto :goto_3

    .line 316
    .end local v0    # "buf":[B
    .end local v7    # "len":I
    :catch_1
    move-exception v3

    move-object v8, v9

    .end local v9    # "out":Ljava/io/OutputStream;
    .restart local v8    # "out":Ljava/io/OutputStream;
    move-object v5, v6

    .line 317
    .end local v6    # "in":Ljava/io/InputStream;
    .restart local v3    # "e":Ljava/io/IOException;
    .restart local v5    # "in":Ljava/io/InputStream;
    :goto_4
    :try_start_5
    sget-object v11, Lcom/samsung/android/app/memo/util/FileHelper;->TAG:Ljava/lang/String;

    const-string v12, "Error occurred during copying the file."

    invoke-static {v11, v12, v3}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 318
    const/4 v10, -0x1

    .line 319
    invoke-virtual {v3}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v11

    const-string v12, "ENOSPC"

    invoke-virtual {v11, v12}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    move-result v11

    if-eqz v11, :cond_8

    .line 320
    const/4 v10, -0x4

    .line 323
    :cond_8
    if-eqz v5, :cond_9

    .line 324
    :try_start_6
    invoke-virtual {v5}, Ljava/io/InputStream;->close()V

    .line 325
    :cond_9
    if-eqz v8, :cond_2

    .line 326
    invoke-virtual {v8}, Ljava/io/OutputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_2

    goto/16 :goto_2

    .line 327
    :catch_2
    move-exception v3

    .line 328
    sget-object v11, Lcom/samsung/android/app/memo/util/FileHelper;->TAG:Ljava/lang/String;

    const-string v12, "Error occurred during closing the streams."

    invoke-static {v11, v12, v3}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_2

    .line 321
    .end local v3    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v11

    .line 323
    :goto_5
    if-eqz v5, :cond_a

    .line 324
    :try_start_7
    invoke-virtual {v5}, Ljava/io/InputStream;->close()V

    .line 325
    :cond_a
    if-eqz v8, :cond_b

    .line 326
    invoke-virtual {v8}, Ljava/io/OutputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_3

    .line 330
    :cond_b
    :goto_6
    throw v11

    .line 327
    :catch_3
    move-exception v3

    .line 328
    .restart local v3    # "e":Ljava/io/IOException;
    sget-object v12, Lcom/samsung/android/app/memo/util/FileHelper;->TAG:Ljava/lang/String;

    const-string v13, "Error occurred during closing the streams."

    invoke-static {v12, v13, v3}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_6

    .line 321
    .end local v3    # "e":Ljava/io/IOException;
    .end local v5    # "in":Ljava/io/InputStream;
    .restart local v6    # "in":Ljava/io/InputStream;
    :catchall_1
    move-exception v11

    move-object v5, v6

    .end local v6    # "in":Ljava/io/InputStream;
    .restart local v5    # "in":Ljava/io/InputStream;
    goto :goto_5

    .end local v5    # "in":Ljava/io/InputStream;
    .end local v8    # "out":Ljava/io/OutputStream;
    .restart local v6    # "in":Ljava/io/InputStream;
    .restart local v9    # "out":Ljava/io/OutputStream;
    :catchall_2
    move-exception v11

    move-object v8, v9

    .end local v9    # "out":Ljava/io/OutputStream;
    .restart local v8    # "out":Ljava/io/OutputStream;
    move-object v5, v6

    .end local v6    # "in":Ljava/io/InputStream;
    .restart local v5    # "in":Ljava/io/InputStream;
    goto :goto_5

    .line 316
    :catch_4
    move-exception v3

    goto :goto_4

    .end local v5    # "in":Ljava/io/InputStream;
    .restart local v6    # "in":Ljava/io/InputStream;
    :catch_5
    move-exception v3

    move-object v5, v6

    .end local v6    # "in":Ljava/io/InputStream;
    .restart local v5    # "in":Ljava/io/InputStream;
    goto :goto_4
.end method

.method public static createPrivateFile(Ljava/lang/String;)Z
    .locals 6
    .param p0, "uuid"    # Ljava/lang/String;

    .prologue
    .line 94
    const/4 v2, 0x0

    .line 96
    .local v2, "ret":Z
    :try_start_0
    invoke-static {p0}, Lcom/samsung/android/app/memo/util/FileHelper;->getPrivateFile(Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    .line 97
    .local v0, "f":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    .line 98
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 99
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 101
    :cond_0
    invoke-virtual {v0}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v3

    invoke-virtual {v3}, Ljava/io/File;->mkdirs()Z

    .line 102
    invoke-virtual {v0}, Ljava/io/File;->createNewFile()Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 106
    .end local v0    # "f":Ljava/io/File;
    :goto_0
    return v2

    .line 103
    :catch_0
    move-exception v1

    .line 104
    .local v1, "ioe":Ljava/io/IOException;
    sget-object v3, Lcom/samsung/android/app/memo/util/FileHelper;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "createPrivateFile() uuid: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v1}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private static createSnbDir(Ljava/lang/String;)V
    .locals 6
    .param p0, "path"    # Ljava/lang/String;

    .prologue
    .line 258
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 259
    .local v2, "targetFile":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v3

    if-nez v3, :cond_0

    .line 260
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v4

    invoke-virtual {p0, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 261
    .local v0, "isFolder":Ljava/lang/String;
    const-string v3, "/"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 262
    invoke-virtual {v2}, Ljava/io/File;->mkdirs()Z

    .line 270
    .end local v0    # "isFolder":Ljava/lang/String;
    :cond_0
    return-void

    .line 264
    .restart local v0    # "isFolder":Ljava/lang/String;
    :cond_1
    invoke-virtual {v2}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v1

    .line 265
    .local v1, "parent":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v3

    if-nez v3, :cond_0

    invoke-virtual {v1}, Ljava/io/File;->mkdirs()Z

    move-result v3

    if-nez v3, :cond_0

    .line 266
    new-instance v3, Ljava/lang/IllegalStateException;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Couldn\'t create dir: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v3
.end method

.method public static deleteFileItem(Ljava/io/File;)V
    .locals 4
    .param p0, "file"    # Ljava/io/File;

    .prologue
    .line 273
    if-eqz p0, :cond_1

    .line 274
    invoke-virtual {p0}, Ljava/io/File;->isDirectory()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 275
    invoke-virtual {p0}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v1

    .line 276
    .local v1, "items":[Ljava/io/File;
    if-eqz v1, :cond_0

    .line 277
    array-length v3, v1

    const/4 v2, 0x0

    :goto_0
    if-lt v2, v3, :cond_2

    .line 281
    .end local v1    # "items":[Ljava/io/File;
    :cond_0
    invoke-virtual {p0}, Ljava/io/File;->delete()Z

    .line 283
    :cond_1
    return-void

    .line 277
    .restart local v1    # "items":[Ljava/io/File;
    :cond_2
    aget-object v0, v1, v2

    .line 278
    .local v0, "child":Ljava/io/File;
    invoke-static {v0}, Lcom/samsung/android/app/memo/util/FileHelper;->deleteFileItem(Ljava/io/File;)V

    .line 277
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public static deletePrivateFiles(Ljava/util/List;)J
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)J"
        }
    .end annotation

    .prologue
    .line 110
    .local p0, "uuids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const-wide/16 v2, 0x0

    .line 111
    .local v2, "ret":J
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-nez v5, :cond_0

    .line 120
    return-wide v2

    .line 111
    :cond_0
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 112
    .local v1, "uuid":Ljava/lang/String;
    invoke-static {v1}, Lcom/samsung/android/app/memo/util/FileHelper;->getPrivateFile(Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    .line 113
    .local v0, "f":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 114
    const-wide/16 v6, 0x1

    add-long/2addr v2, v6

    .line 115
    :cond_1
    invoke-virtual {v0}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v0

    .line 116
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 117
    invoke-virtual {v0}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v0

    .line 118
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    goto :goto_0
.end method

.method public static getAttachFolder()Ljava/io/File;
    .locals 3

    .prologue
    .line 68
    invoke-static {}, Lcom/samsung/android/app/memo/util/Utils;->getAppContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "attach"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v0

    return-object v0
.end method

.method public static declared-synchronized getByteArr(Ljava/io/InputStream;)[B
    .locals 6
    .param p0, "inputStream"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 406
    const-class v4, Lcom/samsung/android/app/memo/util/FileHelper;

    monitor-enter v4

    :try_start_0
    new-instance v1, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v1}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 407
    .local v1, "byteOpStream":Ljava/io/ByteArrayOutputStream;
    const/16 v3, 0x400

    new-array v0, v3, [B

    .line 408
    .local v0, "buff":[B
    :goto_0
    const/4 v3, 0x0

    const/16 v5, 0x400

    invoke-virtual {p0, v0, v3, v5}, Ljava/io/InputStream;->read([BII)I

    move-result v2

    .local v2, "len":I
    const/4 v3, -0x1

    if-ne v2, v3, :cond_0

    .line 411
    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 412
    monitor-exit v4

    return-object v0

    .line 409
    :cond_0
    const/4 v3, 0x0

    :try_start_1
    invoke-virtual {v1, v0, v3, v2}, Ljava/io/ByteArrayOutputStream;->write([BII)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 406
    .end local v0    # "buff":[B
    .end local v1    # "byteOpStream":Ljava/io/ByteArrayOutputStream;
    .end local v2    # "len":I
    :catchall_0
    move-exception v3

    monitor-exit v4

    throw v3
.end method

.method public static getPrivateFile(Ljava/lang/String;)Ljava/io/File;
    .locals 8
    .param p0, "uuid"    # Ljava/lang/String;

    .prologue
    .line 75
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v5

    .line 76
    .local v5, "len":I
    add-int/lit8 v6, v5, -0x2

    invoke-virtual {p0, v6}, Ljava/lang/String;->charAt(I)C

    move-result v6

    invoke-static {v6}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v0

    .line 77
    .local v0, "dir1":Ljava/lang/String;
    add-int/lit8 v6, v5, -0x1

    invoke-virtual {p0, v6}, Ljava/lang/String;->charAt(I)C

    move-result v6

    invoke-static {v6}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v1

    .line 78
    .local v1, "dir2":Ljava/lang/String;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v7, ".blob"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 80
    .local v4, "filename":Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/app/memo/util/FileHelper;->getAttachFolder()Ljava/io/File;

    move-result-object v2

    .line 82
    .local v2, "f":Ljava/io/File;
    new-instance v3, Ljava/io/File;

    invoke-direct {v3, v2, v0}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 83
    .end local v2    # "f":Ljava/io/File;
    .local v3, "f":Ljava/io/File;
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v3, v1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 85
    .end local v3    # "f":Ljava/io/File;
    .restart local v2    # "f":Ljava/io/File;
    new-instance v3, Ljava/io/File;

    invoke-direct {v3, v2, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 86
    .end local v2    # "f":Ljava/io/File;
    .restart local v3    # "f":Ljava/io/File;
    return-object v3
.end method

.method public static getPrivateFilePath(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "uuid"    # Ljava/lang/String;

    .prologue
    .line 90
    invoke-static {p0}, Lcom/samsung/android/app/memo/util/FileHelper;->getPrivateFile(Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static isEncrypted(Ljava/lang/String;)Z
    .locals 5
    .param p0, "path"    # Ljava/lang/String;

    .prologue
    .line 336
    const/4 v1, 0x0

    .line 338
    .local v1, "result":Z
    :try_start_0
    new-instance v2, Lnet/lingala/zip4j/core/ZipFile;

    new-instance v3, Ljava/io/File;

    invoke-direct {v3, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-direct {v2, v3}, Lnet/lingala/zip4j/core/ZipFile;-><init>(Ljava/io/File;)V

    .line 339
    .local v2, "zipFile":Lnet/lingala/zip4j/core/ZipFile;
    invoke-virtual {v2}, Lnet/lingala/zip4j/core/ZipFile;->isEncrypted()Z
    :try_end_0
    .catch Lnet/lingala/zip4j/exception/ZipException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 343
    .end local v2    # "zipFile":Lnet/lingala/zip4j/core/ZipFile;
    :goto_0
    return v1

    .line 340
    :catch_0
    move-exception v0

    .line 341
    .local v0, "e":Lnet/lingala/zip4j/exception/ZipException;
    sget-object v3, Lcom/samsung/android/app/memo/util/FileHelper;->TAG:Ljava/lang/String;

    const-string v4, "Exception occurred during ZipFile access."

    invoke-static {v3, v4, v0}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public static unzip(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
    .locals 5
    .param p0, "src"    # Ljava/lang/String;
    .param p1, "targetDir"    # Ljava/lang/String;
    .param p2, "password"    # Ljava/lang/String;

    .prologue
    .line 347
    const/4 v1, 0x0

    .line 349
    .local v1, "res":I
    :try_start_0
    new-instance v2, Lnet/lingala/zip4j/core/ZipFile;

    invoke-direct {v2, p0}, Lnet/lingala/zip4j/core/ZipFile;-><init>(Ljava/lang/String;)V

    .line 350
    .local v2, "zipFile":Lnet/lingala/zip4j/core/ZipFile;
    invoke-virtual {v2}, Lnet/lingala/zip4j/core/ZipFile;->isEncrypted()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 351
    if-eqz p2, :cond_1

    invoke-virtual {p2}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_1

    .line 352
    invoke-virtual {v2, p2}, Lnet/lingala/zip4j/core/ZipFile;->setPassword(Ljava/lang/String;)V

    .line 353
    invoke-virtual {v2, p1}, Lnet/lingala/zip4j/core/ZipFile;->extractAll(Ljava/lang/String;)V

    .line 366
    .end local v2    # "zipFile":Lnet/lingala/zip4j/core/ZipFile;
    :cond_0
    :goto_0
    return v1

    .line 355
    .restart local v2    # "zipFile":Lnet/lingala/zip4j/core/ZipFile;
    :cond_1
    const/4 v1, -0x1

    .line 356
    goto :goto_0

    .line 357
    :cond_2
    invoke-virtual {v2, p1}, Lnet/lingala/zip4j/core/ZipFile;->extractAll(Ljava/lang/String;)V
    :try_end_0
    .catch Lnet/lingala/zip4j/exception/ZipException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 358
    .end local v2    # "zipFile":Lnet/lingala/zip4j/core/ZipFile;
    :catch_0
    move-exception v0

    .line 359
    .local v0, "e":Lnet/lingala/zip4j/exception/ZipException;
    sget-object v3, Lcom/samsung/android/app/memo/util/FileHelper;->TAG:Ljava/lang/String;

    const-string v4, "Exception during unzip: file is locked, corrupted or there is not enough space to extract."

    invoke-static {v3, v4, v0}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 360
    const/4 v1, -0x1

    .line 361
    invoke-virtual {v0}, Lnet/lingala/zip4j/exception/ZipException;->getMessage()Ljava/lang/String;

    move-result-object v3

    const-string v4, "Wrong Password"

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 362
    const/4 v1, -0x3

    goto :goto_0

    .line 363
    :cond_3
    invoke-virtual {v0}, Lnet/lingala/zip4j/exception/ZipException;->getMessage()Ljava/lang/String;

    move-result-object v3

    const-string v4, "ENOSPC"

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 364
    const/4 v1, -0x4

    goto :goto_0
.end method

.method private static writeFile(Ljava/io/InputStream;Ljava/lang/String;)V
    .locals 8
    .param p0, "inputStream"    # Ljava/io/InputStream;
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 235
    invoke-static {p1}, Lcom/samsung/android/app/memo/util/FileHelper;->createSnbDir(Ljava/lang/String;)V

    .line 236
    const/4 v3, 0x0

    .line 238
    .local v3, "out":Ljava/io/OutputStream;
    :try_start_0
    new-instance v4, Ljava/io/FileOutputStream;

    invoke-direct {v4, p1}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_4
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 239
    .end local v3    # "out":Ljava/io/OutputStream;
    .local v4, "out":Ljava/io/OutputStream;
    const/16 v5, 0x400

    :try_start_1
    new-array v0, v5, [B

    .line 241
    .local v0, "buffer":[B
    :goto_0
    invoke-virtual {p0, v0}, Ljava/io/InputStream;->read([B)I
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v2

    .local v2, "len":I
    if-gtz v2, :cond_2

    .line 247
    if-eqz v4, :cond_0

    .line 248
    :try_start_2
    invoke-virtual {v4}, Ljava/io/OutputStream;->close()V

    .line 249
    :cond_0
    if-eqz p0, :cond_6

    .line 250
    invoke-virtual {p0}, Ljava/io/InputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_3

    move-object v3, v4

    .line 255
    .end local v0    # "buffer":[B
    .end local v2    # "len":I
    .end local v4    # "out":Ljava/io/OutputStream;
    .restart local v3    # "out":Ljava/io/OutputStream;
    :cond_1
    :goto_1
    return-void

    .line 242
    .end local v3    # "out":Ljava/io/OutputStream;
    .restart local v0    # "buffer":[B
    .restart local v2    # "len":I
    .restart local v4    # "out":Ljava/io/OutputStream;
    :cond_2
    const/4 v5, 0x0

    :try_start_3
    invoke-virtual {v4, v0, v5, v2}, Ljava/io/OutputStream;->write([BII)V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_0

    .line 243
    .end local v0    # "buffer":[B
    .end local v2    # "len":I
    :catch_0
    move-exception v1

    move-object v3, v4

    .line 244
    .end local v4    # "out":Ljava/io/OutputStream;
    .local v1, "e":Ljava/io/IOException;
    .restart local v3    # "out":Ljava/io/OutputStream;
    :goto_2
    :try_start_4
    sget-object v5, Lcom/samsung/android/app/memo/util/FileHelper;->TAG:Ljava/lang/String;

    const-string v6, "An error occurred during writing to a file."

    invoke-static {v5, v6, v1}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 247
    if-eqz v3, :cond_3

    .line 248
    :try_start_5
    invoke-virtual {v3}, Ljava/io/OutputStream;->close()V

    .line 249
    :cond_3
    if-eqz p0, :cond_1

    .line 250
    invoke-virtual {p0}, Ljava/io/InputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_1

    goto :goto_1

    .line 251
    :catch_1
    move-exception v1

    .line 252
    sget-object v5, Lcom/samsung/android/app/memo/util/FileHelper;->TAG:Ljava/lang/String;

    const-string v6, "An error occurred during closing a stream."

    invoke-static {v5, v6, v1}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 245
    .end local v1    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v5

    .line 247
    :goto_3
    if-eqz v3, :cond_4

    .line 248
    :try_start_6
    invoke-virtual {v3}, Ljava/io/OutputStream;->close()V

    .line 249
    :cond_4
    if-eqz p0, :cond_5

    .line 250
    invoke-virtual {p0}, Ljava/io/InputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_2

    .line 254
    :cond_5
    :goto_4
    throw v5

    .line 251
    :catch_2
    move-exception v1

    .line 252
    .restart local v1    # "e":Ljava/io/IOException;
    sget-object v6, Lcom/samsung/android/app/memo/util/FileHelper;->TAG:Ljava/lang/String;

    const-string v7, "An error occurred during closing a stream."

    invoke-static {v6, v7, v1}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_4

    .line 251
    .end local v1    # "e":Ljava/io/IOException;
    .end local v3    # "out":Ljava/io/OutputStream;
    .restart local v0    # "buffer":[B
    .restart local v2    # "len":I
    .restart local v4    # "out":Ljava/io/OutputStream;
    :catch_3
    move-exception v1

    .line 252
    .restart local v1    # "e":Ljava/io/IOException;
    sget-object v5, Lcom/samsung/android/app/memo/util/FileHelper;->TAG:Ljava/lang/String;

    const-string v6, "An error occurred during closing a stream."

    invoke-static {v5, v6, v1}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .end local v1    # "e":Ljava/io/IOException;
    :cond_6
    move-object v3, v4

    .end local v4    # "out":Ljava/io/OutputStream;
    .restart local v3    # "out":Ljava/io/OutputStream;
    goto :goto_1

    .line 245
    .end local v0    # "buffer":[B
    .end local v2    # "len":I
    .end local v3    # "out":Ljava/io/OutputStream;
    .restart local v4    # "out":Ljava/io/OutputStream;
    :catchall_1
    move-exception v5

    move-object v3, v4

    .end local v4    # "out":Ljava/io/OutputStream;
    .restart local v3    # "out":Ljava/io/OutputStream;
    goto :goto_3

    .line 243
    :catch_4
    move-exception v1

    goto :goto_2
.end method

.method public static writeFileFromURI(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "data"    # Landroid/net/Uri;
    .param p2, "path"    # Ljava/lang/String;

    .prologue
    .line 227
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    invoke-virtual {v2, p1}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;

    move-result-object v1

    .line 228
    .local v1, "inputStream":Ljava/io/InputStream;
    invoke-static {v1, p2}, Lcom/samsung/android/app/memo/util/FileHelper;->writeFile(Ljava/io/InputStream;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 232
    .end local v1    # "inputStream":Ljava/io/InputStream;
    :goto_0
    return-void

    .line 229
    :catch_0
    move-exception v0

    .line 230
    .local v0, "e":Ljava/io/FileNotFoundException;
    sget-object v2, Lcom/samsung/android/app/memo/util/FileHelper;->TAG:Ljava/lang/String;

    const-string v3, "Error occurred during reading data from uri."

    invoke-static {v2, v3, v0}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public static declared-synchronized writeToFile(Ljava/io/InputStream;Ljava/lang/String;)V
    .locals 12
    .param p0, "inputStream"    # Ljava/io/InputStream;
    .param p1, "filepath"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 372
    const-class v9, Lcom/samsung/android/app/memo/util/FileHelper;

    monitor-enter v9

    :try_start_0
    const-string v8, "/"

    invoke-virtual {p1, v8}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v7

    .line 373
    .local v7, "split":[Ljava/lang/String;
    array-length v8, v7

    add-int/lit8 v8, v8, -0x1

    aget-object v2, v7, v8

    .line 374
    .local v2, "fileName":Ljava/lang/String;
    const/4 v8, 0x0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v10

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v11

    sub-int/2addr v10, v11

    invoke-virtual {p1, v8, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    .line 376
    .local v4, "folderPath":Ljava/lang/String;
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 377
    .local v1, "file":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v8

    if-nez v8, :cond_0

    .line 379
    invoke-virtual {v1}, Ljava/io/File;->mkdirs()Z

    move-result v6

    .line 380
    .local v6, "result":Z
    if-nez v6, :cond_0

    .line 382
    new-instance v8, Ljava/io/IOException;

    invoke-direct {v8}, Ljava/io/IOException;-><init>()V

    throw v8
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 397
    .end local v1    # "file":Ljava/io/File;
    .end local v2    # "fileName":Ljava/lang/String;
    .end local v4    # "folderPath":Ljava/lang/String;
    .end local v6    # "result":Z
    .end local v7    # "split":[Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 399
    .local v0, "e":Ljava/io/IOException;
    :try_start_1
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 372
    .end local v0    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v8

    monitor-exit v9

    throw v8

    .line 387
    .restart local v1    # "file":Ljava/io/File;
    .restart local v2    # "fileName":Ljava/lang/String;
    .restart local v4    # "folderPath":Ljava/lang/String;
    .restart local v7    # "split":[Ljava/lang/String;
    :cond_0
    :try_start_2
    new-instance v3, Ljava/io/FileOutputStream;

    invoke-direct {v3, p1}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 388
    .local v3, "fileOpStream":Ljava/io/FileOutputStream;
    const/4 v5, 0x0

    .line 391
    .local v5, "len":I
    :goto_0
    :try_start_3
    sget-object v8, Lcom/samsung/android/app/memo/util/FileHelper;->mFileBuffer:[B

    invoke-virtual {p0, v8}, Ljava/io/InputStream;->read([B)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    move-result v5

    if-gtz v5, :cond_1

    .line 395
    :try_start_4
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 401
    monitor-exit v9

    return-void

    .line 392
    :cond_1
    :try_start_5
    sget-object v8, Lcom/samsung/android/app/memo/util/FileHelper;->mFileBuffer:[B

    const/4 v10, 0x0

    invoke-virtual {v3, v8, v10, v5}, Ljava/io/FileOutputStream;->write([BII)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    goto :goto_0

    .line 394
    :catchall_1
    move-exception v8

    .line 395
    :try_start_6
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V

    .line 396
    throw v8
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_0
    .catchall {:try_start_6 .. :try_end_6} :catchall_0
.end method
