.class public Lcom/samsung/android/app/memo/FromClipboardReceiver;
.super Landroid/content/BroadcastReceiver;
.source "FromClipboardReceiver.java"


# static fields
.field private static final DEBUG:Z

.field private static mToast:Landroid/widget/Toast;


# instance fields
.field private mContent:Ljava/lang/String;

.field private mContentLen:I

.field private mContext:Landroid/content/Context;

.field private mTitle:Ljava/lang/String;

.field private mTitleLen:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 41
    const-string v2, "ro.debuggable"

    invoke-static {v2, v1}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v2

    if-ne v2, v0, :cond_0

    :goto_0
    sput-boolean v0, Lcom/samsung/android/app/memo/FromClipboardReceiver;->DEBUG:Z

    .line 53
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/android/app/memo/FromClipboardReceiver;->mToast:Landroid/widget/Toast;

    return-void

    :cond_0
    move v0, v1

    .line 41
    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, -0x1

    const/4 v0, 0x0

    .line 40
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 43
    iput-object v0, p0, Lcom/samsung/android/app/memo/FromClipboardReceiver;->mContext:Landroid/content/Context;

    .line 45
    iput-object v0, p0, Lcom/samsung/android/app/memo/FromClipboardReceiver;->mTitle:Ljava/lang/String;

    .line 47
    iput-object v0, p0, Lcom/samsung/android/app/memo/FromClipboardReceiver;->mContent:Ljava/lang/String;

    .line 49
    iput v1, p0, Lcom/samsung/android/app/memo/FromClipboardReceiver;->mTitleLen:I

    .line 51
    iput v1, p0, Lcom/samsung/android/app/memo/FromClipboardReceiver;->mContentLen:I

    .line 40
    return-void
.end method

.method private isDuplicated()Z
    .locals 9

    .prologue
    const/4 v3, 0x0

    .line 128
    const/4 v8, 0x0

    .line 129
    .local v8, "ret":Z
    iget v0, p0, Lcom/samsung/android/app/memo/FromClipboardReceiver;->mContentLen:I

    if-lez v0, :cond_1

    .line 130
    sget-object v0, Lcom/samsung/android/provider/memo/MemoContract;->BASE_URI_CLIPBOARD:Landroid/net/Uri;

    iget-object v2, p0, Lcom/samsung/android/app/memo/FromClipboardReceiver;->mContent:Ljava/lang/String;

    const-string v4, "\'"

    const-string v5, "\'\'"

    invoke-virtual {v2, v4, v5}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 131
    .local v1, "baseUri":Landroid/net/Uri;
    iget-object v0, p0, Lcom/samsung/android/app/memo/FromClipboardReceiver;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 132
    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "strippedContent"

    aput-object v5, v2, v4

    const/4 v4, 0x1

    const-string v5, "isDeleted"

    aput-object v5, v2, v4

    move-object v4, v3

    move-object v5, v3

    .line 131
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 133
    .local v6, "cur":Landroid/database/Cursor;
    if-eqz v6, :cond_1

    .line 134
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_0

    .line 135
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    .line 137
    const-string v0, "isDeleted"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 136
    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 138
    .local v7, "deleted":Ljava/lang/String;
    const-string v0, "1"

    invoke-virtual {v7, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 139
    const/4 v8, 0x0

    .line 143
    .end local v7    # "deleted":Ljava/lang/String;
    :cond_0
    :goto_0
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 147
    .end local v1    # "baseUri":Landroid/net/Uri;
    .end local v6    # "cur":Landroid/database/Cursor;
    :cond_1
    return v8

    .line 141
    .restart local v1    # "baseUri":Landroid/net/Uri;
    .restart local v6    # "cur":Landroid/database/Cursor;
    .restart local v7    # "deleted":Ljava/lang/String;
    :cond_2
    const/4 v8, 0x1

    goto :goto_0
.end method

.method private saveToMemo(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 88
    iget-object v0, p0, Lcom/samsung/android/app/memo/FromClipboardReceiver;->mTitle:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 89
    iget-object v0, p0, Lcom/samsung/android/app/memo/FromClipboardReceiver;->mTitle:Ljava/lang/String;

    const-string v1, "DFT_"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/samsung/android/app/memo/FromClipboardReceiver;->isDuplicated()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 90
    iget-object v0, p0, Lcom/samsung/android/app/memo/FromClipboardReceiver;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b00a7

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/samsung/android/app/memo/FromClipboardReceiver;->showToast(Ljava/lang/String;)V

    .line 103
    :goto_0
    return-void

    .line 94
    :cond_0
    iget v0, p0, Lcom/samsung/android/app/memo/FromClipboardReceiver;->mTitleLen:I

    if-gtz v0, :cond_1

    .line 95
    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/android/app/memo/FromClipboardReceiver;->mTitle:Ljava/lang/String;

    .line 98
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/app/memo/FromClipboardReceiver;->mContent:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 99
    iget-object v0, p0, Lcom/samsung/android/app/memo/FromClipboardReceiver;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/provider/memo/MemoContract;->BASE_URI_MEMO:Landroid/net/Uri;

    invoke-direct {p0}, Lcom/samsung/android/app/memo/FromClipboardReceiver;->toContentValues()Landroid/content/ContentValues;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    .line 102
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/app/memo/FromClipboardReceiver;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b00a8

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/samsung/android/app/memo/FromClipboardReceiver;->showToast(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private showToast(Ljava/lang/String;)V
    .locals 2
    .param p1, "str"    # Ljava/lang/String;

    .prologue
    .line 119
    sget-object v0, Lcom/samsung/android/app/memo/FromClipboardReceiver;->mToast:Landroid/widget/Toast;

    if-nez v0, :cond_0

    .line 120
    iget-object v0, p0, Lcom/samsung/android/app/memo/FromClipboardReceiver;->mContext:Landroid/content/Context;

    const/4 v1, 0x0

    invoke-static {v0, p1, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/app/memo/FromClipboardReceiver;->mToast:Landroid/widget/Toast;

    .line 124
    :goto_0
    sget-object v0, Lcom/samsung/android/app/memo/FromClipboardReceiver;->mToast:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 125
    return-void

    .line 122
    :cond_0
    sget-object v0, Lcom/samsung/android/app/memo/FromClipboardReceiver;->mToast:Landroid/widget/Toast;

    invoke-virtual {v0, p1}, Landroid/widget/Toast;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private toContentValues()Landroid/content/ContentValues;
    .locals 4

    .prologue
    .line 106
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 107
    .local v0, "values":Landroid/content/ContentValues;
    const-string v1, "UUID"

    invoke-static {}, Lcom/samsung/android/app/memo/util/UUIDHelper;->newUUID()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 108
    const-string v1, "createdAt"

    invoke-static {}, Lcom/samsung/android/app/memo/util/Utils;->getCurrentUTCTimeMillis()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 109
    const-string v1, "lastModifiedAt"

    invoke-static {}, Lcom/samsung/android/app/memo/util/Utils;->getCurrentUTCTimeMillis()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 110
    const-string v1, "categoryUUID"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 111
    const-string v1, "title"

    iget-object v2, p0, Lcom/samsung/android/app/memo/FromClipboardReceiver;->mTitle:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 112
    const-string v1, "content"

    iget-object v2, p0, Lcom/samsung/android/app/memo/FromClipboardReceiver;->mContent:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 113
    const-string v1, "vrfileUUID"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 114
    const-string v1, "_data"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 115
    return-object v0
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/16 v3, 0x5dc

    .line 57
    iput-object p1, p0, Lcom/samsung/android/app/memo/FromClipboardReceiver;->mContext:Landroid/content/Context;

    .line 58
    const-string v1, "title"

    invoke-virtual {p2, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/app/memo/FromClipboardReceiver;->mTitle:Ljava/lang/String;

    .line 59
    const-string v1, "content"

    invoke-virtual {p2, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/app/memo/FromClipboardReceiver;->mContent:Ljava/lang/String;

    .line 61
    iget-object v1, p0, Lcom/samsung/android/app/memo/FromClipboardReceiver;->mContent:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 62
    iget-object v1, p0, Lcom/samsung/android/app/memo/FromClipboardReceiver;->mContent:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/app/memo/FromClipboardReceiver;->mContent:Ljava/lang/String;

    .line 64
    :cond_0
    sget-boolean v1, Lcom/samsung/android/app/memo/FromClipboardReceiver;->DEBUG:Z

    if-eqz v1, :cond_1

    .line 65
    const-string v1, "memo_receiver"

    const-string v2, "Broadcast CLIPBOARD_TO_MEMO_INSERT received"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 67
    :cond_1
    invoke-static {}, Landroid/os/Environment;->getExternalStorageState()Ljava/lang/String;

    move-result-object v0

    .line 68
    .local v0, "state":Ljava/lang/String;
    const-string v1, "shared"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 85
    :cond_2
    :goto_0
    return-void

    .line 72
    :cond_3
    iget-object v1, p0, Lcom/samsung/android/app/memo/FromClipboardReceiver;->mTitle:Ljava/lang/String;

    if-eqz v1, :cond_4

    .line 73
    iget-object v1, p0, Lcom/samsung/android/app/memo/FromClipboardReceiver;->mTitle:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    iput v1, p0, Lcom/samsung/android/app/memo/FromClipboardReceiver;->mTitleLen:I

    .line 76
    :cond_4
    iget-object v1, p0, Lcom/samsung/android/app/memo/FromClipboardReceiver;->mContent:Ljava/lang/String;

    if-eqz v1, :cond_5

    .line 77
    iget-object v1, p0, Lcom/samsung/android/app/memo/FromClipboardReceiver;->mContent:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    iput v1, p0, Lcom/samsung/android/app/memo/FromClipboardReceiver;->mContentLen:I

    .line 78
    iget v1, p0, Lcom/samsung/android/app/memo/FromClipboardReceiver;->mContentLen:I

    if-le v1, v3, :cond_5

    .line 79
    iget-object v1, p0, Lcom/samsung/android/app/memo/FromClipboardReceiver;->mContent:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/app/memo/FromClipboardReceiver;->mContent:Ljava/lang/String;

    .line 82
    :cond_5
    iget v1, p0, Lcom/samsung/android/app/memo/FromClipboardReceiver;->mTitleLen:I

    iget v2, p0, Lcom/samsung/android/app/memo/FromClipboardReceiver;->mContentLen:I

    add-int/2addr v1, v2

    if-lez v1, :cond_2

    .line 83
    invoke-direct {p0, p1}, Lcom/samsung/android/app/memo/FromClipboardReceiver;->saveToMemo(Landroid/content/Context;)V

    goto :goto_0
.end method
