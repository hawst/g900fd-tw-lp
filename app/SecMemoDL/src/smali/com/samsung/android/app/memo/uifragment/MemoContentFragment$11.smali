.class Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$11;
.super Ljava/lang/Object;
.source "MemoContentFragment.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->initToolbar()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$11;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;

    .line 586
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$0(Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$11;)Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;
    .locals 1

    .prologue
    .line 586
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$11;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;

    return-object v0
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 590
    iget-object v2, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$11;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;

    invoke-virtual {v2}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->isAdded()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 591
    iget-object v2, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$11;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;

    invoke-virtual {v2}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v2

    const v3, 0x7f040021

    .line 592
    const/4 v4, 0x0

    .line 591
    invoke-virtual {v2, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 593
    .local v1, "v":Landroid/view/View;
    iget-object v2, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$11;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;

    invoke-virtual {v2}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/app/ActionBar;->setCustomView(Landroid/view/View;)V

    .line 594
    iget-object v3, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$11;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;

    const v2, 0x7f0e0049

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    invoke-static {v3, v2}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->access$24(Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;Landroid/widget/TextView;)V

    .line 595
    iget-object v2, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$11;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mFragmentAction:Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;
    invoke-static {v2}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->access$19(Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;)Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;

    move-result-object v2

    invoke-interface {v2}, Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;->getSession()Lcom/samsung/android/app/memo/Session;

    move-result-object v0

    .line 596
    .local v0, "ss":Lcom/samsung/android/app/memo/Session;
    iget-object v2, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$11;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->tv:Landroid/widget/TextView;
    invoke-static {v2}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->access$25(Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;)Landroid/widget/TextView;

    move-result-object v2

    invoke-virtual {v0}, Lcom/samsung/android/app/memo/Session;->getTitle()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 597
    iget-object v2, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$11;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->tv:Landroid/widget/TextView;
    invoke-static {v2}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->access$25(Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;)Landroid/widget/TextView;

    move-result-object v2

    new-instance v3, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$11$1;

    invoke-direct {v3, p0}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$11$1;-><init>(Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$11;)V

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 607
    .end local v0    # "ss":Lcom/samsung/android/app/memo/Session;
    .end local v1    # "v":Landroid/view/View;
    :cond_0
    return-void
.end method
