.class public Lcom/samsung/android/app/memo/MemoApp;
.super Landroid/app/Application;
.source "MemoApp.java"


# static fields
.field private static final TAG:Ljava/lang/String;

.field private static sInstance:Lcom/samsung/android/app/memo/MemoApp;

.field private static sIsCreateMemo:Z

.field private static sIsSaveOnDetach:Z

.field private static sIsShowNotificationEnabled:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 35
    const-class v0, Lcom/samsung/android/app/memo/MemoApp;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/app/memo/MemoApp;->TAG:Ljava/lang/String;

    .line 40
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/android/app/memo/MemoApp;->sInstance:Lcom/samsung/android/app/memo/MemoApp;

    .line 42
    sput-boolean v1, Lcom/samsung/android/app/memo/MemoApp;->sIsShowNotificationEnabled:Z

    .line 44
    sput-boolean v1, Lcom/samsung/android/app/memo/MemoApp;->sIsSaveOnDetach:Z

    .line 46
    sput-boolean v1, Lcom/samsung/android/app/memo/MemoApp;->sIsCreateMemo:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 60
    invoke-direct {p0}, Landroid/app/Application;-><init>()V

    .line 61
    sget-object v0, Lcom/samsung/android/app/memo/MemoApp;->TAG:Ljava/lang/String;

    const-string v1, "MemoApp()"

    invoke-static {v0, v1}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 62
    sput-object p0, Lcom/samsung/android/app/memo/MemoApp;->sInstance:Lcom/samsung/android/app/memo/MemoApp;

    .line 63
    return-void
.end method

.method public static getInstance()Lcom/samsung/android/app/memo/MemoApp;
    .locals 1

    .prologue
    .line 56
    sget-object v0, Lcom/samsung/android/app/memo/MemoApp;->sInstance:Lcom/samsung/android/app/memo/MemoApp;

    return-object v0
.end method

.method public static isCreateMemo()Z
    .locals 1

    .prologue
    .line 93
    sget-boolean v0, Lcom/samsung/android/app/memo/MemoApp;->sIsCreateMemo:Z

    return v0
.end method

.method public static isSaveOnDetach()Z
    .locals 1

    .prologue
    .line 101
    sget-boolean v0, Lcom/samsung/android/app/memo/MemoApp;->sIsSaveOnDetach:Z

    return v0
.end method

.method public static isShowNotificationEnabled()Z
    .locals 1

    .prologue
    .line 109
    sget-boolean v0, Lcom/samsung/android/app/memo/MemoApp;->sIsShowNotificationEnabled:Z

    return v0
.end method

.method public static setIsCreateMemo(Z)V
    .locals 0
    .param p0, "IsCreateMemo"    # Z

    .prologue
    .line 97
    sput-boolean p0, Lcom/samsung/android/app/memo/MemoApp;->sIsCreateMemo:Z

    .line 98
    return-void
.end method

.method public static setIsSaveOnDetach(Z)V
    .locals 0
    .param p0, "isSaveOnDetach"    # Z

    .prologue
    .line 105
    sput-boolean p0, Lcom/samsung/android/app/memo/MemoApp;->sIsSaveOnDetach:Z

    .line 106
    return-void
.end method

.method public static setShowNotificationEnabled(Z)V
    .locals 0
    .param p0, "sIsShowNotificationEnabled"    # Z

    .prologue
    .line 113
    sput-boolean p0, Lcom/samsung/android/app/memo/MemoApp;->sIsShowNotificationEnabled:Z

    .line 114
    return-void
.end method


# virtual methods
.method public onCreate()V
    .locals 3

    .prologue
    .line 67
    invoke-super {p0}, Landroid/app/Application;->onCreate()V

    .line 68
    sget-object v1, Lcom/samsung/android/app/memo/MemoApp;->TAG:Ljava/lang/String;

    const-string v2, "onCreate()"

    invoke-static {v1, v2}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 69
    const-string v1, "notification"

    invoke-virtual {p0, v1}, Lcom/samsung/android/app/memo/MemoApp;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    .line 70
    .local v0, "notificationManager":Landroid/app/NotificationManager;
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/NotificationManager;->cancel(I)V

    .line 71
    const v1, 0x5411111

    invoke-virtual {v0, v1}, Landroid/app/NotificationManager;->cancel(I)V

    .line 74
    invoke-static {p0}, Lcom/samsung/android/app/migration/utils/Utils;->resetSyncDialogsState(Landroid/content/Context;)V

    .line 75
    return-void
.end method

.method public onTerminate()V
    .locals 2

    .prologue
    .line 79
    invoke-static {}, Lcom/samsung/android/app/memo/uiwidget/HtmlImageSpan;->flushCache()V

    .line 80
    invoke-super {p0}, Landroid/app/Application;->onTerminate()V

    .line 81
    sget-object v0, Lcom/samsung/android/app/memo/MemoApp;->TAG:Ljava/lang/String;

    const-string v1, "onTerminate()"

    invoke-static {v0, v1}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 82
    return-void
.end method

.method public onTrimMemory(I)V
    .locals 1
    .param p1, "level"    # I

    .prologue
    .line 86
    const/16 v0, 0xf

    if-lt p1, v0, :cond_0

    .line 87
    invoke-static {}, Lcom/samsung/android/app/memo/uiwidget/HtmlImageSpan;->trimMemory()V

    .line 89
    :cond_0
    invoke-super {p0, p1}, Landroid/app/Application;->onTrimMemory(I)V

    .line 90
    return-void
.end method
