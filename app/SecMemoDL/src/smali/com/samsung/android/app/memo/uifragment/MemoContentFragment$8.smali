.class Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$8;
.super Landroid/os/Handler;
.source "MemoContentFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$8;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;

    .line 2397
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public dispatchMessage(Landroid/os/Message;)V
    .locals 5
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 2400
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 2466
    :cond_0
    :goto_0
    return-void

    .line 2402
    :pswitch_0
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$8;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mIsEditMode:Z
    invoke-static {v0}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->access$5(Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2403
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$8;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;

    iget-object v0, v0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->keypadHandler:Landroid/os/Handler;

    invoke-virtual {v0, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    .line 2407
    :pswitch_1
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$8;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mIsEditMode:Z
    invoke-static {v0}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->access$5(Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2408
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$8;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;

    iget-object v0, v0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->keypadHandler:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    .line 2412
    :pswitch_2
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$8;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mIsEditMode:Z
    invoke-static {v0}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->access$5(Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 2413
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$8;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;

    invoke-static {v0, v2}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->access$6(Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;Z)V

    .line 2414
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$8;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;

    invoke-static {v0, v2}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->access$7(Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;Z)V

    .line 2418
    :goto_1
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$8;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mVoiceView:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;
    invoke-static {v0}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->access$8(Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;)Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2419
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$8;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mVoiceView:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;
    invoke-static {v0}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->access$8(Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;)Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->cleanUp()V

    .line 2420
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$8;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mMainLayout:Landroid/widget/FrameLayout;
    invoke-static {v0}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->access$9(Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;)Landroid/widget/FrameLayout;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getVisibility()I

    move-result v0

    if-nez v0, :cond_2

    .line 2421
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$8;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mMainLayout:Landroid/widget/FrameLayout;
    invoke-static {v0}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->access$9(Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;)Landroid/widget/FrameLayout;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 2423
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$8;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;

    # invokes: Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->enableDisableVoiceButton(Z)V
    invoke-static {v0, v2}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->access$10(Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;Z)V

    .line 2424
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$8;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;

    invoke-static {v0, v4}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->access$11(Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;Lcom/samsung/android/app/memo/voice/VoiceInfo;)V

    .line 2425
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$8;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;

    invoke-static {v0, v3}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->access$12(Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;Z)V

    .line 2426
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$8;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;

    invoke-static {v0, v3}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->access$13(Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;Z)V

    .line 2427
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$8;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mVoiceView:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;
    invoke-static {v0}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->access$8(Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;)Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$8;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mIsVoiceLayoutVisible:Z
    invoke-static {v1}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->access$14(Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;)Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->setVoiceViewLayoutVisible(Z)V

    .line 2428
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$8;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mMenu:Landroid/view/Menu;
    invoke-static {v0}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->access$15(Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;)Landroid/view/Menu;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2442
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$8;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mMenu:Landroid/view/Menu;
    invoke-static {v0}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->access$15(Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;)Landroid/view/Menu;

    move-result-object v0

    const v1, 0x7f0e0095

    invoke-interface {v0, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 2443
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$8;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mMenu:Landroid/view/Menu;
    invoke-static {v0}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->access$15(Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;)Landroid/view/Menu;

    move-result-object v0

    const v1, 0x7f0e0096

    invoke-interface {v0, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    goto/16 :goto_0

    .line 2416
    :cond_3
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$8;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;

    iget-object v0, v0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->keypadHandler:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_1

    .line 2447
    :pswitch_3
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$8;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;

    invoke-static {v0, v2}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->access$12(Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;Z)V

    goto/16 :goto_0

    .line 2450
    :pswitch_4
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$8;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mVoiceView:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;
    invoke-static {v0}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->access$8(Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;)Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 2451
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$8;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mVoiceView:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;
    invoke-static {v0}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->access$8(Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;)Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->releaseVoiceViewResources()V

    .line 2452
    :cond_4
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$8;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;

    invoke-static {v0, v3}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->access$13(Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;Z)V

    .line 2453
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$8;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;

    invoke-static {v0, v3}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->access$12(Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;Z)V

    .line 2454
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$8;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;

    invoke-static {v0, v4}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->access$11(Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;Lcom/samsung/android/app/memo/voice/VoiceInfo;)V

    goto/16 :goto_0

    .line 2457
    :pswitch_5
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$8;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;

    # invokes: Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->enableDisableVoiceButton(Z)V
    invoke-static {v0, v2}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->access$10(Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;Z)V

    goto/16 :goto_0

    .line 2460
    :pswitch_6
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$8;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;

    # invokes: Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->enableDisableVoiceButton(Z)V
    invoke-static {v0, v3}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->access$10(Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;Z)V

    goto/16 :goto_0

    .line 2463
    :pswitch_7
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$8;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;

    invoke-static {v0, v3}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->access$13(Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;Z)V

    goto/16 :goto_0

    .line 2400
    :pswitch_data_0
    .packed-switch 0xfc5
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_0
        :pswitch_1
        :pswitch_7
    .end packed-switch
.end method
