.class public final Lcom/samsung/android/app/memo/util/UUIDHelper;
.super Ljava/lang/Object;
.source "UUIDHelper.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 30
    const-class v0, Lcom/samsung/android/app/memo/util/UUIDHelper;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/app/memo/util/UUIDHelper;->TAG:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 62
    sget-object v0, Lcom/samsung/android/app/memo/util/UUIDHelper;->TAG:Ljava/lang/String;

    sget-object v1, Lcom/samsung/android/app/memo/util/UUIDHelper;->TAG:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 63
    return-void
.end method

.method private static getMostSigBits()J
    .locals 2

    .prologue
    .line 33
    invoke-static {}, Lcom/samsung/android/app/memo/util/Utils;->getUniqueId()J

    move-result-wide v0

    return-wide v0
.end method

.method public static getUUIDforDefaultCategory(J)Ljava/lang/String;
    .locals 4
    .param p0, "leastSigBits"    # J

    .prologue
    .line 46
    const-wide/16 v0, 0x0

    .line 48
    .local v0, "mostSigBits":J
    new-instance v2, Ljava/util/UUID;

    invoke-direct {v2, v0, v1, p0, p1}, Ljava/util/UUID;-><init>(JJ)V

    .line 50
    .local v2, "uuid":Ljava/util/UUID;
    invoke-virtual {v2}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3
.end method

.method public static newUUID()Ljava/lang/String;
    .locals 6

    .prologue
    .line 37
    invoke-static {}, Lcom/samsung/android/app/memo/util/UUIDHelper;->getMostSigBits()J

    move-result-wide v2

    .line 38
    .local v2, "mostSigBits":J
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 40
    .local v0, "leastSigBits":J
    new-instance v4, Ljava/util/UUID;

    invoke-direct {v4, v2, v3, v0, v1}, Ljava/util/UUID;-><init>(JJ)V

    .line 42
    .local v4, "uuid":Ljava/util/UUID;
    invoke-virtual {v4}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v5

    return-object v5
.end method

.method public static verify(Ljava/lang/String;)V
    .locals 2
    .param p0, "uuid"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    .line 55
    :try_start_0
    invoke-static {p0}, Ljava/util/UUID;->fromString(Ljava/lang/String;)Ljava/util/UUID;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 59
    return-void

    .line 56
    :catch_0
    move-exception v0

    .line 57
    .local v0, "e":Ljava/lang/NullPointerException;
    new-instance v1, Ljava/lang/IllegalArgumentException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method
