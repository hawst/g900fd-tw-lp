.class public interface abstract Lcom/samsung/android/app/memo/uiwidget/DndListView$OnDndDragAndDropListener;
.super Ljava/lang/Object;
.source "DndListView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/memo/uiwidget/DndListView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "OnDndDragAndDropListener"
.end annotation


# virtual methods
.method public abstract dragDragEnded()V
.end method

.method public abstract dragDragExited()V
.end method

.method public abstract dragDrop(II)V
.end method

.method public abstract dragEntered()V
.end method

.method public abstract dragLocation(IIII)V
.end method

.method public abstract dragStarted(I)V
.end method
