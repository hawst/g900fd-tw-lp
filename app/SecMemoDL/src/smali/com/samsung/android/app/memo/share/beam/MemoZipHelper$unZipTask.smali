.class Lcom/samsung/android/app/memo/share/beam/MemoZipHelper$unZipTask;
.super Landroid/os/AsyncTask;
.source "MemoZipHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/memo/share/beam/MemoZipHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "unZipTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Lcom/samsung/android/app/memo/share/beam/MemoZipItem;",
        "Ljava/lang/Integer;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field private mHandler:Landroid/os/Handler;

.field private m_oMemoZipItem:Lcom/samsung/android/app/memo/share/beam/MemoZipItem;


# direct methods
.method public constructor <init>(Landroid/os/Handler;)V
    .locals 1
    .param p1, "a_oHandler"    # Landroid/os/Handler;

    .prologue
    const/4 v0, 0x0

    .line 65
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 62
    iput-object v0, p0, Lcom/samsung/android/app/memo/share/beam/MemoZipHelper$unZipTask;->mHandler:Landroid/os/Handler;

    .line 63
    iput-object v0, p0, Lcom/samsung/android/app/memo/share/beam/MemoZipHelper$unZipTask;->m_oMemoZipItem:Lcom/samsung/android/app/memo/share/beam/MemoZipItem;

    .line 66
    iput-object p1, p0, Lcom/samsung/android/app/memo/share/beam/MemoZipHelper$unZipTask;->mHandler:Landroid/os/Handler;

    .line 67
    return-void
.end method

.method private unZip(Lcom/samsung/android/app/memo/share/beam/MemoZipItem;)Ljava/lang/Boolean;
    .locals 4
    .param p1, "a_oItem"    # Lcom/samsung/android/app/memo/share/beam/MemoZipItem;

    .prologue
    .line 94
    const/4 v0, 0x1

    .line 96
    .local v0, "bResult":Z
    :try_start_0
    new-instance v2, Lnet/lingala/zip4j/core/ZipFile;

    iget-object v3, p1, Lcom/samsung/android/app/memo/share/beam/MemoZipItem;->mstrZipFilename:Ljava/lang/String;

    invoke-direct {v2, v3}, Lnet/lingala/zip4j/core/ZipFile;-><init>(Ljava/lang/String;)V

    .line 97
    .local v2, "zipFile":Lnet/lingala/zip4j/core/ZipFile;
    iget-object v3, p1, Lcom/samsung/android/app/memo/share/beam/MemoZipItem;->m_strZipRootpath:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lnet/lingala/zip4j/core/ZipFile;->extractAll(Ljava/lang/String;)V
    :try_end_0
    .catch Lnet/lingala/zip4j/exception/ZipException; {:try_start_0 .. :try_end_0} :catch_0

    .line 102
    .end local v2    # "zipFile":Lnet/lingala/zip4j/core/ZipFile;
    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    return-object v3

    .line 98
    :catch_0
    move-exception v1

    .line 99
    .local v1, "oException":Lnet/lingala/zip4j/exception/ZipException;
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected varargs doInBackground([Lcom/samsung/android/app/memo/share/beam/MemoZipItem;)Ljava/lang/Boolean;
    .locals 3
    .param p1, "items"    # [Lcom/samsung/android/app/memo/share/beam/MemoZipItem;

    .prologue
    const/4 v2, 0x0

    .line 70
    aget-object v0, p1, v2

    .line 71
    .local v0, "oItem":Lcom/samsung/android/app/memo/share/beam/MemoZipItem;
    if-nez v0, :cond_0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    .line 77
    :goto_0
    return-object v1

    .line 73
    :cond_0
    iput-object v0, p0, Lcom/samsung/android/app/memo/share/beam/MemoZipHelper$unZipTask;->m_oMemoZipItem:Lcom/samsung/android/app/memo/share/beam/MemoZipItem;

    .line 74
    invoke-direct {p0, v0}, Lcom/samsung/android/app/memo/share/beam/MemoZipHelper$unZipTask;->unZip(Lcom/samsung/android/app/memo/share/beam/MemoZipItem;)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-nez v1, :cond_1

    .line 75
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    goto :goto_0

    .line 77
    :cond_1
    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    goto :goto_0
.end method

.method protected bridge varargs synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1
    check-cast p1, [Lcom/samsung/android/app/memo/share/beam/MemoZipItem;

    invoke-virtual {p0, p1}, Lcom/samsung/android/app/memo/share/beam/MemoZipHelper$unZipTask;->doInBackground([Lcom/samsung/android/app/memo/share/beam/MemoZipItem;)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Ljava/lang/Boolean;)V
    .locals 2
    .param p1, "result"    # Ljava/lang/Boolean;

    .prologue
    .line 81
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 82
    iget-object v1, p0, Lcom/samsung/android/app/memo/share/beam/MemoZipHelper$unZipTask;->mHandler:Landroid/os/Handler;

    if-eqz v1, :cond_0

    .line 83
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    .line 84
    .local v0, "oMessage":Landroid/os/Message;
    iget-object v1, p0, Lcom/samsung/android/app/memo/share/beam/MemoZipHelper$unZipTask;->m_oMemoZipItem:Lcom/samsung/android/app/memo/share/beam/MemoZipItem;

    iput-object v1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 85
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x0

    iput v1, v0, Landroid/os/Message;->what:I

    .line 89
    :goto_0
    iget-object v1, p0, Lcom/samsung/android/app/memo/share/beam/MemoZipHelper$unZipTask;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 91
    .end local v0    # "oMessage":Landroid/os/Message;
    :cond_0
    return-void

    .line 87
    .restart local v0    # "oMessage":Landroid/os/Message;
    :cond_1
    const/4 v1, -0x1

    iput v1, v0, Landroid/os/Message;->what:I

    goto :goto_0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/samsung/android/app/memo/share/beam/MemoZipHelper$unZipTask;->onPostExecute(Ljava/lang/Boolean;)V

    return-void
.end method
