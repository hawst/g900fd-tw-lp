.class public interface abstract Lcom/samsung/android/app/migration/snbdownload/network/downsync/util/Constants$PREFERERENCE_KEY;
.super Ljava/lang/Object;
.source "Constants.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/migration/snbdownload/network/downsync/util/Constants;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "PREFERERENCE_KEY"
.end annotation


# static fields
.field public static final BASEURL:Ljava/lang/String; = "BASE_URL_1"

.field public static final BASEURL2:Ljava/lang/String; = "BASE_URL_2"

.field public static final DEVICEID:Ljava/lang/String; = "did"

.field public static final IS_USERINFOFAILED:Ljava/lang/String; = "userInfoFailed"

.field public static final MKEY:Ljava/lang/String; = "mKey"

.field public static final REG_ID:Ljava/lang/String; = "RegistrationID"

.field public static final SKEY:Ljava/lang/String; = "sKey"

.field public static final STATUS:Ljava/lang/String; = "status"

.field public static final TOKEN:Ljava/lang/String; = "Access_Token"

.field public static final USER_ID:Ljava/lang/String; = "UserID"
