.class public Lcom/samsung/android/app/memo/uifragment/MemoListFragment;
.super Landroid/app/Fragment;
.source "MemoListFragment.java"

# interfaces
.implements Landroid/app/LoaderManager$LoaderCallbacks;
.implements Landroid/view/View$OnClickListener;
.implements Landroid/widget/SearchView$OnCloseListener;
.implements Landroid/widget/SearchView$OnQueryTextListener;
.implements Lcom/samsung/android/app/memo/uifragment/MemoKeyEventCallback;
.implements Lcom/samsung/android/app/memo/uiwidget/UndoBarController$UndoListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;,
        Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeStyle;,
        Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;,
        Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListItemTag;,
        Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MIME_TYPE;,
        Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MySearchView;,
        Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ShareQuickConnectTask;,
        Lcom/samsung/android/app/memo/uifragment/MemoListFragment$shareOnSConnectTask;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/app/Fragment;",
        "Landroid/view/View$OnClickListener;",
        "Landroid/widget/SearchView$OnQueryTextListener;",
        "Landroid/widget/SearchView$OnCloseListener;",
        "Lcom/samsung/android/app/memo/uiwidget/UndoBarController$UndoListener;",
        "Landroid/app/LoaderManager$LoaderCallbacks",
        "<",
        "Landroid/database/Cursor;",
        ">;",
        "Lcom/samsung/android/app/memo/uifragment/MemoKeyEventCallback;"
    }
.end annotation


# static fields
.field private static synthetic $SWITCH_TABLE$com$samsung$android$app$memo$uifragment$MemoListFragment$MIME_TYPE:[I = null

.field public static final CATEGORY_ALL:I = 0x1

.field public static final CATEGORY_NONE:I = 0x0

.field public static final CATEGORY_OTHER:I = 0x2

.field private static final CID_CATEGORY_UUID:I = 0x3

.field private static final CID_DATA:I = 0x7

.field private static final CID_DIRTY:I = 0x8

.field private static final CID_ID:I = 0x0

.field private static final CID_LAST_MODIFIED_AT:I = 0x2

.field private static final CID_STRIPPED_CONTENT:I = 0x5

.field private static final CID_TITLE:I = 0x4

.field private static final CID_UUID:I = 0x1

.field private static final CID_VR_FILE_UUID:I = 0x6

.field public static final KEY_CATEGORY_TITLE:Ljava/lang/String;

.field public static final KEY_CATEGORY_TYPE:Ljava/lang/String;

.field public static final KEY_CATEGORY_UUID:Ljava/lang/String;

.field public static final KEY_IS_CATEGORY_CHANGE:Ljava/lang/String;

.field private static final LOADER_MEMO_LIST:I = 0x3e8

.field private static final ORDERBY:Ljava/lang/String; = "lastModifiedAt DESC"

.field public static final PREF_CATEGORY:Ljava/lang/String;

.field private static final PROJECTION:[Ljava/lang/String;

.field private static final SELECTION:Ljava/lang/String; = "isDeleted IS 0"

.field public static final SELECT_LIMIT:I = 0x64

.field private static final TAG:Ljava/lang/String;

.field public static mCategoryType:I = 0x0

.field private static mCheckBoxIds:Ljava/util/ArrayList; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field static mIsActionmode:Z = false

.field private static final sDummyItemPosition:I = -0x1


# instance fields
.field private final DELAY_SEARCH:I

.field private final DELAY_TIME_ACTION_MODE:I

.field private final DELAY_TIME_MENU_SEARCH:I

.field private final DELAY_TIME_MODE_FINISH:I

.field private final DELAY_TIME_ROTATE:I

.field private final DELAY_TIME_SELECT_ALL:I

.field private final DELAY_TIME_SET_FOCUS:I

.field private final HOVER_POPUP_MAX_LENGTH:I

.field private final MAX_CHAR_SIZE:I

.field private final SEARCH_COUNT_BODY_LOOP:I

.field private final SEARCH_COUNT_TITLE_LOOP:I

.field private SEARCH_RESULT_BG_COLOR:Ljava/lang/String;

.field private final SELECT_ALL:I

.field private final TOUCH_TIME_MENU_ITEM:I

.field private final TOUCH_TIME_OPEN_CONTENT:I

.field private final UNSELECT_ALL:I

.field private isTaskRunning:Z

.field private mAbsListView:Landroid/widget/AbsListView;

.field public mActionMode:Landroid/view/ActionMode;

.field private mActionModeCallback:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;

.field private mActivity:Landroid/app/Activity;

.field private mAdapter:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;

.field private mAllThumbHash:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mBezelManager:Lcom/samsung/android/quickconnect/QuickConnectManager;

.field private mCategoryMoveDialog:Lcom/samsung/android/app/memo/uifragment/CategoryMoveDialog;

.field private mCategoryNamingDialog:Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;

.field private mFabButton:Landroid/widget/ImageButton;

.field private mFragmentAction:Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;

.field private mGirdItemCount:I

.field private mIDsBeingDeleted:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mIDsForCacheDelete:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public mImageLoader:Lcom/samsung/android/app/memo/util/ImageLoader;

.field mIsCategoryChanged:Z

.field private mIsCheckBoxMode:Z

.field private mIsPause:Z

.field private mIsSearchRequested:Z

.field private mIsSelectAll:Z

.field private mIsSkipFinishForSConnect:Z

.field private mIsWidgetPick:Z

.field private mLastMemoOpenClickTime:J

.field private mListener:Lcom/samsung/android/quickconnect/QuickConnectManager$QuickConnectListener;

.field private mMenu:Landroid/view/Menu;

.field private mMenuDelete:Landroid/view/MenuItem;

.field private mMenuDeleteButton:Landroid/view/MenuItem;

.field private mMenuMove:Landroid/view/MenuItem;

.field private mMenuPrint:Landroid/view/MenuItem;

.field private mMenuSearch:Landroid/view/MenuItem;

.field private mMenuSelect:Landroid/view/MenuItem;

.field private mMenuSelectionMode:Landroid/view/Menu;

.field private mMenuShareVia:Landroid/view/MenuItem;

.field private mMenuType:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeStyle;

.field private mSearchView:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MySearchView;

.field private mSelectAllLayout:Landroid/widget/LinearLayout;

.field private mSelectLimit:I

.field private mStrBeforeQuery:Ljava/lang/String;

.field private mStrCategoryTitle:Ljava/lang/String;

.field private mStrCategoryUUID:Ljava/lang/String;

.field private mStrCurFilter:Ljava/lang/String;

.field private mStrQuery:Ljava/lang/String;

.field private mStrSelection:Ljava/lang/String;

.field private mUndoBarController:Lcom/samsung/android/app/memo/uiwidget/UndoBarController;

.field private mWidgetID:J

.field private mimeType:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MIME_TYPE;


# direct methods
.method static synthetic $SWITCH_TABLE$com$samsung$android$app$memo$uifragment$MemoListFragment$MIME_TYPE()[I
    .locals 3

    .prologue
    .line 150
    sget-object v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->$SWITCH_TABLE$com$samsung$android$app$memo$uifragment$MemoListFragment$MIME_TYPE:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MIME_TYPE;->values()[Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MIME_TYPE;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MIME_TYPE;->APPLICATION_MEMO:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MIME_TYPE;

    invoke-virtual {v1}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MIME_TYPE;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_6

    :goto_1
    :try_start_1
    sget-object v1, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MIME_TYPE;->AUDIO_ALL:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MIME_TYPE;

    invoke-virtual {v1}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MIME_TYPE;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_5

    :goto_2
    :try_start_2
    sget-object v1, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MIME_TYPE;->AUDIO_MP4:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MIME_TYPE;

    invoke-virtual {v1}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MIME_TYPE;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_4

    :goto_3
    :try_start_3
    sget-object v1, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MIME_TYPE;->IMAGE_ALL:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MIME_TYPE;

    invoke-virtual {v1}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MIME_TYPE;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_3

    :goto_4
    :try_start_4
    sget-object v1, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MIME_TYPE;->INVALID:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MIME_TYPE;

    invoke-virtual {v1}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MIME_TYPE;->ordinal()I

    move-result v1

    const/4 v2, 0x7

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_2

    :goto_5
    :try_start_5
    sget-object v1, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MIME_TYPE;->TEXT_IMAGE_AUDIO:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MIME_TYPE;

    invoke-virtual {v1}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MIME_TYPE;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_1

    :goto_6
    :try_start_6
    sget-object v1, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MIME_TYPE;->TEXT_PLAIN:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MIME_TYPE;

    invoke-virtual {v1}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MIME_TYPE;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_0

    :goto_7
    sput-object v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->$SWITCH_TABLE$com$samsung$android$app$memo$uifragment$MemoListFragment$MIME_TYPE:[I

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_7

    :catch_1
    move-exception v1

    goto :goto_6

    :catch_2
    move-exception v1

    goto :goto_5

    :catch_3
    move-exception v1

    goto :goto_4

    :catch_4
    move-exception v1

    goto :goto_3

    :catch_5
    move-exception v1

    goto :goto_2

    :catch_6
    move-exception v1

    goto :goto_1
.end method

.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 154
    const-class v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->TAG:Ljava/lang/String;

    .line 179
    const/16 v0, 0x9

    new-array v0, v0, [Ljava/lang/String;

    .line 180
    const-string v1, "_id"

    aput-object v1, v0, v3

    const/4 v1, 0x1

    .line 181
    const-string v2, "UUID"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    .line 182
    const-string v2, "lastModifiedAt"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    .line 183
    const-string v2, "categoryUUID"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    .line 184
    const-string v2, "title"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    .line 186
    const-string v2, "SUBSTR(strippedContent, 0, 250)"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    .line 187
    const-string v2, "vrfileUUID"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    .line 188
    const-string v2, "_data"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    .line 189
    const-string v2, "isDirty"

    aput-object v2, v0, v1

    .line 179
    sput-object v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->PROJECTION:[Ljava/lang/String;

    .line 335
    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->TAG:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "_category_pref"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->PREF_CATEGORY:Ljava/lang/String;

    .line 337
    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->TAG:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "_category_uuid"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->KEY_CATEGORY_UUID:Ljava/lang/String;

    .line 339
    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->TAG:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "_category_title"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->KEY_CATEGORY_TITLE:Ljava/lang/String;

    .line 341
    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->TAG:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "_is_category_change"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->KEY_IS_CATEGORY_CHANGE:Ljava/lang/String;

    .line 343
    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->TAG:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "_category_type"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->KEY_CATEGORY_TYPE:Ljava/lang/String;

    .line 360
    sput-boolean v3, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mIsActionmode:Z

    .line 368
    return-void
.end method

.method public constructor <init>()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/16 v5, 0x1f4

    const/16 v4, 0x12c

    const/16 v3, 0x64

    const/4 v2, 0x0

    .line 150
    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    .line 210
    const-string v0, "isDeleted IS 0"

    iput-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mStrSelection:Ljava/lang/String;

    .line 292
    sget-object v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeStyle;->NORMAL:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeStyle;

    iput-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mMenuType:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeStyle;

    .line 307
    const/4 v0, -0x1

    iput v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mGirdItemCount:I

    .line 313
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mAllThumbHash:Landroid/util/SparseArray;

    .line 315
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mIDsForCacheDelete:Ljava/util/ArrayList;

    .line 347
    iput-boolean v2, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mIsWidgetPick:Z

    .line 353
    iput-boolean v2, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mIsSkipFinishForSConnect:Z

    .line 355
    iput-boolean v2, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mIsSelectAll:Z

    .line 356
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mWidgetID:J

    .line 364
    iput-boolean v2, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mIsCategoryChanged:Z

    .line 370
    iput-boolean v2, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mIsPause:Z

    .line 372
    iput-boolean v2, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mIsCheckBoxMode:Z

    .line 374
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mLastMemoOpenClickTime:J

    .line 386
    iput-object v6, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mBezelManager:Lcom/samsung/android/quickconnect/QuickConnectManager;

    .line 388
    iput-object v6, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mListener:Lcom/samsung/android/quickconnect/QuickConnectManager$QuickConnectListener;

    .line 392
    iput-boolean v2, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mIsSearchRequested:Z

    .line 394
    iput-boolean v2, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->isTaskRunning:Z

    .line 398
    iput v3, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mSelectLimit:I

    .line 403
    iput v3, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->SELECT_ALL:I

    .line 404
    const/16 v0, 0xc8

    iput v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->UNSELECT_ALL:I

    .line 405
    const/16 v0, 0x5db

    iput v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->MAX_CHAR_SIZE:I

    .line 406
    const/16 v0, 0x46

    iput v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->HOVER_POPUP_MAX_LENGTH:I

    .line 408
    const/16 v0, 0x190

    iput v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->DELAY_TIME_MODE_FINISH:I

    .line 409
    iput v5, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->DELAY_SEARCH:I

    .line 410
    iput v4, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->DELAY_TIME_ROTATE:I

    .line 411
    iput v4, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->DELAY_TIME_ACTION_MODE:I

    .line 412
    iput v3, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->DELAY_TIME_MENU_SEARCH:I

    .line 413
    iput v3, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->DELAY_TIME_SELECT_ALL:I

    .line 414
    iput v4, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->DELAY_TIME_SET_FOCUS:I

    .line 416
    iput v5, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->TOUCH_TIME_MENU_ITEM:I

    .line 417
    const/16 v0, 0x2bc

    iput v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->TOUCH_TIME_OPEN_CONTENT:I

    .line 419
    iput v4, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->SEARCH_COUNT_BODY_LOOP:I

    .line 420
    iput v3, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->SEARCH_COUNT_TITLE_LOOP:I

    .line 422
    const-string v0, "#01A5CE"

    iput-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->SEARCH_RESULT_BG_COLOR:Ljava/lang/String;

    .line 150
    return-void
.end method

.method private SetCategoryTitle(Ljava/lang/String;)V
    .locals 1
    .param p1, "title"    # Ljava/lang/String;

    .prologue
    .line 1442
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mFragmentAction:Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;

    invoke-interface {v0, p1}, Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;->setActionBarTitle(Ljava/lang/CharSequence;)V

    .line 1443
    return-void
.end method

.method private SetCategoryTitleWithNum(Ljava/lang/String;)V
    .locals 13
    .param p1, "title"    # Ljava/lang/String;

    .prologue
    const/16 v12, 0x21

    .line 1446
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    .line 1447
    .local v2, "len":I
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v8, " ("

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "%d"

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    iget-object v11, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mAbsListView:Landroid/widget/AbsListView;

    invoke-virtual {v11}, Landroid/widget/AbsListView;->getCount()I

    move-result v11

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v9, v10

    invoke-static {v8, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ") "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1448
    .local v1, "categoryWithNum":Ljava/lang/String;
    sget-object v7, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "SetCategoryTitleWithNum getListView().getCount()()"

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v9, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mAbsListView:Landroid/widget/AbsListView;

    invoke-virtual {v9}, Landroid/widget/AbsListView;->getCount()I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1449
    iget-object v7, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mFragmentAction:Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;

    invoke-interface {v7, v1}, Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;->setActionBarTitle(Ljava/lang/CharSequence;)V

    .line 1450
    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    move-result-object v7

    const-string v8, "action_bar_title"

    const-string v9, "id"

    const-string v10, "android"

    invoke-virtual {v7, v8, v9, v10}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 1451
    .local v0, "actionBarTitleId":I
    if-lez v0, :cond_1

    .line 1452
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v7

    invoke-virtual {v7, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    .line 1453
    .local v5, "titleView":Landroid/widget/TextView;
    if-eqz v5, :cond_1

    .line 1454
    new-instance v3, Landroid/text/SpannableString;

    invoke-virtual {v5}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v7

    invoke-direct {v3, v7}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 1455
    .local v3, "span":Landroid/text/Spannable;
    new-instance v7, Landroid/text/style/AbsoluteSizeSpan;

    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v8

    invoke-virtual {v8}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    .line 1456
    const v9, 0x7f090008

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v8

    float-to-int v8, v8

    invoke-direct {v7, v8}, Landroid/text/style/AbsoluteSizeSpan;-><init>(I)V

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v8

    .line 1455
    invoke-interface {v3, v7, v2, v8, v12}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    .line 1457
    invoke-static {}, Lcom/samsung/android/app/memo/util/Utils;->isMdpiDevice()Z

    move-result v7

    if-eqz v7, :cond_2

    .line 1458
    new-instance v7, Landroid/text/style/TypefaceSpan;

    const-string v8, "sec-roboto-regular"

    invoke-direct {v7, v8}, Landroid/text/style/TypefaceSpan;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v8

    invoke-interface {v3, v7, v2, v8, v12}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    .line 1461
    :goto_0
    invoke-virtual {v5, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1463
    invoke-static {}, Lcom/samsung/android/app/memo/util/Utils;->isHdpiDevice()Z

    move-result v7

    if-eqz v7, :cond_0

    invoke-static {}, Lcom/samsung/android/app/memo/util/Utils;->isMassProduct()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 1464
    const v7, 0x3e99999a    # 0.3f

    const/4 v8, 0x0

    const/high16 v9, 0x3f800000    # 1.0f

    const/high16 v10, -0x67000000

    invoke-virtual {v5, v7, v8, v9, v10}, Landroid/widget/TextView;->setShadowLayer(FFFI)V

    .line 1467
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v7

    invoke-virtual {v7}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v7

    iget-object v7, v7, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v7}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v7

    const-string v8, "or_IN"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 1468
    invoke-virtual {v5}, Landroid/widget/TextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v4

    .line 1469
    .local v4, "textPaint":Landroid/graphics/Paint;
    invoke-virtual {v4, v1}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v7

    float-to-int v6, v7

    .line 1470
    .local v6, "w":I
    add-int/lit8 v7, v6, 0xa

    invoke-virtual {v5, v7}, Landroid/widget/TextView;->setWidth(I)V

    .line 1474
    .end local v3    # "span":Landroid/text/Spannable;
    .end local v4    # "textPaint":Landroid/graphics/Paint;
    .end local v5    # "titleView":Landroid/widget/TextView;
    .end local v6    # "w":I
    :cond_1
    return-void

    .line 1460
    .restart local v3    # "span":Landroid/text/Spannable;
    .restart local v5    # "titleView":Landroid/widget/TextView;
    :cond_2
    new-instance v7, Landroid/text/style/TypefaceSpan;

    const-string v8, "sec-roboto-light"

    invoke-direct {v7, v8}, Landroid/text/style/TypefaceSpan;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v8

    invoke-interface {v3, v7, v2, v8, v12}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    goto :goto_0
.end method

.method static synthetic access$1()Ljava/lang/String;
    .locals 1

    .prologue
    .line 154
    sget-object v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$10(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;)Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeStyle;
    .locals 1

    .prologue
    .line 292
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mMenuType:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeStyle;

    return-object v0
.end method

.method static synthetic access$11(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;)I
    .locals 1

    .prologue
    .line 398
    iget v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mSelectLimit:I

    return v0
.end method

.method static synthetic access$12(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;)Landroid/app/Activity;
    .locals 1

    .prologue
    .line 376
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mActivity:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic access$13(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;)Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;
    .locals 1

    .prologue
    .line 225
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mAdapter:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;

    return-object v0
.end method

.method static synthetic access$14(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;)Landroid/view/Menu;
    .locals 1

    .prologue
    .line 277
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mMenuSelectionMode:Landroid/view/Menu;

    return-object v0
.end method

.method static synthetic access$15(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;)J
    .locals 2

    .prologue
    .line 374
    iget-wide v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mLastMemoOpenClickTime:J

    return-wide v0
.end method

.method static synthetic access$16(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;J)V
    .locals 1

    .prologue
    .line 374
    iput-wide p1, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mLastMemoOpenClickTime:J

    return-void
.end method

.method static synthetic access$17(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;Ljava/util/ArrayList;)V
    .locals 0

    .prologue
    .line 315
    iput-object p1, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mIDsForCacheDelete:Ljava/util/ArrayList;

    return-void
.end method

.method static synthetic access$18(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;Ljava/util/List;)V
    .locals 0

    .prologue
    .line 2052
    invoke-direct {p0, p1}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->deletingMemos(Ljava/util/List;)V

    return-void
.end method

.method static synthetic access$19(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 320
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mStrCategoryUUID:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$2(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;)Landroid/util/SparseArray;
    .locals 1

    .prologue
    .line 313
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mAllThumbHash:Landroid/util/SparseArray;

    return-object v0
.end method

.method static synthetic access$20(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;Lcom/samsung/android/app/memo/uifragment/CategoryMoveDialog;)V
    .locals 0

    .prologue
    .line 261
    iput-object p1, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mCategoryMoveDialog:Lcom/samsung/android/app/memo/uifragment/CategoryMoveDialog;

    return-void
.end method

.method static synthetic access$21(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;)Lcom/samsung/android/app/memo/uifragment/CategoryMoveDialog;
    .locals 1

    .prologue
    .line 261
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mCategoryMoveDialog:Lcom/samsung/android/app/memo/uifragment/CategoryMoveDialog;

    return-object v0
.end method

.method static synthetic access$22(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;J)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2975
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->getTitle(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$23(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;J)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2985
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->getText(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$24(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;J)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2996
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->getHtmlContent(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$25(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;J)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 3016
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->getImageUriList(J)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$26(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;J)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 3006
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->getVoiceUri(J)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$27(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;Landroid/app/Activity;)V
    .locals 0

    .prologue
    .line 2637
    invoke-direct {p0, p1}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->initSConnect(Landroid/app/Activity;)V

    return-void
.end method

.method static synthetic access$28(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;)Lcom/samsung/android/quickconnect/QuickConnectManager;
    .locals 1

    .prologue
    .line 386
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mBezelManager:Lcom/samsung/android/quickconnect/QuickConnectManager;

    return-object v0
.end method

.method static synthetic access$29(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;)Lcom/samsung/android/quickconnect/QuickConnectManager$QuickConnectListener;
    .locals 1

    .prologue
    .line 388
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mListener:Lcom/samsung/android/quickconnect/QuickConnectManager$QuickConnectListener;

    return-object v0
.end method

.method static synthetic access$3(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;)Z
    .locals 1

    .prologue
    .line 372
    iget-boolean v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mIsCheckBoxMode:Z

    return v0
.end method

.method static synthetic access$30(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;Z)V
    .locals 0

    .prologue
    .line 372
    iput-boolean p1, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mIsCheckBoxMode:Z

    return-void
.end method

.method static synthetic access$31(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;)Landroid/widget/ImageButton;
    .locals 1

    .prologue
    .line 305
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mFabButton:Landroid/widget/ImageButton;

    return-object v0
.end method

.method static synthetic access$32(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;Landroid/view/MenuItem;)V
    .locals 0

    .prologue
    .line 249
    iput-object p1, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mMenuShareVia:Landroid/view/MenuItem;

    return-void
.end method

.method static synthetic access$33(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;Landroid/view/MenuItem;)V
    .locals 0

    .prologue
    .line 259
    iput-object p1, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mMenuMove:Landroid/view/MenuItem;

    return-void
.end method

.method static synthetic access$34(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;Landroid/view/MenuItem;)V
    .locals 0

    .prologue
    .line 268
    iput-object p1, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mMenuDeleteButton:Landroid/view/MenuItem;

    return-void
.end method

.method static synthetic access$35(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;Landroid/view/MenuItem;)V
    .locals 0

    .prologue
    .line 254
    iput-object p1, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mMenuPrint:Landroid/view/MenuItem;

    return-void
.end method

.method static synthetic access$36(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;)Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;
    .locals 1

    .prologue
    .line 218
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mFragmentAction:Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;

    return-object v0
.end method

.method static synthetic access$37(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;Landroid/view/Menu;)V
    .locals 0

    .prologue
    .line 277
    iput-object p1, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mMenuSelectionMode:Landroid/view/Menu;

    return-void
.end method

.method static synthetic access$38(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;Lcom/samsung/android/quickconnect/QuickConnectManager;)V
    .locals 0

    .prologue
    .line 386
    iput-object p1, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mBezelManager:Lcom/samsung/android/quickconnect/QuickConnectManager;

    return-void
.end method

.method static synthetic access$39(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;)Landroid/widget/LinearLayout;
    .locals 1

    .prologue
    .line 309
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mSelectAllLayout:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$4()Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 368
    sget-object v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mCheckBoxIds:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$40(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeStyle;)V
    .locals 0

    .prologue
    .line 292
    iput-object p1, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mMenuType:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeStyle;

    return-void
.end method

.method static synthetic access$41(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;)Landroid/view/MenuItem;
    .locals 1

    .prologue
    .line 273
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mMenuSearch:Landroid/view/MenuItem;

    return-object v0
.end method

.method static synthetic access$42(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;)Z
    .locals 1

    .prologue
    .line 355
    iget-boolean v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mIsSelectAll:Z

    return v0
.end method

.method static synthetic access$43(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;)Landroid/view/MenuItem;
    .locals 1

    .prologue
    .line 249
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mMenuShareVia:Landroid/view/MenuItem;

    return-object v0
.end method

.method static synthetic access$44(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;)Landroid/view/MenuItem;
    .locals 1

    .prologue
    .line 254
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mMenuPrint:Landroid/view/MenuItem;

    return-object v0
.end method

.method static synthetic access$45(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;)Landroid/view/MenuItem;
    .locals 1

    .prologue
    .line 259
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mMenuMove:Landroid/view/MenuItem;

    return-object v0
.end method

.method static synthetic access$46(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;)Landroid/view/MenuItem;
    .locals 1

    .prologue
    .line 268
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mMenuDeleteButton:Landroid/view/MenuItem;

    return-object v0
.end method

.method static synthetic access$47(Ljava/util/ArrayList;)V
    .locals 0

    .prologue
    .line 368
    sput-object p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mCheckBoxIds:Ljava/util/ArrayList;

    return-void
.end method

.method static synthetic access$48(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;Z)V
    .locals 0

    .prologue
    .line 392
    iput-boolean p1, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mIsSearchRequested:Z

    return-void
.end method

.method static synthetic access$49(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;)Z
    .locals 1

    .prologue
    .line 347
    iget-boolean v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mIsWidgetPick:Z

    return v0
.end method

.method static synthetic access$5(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 237
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mStrQuery:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$50(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 235
    iput-object p1, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mStrCurFilter:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$51(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;Z)V
    .locals 0

    .prologue
    .line 394
    iput-boolean p1, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->isTaskRunning:Z

    return-void
.end method

.method static synthetic access$52(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;Z)V
    .locals 0

    .prologue
    .line 353
    iput-boolean p1, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mIsSkipFinishForSConnect:Z

    return-void
.end method

.method static synthetic access$53(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;I)V
    .locals 0

    .prologue
    .line 307
    iput p1, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mGirdItemCount:I

    return-void
.end method

.method static synthetic access$54(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;)Z
    .locals 1

    .prologue
    .line 392
    iget-boolean v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mIsSearchRequested:Z

    return v0
.end method

.method static synthetic access$55(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1441
    invoke-direct {p0, p1}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->SetCategoryTitle(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$56(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 330
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mStrCategoryTitle:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$57(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1445
    invoke-direct {p0, p1}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->SetCategoryTitleWithNum(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$58(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;)Z
    .locals 1

    .prologue
    .line 394
    iget-boolean v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->isTaskRunning:Z

    return v0
.end method

.method static synthetic access$6(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 422
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->SEARCH_RESULT_BG_COLOR:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$60(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;)V
    .locals 0

    .prologue
    .line 2494
    invoke-direct {p0}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->startActionMode()V

    return-void
.end method

.method static synthetic access$61(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;)J
    .locals 2

    .prologue
    .line 356
    iget-wide v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mWidgetID:J

    return-wide v0
.end method

.method static synthetic access$62(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;)Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MIME_TYPE;
    .locals 1

    .prologue
    .line 170
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mimeType:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MIME_TYPE;

    return-object v0
.end method

.method static synthetic access$7(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 315
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mIDsForCacheDelete:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$8(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;Z)V
    .locals 0

    .prologue
    .line 355
    iput-boolean p1, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mIsSelectAll:Z

    return-void
.end method

.method static synthetic access$9(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;)Landroid/widget/AbsListView;
    .locals 1

    .prologue
    .line 303
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mAbsListView:Landroid/widget/AbsListView;

    return-object v0
.end method

.method private cancelDeletingMemos()V
    .locals 3

    .prologue
    .line 2067
    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mActivity:Landroid/app/Activity;

    if-nez v1, :cond_1

    .line 2076
    :cond_0
    :goto_0
    return-void

    .line 2069
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mActivity:Landroid/app/Activity;

    iget-object v2, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mIDsBeingDeleted:Ljava/util/ArrayList;

    invoke-static {v1, v2}, Lcom/samsung/android/app/memo/MemoDataHandler;->cancelDeletingMemos(Landroid/content/Context;Ljava/util/ArrayList;)V
    :try_end_0
    .catch Lcom/samsung/android/app/memo/MemoDataHandler$MemoDataHandlerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2073
    :goto_1
    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mAbsListView:Landroid/widget/AbsListView;

    if-eqz v1, :cond_0

    .line 2074
    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mAbsListView:Landroid/widget/AbsListView;

    invoke-virtual {v1}, Landroid/widget/AbsListView;->requestFocus()Z

    goto :goto_0

    .line 2070
    :catch_0
    move-exception v0

    .line 2071
    .local v0, "se":Lcom/samsung/android/app/memo/MemoDataHandler$MemoDataHandlerException;
    sget-object v1, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->TAG:Ljava/lang/String;

    const-string v2, "cancelDeletingMemos"

    invoke-static {v1, v2, v0}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1
.end method

.method private deleteMemos()V
    .locals 3

    .prologue
    .line 2080
    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mActivity:Landroid/app/Activity;

    if-nez v1, :cond_0

    .line 2086
    :goto_0
    return-void

    .line 2082
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mActivity:Landroid/app/Activity;

    iget-object v2, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mIDsBeingDeleted:Ljava/util/ArrayList;

    invoke-static {v1, v2}, Lcom/samsung/android/app/memo/MemoDataHandler;->deleteMemos(Landroid/content/Context;Ljava/util/ArrayList;)V
    :try_end_0
    .catch Lcom/samsung/android/app/memo/MemoDataHandler$MemoDataHandlerException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 2083
    :catch_0
    move-exception v0

    .line 2084
    .local v0, "se":Lcom/samsung/android/app/memo/MemoDataHandler$MemoDataHandlerException;
    sget-object v1, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->TAG:Ljava/lang/String;

    const-string v2, "deleteMemos"

    invoke-static {v1, v2, v0}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private deletingMemos(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2053
    .local p1, "memoIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mIDsBeingDeleted:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 2055
    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mActivity:Landroid/app/Activity;

    if-nez v1, :cond_0

    .line 2063
    :goto_0
    return-void

    .line 2057
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mActivity:Landroid/app/Activity;

    iget-object v2, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mIDsBeingDeleted:Ljava/util/ArrayList;

    invoke-static {v1, v2}, Lcom/samsung/android/app/memo/MemoDataHandler;->deletingMemos(Landroid/content/Context;Ljava/util/ArrayList;)V
    :try_end_0
    .catch Lcom/samsung/android/app/memo/MemoDataHandler$MemoDataHandlerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2061
    :goto_1
    invoke-direct {p0}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->showUndoBar()V

    .line 2062
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->reloadedCursor()V

    goto :goto_0

    .line 2058
    :catch_0
    move-exception v0

    .line 2059
    .local v0, "se":Lcom/samsung/android/app/memo/MemoDataHandler$MemoDataHandlerException;
    sget-object v1, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->TAG:Ljava/lang/String;

    const-string v2, "deletingMemos"

    invoke-static {v1, v2, v0}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1
.end method

.method private getHtmlContent(J)Ljava/lang/String;
    .locals 5
    .param p1, "id"    # J

    .prologue
    .line 2997
    const-string v0, ""

    .line 2999
    .local v0, "content":Ljava/lang/String;
    :try_start_0
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    long-to-int v3, p1

    invoke-static {v2, v3}, Lcom/samsung/android/app/memo/MemoDataHandler;->getContent(Landroid/content/Context;I)Ljava/lang/String;
    :try_end_0
    .catch Lcom/samsung/android/app/memo/MemoDataHandler$MemoDataHandlerException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 3003
    :goto_0
    return-object v0

    .line 3000
    :catch_0
    move-exception v1

    .line 3001
    .local v1, "e":Lcom/samsung/android/app/memo/MemoDataHandler$MemoDataHandlerException;
    sget-object v2, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->TAG:Ljava/lang/String;

    const-string v3, "getHtmlContent"

    invoke-static {v2, v3, v1}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private getImageUriList(J)Ljava/util/ArrayList;
    .locals 5
    .param p1, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation

    .prologue
    .line 3017
    const/4 v0, 0x0

    .line 3019
    .local v0, "content":Ljava/lang/String;
    :try_start_0
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    long-to-int v3, p1

    invoke-static {v2, v3}, Lcom/samsung/android/app/memo/MemoDataHandler;->getContent(Landroid/content/Context;I)Ljava/lang/String;
    :try_end_0
    .catch Lcom/samsung/android/app/memo/MemoDataHandler$MemoDataHandlerException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 3023
    :goto_0
    invoke-static {v0}, Lcom/samsung/android/app/memo/util/ImageUtil;->getImageUriList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    return-object v2

    .line 3020
    :catch_0
    move-exception v1

    .line 3021
    .local v1, "e":Lcom/samsung/android/app/memo/MemoDataHandler$MemoDataHandlerException;
    sget-object v2, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->TAG:Ljava/lang/String;

    const-string v3, "getImageUris"

    invoke-static {v2, v3, v1}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private getMimeType(Ljava/lang/String;Z)I
    .locals 1
    .param p1, "type"    # Ljava/lang/String;
    .param p2, "getAll"    # Z

    .prologue
    .line 2956
    if-nez p1, :cond_0

    .line 2957
    sget-object v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MIME_TYPE;->INVALID:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MIME_TYPE;

    invoke-virtual {v0}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MIME_TYPE;->ordinal()I

    move-result v0

    .line 2972
    :goto_0
    return v0

    .line 2958
    :cond_0
    const-string v0, "text/plain"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2959
    if-eqz p2, :cond_1

    .line 2960
    sget-object v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MIME_TYPE;->TEXT_IMAGE_AUDIO:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MIME_TYPE;

    invoke-virtual {v0}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MIME_TYPE;->ordinal()I

    move-result v0

    goto :goto_0

    .line 2962
    :cond_1
    sget-object v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MIME_TYPE;->TEXT_PLAIN:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MIME_TYPE;

    invoke-virtual {v0}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MIME_TYPE;->ordinal()I

    move-result v0

    goto :goto_0

    .line 2963
    :cond_2
    const-string v0, "image/*"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2964
    sget-object v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MIME_TYPE;->IMAGE_ALL:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MIME_TYPE;

    invoke-virtual {v0}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MIME_TYPE;->ordinal()I

    move-result v0

    goto :goto_0

    .line 2965
    :cond_3
    const-string v0, "audio/*"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2966
    sget-object v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MIME_TYPE;->AUDIO_ALL:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MIME_TYPE;

    invoke-virtual {v0}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MIME_TYPE;->ordinal()I

    move-result v0

    goto :goto_0

    .line 2967
    :cond_4
    const-string v0, "audio/mp4"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 2968
    sget-object v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MIME_TYPE;->AUDIO_MP4:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MIME_TYPE;

    invoke-virtual {v0}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MIME_TYPE;->ordinal()I

    move-result v0

    goto :goto_0

    .line 2969
    :cond_5
    const-string v0, "application/vnd.samsung.memo"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 2970
    sget-object v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MIME_TYPE;->APPLICATION_MEMO:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MIME_TYPE;

    invoke-virtual {v0}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MIME_TYPE;->ordinal()I

    move-result v0

    goto :goto_0

    .line 2972
    :cond_6
    sget-object v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MIME_TYPE;->INVALID:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MIME_TYPE;

    invoke-virtual {v0}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MIME_TYPE;->ordinal()I

    move-result v0

    goto :goto_0
.end method

.method private getText(J)Ljava/lang/String;
    .locals 7
    .param p1, "id"    # J

    .prologue
    .line 2986
    const-string v0, ""

    .line 2988
    .local v0, "content":Ljava/lang/String;
    :try_start_0
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    long-to-int v3, p1

    invoke-static {v2, v3}, Lcom/samsung/android/app/memo/MemoDataHandler;->getContent(Landroid/content/Context;I)Ljava/lang/String;
    :try_end_0
    .catch Lcom/samsung/android/app/memo/MemoDataHandler$MemoDataHandlerException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 2992
    :goto_0
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    .line 2993
    const/4 v3, 0x0

    const-wide/16 v4, -0x1

    .line 2992
    invoke-static {v0, v2, v3, v4, v5}, Lcom/samsung/android/app/memo/util/HtmlUtil;->getPlainTextFromHTML(Ljava/lang/String;Landroid/content/Context;Ljava/lang/Object;J)Ljava/lang/String;

    move-result-object v2

    return-object v2

    .line 2989
    :catch_0
    move-exception v1

    .line 2990
    .local v1, "e":Lcom/samsung/android/app/memo/MemoDataHandler$MemoDataHandlerException;
    sget-object v2, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->TAG:Ljava/lang/String;

    const-string v3, "getText"

    invoke-static {v2, v3, v1}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private getTitle(J)Ljava/lang/String;
    .locals 5
    .param p1, "id"    # J

    .prologue
    .line 2976
    const-string v1, ""

    .line 2978
    .local v1, "title":Ljava/lang/String;
    :try_start_0
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    long-to-int v3, p1

    invoke-static {v2, v3}, Lcom/samsung/android/app/memo/MemoDataHandler;->getMemoTitle(Landroid/content/Context;I)Ljava/lang/String;
    :try_end_0
    .catch Lcom/samsung/android/app/memo/MemoDataHandler$MemoDataHandlerException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 2982
    :goto_0
    return-object v1

    .line 2979
    :catch_0
    move-exception v0

    .line 2980
    .local v0, "e":Lcom/samsung/android/app/memo/MemoDataHandler$MemoDataHandlerException;
    sget-object v2, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->TAG:Ljava/lang/String;

    const-string v3, "getText"

    invoke-static {v2, v3, v0}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private getVoiceUri(J)Landroid/net/Uri;
    .locals 5
    .param p1, "id"    # J

    .prologue
    .line 3007
    const/4 v1, 0x0

    .line 3009
    .local v1, "voiceUri":Landroid/net/Uri;
    :try_start_0
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    long-to-int v3, p1

    invoke-static {v2, v3}, Lcom/samsung/android/app/memo/MemoDataHandler;->getVoiceUri(Landroid/content/Context;I)Landroid/net/Uri;
    :try_end_0
    .catch Lcom/samsung/android/app/memo/MemoDataHandler$MemoDataHandlerException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 3013
    :goto_0
    return-object v1

    .line 3010
    :catch_0
    move-exception v0

    .line 3011
    .local v0, "e":Lcom/samsung/android/app/memo/MemoDataHandler$MemoDataHandlerException;
    sget-object v2, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->TAG:Ljava/lang/String;

    const-string v3, "getVoiceUri"

    invoke-static {v2, v3, v0}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private handlePickOne()V
    .locals 14

    .prologue
    .line 2723
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v11

    invoke-virtual {v11}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v4

    .line 2724
    .local v4, "intentReceived":Landroid/content/Intent;
    invoke-virtual {v4}, Landroid/content/Intent;->getType()Ljava/lang/String;

    move-result-object v6

    .line 2725
    .local v6, "mimeTypeReceived":Ljava/lang/String;
    const-string v11, "PICKALL"

    const/4 v12, 0x0

    invoke-virtual {v4, v11, v12}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v11

    invoke-static {v11}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    .line 2726
    .local v1, "getAll":Ljava/lang/Boolean;
    const/4 v5, 0x0

    .line 2727
    .local v5, "mCategoryListFragment":Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;
    const-string v11, "from_widget"

    const/4 v12, 0x0

    invoke-virtual {v4, v11, v12}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v11

    iput-boolean v11, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mIsWidgetPick:Z

    .line 2728
    iget-boolean v11, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mIsWidgetPick:Z

    if-eqz v11, :cond_0

    .line 2729
    const-string v11, "appWidgetId"

    const-wide/16 v12, -0x1

    invoke-virtual {v4, v11, v12, v13}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v12

    iput-wide v12, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mWidgetID:J

    .line 2731
    :cond_0
    invoke-static {}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MIME_TYPE;->values()[Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MIME_TYPE;

    move-result-object v11

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v12

    invoke-direct {p0, v6, v12}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->getMimeType(Ljava/lang/String;Z)I

    move-result v12

    aget-object v11, v11, v12

    iput-object v11, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mimeType:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MIME_TYPE;

    .line 2733
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v11

    const v12, 0x7f0e0028

    invoke-virtual {v11, v12}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/view/ViewStub;

    .line 2734
    .local v10, "vstub":Landroid/view/ViewStub;
    const/4 v0, 0x0

    .line 2735
    .local v0, "fm":Landroid/widget/LinearLayout;
    if-eqz v10, :cond_1

    .line 2736
    invoke-virtual {v10}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    .end local v0    # "fm":Landroid/widget/LinearLayout;
    check-cast v0, Landroid/widget/LinearLayout;

    .line 2737
    .restart local v0    # "fm":Landroid/widget/LinearLayout;
    const/4 v11, 0x0

    invoke-virtual {v0, v11}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2740
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v11

    const v12, 0x7f0e0004

    invoke-virtual {v11, v12}, Landroid/app/FragmentManager;->findFragmentById(I)Landroid/app/Fragment;

    move-result-object v11

    if-eqz v11, :cond_3

    .line 2741
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v11

    .line 2742
    const v12, 0x7f0e0004

    invoke-virtual {v11, v12}, Landroid/app/FragmentManager;->findFragmentById(I)Landroid/app/Fragment;

    move-result-object v5

    .line 2741
    .end local v5    # "mCategoryListFragment":Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;
    check-cast v5, Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;

    .line 2750
    .restart local v5    # "mCategoryListFragment":Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;
    :goto_0
    invoke-static {}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->$SWITCH_TABLE$com$samsung$android$app$memo$uifragment$MemoListFragment$MIME_TYPE()[I

    move-result-object v11

    iget-object v12, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mimeType:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MIME_TYPE;

    invoke-virtual {v12}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MIME_TYPE;->ordinal()I

    move-result v12

    aget v11, v11, v12

    packed-switch v11, :pswitch_data_0

    .line 2785
    :goto_1
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->getLoaderManager()Landroid/app/LoaderManager;

    move-result-object v11

    const/16 v12, 0x3e8

    const/4 v13, 0x0

    invoke-virtual {v11, v12, v13, p0}, Landroid/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroid/app/LoaderManager$LoaderCallbacks;)Landroid/content/Loader;

    .line 2786
    iget-object v11, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mMenuType:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeStyle;

    sget-object v12, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeStyle;->PICK_MULTIPLE:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeStyle;

    if-ne v11, v12, :cond_4

    .line 2787
    iget-object v11, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mAbsListView:Landroid/widget/AbsListView;

    const/4 v12, 0x3

    invoke-virtual {v11, v12}, Landroid/widget/AbsListView;->setChoiceMode(I)V

    .line 2788
    new-instance v11, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;

    const/4 v12, 0x0

    invoke-direct {v11, p0, v12}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;-><init>(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;)V

    iput-object v11, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mActionModeCallback:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;

    .line 2789
    iget-object v11, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mAbsListView:Landroid/widget/AbsListView;

    iget-object v12, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mActionModeCallback:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;

    invoke-virtual {v11, v12}, Landroid/widget/AbsListView;->setMultiChoiceModeListener(Landroid/widget/AbsListView$MultiChoiceModeListener;)V

    .line 2790
    new-instance v2, Landroid/os/Handler;

    invoke-direct {v2}, Landroid/os/Handler;-><init>()V

    .line 2791
    .local v2, "handler":Landroid/os/Handler;
    new-instance v11, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$13;

    invoke-direct {v11, p0}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$13;-><init>(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;)V

    .line 2803
    const-wide/16 v12, 0x12c

    .line 2791
    invoke-virtual {v2, v11, v12, v13}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 2903
    .end local v2    # "handler":Landroid/os/Handler;
    :goto_2
    const/4 v11, 0x0

    sput-boolean v11, Lcom/samsung/android/app/memo/Main;->mIsUiPickerMode:Z

    .line 2904
    :cond_2
    return-void

    .line 2743
    :cond_3
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v11

    const-string v12, "category_drawer"

    invoke-virtual {v11, v12}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v11

    if-eqz v11, :cond_2

    .line 2744
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v11

    .line 2745
    const-string v12, "category_drawer"

    invoke-virtual {v11, v12}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v5

    .line 2744
    .end local v5    # "mCategoryListFragment":Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;
    check-cast v5, Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;

    .line 2746
    .restart local v5    # "mCategoryListFragment":Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;
    goto :goto_0

    .line 2752
    :pswitch_0
    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, " AND (LTRIM(RTRIM("

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 2753
    const-string v12, "memo"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const/16 v12, 0x2e

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "strippedContent"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    .line 2754
    const-string v12, ")) <>\'\'"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "OR LTRIM(RTRIM("

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "memo"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    .line 2755
    const/16 v12, 0x2e

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "title"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ")) <>\'\'"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " OR "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    .line 2756
    const-string v12, "_data"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " <>\'\'"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " OR "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "memo"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    .line 2757
    const/16 v12, 0x2e

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "vrfileUUID"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " <>\'\')"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    .line 2758
    .local v8, "textImageFilter":Ljava/lang/StringBuilder;
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v5, v11}, Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;->setPickOneFilter(Ljava/lang/String;)V

    .line 2759
    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "isDeleted IS 0"

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    iput-object v11, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mStrSelection:Ljava/lang/String;

    goto/16 :goto_1

    .line 2762
    .end local v8    # "textImageFilter":Ljava/lang/StringBuilder;
    :pswitch_1
    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, " AND LTRIM(RTRIM("

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 2763
    const-string v12, "memo"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const/16 v12, 0x2e

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "strippedContent"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    .line 2764
    const-string v12, ")) <>\'\'"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    .line 2765
    .local v7, "textFilter":Ljava/lang/StringBuilder;
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v5, v11}, Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;->setPickOneFilter(Ljava/lang/String;)V

    .line 2766
    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "isDeleted IS 0"

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    iput-object v11, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mStrSelection:Ljava/lang/String;

    goto/16 :goto_1

    .line 2769
    .end local v7    # "textFilter":Ljava/lang/StringBuilder;
    :pswitch_2
    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, " AND "

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v12, "memo"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    .line 2770
    const/16 v12, 0x2e

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "_data"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " <>\'\'"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 2771
    .local v3, "imageFilter":Ljava/lang/StringBuilder;
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v5, v11}, Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;->setPickOneFilter(Ljava/lang/String;)V

    .line 2772
    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "isDeleted IS 0"

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    iput-object v11, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mStrSelection:Ljava/lang/String;

    goto/16 :goto_1

    .line 2776
    .end local v3    # "imageFilter":Ljava/lang/StringBuilder;
    :pswitch_3
    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, " AND "

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v12, "memo"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    .line 2777
    const/16 v12, 0x2e

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "vrfileUUID"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " <>\'\'"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    .line 2778
    .local v9, "voiceFilter":Ljava/lang/StringBuilder;
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v5, v11}, Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;->setPickOneFilter(Ljava/lang/String;)V

    .line 2779
    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "isDeleted IS 0"

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    iput-object v11, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mStrSelection:Ljava/lang/String;

    goto/16 :goto_1

    .line 2805
    .end local v9    # "voiceFilter":Ljava/lang/StringBuilder;
    :cond_4
    iget-object v11, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mAbsListView:Landroid/widget/AbsListView;

    new-instance v12, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$14;

    invoke-direct {v12, p0}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$14;-><init>(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;)V

    invoke-virtual {v11, v12}, Landroid/widget/AbsListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    goto/16 :goto_2

    .line 2750
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_3
        :pswitch_3
        :pswitch_0
    .end packed-switch
.end method

.method private hideIme()V
    .locals 3

    .prologue
    .line 2606
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mSearchView:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MySearchView;

    if-eqz v1, :cond_0

    .line 2607
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const-string v2, "input_method"

    invoke-virtual {v1, v2}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 2608
    .local v0, "Imm":Landroid/view/inputmethod/InputMethodManager;
    invoke-virtual {v0}, Landroid/view/inputmethod/InputMethodManager;->isActive()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2609
    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mSearchView:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MySearchView;

    invoke-virtual {v1}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MySearchView;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 2612
    .end local v0    # "Imm":Landroid/view/inputmethod/InputMethodManager;
    :cond_0
    return-void
.end method

.method private initSConnect(Landroid/app/Activity;)V
    .locals 2
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 2639
    sget-boolean v0, Lcom/samsung/android/app/memo/util/Utils;->HAVE_SLOOKBEZELINTERACTION_API:Z

    if-eqz v0, :cond_0

    .line 2641
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const-string v1, "quickconnect"

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/quickconnect/QuickConnectManager;

    iput-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mBezelManager:Lcom/samsung/android/quickconnect/QuickConnectManager;

    .line 2642
    new-instance v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$12;

    invoke-direct {v0, p0, p1}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$12;-><init>(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;Landroid/app/Activity;)V

    iput-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mListener:Lcom/samsung/android/quickconnect/QuickConnectManager$QuickConnectListener;

    .line 2656
    :cond_0
    return-void
.end method

.method private initView(Landroid/view/View;)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 461
    new-instance v0, Lcom/samsung/android/app/memo/uiwidget/UndoBarController;

    const v1, 0x7f0e0039

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-direct {v0, v1, p0}, Lcom/samsung/android/app/memo/uiwidget/UndoBarController;-><init>(Landroid/view/View;Lcom/samsung/android/app/memo/uiwidget/UndoBarController$UndoListener;)V

    iput-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mUndoBarController:Lcom/samsung/android/app/memo/uiwidget/UndoBarController;

    .line 462
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mIDsBeingDeleted:Ljava/util/ArrayList;

    .line 464
    invoke-static {}, Lcom/samsung/android/app/memo/util/Utils;->isTablet()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 465
    const v0, 0x7f0e003c

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/GridView;

    iput-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mAbsListView:Landroid/widget/AbsListView;

    .line 469
    :goto_0
    const v0, 0x7f0e0038

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mFabButton:Landroid/widget/ImageButton;

    .line 470
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mFabButton:Landroid/widget/ImageButton;

    new-instance v1, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$1;

    invoke-direct {v1, p0}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$1;-><init>(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 490
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mAbsListView:Landroid/widget/AbsListView;

    if-eqz v0, :cond_0

    .line 491
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mAbsListView:Landroid/widget/AbsListView;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/widget/AbsListView;->setImportantForAccessibility(I)V

    .line 493
    :cond_0
    const v0, 0x7f0e0013

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mSelectAllLayout:Landroid/widget/LinearLayout;

    .line 494
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mSelectAllLayout:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_1

    .line 495
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mSelectAllLayout:Landroid/widget/LinearLayout;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setFocusable(Z)V

    .line 496
    :cond_1
    new-instance v0, Lcom/samsung/android/app/memo/uiwidget/MemoDragListener;

    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/samsung/android/app/memo/uiwidget/MemoDragListener;-><init>(Landroid/content/Context;)V

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnDragListener(Landroid/view/View$OnDragListener;)V

    .line 498
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mAbsListView:Landroid/widget/AbsListView;

    if-eqz v0, :cond_2

    .line 499
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mAbsListView:Landroid/widget/AbsListView;

    invoke-virtual {v0}, Landroid/widget/AbsListView;->requestFocus()Z

    .line 501
    :cond_2
    invoke-static {}, Lcom/samsung/android/app/memo/util/Utils;->IsNoMemosPopupModel()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 502
    sget v0, Lcom/samsung/android/app/memo/util/Utils;->NO_MEMO_POPUP:I

    invoke-static {p1, v0}, Lcom/samsung/android/app/memo/util/Utils;->setNoMemosPopupText(Landroid/view/View;I)V

    .line 505
    :cond_3
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mStrCategoryTitle:Ljava/lang/String;

    if-nez v0, :cond_4

    .line 506
    const v0, 0x7f0b000d

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mStrCategoryTitle:Ljava/lang/String;

    .line 516
    :cond_4
    return-void

    .line 467
    :cond_5
    const v0, 0x7f0e0037

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mAbsListView:Landroid/widget/AbsListView;

    goto :goto_0
.end method

.method public static isInActionmode()Z
    .locals 1

    .prologue
    .line 2491
    sget-boolean v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mIsActionmode:Z

    return v0
.end method

.method private loadPreferences()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1430
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    sget-object v2, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->PREF_CATEGORY:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/app/Activity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 1431
    .local v0, "pref":Landroid/content/SharedPreferences;
    sget-object v1, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->KEY_CATEGORY_UUID:Ljava/lang/String;

    const-string v2, ""

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mStrCategoryUUID:Ljava/lang/String;

    .line 1432
    sget-object v1, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->KEY_IS_CATEGORY_CHANGE:Ljava/lang/String;

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mIsCategoryChanged:Z

    .line 1434
    sget-object v1, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->KEY_CATEGORY_TITLE:Ljava/lang/String;

    const-string v2, ""

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1435
    sget-object v1, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->KEY_CATEGORY_TITLE:Ljava/lang/String;

    const-string v2, ""

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mStrCategoryTitle:Ljava/lang/String;

    .line 1437
    :cond_0
    sget-object v1, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->KEY_CATEGORY_TYPE:Ljava/lang/String;

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    sput v1, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mCategoryType:I

    .line 1438
    sget-object v1, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "UUID : : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mStrCategoryUUID:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "    Title: : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mStrCategoryTitle:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1439
    return-void
.end method

.method private onSelectSCloud()V
    .locals 4

    .prologue
    .line 1134
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-static {v2}, Lcom/samsung/android/app/migration/utils/SharedPref;->isSADoNotShowFlagSet(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1135
    invoke-static {}, Lcom/samsung/android/app/migration/dialog/SamsungAccountDialog;->newInstance()Lcom/samsung/android/app/migration/dialog/SamsungAccountDialog;

    move-result-object v2

    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/samsung/android/app/migration/dialog/SamsungAccountDialog;->show(Landroid/app/Activity;)V

    .line 1153
    :goto_0
    return-void

    .line 1137
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-static {v2}, Lcom/samsung/android/app/memo/util/AccountHelper;->getRegisteredSamsungAccount(Landroid/content/Context;)Landroid/accounts/Account;

    move-result-object v0

    .line 1138
    .local v0, "account":Landroid/accounts/Account;
    if-nez v0, :cond_1

    .line 1139
    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.osp.app.signin.action.ADD_SAMSUNG_ACCOUNT"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1140
    .local v1, "intent":Landroid/content/Intent;
    const-string v2, "client_id"

    const-string v3, "kqq79c436g"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1141
    const-string v2, "client_secret"

    const-string v3, "51957371B6C4D7552C91EF680479AAE2"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1142
    const-string v2, "mypackage"

    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/Activity;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1143
    const-string v2, "OSP_VER"

    const-string v3, "OSP_02"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1144
    const-string v2, "MODE"

    const-string v3, "ADD_ACCOUNT"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1145
    invoke-virtual {p0, v1}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 1147
    .end local v1    # "intent":Landroid/content/Intent;
    :cond_1
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.settings.ACCOUNT_SYNC_SETTINGS"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1148
    .restart local v1    # "intent":Landroid/content/Intent;
    const/high16 v2, 0x4000000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 1149
    const-string v2, "account"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1150
    invoke-virtual {p0, v1}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method private savePreferences()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1417
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 1418
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    sget-object v3, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->PREF_CATEGORY:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/app/Activity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 1419
    .local v1, "pref":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 1420
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    sget-object v2, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->KEY_CATEGORY_UUID:Ljava/lang/String;

    iget-object v3, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mStrCategoryUUID:Ljava/lang/String;

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 1421
    sget-object v2, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->KEY_CATEGORY_TITLE:Ljava/lang/String;

    iget-object v3, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mStrCategoryTitle:Ljava/lang/String;

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 1422
    sget-object v2, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->KEY_IS_CATEGORY_CHANGE:Ljava/lang/String;

    invoke-interface {v0, v2, v4}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 1424
    sget-object v2, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->KEY_CATEGORY_TYPE:Ljava/lang/String;

    sget v3, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mCategoryType:I

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 1425
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 1427
    .end local v0    # "editor":Landroid/content/SharedPreferences$Editor;
    .end local v1    # "pref":Landroid/content/SharedPreferences;
    :cond_0
    return-void
.end method

.method private showNoMemo(Z)V
    .locals 12
    .param p1, "visible"    # Z

    .prologue
    const/16 v7, 0x8

    const/4 v8, 0x1

    const/4 v11, 0x0

    const v10, 0x7f0e0035

    const/4 v6, 0x0

    .line 791
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v5

    iput-object v5, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mActivity:Landroid/app/Activity;

    .line 792
    iget-object v5, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mActivity:Landroid/app/Activity;

    if-nez v5, :cond_1

    .line 854
    :cond_0
    :goto_0
    return-void

    .line 795
    :cond_1
    iget-object v5, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mActivity:Landroid/app/Activity;

    const v9, 0x7f0e0027

    invoke-virtual {v5, v9}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/FrameLayout;

    .line 796
    .local v1, "f":Landroid/widget/FrameLayout;
    if-eqz v1, :cond_2

    .line 797
    if-eqz p1, :cond_b

    .line 798
    iget-object v5, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mActivity:Landroid/app/Activity;

    invoke-virtual {v5}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v9, 0x7f0b0026

    invoke-virtual {v5, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Landroid/widget/FrameLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 803
    :cond_2
    :goto_1
    iget-object v5, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mActivity:Landroid/app/Activity;

    const v9, 0x7f0e009e

    invoke-virtual {v5, v9}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 804
    .local v2, "menuCreate":Landroid/view/View;
    iget-object v5, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mActivity:Landroid/app/Activity;

    const v9, 0x7f0e0034

    invoke-virtual {v5, v9}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 806
    .local v4, "t":Landroid/widget/TextView;
    iget-object v5, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mMenuSearch:Landroid/view/MenuItem;

    if-eqz v5, :cond_c

    iget-object v5, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mMenuSearch:Landroid/view/MenuItem;

    invoke-interface {v5}, Landroid/view/MenuItem;->isActionViewExpanded()Z

    move-result v5

    if-eqz v5, :cond_c

    .line 807
    const v5, 0x7f0b0028

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 808
    iget-object v5, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mActivity:Landroid/app/Activity;

    invoke-virtual {v5, v10}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v5, v7}, Landroid/view/View;->setVisibility(I)V

    .line 810
    invoke-virtual {v4, v11, v11, v11, v11}, Landroid/widget/TextView;->setCompoundDrawablesRelativeWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 819
    :cond_3
    :goto_2
    iget-object v5, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mActivity:Landroid/app/Activity;

    const v9, 0x7f0e0034

    invoke-virtual {v5, v9}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    if-eqz p1, :cond_4

    move v7, v6

    :cond_4
    invoke-virtual {v5, v7}, Landroid/view/View;->setVisibility(I)V

    .line 820
    iget-object v5, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mActivity:Landroid/app/Activity;

    instance-of v5, v5, Lcom/samsung/android/app/memo/ExternalPicker;

    if-nez v5, :cond_5

    iget-boolean v5, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mIsWidgetPick:Z

    if-nez v5, :cond_5

    iget-object v5, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mMenuType:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeStyle;

    sget-object v7, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeStyle;->PICK_ONE:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeStyle;

    if-ne v5, v7, :cond_6

    .line 821
    :cond_5
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 822
    .local v3, "res":Landroid/content/res/Resources;
    iget-object v5, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mActivity:Landroid/app/Activity;

    .line 823
    invoke-virtual {v5, v10}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v5}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 822
    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 825
    .local v0, "buttonLayoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    const v5, 0x7f0a000f

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v5

    .line 824
    iput v5, v0, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    .line 826
    iget-object v5, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mActivity:Landroid/app/Activity;

    .line 827
    invoke-virtual {v5, v10}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v5, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 828
    iget-object v5, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mActivity:Landroid/app/Activity;

    invoke-virtual {v5, v10}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v5}, Landroid/view/View;->invalidate()V

    .line 831
    .end local v0    # "buttonLayoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    .end local v3    # "res":Landroid/content/res/Resources;
    :cond_6
    iget-object v5, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mMenuSelect:Landroid/view/MenuItem;

    if-eqz v5, :cond_7

    .line 832
    iget-object v7, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mMenuSelect:Landroid/view/MenuItem;

    if-eqz p1, :cond_e

    move v5, v6

    :goto_3
    invoke-interface {v7, v5}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 834
    :cond_7
    iget-object v5, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mMenuDelete:Landroid/view/MenuItem;

    if-eqz v5, :cond_8

    .line 835
    iget-object v7, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mMenuDelete:Landroid/view/MenuItem;

    if-eqz p1, :cond_f

    move v5, v6

    :goto_4
    invoke-interface {v7, v5}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 837
    :cond_8
    iget-object v5, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mMenuSearch:Landroid/view/MenuItem;

    if-eqz v5, :cond_9

    .line 838
    iget-object v5, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mMenuSearch:Landroid/view/MenuItem;

    if-eqz p1, :cond_10

    :goto_5
    invoke-interface {v5, v6}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 840
    :cond_9
    if-eqz p1, :cond_a

    if-eqz v2, :cond_a

    iget-object v5, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mMenuSearch:Landroid/view/MenuItem;

    if-eqz v5, :cond_a

    iget-object v5, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mMenuSearch:Landroid/view/MenuItem;

    invoke-interface {v5}, Landroid/view/MenuItem;->isActionViewExpanded()Z

    move-result v5

    if-nez v5, :cond_a

    .line 841
    invoke-virtual {v2}, Landroid/view/View;->requestFocus()Z

    .line 843
    :cond_a
    if-eqz p1, :cond_0

    iget-object v5, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mActivity:Landroid/app/Activity;

    const v6, 0x7f0e009e

    invoke-virtual {v5, v6}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    if-nez v5, :cond_0

    .line 844
    new-instance v5, Landroid/os/Handler;

    invoke-direct {v5}, Landroid/os/Handler;-><init>()V

    new-instance v6, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$6;

    invoke-direct {v6, p0}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$6;-><init>(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;)V

    .line 852
    const-wide/16 v8, 0x12c

    .line 844
    invoke-virtual {v5, v6, v8, v9}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/16 :goto_0

    .line 800
    .end local v2    # "menuCreate":Landroid/view/View;
    .end local v4    # "t":Landroid/widget/TextView;
    :cond_b
    const-string v5, ""

    invoke-virtual {v1, v5}, Landroid/widget/FrameLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 813
    .restart local v2    # "menuCreate":Landroid/view/View;
    .restart local v4    # "t":Landroid/widget/TextView;
    :cond_c
    const v5, 0x7f0b0026

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 814
    invoke-static {}, Lcom/samsung/android/app/memo/util/Utils;->IsNoMemosPopupModel()Z

    move-result v5

    if-eqz v5, :cond_3

    .line 815
    iget-object v5, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mActivity:Landroid/app/Activity;

    invoke-virtual {v5, v10}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v9

    if-eqz p1, :cond_d

    move v5, v6

    :goto_6
    invoke-virtual {v9, v5}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_2

    :cond_d
    move v5, v7

    goto :goto_6

    :cond_e
    move v5, v8

    .line 832
    goto :goto_3

    :cond_f
    move v5, v8

    .line 835
    goto :goto_4

    :cond_10
    move v6, v8

    .line 838
    goto :goto_5
.end method

.method private showUndoBar()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 2031
    iget-object v3, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mIDsBeingDeleted:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 2032
    .local v0, "deleteCount":I
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->isAdded()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 2033
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 2034
    .local v1, "res":Landroid/content/res/Resources;
    const v3, 0x7f0b005d

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 2036
    .local v2, "strDeletingdMemos":Ljava/lang/String;
    iget-object v3, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mUndoBarController:Lcom/samsung/android/app/memo/uiwidget/UndoBarController;

    const/4 v4, 0x0

    invoke-virtual {v3, v6, v2, v4}, Lcom/samsung/android/app/memo/uiwidget/UndoBarController;->showUndoBar(ZLjava/lang/CharSequence;Landroid/os/Parcelable;)V

    .line 2038
    .end local v1    # "res":Landroid/content/res/Resources;
    .end local v2    # "strDeletingdMemos":Ljava/lang/String;
    :cond_0
    return-void
.end method

.method private startActionMode()V
    .locals 3

    .prologue
    .line 2495
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mAbsListView:Landroid/widget/AbsListView;

    const/4 v1, -0x1

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/widget/AbsListView;->setItemChecked(IZ)V

    .line 2496
    return-void
.end method


# virtual methods
.method public finishActionMode()V
    .locals 1

    .prologue
    .line 2499
    sget-boolean v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mIsActionmode:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mActionMode:Landroid/view/ActionMode;

    if-eqz v0, :cond_0

    .line 2500
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mActionMode:Landroid/view/ActionMode;

    invoke-virtual {v0}, Landroid/view/ActionMode;->finish()V

    .line 2502
    :cond_0
    return-void
.end method

.method public hideUndoBar()V
    .locals 2

    .prologue
    .line 2046
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mUndoBarController:Lcom/samsung/android/app/memo/uiwidget/UndoBarController;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mUndoBarController:Lcom/samsung/android/app/memo/uiwidget/UndoBarController;

    invoke-virtual {v0}, Lcom/samsung/android/app/memo/uiwidget/UndoBarController;->isVisible()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2047
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mUndoBarController:Lcom/samsung/android/app/memo/uiwidget/UndoBarController;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/memo/uiwidget/UndoBarController;->hideUndoBar(Z)V

    .line 2048
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->onHideUndoBar()V

    .line 2050
    :cond_0
    return-void
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 8
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/16 v7, 0x3e8

    const/4 v3, 0x0

    const/4 v4, 0x0

    .line 520
    invoke-super {p0, p1}, Landroid/app/Fragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 521
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->setHasOptionsMenu(Z)V

    .line 522
    iput-boolean v4, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mIsCheckBoxMode:Z

    .line 524
    new-instance v6, Lcom/samsung/android/app/memo/util/ImageCache$ImageCacheParams;

    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-direct {v6, v0}, Lcom/samsung/android/app/memo/util/ImageCache$ImageCacheParams;-><init>(Landroid/content/Context;)V

    .line 525
    .local v6, "cacheParams":Lcom/samsung/android/app/memo/util/ImageCache$ImageCacheParams;
    const/high16 v0, 0x3e800000    # 0.25f

    invoke-virtual {v6, v0}, Lcom/samsung/android/app/memo/util/ImageCache$ImageCacheParams;->setMemCacheSizePercent(F)V

    .line 526
    new-instance v0, Lcom/samsung/android/app/memo/util/ImageLoader;

    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/samsung/android/app/memo/util/ImageLoader;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mImageLoader:Lcom/samsung/android/app/memo/util/ImageLoader;

    .line 527
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mImageLoader:Lcom/samsung/android/app/memo/util/ImageLoader;

    invoke-virtual {v0, v6}, Lcom/samsung/android/app/memo/util/ImageLoader;->addImageCache(Lcom/samsung/android/app/memo/util/ImageCache$ImageCacheParams;)V

    .line 528
    new-instance v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;

    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    iget-object v5, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mImageLoader:Lcom/samsung/android/app/memo/util/ImageLoader;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;-><init>(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;Landroid/content/Context;Landroid/database/Cursor;ILcom/samsung/android/app/memo/util/ImageLoader;)V

    iput-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mAdapter:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;

    .line 530
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mAbsListView:Landroid/widget/AbsListView;

    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mAdapter:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/AbsListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 532
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mMenuType:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeStyle;

    sget-object v1, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeStyle;->PICK_ONE:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeStyle;

    if-ne v0, v1, :cond_1

    .line 533
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getCallingPackage()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 534
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getCallingPackage()Ljava/lang/String;

    move-result-object v0

    .line 535
    const-string v1, "com.samsung.android.sconnect"

    .line 534
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 535
    if-eqz v0, :cond_0

    .line 536
    sget-object v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeStyle;->PICK_MULTIPLE:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeStyle;

    iput-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mMenuType:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeStyle;

    .line 537
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "limit"

    const/16 v2, 0x64

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mSelectLimit:I

    .line 539
    :cond_0
    invoke-direct {p0}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->handlePickOne()V

    .line 584
    :goto_0
    iget-boolean v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mIsCategoryChanged:Z

    if-eqz v0, :cond_2

    .line 585
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->getLoaderManager()Landroid/app/LoaderManager;

    move-result-object v0

    invoke-virtual {v0, v7, v3, p0}, Landroid/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroid/app/LoaderManager$LoaderCallbacks;)Landroid/content/Loader;

    .line 586
    iput-boolean v4, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mIsCategoryChanged:Z

    .line 591
    :goto_1
    return-void

    .line 541
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mAbsListView:Landroid/widget/AbsListView;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/widget/AbsListView;->setChoiceMode(I)V

    .line 542
    new-instance v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;

    invoke-direct {v0, p0, v3}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;-><init>(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;)V

    iput-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mActionModeCallback:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;

    .line 543
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mAbsListView:Landroid/widget/AbsListView;

    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mActionModeCallback:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;

    invoke-virtual {v0, v1}, Landroid/widget/AbsListView;->setMultiChoiceModeListener(Landroid/widget/AbsListView$MultiChoiceModeListener;)V

    .line 544
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mAbsListView:Landroid/widget/AbsListView;

    new-instance v1, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$2;

    invoke-direct {v1, p0}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$2;-><init>(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/AbsListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 566
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mAbsListView:Landroid/widget/AbsListView;

    new-instance v1, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$3;

    invoke-direct {v1, p0}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$3;-><init>(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/AbsListView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    goto :goto_0

    .line 589
    :cond_2
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->getLoaderManager()Landroid/app/LoaderManager;

    move-result-object v0

    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {v0, v7, v1, p0}, Landroid/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/app/LoaderManager$LoaderCallbacks;)Landroid/content/Loader;

    goto :goto_1
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 5
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 426
    invoke-super {p0, p1}, Landroid/app/Fragment;->onAttach(Landroid/app/Activity;)V

    .line 429
    iput-object p1, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mActivity:Landroid/app/Activity;

    .line 431
    :try_start_0
    move-object v0, p1

    check-cast v0, Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;

    move-object v2, v0

    iput-object v2, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mFragmentAction:Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    .line 437
    return-void

    .line 432
    :catch_0
    move-exception v1

    .line 433
    .local v1, "e":Ljava/lang/ClassCastException;
    sget-object v2, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->TAG:Ljava/lang/String;

    const-string v3, "onAttach"

    invoke-static {v2, v3, v1}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 434
    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 435
    const-string v4, " must implement MemoListActionListener"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 434
    invoke-direct {v2, v3, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2
.end method

.method public onClick(Landroid/view/View;)V
    .locals 6
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v4, 0x0

    .line 1320
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 1352
    sget-object v1, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "onClick() unknown: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1353
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const-string v2, "not implemeted yet"

    invoke-static {v1, v2, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    .line 1354
    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 1356
    :goto_0
    return-void

    .line 1322
    :pswitch_0
    sget-boolean v1, Lcom/samsung/android/app/memo/Main;->mIsUiPickerMode:Z

    if-nez v1, :cond_0

    .line 1323
    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mAbsListView:Landroid/widget/AbsListView;

    invoke-virtual {v1, v4}, Landroid/widget/AbsListView;->setChoiceMode(I)V

    .line 1325
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mFragmentAction:Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;

    invoke-interface {v1}, Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;->closeDrawer()V

    .line 1326
    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mActivity:Landroid/app/Activity;

    iget-object v2, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mActivity:Landroid/app/Activity;

    .line 1327
    invoke-virtual {v2}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const-string v3, "search_src_text"

    const-string v4, "id"

    .line 1328
    const-string v5, "android"

    .line 1327
    invoke-virtual {v2, v3, v4, v5}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v2

    .line 1326
    invoke-virtual {v1, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 1330
    .local v0, "searchTextView":Landroid/view/View;
    new-instance v1, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$10;

    invoke-direct {v1, p0}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$10;-><init>(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    goto :goto_0

    .line 1320
    nop

    :pswitch_data_0
    .packed-switch 0x7f0e0097
        :pswitch_0
    .end packed-switch
.end method

.method public onClose()Z
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 2618
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mSearchView:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MySearchView;

    invoke-virtual {v0}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MySearchView;->getQuery()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2619
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mSearchView:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MySearchView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MySearchView;->setQuery(Ljava/lang/CharSequence;Z)V

    .line 2621
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mFragmentAction:Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;->setDrawerLock(Z)V

    .line 2622
    return v2
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 14
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    const v13, 0x7f0e0013

    const v12, 0x7f0e0004

    const/4 v9, 0x0

    .line 1260
    invoke-super {p0, p1}, Landroid/app/Fragment;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 1261
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 1263
    .local v0, "activity":Landroid/app/Activity;
    invoke-static {}, Lcom/samsung/android/app/memo/util/Utils;->isTablet()Z

    move-result v7

    if-nez v7, :cond_1

    .line 1264
    new-instance v7, Landroid/os/Handler;

    invoke-direct {v7}, Landroid/os/Handler;-><init>()V

    new-instance v8, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$9;

    invoke-direct {v8, p0}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$9;-><init>(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;)V

    .line 1290
    const-wide/16 v10, 0x12c

    .line 1264
    invoke-virtual {v7, v8, v10, v11}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 1291
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v7

    const v8, 0x7f0e0028

    invoke-virtual {v7, v8}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/view/ViewStub;

    .line 1292
    .local v6, "vstub":Landroid/view/ViewStub;
    const/4 v1, 0x0

    .line 1293
    .local v1, "fm":Landroid/widget/LinearLayout;
    if-eqz v6, :cond_0

    .line 1294
    invoke-virtual {v6}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v1

    .end local v1    # "fm":Landroid/widget/LinearLayout;
    check-cast v1, Landroid/widget/LinearLayout;

    .line 1295
    .restart local v1    # "fm":Landroid/widget/LinearLayout;
    invoke-virtual {v1, v9}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1298
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v7

    invoke-virtual {v7, v12}, Landroid/app/FragmentManager;->findFragmentById(I)Landroid/app/Fragment;

    move-result-object v7

    if-eqz v7, :cond_1

    .line 1299
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v7

    .line 1300
    invoke-virtual {v7, v12}, Landroid/app/FragmentManager;->findFragmentById(I)Landroid/app/Fragment;

    move-result-object v3

    .line 1299
    check-cast v3, Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;

    .line 1301
    .local v3, "mCategoryListFragment":Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;
    invoke-virtual {v3}, Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;->getView()Landroid/view/View;

    move-result-object v7

    invoke-virtual {v7}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout$LayoutParams;

    .line 1302
    .local v2, "lp":Landroid/widget/LinearLayout$LayoutParams;
    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f09000d

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v7

    float-to-int v7, v7

    iput v7, v2, Landroid/widget/LinearLayout$LayoutParams;->width:I

    .line 1303
    invoke-virtual {v3}, Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;->getView()Landroid/view/View;

    move-result-object v7

    invoke-virtual {v7, v2}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1307
    .end local v1    # "fm":Landroid/widget/LinearLayout;
    .end local v2    # "lp":Landroid/widget/LinearLayout$LayoutParams;
    .end local v3    # "mCategoryListFragment":Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;
    .end local v6    # "vstub":Landroid/view/ViewStub;
    :cond_1
    sget-boolean v7, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mIsActionmode:Z

    if-eqz v7, :cond_2

    .line 1308
    iget-object v7, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mActionModeCallback:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;->mMultiSelectActionBarView:Landroid/view/View;
    invoke-static {v7}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;->access$2(Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;)Landroid/view/View;

    move-result-object v7

    invoke-virtual {v7, v13}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/LinearLayout;

    .line 1309
    .local v4, "selectionAll":Landroid/widget/LinearLayout;
    iget-object v7, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mActionModeCallback:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;->mMultiSelectActionBarView:Landroid/view/View;
    invoke-static {v7}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;->access$2(Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;)Landroid/view/View;

    move-result-object v7

    invoke-virtual {v7, v13}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    invoke-virtual {v7}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout$LayoutParams;

    .line 1310
    .restart local v2    # "lp":Landroid/widget/LinearLayout$LayoutParams;
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f09005f

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v7

    float-to-int v7, v7

    iput v7, v2, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 1311
    invoke-virtual {v4, v2}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1313
    iget-object v7, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mActionModeCallback:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;->mMultiSelectActionBarView:Landroid/view/View;
    invoke-static {v7}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;->access$2(Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;)Landroid/view/View;

    move-result-object v7

    const v8, 0x7f0e0015

    invoke-virtual {v7, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    .line 1314
    .local v5, "selectionTitle":Landroid/widget/TextView;
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f09006b

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v7

    invoke-virtual {v5, v9, v7}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1316
    .end local v2    # "lp":Landroid/widget/LinearLayout$LayoutParams;
    .end local v4    # "selectionAll":Landroid/widget/LinearLayout;
    .end local v5    # "selectionTitle":Landroid/widget/TextView;
    :cond_2
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v1, 0x1

    .line 441
    invoke-super {p0, p1}, Landroid/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 442
    invoke-virtual {p0, v1}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->setRetainInstance(Z)V

    .line 443
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mActivity:Landroid/app/Activity;

    invoke-static {v0, v1}, Lcom/samsung/android/app/migration/utils/SharedPref;->setMemoRelaunchedForSyncMemoDialogFlag(Landroid/content/Context;Z)V

    .line 444
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mActivity:Landroid/app/Activity;

    invoke-static {v0, v1}, Lcom/samsung/android/app/migration/utils/SharedPref;->setMemoRelaunchedForTMemoImportDialogFlag(Landroid/content/Context;Z)V

    .line 445
    return-void
.end method

.method public onCreateLoader(ILandroid/os/Bundle;)Landroid/content/Loader;
    .locals 15
    .param p1, "loaderId"    # I
    .param p2, "args"    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Landroid/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 596
    sget-object v2, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "onCreateLoader("

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move/from16 v0, p1

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ","

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p2

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const/16 v4, 0x29

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 597
    const/16 v2, 0x3e8

    move/from16 v0, p1

    if-eq v0, v2, :cond_0

    .line 598
    const/4 v1, 0x0

    .line 644
    :goto_0
    return-object v1

    .line 602
    :cond_0
    iget-object v2, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mStrCurFilter:Ljava/lang/String;

    if-eqz v2, :cond_7

    .line 603
    sget-object v2, Lcom/samsung/android/provider/memo/MemoContract;->BASE_URI_SEARCH:Landroid/net/Uri;

    iget-object v3, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mStrCurFilter:Ljava/lang/String;

    invoke-static {v3}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v8

    .line 608
    .local v8, "baseUri":Landroid/net/Uri;
    :goto_1
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, " AND categoryUUID IS \'"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mStrCategoryUUID:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\'"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    .line 610
    .local v10, "categorySelection":Ljava/lang/String;
    sget v2, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mCategoryType:I

    if-nez v2, :cond_5

    .line 611
    const/4 v12, 0x0

    .line 613
    .local v12, "cursor":Landroid/database/Cursor;
    :try_start_0
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 614
    .local v9, "categoryNonExistingList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 615
    sget-object v2, Lcom/samsung/android/provider/memo/MemoContract;->BASE_URI_MEMO:Landroid/net/Uri;

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "categoryUUID"

    aput-object v5, v3, v4

    const-string v4, "isDeleted = 0"

    const/4 v5, 0x0

    const/4 v6, 0x0

    .line 614
    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v12

    .line 616
    if-eqz v12, :cond_3

    invoke-interface {v12}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 618
    :cond_1
    const-string v2, "categoryUUID"

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    .line 619
    .local v11, "categoryUUID":Ljava/lang/String;
    invoke-virtual {v11}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_2

    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-static {v2, v11}, Lcom/samsung/android/app/memo/MemoDataHandler;->isCategoryExist(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 620
    invoke-interface {v9, v11}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 621
    :cond_2
    invoke-interface {v12}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-nez v2, :cond_1

    .line 623
    .end local v11    # "categoryUUID":Ljava/lang/String;
    :cond_3
    invoke-interface {v9}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_4

    .line 624
    const-string v10, " AND categoryUUID IN (\'\',"

    .line 625
    const/4 v14, 0x0

    .local v14, "i":I
    :goto_2
    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v2

    if-lt v14, v2, :cond_8

    .line 627
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v10}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v10

    .line 633
    .end local v14    # "i":I
    :cond_4
    if-eqz v12, :cond_5

    .line 634
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    .line 638
    .end local v9    # "categoryNonExistingList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v12    # "cursor":Landroid/database/Cursor;
    :cond_5
    new-instance v1, Landroid/content/CursorLoader;

    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    sget-object v4, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->PROJECTION:[Ljava/lang/String;

    .line 639
    sget v3, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mCategoryType:I

    const/4 v5, 0x1

    if-eq v3, v5, :cond_6

    iget-object v3, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mMenuType:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeStyle;

    sget-object v5, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeStyle;->PICK_MULTIPLE:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeStyle;

    if-ne v3, v5, :cond_c

    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    if-eqz v3, :cond_c

    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/Activity;->getCallingPackage()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_c

    .line 640
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/Activity;->getCallingPackage()Ljava/lang/String;

    move-result-object v3

    const-string v5, "com.samsung.android.sconnect"

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_c

    :cond_6
    iget-object v5, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mStrSelection:Ljava/lang/String;

    .line 641
    :goto_3
    const/4 v6, 0x0

    const-string v7, "lastModifiedAt DESC"

    move-object v3, v8

    .line 638
    invoke-direct/range {v1 .. v7}, Landroid/content/CursorLoader;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 644
    .local v1, "cursorLoader":Landroid/content/CursorLoader;
    goto/16 :goto_0

    .line 605
    .end local v1    # "cursorLoader":Landroid/content/CursorLoader;
    .end local v8    # "baseUri":Landroid/net/Uri;
    .end local v10    # "categorySelection":Ljava/lang/String;
    :cond_7
    sget-object v8, Lcom/samsung/android/provider/memo/MemoContract;->BASE_URI_MEMO:Landroid/net/Uri;

    .restart local v8    # "baseUri":Landroid/net/Uri;
    goto/16 :goto_1

    .line 626
    .restart local v9    # "categoryNonExistingList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .restart local v10    # "categorySelection":Ljava/lang/String;
    .restart local v12    # "cursor":Landroid/database/Cursor;
    .restart local v14    # "i":I
    :cond_8
    :try_start_1
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v10}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, "\'"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-interface {v9, v14}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    if-ne v14, v2, :cond_9

    const-string v2, "\'"

    :goto_4
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    .line 625
    add-int/lit8 v14, v14, 0x1

    goto/16 :goto_2

    .line 626
    :cond_9
    const-string v2, "\',"
    :try_end_1
    .catch Landroid/database/SQLException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_4

    .line 629
    .end local v9    # "categoryNonExistingList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v14    # "i":I
    :catch_0
    move-exception v13

    .line 630
    .local v13, "e":Landroid/database/SQLException;
    :try_start_2
    sget-object v2, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->TAG:Ljava/lang/String;

    const-string v3, "SQLException occurred in onCreateLoader(). "

    invoke-static {v2, v3, v13}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 633
    if-eqz v12, :cond_a

    .line 634
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    .line 631
    :cond_a
    const/4 v1, 0x0

    goto/16 :goto_0

    .line 632
    .end local v13    # "e":Landroid/database/SQLException;
    :catchall_0
    move-exception v2

    .line 633
    if-eqz v12, :cond_b

    .line 634
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    .line 635
    :cond_b
    throw v2

    .line 640
    .end local v12    # "cursor":Landroid/database/Cursor;
    :cond_c
    new-instance v3, Ljava/lang/StringBuilder;

    iget-object v5, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mStrSelection:Ljava/lang/String;

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    goto :goto_3
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 11
    .param p1, "menu"    # Landroid/view/Menu;
    .param p2, "inflater"    # Landroid/view/MenuInflater;

    .prologue
    const v10, 0x7f0b000d

    const/4 v9, 0x1

    const/4 v8, -0x2

    const/4 v7, 0x0

    const v6, 0x7f0e0004

    .line 872
    sget-object v4, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->TAG:Ljava/lang/String;

    const-string v5, "onCreateOptionsMenu()"

    invoke-static {v4, v5}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 873
    iput-object p1, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mMenu:Landroid/view/Menu;

    .line 874
    invoke-static {}, Lcom/samsung/android/app/memo/util/Utils;->isNoOutgoingCall()Z

    move-result v4

    if-nez v4, :cond_1

    .line 875
    invoke-interface {p1}, Landroid/view/Menu;->clear()V

    .line 876
    iget-object v4, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mFragmentAction:Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;

    sget-object v5, Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface$ActionBarStyle;->LIST_VIEW:Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface$ActionBarStyle;

    invoke-interface {v4, v5}, Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;->setActionBarStyle(Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface$ActionBarStyle;)V

    .line 877
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    const v5, 0x7f0e0028

    invoke-virtual {v4, v5}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/view/ViewStub;

    .line 878
    .local v3, "vstub":Landroid/view/ViewStub;
    const/4 v1, 0x0

    .line 879
    .local v1, "fm":Landroid/widget/LinearLayout;
    if-eqz v3, :cond_0

    .line 880
    invoke-virtual {v3}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v1

    .end local v1    # "fm":Landroid/widget/LinearLayout;
    check-cast v1, Landroid/widget/LinearLayout;

    .line 881
    .restart local v1    # "fm":Landroid/widget/LinearLayout;
    invoke-virtual {v1, v7}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 883
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v4

    invoke-virtual {v4, v6}, Landroid/app/FragmentManager;->findFragmentById(I)Landroid/app/Fragment;

    move-result-object v4

    if-eqz v4, :cond_1

    .line 884
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v4

    .line 885
    invoke-virtual {v4, v6}, Landroid/app/FragmentManager;->findFragmentById(I)Landroid/app/Fragment;

    move-result-object v2

    .line 884
    check-cast v2, Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;

    .line 886
    .local v2, "mCategoryListFragment":Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;
    invoke-virtual {v2}, Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;->focusHeader()V

    .line 887
    iget-object v4, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mAbsListView:Landroid/widget/AbsListView;

    invoke-virtual {v4, v7}, Landroid/widget/AbsListView;->setFocusable(Z)V

    .line 890
    .end local v1    # "fm":Landroid/widget/LinearLayout;
    .end local v2    # "mCategoryListFragment":Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;
    .end local v3    # "vstub":Landroid/view/ViewStub;
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    const v5, 0x7f0e0026

    invoke-virtual {v4, v5}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/widget/DrawerLayout;

    .line 891
    .local v0, "drawerLayout":Landroid/support/v4/widget/DrawerLayout;
    if-eqz v0, :cond_5

    .line 892
    const/4 v4, 0x3

    invoke-virtual {v0, v4}, Landroid/support/v4/widget/DrawerLayout;->isDrawerOpen(I)Z

    move-result v4

    if-nez v4, :cond_2

    iget-object v4, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mFragmentAction:Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;

    invoke-interface {v4}, Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;->isNeedDrawerOpen()Z

    move-result v4

    if-eqz v4, :cond_5

    .line 893
    :cond_2
    invoke-interface {p1}, Landroid/view/Menu;->clear()V

    .line 894
    iget-object v4, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mFragmentAction:Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;

    sget-object v5, Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface$ActionBarStyle;->LIST_VIEW:Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface$ActionBarStyle;

    invoke-interface {v4, v5}, Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;->setActionBarStyle(Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface$ActionBarStyle;)V

    .line 895
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    const v5, 0x7f0e0028

    invoke-virtual {v4, v5}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/view/ViewStub;

    .line 896
    .restart local v3    # "vstub":Landroid/view/ViewStub;
    const/4 v1, 0x0

    .line 897
    .restart local v1    # "fm":Landroid/widget/LinearLayout;
    if-eqz v3, :cond_3

    .line 898
    invoke-virtual {v3}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v1

    .end local v1    # "fm":Landroid/widget/LinearLayout;
    check-cast v1, Landroid/widget/LinearLayout;

    .line 899
    .restart local v1    # "fm":Landroid/widget/LinearLayout;
    invoke-virtual {v1, v7}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 901
    :cond_3
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v4

    invoke-virtual {v4, v6}, Landroid/app/FragmentManager;->findFragmentById(I)Landroid/app/Fragment;

    move-result-object v4

    if-eqz v4, :cond_4

    .line 902
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v4

    .line 903
    invoke-virtual {v4, v6}, Landroid/app/FragmentManager;->findFragmentById(I)Landroid/app/Fragment;

    move-result-object v2

    .line 902
    check-cast v2, Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;

    .line 904
    .restart local v2    # "mCategoryListFragment":Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;
    invoke-virtual {v2}, Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;->focusHeader()V

    .line 905
    iget-object v4, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mAbsListView:Landroid/widget/AbsListView;

    invoke-virtual {v4, v7}, Landroid/widget/AbsListView;->setFocusable(Z)V

    .line 907
    .end local v2    # "mCategoryListFragment":Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;
    :cond_4
    const/high16 v4, 0x7f0b0000

    invoke-virtual {p0, v4}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v4}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->SetCategoryTitle(Ljava/lang/String;)V

    .line 957
    .end local v1    # "fm":Landroid/widget/LinearLayout;
    .end local v3    # "vstub":Landroid/view/ViewStub;
    :goto_0
    return-void

    .line 909
    :cond_5
    invoke-interface {p1}, Landroid/view/Menu;->clear()V

    .line 911
    invoke-static {}, Lcom/samsung/android/app/memo/util/Utils;->isFonbletHD()Z

    move-result v4

    if-eqz v4, :cond_8

    .line 912
    const v4, 0x7f0d0005

    invoke-virtual {p2, v4, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 916
    :goto_1
    const v4, 0x7f0e0098

    invoke-interface {p1, v4}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v4

    iput-object v4, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mMenuSelect:Landroid/view/MenuItem;

    .line 917
    const v4, 0x7f0e0092

    invoke-interface {p1, v4}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v4

    iput-object v4, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mMenuDelete:Landroid/view/MenuItem;

    .line 920
    iget-object v4, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mFragmentAction:Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;

    sget-object v5, Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface$ActionBarStyle;->LIST_VIEW:Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface$ActionBarStyle;

    invoke-interface {v4, v5}, Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;->setActionBarStyle(Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface$ActionBarStyle;)V

    .line 921
    sget-boolean v4, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mIsActionmode:Z

    if-eqz v4, :cond_6

    .line 922
    iget-object v4, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mFragmentAction:Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;

    invoke-interface {v4, v9}, Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;->setDrawerLock(Z)V

    .line 924
    :cond_6
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v4

    invoke-virtual {v4, v6}, Landroid/app/FragmentManager;->findFragmentById(I)Landroid/app/Fragment;

    move-result-object v4

    if-eqz v4, :cond_7

    .line 925
    iget-object v4, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mAbsListView:Landroid/widget/AbsListView;

    invoke-virtual {v4, v9}, Landroid/widget/AbsListView;->setFocusable(Z)V

    .line 929
    :cond_7
    new-instance v4, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MySearchView;

    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v5

    invoke-direct {v4, p0, v5}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MySearchView;-><init>(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;Landroid/content/Context;)V

    iput-object v4, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mSearchView:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MySearchView;

    .line 931
    iget-object v4, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mSearchView:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MySearchView;

    invoke-virtual {v4, p0}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MySearchView;->setOnQueryTextListener(Landroid/widget/SearchView$OnQueryTextListener;)V

    .line 932
    iget-object v4, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mSearchView:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MySearchView;

    invoke-virtual {v4, p0}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MySearchView;->setOnCloseListener(Landroid/widget/SearchView$OnCloseListener;)V

    .line 933
    iget-object v4, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mSearchView:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MySearchView;

    invoke-virtual {v4, v9}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MySearchView;->setIconifiedByDefault(Z)V

    .line 934
    iget-object v4, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mSearchView:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MySearchView;

    invoke-virtual {v4, p0}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MySearchView;->setOnSearchClickListener(Landroid/view/View$OnClickListener;)V

    .line 935
    iget-object v4, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mSearchView:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MySearchView;

    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0b009d

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MySearchView;->setQueryHint(Ljava/lang/CharSequence;)V

    .line 936
    const v4, 0x7f0e0097

    invoke-interface {p1, v4}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v4

    iget-object v5, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mSearchView:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MySearchView;

    invoke-interface {v4, v5}, Landroid/view/MenuItem;->setActionView(Landroid/view/View;)Landroid/view/MenuItem;

    move-result-object v4

    iput-object v4, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mMenuSearch:Landroid/view/MenuItem;

    .line 938
    invoke-static {}, Lcom/samsung/android/app/memo/util/Utils;->isTablet()Z

    move-result v4

    if-nez v4, :cond_b

    .line 939
    iget-object v4, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mSearchView:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MySearchView;

    new-instance v5, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v6, -0x1

    invoke-direct {v5, v6, v8}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v4, v5}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MySearchView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 940
    iget-object v4, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mStrCategoryUUID:Ljava/lang/String;

    const-string v5, ""

    if-ne v4, v5, :cond_a

    .line 941
    sget v4, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mCategoryType:I

    if-nez v4, :cond_9

    invoke-virtual {p0, v10}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->getString(I)Ljava/lang/String;

    move-result-object v4

    :goto_2
    invoke-direct {p0, v4}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->SetCategoryTitleWithNum(Ljava/lang/String;)V

    .line 948
    :goto_3
    iget-object v4, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mStrCategoryUUID:Ljava/lang/String;

    const-string v5, ""

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_d

    .line 949
    sget v4, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mCategoryType:I

    if-nez v4, :cond_c

    invoke-virtual {p0, v10}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->getString(I)Ljava/lang/String;

    move-result-object v4

    :goto_4
    invoke-direct {p0, v4}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->SetCategoryTitleWithNum(Ljava/lang/String;)V

    .line 955
    :goto_5
    const v4, 0x7f0e009b

    invoke-interface {p1, v4}, Landroid/view/Menu;->removeItem(I)V

    goto/16 :goto_0

    .line 914
    :cond_8
    const v4, 0x7f0d0003

    invoke-virtual {p2, v4, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    goto/16 :goto_1

    .line 942
    :cond_9
    const v4, 0x7f0b0017

    invoke-virtual {p0, v4}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->getString(I)Ljava/lang/String;

    move-result-object v4

    goto :goto_2

    .line 944
    :cond_a
    iget-object v4, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mStrCategoryTitle:Ljava/lang/String;

    invoke-direct {p0, v4}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->SetCategoryTitleWithNum(Ljava/lang/String;)V

    goto :goto_3

    .line 946
    :cond_b
    iget-object v4, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mSearchView:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MySearchView;

    new-instance v5, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v5, v8, v8}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v4, v5}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MySearchView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_3

    .line 950
    :cond_c
    const v4, 0x7f0b0017

    invoke-virtual {p0, v4}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->getString(I)Ljava/lang/String;

    move-result-object v4

    goto :goto_4

    .line 952
    :cond_d
    invoke-direct {p0}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->loadPreferences()V

    .line 953
    iget-object v4, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mStrCategoryTitle:Ljava/lang/String;

    invoke-direct {p0, v4}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->SetCategoryTitleWithNum(Ljava/lang/String;)V

    goto :goto_5
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 449
    invoke-direct {p0}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->loadPreferences()V

    .line 450
    const v1, 0x7f04001c

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 451
    .local v0, "v":Landroid/view/View;
    invoke-direct {p0, v0}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->initView(Landroid/view/View;)V

    .line 457
    return-object v0
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1219
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->hideUndoBar()V

    .line 1220
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mImageLoader:Lcom/samsung/android/app/memo/util/ImageLoader;

    if-eqz v0, :cond_0

    .line 1221
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mImageLoader:Lcom/samsung/android/app/memo/util/ImageLoader;

    invoke-virtual {v0}, Lcom/samsung/android/app/memo/util/ImageLoader;->clearCache()V

    .line 1222
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mImageLoader:Lcom/samsung/android/app/memo/util/ImageLoader;

    invoke-virtual {v0}, Lcom/samsung/android/app/memo/util/ImageLoader;->closeCache()V

    .line 1223
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mImageLoader:Lcom/samsung/android/app/memo/util/ImageLoader;

    invoke-virtual {v0}, Lcom/samsung/android/app/memo/util/ImageLoader;->unRegisteredReciever()V

    .line 1224
    iput-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mImageLoader:Lcom/samsung/android/app/memo/util/ImageLoader;

    .line 1226
    :cond_0
    iput-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mSearchView:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MySearchView;

    .line 1228
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mIDsForCacheDelete:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    .line 1229
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mIDsForCacheDelete:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 1230
    iput-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mIDsForCacheDelete:Ljava/util/ArrayList;

    .line 1233
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mAllThumbHash:Landroid/util/SparseArray;

    if-eqz v0, :cond_2

    .line 1234
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mAllThumbHash:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->clear()V

    .line 1235
    iput-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mAllThumbHash:Landroid/util/SparseArray;

    .line 1238
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mListener:Lcom/samsung/android/quickconnect/QuickConnectManager$QuickConnectListener;

    if-eqz v0, :cond_3

    iput-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mListener:Lcom/samsung/android/quickconnect/QuickConnectManager$QuickConnectListener;

    .line 1239
    :cond_3
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mBezelManager:Lcom/samsung/android/quickconnect/QuickConnectManager;

    if-eqz v0, :cond_4

    iput-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mBezelManager:Lcom/samsung/android/quickconnect/QuickConnectManager;

    .line 1241
    :cond_4
    invoke-super {p0}, Landroid/app/Fragment;->onDestroy()V

    .line 1242
    return-void
.end method

.method public onDetach()V
    .locals 2

    .prologue
    .line 1246
    sget-boolean v0, Lcom/samsung/android/app/memo/util/Utils;->HAVE_SLOOKBEZELINTERACTION_API:Z

    if-eqz v0, :cond_1

    .line 1248
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mBezelManager:Lcom/samsung/android/quickconnect/QuickConnectManager;

    if-eqz v0, :cond_0

    .line 1249
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mBezelManager:Lcom/samsung/android/quickconnect/QuickConnectManager;

    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mListener:Lcom/samsung/android/quickconnect/QuickConnectManager$QuickConnectListener;

    invoke-virtual {v0, v1}, Lcom/samsung/android/quickconnect/QuickConnectManager;->unregisterListener(Lcom/samsung/android/quickconnect/QuickConnectManager$QuickConnectListener;)V

    .line 1250
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mBezelManager:Lcom/samsung/android/quickconnect/QuickConnectManager;

    .line 1252
    :cond_1
    invoke-super {p0}, Landroid/app/Fragment;->onDetach()V

    .line 1253
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mActionMode:Landroid/view/ActionMode;

    if-eqz v0, :cond_2

    .line 1254
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mActionMode:Landroid/view/ActionMode;

    invoke-virtual {v0}, Landroid/view/ActionMode;->finish()V

    .line 1256
    :cond_2
    return-void
.end method

.method public onHideUndoBar()V
    .locals 2

    .prologue
    .line 2016
    sget-object v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->TAG:Ljava/lang/String;

    const-string v1, "onHideUndobar"

    invoke-static {v0, v1}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2017
    invoke-direct {p0}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->deleteMemos()V

    .line 2018
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mIDsBeingDeleted:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 2019
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->sendUpdateAllWidgetBroadcastEvent()V

    .line 2020
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mAdapter:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;

    if-eqz v0, :cond_0

    .line 2021
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mAdapter:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;

    invoke-virtual {v0}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;->removeDeletedMemoCache()V

    .line 2022
    :cond_0
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 1360
    const/4 v0, 0x0

    return v0
.end method

.method public onKeyLongPress(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 1413
    const/4 v0, 0x0

    return v0
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 6
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 1365
    sparse-switch p1, :sswitch_data_0

    :cond_0
    move v1, v2

    .line 1408
    :goto_0
    return v1

    .line 1367
    :sswitch_0
    invoke-virtual {p2}, Landroid/view/KeyEvent;->isCtrlPressed()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1368
    invoke-static {}, Lcom/samsung/android/app/memo/util/Utils;->checkLowMemory()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1369
    iget-object v2, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mActivity:Landroid/app/Activity;

    const v3, 0x7f0b00b9

    invoke-static {v2, v3}, Lcom/samsung/android/app/memo/util/Utils;->showToast(Landroid/content/Context;I)V

    goto :goto_0

    .line 1372
    :cond_1
    iput-boolean v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mIsSkipFinishForSConnect:Z

    .line 1373
    iget-object v2, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mFragmentAction:Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;

    const-wide/16 v4, -0x1

    iget-object v3, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mStrCategoryUUID:Ljava/lang/String;

    invoke-interface {v2, v4, v5, v3, v1}, Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;->openContent(JLjava/lang/String;Z)V

    goto :goto_0

    .line 1378
    :sswitch_1
    invoke-virtual {p2}, Landroid/view/KeyEvent;->isCtrlPressed()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1379
    iget-object v2, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mMenuSearch:Landroid/view/MenuItem;

    invoke-interface {v2}, Landroid/view/MenuItem;->expandActionView()Z

    .line 1380
    iget-object v2, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mSearchView:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MySearchView;

    invoke-virtual {v2}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MySearchView;->requestFocus()Z

    goto :goto_0

    .line 1385
    :sswitch_2
    invoke-virtual {p2}, Landroid/view/KeyEvent;->isCtrlPressed()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1386
    iget-object v2, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mSearchView:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MySearchView;

    invoke-virtual {v2}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MySearchView;->performLongClick()Z

    goto :goto_0

    .line 1392
    :sswitch_3
    iget-object v3, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mMenuSearch:Landroid/view/MenuItem;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mMenuSearch:Landroid/view/MenuItem;

    invoke-interface {v3}, Landroid/view/MenuItem;->isActionViewExpanded()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1393
    iget-object v3, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mSearchView:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MySearchView;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mSearchView:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MySearchView;

    invoke-virtual {v3}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MySearchView;->isIconified()Z

    move-result v3

    if-nez v3, :cond_0

    .line 1394
    iget-object v3, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mSearchView:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MySearchView;

    invoke-virtual {v3}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MySearchView;->getQuery()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-interface {v3}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1395
    .local v0, "queryString":Ljava/lang/String;
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-lez v3, :cond_2

    .line 1396
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-virtual {v0, v2, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1400
    :goto_1
    iget-object v3, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mSearchView:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MySearchView;

    invoke-virtual {v3, v0, v2}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MySearchView;->setQuery(Ljava/lang/CharSequence;Z)V

    .line 1401
    iget-object v2, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mSearchView:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MySearchView;

    invoke-virtual {v2}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MySearchView;->requestFocus()Z

    goto/16 :goto_0

    .line 1398
    :cond_2
    const-string v0, " "

    goto :goto_1

    .line 1365
    nop

    :sswitch_data_0
    .sparse-switch
        0x1d -> :sswitch_2
        0x22 -> :sswitch_1
        0x2a -> :sswitch_0
        0xa0 -> :sswitch_3
    .end sparse-switch
.end method

.method public onLoadFinished(Landroid/content/Loader;Landroid/database/Cursor;)V
    .locals 16
    .param p2, "data"    # Landroid/database/Cursor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    .prologue
    .line 675
    .local p1, "loader":Landroid/content/Loader;, "Landroid/content/Loader<Landroid/database/Cursor;>;"
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mAdapter:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;

    if-nez v10, :cond_1

    .line 676
    sget-object v10, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->TAG:Ljava/lang/String;

    const-string v11, "onLoadFinished, but mAdapter is null"

    invoke-static {v10, v11}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 788
    :cond_0
    :goto_0
    return-void

    .line 679
    :cond_1
    sget-object v10, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->TAG:Ljava/lang/String;

    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "onLoadFinished "

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface/range {p2 .. p2}, Landroid/database/Cursor;->getColumnCount()I

    move-result v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 680
    const/4 v8, 0x0

    .line 682
    .local v8, "memoCountChange":Z
    invoke-interface/range {p2 .. p2}, Landroid/database/Cursor;->getCount()I

    move-result v10

    if-nez v10, :cond_a

    const/4 v10, 0x1

    :goto_1
    move-object/from16 v0, p0

    invoke-direct {v0, v10}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->showNoMemo(Z)V

    .line 684
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    .line 685
    .local v2, "activity":Landroid/app/Activity;
    if-eqz v2, :cond_2

    .line 686
    const v10, 0x7f0e0026

    invoke-virtual {v2, v10}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/support/v4/widget/DrawerLayout;

    .line 687
    .local v3, "drawerLayout":Landroid/support/v4/widget/DrawerLayout;
    if-eqz v3, :cond_2

    const/4 v10, 0x3

    invoke-virtual {v3, v10}, Landroid/support/v4/widget/DrawerLayout;->isDrawerOpen(I)Z

    move-result v10

    if-nez v10, :cond_2

    .line 688
    invoke-interface/range {p2 .. p2}, Landroid/database/Cursor;->getCount()I

    move-result v10

    if-nez v10, :cond_b

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mUndoBarController:Lcom/samsung/android/app/memo/uiwidget/UndoBarController;

    if-eqz v10, :cond_b

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mUndoBarController:Lcom/samsung/android/app/memo/uiwidget/UndoBarController;

    invoke-virtual {v10}, Lcom/samsung/android/app/memo/uiwidget/UndoBarController;->isVisible()Z

    move-result v10

    if-eqz v10, :cond_b

    .line 689
    const v10, 0x7f0e003b

    invoke-virtual {v2, v10}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v10

    invoke-virtual {v10}, Landroid/view/View;->requestFocus()Z

    .line 690
    const v10, 0x7f0e003b

    invoke-virtual {v3, v10}, Landroid/support/v4/widget/DrawerLayout;->setNextFocusDownId(I)V

    .line 694
    :goto_2
    const/4 v10, 0x0

    invoke-virtual {v3, v10}, Landroid/support/v4/widget/DrawerLayout;->setFocusable(Z)V

    .line 698
    .end local v3    # "drawerLayout":Landroid/support/v4/widget/DrawerLayout;
    :cond_2
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v10

    if-eqz v10, :cond_4

    .line 699
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v10

    .line 700
    sget-object v11, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->PREF_CATEGORY:Ljava/lang/String;

    const/4 v12, 0x0

    .line 699
    invoke-virtual {v10, v11, v12}, Landroid/app/Activity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v9

    .line 701
    .local v9, "pref":Landroid/content/SharedPreferences;
    sget-object v10, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->KEY_CATEGORY_UUID:Ljava/lang/String;

    const-string v11, ""

    invoke-interface {v9, v10, v11}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 702
    .local v7, "mTempCategoryUUID":Ljava/lang/String;
    const-string v5, ""

    .line 703
    .local v5, "mTempCategoryTitle":Ljava/lang/String;
    sget-object v10, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->KEY_CATEGORY_TYPE:Ljava/lang/String;

    const/4 v11, 0x0

    invoke-interface {v9, v10, v11}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v6

    .line 704
    .local v6, "mTempCategoryType":I
    sget-object v10, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->KEY_CATEGORY_TITLE:Ljava/lang/String;

    const-string v11, ""

    invoke-interface {v9, v10, v11}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v10

    if-nez v10, :cond_3

    .line 705
    sget-object v10, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->KEY_CATEGORY_TITLE:Ljava/lang/String;

    const-string v11, ""

    invoke-interface {v9, v10, v11}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 707
    :cond_3
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mStrCategoryUUID:Ljava/lang/String;

    invoke-virtual {v7, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_4

    .line 708
    move-object/from16 v0, p0

    invoke-virtual {v0, v7, v5, v6}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->reloadedCursor(Ljava/lang/String;Ljava/lang/String;I)V

    .line 713
    .end local v5    # "mTempCategoryTitle":Ljava/lang/String;
    .end local v6    # "mTempCategoryType":I
    .end local v7    # "mTempCategoryUUID":Ljava/lang/String;
    .end local v9    # "pref":Landroid/content/SharedPreferences;
    :cond_4
    move-object/from16 v0, p0

    iget-boolean v10, v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mIsCheckBoxMode:Z

    if-eqz v10, :cond_5

    .line 714
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mAdapter:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;

    invoke-virtual {v10}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;->getCount()I

    move-result v10

    invoke-interface/range {p2 .. p2}, Landroid/database/Cursor;->getCount()I

    move-result v11

    if-eq v10, v11, :cond_c

    const/4 v8, 0x1

    .line 715
    :cond_5
    :goto_3
    move-object/from16 v0, p0

    iget v10, v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mGirdItemCount:I

    const/4 v11, -0x1

    if-eq v10, v11, :cond_6

    move-object/from16 v0, p0

    iget v10, v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mGirdItemCount:I

    invoke-interface/range {p2 .. p2}, Landroid/database/Cursor;->getCount()I

    move-result v11

    if-ge v10, v11, :cond_6

    .line 716
    const/4 v10, -0x1

    move-object/from16 v0, p0

    iput v10, v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mGirdItemCount:I

    .line 717
    new-instance v4, Landroid/os/Handler;

    invoke-direct {v4}, Landroid/os/Handler;-><init>()V

    .line 718
    .local v4, "handler":Landroid/os/Handler;
    new-instance v10, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$4;

    move-object/from16 v0, p0

    invoke-direct {v10, v0}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$4;-><init>(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;)V

    .line 723
    const-wide/16 v12, 0x0

    .line 718
    invoke-virtual {v4, v10, v12, v13}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 728
    .end local v4    # "handler":Landroid/os/Handler;
    :cond_6
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mAdapter:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;

    move-object/from16 v0, p2

    invoke-virtual {v10, v0}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    .line 729
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mAdapter:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;

    invoke-virtual {v10}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;->notifyDataSetChanged()V

    .line 731
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v10

    const v11, 0x7f0e0026

    invoke-virtual {v10, v11}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/support/v4/widget/DrawerLayout;

    .line 733
    .restart local v3    # "drawerLayout":Landroid/support/v4/widget/DrawerLayout;
    if-eqz v3, :cond_d

    const/4 v10, 0x3

    invoke-virtual {v3, v10}, Landroid/support/v4/widget/DrawerLayout;->isDrawerOpen(I)Z

    move-result v10

    if-nez v10, :cond_7

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mFragmentAction:Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;

    invoke-interface {v10}, Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;->isNeedDrawerOpen()Z

    move-result v10

    if-eqz v10, :cond_d

    .line 734
    :cond_7
    const/high16 v10, 0x7f0b0000

    move-object/from16 v0, p0

    invoke-virtual {v0, v10}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->getString(I)Ljava/lang/String;

    move-result-object v10

    move-object/from16 v0, p0

    invoke-direct {v0, v10}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->SetCategoryTitle(Ljava/lang/String;)V

    .line 758
    :goto_4
    move-object/from16 v0, p0

    iget-boolean v10, v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mIsCheckBoxMode:Z

    if-eqz v10, :cond_8

    if-eqz v8, :cond_8

    .line 759
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->finishActionMode()V

    .line 762
    :cond_8
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Landroid/app/Activity;->isFinishing()Z

    move-result v10

    if-nez v10, :cond_0

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mAbsListView:Landroid/widget/AbsListView;

    if-eqz v10, :cond_0

    .line 763
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mAbsListView:Landroid/widget/AbsListView;

    invoke-virtual {v10}, Landroid/widget/AbsListView;->getCount()I

    move-result v10

    if-lez v10, :cond_14

    .line 764
    const v10, 0x7f0e0038

    invoke-virtual {v2, v10}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v10

    const v11, 0x7f0e0037

    invoke-virtual {v10, v11}, Landroid/view/View;->setNextFocusUpId(I)V

    .line 765
    const v10, 0x7f0e0038

    invoke-virtual {v2, v10}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v10

    invoke-virtual {v10}, Landroid/view/View;->requestFocus()Z

    .line 766
    invoke-static {}, Lcom/samsung/android/app/memo/util/Utils;->isTablet()Z

    move-result v10

    if-eqz v10, :cond_13

    .line 767
    const v10, 0x7f0e003c

    invoke-virtual {v2, v10}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v10

    const v11, 0x7f0e0038

    invoke-virtual {v10, v11}, Landroid/view/View;->setNextFocusRightId(I)V

    .line 774
    :cond_9
    :goto_5
    new-instance v11, Landroid/os/Handler;

    invoke-direct {v11}, Landroid/os/Handler;-><init>()V

    new-instance v12, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$5;

    move-object/from16 v0, p0

    invoke-direct {v12, v0}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$5;-><init>(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;)V

    .line 780
    move-object/from16 v0, p0

    iget-boolean v10, v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mIsSearchRequested:Z

    if-eqz v10, :cond_15

    const/4 v10, 0x0

    :goto_6
    int-to-long v14, v10

    .line 774
    invoke-virtual {v11, v12, v14, v15}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 782
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mUndoBarController:Lcom/samsung/android/app/memo/uiwidget/UndoBarController;

    if-eqz v10, :cond_16

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mUndoBarController:Lcom/samsung/android/app/memo/uiwidget/UndoBarController;

    invoke-virtual {v10}, Lcom/samsung/android/app/memo/uiwidget/UndoBarController;->isVisible()Z

    move-result v10

    if-eqz v10, :cond_16

    .line 783
    const v10, 0x7f0e0038

    invoke-virtual {v2, v10}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v10

    const v11, 0x7f0e003b

    invoke-virtual {v10, v11}, Landroid/view/View;->setNextFocusDownId(I)V

    goto/16 :goto_0

    .line 682
    .end local v2    # "activity":Landroid/app/Activity;
    .end local v3    # "drawerLayout":Landroid/support/v4/widget/DrawerLayout;
    :cond_a
    const/4 v10, 0x0

    goto/16 :goto_1

    .line 692
    .restart local v2    # "activity":Landroid/app/Activity;
    .restart local v3    # "drawerLayout":Landroid/support/v4/widget/DrawerLayout;
    :cond_b
    const v10, 0x7f0e0037

    invoke-virtual {v3, v10}, Landroid/support/v4/widget/DrawerLayout;->setNextFocusDownId(I)V

    goto/16 :goto_2

    .line 714
    .end local v3    # "drawerLayout":Landroid/support/v4/widget/DrawerLayout;
    :cond_c
    const/4 v8, 0x0

    goto/16 :goto_3

    .line 735
    .restart local v3    # "drawerLayout":Landroid/support/v4/widget/DrawerLayout;
    :cond_d
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mStrCategoryUUID:Ljava/lang/String;

    const-string v11, ""

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_10

    .line 736
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v10

    invoke-static {v10}, Lcom/samsung/android/app/memo/util/Utils;->getCategoryCount(Landroid/content/Context;)I

    move-result v10

    if-nez v10, :cond_e

    .line 737
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v10

    invoke-static {v10}, Lcom/samsung/android/app/memo/util/AccountHelper;->getRegisteredSamsungAccount(Landroid/content/Context;)Landroid/accounts/Account;

    move-result-object v10

    if-nez v10, :cond_e

    .line 738
    const v10, 0x7f0b000d

    move-object/from16 v0, p0

    invoke-virtual {v0, v10}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->getString(I)Ljava/lang/String;

    move-result-object v10

    move-object/from16 v0, p0

    iput-object v10, v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mStrCategoryTitle:Ljava/lang/String;

    .line 739
    const/4 v10, 0x0

    sput v10, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mCategoryType:I

    .line 741
    :cond_e
    sget v10, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mCategoryType:I

    if-nez v10, :cond_f

    const v10, 0x7f0b000d

    move-object/from16 v0, p0

    invoke-virtual {v0, v10}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->getString(I)Ljava/lang/String;

    move-result-object v10

    :goto_7
    move-object/from16 v0, p0

    invoke-direct {v0, v10}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->SetCategoryTitleWithNum(Ljava/lang/String;)V

    goto/16 :goto_4

    .line 742
    :cond_f
    const v10, 0x7f0b0017

    move-object/from16 v0, p0

    invoke-virtual {v0, v10}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->getString(I)Ljava/lang/String;

    move-result-object v10

    goto :goto_7

    .line 744
    :cond_10
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v10

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mStrCategoryUUID:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mStrCategoryTitle:Ljava/lang/String;

    invoke-static {v10, v11, v12}, Lcom/samsung/android/app/memo/util/Utils;->isCategoryExist(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v10

    if-nez v10, :cond_11

    .line 745
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v10

    invoke-static {v10}, Lcom/samsung/android/app/memo/util/AccountHelper;->getRegisteredSamsungAccount(Landroid/content/Context;)Landroid/accounts/Account;

    move-result-object v10

    if-nez v10, :cond_11

    .line 746
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v10

    invoke-static {v10}, Lcom/samsung/android/app/memo/util/Utils;->getCategoryCount(Landroid/content/Context;)I

    move-result v10

    if-nez v10, :cond_12

    .line 747
    const v10, 0x7f0b000d

    move-object/from16 v0, p0

    invoke-virtual {v0, v10}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->getString(I)Ljava/lang/String;

    move-result-object v10

    move-object/from16 v0, p0

    iput-object v10, v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mStrCategoryTitle:Ljava/lang/String;

    .line 748
    const/4 v10, 0x0

    sput v10, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mCategoryType:I

    .line 753
    :goto_8
    const-string v10, ""

    move-object/from16 v0, p0

    iput-object v10, v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mStrCategoryUUID:Ljava/lang/String;

    .line 755
    :cond_11
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mStrCategoryTitle:Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-direct {v0, v10}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->SetCategoryTitleWithNum(Ljava/lang/String;)V

    goto/16 :goto_4

    .line 750
    :cond_12
    const v10, 0x7f0b0017

    move-object/from16 v0, p0

    invoke-virtual {v0, v10}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->getString(I)Ljava/lang/String;

    move-result-object v10

    move-object/from16 v0, p0

    iput-object v10, v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mStrCategoryTitle:Ljava/lang/String;

    .line 751
    const/4 v10, 0x1

    sput v10, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mCategoryType:I

    goto :goto_8

    .line 769
    :cond_13
    const v10, 0x7f0e0037

    invoke-virtual {v2, v10}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v10

    const v11, 0x7f0e0038

    invoke-virtual {v10, v11}, Landroid/view/View;->setNextFocusRightId(I)V

    goto/16 :goto_5

    .line 770
    :cond_14
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mAbsListView:Landroid/widget/AbsListView;

    invoke-virtual {v10}, Landroid/widget/AbsListView;->getCount()I

    move-result v10

    if-nez v10, :cond_9

    .line 771
    const v10, 0x7f0e0038

    invoke-virtual {v2, v10}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v10

    invoke-virtual {v10}, Landroid/view/View;->requestFocus()Z

    goto/16 :goto_5

    .line 780
    :cond_15
    const/16 v10, 0x1f4

    goto/16 :goto_6

    .line 785
    :cond_16
    const v10, 0x7f0e0038

    invoke-virtual {v2, v10}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v10

    const/4 v11, 0x0

    invoke-virtual {v10, v11}, Landroid/view/View;->setNextFocusDownId(I)V

    goto/16 :goto_0
.end method

.method public bridge synthetic onLoadFinished(Landroid/content/Loader;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1
    check-cast p1, Landroid/content/Loader;

    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p1, p2}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->onLoadFinished(Landroid/content/Loader;Landroid/database/Cursor;)V

    return-void
.end method

.method public onLoaderReset(Landroid/content/Loader;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 858
    .local p1, "loader":Landroid/content/Loader;, "Landroid/content/Loader<Landroid/database/Cursor;>;"
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mAdapter:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;

    if-nez v0, :cond_0

    .line 859
    sget-object v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onLoaderReset, but mAdapter is null, loader="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 867
    :goto_0
    return-void

    .line 862
    :cond_0
    sget-object v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->TAG:Ljava/lang/String;

    const-string v1, "onLoaderReset"

    invoke-static {v0, v1}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 866
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mAdapter:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    goto :goto_0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 12
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 1048
    iget-object v5, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mMenu:Landroid/view/Menu;

    if-eqz v5, :cond_0

    .line 1049
    iget-object v5, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mMenu:Landroid/view/Menu;

    invoke-interface {v5}, Landroid/view/Menu;->close()V

    .line 1051
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->hideUndoBar()V

    .line 1052
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v4

    .line 1053
    .local v4, "mitem":I
    packed-switch v4, :pswitch_data_0

    .line 1125
    :goto_0
    :pswitch_0
    invoke-super {p0, p1}, Landroid/app/Fragment;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v5

    .line 1130
    :goto_1
    return v5

    .line 1055
    :pswitch_1
    invoke-static {}, Lcom/samsung/android/app/memo/util/Utils;->checkLowMemory()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 1056
    iget-object v5, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mActivity:Landroid/app/Activity;

    const v6, 0x7f0b00b9

    invoke-static {v5, v6}, Lcom/samsung/android/app/memo/util/Utils;->showToast(Landroid/content/Context;I)V

    .line 1057
    const/4 v5, 0x0

    goto :goto_1

    .line 1059
    :cond_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 1060
    .local v2, "currentTouchTime":J
    iget-wide v8, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mLastMemoOpenClickTime:J

    sub-long v8, v2, v8

    const-wide/16 v10, 0x1f4

    cmp-long v5, v8, v10

    if-lez v5, :cond_2

    .line 1061
    iput-boolean v6, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mIsSkipFinishForSConnect:Z

    .line 1062
    invoke-static {v6}, Lcom/samsung/android/app/memo/MemoApp;->setIsCreateMemo(Z)V

    .line 1063
    iget-object v5, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mFragmentAction:Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;

    const-wide/16 v8, -0x1

    iget-object v7, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mStrCategoryUUID:Ljava/lang/String;

    invoke-interface {v5, v8, v9, v7, v6}, Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;->openContent(JLjava/lang/String;Z)V

    .line 1065
    :cond_2
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    iput-wide v8, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mLastMemoOpenClickTime:J

    .line 1066
    iget-object v5, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mAdapter:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;

    invoke-virtual {v5}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;->getCount()I

    move-result v5

    iput v5, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mGirdItemCount:I

    .line 1127
    .end local v2    # "currentTouchTime":J
    :cond_3
    :goto_2
    iget-object v5, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mMenuSearch:Landroid/view/MenuItem;

    if-eqz v5, :cond_4

    iget-object v5, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mMenuSearch:Landroid/view/MenuItem;

    invoke-interface {v5}, Landroid/view/MenuItem;->isActionViewExpanded()Z

    move-result v5

    if-eqz v5, :cond_4

    .line 1128
    iget-object v5, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mMenuSearch:Landroid/view/MenuItem;

    invoke-interface {v5}, Landroid/view/MenuItem;->collapseActionView()Z

    :cond_4
    move v5, v6

    .line 1130
    goto :goto_1

    .line 1069
    :pswitch_2
    iget-object v5, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mStrCategoryTitle:Ljava/lang/String;

    iget-object v7, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mStrCategoryUUID:Ljava/lang/String;

    invoke-static {v5, v7}, Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;->newInstance(Ljava/lang/String;Ljava/lang/String;)Landroid/app/DialogFragment;

    move-result-object v5

    check-cast v5, Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;

    iput-object v5, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mCategoryNamingDialog:Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;

    .line 1070
    iget-object v5, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mCategoryNamingDialog:Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;

    new-instance v7, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$7;

    invoke-direct {v7, p0}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$7;-><init>(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;)V

    invoke-virtual {v5, v7}, Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;->setOnDialogResultListener(Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog$OnDialogResultListener;)V

    .line 1077
    iget-object v5, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mCategoryNamingDialog:Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;

    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v7

    const-string v8, "dialog"

    invoke-virtual {v5, v7, v8}, Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_2

    .line 1080
    :pswitch_3
    sget-object v5, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeStyle;->NORMAL:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeStyle;

    iput-object v5, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mMenuType:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeStyle;

    .line 1081
    invoke-direct {p0}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->startActionMode()V

    .line 1082
    iget-object v5, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mSelectAllLayout:Landroid/widget/LinearLayout;

    if-eqz v5, :cond_3

    .line 1083
    iget-object v5, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mSelectAllLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v5}, Landroid/widget/LinearLayout;->requestFocus()Z

    goto :goto_2

    .line 1087
    :pswitch_4
    sget-object v5, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeStyle;->DELETE:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeStyle;

    iput-object v5, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mMenuType:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeStyle;

    .line 1088
    invoke-direct {p0}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->startActionMode()V

    .line 1089
    iget-object v5, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mSelectAllLayout:Landroid/widget/LinearLayout;

    if-eqz v5, :cond_3

    .line 1090
    iget-object v5, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mSelectAllLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v5}, Landroid/widget/LinearLayout;->requestFocus()Z

    goto :goto_2

    .line 1094
    :pswitch_5
    new-instance v5, Landroid/content/Intent;

    invoke-direct {v5}, Landroid/content/Intent;-><init>()V

    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v7

    const-class v8, Lcom/samsung/android/app/memo/CategoryManagementActivity;

    invoke-virtual {v5, v7, v8}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v1

    .line 1095
    .local v1, "intent":Landroid/content/Intent;
    const/high16 v5, 0x4000000

    invoke-virtual {v1, v5}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 1096
    invoke-virtual {p0, v1}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->startActivity(Landroid/content/Intent;)V

    goto :goto_2

    .line 1100
    .end local v1    # "intent":Landroid/content/Intent;
    :pswitch_6
    invoke-static {v7, v7}, Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;->newInstance(Ljava/lang/String;Ljava/lang/String;)Landroid/app/DialogFragment;

    move-result-object v5

    .line 1099
    check-cast v5, Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;

    iput-object v5, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mCategoryNamingDialog:Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;

    .line 1102
    iget-object v5, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mCategoryNamingDialog:Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;

    new-instance v7, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$8;

    invoke-direct {v7, p0}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$8;-><init>(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;)V

    invoke-virtual {v5, v7}, Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;->setOnDialogResultListener(Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog$OnDialogResultListener;)V

    .line 1116
    iget-object v5, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mCategoryNamingDialog:Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;

    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v7

    const-string v8, "dialog"

    invoke-virtual {v5, v7, v8}, Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 1119
    :pswitch_7
    invoke-direct {p0}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->onSelectSCloud()V

    goto/16 :goto_2

    .line 1122
    :pswitch_8
    new-instance v5, Landroid/content/Intent;

    invoke-direct {v5}, Landroid/content/Intent;-><init>()V

    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v6

    const-class v7, Lcom/samsung/android/app/memo/AboutThisServiceActivity;

    invoke-virtual {v5, v6, v7}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    .line 1123
    .local v0, "aboutThisService":Landroid/content/Intent;
    invoke-virtual {p0, v0}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 1053
    nop

    :pswitch_data_0
    .packed-switch 0x7f0e0092
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_2
        :pswitch_6
        :pswitch_5
        :pswitch_7
        :pswitch_8
        :pswitch_1
    .end packed-switch
.end method

.method public onPause()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1195
    iget-boolean v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mIsWidgetPick:Z

    if-eqz v0, :cond_0

    .line 1196
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->sendUpdateAllWidgetBroadcastEvent()V

    .line 1197
    :cond_0
    sget-boolean v0, Lcom/samsung/android/app/memo/util/Utils;->HAVE_SLOOKBEZELINTERACTION_API:Z

    if-eqz v0, :cond_3

    .line 1200
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mBezelManager:Lcom/samsung/android/quickconnect/QuickConnectManager;

    if-eqz v0, :cond_1

    .line 1201
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mBezelManager:Lcom/samsung/android/quickconnect/QuickConnectManager;

    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mListener:Lcom/samsung/android/quickconnect/QuickConnectManager$QuickConnectListener;

    invoke-virtual {v0, v1}, Lcom/samsung/android/quickconnect/QuickConnectManager;->unregisterListener(Lcom/samsung/android/quickconnect/QuickConnectManager$QuickConnectListener;)V

    .line 1203
    :cond_1
    const-string v0, "com.samsung.android.sconnect"

    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getCallingPackage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mIsSkipFinishForSConnect:Z

    if-nez v0, :cond_2

    .line 1204
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 1211
    :goto_0
    invoke-direct {p0}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->savePreferences()V

    .line 1212
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->hideUndoBar()V

    .line 1213
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mIsPause:Z

    .line 1214
    invoke-super {p0}, Landroid/app/Fragment;->onPause()V

    .line 1215
    return-void

    .line 1206
    :cond_2
    iput-boolean v2, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mIsSkipFinishForSConnect:Z

    goto :goto_0

    .line 1210
    :cond_3
    iput-boolean v2, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mIsSkipFinishForSConnect:Z

    goto :goto_0
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)V
    .locals 10
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    const v9, 0x7f0e009c

    const v8, 0x7f0e009d

    const v7, 0x7f0e0092

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 962
    sget-object v2, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->TAG:Ljava/lang/String;

    const-string v5, "onPrepareOptionsMenu()"

    invoke-static {v2, v5}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 963
    iget-boolean v2, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mIsSearchRequested:Z

    if-eqz v2, :cond_1

    .line 964
    invoke-interface {p1, v3, v3}, Landroid/view/Menu;->setGroupVisible(IZ)V

    .line 1044
    :cond_0
    :goto_0
    return-void

    .line 968
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    const v5, 0x7f0e0026

    invoke-virtual {v2, v5}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/support/v4/widget/DrawerLayout;

    .line 970
    .local v1, "drawerLayout":Landroid/support/v4/widget/DrawerLayout;
    if-eqz v1, :cond_2

    iget-object v2, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mAdapter:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;

    invoke-virtual {v2}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;->getCount()I

    move-result v2

    if-nez v2, :cond_2

    invoke-static {}, Lcom/samsung/android/app/memo/util/Utils;->IsNoMemosPopupModel()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 971
    iget-object v2, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mActivity:Landroid/app/Activity;

    const v5, 0x7f0e0035

    invoke-virtual {v2, v5}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    const/4 v2, 0x3

    invoke-virtual {v1, v2}, Landroid/support/v4/widget/DrawerLayout;->isDrawerOpen(I)Z

    move-result v2

    if-eqz v2, :cond_3

    const/16 v2, 0x8

    :goto_1
    invoke-virtual {v5, v2}, Landroid/view/View;->setVisibility(I)V

    .line 974
    :cond_2
    if-eqz v1, :cond_4

    const/4 v2, 0x3

    invoke-virtual {v1, v2}, Landroid/support/v4/widget/DrawerLayout;->isDrawerOpen(I)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 975
    iget-object v2, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mFabButton:Landroid/widget/ImageButton;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mFabButton:Landroid/widget/ImageButton;

    invoke-virtual {v2, v3}, Landroid/widget/ImageButton;->setFocusable(Z)V

    goto :goto_0

    :cond_3
    move v2, v3

    .line 971
    goto :goto_1

    .line 979
    :cond_4
    iget-object v2, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mFabButton:Landroid/widget/ImageButton;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mFabButton:Landroid/widget/ImageButton;

    invoke-virtual {v2, v4}, Landroid/widget/ImageButton;->setFocusable(Z)V

    .line 981
    :cond_5
    if-eqz v1, :cond_6

    iget-object v2, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mFragmentAction:Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;

    invoke-interface {v2}, Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;->isNeedDrawerOpen()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 982
    iget-object v2, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mFragmentAction:Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;

    iget-object v5, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mStrCategoryUUID:Ljava/lang/String;

    iget-object v6, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mStrCategoryTitle:Ljava/lang/String;

    invoke-interface {v2, v5, v6, v4}, Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;->onCategroyChanged(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 983
    iget-object v2, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mFragmentAction:Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;

    invoke-interface {v2}, Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;->openDrawer()V

    .line 984
    iget-object v2, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mFragmentAction:Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;

    invoke-interface {v2, v3}, Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;->setNeedDrawerOpen(Z)V

    goto :goto_0

    .line 988
    :cond_6
    if-eqz p1, :cond_0

    .line 989
    const v2, 0x7f0e0099

    invoke-interface {p1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v2

    if-eqz v2, :cond_8

    .line 990
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    instance-of v2, v2, Lcom/samsung/android/app/memo/ExternalPicker;

    if-nez v2, :cond_7

    iget-boolean v2, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mIsWidgetPick:Z

    if-eqz v2, :cond_12

    .line 991
    :cond_7
    const v2, 0x7f0e0099

    invoke-interface {p1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v2

    invoke-interface {v2, v3}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 998
    :cond_8
    :goto_2
    invoke-interface {p1, v8}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v2

    if-eqz v2, :cond_9

    .line 999
    invoke-static {}, Lcom/samsung/android/app/memo/util/Utils;->isSamsungAppsDownloadable()Z

    move-result v2

    if-eqz v2, :cond_15

    .line 1000
    invoke-interface {p1, v8}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v2

    invoke-interface {v2, v4}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 1005
    :cond_9
    :goto_3
    const v2, 0x7f0e0098

    invoke-interface {p1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v2

    if-eqz v2, :cond_a

    invoke-interface {p1, v7}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v2

    if-eqz v2, :cond_a

    const v2, 0x7f0e0097

    invoke-interface {p1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v2

    if-eqz v2, :cond_a

    iget-object v2, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mAbsListView:Landroid/widget/AbsListView;

    if-eqz v2, :cond_a

    .line 1006
    const v2, 0x7f0e0098

    invoke-interface {p1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v5

    iget-object v2, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mAbsListView:Landroid/widget/AbsListView;

    invoke-virtual {v2}, Landroid/widget/AbsListView;->getCount()I

    move-result v2

    if-nez v2, :cond_16

    move v2, v3

    :goto_4
    invoke-interface {v5, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 1007
    invoke-static {}, Lcom/samsung/android/app/memo/util/Utils;->isTModel()Z

    move-result v2

    if-eqz v2, :cond_17

    .line 1008
    invoke-interface {p1, v7}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v2

    invoke-interface {v2, v3}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 1009
    sget-object v2, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->TAG:Ljava/lang/String;

    const-string v5, "TTTTTTTTTTTTTTT"

    invoke-static {v2, v5}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1017
    :goto_5
    const v2, 0x7f0e0097

    invoke-interface {p1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v5

    iget-object v2, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mAbsListView:Landroid/widget/AbsListView;

    invoke-virtual {v2}, Landroid/widget/AbsListView;->getCount()I

    move-result v2

    if-nez v2, :cond_19

    move v2, v3

    :goto_6
    invoke-interface {v5, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 1020
    :cond_a
    iget-object v2, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mMenuType:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeStyle;

    sget-object v5, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeStyle;->PICK_ONE:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeStyle;

    if-eq v2, v5, :cond_b

    iget-boolean v2, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mIsWidgetPick:Z

    if-eqz v2, :cond_d

    .line 1021
    :cond_b
    invoke-interface {p1, v8}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v2

    if-eqz v2, :cond_c

    .line 1022
    invoke-interface {p1, v8}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v2

    invoke-interface {v2, v3}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 1023
    :cond_c
    const v2, 0x7f0e0098

    invoke-interface {p1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v2

    invoke-interface {v2, v3}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 1024
    invoke-interface {p1, v9}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v2

    invoke-interface {v2, v3}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 1025
    invoke-interface {p1, v7}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v2

    invoke-interface {v2, v3}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 1028
    :cond_d
    const v2, 0x7f0e009a

    invoke-interface {p1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 1029
    .local v0, "addCategoryMenuItem":Landroid/view/MenuItem;
    if-eqz v0, :cond_f

    .line 1030
    invoke-static {}, Lcom/samsung/android/app/memo/util/Utils;->isTablet()Z

    move-result v2

    if-eqz v2, :cond_e

    iget-object v2, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mMenuType:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeStyle;

    sget-object v5, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeStyle;->PICK_ONE:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeStyle;

    if-eq v2, v5, :cond_e

    iget-boolean v2, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mIsWidgetPick:Z

    if-eqz v2, :cond_f

    .line 1031
    :cond_e
    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 1035
    :cond_f
    invoke-static {}, Lcom/samsung/android/app/memo/util/Utils;->isKnoxMode()Z

    move-result v2

    if-nez v2, :cond_10

    invoke-static {}, Lcom/samsung/android/app/memo/util/Utils;->isGuestMode()Z

    move-result v2

    if-nez v2, :cond_10

    invoke-static {}, Lcom/samsung/android/app/memo/util/Utils;->isMemoSyncAvailable()Z

    move-result v2

    if-eqz v2, :cond_10

    invoke-static {}, Lcom/samsung/android/app/memo/util/Utils;->isUPSM()I

    move-result v2

    if-ne v2, v4, :cond_11

    .line 1036
    :cond_10
    invoke-interface {p1, v9}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v2

    if-eqz v2, :cond_11

    .line 1037
    invoke-interface {p1, v9}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v2

    invoke-interface {v2, v3}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 1040
    :cond_11
    invoke-static {}, Lcom/samsung/android/app/memo/util/Utils;->isChinaCMCC()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1041
    invoke-interface {p1, v9}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v2

    invoke-interface {v2, v3}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto/16 :goto_0

    .line 993
    .end local v0    # "addCategoryMenuItem":Landroid/view/MenuItem;
    :cond_12
    const v2, 0x7f0e0099

    invoke-interface {p1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v5

    .line 994
    iget-object v2, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mStrCategoryUUID:Ljava/lang/String;

    if-nez v2, :cond_13

    move v2, v3

    .line 993
    :goto_7
    invoke-interface {v5, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto/16 :goto_2

    .line 994
    :cond_13
    iget-object v2, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mStrCategoryUUID:Ljava/lang/String;

    const-string v6, ""

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_14

    move v2, v3

    goto :goto_7

    :cond_14
    move v2, v4

    goto :goto_7

    .line 1002
    :cond_15
    invoke-interface {p1, v8}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v2

    invoke-interface {v2, v3}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto/16 :goto_3

    :cond_16
    move v2, v4

    .line 1006
    goto/16 :goto_4

    .line 1012
    :cond_17
    sget-object v2, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->TAG:Ljava/lang/String;

    const-string v5, "not t"

    invoke-static {v2, v5}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1013
    invoke-interface {p1, v7}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v2

    invoke-interface {v2, v4}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 1014
    invoke-interface {p1, v7}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v5

    .line 1015
    iget-object v2, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mAbsListView:Landroid/widget/AbsListView;

    invoke-virtual {v2}, Landroid/widget/AbsListView;->getCount()I

    move-result v2

    if-nez v2, :cond_18

    move v2, v3

    .line 1014
    :goto_8
    invoke-interface {v5, v2}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    goto/16 :goto_5

    :cond_18
    move v2, v4

    .line 1015
    goto :goto_8

    :cond_19
    move v2, v4

    .line 1017
    goto/16 :goto_6
.end method

.method public onQueryTextChange(Ljava/lang/String;)Z
    .locals 7
    .param p1, "newText"    # Ljava/lang/String;

    .prologue
    const v3, 0x7f0b0048

    const/4 v6, 0x1

    const/4 v5, 0x0

    const/16 v4, 0x5db

    .line 2561
    iput-object p1, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mStrQuery:Ljava/lang/String;

    .line 2562
    iget-object v2, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mFragmentAction:Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;

    invoke-interface {v2}, Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;->closeDrawer()V

    .line 2563
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    move-object v1, p1

    .line 2564
    .local v1, "newFilter":Ljava/lang/String;
    :goto_0
    iget-object v2, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mStrCurFilter:Ljava/lang/String;

    if-nez v2, :cond_2

    if-nez v1, :cond_2

    .line 2595
    :cond_0
    :goto_1
    return v6

    .line 2563
    .end local v1    # "newFilter":Ljava/lang/String;
    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    .line 2567
    .restart local v1    # "newFilter":Ljava/lang/String;
    :cond_2
    iget-object v2, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mStrCurFilter:Ljava/lang/String;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mStrCurFilter:Ljava/lang/String;

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 2570
    :cond_3
    iget-object v2, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mStrBeforeQuery:Ljava/lang/String;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mStrBeforeQuery:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-ne v2, v4, :cond_4

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    if-le v2, v4, :cond_4

    .line 2571
    iget-object v2, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mActivity:Landroid/app/Activity;

    invoke-static {v2, v3}, Lcom/samsung/android/app/memo/util/Utils;->showToast(Landroid/content/Context;I)V

    .line 2572
    iget-object v2, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mSearchView:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MySearchView;

    iget-object v3, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mStrBeforeQuery:Ljava/lang/String;

    invoke-virtual {v3, v5, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3, v5}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MySearchView;->setQuery(Ljava/lang/CharSequence;Z)V

    goto :goto_1

    .line 2576
    :cond_4
    if-eqz p1, :cond_5

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    if-le v2, v4, :cond_5

    .line 2577
    iget-object v2, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mActivity:Landroid/app/Activity;

    invoke-static {v2, v3}, Lcom/samsung/android/app/memo/util/Utils;->showToast(Landroid/content/Context;I)V

    .line 2578
    iget-object v2, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mSearchView:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MySearchView;

    invoke-virtual {p1, v5, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3, v5}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MySearchView;->setQuery(Ljava/lang/CharSequence;Z)V

    .line 2579
    invoke-virtual {p1, v5, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mStrBeforeQuery:Ljava/lang/String;

    .line 2582
    :cond_5
    if-eqz p1, :cond_6

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    if-gt v2, v4, :cond_6

    .line 2583
    iput-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mStrCurFilter:Ljava/lang/String;

    .line 2584
    iput-object p1, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mStrBeforeQuery:Ljava/lang/String;

    .line 2586
    :cond_6
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    .line 2587
    .local v0, "handler":Landroid/os/Handler;
    iget-boolean v2, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mIsSearchRequested:Z

    if-eqz v2, :cond_0

    .line 2588
    new-instance v2, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$11;

    invoke-direct {v2, p0}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$11;-><init>(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;)V

    .line 2593
    const-wide/16 v4, 0x64

    .line 2588
    invoke-virtual {v0, v2, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_1
.end method

.method public onQueryTextSubmit(Ljava/lang/String;)Z
    .locals 1
    .param p1, "query"    # Ljava/lang/String;

    .prologue
    .line 2600
    invoke-direct {p0}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->hideIme()V

    .line 2601
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mFragmentAction:Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;

    invoke-interface {v0}, Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;->closeDrawer()V

    .line 2602
    const/4 v0, 0x1

    return v0
.end method

.method public onResume()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1157
    invoke-super {p0}, Landroid/app/Fragment;->onResume()V

    .line 1158
    sget-object v1, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->TAG:Ljava/lang/String;

    const-string v2, "onResume()"

    invoke-static {v1, v2}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1160
    sget-boolean v1, Lcom/samsung/android/app/memo/util/Utils;->HAVE_SLOOKBEZELINTERACTION_API:Z

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mIsCheckBoxMode:Z

    if-eqz v1, :cond_0

    .line 1161
    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mBezelManager:Lcom/samsung/android/quickconnect/QuickConnectManager;

    if-eqz v1, :cond_0

    .line 1162
    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mBezelManager:Lcom/samsung/android/quickconnect/QuickConnectManager;

    iget-object v2, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mListener:Lcom/samsung/android/quickconnect/QuickConnectManager$QuickConnectListener;

    invoke-virtual {v1, v2}, Lcom/samsung/android/quickconnect/QuickConnectManager;->registerListener(Lcom/samsung/android/quickconnect/QuickConnectManager$QuickConnectListener;)V

    .line 1166
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mSearchView:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MySearchView;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mSearchView:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MySearchView;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mSearchView:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MySearchView;

    invoke-virtual {v1}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MySearchView;->isIconified()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1167
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->invalidateOptionsMenu()V

    .line 1169
    :cond_2
    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mAdapter:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;

    if-eqz v1, :cond_3

    .line 1170
    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mAdapter:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;

    invoke-virtual {v1}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;->notifyDataSetChanged()V

    .line 1171
    :cond_3
    iget-boolean v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mIsPause:Z

    if-eqz v1, :cond_5

    .line 1172
    invoke-direct {p0}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->loadPreferences()V

    .line 1173
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const v2, 0x7f0e0026

    invoke-virtual {v1, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/widget/DrawerLayout;

    .line 1174
    .local v0, "drawerLayout":Landroid/support/v4/widget/DrawerLayout;
    if-eqz v0, :cond_4

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/DrawerLayout;->isDrawerOpen(I)Z

    move-result v1

    if-nez v1, :cond_4

    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mFragmentAction:Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;

    invoke-interface {v1}, Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;->isNeedDrawerOpen()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 1175
    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mFragmentAction:Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;

    iget-object v2, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mStrCategoryUUID:Ljava/lang/String;

    iget-object v3, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mStrCategoryTitle:Ljava/lang/String;

    invoke-interface {v1, v2, v3, v4}, Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;->onCategroyChanged(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 1177
    :cond_4
    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mStrCategoryUUID:Ljava/lang/String;

    iget-object v2, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mStrCategoryTitle:Ljava/lang/String;

    sget v3, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mCategoryType:I

    invoke-virtual {p0, v1, v2, v3}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->reloadedCursor(Ljava/lang/String;Ljava/lang/String;I)V

    .line 1178
    iput-boolean v4, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mIsPause:Z

    .line 1180
    .end local v0    # "drawerLayout":Landroid/support/v4/widget/DrawerLayout;
    :cond_5
    iget-boolean v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mIsCategoryChanged:Z

    if-eqz v1, :cond_6

    .line 1181
    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mFragmentAction:Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;

    const-string v2, ""

    iget-object v3, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mStrCategoryTitle:Ljava/lang/String;

    invoke-interface {v1, v2, v3, v4}, Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;->onCategroyChanged(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 1182
    :cond_6
    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mSearchView:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MySearchView;

    if-eqz v1, :cond_8

    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mSearchView:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MySearchView;

    invoke-virtual {v1}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MySearchView;->isIconified()Z

    move-result v1

    if-nez v1, :cond_8

    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mSearchView:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MySearchView;

    invoke-virtual {v1}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MySearchView;->getQuery()Ljava/lang/CharSequence;

    move-result-object v1

    if-eqz v1, :cond_8

    .line 1183
    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mSearchView:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MySearchView;

    invoke-virtual {v1}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MySearchView;->getQuery()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mStrQuery:Ljava/lang/String;

    .line 1190
    :cond_7
    :goto_0
    const-string v1, "VerificationLog"

    const-string v2, "Executed"

    invoke-static {v1, v2}, Lcom/samsung/android/app/memo/util/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1191
    return-void

    .line 1185
    :cond_8
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mStrQuery:Ljava/lang/String;

    .line 1186
    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mAbsListView:Landroid/widget/AbsListView;

    if-eqz v1, :cond_7

    .line 1187
    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mAbsListView:Landroid/widget/AbsListView;

    invoke-virtual {v1}, Landroid/widget/AbsListView;->requestFocus()Z

    goto :goto_0
.end method

.method public onSearchRequested()V
    .locals 2

    .prologue
    .line 2627
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mMenuSearch:Landroid/view/MenuItem;

    if-eqz v0, :cond_0

    .line 2628
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mMenuSearch:Landroid/view/MenuItem;

    invoke-interface {v0}, Landroid/view/MenuItem;->expandActionView()Z

    .line 2629
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mFragmentAction:Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;->setDrawerLock(Z)V

    .line 2632
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mSearchView:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MySearchView;

    if-eqz v0, :cond_1

    .line 2633
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mSearchView:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MySearchView;

    invoke-virtual {v0}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MySearchView;->requestFocus()Z

    .line 2635
    :cond_1
    return-void
.end method

.method public onUndo(Landroid/os/Parcelable;)V
    .locals 2
    .param p1, "token"    # Landroid/os/Parcelable;

    .prologue
    .line 2009
    sget-object v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->TAG:Ljava/lang/String;

    const-string v1, "onUndo"

    invoke-static {v0, v1}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2010
    invoke-direct {p0}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->cancelDeletingMemos()V

    .line 2011
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mIDsBeingDeleted:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 2012
    return-void
.end method

.method public reloadedCursor()V
    .locals 3

    .prologue
    .line 668
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->isAdded()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 669
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->getLoaderManager()Landroid/app/LoaderManager;

    move-result-object v0

    const/16 v1, 0x3e8

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p0}, Landroid/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroid/app/LoaderManager$LoaderCallbacks;)Landroid/content/Loader;

    .line 671
    :cond_0
    return-void
.end method

.method public reloadedCursor(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1, "uuid"    # Ljava/lang/String;
    .param p2, "title"    # Ljava/lang/String;

    .prologue
    .line 658
    iput-object p1, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mStrCategoryUUID:Ljava/lang/String;

    .line 659
    iput-object p2, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mStrCategoryTitle:Ljava/lang/String;

    .line 660
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mActivity:Landroid/app/Activity;

    invoke-static {v0, p1, p2}, Lcom/samsung/android/app/memo/util/Utils;->getCategoryType(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    sput v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mCategoryType:I

    .line 661
    invoke-direct {p0}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->savePreferences()V

    .line 662
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->isAdded()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 663
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->getLoaderManager()Landroid/app/LoaderManager;

    move-result-object v0

    const/16 v1, 0x3e8

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p0}, Landroid/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroid/app/LoaderManager$LoaderCallbacks;)Landroid/content/Loader;

    .line 665
    :cond_0
    return-void
.end method

.method public reloadedCursor(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 3
    .param p1, "uuid"    # Ljava/lang/String;
    .param p2, "title"    # Ljava/lang/String;
    .param p3, "type"    # I

    .prologue
    .line 648
    iput-object p1, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mStrCategoryUUID:Ljava/lang/String;

    .line 649
    iput-object p2, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mStrCategoryTitle:Ljava/lang/String;

    .line 650
    sput p3, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mCategoryType:I

    .line 651
    invoke-direct {p0}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->savePreferences()V

    .line 652
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->isAdded()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 653
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->getLoaderManager()Landroid/app/LoaderManager;

    move-result-object v0

    const/16 v1, 0x3e8

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p0}, Landroid/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroid/app/LoaderManager$LoaderCallbacks;)Landroid/content/Loader;

    .line 655
    :cond_0
    return-void
.end method

.method public sendUpdateAllWidgetBroadcastEvent()V
    .locals 2

    .prologue
    .line 2025
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.samsung.android.widget.memo.UPDATE_ALL_WIDGET"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2026
    .local v0, "intent":Landroid/content/Intent;
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2027
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/app/Activity;->sendBroadcast(Landroid/content/Intent;)V

    .line 2028
    :cond_0
    return-void
.end method

.method public setActionModeStyle(Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeStyle;)V
    .locals 0
    .param p1, "actionModeStyle"    # Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeStyle;

    .prologue
    .line 2505
    iput-object p1, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mMenuType:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeStyle;

    .line 2506
    return-void
.end method

.method public showUndoBar(I)V
    .locals 2
    .param p1, "memoId"    # I

    .prologue
    .line 2041
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mIDsBeingDeleted:Ljava/util/ArrayList;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2042
    invoke-direct {p0}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->showUndoBar()V

    .line 2043
    return-void
.end method
