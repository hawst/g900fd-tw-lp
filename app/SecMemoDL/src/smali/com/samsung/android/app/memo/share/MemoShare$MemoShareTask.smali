.class public Lcom/samsung/android/app/memo/share/MemoShare$MemoShareTask;
.super Landroid/os/AsyncTask;
.source "MemoShare.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/memo/share/MemoShare;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "MemoShareTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Integer;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field id:J

.field mActivity:Landroid/app/Activity;

.field mProgress:Landroid/app/ProgressDialog;

.field final synthetic this$0:Lcom/samsung/android/app/memo/share/MemoShare;

.field xmlUtils:Lcom/samsung/android/app/memo/share/beam/XMLUtills;


# direct methods
.method public constructor <init>(Lcom/samsung/android/app/memo/share/MemoShare;Landroid/app/Activity;J)V
    .locals 3
    .param p2, "activity"    # Landroid/app/Activity;
    .param p3, "id"    # J

    .prologue
    .line 362
    iput-object p1, p0, Lcom/samsung/android/app/memo/share/MemoShare$MemoShareTask;->this$0:Lcom/samsung/android/app/memo/share/MemoShare;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 363
    iput-object p2, p0, Lcom/samsung/android/app/memo/share/MemoShare$MemoShareTask;->mActivity:Landroid/app/Activity;

    .line 364
    iput-wide p3, p0, Lcom/samsung/android/app/memo/share/MemoShare$MemoShareTask;->id:J

    .line 365
    new-instance v0, Lcom/samsung/android/app/memo/share/beam/XMLUtills;

    # getter for: Lcom/samsung/android/app/memo/share/MemoShare;->context:Landroid/content/Context;
    invoke-static {p1}, Lcom/samsung/android/app/memo/share/MemoShare;->access$0(Lcom/samsung/android/app/memo/share/MemoShare;)Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/samsung/android/app/memo/share/beam/XMLUtills;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/app/memo/share/MemoShare$MemoShareTask;->xmlUtils:Lcom/samsung/android/app/memo/share/beam/XMLUtills;

    .line 366
    return-void
.end method


# virtual methods
.method protected bridge varargs synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/samsung/android/app/memo/share/MemoShare$MemoShareTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 4
    .param p1, "arg0"    # [Ljava/lang/Void;

    .prologue
    .line 380
    iget-object v1, p0, Lcom/samsung/android/app/memo/share/MemoShare$MemoShareTask;->this$0:Lcom/samsung/android/app/memo/share/MemoShare;

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    invoke-static {v1, v2}, Lcom/samsung/android/app/memo/share/MemoShare;->access$1(Lcom/samsung/android/app/memo/share/MemoShare;Ljava/util/ArrayList;)V

    .line 381
    iget-object v1, p0, Lcom/samsung/android/app/memo/share/MemoShare$MemoShareTask;->this$0:Lcom/samsung/android/app/memo/share/MemoShare;

    # getter for: Lcom/samsung/android/app/memo/share/MemoShare;->imageAttachmentList:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/samsung/android/app/memo/share/MemoShare;->access$2(Lcom/samsung/android/app/memo/share/MemoShare;)Ljava/util/ArrayList;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 382
    iget-object v1, p0, Lcom/samsung/android/app/memo/share/MemoShare$MemoShareTask;->this$0:Lcom/samsung/android/app/memo/share/MemoShare;

    # getter for: Lcom/samsung/android/app/memo/share/MemoShare;->attachmentList:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/samsung/android/app/memo/share/MemoShare;->access$3(Lcom/samsung/android/app/memo/share/MemoShare;)Ljava/util/ArrayList;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/app/memo/share/MemoShare$MemoShareTask;->this$0:Lcom/samsung/android/app/memo/share/MemoShare;

    # getter for: Lcom/samsung/android/app/memo/share/MemoShare;->imageAttachmentList:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/samsung/android/app/memo/share/MemoShare;->access$2(Lcom/samsung/android/app/memo/share/MemoShare;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 384
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/app/memo/share/MemoShare$MemoShareTask;->this$0:Lcom/samsung/android/app/memo/share/MemoShare;

    # getter for: Lcom/samsung/android/app/memo/share/MemoShare;->voiceAttachment:Landroid/net/Uri;
    invoke-static {v1}, Lcom/samsung/android/app/memo/share/MemoShare;->access$4(Lcom/samsung/android/app/memo/share/MemoShare;)Landroid/net/Uri;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 385
    iget-object v1, p0, Lcom/samsung/android/app/memo/share/MemoShare$MemoShareTask;->this$0:Lcom/samsung/android/app/memo/share/MemoShare;

    # getter for: Lcom/samsung/android/app/memo/share/MemoShare;->attachmentList:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/samsung/android/app/memo/share/MemoShare;->access$3(Lcom/samsung/android/app/memo/share/MemoShare;)Ljava/util/ArrayList;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/app/memo/share/MemoShare$MemoShareTask;->this$0:Lcom/samsung/android/app/memo/share/MemoShare;

    # getter for: Lcom/samsung/android/app/memo/share/MemoShare;->voiceAttachment:Landroid/net/Uri;
    invoke-static {v2}, Lcom/samsung/android/app/memo/share/MemoShare;->access$4(Lcom/samsung/android/app/memo/share/MemoShare;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 387
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/app/memo/share/MemoShare$MemoShareTask;->this$0:Lcom/samsung/android/app/memo/share/MemoShare;

    # getter for: Lcom/samsung/android/app/memo/share/MemoShare;->memoFileUri:Landroid/net/Uri;
    invoke-static {v1}, Lcom/samsung/android/app/memo/share/MemoShare;->access$5(Lcom/samsung/android/app/memo/share/MemoShare;)Landroid/net/Uri;

    move-result-object v1

    if-nez v1, :cond_3

    .line 388
    iget-object v1, p0, Lcom/samsung/android/app/memo/share/MemoShare$MemoShareTask;->mActivity:Landroid/app/Activity;

    instance-of v1, v1, Lcom/samsung/android/app/memo/MemoContentActivity;

    if-eqz v1, :cond_2

    .line 389
    iget-object v1, p0, Lcom/samsung/android/app/memo/share/MemoShare$MemoShareTask;->this$0:Lcom/samsung/android/app/memo/share/MemoShare;

    # getter for: Lcom/samsung/android/app/memo/share/MemoShare;->context:Landroid/content/Context;
    invoke-static {v1}, Lcom/samsung/android/app/memo/share/MemoShare;->access$0(Lcom/samsung/android/app/memo/share/MemoShare;)Landroid/content/Context;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/app/memo/share/MemoShare$SessionUpdateInterfaceForMemoShare;

    invoke-interface {v1}, Lcom/samsung/android/app/memo/share/MemoShare$SessionUpdateInterfaceForMemoShare;->updateSessionOnMemoShare()V

    .line 391
    :cond_2
    iget-object v1, p0, Lcom/samsung/android/app/memo/share/MemoShare$MemoShareTask;->xmlUtils:Lcom/samsung/android/app/memo/share/beam/XMLUtills;

    iget-wide v2, p0, Lcom/samsung/android/app/memo/share/MemoShare$MemoShareTask;->id:J

    invoke-virtual {v1, v2, v3}, Lcom/samsung/android/app/memo/share/beam/XMLUtills;->createXMLFile(J)Ljava/lang/String;

    move-result-object v0

    .line 392
    .local v0, "path":Ljava/lang/String;
    if-eqz v0, :cond_3

    .line 393
    iget-object v1, p0, Lcom/samsung/android/app/memo/share/MemoShare$MemoShareTask;->this$0:Lcom/samsung/android/app/memo/share/MemoShare;

    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v2}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/android/app/memo/share/MemoShare;->access$6(Lcom/samsung/android/app/memo/share/MemoShare;Landroid/net/Uri;)V

    .line 395
    .end local v0    # "path":Ljava/lang/String;
    :cond_3
    const/4 v1, 0x0

    return-object v1
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1
    check-cast p1, Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/samsung/android/app/memo/share/MemoShare$MemoShareTask;->onPostExecute(Ljava/lang/Void;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/Void;)V
    .locals 4
    .param p1, "result"    # Ljava/lang/Void;

    .prologue
    const/4 v3, 0x0

    .line 401
    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/app/memo/share/MemoShare$MemoShareTask;->mProgress:Landroid/app/ProgressDialog;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/android/app/memo/share/MemoShare$MemoShareTask;->mProgress:Landroid/app/ProgressDialog;

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 402
    iget-object v1, p0, Lcom/samsung/android/app/memo/share/MemoShare$MemoShareTask;->mProgress:Landroid/app/ProgressDialog;

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->dismiss()V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 406
    :cond_0
    iput-object v3, p0, Lcom/samsung/android/app/memo/share/MemoShare$MemoShareTask;->mProgress:Landroid/app/ProgressDialog;

    .line 408
    :goto_0
    iget-object v1, p0, Lcom/samsung/android/app/memo/share/MemoShare$MemoShareTask;->this$0:Lcom/samsung/android/app/memo/share/MemoShare;

    # getter for: Lcom/samsung/android/app/memo/share/MemoShare;->memoFileUri:Landroid/net/Uri;
    invoke-static {v1}, Lcom/samsung/android/app/memo/share/MemoShare;->access$5(Lcom/samsung/android/app/memo/share/MemoShare;)Landroid/net/Uri;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 409
    iget-object v1, p0, Lcom/samsung/android/app/memo/share/MemoShare$MemoShareTask;->this$0:Lcom/samsung/android/app/memo/share/MemoShare;

    invoke-virtual {v1}, Lcom/samsung/android/app/memo/share/MemoShare;->share()V

    .line 410
    :cond_1
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 411
    return-void

    .line 403
    :catch_0
    move-exception v0

    .line 404
    .local v0, "ie":Ljava/lang/IllegalArgumentException;
    :try_start_1
    # getter for: Lcom/samsung/android/app/memo/share/MemoShare;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/app/memo/share/MemoShare;->access$7()Ljava/lang/String;

    move-result-object v1

    const-string v2, "onPostExecute"

    invoke-static {v1, v2, v0}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 406
    iput-object v3, p0, Lcom/samsung/android/app/memo/share/MemoShare$MemoShareTask;->mProgress:Landroid/app/ProgressDialog;

    goto :goto_0

    .line 405
    .end local v0    # "ie":Ljava/lang/IllegalArgumentException;
    :catchall_0
    move-exception v1

    .line 406
    iput-object v3, p0, Lcom/samsung/android/app/memo/share/MemoShare$MemoShareTask;->mProgress:Landroid/app/ProgressDialog;

    .line 407
    throw v1
.end method

.method protected onPreExecute()V
    .locals 4

    .prologue
    .line 370
    iget-object v0, p0, Lcom/samsung/android/app/memo/share/MemoShare$MemoShareTask;->mActivity:Landroid/app/Activity;

    const-string v1, ""

    .line 371
    iget-object v2, p0, Lcom/samsung/android/app/memo/share/MemoShare$MemoShareTask;->mActivity:Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b00ab

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 370
    invoke-static {v0, v1, v2}, Landroid/app/ProgressDialog;->show(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Landroid/app/ProgressDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/memo/share/MemoShare$MemoShareTask;->mProgress:Landroid/app/ProgressDialog;

    .line 372
    iget-object v0, p0, Lcom/samsung/android/app/memo/share/MemoShare$MemoShareTask;->mProgress:Landroid/app/ProgressDialog;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 373
    iget-object v0, p0, Lcom/samsung/android/app/memo/share/MemoShare$MemoShareTask;->mProgress:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isIndeterminate()Z

    .line 374
    iget-object v0, p0, Lcom/samsung/android/app/memo/share/MemoShare$MemoShareTask;->mProgress:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    .line 375
    invoke-super {p0}, Landroid/os/AsyncTask;->onPreExecute()V

    .line 376
    return-void
.end method
