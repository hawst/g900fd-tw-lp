.class public Lcom/samsung/android/app/migration/snbdownload/network/util/RelayUtil;
.super Ljava/lang/Object;
.source "RelayUtil.java"


# static fields
.field private static mListener:Lcom/samsung/android/app/migration/snbdownload/DownloadResponseListener;

.field private static mNetworkServiceApi:Lcom/samsung/android/app/migration/snbdownload/network/NetworkServiceApi;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getListener()Lcom/samsung/android/app/migration/snbdownload/DownloadResponseListener;
    .locals 1

    .prologue
    .line 42
    sget-object v0, Lcom/samsung/android/app/migration/snbdownload/network/util/RelayUtil;->mListener:Lcom/samsung/android/app/migration/snbdownload/DownloadResponseListener;

    return-object v0
.end method

.method public static getNetworkServiceInstance()Lcom/samsung/android/app/migration/snbdownload/network/NetworkServiceApi;
    .locals 1

    .prologue
    .line 34
    sget-object v0, Lcom/samsung/android/app/migration/snbdownload/network/util/RelayUtil;->mNetworkServiceApi:Lcom/samsung/android/app/migration/snbdownload/network/NetworkServiceApi;

    return-object v0
.end method

.method public static setListener(Lcom/samsung/android/app/migration/snbdownload/DownloadResponseListener;)V
    .locals 0
    .param p0, "receiver"    # Lcom/samsung/android/app/migration/snbdownload/DownloadResponseListener;

    .prologue
    .line 38
    sput-object p0, Lcom/samsung/android/app/migration/snbdownload/network/util/RelayUtil;->mListener:Lcom/samsung/android/app/migration/snbdownload/DownloadResponseListener;

    .line 39
    return-void
.end method

.method public static setNetworkServiceInstance(Lcom/samsung/android/app/migration/snbdownload/network/NetworkServiceApi;)V
    .locals 0
    .param p0, "networkServiceApi"    # Lcom/samsung/android/app/migration/snbdownload/network/NetworkServiceApi;

    .prologue
    .line 30
    sput-object p0, Lcom/samsung/android/app/migration/snbdownload/network/util/RelayUtil;->mNetworkServiceApi:Lcom/samsung/android/app/migration/snbdownload/network/NetworkServiceApi;

    .line 31
    return-void
.end method
