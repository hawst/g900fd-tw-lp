.class Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy$ClipboardDataPasteHandler;
.super Ljava/lang/Object;
.source "ClipboardEXProxy.java"

# interfaces
.implements Ljava/lang/reflect/InvocationHandler;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ClipboardDataPasteHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;


# direct methods
.method private constructor <init>(Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;)V
    .locals 0

    .prologue
    .line 414
    iput-object p1, p0, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy$ClipboardDataPasteHandler;->this$0:Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy$ClipboardDataPasteHandler;)V
    .locals 0

    .prologue
    .line 414
    invoke-direct {p0, p1}, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy$ClipboardDataPasteHandler;-><init>(Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;)V

    return-void
.end method


# virtual methods
.method public invoke(Ljava/lang/Object;Ljava/lang/reflect/Method;[Ljava/lang/Object;)Ljava/lang/Object;
    .locals 5
    .param p1, "proxy"    # Ljava/lang/Object;
    .param p2, "method"    # Ljava/lang/reflect/Method;
    .param p3, "args"    # [Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 419
    invoke-virtual {p2}, Ljava/lang/reflect/Method;->getName()Ljava/lang/String;

    move-result-object v0

    .line 421
    .local v0, "method_name":Ljava/lang/String;
    const-string v2, "asBinder"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 422
    iget-object v2, p0, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy$ClipboardDataPasteHandler;->this$0:Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;

    new-instance v3, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy$IClipboardDataPasteEventStub;

    iget-object v4, p0, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy$ClipboardDataPasteHandler;->this$0:Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;

    invoke-direct {v3, v4, v1}, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy$IClipboardDataPasteEventStub;-><init>(Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy$IClipboardDataPasteEventStub;)V

    invoke-static {v2, v3}, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->access$0(Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;Ljava/lang/Object;)V

    .line 423
    iget-object v1, p0, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy$ClipboardDataPasteHandler;->this$0:Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;

    # getter for: Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->mBinder:Ljava/lang/Object;
    invoke-static {v1}, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->access$1(Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;)Ljava/lang/Object;

    move-result-object v1

    .line 425
    :cond_0
    return-object v1
.end method
