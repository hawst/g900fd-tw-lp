.class public interface abstract Lcom/samsung/android/app/migration/snbdownload/network/downsync/util/Constants$SERVER;
.super Ljava/lang/Object;
.source "Constants.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/migration/snbdownload/network/downsync/util/Constants;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "SERVER"
.end annotation


# static fields
.field public static final ACCESS_TOKEN_PARM:Ljava/lang/String; = "&access_token="

.field public static final BAD_ACCESS_TOKEN:I = 0x4a40

.field public static final CID_PARM:Ljava/lang/String; = "&cid="

.field public static final DEVICE_ID_PARM:Ljava/lang/String; = "&did="

.field public static final KEY:Ljava/lang/String; = "&key="

.field public static final USER_ID_PARM:Ljava/lang/String; = "&uid="
