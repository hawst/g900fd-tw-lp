.class Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MySearchView;
.super Landroid/widget/SearchView;
.source "MemoListFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/memo/uifragment/MemoListFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MySearchView"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;


# direct methods
.method public constructor <init>(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;Landroid/content/Context;)V
    .locals 0
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 2509
    iput-object p1, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MySearchView;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    .line 2510
    invoke-direct {p0, p2}, Landroid/widget/SearchView;-><init>(Landroid/content/Context;)V

    .line 2511
    return-void
.end method

.method private setSearchViewPadding()V
    .locals 6

    .prologue
    .line 2514
    iget-object v4, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MySearchView;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    invoke-virtual {v4}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-virtual {v4}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0900e1

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v4

    float-to-int v1, v4

    .line 2515
    .local v1, "left":I
    iget-object v4, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MySearchView;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    invoke-virtual {v4}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-virtual {v4}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0900e3

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v4

    float-to-int v3, v4

    .line 2516
    .local v3, "top":I
    iget-object v4, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MySearchView;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    invoke-virtual {v4}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-virtual {v4}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0900e2

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v4

    float-to-int v2, v4

    .line 2517
    .local v2, "right":I
    iget-object v4, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MySearchView;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    invoke-virtual {v4}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-virtual {v4}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0900e4

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v4

    float-to-int v0, v4

    .line 2518
    .local v0, "bottom":I
    invoke-virtual {p0, v1, v3, v2, v0}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MySearchView;->setPadding(IIII)V

    .line 2519
    return-void
.end method


# virtual methods
.method public onActionViewCollapsed()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2540
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MySearchView;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    invoke-virtual {v0}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    instance-of v0, v0, Lcom/samsung/android/app/memo/ExternalPicker;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MySearchView;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mIsWidgetPick:Z
    invoke-static {v0}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->access$49(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MySearchView;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mMenuType:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeStyle;
    invoke-static {v0}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->access$10(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;)Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeStyle;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeStyle;->PICK_ONE:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeStyle;

    if-ne v0, v1, :cond_2

    .line 2541
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MySearchView;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mAbsListView:Landroid/widget/AbsListView;
    invoke-static {v0}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->access$9(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;)Landroid/widget/AbsListView;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/AbsListView;->setChoiceMode(I)V

    .line 2545
    :goto_0
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MySearchView;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    invoke-static {v0, v2}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->access$48(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;Z)V

    .line 2546
    const-string v0, " "

    invoke-virtual {p0, v0, v2}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MySearchView;->setQuery(Ljava/lang/CharSequence;Z)V

    .line 2547
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MySearchView;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mFragmentAction:Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;
    invoke-static {v0}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->access$36(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;)Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;

    move-result-object v0

    invoke-interface {v0, v2}, Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;->setDrawerLock(Z)V

    .line 2548
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MySearchView;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mFragmentAction:Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;
    invoke-static {v0}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->access$36(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;)Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;

    move-result-object v0

    invoke-interface {v0}, Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;->showCategoryList()V

    .line 2549
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MySearchView;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->access$50(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;Ljava/lang/String;)V

    .line 2551
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MySearchView;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    invoke-virtual {v0}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2552
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MySearchView;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    invoke-virtual {v0}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->invalidateOptionsMenu()V

    .line 2553
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MySearchView;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    invoke-virtual {v0}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->reloadedCursor()V

    .line 2554
    invoke-super {p0}, Landroid/widget/SearchView;->onActionViewCollapsed()V

    .line 2555
    return-void

    .line 2543
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MySearchView;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mAbsListView:Landroid/widget/AbsListView;
    invoke-static {v0}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->access$9(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;)Landroid/widget/AbsListView;

    move-result-object v0

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/widget/AbsListView;->setChoiceMode(I)V

    goto :goto_0
.end method

.method public onActionViewExpanded()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 2528
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MySearchView;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    invoke-static {v0, v1}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->access$48(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;Z)V

    .line 2529
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MySearchView;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mFragmentAction:Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;
    invoke-static {v0}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->access$36(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;)Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;

    move-result-object v0

    invoke-interface {v0}, Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;->hideCategoryList()V

    .line 2530
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MySearchView;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mFragmentAction:Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;
    invoke-static {v0}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->access$36(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;)Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;

    move-result-object v0

    invoke-interface {v0, v1}, Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;->setDrawerLock(Z)V

    .line 2531
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MySearchView;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mFabButton:Landroid/widget/ImageButton;
    invoke-static {v0}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->access$31(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;)Landroid/widget/ImageButton;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 2532
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MySearchView;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    invoke-virtual {v0}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->reloadedCursor()V

    .line 2533
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MySearchView;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    invoke-virtual {v0}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->invalidateOptionsMenu()V

    .line 2534
    invoke-static {}, Lcom/samsung/android/app/memo/util/Utils;->isTablet()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2535
    invoke-direct {p0}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$MySearchView;->setSearchViewPadding()V

    .line 2536
    :cond_0
    invoke-super {p0}, Landroid/widget/SearchView;->onActionViewExpanded()V

    .line 2537
    return-void
.end method

.method public performLongClick()Z
    .locals 1

    .prologue
    .line 2523
    iget-object v0, p0, Landroid/widget/SearchView;->mQueryTextView:Landroid/widget/SearchView$SearchAutoComplete;

    invoke-virtual {v0}, Landroid/widget/SearchView$SearchAutoComplete;->performLongClick()Z

    move-result v0

    return v0
.end method
