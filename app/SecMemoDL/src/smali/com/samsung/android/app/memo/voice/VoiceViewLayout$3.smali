.class Lcom/samsung/android/app/memo/voice/VoiceViewLayout$3;
.super Ljava/lang/Object;
.source "VoiceViewLayout.java"

# interfaces
.implements Landroid/media/MediaPlayer$OnCompletionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/memo/voice/VoiceViewLayout;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$3;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    .line 1089
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$0(Lcom/samsung/android/app/memo/voice/VoiceViewLayout$3;)Lcom/samsung/android/app/memo/voice/VoiceViewLayout;
    .locals 1

    .prologue
    .line 1089
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$3;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    return-object v0
.end method


# virtual methods
.method public onCompletion(Landroid/media/MediaPlayer;)V
    .locals 4
    .param p1, "mp"    # Landroid/media/MediaPlayer;

    .prologue
    .line 1092
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$3;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    # getter for: Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mVoiceManger:Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;
    invoke-static {v0}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->access$9(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;

    move-result-object v0

    if-nez v0, :cond_0

    .line 1106
    :goto_0
    return-void

    .line 1094
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$3;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    sget-object v1, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$State;->STATE_PLAY_STOP:Lcom/samsung/android/app/memo/voice/VoiceViewLayout$State;

    invoke-static {v0, v1}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->access$2(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;Lcom/samsung/android/app/memo/voice/VoiceViewLayout$State;)V

    .line 1095
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$3;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->access$10(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;Z)V

    .line 1096
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$3;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    invoke-virtual {v0}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->updateCompletionTimerView()V

    .line 1097
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$3;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    # getter for: Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mVoiceManger:Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;
    invoke-static {v0}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->access$9(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->stopSoundPlay()V

    .line 1098
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v1, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$3$1;

    invoke-direct {v1, p0}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$3$1;-><init>(Lcom/samsung/android/app/memo/voice/VoiceViewLayout$3;)V

    .line 1105
    const-wide/16 v2, 0x64

    .line 1098
    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method
