.class public Lcom/samsung/android/app/migration/receiver/VerifySamsungAccountReceiver;
.super Landroid/content/BroadcastReceiver;
.source "VerifySamsungAccountReceiver.java"


# static fields
.field private static final ACTION_EMAIL_VALIDATION_COMPLETED:Ljava/lang/String; = "com.osp.app.signin.action.EMAIL_VALIDATION_COMPLETED"

.field private static final TAG:Ljava/lang/String; = "VerifySamsungAccountReceiver"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 38
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 39
    const-string v0, "VerifySamsungAccountReceiver"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, " received"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 40
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.osp.app.signin.action.EMAIL_VALIDATION_COMPLETED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 41
    invoke-static {p1}, Lcom/samsung/android/app/memo/MemoDataHandler;->updateExistingEntries(Landroid/content/Context;)V

    .line 42
    const/4 v0, 0x1

    invoke-static {p1, v0}, Lcom/samsung/android/app/migration/utils/SharedPref;->updateVerifiedSA(Landroid/content/Context;Z)V

    .line 43
    invoke-static {}, Lcom/samsung/android/app/migration/Migration;->getInstance()Lcom/samsung/android/app/migration/Migration;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 44
    invoke-static {}, Lcom/samsung/android/app/migration/Migration;->getInstance()Lcom/samsung/android/app/migration/Migration;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/app/migration/Migration;->showSyncMemoDialog()V

    .line 47
    :cond_0
    return-void
.end method
