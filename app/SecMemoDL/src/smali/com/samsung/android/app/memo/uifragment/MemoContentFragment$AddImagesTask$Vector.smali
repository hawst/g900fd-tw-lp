.class Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$AddImagesTask$Vector;
.super Ljava/lang/Object;
.source "MemoContentFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$AddImagesTask;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "Vector"
.end annotation


# instance fields
.field private attachmentUri:Landroid/net/Uri;

.field private orientation:I

.field final synthetic this$1:Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$AddImagesTask;


# direct methods
.method private constructor <init>(Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$AddImagesTask;)V
    .locals 0

    .prologue
    .line 788
    iput-object p1, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$AddImagesTask$Vector;->this$1:Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$AddImagesTask;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$AddImagesTask;Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$AddImagesTask$Vector;)V
    .locals 0

    .prologue
    .line 788
    invoke-direct {p0, p1}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$AddImagesTask$Vector;-><init>(Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$AddImagesTask;)V

    return-void
.end method


# virtual methods
.method public getAttachmentUri()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 798
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$AddImagesTask$Vector;->attachmentUri:Landroid/net/Uri;

    return-object v0
.end method

.method public getOrientation()I
    .locals 1

    .prologue
    .line 792
    iget v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$AddImagesTask$Vector;->orientation:I

    return v0
.end method

.method public setAttachmentUri(Landroid/net/Uri;)V
    .locals 0
    .param p1, "attachmentUri"    # Landroid/net/Uri;

    .prologue
    .line 801
    iput-object p1, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$AddImagesTask$Vector;->attachmentUri:Landroid/net/Uri;

    .line 802
    return-void
.end method

.method public setOrientation(I)V
    .locals 0
    .param p1, "orientation"    # I

    .prologue
    .line 795
    iput p1, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$AddImagesTask$Vector;->orientation:I

    .line 796
    return-void
.end method
