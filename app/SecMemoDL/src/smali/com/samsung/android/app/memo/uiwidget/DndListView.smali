.class public Lcom/samsung/android/app/memo/uiwidget/DndListView;
.super Landroid/widget/ListView;
.source "DndListView.java"

# interfaces
.implements Landroid/view/View$OnDragListener;
.implements Landroid/widget/AbsListView$OnScrollListener;
.implements Landroid/widget/AbsListView$RecyclerListener;
.implements Landroid/widget/AdapterView$OnItemLongClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/app/memo/uiwidget/DndListView$OnDndDragAndDropListener;,
        Lcom/samsung/android/app/memo/uiwidget/DndListView$OnDndDragListener;,
        Lcom/samsung/android/app/memo/uiwidget/DndListView$OnMoveCompletedListener;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ListView;",
        "Landroid/widget/AdapterView$OnItemLongClickListener;",
        "Landroid/widget/AbsListView$OnScrollListener;",
        "Landroid/widget/AbsListView$RecyclerListener;",
        "Landroid/view/View$OnDragListener;"
    }
.end annotation


# static fields
.field protected static final ANIMATION_TYPE_CREATE_FOLDER:I = 0x0

.field protected static final ANIMATION_TYPE_MULTI_DELETE:I = 0x2

.field protected static final ANIMATION_TYPE_PASTE:I = 0x1

.field private static final TAG:Ljava/lang/String; = "LibraryListView"

.field private static mSrcDragPos:I


# instance fields
.field private mChangeOrderMode:Z

.field private final mDndContext:Landroid/content/Context;

.field private mDragAndDropListener:Lcom/samsung/android/app/memo/uiwidget/DndListView$OnDndDragAndDropListener;

.field private mDragDestPos:I

.field private mDragDrawable:Lcom/samsung/android/app/memo/uiwidget/animation/AnimateDrawable;

.field private mDragListener:Lcom/samsung/android/app/memo/uiwidget/DndListView$OnDndDragListener;

.field private mDragPos:I

.field private mDragSrcPos:I

.field private mDragX:I

.field private mDragY:I

.field private mDragging:Z

.field private mFirstPos:I

.field private mIsChangedItemInChangeOrder:Z

.field private mItemHeight:I

.field private mItemWidth:I

.field private mLastPos:I

.field private mLowerBound:I

.field private mOffsetXInDraggingItem:I

.field private mOffsetYInDraggingItem:I

.field private mOnMoveCompletedListener:Lcom/samsung/android/app/memo/uiwidget/DndListView$OnMoveCompletedListener;

.field private mOnScrollListener:Landroid/widget/AbsListView$OnScrollListener;

.field private mProcessingAnimation:Z

.field private mRecyclerListener:Landroid/widget/AbsListView$RecyclerListener;

.field private final mScrollRunnable:Ljava/lang/Runnable;

.field private mUpperBound:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 89
    const/4 v0, 0x0

    sput v0, Lcom/samsung/android/app/memo/uiwidget/DndListView;->mSrcDragPos:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v1, 0x0

    .line 148
    invoke-direct {p0, p1, p2}, Landroid/widget/ListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 115
    const/4 v0, -0x1

    iput v0, p0, Lcom/samsung/android/app/memo/uiwidget/DndListView;->mDragSrcPos:I

    .line 121
    iput-boolean v1, p0, Lcom/samsung/android/app/memo/uiwidget/DndListView;->mChangeOrderMode:Z

    .line 123
    iput-boolean v1, p0, Lcom/samsung/android/app/memo/uiwidget/DndListView;->mIsChangedItemInChangeOrder:Z

    .line 133
    new-instance v0, Lcom/samsung/android/app/memo/uiwidget/DndListView$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/app/memo/uiwidget/DndListView$1;-><init>(Lcom/samsung/android/app/memo/uiwidget/DndListView;)V

    iput-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/DndListView;->mScrollRunnable:Ljava/lang/Runnable;

    .line 149
    iput-object p1, p0, Lcom/samsung/android/app/memo/uiwidget/DndListView;->mDndContext:Landroid/content/Context;

    .line 150
    invoke-direct {p0}, Lcom/samsung/android/app/memo/uiwidget/DndListView;->init()V

    .line 151
    invoke-super {p0, p0}, Landroid/widget/ListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 152
    invoke-super {p0, p0}, Landroid/widget/ListView;->setRecyclerListener(Landroid/widget/AbsListView$RecyclerListener;)V

    .line 153
    return-void
.end method

.method static synthetic access$0(Lcom/samsung/android/app/memo/uiwidget/DndListView;)Z
    .locals 1

    .prologue
    .line 533
    invoke-direct {p0}, Lcom/samsung/android/app/memo/uiwidget/DndListView;->doScroll()Z

    move-result v0

    return v0
.end method

.method static synthetic access$1(Lcom/samsung/android/app/memo/uiwidget/DndListView;)Z
    .locals 1

    .prologue
    .line 103
    iget-boolean v0, p0, Lcom/samsung/android/app/memo/uiwidget/DndListView;->mDragging:Z

    return v0
.end method

.method static synthetic access$10(Lcom/samsung/android/app/memo/uiwidget/DndListView;Z)V
    .locals 0

    .prologue
    .line 113
    iput-boolean p1, p0, Lcom/samsung/android/app/memo/uiwidget/DndListView;->mProcessingAnimation:Z

    return-void
.end method

.method static synthetic access$2(Lcom/samsung/android/app/memo/uiwidget/DndListView;)V
    .locals 0

    .prologue
    .line 528
    invoke-direct {p0}, Lcom/samsung/android/app/memo/uiwidget/DndListView;->scroll()V

    return-void
.end method

.method static synthetic access$3(Lcom/samsung/android/app/memo/uiwidget/DndListView;Lcom/samsung/android/app/memo/uiwidget/animation/AnimateDrawable;)V
    .locals 0

    .prologue
    .line 99
    iput-object p1, p0, Lcom/samsung/android/app/memo/uiwidget/DndListView;->mDragDrawable:Lcom/samsung/android/app/memo/uiwidget/animation/AnimateDrawable;

    return-void
.end method

.method static synthetic access$4(Lcom/samsung/android/app/memo/uiwidget/DndListView;)Lcom/samsung/android/app/memo/uiwidget/DndListView$OnMoveCompletedListener;
    .locals 1

    .prologue
    .line 125
    iget-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/DndListView;->mOnMoveCompletedListener:Lcom/samsung/android/app/memo/uiwidget/DndListView$OnMoveCompletedListener;

    return-object v0
.end method

.method static synthetic access$5(Lcom/samsung/android/app/memo/uiwidget/DndListView;)I
    .locals 1

    .prologue
    .line 91
    iget v0, p0, Lcom/samsung/android/app/memo/uiwidget/DndListView;->mDragPos:I

    return v0
.end method

.method static synthetic access$6()I
    .locals 1

    .prologue
    .line 89
    sget v0, Lcom/samsung/android/app/memo/uiwidget/DndListView;->mSrcDragPos:I

    return v0
.end method

.method static synthetic access$7(Lcom/samsung/android/app/memo/uiwidget/DndListView;)V
    .locals 0

    .prologue
    .line 515
    invoke-direct {p0}, Lcom/samsung/android/app/memo/uiwidget/DndListView;->clearChildViewsAnimation()V

    return-void
.end method

.method static synthetic access$8(I)V
    .locals 0

    .prologue
    .line 89
    sput p0, Lcom/samsung/android/app/memo/uiwidget/DndListView;->mSrcDragPos:I

    return-void
.end method

.method static synthetic access$9(Lcom/samsung/android/app/memo/uiwidget/DndListView;)V
    .locals 0

    .prologue
    .line 386
    invoke-direct {p0}, Lcom/samsung/android/app/memo/uiwidget/DndListView;->endProcessingAnimation()V

    return-void
.end method

.method private adjustScrollBounds()V
    .locals 2

    .prologue
    .line 578
    iget v0, p0, Lcom/samsung/android/app/memo/uiwidget/DndListView;->mItemHeight:I

    div-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/samsung/android/app/memo/uiwidget/DndListView;->mUpperBound:I

    .line 579
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uiwidget/DndListView;->getBottom()I

    move-result v0

    iget v1, p0, Lcom/samsung/android/app/memo/uiwidget/DndListView;->mItemHeight:I

    div-int/lit8 v1, v1, 0x2

    sub-int/2addr v0, v1

    iput v0, p0, Lcom/samsung/android/app/memo/uiwidget/DndListView;->mLowerBound:I

    .line 580
    return-void
.end method

.method private clearChildViewsAnimation()V
    .locals 4

    .prologue
    .line 516
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uiwidget/DndListView;->getChildCount()I

    move-result v0

    .line 517
    .local v0, "childCount":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-lt v1, v0, :cond_0

    .line 522
    return-void

    .line 518
    :cond_0
    invoke-virtual {p0, v1}, Lcom/samsung/android/app/memo/uiwidget/DndListView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 519
    .local v2, "localView":Landroid/view/View;
    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    .line 520
    invoke-virtual {v2}, Landroid/view/View;->clearAnimation()V

    .line 517
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method private clearReverseScrollViewAnimation(Landroid/view/View;)V
    .locals 2
    .param p1, "paramView"    # Landroid/view/View;

    .prologue
    .line 645
    sget v0, Lcom/samsung/android/app/memo/uiwidget/DndListView;->mSrcDragPos:I

    iget v1, p0, Lcom/samsung/android/app/memo/uiwidget/DndListView;->mDragPos:I

    if-ge v0, v1, :cond_0

    iget v0, p0, Lcom/samsung/android/app/memo/uiwidget/DndListView;->mLowerBound:I

    iget v1, p0, Lcom/samsung/android/app/memo/uiwidget/DndListView;->mDragY:I

    if-lt v0, v1, :cond_1

    .line 646
    :cond_0
    sget v0, Lcom/samsung/android/app/memo/uiwidget/DndListView;->mSrcDragPos:I

    iget v1, p0, Lcom/samsung/android/app/memo/uiwidget/DndListView;->mDragPos:I

    if-le v0, v1, :cond_2

    iget v0, p0, Lcom/samsung/android/app/memo/uiwidget/DndListView;->mUpperBound:I

    iget v1, p0, Lcom/samsung/android/app/memo/uiwidget/DndListView;->mDragY:I

    if-le v0, v1, :cond_2

    .line 647
    :cond_1
    invoke-virtual {p1}, Landroid/view/View;->clearAnimation()V

    .line 648
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 650
    :cond_2
    return-void
.end method

.method private createAnimation(IIII)Landroid/view/animation/Animation;
    .locals 5
    .param p1, "fromX"    # I
    .param p2, "toX"    # I
    .param p3, "fromY"    # I
    .param p4, "toY"    # I

    .prologue
    .line 525
    new-instance v0, Lcom/samsung/android/app/memo/uiwidget/animation/MoveAnimation;

    int-to-float v1, p1

    int-to-float v2, p2

    int-to-float v3, p3

    int-to-float v4, p4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/samsung/android/app/memo/uiwidget/animation/MoveAnimation;-><init>(FFFF)V

    return-object v0
.end method

.method private doAnimationForDrag(Landroid/graphics/Bitmap;II)V
    .locals 8
    .param p1, "paramBitmap"    # Landroid/graphics/Bitmap;
    .param p2, "x"    # I
    .param p3, "y"    # I

    .prologue
    const/high16 v7, 0x3f800000    # 1.0f

    const/4 v6, 0x0

    .line 313
    iget v4, p0, Lcom/samsung/android/app/memo/uiwidget/DndListView;->mOffsetXInDraggingItem:I

    sub-int v1, p2, v4

    .line 314
    .local v1, "i":I
    iget v4, p0, Lcom/samsung/android/app/memo/uiwidget/DndListView;->mOffsetYInDraggingItem:I

    sub-int v2, p3, v4

    .line 315
    .local v2, "j":I
    new-instance v3, Landroid/graphics/Rect;

    iget v4, p0, Lcom/samsung/android/app/memo/uiwidget/DndListView;->mItemWidth:I

    add-int/2addr v4, v1

    iget v5, p0, Lcom/samsung/android/app/memo/uiwidget/DndListView;->mItemHeight:I

    add-int/2addr v5, v2

    invoke-direct {v3, v1, v2, v4, v5}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 316
    .local v3, "localRect":Landroid/graphics/Rect;
    new-instance v4, Lcom/samsung/android/app/memo/uiwidget/animation/AnimateDrawable;

    new-instance v5, Landroid/graphics/drawable/BitmapDrawable;

    invoke-direct {v5, p1}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/graphics/Bitmap;)V

    invoke-direct {v4, v5}, Lcom/samsung/android/app/memo/uiwidget/animation/AnimateDrawable;-><init>(Landroid/graphics/drawable/Drawable;)V

    iput-object v4, p0, Lcom/samsung/android/app/memo/uiwidget/DndListView;->mDragDrawable:Lcom/samsung/android/app/memo/uiwidget/animation/AnimateDrawable;

    .line 317
    iget-object v4, p0, Lcom/samsung/android/app/memo/uiwidget/DndListView;->mDragDrawable:Lcom/samsung/android/app/memo/uiwidget/animation/AnimateDrawable;

    invoke-virtual {v4}, Lcom/samsung/android/app/memo/uiwidget/animation/AnimateDrawable;->getProxy()Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v4, v3}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    .line 318
    new-instance v0, Lcom/samsung/android/app/memo/uiwidget/animation/MoveAnimation;

    invoke-direct {v0, v6, v6, v6, v6}, Lcom/samsung/android/app/memo/uiwidget/animation/MoveAnimation;-><init>(FFFF)V

    .line 319
    .local v0, "dragAnimation":Lcom/samsung/android/app/memo/uiwidget/animation/MoveAnimation;
    const v4, 0x3f8ccccd    # 1.1f

    invoke-virtual {v0, v7, v7, v7, v4}, Lcom/samsung/android/app/memo/uiwidget/animation/MoveAnimation;->setScale(FFFF)V

    .line 320
    invoke-virtual {v3}, Landroid/graphics/Rect;->centerX()I

    move-result v4

    int-to-float v4, v4

    invoke-virtual {v3}, Landroid/graphics/Rect;->centerY()I

    move-result v5

    int-to-float v5, v5

    invoke-virtual {v0, v4, v5}, Lcom/samsung/android/app/memo/uiwidget/animation/MoveAnimation;->setPivot(FF)V

    .line 321
    const-wide/16 v4, 0x64

    invoke-virtual {v0, v4, v5}, Lcom/samsung/android/app/memo/uiwidget/animation/MoveAnimation;->setDuration(J)V

    .line 322
    const/4 v4, 0x1

    invoke-virtual {v0, v4}, Lcom/samsung/android/app/memo/uiwidget/animation/MoveAnimation;->setFillAfter(Z)V

    .line 323
    new-instance v4, Lcom/samsung/android/app/memo/uiwidget/DndListView$2;

    invoke-direct {v4, p0, v0, v3}, Lcom/samsung/android/app/memo/uiwidget/DndListView$2;-><init>(Lcom/samsung/android/app/memo/uiwidget/DndListView;Lcom/samsung/android/app/memo/uiwidget/animation/MoveAnimation;Landroid/graphics/Rect;)V

    invoke-virtual {v0, v4}, Lcom/samsung/android/app/memo/uiwidget/animation/MoveAnimation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 340
    iget-object v4, p0, Lcom/samsung/android/app/memo/uiwidget/DndListView;->mDragDrawable:Lcom/samsung/android/app/memo/uiwidget/animation/AnimateDrawable;

    invoke-virtual {v4, v0}, Lcom/samsung/android/app/memo/uiwidget/animation/AnimateDrawable;->setAnimation(Landroid/view/animation/Animation;)V

    .line 341
    invoke-virtual {v0}, Lcom/samsung/android/app/memo/uiwidget/animation/MoveAnimation;->startNow()V

    .line 342
    invoke-virtual {p0, v3}, Lcom/samsung/android/app/memo/uiwidget/DndListView;->invalidate(Landroid/graphics/Rect;)V

    .line 343
    return-void
.end method

.method private doAnimationForDrop()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/high16 v6, 0x3f800000    # 1.0f

    .line 346
    iget-object v4, p0, Lcom/samsung/android/app/memo/uiwidget/DndListView;->mDragDrawable:Lcom/samsung/android/app/memo/uiwidget/animation/AnimateDrawable;

    if-eqz v4, :cond_0

    .line 347
    const/4 v2, 0x0

    .line 348
    .local v2, "x":F
    iget v4, p0, Lcom/samsung/android/app/memo/uiwidget/DndListView;->mDragPos:I

    invoke-direct {p0, v4}, Lcom/samsung/android/app/memo/uiwidget/DndListView;->getTop(I)I

    move-result v4

    iget-object v5, p0, Lcom/samsung/android/app/memo/uiwidget/DndListView;->mDragDrawable:Lcom/samsung/android/app/memo/uiwidget/animation/AnimateDrawable;

    invoke-virtual {v5}, Lcom/samsung/android/app/memo/uiwidget/animation/AnimateDrawable;->getProxy()Landroid/graphics/drawable/Drawable;

    move-result-object v5

    invoke-virtual {v5}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v5

    iget v5, v5, Landroid/graphics/Rect;->top:I

    sub-int v3, v4, v5

    .line 349
    .local v3, "y":I
    iget-object v4, p0, Lcom/samsung/android/app/memo/uiwidget/DndListView;->mDragDrawable:Lcom/samsung/android/app/memo/uiwidget/animation/AnimateDrawable;

    invoke-virtual {v4}, Lcom/samsung/android/app/memo/uiwidget/animation/AnimateDrawable;->getProxy()Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v4}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v1

    .line 350
    .local v1, "localRect":Landroid/graphics/Rect;
    new-instance v0, Lcom/samsung/android/app/memo/uiwidget/animation/MoveAnimation;

    int-to-float v4, v3

    invoke-direct {v0, v7, v2, v7, v4}, Lcom/samsung/android/app/memo/uiwidget/animation/MoveAnimation;-><init>(FFFF)V

    .line 351
    .local v0, "dropAnimation":Lcom/samsung/android/app/memo/uiwidget/animation/MoveAnimation;
    const-wide/16 v4, 0x12c

    invoke-virtual {v0, v4, v5}, Lcom/samsung/android/app/memo/uiwidget/animation/MoveAnimation;->setDuration(J)V

    .line 352
    const/4 v4, 0x0

    invoke-virtual {v0, v4}, Lcom/samsung/android/app/memo/uiwidget/animation/MoveAnimation;->setFillAfter(Z)V

    .line 353
    invoke-virtual {v0, v6, v6, v6, v6}, Lcom/samsung/android/app/memo/uiwidget/animation/MoveAnimation;->setScale(FFFF)V

    .line 354
    invoke-virtual {v1}, Landroid/graphics/Rect;->centerX()I

    move-result v4

    int-to-float v4, v4

    invoke-virtual {v1}, Landroid/graphics/Rect;->centerY()I

    move-result v5

    int-to-float v5, v5

    invoke-virtual {v0, v4, v5}, Lcom/samsung/android/app/memo/uiwidget/animation/MoveAnimation;->setPivot(FF)V

    .line 355
    new-instance v4, Lcom/samsung/android/app/memo/uiwidget/DndListView$3;

    invoke-direct {v4, p0}, Lcom/samsung/android/app/memo/uiwidget/DndListView$3;-><init>(Lcom/samsung/android/app/memo/uiwidget/DndListView;)V

    invoke-virtual {v0, v4}, Lcom/samsung/android/app/memo/uiwidget/animation/MoveAnimation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 378
    iget-object v4, p0, Lcom/samsung/android/app/memo/uiwidget/DndListView;->mDragDrawable:Lcom/samsung/android/app/memo/uiwidget/animation/AnimateDrawable;

    invoke-virtual {v4, v0}, Lcom/samsung/android/app/memo/uiwidget/animation/AnimateDrawable;->setAnimation(Landroid/view/animation/Animation;)V

    .line 379
    invoke-virtual {v0}, Lcom/samsung/android/app/memo/uiwidget/animation/MoveAnimation;->startNow()V

    .line 380
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uiwidget/DndListView;->invalidate()V

    .line 384
    .end local v0    # "dropAnimation":Lcom/samsung/android/app/memo/uiwidget/animation/MoveAnimation;
    .end local v1    # "localRect":Landroid/graphics/Rect;
    .end local v2    # "x":F
    .end local v3    # "y":I
    :goto_0
    return-void

    .line 383
    :cond_0
    invoke-direct {p0}, Lcom/samsung/android/app/memo/uiwidget/DndListView;->endProcessingAnimation()V

    goto :goto_0
.end method

.method private doAnimationForMove(II)V
    .locals 5
    .param p1, "dragPos"    # I
    .param p2, "movePos"    # I

    .prologue
    const/4 v4, 0x0

    .line 448
    sget v1, Lcom/samsung/android/app/memo/uiwidget/DndListView;->mSrcDragPos:I

    iget v2, p0, Lcom/samsung/android/app/memo/uiwidget/DndListView;->mDragPos:I

    if-lt v1, v2, :cond_1

    if-ge p1, p2, :cond_1

    .line 449
    iget v1, p0, Lcom/samsung/android/app/memo/uiwidget/DndListView;->mDragPos:I

    add-int/lit8 v1, v1, -0x1

    invoke-direct {p0, v1}, Lcom/samsung/android/app/memo/uiwidget/DndListView;->reverseAnimation(I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 450
    iget v1, p0, Lcom/samsung/android/app/memo/uiwidget/DndListView;->mDragPos:I

    invoke-direct {p0, v1}, Lcom/samsung/android/app/memo/uiwidget/DndListView;->getLeft(I)I

    move-result v1

    iget v2, p0, Lcom/samsung/android/app/memo/uiwidget/DndListView;->mDragPos:I

    add-int/lit8 v2, v2, -0x1

    invoke-direct {p0, v2}, Lcom/samsung/android/app/memo/uiwidget/DndListView;->getLeft(I)I

    move-result v2

    sub-int/2addr v1, v2

    .line 451
    iget v2, p0, Lcom/samsung/android/app/memo/uiwidget/DndListView;->mDragPos:I

    invoke-direct {p0, v2}, Lcom/samsung/android/app/memo/uiwidget/DndListView;->getTop(I)I

    move-result v2

    iget v3, p0, Lcom/samsung/android/app/memo/uiwidget/DndListView;->mDragPos:I

    add-int/lit8 v3, v3, -0x1

    invoke-direct {p0, v3}, Lcom/samsung/android/app/memo/uiwidget/DndListView;->getTop(I)I

    move-result v3

    sub-int/2addr v2, v3

    .line 450
    invoke-direct {p0, v1, v4, v2, v4}, Lcom/samsung/android/app/memo/uiwidget/DndListView;->createAnimation(IIII)Landroid/view/animation/Animation;

    move-result-object v0

    .line 452
    .local v0, "animation":Landroid/view/animation/Animation;
    iget v1, p0, Lcom/samsung/android/app/memo/uiwidget/DndListView;->mDragPos:I

    add-int/lit8 v1, v1, -0x1

    invoke-direct {p0, v1, v0}, Lcom/samsung/android/app/memo/uiwidget/DndListView;->startAnimationByPosition(ILandroid/view/animation/Animation;)V

    .line 471
    .end local v0    # "animation":Landroid/view/animation/Animation;
    :cond_0
    :goto_0
    return-void

    .line 454
    :cond_1
    sget v1, Lcom/samsung/android/app/memo/uiwidget/DndListView;->mSrcDragPos:I

    iget v2, p0, Lcom/samsung/android/app/memo/uiwidget/DndListView;->mDragPos:I

    if-ge v1, v2, :cond_2

    if-ge p1, p2, :cond_2

    .line 455
    iget v1, p0, Lcom/samsung/android/app/memo/uiwidget/DndListView;->mDragPos:I

    invoke-direct {p0, v1}, Lcom/samsung/android/app/memo/uiwidget/DndListView;->reverseAnimation(I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 456
    iget v1, p0, Lcom/samsung/android/app/memo/uiwidget/DndListView;->mDragPos:I

    add-int/lit8 v1, v1, -0x1

    invoke-direct {p0, v1}, Lcom/samsung/android/app/memo/uiwidget/DndListView;->getLeft(I)I

    move-result v1

    iget v2, p0, Lcom/samsung/android/app/memo/uiwidget/DndListView;->mDragPos:I

    invoke-direct {p0, v2}, Lcom/samsung/android/app/memo/uiwidget/DndListView;->getLeft(I)I

    move-result v2

    sub-int/2addr v1, v2

    .line 457
    iget v2, p0, Lcom/samsung/android/app/memo/uiwidget/DndListView;->mDragPos:I

    add-int/lit8 v2, v2, -0x1

    invoke-direct {p0, v2}, Lcom/samsung/android/app/memo/uiwidget/DndListView;->getTop(I)I

    move-result v2

    iget v3, p0, Lcom/samsung/android/app/memo/uiwidget/DndListView;->mDragPos:I

    invoke-direct {p0, v3}, Lcom/samsung/android/app/memo/uiwidget/DndListView;->getTop(I)I

    move-result v3

    sub-int/2addr v2, v3

    .line 456
    invoke-direct {p0, v4, v1, v4, v2}, Lcom/samsung/android/app/memo/uiwidget/DndListView;->createAnimation(IIII)Landroid/view/animation/Animation;

    move-result-object v0

    .line 458
    .restart local v0    # "animation":Landroid/view/animation/Animation;
    iget v1, p0, Lcom/samsung/android/app/memo/uiwidget/DndListView;->mDragPos:I

    invoke-direct {p0, v1, v0}, Lcom/samsung/android/app/memo/uiwidget/DndListView;->startAnimationByPosition(ILandroid/view/animation/Animation;)V

    goto :goto_0

    .line 460
    .end local v0    # "animation":Landroid/view/animation/Animation;
    :cond_2
    sget v1, Lcom/samsung/android/app/memo/uiwidget/DndListView;->mSrcDragPos:I

    iget v2, p0, Lcom/samsung/android/app/memo/uiwidget/DndListView;->mDragPos:I

    if-gt v1, v2, :cond_3

    if-le p1, p2, :cond_3

    .line 461
    iget v1, p0, Lcom/samsung/android/app/memo/uiwidget/DndListView;->mDragPos:I

    add-int/lit8 v1, v1, 0x1

    invoke-direct {p0, v1}, Lcom/samsung/android/app/memo/uiwidget/DndListView;->reverseAnimation(I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 462
    iget v1, p0, Lcom/samsung/android/app/memo/uiwidget/DndListView;->mDragPos:I

    invoke-direct {p0, v1}, Lcom/samsung/android/app/memo/uiwidget/DndListView;->getLeft(I)I

    move-result v1

    iget v2, p0, Lcom/samsung/android/app/memo/uiwidget/DndListView;->mDragPos:I

    add-int/lit8 v2, v2, 0x1

    invoke-direct {p0, v2}, Lcom/samsung/android/app/memo/uiwidget/DndListView;->getLeft(I)I

    move-result v2

    sub-int/2addr v1, v2

    .line 463
    iget v2, p0, Lcom/samsung/android/app/memo/uiwidget/DndListView;->mDragPos:I

    invoke-direct {p0, v2}, Lcom/samsung/android/app/memo/uiwidget/DndListView;->getTop(I)I

    move-result v2

    iget v3, p0, Lcom/samsung/android/app/memo/uiwidget/DndListView;->mDragPos:I

    add-int/lit8 v3, v3, 0x1

    invoke-direct {p0, v3}, Lcom/samsung/android/app/memo/uiwidget/DndListView;->getTop(I)I

    move-result v3

    sub-int/2addr v2, v3

    .line 462
    invoke-direct {p0, v1, v4, v2, v4}, Lcom/samsung/android/app/memo/uiwidget/DndListView;->createAnimation(IIII)Landroid/view/animation/Animation;

    move-result-object v0

    .line 464
    .restart local v0    # "animation":Landroid/view/animation/Animation;
    iget v1, p0, Lcom/samsung/android/app/memo/uiwidget/DndListView;->mDragPos:I

    add-int/lit8 v1, v1, 0x1

    invoke-direct {p0, v1, v0}, Lcom/samsung/android/app/memo/uiwidget/DndListView;->startAnimationByPosition(ILandroid/view/animation/Animation;)V

    goto :goto_0

    .line 467
    .end local v0    # "animation":Landroid/view/animation/Animation;
    :cond_3
    iget v1, p0, Lcom/samsung/android/app/memo/uiwidget/DndListView;->mDragPos:I

    add-int/lit8 v1, v1, 0x1

    invoke-direct {p0, v1}, Lcom/samsung/android/app/memo/uiwidget/DndListView;->getLeft(I)I

    move-result v1

    iget v2, p0, Lcom/samsung/android/app/memo/uiwidget/DndListView;->mDragPos:I

    invoke-direct {p0, v2}, Lcom/samsung/android/app/memo/uiwidget/DndListView;->getLeft(I)I

    move-result v2

    sub-int/2addr v1, v2

    .line 468
    iget v2, p0, Lcom/samsung/android/app/memo/uiwidget/DndListView;->mDragPos:I

    add-int/lit8 v2, v2, 0x1

    invoke-direct {p0, v2}, Lcom/samsung/android/app/memo/uiwidget/DndListView;->getTop(I)I

    move-result v2

    iget v3, p0, Lcom/samsung/android/app/memo/uiwidget/DndListView;->mDragPos:I

    invoke-direct {p0, v3}, Lcom/samsung/android/app/memo/uiwidget/DndListView;->getTop(I)I

    move-result v3

    sub-int/2addr v2, v3

    .line 467
    invoke-direct {p0, v4, v1, v4, v2}, Lcom/samsung/android/app/memo/uiwidget/DndListView;->createAnimation(IIII)Landroid/view/animation/Animation;

    move-result-object v0

    .line 469
    .restart local v0    # "animation":Landroid/view/animation/Animation;
    iget v1, p0, Lcom/samsung/android/app/memo/uiwidget/DndListView;->mDragPos:I

    invoke-direct {p0, v1, v0}, Lcom/samsung/android/app/memo/uiwidget/DndListView;->startAnimationByPosition(ILandroid/view/animation/Animation;)V

    goto/16 :goto_0
.end method

.method private doAnimationForUpdate(I)V
    .locals 2
    .param p1, "movePos"    # I

    .prologue
    .line 429
    iget v0, p0, Lcom/samsung/android/app/memo/uiwidget/DndListView;->mDragPos:I

    .line 430
    .local v0, "i":I
    iget v1, p0, Lcom/samsung/android/app/memo/uiwidget/DndListView;->mDragPos:I

    if-ge v1, p1, :cond_1

    .line 431
    :cond_0
    :goto_0
    iget v1, p0, Lcom/samsung/android/app/memo/uiwidget/DndListView;->mDragPos:I

    if-lt v1, p1, :cond_3

    .line 438
    :cond_1
    iget v1, p0, Lcom/samsung/android/app/memo/uiwidget/DndListView;->mDragPos:I

    if-le v1, p1, :cond_2

    .line 439
    :goto_1
    iget v1, p0, Lcom/samsung/android/app/memo/uiwidget/DndListView;->mDragPos:I

    if-gt v1, p1, :cond_4

    .line 444
    :cond_2
    return-void

    .line 432
    :cond_3
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uiwidget/DndListView;->getCount()I

    move-result v1

    if-ge p1, v1, :cond_0

    .line 433
    iget v1, p0, Lcom/samsung/android/app/memo/uiwidget/DndListView;->mDragPos:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/samsung/android/app/memo/uiwidget/DndListView;->mDragPos:I

    .line 434
    invoke-direct {p0, v0, p1}, Lcom/samsung/android/app/memo/uiwidget/DndListView;->doAnimationForMove(II)V

    goto :goto_0

    .line 440
    :cond_4
    iget v1, p0, Lcom/samsung/android/app/memo/uiwidget/DndListView;->mDragPos:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lcom/samsung/android/app/memo/uiwidget/DndListView;->mDragPos:I

    .line 441
    invoke-direct {p0, v0, p1}, Lcom/samsung/android/app/memo/uiwidget/DndListView;->doAnimationForMove(II)V

    goto :goto_1
.end method

.method private doScroll()Z
    .locals 13

    .prologue
    const/16 v12, 0x10

    const/4 v8, 0x0

    .line 534
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uiwidget/DndListView;->getLastVisiblePosition()I

    move-result v3

    .line 535
    .local v3, "lastVisiblePosition":I
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uiwidget/DndListView;->getFirstVisiblePosition()I

    move-result v1

    .line 536
    .local v1, "firstVisiblePosition":I
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uiwidget/DndListView;->getCount()I

    move-result v0

    .line 537
    .local v0, "count":I
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uiwidget/DndListView;->getListPaddingBottom()I

    move-result v5

    .line 539
    .local v5, "paddingBottom":I
    const/4 v6, 0x0

    .line 540
    .local v6, "ret":Z
    iget v9, p0, Lcom/samsung/android/app/memo/uiwidget/DndListView;->mDragY:I

    iget v10, p0, Lcom/samsung/android/app/memo/uiwidget/DndListView;->mLowerBound:I

    if-le v9, v10, :cond_4

    .line 541
    add-int/lit8 v9, v0, -0x1

    if-ne v3, v9, :cond_1

    .line 542
    sub-int v9, v3, v1

    invoke-virtual {p0, v9}, Lcom/samsung/android/app/memo/uiwidget/DndListView;->getChildAt(I)Landroid/view/View;

    move-result-object v9

    invoke-virtual {v9}, Landroid/view/View;->getBottom()I

    move-result v9

    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uiwidget/DndListView;->getHeight()I

    move-result v10

    .line 543
    sub-int/2addr v10, v5

    if-gt v9, v10, :cond_1

    .line 574
    :cond_0
    :goto_0
    return v8

    .line 546
    :cond_1
    iget v9, p0, Lcom/samsung/android/app/memo/uiwidget/DndListView;->mDragY:I

    iget v10, p0, Lcom/samsung/android/app/memo/uiwidget/DndListView;->mLowerBound:I

    iget v11, p0, Lcom/samsung/android/app/memo/uiwidget/DndListView;->mItemHeight:I

    div-int/lit8 v11, v11, 0x4

    add-int/2addr v10, v11

    if-le v9, v10, :cond_3

    .line 547
    const/16 v7, 0xc

    .line 551
    .local v7, "scrollDistance":I
    :goto_1
    invoke-virtual {p0, v7, v12}, Lcom/samsung/android/app/memo/uiwidget/DndListView;->smoothScrollBy(II)V

    .line 552
    const/4 v6, 0x1

    .line 565
    .end local v7    # "scrollDistance":I
    :cond_2
    :goto_2
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_3
    sub-int v9, v3, v1

    if-lt v2, v9, :cond_7

    move v8, v6

    .line 574
    goto :goto_0

    .line 549
    .end local v2    # "i":I
    :cond_3
    const/4 v7, 0x4

    .restart local v7    # "scrollDistance":I
    goto :goto_1

    .line 553
    .end local v7    # "scrollDistance":I
    :cond_4
    iget v9, p0, Lcom/samsung/android/app/memo/uiwidget/DndListView;->mDragY:I

    iget v10, p0, Lcom/samsung/android/app/memo/uiwidget/DndListView;->mUpperBound:I

    if-ge v9, v10, :cond_2

    .line 554
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uiwidget/DndListView;->getFirstVisiblePosition()I

    move-result v9

    if-nez v9, :cond_5

    invoke-virtual {p0, v8}, Lcom/samsung/android/app/memo/uiwidget/DndListView;->getChildAt(I)Landroid/view/View;

    move-result-object v9

    invoke-virtual {v9}, Landroid/view/View;->getTop()I

    move-result v9

    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uiwidget/DndListView;->getListPaddingTop()I

    move-result v10

    if-ge v9, v10, :cond_0

    .line 557
    :cond_5
    iget v9, p0, Lcom/samsung/android/app/memo/uiwidget/DndListView;->mDragY:I

    iget v10, p0, Lcom/samsung/android/app/memo/uiwidget/DndListView;->mUpperBound:I

    iget v11, p0, Lcom/samsung/android/app/memo/uiwidget/DndListView;->mItemHeight:I

    div-int/lit8 v11, v11, 0x4

    sub-int/2addr v10, v11

    if-ge v9, v10, :cond_6

    .line 558
    const/16 v7, -0xc

    .line 562
    .restart local v7    # "scrollDistance":I
    :goto_4
    invoke-virtual {p0, v7, v12}, Lcom/samsung/android/app/memo/uiwidget/DndListView;->smoothScrollBy(II)V

    .line 563
    const/4 v6, 0x1

    goto :goto_2

    .line 560
    .end local v7    # "scrollDistance":I
    :cond_6
    const/4 v7, -0x4

    .restart local v7    # "scrollDistance":I
    goto :goto_4

    .line 566
    .end local v7    # "scrollDistance":I
    .restart local v2    # "i":I
    :cond_7
    invoke-virtual {p0, v2}, Lcom/samsung/android/app/memo/uiwidget/DndListView;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    .line 567
    .local v4, "localView":Landroid/view/View;
    sget v9, Lcom/samsung/android/app/memo/uiwidget/DndListView;->mSrcDragPos:I

    add-int v10, v2, v1

    if-ne v9, v10, :cond_8

    .line 568
    invoke-virtual {v4}, Landroid/view/View;->clearAnimation()V

    .line 569
    const/4 v9, 0x4

    invoke-virtual {v4, v9}, Landroid/view/View;->setVisibility(I)V

    .line 565
    :goto_5
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 571
    :cond_8
    invoke-virtual {v4, v8}, Landroid/view/View;->setVisibility(I)V

    goto :goto_5
.end method

.method private dragDrawable(II)V
    .locals 6
    .param p1, "x"    # I
    .param p2, "y"    # I

    .prologue
    .line 300
    const/4 v0, 0x0

    .line 301
    .local v0, "i":I
    iget v3, p0, Lcom/samsung/android/app/memo/uiwidget/DndListView;->mOffsetYInDraggingItem:I

    sub-int v1, p2, v3

    .line 302
    .local v1, "j":I
    new-instance v2, Landroid/graphics/Rect;

    iget v3, p0, Lcom/samsung/android/app/memo/uiwidget/DndListView;->mItemWidth:I

    add-int/2addr v3, v0

    iget v4, p0, Lcom/samsung/android/app/memo/uiwidget/DndListView;->mItemHeight:I

    add-int/2addr v4, v1

    invoke-direct {v2, v0, v1, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 303
    .local v2, "localRect":Landroid/graphics/Rect;
    iget-object v3, p0, Lcom/samsung/android/app/memo/uiwidget/DndListView;->mDragDrawable:Lcom/samsung/android/app/memo/uiwidget/animation/AnimateDrawable;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/samsung/android/app/memo/uiwidget/DndListView;->mDragDrawable:Lcom/samsung/android/app/memo/uiwidget/animation/AnimateDrawable;

    invoke-virtual {v3}, Lcom/samsung/android/app/memo/uiwidget/animation/AnimateDrawable;->getProxy()Landroid/graphics/drawable/Drawable;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 304
    iget-object v3, p0, Lcom/samsung/android/app/memo/uiwidget/DndListView;->mDragDrawable:Lcom/samsung/android/app/memo/uiwidget/animation/AnimateDrawable;

    invoke-virtual {v3}, Lcom/samsung/android/app/memo/uiwidget/animation/AnimateDrawable;->getProxy()Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    .line 305
    iget-object v3, p0, Lcom/samsung/android/app/memo/uiwidget/DndListView;->mDragDrawable:Lcom/samsung/android/app/memo/uiwidget/animation/AnimateDrawable;

    invoke-virtual {v3}, Lcom/samsung/android/app/memo/uiwidget/animation/AnimateDrawable;->getAnimation()Landroid/view/animation/Animation;

    move-result-object v3

    check-cast v3, Lcom/samsung/android/app/memo/uiwidget/animation/MoveAnimation;

    invoke-virtual {v2}, Landroid/graphics/Rect;->centerX()I

    move-result v4

    int-to-float v4, v4

    .line 306
    invoke-virtual {v2}, Landroid/graphics/Rect;->centerY()I

    move-result v5

    int-to-float v5, v5

    .line 305
    invoke-virtual {v3, v4, v5}, Lcom/samsung/android/app/memo/uiwidget/animation/MoveAnimation;->setPivot(FF)V

    .line 307
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uiwidget/DndListView;->invalidate()V

    .line 309
    :cond_0
    return-void
.end method

.method private dragDrop()V
    .locals 3

    .prologue
    .line 777
    iget-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/DndListView;->mDragAndDropListener:Lcom/samsung/android/app/memo/uiwidget/DndListView$OnDndDragAndDropListener;

    if-eqz v0, :cond_0

    .line 778
    iget-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/DndListView;->mDragAndDropListener:Lcom/samsung/android/app/memo/uiwidget/DndListView$OnDndDragAndDropListener;

    iget v1, p0, Lcom/samsung/android/app/memo/uiwidget/DndListView;->mDragSrcPos:I

    iget v2, p0, Lcom/samsung/android/app/memo/uiwidget/DndListView;->mDragDestPos:I

    invoke-interface {v0, v1, v2}, Lcom/samsung/android/app/memo/uiwidget/DndListView$OnDndDragAndDropListener;->dragDrop(II)V

    .line 780
    :cond_0
    return-void
.end method

.method private dragExited()V
    .locals 2

    .prologue
    .line 765
    const-string v0, "LibraryListView"

    const-string v1, "dragExited"

    invoke-static {v0, v1}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 767
    const/4 v0, -0x1

    iput v0, p0, Lcom/samsung/android/app/memo/uiwidget/DndListView;->mDragDestPos:I

    .line 769
    iget-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/DndListView;->mDragAndDropListener:Lcom/samsung/android/app/memo/uiwidget/DndListView$OnDndDragAndDropListener;

    if-eqz v0, :cond_0

    .line 770
    iget-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/DndListView;->mDragAndDropListener:Lcom/samsung/android/app/memo/uiwidget/DndListView$OnDndDragAndDropListener;

    invoke-interface {v0}, Lcom/samsung/android/app/memo/uiwidget/DndListView$OnDndDragAndDropListener;->dragDragExited()V

    .line 773
    :cond_0
    return-void
.end method

.method private dragLocation(Landroid/view/DragEvent;)V
    .locals 9
    .param p1, "event"    # Landroid/view/DragEvent;

    .prologue
    const/16 v7, 0x10

    const/4 v8, -0x1

    .line 784
    const/4 v3, 0x0

    .line 786
    .local v3, "scrollDistance":I
    invoke-virtual {p0, v8}, Lcom/samsung/android/app/memo/uiwidget/DndListView;->canScrollVertically(I)Z

    move-result v4

    if-eqz v4, :cond_5

    iget v4, p0, Lcom/samsung/android/app/memo/uiwidget/DndListView;->mDragY:I

    iget v5, p0, Lcom/samsung/android/app/memo/uiwidget/DndListView;->mUpperBound:I

    if-ge v4, v5, :cond_5

    .line 787
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uiwidget/DndListView;->getScrollY()I

    move-result v4

    if-eqz v4, :cond_0

    .line 788
    const/4 v4, 0x0

    invoke-virtual {p0, v4}, Lcom/samsung/android/app/memo/uiwidget/DndListView;->setScrollY(I)V

    .line 791
    :cond_0
    iget v4, p0, Lcom/samsung/android/app/memo/uiwidget/DndListView;->mDragY:I

    iget v5, p0, Lcom/samsung/android/app/memo/uiwidget/DndListView;->mUpperBound:I

    iget v6, p0, Lcom/samsung/android/app/memo/uiwidget/DndListView;->mItemHeight:I

    div-int/lit8 v6, v6, 0x4

    sub-int/2addr v5, v6

    if-ge v4, v5, :cond_4

    .line 792
    const/16 v3, 0x20

    .line 796
    :goto_0
    neg-int v4, v3

    invoke-virtual {p0, v4, v7}, Lcom/samsung/android/app/memo/uiwidget/DndListView;->smoothScrollBy(II)V

    .line 807
    :cond_1
    :goto_1
    const/4 v2, 0x0

    .line 808
    .local v2, "position":I
    invoke-virtual {p1}, Landroid/view/DragEvent;->getX()F

    move-result v4

    float-to-int v4, v4

    iput v4, p0, Lcom/samsung/android/app/memo/uiwidget/DndListView;->mDragX:I

    .line 809
    invoke-virtual {p1}, Landroid/view/DragEvent;->getY()F

    move-result v4

    float-to-int v4, v4

    iput v4, p0, Lcom/samsung/android/app/memo/uiwidget/DndListView;->mDragY:I

    .line 811
    iget v4, p0, Lcom/samsung/android/app/memo/uiwidget/DndListView;->mDragX:I

    iget v5, p0, Lcom/samsung/android/app/memo/uiwidget/DndListView;->mDragY:I

    invoke-virtual {p0, v4, v5}, Lcom/samsung/android/app/memo/uiwidget/DndListView;->pointToPosition(II)I

    move-result v2

    .line 813
    if-eq v2, v8, :cond_7

    .line 814
    iget v4, p0, Lcom/samsung/android/app/memo/uiwidget/DndListView;->mDragDestPos:I

    if-eq v4, v2, :cond_2

    .line 815
    iget-object v4, p0, Lcom/samsung/android/app/memo/uiwidget/DndListView;->mDragAndDropListener:Lcom/samsung/android/app/memo/uiwidget/DndListView$OnDndDragAndDropListener;

    if-eqz v4, :cond_2

    .line 816
    iget-object v4, p0, Lcom/samsung/android/app/memo/uiwidget/DndListView;->mDragAndDropListener:Lcom/samsung/android/app/memo/uiwidget/DndListView$OnDndDragAndDropListener;

    iget v5, p0, Lcom/samsung/android/app/memo/uiwidget/DndListView;->mDragDestPos:I

    iget v6, p0, Lcom/samsung/android/app/memo/uiwidget/DndListView;->mDragX:I

    iget v7, p0, Lcom/samsung/android/app/memo/uiwidget/DndListView;->mDragY:I

    invoke-interface {v4, v5, v2, v6, v7}, Lcom/samsung/android/app/memo/uiwidget/DndListView$OnDndDragAndDropListener;->dragLocation(IIII)V

    .line 820
    :cond_2
    iput v2, p0, Lcom/samsung/android/app/memo/uiwidget/DndListView;->mDragDestPos:I

    .line 837
    :cond_3
    :goto_2
    return-void

    .line 794
    .end local v2    # "position":I
    :cond_4
    const/16 v3, 0x10

    goto :goto_0

    .line 797
    :cond_5
    const/4 v4, 0x1

    invoke-virtual {p0, v4}, Lcom/samsung/android/app/memo/uiwidget/DndListView;->canScrollVertically(I)Z

    move-result v4

    if-eqz v4, :cond_1

    iget v4, p0, Lcom/samsung/android/app/memo/uiwidget/DndListView;->mDragY:I

    iget v5, p0, Lcom/samsung/android/app/memo/uiwidget/DndListView;->mLowerBound:I

    if-le v4, v5, :cond_1

    .line 799
    iget v4, p0, Lcom/samsung/android/app/memo/uiwidget/DndListView;->mDragY:I

    iget v5, p0, Lcom/samsung/android/app/memo/uiwidget/DndListView;->mLowerBound:I

    iget v6, p0, Lcom/samsung/android/app/memo/uiwidget/DndListView;->mItemHeight:I

    div-int/lit8 v6, v6, 0x4

    sub-int/2addr v5, v6

    if-le v4, v5, :cond_6

    .line 800
    const/16 v3, 0x20

    .line 804
    :goto_3
    invoke-virtual {p0, v3, v7}, Lcom/samsung/android/app/memo/uiwidget/DndListView;->smoothScrollBy(II)V

    goto :goto_1

    .line 802
    :cond_6
    const/16 v3, 0x10

    goto :goto_3

    .line 822
    .restart local v2    # "position":I
    :cond_7
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uiwidget/DndListView;->getLastVisiblePosition()I

    move-result v4

    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uiwidget/DndListView;->getFirstVisiblePosition()I

    move-result v5

    sub-int/2addr v4, v5

    invoke-virtual {p0, v4}, Lcom/samsung/android/app/memo/uiwidget/DndListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 824
    .local v0, "lastChild":Landroid/view/View;
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    .line 825
    .local v1, "lastChildRect":Landroid/graphics/Rect;
    invoke-virtual {v0, v1}, Landroid/view/View;->getHitRect(Landroid/graphics/Rect;)V

    .line 827
    iget v4, p0, Lcom/samsung/android/app/memo/uiwidget/DndListView;->mDragY:I

    iget v5, v1, Landroid/graphics/Rect;->bottom:I

    if-gt v4, v5, :cond_8

    .line 828
    iget v4, p0, Lcom/samsung/android/app/memo/uiwidget/DndListView;->mDragX:I

    iget v5, v1, Landroid/graphics/Rect;->right:I

    if-le v4, v5, :cond_3

    iget v4, p0, Lcom/samsung/android/app/memo/uiwidget/DndListView;->mDragY:I

    iget v5, v1, Landroid/graphics/Rect;->top:I

    if-le v4, v5, :cond_3

    iget v4, p0, Lcom/samsung/android/app/memo/uiwidget/DndListView;->mDragY:I

    iget v5, v1, Landroid/graphics/Rect;->bottom:I

    if-ge v4, v5, :cond_3

    .line 830
    :cond_8
    iget-object v4, p0, Lcom/samsung/android/app/memo/uiwidget/DndListView;->mDragAndDropListener:Lcom/samsung/android/app/memo/uiwidget/DndListView$OnDndDragAndDropListener;

    iget v5, p0, Lcom/samsung/android/app/memo/uiwidget/DndListView;->mDragDestPos:I

    iget v6, p0, Lcom/samsung/android/app/memo/uiwidget/DndListView;->mDragX:I

    .line 831
    iget v7, p0, Lcom/samsung/android/app/memo/uiwidget/DndListView;->mDragY:I

    .line 830
    invoke-interface {v4, v5, v8, v6, v7}, Lcom/samsung/android/app/memo/uiwidget/DndListView$OnDndDragAndDropListener;->dragLocation(IIII)V

    .line 833
    iput v8, p0, Lcom/samsung/android/app/memo/uiwidget/DndListView;->mDragDestPos:I

    goto :goto_2
.end method

.method private dragStart()V
    .locals 3

    .prologue
    .line 749
    const-string v1, "LibraryListView"

    const-string v2, "dragStart"

    invoke-static {v1, v2}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 751
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/samsung/android/app/memo/uiwidget/DndListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 752
    .local v0, "firstView":Landroid/view/View;
    if-eqz v0, :cond_0

    .line 753
    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v1

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v2

    invoke-virtual {p0, v1, v2}, Lcom/samsung/android/app/memo/uiwidget/DndListView;->setItemSize(II)V

    .line 756
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/app/memo/uiwidget/DndListView;->mDragAndDropListener:Lcom/samsung/android/app/memo/uiwidget/DndListView$OnDndDragAndDropListener;

    if-eqz v1, :cond_1

    .line 757
    iget-object v1, p0, Lcom/samsung/android/app/memo/uiwidget/DndListView;->mDragAndDropListener:Lcom/samsung/android/app/memo/uiwidget/DndListView$OnDndDragAndDropListener;

    iget v2, p0, Lcom/samsung/android/app/memo/uiwidget/DndListView;->mDragSrcPos:I

    invoke-interface {v1, v2}, Lcom/samsung/android/app/memo/uiwidget/DndListView$OnDndDragAndDropListener;->dragStarted(I)V

    .line 760
    :cond_1
    invoke-direct {p0}, Lcom/samsung/android/app/memo/uiwidget/DndListView;->adjustScrollBounds()V

    .line 761
    return-void
.end method

.method private endProcessingAnimation()V
    .locals 4

    .prologue
    .line 387
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v1, Lcom/samsung/android/app/memo/uiwidget/DndListView$4;

    invoke-direct {v1, p0}, Lcom/samsung/android/app/memo/uiwidget/DndListView$4;-><init>(Lcom/samsung/android/app/memo/uiwidget/DndListView;)V

    .line 392
    const-wide/16 v2, 0x64

    .line 387
    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 393
    return-void
.end method

.method private getChangeorderMode()Z
    .locals 1

    .prologue
    .line 680
    iget-boolean v0, p0, Lcom/samsung/android/app/memo/uiwidget/DndListView;->mChangeOrderMode:Z

    return v0
.end method

.method private getItemForPosition(II)I
    .locals 8
    .param p1, "x"    # I
    .param p2, "y"    # I

    .prologue
    .line 409
    move v4, p1

    .line 410
    .local v4, "xPos":I
    iget v6, p0, Lcom/samsung/android/app/memo/uiwidget/DndListView;->mOffsetYInDraggingItem:I

    sub-int v6, p2, v6

    iget v7, p0, Lcom/samsung/android/app/memo/uiwidget/DndListView;->mItemHeight:I

    div-int/lit8 v7, v7, 0x2

    add-int v5, v6, v7

    .line 412
    .local v5, "yPos":I
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uiwidget/DndListView;->getChildCount()I

    move-result v0

    .line 413
    .local v0, "childCount":I
    const/4 v3, 0x0

    .local v3, "pos":I
    :goto_0
    if-lt v3, v0, :cond_1

    .line 422
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uiwidget/DndListView;->getChildCount()I

    move-result v6

    if-lt v3, v6, :cond_2

    .line 423
    iget v6, p0, Lcom/samsung/android/app/memo/uiwidget/DndListView;->mDragPos:I

    .line 425
    :goto_1
    return v6

    .line 414
    :cond_1
    invoke-virtual {p0, v3}, Lcom/samsung/android/app/memo/uiwidget/DndListView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 416
    .local v1, "itemView":Landroid/view/View;
    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    .line 417
    .local v2, "localRect":Landroid/graphics/Rect;
    invoke-virtual {v1, v2}, Landroid/view/View;->getHitRect(Landroid/graphics/Rect;)V

    .line 418
    invoke-virtual {v2, v4, v5}, Landroid/graphics/Rect;->contains(II)Z

    move-result v6

    if-nez v6, :cond_0

    .line 413
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 425
    .end local v1    # "itemView":Landroid/view/View;
    .end local v2    # "localRect":Landroid/graphics/Rect;
    :cond_2
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uiwidget/DndListView;->getFirstVisiblePosition()I

    move-result v6

    add-int/2addr v6, v3

    goto :goto_1
.end method

.method private getLeft(I)I
    .locals 1
    .param p1, "itemPos"    # I

    .prologue
    .line 396
    const/4 v0, 0x0

    return v0
.end method

.method private getTop(I)I
    .locals 6
    .param p1, "itemPos"    # I

    .prologue
    .line 400
    const/4 v3, 0x0

    invoke-virtual {p0, v3}, Lcom/samsung/android/app/memo/uiwidget/DndListView;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/View;->getTop()I

    move-result v0

    .line 401
    .local v0, "firstRowTop":I
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uiwidget/DndListView;->getFirstVisiblePosition()I

    move-result v3

    sub-int v1, p1, v3

    .line 402
    .local v1, "row":I
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uiwidget/DndListView;->getFirstVisiblePosition()I

    move-result v3

    sub-int v3, p1, v3

    .line 403
    iget-object v4, p0, Lcom/samsung/android/app/memo/uiwidget/DndListView;->mDndContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f090038

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v4

    float-to-int v4, v4

    .line 402
    mul-int v2, v3, v4

    .line 405
    .local v2, "seperateLineHeight":I
    iget v3, p0, Lcom/samsung/android/app/memo/uiwidget/DndListView;->mItemHeight:I

    mul-int/2addr v3, v1

    add-int/2addr v3, v0

    add-int/2addr v3, v2

    return v3
.end method

.method private init()V
    .locals 1

    .prologue
    .line 156
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/app/memo/uiwidget/DndListView;->mProcessingAnimation:Z

    .line 157
    invoke-virtual {p0, p0}, Lcom/samsung/android/app/memo/uiwidget/DndListView;->setOnDragListener(Landroid/view/View$OnDragListener;)V

    .line 158
    invoke-virtual {p0, p0}, Lcom/samsung/android/app/memo/uiwidget/DndListView;->setOnItemLongClickListener(Landroid/widget/AdapterView$OnItemLongClickListener;)V

    .line 159
    return-void
.end method

.method private reverseAnimation(I)Z
    .locals 3
    .param p1, "paramInt"    # I

    .prologue
    .line 481
    const/4 v1, 0x0

    .line 482
    .local v1, "ret":Z
    const/4 v0, 0x0

    .line 483
    .local v0, "moveAnimation":Landroid/view/animation/Animation;
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uiwidget/DndListView;->getFirstVisiblePosition()I

    move-result v2

    sub-int v2, p1, v2

    invoke-virtual {p0, v2}, Lcom/samsung/android/app/memo/uiwidget/DndListView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 484
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uiwidget/DndListView;->getFirstVisiblePosition()I

    move-result v2

    sub-int v2, p1, v2

    invoke-virtual {p0, v2}, Lcom/samsung/android/app/memo/uiwidget/DndListView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getAnimation()Landroid/view/animation/Animation;

    move-result-object v0

    .line 485
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/view/animation/Animation;->hasStarted()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v0}, Landroid/view/animation/Animation;->hasEnded()Z

    move-result v2

    if-nez v2, :cond_0

    move-object v2, v0

    .line 486
    check-cast v2, Lcom/samsung/android/app/memo/uiwidget/animation/MoveAnimation;

    invoke-virtual {v2}, Lcom/samsung/android/app/memo/uiwidget/animation/MoveAnimation;->reverseAnimation()V

    .line 487
    const/4 v1, 0x1

    .line 490
    :cond_0
    return v1
.end method

.method private reverseScroll(II)V
    .locals 10
    .param p1, "firstVisibleItem"    # I
    .param p2, "lastVisibleItem"    # I

    .prologue
    const-wide/16 v8, 0x0

    const/4 v7, 0x0

    .line 594
    iget v0, p0, Lcom/samsung/android/app/memo/uiwidget/DndListView;->mFirstPos:I

    .line 595
    .local v0, "i":I
    iput p1, p0, Lcom/samsung/android/app/memo/uiwidget/DndListView;->mFirstPos:I

    .line 596
    iget v1, p0, Lcom/samsung/android/app/memo/uiwidget/DndListView;->mLastPos:I

    .line 597
    .local v1, "j":I
    iput p2, p0, Lcom/samsung/android/app/memo/uiwidget/DndListView;->mLastPos:I

    .line 598
    iget v5, p0, Lcom/samsung/android/app/memo/uiwidget/DndListView;->mDragX:I

    iget v6, p0, Lcom/samsung/android/app/memo/uiwidget/DndListView;->mDragY:I

    invoke-direct {p0, v5, v6}, Lcom/samsung/android/app/memo/uiwidget/DndListView;->getItemForPosition(II)I

    move-result v2

    .line 600
    .local v2, "k":I
    sget v5, Lcom/samsung/android/app/memo/uiwidget/DndListView;->mSrcDragPos:I

    iget v6, p0, Lcom/samsung/android/app/memo/uiwidget/DndListView;->mDragPos:I

    if-ge v5, v6, :cond_2

    if-ge p1, v0, :cond_2

    .line 601
    iget v5, p0, Lcom/samsung/android/app/memo/uiwidget/DndListView;->mItemHeight:I

    neg-int v5, v5

    invoke-direct {p0, v7, v7, v7, v5}, Lcom/samsung/android/app/memo/uiwidget/DndListView;->createAnimation(IIII)Landroid/view/animation/Animation;

    move-result-object v4

    .line 602
    .local v4, "localAnimation2":Landroid/view/animation/Animation;
    invoke-virtual {v4, v8, v9}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 603
    invoke-direct {p0, p1, v4}, Lcom/samsung/android/app/memo/uiwidget/DndListView;->startAnimationByPosition(ILandroid/view/animation/Animation;)V

    .line 610
    .end local v4    # "localAnimation2":Landroid/view/animation/Animation;
    :cond_0
    :goto_0
    iget v5, p0, Lcom/samsung/android/app/memo/uiwidget/DndListView;->mDragPos:I

    if-eq v5, v2, :cond_1

    .line 611
    invoke-direct {p0, v2}, Lcom/samsung/android/app/memo/uiwidget/DndListView;->doAnimationForUpdate(I)V

    .line 613
    :cond_1
    return-void

    .line 604
    :cond_2
    sget v5, Lcom/samsung/android/app/memo/uiwidget/DndListView;->mSrcDragPos:I

    iget v6, p0, Lcom/samsung/android/app/memo/uiwidget/DndListView;->mDragPos:I

    if-le v5, v6, :cond_0

    if-le p2, v1, :cond_0

    .line 605
    iget v5, p0, Lcom/samsung/android/app/memo/uiwidget/DndListView;->mItemHeight:I

    invoke-direct {p0, v7, v7, v7, v5}, Lcom/samsung/android/app/memo/uiwidget/DndListView;->createAnimation(IIII)Landroid/view/animation/Animation;

    move-result-object v3

    .line 606
    .local v3, "localAnimation1":Landroid/view/animation/Animation;
    invoke-virtual {v3, v8, v9}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 607
    invoke-direct {p0, p2, v3}, Lcom/samsung/android/app/memo/uiwidget/DndListView;->startAnimationByPosition(ILandroid/view/animation/Animation;)V

    goto :goto_0
.end method

.method private scroll()V
    .locals 1

    .prologue
    .line 529
    iget-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/DndListView;->mScrollRunnable:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/memo/uiwidget/DndListView;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 530
    iget-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/DndListView;->mScrollRunnable:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/memo/uiwidget/DndListView;->post(Ljava/lang/Runnable;)Z

    .line 531
    return-void
.end method

.method public static setSrcDragPos(I)V
    .locals 0
    .param p0, "srcDragPos"    # I

    .prologue
    .line 238
    sput p0, Lcom/samsung/android/app/memo/uiwidget/DndListView;->mSrcDragPos:I

    .line 239
    return-void
.end method

.method private startAnimationByPosition(ILandroid/view/animation/Animation;)V
    .locals 2
    .param p1, "pos"    # I
    .param p2, "animation"    # Landroid/view/animation/Animation;

    .prologue
    .line 474
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uiwidget/DndListView;->getFirstVisiblePosition()I

    move-result v1

    sub-int v1, p1, v1

    invoke-virtual {p0, v1}, Lcom/samsung/android/app/memo/uiwidget/DndListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 475
    .local v0, "localView":Landroid/view/View;
    if-eqz v0, :cond_0

    .line 476
    invoke-virtual {v0, p2}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 478
    :cond_0
    return-void
.end method


# virtual methods
.method protected dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 1
    .param p1, "paramCanvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 505
    invoke-super {p0, p1}, Landroid/widget/ListView;->dispatchDraw(Landroid/graphics/Canvas;)V

    .line 506
    iget-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/DndListView;->mDragDrawable:Lcom/samsung/android/app/memo/uiwidget/animation/AnimateDrawable;

    if-eqz v0, :cond_0

    .line 507
    iget-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/DndListView;->mDragDrawable:Lcom/samsung/android/app/memo/uiwidget/animation/AnimateDrawable;

    invoke-virtual {v0, p1}, Lcom/samsung/android/app/memo/uiwidget/animation/AnimateDrawable;->draw(Landroid/graphics/Canvas;)V

    .line 508
    iget-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/DndListView;->mDragDrawable:Lcom/samsung/android/app/memo/uiwidget/animation/AnimateDrawable;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/DndListView;->mDragDrawable:Lcom/samsung/android/app/memo/uiwidget/animation/AnimateDrawable;

    invoke-virtual {v0}, Lcom/samsung/android/app/memo/uiwidget/animation/AnimateDrawable;->hasStarted()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 509
    iget-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/DndListView;->mDragDrawable:Lcom/samsung/android/app/memo/uiwidget/animation/AnimateDrawable;

    invoke-virtual {v0}, Lcom/samsung/android/app/memo/uiwidget/animation/AnimateDrawable;->hasEnded()Z

    move-result v0

    if-nez v0, :cond_0

    .line 510
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uiwidget/DndListView;->invalidate()V

    .line 513
    :cond_0
    return-void
.end method

.method public getDragDrawable()Lcom/samsung/android/app/memo/uiwidget/animation/AnimateDrawable;
    .locals 1

    .prologue
    .line 841
    iget-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/DndListView;->mDragDrawable:Lcom/samsung/android/app/memo/uiwidget/animation/AnimateDrawable;

    return-object v0
.end method

.method public getIsChagnedItemInChangeOrder()Z
    .locals 1

    .prologue
    .line 688
    iget-boolean v0, p0, Lcom/samsung/android/app/memo/uiwidget/DndListView;->mIsChangedItemInChangeOrder:Z

    return v0
.end method

.method public getProcessingAnimation()Z
    .locals 1

    .prologue
    .line 234
    iget-boolean v0, p0, Lcom/samsung/android/app/memo/uiwidget/DndListView;->mProcessingAnimation:Z

    return v0
.end method

.method public onDrag(Landroid/view/View;Landroid/view/DragEvent;)Z
    .locals 3
    .param p1, "view"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/DragEvent;

    .prologue
    const/4 v0, 0x1

    .line 699
    const-string v1, "LibraryListView"

    const-string v2, "onDrag"

    invoke-static {v1, v2}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 701
    invoke-virtual {p2}, Landroid/view/DragEvent;->getAction()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 745
    :cond_0
    :goto_0
    return v0

    .line 703
    :pswitch_0
    iget v1, p0, Lcom/samsung/android/app/memo/uiwidget/DndListView;->mDragSrcPos:I

    if-ltz v1, :cond_0

    .line 706
    invoke-direct {p0}, Lcom/samsung/android/app/memo/uiwidget/DndListView;->dragStart()V

    goto :goto_0

    .line 711
    :pswitch_1
    iget v1, p0, Lcom/samsung/android/app/memo/uiwidget/DndListView;->mDragSrcPos:I

    if-gez v1, :cond_1

    .line 712
    const/4 v0, 0x0

    goto :goto_0

    .line 714
    :cond_1
    invoke-direct {p0}, Lcom/samsung/android/app/memo/uiwidget/DndListView;->dragDrop()V

    goto :goto_0

    .line 718
    :pswitch_2
    iget v1, p0, Lcom/samsung/android/app/memo/uiwidget/DndListView;->mDragSrcPos:I

    if-ltz v1, :cond_0

    .line 721
    invoke-direct {p0}, Lcom/samsung/android/app/memo/uiwidget/DndListView;->dragExited()V

    goto :goto_0

    .line 725
    :pswitch_3
    const/4 v1, -0x1

    iput v1, p0, Lcom/samsung/android/app/memo/uiwidget/DndListView;->mDragSrcPos:I

    goto :goto_0

    .line 729
    :pswitch_4
    iget v1, p0, Lcom/samsung/android/app/memo/uiwidget/DndListView;->mDragSrcPos:I

    if-ltz v1, :cond_0

    .line 732
    invoke-direct {p0, p2}, Lcom/samsung/android/app/memo/uiwidget/DndListView;->dragLocation(Landroid/view/DragEvent;)V

    goto :goto_0

    .line 736
    :pswitch_5
    iget v1, p0, Lcom/samsung/android/app/memo/uiwidget/DndListView;->mDragSrcPos:I

    if-gez v1, :cond_0

    goto :goto_0

    .line 701
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_4
        :pswitch_1
        :pswitch_3
        :pswitch_5
        :pswitch_2
    .end packed-switch
.end method

.method public onItemLongClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)Z
    .locals 3
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "arg3"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)Z"
        }
    .end annotation

    .prologue
    .line 277
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    const-string v1, "LibraryListView"

    const-string v2, "onItemLongClick"

    invoke-static {v1, v2}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 278
    const/4 v0, 0x1

    .line 280
    .local v0, "ret":Z
    iget-object v1, p0, Lcom/samsung/android/app/memo/uiwidget/DndListView;->mDragAndDropListener:Lcom/samsung/android/app/memo/uiwidget/DndListView$OnDndDragAndDropListener;

    if-eqz v1, :cond_0

    .line 281
    iget-object v1, p0, Lcom/samsung/android/app/memo/uiwidget/DndListView;->mDragAndDropListener:Lcom/samsung/android/app/memo/uiwidget/DndListView$OnDndDragAndDropListener;

    invoke-interface {v1, p3}, Lcom/samsung/android/app/memo/uiwidget/DndListView$OnDndDragAndDropListener;->dragStarted(I)V

    .line 290
    :cond_0
    const/4 v1, 0x0

    invoke-virtual {p2, v1}, Landroid/view/View;->setPressed(Z)V

    .line 293
    iput p3, p0, Lcom/samsung/android/app/memo/uiwidget/DndListView;->mDragDestPos:I

    iput p3, p0, Lcom/samsung/android/app/memo/uiwidget/DndListView;->mDragSrcPos:I

    .line 294
    const/4 v0, 0x1

    .line 296
    return v0
.end method

.method public onMovedToScrapHeap(Landroid/view/View;)V
    .locals 1
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 654
    invoke-direct {p0, p1}, Lcom/samsung/android/app/memo/uiwidget/DndListView;->clearReverseScrollViewAnimation(Landroid/view/View;)V

    .line 655
    iget-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/DndListView;->mRecyclerListener:Landroid/widget/AbsListView$RecyclerListener;

    if-eqz v0, :cond_0

    .line 656
    iget-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/DndListView;->mRecyclerListener:Landroid/widget/AbsListView$RecyclerListener;

    invoke-interface {v0, p1}, Landroid/widget/AbsListView$RecyclerListener;->onMovedToScrapHeap(Landroid/view/View;)V

    .line 658
    :cond_0
    return-void
.end method

.method public onScroll(Landroid/widget/AbsListView;III)V
    .locals 1
    .param p1, "view"    # Landroid/widget/AbsListView;
    .param p2, "firstVisibleItem"    # I
    .param p3, "visibleItemCount"    # I
    .param p4, "totalItemCount"    # I

    .prologue
    .line 585
    iget-boolean v0, p0, Lcom/samsung/android/app/memo/uiwidget/DndListView;->mDragging:Z

    if-eqz v0, :cond_0

    .line 586
    add-int v0, p2, p3

    add-int/lit8 v0, v0, -0x1

    invoke-direct {p0, p2, v0}, Lcom/samsung/android/app/memo/uiwidget/DndListView;->reverseScroll(II)V

    .line 588
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/DndListView;->mOnScrollListener:Landroid/widget/AbsListView$OnScrollListener;

    if-eqz v0, :cond_1

    .line 589
    iget-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/DndListView;->mOnScrollListener:Landroid/widget/AbsListView$OnScrollListener;

    invoke-interface {v0, p1, p2, p3, p4}, Landroid/widget/AbsListView$OnScrollListener;->onScroll(Landroid/widget/AbsListView;III)V

    .line 591
    :cond_1
    return-void
.end method

.method public onScrollStateChanged(Landroid/widget/AbsListView;I)V
    .locals 1
    .param p1, "arg0"    # Landroid/widget/AbsListView;
    .param p2, "arg1"    # I

    .prologue
    .line 617
    iget-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/DndListView;->mOnScrollListener:Landroid/widget/AbsListView$OnScrollListener;

    if-eqz v0, :cond_0

    .line 618
    iget-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/DndListView;->mOnScrollListener:Landroid/widget/AbsListView$OnScrollListener;

    invoke-interface {v0, p1, p2}, Landroid/widget/AbsListView$OnScrollListener;->onScrollStateChanged(Landroid/widget/AbsListView;I)V

    .line 620
    :cond_0
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 12
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v8, 0x0

    const/4 v9, 0x1

    .line 164
    iget-boolean v10, p0, Lcom/samsung/android/app/memo/uiwidget/DndListView;->mProcessingAnimation:Z

    if-eqz v10, :cond_1

    .line 226
    :cond_0
    :goto_0
    return v8

    .line 167
    :cond_1
    iget-boolean v10, p0, Lcom/samsung/android/app/memo/uiwidget/DndListView;->mDragging:Z

    if-eqz v10, :cond_5

    .line 169
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v10

    packed-switch v10, :pswitch_data_0

    :cond_2
    :goto_1
    move v8, v9

    .line 223
    goto :goto_0

    .line 171
    :pswitch_0
    iget-object v10, p0, Lcom/samsung/android/app/memo/uiwidget/DndListView;->mDragListener:Lcom/samsung/android/app/memo/uiwidget/DndListView$OnDndDragListener;

    if-eqz v10, :cond_3

    .line 172
    iget-object v10, p0, Lcom/samsung/android/app/memo/uiwidget/DndListView;->mDragListener:Lcom/samsung/android/app/memo/uiwidget/DndListView$OnDndDragListener;

    invoke-interface {v10}, Lcom/samsung/android/app/memo/uiwidget/DndListView$OnDndDragListener;->startDrag()V

    .line 174
    :cond_3
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v10

    float-to-int v6, v10

    .line 175
    .local v6, "x":I
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v10

    float-to-int v7, v10

    .line 176
    .local v7, "y":I
    invoke-virtual {p0, v6, v7}, Lcom/samsung/android/app/memo/uiwidget/DndListView;->pointToPosition(II)I

    move-result v5

    .line 177
    .local v5, "pos":I
    const/4 v10, -0x1

    if-le v5, v10, :cond_0

    .line 180
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uiwidget/DndListView;->getFirstVisiblePosition()I

    move-result v10

    sub-int v10, v5, v10

    invoke-virtual {p0, v10}, Lcom/samsung/android/app/memo/uiwidget/DndListView;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 181
    .local v3, "localView":Landroid/view/View;
    invoke-virtual {v3}, Landroid/view/View;->getWidth()I

    move-result v10

    invoke-virtual {v3}, Landroid/view/View;->getHeight()I

    move-result v11

    invoke-virtual {p0, v10, v11}, Lcom/samsung/android/app/memo/uiwidget/DndListView;->setItemSize(II)V

    .line 182
    invoke-virtual {p0, v9}, Lcom/samsung/android/app/memo/uiwidget/DndListView;->setDragging(Z)V

    .line 183
    invoke-virtual {p0, v6}, Lcom/samsung/android/app/memo/uiwidget/DndListView;->setDragX(I)V

    .line 184
    invoke-virtual {p0, v7}, Lcom/samsung/android/app/memo/uiwidget/DndListView;->setDragY(I)V

    .line 185
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uiwidget/DndListView;->getFirstVisiblePosition()I

    move-result v10

    invoke-virtual {p0, v10}, Lcom/samsung/android/app/memo/uiwidget/DndListView;->setFirstPos(I)V

    .line 186
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uiwidget/DndListView;->getLastVisiblePosition()I

    move-result v10

    invoke-virtual {p0, v10}, Lcom/samsung/android/app/memo/uiwidget/DndListView;->setLastPos(I)V

    .line 187
    invoke-virtual {v3}, Landroid/view/View;->getLeft()I

    move-result v10

    sub-int v10, v6, v10

    invoke-virtual {v3}, Landroid/view/View;->getTop()I

    move-result v11

    sub-int v11, v7, v11

    invoke-virtual {p0, v10, v11}, Lcom/samsung/android/app/memo/uiwidget/DndListView;->setOffsetInDraggingItem(II)V

    .line 189
    invoke-virtual {v3, v9}, Landroid/view/View;->setDrawingCacheEnabled(Z)V

    .line 190
    invoke-virtual {v3, v9}, Landroid/view/View;->buildDrawingCache(Z)V

    .line 191
    invoke-virtual {v3}, Landroid/view/View;->getDrawingCache()Landroid/graphics/Bitmap;

    move-result-object v10

    invoke-static {v10}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 192
    .local v2, "localBitmap":Landroid/graphics/Bitmap;
    invoke-virtual {v3, v8}, Landroid/view/View;->setDrawingCacheEnabled(Z)V

    .line 194
    invoke-direct {p0, v2, v6, v7}, Lcom/samsung/android/app/memo/uiwidget/DndListView;->doAnimationForDrag(Landroid/graphics/Bitmap;II)V

    .line 195
    const/4 v8, 0x4

    invoke-virtual {v3, v8}, Landroid/view/View;->setVisibility(I)V

    .line 196
    invoke-static {v5}, Lcom/samsung/android/app/memo/uiwidget/DndListView;->setSrcDragPos(I)V

    .line 197
    invoke-virtual {p0, v5}, Lcom/samsung/android/app/memo/uiwidget/DndListView;->setDragPos(I)V

    .line 198
    invoke-direct {p0}, Lcom/samsung/android/app/memo/uiwidget/DndListView;->adjustScrollBounds()V

    goto :goto_1

    .line 201
    .end local v2    # "localBitmap":Landroid/graphics/Bitmap;
    .end local v3    # "localView":Landroid/view/View;
    .end local v5    # "pos":I
    .end local v6    # "x":I
    .end local v7    # "y":I
    :pswitch_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v8

    float-to-int v0, v8

    .line 202
    .local v0, "i":I
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v8

    float-to-int v1, v8

    .line 203
    .local v1, "j":I
    iput v0, p0, Lcom/samsung/android/app/memo/uiwidget/DndListView;->mDragX:I

    .line 204
    iput v1, p0, Lcom/samsung/android/app/memo/uiwidget/DndListView;->mDragY:I

    .line 205
    invoke-direct {p0, v0, v1}, Lcom/samsung/android/app/memo/uiwidget/DndListView;->dragDrawable(II)V

    .line 206
    invoke-direct {p0, v0, v1}, Lcom/samsung/android/app/memo/uiwidget/DndListView;->getItemForPosition(II)I

    move-result v4

    .line 207
    .local v4, "movePos":I
    invoke-direct {p0, v4}, Lcom/samsung/android/app/memo/uiwidget/DndListView;->doAnimationForUpdate(I)V

    .line 208
    iget v8, p0, Lcom/samsung/android/app/memo/uiwidget/DndListView;->mDragY:I

    iget v10, p0, Lcom/samsung/android/app/memo/uiwidget/DndListView;->mUpperBound:I

    if-lt v8, v10, :cond_4

    iget v8, p0, Lcom/samsung/android/app/memo/uiwidget/DndListView;->mDragY:I

    iget v10, p0, Lcom/samsung/android/app/memo/uiwidget/DndListView;->mLowerBound:I

    if-le v8, v10, :cond_2

    .line 209
    :cond_4
    invoke-direct {p0}, Lcom/samsung/android/app/memo/uiwidget/DndListView;->scroll()V

    goto/16 :goto_1

    .line 214
    .end local v0    # "i":I
    .end local v1    # "j":I
    .end local v4    # "movePos":I
    :pswitch_2
    iput-boolean v9, p0, Lcom/samsung/android/app/memo/uiwidget/DndListView;->mProcessingAnimation:Z

    .line 215
    iput-boolean v8, p0, Lcom/samsung/android/app/memo/uiwidget/DndListView;->mDragging:Z

    .line 216
    invoke-direct {p0}, Lcom/samsung/android/app/memo/uiwidget/DndListView;->doAnimationForDrop()V

    .line 217
    iget-object v8, p0, Lcom/samsung/android/app/memo/uiwidget/DndListView;->mDragListener:Lcom/samsung/android/app/memo/uiwidget/DndListView$OnDndDragListener;

    if-eqz v8, :cond_2

    .line 218
    iget-object v8, p0, Lcom/samsung/android/app/memo/uiwidget/DndListView;->mDragListener:Lcom/samsung/android/app/memo/uiwidget/DndListView$OnDndDragListener;

    invoke-interface {v8}, Lcom/samsung/android/app/memo/uiwidget/DndListView$OnDndDragListener;->endDrag()V

    goto/16 :goto_1

    .line 226
    :cond_5
    invoke-super {p0, p1}, Landroid/widget/ListView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v8

    goto/16 :goto_0

    .line 169
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public setAdapter(Landroid/widget/ListAdapter;)V
    .locals 1
    .param p1, "adapter"    # Landroid/widget/ListAdapter;

    .prologue
    .line 624
    if-nez p1, :cond_0

    .line 642
    :goto_0
    return-void

    .line 627
    :cond_0
    invoke-super {p0, p1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 628
    new-instance v0, Lcom/samsung/android/app/memo/uiwidget/DndListView$5;

    invoke-direct {v0, p0}, Lcom/samsung/android/app/memo/uiwidget/DndListView$5;-><init>(Lcom/samsung/android/app/memo/uiwidget/DndListView;)V

    invoke-interface {p1, v0}, Landroid/widget/ListAdapter;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    goto :goto_0
.end method

.method public setChangeOrderMode(Z)V
    .locals 1
    .param p1, "enable"    # Z

    .prologue
    .line 672
    if-nez p1, :cond_0

    .line 673
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/app/memo/uiwidget/DndListView;->mIsChangedItemInChangeOrder:Z

    .line 675
    :cond_0
    iput-boolean p1, p0, Lcom/samsung/android/app/memo/uiwidget/DndListView;->mChangeOrderMode:Z

    .line 676
    return-void
.end method

.method public setDragListener(Lcom/samsung/android/app/memo/uiwidget/DndListView$OnDndDragListener;)V
    .locals 2
    .param p1, "listener"    # Lcom/samsung/android/app/memo/uiwidget/DndListView$OnDndDragListener;

    .prologue
    .line 661
    const-string v0, "LibraryListView"

    const-string v1, "setDragListener"

    invoke-static {v0, v1}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 662
    iput-object p1, p0, Lcom/samsung/android/app/memo/uiwidget/DndListView;->mDragListener:Lcom/samsung/android/app/memo/uiwidget/DndListView$OnDndDragListener;

    .line 663
    return-void
.end method

.method public setDragPos(I)V
    .locals 0
    .param p1, "dragPos"    # I

    .prologue
    .line 242
    iput p1, p0, Lcom/samsung/android/app/memo/uiwidget/DndListView;->mDragPos:I

    .line 243
    return-void
.end method

.method public setDragX(I)V
    .locals 0
    .param p1, "dragX"    # I

    .prologue
    .line 258
    iput p1, p0, Lcom/samsung/android/app/memo/uiwidget/DndListView;->mDragX:I

    .line 259
    return-void
.end method

.method public setDragY(I)V
    .locals 0
    .param p1, "dragY"    # I

    .prologue
    .line 262
    iput p1, p0, Lcom/samsung/android/app/memo/uiwidget/DndListView;->mDragY:I

    .line 263
    return-void
.end method

.method public setDragging(Z)V
    .locals 0
    .param p1, "dragging"    # Z

    .prologue
    .line 246
    iput-boolean p1, p0, Lcom/samsung/android/app/memo/uiwidget/DndListView;->mDragging:Z

    .line 247
    return-void
.end method

.method public setFirstPos(I)V
    .locals 0
    .param p1, "firstPos"    # I

    .prologue
    .line 250
    iput p1, p0, Lcom/samsung/android/app/memo/uiwidget/DndListView;->mFirstPos:I

    .line 251
    return-void
.end method

.method public setIsChagnedItemInChangeOrder(Z)V
    .locals 0
    .param p1, "set"    # Z

    .prologue
    .line 684
    iput-boolean p1, p0, Lcom/samsung/android/app/memo/uiwidget/DndListView;->mIsChangedItemInChangeOrder:Z

    .line 685
    return-void
.end method

.method public setItemSize(II)V
    .locals 0
    .param p1, "width"    # I
    .param p2, "height"    # I

    .prologue
    .line 271
    iput p1, p0, Lcom/samsung/android/app/memo/uiwidget/DndListView;->mItemWidth:I

    .line 272
    iput p2, p0, Lcom/samsung/android/app/memo/uiwidget/DndListView;->mItemHeight:I

    .line 273
    return-void
.end method

.method public setLastPos(I)V
    .locals 0
    .param p1, "lastPos"    # I

    .prologue
    .line 254
    iput p1, p0, Lcom/samsung/android/app/memo/uiwidget/DndListView;->mLastPos:I

    .line 255
    return-void
.end method

.method public setOffsetInDraggingItem(II)V
    .locals 0
    .param p1, "dx"    # I
    .param p2, "dy"    # I

    .prologue
    .line 266
    iput p1, p0, Lcom/samsung/android/app/memo/uiwidget/DndListView;->mOffsetXInDraggingItem:I

    .line 267
    iput p2, p0, Lcom/samsung/android/app/memo/uiwidget/DndListView;->mOffsetYInDraggingItem:I

    .line 268
    return-void
.end method

.method public setOnDndDragAndDropListener(Lcom/samsung/android/app/memo/uiwidget/DndListView$OnDndDragAndDropListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/samsung/android/app/memo/uiwidget/DndListView$OnDndDragAndDropListener;

    .prologue
    .line 144
    iput-object p1, p0, Lcom/samsung/android/app/memo/uiwidget/DndListView;->mDragAndDropListener:Lcom/samsung/android/app/memo/uiwidget/DndListView$OnDndDragAndDropListener;

    .line 145
    return-void
.end method

.method public setOnMoveCompletedListener(Lcom/samsung/android/app/memo/uiwidget/DndListView$OnMoveCompletedListener;)V
    .locals 2
    .param p1, "moveCompletedListener"    # Lcom/samsung/android/app/memo/uiwidget/DndListView$OnMoveCompletedListener;

    .prologue
    .line 494
    const-string v0, "LibraryListView"

    const-string v1, "setOnMoveCompletedListener"

    invoke-static {v0, v1}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 495
    iput-object p1, p0, Lcom/samsung/android/app/memo/uiwidget/DndListView;->mOnMoveCompletedListener:Lcom/samsung/android/app/memo/uiwidget/DndListView$OnMoveCompletedListener;

    .line 496
    return-void
.end method

.method public setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V
    .locals 0
    .param p1, "paramOnScrollListener"    # Landroid/widget/AbsListView$OnScrollListener;

    .prologue
    .line 500
    iput-object p1, p0, Lcom/samsung/android/app/memo/uiwidget/DndListView;->mOnScrollListener:Landroid/widget/AbsListView$OnScrollListener;

    .line 501
    return-void
.end method

.method public setProcessingAnimation(Z)V
    .locals 0
    .param p1, "processingAnimation"    # Z

    .prologue
    .line 230
    iput-boolean p1, p0, Lcom/samsung/android/app/memo/uiwidget/DndListView;->mProcessingAnimation:Z

    .line 231
    return-void
.end method

.method public setRecyclerListener(Landroid/widget/AbsListView$RecyclerListener;)V
    .locals 2
    .param p1, "paramRecyclerListener"    # Landroid/widget/AbsListView$RecyclerListener;

    .prologue
    .line 667
    const-string v0, "LibraryListView"

    const-string v1, "setRecyclerListener"

    invoke-static {v0, v1}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 668
    iput-object p1, p0, Lcom/samsung/android/app/memo/uiwidget/DndListView;->mRecyclerListener:Landroid/widget/AbsListView$RecyclerListener;

    .line 669
    return-void
.end method
