.class public Lcom/samsung/android/app/memo/uiwidget/RichEditor$SpannableStringBuilderTask;
.super Landroid/os/AsyncTask;
.source "RichEditor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/memo/uiwidget/RichEditor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "SpannableStringBuilderTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Integer;",
        "Landroid/text/SpannableStringBuilder;",
        ">;"
    }
.end annotation


# instance fields
.field private fromClipboard:Z

.field private mActivity:Landroid/content/Context;

.field private mHtmlStr:Ljava/lang/String;

.field private mMax:I

.field private mMin:I

.field private mProgress:Landroid/app/ProgressDialog;

.field private mRichEditor:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

.field private start:I

.field final synthetic this$0:Lcom/samsung/android/app/memo/uiwidget/RichEditor;


# direct methods
.method public constructor <init>(Lcom/samsung/android/app/memo/uiwidget/RichEditor;Ljava/lang/String;Landroid/content/Context;Landroid/text/SpannableStringBuilder;IILcom/samsung/android/app/memo/uiwidget/RichEditor;Z)V
    .locals 1
    .param p2, "htmlStr"    # Ljava/lang/String;
    .param p3, "mActivity"    # Landroid/content/Context;
    .param p4, "spn"    # Landroid/text/SpannableStringBuilder;
    .param p5, "min"    # I
    .param p6, "max"    # I
    .param p7, "richEditor"    # Lcom/samsung/android/app/memo/uiwidget/RichEditor;
    .param p8, "fromClip"    # Z

    .prologue
    .line 1581
    iput-object p1, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$SpannableStringBuilderTask;->this$0:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    .line 1580
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 1582
    iput-object p2, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$SpannableStringBuilderTask;->mHtmlStr:Ljava/lang/String;

    .line 1583
    iput-object p3, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$SpannableStringBuilderTask;->mActivity:Landroid/content/Context;

    .line 1585
    iput p5, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$SpannableStringBuilderTask;->mMin:I

    .line 1586
    iput p6, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$SpannableStringBuilderTask;->mMax:I

    .line 1587
    const/4 v0, -0x1

    iput v0, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$SpannableStringBuilderTask;->start:I

    .line 1588
    iput-object p7, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$SpannableStringBuilderTask;->mRichEditor:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    .line 1589
    iput-boolean p8, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$SpannableStringBuilderTask;->fromClipboard:Z

    .line 1590
    return-void
.end method

.method public constructor <init>(Lcom/samsung/android/app/memo/uiwidget/RichEditor;Ljava/lang/String;Landroid/content/Context;Landroid/text/SpannableStringBuilder;ILcom/samsung/android/app/memo/uiwidget/RichEditor;I)V
    .locals 1
    .param p2, "htmlStr"    # Ljava/lang/String;
    .param p3, "mActivity"    # Landroid/content/Context;
    .param p4, "spn"    # Landroid/text/SpannableStringBuilder;
    .param p5, "min"    # I
    .param p6, "richEditor"    # Lcom/samsung/android/app/memo/uiwidget/RichEditor;
    .param p7, "start"    # I

    .prologue
    .line 1593
    iput-object p1, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$SpannableStringBuilderTask;->this$0:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    .line 1592
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 1594
    iput-object p2, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$SpannableStringBuilderTask;->mHtmlStr:Ljava/lang/String;

    .line 1595
    iput-object p3, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$SpannableStringBuilderTask;->mActivity:Landroid/content/Context;

    .line 1597
    iput p5, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$SpannableStringBuilderTask;->mMin:I

    .line 1598
    iput p5, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$SpannableStringBuilderTask;->mMax:I

    .line 1599
    iput p7, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$SpannableStringBuilderTask;->start:I

    .line 1600
    iput-object p6, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$SpannableStringBuilderTask;->mRichEditor:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    .line 1601
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$SpannableStringBuilderTask;->fromClipboard:Z

    .line 1602
    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Landroid/text/SpannableStringBuilder;
    .locals 4
    .param p1, "arg0"    # [Ljava/lang/Void;

    .prologue
    .line 1606
    iget-object v1, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$SpannableStringBuilderTask;->this$0:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    iget-object v2, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$SpannableStringBuilderTask;->mHtmlStr:Ljava/lang/String;

    iget-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$SpannableStringBuilderTask;->this$0:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    # getter for: Lcom/samsung/android/app/memo/uiwidget/RichEditor;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->access$1(Lcom/samsung/android/app/memo/uiwidget/RichEditor;)Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    iget-object v3, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$SpannableStringBuilderTask;->this$0:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    # getter for: Lcom/samsung/android/app/memo/uiwidget/RichEditor;->mSession:Lcom/samsung/android/app/memo/Session;
    invoke-static {v3}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->access$8(Lcom/samsung/android/app/memo/uiwidget/RichEditor;)Lcom/samsung/android/app/memo/Session;

    move-result-object v3

    invoke-virtual {v1, v2, v0, v3}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->getCustomSpannableString(Ljava/lang/String;Landroid/content/Context;Lcom/samsung/android/app/memo/Session;)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    return-object v0
.end method

.method protected bridge varargs synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/samsung/android/app/memo/uiwidget/RichEditor$SpannableStringBuilderTask;->doInBackground([Ljava/lang/Void;)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Landroid/text/SpannableStringBuilder;)V
    .locals 14
    .param p1, "result"    # Landroid/text/SpannableStringBuilder;

    .prologue
    const/16 v13, 0xa

    const v12, 0x7f0e0032

    const/4 v11, -0x1

    const/4 v10, 0x0

    .line 1489
    sget v6, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->mMaxCharSize:I

    iget-object v7, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$SpannableStringBuilderTask;->this$0:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    invoke-virtual {v7}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->getText()Landroid/text/Editable;

    move-result-object v7

    invoke-interface {v7}, Landroid/text/Editable;->length()I

    move-result v7

    sub-int/2addr v6, v7

    iget v7, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$SpannableStringBuilderTask;->mMax:I

    iget v8, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$SpannableStringBuilderTask;->mMin:I

    sub-int/2addr v7, v8

    add-int v0, v6, v7

    .line 1490
    .local v0, "availLength":I
    invoke-virtual {p1}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v2

    .line 1491
    .local v2, "origLength":I
    if-ge v0, v2, :cond_0

    iget v6, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$SpannableStringBuilderTask;->start:I

    if-ne v6, v11, :cond_0

    .line 1492
    iget-object v6, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$SpannableStringBuilderTask;->this$0:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    # getter for: Lcom/samsung/android/app/memo/uiwidget/RichEditor;->mContext:Landroid/content/Context;
    invoke-static {v6}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->access$1(Lcom/samsung/android/app/memo/uiwidget/RichEditor;)Landroid/content/Context;

    move-result-object v6

    const v7, 0x7f0b0048

    invoke-static {v6, v7}, Lcom/samsung/android/app/memo/util/Utils;->showToast(Landroid/content/Context;I)V

    .line 1493
    const-string v6, ""

    invoke-virtual {p1, v0, v2, v6}, Landroid/text/SpannableStringBuilder;->replace(IILjava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 1495
    :cond_0
    iget v6, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$SpannableStringBuilderTask;->start:I

    if-eq v6, v11, :cond_a

    .line 1496
    iget v6, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$SpannableStringBuilderTask;->start:I

    invoke-virtual {p1}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v7

    sub-int/2addr v6, v7

    if-ltz v6, :cond_2

    .line 1497
    iget-object v6, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$SpannableStringBuilderTask;->mRichEditor:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    invoke-virtual {v6}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->getText()Landroid/text/Editable;

    move-result-object v6

    iget v7, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$SpannableStringBuilderTask;->start:I

    invoke-virtual {p1}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v8

    sub-int/2addr v7, v8

    iget v8, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$SpannableStringBuilderTask;->start:I

    const-string v9, ""

    invoke-interface {v6, v7, v8, v9}, Landroid/text/Editable;->replace(IILjava/lang/CharSequence;)Landroid/text/Editable;

    .line 1500
    :goto_0
    iget v6, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$SpannableStringBuilderTask;->start:I

    iget v7, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$SpannableStringBuilderTask;->mMin:I

    if-le v6, v7, :cond_3

    iget v6, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$SpannableStringBuilderTask;->start:I

    invoke-virtual {p1}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v7

    sub-int/2addr v6, v7

    iget v7, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$SpannableStringBuilderTask;->mMin:I

    if-ge v6, v7, :cond_3

    .line 1501
    iget-object v6, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$SpannableStringBuilderTask;->mProgress:Landroid/app/ProgressDialog;

    invoke-virtual {v6}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 1502
    iget-object v6, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$SpannableStringBuilderTask;->mProgress:Landroid/app/ProgressDialog;

    invoke-virtual {v6}, Landroid/app/ProgressDialog;->dismiss()V

    .line 1503
    :cond_1
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 1578
    :goto_1
    return-void

    .line 1499
    :cond_2
    iget-object v6, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$SpannableStringBuilderTask;->mRichEditor:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    invoke-virtual {v6}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->getText()Landroid/text/Editable;

    move-result-object v6

    invoke-virtual {p1}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v7

    const-string v8, ""

    invoke-interface {v6, v10, v7, v8}, Landroid/text/Editable;->replace(IILjava/lang/CharSequence;)Landroid/text/Editable;

    goto :goto_0

    .line 1506
    :cond_3
    iget v6, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$SpannableStringBuilderTask;->mMin:I

    iget v7, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$SpannableStringBuilderTask;->start:I

    if-lt v6, v7, :cond_8

    iget v6, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$SpannableStringBuilderTask;->mMin:I

    invoke-virtual {p1}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v7

    sub-int/2addr v6, v7

    if-ltz v6, :cond_8

    .line 1507
    iget-object v6, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$SpannableStringBuilderTask;->mRichEditor:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    invoke-virtual {v6}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->getText()Landroid/text/Editable;

    move-result-object v6

    iget v7, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$SpannableStringBuilderTask;->mMin:I

    invoke-virtual {p1}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v8

    sub-int/2addr v7, v8

    iget v8, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$SpannableStringBuilderTask;->mMax:I

    invoke-virtual {p1}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v9

    sub-int/2addr v8, v9

    invoke-interface {v6, v7, v8, p1}, Landroid/text/Editable;->replace(IILjava/lang/CharSequence;)Landroid/text/Editable;

    .line 1542
    :goto_2
    :try_start_0
    iget v6, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$SpannableStringBuilderTask;->start:I

    if-eq v6, v11, :cond_4

    .line 1543
    iget-object v6, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$SpannableStringBuilderTask;->mRichEditor:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    iget v7, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$SpannableStringBuilderTask;->mMin:I

    invoke-virtual {v6, v7}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->setSelection(I)V
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_1

    .line 1551
    :cond_4
    :goto_3
    :try_start_1
    iget-object v6, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$SpannableStringBuilderTask;->mProgress:Landroid/app/ProgressDialog;

    invoke-virtual {v6}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v6

    if-eqz v6, :cond_5

    .line 1552
    iget-object v6, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$SpannableStringBuilderTask;->mProgress:Landroid/app/ProgressDialog;

    invoke-virtual {v6}, Landroid/app/ProgressDialog;->dismiss()V
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_2

    .line 1556
    :cond_5
    :goto_4
    if-le v0, v2, :cond_6

    iget-boolean v6, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$SpannableStringBuilderTask;->fromClipboard:Z

    if-eqz v6, :cond_6

    sget-boolean v6, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->mImagePasteFailed:Z

    if-nez v6, :cond_6

    .line 1557
    iget-object v6, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$SpannableStringBuilderTask;->this$0:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    # getter for: Lcom/samsung/android/app/memo/uiwidget/RichEditor;->isMaxImageReached:Z
    invoke-static {v6}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->access$11(Lcom/samsung/android/app/memo/uiwidget/RichEditor;)Z

    move-result v6

    if-nez v6, :cond_6

    iget-object v6, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$SpannableStringBuilderTask;->this$0:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    # getter for: Lcom/samsung/android/app/memo/uiwidget/RichEditor;->mClipboardEXProxy:Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;
    invoke-static {v6}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->access$10(Lcom/samsung/android/app/memo/uiwidget/RichEditor;)Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;

    move-result-object v6

    invoke-virtual {v6}, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->isCalledPasteFromClipboard()Z

    move-result v6

    if-eqz v6, :cond_6

    .line 1558
    iget-object v6, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$SpannableStringBuilderTask;->this$0:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    # getter for: Lcom/samsung/android/app/memo/uiwidget/RichEditor;->mContext:Landroid/content/Context;
    invoke-static {v6}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->access$1(Lcom/samsung/android/app/memo/uiwidget/RichEditor;)Landroid/content/Context;

    move-result-object v6

    const v7, 0x7f0b00b1

    invoke-static {v6, v7}, Lcom/samsung/android/app/memo/util/Utils;->showToast(Landroid/content/Context;I)V

    .line 1559
    iget-object v6, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$SpannableStringBuilderTask;->this$0:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    # getter for: Lcom/samsung/android/app/memo/uiwidget/RichEditor;->mClipboardEXProxy:Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;
    invoke-static {v6}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->access$10(Lcom/samsung/android/app/memo/uiwidget/RichEditor;)Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;

    move-result-object v6

    invoke-virtual {v6}, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->resetIsPastedFromTheClipboard()V

    .line 1561
    :cond_6
    sget-object v5, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->mToolbarView:Landroid/view/View;

    .line 1562
    .local v5, "v":Landroid/view/View;
    if-eqz v5, :cond_7

    invoke-virtual {v5, v12}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    if-eqz v6, :cond_7

    .line 1563
    invoke-static {}, Lcom/samsung/android/app/memo/util/Utils;->isNoOutgoingCall()Z

    move-result v6

    if-nez v6, :cond_f

    .line 1564
    invoke-virtual {v5, v12}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    invoke-virtual {v6, v10}, Landroid/view/View;->setEnabled(Z)V

    .line 1575
    :cond_7
    :goto_5
    sput-boolean v10, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->ongoingAsynctask:Z

    .line 1576
    iget-object v6, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$SpannableStringBuilderTask;->mRichEditor:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    invoke-virtual {v6, v10}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->setPasteOperationState(Z)V

    .line 1577
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 1508
    .end local v5    # "v":Landroid/view/View;
    :cond_8
    iget v6, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$SpannableStringBuilderTask;->mMin:I

    invoke-virtual {p1}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v7

    if-le v6, v7, :cond_9

    .line 1509
    iget-object v6, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$SpannableStringBuilderTask;->mRichEditor:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    invoke-virtual {v6}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->getText()Landroid/text/Editable;

    move-result-object v6

    invoke-virtual {p1}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v7

    invoke-virtual {p1}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v8

    invoke-interface {v6, v7, v8, p1}, Landroid/text/Editable;->replace(IILjava/lang/CharSequence;)Landroid/text/Editable;

    goto/16 :goto_2

    .line 1511
    :cond_9
    iget-object v6, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$SpannableStringBuilderTask;->mRichEditor:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    invoke-virtual {v6}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->getText()Landroid/text/Editable;

    move-result-object v6

    iget v7, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$SpannableStringBuilderTask;->mMin:I

    iget v8, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$SpannableStringBuilderTask;->mMax:I

    invoke-interface {v6, v7, v8, p1}, Landroid/text/Editable;->replace(IILjava/lang/CharSequence;)Landroid/text/Editable;

    goto/16 :goto_2

    .line 1513
    :cond_a
    iget v6, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$SpannableStringBuilderTask;->mMin:I

    iget v7, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$SpannableStringBuilderTask;->mMax:I

    if-ne v6, v7, :cond_e

    .line 1514
    iget v6, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$SpannableStringBuilderTask;->mMin:I

    if-lez v6, :cond_b

    iget-object v6, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$SpannableStringBuilderTask;->mRichEditor:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    invoke-virtual {v6}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->getText()Landroid/text/Editable;

    move-result-object v6

    iget v7, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$SpannableStringBuilderTask;->mMin:I

    add-int/lit8 v7, v7, -0x1

    invoke-interface {v6, v7}, Landroid/text/Editable;->charAt(I)C

    move-result v6

    const/16 v7, 0x20

    if-eq v6, v7, :cond_b

    .line 1515
    if-lt v0, v2, :cond_b

    .line 1516
    iget-object v6, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$SpannableStringBuilderTask;->mRichEditor:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    invoke-virtual {v6}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->getText()Landroid/text/Editable;

    move-result-object v6

    iget v7, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$SpannableStringBuilderTask;->mMin:I

    add-int/lit8 v7, v7, -0x1

    invoke-interface {v6, v7}, Landroid/text/Editable;->charAt(I)C

    move-result v6

    if-eq v6, v13, :cond_b

    .line 1517
    const-string v6, " "

    invoke-virtual {p1, v10, v6}, Landroid/text/SpannableStringBuilder;->insert(ILjava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 1518
    :cond_b
    iget-object v6, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$SpannableStringBuilderTask;->mRichEditor:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    invoke-virtual {v6}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->getEditableText()Landroid/text/Editable;

    move-result-object v6

    iget v7, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$SpannableStringBuilderTask;->mMin:I

    iget v8, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$SpannableStringBuilderTask;->mMax:I

    .line 1519
    const-class v9, Lcom/samsung/android/app/memo/uiwidget/ClickableSpanType;

    .line 1518
    invoke-interface {v6, v7, v8, v9}, Landroid/text/Editable;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [Lcom/samsung/android/app/memo/uiwidget/ClickableSpanType;

    .line 1520
    .local v3, "spans":[Lcom/samsung/android/app/memo/uiwidget/ClickableSpanType;
    array-length v6, v3

    if-lez v6, :cond_c

    .line 1521
    iget v6, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$SpannableStringBuilderTask;->mMin:I

    iget-object v7, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$SpannableStringBuilderTask;->mRichEditor:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    invoke-virtual {v7}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->getEditableText()Landroid/text/Editable;

    move-result-object v7

    aget-object v8, v3, v10

    invoke-interface {v7, v8}, Landroid/text/Editable;->getSpanStart(Ljava/lang/Object;)I

    move-result v7

    if-ne v6, v7, :cond_d

    iget-object v6, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$SpannableStringBuilderTask;->mRichEditor:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    invoke-virtual {v6}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->getEditableText()Landroid/text/Editable;

    move-result-object v6

    aget-object v7, v3, v10

    invoke-interface {v6, v7}, Landroid/text/Editable;->getSpanStart(Ljava/lang/Object;)I

    move-result v6

    add-int/lit8 v6, v6, -0x1

    :goto_6
    iput v6, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$SpannableStringBuilderTask;->mMin:I

    .line 1522
    iget v6, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$SpannableStringBuilderTask;->mMin:I

    iput v6, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$SpannableStringBuilderTask;->mMax:I

    .line 1535
    :cond_c
    :goto_7
    :try_start_2
    iget-object v6, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$SpannableStringBuilderTask;->mRichEditor:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    invoke-virtual {v6}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->getText()Landroid/text/Editable;

    move-result-object v6

    iget v7, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$SpannableStringBuilderTask;->mMin:I

    iget v8, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$SpannableStringBuilderTask;->mMax:I

    invoke-interface {v6, v7, v8, p1}, Landroid/text/Editable;->replace(IILjava/lang/CharSequence;)Landroid/text/Editable;
    :try_end_2
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_2 .. :try_end_2} :catch_0

    goto/16 :goto_2

    .line 1536
    :catch_0
    move-exception v1

    .line 1537
    .local v1, "e":Ljava/lang/IndexOutOfBoundsException;
    const-string v6, "SpannableStringBuilderTask"

    const-string v7, "onPostExecute "

    invoke-static {v6, v7, v1}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_2

    .line 1521
    .end local v1    # "e":Ljava/lang/IndexOutOfBoundsException;
    :cond_d
    iget-object v6, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$SpannableStringBuilderTask;->mRichEditor:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    invoke-virtual {v6}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->getEditableText()Landroid/text/Editable;

    move-result-object v6

    aget-object v7, v3, v10

    invoke-interface {v6, v7}, Landroid/text/Editable;->getSpanEnd(Ljava/lang/Object;)I

    move-result v6

    goto :goto_6

    .line 1525
    .end local v3    # "spans":[Lcom/samsung/android/app/memo/uiwidget/ClickableSpanType;
    :cond_e
    iget-object v6, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$SpannableStringBuilderTask;->mRichEditor:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    invoke-virtual {v6}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->getEditableText()Landroid/text/Editable;

    move-result-object v6

    iget v7, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$SpannableStringBuilderTask;->mMin:I

    iget v8, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$SpannableStringBuilderTask;->mMax:I

    .line 1526
    const-class v9, Lcom/samsung/android/app/memo/uiwidget/ClickableSpanType;

    .line 1525
    invoke-interface {v6, v7, v8, v9}, Landroid/text/Editable;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [Lcom/samsung/android/app/memo/uiwidget/ClickableSpanType;

    .line 1527
    .restart local v3    # "spans":[Lcom/samsung/android/app/memo/uiwidget/ClickableSpanType;
    array-length v6, v3

    if-lez v6, :cond_c

    .line 1528
    iget-object v6, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$SpannableStringBuilderTask;->mRichEditor:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    invoke-virtual {v6}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->getText()Landroid/text/Editable;

    move-result-object v4

    .line 1529
    .local v4, "text":Landroid/text/Editable;
    iget-object v6, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$SpannableStringBuilderTask;->mRichEditor:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    invoke-virtual {v6}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->getText()Landroid/text/Editable;

    move-result-object v6

    aget-object v7, v3, v10

    invoke-interface {v6, v7}, Landroid/text/Editable;->removeSpan(Ljava/lang/Object;)V

    .line 1530
    iget v6, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$SpannableStringBuilderTask;->mMin:I

    if-eqz v6, :cond_c

    iget v6, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$SpannableStringBuilderTask;->mMin:I

    add-int/lit8 v6, v6, -0x1

    invoke-interface {v4, v6}, Landroid/text/Editable;->charAt(I)C

    move-result v6

    if-ne v6, v13, :cond_c

    .line 1531
    iget v6, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$SpannableStringBuilderTask;->mMin:I

    add-int/lit8 v6, v6, -0x1

    iput v6, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$SpannableStringBuilderTask;->mMin:I

    goto :goto_7

    .line 1546
    .end local v3    # "spans":[Lcom/samsung/android/app/memo/uiwidget/ClickableSpanType;
    .end local v4    # "text":Landroid/text/Editable;
    :catch_1
    move-exception v1

    .line 1547
    .restart local v1    # "e":Ljava/lang/IndexOutOfBoundsException;
    iget-object v6, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$SpannableStringBuilderTask;->mRichEditor:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    const/16 v7, 0x1000

    invoke-virtual {v6, v7}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->setSelection(I)V

    goto/16 :goto_3

    .line 1553
    .end local v1    # "e":Ljava/lang/IndexOutOfBoundsException;
    :catch_2
    move-exception v1

    .line 1554
    .local v1, "e":Ljava/lang/IllegalArgumentException;
    # getter for: Lcom/samsung/android/app/memo/uiwidget/RichEditor;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->access$9()Ljava/lang/String;

    move-result-object v6

    const-string v7, "onPostExecute"

    invoke-static {v6, v7, v1}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_4

    .line 1565
    .end local v1    # "e":Ljava/lang/IllegalArgumentException;
    .restart local v5    # "v":Landroid/view/View;
    :cond_f
    invoke-static {}, Lcom/samsung/android/app/memo/util/Utils;->isUPSM()I

    move-result v6

    const/4 v7, 0x1

    if-ne v6, v7, :cond_7

    .line 1566
    invoke-virtual {v5, v12}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    invoke-virtual {v6, v10}, Landroid/view/View;->setEnabled(Z)V

    goto/16 :goto_5
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1
    check-cast p1, Landroid/text/SpannableStringBuilder;

    invoke-virtual {p0, p1}, Lcom/samsung/android/app/memo/uiwidget/RichEditor$SpannableStringBuilderTask;->onPostExecute(Landroid/text/SpannableStringBuilder;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 4

    .prologue
    .line 1477
    iget-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$SpannableStringBuilderTask;->mActivity:Landroid/content/Context;

    if-eqz v0, :cond_0

    .line 1478
    iget-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$SpannableStringBuilderTask;->mActivity:Landroid/content/Context;

    .line 1479
    const/4 v1, 0x0

    iget-object v2, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$SpannableStringBuilderTask;->mActivity:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b00ab

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 1478
    invoke-static {v0, v1, v2}, Landroid/app/ProgressDialog;->show(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Landroid/app/ProgressDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$SpannableStringBuilderTask;->mProgress:Landroid/app/ProgressDialog;

    .line 1480
    iget-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$SpannableStringBuilderTask;->mProgress:Landroid/app/ProgressDialog;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 1481
    iget-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$SpannableStringBuilderTask;->mProgress:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isIndeterminate()Z

    .line 1482
    iget-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$SpannableStringBuilderTask;->mProgress:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    .line 1484
    :cond_0
    invoke-super {p0}, Landroid/os/AsyncTask;->onPreExecute()V

    .line 1485
    return-void
.end method
