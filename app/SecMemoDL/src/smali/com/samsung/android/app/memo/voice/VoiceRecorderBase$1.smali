.class Lcom/samsung/android/app/memo/voice/VoiceRecorderBase$1;
.super Landroid/telephony/PhoneStateListener;
.source "VoiceRecorderBase.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase$1;->this$0:Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;

    .line 432
    invoke-direct {p0}, Landroid/telephony/PhoneStateListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onCallStateChanged(ILjava/lang/String;)V
    .locals 4
    .param p1, "state"    # I
    .param p2, "incomingNumber"    # Ljava/lang/String;

    .prologue
    const v3, 0x7f0b0093

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 435
    packed-switch p1, :pswitch_data_0

    .line 475
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 449
    :pswitch_1
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase$1;->this$0:Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;

    invoke-virtual {v0}, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->isNowRecording()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 452
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase$1;->this$0:Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;

    invoke-virtual {v0}, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->pauseRecording()V

    .line 453
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase$1;->this$0:Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;

    iget-object v0, v0, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->mRecordListener:Lcom/samsung/android/app/memo/voice/VoiceRecorderBase$RecordListener;

    invoke-interface {v0}, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase$RecordListener;->onPauseRecording()V

    .line 455
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase$1;->this$0:Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;

    invoke-virtual {v0}, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->isNowPlaying()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 456
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase$1;->this$0:Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;

    invoke-static {v0, v2}, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->access$0(Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;Z)V

    .line 457
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase$1;->this$0:Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;

    invoke-virtual {v0}, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->pauseSoundPlay()V

    .line 458
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase$1;->this$0:Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;

    iget-object v0, v0, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->mActivity:Landroid/content/Context;

    invoke-static {v0, v3, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 462
    :pswitch_2
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase$1;->this$0:Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;

    invoke-virtual {v0}, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->isNowRecording()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 463
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase$1;->this$0:Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;

    invoke-virtual {v0}, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->stopRecording()Lcom/samsung/android/app/memo/voice/VoiceInfo;

    .line 464
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase$1;->this$0:Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;

    iget-object v0, v0, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->mRecordListener:Lcom/samsung/android/app/memo/voice/VoiceRecorderBase$RecordListener;

    invoke-interface {v0}, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase$RecordListener;->onForceStop()V

    .line 467
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase$1;->this$0:Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;

    invoke-virtual {v0}, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->isNowPlaying()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 468
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase$1;->this$0:Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;

    invoke-virtual {v0}, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->pauseSoundPlay()V

    .line 469
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase$1;->this$0:Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;

    invoke-static {v0, v2}, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->access$0(Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;Z)V

    .line 470
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase$1;->this$0:Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;

    iget-object v0, v0, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->mActivity:Landroid/content/Context;

    invoke-static {v0, v3, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 435
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
