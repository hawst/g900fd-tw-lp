.class Lcom/samsung/android/app/memo/voice/VoiceViewLayout$5;
.super Ljava/lang/Object;
.source "VoiceViewLayout.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/memo/voice/VoiceViewLayout;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$5;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    .line 1198
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 19
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 1202
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$5;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    # getter for: Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mVoiceManger:Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;
    invoke-static {v14}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->access$9(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;

    move-result-object v14

    if-nez v14, :cond_1

    .line 1401
    :cond_0
    :goto_0
    return-void

    .line 1204
    :cond_1
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getId()I

    move-result v14

    sparse-switch v14, :sswitch_data_0

    goto :goto_0

    .line 1206
    :sswitch_0
    const/4 v2, 0x0

    .line 1207
    .local v2, "batteryStatus":Landroid/content/Intent;
    const/4 v8, -0x1

    .line 1208
    .local v8, "level":I
    const/4 v11, -0x1

    .line 1209
    .local v11, "scale":I
    const/4 v7, 0x0

    .line 1210
    .local v7, "isCharging":Z
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$5;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    # getter for: Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mContext:Landroid/content/Context;
    invoke-static {v14}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->access$1(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)Landroid/content/Context;

    move-result-object v14

    if-eqz v14, :cond_3

    .line 1211
    new-instance v6, Landroid/content/IntentFilter;

    const-string v14, "android.intent.action.BATTERY_CHANGED"

    invoke-direct {v6, v14}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 1212
    .local v6, "ifilter":Landroid/content/IntentFilter;
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$5;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    # getter for: Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mContext:Landroid/content/Context;
    invoke-static {v14}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->access$1(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)Landroid/content/Context;

    move-result-object v14

    const/4 v15, 0x0

    invoke-virtual {v14, v15, v6}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    move-result-object v2

    .line 1214
    const-string v14, "level"

    const/4 v15, -0x1

    invoke-virtual {v2, v14, v15}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v8

    .line 1215
    const-string v14, "scale"

    const/4 v15, -0x1

    invoke-virtual {v2, v14, v15}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v11

    .line 1216
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$5;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    int-to-float v15, v8

    int-to-float v0, v11

    move/from16 v16, v0

    div-float v15, v15, v16

    iput v15, v14, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->batteryPercentage:F

    .line 1217
    const-string v14, "status"

    const/4 v15, -0x1

    invoke-virtual {v2, v14, v15}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    .line 1218
    .local v3, "chargingStatus":I
    const/4 v14, 0x2

    if-eq v3, v14, :cond_2

    .line 1219
    const/4 v14, 0x5

    if-eq v3, v14, :cond_2

    .line 1218
    const/4 v7, 0x0

    .line 1221
    :goto_1
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$5;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    # getter for: Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mContext:Landroid/content/Context;
    invoke-static {v14}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->access$1(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)Landroid/content/Context;

    move-result-object v14

    if-eqz v14, :cond_3

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$5;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    iget v14, v14, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->batteryPercentage:F

    float-to-double v14, v14

    const-wide v16, 0x3f947ae147ae147bL    # 0.02

    cmpg-double v14, v14, v16

    if-gez v14, :cond_3

    if-nez v7, :cond_3

    .line 1222
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$5;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    # getter for: Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mContext:Landroid/content/Context;
    invoke-static {v14}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->access$1(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)Landroid/content/Context;

    move-result-object v14

    const v15, 0x7f0b00bd

    invoke-static {v14, v15}, Lcom/samsung/android/app/memo/util/Utils;->showToast(Landroid/content/Context;I)V

    goto :goto_0

    .line 1218
    :cond_2
    const/4 v7, 0x1

    goto :goto_1

    .line 1227
    .end local v3    # "chargingStatus":I
    .end local v6    # "ifilter":Landroid/content/IntentFilter;
    :cond_3
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$5;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    # getter for: Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mContext:Landroid/content/Context;
    invoke-static {v14}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->access$1(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)Landroid/content/Context;

    move-result-object v14

    if-eqz v14, :cond_4

    .line 1228
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$5;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    # getter for: Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mContext:Landroid/content/Context;
    invoke-static {v14}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->access$1(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)Landroid/content/Context;

    move-result-object v14

    .line 1229
    const-string v15, "audio"

    invoke-virtual {v14, v15}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v9

    .line 1228
    check-cast v9, Landroid/media/AudioManager;

    .line 1231
    .local v9, "mAudioManager":Landroid/media/AudioManager;
    if-eqz v9, :cond_4

    invoke-virtual {v9}, Landroid/media/AudioManager;->isRecordActive()Z

    move-result v14

    if-eqz v14, :cond_4

    .line 1232
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$5;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    const v15, 0x7f0b0096

    invoke-virtual {v14, v15}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->showToast(I)V

    goto/16 :goto_0

    .line 1237
    .end local v9    # "mAudioManager":Landroid/media/AudioManager;
    :cond_4
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$5;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    # getter for: Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mRemoteViews:Landroid/widget/RemoteViews;
    invoke-static {v14}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->access$8(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)Landroid/widget/RemoteViews;

    move-result-object v14

    if-eqz v14, :cond_5

    .line 1238
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$5;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    # getter for: Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mRemoteViews:Landroid/widget/RemoteViews;
    invoke-static {v14}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->access$8(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)Landroid/widget/RemoteViews;

    move-result-object v14

    const v15, 0x7f0e006a

    .line 1239
    const/16 v16, 0x0

    .line 1238
    invoke-virtual/range {v14 .. v16}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 1240
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$5;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    # getter for: Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mRemoteViews:Landroid/widget/RemoteViews;
    invoke-static {v14}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->access$8(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)Landroid/widget/RemoteViews;

    move-result-object v14

    const v15, 0x7f0e0068

    .line 1241
    const/16 v16, 0x8

    .line 1240
    invoke-virtual/range {v14 .. v16}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 1243
    :cond_5
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$5;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    # invokes: Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->callCheck()Z
    invoke-static {v14}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->access$11(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)Z

    move-result v14

    if-nez v14, :cond_6

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$5;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    invoke-virtual {v14}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->isGoogleTalkVideoChatRunning()Z

    move-result v14

    if-eqz v14, :cond_7

    .line 1244
    :cond_6
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$5;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    const v15, 0x7f0b0097

    invoke-virtual {v14, v15}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->showToast(I)V

    goto/16 :goto_0

    .line 1248
    :cond_7
    new-instance v10, Landroid/sec/enterprise/RestrictionPolicy;

    invoke-direct {v10}, Landroid/sec/enterprise/RestrictionPolicy;-><init>()V

    .line 1249
    .local v10, "restrictionPolicy":Landroid/sec/enterprise/RestrictionPolicy;
    const/4 v14, 0x1

    invoke-virtual {v10, v14}, Landroid/sec/enterprise/RestrictionPolicy;->isAudioRecordAllowed(Z)Z

    move-result v14

    if-eqz v14, :cond_0

    .line 1250
    const/4 v14, 0x1

    invoke-virtual {v10, v14}, Landroid/sec/enterprise/RestrictionPolicy;->isMicrophoneEnabled(Z)Z

    move-result v14

    if-eqz v14, :cond_0

    .line 1253
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$5;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    # getter for: Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mContext:Landroid/content/Context;
    invoke-static {v14}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->access$1(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)Landroid/content/Context;

    move-result-object v14

    if-eqz v14, :cond_8

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$5;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    # getter for: Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mVoiceManger:Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;
    invoke-static {v14}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->access$9(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$5;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    # getter for: Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mContext:Landroid/content/Context;
    invoke-static {v15}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->access$1(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)Landroid/content/Context;

    move-result-object v15

    invoke-virtual {v14, v15}, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->isRecordActive(Landroid/content/Context;)Z

    move-result v14

    if-eqz v14, :cond_8

    .line 1254
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$5;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    # getter for: Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mVoiceManger:Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;
    invoke-static {v14}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->access$9(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;

    move-result-object v14

    invoke-virtual {v14}, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->isNowRecording()Z

    move-result v14

    if-eqz v14, :cond_e

    .line 1255
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$5;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    # getter for: Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mVoiceManger:Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;
    invoke-static {v14}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->access$9(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;

    move-result-object v14

    invoke-virtual {v14}, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->stopRecording()Lcom/samsung/android/app/memo/voice/VoiceInfo;

    .line 1261
    :cond_8
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$5;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    # getter for: Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mVoiceManger:Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;
    invoke-static {v14}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->access$9(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;

    move-result-object v14

    invoke-virtual {v14}, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->isNowPlaying()Z

    move-result v14

    if-nez v14, :cond_9

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$5;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    # getter for: Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mVoiceManger:Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;
    invoke-static {v14}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->access$9(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;

    move-result-object v14

    invoke-virtual {v14}, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->isNowPaused()Z

    move-result v14

    if-eqz v14, :cond_a

    .line 1262
    :cond_9
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$5;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    # getter for: Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mVoiceManger:Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;
    invoke-static {v14}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->access$9(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;

    move-result-object v14

    invoke-virtual {v14}, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->stopSoundPlay()V

    .line 1265
    :cond_a
    :try_start_0
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$5;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    # getter for: Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mContext:Landroid/content/Context;
    invoke-static {v14}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->access$1(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)Landroid/content/Context;

    move-result-object v14

    if-eqz v14, :cond_b

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$5;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    # getter for: Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mVoiceinfo:Lcom/samsung/android/app/memo/voice/VoiceInfo;
    invoke-static {v14}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->access$0(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)Lcom/samsung/android/app/memo/voice/VoiceInfo;

    move-result-object v14

    if-eqz v14, :cond_b

    .line 1266
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$5;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    # getter for: Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mVoiceinfo:Lcom/samsung/android/app/memo/voice/VoiceInfo;
    invoke-static {v14}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->access$0(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)Lcom/samsung/android/app/memo/voice/VoiceInfo;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$5;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    # getter for: Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mFragmentAction:Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;
    invoke-static {v15}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->access$12(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;

    move-result-object v15

    invoke-interface {v15}, Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;->getSession()Lcom/samsung/android/app/memo/Session;

    move-result-object v15

    const/16 v16, 0x0

    invoke-virtual/range {v15 .. v16}, Lcom/samsung/android/app/memo/Session;->setVoiceRecordedFile(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v15

    iput-object v15, v14, Lcom/samsung/android/app/memo/voice/VoiceInfo;->mUri:Landroid/net/Uri;

    .line 1267
    :cond_b
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$5;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    # getter for: Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mVoiceinfo:Lcom/samsung/android/app/memo/voice/VoiceInfo;
    invoke-static {v14}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->access$0(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)Lcom/samsung/android/app/memo/voice/VoiceInfo;

    move-result-object v14

    if-nez v14, :cond_c

    .line 1268
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$5;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    new-instance v15, Lcom/samsung/android/app/memo/voice/VoiceInfo;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$5;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    move-object/from16 v16, v0

    # getter for: Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mFragmentAction:Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;
    invoke-static/range {v16 .. v16}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->access$12(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;

    move-result-object v16

    invoke-interface/range {v16 .. v16}, Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;->getSession()Lcom/samsung/android/app/memo/Session;

    move-result-object v16

    const/16 v17, 0x0

    invoke-virtual/range {v16 .. v17}, Lcom/samsung/android/app/memo/Session;->setVoiceRecordedFile(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v16

    invoke-direct/range {v15 .. v16}, Lcom/samsung/android/app/memo/voice/VoiceInfo;-><init>(Landroid/net/Uri;)V

    invoke-static {v14, v15}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->access$3(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;Lcom/samsung/android/app/memo/voice/VoiceInfo;)V
    :try_end_0
    .catch Lcom/samsung/android/app/memo/Session$SessionException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1273
    :cond_c
    :goto_2
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$5;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    # getter for: Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mHandler:Landroid/os/Handler;
    invoke-static {v14}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->access$13(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)Landroid/os/Handler;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$5;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    # getter for: Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->startRecording:Ljava/lang/Runnable;
    invoke-static {v15}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->access$14(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)Ljava/lang/Runnable;

    move-result-object v15

    invoke-virtual {v14, v15}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 1274
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$5;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    # getter for: Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mRecordingPauseButton:Landroid/widget/ImageButton;
    invoke-static {v14}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->access$15(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)Landroid/widget/ImageButton;

    move-result-object v14

    invoke-virtual {v14}, Landroid/widget/ImageButton;->requestFocus()Z

    .line 1275
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$5;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    iget v14, v14, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->batteryPercentage:F

    const/4 v15, 0x0

    cmpl-float v14, v14, v15

    if-lez v14, :cond_d

    .line 1276
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$5;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    const/4 v15, 0x1

    # invokes: Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->updateBatteryReceiver(Z)V
    invoke-static {v14, v15}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->access$16(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;Z)V

    .line 1277
    :cond_d
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$5;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    const/4 v15, 0x1

    # invokes: Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->updateCameraReceiver(Z)V
    invoke-static {v14, v15}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->access$17(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;Z)V

    .line 1278
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$5;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    const/4 v15, 0x1

    # invokes: Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->updateKnoxSwitchReceiver(Z)V
    invoke-static {v14, v15}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->access$18(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;Z)V

    .line 1279
    const/4 v14, 0x1

    sput-boolean v14, Lcom/samsung/android/app/memo/voice/VRUtils;->isRecording:Z

    goto/16 :goto_0

    .line 1257
    :cond_e
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$5;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    const v15, 0x7f0b0098

    invoke-virtual {v14, v15}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->showToast(I)V

    goto/16 :goto_0

    .line 1270
    :catch_0
    move-exception v12

    .line 1271
    .local v12, "se":Lcom/samsung/android/app/memo/Session$SessionException;
    const-string v14, "Voice"

    const-string v15, "recording fail"

    invoke-static {v14, v15, v12}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_2

    .line 1282
    .end local v2    # "batteryStatus":Landroid/content/Intent;
    .end local v7    # "isCharging":Z
    .end local v8    # "level":I
    .end local v10    # "restrictionPolicy":Landroid/sec/enterprise/RestrictionPolicy;
    .end local v11    # "scale":I
    .end local v12    # "se":Lcom/samsung/android/app/memo/Session$SessionException;
    :sswitch_1
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$5;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    invoke-virtual {v14}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->handleRecordPause()V

    .line 1283
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$5;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    # getter for: Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mRemoteViews:Landroid/widget/RemoteViews;
    invoke-static {v14}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->access$8(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)Landroid/widget/RemoteViews;

    move-result-object v14

    if-eqz v14, :cond_0

    .line 1284
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$5;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    # getter for: Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mRemoteViews:Landroid/widget/RemoteViews;
    invoke-static {v14}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->access$8(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)Landroid/widget/RemoteViews;

    move-result-object v14

    const v15, 0x7f0e006a

    .line 1285
    const/16 v16, 0x0

    .line 1284
    invoke-virtual/range {v14 .. v16}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 1286
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$5;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    # getter for: Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mRemoteViews:Landroid/widget/RemoteViews;
    invoke-static {v14}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->access$8(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)Landroid/widget/RemoteViews;

    move-result-object v14

    const v15, 0x7f0e0068

    .line 1287
    const/16 v16, 0x8

    .line 1286
    invoke-virtual/range {v14 .. v16}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    goto/16 :goto_0

    .line 1291
    :sswitch_2
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$5;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    invoke-virtual {v14}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->handleRecordingStop()V

    .line 1292
    const/4 v14, 0x0

    sput-boolean v14, Lcom/samsung/android/app/memo/voice/VRUtils;->isRecording:Z

    goto/16 :goto_0

    .line 1296
    :sswitch_3
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$5;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    const/4 v15, 0x1

    # invokes: Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->updateAlarmReceiver(Z)V
    invoke-static {v14, v15}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->access$19(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;Z)V

    .line 1297
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$5;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    const/4 v15, 0x1

    # invokes: Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->updateEarphoneReceiver(Z)V
    invoke-static {v14, v15}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->access$20(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;Z)V

    .line 1298
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$5;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    # getter for: Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mVoiceManger:Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;
    invoke-static {v14}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->access$9(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$5;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    # getter for: Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mContext:Landroid/content/Context;
    invoke-static {v15}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->access$1(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)Landroid/content/Context;

    move-result-object v15

    invoke-virtual {v14, v15}, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->isRecordActive(Landroid/content/Context;)Z

    move-result v14

    if-eqz v14, :cond_f

    .line 1299
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$5;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    const v15, 0x7f0b0094

    invoke-virtual {v14, v15}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->showToast(I)V

    goto/16 :goto_0

    .line 1302
    :cond_f
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$5;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    # invokes: Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->fileCheck()Z
    invoke-static {v14}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->access$21(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)Z

    move-result v14

    if-nez v14, :cond_10

    .line 1303
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$5;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    # invokes: Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->initLayout()V
    invoke-static {v14}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->access$22(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)V

    goto/16 :goto_0

    .line 1306
    :cond_10
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$5;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    # getter for: Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mVoiceManger:Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;
    invoke-static {v14}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->access$9(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;

    move-result-object v14

    invoke-virtual {v14}, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->isNowRecording()Z

    move-result v14

    if-eqz v14, :cond_11

    .line 1307
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$5;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    # getter for: Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mVoiceManger:Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;
    invoke-static {v14}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->access$9(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;

    move-result-object v14

    invoke-virtual {v14}, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->stopRecording()Lcom/samsung/android/app/memo/voice/VoiceInfo;

    .line 1309
    :cond_11
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$5;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    # getter for: Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mVoiceManger:Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;
    invoke-static {v14}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->access$9(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;

    move-result-object v14

    invoke-virtual {v14}, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->getPlayerState()I

    move-result v13

    .line 1310
    .local v13, "state":I
    packed-switch v13, :pswitch_data_0

    .line 1364
    :cond_12
    :goto_3
    :pswitch_0
    const/4 v14, 0x0

    sput-boolean v14, Lcom/samsung/android/app/memo/voice/VRUtils;->isRecording:Z

    goto/16 :goto_0

    .line 1312
    :pswitch_1
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$5;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    # getter for: Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mVoiceManger:Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;
    invoke-static {v14}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->access$9(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;

    move-result-object v14

    invoke-virtual {v14}, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->requestAudioFocus()Z

    move-result v14

    if-eqz v14, :cond_13

    .line 1313
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$5;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    const v15, 0x7f0b0093

    invoke-virtual {v14, v15}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->showToast(I)V

    goto/16 :goto_0

    .line 1316
    :cond_13
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$5;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    # getter for: Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mRemoteViews:Landroid/widget/RemoteViews;
    invoke-static {v14}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->access$8(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)Landroid/widget/RemoteViews;

    move-result-object v14

    if-eqz v14, :cond_14

    .line 1317
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$5;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    # getter for: Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mRemoteViews:Landroid/widget/RemoteViews;
    invoke-static {v14}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->access$8(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)Landroid/widget/RemoteViews;

    move-result-object v14

    const v15, 0x7f0e006a

    .line 1318
    const/16 v16, 0x0

    .line 1317
    invoke-virtual/range {v14 .. v16}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 1319
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$5;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    # getter for: Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mRemoteViews:Landroid/widget/RemoteViews;
    invoke-static {v14}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->access$8(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)Landroid/widget/RemoteViews;

    move-result-object v14

    const v15, 0x7f0e0069

    .line 1320
    const/16 v16, 0x8

    .line 1319
    invoke-virtual/range {v14 .. v16}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 1321
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$5;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    # getter for: Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mRemoteViews:Landroid/widget/RemoteViews;
    invoke-static {v14}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->access$8(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)Landroid/widget/RemoteViews;

    move-result-object v14

    const v15, 0x7f0e0068

    .line 1322
    const/16 v16, 0x8

    .line 1321
    invoke-virtual/range {v14 .. v16}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 1324
    :cond_14
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$5;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    const/4 v15, 0x0

    invoke-static {v14, v15}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->access$10(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;Z)V

    .line 1325
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$5;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    # getter for: Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mVoiceManger:Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;
    invoke-static {v14}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->access$9(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$5;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    # getter for: Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mVoiceinfo:Lcom/samsung/android/app/memo/voice/VoiceInfo;
    invoke-static {v15}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->access$0(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)Lcom/samsung/android/app/memo/voice/VoiceInfo;

    move-result-object v15

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$5;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->playerCompletionListener:Landroid/media/MediaPlayer$OnCompletionListener;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$5;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->playerErrorListener:Landroid/media/MediaPlayer$OnErrorListener;

    move-object/from16 v17, v0

    .line 1326
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$5;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mSoundListener:Lcom/samsung/android/app/memo/voice/VoiceRecorderBase$SoundListener;

    move-object/from16 v18, v0

    .line 1325
    invoke-virtual/range {v14 .. v18}, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->playSound(Lcom/samsung/android/app/memo/voice/VoiceInfo;Landroid/media/MediaPlayer$OnCompletionListener;Landroid/media/MediaPlayer$OnErrorListener;Lcom/samsung/android/app/memo/voice/VoiceRecorderBase$SoundListener;)V

    .line 1327
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$5;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    # getter for: Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mPlayingButton:Landroid/widget/ImageButton;
    invoke-static {v14}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->access$7(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)Landroid/widget/ImageButton;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$5;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    # getter for: Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mContext:Landroid/content/Context;
    invoke-static {v15}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->access$1(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)Landroid/content/Context;

    move-result-object v15

    invoke-virtual {v15}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v15

    const v16, 0x7f0b00a5

    invoke-virtual/range {v15 .. v16}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1328
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$5;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    # getter for: Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mPlayingPauseButton:Landroid/widget/ImageButton;
    invoke-static {v14}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->access$23(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)Landroid/widget/ImageButton;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$5;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    # getter for: Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mContext:Landroid/content/Context;
    invoke-static {v15}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->access$1(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)Landroid/content/Context;

    move-result-object v15

    invoke-virtual {v15}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v15

    const v16, 0x7f0b00a6

    invoke-virtual/range {v15 .. v16}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1329
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$5;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    invoke-virtual {v14}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->setPlayingLayout()V

    .line 1330
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$5;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    const/4 v15, 0x1

    # invokes: Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->updateKnoxSwitchReceiver(Z)V
    invoke-static {v14, v15}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->access$18(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;Z)V

    goto/16 :goto_3

    .line 1333
    :pswitch_2
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$5;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    # getter for: Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mRemoteViews:Landroid/widget/RemoteViews;
    invoke-static {v14}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->access$8(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)Landroid/widget/RemoteViews;

    move-result-object v14

    if-eqz v14, :cond_15

    .line 1334
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$5;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    # getter for: Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mRemoteViews:Landroid/widget/RemoteViews;
    invoke-static {v14}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->access$8(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)Landroid/widget/RemoteViews;

    move-result-object v14

    const v15, 0x7f0e006a

    .line 1335
    const/16 v16, 0x8

    .line 1334
    invoke-virtual/range {v14 .. v16}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 1339
    :cond_15
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$5;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    # getter for: Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mVoiceManger:Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;
    invoke-static {v14}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->access$9(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;

    move-result-object v14

    const/4 v15, 0x0

    invoke-virtual {v14, v15}, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->pauseSound(Z)V

    .line 1340
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$5;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    # getter for: Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mPlayingButton:Landroid/widget/ImageButton;
    invoke-static {v14}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->access$7(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)Landroid/widget/ImageButton;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$5;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    # getter for: Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mContext:Landroid/content/Context;
    invoke-static {v15}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->access$1(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)Landroid/content/Context;

    move-result-object v15

    invoke-virtual {v15}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v15

    const v16, 0x7f0b00a4

    invoke-virtual/range {v15 .. v16}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1341
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$5;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    # getter for: Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mPlayingButton:Landroid/widget/ImageButton;
    invoke-static {v14}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->access$7(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)Landroid/widget/ImageButton;

    move-result-object v14

    const v15, 0x7f0200ad

    invoke-virtual {v14, v15}, Landroid/widget/ImageButton;->setImageResource(I)V

    goto/16 :goto_3

    .line 1344
    :pswitch_3
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$5;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    # getter for: Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mRemoteViews:Landroid/widget/RemoteViews;
    invoke-static {v14}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->access$8(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)Landroid/widget/RemoteViews;

    move-result-object v14

    if-eqz v14, :cond_16

    .line 1345
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$5;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    # getter for: Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mRemoteViews:Landroid/widget/RemoteViews;
    invoke-static {v14}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->access$8(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)Landroid/widget/RemoteViews;

    move-result-object v14

    const v15, 0x7f0e006a

    .line 1346
    const/16 v16, 0x0

    .line 1345
    invoke-virtual/range {v14 .. v16}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 1347
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$5;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    # getter for: Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mRemoteViews:Landroid/widget/RemoteViews;
    invoke-static {v14}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->access$8(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)Landroid/widget/RemoteViews;

    move-result-object v14

    const v15, 0x7f0e0069

    .line 1348
    const/16 v16, 0x8

    .line 1347
    invoke-virtual/range {v14 .. v16}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 1350
    :cond_16
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$5;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    # getter for: Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mVoiceManger:Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;
    invoke-static {v14}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->access$9(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;

    move-result-object v14

    invoke-virtual {v14}, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->requestAudioFocus()Z

    move-result v14

    if-eqz v14, :cond_17

    .line 1351
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$5;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    const v15, 0x7f0b0093

    invoke-virtual {v14, v15}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->showToast(I)V

    goto/16 :goto_0

    .line 1354
    :cond_17
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$5;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    # getter for: Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mPlayingButton:Landroid/widget/ImageButton;
    invoke-static {v14}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->access$7(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)Landroid/widget/ImageButton;

    move-result-object v14

    const v15, 0x7f0200ac

    invoke-virtual {v14, v15}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 1355
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$5;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    # getter for: Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mPlayingButton:Landroid/widget/ImageButton;
    invoke-static {v14}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->access$7(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)Landroid/widget/ImageButton;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$5;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    # getter for: Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mContext:Landroid/content/Context;
    invoke-static {v15}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->access$1(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)Landroid/content/Context;

    move-result-object v15

    invoke-virtual {v15}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v15

    const v16, 0x7f0b00a5

    invoke-virtual/range {v15 .. v16}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1356
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$5;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    # getter for: Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mVoiceManger:Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;
    invoke-static {v14}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->access$9(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;

    move-result-object v14

    invoke-virtual {v14}, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->isNowRecording()Z

    move-result v14

    if-nez v14, :cond_12

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$5;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    # getter for: Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mVoiceManger:Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;
    invoke-static {v14}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->access$9(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;

    move-result-object v14

    invoke-virtual {v14}, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->isNowPlaying()Z

    move-result v14

    if-nez v14, :cond_12

    .line 1357
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$5;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    # getter for: Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mVoiceManger:Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;
    invoke-static {v14}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->access$9(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$5;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    # getter for: Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mVoiceinfo:Lcom/samsung/android/app/memo/voice/VoiceInfo;
    invoke-static {v15}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->access$0(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)Lcom/samsung/android/app/memo/voice/VoiceInfo;

    move-result-object v15

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$5;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->playerCompletionListener:Landroid/media/MediaPlayer$OnCompletionListener;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$5;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->playerErrorListener:Landroid/media/MediaPlayer$OnErrorListener;

    move-object/from16 v17, v0

    .line 1358
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$5;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mSoundListener:Lcom/samsung/android/app/memo/voice/VoiceRecorderBase$SoundListener;

    move-object/from16 v18, v0

    .line 1357
    invoke-virtual/range {v14 .. v18}, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->playSound(Lcom/samsung/android/app/memo/voice/VoiceInfo;Landroid/media/MediaPlayer$OnCompletionListener;Landroid/media/MediaPlayer$OnErrorListener;Lcom/samsung/android/app/memo/voice/VoiceRecorderBase$SoundListener;)V

    goto/16 :goto_3

    .line 1368
    .end local v13    # "state":I
    :sswitch_4
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$5;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    invoke-virtual {v14}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->setReadyToPlayLayout()V

    .line 1369
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$5;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    # getter for: Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mPlayingButton:Landroid/widget/ImageButton;
    invoke-static {v14}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->access$7(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)Landroid/widget/ImageButton;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$5;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    # getter for: Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mContext:Landroid/content/Context;
    invoke-static {v15}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->access$1(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)Landroid/content/Context;

    move-result-object v15

    invoke-virtual {v15}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v15

    const v16, 0x7f0b00a4

    invoke-virtual/range {v15 .. v16}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1370
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$5;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    # getter for: Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mPlayingButton:Landroid/widget/ImageButton;
    invoke-static {v14}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->access$7(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)Landroid/widget/ImageButton;

    move-result-object v14

    invoke-virtual {v14}, Landroid/widget/ImageButton;->requestFocus()Z

    .line 1371
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$5;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    # getter for: Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mVoiceManger:Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;
    invoke-static {v14}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->access$9(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;

    move-result-object v14

    invoke-virtual {v14}, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->stopSoundPlay()V

    .line 1372
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$5;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    # getter for: Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mPlayingSeekbar:Landroid/widget/SeekBar;
    invoke-static {v14}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->access$6(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)Landroid/widget/SeekBar;

    move-result-object v14

    const/4 v15, 0x0

    invoke-virtual {v14, v15}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 1373
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$5;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    # getter for: Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mPlayingSeekbar:Landroid/widget/SeekBar;
    invoke-static {v14}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->access$6(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)Landroid/widget/SeekBar;

    move-result-object v14

    const/16 v15, 0x8

    invoke-virtual {v14, v15}, Landroid/widget/SeekBar;->setVisibility(I)V

    .line 1374
    const/4 v14, 0x0

    sput-boolean v14, Lcom/samsung/android/app/memo/voice/VRUtils;->isRecording:Z

    goto/16 :goto_0

    .line 1377
    :sswitch_5
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    .line 1378
    .local v4, "currentTouchTime":J
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$5;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    const/4 v15, 0x0

    invoke-virtual {v14, v15}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->setVoiceViewLayoutVisible(Z)V

    .line 1379
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$5;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    # getter for: Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->lastNewButtonClickTime:J
    invoke-static {v14}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->access$24(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)J

    move-result-wide v14

    sub-long v14, v4, v14

    const-wide/16 v16, 0x1f4

    cmp-long v14, v14, v16

    if-lez v14, :cond_19

    .line 1380
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$5;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    # getter for: Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mVoiceinfo:Lcom/samsung/android/app/memo/voice/VoiceInfo;
    invoke-static {v14}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->access$0(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)Lcom/samsung/android/app/memo/voice/VoiceInfo;

    move-result-object v14

    if-eqz v14, :cond_18

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$5;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    # getter for: Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mVoiceinfo:Lcom/samsung/android/app/memo/voice/VoiceInfo;
    invoke-static {v14}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->access$0(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)Lcom/samsung/android/app/memo/voice/VoiceInfo;

    move-result-object v14

    iget-object v14, v14, Lcom/samsung/android/app/memo/voice/VoiceInfo;->mUri:Landroid/net/Uri;

    if-nez v14, :cond_1a

    .line 1382
    :cond_18
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$5;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    # getter for: Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mMainView:Landroid/view/View;
    invoke-static {v14}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->access$25(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)Landroid/view/View;

    move-result-object v14

    const/16 v15, 0x8

    invoke-virtual {v14, v15}, Landroid/view/View;->setVisibility(I)V

    .line 1393
    :cond_19
    :goto_4
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$5;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v16

    move-wide/from16 v0, v16

    invoke-static {v14, v0, v1}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->access$29(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;J)V

    .line 1394
    const/4 v14, 0x0

    sput-boolean v14, Lcom/samsung/android/app/memo/voice/VRUtils;->isRecording:Z

    .line 1395
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$5;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    # getter for: Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mContentHandler:Landroid/os/Handler;
    invoke-static {v14}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->access$28(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)Landroid/os/Handler;

    move-result-object v14

    const/16 v15, 0xfcc

    invoke-virtual {v14, v15}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto/16 :goto_0

    .line 1385
    :cond_1a
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$5;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    # getter for: Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mDialog:Landroid/app/Dialog;
    invoke-static {v14}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->access$26(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)Landroid/app/Dialog;

    move-result-object v14

    if-eqz v14, :cond_1b

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$5;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    # getter for: Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mDialog:Landroid/app/Dialog;
    invoke-static {v14}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->access$26(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)Landroid/app/Dialog;

    move-result-object v14

    invoke-virtual {v14}, Landroid/app/Dialog;->isShowing()Z

    move-result v14

    if-eqz v14, :cond_1b

    .line 1386
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$5;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    const/4 v15, 0x0

    invoke-static {v14, v15}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->access$27(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;Landroid/app/Dialog;)V

    .line 1388
    :cond_1b
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$5;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    # getter for: Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mContentHandler:Landroid/os/Handler;
    invoke-static {v14}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->access$28(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)Landroid/os/Handler;

    move-result-object v14

    const/16 v15, 0xfca

    invoke-virtual {v14, v15}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 1389
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$5;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    const/4 v15, 0x1

    invoke-virtual {v14, v15}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->setVoiceViewLayoutVisible(Z)V

    .line 1390
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$5;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    invoke-virtual {v14}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->showDialog()V

    goto :goto_4

    .line 1204
    :sswitch_data_0
    .sparse-switch
        0x7f0e006f -> :sswitch_0
        0x7f0e0072 -> :sswitch_1
        0x7f0e0073 -> :sswitch_2
        0x7f0e0076 -> :sswitch_3
        0x7f0e0078 -> :sswitch_4
        0x7f0e007e -> :sswitch_5
    .end sparse-switch

    .line 1310
    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
