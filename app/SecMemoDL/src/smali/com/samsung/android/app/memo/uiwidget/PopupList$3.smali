.class Lcom/samsung/android/app/memo/uiwidget/PopupList$3;
.super Ljava/lang/Object;
.source "PopupList.java"

# interfaces
.implements Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/memo/uiwidget/PopupList;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/memo/uiwidget/PopupList;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/memo/uiwidget/PopupList;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsung/android/app/memo/uiwidget/PopupList$3;->this$0:Lcom/samsung/android/app/memo/uiwidget/PopupList;

    .line 129
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onGlobalLayout()V
    .locals 6

    .prologue
    .line 132
    iget-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/PopupList$3;->this$0:Lcom/samsung/android/app/memo/uiwidget/PopupList;

    # getter for: Lcom/samsung/android/app/memo/uiwidget/PopupList;->mPopupWindow:Landroid/widget/PopupWindow;
    invoke-static {v0}, Lcom/samsung/android/app/memo/uiwidget/PopupList;->access$0(Lcom/samsung/android/app/memo/uiwidget/PopupList;)Landroid/widget/PopupWindow;

    move-result-object v0

    if-nez v0, :cond_0

    .line 139
    :goto_0
    return-void

    .line 135
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/PopupList$3;->this$0:Lcom/samsung/android/app/memo/uiwidget/PopupList;

    # invokes: Lcom/samsung/android/app/memo/uiwidget/PopupList;->updatePopupLayoutParams()V
    invoke-static {v0}, Lcom/samsung/android/app/memo/uiwidget/PopupList;->access$5(Lcom/samsung/android/app/memo/uiwidget/PopupList;)V

    .line 137
    iget-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/PopupList$3;->this$0:Lcom/samsung/android/app/memo/uiwidget/PopupList;

    # getter for: Lcom/samsung/android/app/memo/uiwidget/PopupList;->mPopupWindow:Landroid/widget/PopupWindow;
    invoke-static {v0}, Lcom/samsung/android/app/memo/uiwidget/PopupList;->access$0(Lcom/samsung/android/app/memo/uiwidget/PopupList;)Landroid/widget/PopupWindow;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/app/memo/uiwidget/PopupList$3;->this$0:Lcom/samsung/android/app/memo/uiwidget/PopupList;

    # getter for: Lcom/samsung/android/app/memo/uiwidget/PopupList;->mAnchorView:Landroid/view/View;
    invoke-static {v1}, Lcom/samsung/android/app/memo/uiwidget/PopupList;->access$2(Lcom/samsung/android/app/memo/uiwidget/PopupList;)Landroid/view/View;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/app/memo/uiwidget/PopupList$3;->this$0:Lcom/samsung/android/app/memo/uiwidget/PopupList;

    # getter for: Lcom/samsung/android/app/memo/uiwidget/PopupList;->mPopupOffsetX:I
    invoke-static {v2}, Lcom/samsung/android/app/memo/uiwidget/PopupList;->access$6(Lcom/samsung/android/app/memo/uiwidget/PopupList;)I

    move-result v2

    iget-object v3, p0, Lcom/samsung/android/app/memo/uiwidget/PopupList$3;->this$0:Lcom/samsung/android/app/memo/uiwidget/PopupList;

    # getter for: Lcom/samsung/android/app/memo/uiwidget/PopupList;->mPopupOffsetY:I
    invoke-static {v3}, Lcom/samsung/android/app/memo/uiwidget/PopupList;->access$7(Lcom/samsung/android/app/memo/uiwidget/PopupList;)I

    move-result v3

    iget-object v4, p0, Lcom/samsung/android/app/memo/uiwidget/PopupList$3;->this$0:Lcom/samsung/android/app/memo/uiwidget/PopupList;

    # getter for: Lcom/samsung/android/app/memo/uiwidget/PopupList;->mPopupWidth:I
    invoke-static {v4}, Lcom/samsung/android/app/memo/uiwidget/PopupList;->access$8(Lcom/samsung/android/app/memo/uiwidget/PopupList;)I

    move-result v4

    .line 138
    iget-object v5, p0, Lcom/samsung/android/app/memo/uiwidget/PopupList$3;->this$0:Lcom/samsung/android/app/memo/uiwidget/PopupList;

    # getter for: Lcom/samsung/android/app/memo/uiwidget/PopupList;->mPopupHeight:I
    invoke-static {v5}, Lcom/samsung/android/app/memo/uiwidget/PopupList;->access$9(Lcom/samsung/android/app/memo/uiwidget/PopupList;)I

    move-result v5

    .line 137
    invoke-virtual/range {v0 .. v5}, Landroid/widget/PopupWindow;->update(Landroid/view/View;IIII)V

    goto :goto_0
.end method
