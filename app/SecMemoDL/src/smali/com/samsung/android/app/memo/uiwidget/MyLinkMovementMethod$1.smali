.class Lcom/samsung/android/app/memo/uiwidget/MyLinkMovementMethod$1;
.super Ljava/lang/Object;
.source "RichEditor.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/app/memo/uiwidget/MyLinkMovementMethod;->onTouchEvent(Landroid/widget/TextView;Landroid/text/Spannable;Landroid/view/MotionEvent;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/memo/uiwidget/MyLinkMovementMethod;

.field private final synthetic val$spans:[Lcom/samsung/android/app/memo/uiwidget/ClickableSpanType;

.field private final synthetic val$touchPointX:F


# direct methods
.method constructor <init>(Lcom/samsung/android/app/memo/uiwidget/MyLinkMovementMethod;[Lcom/samsung/android/app/memo/uiwidget/ClickableSpanType;F)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsung/android/app/memo/uiwidget/MyLinkMovementMethod$1;->this$0:Lcom/samsung/android/app/memo/uiwidget/MyLinkMovementMethod;

    iput-object p2, p0, Lcom/samsung/android/app/memo/uiwidget/MyLinkMovementMethod$1;->val$spans:[Lcom/samsung/android/app/memo/uiwidget/ClickableSpanType;

    iput p3, p0, Lcom/samsung/android/app/memo/uiwidget/MyLinkMovementMethod$1;->val$touchPointX:F

    .line 2251
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .prologue
    .line 2254
    iget-object v3, p0, Lcom/samsung/android/app/memo/uiwidget/MyLinkMovementMethod$1;->val$spans:[Lcom/samsung/android/app/memo/uiwidget/ClickableSpanType;

    array-length v4, v3

    const/4 v2, 0x0

    :goto_0
    if-lt v2, v4, :cond_0

    .line 2266
    return-void

    .line 2254
    :cond_0
    aget-object v1, v3, v2

    .line 2255
    .local v1, "span":Lcom/samsung/android/app/memo/uiwidget/ClickableSpanType;
    instance-of v5, v1, Lcom/samsung/android/app/memo/uiwidget/HtmlImageSpan;

    if-eqz v5, :cond_2

    iget-object v5, p0, Lcom/samsung/android/app/memo/uiwidget/MyLinkMovementMethod$1;->this$0:Lcom/samsung/android/app/memo/uiwidget/MyLinkMovementMethod;

    # getter for: Lcom/samsung/android/app/memo/uiwidget/MyLinkMovementMethod;->ViewMode:Z
    invoke-static {v5}, Lcom/samsung/android/app/memo/uiwidget/MyLinkMovementMethod;->access$0(Lcom/samsung/android/app/memo/uiwidget/MyLinkMovementMethod;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 2254
    :cond_1
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_2
    move-object v0, v1

    .line 2257
    check-cast v0, Lcom/samsung/android/app/memo/uiwidget/HtmlImageSpan;

    .line 2258
    .local v0, "imgSpan":Lcom/samsung/android/app/memo/uiwidget/HtmlImageSpan;
    instance-of v5, v0, Lcom/samsung/android/app/memo/uiwidget/HtmlImageSpan;

    if-eqz v5, :cond_3

    .line 2259
    iget v5, p0, Lcom/samsung/android/app/memo/uiwidget/MyLinkMovementMethod$1;->val$touchPointX:F

    invoke-virtual {v0}, Lcom/samsung/android/app/memo/uiwidget/HtmlImageSpan;->getLeftBound()I

    move-result v6

    add-int/lit8 v6, v6, 0x1e

    int-to-float v6, v6

    cmpg-float v5, v5, v6

    if-ltz v5, :cond_1

    iget v5, p0, Lcom/samsung/android/app/memo/uiwidget/MyLinkMovementMethod$1;->val$touchPointX:F

    .line 2260
    invoke-virtual {v0}, Lcom/samsung/android/app/memo/uiwidget/HtmlImageSpan;->getLeftBound()I

    move-result v6

    .line 2261
    sget v7, Lcom/samsung/android/app/memo/uiwidget/HtmlImageSpan;->MAX_WIDTH:I

    add-int/2addr v6, v7

    add-int/lit8 v6, v6, -0x1e

    int-to-float v6, v6

    cmpl-float v5, v5, v6

    if-gtz v5, :cond_1

    .line 2264
    :cond_3
    invoke-interface {v1, v1}, Lcom/samsung/android/app/memo/uiwidget/ClickableSpanType;->onClick(Lcom/samsung/android/app/memo/uiwidget/ClickableSpanType;)V

    goto :goto_1
.end method
