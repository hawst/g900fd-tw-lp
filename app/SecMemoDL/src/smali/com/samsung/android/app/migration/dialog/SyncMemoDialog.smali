.class public Lcom/samsung/android/app/migration/dialog/SyncMemoDialog;
.super Landroid/app/DialogFragment;
.source "SyncMemoDialog.java"


# static fields
.field public static TAG:Ljava/lang/String;


# instance fields
.field private mDialog:Landroid/app/AlertDialog;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 41
    const-string v0, "syncMemoDialog"

    sput-object v0, Lcom/samsung/android/app/migration/dialog/SyncMemoDialog;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    return-void
.end method

.method static synthetic access$0(Lcom/samsung/android/app/migration/dialog/SyncMemoDialog;Landroid/app/Activity;)V
    .locals 0

    .prologue
    .line 98
    invoke-direct {p0, p1}, Lcom/samsung/android/app/migration/dialog/SyncMemoDialog;->launchTMemoImport(Landroid/app/Activity;)V

    return-void
.end method

.method private isContextAlive(Landroid/app/Activity;)Z
    .locals 1
    .param p1, "context"    # Landroid/app/Activity;

    .prologue
    .line 49
    if-eqz p1, :cond_0

    invoke-static {p1}, Lcom/samsung/android/app/memo/util/AccountHelper;->getRegisteredSamsungAccount(Landroid/content/Context;)Landroid/accounts/Account;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 50
    invoke-virtual {p1}, Landroid/app/Activity;->isDestroyed()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Landroid/app/Activity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Landroid/app/Activity;->isResumed()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 51
    const/4 v0, 0x1

    .line 53
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private launchTMemoImport(Landroid/app/Activity;)V
    .locals 1
    .param p1, "ctx"    # Landroid/app/Activity;

    .prologue
    .line 99
    const/4 v0, 0x0

    invoke-static {p1, v0}, Lcom/samsung/android/app/migration/utils/SharedPref;->setTMemoImportDialogState(Landroid/content/Context;I)V

    .line 100
    const/4 v0, 0x2

    invoke-static {p1, v0}, Lcom/samsung/android/app/migration/utils/Utils;->sendRequestToSAService(Landroid/content/Context;I)V

    .line 101
    return-void
.end method

.method public static newInstance()Lcom/samsung/android/app/migration/dialog/SyncMemoDialog;
    .locals 1

    .prologue
    .line 45
    new-instance v0, Lcom/samsung/android/app/migration/dialog/SyncMemoDialog;

    invoke-direct {v0}, Lcom/samsung/android/app/migration/dialog/SyncMemoDialog;-><init>()V

    return-object v0
.end method


# virtual methods
.method public onCancel(Landroid/content/DialogInterface;)V
    .locals 2
    .param p1, "dialog"    # Landroid/content/DialogInterface;

    .prologue
    .line 66
    invoke-virtual {p0}, Lcom/samsung/android/app/migration/dialog/SyncMemoDialog;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/samsung/android/app/migration/utils/SharedPref;->setSyncMemoDialogState(Landroid/content/Context;I)V

    .line 67
    invoke-virtual {p0}, Lcom/samsung/android/app/migration/dialog/SyncMemoDialog;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/samsung/android/app/migration/dialog/SyncMemoDialog;->launchTMemoImport(Landroid/app/Activity;)V

    .line 68
    return-void
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 5
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 72
    invoke-virtual {p0}, Lcom/samsung/android/app/migration/dialog/SyncMemoDialog;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 73
    .local v0, "ctx":Landroid/app/Activity;
    invoke-virtual {v0}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v2

    const v3, 0x7f040029

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 74
    .local v1, "v":Landroid/view/View;
    new-instance v2, Landroid/app/AlertDialog$Builder;

    invoke-direct {v2, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v2, v1}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    .line 75
    const v3, 0x7f0b0005

    new-instance v4, Lcom/samsung/android/app/migration/dialog/SyncMemoDialog$1;

    invoke-direct {v4, p0, v0}, Lcom/samsung/android/app/migration/dialog/SyncMemoDialog$1;-><init>(Lcom/samsung/android/app/migration/dialog/SyncMemoDialog;Landroid/app/Activity;)V

    invoke-virtual {v2, v3, v4}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    .line 84
    const v3, 0x7f0b0047

    new-instance v4, Lcom/samsung/android/app/migration/dialog/SyncMemoDialog$2;

    invoke-direct {v4, p0, v0}, Lcom/samsung/android/app/migration/dialog/SyncMemoDialog$2;-><init>(Lcom/samsung/android/app/migration/dialog/SyncMemoDialog;Landroid/app/Activity;)V

    invoke-virtual {v2, v3, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    .line 92
    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v2

    .line 74
    iput-object v2, p0, Lcom/samsung/android/app/migration/dialog/SyncMemoDialog;->mDialog:Landroid/app/AlertDialog;

    .line 93
    iget-object v2, p0, Lcom/samsung/android/app/migration/dialog/SyncMemoDialog;->mDialog:Landroid/app/AlertDialog;

    const v3, 0x7f0b0033

    invoke-virtual {p0, v3}, Lcom/samsung/android/app/migration/dialog/SyncMemoDialog;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog;->setTitle(Ljava/lang/CharSequence;)V

    .line 94
    iget-object v2, p0, Lcom/samsung/android/app/migration/dialog/SyncMemoDialog;->mDialog:Landroid/app/AlertDialog;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog;->setCanceledOnTouchOutside(Z)V

    .line 95
    iget-object v2, p0, Lcom/samsung/android/app/migration/dialog/SyncMemoDialog;->mDialog:Landroid/app/AlertDialog;

    return-object v2
.end method

.method public show(Landroid/app/Activity;)V
    .locals 2
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 57
    invoke-direct {p0, p1}, Lcom/samsung/android/app/migration/dialog/SyncMemoDialog;->isContextAlive(Landroid/app/Activity;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 58
    const/4 v0, 0x1

    invoke-static {p1, v0}, Lcom/samsung/android/app/migration/utils/SharedPref;->setSyncMemoDialogState(Landroid/content/Context;I)V

    .line 59
    const/4 v0, 0x0

    invoke-static {p1, v0}, Lcom/samsung/android/app/migration/utils/SharedPref;->setMemoRelaunchedForSyncMemoDialogFlag(Landroid/content/Context;Z)V

    .line 60
    invoke-virtual {p1}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/app/migration/dialog/SyncMemoDialog;->TAG:Ljava/lang/String;

    invoke-virtual {p0, v0, v1}, Lcom/samsung/android/app/migration/dialog/SyncMemoDialog;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 62
    :cond_0
    return-void
.end method
