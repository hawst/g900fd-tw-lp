.class Lcom/samsung/android/app/memo/voice/VoiceViewLayout$11;
.super Landroid/content/BroadcastReceiver;
.source "VoiceViewLayout.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->updateBatteryReceiver(Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$11;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    .line 616
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 10
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const-wide v8, 0x3f947ae147ae147bL    # 0.02

    const/4 v7, -0x1

    .line 621
    if-eqz p2, :cond_1

    .line 622
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    const-string v5, "android.intent.action.BATTERY_CHANGED"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 623
    const-string v4, "level"

    invoke-virtual {p2, v4, v7}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    .line 624
    .local v2, "level":I
    const-string v4, "scale"

    invoke-virtual {p2, v4, v7}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    .line 625
    .local v3, "scale":I
    iget-object v4, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$11;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    int-to-float v5, v2

    int-to-float v6, v3

    div-float/2addr v5, v6

    iput v5, v4, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->batteryPercentage:F

    .line 626
    const-string v4, "status"

    invoke-virtual {p2, v4, v7}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 627
    .local v0, "chargingStatus":I
    const/4 v4, 0x2

    if-eq v0, v4, :cond_2

    .line 628
    const/4 v4, 0x5

    if-eq v0, v4, :cond_2

    .line 627
    const/4 v1, 0x0

    .line 630
    .local v1, "isCharging":Z
    :goto_0
    iget-object v4, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$11;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    iget v4, v4, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->batteryPercentage:F

    float-to-double v4, v4

    cmpg-double v4, v4, v8

    if-gez v4, :cond_0

    if-nez v1, :cond_0

    .line 631
    iget-object v4, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$11;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    invoke-virtual {v4}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->handleRecordingStop()V

    .line 634
    .end local v0    # "chargingStatus":I
    .end local v1    # "isCharging":Z
    .end local v2    # "level":I
    .end local v3    # "scale":I
    :cond_0
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    const-string v5, "android.intent.action.ACTION_POWER_DISCONNECTED"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$11;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    iget v4, v4, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->batteryPercentage:F

    float-to-double v4, v4

    cmpg-double v4, v4, v8

    if-gez v4, :cond_1

    .line 635
    iget-object v4, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$11;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    invoke-virtual {v4}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->handleRecordingStop()V

    .line 638
    :cond_1
    return-void

    .line 627
    .restart local v0    # "chargingStatus":I
    .restart local v2    # "level":I
    .restart local v3    # "scale":I
    :cond_2
    const/4 v1, 0x1

    goto :goto_0
.end method
