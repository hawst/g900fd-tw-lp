.class public Lcom/samsung/android/app/migration/dialog/ImportDialog;
.super Landroid/app/DialogFragment;
.source "ImportDialog.java"


# static fields
.field public static TAG:Ljava/lang/String;

.field private static mKeys:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mDialog:Landroid/app/AlertDialog;

.field private mDoNotShowAgain:Landroid/widget/CheckBox;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 44
    const-string v0, "importDialog"

    sput-object v0, Lcom/samsung/android/app/migration/dialog/ImportDialog;->TAG:Ljava/lang/String;

    .line 47
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    return-void
.end method

.method static synthetic access$0(Lcom/samsung/android/app/migration/dialog/ImportDialog;)Landroid/widget/CheckBox;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/samsung/android/app/migration/dialog/ImportDialog;->mDoNotShowAgain:Landroid/widget/CheckBox;

    return-object v0
.end method

.method static synthetic access$1()Ljava/util/List;
    .locals 1

    .prologue
    .line 47
    sget-object v0, Lcom/samsung/android/app/migration/dialog/ImportDialog;->mKeys:Ljava/util/List;

    return-object v0
.end method

.method private isContextAlive(Landroid/app/Activity;)Z
    .locals 1
    .param p1, "context"    # Landroid/app/Activity;

    .prologue
    .line 55
    if-eqz p1, :cond_0

    invoke-static {p1}, Lcom/samsung/android/app/memo/util/AccountHelper;->getRegisteredSamsungAccount(Landroid/content/Context;)Landroid/accounts/Account;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 56
    invoke-virtual {p1}, Landroid/app/Activity;->isDestroyed()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Landroid/app/Activity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Landroid/app/Activity;->isResumed()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 57
    const/4 v0, 0x1

    .line 59
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static newInstance(Ljava/util/List;)Lcom/samsung/android/app/migration/dialog/ImportDialog;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/samsung/android/app/migration/dialog/ImportDialog;"
        }
    .end annotation

    .prologue
    .line 50
    .local p0, "keys":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    sput-object p0, Lcom/samsung/android/app/migration/dialog/ImportDialog;->mKeys:Ljava/util/List;

    .line 51
    new-instance v0, Lcom/samsung/android/app/migration/dialog/ImportDialog;

    invoke-direct {v0}, Lcom/samsung/android/app/migration/dialog/ImportDialog;-><init>()V

    return-object v0
.end method


# virtual methods
.method public onCancel(Landroid/content/DialogInterface;)V
    .locals 2
    .param p1, "dialog"    # Landroid/content/DialogInterface;

    .prologue
    const/4 v1, 0x0

    .line 81
    invoke-virtual {p0}, Lcom/samsung/android/app/migration/dialog/ImportDialog;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 82
    invoke-virtual {p0}, Lcom/samsung/android/app/migration/dialog/ImportDialog;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0, v1}, Lcom/samsung/android/app/migration/utils/SharedPref;->setImportDoNotShowFlag(Landroid/content/Context;Z)V

    .line 83
    invoke-virtual {p0}, Lcom/samsung/android/app/migration/dialog/ImportDialog;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0, v1}, Lcom/samsung/android/app/migration/utils/SharedPref;->setTMemoImportDialogState(Landroid/content/Context;I)V

    .line 85
    :cond_0
    return-void
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 6
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 89
    invoke-virtual {p0}, Lcom/samsung/android/app/migration/dialog/ImportDialog;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 90
    .local v0, "ctx":Landroid/app/Activity;
    invoke-virtual {v0}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v3

    const v4, 0x7f040016

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 91
    .local v2, "v":Landroid/view/View;
    const v3, 0x7f0e0020

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 92
    .local v1, "tv":Landroid/widget/TextView;
    const v3, 0x7f0e0021

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/CheckBox;

    iput-object v3, p0, Lcom/samsung/android/app/migration/dialog/ImportDialog;->mDoNotShowAgain:Landroid/widget/CheckBox;

    .line 93
    iget-object v3, p0, Lcom/samsung/android/app/migration/dialog/ImportDialog;->mDoNotShowAgain:Landroid/widget/CheckBox;

    new-instance v4, Lcom/samsung/android/app/migration/dialog/ImportDialog$2;

    invoke-direct {v4, p0}, Lcom/samsung/android/app/migration/dialog/ImportDialog$2;-><init>(Lcom/samsung/android/app/migration/dialog/ImportDialog;)V

    invoke-virtual {v3, v4}, Landroid/widget/CheckBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 99
    iget-object v3, p0, Lcom/samsung/android/app/migration/dialog/ImportDialog;->mDoNotShowAgain:Landroid/widget/CheckBox;

    new-instance v4, Lcom/samsung/android/app/migration/dialog/ImportDialog$3;

    invoke-direct {v4, p0, v1}, Lcom/samsung/android/app/migration/dialog/ImportDialog$3;-><init>(Lcom/samsung/android/app/migration/dialog/ImportDialog;Landroid/widget/TextView;)V

    invoke-virtual {v3, v4}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 105
    new-instance v3, Landroid/app/AlertDialog$Builder;

    invoke-direct {v3, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v3, v2}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    .line 106
    const v4, 0x7f0b0030

    new-instance v5, Lcom/samsung/android/app/migration/dialog/ImportDialog$4;

    invoke-direct {v5, p0, v0}, Lcom/samsung/android/app/migration/dialog/ImportDialog$4;-><init>(Lcom/samsung/android/app/migration/dialog/ImportDialog;Landroid/app/Activity;)V

    invoke-virtual {v3, v4, v5}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    .line 115
    const v4, 0x7f0b002f

    new-instance v5, Lcom/samsung/android/app/migration/dialog/ImportDialog$5;

    invoke-direct {v5, p0, v0}, Lcom/samsung/android/app/migration/dialog/ImportDialog$5;-><init>(Lcom/samsung/android/app/migration/dialog/ImportDialog;Landroid/app/Activity;)V

    invoke-virtual {v3, v4, v5}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    .line 123
    invoke-virtual {v3}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v3

    .line 105
    iput-object v3, p0, Lcom/samsung/android/app/migration/dialog/ImportDialog;->mDialog:Landroid/app/AlertDialog;

    .line 124
    iget-object v3, p0, Lcom/samsung/android/app/migration/dialog/ImportDialog;->mDialog:Landroid/app/AlertDialog;

    const v4, 0x7f0b002b

    invoke-virtual {p0, v4}, Lcom/samsung/android/app/migration/dialog/ImportDialog;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/app/AlertDialog;->setTitle(Ljava/lang/CharSequence;)V

    .line 125
    iget-object v3, p0, Lcom/samsung/android/app/migration/dialog/ImportDialog;->mDialog:Landroid/app/AlertDialog;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/app/AlertDialog;->setCanceledOnTouchOutside(Z)V

    .line 126
    iget-object v3, p0, Lcom/samsung/android/app/migration/dialog/ImportDialog;->mDialog:Landroid/app/AlertDialog;

    return-object v3
.end method

.method public show(Landroid/app/Activity;)V
    .locals 3
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    const/4 v2, 0x0

    .line 63
    invoke-direct {p0, p1}, Lcom/samsung/android/app/migration/dialog/ImportDialog;->isContextAlive(Landroid/app/Activity;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 64
    invoke-static {p1}, Lcom/samsung/android/app/migration/utils/SharedPref;->getTMemoImportDialogState(Landroid/content/Context;)I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 65
    new-instance v0, Lcom/samsung/android/app/migration/dialog/ImportDialog$1;

    invoke-direct {v0, p0, p1}, Lcom/samsung/android/app/migration/dialog/ImportDialog$1;-><init>(Lcom/samsung/android/app/migration/dialog/ImportDialog;Landroid/app/Activity;)V

    invoke-virtual {p1, v0}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 71
    const/4 v0, 0x2

    invoke-static {p1, v0}, Lcom/samsung/android/app/migration/utils/SharedPref;->setTMemoImportDialogState(Landroid/content/Context;I)V

    .line 72
    invoke-static {p1, v2}, Lcom/samsung/android/app/migration/utils/SharedPref;->setMemoRelaunchedForTMemoImportDialogFlag(Landroid/content/Context;Z)V

    .line 77
    :cond_0
    :goto_0
    return-void

    .line 75
    :cond_1
    invoke-static {p1, v2}, Lcom/samsung/android/app/migration/utils/SharedPref;->setTMemoImportDialogState(Landroid/content/Context;I)V

    goto :goto_0
.end method
