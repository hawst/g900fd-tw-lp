.class public Lcom/samsung/android/app/memo/MemoDataHandler;
.super Ljava/lang/Object;
.source "MemoDataHandler.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/app/memo/MemoDataHandler$MemoDataHandlerException;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 53
    const-class v0, Lcom/samsung/android/app/memo/MemoDataHandler;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/app/memo/MemoDataHandler;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static declared-synchronized cancelDeletingMemos(Landroid/content/Context;Ljava/util/ArrayList;)V
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/android/app/memo/MemoDataHandler$MemoDataHandlerException;
        }
    .end annotation

    .prologue
    .line 66
    .local p1, "memoIds":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    const-class v2, Lcom/samsung/android/app/memo/MemoDataHandler;

    monitor-enter v2

    :try_start_0
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-nez v1, :cond_0

    .line 71
    :goto_0
    monitor-exit v2

    return-void

    .line 68
    :cond_0
    :try_start_1
    new-instance v0, Ljava/lang/StringBuffer;

    const-string v1, "_id"

    invoke-direct {v0, v1}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 69
    .local v0, "sb":Ljava/lang/StringBuffer;
    invoke-static {p0, v0, p1}, Lcom/samsung/android/app/memo/MemoDataHandler;->toSQLInStateIntList(Landroid/content/Context;Ljava/lang/StringBuffer;Ljava/util/List;)V

    .line 70
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v3, Lcom/samsung/android/provider/memo/MemoContract;->BASE_URI_MEMO_UNDO:Landroid/net/Uri;

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v1, v3, v4, v5}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 66
    .end local v0    # "sb":Ljava/lang/StringBuffer;
    :catchall_0
    move-exception v1

    monitor-exit v2

    throw v1
.end method

.method public static clearDataFor(Landroid/content/Context;Ljava/lang/String;)V
    .locals 8
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "uuid"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/android/app/memo/MemoDataHandler$MemoDataHandlerException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x1

    .line 155
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 156
    .local v2, "values":Landroid/content/ContentValues;
    const-string v3, "_data"

    const-string v4, ""

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 157
    const-string v3, "v_skipDirtySet"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 158
    new-instance v0, Ljava/lang/StringBuffer;

    const-string v3, "UUID=?"

    invoke-direct {v0, v3}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 160
    .local v0, "sb":Ljava/lang/StringBuffer;
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    sget-object v4, Lcom/samsung/android/provider/memo/MemoContract;->BASE_URI_MEMO:Landroid/net/Uri;

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    .line 161
    aput-object p1, v6, v7

    .line 160
    invoke-virtual {v3, v4, v2, v5, v6}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    .line 167
    return-void

    .line 163
    :catch_0
    move-exception v1

    .line 164
    .local v1, "sqle":Landroid/database/SQLException;
    sget-object v3, Lcom/samsung/android/app/memo/MemoDataHandler;->TAG:Ljava/lang/String;

    const-string v4, "updateFiles"

    invoke-static {v3, v4, v1}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 165
    new-instance v3, Lcom/samsung/android/app/memo/MemoDataHandler$MemoDataHandlerException;

    const-string v4, "updateFiles"

    invoke-direct {v3, v4, v1}, Lcom/samsung/android/app/memo/MemoDataHandler$MemoDataHandlerException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v3
.end method

.method public static declared-synchronized deleteMemos(Landroid/content/Context;Ljava/util/ArrayList;)V
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/android/app/memo/MemoDataHandler$MemoDataHandlerException;
        }
    .end annotation

    .prologue
    .line 75
    .local p1, "memoIds":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    const-class v2, Lcom/samsung/android/app/memo/MemoDataHandler;

    monitor-enter v2

    :try_start_0
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-nez v1, :cond_0

    .line 80
    :goto_0
    monitor-exit v2

    return-void

    .line 77
    :cond_0
    :try_start_1
    new-instance v0, Ljava/lang/StringBuffer;

    const-string v1, "_id"

    invoke-direct {v0, v1}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 78
    .local v0, "sb":Ljava/lang/StringBuffer;
    invoke-static {p0, v0, p1}, Lcom/samsung/android/app/memo/MemoDataHandler;->toSQLInStateIntList(Landroid/content/Context;Ljava/lang/StringBuffer;Ljava/util/List;)V

    .line 79
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v3, Lcom/samsung/android/provider/memo/MemoContract;->BASE_URI_MEMO:Landroid/net/Uri;

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v1, v3, v4, v5}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 75
    .end local v0    # "sb":Ljava/lang/StringBuffer;
    :catchall_0
    move-exception v1

    monitor-exit v2

    throw v1
.end method

.method public static declared-synchronized deletingMemos(Landroid/content/Context;Ljava/util/ArrayList;)V
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/android/app/memo/MemoDataHandler$MemoDataHandlerException;
        }
    .end annotation

    .prologue
    .line 57
    .local p1, "memoIds":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    const-class v2, Lcom/samsung/android/app/memo/MemoDataHandler;

    monitor-enter v2

    :try_start_0
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-nez v1, :cond_0

    .line 62
    :goto_0
    monitor-exit v2

    return-void

    .line 59
    :cond_0
    :try_start_1
    new-instance v0, Ljava/lang/StringBuffer;

    const-string v1, "_id"

    invoke-direct {v0, v1}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 60
    .local v0, "sb":Ljava/lang/StringBuffer;
    invoke-static {p0, v0, p1}, Lcom/samsung/android/app/memo/MemoDataHandler;->toSQLInStateIntList(Landroid/content/Context;Ljava/lang/StringBuffer;Ljava/util/List;)V

    .line 61
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v3, Lcom/samsung/android/provider/memo/MemoContract;->BASE_URI_MEMO_UNDOABLE:Landroid/net/Uri;

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v1, v3, v4, v5}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 57
    .end local v0    # "sb":Ljava/lang/StringBuffer;
    :catchall_0
    move-exception v1

    monitor-exit v2

    throw v1
.end method

.method public static getCategoryCount(Landroid/content/Context;)I
    .locals 8
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    .line 94
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/provider/memo/MemoContract;->BASE_URI_CATEGORY:Landroid/net/Uri;

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 95
    .local v6, "c":Landroid/database/Cursor;
    const/4 v7, 0x0

    .line 96
    .local v7, "cnt":I
    if-eqz v6, :cond_0

    .line 97
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v7

    .line 98
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 101
    :cond_0
    return v7
.end method

.method public static getContent(Landroid/content/Context;I)Ljava/lang/String;
    .locals 10
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "id"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/android/app/memo/MemoDataHandler$MemoDataHandlerException;
        }
    .end annotation

    .prologue
    .line 217
    new-instance v8, Ljava/lang/StringBuffer;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "_id=\'"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v8, v0}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 218
    .local v8, "sb":Ljava/lang/StringBuffer;
    const/4 v6, 0x0

    .line 219
    .local v6, "c":Landroid/database/Cursor;
    const/4 v7, 0x0

    .line 221
    .local v7, "content":Ljava/lang/String;
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/provider/memo/MemoContract;->BASE_URI_MEMO:Landroid/net/Uri;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    .line 222
    const-string v4, "content"

    aput-object v4, v2, v3

    .line 223
    invoke-virtual {v8}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    .line 221
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 225
    if-eqz v6, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 226
    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v7

    .line 232
    :cond_0
    if-eqz v6, :cond_1

    .line 233
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 227
    :cond_1
    return-object v7

    .line 228
    :catch_0
    move-exception v9

    .line 229
    .local v9, "sqle":Landroid/database/SQLException;
    :try_start_1
    sget-object v0, Lcom/samsung/android/app/memo/MemoDataHandler;->TAG:Ljava/lang/String;

    const-string v1, "updateFiles"

    invoke-static {v0, v1, v9}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 230
    new-instance v0, Lcom/samsung/android/app/memo/MemoDataHandler$MemoDataHandlerException;

    const-string v1, "updateFiles"

    invoke-direct {v0, v1, v9}, Lcom/samsung/android/app/memo/MemoDataHandler$MemoDataHandlerException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 231
    .end local v9    # "sqle":Landroid/database/SQLException;
    :catchall_0
    move-exception v0

    .line 232
    if-eqz v6, :cond_2

    .line 233
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 234
    :cond_2
    throw v0
.end method

.method public static getContent(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;
    .locals 10
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "uuid"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/android/app/memo/MemoDataHandler$MemoDataHandlerException;
        }
    .end annotation

    .prologue
    .line 197
    new-instance v8, Ljava/lang/StringBuffer;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "UUID=\'"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v8, v0}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 198
    .local v8, "sb":Ljava/lang/StringBuffer;
    const/4 v6, 0x0

    .line 199
    .local v6, "c":Landroid/database/Cursor;
    const/4 v7, 0x0

    .line 201
    .local v7, "content":Ljava/lang/String;
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/provider/memo/MemoContract;->BASE_URI_MEMO:Landroid/net/Uri;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    .line 202
    const-string v4, "content"

    aput-object v4, v2, v3

    .line 203
    invoke-virtual {v8}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    .line 201
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 204
    if-eqz v6, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 205
    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v7

    .line 211
    :cond_0
    if-eqz v6, :cond_1

    .line 212
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 206
    :cond_1
    return-object v7

    .line 207
    :catch_0
    move-exception v9

    .line 208
    .local v9, "sqle":Landroid/database/SQLException;
    :try_start_1
    sget-object v0, Lcom/samsung/android/app/memo/MemoDataHandler;->TAG:Ljava/lang/String;

    const-string v1, "getContent"

    invoke-static {v0, v1, v9}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 209
    new-instance v0, Lcom/samsung/android/app/memo/MemoDataHandler$MemoDataHandlerException;

    const-string v1, "getContent"

    invoke-direct {v0, v1, v9}, Lcom/samsung/android/app/memo/MemoDataHandler$MemoDataHandlerException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 210
    .end local v9    # "sqle":Landroid/database/SQLException;
    :catchall_0
    move-exception v0

    .line 211
    if-eqz v6, :cond_2

    .line 212
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 213
    :cond_2
    throw v0
.end method

.method public static getContentName(Landroid/content/ContentResolver;Landroid/net/Uri;)Ljava/lang/String;
    .locals 10
    .param p0, "resolver"    # Landroid/content/ContentResolver;
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 348
    const/4 v6, 0x0

    .line 349
    .local v6, "cursor":Landroid/database/Cursor;
    const/4 v9, 0x0

    .line 351
    .local v9, "res":Ljava/lang/String;
    const/4 v0, 0x1

    :try_start_0
    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    .line 352
    const-string v1, "_display_name"

    aput-object v1, v2, v0

    .line 353
    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    .line 351
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 354
    if-eqz v6, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 355
    const-string v0, "_display_name"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v8

    .line 356
    .local v8, "nameIndex":I
    if-ltz v8, :cond_0

    .line 357
    invoke-interface {v6, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v9

    .line 362
    .end local v8    # "nameIndex":I
    :cond_0
    if-eqz v6, :cond_1

    .line 363
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 365
    :cond_1
    :goto_0
    return-object v9

    .line 359
    :catch_0
    move-exception v7

    .line 360
    .local v7, "e":Landroid/database/sqlite/SQLiteException;
    :try_start_1
    sget-object v0, Lcom/samsung/android/app/memo/MemoDataHandler;->TAG:Ljava/lang/String;

    const-string v1, "Exception occurred in getContentName."

    invoke-static {v0, v1}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 362
    if-eqz v6, :cond_1

    .line 363
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 361
    .end local v7    # "e":Landroid/database/sqlite/SQLiteException;
    :catchall_0
    move-exception v0

    .line 362
    if-eqz v6, :cond_2

    .line 363
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 364
    :cond_2
    throw v0
.end method

.method public static getMemoTitle(Landroid/content/Context;I)Ljava/lang/String;
    .locals 10
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "id"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/android/app/memo/MemoDataHandler$MemoDataHandlerException;
        }
    .end annotation

    .prologue
    .line 262
    new-instance v7, Ljava/lang/StringBuffer;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "_id=\'"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v7, v0}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 263
    .local v7, "sb":Ljava/lang/StringBuffer;
    const/4 v6, 0x0

    .line 264
    .local v6, "c":Landroid/database/Cursor;
    const/4 v9, 0x0

    .line 266
    .local v9, "title":Ljava/lang/String;
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/provider/memo/MemoContract;->BASE_URI_MEMO:Landroid/net/Uri;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    .line 267
    const-string v4, "title"

    aput-object v4, v2, v3

    .line 268
    invoke-virtual {v7}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    .line 266
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 270
    if-eqz v6, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 271
    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v9

    .line 277
    :cond_0
    if-eqz v6, :cond_1

    .line 278
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 272
    :cond_1
    return-object v9

    .line 273
    :catch_0
    move-exception v8

    .line 274
    .local v8, "sqle":Landroid/database/SQLException;
    :try_start_1
    sget-object v0, Lcom/samsung/android/app/memo/MemoDataHandler;->TAG:Ljava/lang/String;

    const-string v1, "getStrippedContent"

    invoke-static {v0, v1, v8}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 275
    new-instance v0, Lcom/samsung/android/app/memo/MemoDataHandler$MemoDataHandlerException;

    const-string v1, "updateFiles"

    invoke-direct {v0, v1, v8}, Lcom/samsung/android/app/memo/MemoDataHandler$MemoDataHandlerException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 276
    .end local v8    # "sqle":Landroid/database/SQLException;
    :catchall_0
    move-exception v0

    .line 277
    if-eqz v6, :cond_2

    .line 278
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 279
    :cond_2
    throw v0
.end method

.method public static getStrippedContent(Landroid/content/Context;I)Ljava/lang/String;
    .locals 10
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "id"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/android/app/memo/MemoDataHandler$MemoDataHandlerException;
        }
    .end annotation

    .prologue
    .line 238
    new-instance v8, Ljava/lang/StringBuffer;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "_id=\'"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v8, v0}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 239
    .local v8, "sb":Ljava/lang/StringBuffer;
    const/4 v6, 0x0

    .line 240
    .local v6, "c":Landroid/database/Cursor;
    const/4 v7, 0x0

    .line 242
    .local v7, "content":Ljava/lang/String;
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/provider/memo/MemoContract;->BASE_URI_MEMO:Landroid/net/Uri;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "strippedContent"

    aput-object v4, v2, v3

    invoke-virtual {v8}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 244
    if-eqz v6, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 245
    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 247
    :cond_0
    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_2

    .line 256
    if-eqz v6, :cond_1

    .line 257
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 248
    :cond_1
    const-string v0, ""

    .line 251
    :goto_0
    return-object v0

    .line 250
    :cond_2
    :try_start_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-static {v7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_1
    .catch Landroid/database/SQLException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v7

    .line 256
    if-eqz v6, :cond_3

    .line 257
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_3
    move-object v0, v7

    .line 251
    goto :goto_0

    .line 252
    :catch_0
    move-exception v9

    .line 253
    .local v9, "sqle":Landroid/database/SQLException;
    :try_start_2
    sget-object v0, Lcom/samsung/android/app/memo/MemoDataHandler;->TAG:Ljava/lang/String;

    const-string v1, "getStrippedContent"

    invoke-static {v0, v1, v9}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 254
    new-instance v0, Lcom/samsung/android/app/memo/MemoDataHandler$MemoDataHandlerException;

    const-string v1, "updateFiles"

    invoke-direct {v0, v1, v9}, Lcom/samsung/android/app/memo/MemoDataHandler$MemoDataHandlerException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 255
    .end local v9    # "sqle":Landroid/database/SQLException;
    :catchall_0
    move-exception v0

    .line 256
    if-eqz v6, :cond_4

    .line 257
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 258
    :cond_4
    throw v0
.end method

.method public static getVoiceUri(Landroid/content/Context;I)Landroid/net/Uri;
    .locals 10
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "id"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/android/app/memo/MemoDataHandler$MemoDataHandlerException;
        }
    .end annotation

    .prologue
    .line 283
    new-instance v7, Ljava/lang/StringBuffer;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "_id=\'"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v7, v0}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 284
    .local v7, "sb":Ljava/lang/StringBuffer;
    const/4 v6, 0x0

    .line 285
    .local v6, "c":Landroid/database/Cursor;
    const/4 v9, 0x0

    .line 287
    .local v9, "voiceUriStr":Ljava/lang/String;
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/provider/memo/MemoContract;->BASE_URI_MEMO:Landroid/net/Uri;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    .line 288
    const-string v4, "vrfileUUID"

    aput-object v4, v2, v3

    .line 289
    invoke-virtual {v7}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    .line 287
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 291
    if-eqz v6, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 292
    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 293
    :cond_0
    invoke-static {v9}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 298
    if-eqz v6, :cond_1

    .line 299
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 293
    :cond_1
    return-object v0

    .line 294
    :catch_0
    move-exception v8

    .line 295
    .local v8, "sqle":Landroid/database/SQLException;
    :try_start_1
    sget-object v0, Lcom/samsung/android/app/memo/MemoDataHandler;->TAG:Ljava/lang/String;

    const-string v1, "getVoiceUri"

    invoke-static {v0, v1, v8}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 296
    new-instance v0, Lcom/samsung/android/app/memo/MemoDataHandler$MemoDataHandlerException;

    const-string v1, "updateFiles"

    invoke-direct {v0, v1, v8}, Lcom/samsung/android/app/memo/MemoDataHandler$MemoDataHandlerException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 297
    .end local v8    # "sqle":Landroid/database/SQLException;
    :catchall_0
    move-exception v0

    .line 298
    if-eqz v6, :cond_2

    .line 299
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 300
    :cond_2
    throw v0
.end method

.method public static isCategoryExist(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 8
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "categoryUUID"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 105
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/provider/memo/MemoContract;->BASE_URI_CATEGORY:Landroid/net/Uri;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "UUID = \'"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 106
    .local v6, "c":Landroid/database/Cursor;
    const/4 v7, 0x0

    .line 107
    .local v7, "cnt":I
    if-eqz v6, :cond_0

    .line 108
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v7

    .line 109
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 111
    :cond_0
    if-lez v7, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isDBEmpty(Landroid/content/Context;)Z
    .locals 10
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 324
    const/4 v9, 0x1

    .local v9, "resMemo":Z
    const/4 v8, 0x1

    .line 325
    .local v8, "resCategory":Z
    const/4 v6, 0x0

    .line 327
    .local v6, "c":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/provider/memo/MemoContract;->BASE_URI_MEMO:Landroid/net/Uri;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 328
    if-eqz v6, :cond_1

    .line 329
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_0

    .line 330
    const/4 v9, 0x0

    .line 331
    :cond_0
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 333
    :cond_1
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/provider/memo/MemoContract;->BASE_URI_CATEGORY:Landroid/net/Uri;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 334
    if-eqz v6, :cond_2

    .line 335
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-lez v0, :cond_2

    .line 336
    const/4 v8, 0x0

    .line 341
    :cond_2
    if-eqz v6, :cond_3

    .line 342
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 344
    :cond_3
    :goto_0
    if-eqz v9, :cond_5

    if-eqz v8, :cond_5

    const/4 v0, 0x1

    :goto_1
    return v0

    .line 338
    :catch_0
    move-exception v7

    .line 339
    .local v7, "e":Landroid/database/sqlite/SQLiteException;
    :try_start_1
    sget-object v0, Lcom/samsung/android/app/memo/MemoDataHandler;->TAG:Ljava/lang/String;

    const-string v1, "Exception occurred in isDBEmpty."

    invoke-static {v0, v1, v7}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 341
    if-eqz v6, :cond_3

    .line 342
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 340
    .end local v7    # "e":Landroid/database/sqlite/SQLiteException;
    :catchall_0
    move-exception v0

    .line 341
    if-eqz v6, :cond_4

    .line 342
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 343
    :cond_4
    throw v0

    .line 344
    :cond_5
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static isFileExists(Landroid/content/Context;Landroid/net/Uri;)Z
    .locals 9
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 304
    const/4 v8, 0x0

    .line 305
    .local v8, "res":Z
    const/4 v6, 0x0

    .line 307
    .local v6, "c":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 308
    if-eqz v6, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-lez v0, :cond_0

    .line 309
    const/4 v8, 0x1

    .line 313
    :cond_0
    if-eqz v6, :cond_1

    .line 314
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 316
    :cond_1
    :goto_0
    return v8

    .line 310
    :catch_0
    move-exception v7

    .line 311
    .local v7, "e":Landroid/database/SQLException;
    :try_start_1
    sget-object v0, Lcom/samsung/android/app/memo/MemoDataHandler;->TAG:Ljava/lang/String;

    const-string v1, "SQLException in isFileExists. "

    invoke-static {v0, v1, v7}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 313
    if-eqz v6, :cond_1

    .line 314
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 312
    .end local v7    # "e":Landroid/database/SQLException;
    :catchall_0
    move-exception v0

    .line 313
    if-eqz v6, :cond_2

    .line 314
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 315
    :cond_2
    throw v0
.end method

.method public static isMemoExist(Landroid/content/Context;Ljava/lang/String;)J
    .locals 10
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "title"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x0

    const/4 v7, 0x0

    .line 369
    const-wide/16 v8, -0x1

    .line 370
    .local v8, "id":J
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "title = \'"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p1}, Lcom/samsung/android/app/migration/utils/Utils;->forSQL(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 371
    .local v3, "selection":Ljava/lang/String;
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 372
    sget-object v1, Lcom/samsung/android/provider/memo/MemoContract;->BASE_URI_MEMO:Landroid/net/Uri;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    .line 373
    const-string v5, "_id"

    aput-object v5, v2, v7

    move-object v5, v4

    .line 372
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 375
    .local v6, "c":Landroid/database/Cursor;
    if-eqz v6, :cond_1

    .line 376
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 377
    invoke-interface {v6, v7}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v8

    .line 378
    :cond_0
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 380
    :cond_1
    return-wide v8
.end method

.method public static isMemoExists(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 10
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "title"    # Ljava/lang/String;
    .param p2, "content"    # Ljava/lang/String;

    .prologue
    .line 384
    const/4 v8, 0x0

    .line 385
    .local v8, "res":Z
    const/4 v6, 0x0

    .line 387
    .local v6, "c":Landroid/database/Cursor;
    :try_start_0
    new-instance v9, Ljava/lang/StringBuffer;

    invoke-direct {v9}, Ljava/lang/StringBuffer;-><init>()V

    .line 388
    .local v9, "taggedContent":Ljava/lang/StringBuffer;
    const-string v0, "<p>"

    invoke-virtual {v9, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string v1, "</p>"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 389
    new-instance v1, Ljava/lang/StringBuilder;

    if-eqz p1, :cond_2

    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "title = \'"

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 390
    invoke-static {p1}, Lcom/samsung/android/app/migration/utils/Utils;->forSQL(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\' AND "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 391
    const-string v0, "content"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " = \'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v9}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/samsung/android/app/migration/utils/Utils;->forSQL(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 389
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 392
    .local v3, "selection":Ljava/lang/String;
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/provider/memo/MemoContract;->BASE_URI_MEMO:Landroid/net/Uri;

    const/4 v2, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 393
    if-eqz v6, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_0

    .line 394
    const/4 v8, 0x1

    .line 395
    :cond_0
    sget-object v0, Lcom/samsung/android/app/memo/MemoDataHandler;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Memo exists? "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 399
    if-eqz v6, :cond_1

    .line 400
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 402
    .end local v3    # "selection":Ljava/lang/String;
    .end local v9    # "taggedContent":Ljava/lang/StringBuffer;
    :cond_1
    :goto_1
    return v8

    .line 390
    .restart local v9    # "taggedContent":Ljava/lang/StringBuffer;
    :cond_2
    :try_start_1
    const-string v0, ""
    :try_end_1
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 396
    .end local v9    # "taggedContent":Ljava/lang/StringBuffer;
    :catch_0
    move-exception v7

    .line 397
    .local v7, "e":Landroid/database/sqlite/SQLiteException;
    :try_start_2
    sget-object v0, Lcom/samsung/android/app/memo/MemoDataHandler;->TAG:Ljava/lang/String;

    const-string v1, "Exception occurred in isMemoExists. "

    invoke-static {v0, v1, v7}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 399
    if-eqz v6, :cond_1

    .line 400
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_1

    .line 398
    .end local v7    # "e":Landroid/database/sqlite/SQLiteException;
    :catchall_0
    move-exception v0

    .line 399
    if-eqz v6, :cond_3

    .line 400
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 401
    :cond_3
    throw v0
.end method

.method public static moveCategory(Landroid/content/Context;Ljava/util/ArrayList;Ljava/lang/String;)V
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .param p2, "toCategoryUUID"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 83
    .local p1, "memoIds":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-nez v2, :cond_0

    .line 91
    :goto_0
    return-void

    .line 85
    :cond_0
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 86
    .local v1, "values":Landroid/content/ContentValues;
    const-string v2, "categoryUUID"

    invoke-virtual {v1, v2, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 88
    new-instance v0, Ljava/lang/StringBuffer;

    const-string v2, "_id"

    invoke-direct {v0, v2}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 89
    .local v0, "sb":Ljava/lang/StringBuffer;
    invoke-static {p0, v0, p1}, Lcom/samsung/android/app/memo/MemoDataHandler;->toSQLInStateIntList(Landroid/content/Context;Ljava/lang/StringBuffer;Ljava/util/List;)V

    .line 90
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Lcom/samsung/android/provider/memo/MemoContract;->BASE_URI_MEMO:Landroid/net/Uri;

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v1, v4, v5}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static setDataFor(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .locals 8
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "uuid"    # Ljava/lang/String;
    .param p2, "path"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/android/app/memo/MemoDataHandler$MemoDataHandlerException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x1

    .line 170
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 171
    .local v2, "values":Landroid/content/ContentValues;
    const-string v3, "_data"

    invoke-static {p2}, Lcom/samsung/android/app/memo/util/Utils;->getRelativePath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 172
    const-string v3, "v_skipDirtySet"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 173
    const-string v3, "v_skipSync1Set"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 174
    new-instance v0, Ljava/lang/StringBuffer;

    const-string v3, "UUID=?"

    invoke-direct {v0, v3}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 176
    .local v0, "sb":Ljava/lang/StringBuffer;
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    sget-object v4, Lcom/samsung/android/provider/memo/MemoContract;->BASE_URI_MEMO:Landroid/net/Uri;

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    .line 177
    aput-object p1, v6, v7

    .line 176
    invoke-virtual {v3, v4, v2, v5, v6}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    .line 183
    return-void

    .line 179
    :catch_0
    move-exception v1

    .line 180
    .local v1, "sqle":Landroid/database/SQLException;
    sget-object v3, Lcom/samsung/android/app/memo/MemoDataHandler;->TAG:Ljava/lang/String;

    const-string v4, "updateFiles"

    invoke-static {v3, v4, v1}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 181
    new-instance v3, Lcom/samsung/android/app/memo/MemoDataHandler$MemoDataHandlerException;

    const-string v4, "updateFiles"

    invoke-direct {v3, v4, v1}, Lcom/samsung/android/app/memo/MemoDataHandler$MemoDataHandlerException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v3
.end method

.method private static toSQLInStateIntList(Landroid/content/Context;Ljava/lang/StringBuffer;Ljava/util/List;)V
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "sb"    # Ljava/lang/StringBuffer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/StringBuffer;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p2, "memoIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    const/16 v4, 0x27

    .line 139
    const-string v1, " IN ("

    invoke-virtual {p1, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 140
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-nez v1, :cond_0

    .line 143
    invoke-virtual {p1}, Ljava/lang/StringBuffer;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    const/16 v2, 0x29

    invoke-virtual {p1, v1, v2}, Ljava/lang/StringBuffer;->setCharAt(IC)V

    .line 144
    return-void

    .line 140
    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 141
    .local v0, "memoId":I
    invoke-virtual {p1, v4}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, v4}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    move-result-object v1

    const/16 v3, 0x2c

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    goto :goto_0
.end method

.method private static toSQLInStateStringList(Landroid/content/Context;Ljava/lang/StringBuffer;Ljava/util/List;)V
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "sb"    # Ljava/lang/StringBuffer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/StringBuffer;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p2, "uuids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/16 v4, 0x27

    .line 147
    const-string v1, " IN ("

    invoke-virtual {p1, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 148
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_0

    .line 151
    invoke-virtual {p1}, Ljava/lang/StringBuffer;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    const/16 v2, 0x29

    invoke-virtual {p1, v1, v2}, Ljava/lang/StringBuffer;->setCharAt(IC)V

    .line 152
    return-void

    .line 148
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 149
    .local v0, "uuid":Ljava/lang/String;
    invoke-virtual {p1, v4}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2, v4}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    move-result-object v2

    const/16 v3, 0x2c

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    goto :goto_0
.end method

.method public static updateCategorUUIDS(Landroid/content/Context;Ljava/util/List;Ljava/lang/String;)I
    .locals 8
    .param p0, "context"    # Landroid/content/Context;
    .param p2, "toCategoryUUID"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            ")I"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/android/app/memo/MemoDataHandler$MemoDataHandlerException;
        }
    .end annotation

    .prologue
    .line 116
    .local p1, "fromCategoryUUIDs":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v4

    if-nez v4, :cond_0

    .line 117
    const/4 v0, 0x0

    .line 135
    :goto_0
    return v0

    .line 119
    :cond_0
    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    .line 120
    .local v3, "values":Landroid/content/ContentValues;
    const-string v4, "isDirty"

    const/4 v5, 0x1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 121
    const-string v4, "categoryUUID"

    invoke-virtual {v3, v4, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 123
    new-instance v1, Ljava/lang/StringBuffer;

    const-string v4, "categoryUUID"

    invoke-direct {v1, v4}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 124
    .local v1, "sb":Ljava/lang/StringBuffer;
    invoke-static {p0, v1, p1}, Lcom/samsung/android/app/memo/MemoDataHandler;->toSQLInStateStringList(Landroid/content/Context;Ljava/lang/StringBuffer;Ljava/util/List;)V

    .line 126
    const/4 v0, 0x0

    .line 128
    .local v0, "ret":I
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    sget-object v5, Lcom/samsung/android/provider/memo/MemoContract;->BASE_URI_MEMO:Landroid/net/Uri;

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    invoke-virtual {v4, v5, v3, v6, v7}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 134
    sget-object v4, Lcom/samsung/android/app/memo/MemoDataHandler;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "updateCategorUUIDS() "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 129
    :catch_0
    move-exception v2

    .line 130
    .local v2, "sqle":Landroid/database/SQLException;
    sget-object v4, Lcom/samsung/android/app/memo/MemoDataHandler;->TAG:Ljava/lang/String;

    const-string v5, "updateCategorUUIDS"

    invoke-static {v4, v5, v2}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 131
    new-instance v4, Lcom/samsung/android/app/memo/MemoDataHandler$MemoDataHandlerException;

    const-string v5, "updateCategorUUIDS"

    invoke-direct {v4, v5, v2}, Lcom/samsung/android/app/memo/MemoDataHandler$MemoDataHandlerException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v4
.end method

.method public static updateCategoryUUIDToNone(Landroid/content/Context;Ljava/lang/String;)V
    .locals 7
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "uuid"    # Ljava/lang/String;

    .prologue
    .line 186
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 187
    .local v2, "values":Landroid/content/ContentValues;
    const-string v3, "categoryUUID"

    const-string v4, ""

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 188
    new-instance v0, Ljava/lang/StringBuffer;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "categoryUUID=\'"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v3}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 190
    .local v0, "sb":Ljava/lang/StringBuffer;
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    sget-object v4, Lcom/samsung/android/provider/memo/MemoContract;->BASE_URI_MEMO:Landroid/net/Uri;

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v3, v4, v2, v5, v6}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    .line 194
    :goto_0
    return-void

    .line 191
    :catch_0
    move-exception v1

    .line 192
    .local v1, "sqle":Landroid/database/SQLException;
    sget-object v3, Lcom/samsung/android/app/memo/MemoDataHandler;->TAG:Ljava/lang/String;

    const-string v4, "updateFiles"

    invoke-static {v3, v4, v1}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public static updateExistingEntries(Landroid/content/Context;)V
    .locals 6
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 407
    invoke-static {p0}, Lcom/samsung/android/app/memo/util/AccountHelper;->getRegisteredSamsungAccount(Landroid/content/Context;)Landroid/accounts/Account;

    move-result-object v0

    .line 408
    .local v0, "account":Landroid/accounts/Account;
    if-eqz v0, :cond_0

    .line 409
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 410
    .local v1, "cv":Landroid/content/ContentValues;
    const-string v2, "accountType"

    const-string v3, "com.osp.app.signin"

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 411
    const-string v2, "accountName"

    iget-object v3, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 412
    const-string v2, "isDirty"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 414
    const-string v2, "v_skipSync1Set"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 415
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Lcom/samsung/android/provider/memo/MemoContract;->BASE_URI_MEMO:Landroid/net/Uri;

    invoke-virtual {v2, v3, v1, v4, v4}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 416
    const-string v2, "v_skipSync1Set"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 417
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Lcom/samsung/android/provider/memo/MemoContract;->BASE_URI_FILE:Landroid/net/Uri;

    invoke-virtual {v2, v3, v1, v4, v4}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 418
    const-string v2, "v_skipSync1Set"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 419
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Lcom/samsung/android/provider/memo/MemoContract;->BASE_URI_CATEGORY:Landroid/net/Uri;

    invoke-virtual {v2, v3, v1, v4, v4}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 422
    .end local v1    # "cv":Landroid/content/ContentValues;
    :goto_0
    return-void

    .line 421
    :cond_0
    sget-object v2, Lcom/samsung/android/app/memo/MemoDataHandler;->TAG:Ljava/lang/String;

    const-string v3, "Unknown error in updateExistingEntries(): SA is null."

    invoke-static {v2, v3}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
