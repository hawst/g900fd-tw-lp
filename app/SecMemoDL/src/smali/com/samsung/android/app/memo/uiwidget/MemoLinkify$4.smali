.class Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$4;
.super Ljava/lang/Object;
.source "MemoLinkify.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/app/memo/uiwidget/MemoLinkify;->pruneOverlaps(Ljava/util/ArrayList;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lcom/samsung/android/app/memo/uiwidget/LinkSpec;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 1229
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1
    return-void
.end method


# virtual methods
.method public final compare(Lcom/samsung/android/app/memo/uiwidget/LinkSpec;Lcom/samsung/android/app/memo/uiwidget/LinkSpec;)I
    .locals 4
    .param p1, "a"    # Lcom/samsung/android/app/memo/uiwidget/LinkSpec;
    .param p2, "b"    # Lcom/samsung/android/app/memo/uiwidget/LinkSpec;

    .prologue
    const/4 v1, 0x1

    const/4 v0, -0x1

    .line 1231
    iget v2, p1, Lcom/samsung/android/app/memo/uiwidget/LinkSpec;->mStart:I

    iget v3, p2, Lcom/samsung/android/app/memo/uiwidget/LinkSpec;->mStart:I

    if-ge v2, v3, :cond_1

    .line 1247
    :cond_0
    :goto_0
    return v0

    .line 1235
    :cond_1
    iget v2, p1, Lcom/samsung/android/app/memo/uiwidget/LinkSpec;->mStart:I

    iget v3, p2, Lcom/samsung/android/app/memo/uiwidget/LinkSpec;->mStart:I

    if-le v2, v3, :cond_2

    move v0, v1

    .line 1236
    goto :goto_0

    .line 1239
    :cond_2
    iget v2, p1, Lcom/samsung/android/app/memo/uiwidget/LinkSpec;->mEnd:I

    iget v3, p2, Lcom/samsung/android/app/memo/uiwidget/LinkSpec;->mEnd:I

    if-ge v2, v3, :cond_3

    move v0, v1

    .line 1240
    goto :goto_0

    .line 1243
    :cond_3
    iget v1, p1, Lcom/samsung/android/app/memo/uiwidget/LinkSpec;->mEnd:I

    iget v2, p2, Lcom/samsung/android/app/memo/uiwidget/LinkSpec;->mEnd:I

    if-gt v1, v2, :cond_0

    .line 1247
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 1
    check-cast p1, Lcom/samsung/android/app/memo/uiwidget/LinkSpec;

    check-cast p2, Lcom/samsung/android/app/memo/uiwidget/LinkSpec;

    invoke-virtual {p0, p1, p2}, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$4;->compare(Lcom/samsung/android/app/memo/uiwidget/LinkSpec;Lcom/samsung/android/app/memo/uiwidget/LinkSpec;)I

    move-result v0

    return v0
.end method
