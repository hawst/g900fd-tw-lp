.class Lcom/samsung/android/app/memo/voice/VoiceViewLayout$15;
.super Landroid/content/BroadcastReceiver;
.source "VoiceViewLayout.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->updateAlarmReceiver(Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$15;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    .line 782
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2
    .param p1, "ctx"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 786
    if-eqz p2, :cond_1

    .line 787
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 788
    const-string v1, "com.samsung.sec.android.clockpackage.alarm.ALARM_ALERT"

    .line 787
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 788
    if-nez v0, :cond_0

    .line 789
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.sec.android.app.voicecommand"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 791
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$15;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    # getter for: Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mVoiceManger:Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;
    invoke-static {v0}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->access$9(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$15;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    # getter for: Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mVoiceManger:Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;
    invoke-static {v0}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->access$9(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->isRecording()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 792
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$15;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    invoke-virtual {v0}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->handleRecordPause()V

    .line 799
    :cond_1
    :goto_0
    return-void

    .line 793
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$15;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    # getter for: Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mVoiceManger:Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;
    invoke-static {v0}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->access$9(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$15;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    # getter for: Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mVoiceManger:Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;
    invoke-static {v0}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->access$9(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->isNowPlaying()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 794
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$15;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    # invokes: Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->pauseDuringAlarm()V
    invoke-static {v0}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->access$39(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)V

    goto :goto_0
.end method
