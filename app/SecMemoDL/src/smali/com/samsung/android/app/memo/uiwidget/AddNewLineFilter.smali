.class Lcom/samsung/android/app/memo/uiwidget/AddNewLineFilter;
.super Ljava/lang/Object;
.source "RichEditor.java"

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field private beforeText:Ljava/lang/CharSequence;

.field private curSelection:I

.field private isBeforeLine:Z

.field private isIMgSpan:Z

.field private isLastLine:Z

.field private isMiddle:Z

.field private line:I

.field private mIsBackspace:Z

.field private proxy:Landroid/widget/EditText;

.field private repStart:I

.field private replacedText:Ljava/lang/CharSequence;

.field private spanStart:I


# direct methods
.method public constructor <init>(Landroid/widget/EditText;)V
    .locals 1
    .param p1, "et"    # Landroid/widget/EditText;

    .prologue
    const/4 v0, 0x0

    .line 1900
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1896
    iput-boolean v0, p0, Lcom/samsung/android/app/memo/uiwidget/AddNewLineFilter;->isLastLine:Z

    .line 1898
    iput-boolean v0, p0, Lcom/samsung/android/app/memo/uiwidget/AddNewLineFilter;->isMiddle:Z

    .line 1901
    iput-object p1, p0, Lcom/samsung/android/app/memo/uiwidget/AddNewLineFilter;->proxy:Landroid/widget/EditText;

    .line 1902
    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 13
    .param p1, "s"    # Landroid/text/Editable;

    .prologue
    const/16 v12, 0xa

    const/4 v11, -0x1

    const v9, 0x7f0e0032

    const/4 v10, 0x0

    .line 1917
    sget-object v6, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->mToolbarView:Landroid/view/View;

    .line 1918
    .local v6, "v":Landroid/view/View;
    if-nez p1, :cond_1

    .line 2020
    :cond_0
    :goto_0
    return-void

    .line 1920
    :cond_1
    if-eqz v6, :cond_2

    invoke-virtual {v6, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    if-eqz v7, :cond_2

    .line 1921
    invoke-static {}, Lcom/samsung/android/app/memo/util/Utils;->isNoOutgoingCall()Z

    move-result v7

    if-nez v7, :cond_4

    .line 1922
    invoke-virtual {v6, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    invoke-virtual {v7, v10}, Landroid/view/View;->setEnabled(Z)V

    .line 1934
    :cond_2
    :goto_1
    iget-boolean v7, p0, Lcom/samsung/android/app/memo/uiwidget/AddNewLineFilter;->mIsBackspace:Z

    if-nez v7, :cond_9

    .line 1935
    iget-object v7, p0, Lcom/samsung/android/app/memo/uiwidget/AddNewLineFilter;->replacedText:Ljava/lang/CharSequence;

    invoke-interface {v7}, Ljava/lang/CharSequence;->length()I

    move-result v7

    iget-object v8, p0, Lcom/samsung/android/app/memo/uiwidget/AddNewLineFilter;->beforeText:Ljava/lang/CharSequence;

    invoke-interface {v8}, Ljava/lang/CharSequence;->length()I

    move-result v8

    if-lt v7, v8, :cond_0

    .line 1938
    iget-object v7, p0, Lcom/samsung/android/app/memo/uiwidget/AddNewLineFilter;->replacedText:Ljava/lang/CharSequence;

    iget-object v8, p0, Lcom/samsung/android/app/memo/uiwidget/AddNewLineFilter;->beforeText:Ljava/lang/CharSequence;

    invoke-interface {v8}, Ljava/lang/CharSequence;->length()I

    move-result v8

    iget-object v9, p0, Lcom/samsung/android/app/memo/uiwidget/AddNewLineFilter;->replacedText:Ljava/lang/CharSequence;

    invoke-interface {v9}, Ljava/lang/CharSequence;->length()I

    move-result v9

    invoke-interface {v7, v8, v9}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v0

    .line 1939
    .local v0, "chg":Ljava/lang/CharSequence;
    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v7

    if-lez v7, :cond_3

    invoke-interface {v0, v10}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v7

    if-eq v7, v12, :cond_0

    .line 1945
    :cond_3
    iget-boolean v7, p0, Lcom/samsung/android/app/memo/uiwidget/AddNewLineFilter;->isIMgSpan:Z

    if-eqz v7, :cond_7

    .line 1946
    iget v7, p0, Lcom/samsung/android/app/memo/uiwidget/AddNewLineFilter;->line:I

    if-eq v7, v11, :cond_0

    iget-object v7, p0, Lcom/samsung/android/app/memo/uiwidget/AddNewLineFilter;->proxy:Landroid/widget/EditText;

    invoke-virtual {v7}, Landroid/widget/EditText;->getLayout()Landroid/text/Layout;

    move-result-object v7

    if-eqz v7, :cond_0

    .line 1948
    iget-object v7, p0, Lcom/samsung/android/app/memo/uiwidget/AddNewLineFilter;->proxy:Landroid/widget/EditText;

    invoke-virtual {v7}, Landroid/widget/EditText;->getSelectionStart()I

    move-result v7

    iput v7, p0, Lcom/samsung/android/app/memo/uiwidget/AddNewLineFilter;->curSelection:I

    .line 1950
    :try_start_0
    iget-object v7, p0, Lcom/samsung/android/app/memo/uiwidget/AddNewLineFilter;->proxy:Landroid/widget/EditText;

    invoke-virtual {v7}, Landroid/widget/EditText;->getLayout()Landroid/text/Layout;

    move-result-object v7

    iget v8, p0, Lcom/samsung/android/app/memo/uiwidget/AddNewLineFilter;->line:I

    invoke-virtual {v7, v8}, Landroid/text/Layout;->getLineStart(I)I

    move-result v3

    .line 1951
    .local v3, "lineStart":I
    iget-object v7, p0, Lcom/samsung/android/app/memo/uiwidget/AddNewLineFilter;->proxy:Landroid/widget/EditText;

    invoke-virtual {v7}, Landroid/widget/EditText;->getLayout()Landroid/text/Layout;

    move-result-object v7

    iget v8, p0, Lcom/samsung/android/app/memo/uiwidget/AddNewLineFilter;->line:I

    invoke-virtual {v7, v8}, Landroid/text/Layout;->getLineEnd(I)I

    move-result v2

    .line 1952
    .local v2, "lineEnd":I
    iget-object v7, p0, Lcom/samsung/android/app/memo/uiwidget/AddNewLineFilter;->proxy:Landroid/widget/EditText;

    invoke-virtual {v7}, Landroid/widget/EditText;->getEditableText()Landroid/text/Editable;

    move-result-object v7

    .line 1953
    const-class v8, Lcom/samsung/android/app/memo/uiwidget/ClickableSpanType;

    .line 1952
    invoke-interface {v7, v3, v2, v8}, Landroid/text/Editable;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v5

    check-cast v5, [Lcom/samsung/android/app/memo/uiwidget/ClickableSpanType;

    .line 1954
    .local v5, "spans":[Lcom/samsung/android/app/memo/uiwidget/ClickableSpanType;
    array-length v7, v5

    if-lez v7, :cond_5

    .line 1955
    iget-object v7, p0, Lcom/samsung/android/app/memo/uiwidget/AddNewLineFilter;->proxy:Landroid/widget/EditText;

    invoke-virtual {v7}, Landroid/widget/EditText;->getEditableText()Landroid/text/Editable;

    move-result-object v7

    const/4 v8, 0x0

    aget-object v8, v5, v8

    invoke-interface {v7, v8}, Landroid/text/Editable;->getSpanStart(Ljava/lang/Object;)I

    move-result v7

    iput v7, p0, Lcom/samsung/android/app/memo/uiwidget/AddNewLineFilter;->spanStart:I
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1963
    .end local v2    # "lineEnd":I
    .end local v3    # "lineStart":I
    .end local v5    # "spans":[Lcom/samsung/android/app/memo/uiwidget/ClickableSpanType;
    :goto_2
    iget v7, p0, Lcom/samsung/android/app/memo/uiwidget/AddNewLineFilter;->curSelection:I

    iget v8, p0, Lcom/samsung/android/app/memo/uiwidget/AddNewLineFilter;->spanStart:I

    if-gt v7, v8, :cond_6

    .line 1964
    iget v7, p0, Lcom/samsung/android/app/memo/uiwidget/AddNewLineFilter;->curSelection:I

    const-string v8, "\n"

    invoke-interface {p1, v7, v8}, Landroid/text/Editable;->insert(ILjava/lang/CharSequence;)Landroid/text/Editable;

    .line 1965
    iget-object v7, p0, Lcom/samsung/android/app/memo/uiwidget/AddNewLineFilter;->proxy:Landroid/widget/EditText;

    iget v8, p0, Lcom/samsung/android/app/memo/uiwidget/AddNewLineFilter;->curSelection:I

    invoke-virtual {v7, v8}, Landroid/widget/EditText;->setSelection(I)V

    goto/16 :goto_0

    .line 1924
    .end local v0    # "chg":Ljava/lang/CharSequence;
    :cond_4
    invoke-static {}, Lcom/samsung/android/app/memo/util/Utils;->isUPSM()I

    move-result v7

    const/4 v8, 0x1

    if-ne v7, v8, :cond_2

    .line 1925
    invoke-virtual {v6, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    invoke-virtual {v7, v10}, Landroid/view/View;->setEnabled(Z)V

    goto/16 :goto_1

    .line 1958
    .restart local v0    # "chg":Ljava/lang/CharSequence;
    .restart local v2    # "lineEnd":I
    .restart local v3    # "lineStart":I
    .restart local v5    # "spans":[Lcom/samsung/android/app/memo/uiwidget/ClickableSpanType;
    :cond_5
    :try_start_1
    iget v7, p0, Lcom/samsung/android/app/memo/uiwidget/AddNewLineFilter;->curSelection:I

    iput v7, p0, Lcom/samsung/android/app/memo/uiwidget/AddNewLineFilter;->spanStart:I
    :try_end_1
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_2

    .line 1960
    .end local v2    # "lineEnd":I
    .end local v3    # "lineStart":I
    .end local v5    # "spans":[Lcom/samsung/android/app/memo/uiwidget/ClickableSpanType;
    :catch_0
    move-exception v1

    .line 1961
    .local v1, "e":Ljava/lang/IndexOutOfBoundsException;
    const-string v7, "AddNewLineFilter"

    const-string v8, "afterTextChanged "

    invoke-static {v7, v8, v1}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_2

    .line 1967
    .end local v1    # "e":Ljava/lang/IndexOutOfBoundsException;
    :cond_6
    iget v7, p0, Lcom/samsung/android/app/memo/uiwidget/AddNewLineFilter;->repStart:I

    const-string v8, "\n"

    invoke-interface {p1, v7, v8}, Landroid/text/Editable;->insert(ILjava/lang/CharSequence;)Landroid/text/Editable;

    goto/16 :goto_0

    .line 1970
    :cond_7
    iget-object v8, p0, Lcom/samsung/android/app/memo/uiwidget/AddNewLineFilter;->proxy:Landroid/widget/EditText;

    iget-object v7, p0, Lcom/samsung/android/app/memo/uiwidget/AddNewLineFilter;->proxy:Landroid/widget/EditText;

    invoke-virtual {v7}, Landroid/widget/EditText;->getSelectionStart()I

    move-result v7

    iget-object v9, p0, Lcom/samsung/android/app/memo/uiwidget/AddNewLineFilter;->proxy:Landroid/widget/EditText;

    invoke-virtual {v9}, Landroid/widget/EditText;->getSelectionEnd()I

    move-result v9

    if-lt v7, v9, :cond_8

    iget-object v7, p0, Lcom/samsung/android/app/memo/uiwidget/AddNewLineFilter;->proxy:Landroid/widget/EditText;

    invoke-virtual {v7}, Landroid/widget/EditText;->getSelectionStart()I

    move-result v7

    :goto_3
    invoke-virtual {v8, v7}, Landroid/widget/EditText;->setSelection(I)V

    goto/16 :goto_0

    :cond_8
    iget-object v7, p0, Lcom/samsung/android/app/memo/uiwidget/AddNewLineFilter;->proxy:Landroid/widget/EditText;

    invoke-virtual {v7}, Landroid/widget/EditText;->getSelectionEnd()I

    move-result v7

    goto :goto_3

    .line 1973
    .end local v0    # "chg":Ljava/lang/CharSequence;
    :cond_9
    iget-boolean v7, p0, Lcom/samsung/android/app/memo/uiwidget/AddNewLineFilter;->isIMgSpan:Z

    if-eqz v7, :cond_d

    .line 1974
    iget-object v7, p0, Lcom/samsung/android/app/memo/uiwidget/AddNewLineFilter;->proxy:Landroid/widget/EditText;

    invoke-virtual {v7}, Landroid/widget/EditText;->getSelectionStart()I

    move-result v7

    iput v7, p0, Lcom/samsung/android/app/memo/uiwidget/AddNewLineFilter;->curSelection:I

    .line 1976
    :try_start_2
    iget-object v7, p0, Lcom/samsung/android/app/memo/uiwidget/AddNewLineFilter;->proxy:Landroid/widget/EditText;

    invoke-virtual {v7}, Landroid/widget/EditText;->getLayout()Landroid/text/Layout;

    move-result-object v7

    iget v8, p0, Lcom/samsung/android/app/memo/uiwidget/AddNewLineFilter;->line:I

    invoke-virtual {v7, v8}, Landroid/text/Layout;->getLineStart(I)I

    move-result v3

    .line 1977
    .restart local v3    # "lineStart":I
    iget-object v7, p0, Lcom/samsung/android/app/memo/uiwidget/AddNewLineFilter;->proxy:Landroid/widget/EditText;

    invoke-virtual {v7}, Landroid/widget/EditText;->getLayout()Landroid/text/Layout;

    move-result-object v7

    iget v8, p0, Lcom/samsung/android/app/memo/uiwidget/AddNewLineFilter;->line:I

    invoke-virtual {v7, v8}, Landroid/text/Layout;->getLineEnd(I)I

    move-result v2

    .line 1978
    .restart local v2    # "lineEnd":I
    iget-object v7, p0, Lcom/samsung/android/app/memo/uiwidget/AddNewLineFilter;->proxy:Landroid/widget/EditText;

    invoke-virtual {v7}, Landroid/widget/EditText;->getEditableText()Landroid/text/Editable;

    move-result-object v7

    .line 1979
    const-class v8, Lcom/samsung/android/app/memo/uiwidget/ClickableSpanType;

    .line 1978
    invoke-interface {v7, v3, v2, v8}, Landroid/text/Editable;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v5

    check-cast v5, [Lcom/samsung/android/app/memo/uiwidget/ClickableSpanType;

    .line 1980
    .restart local v5    # "spans":[Lcom/samsung/android/app/memo/uiwidget/ClickableSpanType;
    array-length v7, v5

    if-lez v7, :cond_b

    .line 1981
    iget-object v7, p0, Lcom/samsung/android/app/memo/uiwidget/AddNewLineFilter;->proxy:Landroid/widget/EditText;

    invoke-virtual {v7}, Landroid/widget/EditText;->getEditableText()Landroid/text/Editable;

    move-result-object v7

    const/4 v8, 0x0

    aget-object v8, v5, v8

    invoke-interface {v7, v8}, Landroid/text/Editable;->getSpanStart(Ljava/lang/Object;)I

    move-result v7

    iput v7, p0, Lcom/samsung/android/app/memo/uiwidget/AddNewLineFilter;->spanStart:I
    :try_end_2
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_2 .. :try_end_2} :catch_1

    .line 1989
    .end local v2    # "lineEnd":I
    .end local v3    # "lineStart":I
    .end local v5    # "spans":[Lcom/samsung/android/app/memo/uiwidget/ClickableSpanType;
    :goto_4
    iget v7, p0, Lcom/samsung/android/app/memo/uiwidget/AddNewLineFilter;->curSelection:I

    iget v8, p0, Lcom/samsung/android/app/memo/uiwidget/AddNewLineFilter;->spanStart:I

    if-gt v7, v8, :cond_c

    .line 1990
    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result v7

    iget v8, p0, Lcom/samsung/android/app/memo/uiwidget/AddNewLineFilter;->curSelection:I

    if-le v7, v8, :cond_a

    iget v7, p0, Lcom/samsung/android/app/memo/uiwidget/AddNewLineFilter;->curSelection:I

    invoke-interface {p1, v7}, Landroid/text/Editable;->charAt(I)C

    move-result v7

    const v8, 0xfffc

    if-ne v7, v8, :cond_a

    .line 1991
    iget v7, p0, Lcom/samsung/android/app/memo/uiwidget/AddNewLineFilter;->curSelection:I

    const-string v8, "\n"

    invoke-interface {p1, v7, v8}, Landroid/text/Editable;->insert(ILjava/lang/CharSequence;)Landroid/text/Editable;

    .line 1993
    :cond_a
    iget-object v7, p0, Lcom/samsung/android/app/memo/uiwidget/AddNewLineFilter;->proxy:Landroid/widget/EditText;

    iget v8, p0, Lcom/samsung/android/app/memo/uiwidget/AddNewLineFilter;->curSelection:I

    invoke-virtual {v7, v8}, Landroid/widget/EditText;->setSelection(I)V

    goto/16 :goto_0

    .line 1984
    .restart local v2    # "lineEnd":I
    .restart local v3    # "lineStart":I
    .restart local v5    # "spans":[Lcom/samsung/android/app/memo/uiwidget/ClickableSpanType;
    :cond_b
    :try_start_3
    iget v7, p0, Lcom/samsung/android/app/memo/uiwidget/AddNewLineFilter;->curSelection:I

    iput v7, p0, Lcom/samsung/android/app/memo/uiwidget/AddNewLineFilter;->spanStart:I
    :try_end_3
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_4

    .line 1986
    .end local v2    # "lineEnd":I
    .end local v3    # "lineStart":I
    .end local v5    # "spans":[Lcom/samsung/android/app/memo/uiwidget/ClickableSpanType;
    :catch_1
    move-exception v1

    .line 1987
    .restart local v1    # "e":Ljava/lang/IndexOutOfBoundsException;
    const-string v7, "AddNewLineFilter"

    const-string v8, "afterTextChanged "

    invoke-static {v7, v8, v1}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_4

    .line 1995
    .end local v1    # "e":Ljava/lang/IndexOutOfBoundsException;
    :cond_c
    iget v7, p0, Lcom/samsung/android/app/memo/uiwidget/AddNewLineFilter;->repStart:I

    const-string v8, "\n"

    invoke-interface {p1, v7, v8}, Landroid/text/Editable;->insert(ILjava/lang/CharSequence;)Landroid/text/Editable;

    goto/16 :goto_0

    .line 1998
    :cond_d
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uiwidget/AddNewLineFilter;->getCurrentCursorLine()I

    move-result v4

    .line 1999
    .local v4, "lineTemp":I
    iget-boolean v7, p0, Lcom/samsung/android/app/memo/uiwidget/AddNewLineFilter;->isBeforeLine:Z

    if-eqz v7, :cond_e

    iget v7, p0, Lcom/samsung/android/app/memo/uiwidget/AddNewLineFilter;->line:I

    if-ne v7, v4, :cond_e

    .line 2000
    add-int/lit8 v4, v4, -0x1

    .line 2003
    :cond_e
    if-eq v4, v11, :cond_0

    iget-object v7, p0, Lcom/samsung/android/app/memo/uiwidget/AddNewLineFilter;->proxy:Landroid/widget/EditText;

    invoke-virtual {v7}, Landroid/widget/EditText;->getLayout()Landroid/text/Layout;

    move-result-object v7

    if-eqz v7, :cond_0

    .line 2006
    iget-object v7, p0, Lcom/samsung/android/app/memo/uiwidget/AddNewLineFilter;->proxy:Landroid/widget/EditText;

    invoke-virtual {v7}, Landroid/widget/EditText;->getLayout()Landroid/text/Layout;

    move-result-object v7

    invoke-virtual {v7, v4}, Landroid/text/Layout;->getLineStart(I)I

    move-result v3

    .line 2007
    .restart local v3    # "lineStart":I
    iget-object v7, p0, Lcom/samsung/android/app/memo/uiwidget/AddNewLineFilter;->proxy:Landroid/widget/EditText;

    invoke-virtual {v7}, Landroid/widget/EditText;->getLayout()Landroid/text/Layout;

    move-result-object v7

    invoke-virtual {v7, v4}, Landroid/text/Layout;->getLineEnd(I)I

    move-result v2

    .line 2008
    .restart local v2    # "lineEnd":I
    iget-object v7, p0, Lcom/samsung/android/app/memo/uiwidget/AddNewLineFilter;->proxy:Landroid/widget/EditText;

    invoke-virtual {v7}, Landroid/widget/EditText;->getEditableText()Landroid/text/Editable;

    move-result-object v7

    .line 2009
    const-class v8, Lcom/samsung/android/app/memo/uiwidget/ClickableSpanType;

    .line 2008
    invoke-interface {v7, v3, v2, v8}, Landroid/text/Editable;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v5

    check-cast v5, [Lcom/samsung/android/app/memo/uiwidget/ClickableSpanType;

    .line 2010
    .restart local v5    # "spans":[Lcom/samsung/android/app/memo/uiwidget/ClickableSpanType;
    array-length v7, v5

    if-lez v7, :cond_0

    .line 2011
    iget-object v7, p0, Lcom/samsung/android/app/memo/uiwidget/AddNewLineFilter;->proxy:Landroid/widget/EditText;

    invoke-virtual {v7}, Landroid/widget/EditText;->getSelectionStart()I

    move-result v7

    iput v7, p0, Lcom/samsung/android/app/memo/uiwidget/AddNewLineFilter;->curSelection:I

    .line 2012
    iget-boolean v7, p0, Lcom/samsung/android/app/memo/uiwidget/AddNewLineFilter;->isLastLine:Z

    if-nez v7, :cond_f

    iget v7, p0, Lcom/samsung/android/app/memo/uiwidget/AddNewLineFilter;->curSelection:I

    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result v8

    if-eq v7, v8, :cond_10

    iget v7, p0, Lcom/samsung/android/app/memo/uiwidget/AddNewLineFilter;->curSelection:I

    invoke-interface {p1, v7}, Landroid/text/Editable;->charAt(I)C

    move-result v7

    if-ne v7, v12, :cond_10

    :cond_f
    iget-boolean v7, p0, Lcom/samsung/android/app/memo/uiwidget/AddNewLineFilter;->isMiddle:Z

    if-eqz v7, :cond_11

    .line 2013
    :cond_10
    iput-boolean v10, p0, Lcom/samsung/android/app/memo/uiwidget/AddNewLineFilter;->isLastLine:Z

    .line 2014
    iput-boolean v10, p0, Lcom/samsung/android/app/memo/uiwidget/AddNewLineFilter;->isMiddle:Z

    .line 2016
    :cond_11
    iget-object v7, p0, Lcom/samsung/android/app/memo/uiwidget/AddNewLineFilter;->proxy:Landroid/widget/EditText;

    iget v8, p0, Lcom/samsung/android/app/memo/uiwidget/AddNewLineFilter;->curSelection:I

    invoke-virtual {v7, v8}, Landroid/widget/EditText;->setSelection(I)V

    goto/16 :goto_0
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 11
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "count"    # I
    .param p4, "after"    # I

    .prologue
    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 2024
    add-int v5, p2, p3

    invoke-interface {p1, p2, v5}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v5

    iput-object v5, p0, Lcom/samsung/android/app/memo/uiwidget/AddNewLineFilter;->beforeText:Ljava/lang/CharSequence;

    .line 2025
    sub-int v5, p3, p4

    if-eq v5, v10, :cond_0

    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v5

    if-nez v5, :cond_1

    :cond_0
    sget-boolean v5, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->isFromImageDelete:Z

    if-nez v5, :cond_1

    .line 2026
    iput-boolean v10, p0, Lcom/samsung/android/app/memo/uiwidget/AddNewLineFilter;->mIsBackspace:Z

    .line 2029
    :goto_0
    sput-boolean v9, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->isFromImageDelete:Z

    .line 2030
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uiwidget/AddNewLineFilter;->getCurrentCursorLine()I

    move-result v5

    iput v5, p0, Lcom/samsung/android/app/memo/uiwidget/AddNewLineFilter;->line:I

    .line 2031
    iget v5, p0, Lcom/samsung/android/app/memo/uiwidget/AddNewLineFilter;->line:I

    const/4 v6, -0x1

    if-ne v5, v6, :cond_2

    .line 2060
    :goto_1
    return-void

    .line 2028
    :cond_1
    iput-boolean v9, p0, Lcom/samsung/android/app/memo/uiwidget/AddNewLineFilter;->mIsBackspace:Z

    goto :goto_0

    .line 2033
    :cond_2
    iget-object v5, p0, Lcom/samsung/android/app/memo/uiwidget/AddNewLineFilter;->proxy:Landroid/widget/EditText;

    invoke-virtual {v5}, Landroid/widget/EditText;->getSelectionStart()I

    move-result v5

    iput v5, p0, Lcom/samsung/android/app/memo/uiwidget/AddNewLineFilter;->curSelection:I

    .line 2034
    iget-object v5, p0, Lcom/samsung/android/app/memo/uiwidget/AddNewLineFilter;->proxy:Landroid/widget/EditText;

    invoke-virtual {v5}, Landroid/widget/EditText;->getLayout()Landroid/text/Layout;

    move-result-object v5

    iget v6, p0, Lcom/samsung/android/app/memo/uiwidget/AddNewLineFilter;->line:I

    invoke-virtual {v5, v6}, Landroid/text/Layout;->getLineStart(I)I

    move-result v1

    .line 2035
    .local v1, "lineStart":I
    iget-object v5, p0, Lcom/samsung/android/app/memo/uiwidget/AddNewLineFilter;->proxy:Landroid/widget/EditText;

    invoke-virtual {v5}, Landroid/widget/EditText;->getLayout()Landroid/text/Layout;

    move-result-object v5

    iget v6, p0, Lcom/samsung/android/app/memo/uiwidget/AddNewLineFilter;->line:I

    invoke-virtual {v5, v6}, Landroid/text/Layout;->getLineEnd(I)I

    move-result v0

    .line 2036
    .local v0, "lineEnd":I
    iget-object v5, p0, Lcom/samsung/android/app/memo/uiwidget/AddNewLineFilter;->proxy:Landroid/widget/EditText;

    invoke-virtual {v5}, Landroid/widget/EditText;->getEditableText()Landroid/text/Editable;

    move-result-object v5

    .line 2037
    const-class v6, Lcom/samsung/android/app/memo/uiwidget/ClickableSpanType;

    .line 2036
    invoke-interface {v5, v1, v0, v6}, Landroid/text/Editable;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Lcom/samsung/android/app/memo/uiwidget/ClickableSpanType;

    .line 2038
    .local v2, "spans":[Lcom/samsung/android/app/memo/uiwidget/ClickableSpanType;
    iget-object v5, p0, Lcom/samsung/android/app/memo/uiwidget/AddNewLineFilter;->proxy:Landroid/widget/EditText;

    invoke-virtual {v5}, Landroid/widget/EditText;->getEditableText()Landroid/text/Editable;

    move-result-object v5

    add-int/lit8 v6, v0, 0x1

    .line 2039
    const-class v7, Lcom/samsung/android/app/memo/uiwidget/ClickableSpanType;

    .line 2038
    invoke-interface {v5, v0, v6, v7}, Landroid/text/Editable;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [Lcom/samsung/android/app/memo/uiwidget/ClickableSpanType;

    .line 2040
    .local v3, "spansNext":[Lcom/samsung/android/app/memo/uiwidget/ClickableSpanType;
    iget-object v5, p0, Lcom/samsung/android/app/memo/uiwidget/AddNewLineFilter;->proxy:Landroid/widget/EditText;

    invoke-virtual {v5}, Landroid/widget/EditText;->getEditableText()Landroid/text/Editable;

    move-result-object v5

    add-int/lit8 v6, v1, -0x2

    add-int/lit8 v7, v1, -0x1

    .line 2041
    const-class v8, Lcom/samsung/android/app/memo/uiwidget/ClickableSpanType;

    .line 2040
    invoke-interface {v5, v6, v7, v8}, Landroid/text/Editable;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [Lcom/samsung/android/app/memo/uiwidget/ClickableSpanType;

    .line 2042
    .local v4, "spansPrev":[Lcom/samsung/android/app/memo/uiwidget/ClickableSpanType;
    array-length v5, v2

    if-lez v5, :cond_3

    .line 2043
    iput-boolean v10, p0, Lcom/samsung/android/app/memo/uiwidget/AddNewLineFilter;->isIMgSpan:Z

    .line 2047
    :goto_2
    iget v5, p0, Lcom/samsung/android/app/memo/uiwidget/AddNewLineFilter;->curSelection:I

    if-ne v5, v1, :cond_4

    .line 2048
    iput-boolean v10, p0, Lcom/samsung/android/app/memo/uiwidget/AddNewLineFilter;->isBeforeLine:Z

    .line 2052
    :goto_3
    iget v5, p0, Lcom/samsung/android/app/memo/uiwidget/AddNewLineFilter;->curSelection:I

    add-int/lit8 v5, v5, 0x1

    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v6

    if-ne v5, v6, :cond_5

    iget v5, p0, Lcom/samsung/android/app/memo/uiwidget/AddNewLineFilter;->curSelection:I

    invoke-interface {p1, v5}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v5

    const/16 v6, 0xa

    if-ne v5, v6, :cond_5

    .line 2053
    iput-boolean v10, p0, Lcom/samsung/android/app/memo/uiwidget/AddNewLineFilter;->isLastLine:Z

    .line 2056
    :goto_4
    array-length v5, v3

    if-lez v5, :cond_6

    array-length v5, v4

    if-lez v5, :cond_6

    .line 2057
    iput-boolean v10, p0, Lcom/samsung/android/app/memo/uiwidget/AddNewLineFilter;->isMiddle:Z

    goto :goto_1

    .line 2045
    :cond_3
    iput-boolean v9, p0, Lcom/samsung/android/app/memo/uiwidget/AddNewLineFilter;->isIMgSpan:Z

    goto :goto_2

    .line 2050
    :cond_4
    iput-boolean v9, p0, Lcom/samsung/android/app/memo/uiwidget/AddNewLineFilter;->isBeforeLine:Z

    goto :goto_3

    .line 2055
    :cond_5
    iput-boolean v9, p0, Lcom/samsung/android/app/memo/uiwidget/AddNewLineFilter;->isLastLine:Z

    goto :goto_4

    .line 2059
    :cond_6
    iput-boolean v9, p0, Lcom/samsung/android/app/memo/uiwidget/AddNewLineFilter;->isMiddle:Z

    goto/16 :goto_1
.end method

.method public getCurrentCursorLine()I
    .locals 4

    .prologue
    const/4 v2, -0x1

    .line 1905
    iget-object v3, p0, Lcom/samsung/android/app/memo/uiwidget/AddNewLineFilter;->proxy:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-static {v3}, Landroid/text/Selection;->getSelectionStart(Ljava/lang/CharSequence;)I

    move-result v1

    .line 1906
    .local v1, "selectionStart":I
    iget-object v3, p0, Lcom/samsung/android/app/memo/uiwidget/AddNewLineFilter;->proxy:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getLayout()Landroid/text/Layout;

    move-result-object v0

    .line 1908
    .local v0, "layout":Landroid/text/Layout;
    if-eqz v0, :cond_0

    if-eq v1, v2, :cond_0

    .line 1909
    invoke-virtual {v0, v1}, Landroid/text/Layout;->getLineForOffset(I)I

    move-result v2

    .line 1912
    :cond_0
    return v2
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 1
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "before"    # I
    .param p4, "count"    # I

    .prologue
    .line 2064
    iput p2, p0, Lcom/samsung/android/app/memo/uiwidget/AddNewLineFilter;->repStart:I

    .line 2066
    add-int v0, p2, p4

    invoke-interface {p1, p2, v0}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/AddNewLineFilter;->replacedText:Ljava/lang/CharSequence;

    .line 2067
    return-void
.end method
