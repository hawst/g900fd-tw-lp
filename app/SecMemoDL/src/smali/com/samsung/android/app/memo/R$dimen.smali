.class public final Lcom/samsung/android/app/memo/R$dimen;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/memo/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "dimen"
.end annotation


# static fields
.field public static final action_mode_back_icon_height:I = 0x7f090154

.field public static final action_mode_back_icon_padding_left:I = 0x7f090155

.field public static final action_mode_back_icon_padding_top:I = 0x7f090156

.field public static final action_mode_back_icon_width:I = 0x7f090153

.field public static final action_mode_spinner_drop_down_minHeight:I = 0x7f09006e

.field public static final action_mode_spinner_drop_down_minWidth:I = 0x7f09006f

.field public static final action_mode_spinner_drop_down_paddingLeft:I = 0x7f090070

.field public static final action_mode_spinner_drop_down_paddingRight:I = 0x7f090071

.field public static final action_mode_spinner_drop_down_width:I = 0x7f090157

.field public static final action_mode_spinner_img_margin_end:I = 0x7f090069

.field public static final action_mode_spinner_margin_bottom:I = 0x7f09006d

.field public static final action_mode_spinner_margin_left:I = 0x7f090149

.field public static final action_mode_spinner_margin_top:I = 0x7f09006c

.field public static final action_mode_spinner_padding_end:I = 0x7f09006a

.field public static final action_mode_spinner_text_size:I = 0x7f09006b

.field public static final app_defaultsize_h:I = 0x7f090096

.field public static final app_defaultsize_w:I = 0x7f090095

.field public static final app_minimumsize_h:I = 0x7f090098

.field public static final app_minimumsize_w:I = 0x7f090097

.field public static final btn_category_layout_height:I = 0x7f090107

.field public static final btn_category_layout_marginBottom:I = 0x7f090197

.field public static final btn_category_layout_marginLeft:I = 0x7f090198

.field public static final btn_category_layout_margin_bottom:I = 0x7f09010c

.field public static final btn_category_layout_margin_left:I = 0x7f09010d

.field public static final btn_category_layout_margin_right:I = 0x7f09010e

.field public static final btn_category_layout_margin_top:I = 0x7f0901a0

.field public static final btn_category_layout_margintop:I = 0x7f090199

.field public static final btn_category_layout_width:I = 0x7f090106

.field public static final btn_category_paddingBottom:I = 0x7f090196

.field public static final btn_category_padding_bottom:I = 0x7f090109

.field public static final btn_category_padding_left:I = 0x7f09010a

.field public static final btn_category_padding_right:I = 0x7f09010b

.field public static final btn_category_padding_top:I = 0x7f090108

.field public static final btn_insert_layout_height:I = 0x7f090113

.field public static final btn_insert_layout_marginBottom:I = 0x7f090118

.field public static final btn_insert_layout_marginLeft:I = 0x7f090119

.field public static final btn_insert_layout_marginRight:I = 0x7f09011a

.field public static final btn_insert_layout_marginTop:I = 0x7f09019a

.field public static final btn_insert_layout_width:I = 0x7f090112

.field public static final btn_insert_padding_bottom:I = 0x7f090117

.field public static final btn_insert_padding_left:I = 0x7f090114

.field public static final btn_insert_padding_right:I = 0x7f090115

.field public static final btn_insert_padding_top:I = 0x7f090116

.field public static final btn_voice_layout_height:I = 0x7f09011c

.field public static final btn_voice_layout_marginBottom:I = 0x7f090121

.field public static final btn_voice_layout_marginLeft:I = 0x7f090122

.field public static final btn_voice_layout_marginRight:I = 0x7f090123

.field public static final btn_voice_layout_marginTop:I = 0x7f09019b

.field public static final btn_voice_layout_width:I = 0x7f09011b

.field public static final btn_voice_padding_bottom:I = 0x7f09011e

.field public static final btn_voice_padding_left:I = 0x7f09011f

.field public static final btn_voice_padding_right:I = 0x7f090120

.field public static final btn_voice_padding_top:I = 0x7f09011d

.field public static final button_pause_layout_height:I = 0x7f090188

.field public static final button_pause_layout_width:I = 0x7f090187

.field public static final button_pause_paddingBottom:I = 0x7f090189

.field public static final button_pause_paddingLeft:I = 0x7f09018a

.field public static final button_pause_paddingRight:I = 0x7f09018b

.field public static final button_pause_paddingTop:I = 0x7f09018c

.field public static final button_play_layout_height:I = 0x7f090182

.field public static final button_play_layout_width:I = 0x7f090181

.field public static final button_play_paddingBottom:I = 0x7f090183

.field public static final button_play_paddingLeft:I = 0x7f090184

.field public static final button_play_paddingRight:I = 0x7f090185

.field public static final button_play_paddingTop:I = 0x7f090186

.field public static final category_chooser_add_button_side_margin_left:I = 0x7f09001a

.field public static final category_chooser_bubble_top_margin:I = 0x7f090017

.field public static final category_chooser_height:I = 0x7f090018

.field public static final category_chooser_img_margin_right:I = 0x7f090016

.field public static final category_chooser_item_height:I = 0x7f090013

.field public static final category_chooser_item_name_padding_right:I = 0x7f090014

.field public static final category_chooser_list_width:I = 0x7f090012

.field public static final category_chooser_side_margin_left:I = 0x7f090019

.field public static final category_chooser_side_margin_right:I = 0x7f09001b

.field public static final category_chooser_text_size:I = 0x7f090015

.field public static final category_edit_padding:I = 0x7f09001c

.field public static final category_fragment_list_padding_left:I = 0x7f09001f

.field public static final category_fragment_list_padding_right:I = 0x7f090020

.field public static final category_fragment_listview_padding_left:I = 0x7f09015a

.field public static final category_fragment_listview_padding_right:I = 0x7f09015b

.field public static final category_fragment_padding:I = 0x7f09001e

.field public static final category_fragment_padding_top:I = 0x7f090159

.field public static final category_input_dialog_height:I = 0x7f090031

.field public static final category_input_dialog_magin_bottom:I = 0x7f090030

.field public static final category_input_dialog_magin_left:I = 0x7f09002d

.field public static final category_input_dialog_magin_right:I = 0x7f09002e

.field public static final category_input_dialog_magin_top:I = 0x7f09002f

.field public static final category_input_dialog_text_height:I = 0x7f090032

.field public static final category_list_count_padding_left:I = 0x7f09000f

.field public static final category_list_count_padding_right:I = 0x7f090010

.field public static final category_list_edit_text_height:I = 0x7f09001d

.field public static final category_list_edit_text_size:I = 0x7f09000b

.field public static final category_list_edit_text_size_fonblet_hd:I = 0x7f090144

.field public static final category_list_height:I = 0x7f09000e

.field public static final category_list_item_height_fonblet_hd:I = 0x7f090134

.field public static final category_list_item_text_size_fonblet_hd:I = 0x7f090133

.field public static final category_list_padding:I = 0x7f09000c

.field public static final category_list_text_size:I = 0x7f09000a

.field public static final category_list_width:I = 0x7f09000d

.field public static final category_management_list_delete_btn_padding_left:I = 0x7f090024

.field public static final category_management_list_delete_btn_padding_right:I = 0x7f090025

.field public static final category_management_list_margin_right:I = 0x7f090011

.field public static final category_management_list_padding:I = 0x7f09014d

.field public static final category_management_list_padding_vertical:I = 0x7f090021

.field public static final category_management_list_reorder_btn_padding_left:I = 0x7f090022

.field public static final category_management_list_reorder_btn_padding_right:I = 0x7f090023

.field public static final category_management_noitem_popup_margin_right:I = 0x7f09002b

.field public static final category_management_noitem_popup_margin_top:I = 0x7f09002c

.field public static final category_management_noitem_popup_padding_bottom:I = 0x7f09002a

.field public static final category_management_noitem_popup_padding_left:I = 0x7f090027

.field public static final category_management_noitem_popup_padding_right:I = 0x7f090028

.field public static final category_management_noitem_popup_padding_top:I = 0x7f090029

.field public static final category_management_noitem_popup_text_size:I = 0x7f090026

.field public static final category_move_dialog_item_height:I = 0x7f090146

.field public static final category_move_dialog_padding_left:I = 0x7f090033

.field public static final category_move_dialog_padding_right:I = 0x7f090034

.field public static final category_move_dialog_padding_top:I = 0x7f090035

.field public static final category_move_padding_left:I = 0x7f090036

.field public static final category_move_padding_right:I = 0x7f090037

.field public static final category_move_text_padding_left:I = 0x7f090124

.field public static final chooser_image_margin_left:I = 0x7f09009f

.field public static final chooser_image_margin_right:I = 0x7f0900a0

.field public static final chooser_image_padding_bottom:I = 0x7f09009c

.field public static final chooser_image_padding_left:I = 0x7f09009d

.field public static final chooser_image_padding_right:I = 0x7f09009e

.field public static final chooser_image_padding_top:I = 0x7f09009b

.field public static final chooser_image_size:I = 0x7f0900a2

.field public static final chooser_image_text_size:I = 0x7f0900a1

.field public static final divder_padding:I = 0x7f090006

.field public static final dropdown_menu_left_margin:I = 0x7f090074

.field public static final edit_editor_margin_top:I = 0x7f09008e

.field public static final edit_editor_min_height:I = 0x7f090088

.field public static final edit_font_size:I = 0x7f090087

.field public static final edit_margin_bottom:I = 0x7f09008c

.field public static final edit_margin_left:I = 0x7f09008a

.field public static final edit_margin_right:I = 0x7f09008b

.field public static final edit_padding:I = 0x7f090089

.field public static final edit_scroll_view_height:I = 0x7f090148

.field public static final edit_shadow_image_height:I = 0x7f090132

.field public static final edit_title_margin_top:I = 0x7f09008d

.field public static final edit_title_padding_right:I = 0x7f090085

.field public static final edit_toolbar_bg_padding_bottom:I = 0x7f0900d8

.field public static final edit_toolbar_bg_padding_top:I = 0x7f0900d7

.field public static final edit_toolbar_divider_margin_bottom:I = 0x7f090083

.field public static final edit_toolbar_divider_margin_top:I = 0x7f090082

.field public static final edit_toolbar_divider_width:I = 0x7f090081

.field public static final edit_toolbar_ic_height:I = 0x7f090076

.field public static final edit_toolbar_ic_margin_bottom:I = 0x7f090080

.field public static final edit_toolbar_ic_margin_top:I = 0x7f09007f

.field public static final edit_toolbar_ic_padding_bottom:I = 0x7f09007a

.field public static final edit_toolbar_ic_padding_left:I = 0x7f09007b

.field public static final edit_toolbar_ic_padding_left_edge:I = 0x7f09007d

.field public static final edit_toolbar_ic_padding_right:I = 0x7f09007c

.field public static final edit_toolbar_ic_padding_right_edge:I = 0x7f09007e

.field public static final edit_toolbar_ic_padding_top:I = 0x7f090079

.field public static final edit_toolbar_ic_width:I = 0x7f090077

.field public static final edit_toolbar_ic_width_center:I = 0x7f090078

.field public static final edit_toolbar_padding_bottom:I = 0x7f09012a

.field public static final edit_toolbar_padding_left:I = 0x7f090075

.field public static final edit_toolbar_padding_top:I = 0x7f090129

.field public static final edit_toolbar_side_padding:I = 0x7f090084

.field public static final editor_font_size:I = 0x7f090158

.field public static final editor_line_spacing_extra:I = 0x7f090094

.field public static final editor_max_image_width:I = 0x7f09008f

.field public static final editor_shadow_img_height:I = 0x7f090090

.field public static final extra_bottom_margin:I = 0x7f090092

.field public static final extra_top_margin:I = 0x7f090091

.field public static final fab_btn_elevation:I = 0x7f090102

.field public static final fab_btn_height:I = 0x7f090101

.field public static final fab_btn_width:I = 0x7f090100

.field public static final image_popup_padding_bottom:I = 0x7f09009a

.field public static final image_popup_padding_top:I = 0x7f090099

.field public static final import_dialog_cb_margin_top:I = 0x7f090067

.field public static final import_dialog_cb_padding_left:I = 0x7f090068

.field public static final import_dialog_margin_left:I = 0x7f090063

.field public static final import_dialog_margin_right:I = 0x7f090062

.field public static final import_dialog_padding_bottom:I = 0x7f090065

.field public static final import_dialog_padding_top:I = 0x7f090064

.field public static final import_dialog_tv_margin_top:I = 0x7f090066

.field public static final insert_image_popup_icon_width:I = 0x7f0900a3

.field public static final linear_expandable_opened_layout_height:I = 0x7f0901a1

.field public static final linear_layout_layout_marginBottom:I = 0x7f09016e

.field public static final linear_layout_layout_marginLeft:I = 0x7f09016f

.field public static final linear_layout_layout_marginRight:I = 0x7f090170

.field public static final linear_layout_layout_marginTop:I = 0x7f090171

.field public static final linear_layout_layout_padding:I = 0x7f090172

.field public static final listview_divider_height:I = 0x7f090038

.field public static final main_selec_checkbox_margin_right:I = 0x7f09005d

.field public static final main_selectall_checkbox_margin_left:I = 0x7f09005c

.field public static final main_selectall_checkbox_margin_right:I = 0x7f09005b

.field public static final main_selectall_checkbox_margin_top:I = 0x7f09005a

.field public static final main_selectall_checkbox_width:I = 0x7f090059

.field public static final main_selectall_layout_height:I = 0x7f090052

.field public static final main_selectall_layout_margin_bottom:I = 0x7f090056

.field public static final main_selectall_layout_margin_left:I = 0x7f090053

.field public static final main_selectall_layout_margin_right:I = 0x7f090054

.field public static final main_selectall_layout_margin_textview_left:I = 0x7f090057

.field public static final main_selectall_layout_margin_top:I = 0x7f090055

.field public static final main_selectall_text_font_size:I = 0x7f090058

.field public static final memo_content_layout_margin:I = 0x7f09015c

.field public static final memo_list_contents_lineSpacingExtra:I = 0x7f09014c

.field public static final memo_list_date_time_layout_height:I = 0x7f09014b

.field public static final memo_list_date_time_margin_bottom:I = 0x7f090150

.field public static final memo_list_date_time_margin_left:I = 0x7f090128

.field public static final memo_list_gridview_horizontalSpacing:I = 0x7f09019d

.field public static final memo_list_gridview_layout_margin:I = 0x7f09019e

.field public static final memo_list_gridview_verticalSpacing:I = 0x7f09019c

.field public static final memo_list_image_layout_height:I = 0x7f090143

.field public static final memo_list_image_layout_height_fonblet_hd:I = 0x7f090141

.field public static final memo_list_image_layout_width:I = 0x7f090142

.field public static final memo_list_image_layout_width_fonblet_hd:I = 0x7f090140

.field public static final memo_list_item_contents_height:I = 0x7f09014f

.field public static final memo_list_item_contents_line_spacing_extra:I = 0x7f090151

.field public static final memo_list_item_contents_margin_bottom:I = 0x7f090135

.field public static final memo_list_item_contents_margin_left:I = 0x7f09003c

.field public static final memo_list_item_contents_margin_left_date_time:I = 0x7f09019f

.field public static final memo_list_item_contents_margin_left_fonblet_hd:I = 0x7f09013a

.field public static final memo_list_item_contents_margin_right:I = 0x7f09003d

.field public static final memo_list_item_contents_margin_right_selection_mode:I = 0x7f09003e

.field public static final memo_list_item_contents_margin_top:I = 0x7f09003b

.field public static final memo_list_item_contents_margin_top_fonblet_hd:I = 0x7f090139

.field public static final memo_list_item_contents_text_size:I = 0x7f090046

.field public static final memo_list_item_contents_text_size_fonblet_hd:I = 0x7f09013e

.field public static final memo_list_item_date_time_compound_padding:I = 0x7f090042

.field public static final memo_list_item_date_time_height:I = 0x7f090126

.field public static final memo_list_item_date_time_margin_bottom:I = 0x7f090136

.field public static final memo_list_item_date_time_margin_left:I = 0x7f090104

.field public static final memo_list_item_date_time_margin_left_fonblet_hd:I = 0x7f09013b

.field public static final memo_list_item_date_time_margin_top:I = 0x7f090127

.field public static final memo_list_item_date_time_padding_bottom:I = 0x7f090041

.field public static final memo_list_item_date_time_padding_bottom_fonblet_hd:I = 0x7f09013c

.field public static final memo_list_item_date_time_padding_left:I = 0x7f090145

.field public static final memo_list_item_date_time_text_size:I = 0x7f090048

.field public static final memo_list_item_date_time_text_size_fonblet_hd:I = 0x7f09013f

.field public static final memo_list_item_height:I = 0x7f09014e

.field public static final memo_list_item_hover_image_padding:I = 0x7f090061

.field public static final memo_list_item_hover_image_width:I = 0x7f090060

.field public static final memo_list_item_image_alpha:I = 0x7f090045

.field public static final memo_list_item_image_height:I = 0x7f090044

.field public static final memo_list_item_image_width:I = 0x7f090043

.field public static final memo_list_item_min_height:I = 0x7f09003a

.field public static final memo_list_item_min_height_fonblet:I = 0x7f090137

.field public static final memo_list_item_padding_left:I = 0x7f09004b

.field public static final memo_list_item_padding_right:I = 0x7f09014a

.field public static final memo_list_item_padding_top:I = 0x7f09012b

.field public static final memo_list_item_rl_padding_left:I = 0x7f090039

.field public static final memo_list_item_title_height:I = 0x7f090125

.field public static final memo_list_item_title_margin_bottom:I = 0x7f09012c

.field public static final memo_list_item_title_margin_left_selection_mode:I = 0x7f090040

.field public static final memo_list_item_title_margin_top:I = 0x7f09003f

.field public static final memo_list_item_title_margin_top_fonblet_hd:I = 0x7f090138

.field public static final memo_list_item_title_text_size:I = 0x7f090047

.field public static final memo_list_item_title_text_size_fonblet_hd:I = 0x7f09013d

.field public static final memo_list_no_memos_popup_margin_right:I = 0x7f09004c

.field public static final memo_list_no_memos_popup_padding_bottom:I = 0x7f09004d

.field public static final memo_list_no_memos_popup_padding_top:I = 0x7f090049

.field public static final memo_list_no_memos_text_size:I = 0x7f09004a

.field public static final memo_list_search_margin_bottom:I = 0x7f0900e4

.field public static final memo_list_search_margin_left:I = 0x7f0900e1

.field public static final memo_list_search_margin_right:I = 0x7f0900e2

.field public static final memo_list_search_margin_top:I = 0x7f0900e3

.field public static final memo_select_dialog_height:I = 0x7f09004e

.field public static final memo_select_dialog_margin_left:I = 0x7f09004f

.field public static final memo_select_dialog_margin_right:I = 0x7f090050

.field public static final memo_select_dialog_text_size:I = 0x7f090051

.field public static final memo_view_tb_inc_text_size:I = 0x7f09015f

.field public static final memo_view_tb_padding_left:I = 0x7f090160

.field public static final open_source_licenses_text_size:I = 0x7f090103

.field public static final password_dialog_margin_left:I = 0x7f0900f1

.field public static final password_dialog_margin_right:I = 0x7f0900f2

.field public static final quick_panel_memo_button_margin_bottom:I = 0x7f090001

.field public static final quick_panel_memo_button_margin_left:I = 0x7f090000

.field public static final quick_panel_memo_button_margin_top:I = 0x7f090002

.field public static final quick_panel_memo_icon_width:I = 0x7f090131

.field public static final quick_panel_memo_time_width:I = 0x7f090130

.field public static final quick_panel_voice_image_button_height:I = 0x7f090004

.field public static final quick_panel_voice_image_button_margin_left:I = 0x7f090005

.field public static final quick_panel_voice_image_button_width:I = 0x7f090003

.field public static final recording_button_pause_layout_height:I = 0x7f090174

.field public static final recording_button_pause_layout_width:I = 0x7f090173

.field public static final recording_button_pause_paddingBottom:I = 0x7f090175

.field public static final recording_button_pause_paddingLeft:I = 0x7f090176

.field public static final recording_button_pause_paddingRight:I = 0x7f090177

.field public static final recording_button_pause_paddingTop:I = 0x7f090178

.field public static final recording_button_stop_layout_height:I = 0x7f09017a

.field public static final recording_button_stop_layout_width:I = 0x7f090179

.field public static final recording_button_stop_paddingBottom:I = 0x7f09017b

.field public static final recording_button_stop_paddingLeft:I = 0x7f09017c

.field public static final recording_button_stop_paddingRight:I = 0x7f09017d

.field public static final recording_button_stop_paddingTop:I = 0x7f09017e

.field public static final samsung_account_dialog_check_box_margin_top:I = 0x7f0900de

.field public static final samsung_account_dialog_check_box_padding_bottom:I = 0x7f0900e0

.field public static final samsung_account_dialog_check_box_padding_left:I = 0x7f0900df

.field public static final samsung_account_dialog_checkbox_margin_left:I = 0x7f0900db

.field public static final samsung_account_dialog_margin_left:I = 0x7f0900da

.field public static final samsung_account_dialog_margin_right:I = 0x7f0900d9

.field public static final samsung_account_dialog_padding_bottom:I = 0x7f0900dc

.field public static final samsung_account_dialog_padding_top:I = 0x7f0900dd

.field public static final selectall_layout_height:I = 0x7f09005f

.field public static final selectall_layout_padding_right:I = 0x7f090147

.field public static final selectall_padding_left:I = 0x7f09005e

.field public static final selectall_text_padding_left:I = 0x7f090152

.field public static final selection_menu_margin_bottom:I = 0x7f090195

.field public static final selection_menu_margin_top:I = 0x7f090194

.field public static final selection_menu_padding_left:I = 0x7f090072

.field public static final selection_menu_padding_right:I = 0x7f090073

.field public static final share_icon_width:I = 0x7f0900fd

.field public static final share_popup_height:I = 0x7f0900fb

.field public static final share_popup_list_height:I = 0x7f0900fa

.field public static final share_popup_padding:I = 0x7f0900fc

.field public static final share_popup_width:I = 0x7f0900f9

.field public static final share_text_left_padding:I = 0x7f0900fe

.field public static final share_via_text_size:I = 0x7f0900ff

.field public static final show_record_porgress_layout_marginLeft:I = 0x7f09018e

.field public static final show_record_porgress_layout_marginRight:I = 0x7f09018d

.field public static final spinner_text_size:I = 0x7f090193

.field public static final sync_memo_dialog_margin_bottom:I = 0x7f0900f6

.field public static final sync_memo_dialog_margin_left:I = 0x7f0900f4

.field public static final sync_memo_dialog_margin_right:I = 0x7f0900f3

.field public static final sync_memo_dialog_margin_top:I = 0x7f0900f5

.field public static final theme_action_bar_count_style_size:I = 0x7f090008

.field public static final theme_action_bar_text_style_size:I = 0x7f090007

.field public static final theme_edit_action_bar_menu_text_style_size:I = 0x7f090009

.field public static final thumbnail_size:I = 0x7f090093

.field public static final timeText_endtime_textsize:I = 0x7f0900ef

.field public static final timeText_playtime_textsize:I = 0x7f0900ee

.field public static final title_font_size:I = 0x7f090086

.field public static final toolbar_bg_imageview_layout_height:I = 0x7f090110

.field public static final toolbar_bg_imageview_layout_margin_top:I = 0x7f090111

.field public static final toolbar_bg_imageview_layout_width:I = 0x7f09010f

.field public static final toolbar_margin_left:I = 0x7f09015d

.field public static final toolbar_margin_top:I = 0x7f09015e

.field public static final tv_record_button_layout_height:I = 0x7f090162

.field public static final tv_record_button_layout_marginBottom:I = 0x7f090163

.field public static final tv_record_button_layout_marginLeft:I = 0x7f090164

.field public static final tv_record_button_layout_marginRight:I = 0x7f090165

.field public static final tv_record_button_layout_marginTop:I = 0x7f090166

.field public static final tv_record_button_layout_width:I = 0x7f090161

.field public static final tv_record_button_paddingBottom:I = 0x7f090167

.field public static final tv_record_button_paddingLeft:I = 0x7f090168

.field public static final tv_record_button_paddingRight:I = 0x7f090169

.field public static final tv_record_button_paddingTop:I = 0x7f09016a

.field public static final tv_record_duration_layout_marginBottom:I = 0x7f09016b

.field public static final tv_record_duration_layout_marginRight:I = 0x7f09016d

.field public static final tv_record_duration_layout_marginTop:I = 0x7f09016c

.field public static final tv_record_duration_textSize:I = 0x7f0900ec

.field public static final tv_recording_duration_layout_marginBottom:I = 0x7f09017f

.field public static final tv_recording_duration_layout_marginTop:I = 0x7f090180

.field public static final tv_recording_duration_textSize:I = 0x7f0900ed

.field public static final tw_list_divider_layout_height:I = 0x7f090105

.field public static final voice_delete_btn_layout_height:I = 0x7f090192

.field public static final voice_delete_btn_layout_width:I = 0x7f090191

.field public static final voice_notification_margin_top:I = 0x7f0900f7

.field public static final voice_notification_text_size:I = 0x7f0900f8

.field public static final voice_record_btn_margin:I = 0x7f0900f0

.field public static final voice_seekbar_height:I = 0x7f0900e5

.field public static final voice_seekbar_margin_bottom:I = 0x7f0900ea

.field public static final voice_seekbar_margin_top:I = 0x7f0900eb

.field public static final voice_seekbar_padding_bottom:I = 0x7f0900e9

.field public static final voice_seekbar_padding_left:I = 0x7f0900e6

.field public static final voice_seekbar_padding_right:I = 0x7f0900e7

.field public static final voice_seekbar_padding_top:I = 0x7f0900e8

.field public static final vr_progress_layout_height:I = 0x7f09018f

.field public static final vr_progress_padding:I = 0x7f090190

.field public static final widget_background_img_margin_bottom:I = 0x7f09012e

.field public static final widget_background_img_margin_top:I = 0x7f09012d

.field public static final widget_empty_list_view_tablet_text_size:I = 0x7f0900c4

.field public static final widget_image_margin_bottom:I = 0x7f0900a7

.field public static final widget_image_margin_top:I = 0x7f0900a6

.field public static final widget_image_padding_bottom:I = 0x7f0900a8

.field public static final widget_image_padding_top_bottom:I = 0x7f0900a5

.field public static final widget_image_width:I = 0x7f0900ac

.field public static final widget_listview_bottom_margin:I = 0x7f0900c2

.field public static final widget_listview_padding:I = 0x7f0900c3

.field public static final widget_listview_top_margin:I = 0x7f0900c1

.field public static final widget_min_height:I = 0x7f0900ad

.field public static final widget_min_height_resize:I = 0x7f0900d5

.field public static final widget_min_width:I = 0x7f0900ae

.field public static final widget_min_width_resize:I = 0x7f0900d6

.field public static final widget_no_memos_bg_height:I = 0x7f0900b4

.field public static final widget_no_memos_image_height:I = 0x7f0900b3

.field public static final widget_no_memos_image_width:I = 0x7f0900b2

.field public static final widget_no_memos_min_text_size:I = 0x7f0900af

.field public static final widget_no_memos_over_two_lines_text_size:I = 0x7f0900b1

.field public static final widget_no_memos_text_alpha:I = 0x7f0900b6

.field public static final widget_no_memos_text_padding:I = 0x7f0900b5

.field public static final widget_no_memos_text_size:I = 0x7f0900b0

.field public static final widget_outline_margin_bottom:I = 0x7f0900c0

.field public static final widget_outline_top_padding:I = 0x7f0900bf

.field public static final widget_row_padding_left:I = 0x7f0900d1

.field public static final widget_row_padding_right:I = 0x7f0900d2

.field public static final widget_row_padding_top:I = 0x7f0900d4

.field public static final widget_shadow_height:I = 0x7f0900c6

.field public static final widget_shadow_img_height:I = 0x7f09012f

.field public static final widget_shadow_img_padding_left:I = 0x7f0900c7

.field public static final widget_shadow_img_padding_right:I = 0x7f0900c8

.field public static final widget_shadow_padding_left:I = 0x7f0900c9

.field public static final widget_shadow_padding_right:I = 0x7f0900ca

.field public static final widget_text_line_spacing_extra:I = 0x7f0900ab

.field public static final widget_text_size:I = 0x7f0900aa

.field public static final widget_text_view_height_1:I = 0x7f0900b8

.field public static final widget_text_view_height_2:I = 0x7f0900b9

.field public static final widget_text_view_height_3:I = 0x7f0900ba

.field public static final widget_text_view_height_4:I = 0x7f0900bb

.field public static final widget_text_view_height_5:I = 0x7f0900bc

.field public static final widget_text_view_height_6:I = 0x7f0900bd

.field public static final widget_text_view_height_with_image:I = 0x7f0900b7

.field public static final widget_title_height:I = 0x7f0900be

.field public static final widget_title_padding_bottom:I = 0x7f0900a4

.field public static final widget_title_size:I = 0x7f0900a9

.field public static final widget_voice_icon_height:I = 0x7f0900cc

.field public static final widget_voice_icon_margin_bottom:I = 0x7f0900d0

.field public static final widget_voice_icon_margin_right:I = 0x7f0900cf

.field public static final widget_voice_icon_padding_left:I = 0x7f0900cd

.field public static final widget_voice_icon_padding_right:I = 0x7f0900ce

.field public static final widget_voice_icon_width:I = 0x7f0900cb

.field public static final widget_voice_memo_list_view_text_size:I = 0x7f0900c5

.field public static final widget_voice_row_padding_right:I = 0x7f0900d3


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 447
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
