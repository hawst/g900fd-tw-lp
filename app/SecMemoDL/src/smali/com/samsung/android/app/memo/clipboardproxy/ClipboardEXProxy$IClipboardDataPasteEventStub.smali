.class Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy$IClipboardDataPasteEventStub;
.super Landroid/os/Binder;
.source "ClipboardEXProxy.java"

# interfaces
.implements Landroid/os/IInterface;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "IClipboardDataPasteEventStub"
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "android.sec.clipboard.IClipboardDataPasteEvent"

.field public static final TRANSACTION_onClipboardDataPaste:I = 0x1


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;


# direct methods
.method private constructor <init>(Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;)V
    .locals 0

    .prologue
    .line 430
    iput-object p1, p0, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy$IClipboardDataPasteEventStub;->this$0:Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;

    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy$IClipboardDataPasteEventStub;)V
    .locals 0

    .prologue
    .line 430
    invoke-direct {p0, p1}, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy$IClipboardDataPasteEventStub;-><init>(Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;)V

    return-void
.end method

.method private onClipboardDataPaste(Ljava/lang/Object;)V
    .locals 15
    .param p1, "clipdata"    # Ljava/lang/Object;

    .prologue
    .line 489
    iget-object v12, p0, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy$IClipboardDataPasteEventStub;->this$0:Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;

    const/4 v13, 0x1

    invoke-static {v12, v13}, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->access$4(Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;Z)V

    .line 490
    if-eqz p1, :cond_0

    .line 491
    const-string v12, "ClipboardManagerProxy"

    new-instance v13, Ljava/lang/StringBuilder;

    const-string v14, "onClipboardDataPaste-"

    invoke-direct {v13, v14}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p1

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 493
    const/4 v11, 0x0

    .line 494
    .local v11, "seqText":Ljava/lang/CharSequence;
    new-instance v10, Lcom/samsung/android/app/memo/clipboardproxy/ClipProxyData;

    invoke-direct {v10}, Lcom/samsung/android/app/memo/clipboardproxy/ClipProxyData;-><init>()V

    .line 496
    .local v10, "proxyData":Lcom/samsung/android/app/memo/clipboardproxy/ClipProxyData;
    :try_start_0
    invoke-virtual/range {p1 .. p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v12

    .line 497
    const-string v13, "android.sec.clipboard.data.list.ClipboardDataHTMLFragment"

    invoke-virtual {v12, v13}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v12

    if-eqz v12, :cond_1

    .line 498
    invoke-virtual/range {p1 .. p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    .line 500
    .local v2, "ClipboardDataHTMLFragment":Ljava/lang/Class;, "Ljava/lang/Class<+Ljava/lang/Object;>;"
    const-string v12, "GetHTMLFragment"

    const/4 v13, 0x0

    .line 499
    invoke-virtual {v2, v12, v13}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v6

    .line 501
    .local v6, "GetHTMLFragment":Ljava/lang/reflect/Method;
    const/4 v12, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v6, v0, v12}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v12

    move-object v0, v12

    check-cast v0, Ljava/lang/CharSequence;

    move-object v11, v0

    .line 502
    const/4 v12, 0x4

    invoke-virtual {v10, v12}, Lcom/samsung/android/app/memo/clipboardproxy/ClipProxyData;->setFormat(I)V

    .line 522
    .end local v2    # "ClipboardDataHTMLFragment":Ljava/lang/Class;, "Ljava/lang/Class<+Ljava/lang/Object;>;"
    .end local v6    # "GetHTMLFragment":Ljava/lang/reflect/Method;
    :goto_0
    invoke-virtual {v10, v11}, Lcom/samsung/android/app/memo/clipboardproxy/ClipProxyData;->setData(Ljava/lang/CharSequence;)V
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_3

    .line 533
    :goto_1
    iget-object v12, p0, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy$IClipboardDataPasteEventStub;->this$0:Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;

    # getter for: Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->pasteListener:Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy$IClipboardEXProxyPateListener;
    invoke-static {v12}, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->access$5(Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;)Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy$IClipboardEXProxyPateListener;

    move-result-object v12

    if-eqz v12, :cond_0

    .line 534
    iget-object v12, p0, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy$IClipboardDataPasteEventStub;->this$0:Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;

    # getter for: Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->pasteListener:Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy$IClipboardEXProxyPateListener;
    invoke-static {v12}, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->access$5(Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;)Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy$IClipboardEXProxyPateListener;

    move-result-object v12

    invoke-interface {v12, v10}, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy$IClipboardEXProxyPateListener;->onClipboardPaste(Lcom/samsung/android/app/memo/clipboardproxy/ClipProxyData;)V

    .line 537
    .end local v10    # "proxyData":Lcom/samsung/android/app/memo/clipboardproxy/ClipProxyData;
    .end local v11    # "seqText":Ljava/lang/CharSequence;
    :cond_0
    return-void

    .line 503
    .restart local v10    # "proxyData":Lcom/samsung/android/app/memo/clipboardproxy/ClipProxyData;
    .restart local v11    # "seqText":Ljava/lang/CharSequence;
    :cond_1
    :try_start_1
    invoke-virtual/range {p1 .. p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v12

    .line 504
    const-string v13, "android.sec.clipboard.data.list.ClipboardDataBitmap"

    invoke-virtual {v12, v13}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v12

    if-eqz v12, :cond_2

    .line 505
    invoke-virtual/range {p1 .. p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    .line 506
    .local v1, "ClipboardDataBitmap":Ljava/lang/Class;, "Ljava/lang/Class<+Ljava/lang/Object;>;"
    const-string v12, "GetBitmapPath"

    const/4 v13, 0x0

    invoke-virtual {v1, v12, v13}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v4

    .line 507
    .local v4, "GetBitmapPath":Ljava/lang/reflect/Method;
    const/4 v12, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v4, v0, v12}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v12

    move-object v0, v12

    check-cast v0, Ljava/lang/CharSequence;

    move-object v11, v0

    .line 508
    const/4 v12, 0x3

    invoke-virtual {v10, v12}, Lcom/samsung/android/app/memo/clipboardproxy/ClipProxyData;->setFormat(I)V
    :try_end_1
    .catch Ljava/lang/NoSuchMethodException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_1 .. :try_end_1} :catch_3

    goto :goto_0

    .line 523
    .end local v1    # "ClipboardDataBitmap":Ljava/lang/Class;, "Ljava/lang/Class<+Ljava/lang/Object;>;"
    .end local v4    # "GetBitmapPath":Ljava/lang/reflect/Method;
    :catch_0
    move-exception v8

    .line 524
    .local v8, "e":Ljava/lang/NoSuchMethodException;
    iget-object v12, p0, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy$IClipboardDataPasteEventStub;->this$0:Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;

    # getter for: Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->TAG:Ljava/lang/String;
    invoke-static {v12}, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->access$2(Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;)Ljava/lang/String;

    move-result-object v12

    const-string v13, "onClipboardDataPaste"

    invoke-static {v12, v13, v8}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 509
    .end local v8    # "e":Ljava/lang/NoSuchMethodException;
    :cond_2
    :try_start_2
    invoke-virtual/range {p1 .. p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v12

    .line 510
    const-string v13, "android.sec.clipboard.data.list.ClipboardDataUri"

    invoke-virtual {v12, v13}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v12

    if-eqz v12, :cond_3

    .line 511
    invoke-virtual/range {p1 .. p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    .line 512
    .restart local v1    # "ClipboardDataBitmap":Ljava/lang/Class;, "Ljava/lang/Class<+Ljava/lang/Object;>;"
    const-string v12, "GetUri"

    const/4 v13, 0x0

    invoke-virtual {v1, v12, v13}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v5

    .line 513
    .local v5, "GetClipdataUri":Ljava/lang/reflect/Method;
    const/4 v12, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v5, v0, v12}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/net/Uri;

    .line 514
    .local v9, "mUri":Landroid/net/Uri;
    invoke-virtual {v9}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v11

    .line 515
    const/4 v12, 0x5

    invoke-virtual {v10, v12}, Lcom/samsung/android/app/memo/clipboardproxy/ClipProxyData;->setFormat(I)V
    :try_end_2
    .catch Ljava/lang/NoSuchMethodException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_2 .. :try_end_2} :catch_3

    goto :goto_0

    .line 525
    .end local v1    # "ClipboardDataBitmap":Ljava/lang/Class;, "Ljava/lang/Class<+Ljava/lang/Object;>;"
    .end local v5    # "GetClipdataUri":Ljava/lang/reflect/Method;
    .end local v9    # "mUri":Landroid/net/Uri;
    :catch_1
    move-exception v8

    .line 526
    .local v8, "e":Ljava/lang/IllegalArgumentException;
    iget-object v12, p0, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy$IClipboardDataPasteEventStub;->this$0:Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;

    # getter for: Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->TAG:Ljava/lang/String;
    invoke-static {v12}, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->access$2(Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;)Ljava/lang/String;

    move-result-object v12

    const-string v13, "onClipboardDataPaste"

    invoke-static {v12, v13, v8}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_1

    .line 517
    .end local v8    # "e":Ljava/lang/IllegalArgumentException;
    :cond_3
    :try_start_3
    invoke-virtual/range {p1 .. p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    .line 518
    .local v3, "ClipboardDataText":Ljava/lang/Class;, "Ljava/lang/Class<+Ljava/lang/Object;>;"
    const-string v12, "GetText"

    const/4 v13, 0x0

    invoke-virtual {v3, v12, v13}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v7

    .line 519
    .local v7, "GetText":Ljava/lang/reflect/Method;
    const/4 v12, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v7, v0, v12}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v12

    move-object v0, v12

    check-cast v0, Ljava/lang/CharSequence;

    move-object v11, v0

    .line 520
    const/4 v12, 0x2

    invoke-virtual {v10, v12}, Lcom/samsung/android/app/memo/clipboardproxy/ClipProxyData;->setFormat(I)V
    :try_end_3
    .catch Ljava/lang/NoSuchMethodException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_3 .. :try_end_3} :catch_2
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_3 .. :try_end_3} :catch_3

    goto/16 :goto_0

    .line 527
    .end local v3    # "ClipboardDataText":Ljava/lang/Class;, "Ljava/lang/Class<+Ljava/lang/Object;>;"
    .end local v7    # "GetText":Ljava/lang/reflect/Method;
    :catch_2
    move-exception v8

    .line 528
    .local v8, "e":Ljava/lang/IllegalAccessException;
    iget-object v12, p0, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy$IClipboardDataPasteEventStub;->this$0:Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;

    # getter for: Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->TAG:Ljava/lang/String;
    invoke-static {v12}, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->access$2(Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;)Ljava/lang/String;

    move-result-object v12

    const-string v13, "onClipboardDataPaste"

    invoke-static {v12, v13, v8}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_1

    .line 529
    .end local v8    # "e":Ljava/lang/IllegalAccessException;
    :catch_3
    move-exception v8

    .line 530
    .local v8, "e":Ljava/lang/reflect/InvocationTargetException;
    iget-object v12, p0, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy$IClipboardDataPasteEventStub;->this$0:Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;

    # getter for: Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->TAG:Ljava/lang/String;
    invoke-static {v12}, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->access$2(Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;)Ljava/lang/String;

    move-result-object v12

    const-string v13, "onClipboardDataPaste"

    invoke-static {v12, v13, v8}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_1
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .locals 0

    .prologue
    .line 443
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .locals 11
    .param p1, "code"    # I
    .param p2, "data"    # Landroid/os/Parcel;
    .param p3, "reply"    # Landroid/os/Parcel;
    .param p4, "flags"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 449
    packed-switch p1, :pswitch_data_0

    .line 485
    iget-object v7, p0, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy$IClipboardDataPasteEventStub;->this$0:Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;

    # getter for: Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->isClipBoardExServiceEnabled:Z
    invoke-static {v7}, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->access$3(Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;)Z

    move-result v7

    :goto_0
    return v7

    .line 451
    :pswitch_0
    const-string v7, "android.sec.clipboard.IClipboardDataPasteEvent"

    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 452
    const/4 v4, 0x0

    .line 453
    .local v4, "_arg0":Ljava/lang/Object;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v7

    if-eqz v7, :cond_0

    .line 456
    :try_start_0
    const-string v7, "android.sec.clipboard.data.ClipboardData"

    invoke-static {v7}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    .line 457
    .local v1, "ClipboardData":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    const-string v7, "CREATOR"

    invoke-virtual {v1, v7}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v0

    .line 458
    .local v0, "CREATOR":Ljava/lang/reflect/Field;
    const/4 v7, 0x0

    invoke-virtual {v0, v7}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    .line 459
    .local v5, "creObj":Ljava/lang/Object;
    invoke-virtual {v5}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    .line 460
    .local v2, "Creator":Ljava/lang/Class;, "Ljava/lang/Class<+Ljava/lang/Object;>;"
    const-string v7, "createFromParcel"

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Class;

    const/4 v9, 0x0

    const-class v10, Landroid/os/Parcel;

    aput-object v10, v8, v9

    invoke-virtual {v2, v7, v8}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v3

    .line 461
    .local v3, "CreatorM":Ljava/lang/reflect/Method;
    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    aput-object p2, v7, v8

    invoke-virtual {v3, v5, v7}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NoSuchFieldException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_5

    move-result-object v4

    .line 478
    .end local v0    # "CREATOR":Ljava/lang/reflect/Field;
    .end local v1    # "ClipboardData":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    .end local v2    # "Creator":Ljava/lang/Class;, "Ljava/lang/Class<+Ljava/lang/Object;>;"
    .end local v3    # "CreatorM":Ljava/lang/reflect/Method;
    .end local v4    # "_arg0":Ljava/lang/Object;
    .end local v5    # "creObj":Ljava/lang/Object;
    :goto_1
    invoke-direct {p0, v4}, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy$IClipboardDataPasteEventStub;->onClipboardDataPaste(Ljava/lang/Object;)V

    .line 479
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 480
    const/4 v7, 0x1

    goto :goto_0

    .line 462
    .restart local v4    # "_arg0":Ljava/lang/Object;
    :catch_0
    move-exception v6

    .line 463
    .local v6, "e":Ljava/lang/ClassNotFoundException;
    iget-object v7, p0, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy$IClipboardDataPasteEventStub;->this$0:Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;

    # getter for: Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->TAG:Ljava/lang/String;
    invoke-static {v7}, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->access$2(Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;)Ljava/lang/String;

    move-result-object v7

    const-string v8, "onTransact"

    invoke-static {v7, v8, v6}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 464
    .end local v6    # "e":Ljava/lang/ClassNotFoundException;
    :catch_1
    move-exception v6

    .line 465
    .local v6, "e":Ljava/lang/NoSuchFieldException;
    iget-object v7, p0, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy$IClipboardDataPasteEventStub;->this$0:Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;

    # getter for: Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->TAG:Ljava/lang/String;
    invoke-static {v7}, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->access$2(Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;)Ljava/lang/String;

    move-result-object v7

    const-string v8, "onTransact"

    invoke-static {v7, v8, v6}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 466
    .end local v6    # "e":Ljava/lang/NoSuchFieldException;
    :catch_2
    move-exception v6

    .line 467
    .local v6, "e":Ljava/lang/IllegalArgumentException;
    iget-object v7, p0, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy$IClipboardDataPasteEventStub;->this$0:Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;

    # getter for: Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->TAG:Ljava/lang/String;
    invoke-static {v7}, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->access$2(Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;)Ljava/lang/String;

    move-result-object v7

    const-string v8, "onTransact"

    invoke-static {v7, v8, v6}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 468
    .end local v6    # "e":Ljava/lang/IllegalArgumentException;
    :catch_3
    move-exception v6

    .line 469
    .local v6, "e":Ljava/lang/IllegalAccessException;
    iget-object v7, p0, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy$IClipboardDataPasteEventStub;->this$0:Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;

    # getter for: Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->TAG:Ljava/lang/String;
    invoke-static {v7}, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->access$2(Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;)Ljava/lang/String;

    move-result-object v7

    const-string v8, "onTransact"

    invoke-static {v7, v8, v6}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 470
    .end local v6    # "e":Ljava/lang/IllegalAccessException;
    :catch_4
    move-exception v6

    .line 471
    .local v6, "e":Ljava/lang/NoSuchMethodException;
    iget-object v7, p0, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy$IClipboardDataPasteEventStub;->this$0:Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;

    # getter for: Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->TAG:Ljava/lang/String;
    invoke-static {v7}, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->access$2(Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;)Ljava/lang/String;

    move-result-object v7

    const-string v8, "onTransact"

    invoke-static {v7, v8, v6}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 472
    .end local v6    # "e":Ljava/lang/NoSuchMethodException;
    :catch_5
    move-exception v6

    .line 473
    .local v6, "e":Ljava/lang/reflect/InvocationTargetException;
    iget-object v7, p0, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy$IClipboardDataPasteEventStub;->this$0:Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;

    # getter for: Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->TAG:Ljava/lang/String;
    invoke-static {v7}, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->access$2(Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;)Ljava/lang/String;

    move-result-object v7

    const-string v8, "onTransact"

    invoke-static {v7, v8, v6}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 476
    .end local v6    # "e":Ljava/lang/reflect/InvocationTargetException;
    :cond_0
    const/4 v4, 0x0

    goto :goto_1

    .line 449
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method
