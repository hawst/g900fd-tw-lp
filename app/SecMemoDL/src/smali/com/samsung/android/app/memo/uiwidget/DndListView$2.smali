.class Lcom/samsung/android/app/memo/uiwidget/DndListView$2;
.super Ljava/lang/Object;
.source "DndListView.java"

# interfaces
.implements Landroid/view/animation/Animation$AnimationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/app/memo/uiwidget/DndListView;->doAnimationForDrag(Landroid/graphics/Bitmap;II)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/memo/uiwidget/DndListView;

.field private final synthetic val$dragAnimation:Lcom/samsung/android/app/memo/uiwidget/animation/MoveAnimation;

.field private final synthetic val$localRect:Landroid/graphics/Rect;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/memo/uiwidget/DndListView;Lcom/samsung/android/app/memo/uiwidget/animation/MoveAnimation;Landroid/graphics/Rect;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsung/android/app/memo/uiwidget/DndListView$2;->this$0:Lcom/samsung/android/app/memo/uiwidget/DndListView;

    iput-object p2, p0, Lcom/samsung/android/app/memo/uiwidget/DndListView$2;->val$dragAnimation:Lcom/samsung/android/app/memo/uiwidget/animation/MoveAnimation;

    iput-object p3, p0, Lcom/samsung/android/app/memo/uiwidget/DndListView$2;->val$localRect:Landroid/graphics/Rect;

    .line 323
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/view/animation/Animation;)V
    .locals 2
    .param p1, "arg0"    # Landroid/view/animation/Animation;

    .prologue
    const/high16 v1, 0x3f800000    # 1.0f

    .line 335
    iget-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/DndListView$2;->val$dragAnimation:Lcom/samsung/android/app/memo/uiwidget/animation/MoveAnimation;

    invoke-virtual {v0, v1, v1, v1, v1}, Lcom/samsung/android/app/memo/uiwidget/animation/MoveAnimation;->setScale(FFFF)V

    .line 336
    iget-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/DndListView$2;->val$dragAnimation:Lcom/samsung/android/app/memo/uiwidget/animation/MoveAnimation;

    invoke-virtual {v0}, Lcom/samsung/android/app/memo/uiwidget/animation/MoveAnimation;->startNow()V

    .line 337
    iget-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/DndListView$2;->this$0:Lcom/samsung/android/app/memo/uiwidget/DndListView;

    iget-object v1, p0, Lcom/samsung/android/app/memo/uiwidget/DndListView$2;->val$localRect:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/memo/uiwidget/DndListView;->invalidate(Landroid/graphics/Rect;)V

    .line 338
    return-void
.end method

.method public onAnimationRepeat(Landroid/view/animation/Animation;)V
    .locals 0
    .param p1, "arg0"    # Landroid/view/animation/Animation;

    .prologue
    .line 331
    return-void
.end method

.method public onAnimationStart(Landroid/view/animation/Animation;)V
    .locals 0
    .param p1, "arg0"    # Landroid/view/animation/Animation;

    .prologue
    .line 327
    return-void
.end method
