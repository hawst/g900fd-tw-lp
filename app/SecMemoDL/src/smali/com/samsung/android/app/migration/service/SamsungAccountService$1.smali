.class Lcom/samsung/android/app/migration/service/SamsungAccountService$1;
.super Ljava/lang/Object;
.source "SamsungAccountService.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/migration/service/SamsungAccountService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/migration/service/SamsungAccountService;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/migration/service/SamsungAccountService;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsung/android/app/migration/service/SamsungAccountService$1;->this$0:Lcom/samsung/android/app/migration/service/SamsungAccountService;

    .line 78
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 4
    .param p1, "name"    # Landroid/content/ComponentName;
    .param p2, "service"    # Landroid/os/IBinder;

    .prologue
    const/4 v3, 0x1

    .line 88
    const-string v0, "SamsungAccountService"

    const-string v1, "Service connected"

    invoke-static {v0, v1}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 89
    iget-object v0, p0, Lcom/samsung/android/app/migration/service/SamsungAccountService$1;->this$0:Lcom/samsung/android/app/migration/service/SamsungAccountService;

    invoke-static {p2}, Lcom/msc/sa/aidl/ISAService$Stub;->asInterface(Landroid/os/IBinder;)Lcom/msc/sa/aidl/ISAService;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/app/migration/service/SamsungAccountService;->access$0(Lcom/samsung/android/app/migration/service/SamsungAccountService;Lcom/msc/sa/aidl/ISAService;)V

    .line 90
    iget-object v0, p0, Lcom/samsung/android/app/migration/service/SamsungAccountService$1;->this$0:Lcom/samsung/android/app/migration/service/SamsungAccountService;

    new-instance v1, Lcom/samsung/android/app/migration/service/SamsungAccountService$SACallback;

    iget-object v2, p0, Lcom/samsung/android/app/migration/service/SamsungAccountService$1;->this$0:Lcom/samsung/android/app/migration/service/SamsungAccountService;

    invoke-direct {v1, v2}, Lcom/samsung/android/app/migration/service/SamsungAccountService$SACallback;-><init>(Lcom/samsung/android/app/migration/service/SamsungAccountService;)V

    invoke-static {v0, v1}, Lcom/samsung/android/app/migration/service/SamsungAccountService;->access$1(Lcom/samsung/android/app/migration/service/SamsungAccountService;Lcom/samsung/android/app/migration/service/SamsungAccountService$SACallback;)V

    .line 91
    iget-object v0, p0, Lcom/samsung/android/app/migration/service/SamsungAccountService$1;->this$0:Lcom/samsung/android/app/migration/service/SamsungAccountService;

    # invokes: Lcom/samsung/android/app/migration/service/SamsungAccountService;->registerCallback()Z
    invoke-static {v0}, Lcom/samsung/android/app/migration/service/SamsungAccountService;->access$2(Lcom/samsung/android/app/migration/service/SamsungAccountService;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 92
    iget-object v0, p0, Lcom/samsung/android/app/migration/service/SamsungAccountService$1;->this$0:Lcom/samsung/android/app/migration/service/SamsungAccountService;

    iput-boolean v3, v0, Lcom/samsung/android/app/migration/service/SamsungAccountService;->mIsConnected:Z

    .line 93
    iget-object v0, p0, Lcom/samsung/android/app/migration/service/SamsungAccountService$1;->this$0:Lcom/samsung/android/app/migration/service/SamsungAccountService;

    invoke-virtual {v0}, Lcom/samsung/android/app/migration/service/SamsungAccountService;->requestAIDLInterface()V

    .line 100
    :cond_0
    :goto_0
    return-void

    .line 95
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/app/migration/service/SamsungAccountService$1;->this$0:Lcom/samsung/android/app/migration/service/SamsungAccountService;

    # invokes: Lcom/samsung/android/app/migration/service/SamsungAccountService;->registerCallback()Z
    invoke-static {v0}, Lcom/samsung/android/app/migration/service/SamsungAccountService;->access$2(Lcom/samsung/android/app/migration/service/SamsungAccountService;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 96
    iget-object v0, p0, Lcom/samsung/android/app/migration/service/SamsungAccountService$1;->this$0:Lcom/samsung/android/app/migration/service/SamsungAccountService;

    iput-boolean v3, v0, Lcom/samsung/android/app/migration/service/SamsungAccountService;->mIsConnected:Z

    .line 97
    iget-object v0, p0, Lcom/samsung/android/app/migration/service/SamsungAccountService$1;->this$0:Lcom/samsung/android/app/migration/service/SamsungAccountService;

    invoke-virtual {v0}, Lcom/samsung/android/app/migration/service/SamsungAccountService;->requestAIDLInterface()V

    goto :goto_0
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 2
    .param p1, "name"    # Landroid/content/ComponentName;

    .prologue
    .line 82
    const-string v0, "SamsungAccountService"

    const-string v1, "Service disconnected"

    invoke-static {v0, v1}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 83
    iget-object v0, p0, Lcom/samsung/android/app/migration/service/SamsungAccountService$1;->this$0:Lcom/samsung/android/app/migration/service/SamsungAccountService;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/samsung/android/app/migration/service/SamsungAccountService;->access$0(Lcom/samsung/android/app/migration/service/SamsungAccountService;Lcom/msc/sa/aidl/ISAService;)V

    .line 84
    return-void
.end method
