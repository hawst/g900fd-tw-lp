.class public Lcom/samsung/android/app/memo/print/AsyncPrintTask;
.super Landroid/os/AsyncTask;
.source "AsyncPrintTask.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Integer;",
        "Ljava/lang/Integer;",
        "Ljava/lang/Integer;",
        ">;"
    }
.end annotation


# static fields
.field private static final BITMAP_QUALITY_REDUCTION_FACTOR:I = 0x3

.field private static final BITMAP_SIZE_LIMIT:I = 0x1e8480

.field public static final GOOGLE_PRINT_FOLDER_NAME:Ljava/lang/String; = "/.tempMemoPrint"

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final PACKAGE_NAME_PRINT:Ljava/lang/String;

.field private final PACKAGE_NAME_SCONNECT_PRINT:Ljava/lang/String;

.field private final PRINT_FOLDER_NAME:Ljava/lang/String;

.field private imageWriter:Lcom/samsung/android/app/memo/print/Memo2ImageWritter;

.field isCreateFolderSuccess:Z

.field private isFromSConnectPrint:Z

.field private isGooglePrint:Z

.field private mDirFile:Ljava/io/File;

.field private mHtmlContent:Ljava/lang/String;

.field private mMemoSize:I

.field private final mPrintContext:Landroid/content/Context;

.field private mProgressDialog:Landroid/app/ProgressDialog;

.field private mSession:Lcom/samsung/android/app/memo/Session;

.field private mTitle:Ljava/lang/String;

.field private printerInfo:Landroid/os/Bundle;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 31
    const-class v0, Lcom/samsung/android/app/memo/print/Memo2ImageWritter;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/app/memo/print/AsyncPrintTask;->TAG:Ljava/lang/String;

    .line 37
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;ZILcom/samsung/android/app/memo/Session;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "showProgressBar"    # Z
    .param p3, "memoSize"    # I
    .param p4, "ss"    # Lcom/samsung/android/app/memo/Session;

    .prologue
    const/4 v1, 0x1

    const/4 v3, 0x0

    const/4 v0, 0x0

    .line 81
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 39
    const-string v2, "com.sec.android.app.mobileprint.PRINT"

    iput-object v2, p0, Lcom/samsung/android/app/memo/print/AsyncPrintTask;->PACKAGE_NAME_PRINT:Ljava/lang/String;

    .line 41
    const-string v2, "com.sec.android.app.mobileprint.SCONNECT_PRINT"

    iput-object v2, p0, Lcom/samsung/android/app/memo/print/AsyncPrintTask;->PACKAGE_NAME_SCONNECT_PRINT:Ljava/lang/String;

    .line 43
    const-string v2, "/temp"

    iput-object v2, p0, Lcom/samsung/android/app/memo/print/AsyncPrintTask;->PRINT_FOLDER_NAME:Ljava/lang/String;

    .line 47
    iput-object v3, p0, Lcom/samsung/android/app/memo/print/AsyncPrintTask;->imageWriter:Lcom/samsung/android/app/memo/print/Memo2ImageWritter;

    .line 49
    iput-object v3, p0, Lcom/samsung/android/app/memo/print/AsyncPrintTask;->mDirFile:Ljava/io/File;

    .line 51
    iput-object v3, p0, Lcom/samsung/android/app/memo/print/AsyncPrintTask;->mTitle:Ljava/lang/String;

    .line 55
    iput v0, p0, Lcom/samsung/android/app/memo/print/AsyncPrintTask;->mMemoSize:I

    .line 57
    iput-boolean v0, p0, Lcom/samsung/android/app/memo/print/AsyncPrintTask;->isCreateFolderSuccess:Z

    .line 59
    iput-boolean v0, p0, Lcom/samsung/android/app/memo/print/AsyncPrintTask;->isFromSConnectPrint:Z

    .line 61
    iput-boolean v1, p0, Lcom/samsung/android/app/memo/print/AsyncPrintTask;->isGooglePrint:Z

    .line 65
    iput-object v3, p0, Lcom/samsung/android/app/memo/print/AsyncPrintTask;->mSession:Lcom/samsung/android/app/memo/Session;

    .line 67
    const-string v2, ""

    iput-object v2, p0, Lcom/samsung/android/app/memo/print/AsyncPrintTask;->mHtmlContent:Ljava/lang/String;

    .line 82
    iput-object p1, p0, Lcom/samsung/android/app/memo/print/AsyncPrintTask;->mPrintContext:Landroid/content/Context;

    .line 83
    iput p3, p0, Lcom/samsung/android/app/memo/print/AsyncPrintTask;->mMemoSize:I

    .line 84
    iput-object p4, p0, Lcom/samsung/android/app/memo/print/AsyncPrintTask;->mSession:Lcom/samsung/android/app/memo/Session;

    .line 85
    sget-boolean v2, Lcom/samsung/android/app/memo/util/Utils;->HAVE_MOBILE_PRINT_APP:Z

    if-eqz v2, :cond_0

    :goto_0
    iput-boolean v0, p0, Lcom/samsung/android/app/memo/print/AsyncPrintTask;->isGooglePrint:Z

    .line 86
    return-void

    :cond_0
    move v0, v1

    .line 85
    goto :goto_0
.end method

.method private createScaledImages(Landroid/net/Uri;Ljava/lang/String;I)V
    .locals 8
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "imageName"    # Ljava/lang/String;
    .param p3, "orientation"    # I

    .prologue
    .line 235
    sget-object v5, Lcom/samsung/android/app/memo/print/AsyncPrintTask;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "getDrawableFromLocalUri() uri: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 236
    const/4 v1, 0x0

    .line 240
    .local v1, "bm":Landroid/graphics/Bitmap;
    :try_start_0
    iget-object v5, p0, Lcom/samsung/android/app/memo/print/AsyncPrintTask;->mPrintContext:Landroid/content/Context;

    invoke-static {v5, p1, p3}, Lcom/samsung/android/app/memo/util/ImageUtil;->getActualImageWidth(Landroid/content/Context;Landroid/net/Uri;I)I

    move-result v3

    .line 241
    .local v3, "imageWidth":I
    iget-object v5, p0, Lcom/samsung/android/app/memo/print/AsyncPrintTask;->mPrintContext:Landroid/content/Context;

    .line 242
    invoke-static {}, Lcom/samsung/android/app/memo/util/Utils;->getScreenWidthAbs()I

    move-result v6

    .line 241
    invoke-static {v5, p1, v3, p3, v6}, Lcom/samsung/android/app/memo/util/ImageUtil;->decodeImageScaledIf(Landroid/content/Context;Landroid/net/Uri;III)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 243
    new-instance v2, Ljava/io/FileOutputStream;

    new-instance v5, Ljava/io/File;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v7, p0, Lcom/samsung/android/app/memo/print/AsyncPrintTask;->mDirFile:Ljava/io/File;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "/"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-direct {v2, v5}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 244
    .local v2, "fosImage":Ljava/io/FileOutputStream;
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getAllocationByteCount()I

    move-result v5

    const v6, 0x1e8480

    if-le v5, v6, :cond_0

    const/16 v0, 0x21

    .line 246
    .local v0, "bitmap_quality":I
    :goto_0
    sget-object v5, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    invoke-virtual {v1, v5, v0, v2}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 247
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V

    .line 248
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 254
    .end local v0    # "bitmap_quality":I
    .end local v2    # "fosImage":Ljava/io/FileOutputStream;
    .end local v3    # "imageWidth":I
    :goto_1
    return-void

    .line 245
    .restart local v2    # "fosImage":Ljava/io/FileOutputStream;
    .restart local v3    # "imageWidth":I
    :cond_0
    const/16 v0, 0x64

    goto :goto_0

    .line 250
    .end local v2    # "fosImage":Ljava/io/FileOutputStream;
    .end local v3    # "imageWidth":I
    :catch_0
    move-exception v4

    .line 251
    .local v4, "ioe":Ljava/io/IOException;
    sget-object v5, Lcom/samsung/android/app/memo/print/AsyncPrintTask;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Failed to load image "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6, v4}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1
.end method

.method private customHtmlParser(Ljava/lang/String;)Ljava/lang/String;
    .locals 14
    .param p1, "content"    # Ljava/lang/String;

    .prologue
    .line 257
    move-object v9, p1

    .line 258
    .local v9, "returnString":Ljava/lang/String;
    const/4 v10, 0x0

    .line 259
    .local v10, "start":I
    const/4 v2, 0x0

    .line 260
    .local v2, "end":I
    const/4 v3, 0x0

    .line 262
    .local v3, "endOrientation":I
    const/4 v7, 0x0

    .line 263
    .local v7, "orientation":I
    const-string v5, "<img src="

    .line 264
    .local v5, "imageTag":Ljava/lang/String;
    const-string v0, " altText="

    .line 265
    .local v0, "altTextTag":Ljava/lang/String;
    const-string v8, " orientation="

    .line 267
    .local v8, "orientationTag":Ljava/lang/String;
    invoke-virtual {p1, v5, v10}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result v10

    .line 269
    :goto_0
    if-gtz v10, :cond_0

    .line 288
    return-object v9

    .line 270
    :cond_0
    add-int/lit8 v11, v10, 0x1

    invoke-virtual {p1, v8, v11}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result v2

    .line 271
    add-int/lit8 v11, v2, 0x1

    invoke-virtual {p1, v0, v11}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result v3

    .line 274
    :try_start_0
    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v11

    add-int/2addr v11, v2

    add-int/lit8 v11, v11, 0x1

    add-int/lit8 v12, v3, -0x1

    .line 273
    invoke-virtual {p1, v11, v12}, Ljava/lang/String;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v11

    check-cast v11, Ljava/lang/String;

    invoke-static {v11}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v7

    .line 278
    :goto_1
    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v11

    add-int/2addr v11, v10

    add-int/lit8 v11, v11, 0x1

    .line 279
    add-int/lit8 v12, v2, -0x1

    .line 278
    invoke-virtual {p1, v11, v12}, Ljava/lang/String;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v11

    check-cast v11, Ljava/lang/String;

    invoke-static {v11}, Landroid/net/Uri;->decode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    invoke-static {v11}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    .line 280
    .local v6, "mUri":Landroid/net/Uri;
    invoke-virtual {v6}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v11

    const/4 v12, 0x1

    invoke-interface {v11, v12}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 281
    .local v4, "imageName":Ljava/lang/String;
    invoke-direct {p0, v6, v4, v7}, Lcom/samsung/android/app/memo/print/AsyncPrintTask;->createScaledImages(Landroid/net/Uri;Ljava/lang/String;I)V

    .line 283
    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v11

    add-int/2addr v11, v10

    add-int/lit8 v11, v11, 0x1

    add-int/lit8 v12, v2, -0x1

    invoke-virtual {p1, v11, v12}, Ljava/lang/String;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v11

    check-cast v11, Ljava/lang/String;

    new-instance v12, Ljava/lang/StringBuilder;

    const-string v13, "file://"

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 284
    iget-object v13, p0, Lcom/samsung/android/app/memo/print/AsyncPrintTask;->mDirFile:Ljava/io/File;

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "/"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    .line 282
    invoke-virtual {v9, v11, v12}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v9

    .line 285
    move v10, v2

    .line 286
    const-string v11, "<img src"

    invoke-virtual {p1, v11, v10}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result v10

    goto :goto_0

    .line 275
    .end local v4    # "imageName":Ljava/lang/String;
    .end local v6    # "mUri":Landroid/net/Uri;
    :catch_0
    move-exception v1

    .line 276
    .local v1, "e":Ljava/lang/NumberFormatException;
    sget-object v11, Lcom/samsung/android/app/memo/print/AsyncPrintTask;->TAG:Ljava/lang/String;

    const-string v12, "customHtmlParser "

    invoke-static {v11, v12, v1}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1
.end method

.method private getHtmlContent(I)Ljava/lang/String;
    .locals 6
    .param p1, "id"    # I

    .prologue
    .line 292
    const-string v0, ""

    .line 293
    .local v0, "content":Ljava/lang/String;
    iget-object v2, p0, Lcom/samsung/android/app/memo/print/AsyncPrintTask;->mSession:Lcom/samsung/android/app/memo/Session;

    if-nez v2, :cond_0

    .line 295
    :try_start_0
    new-instance v2, Lcom/samsung/android/app/memo/Session;

    int-to-long v4, p1

    invoke-direct {v2, v4, v5}, Lcom/samsung/android/app/memo/Session;-><init>(J)V

    iput-object v2, p0, Lcom/samsung/android/app/memo/print/AsyncPrintTask;->mSession:Lcom/samsung/android/app/memo/Session;
    :try_end_0
    .catch Lcom/samsung/android/app/memo/Session$SessionException; {:try_start_0 .. :try_end_0} :catch_0

    .line 300
    :cond_0
    :goto_0
    iget-object v2, p0, Lcom/samsung/android/app/memo/print/AsyncPrintTask;->mSession:Lcom/samsung/android/app/memo/Session;

    invoke-virtual {v2}, Lcom/samsung/android/app/memo/Session;->getContent()Ljava/lang/String;

    move-result-object v0

    .line 301
    return-object v0

    .line 296
    :catch_0
    move-exception v1

    .line 297
    .local v1, "e":Lcom/samsung/android/app/memo/Session$SessionException;
    sget-object v2, Lcom/samsung/android/app/memo/print/AsyncPrintTask;->TAG:Ljava/lang/String;

    const-string v3, "getHtmlContent"

    invoke-static {v2, v3, v1}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private getMemoTitle(I)Ljava/lang/String;
    .locals 6
    .param p1, "id"    # I

    .prologue
    .line 305
    const-string v0, ""

    .line 306
    .local v0, "content":Ljava/lang/String;
    iget-object v2, p0, Lcom/samsung/android/app/memo/print/AsyncPrintTask;->mSession:Lcom/samsung/android/app/memo/Session;

    if-nez v2, :cond_0

    .line 308
    :try_start_0
    new-instance v2, Lcom/samsung/android/app/memo/Session;

    int-to-long v4, p1

    invoke-direct {v2, v4, v5}, Lcom/samsung/android/app/memo/Session;-><init>(J)V

    iput-object v2, p0, Lcom/samsung/android/app/memo/print/AsyncPrintTask;->mSession:Lcom/samsung/android/app/memo/Session;
    :try_end_0
    .catch Lcom/samsung/android/app/memo/Session$SessionException; {:try_start_0 .. :try_end_0} :catch_0

    .line 314
    :cond_0
    :goto_0
    iget-object v2, p0, Lcom/samsung/android/app/memo/print/AsyncPrintTask;->mSession:Lcom/samsung/android/app/memo/Session;

    invoke-virtual {v2}, Lcom/samsung/android/app/memo/Session;->getTitle()Ljava/lang/String;

    move-result-object v0

    .line 315
    return-object v0

    .line 309
    :catch_0
    move-exception v1

    .line 311
    .local v1, "e":Lcom/samsung/android/app/memo/Session$SessionException;
    sget-object v2, Lcom/samsung/android/app/memo/print/AsyncPrintTask;->TAG:Ljava/lang/String;

    const-string v3, "getHtmlContent"

    invoke-static {v2, v3, v1}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private startGooglePrint()V
    .locals 3

    .prologue
    .line 230
    new-instance v0, Lcom/samsung/android/app/memo/print/GooglePrintHtml;

    invoke-direct {v0}, Lcom/samsung/android/app/memo/print/GooglePrintHtml;-><init>()V

    .line 231
    .local v0, "googlePrintHtml":Lcom/samsung/android/app/memo/print/GooglePrintHtml;
    iget-object v1, p0, Lcom/samsung/android/app/memo/print/AsyncPrintTask;->mPrintContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/samsung/android/app/memo/print/AsyncPrintTask;->mHtmlContent:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/app/memo/print/GooglePrintHtml;->print(Landroid/content/Context;Ljava/lang/String;)V

    .line 232
    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Integer;)Ljava/lang/Integer;
    .locals 12
    .param p1, "ids"    # [Ljava/lang/Integer;

    .prologue
    const/4 v5, 0x0

    .line 144
    const/4 v0, 0x0

    .line 146
    .local v0, "count":I
    iget-boolean v4, p0, Lcom/samsung/android/app/memo/print/AsyncPrintTask;->isCreateFolderSuccess:Z

    if-nez v4, :cond_0

    .line 147
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    .line 167
    :goto_0
    return-object v4

    .line 150
    :cond_0
    array-length v6, p1

    move v4, v5

    :goto_1
    if-lt v4, v6, :cond_1

    .line 167
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    goto :goto_0

    .line 150
    :cond_1
    aget-object v7, p1, v4

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 152
    .local v2, "id":I
    :try_start_0
    iget-boolean v7, p0, Lcom/samsung/android/app/memo/print/AsyncPrintTask;->isGooglePrint:Z

    if-eqz v7, :cond_3

    .line 153
    invoke-direct {p0, v2}, Lcom/samsung/android/app/memo/print/AsyncPrintTask;->getHtmlContent(I)Ljava/lang/String;

    move-result-object v7

    if-eqz v7, :cond_2

    .line 154
    invoke-direct {p0, v2}, Lcom/samsung/android/app/memo/print/AsyncPrintTask;->getHtmlContent(I)Ljava/lang/String;

    move-result-object v7

    invoke-direct {p0, v7}, Lcom/samsung/android/app/memo/print/AsyncPrintTask;->customHtmlParser(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    iput-object v7, p0, Lcom/samsung/android/app/memo/print/AsyncPrintTask;->mHtmlContent:Ljava/lang/String;

    .line 155
    invoke-direct {p0, v2}, Lcom/samsung/android/app/memo/print/AsyncPrintTask;->getMemoTitle(I)Ljava/lang/String;

    move-result-object v3

    .line 156
    .local v3, "memoTitle":Ljava/lang/String;
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_2

    .line 157
    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "<b>"

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "</b><br><br>"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/samsung/android/app/memo/print/AsyncPrintTask;->mHtmlContent:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    iput-object v7, p0, Lcom/samsung/android/app/memo/print/AsyncPrintTask;->mHtmlContent:Ljava/lang/String;
    :try_end_0
    .catch Lcom/samsung/android/app/memo/Session$SessionException; {:try_start_0 .. :try_end_0} :catch_0

    .line 165
    .end local v3    # "memoTitle":Ljava/lang/String;
    :cond_2
    :goto_2
    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Integer;

    add-int/lit8 v0, v0, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v5

    invoke-virtual {p0, v7}, Lcom/samsung/android/app/memo/print/AsyncPrintTask;->publishProgress([Ljava/lang/Object;)V

    .line 150
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 160
    :cond_3
    :try_start_1
    iget-object v7, p0, Lcom/samsung/android/app/memo/print/AsyncPrintTask;->imageWriter:Lcom/samsung/android/app/memo/print/Memo2ImageWritter;

    int-to-long v8, v2

    const/4 v10, 0x0

    iget-object v11, p0, Lcom/samsung/android/app/memo/print/AsyncPrintTask;->mSession:Lcom/samsung/android/app/memo/Session;

    invoke-virtual {v7, v8, v9, v10, v11}, Lcom/samsung/android/app/memo/print/Memo2ImageWritter;->convert2image(JLjava/lang/String;Lcom/samsung/android/app/memo/Session;)V
    :try_end_1
    .catch Lcom/samsung/android/app/memo/Session$SessionException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_2

    .line 161
    :catch_0
    move-exception v1

    .line 162
    .local v1, "e":Lcom/samsung/android/app/memo/Session$SessionException;
    sget-object v7, Lcom/samsung/android/app/memo/print/AsyncPrintTask;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, " printing error for "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8, v1}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_2
.end method

.method protected bridge varargs synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1
    check-cast p1, [Ljava/lang/Integer;

    invoke-virtual {p0, p1}, Lcom/samsung/android/app/memo/print/AsyncPrintTask;->doInBackground([Ljava/lang/Integer;)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public isFromSConnectPrint()Z
    .locals 1

    .prologue
    .line 74
    iget-boolean v0, p0, Lcom/samsung/android/app/memo/print/AsyncPrintTask;->isFromSConnectPrint:Z

    return v0
.end method

.method protected onCancelled()V
    .locals 0

    .prologue
    .line 226
    invoke-super {p0}, Landroid/os/AsyncTask;->onCancelled()V

    .line 227
    return-void
.end method

.method protected onPostExecute(Ljava/lang/Integer;)V
    .locals 10
    .param p1, "result"    # Ljava/lang/Integer;

    .prologue
    const/4 v6, 0x0

    const/4 v9, 0x1

    .line 172
    iget-boolean v7, p0, Lcom/samsung/android/app/memo/print/AsyncPrintTask;->isCreateFolderSuccess:Z

    if-nez v7, :cond_1

    .line 213
    :cond_0
    :goto_0
    return-void

    .line 175
    :cond_1
    iget-boolean v7, p0, Lcom/samsung/android/app/memo/print/AsyncPrintTask;->isGooglePrint:Z

    if-eqz v7, :cond_3

    .line 176
    invoke-direct {p0}, Lcom/samsung/android/app/memo/print/AsyncPrintTask;->startGooglePrint()V

    .line 203
    :cond_2
    :goto_1
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/print/AsyncPrintTask;->isFromSConnectPrint()Z

    move-result v6

    if-nez v6, :cond_0

    iget-object v6, p0, Lcom/samsung/android/app/memo/print/AsyncPrintTask;->mProgressDialog:Landroid/app/ProgressDialog;

    if-eqz v6, :cond_0

    .line 205
    :try_start_0
    iget-object v6, p0, Lcom/samsung/android/app/memo/print/AsyncPrintTask;->mProgressDialog:Landroid/app/ProgressDialog;

    const/4 v7, 0x1

    invoke-virtual {v6, v7}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 206
    iget-object v6, p0, Lcom/samsung/android/app/memo/print/AsyncPrintTask;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v6}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 207
    iget-object v6, p0, Lcom/samsung/android/app/memo/print/AsyncPrintTask;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v6}, Landroid/app/ProgressDialog;->dismiss()V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 209
    :catch_0
    move-exception v0

    .line 210
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    sget-object v6, Lcom/samsung/android/app/memo/print/AsyncPrintTask;->TAG:Ljava/lang/String;

    const-string v7, "onPostExecute"

    invoke-static {v6, v7, v0}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 178
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :cond_3
    iget-object v7, p0, Lcom/samsung/android/app/memo/print/AsyncPrintTask;->imageWriter:Lcom/samsung/android/app/memo/print/Memo2ImageWritter;

    invoke-virtual {v7}, Lcom/samsung/android/app/memo/print/Memo2ImageWritter;->releaseAll()V

    .line 179
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/print/AsyncPrintTask;->isFromSConnectPrint()Z

    move-result v7

    if-eqz v7, :cond_5

    .line 180
    invoke-virtual {p0, v6}, Lcom/samsung/android/app/memo/print/AsyncPrintTask;->setFromSConnectPrint(Z)V

    .line 181
    iget-object v7, p0, Lcom/samsung/android/app/memo/print/AsyncPrintTask;->mDirFile:Ljava/io/File;

    invoke-virtual {v7}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v2

    .line 182
    .local v2, "fileList":[Ljava/io/File;
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 183
    .local v3, "fileListUri":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    array-length v7, v2

    :goto_2
    if-lt v6, v7, :cond_4

    .line 186
    new-instance v4, Landroid/content/Intent;

    const-string v6, "com.sec.android.app.mobileprint.SCONNECT_PRINT"

    invoke-direct {v4, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 187
    .local v4, "intent":Landroid/content/Intent;
    const/high16 v6, 0x10000000

    invoke-virtual {v4, v6}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 188
    const-string v6, "PRINTER"

    iget-object v7, p0, Lcom/samsung/android/app/memo/print/AsyncPrintTask;->printerInfo:Landroid/os/Bundle;

    invoke-virtual {v4, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    .line 189
    const-string v6, "android.intent.extra.TITLE"

    iget-object v7, p0, Lcom/samsung/android/app/memo/print/AsyncPrintTask;->mTitle:Ljava/lang/String;

    invoke-virtual {v4, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 190
    const-string v6, "android.intent.extra.SUBJECT"

    iget-object v7, p0, Lcom/samsung/android/app/memo/print/AsyncPrintTask;->mTitle:Ljava/lang/String;

    invoke-virtual {v4, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 191
    const-string v6, "android.intent.extra.DATA_REMOVED"

    invoke-virtual {v4, v6, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 192
    const-string v6, "android.intent.extra.STREAM"

    invoke-virtual {v4, v6, v3}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 193
    iget-object v6, p0, Lcom/samsung/android/app/memo/print/AsyncPrintTask;->mPrintContext:Landroid/content/Context;

    invoke-virtual {v6, v4}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_1

    .line 183
    .end local v4    # "intent":Landroid/content/Intent;
    :cond_4
    aget-object v1, v2, v6

    .line 184
    .local v1, "file":Ljava/io/File;
    invoke-static {v1}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v8

    invoke-virtual {v3, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 183
    add-int/lit8 v6, v6, 0x1

    goto :goto_2

    .line 194
    .end local v1    # "file":Ljava/io/File;
    .end local v2    # "fileList":[Ljava/io/File;
    .end local v3    # "fileListUri":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    :cond_5
    iget-object v6, p0, Lcom/samsung/android/app/memo/print/AsyncPrintTask;->mPrintContext:Landroid/content/Context;

    invoke-static {v6}, Lcom/samsung/android/app/memo/util/Utils;->isMobilePrintAppAvailable(Landroid/content/Context;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 195
    new-instance v5, Landroid/content/Intent;

    const-string v6, "com.sec.android.app.mobileprint.PRINT"

    invoke-direct {v5, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 196
    .local v5, "intentPrintEvent":Landroid/content/Intent;
    const-string v6, "android.intent.extra.SUBJECT"

    iget-object v7, p0, Lcom/samsung/android/app/memo/print/AsyncPrintTask;->mTitle:Ljava/lang/String;

    invoke-virtual {v5, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 197
    const-string v6, "android.intent.extra.STREAM"

    iget-object v7, p0, Lcom/samsung/android/app/memo/print/AsyncPrintTask;->mDirFile:Ljava/io/File;

    invoke-static {v7}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 198
    const-string v6, "android.intent.extra.DATA_REMOVED"

    invoke-virtual {v5, v6, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 199
    iget-object v6, p0, Lcom/samsung/android/app/memo/print/AsyncPrintTask;->mPrintContext:Landroid/content/Context;

    invoke-virtual {v6, v5}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_1
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1
    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p0, p1}, Lcom/samsung/android/app/memo/print/AsyncPrintTask;->onPostExecute(Ljava/lang/Integer;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 9

    .prologue
    const/4 v8, 0x1

    .line 90
    iget-object v5, p0, Lcom/samsung/android/app/memo/print/AsyncPrintTask;->mPrintContext:Landroid/content/Context;

    if-eqz v5, :cond_0

    .line 91
    iget-object v5, p0, Lcom/samsung/android/app/memo/print/AsyncPrintTask;->mPrintContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const/high16 v6, 0x7f0b0000

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/samsung/android/app/memo/print/AsyncPrintTask;->mTitle:Ljava/lang/String;

    .line 94
    :cond_0
    iget-boolean v5, p0, Lcom/samsung/android/app/memo/print/AsyncPrintTask;->isGooglePrint:Z

    if-eqz v5, :cond_6

    .line 95
    new-instance v5, Ljava/io/File;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v7

    invoke-virtual {v7}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v7, "/.tempMemoPrint"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    iput-object v5, p0, Lcom/samsung/android/app/memo/print/AsyncPrintTask;->mDirFile:Ljava/io/File;

    .line 96
    iget-object v5, p0, Lcom/samsung/android/app/memo/print/AsyncPrintTask;->mDirFile:Ljava/io/File;

    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v5

    if-eqz v5, :cond_5

    iget-object v5, p0, Lcom/samsung/android/app/memo/print/AsyncPrintTask;->mDirFile:Ljava/io/File;

    invoke-virtual {v5}, Ljava/io/File;->isDirectory()Z

    move-result v5

    if-eqz v5, :cond_5

    .line 97
    iput-boolean v8, p0, Lcom/samsung/android/app/memo/print/AsyncPrintTask;->isCreateFolderSuccess:Z

    .line 98
    iget-object v5, p0, Lcom/samsung/android/app/memo/print/AsyncPrintTask;->mDirFile:Ljava/io/File;

    invoke-virtual {v5}, Ljava/io/File;->list()[Ljava/lang/String;

    move-result-object v1

    .line 99
    .local v1, "children":[Ljava/lang/String;
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    array-length v5, v1

    if-lt v3, v5, :cond_4

    .line 105
    .end local v1    # "children":[Ljava/lang/String;
    .end local v3    # "i":I
    :goto_1
    iget-boolean v5, p0, Lcom/samsung/android/app/memo/print/AsyncPrintTask;->isCreateFolderSuccess:Z

    if-eqz v5, :cond_1

    .line 107
    :try_start_0
    new-instance v4, Ljava/io/File;

    iget-object v5, p0, Lcom/samsung/android/app/memo/print/AsyncPrintTask;->mDirFile:Ljava/io/File;

    const-string v6, ".nomedia"

    invoke-direct {v4, v5, v6}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 108
    .local v4, "nomediaFile":Ljava/io/File;
    invoke-virtual {v4}, Ljava/io/File;->createNewFile()Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 130
    .end local v4    # "nomediaFile":Ljava/io/File;
    :cond_1
    :goto_2
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/print/AsyncPrintTask;->isFromSConnectPrint()Z

    move-result v5

    if-nez v5, :cond_3

    iget-object v5, p0, Lcom/samsung/android/app/memo/print/AsyncPrintTask;->mPrintContext:Landroid/content/Context;

    if-eqz v5, :cond_3

    .line 131
    new-instance v5, Landroid/app/ProgressDialog;

    iget-object v6, p0, Lcom/samsung/android/app/memo/print/AsyncPrintTask;->mPrintContext:Landroid/content/Context;

    invoke-direct {v5, v6}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v5, p0, Lcom/samsung/android/app/memo/print/AsyncPrintTask;->mProgressDialog:Landroid/app/ProgressDialog;

    .line 132
    iget-boolean v5, p0, Lcom/samsung/android/app/memo/print/AsyncPrintTask;->isGooglePrint:Z

    if-nez v5, :cond_2

    .line 133
    iget-object v5, p0, Lcom/samsung/android/app/memo/print/AsyncPrintTask;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v5, v8}, Landroid/app/ProgressDialog;->setProgressStyle(I)V

    .line 134
    :cond_2
    iget-object v5, p0, Lcom/samsung/android/app/memo/print/AsyncPrintTask;->mProgressDialog:Landroid/app/ProgressDialog;

    iget-object v6, p0, Lcom/samsung/android/app/memo/print/AsyncPrintTask;->mPrintContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0b000e

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 135
    iget-object v5, p0, Lcom/samsung/android/app/memo/print/AsyncPrintTask;->mProgressDialog:Landroid/app/ProgressDialog;

    iget v6, p0, Lcom/samsung/android/app/memo/print/AsyncPrintTask;->mMemoSize:I

    invoke-virtual {v5, v6}, Landroid/app/ProgressDialog;->setMax(I)V

    .line 136
    iget-object v5, p0, Lcom/samsung/android/app/memo/print/AsyncPrintTask;->mProgressDialog:Landroid/app/ProgressDialog;

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 137
    iget-object v5, p0, Lcom/samsung/android/app/memo/print/AsyncPrintTask;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v5}, Landroid/app/ProgressDialog;->show()V

    .line 140
    :cond_3
    :goto_3
    return-void

    .line 100
    .restart local v1    # "children":[Ljava/lang/String;
    .restart local v3    # "i":I
    :cond_4
    new-instance v5, Ljava/io/File;

    iget-object v6, p0, Lcom/samsung/android/app/memo/print/AsyncPrintTask;->mDirFile:Ljava/io/File;

    aget-object v7, v1, v3

    invoke-direct {v5, v6, v7}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v5}, Ljava/io/File;->delete()Z

    .line 99
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 103
    .end local v1    # "children":[Ljava/lang/String;
    .end local v3    # "i":I
    :cond_5
    iget-object v5, p0, Lcom/samsung/android/app/memo/print/AsyncPrintTask;->mDirFile:Ljava/io/File;

    invoke-virtual {v5}, Ljava/io/File;->mkdirs()Z

    move-result v5

    iput-boolean v5, p0, Lcom/samsung/android/app/memo/print/AsyncPrintTask;->isCreateFolderSuccess:Z

    goto :goto_1

    .line 109
    :catch_0
    move-exception v2

    .line 110
    .local v2, "e":Ljava/io/IOException;
    sget-object v5, Lcom/samsung/android/app/memo/print/AsyncPrintTask;->TAG:Ljava/lang/String;

    const-string v6, "onPreExecute"

    invoke-static {v5, v6, v2}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_2

    .line 114
    .end local v2    # "e":Ljava/io/IOException;
    :cond_6
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v6

    invoke-virtual {v6}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v6, "/temp"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 115
    const-string v6, "/"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 114
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 116
    .local v0, "basePath":Ljava/lang/String;
    const/4 v3, 0x0

    .line 117
    .restart local v3    # "i":I
    :goto_4
    new-instance v5, Ljava/io/File;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v7, p0, Lcom/samsung/android/app/memo/print/AsyncPrintTask;->mTitle:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const/16 v7, 0x5f

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    iput-object v5, p0, Lcom/samsung/android/app/memo/print/AsyncPrintTask;->mDirFile:Ljava/io/File;

    .line 118
    iget-object v5, p0, Lcom/samsung/android/app/memo/print/AsyncPrintTask;->mDirFile:Ljava/io/File;

    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v5

    if-nez v5, :cond_7

    .line 119
    iget-object v5, p0, Lcom/samsung/android/app/memo/print/AsyncPrintTask;->mDirFile:Ljava/io/File;

    invoke-virtual {v5}, Ljava/io/File;->mkdirs()Z

    move-result v5

    iput-boolean v5, p0, Lcom/samsung/android/app/memo/print/AsyncPrintTask;->isCreateFolderSuccess:Z

    .line 124
    iget-boolean v5, p0, Lcom/samsung/android/app/memo/print/AsyncPrintTask;->isCreateFolderSuccess:Z

    if-nez v5, :cond_8

    .line 125
    iget-object v5, p0, Lcom/samsung/android/app/memo/print/AsyncPrintTask;->mPrintContext:Landroid/content/Context;

    const v6, 0x7f0b000a

    invoke-static {v5, v6}, Lcom/samsung/android/app/memo/util/Utils;->showToast(Landroid/content/Context;I)V

    goto/16 :goto_3

    .line 116
    :cond_7
    add-int/lit8 v3, v3, 0x1

    goto :goto_4

    .line 128
    :cond_8
    new-instance v5, Lcom/samsung/android/app/memo/print/Memo2ImageWritter;

    iget-object v6, p0, Lcom/samsung/android/app/memo/print/AsyncPrintTask;->mPrintContext:Landroid/content/Context;

    iget-object v7, p0, Lcom/samsung/android/app/memo/print/AsyncPrintTask;->mDirFile:Ljava/io/File;

    invoke-direct {v5, v6, v7, v8}, Lcom/samsung/android/app/memo/print/Memo2ImageWritter;-><init>(Landroid/content/Context;Ljava/io/File;Z)V

    iput-object v5, p0, Lcom/samsung/android/app/memo/print/AsyncPrintTask;->imageWriter:Lcom/samsung/android/app/memo/print/Memo2ImageWritter;

    goto/16 :goto_2
.end method

.method protected varargs onProgressUpdate([Ljava/lang/Integer;)V
    .locals 2
    .param p1, "values"    # [Ljava/lang/Integer;

    .prologue
    .line 217
    iget-boolean v0, p0, Lcom/samsung/android/app/memo/print/AsyncPrintTask;->isCreateFolderSuccess:Z

    if-eqz v0, :cond_0

    .line 218
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/print/AsyncPrintTask;->isFromSConnectPrint()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/app/memo/print/AsyncPrintTask;->mProgressDialog:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    .line 219
    iget-object v0, p0, Lcom/samsung/android/app/memo/print/AsyncPrintTask;->mProgressDialog:Landroid/app/ProgressDialog;

    const/4 v1, 0x0

    aget-object v1, p1, v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setProgress(I)V

    .line 222
    :cond_0
    return-void
.end method

.method protected bridge varargs synthetic onProgressUpdate([Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1
    check-cast p1, [Ljava/lang/Integer;

    invoke-virtual {p0, p1}, Lcom/samsung/android/app/memo/print/AsyncPrintTask;->onProgressUpdate([Ljava/lang/Integer;)V

    return-void
.end method

.method public setFromSConnectPrint(Z)V
    .locals 0
    .param p1, "isFromSConnectPrint"    # Z

    .prologue
    .line 78
    iput-boolean p1, p0, Lcom/samsung/android/app/memo/print/AsyncPrintTask;->isFromSConnectPrint:Z

    .line 79
    return-void
.end method

.method public setPrinterInfo(Landroid/os/Bundle;)V
    .locals 0
    .param p1, "printerInfo"    # Landroid/os/Bundle;

    .prologue
    .line 70
    iput-object p1, p0, Lcom/samsung/android/app/memo/print/AsyncPrintTask;->printerInfo:Landroid/os/Bundle;

    .line 71
    return-void
.end method
