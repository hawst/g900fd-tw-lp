.class public Lcom/samsung/android/app/migration/snbdownload/network/downsync/util/SharedInfo;
.super Ljava/lang/Object;
.source "SharedInfo.java"


# instance fields
.field public accessToken:Ljava/lang/String;

.field public baseUrl:Ljava/lang/String;

.field public baseUrl2:Ljava/lang/String;

.field public did:Ljava/lang/String;

.field public mKey:Ljava/lang/String;

.field public regId:Ljava/lang/String;

.field public sKey:I

.field public status:I

.field public userId:Ljava/lang/String;

.field public userInfoFailed:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, -0x1

    const/4 v0, 0x0

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput v1, p0, Lcom/samsung/android/app/migration/snbdownload/network/downsync/util/SharedInfo;->status:I

    .line 27
    iput v1, p0, Lcom/samsung/android/app/migration/snbdownload/network/downsync/util/SharedInfo;->sKey:I

    .line 28
    iput-object v0, p0, Lcom/samsung/android/app/migration/snbdownload/network/downsync/util/SharedInfo;->mKey:Ljava/lang/String;

    .line 29
    iput-object v0, p0, Lcom/samsung/android/app/migration/snbdownload/network/downsync/util/SharedInfo;->userId:Ljava/lang/String;

    .line 30
    iput-object v0, p0, Lcom/samsung/android/app/migration/snbdownload/network/downsync/util/SharedInfo;->did:Ljava/lang/String;

    .line 31
    iput-object v0, p0, Lcom/samsung/android/app/migration/snbdownload/network/downsync/util/SharedInfo;->regId:Ljava/lang/String;

    .line 32
    iput-object v0, p0, Lcom/samsung/android/app/migration/snbdownload/network/downsync/util/SharedInfo;->accessToken:Ljava/lang/String;

    .line 33
    iput-object v0, p0, Lcom/samsung/android/app/migration/snbdownload/network/downsync/util/SharedInfo;->baseUrl:Ljava/lang/String;

    .line 34
    iput-object v0, p0, Lcom/samsung/android/app/migration/snbdownload/network/downsync/util/SharedInfo;->baseUrl2:Ljava/lang/String;

    .line 35
    iput v1, p0, Lcom/samsung/android/app/migration/snbdownload/network/downsync/util/SharedInfo;->userInfoFailed:I

    .line 24
    return-void
.end method


# virtual methods
.method public getPushInfo(Landroid/content/Context;)V
    .locals 5
    .param p1, "mContext"    # Landroid/content/Context;

    .prologue
    const/4 v4, -0x1

    const/4 v3, 0x0

    .line 75
    const-string v1, "PushInformation"

    const/4 v2, 0x0

    invoke-virtual {p1, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 76
    .local v0, "sPushInfo":Landroid/content/SharedPreferences;
    const-string v1, "status"

    invoke-interface {v0, v1, v4}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/samsung/android/app/migration/snbdownload/network/downsync/util/SharedInfo;->status:I

    .line 77
    const-string v1, "sKey"

    invoke-interface {v0, v1, v4}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/samsung/android/app/migration/snbdownload/network/downsync/util/SharedInfo;->sKey:I

    .line 78
    const-string v1, "mKey"

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/app/migration/snbdownload/network/downsync/util/SharedInfo;->mKey:Ljava/lang/String;

    .line 79
    const-string v1, "UserID"

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/app/migration/snbdownload/network/downsync/util/SharedInfo;->userId:Ljava/lang/String;

    .line 80
    const-string v1, "RegistrationID"

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/app/migration/snbdownload/network/downsync/util/SharedInfo;->regId:Ljava/lang/String;

    .line 81
    const-string v1, "Access_Token"

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/app/migration/snbdownload/network/downsync/util/SharedInfo;->accessToken:Ljava/lang/String;

    .line 82
    const-string v1, "BASE_URL_1"

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/app/migration/snbdownload/network/downsync/util/SharedInfo;->baseUrl:Ljava/lang/String;

    .line 83
    const-string v1, "BASE_URL_2"

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/app/migration/snbdownload/network/downsync/util/SharedInfo;->baseUrl2:Ljava/lang/String;

    .line 84
    const-string v1, "userInfoFailed"

    invoke-interface {v0, v1, v4}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/samsung/android/app/migration/snbdownload/network/downsync/util/SharedInfo;->userInfoFailed:I

    .line 85
    return-void
.end method

.method public savePushInfo(Landroid/content/Context;)V
    .locals 5
    .param p1, "mContext"    # Landroid/content/Context;

    .prologue
    const/4 v4, -0x1

    .line 38
    const-string v2, "PushInformation"

    const/4 v3, 0x0

    invoke-virtual {p1, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 39
    .local v1, "sPushInfo":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 40
    .local v0, "ePushInfo":Landroid/content/SharedPreferences$Editor;
    iget v2, p0, Lcom/samsung/android/app/migration/snbdownload/network/downsync/util/SharedInfo;->status:I

    if-eq v2, v4, :cond_0

    .line 41
    const-string v2, "status"

    iget v3, p0, Lcom/samsung/android/app/migration/snbdownload/network/downsync/util/SharedInfo;->status:I

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 43
    :cond_0
    iget v2, p0, Lcom/samsung/android/app/migration/snbdownload/network/downsync/util/SharedInfo;->sKey:I

    if-eq v2, v4, :cond_1

    .line 44
    const-string v2, "sKey"

    iget v3, p0, Lcom/samsung/android/app/migration/snbdownload/network/downsync/util/SharedInfo;->sKey:I

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 46
    :cond_1
    iget-object v2, p0, Lcom/samsung/android/app/migration/snbdownload/network/downsync/util/SharedInfo;->mKey:Ljava/lang/String;

    if-eqz v2, :cond_2

    .line 47
    const-string v2, "mKey"

    iget-object v3, p0, Lcom/samsung/android/app/migration/snbdownload/network/downsync/util/SharedInfo;->mKey:Ljava/lang/String;

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 49
    :cond_2
    iget-object v2, p0, Lcom/samsung/android/app/migration/snbdownload/network/downsync/util/SharedInfo;->userId:Ljava/lang/String;

    if-eqz v2, :cond_3

    .line 50
    const-string v2, "UserID"

    iget-object v3, p0, Lcom/samsung/android/app/migration/snbdownload/network/downsync/util/SharedInfo;->userId:Ljava/lang/String;

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 52
    :cond_3
    iget-object v2, p0, Lcom/samsung/android/app/migration/snbdownload/network/downsync/util/SharedInfo;->regId:Ljava/lang/String;

    if-eqz v2, :cond_4

    .line 53
    const-string v2, "RegistrationID"

    iget-object v3, p0, Lcom/samsung/android/app/migration/snbdownload/network/downsync/util/SharedInfo;->regId:Ljava/lang/String;

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 55
    :cond_4
    iget-object v2, p0, Lcom/samsung/android/app/migration/snbdownload/network/downsync/util/SharedInfo;->accessToken:Ljava/lang/String;

    if-eqz v2, :cond_5

    .line 56
    const-string v2, "Access_Token"

    iget-object v3, p0, Lcom/samsung/android/app/migration/snbdownload/network/downsync/util/SharedInfo;->accessToken:Ljava/lang/String;

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 58
    :cond_5
    iget-object v2, p0, Lcom/samsung/android/app/migration/snbdownload/network/downsync/util/SharedInfo;->baseUrl:Ljava/lang/String;

    if-eqz v2, :cond_6

    .line 59
    const-string v2, "BASE_URL_1"

    iget-object v3, p0, Lcom/samsung/android/app/migration/snbdownload/network/downsync/util/SharedInfo;->baseUrl:Ljava/lang/String;

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 61
    :cond_6
    iget-object v2, p0, Lcom/samsung/android/app/migration/snbdownload/network/downsync/util/SharedInfo;->baseUrl2:Ljava/lang/String;

    if-eqz v2, :cond_7

    .line 62
    const-string v2, "BASE_URL_2"

    iget-object v3, p0, Lcom/samsung/android/app/migration/snbdownload/network/downsync/util/SharedInfo;->baseUrl2:Ljava/lang/String;

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 64
    :cond_7
    iget-object v2, p0, Lcom/samsung/android/app/migration/snbdownload/network/downsync/util/SharedInfo;->did:Ljava/lang/String;

    if-eqz v2, :cond_8

    .line 65
    const-string v2, "did"

    iget-object v3, p0, Lcom/samsung/android/app/migration/snbdownload/network/downsync/util/SharedInfo;->did:Ljava/lang/String;

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 67
    :cond_8
    iget v2, p0, Lcom/samsung/android/app/migration/snbdownload/network/downsync/util/SharedInfo;->userInfoFailed:I

    if-eq v2, v4, :cond_9

    .line 68
    const-string v2, "userInfoFailed"

    iget v3, p0, Lcom/samsung/android/app/migration/snbdownload/network/downsync/util/SharedInfo;->userInfoFailed:I

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 70
    :cond_9
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 72
    return-void
.end method
