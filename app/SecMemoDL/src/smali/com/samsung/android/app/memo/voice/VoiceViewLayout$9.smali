.class Lcom/samsung/android/app/memo/voice/VoiceViewLayout$9;
.super Landroid/os/Handler;
.source "VoiceViewLayout.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/memo/voice/VoiceViewLayout;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$9;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    .line 1651
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v2, 0x1

    .line 1654
    iget v0, p1, Landroid/os/Message;->what:I

    .line 1656
    .local v0, "what":I
    if-nez v0, :cond_1

    .line 1657
    iget-object v1, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$9;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    # getter for: Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mAudioManager:Landroid/media/AudioManager;
    invoke-static {v1}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->access$33(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)Landroid/media/AudioManager;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/media/AudioManager;->getStreamVolume(I)I

    move-result v1

    if-nez v1, :cond_0

    .line 1658
    iget-object v1, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$9;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    const/4 v2, 0x0

    # invokes: Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->setAudioManagerStatus(Z)V
    invoke-static {v1, v2}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->access$34(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;Z)V

    .line 1665
    :cond_0
    :goto_0
    return-void

    .line 1661
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$9;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    # getter for: Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mAudioManager:Landroid/media/AudioManager;
    invoke-static {v1}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->access$33(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)Landroid/media/AudioManager;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/media/AudioManager;->getStreamVolume(I)I

    move-result v1

    if-lez v1, :cond_0

    .line 1662
    iget-object v1, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$9;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    # invokes: Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->setAudioManagerStatus(Z)V
    invoke-static {v1, v2}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->access$34(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;Z)V

    goto :goto_0
.end method
