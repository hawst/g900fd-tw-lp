.class public Lcom/samsung/android/app/memo/share/MemoShare;
.super Ljava/lang/Object;
.source "MemoShare.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/app/memo/share/MemoShare$MemoShareTask;,
        Lcom/samsung/android/app/memo/share/MemoShare$SessionUpdateInterfaceForMemoShare;
    }
.end annotation


# static fields
.field public static final ANDROID_BEAM_PKG_NAME:Ljava/lang/String; = "com.android.nfc"

.field private static final BAIDUCLOUD_PKG_NAME:Ljava/lang/String; = "com.baidu.netdisk_ss"

.field public static final BLUETHOOTH_APP_NAME:Ljava/lang/String; = "com.android.bluetooth.opp.BluetoothOppLauncherActivity"

.field public static final BLUETHOOTH_PKG_NAME:Ljava/lang/String; = "com.android.bluetooth"

.field public static final CLIP_DATA_LABEL:Ljava/lang/String; = "Clipdata from Memo"

.field private static final CLOUD189_PKG_NAME:Ljava/lang/String; = "com.cn21.ecloud"

.field private static final DOCS_APP_NAME:Ljava/lang/String; = "com.google.android.apps.docs.app.SendTextToClipboardActivity"

.field public static final FACEBOOK_PKG_NAME:Ljava/lang/String; = "com.facebook.katana"

.field private static final FLIPBRD_CHN_PKG_NAME:Ljava/lang/String; = "flipboard.cn"

.field public static final GOOGLE_DRIVE_PKG_NAME:Ljava/lang/String; = "com.google.android.apps.docs"

.field private static final MAIL139_PKG_NAME:Ljava/lang/String; = "cn.cj.pe"

.field private static final MEMO_PKG_NAME:Ljava/lang/String; = "com.samsung.android.app.memo"

.field private static final MEMO_PKG_NAME_IN_KNOX:Ljava/lang/String; = "sec_container_1.com.samsung.android.app.memo"

.field public static final MIME_TYPE_ALL:Ljava/lang/String; = "*/*"

.field public static final MIME_TYPE_AUDIO_ALL:Ljava/lang/String; = "audio/*"

.field public static final MIME_TYPE_AUDIO_MP4:Ljava/lang/String; = "audio/mp4"

.field public static final MIME_TYPE_IMAGE_ALL:Ljava/lang/String; = "image/*"

.field public static final MIME_TYPE_MEMO_FILE:Ljava/lang/String; = "application/vnd.samsung.android.memo"

.field public static final MIME_TYPE_TEXT_HTML:Ljava/lang/String; = "text/html"

.field public static final MIME_TYPE_TEXT_PLAIN:Ljava/lang/String; = "text/plain"

.field private static final MOMENTS_PKG_NAME:Ljava/lang/String; = "im.yixin"

.field private static final QQ_PKG_NAME:Ljava/lang/String; = "com.tencent.mtt"

.field public static final SCONNECT_PKG_NAME:Ljava/lang/String; = "com.samsung.android.sconnect"

.field private static final TAG:Ljava/lang/String;

.field private static final WECHAT_PKG_NAME:Ljava/lang/String; = "com.tencent.mm"

.field private static final WEIBO_PKG_NAME:Ljava/lang/String; = "com.sina.weibo"

.field public static final WIFI_DIRECT:Ljava/lang/String; = "com.samsung.android.app.FileShareClient"

.field public static final YOUTUBE_PKG_NAME:Ljava/lang/String; = "com.google.android.youtube"


# instance fields
.field private final IMAGE_PICKER_THEME:Ljava/lang/String;

.field private final LIGHT_THEME:I

.field private attachmentList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation
.end field

.field private context:Landroid/content/Context;

.field private htmlText:Ljava/lang/String;

.field private imageAttachmentList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation
.end field

.field private intentMetaInfo:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private mPm:Landroid/content/pm/PackageManager;

.field private memoFileUri:Landroid/net/Uri;

.field private rList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/content/pm/ResolveInfo;",
            ">;"
        }
    .end annotation
.end field

.field private subject:Ljava/lang/String;

.field private target:Landroid/content/Intent;

.field private targetedShareIntents:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/content/Intent;",
            ">;"
        }
    .end annotation
.end field

.field private text:Ljava/lang/String;

.field private title:Ljava/lang/String;

.field private voiceAttachment:Landroid/net/Uri;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 52
    const-class v0, Lcom/samsung/android/app/memo/share/MemoShare;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/app/memo/share/MemoShare;->TAG:Ljava/lang/String;

    .line 106
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    .line 153
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 108
    iput-object v1, p0, Lcom/samsung/android/app/memo/share/MemoShare;->mPm:Landroid/content/pm/PackageManager;

    .line 110
    iput-object v1, p0, Lcom/samsung/android/app/memo/share/MemoShare;->rList:Ljava/util/List;

    .line 112
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/app/memo/share/MemoShare;->targetedShareIntents:Ljava/util/List;

    .line 114
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/app/memo/share/MemoShare;->intentMetaInfo:Ljava/util/List;

    .line 118
    const-string v0, "theme"

    iput-object v0, p0, Lcom/samsung/android/app/memo/share/MemoShare;->IMAGE_PICKER_THEME:Ljava/lang/String;

    .line 120
    const/4 v0, 0x2

    iput v0, p0, Lcom/samsung/android/app/memo/share/MemoShare;->LIGHT_THEME:I

    .line 151
    iput-object v1, p0, Lcom/samsung/android/app/memo/share/MemoShare;->memoFileUri:Landroid/net/Uri;

    .line 154
    iput-object p1, p0, Lcom/samsung/android/app/memo/share/MemoShare;->context:Landroid/content/Context;

    .line 155
    return-void
.end method

.method static synthetic access$0(Lcom/samsung/android/app/memo/share/MemoShare;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 122
    iget-object v0, p0, Lcom/samsung/android/app/memo/share/MemoShare;->context:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$1(Lcom/samsung/android/app/memo/share/MemoShare;Ljava/util/ArrayList;)V
    .locals 0

    .prologue
    .line 149
    iput-object p1, p0, Lcom/samsung/android/app/memo/share/MemoShare;->attachmentList:Ljava/util/ArrayList;

    return-void
.end method

.method static synthetic access$2(Lcom/samsung/android/app/memo/share/MemoShare;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 132
    iget-object v0, p0, Lcom/samsung/android/app/memo/share/MemoShare;->imageAttachmentList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$3(Lcom/samsung/android/app/memo/share/MemoShare;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 149
    iget-object v0, p0, Lcom/samsung/android/app/memo/share/MemoShare;->attachmentList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$4(Lcom/samsung/android/app/memo/share/MemoShare;)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 134
    iget-object v0, p0, Lcom/samsung/android/app/memo/share/MemoShare;->voiceAttachment:Landroid/net/Uri;

    return-object v0
.end method

.method static synthetic access$5(Lcom/samsung/android/app/memo/share/MemoShare;)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 151
    iget-object v0, p0, Lcom/samsung/android/app/memo/share/MemoShare;->memoFileUri:Landroid/net/Uri;

    return-object v0
.end method

.method static synthetic access$6(Lcom/samsung/android/app/memo/share/MemoShare;Landroid/net/Uri;)V
    .locals 0

    .prologue
    .line 151
    iput-object p1, p0, Lcom/samsung/android/app/memo/share/MemoShare;->memoFileUri:Landroid/net/Uri;

    return-void
.end method

.method static synthetic access$7()Ljava/lang/String;
    .locals 1

    .prologue
    .line 52
    sget-object v0, Lcom/samsung/android/app/memo/share/MemoShare;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method private isAudioInAttachment()Z
    .locals 1

    .prologue
    .line 415
    iget-object v0, p0, Lcom/samsung/android/app/memo/share/MemoShare;->voiceAttachment:Landroid/net/Uri;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method createTargetShareIntents(Ljava/util/List;Landroid/content/Intent;[Landroid/content/Intent;)V
    .locals 11
    .param p2, "prototype"    # Landroid/content/Intent;
    .param p3, "extraInitial"    # [Landroid/content/Intent;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/content/pm/ResolveInfo;",
            ">;",
            "Landroid/content/Intent;",
            "[",
            "Landroid/content/Intent;",
            ")V"
        }
    .end annotation

    .prologue
    .line 514
    .local p1, "resInfo":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    sget-object v6, Lcom/samsung/android/app/memo/share/MemoShare;->TAG:Ljava/lang/String;

    const-string v7, "createTargetShareIntents"

    invoke-static {v6, v7}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 515
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_2

    .line 516
    iget-object v6, p0, Lcom/samsung/android/app/memo/share/MemoShare;->targetedShareIntents:Ljava/util/List;

    if-eqz v6, :cond_0

    .line 517
    iget-object v6, p0, Lcom/samsung/android/app/memo/share/MemoShare;->targetedShareIntents:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->clear()V

    .line 519
    :cond_0
    iget-object v6, p0, Lcom/samsung/android/app/memo/share/MemoShare;->intentMetaInfo:Ljava/util/List;

    if-eqz v6, :cond_1

    .line 520
    iget-object v6, p0, Lcom/samsung/android/app/memo/share/MemoShare;->intentMetaInfo:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->clear()V

    .line 523
    :cond_1
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-nez v7, :cond_3

    .line 532
    iget-object v6, p0, Lcom/samsung/android/app/memo/share/MemoShare;->intentMetaInfo:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_2

    .line 534
    iget-object v6, p0, Lcom/samsung/android/app/memo/share/MemoShare;->intentMetaInfo:Ljava/util/List;

    new-instance v7, Lcom/samsung/android/app/memo/share/MemoShare$1;

    invoke-direct {v7, p0}, Lcom/samsung/android/app/memo/share/MemoShare$1;-><init>(Lcom/samsung/android/app/memo/share/MemoShare;)V

    invoke-static {v6, v7}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 542
    iget-object v6, p0, Lcom/samsung/android/app/memo/share/MemoShare;->targetedShareIntents:Ljava/util/List;

    invoke-static {v6, p3}, Ljava/util/Collections;->addAll(Ljava/util/Collection;[Ljava/lang/Object;)Z

    .line 547
    iget-object v6, p0, Lcom/samsung/android/app/memo/share/MemoShare;->intentMetaInfo:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-nez v6, :cond_4

    .line 557
    :cond_2
    return-void

    .line 523
    :cond_3
    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/pm/ResolveInfo;

    .line 524
    .local v4, "resolveInfo":Landroid/content/pm/ResolveInfo;
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 525
    .local v1, "info":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v7, "packageName"

    iget-object v8, v4, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v8, v8, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v1, v7, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 526
    const-string v7, "className"

    iget-object v8, v4, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v8, v8, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    invoke-virtual {v1, v7, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 527
    iget-object v7, p0, Lcom/samsung/android/app/memo/share/MemoShare;->context:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v7

    invoke-virtual {v4, v7}, Landroid/content/pm/ResolveInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 528
    .local v0, "appName":Ljava/lang/String;
    const-string v7, "simpleName"

    invoke-virtual {v1, v7, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 529
    iget-object v7, p0, Lcom/samsung/android/app/memo/share/MemoShare;->intentMetaInfo:Ljava/util/List;

    invoke-interface {v7, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 547
    .end local v0    # "appName":Ljava/lang/String;
    .end local v1    # "info":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v4    # "resolveInfo":Landroid/content/pm/ResolveInfo;
    :cond_4
    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/HashMap;

    .line 548
    .local v2, "metaInfo":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-virtual {p2}, Landroid/content/Intent;->clone()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/content/Intent;

    .line 549
    .local v5, "targetedShareIntent":Landroid/content/Intent;
    const-string v6, "packageName"

    invoke-virtual {v2, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 550
    .local v3, "packageName":Ljava/lang/String;
    new-instance v8, Landroid/content/ComponentName;

    .line 551
    const-string v6, "className"

    invoke-virtual {v2, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    invoke-direct {v8, v3, v6}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 550
    invoke-virtual {v5, v8}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 552
    iget-object v8, p0, Lcom/samsung/android/app/memo/share/MemoShare;->targetedShareIntents:Ljava/util/List;

    new-instance v9, Landroid/content/pm/LabeledIntent;

    .line 553
    const-string v6, "simpleName"

    invoke-virtual {v2, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/CharSequence;

    const/4 v10, 0x0

    invoke-direct {v9, v5, v3, v6, v10}, Landroid/content/pm/LabeledIntent;-><init>(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/CharSequence;I)V

    .line 552
    invoke-interface {v8, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method

.method public filterResolveList(Landroid/content/Intent;Ljava/util/List;Z)V
    .locals 4
    .param p1, "intent"    # Landroid/content/Intent;
    .param p3, "skipKnownPkg"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Intent;",
            "Ljava/util/List",
            "<",
            "Landroid/content/pm/ResolveInfo;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 478
    .local p2, "list":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    sget-object v1, Lcom/samsung/android/app/memo/share/MemoShare;->TAG:Ljava/lang/String;

    const-string v2, "filterResolveList"

    invoke-static {v1, v2}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 479
    iget-object v1, p0, Lcom/samsung/android/app/memo/share/MemoShare;->mPm:Landroid/content/pm/PackageManager;

    iget-object v2, p0, Lcom/samsung/android/app/memo/share/MemoShare;->target:Landroid/content/Intent;

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_1

    .line 511
    return-void

    .line 479
    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/ResolveInfo;

    .line 480
    .local v0, "ri":Landroid/content/pm/ResolveInfo;
    const-string v2, "com.samsung.android.app.memo"

    iget-object v3, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v3, v3, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 481
    const-string v2, "sec_container_1.com.samsung.android.app.memo"

    iget-object v3, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v3, v3, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 483
    if-eqz p3, :cond_2

    .line 484
    const-string v2, "com.google.android.apps.docs.app.SendTextToClipboardActivity"

    iget-object v3, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v3, v3, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 485
    const-string v2, "com.android.bluetooth"

    iget-object v3, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v3, v3, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 486
    const-string v2, "com.android.bluetooth.opp.BluetoothOppLauncherActivity"

    iget-object v3, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v3, v3, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 487
    const-string v2, "com.samsung.android.app.FileShareClient"

    iget-object v3, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v3, v3, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 488
    const-string v2, "com.android.nfc"

    iget-object v3, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v3, v3, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 489
    const-string v2, "com.google.android.youtube"

    iget-object v3, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v3, v3, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 492
    :cond_2
    iget-object v2, p0, Lcom/samsung/android/app/memo/share/MemoShare;->target:Landroid/content/Intent;

    invoke-virtual {v2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    const-string v3, "android.intent.action.SEND_MULTIPLE"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 493
    const-string v2, "com.tencent.mm"

    iget-object v3, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v3, v3, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 494
    const-string v2, "im.yixin"

    iget-object v3, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v3, v3, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 495
    const-string v2, "cn.cj.pe"

    iget-object v3, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v3, v3, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 496
    const-string v2, "com.cn21.ecloud"

    iget-object v3, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v3, v3, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 497
    const-string v2, "flipboard.cn"

    iget-object v3, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v3, v3, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 498
    const-string v2, "com.baidu.netdisk_ss"

    iget-object v3, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v3, v3, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 499
    const-string v2, "com.tencent.mtt"

    iget-object v3, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v3, v3, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 500
    const-string v2, "com.sina.weibo"

    iget-object v3, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v3, v3, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 501
    const-string v2, "com.facebook.katana"

    iget-object v3, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v3, v3, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "com.samsung.android.sconnect"

    .line 502
    iget-object v3, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v3, v3, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 504
    :cond_3
    iget-object v2, p0, Lcom/samsung/android/app/memo/share/MemoShare;->target:Landroid/content/Intent;

    invoke-virtual {v2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    const-string v3, "android.intent.action.SEND"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 505
    const-string v2, "com.google.android.apps.docs"

    iget-object v3, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v3, v3, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 508
    :cond_4
    invoke-interface {p2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0
.end method

.method public getContext()Landroid/content/Context;
    .locals 1

    .prologue
    .line 190
    iget-object v0, p0, Lcom/samsung/android/app/memo/share/MemoShare;->context:Landroid/content/Context;

    return-object v0
.end method

.method public getHtmlText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 166
    iget-object v0, p0, Lcom/samsung/android/app/memo/share/MemoShare;->htmlText:Ljava/lang/String;

    return-object v0
.end method

.method public getImageAttachmentList()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation

    .prologue
    .line 198
    iget-object v0, p0, Lcom/samsung/android/app/memo/share/MemoShare;->imageAttachmentList:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getSubject()Ljava/lang/String;
    .locals 1

    .prologue
    .line 182
    iget-object v0, p0, Lcom/samsung/android/app/memo/share/MemoShare;->subject:Ljava/lang/String;

    return-object v0
.end method

.method public getText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 158
    iget-object v0, p0, Lcom/samsung/android/app/memo/share/MemoShare;->text:Ljava/lang/String;

    return-object v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 174
    iget-object v0, p0, Lcom/samsung/android/app/memo/share/MemoShare;->title:Ljava/lang/String;

    return-object v0
.end method

.method public getVoiceAttachment()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 138
    iget-object v0, p0, Lcom/samsung/android/app/memo/share/MemoShare;->voiceAttachment:Landroid/net/Uri;

    return-object v0
.end method

.method public setContext(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 194
    iput-object p1, p0, Lcom/samsung/android/app/memo/share/MemoShare;->context:Landroid/content/Context;

    .line 195
    return-void
.end method

.method public setHtmlText(Ljava/lang/String;)V
    .locals 0
    .param p1, "htmlText"    # Ljava/lang/String;

    .prologue
    .line 170
    iput-object p1, p0, Lcom/samsung/android/app/memo/share/MemoShare;->htmlText:Ljava/lang/String;

    .line 171
    return-void
.end method

.method public setImageAttachmentList(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/Uri;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 202
    .local p1, "attachmentList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    iput-object p1, p0, Lcom/samsung/android/app/memo/share/MemoShare;->imageAttachmentList:Ljava/util/ArrayList;

    .line 203
    return-void
.end method

.method public setMemoFileUri(Landroid/net/Uri;)V
    .locals 0
    .param p1, "memoFileUri"    # Landroid/net/Uri;

    .prologue
    .line 146
    iput-object p1, p0, Lcom/samsung/android/app/memo/share/MemoShare;->memoFileUri:Landroid/net/Uri;

    .line 147
    return-void
.end method

.method public setSubject(Ljava/lang/String;)V
    .locals 0
    .param p1, "subject"    # Ljava/lang/String;

    .prologue
    .line 186
    iput-object p1, p0, Lcom/samsung/android/app/memo/share/MemoShare;->subject:Ljava/lang/String;

    .line 187
    return-void
.end method

.method public setText(Ljava/lang/String;)V
    .locals 0
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 162
    iput-object p1, p0, Lcom/samsung/android/app/memo/share/MemoShare;->text:Ljava/lang/String;

    .line 163
    return-void
.end method

.method public setTitle(Ljava/lang/String;)V
    .locals 0
    .param p1, "title"    # Ljava/lang/String;

    .prologue
    .line 178
    iput-object p1, p0, Lcom/samsung/android/app/memo/share/MemoShare;->title:Ljava/lang/String;

    .line 179
    return-void
.end method

.method public setVoiceAttachment(Landroid/net/Uri;)V
    .locals 0
    .param p1, "voiceAttachment"    # Landroid/net/Uri;

    .prologue
    .line 142
    iput-object p1, p0, Lcom/samsung/android/app/memo/share/MemoShare;->voiceAttachment:Landroid/net/Uri;

    .line 143
    return-void
.end method

.method public share()V
    .locals 28

    .prologue
    .line 206
    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3}, Landroid/content/Intent;-><init>()V

    .line 208
    .local v3, "chooserIntent":Landroid/content/Intent;
    const-string v26, "android.intent.extra.TITLE"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/share/MemoShare;->title:Ljava/lang/String;

    move-object/from16 v25, v0

    if-eqz v25, :cond_2

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/share/MemoShare;->title:Ljava/lang/String;

    move-object/from16 v25, v0

    :goto_0
    move-object/from16 v0, v26

    move-object/from16 v1, v25

    invoke-virtual {v3, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/CharSequence;)Landroid/content/Intent;

    .line 211
    const/4 v10, 0x0

    .line 213
    .local v10, "mExtraIntentList":[Landroid/content/Intent;
    new-instance v21, Landroid/content/Intent;

    const-string v25, "android.intent.action.SEND"

    move-object/from16 v0, v21

    move-object/from16 v1, v25

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 214
    .local v21, "textShareIntent":Landroid/content/Intent;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/share/MemoShare;->text:Ljava/lang/String;

    move-object/from16 v25, v0

    invoke-static/range {v25 .. v25}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v25

    if-eqz v25, :cond_0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/share/MemoShare;->subject:Ljava/lang/String;

    move-object/from16 v25, v0

    invoke-static/range {v25 .. v25}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v25

    if-nez v25, :cond_3

    .line 215
    :cond_0
    const-string v25, "text/plain"

    move-object/from16 v0, v21

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 219
    :goto_1
    const-string v25, "android.intent.extra.TEXT"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/share/MemoShare;->text:Ljava/lang/String;

    move-object/from16 v26, v0

    move-object/from16 v0, v21

    move-object/from16 v1, v25

    move-object/from16 v2, v26

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 220
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/share/MemoShare;->subject:Ljava/lang/String;

    move-object/from16 v25, v0

    if-eqz v25, :cond_1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/share/MemoShare;->subject:Ljava/lang/String;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Ljava/lang/String;->isEmpty()Z

    move-result v25

    if-nez v25, :cond_1

    .line 221
    const-string v25, "android.intent.extra.SUBJECT"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/share/MemoShare;->subject:Ljava/lang/String;

    move-object/from16 v26, v0

    move-object/from16 v0, v21

    move-object/from16 v1, v25

    move-object/from16 v2, v26

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 222
    :cond_1
    const-string v25, "theme"

    const/16 v26, 0x2

    move-object/from16 v0, v21

    move-object/from16 v1, v25

    move/from16 v2, v26

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 224
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/share/MemoShare;->attachmentList:Ljava/util/ArrayList;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v25

    if-eqz v25, :cond_a

    .line 225
    const-string v25, "android.intent.extra.INTENT"

    move-object/from16 v0, v25

    move-object/from16 v1, v21

    invoke-virtual {v3, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 226
    const-string v25, "theme"

    const/16 v26, 0x2

    move-object/from16 v0, v25

    move/from16 v1, v26

    invoke-virtual {v3, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 227
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/share/MemoShare;->context:Landroid/content/Context;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v17

    .line 228
    .local v17, "pm":Landroid/content/pm/PackageManager;
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 229
    .local v5, "extraIntentList":Ljava/util/List;, "Ljava/util/List<Landroid/content/Intent;>;"
    const/16 v25, 0x0

    move-object/from16 v0, v17

    move-object/from16 v1, v21

    move/from16 v2, v25

    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v22

    .line 230
    .local v22, "textShareResInfo":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    invoke-interface/range {v22 .. v22}, Ljava/util/List;->size()I

    move-result v23

    .line 231
    .local v23, "textShareResInfoSize":I
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_2
    move/from16 v0, v23

    if-lt v7, v0, :cond_4

    .line 255
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v25

    move/from16 v0, v25

    new-array v0, v0, [Landroid/content/Intent;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    invoke-interface {v5, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v6

    check-cast v6, [Landroid/content/Intent;

    .line 256
    .local v6, "extraIntents":[Landroid/content/Intent;
    const-string v25, "android.intent.extra.INITIAL_INTENTS"

    move-object/from16 v0, v25

    invoke-virtual {v3, v0, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 257
    move-object v10, v6

    .line 351
    .end local v22    # "textShareResInfo":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    .end local v23    # "textShareResInfoSize":I
    :goto_3
    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v10}, Lcom/samsung/android/app/memo/share/MemoShare;->startChooser(Landroid/content/Intent;[Landroid/content/Intent;)V

    .line 355
    return-void

    .line 208
    .end local v5    # "extraIntentList":Ljava/util/List;, "Ljava/util/List<Landroid/content/Intent;>;"
    .end local v6    # "extraIntents":[Landroid/content/Intent;
    .end local v7    # "i":I
    .end local v10    # "mExtraIntentList":[Landroid/content/Intent;
    .end local v17    # "pm":Landroid/content/pm/PackageManager;
    .end local v21    # "textShareIntent":Landroid/content/Intent;
    :cond_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/share/MemoShare;->context:Landroid/content/Context;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v25

    .line 209
    const v27, 0x7f0b0009

    move-object/from16 v0, v25

    move/from16 v1, v27

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v25

    goto/16 :goto_0

    .line 217
    .restart local v10    # "mExtraIntentList":[Landroid/content/Intent;
    .restart local v21    # "textShareIntent":Landroid/content/Intent;
    :cond_3
    const-string v25, "application/vnd.samsung.android.memo"

    move-object/from16 v0, v21

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    goto/16 :goto_1

    .line 232
    .restart local v5    # "extraIntentList":Ljava/util/List;, "Ljava/util/List<Landroid/content/Intent;>;"
    .restart local v7    # "i":I
    .restart local v17    # "pm":Landroid/content/pm/PackageManager;
    .restart local v22    # "textShareResInfo":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    .restart local v23    # "textShareResInfoSize":I
    :cond_4
    move-object/from16 v0, v22

    invoke-interface {v0, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Landroid/content/pm/ResolveInfo;

    .line 233
    .local v18, "ri":Landroid/content/pm/ResolveInfo;
    move-object/from16 v0, v18

    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget-object v0, v0, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    move-object/from16 v16, v0

    .line 234
    .local v16, "packageName":Ljava/lang/String;
    const-string v25, "com.android.bluetooth"

    move-object/from16 v0, v16

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-nez v25, :cond_5

    const-string v25, "com.samsung.android.app.FileShareClient"

    move-object/from16 v0, v16

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-nez v25, :cond_5

    .line 235
    const-string v25, "com.android.nfc"

    move-object/from16 v0, v16

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_7

    .line 236
    :cond_5
    new-instance v11, Landroid/content/Intent;

    const-string v25, "android.intent.action.SEND"

    move-object/from16 v0, v25

    invoke-direct {v11, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 237
    .local v11, "memoFileShareIntent":Landroid/content/Intent;
    const-string v25, "application/vnd.samsung.android.memo"

    move-object/from16 v0, v25

    invoke-virtual {v11, v0}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 238
    const-string v25, "android.intent.extra.STREAM"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/share/MemoShare;->memoFileUri:Landroid/net/Uri;

    move-object/from16 v26, v0

    move-object/from16 v0, v25

    move-object/from16 v1, v26

    invoke-virtual {v11, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 239
    const-string v25, "theme"

    const/16 v26, 0x2

    move-object/from16 v0, v25

    move/from16 v1, v26

    invoke-virtual {v11, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 240
    new-instance v25, Landroid/content/ComponentName;

    move-object/from16 v0, v18

    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    iget-object v0, v0, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    move-object/from16 v26, v0

    move-object/from16 v0, v25

    move-object/from16 v1, v16

    move-object/from16 v2, v26

    invoke-direct {v0, v1, v2}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, v25

    invoke-virtual {v11, v0}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 241
    invoke-interface {v5, v11}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 231
    .end local v11    # "memoFileShareIntent":Landroid/content/Intent;
    :cond_6
    :goto_4
    add-int/lit8 v7, v7, 0x1

    goto/16 :goto_2

    .line 242
    :cond_7
    const-string v25, "com.google.android.apps.docs"

    move-object/from16 v0, v16

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_6

    .line 243
    new-instance v20, Landroid/content/Intent;

    invoke-direct/range {v20 .. v21}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    .line 244
    .local v20, "textShareGDriveIntent":Landroid/content/Intent;
    const-string v25, "android.intent.extra.SUBJECT"

    move-object/from16 v0, v20

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v25

    if-eqz v25, :cond_8

    .line 245
    const-string v25, "android.intent.extra.SUBJECT"

    move-object/from16 v0, v20

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    .line 247
    :cond_8
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/share/MemoShare;->subject:Ljava/lang/String;

    move-object/from16 v25, v0

    if-eqz v25, :cond_9

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/share/MemoShare;->subject:Ljava/lang/String;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Ljava/lang/String;->isEmpty()Z

    move-result v25

    if-nez v25, :cond_9

    .line 248
    const-string v25, "android.intent.extra.SUBJECT"

    new-instance v26, Ljava/lang/StringBuilder;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/share/MemoShare;->subject:Ljava/lang/String;

    move-object/from16 v27, v0

    invoke-static/range {v27 .. v27}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v27

    invoke-direct/range {v26 .. v27}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v27, ".txt"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    move-object/from16 v0, v20

    move-object/from16 v1, v25

    move-object/from16 v2, v26

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 250
    :cond_9
    new-instance v25, Landroid/content/ComponentName;

    .line 251
    move-object/from16 v0, v18

    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    iget-object v0, v0, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    move-object/from16 v26, v0

    move-object/from16 v0, v25

    move-object/from16 v1, v16

    move-object/from16 v2, v26

    invoke-direct {v0, v1, v2}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 250
    move-object/from16 v0, v20

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 252
    move-object/from16 v0, v20

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_4

    .line 259
    .end local v5    # "extraIntentList":Ljava/util/List;, "Ljava/util/List<Landroid/content/Intent;>;"
    .end local v7    # "i":I
    .end local v16    # "packageName":Ljava/lang/String;
    .end local v17    # "pm":Landroid/content/pm/PackageManager;
    .end local v18    # "ri":Landroid/content/pm/ResolveInfo;
    .end local v20    # "textShareGDriveIntent":Landroid/content/Intent;
    .end local v22    # "textShareResInfo":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    .end local v23    # "textShareResInfoSize":I
    :cond_a
    new-instance v12, Landroid/content/Intent;

    const-string v25, "android.intent.action.SEND_MULTIPLE"

    move-object/from16 v0, v25

    invoke-direct {v12, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 261
    .local v12, "multiShareIntent":Landroid/content/Intent;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/share/MemoShare;->attachmentList:Ljava/util/ArrayList;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Ljava/util/ArrayList;->size()I

    move-result v25

    const/16 v26, 0x1

    move/from16 v0, v25

    move/from16 v1, v26

    if-ne v0, v1, :cond_e

    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/app/memo/share/MemoShare;->isAudioInAttachment()Z

    move-result v25

    if-eqz v25, :cond_e

    .line 262
    const-string v25, "audio/mp4"

    move-object/from16 v0, v25

    invoke-virtual {v12, v0}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 268
    :goto_5
    const-string v25, "android.intent.extra.TEXT"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/share/MemoShare;->text:Ljava/lang/String;

    move-object/from16 v26, v0

    move-object/from16 v0, v25

    move-object/from16 v1, v26

    invoke-virtual {v12, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 269
    const-string v25, "android.intent.extra.HTML_TEXT"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/share/MemoShare;->htmlText:Ljava/lang/String;

    move-object/from16 v26, v0

    move-object/from16 v0, v25

    move-object/from16 v1, v26

    invoke-virtual {v12, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 270
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/share/MemoShare;->subject:Ljava/lang/String;

    move-object/from16 v25, v0

    if-eqz v25, :cond_b

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/share/MemoShare;->subject:Ljava/lang/String;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Ljava/lang/String;->isEmpty()Z

    move-result v25

    if-nez v25, :cond_b

    .line 271
    const-string v25, "android.intent.extra.SUBJECT"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/share/MemoShare;->subject:Ljava/lang/String;

    move-object/from16 v26, v0

    move-object/from16 v0, v25

    move-object/from16 v1, v26

    invoke-virtual {v12, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 272
    :cond_b
    const-string v25, "theme"

    const/16 v26, 0x2

    move-object/from16 v0, v25

    move/from16 v1, v26

    invoke-virtual {v12, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 273
    const-string v25, "android.intent.extra.STREAM"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/share/MemoShare;->attachmentList:Ljava/util/ArrayList;

    move-object/from16 v26, v0

    move-object/from16 v0, v25

    move-object/from16 v1, v26

    invoke-virtual {v12, v0, v1}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 274
    const-string v25, "Clipdata from Memo"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/share/MemoShare;->text:Ljava/lang/String;

    move-object/from16 v26, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/share/MemoShare;->htmlText:Ljava/lang/String;

    move-object/from16 v27, v0

    invoke-static/range {v25 .. v27}, Landroid/content/ClipData;->newHtmlText(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/String;)Landroid/content/ClipData;

    move-result-object v4

    .line 275
    .local v4, "clipData":Landroid/content/ClipData;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/share/MemoShare;->attachmentList:Ljava/util/ArrayList;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v25

    :goto_6
    invoke-interface/range {v25 .. v25}, Ljava/util/Iterator;->hasNext()Z

    move-result v26

    if-nez v26, :cond_10

    .line 278
    invoke-virtual {v12, v4}, Landroid/content/Intent;->setClipData(Landroid/content/ClipData;)V

    .line 279
    const/16 v25, 0x1

    move/from16 v0, v25

    invoke-virtual {v12, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 281
    const-string v25, "android.intent.extra.INTENT"

    move-object/from16 v0, v25

    invoke-virtual {v3, v0, v12}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 282
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/share/MemoShare;->context:Landroid/content/Context;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v17

    .line 283
    .restart local v17    # "pm":Landroid/content/pm/PackageManager;
    const/16 v25, 0x0

    move-object/from16 v0, v17

    move/from16 v1, v25

    invoke-virtual {v0, v12, v1}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v14

    .line 284
    .local v14, "multiShareResInfo":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 285
    .restart local v5    # "extraIntentList":Ljava/util/List;, "Ljava/util/List<Landroid/content/Intent;>;"
    invoke-interface {v14}, Ljava/util/List;->size()I

    move-result v15

    .line 286
    .local v15, "multiShareResInfoSize":I
    const/4 v7, 0x0

    .restart local v7    # "i":I
    :goto_7
    if-lt v7, v15, :cond_11

    .line 316
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/app/memo/share/MemoShare;->isAudioInAttachment()Z

    move-result v25

    if-eqz v25, :cond_c

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/share/MemoShare;->text:Ljava/lang/String;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/String;->isEmpty()Z

    move-result v25

    if-nez v25, :cond_d

    .line 317
    :cond_c
    const/16 v25, 0x0

    move-object/from16 v0, v17

    move-object/from16 v1, v21

    move/from16 v2, v25

    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v22

    .line 318
    .restart local v22    # "textShareResInfo":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    invoke-interface/range {v22 .. v22}, Ljava/util/List;->size()I

    move-result v23

    .line 319
    .restart local v23    # "textShareResInfoSize":I
    const/4 v7, 0x0

    :goto_8
    move/from16 v0, v23

    if-lt v7, v0, :cond_17

    .line 344
    .end local v22    # "textShareResInfo":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    .end local v23    # "textShareResInfoSize":I
    :cond_d
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v25

    move/from16 v0, v25

    new-array v0, v0, [Landroid/content/Intent;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    invoke-interface {v5, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v6

    check-cast v6, [Landroid/content/Intent;

    .line 345
    .restart local v6    # "extraIntents":[Landroid/content/Intent;
    const-string v25, "android.intent.extra.INITIAL_INTENTS"

    move-object/from16 v0, v25

    invoke-virtual {v3, v0, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 347
    move-object v10, v6

    goto/16 :goto_3

    .line 263
    .end local v4    # "clipData":Landroid/content/ClipData;
    .end local v5    # "extraIntentList":Ljava/util/List;, "Ljava/util/List<Landroid/content/Intent;>;"
    .end local v6    # "extraIntents":[Landroid/content/Intent;
    .end local v7    # "i":I
    .end local v14    # "multiShareResInfo":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    .end local v15    # "multiShareResInfoSize":I
    .end local v17    # "pm":Landroid/content/pm/PackageManager;
    :cond_e
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/app/memo/share/MemoShare;->isAudioInAttachment()Z

    move-result v25

    if-eqz v25, :cond_f

    .line 264
    const-string v25, "*/*"

    move-object/from16 v0, v25

    invoke-virtual {v12, v0}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    goto/16 :goto_5

    .line 266
    :cond_f
    const-string v25, "image/*"

    move-object/from16 v0, v25

    invoke-virtual {v12, v0}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    goto/16 :goto_5

    .line 275
    .restart local v4    # "clipData":Landroid/content/ClipData;
    :cond_10
    invoke-interface/range {v25 .. v25}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v24

    check-cast v24, Landroid/net/Uri;

    .line 276
    .local v24, "uri":Landroid/net/Uri;
    new-instance v26, Landroid/content/ClipData$Item;

    move-object/from16 v0, v26

    move-object/from16 v1, v24

    invoke-direct {v0, v1}, Landroid/content/ClipData$Item;-><init>(Landroid/net/Uri;)V

    move-object/from16 v0, v26

    invoke-virtual {v4, v0}, Landroid/content/ClipData;->addItem(Landroid/content/ClipData$Item;)V

    goto/16 :goto_6

    .line 287
    .end local v24    # "uri":Landroid/net/Uri;
    .restart local v5    # "extraIntentList":Ljava/util/List;, "Ljava/util/List<Landroid/content/Intent;>;"
    .restart local v7    # "i":I
    .restart local v14    # "multiShareResInfo":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    .restart local v15    # "multiShareResInfoSize":I
    .restart local v17    # "pm":Landroid/content/pm/PackageManager;
    :cond_11
    invoke-interface {v14, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v25

    check-cast v25, Landroid/content/pm/ResolveInfo;

    move-object/from16 v0, v25

    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget-object v13, v0, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    .line 288
    .local v13, "multiSharePackageName":Ljava/lang/String;
    invoke-interface {v14, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Landroid/content/pm/ResolveInfo;

    .line 289
    .restart local v18    # "ri":Landroid/content/pm/ResolveInfo;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/share/MemoShare;->attachmentList:Ljava/util/ArrayList;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v25

    :goto_9
    invoke-interface/range {v25 .. v25}, Ljava/util/Iterator;->hasNext()Z

    move-result v26

    if-nez v26, :cond_15

    .line 293
    const-string v25, "com.android.bluetooth"

    move-object/from16 v0, v25

    invoke-virtual {v13, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-nez v25, :cond_12

    .line 294
    const-string v25, "com.samsung.android.app.FileShareClient"

    move-object/from16 v0, v25

    invoke-virtual {v13, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-nez v25, :cond_12

    .line 295
    const-string v25, "com.samsung.android.sconnect"

    move-object/from16 v0, v25

    invoke-virtual {v13, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-nez v25, :cond_12

    .line 296
    const-string v25, "com.android.nfc"

    move-object/from16 v0, v25

    invoke-virtual {v13, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_13

    .line 297
    :cond_12
    new-instance v11, Landroid/content/Intent;

    .line 298
    const-string v25, "com.samsung.android.sconnect"

    move-object/from16 v0, v25

    invoke-virtual {v13, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_16

    const-string v25, "android.intent.action.SEND_MULTIPLE"

    .line 297
    :goto_a
    move-object/from16 v0, v25

    invoke-direct {v11, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 300
    .restart local v11    # "memoFileShareIntent":Landroid/content/Intent;
    const-string v25, "application/vnd.samsung.android.memo"

    move-object/from16 v0, v25

    invoke-virtual {v11, v0}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 301
    const-string v25, "android.intent.extra.STREAM"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/share/MemoShare;->memoFileUri:Landroid/net/Uri;

    move-object/from16 v26, v0

    move-object/from16 v0, v25

    move-object/from16 v1, v26

    invoke-virtual {v11, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 302
    const-string v25, "theme"

    const/16 v26, 0x2

    move-object/from16 v0, v25

    move/from16 v1, v26

    invoke-virtual {v11, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 303
    new-instance v25, Landroid/content/ComponentName;

    .line 304
    move-object/from16 v0, v18

    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    iget-object v0, v0, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    move-object/from16 v26, v0

    move-object/from16 v0, v25

    move-object/from16 v1, v26

    invoke-direct {v0, v13, v1}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 303
    move-object/from16 v0, v25

    invoke-virtual {v11, v0}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 305
    invoke-interface {v5, v11}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 307
    .end local v11    # "memoFileShareIntent":Landroid/content/Intent;
    :cond_13
    const-string v25, "com.facebook.katana"

    move-object/from16 v0, v25

    invoke-virtual {v13, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_14

    .line 308
    new-instance v11, Landroid/content/Intent;

    const-string v25, "android.intent.action.SEND"

    move-object/from16 v0, v25

    invoke-direct {v11, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 309
    .restart local v11    # "memoFileShareIntent":Landroid/content/Intent;
    const-string v25, "text/plain"

    move-object/from16 v0, v25

    invoke-virtual {v11, v0}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 310
    const-string v25, "android.intent.extra.TEXT"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/share/MemoShare;->text:Ljava/lang/String;

    move-object/from16 v26, v0

    move-object/from16 v0, v25

    move-object/from16 v1, v26

    invoke-virtual {v11, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 311
    new-instance v25, Landroid/content/ComponentName;

    .line 312
    move-object/from16 v0, v18

    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    iget-object v0, v0, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    move-object/from16 v26, v0

    move-object/from16 v0, v25

    move-object/from16 v1, v26

    invoke-direct {v0, v13, v1}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 311
    move-object/from16 v0, v25

    invoke-virtual {v11, v0}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 313
    invoke-interface {v5, v11}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 286
    .end local v11    # "memoFileShareIntent":Landroid/content/Intent;
    :cond_14
    add-int/lit8 v7, v7, 0x1

    goto/16 :goto_7

    .line 289
    :cond_15
    invoke-interface/range {v25 .. v25}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v24

    check-cast v24, Landroid/net/Uri;

    .line 290
    .restart local v24    # "uri":Landroid/net/Uri;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/share/MemoShare;->context:Landroid/content/Context;

    move-object/from16 v26, v0

    .line 291
    const/16 v27, 0x1

    .line 290
    move-object/from16 v0, v26

    move-object/from16 v1, v24

    move/from16 v2, v27

    invoke-virtual {v0, v13, v1, v2}, Landroid/content/Context;->grantUriPermission(Ljava/lang/String;Landroid/net/Uri;I)V

    goto/16 :goto_9

    .line 299
    .end local v24    # "uri":Landroid/net/Uri;
    :cond_16
    const-string v25, "android.intent.action.SEND"

    goto/16 :goto_a

    .line 320
    .end local v13    # "multiSharePackageName":Ljava/lang/String;
    .end local v18    # "ri":Landroid/content/pm/ResolveInfo;
    .restart local v22    # "textShareResInfo":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    .restart local v23    # "textShareResInfoSize":I
    :cond_17
    move-object/from16 v0, v22

    invoke-interface {v0, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Landroid/content/pm/ResolveInfo;

    .line 321
    .restart local v18    # "ri":Landroid/content/pm/ResolveInfo;
    move-object/from16 v0, v18

    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget-object v0, v0, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    move-object/from16 v16, v0

    .line 322
    .restart local v16    # "packageName":Ljava/lang/String;
    const/16 v19, 0x0

    .line 323
    .local v19, "skip":Z
    const/4 v8, 0x0

    .local v8, "i1":I
    :goto_b
    if-lt v8, v15, :cond_19

    .line 333
    :goto_c
    if-nez v19, :cond_18

    const-string v25, "com.samsung.android.app.memo"

    move-object/from16 v0, v25

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-nez v25, :cond_18

    const-string v25, "com.sec.chaton"

    move-object/from16 v0, v25

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-nez v25, :cond_18

    .line 334
    new-instance v9, Landroid/content/Intent;

    invoke-direct {v9}, Landroid/content/Intent;-><init>()V

    .line 335
    .local v9, "intent":Landroid/content/Intent;
    new-instance v25, Landroid/content/ComponentName;

    move-object/from16 v0, v18

    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    iget-object v0, v0, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    move-object/from16 v26, v0

    move-object/from16 v0, v25

    move-object/from16 v1, v16

    move-object/from16 v2, v26

    invoke-direct {v0, v1, v2}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, v25

    invoke-virtual {v9, v0}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 336
    const-string v25, "android.intent.action.SEND"

    move-object/from16 v0, v25

    invoke-virtual {v9, v0}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 337
    const-string v25, "text/plain"

    move-object/from16 v0, v25

    invoke-virtual {v9, v0}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 338
    const-string v25, "android.intent.extra.TEXT"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/share/MemoShare;->text:Ljava/lang/String;

    move-object/from16 v26, v0

    move-object/from16 v0, v25

    move-object/from16 v1, v26

    invoke-virtual {v9, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 339
    invoke-interface {v5, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 319
    .end local v9    # "intent":Landroid/content/Intent;
    :cond_18
    add-int/lit8 v7, v7, 0x1

    goto/16 :goto_8

    .line 324
    :cond_19
    invoke-interface {v14, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v25

    check-cast v25, Landroid/content/pm/ResolveInfo;

    move-object/from16 v0, v25

    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget-object v13, v0, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    .line 325
    .restart local v13    # "multiSharePackageName":Ljava/lang/String;
    move-object/from16 v0, v16

    invoke-virtual {v13, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-nez v25, :cond_1a

    .line 326
    const-string v25, "com.cn21.ecloud"

    move-object/from16 v0, v25

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-nez v25, :cond_1a

    .line 327
    const-string v25, "com.sina.weibo"

    move-object/from16 v0, v25

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-nez v25, :cond_1a

    .line 328
    const-string v25, "flipboard.cn"

    move-object/from16 v0, v25

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_1b

    .line 329
    :cond_1a
    const/16 v19, 0x1

    .line 330
    goto/16 :goto_c

    .line 323
    :cond_1b
    add-int/lit8 v8, v8, 0x1

    goto/16 :goto_b
.end method

.method public shareMemoFile()Landroid/net/Uri;
    .locals 4

    .prologue
    .line 423
    iget-object v2, p0, Lcom/samsung/android/app/memo/share/MemoShare;->context:Landroid/content/Context;

    check-cast v2, Lcom/samsung/android/app/memo/share/MemoShare$SessionUpdateInterfaceForMemoShare;

    invoke-interface {v2}, Lcom/samsung/android/app/memo/share/MemoShare$SessionUpdateInterfaceForMemoShare;->updateSessionOnMemoShare()V

    .line 424
    new-instance v1, Lcom/samsung/android/app/memo/share/beam/XMLUtills;

    iget-object v2, p0, Lcom/samsung/android/app/memo/share/MemoShare;->context:Landroid/content/Context;

    invoke-direct {v1, v2}, Lcom/samsung/android/app/memo/share/beam/XMLUtills;-><init>(Landroid/content/Context;)V

    .line 425
    .local v1, "xmlUtils":Lcom/samsung/android/app/memo/share/beam/XMLUtills;
    const-wide/16 v2, -0x1

    invoke-virtual {v1, v2, v3}, Lcom/samsung/android/app/memo/share/beam/XMLUtills;->createXMLFile(J)Ljava/lang/String;

    move-result-object v0

    .line 426
    .local v0, "path":Ljava/lang/String;
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v2}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v2

    return-object v2
.end method

.method public shareOnSConnect()V
    .locals 4

    .prologue
    .line 431
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/samsung/android/app/memo/share/MemoShare;->attachmentList:Ljava/util/ArrayList;

    .line 432
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/share/MemoShare;->shareMemoFile()Landroid/net/Uri;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/android/app/memo/share/MemoShare;->memoFileUri:Landroid/net/Uri;

    .line 433
    iget-object v2, p0, Lcom/samsung/android/app/memo/share/MemoShare;->attachmentList:Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/samsung/android/app/memo/share/MemoShare;->memoFileUri:Landroid/net/Uri;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 434
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 435
    .local v1, "intent":Landroid/content/Intent;
    const-string v2, "com.samsung.android.sconnect.START"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 436
    const/high16 v2, 0x10000000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 437
    iget-object v2, p0, Lcom/samsung/android/app/memo/share/MemoShare;->attachmentList:Ljava/util/ArrayList;

    if-eqz v2, :cond_0

    .line 438
    const-string v2, "android.intent.extra.STREAM"

    iget-object v3, p0, Lcom/samsung/android/app/memo/share/MemoShare;->attachmentList:Ljava/util/ArrayList;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 440
    :try_start_0
    iget-object v2, p0, Lcom/samsung/android/app/memo/share/MemoShare;->context:Landroid/content/Context;

    invoke-virtual {v2, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 445
    :cond_0
    :goto_0
    return-void

    .line 441
    :catch_0
    move-exception v0

    .line 442
    .local v0, "e":Landroid/content/ActivityNotFoundException;
    sget-object v2, Lcom/samsung/android/app/memo/share/MemoShare;->TAG:Ljava/lang/String;

    const-string v3, "MemoShare"

    invoke-static {v2, v3, v0}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public startChooser(Landroid/content/Intent;[Landroid/content/Intent;)V
    .locals 8
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "extraIntentList"    # [Landroid/content/Intent;

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 448
    sget-object v4, Lcom/samsung/android/app/memo/share/MemoShare;->TAG:Ljava/lang/String;

    const-string v5, "startChooser"

    invoke-static {v4, v5}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 449
    iget-object v4, p0, Lcom/samsung/android/app/memo/share/MemoShare;->context:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    iput-object v4, p0, Lcom/samsung/android/app/memo/share/MemoShare;->mPm:Landroid/content/pm/PackageManager;

    .line 450
    const-string v4, "android.intent.extra.INTENT"

    invoke-virtual {p1, v4}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    .line 451
    .local v1, "targetParcelable":Landroid/os/Parcelable;
    check-cast v1, Landroid/content/Intent;

    .end local v1    # "targetParcelable":Landroid/os/Parcelable;
    iput-object v1, p0, Lcom/samsung/android/app/memo/share/MemoShare;->target:Landroid/content/Intent;

    .line 453
    const-string v4, "android.intent.extra.TITLE"

    invoke-virtual {p1, v4}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 454
    const-string v4, "android.intent.extra.TITLE"

    invoke-virtual {p1, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 456
    .local v3, "title":Ljava/lang/CharSequence;
    :goto_0
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, p0, Lcom/samsung/android/app/memo/share/MemoShare;->rList:Ljava/util/List;

    .line 457
    iget-object v4, p0, Lcom/samsung/android/app/memo/share/MemoShare;->target:Landroid/content/Intent;

    iget-object v5, p0, Lcom/samsung/android/app/memo/share/MemoShare;->rList:Ljava/util/List;

    invoke-virtual {p0, v4, v5, v6}, Lcom/samsung/android/app/memo/share/MemoShare;->filterResolveList(Landroid/content/Intent;Ljava/util/List;Z)V

    .line 458
    iget-object v4, p0, Lcom/samsung/android/app/memo/share/MemoShare;->rList:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_0

    if-eqz p2, :cond_0

    array-length v4, p2

    if-le v4, v6, :cond_0

    .line 459
    aget-object v4, p2, v6

    iget-object v5, p0, Lcom/samsung/android/app/memo/share/MemoShare;->rList:Ljava/util/List;

    invoke-virtual {p0, v4, v5, v7}, Lcom/samsung/android/app/memo/share/MemoShare;->filterResolveList(Landroid/content/Intent;Ljava/util/List;Z)V

    .line 460
    array-length v4, p2

    add-int/lit8 v4, v4, -0x2

    new-array v2, v4, [Landroid/content/Intent;

    .line 461
    .local v2, "temp":[Landroid/content/Intent;
    const/4 v4, 0x2

    array-length v5, v2

    invoke-static {p2, v4, v2, v7, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 462
    aget-object v4, p2, v6

    iput-object v4, p0, Lcom/samsung/android/app/memo/share/MemoShare;->target:Landroid/content/Intent;

    .line 463
    move-object p2, v2

    .line 466
    .end local v2    # "temp":[Landroid/content/Intent;
    :cond_0
    iget-object v4, p0, Lcom/samsung/android/app/memo/share/MemoShare;->rList:Ljava/util/List;

    iget-object v5, p0, Lcom/samsung/android/app/memo/share/MemoShare;->target:Landroid/content/Intent;

    invoke-virtual {p0, v4, v5, p2}, Lcom/samsung/android/app/memo/share/MemoShare;->createTargetShareIntents(Ljava/util/List;Landroid/content/Intent;[Landroid/content/Intent;)V

    .line 468
    iget-object v4, p0, Lcom/samsung/android/app/memo/share/MemoShare;->targetedShareIntents:Ljava/util/List;

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/samsung/android/app/memo/share/MemoShare;->targetedShareIntents:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-eqz v4, :cond_1

    .line 470
    iget-object v4, p0, Lcom/samsung/android/app/memo/share/MemoShare;->targetedShareIntents:Ljava/util/List;

    iget-object v5, p0, Lcom/samsung/android/app/memo/share/MemoShare;->targetedShareIntents:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    invoke-interface {v4, v5}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Intent;

    .line 469
    invoke-static {v4, v3}, Landroid/content/Intent;->createChooser(Landroid/content/Intent;Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object v0

    .line 471
    .local v0, "customChooserIntent":Landroid/content/Intent;
    const-string v5, "android.intent.extra.INITIAL_INTENTS"

    .line 472
    iget-object v4, p0, Lcom/samsung/android/app/memo/share/MemoShare;->targetedShareIntents:Ljava/util/List;

    new-array v6, v7, [Landroid/os/Parcelable;

    invoke-interface {v4, v6}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [Landroid/os/Parcelable;

    .line 471
    invoke-virtual {v0, v5, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 473
    iget-object v4, p0, Lcom/samsung/android/app/memo/share/MemoShare;->context:Landroid/content/Context;

    invoke-virtual {v4, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 475
    .end local v0    # "customChooserIntent":Landroid/content/Intent;
    :cond_1
    return-void

    .line 454
    .end local v3    # "title":Ljava/lang/CharSequence;
    :cond_2
    iget-object v4, p0, Lcom/samsung/android/app/memo/share/MemoShare;->context:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 455
    const v5, 0x7f0b0009

    .line 454
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v3

    goto :goto_0
.end method
