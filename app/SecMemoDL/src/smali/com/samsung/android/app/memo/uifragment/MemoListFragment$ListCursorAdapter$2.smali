.class Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter$2;
.super Ljava/lang/Object;
.source "MemoListFragment.java"

# interfaces
.implements Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;->bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;

.field private final synthetic val$t1:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListItemTag;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListItemTag;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter$2;->this$1:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;

    iput-object p2, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter$2;->val$t1:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListItemTag;

    .line 1787
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onGlobalLayout()V
    .locals 7

    .prologue
    .line 1791
    iget-object v4, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter$2;->val$t1:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListItemTag;

    iget-object v4, v4, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListItemTag;->snippet:Landroid/widget/TextView;

    invoke-virtual {v4}, Landroid/widget/TextView;->getLayout()Landroid/text/Layout;

    move-result-object v0

    .line 1792
    .local v0, "l":Landroid/text/Layout;
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/text/Layout;->getLineCount()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-virtual {v0, v4}, Landroid/text/Layout;->getEllipsisCount(I)I

    move-result v4

    if-lez v4, :cond_1

    .line 1793
    iget-object v4, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter$2;->val$t1:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListItemTag;

    iget-object v4, v4, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListItemTag;->snippet:Landroid/widget/TextView;

    invoke-virtual {v4}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v4

    invoke-interface {v4}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1794
    .local v2, "tempContent":Ljava/lang/String;
    const-string v4, " "

    const-string v5, ""

    invoke-virtual {v2, v4, v5}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1795
    .local v1, "noContents":Ljava/lang/String;
    iget-object v4, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter$2;->this$1:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;

    iget-object v5, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter$2;->val$t1:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListItemTag;

    iget-object v5, v5, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListItemTag;->snippet:Landroid/widget/TextView;

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v6

    # invokes: Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;->setHoverPopupContents(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;)V
    invoke-static {v4, v5, v6, v1}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;->access$1(Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;Landroid/view/View;Ljava/lang/String;Ljava/lang/String;)V

    .line 1801
    .end local v1    # "noContents":Ljava/lang/String;
    .end local v2    # "tempContent":Ljava/lang/String;
    :cond_0
    :goto_0
    iget-object v4, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter$2;->val$t1:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListItemTag;

    iget-object v4, v4, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListItemTag;->snippet:Landroid/widget/TextView;

    invoke-virtual {v4}, Landroid/widget/TextView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v3

    .line 1802
    .local v3, "vto":Landroid/view/ViewTreeObserver;
    invoke-virtual {v3, p0}, Landroid/view/ViewTreeObserver;->removeOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 1803
    return-void

    .line 1797
    .end local v3    # "vto":Landroid/view/ViewTreeObserver;
    :cond_1
    iget-object v4, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter$2;->val$t1:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListItemTag;

    iget-object v4, v4, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListItemTag;->mParentView:Landroid/view/View;

    if-eqz v4, :cond_0

    .line 1798
    iget-object v4, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter$2;->val$t1:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListItemTag;

    iget-object v4, v4, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListItemTag;->mParentView:Landroid/view/View;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/view/View;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    .line 1799
    iget-object v4, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter$2;->val$t1:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListItemTag;

    iget-object v4, v4, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListItemTag;->mParentView:Landroid/view/View;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/view/View;->setHovered(Z)V

    goto :goto_0
.end method
