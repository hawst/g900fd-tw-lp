.class Lcom/samsung/android/app/migration/xml/XMLParser$XMLHandler;
.super Lorg/xml/sax/helpers/DefaultHandler;
.source "XMLParser.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/migration/xml/XMLParser;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "XMLHandler"
.end annotation


# static fields
.field private static final TAG_CONTENT:Ljava/lang/String; = "t"

.field private static final TAG_CREATED_TIME:Ljava/lang/String; = "createdTime"

.field private static final TAG_MODIFIED_TIME:Ljava/lang/String; = "modifiedTime"

.field private static final TAG_TEMPLATE_TYPE:Ljava/lang/String; = "TemplateInfo"

.field private static final TAG_VALUE:Ljava/lang/String; = "val"


# instance fields
.field private buf:Ljava/lang/StringBuilder;

.field private currentElement:Z

.field private item:Lcom/samsung/android/app/migration/xml/XMLItem;


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 137
    invoke-direct {p0}, Lorg/xml/sax/helpers/DefaultHandler;-><init>()V

    .line 149
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/app/migration/xml/XMLParser$XMLHandler;->currentElement:Z

    .line 151
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/app/migration/xml/XMLParser$XMLHandler;->buf:Ljava/lang/StringBuilder;

    .line 153
    new-instance v0, Lcom/samsung/android/app/migration/xml/XMLItem;

    invoke-direct {v0}, Lcom/samsung/android/app/migration/xml/XMLItem;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/app/migration/xml/XMLParser$XMLHandler;->item:Lcom/samsung/android/app/migration/xml/XMLItem;

    .line 137
    return-void
.end method

.method synthetic constructor <init>(Lcom/samsung/android/app/migration/xml/XMLParser$XMLHandler;)V
    .locals 0

    .prologue
    .line 137
    invoke-direct {p0}, Lcom/samsung/android/app/migration/xml/XMLParser$XMLHandler;-><init>()V

    return-void
.end method


# virtual methods
.method public characters([CII)V
    .locals 3
    .param p1, "ch"    # [C
    .param p2, "start"    # I
    .param p3, "length"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xml/sax/SAXException;
        }
    .end annotation

    .prologue
    .line 199
    iget-boolean v1, p0, Lcom/samsung/android/app/migration/xml/XMLParser$XMLHandler;->currentElement:Z

    if-eqz v1, :cond_0

    .line 200
    move v0, p2

    .local v0, "i":I
    :goto_0
    add-int v1, p2, p3

    if-lt v0, v1, :cond_1

    .line 203
    .end local v0    # "i":I
    :cond_0
    return-void

    .line 201
    .restart local v0    # "i":I
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/app/migration/xml/XMLParser$XMLHandler;->buf:Ljava/lang/StringBuilder;

    aget-char v2, p1, v0

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 200
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public endElement(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "uri"    # Ljava/lang/String;
    .param p2, "localName"    # Ljava/lang/String;
    .param p3, "qName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xml/sax/SAXException;
        }
    .end annotation

    .prologue
    .line 191
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/app/migration/xml/XMLParser$XMLHandler;->currentElement:Z

    .line 192
    const-string v0, "t"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 193
    iget-object v0, p0, Lcom/samsung/android/app/migration/xml/XMLParser$XMLHandler;->item:Lcom/samsung/android/app/migration/xml/XMLItem;

    iget-object v1, p0, Lcom/samsung/android/app/migration/xml/XMLParser$XMLHandler;->buf:Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/migration/xml/XMLItem;->setContent(Ljava/lang/String;)V

    .line 194
    :cond_0
    return-void
.end method

.method public getContent()Ljava/lang/String;
    .locals 1

    .prologue
    .line 156
    iget-object v0, p0, Lcom/samsung/android/app/migration/xml/XMLParser$XMLHandler;->item:Lcom/samsung/android/app/migration/xml/XMLItem;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/app/migration/xml/XMLParser$XMLHandler;->item:Lcom/samsung/android/app/migration/xml/XMLItem;

    invoke-virtual {v0}, Lcom/samsung/android/app/migration/xml/XMLItem;->getContent()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getCreatedTime()J
    .locals 2

    .prologue
    .line 164
    iget-object v0, p0, Lcom/samsung/android/app/migration/xml/XMLParser$XMLHandler;->item:Lcom/samsung/android/app/migration/xml/XMLItem;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    return-wide v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/app/migration/xml/XMLParser$XMLHandler;->item:Lcom/samsung/android/app/migration/xml/XMLItem;

    invoke-virtual {v0}, Lcom/samsung/android/app/migration/xml/XMLItem;->getCreatedTime()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    goto :goto_0
.end method

.method public getModifiedTime()J
    .locals 2

    .prologue
    .line 168
    iget-object v0, p0, Lcom/samsung/android/app/migration/xml/XMLParser$XMLHandler;->item:Lcom/samsung/android/app/migration/xml/XMLItem;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    return-wide v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/app/migration/xml/XMLParser$XMLHandler;->item:Lcom/samsung/android/app/migration/xml/XMLItem;

    invoke-virtual {v0}, Lcom/samsung/android/app/migration/xml/XMLItem;->getModifiedTime()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    goto :goto_0
.end method

.method public getTemplateType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 160
    iget-object v0, p0, Lcom/samsung/android/app/migration/xml/XMLParser$XMLHandler;->item:Lcom/samsung/android/app/migration/xml/XMLItem;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/app/migration/xml/XMLParser$XMLHandler;->item:Lcom/samsung/android/app/migration/xml/XMLItem;

    invoke-virtual {v0}, Lcom/samsung/android/app/migration/xml/XMLItem;->getTemplateType()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public startElement(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 4
    .param p1, "uri"    # Ljava/lang/String;
    .param p2, "localName"    # Ljava/lang/String;
    .param p3, "qName"    # Ljava/lang/String;
    .param p4, "attributes"    # Lorg/xml/sax/Attributes;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xml/sax/SAXException;
        }
    .end annotation

    .prologue
    .line 175
    const-string v2, "t"

    invoke-virtual {p2, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 176
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/samsung/android/app/migration/xml/XMLParser$XMLHandler;->currentElement:Z

    .line 186
    :cond_0
    :goto_0
    return-void

    .line 177
    :cond_1
    const-string v2, "TemplateInfo"

    invoke-virtual {p2, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 178
    iget-object v2, p0, Lcom/samsung/android/app/migration/xml/XMLParser$XMLHandler;->item:Lcom/samsung/android/app/migration/xml/XMLItem;

    const-string v3, "val"

    invoke-interface {p4, v3}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/samsung/android/app/migration/xml/XMLItem;->setTemplateType(Ljava/lang/String;)V

    goto :goto_0

    .line 179
    :cond_2
    const-string v2, "createdTime"

    invoke-virtual {p2, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 180
    const-string v2, "val"

    invoke-interface {p4, v2}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    .line 181
    .local v0, "t":J
    iget-object v2, p0, Lcom/samsung/android/app/migration/xml/XMLParser$XMLHandler;->item:Lcom/samsung/android/app/migration/xml/XMLItem;

    invoke-virtual {v2, v0, v1}, Lcom/samsung/android/app/migration/xml/XMLItem;->setCreatedTime(J)V

    goto :goto_0

    .line 182
    .end local v0    # "t":J
    :cond_3
    const-string v2, "modifiedTime"

    invoke-virtual {p2, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 183
    const-string v2, "val"

    invoke-interface {p4, v2}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    .line 184
    .restart local v0    # "t":J
    iget-object v2, p0, Lcom/samsung/android/app/migration/xml/XMLParser$XMLHandler;->item:Lcom/samsung/android/app/migration/xml/XMLItem;

    invoke-virtual {v2, v0, v1}, Lcom/samsung/android/app/migration/xml/XMLItem;->setModifiedTime(J)V

    goto :goto_0
.end method
