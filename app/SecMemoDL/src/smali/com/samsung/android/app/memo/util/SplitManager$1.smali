.class Lcom/samsung/android/app/memo/util/SplitManager$1;
.super Ljava/lang/Object;
.source "SplitManager.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/memo/util/SplitManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field mDownX:I

.field final synthetic this$0:Lcom/samsung/android/app/memo/util/SplitManager;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/memo/util/SplitManager;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsung/android/app/memo/util/SplitManager$1;->this$0:Lcom/samsung/android/app/memo/util/SplitManager;

    .line 206
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 5
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/16 v4, 0x8

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 210
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v3

    packed-switch v3, :pswitch_data_0

    move v1, v2

    .line 228
    :goto_0
    return v1

    .line 212
    :pswitch_0
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawX()F

    move-result v3

    float-to-int v3, v3

    iput v3, p0, Lcom/samsung/android/app/memo/util/SplitManager$1;->mDownX:I

    .line 213
    iget-object v3, p0, Lcom/samsung/android/app/memo/util/SplitManager$1;->this$0:Lcom/samsung/android/app/memo/util/SplitManager;

    # getter for: Lcom/samsung/android/app/memo/util/SplitManager;->mNormalBar:Landroid/view/View;
    invoke-static {v3}, Lcom/samsung/android/app/memo/util/SplitManager;->access$0(Lcom/samsung/android/app/memo/util/SplitManager;)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v4}, Landroid/view/View;->setVisibility(I)V

    .line 214
    iget-object v3, p0, Lcom/samsung/android/app/memo/util/SplitManager$1;->this$0:Lcom/samsung/android/app/memo/util/SplitManager;

    # getter for: Lcom/samsung/android/app/memo/util/SplitManager;->mFocusedBar:Landroid/view/View;
    invoke-static {v3}, Lcom/samsung/android/app/memo/util/SplitManager;->access$1(Lcom/samsung/android/app/memo/util/SplitManager;)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 217
    :pswitch_1
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawX()F

    move-result v2

    float-to-int v0, v2

    .line 218
    .local v0, "x":I
    iget-object v2, p0, Lcom/samsung/android/app/memo/util/SplitManager$1;->this$0:Lcom/samsung/android/app/memo/util/SplitManager;

    iget v3, p0, Lcom/samsung/android/app/memo/util/SplitManager$1;->mDownX:I

    sub-int v3, v0, v3

    invoke-virtual {v2, v3}, Lcom/samsung/android/app/memo/util/SplitManager;->UpdateSplitPosition(I)Z

    .line 219
    iput v0, p0, Lcom/samsung/android/app/memo/util/SplitManager$1;->mDownX:I

    goto :goto_0

    .line 223
    .end local v0    # "x":I
    :pswitch_2
    iget-object v3, p0, Lcom/samsung/android/app/memo/util/SplitManager$1;->this$0:Lcom/samsung/android/app/memo/util/SplitManager;

    invoke-virtual {v3}, Lcom/samsung/android/app/memo/util/SplitManager;->saveSplitPosition()V

    .line 224
    iget-object v3, p0, Lcom/samsung/android/app/memo/util/SplitManager$1;->this$0:Lcom/samsung/android/app/memo/util/SplitManager;

    # getter for: Lcom/samsung/android/app/memo/util/SplitManager;->mNormalBar:Landroid/view/View;
    invoke-static {v3}, Lcom/samsung/android/app/memo/util/SplitManager;->access$0(Lcom/samsung/android/app/memo/util/SplitManager;)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/view/View;->setVisibility(I)V

    .line 225
    iget-object v2, p0, Lcom/samsung/android/app/memo/util/SplitManager$1;->this$0:Lcom/samsung/android/app/memo/util/SplitManager;

    # getter for: Lcom/samsung/android/app/memo/util/SplitManager;->mFocusedBar:Landroid/view/View;
    invoke-static {v2}, Lcom/samsung/android/app/memo/util/SplitManager;->access$1(Lcom/samsung/android/app/memo/util/SplitManager;)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 210
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
