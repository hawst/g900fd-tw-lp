.class public Lcom/samsung/android/app/migration/Migration$MigrationTask;
.super Landroid/os/AsyncTask;
.source "Migration.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/migration/Migration;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "MigrationTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/migration/Migration;


# direct methods
.method public constructor <init>(Lcom/samsung/android/app/migration/Migration;)V
    .locals 0

    .prologue
    .line 219
    iput-object p1, p0, Lcom/samsung/android/app/migration/Migration$MigrationTask;->this$0:Lcom/samsung/android/app/migration/Migration;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method private handleExpiredSyncDialogs()V
    .locals 4

    .prologue
    .line 326
    iget-object v2, p0, Lcom/samsung/android/app/migration/Migration$MigrationTask;->this$0:Lcom/samsung/android/app/migration/Migration;

    iget-object v2, v2, Lcom/samsung/android/app/migration/Migration;->activityContext:Lcom/samsung/android/app/memo/Main;

    invoke-static {v2}, Lcom/samsung/android/app/memo/util/AccountHelper;->getRegisteredSamsungAccount(Landroid/content/Context;)Landroid/accounts/Account;

    move-result-object v2

    if-nez v2, :cond_1

    .line 328
    iget-object v2, p0, Lcom/samsung/android/app/migration/Migration$MigrationTask;->this$0:Lcom/samsung/android/app/migration/Migration;

    iget-object v2, v2, Lcom/samsung/android/app/migration/Migration;->activityContext:Lcom/samsung/android/app/memo/Main;

    invoke-virtual {v2}, Lcom/samsung/android/app/memo/Main;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    sget-object v3, Lcom/samsung/android/app/migration/dialog/SyncMemoDialog;->TAG:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v1

    check-cast v1, Landroid/app/DialogFragment;

    .line 329
    .local v1, "syncMemoDialog":Landroid/app/DialogFragment;
    if-eqz v1, :cond_0

    .line 330
    invoke-virtual {v1}, Landroid/app/DialogFragment;->dismiss()V

    .line 332
    :cond_0
    iget-object v2, p0, Lcom/samsung/android/app/migration/Migration$MigrationTask;->this$0:Lcom/samsung/android/app/migration/Migration;

    iget-object v2, v2, Lcom/samsung/android/app/migration/Migration;->activityContext:Lcom/samsung/android/app/memo/Main;

    invoke-virtual {v2}, Lcom/samsung/android/app/memo/Main;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    sget-object v3, Lcom/samsung/android/app/migration/dialog/ImportDialog;->TAG:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Landroid/app/DialogFragment;

    .line 333
    .local v0, "importDialog":Landroid/app/DialogFragment;
    if-eqz v0, :cond_1

    .line 334
    invoke-virtual {v0}, Landroid/app/DialogFragment;->dismiss()V

    .line 336
    .end local v0    # "importDialog":Landroid/app/DialogFragment;
    .end local v1    # "syncMemoDialog":Landroid/app/DialogFragment;
    :cond_1
    return-void
.end method

.method private handleLocalMemosMigration()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 252
    iget-object v0, p0, Lcom/samsung/android/app/migration/Migration$MigrationTask;->this$0:Lcom/samsung/android/app/migration/Migration;

    iget-object v0, v0, Lcom/samsung/android/app/migration/Migration;->activityContext:Lcom/samsung/android/app/memo/Main;

    invoke-static {v0}, Lcom/samsung/android/app/migration/utils/SharedPref;->isLocalMemosMigrationCompleted(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 253
    iget-object v0, p0, Lcom/samsung/android/app/migration/Migration$MigrationTask;->this$0:Lcom/samsung/android/app/migration/Migration;

    # getter for: Lcom/samsung/android/app/migration/Migration;->TMemoApp:Ljava/lang/String;
    invoke-static {v0}, Lcom/samsung/android/app/migration/Migration;->access$1(Lcom/samsung/android/app/migration/Migration;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "TMemo1"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 254
    iget-object v0, p0, Lcom/samsung/android/app/migration/Migration$MigrationTask;->this$0:Lcom/samsung/android/app/migration/Migration;

    iget-object v1, p0, Lcom/samsung/android/app/migration/Migration$MigrationTask;->this$0:Lcom/samsung/android/app/migration/Migration;

    # invokes: Lcom/samsung/android/app/migration/Migration;->getTMemo1Data()Landroid/database/Cursor;
    invoke-static {v1}, Lcom/samsung/android/app/migration/Migration;->access$2(Lcom/samsung/android/app/migration/Migration;)Landroid/database/Cursor;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/app/migration/Migration;->access$3(Lcom/samsung/android/app/migration/Migration;Landroid/database/Cursor;)V

    .line 255
    iget-object v0, p0, Lcom/samsung/android/app/migration/Migration$MigrationTask;->this$0:Lcom/samsung/android/app/migration/Migration;

    # getter for: Lcom/samsung/android/app/migration/Migration;->cursor:Landroid/database/Cursor;
    invoke-static {v0}, Lcom/samsung/android/app/migration/Migration;->access$4(Lcom/samsung/android/app/migration/Migration;)Landroid/database/Cursor;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 256
    iget-object v0, p0, Lcom/samsung/android/app/migration/Migration$MigrationTask;->this$0:Lcom/samsung/android/app/migration/Migration;

    iget-object v0, v0, Lcom/samsung/android/app/migration/Migration;->activityContext:Lcom/samsung/android/app/memo/Main;

    invoke-static {v0, v3}, Lcom/samsung/android/app/migration/utils/SharedPref;->setLocalMemosMigrationInProcessFlag(Landroid/content/Context;Z)V

    .line 257
    iget-object v0, p0, Lcom/samsung/android/app/migration/Migration$MigrationTask;->this$0:Lcom/samsung/android/app/migration/Migration;

    invoke-virtual {v0}, Lcom/samsung/android/app/migration/Migration;->startLocalMemosMigration()V

    .line 267
    :cond_0
    :goto_0
    return-void

    .line 259
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/app/migration/Migration$MigrationTask;->this$0:Lcom/samsung/android/app/migration/Migration;

    # getter for: Lcom/samsung/android/app/migration/Migration;->TMemoApp:Ljava/lang/String;
    invoke-static {v0}, Lcom/samsung/android/app/migration/Migration;->access$1(Lcom/samsung/android/app/migration/Migration;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "TMemo2"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 260
    iget-object v0, p0, Lcom/samsung/android/app/migration/Migration$MigrationTask;->this$0:Lcom/samsung/android/app/migration/Migration;

    iget-object v1, p0, Lcom/samsung/android/app/migration/Migration$MigrationTask;->this$0:Lcom/samsung/android/app/migration/Migration;

    # getter for: Lcom/samsung/android/app/migration/Migration;->LOCAL_TMEMO2_DEFAULT_PATH:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/app/migration/Migration;->access$5()Ljava/lang/String;

    move-result-object v2

    # invokes: Lcom/samsung/android/app/migration/Migration;->getTMemo2Data(Ljava/lang/String;)Ljava/util/ArrayList;
    invoke-static {v1, v2}, Lcom/samsung/android/app/migration/Migration;->access$6(Lcom/samsung/android/app/migration/Migration;Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/app/migration/Migration;->access$7(Lcom/samsung/android/app/migration/Migration;Ljava/util/ArrayList;)V

    .line 261
    iget-object v0, p0, Lcom/samsung/android/app/migration/Migration$MigrationTask;->this$0:Lcom/samsung/android/app/migration/Migration;

    # getter for: Lcom/samsung/android/app/migration/Migration;->localSnbPaths:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/samsung/android/app/migration/Migration;->access$8(Lcom/samsung/android/app/migration/Migration;)Ljava/util/ArrayList;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 262
    iget-object v0, p0, Lcom/samsung/android/app/migration/Migration$MigrationTask;->this$0:Lcom/samsung/android/app/migration/Migration;

    iget-object v0, v0, Lcom/samsung/android/app/migration/Migration;->activityContext:Lcom/samsung/android/app/memo/Main;

    invoke-static {v0, v3}, Lcom/samsung/android/app/migration/utils/SharedPref;->setLocalMemosMigrationInProcessFlag(Landroid/content/Context;Z)V

    .line 263
    iget-object v0, p0, Lcom/samsung/android/app/migration/Migration$MigrationTask;->this$0:Lcom/samsung/android/app/migration/Migration;

    invoke-virtual {v0}, Lcom/samsung/android/app/migration/Migration;->startLocalMemosMigration()V

    goto :goto_0
.end method

.method private handleMemoBackingUp()V
    .locals 2

    .prologue
    .line 291
    iget-object v0, p0, Lcom/samsung/android/app/migration/Migration$MigrationTask;->this$0:Lcom/samsung/android/app/migration/Migration;

    # getter for: Lcom/samsung/android/app/migration/Migration;->backupMemoTask:Lcom/samsung/android/app/migration/task/BackupMemoTask;
    invoke-static {v0}, Lcom/samsung/android/app/migration/Migration;->access$14(Lcom/samsung/android/app/migration/Migration;)Lcom/samsung/android/app/migration/task/BackupMemoTask;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 292
    iget-object v0, p0, Lcom/samsung/android/app/migration/Migration$MigrationTask;->this$0:Lcom/samsung/android/app/migration/Migration;

    iget-object v0, v0, Lcom/samsung/android/app/migration/Migration;->activityContext:Lcom/samsung/android/app/memo/Main;

    invoke-static {v0}, Lcom/samsung/android/app/migration/utils/SharedPref;->isMemoBackingUpInProcess(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 293
    iget-object v0, p0, Lcom/samsung/android/app/migration/Migration$MigrationTask;->this$0:Lcom/samsung/android/app/migration/Migration;

    # getter for: Lcom/samsung/android/app/migration/Migration;->backupMemoTask:Lcom/samsung/android/app/migration/task/BackupMemoTask;
    invoke-static {v0}, Lcom/samsung/android/app/migration/Migration;->access$14(Lcom/samsung/android/app/migration/Migration;)Lcom/samsung/android/app/migration/task/BackupMemoTask;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/app/migration/Migration$MigrationTask;->this$0:Lcom/samsung/android/app/migration/Migration;

    iget-object v1, v1, Lcom/samsung/android/app/migration/Migration;->activityContext:Lcom/samsung/android/app/memo/Main;

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/migration/task/BackupMemoTask;->showDialog(Landroid/app/Activity;)V

    .line 297
    :cond_0
    :goto_0
    return-void

    .line 295
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/app/migration/Migration$MigrationTask;->this$0:Lcom/samsung/android/app/migration/Migration;

    # getter for: Lcom/samsung/android/app/migration/Migration;->backupMemoTask:Lcom/samsung/android/app/migration/task/BackupMemoTask;
    invoke-static {v0}, Lcom/samsung/android/app/migration/Migration;->access$14(Lcom/samsung/android/app/migration/Migration;)Lcom/samsung/android/app/migration/task/BackupMemoTask;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/app/migration/Migration$MigrationTask;->this$0:Lcom/samsung/android/app/migration/Migration;

    iget-object v1, v1, Lcom/samsung/android/app/migration/Migration;->activityContext:Lcom/samsung/android/app/memo/Main;

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/migration/task/BackupMemoTask;->hideDialog(Landroid/app/Activity;)V

    goto :goto_0
.end method

.method private handleMemoRestoring()V
    .locals 2

    .prologue
    .line 300
    iget-object v0, p0, Lcom/samsung/android/app/migration/Migration$MigrationTask;->this$0:Lcom/samsung/android/app/migration/Migration;

    # getter for: Lcom/samsung/android/app/migration/Migration;->restoreMemoTask:Lcom/samsung/android/app/migration/task/RestoreMemoTask;
    invoke-static {v0}, Lcom/samsung/android/app/migration/Migration;->access$15(Lcom/samsung/android/app/migration/Migration;)Lcom/samsung/android/app/migration/task/RestoreMemoTask;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 301
    iget-object v0, p0, Lcom/samsung/android/app/migration/Migration$MigrationTask;->this$0:Lcom/samsung/android/app/migration/Migration;

    iget-object v0, v0, Lcom/samsung/android/app/migration/Migration;->activityContext:Lcom/samsung/android/app/memo/Main;

    invoke-static {v0}, Lcom/samsung/android/app/migration/utils/SharedPref;->isMemoRestoringInProcess(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 302
    iget-object v0, p0, Lcom/samsung/android/app/migration/Migration$MigrationTask;->this$0:Lcom/samsung/android/app/migration/Migration;

    # getter for: Lcom/samsung/android/app/migration/Migration;->restoreMemoTask:Lcom/samsung/android/app/migration/task/RestoreMemoTask;
    invoke-static {v0}, Lcom/samsung/android/app/migration/Migration;->access$15(Lcom/samsung/android/app/migration/Migration;)Lcom/samsung/android/app/migration/task/RestoreMemoTask;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/app/migration/Migration$MigrationTask;->this$0:Lcom/samsung/android/app/migration/Migration;

    iget-object v1, v1, Lcom/samsung/android/app/migration/Migration;->activityContext:Lcom/samsung/android/app/memo/Main;

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/migration/task/RestoreMemoTask;->showDialog(Landroid/app/Activity;)V

    .line 306
    :cond_0
    :goto_0
    return-void

    .line 304
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/app/migration/Migration$MigrationTask;->this$0:Lcom/samsung/android/app/migration/Migration;

    # getter for: Lcom/samsung/android/app/migration/Migration;->restoreMemoTask:Lcom/samsung/android/app/migration/task/RestoreMemoTask;
    invoke-static {v0}, Lcom/samsung/android/app/migration/Migration;->access$15(Lcom/samsung/android/app/migration/Migration;)Lcom/samsung/android/app/migration/task/RestoreMemoTask;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/app/migration/Migration$MigrationTask;->this$0:Lcom/samsung/android/app/migration/Migration;

    iget-object v1, v1, Lcom/samsung/android/app/migration/Migration;->activityContext:Lcom/samsung/android/app/memo/Main;

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/migration/task/RestoreMemoTask;->hideDialog(Landroid/app/Activity;)V

    goto :goto_0
.end method

.method private handleSmartSwitchMemoRestoring()V
    .locals 0

    .prologue
    .line 323
    return-void
.end method

.method private handleTMemo1Restoring()V
    .locals 2

    .prologue
    .line 270
    invoke-static {}, Lcom/samsung/android/app/migration/task/ConvertKiesTMemo1Task;->getInstance()Lcom/samsung/android/app/migration/task/ConvertKiesTMemo1Task;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/app/migration/Migration$MigrationTask;->this$0:Lcom/samsung/android/app/migration/Migration;

    iget-object v1, v1, Lcom/samsung/android/app/migration/Migration;->activityContext:Lcom/samsung/android/app/memo/Main;

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/migration/task/ConvertKiesTMemo1Task;->setContext(Landroid/app/Activity;)V

    .line 271
    return-void
.end method

.method private handleTMemo2Restoring()V
    .locals 7

    .prologue
    .line 274
    iget-object v0, p0, Lcom/samsung/android/app/migration/Migration$MigrationTask;->this$0:Lcom/samsung/android/app/migration/Migration;

    iget-object v0, v0, Lcom/samsung/android/app/migration/Migration;->activityContext:Lcom/samsung/android/app/memo/Main;

    invoke-static {v0}, Lcom/samsung/android/app/migration/utils/SharedPref;->isTMemo2RestoringInProcess(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-static {}, Lcom/samsung/android/app/migration/task/ConvertTMemo2Task;->getInstance()Lcom/samsung/android/app/migration/task/ConvertTMemo2Task;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 275
    iget-object v0, p0, Lcom/samsung/android/app/migration/Migration$MigrationTask;->this$0:Lcom/samsung/android/app/migration/Migration;

    # getter for: Lcom/samsung/android/app/migration/Migration;->convertTask:Lcom/samsung/android/app/migration/task/ConvertTMemo2Task;
    invoke-static {v0}, Lcom/samsung/android/app/migration/Migration;->access$9(Lcom/samsung/android/app/migration/Migration;)Lcom/samsung/android/app/migration/task/ConvertTMemo2Task;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/app/migration/task/ConvertTMemo2Task;->isAlive()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 277
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/app/migration/Migration$MigrationTask;->this$0:Lcom/samsung/android/app/migration/Migration;

    # getter for: Lcom/samsung/android/app/migration/Migration;->convertTask:Lcom/samsung/android/app/migration/task/ConvertTMemo2Task;
    invoke-static {v0}, Lcom/samsung/android/app/migration/Migration;->access$9(Lcom/samsung/android/app/migration/Migration;)Lcom/samsung/android/app/migration/task/ConvertTMemo2Task;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/app/migration/task/ConvertTMemo2Task;->join()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 281
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/samsung/android/app/migration/Migration$MigrationTask;->this$0:Lcom/samsung/android/app/migration/Migration;

    # getter for: Lcom/samsung/android/app/migration/Migration;->context:Landroid/content/Context;
    invoke-static {v0}, Lcom/samsung/android/app/migration/Migration;->access$10(Lcom/samsung/android/app/migration/Migration;)Landroid/content/Context;

    move-result-object v0

    if-nez v0, :cond_1

    .line 282
    iget-object v0, p0, Lcom/samsung/android/app/migration/Migration$MigrationTask;->this$0:Lcom/samsung/android/app/migration/Migration;

    iget-object v1, p0, Lcom/samsung/android/app/migration/Migration$MigrationTask;->this$0:Lcom/samsung/android/app/migration/Migration;

    iget-object v1, v1, Lcom/samsung/android/app/migration/Migration;->activityContext:Lcom/samsung/android/app/memo/Main;

    invoke-virtual {v1}, Lcom/samsung/android/app/memo/Main;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/app/migration/Migration;->access$11(Lcom/samsung/android/app/migration/Migration;Landroid/content/Context;)V

    .line 283
    :cond_1
    iget-object v6, p0, Lcom/samsung/android/app/migration/Migration$MigrationTask;->this$0:Lcom/samsung/android/app/migration/Migration;

    new-instance v0, Lcom/samsung/android/app/migration/task/ConvertTMemo2Task;

    iget-object v1, p0, Lcom/samsung/android/app/migration/Migration$MigrationTask;->this$0:Lcom/samsung/android/app/migration/Migration;

    iget-object v1, v1, Lcom/samsung/android/app/migration/Migration;->activityContext:Lcom/samsung/android/app/memo/Main;

    iget-object v2, p0, Lcom/samsung/android/app/migration/Migration$MigrationTask;->this$0:Lcom/samsung/android/app/migration/Migration;

    # getter for: Lcom/samsung/android/app/migration/Migration;->context:Landroid/content/Context;
    invoke-static {v2}, Lcom/samsung/android/app/migration/Migration;->access$10(Lcom/samsung/android/app/migration/Migration;)Landroid/content/Context;

    move-result-object v2

    .line 284
    iget-object v3, p0, Lcom/samsung/android/app/migration/Migration$MigrationTask;->this$0:Lcom/samsung/android/app/migration/Migration;

    # getter for: Lcom/samsung/android/app/migration/Migration;->context:Landroid/content/Context;
    invoke-static {v3}, Lcom/samsung/android/app/migration/Migration;->access$10(Lcom/samsung/android/app/migration/Migration;)Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f0b0052

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/android/app/migration/Migration$MigrationTask;->this$0:Lcom/samsung/android/app/migration/Migration;

    # getter for: Lcom/samsung/android/app/migration/Migration;->kiesSnbPaths:Ljava/util/ArrayList;
    invoke-static {v4}, Lcom/samsung/android/app/migration/Migration;->access$12(Lcom/samsung/android/app/migration/Migration;)Ljava/util/ArrayList;

    move-result-object v4

    .line 285
    const/4 v5, 0x1

    invoke-direct/range {v0 .. v5}, Lcom/samsung/android/app/migration/task/ConvertTMemo2Task;-><init>(Landroid/app/Activity;Landroid/content/Context;Ljava/lang/String;Ljava/util/ArrayList;Z)V

    .line 283
    invoke-static {v6, v0}, Lcom/samsung/android/app/migration/Migration;->access$13(Lcom/samsung/android/app/migration/Migration;Lcom/samsung/android/app/migration/task/ConvertTMemo2Task;)V

    .line 286
    iget-object v0, p0, Lcom/samsung/android/app/migration/Migration$MigrationTask;->this$0:Lcom/samsung/android/app/migration/Migration;

    # getter for: Lcom/samsung/android/app/migration/Migration;->convertTask:Lcom/samsung/android/app/migration/task/ConvertTMemo2Task;
    invoke-static {v0}, Lcom/samsung/android/app/migration/Migration;->access$9(Lcom/samsung/android/app/migration/Migration;)Lcom/samsung/android/app/migration/task/ConvertTMemo2Task;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/app/migration/task/ConvertTMemo2Task;->start()V

    .line 288
    :cond_2
    return-void

    .line 278
    :catch_0
    move-exception v0

    goto :goto_0
.end method


# virtual methods
.method protected bridge varargs synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/samsung/android/app/migration/Migration$MigrationTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 1
    .param p1, "voids"    # [Ljava/lang/Void;

    .prologue
    .line 233
    iget-object v0, p0, Lcom/samsung/android/app/migration/Migration$MigrationTask;->this$0:Lcom/samsung/android/app/migration/Migration;

    iget-object v0, v0, Lcom/samsung/android/app/migration/Migration;->activityContext:Lcom/samsung/android/app/memo/Main;

    if-eqz v0, :cond_0

    .line 234
    invoke-direct {p0}, Lcom/samsung/android/app/migration/Migration$MigrationTask;->handleLocalMemosMigration()V

    .line 235
    invoke-direct {p0}, Lcom/samsung/android/app/migration/Migration$MigrationTask;->handleTMemo1Restoring()V

    .line 236
    invoke-direct {p0}, Lcom/samsung/android/app/migration/Migration$MigrationTask;->handleTMemo2Restoring()V

    .line 237
    invoke-direct {p0}, Lcom/samsung/android/app/migration/Migration$MigrationTask;->handleMemoBackingUp()V

    .line 238
    invoke-direct {p0}, Lcom/samsung/android/app/migration/Migration$MigrationTask;->handleMemoRestoring()V

    .line 239
    invoke-direct {p0}, Lcom/samsung/android/app/migration/Migration$MigrationTask;->handleSmartSwitchMemoRestoring()V

    .line 240
    invoke-direct {p0}, Lcom/samsung/android/app/migration/Migration$MigrationTask;->handleExpiredSyncDialogs()V

    .line 242
    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1
    check-cast p1, Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/samsung/android/app/migration/Migration$MigrationTask;->onPostExecute(Ljava/lang/Void;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/Void;)V
    .locals 1
    .param p1, "voids"    # Ljava/lang/Void;

    .prologue
    .line 247
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 248
    iget-object v0, p0, Lcom/samsung/android/app/migration/Migration$MigrationTask;->this$0:Lcom/samsung/android/app/migration/Migration;

    invoke-virtual {v0}, Lcom/samsung/android/app/migration/Migration;->showSyncMemoDialog()V

    .line 249
    return-void
.end method

.method protected onPreExecute()V
    .locals 2

    .prologue
    .line 223
    invoke-super {p0}, Landroid/os/AsyncTask;->onPreExecute()V

    .line 224
    iget-object v0, p0, Lcom/samsung/android/app/migration/Migration$MigrationTask;->this$0:Lcom/samsung/android/app/migration/Migration;

    iget-object v0, v0, Lcom/samsung/android/app/migration/Migration;->activityContext:Lcom/samsung/android/app/memo/Main;

    if-eqz v0, :cond_0

    .line 225
    iget-object v0, p0, Lcom/samsung/android/app/migration/Migration$MigrationTask;->this$0:Lcom/samsung/android/app/migration/Migration;

    iget-object v1, p0, Lcom/samsung/android/app/migration/Migration$MigrationTask;->this$0:Lcom/samsung/android/app/migration/Migration;

    iget-object v1, v1, Lcom/samsung/android/app/migration/Migration;->activityContext:Lcom/samsung/android/app/memo/Main;

    invoke-static {v1}, Lcom/samsung/android/app/migration/utils/MemoAppInfoHelper;->getTMemoApp(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/app/migration/Migration;->access$0(Lcom/samsung/android/app/migration/Migration;Ljava/lang/String;)V

    .line 226
    iget-object v0, p0, Lcom/samsung/android/app/migration/Migration$MigrationTask;->this$0:Lcom/samsung/android/app/migration/Migration;

    # getter for: Lcom/samsung/android/app/migration/Migration;->TMemoApp:Ljava/lang/String;
    invoke-static {v0}, Lcom/samsung/android/app/migration/Migration;->access$1(Lcom/samsung/android/app/migration/Migration;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    .line 227
    iget-object v0, p0, Lcom/samsung/android/app/migration/Migration$MigrationTask;->this$0:Lcom/samsung/android/app/migration/Migration;

    iget-object v0, v0, Lcom/samsung/android/app/migration/Migration;->activityContext:Lcom/samsung/android/app/memo/Main;

    invoke-static {v0}, Lcom/samsung/android/app/migration/utils/SharedPref;->setLocalMemosMigrationCompletedFlag(Landroid/content/Context;)V

    .line 229
    :cond_0
    return-void
.end method
