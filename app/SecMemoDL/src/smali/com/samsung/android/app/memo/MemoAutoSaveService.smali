.class public Lcom/samsung/android/app/memo/MemoAutoSaveService;
.super Landroid/app/Service;
.source "MemoAutoSaveService.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "MemoAutoSaveService"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    return-void
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1, "arg0"    # Landroid/content/Intent;

    .prologue
    .line 17
    const/4 v0, 0x0

    return-object v0
.end method

.method public onStart(Landroid/content/Intent;I)V
    .locals 5
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "startid"    # I

    .prologue
    .line 22
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "uuid"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 23
    .local v1, "uuid":Ljava/lang/String;
    if-eqz v1, :cond_0

    .line 25
    :try_start_0
    invoke-static {p0, v1}, Lcom/samsung/android/app/memo/util/ImageUtil;->createThumbnail(Landroid/content/Context;Ljava/lang/String;)V
    :try_end_0
    .catch Lcom/samsung/android/app/memo/MemoDataHandler$MemoDataHandlerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 30
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/MemoAutoSaveService;->stopSelf()V

    .line 31
    return-void

    .line 26
    :catch_0
    move-exception v0

    .line 27
    .local v0, "e":Lcom/samsung/android/app/memo/MemoDataHandler$MemoDataHandlerException;
    const-string v2, "MemoAutoSaveService"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "MemoDataHandlerException :"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/samsung/android/app/memo/MemoDataHandler$MemoDataHandlerException;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
