.class Lcom/samsung/android/app/memo/uiwidget/RichEditor$1;
.super Ljava/lang/Object;
.source "RichEditor.java"

# interfaces
.implements Landroid/text/InputFilter;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/memo/uiwidget/RichEditor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/memo/uiwidget/RichEditor;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/memo/uiwidget/RichEditor;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$1;->this$0:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    .line 456
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public filter(Ljava/lang/CharSequence;IILandroid/text/Spanned;II)Ljava/lang/CharSequence;
    .locals 5
    .param p1, "source"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "end"    # I
    .param p4, "dest"    # Landroid/text/Spanned;
    .param p5, "dstart"    # I
    .param p6, "dend"    # I

    .prologue
    .line 460
    sget v2, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->mMaxCharSize:I

    invoke-interface {p4}, Landroid/text/Spanned;->length()I

    move-result v3

    sub-int v4, p6, p5

    sub-int/2addr v3, v4

    sub-int v1, v2, v3

    .line 462
    .local v1, "keep":I
    sub-int v2, p3, p2

    if-ge v1, v2, :cond_0

    iget-object v2, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$1;->this$0:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    # getter for: Lcom/samsung/android/app/memo/uiwidget/RichEditor;->mIsEditMode:Z
    invoke-static {v2}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->access$0(Lcom/samsung/android/app/memo/uiwidget/RichEditor;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 463
    iget-object v2, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$1;->this$0:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    # getter for: Lcom/samsung/android/app/memo/uiwidget/RichEditor;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->access$1(Lcom/samsung/android/app/memo/uiwidget/RichEditor;)Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f0b0048

    invoke-static {v2, v3}, Lcom/samsung/android/app/memo/util/Utils;->showToast(Landroid/content/Context;I)V

    .line 465
    :cond_0
    if-gtz v1, :cond_1

    .line 466
    const-string v2, ""

    .line 473
    :goto_0
    return-object v2

    .line 467
    :cond_1
    sub-int v2, p3, p2

    if-lt v1, v2, :cond_2

    .line 468
    const/4 v2, 0x0

    goto :goto_0

    .line 471
    :cond_2
    add-int v2, p2, v1

    :try_start_0
    invoke-interface {p1, p2, v2}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/samsung/android/app/memo/util/Utils;->removeBrokenEmojiChar(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    goto :goto_0

    .line 472
    :catch_0
    move-exception v0

    .line 473
    .local v0, "e":Ljava/lang/IndexOutOfBoundsException;
    const-string v2, ""

    goto :goto_0
.end method
