.class public interface abstract Lcom/samsung/android/app/memo/voice/VoiceRecorderBase$SoundListener;
.super Ljava/lang/Object;
.source "VoiceRecorderBase.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "SoundListener"
.end annotation


# virtual methods
.method public abstract onFailPlaying()V
.end method

.method public abstract onPausePlaying()V
.end method

.method public abstract onStartPlaying()V
.end method

.method public abstract onStopPlaying()V
.end method
