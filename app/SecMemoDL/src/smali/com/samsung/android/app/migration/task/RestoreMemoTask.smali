.class public Lcom/samsung/android/app/migration/task/RestoreMemoTask;
.super Ljava/lang/Thread;
.source "RestoreMemoTask.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "RestoreMemoTask"


# instance fields
.field private context:Landroid/app/Activity;

.field private ctx:Landroid/content/Context;

.field private filePath:Ljava/lang/String;

.field private isFromSmartSwitch:Z

.field private progressDialog:Lcom/samsung/android/app/migration/dialog/ProgressBarDialog;

.field private sessionKey:Ljava/lang/String;

.field private source:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 0
    .param p1, "context"    # Landroid/app/Activity;
    .param p2, "ctx"    # Landroid/content/Context;
    .param p3, "path"    # Ljava/lang/String;
    .param p4, "sourceApp"    # Ljava/lang/String;
    .param p5, "key"    # Ljava/lang/String;
    .param p6, "isFromSmartSwitch"    # Z

    .prologue
    .line 73
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 74
    iput-object p1, p0, Lcom/samsung/android/app/migration/task/RestoreMemoTask;->context:Landroid/app/Activity;

    .line 75
    iput-object p2, p0, Lcom/samsung/android/app/migration/task/RestoreMemoTask;->ctx:Landroid/content/Context;

    .line 76
    iput-object p3, p0, Lcom/samsung/android/app/migration/task/RestoreMemoTask;->filePath:Ljava/lang/String;

    .line 77
    iput-object p4, p0, Lcom/samsung/android/app/migration/task/RestoreMemoTask;->source:Ljava/lang/String;

    .line 78
    iput-object p5, p0, Lcom/samsung/android/app/migration/task/RestoreMemoTask;->sessionKey:Ljava/lang/String;

    .line 79
    iput-boolean p6, p0, Lcom/samsung/android/app/migration/task/RestoreMemoTask;->isFromSmartSwitch:Z

    .line 80
    return-void
.end method

.method static synthetic access$0(Lcom/samsung/android/app/migration/task/RestoreMemoTask;)Lcom/samsung/android/app/migration/dialog/ProgressBarDialog;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/samsung/android/app/migration/task/RestoreMemoTask;->progressDialog:Lcom/samsung/android/app/migration/dialog/ProgressBarDialog;

    return-object v0
.end method

.method private decryptStream(Ljava/io/InputStream;Ljava/lang/String;)Ljava/io/InputStream;
    .locals 9
    .param p1, "in"    # Ljava/io/InputStream;
    .param p2, "password"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/4 v8, 0x0

    .line 100
    const-string v6, "SHA-256"

    invoke-static {v6}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v1

    .line 101
    .local v1, "digest":Ljava/security/MessageDigest;
    const-string v6, "UTF-8"

    invoke-virtual {p2, v6}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v6

    invoke-virtual {v1, v6}, Ljava/security/MessageDigest;->update([B)V

    .line 102
    const/16 v6, 0x10

    new-array v4, v6, [B

    .line 103
    .local v4, "keyBytes":[B
    invoke-virtual {v1}, Ljava/security/MessageDigest;->digest()[B

    move-result-object v6

    array-length v7, v4

    invoke-static {v6, v8, v4, v8, v7}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 104
    const-string v6, "AES/CBC/PKCS5Padding"

    invoke-static {v6}, Ljavax/crypto/Cipher;->getInstance(Ljava/lang/String;)Ljavax/crypto/Cipher;

    move-result-object v0

    .line 105
    .local v0, "cipher":Ljavax/crypto/Cipher;
    new-instance v3, Ljavax/crypto/spec/SecretKeySpec;

    const-string v6, "AES"

    invoke-direct {v3, v4, v6}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BLjava/lang/String;)V

    .line 107
    .local v3, "key":Ljavax/crypto/spec/SecretKeySpec;
    invoke-virtual {v0}, Ljavax/crypto/Cipher;->getBlockSize()I

    move-result v6

    new-array v2, v6, [B

    .line 108
    .local v2, "iv":[B
    if-eqz v2, :cond_0

    .line 109
    invoke-virtual {p1, v2}, Ljava/io/InputStream;->read([B)I

    .line 110
    new-instance v5, Ljavax/crypto/spec/IvParameterSpec;

    invoke-direct {v5, v2}, Ljavax/crypto/spec/IvParameterSpec;-><init>([B)V

    .line 112
    .local v5, "spec":Ljava/security/spec/AlgorithmParameterSpec;
    const/4 v6, 0x2

    invoke-virtual {v0, v6, v3, v5}, Ljavax/crypto/Cipher;->init(ILjava/security/Key;Ljava/security/spec/AlgorithmParameterSpec;)V

    .line 113
    new-instance v6, Ljavax/crypto/CipherInputStream;

    invoke-direct {v6, p1, v0}, Ljavax/crypto/CipherInputStream;-><init>(Ljava/io/InputStream;Ljavax/crypto/Cipher;)V

    .line 115
    .end local v5    # "spec":Ljava/security/spec/AlgorithmParameterSpec;
    :goto_0
    return-object v6

    :cond_0
    const/4 v6, 0x0

    goto :goto_0
.end method

.method private insertCategoryEntry(Landroid/content/Context;Ljava/lang/String;Landroid/database/Cursor;Z)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "uuid"    # Ljava/lang/String;
    .param p3, "categoryCursor"    # Landroid/database/Cursor;
    .param p4, "skipSync"    # Z

    .prologue
    .line 459
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 460
    .local v0, "cv":Landroid/content/ContentValues;
    const-string v1, "UUID"

    invoke-virtual {v0, v1, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 461
    const-string v1, "isDirty"

    .line 462
    const-string v2, "isDirty"

    invoke-interface {p3, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    .line 461
    invoke-interface {p3, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 463
    const-string v1, "isDeleted"

    .line 464
    const-string v2, "isDeleted"

    invoke-interface {p3, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    .line 463
    invoke-interface {p3, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 465
    const-string v1, "orderBy"

    .line 466
    const-string v2, "orderBy"

    invoke-interface {p3, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    .line 465
    invoke-interface {p3, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 467
    const-string v1, "_display_name"

    .line 468
    const-string v2, "_display_name"

    invoke-interface {p3, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    .line 467
    invoke-interface {p3, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 469
    if-nez p4, :cond_0

    .line 470
    const-string v1, "v_skipSync1Set"

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 471
    const-string v1, "sync1"

    .line 472
    const-string v2, "sync1"

    invoke-interface {p3, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p3, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    .line 471
    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 473
    const-string v1, "sync2"

    .line 474
    const-string v2, "sync2"

    invoke-interface {p3, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p3, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 473
    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 476
    :cond_0
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Lcom/samsung/android/provider/memo/MemoContract;->BASE_URI_CATEGORY:Landroid/net/Uri;

    invoke-virtual {v1, v2, v0}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    .line 477
    return-void
.end method

.method private insertFileEntry(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/database/Cursor;Z)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "fileUUID"    # Ljava/lang/String;
    .param p3, "memoUUID"    # Ljava/lang/String;
    .param p4, "fileData"    # Ljava/lang/String;
    .param p5, "fileCursor"    # Landroid/database/Cursor;
    .param p6, "skipSync"    # Z

    .prologue
    .line 431
    :try_start_0
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 432
    .local v0, "cv":Landroid/content/ContentValues;
    const-string v2, "UUID"

    invoke-virtual {v0, v2, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 433
    const-string v2, "memoUUID"

    invoke-virtual {v0, v2, p3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 434
    const-string v2, "mime_type"

    .line 435
    const-string v3, "mime_type"

    invoke-interface {p5, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    .line 434
    invoke-interface {p5, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 436
    const-string v2, "_display_name"

    .line 437
    const-string v3, "_display_name"

    invoke-interface {p5, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {p5, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 436
    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 438
    const-string v2, "_size"

    .line 439
    const-string v3, "_size"

    invoke-interface {p5, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {p5, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    .line 438
    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 440
    const-string v2, "orientation"

    .line 441
    const-string v3, "orientation"

    invoke-interface {p5, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {p5, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    .line 440
    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 442
    const-string v2, "_data"

    invoke-virtual {v0, v2, p4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 443
    const-string v2, "_srcUri"

    invoke-virtual {v0, v2, p4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 444
    if-nez p6, :cond_0

    .line 445
    const-string v2, "v_skipSync1Set"

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 446
    const-string v2, "sync1"

    .line 447
    const-string v3, "sync1"

    invoke-interface {p5, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {p5, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    .line 446
    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 448
    const-string v2, "sync2"

    .line 449
    const-string v3, "sync2"

    invoke-interface {p5, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {p5, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 448
    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 452
    :cond_0
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Lcom/samsung/android/provider/memo/MemoContract;->BASE_URI_FILE:Landroid/net/Uri;

    invoke-virtual {v2, v3, v0}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 456
    .end local v0    # "cv":Landroid/content/ContentValues;
    :goto_0
    return-void

    .line 453
    :catch_0
    move-exception v1

    .line 454
    .local v1, "e":Landroid/database/sqlite/SQLiteException;
    const-string v2, "RestoreMemoTask"

    const-string v3, "Exception occurred in insertFileEntry."

    invoke-static {v2, v3, v1}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private isContextAlive(Landroid/app/Activity;)Z
    .locals 1
    .param p1, "context"    # Landroid/app/Activity;

    .prologue
    .line 83
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/app/Activity;->isDestroyed()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Landroid/app/Activity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 84
    const/4 v0, 0x1

    .line 86
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private restoreMemoDB(Ljava/lang/String;Ljava/lang/String;)I
    .locals 13
    .param p1, "targetDir"    # Ljava/lang/String;
    .param p2, "sessionKey"    # Ljava/lang/String;

    .prologue
    .line 120
    const-string v10, "RestoreMemoTask"

    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "restoreMemoDB: targetDir = "

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v11, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " sessionKey = "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 121
    const/4 v8, 0x0

    .line 122
    .local v8, "res":I
    const/4 v4, 0x0

    .line 123
    .local v4, "is":Ljava/io/InputStream;
    const/4 v2, 0x0

    .line 124
    .local v2, "fis":Ljava/io/InputStream;
    const/4 v5, 0x0

    .line 126
    .local v5, "newfos":Ljava/io/OutputStream;
    :try_start_0
    new-instance v3, Ljava/io/FileInputStream;

    const-string v10, "memo.bk"

    invoke-static {p1, v10}, Lcom/samsung/android/app/migration/utils/Utils;->concat(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-direct {v3, v10}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_4
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 127
    .end local v2    # "fis":Ljava/io/InputStream;
    .local v3, "fis":Ljava/io/InputStream;
    :try_start_1
    invoke-direct {p0, v3, p2}, Lcom/samsung/android/app/migration/task/RestoreMemoTask;->decryptStream(Ljava/io/InputStream;Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v4

    .line 128
    new-instance v10, Ljava/io/File;

    sget-object v11, Lcom/samsung/android/app/migration/utils/Utils;->TMP_FOLDER_PATH:Ljava/lang/String;

    invoke-direct {v10, v11}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10}, Ljava/io/File;->mkdirs()Z

    .line 129
    new-instance v6, Ljava/io/FileOutputStream;

    sget-object v10, Lcom/samsung/android/app/migration/utils/Utils;->TMP_FOLDER_PATH:Ljava/lang/String;

    .line 130
    const-string v11, "memo.zip"

    .line 129
    invoke-static {v10, v11}, Lcom/samsung/android/app/migration/utils/Utils;->concat(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-direct {v6, v10}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_5
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 131
    .end local v5    # "newfos":Ljava/io/OutputStream;
    .local v6, "newfos":Ljava/io/OutputStream;
    const/16 v10, 0x4000

    :try_start_2
    new-array v0, v10, [B

    .line 132
    .local v0, "buffer":[B
    const/4 v7, 0x0

    .line 133
    .local v7, "readcount":I
    :goto_0
    const/4 v10, 0x0

    array-length v11, v0

    invoke-virtual {v4, v0, v10, v11}, Ljava/io/InputStream;->read([BII)I

    move-result v7

    const/4 v10, -0x1

    if-ne v7, v10, :cond_3

    .line 135
    new-instance v9, Lnet/lingala/zip4j/core/ZipFile;

    sget-object v10, Lcom/samsung/android/app/migration/utils/Utils;->TMP_FOLDER_PATH:Ljava/lang/String;

    const-string v11, "memo.zip"

    invoke-static {v10, v11}, Lcom/samsung/android/app/migration/utils/Utils;->concat(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-direct {v9, v10}, Lnet/lingala/zip4j/core/ZipFile;-><init>(Ljava/lang/String;)V

    .line 136
    .local v9, "zipFile":Lnet/lingala/zip4j/core/ZipFile;
    sget-object v10, Lcom/samsung/android/app/migration/utils/Utils;->TMP_FOLDER_PATH:Ljava/lang/String;

    invoke-virtual {v9, v10}, Lnet/lingala/zip4j/core/ZipFile;->extractAll(Ljava/lang/String;)V

    .line 137
    new-instance v10, Ljava/io/File;

    sget-object v11, Lcom/samsung/android/app/migration/utils/Utils;->TMP_FOLDER_PATH:Ljava/lang/String;

    const-string v12, "memo.zip"

    invoke-static {v11, v12}, Lcom/samsung/android/app/migration/utils/Utils;->concat(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10}, Ljava/io/File;->delete()Z
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 145
    if-eqz v6, :cond_0

    .line 146
    :try_start_3
    invoke-virtual {v6}, Ljava/io/OutputStream;->close()V

    .line 147
    :cond_0
    if-eqz v4, :cond_1

    .line 148
    invoke-virtual {v4}, Ljava/io/InputStream;->close()V

    .line 149
    :cond_1
    if-eqz v3, :cond_a

    .line 150
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_3

    move-object v5, v6

    .end local v6    # "newfos":Ljava/io/OutputStream;
    .restart local v5    # "newfos":Ljava/io/OutputStream;
    move-object v2, v3

    .line 155
    .end local v0    # "buffer":[B
    .end local v3    # "fis":Ljava/io/InputStream;
    .end local v7    # "readcount":I
    .end local v9    # "zipFile":Lnet/lingala/zip4j/core/ZipFile;
    .restart local v2    # "fis":Ljava/io/InputStream;
    :cond_2
    :goto_1
    return v8

    .line 134
    .end local v2    # "fis":Ljava/io/InputStream;
    .end local v5    # "newfos":Ljava/io/OutputStream;
    .restart local v0    # "buffer":[B
    .restart local v3    # "fis":Ljava/io/InputStream;
    .restart local v6    # "newfos":Ljava/io/OutputStream;
    .restart local v7    # "readcount":I
    :cond_3
    const/4 v10, 0x0

    :try_start_4
    invoke-virtual {v6, v0, v10, v7}, Ljava/io/OutputStream;->write([BII)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    goto :goto_0

    .line 138
    .end local v0    # "buffer":[B
    .end local v7    # "readcount":I
    :catch_0
    move-exception v1

    move-object v5, v6

    .end local v6    # "newfos":Ljava/io/OutputStream;
    .restart local v5    # "newfos":Ljava/io/OutputStream;
    move-object v2, v3

    .line 139
    .end local v3    # "fis":Ljava/io/InputStream;
    .local v1, "e":Ljava/lang/Exception;
    .restart local v2    # "fis":Ljava/io/InputStream;
    :goto_2
    :try_start_5
    const-string v10, "RestoreMemoTask"

    const-string v11, "Exception occurred when restoring memo.db."

    invoke-static {v10, v11, v1}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 140
    const/4 v8, -0x1

    .line 141
    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v10

    const-string v11, "ENOSPC"

    invoke-virtual {v10, v11}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    move-result v10

    if-eqz v10, :cond_4

    .line 142
    const/4 v8, -0x4

    .line 145
    :cond_4
    if-eqz v5, :cond_5

    .line 146
    :try_start_6
    invoke-virtual {v5}, Ljava/io/OutputStream;->close()V

    .line 147
    :cond_5
    if-eqz v4, :cond_6

    .line 148
    invoke-virtual {v4}, Ljava/io/InputStream;->close()V

    .line 149
    :cond_6
    if-eqz v2, :cond_2

    .line 150
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_1

    goto :goto_1

    .line 151
    :catch_1
    move-exception v1

    .line 152
    .local v1, "e":Ljava/io/IOException;
    const-string v10, "RestoreMemoTask"

    const-string v11, "IOException occurred when closing streams in restoreMemoDB."

    invoke-static {v10, v11, v1}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 143
    .end local v1    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v10

    .line 145
    :goto_3
    if-eqz v5, :cond_7

    .line 146
    :try_start_7
    invoke-virtual {v5}, Ljava/io/OutputStream;->close()V

    .line 147
    :cond_7
    if-eqz v4, :cond_8

    .line 148
    invoke-virtual {v4}, Ljava/io/InputStream;->close()V

    .line 149
    :cond_8
    if-eqz v2, :cond_9

    .line 150
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_2

    .line 154
    :cond_9
    :goto_4
    throw v10

    .line 151
    :catch_2
    move-exception v1

    .line 152
    .restart local v1    # "e":Ljava/io/IOException;
    const-string v11, "RestoreMemoTask"

    const-string v12, "IOException occurred when closing streams in restoreMemoDB."

    invoke-static {v11, v12, v1}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_4

    .line 151
    .end local v1    # "e":Ljava/io/IOException;
    .end local v2    # "fis":Ljava/io/InputStream;
    .end local v5    # "newfos":Ljava/io/OutputStream;
    .restart local v0    # "buffer":[B
    .restart local v3    # "fis":Ljava/io/InputStream;
    .restart local v6    # "newfos":Ljava/io/OutputStream;
    .restart local v7    # "readcount":I
    .restart local v9    # "zipFile":Lnet/lingala/zip4j/core/ZipFile;
    :catch_3
    move-exception v1

    .line 152
    .restart local v1    # "e":Ljava/io/IOException;
    const-string v10, "RestoreMemoTask"

    const-string v11, "IOException occurred when closing streams in restoreMemoDB."

    invoke-static {v10, v11, v1}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .end local v1    # "e":Ljava/io/IOException;
    :cond_a
    move-object v5, v6

    .end local v6    # "newfos":Ljava/io/OutputStream;
    .restart local v5    # "newfos":Ljava/io/OutputStream;
    move-object v2, v3

    .end local v3    # "fis":Ljava/io/InputStream;
    .restart local v2    # "fis":Ljava/io/InputStream;
    goto :goto_1

    .line 143
    .end local v0    # "buffer":[B
    .end local v2    # "fis":Ljava/io/InputStream;
    .end local v7    # "readcount":I
    .end local v9    # "zipFile":Lnet/lingala/zip4j/core/ZipFile;
    .restart local v3    # "fis":Ljava/io/InputStream;
    :catchall_1
    move-exception v10

    move-object v2, v3

    .end local v3    # "fis":Ljava/io/InputStream;
    .restart local v2    # "fis":Ljava/io/InputStream;
    goto :goto_3

    .end local v2    # "fis":Ljava/io/InputStream;
    .end local v5    # "newfos":Ljava/io/OutputStream;
    .restart local v3    # "fis":Ljava/io/InputStream;
    .restart local v6    # "newfos":Ljava/io/OutputStream;
    :catchall_2
    move-exception v10

    move-object v5, v6

    .end local v6    # "newfos":Ljava/io/OutputStream;
    .restart local v5    # "newfos":Ljava/io/OutputStream;
    move-object v2, v3

    .end local v3    # "fis":Ljava/io/InputStream;
    .restart local v2    # "fis":Ljava/io/InputStream;
    goto :goto_3

    .line 138
    :catch_4
    move-exception v1

    goto :goto_2

    .end local v2    # "fis":Ljava/io/InputStream;
    .restart local v3    # "fis":Ljava/io/InputStream;
    :catch_5
    move-exception v1

    move-object v2, v3

    .end local v3    # "fis":Ljava/io/InputStream;
    .restart local v2    # "fis":Ljava/io/InputStream;
    goto :goto_2
.end method

.method private sendRestoreResponse(Landroid/content/Context;Ljava/lang/String;I)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "source"    # Ljava/lang/String;
    .param p3, "res"    # I

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 90
    if-nez p3, :cond_1

    .line 91
    invoke-static {p1, p2, v0, v0}, Lcom/samsung/android/app/migration/receiver/KiesReceiver;->sendRestoreResponse(Landroid/content/Context;Ljava/lang/String;II)V

    .line 96
    :cond_0
    :goto_0
    return-void

    .line 92
    :cond_1
    const/4 v0, -0x1

    if-ne p3, v0, :cond_2

    .line 93
    invoke-static {p1, p2, v1, v1}, Lcom/samsung/android/app/migration/receiver/KiesReceiver;->sendRestoreResponse(Landroid/content/Context;Ljava/lang/String;II)V

    goto :goto_0

    .line 94
    :cond_2
    const/4 v0, -0x4

    if-ne p3, v0, :cond_0

    .line 95
    const/4 v0, 0x2

    invoke-static {p1, p2, v1, v0}, Lcom/samsung/android/app/migration/receiver/KiesReceiver;->sendRestoreResponse(Landroid/content/Context;Ljava/lang/String;II)V

    goto :goto_0
.end method

.method private updateCategories(Landroid/content/Context;Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 19
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "checkDB"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 375
    const/4 v14, 0x0

    .local v14, "categoryCursor":Landroid/database/Cursor;
    const/4 v12, 0x0

    .local v12, "c1":Landroid/database/Cursor;
    const/4 v13, 0x0

    .line 377
    .local v13, "c2":Landroid/database/Cursor;
    :try_start_0
    const-string v5, "category"

    const/4 v6, 0x0

    const-string v7, "isDeleted = 0"

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    move-object/from16 v4, p2

    invoke-virtual/range {v4 .. v11}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v14

    .line 378
    if-eqz v14, :cond_4

    invoke-interface {v14}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v4

    if-eqz v4, :cond_4

    .line 381
    :cond_0
    const-string v4, "_display_name"

    invoke-interface {v14, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    .line 380
    invoke-interface {v14, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v16

    .line 382
    .local v16, "name":Ljava/lang/String;
    const-string v4, "UUID"

    invoke-interface {v14, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v14, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v18

    .line 383
    .local v18, "uuid":Ljava/lang/String;
    if-eqz v12, :cond_1

    .line 384
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    .line 386
    :cond_1
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    sget-object v5, Lcom/samsung/android/provider/memo/MemoContract;->BASE_URI_CATEGORY:Landroid/net/Uri;

    invoke-virtual {v5}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v5

    .line 387
    const-string v6, "force"

    invoke-virtual {v5, v6}, Landroid/net/Uri$Builder;->fragment(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v5

    invoke-virtual {v5}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v5

    const/4 v6, 0x0

    .line 388
    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "_display_name = \'"

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static/range {v16 .. v16}, Lcom/samsung/android/app/migration/utils/Utils;->forSQL(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "\'"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x0

    const/4 v9, 0x0

    .line 386
    invoke-virtual/range {v4 .. v9}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v12

    .line 389
    if-eqz v12, :cond_3

    .line 390
    invoke-interface {v12}, Landroid/database/Cursor;->getCount()I

    move-result v4

    if-nez v4, :cond_8

    .line 392
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    sget-object v5, Lcom/samsung/android/provider/memo/MemoContract;->BASE_URI_CATEGORY:Landroid/net/Uri;

    const/4 v6, 0x0

    .line 393
    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "UUID = \'"

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v18

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "\'"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x0

    const/4 v9, 0x0

    .line 392
    invoke-virtual/range {v4 .. v9}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v13

    .line 394
    const/16 v17, 0x0

    .line 395
    .local v17, "skipSync":Z
    if-eqz v13, :cond_2

    invoke-interface {v13}, Landroid/database/Cursor;->getCount()I

    move-result v4

    if-lez v4, :cond_2

    .line 396
    invoke-static {}, Lcom/samsung/android/app/memo/util/UUIDHelper;->newUUID()Ljava/lang/String;

    move-result-object v18

    .line 397
    const/16 v17, 0x1

    .line 399
    :cond_2
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v18

    move/from16 v3, v17

    invoke-direct {v0, v1, v2, v14, v3}, Lcom/samsung/android/app/migration/task/RestoreMemoTask;->insertCategoryEntry(Landroid/content/Context;Ljava/lang/String;Landroid/database/Cursor;Z)V

    .line 415
    .end local v17    # "skipSync":Z
    :cond_3
    :goto_0
    invoke-interface {v14}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v4

    if-nez v4, :cond_0

    .line 420
    .end local v16    # "name":Ljava/lang/String;
    .end local v18    # "uuid":Ljava/lang/String;
    :cond_4
    if-eqz v14, :cond_5

    .line 421
    invoke-interface {v14}, Landroid/database/Cursor;->close()V

    .line 422
    :cond_5
    if-eqz v12, :cond_6

    .line 423
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    .line 424
    :cond_6
    if-eqz v13, :cond_7

    .line 425
    invoke-interface {v13}, Landroid/database/Cursor;->close()V

    .line 427
    :cond_7
    :goto_1
    return-void

    .line 400
    .restart local v16    # "name":Ljava/lang/String;
    .restart local v18    # "uuid":Ljava/lang/String;
    :cond_8
    :try_start_1
    invoke-interface {v12}, Landroid/database/Cursor;->getCount()I

    move-result v4

    const/4 v5, 0x1

    if-ne v4, v5, :cond_b

    .line 401
    invoke-interface {v12}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v4

    if-eqz v4, :cond_3

    const-string v4, "isDeleted"

    invoke-interface {v12, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v12, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    const/4 v5, 0x1

    if-ne v4, v5, :cond_3

    .line 402
    invoke-static {}, Lcom/samsung/android/app/memo/util/UUIDHelper;->newUUID()Ljava/lang/String;

    move-result-object v18

    .line 403
    const/4 v4, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v18

    invoke-direct {v0, v1, v2, v14, v4}, Lcom/samsung/android/app/migration/task/RestoreMemoTask;->insertCategoryEntry(Landroid/content/Context;Ljava/lang/String;Landroid/database/Cursor;Z)V
    :try_end_1
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 417
    .end local v16    # "name":Ljava/lang/String;
    .end local v18    # "uuid":Ljava/lang/String;
    :catch_0
    move-exception v15

    .line 418
    .local v15, "e":Landroid/database/sqlite/SQLiteException;
    :try_start_2
    const-string v4, "RestoreMemoTask"

    const-string v5, "Exception occurred in updateCategories."

    invoke-static {v4, v5, v15}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 420
    if-eqz v14, :cond_9

    .line 421
    invoke-interface {v14}, Landroid/database/Cursor;->close()V

    .line 422
    :cond_9
    if-eqz v12, :cond_a

    .line 423
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    .line 424
    :cond_a
    if-eqz v13, :cond_7

    .line 425
    invoke-interface {v13}, Landroid/database/Cursor;->close()V

    goto :goto_1

    .line 405
    .end local v15    # "e":Landroid/database/sqlite/SQLiteException;
    .restart local v16    # "name":Ljava/lang/String;
    .restart local v18    # "uuid":Ljava/lang/String;
    :cond_b
    :try_start_3
    invoke-interface {v12}, Landroid/database/Cursor;->getCount()I

    move-result v4

    const/4 v5, 0x1

    if-le v4, v5, :cond_3

    .line 407
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    sget-object v5, Lcom/samsung/android/provider/memo/MemoContract;->BASE_URI_CATEGORY:Landroid/net/Uri;

    invoke-virtual {v5}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v5

    .line 408
    const-string v6, "force"

    invoke-virtual {v5, v6}, Landroid/net/Uri$Builder;->fragment(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v5

    invoke-virtual {v5}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v5

    const/4 v6, 0x0

    .line 409
    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "_display_name = \'"

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static/range {v16 .. v16}, Lcom/samsung/android/app/migration/utils/Utils;->forSQL(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "\' AND "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "isDeleted"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " = 0"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x0

    const/4 v9, 0x0

    .line 407
    invoke-virtual/range {v4 .. v9}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v13

    .line 410
    if-eqz v13, :cond_3

    invoke-interface {v13}, Landroid/database/Cursor;->getCount()I

    move-result v4

    if-nez v4, :cond_3

    .line 411
    const/4 v4, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v18

    invoke-direct {v0, v1, v2, v14, v4}, Lcom/samsung/android/app/migration/task/RestoreMemoTask;->insertCategoryEntry(Landroid/content/Context;Ljava/lang/String;Landroid/database/Cursor;Z)V
    :try_end_3
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto/16 :goto_0

    .line 419
    .end local v16    # "name":Ljava/lang/String;
    .end local v18    # "uuid":Ljava/lang/String;
    :catchall_0
    move-exception v4

    .line 420
    if-eqz v14, :cond_c

    .line 421
    invoke-interface {v14}, Landroid/database/Cursor;->close()V

    .line 422
    :cond_c
    if-eqz v12, :cond_d

    .line 423
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    .line 424
    :cond_d
    if-eqz v13, :cond_e

    .line 425
    invoke-interface {v13}, Landroid/database/Cursor;->close()V

    .line 426
    :cond_e
    throw v4
.end method

.method private updateCategoryEntry(Landroid/content/Context;Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;
    .locals 12
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "categoryCursor"    # Landroid/database/Cursor;
    .param p3, "uuid"    # Ljava/lang/String;

    .prologue
    const/4 v11, 0x1

    .line 307
    const/4 v6, 0x0

    .local v6, "c1":Landroid/database/Cursor;
    const/4 v7, 0x0

    .line 310
    .local v7, "c2":Landroid/database/Cursor;
    :try_start_0
    const-string v0, "_display_name"

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 309
    invoke-interface {p2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 311
    .local v10, "name":Ljava/lang/String;
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/provider/memo/MemoContract;->BASE_URI_CATEGORY:Landroid/net/Uri;

    const/4 v2, 0x0

    .line 312
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "_display_name = \'"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v10}, Lcom/samsung/android/app/migration/utils/Utils;->forSQL(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    .line 311
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 313
    if-eqz v6, :cond_0

    .line 314
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-nez v0, :cond_5

    .line 316
    new-instance v8, Landroid/content/ContentValues;

    invoke-direct {v8}, Landroid/content/ContentValues;-><init>()V

    .line 318
    .local v8, "cv":Landroid/content/ContentValues;
    const-string v0, "UUID"

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 317
    invoke-interface {p2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object p3

    .line 319
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/provider/memo/MemoContract;->BASE_URI_CATEGORY:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    .line 320
    const-string v2, "force"

    invoke-virtual {v1, v2}, Landroid/net/Uri$Builder;->fragment(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    const/4 v2, 0x0

    .line 321
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "UUID = \'"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    .line 319
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 322
    if-eqz v7, :cond_3

    invoke-interface {v7}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-eqz v0, :cond_3

    .line 324
    invoke-static {}, Lcom/samsung/android/app/memo/util/UUIDHelper;->newUUID()Ljava/lang/String;

    move-result-object p3

    .line 332
    :goto_0
    const-string v0, "UUID"

    invoke-virtual {v8, v0, p3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 333
    const-string v0, "isDirty"

    .line 334
    const-string v1, "isDirty"

    invoke-interface {p2, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    .line 333
    invoke-interface {p2, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v8, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 335
    const-string v0, "isDeleted"

    .line 336
    const-string v1, "isDeleted"

    invoke-interface {p2, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    .line 335
    invoke-interface {p2, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v8, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 337
    const-string v0, "orderBy"

    .line 338
    const-string v1, "orderBy"

    invoke-interface {p2, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    .line 337
    invoke-interface {p2, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v8, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 339
    const-string v0, "_display_name"

    invoke-virtual {v8, v0, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 340
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/provider/memo/MemoContract;->BASE_URI_CATEGORY:Landroid/net/Uri;

    invoke-virtual {v0, v1, v8}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 365
    .end local v8    # "cv":Landroid/content/ContentValues;
    :cond_0
    :goto_1
    if-eqz v6, :cond_1

    .line 366
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 367
    :cond_1
    if-eqz v7, :cond_2

    .line 368
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 370
    .end local v10    # "name":Ljava/lang/String;
    :cond_2
    :goto_2
    return-object p3

    .line 326
    .restart local v8    # "cv":Landroid/content/ContentValues;
    .restart local v10    # "name":Ljava/lang/String;
    :cond_3
    :try_start_1
    const-string v0, "v_skipSync1Set"

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v8, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 327
    const-string v0, "sync1"

    .line 328
    const-string v1, "sync1"

    invoke-interface {p2, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p2, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    .line 327
    invoke-virtual {v8, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 329
    const-string v0, "sync2"

    .line 330
    const-string v1, "sync2"

    invoke-interface {p2, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p2, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 329
    invoke-virtual {v8, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_0

    .line 362
    .end local v8    # "cv":Landroid/content/ContentValues;
    .end local v10    # "name":Ljava/lang/String;
    :catch_0
    move-exception v9

    .line 363
    .local v9, "e":Landroid/database/sqlite/SQLiteException;
    :try_start_2
    const-string v0, "RestoreMemoTask"

    const-string v1, "Exception occurred in updateCategoryEntry."

    invoke-static {v0, v1, v9}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 365
    if-eqz v6, :cond_4

    .line 366
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 367
    :cond_4
    if-eqz v7, :cond_2

    .line 368
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    goto :goto_2

    .line 342
    .end local v9    # "e":Landroid/database/sqlite/SQLiteException;
    .restart local v10    # "name":Ljava/lang/String;
    :cond_5
    :try_start_3
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 343
    const-string v0, "isDeleted"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-ne v0, v11, :cond_8

    .line 345
    invoke-static {}, Lcom/samsung/android/app/memo/util/UUIDHelper;->newUUID()Ljava/lang/String;

    move-result-object p3

    .line 346
    new-instance v8, Landroid/content/ContentValues;

    invoke-direct {v8}, Landroid/content/ContentValues;-><init>()V

    .line 347
    .restart local v8    # "cv":Landroid/content/ContentValues;
    const-string v0, "UUID"

    invoke-virtual {v8, v0, p3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 348
    const-string v0, "isDirty"

    .line 349
    const-string v1, "isDirty"

    invoke-interface {p2, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    .line 348
    invoke-interface {p2, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v8, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 350
    const-string v0, "isDeleted"

    .line 351
    const-string v1, "isDeleted"

    invoke-interface {p2, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    .line 350
    invoke-interface {p2, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v8, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 352
    const-string v0, "orderBy"

    .line 353
    const-string v1, "orderBy"

    invoke-interface {p2, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    .line 352
    invoke-interface {p2, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v8, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 354
    const-string v0, "_display_name"

    invoke-virtual {v8, v0, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 355
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/provider/memo/MemoContract;->BASE_URI_CATEGORY:Landroid/net/Uri;

    invoke-virtual {v0, v1, v8}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    :try_end_3
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto/16 :goto_1

    .line 364
    .end local v8    # "cv":Landroid/content/ContentValues;
    .end local v10    # "name":Ljava/lang/String;
    :catchall_0
    move-exception v0

    .line 365
    if-eqz v6, :cond_6

    .line 366
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 367
    :cond_6
    if-eqz v7, :cond_7

    .line 368
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 369
    :cond_7
    throw v0

    .line 358
    .restart local v10    # "name":Ljava/lang/String;
    :cond_8
    :try_start_4
    const-string v0, "UUID"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_4
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    move-result-object p3

    goto/16 :goto_1
.end method

.method private updateDB(Landroid/content/Context;Ljava/lang/String;)V
    .locals 37
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "dbPath"    # Ljava/lang/String;

    .prologue
    .line 159
    const/4 v4, 0x0

    .line 160
    .local v4, "checkDB":Landroid/database/sqlite/SQLiteDatabase;
    const/16 v30, 0x0

    .local v30, "memoCursor":Landroid/database/Cursor;
    const/16 v24, 0x0

    .local v24, "c":Landroid/database/Cursor;
    const/4 v15, 0x0

    .local v15, "fileCursor":Landroid/database/Cursor;
    const/16 v25, 0x0

    .line 162
    .local v25, "categoryCursor":Landroid/database/Cursor;
    const/4 v5, 0x0

    const/4 v6, 0x1

    :try_start_0
    move-object/from16 v0, p2

    invoke-static {v0, v5, v6}, Landroid/database/sqlite/SQLiteDatabase;->openDatabase(Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)Landroid/database/sqlite/SQLiteDatabase;
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v4

    .line 166
    :goto_0
    if-nez v4, :cond_1

    .line 167
    const-string v5, "RestoreMemoTask"

    const-string v6, "DB can\'t be opened."

    invoke-static {v5, v6}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 304
    :cond_0
    :goto_1
    return-void

    .line 163
    :catch_0
    move-exception v29

    .line 164
    .local v29, "e":Landroid/database/sqlite/SQLiteException;
    const-string v5, "RestoreMemoTask"

    const-string v6, "DB doesn\'t exist."

    move-object/from16 v0, v29

    invoke-static {v5, v6, v0}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 171
    .end local v29    # "e":Landroid/database/sqlite/SQLiteException;
    :cond_1
    :try_start_1
    const-string v5, "memo"

    const/4 v6, 0x0

    const-string v7, "isDeleted = 0"

    .line 172
    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    .line 171
    invoke-virtual/range {v4 .. v11}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v30

    .line 173
    if-eqz v30, :cond_10

    invoke-interface/range {v30 .. v30}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v5

    if-eqz v5, :cond_10

    .line 175
    :cond_2
    const-string v5, "UUID"

    move-object/from16 v0, v30

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    move-object/from16 v0, v30

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v13

    .line 176
    .local v13, "uuid":Ljava/lang/String;
    const-string v5, "_data"

    move-object/from16 v0, v30

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    move-object/from16 v0, v30

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-static {v0, v5}, Lcom/samsung/android/app/memo/util/Utils;->getAbsolutePath(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v28

    .line 177
    .local v28, "data":Ljava/lang/String;
    const-string v5, "content"

    move-object/from16 v0, v30

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    move-object/from16 v0, v30

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v27

    .line 178
    .local v27, "content":Ljava/lang/String;
    new-instance v35, Landroid/content/ContentValues;

    invoke-direct/range {v35 .. v35}, Landroid/content/ContentValues;-><init>()V

    .line 180
    .local v35, "values":Landroid/content/ContentValues;
    const-string v5, "lastModifiedAt"

    move-object/from16 v0, v30

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    .line 179
    move-object/from16 v0, v30

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v32

    .line 181
    .local v32, "modifiedAt":J
    if-eqz v15, :cond_3

    .line 182
    invoke-interface {v15}, Landroid/database/Cursor;->close()V

    .line 183
    :cond_3
    const-string v5, "file"

    const/4 v6, 0x0

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v10, "memoUUID = \'"

    invoke-direct {v8, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 184
    invoke-virtual {v8, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v10, "\' AND "

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v10, "isDeleted"

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v10, " = 0"

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    .line 183
    invoke-virtual/range {v4 .. v11}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v15

    .line 186
    if-eqz v24, :cond_4

    .line 187
    invoke-interface/range {v24 .. v24}, Landroid/database/Cursor;->close()V

    .line 188
    :cond_4
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    .line 189
    sget-object v6, Lcom/samsung/android/provider/memo/MemoContract;->BASE_URI_MEMO:Landroid/net/Uri;

    invoke-virtual {v6}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v6

    .line 190
    const-string v8, "force"

    invoke-virtual {v6, v8}, Landroid/net/Uri$Builder;->fragment(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v6

    invoke-virtual {v6}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v6

    const/4 v7, 0x0

    .line 191
    new-instance v8, Ljava/lang/StringBuilder;

    const-string v10, "UUID = \'"

    invoke-direct {v8, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v10, "\'"

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x0

    const/4 v10, 0x0

    .line 188
    invoke-virtual/range {v5 .. v10}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v24

    .line 192
    if-eqz v24, :cond_15

    invoke-interface/range {v24 .. v24}, Landroid/database/Cursor;->getCount()I

    move-result v5

    if-lez v5, :cond_15

    invoke-interface/range {v24 .. v24}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v5

    if-eqz v5, :cond_15

    .line 194
    const-string v5, "isDeleted"

    move-object/from16 v0, v24

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    move-object/from16 v0, v24

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    const/4 v6, 0x1

    if-eq v5, v6, :cond_5

    .line 195
    const-string v5, "lastModifiedAt"

    move-object/from16 v0, v24

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    move-object/from16 v0, v24

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v10

    cmp-long v5, v10, v32

    if-eqz v5, :cond_f

    .line 197
    :cond_5
    invoke-static {}, Lcom/samsung/android/app/memo/util/UUIDHelper;->newUUID()Ljava/lang/String;

    move-result-object v13

    .line 198
    new-instance v5, Ljava/io/File;

    move-object/from16 v0, v28

    invoke-direct {v5, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    new-instance v6, Ljava/io/File;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-static/range {v28 .. v28}, Lcom/samsung/android/app/migration/utils/Utils;->getFolderPath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-direct {v8, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 199
    invoke-virtual {v8, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v10, ".blob"

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v6, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 198
    invoke-static {v5, v6}, Lcom/samsung/android/app/memo/util/FileHelper;->copyFileItem(Ljava/io/File;Ljava/io/File;)I

    .line 200
    invoke-static/range {v28 .. v28}, Lcom/samsung/android/app/migration/utils/Utils;->getFolderPath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_6

    .line 201
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-static/range {v28 .. v28}, Lcom/samsung/android/app/migration/utils/Utils;->getFolderPath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ".blob"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v28

    .line 215
    :cond_6
    :goto_2
    const-string v5, "categoryUUID"

    move-object/from16 v0, v30

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    .line 214
    move-object/from16 v0, v30

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v26

    .line 216
    .local v26, "categoryUUID":Ljava/lang/String;
    const-string v5, "UUID"

    move-object/from16 v0, v35

    invoke-virtual {v0, v5, v13}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 217
    const-string v5, "isDirty"

    .line 218
    const-string v6, "isDirty"

    move-object/from16 v0, v30

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    move-object/from16 v0, v30

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    .line 217
    move-object/from16 v0, v35

    invoke-virtual {v0, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 219
    const-string v5, "isDeleted"

    .line 220
    const-string v6, "isDeleted"

    move-object/from16 v0, v30

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    move-object/from16 v0, v30

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    .line 219
    move-object/from16 v0, v35

    invoke-virtual {v0, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 221
    const-string v5, "createdAt"

    .line 222
    const-string v6, "createdAt"

    move-object/from16 v0, v30

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    move-object/from16 v0, v30

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    .line 221
    move-object/from16 v0, v35

    invoke-virtual {v0, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 223
    const-string v5, "lastModifiedAt"

    .line 224
    const-string v6, "lastModifiedAt"

    move-object/from16 v0, v30

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    .line 223
    move-object/from16 v0, v30

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    move-object/from16 v0, v35

    invoke-virtual {v0, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 225
    const-string v5, "title"

    .line 226
    const-string v6, "title"

    move-object/from16 v0, v30

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    move-object/from16 v0, v30

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 225
    move-object/from16 v0, v35

    invoke-virtual {v0, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 227
    const-string v5, "content"

    move-object/from16 v0, v35

    move-object/from16 v1, v27

    invoke-virtual {v0, v5, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 228
    const-string v5, "strippedContent"

    .line 229
    const-string v6, "strippedContent"

    move-object/from16 v0, v30

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    .line 228
    move-object/from16 v0, v30

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, v35

    invoke-virtual {v0, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 230
    const-string v5, "_data"

    invoke-static/range {v28 .. v28}, Lcom/samsung/android/app/memo/util/Utils;->getRelativePath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, v35

    invoke-virtual {v0, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 231
    const/16 v36, 0x0

    .line 232
    .local v36, "vrUUID":Ljava/lang/String;
    if-eqz v15, :cond_b

    invoke-interface {v15}, Landroid/database/Cursor;->getCount()I

    move-result v5

    if-lez v5, :cond_b

    invoke-interface {v15}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v5

    if-eqz v5, :cond_b

    .line 234
    :cond_7
    const-string v5, "UUID"

    invoke-interface {v15, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    invoke-interface {v15, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v12

    .line 235
    .local v12, "fileUUID":Ljava/lang/String;
    move-object/from16 v34, v12

    .line 236
    .local v34, "oldUUID":Ljava/lang/String;
    if-eqz v24, :cond_8

    .line 237
    invoke-interface/range {v24 .. v24}, Landroid/database/Cursor;->close()V

    .line 238
    :cond_8
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    .line 239
    sget-object v6, Lcom/samsung/android/provider/memo/MemoContract;->BASE_URI_FILE:Landroid/net/Uri;

    invoke-virtual {v6}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v6

    .line 240
    const-string v8, "force"

    invoke-virtual {v6, v8}, Landroid/net/Uri$Builder;->fragment(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v6

    invoke-virtual {v6}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v6

    const/4 v7, 0x0

    .line 241
    new-instance v8, Ljava/lang/StringBuilder;

    const-string v10, "UUID = \'"

    invoke-direct {v8, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v10, "\'"

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x0

    const/4 v10, 0x0

    .line 238
    invoke-virtual/range {v5 .. v10}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v24

    .line 242
    if-eqz v24, :cond_1a

    invoke-interface/range {v24 .. v24}, Landroid/database/Cursor;->getCount()I

    move-result v5

    if-lez v5, :cond_1a

    invoke-interface/range {v24 .. v24}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v5

    if-eqz v5, :cond_1a

    .line 243
    const-string v5, "isDeleted"

    move-object/from16 v0, v24

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    move-object/from16 v0, v24

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    const/4 v6, 0x1

    if-ne v5, v6, :cond_1c

    .line 245
    invoke-static {}, Lcom/samsung/android/app/memo/util/UUIDHelper;->newUUID()Ljava/lang/String;

    move-result-object v7

    .line 247
    .end local v12    # "fileUUID":Ljava/lang/String;
    .local v7, "fileUUID":Ljava/lang/String;
    const-string v5, "_data"

    invoke-interface {v15, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    invoke-interface {v15, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 246
    move-object/from16 v0, p1

    invoke-static {v0, v5}, Lcom/samsung/android/app/memo/util/Utils;->getAbsolutePath(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    .line 248
    .local v14, "fileData":Ljava/lang/String;
    invoke-static {v7}, Lcom/samsung/android/app/memo/util/FileHelper;->getPrivateFilePath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 249
    .local v9, "newFileData":Ljava/lang/String;
    new-instance v5, Ljava/io/File;

    invoke-direct {v5, v14}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    new-instance v6, Ljava/io/File;

    invoke-direct {v6, v9}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v5, v6}, Lcom/samsung/android/app/memo/util/FileHelper;->copyFileItem(Ljava/io/File;Ljava/io/File;)I

    .line 250
    invoke-static {v9}, Lcom/samsung/android/app/migration/utils/Utils;->getFolderPath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_9

    .line 251
    move-object v14, v9

    .line 252
    :cond_9
    const/4 v11, 0x1

    move-object/from16 v5, p0

    move-object/from16 v6, p1

    move-object v8, v13

    move-object v10, v15

    invoke-direct/range {v5 .. v11}, Lcom/samsung/android/app/migration/task/RestoreMemoTask;->insertFileEntry(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/database/Cursor;Z)V

    .line 254
    const-string v5, "content"

    move-object/from16 v0, v35

    invoke-virtual {v0, v5}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v27

    .line 255
    move-object/from16 v0, v27

    move-object/from16 v1, v34

    invoke-virtual {v0, v1, v7}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v31

    .line 256
    .local v31, "newContent":Ljava/lang/String;
    const-string v5, "content"

    move-object/from16 v0, v35

    move-object/from16 v1, v31

    invoke-virtual {v0, v5, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 269
    .end local v9    # "newFileData":Ljava/lang/String;
    .end local v14    # "fileData":Ljava/lang/String;
    .end local v31    # "newContent":Ljava/lang/String;
    :goto_3
    const-string v5, "mime_type"

    invoke-interface {v15, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    .line 268
    invoke-interface {v15, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 269
    const-string v6, "audio"

    invoke-virtual {v5, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_a

    .line 270
    move-object/from16 v36, v7

    .line 271
    :cond_a
    invoke-interface {v15}, Landroid/database/Cursor;->moveToNext()Z

    move-result v5

    if-nez v5, :cond_7

    .line 275
    .end local v7    # "fileUUID":Ljava/lang/String;
    .end local v34    # "oldUUID":Ljava/lang/String;
    :cond_b
    if-eqz v36, :cond_c

    .line 276
    const-string v5, "vrfileUUID"

    move-object/from16 v0, v35

    move-object/from16 v1, v36

    invoke-virtual {v0, v5, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 278
    :cond_c
    if-eqz v25, :cond_d

    .line 279
    invoke-interface/range {v25 .. v25}, Landroid/database/Cursor;->close()V

    .line 280
    :cond_d
    const-string v17, "category"

    const/16 v18, 0x0

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "UUID = \'"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 281
    move-object/from16 v0, v26

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\' AND "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "isDeleted"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " = 0"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    const/16 v20, 0x0

    const/16 v21, 0x0

    const/16 v22, 0x0

    const/16 v23, 0x0

    move-object/from16 v16, v4

    .line 280
    invoke-virtual/range {v16 .. v23}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v25

    .line 282
    if-eqz v25, :cond_e

    invoke-interface/range {v25 .. v25}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v5

    if-eqz v5, :cond_e

    .line 283
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v25

    invoke-direct {v0, v1, v2, v13}, Lcom/samsung/android/app/migration/task/RestoreMemoTask;->updateCategoryEntry(Landroid/content/Context;Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v26

    .line 284
    :cond_e
    const-string v5, "categoryUUID"

    move-object/from16 v0, v35

    move-object/from16 v1, v26

    invoke-virtual {v0, v5, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 286
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    sget-object v6, Lcom/samsung/android/provider/memo/MemoContract;->BASE_URI_MEMO:Landroid/net/Uri;

    move-object/from16 v0, v35

    invoke-virtual {v5, v6, v0}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    .line 287
    .end local v26    # "categoryUUID":Ljava/lang/String;
    .end local v36    # "vrUUID":Ljava/lang/String;
    :cond_f
    invoke-interface/range {v30 .. v30}, Landroid/database/Cursor;->moveToNext()Z

    move-result v5

    if-nez v5, :cond_2

    .line 289
    .end local v13    # "uuid":Ljava/lang/String;
    .end local v27    # "content":Ljava/lang/String;
    .end local v28    # "data":Ljava/lang/String;
    .end local v32    # "modifiedAt":J
    .end local v35    # "values":Landroid/content/ContentValues;
    :cond_10
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v4}, Lcom/samsung/android/app/migration/task/RestoreMemoTask;->updateCategories(Landroid/content/Context;Landroid/database/sqlite/SQLiteDatabase;)V
    :try_end_1
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 293
    if-eqz v24, :cond_11

    .line 294
    invoke-interface/range {v24 .. v24}, Landroid/database/Cursor;->close()V

    .line 295
    :cond_11
    if-eqz v30, :cond_12

    .line 296
    invoke-interface/range {v30 .. v30}, Landroid/database/Cursor;->close()V

    .line 297
    :cond_12
    if-eqz v15, :cond_13

    .line 298
    invoke-interface {v15}, Landroid/database/Cursor;->close()V

    .line 299
    :cond_13
    if-eqz v25, :cond_14

    .line 300
    invoke-interface/range {v25 .. v25}, Landroid/database/Cursor;->close()V

    .line 301
    :cond_14
    if-eqz v4, :cond_0

    .line 302
    invoke-virtual {v4}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    goto/16 :goto_1

    .line 208
    .restart local v13    # "uuid":Ljava/lang/String;
    .restart local v27    # "content":Ljava/lang/String;
    .restart local v28    # "data":Ljava/lang/String;
    .restart local v32    # "modifiedAt":J
    .restart local v35    # "values":Landroid/content/ContentValues;
    :cond_15
    :try_start_2
    const-string v5, "v_skipSync1Set"

    const/4 v6, 0x1

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    move-object/from16 v0, v35

    invoke-virtual {v0, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 209
    const-string v5, "sync1"

    .line 210
    const-string v6, "sync1"

    move-object/from16 v0, v30

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    move-object/from16 v0, v30

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    .line 209
    move-object/from16 v0, v35

    invoke-virtual {v0, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 211
    const-string v5, "sync2"

    .line 212
    const-string v6, "sync2"

    move-object/from16 v0, v30

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    move-object/from16 v0, v30

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 211
    move-object/from16 v0, v35

    invoke-virtual {v0, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_2

    .line 290
    .end local v13    # "uuid":Ljava/lang/String;
    .end local v27    # "content":Ljava/lang/String;
    .end local v28    # "data":Ljava/lang/String;
    .end local v32    # "modifiedAt":J
    .end local v35    # "values":Landroid/content/ContentValues;
    :catch_1
    move-exception v29

    .line 291
    .restart local v29    # "e":Landroid/database/sqlite/SQLiteException;
    :try_start_3
    const-string v5, "RestoreMemoTask"

    const-string v6, "Exception occurred in updateDB."

    move-object/from16 v0, v29

    invoke-static {v5, v6, v0}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 293
    if-eqz v24, :cond_16

    .line 294
    invoke-interface/range {v24 .. v24}, Landroid/database/Cursor;->close()V

    .line 295
    :cond_16
    if-eqz v30, :cond_17

    .line 296
    invoke-interface/range {v30 .. v30}, Landroid/database/Cursor;->close()V

    .line 297
    :cond_17
    if-eqz v15, :cond_18

    .line 298
    invoke-interface {v15}, Landroid/database/Cursor;->close()V

    .line 299
    :cond_18
    if-eqz v25, :cond_19

    .line 300
    invoke-interface/range {v25 .. v25}, Landroid/database/Cursor;->close()V

    .line 301
    :cond_19
    if-eqz v4, :cond_0

    .line 302
    invoke-virtual {v4}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    goto/16 :goto_1

    .line 263
    .end local v29    # "e":Landroid/database/sqlite/SQLiteException;
    .restart local v12    # "fileUUID":Ljava/lang/String;
    .restart local v13    # "uuid":Ljava/lang/String;
    .restart local v26    # "categoryUUID":Ljava/lang/String;
    .restart local v27    # "content":Ljava/lang/String;
    .restart local v28    # "data":Ljava/lang/String;
    .restart local v32    # "modifiedAt":J
    .restart local v34    # "oldUUID":Ljava/lang/String;
    .restart local v35    # "values":Landroid/content/ContentValues;
    .restart local v36    # "vrUUID":Ljava/lang/String;
    :cond_1a
    :try_start_4
    const-string v5, "_data"

    invoke-interface {v15, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    invoke-interface {v15, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 262
    move-object/from16 v0, p1

    invoke-static {v0, v5}, Lcom/samsung/android/app/memo/util/Utils;->getAbsolutePath(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    .line 264
    .restart local v14    # "fileData":Ljava/lang/String;
    invoke-static {v14}, Lcom/samsung/android/app/migration/utils/Utils;->getFolderPath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_1b

    .line 265
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-static {v14}, Lcom/samsung/android/app/migration/utils/Utils;->getFolderPath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ".blob"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    .line 266
    :cond_1b
    const/16 v16, 0x0

    move-object/from16 v10, p0

    move-object/from16 v11, p1

    invoke-direct/range {v10 .. v16}, Lcom/samsung/android/app/migration/task/RestoreMemoTask;->insertFileEntry(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/database/Cursor;Z)V
    :try_end_4
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .end local v14    # "fileData":Ljava/lang/String;
    :cond_1c
    move-object v7, v12

    .end local v12    # "fileUUID":Ljava/lang/String;
    .restart local v7    # "fileUUID":Ljava/lang/String;
    goto/16 :goto_3

    .line 292
    .end local v7    # "fileUUID":Ljava/lang/String;
    .end local v13    # "uuid":Ljava/lang/String;
    .end local v26    # "categoryUUID":Ljava/lang/String;
    .end local v27    # "content":Ljava/lang/String;
    .end local v28    # "data":Ljava/lang/String;
    .end local v32    # "modifiedAt":J
    .end local v34    # "oldUUID":Ljava/lang/String;
    .end local v35    # "values":Landroid/content/ContentValues;
    .end local v36    # "vrUUID":Ljava/lang/String;
    :catchall_0
    move-exception v5

    .line 293
    if-eqz v24, :cond_1d

    .line 294
    invoke-interface/range {v24 .. v24}, Landroid/database/Cursor;->close()V

    .line 295
    :cond_1d
    if-eqz v30, :cond_1e

    .line 296
    invoke-interface/range {v30 .. v30}, Landroid/database/Cursor;->close()V

    .line 297
    :cond_1e
    if-eqz v15, :cond_1f

    .line 298
    invoke-interface {v15}, Landroid/database/Cursor;->close()V

    .line 299
    :cond_1f
    if-eqz v25, :cond_20

    .line 300
    invoke-interface/range {v25 .. v25}, Landroid/database/Cursor;->close()V

    .line 301
    :cond_20
    if-eqz v4, :cond_21

    .line 302
    invoke-virtual {v4}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    .line 303
    :cond_21
    throw v5
.end method


# virtual methods
.method public hideDialog(Landroid/app/Activity;)V
    .locals 1
    .param p1, "context"    # Landroid/app/Activity;

    .prologue
    .line 500
    iput-object p1, p0, Lcom/samsung/android/app/migration/task/RestoreMemoTask;->context:Landroid/app/Activity;

    .line 501
    invoke-direct {p0, p1}, Lcom/samsung/android/app/migration/task/RestoreMemoTask;->isContextAlive(Landroid/app/Activity;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 502
    new-instance v0, Lcom/samsung/android/app/migration/task/RestoreMemoTask$2;

    invoke-direct {v0, p0}, Lcom/samsung/android/app/migration/task/RestoreMemoTask$2;-><init>(Lcom/samsung/android/app/migration/task/RestoreMemoTask;)V

    invoke-virtual {p1, v0}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 509
    :cond_0
    return-void
.end method

.method public run()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 513
    iget-object v2, p0, Lcom/samsung/android/app/migration/task/RestoreMemoTask;->ctx:Landroid/content/Context;

    invoke-static {v2, v5}, Lcom/samsung/android/app/migration/utils/SharedPref;->setMemoRestoringInProcessFlag(Landroid/content/Context;Z)V

    .line 514
    const/4 v2, 0x0

    .line 515
    iget-object v3, p0, Lcom/samsung/android/app/migration/task/RestoreMemoTask;->ctx:Landroid/content/Context;

    const v4, 0x7f0b0052

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 514
    invoke-static {v2, v3, v5}, Lcom/samsung/android/app/migration/dialog/ProgressBarDialog;->newInstance(Ljava/lang/String;Ljava/lang/String;Z)Lcom/samsung/android/app/migration/dialog/ProgressBarDialog;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/android/app/migration/task/RestoreMemoTask;->progressDialog:Lcom/samsung/android/app/migration/dialog/ProgressBarDialog;

    .line 516
    iget-object v2, p0, Lcom/samsung/android/app/migration/task/RestoreMemoTask;->context:Landroid/app/Activity;

    invoke-virtual {p0, v2}, Lcom/samsung/android/app/migration/task/RestoreMemoTask;->showDialog(Landroid/app/Activity;)V

    .line 517
    iget-object v2, p0, Lcom/samsung/android/app/migration/task/RestoreMemoTask;->filePath:Ljava/lang/String;

    iget-object v3, p0, Lcom/samsung/android/app/migration/task/RestoreMemoTask;->sessionKey:Ljava/lang/String;

    invoke-direct {p0, v2, v3}, Lcom/samsung/android/app/migration/task/RestoreMemoTask;->restoreMemoDB(Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    .line 518
    .local v1, "res":I
    if-nez v1, :cond_0

    .line 520
    new-instance v2, Ljava/io/File;

    sget-object v3, Lcom/samsung/android/app/migration/utils/Utils;->TMP_FOLDER_PATH:Ljava/lang/String;

    const-string v4, "app_attach"

    invoke-static {v3, v4}, Lcom/samsung/android/app/migration/utils/Utils;->concat(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 521
    new-instance v3, Ljava/io/File;

    new-instance v4, Ljava/lang/StringBuilder;

    iget-object v5, p0, Lcom/samsung/android/app/migration/task/RestoreMemoTask;->ctx:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v5

    iget-object v5, v5, Landroid/content/pm/ApplicationInfo;->dataDir:Ljava/lang/String;

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v5, "/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "app_attach"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 519
    invoke-static {v2, v3}, Lcom/samsung/android/app/memo/util/FileHelper;->copyFileItem(Ljava/io/File;Ljava/io/File;)I

    move-result v1

    .line 522
    if-nez v1, :cond_0

    .line 523
    sget-object v2, Lcom/samsung/android/app/migration/utils/Utils;->TMP_FOLDER_PATH:Ljava/lang/String;

    const-string v3, "memo.db"

    invoke-static {v2, v3}, Lcom/samsung/android/app/migration/utils/Utils;->concat(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 524
    .local v0, "dbPath":Ljava/lang/String;
    iget-object v2, p0, Lcom/samsung/android/app/migration/task/RestoreMemoTask;->ctx:Landroid/content/Context;

    invoke-direct {p0, v2, v0}, Lcom/samsung/android/app/migration/task/RestoreMemoTask;->updateDB(Landroid/content/Context;Ljava/lang/String;)V

    .line 525
    new-instance v2, Ljava/io/File;

    sget-object v3, Lcom/samsung/android/app/migration/utils/Utils;->TMP_FOLDER_PATH:Ljava/lang/String;

    const-string v4, "memo.db"

    invoke-static {v3, v4}, Lcom/samsung/android/app/migration/utils/Utils;->concat(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v2}, Lcom/samsung/android/app/memo/util/FileHelper;->deleteFileItem(Ljava/io/File;)V

    .line 526
    new-instance v2, Ljava/io/File;

    sget-object v3, Lcom/samsung/android/app/migration/utils/Utils;->TMP_FOLDER_PATH:Ljava/lang/String;

    .line 527
    const-string v4, "app_attach"

    .line 526
    invoke-static {v3, v4}, Lcom/samsung/android/app/migration/utils/Utils;->concat(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v2}, Lcom/samsung/android/app/memo/util/FileHelper;->deleteFileItem(Ljava/io/File;)V

    .line 528
    iget-boolean v2, p0, Lcom/samsung/android/app/migration/task/RestoreMemoTask;->isFromSmartSwitch:Z

    if-eqz v2, :cond_0

    .line 529
    new-instance v2, Ljava/io/File;

    iget-object v3, p0, Lcom/samsung/android/app/migration/task/RestoreMemoTask;->filePath:Ljava/lang/String;

    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v2}, Lcom/samsung/android/app/memo/util/FileHelper;->deleteFileItem(Ljava/io/File;)V

    .line 532
    .end local v0    # "dbPath":Ljava/lang/String;
    :cond_0
    iget-boolean v2, p0, Lcom/samsung/android/app/migration/task/RestoreMemoTask;->isFromSmartSwitch:Z

    if-nez v2, :cond_1

    .line 533
    iget-object v2, p0, Lcom/samsung/android/app/migration/task/RestoreMemoTask;->ctx:Landroid/content/Context;

    iget-object v3, p0, Lcom/samsung/android/app/migration/task/RestoreMemoTask;->source:Ljava/lang/String;

    invoke-direct {p0, v2, v3, v1}, Lcom/samsung/android/app/migration/task/RestoreMemoTask;->sendRestoreResponse(Landroid/content/Context;Ljava/lang/String;I)V

    .line 534
    :cond_1
    iget-object v2, p0, Lcom/samsung/android/app/migration/task/RestoreMemoTask;->context:Landroid/app/Activity;

    invoke-virtual {p0, v2}, Lcom/samsung/android/app/migration/task/RestoreMemoTask;->hideDialog(Landroid/app/Activity;)V

    .line 535
    iget-object v2, p0, Lcom/samsung/android/app/migration/task/RestoreMemoTask;->ctx:Landroid/content/Context;

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/samsung/android/app/migration/utils/SharedPref;->setMemoRestoringInProcessFlag(Landroid/content/Context;Z)V

    .line 536
    return-void
.end method

.method public showDialog(Landroid/app/Activity;)V
    .locals 4
    .param p1, "context"    # Landroid/app/Activity;

    .prologue
    .line 480
    iput-object p1, p0, Lcom/samsung/android/app/migration/task/RestoreMemoTask;->context:Landroid/app/Activity;

    .line 481
    invoke-direct {p0, p1}, Lcom/samsung/android/app/migration/task/RestoreMemoTask;->isContextAlive(Landroid/app/Activity;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p1}, Landroid/app/Activity;->isResumed()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 482
    new-instance v1, Ljava/util/concurrent/Semaphore;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Ljava/util/concurrent/Semaphore;-><init>(I)V

    .line 483
    .local v1, "mutex":Ljava/util/concurrent/Semaphore;
    new-instance v2, Lcom/samsung/android/app/migration/task/RestoreMemoTask$1;

    invoke-direct {v2, p0, p1, v1}, Lcom/samsung/android/app/migration/task/RestoreMemoTask$1;-><init>(Lcom/samsung/android/app/migration/task/RestoreMemoTask;Landroid/app/Activity;Ljava/util/concurrent/Semaphore;)V

    invoke-virtual {p1, v2}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 492
    :try_start_0
    invoke-virtual {v1}, Ljava/util/concurrent/Semaphore;->acquire()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 497
    .end local v1    # "mutex":Ljava/util/concurrent/Semaphore;
    :cond_0
    :goto_0
    return-void

    .line 493
    .restart local v1    # "mutex":Ljava/util/concurrent/Semaphore;
    :catch_0
    move-exception v0

    .line 494
    .local v0, "e":Ljava/lang/InterruptedException;
    const-string v2, "RestoreMemoTask"

    const-string v3, "Error in acquiring mutex in showDialog."

    invoke-static {v2, v3}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
