.class public Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView$ScaleListener;
.super Landroid/view/ScaleGestureDetector$SimpleOnScaleGestureListener;
.source "ZoomableImageView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "ScaleListener"
.end annotation


# instance fields
.field protected mScaled:Z

.field final synthetic this$0:Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView;


# direct methods
.method public constructor <init>(Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView;)V
    .locals 1

    .prologue
    .line 360
    iput-object p1, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView$ScaleListener;->this$0:Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView;

    invoke-direct {p0}, Landroid/view/ScaleGestureDetector$SimpleOnScaleGestureListener;-><init>()V

    .line 362
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView$ScaleListener;->mScaled:Z

    return-void
.end method


# virtual methods
.method public onScale(Landroid/view/ScaleGestureDetector;)Z
    .locals 6
    .param p1, "detector"    # Landroid/view/ScaleGestureDetector;

    .prologue
    const/4 v2, 0x1

    .line 367
    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getCurrentSpan()F

    move-result v3

    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getCurrentSpanY()F

    move-result v4

    cmpl-float v3, v3, v4

    if-nez v3, :cond_1

    .line 368
    const/4 v2, 0x0

    .line 385
    :cond_0
    :goto_0
    return v2

    .line 369
    :cond_1
    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getCurrentSpan()F

    move-result v3

    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getPreviousSpan()F

    move-result v4

    sub-float v0, v3, v4

    .line 370
    .local v0, "span":F
    iget-object v3, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView$ScaleListener;->this$0:Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView;

    invoke-virtual {v3}, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView;->getScale()F

    move-result v3

    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getScaleFactor()F

    move-result v4

    mul-float v1, v3, v4

    .line 372
    .local v1, "targetScale":F
    iget-object v3, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView$ScaleListener;->this$0:Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView;

    iget-boolean v3, v3, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView;->mScaleEnabled:Z

    if-eqz v3, :cond_0

    .line 373
    iget-boolean v3, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView$ScaleListener;->mScaled:Z

    if-eqz v3, :cond_2

    const/4 v3, 0x0

    cmpl-float v3, v0, v3

    if-eqz v3, :cond_2

    .line 374
    iget-object v3, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView$ScaleListener;->this$0:Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView;

    iput-boolean v2, v3, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView;->mUserScaled:Z

    .line 375
    iget-object v3, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView$ScaleListener;->this$0:Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView;

    invoke-virtual {v3}, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView;->getMaxScale()F

    move-result v3

    iget-object v4, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView$ScaleListener;->this$0:Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView;

    invoke-virtual {v4}, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView;->getMinScale()F

    move-result v4

    invoke-static {v1, v4}, Ljava/lang/Math;->max(FF)F

    move-result v4

    invoke-static {v3, v4}, Ljava/lang/Math;->min(FF)F

    move-result v1

    .line 376
    iget-object v3, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView$ScaleListener;->this$0:Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView;

    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getFocusX()F

    move-result v4

    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getFocusY()F

    move-result v5

    invoke-virtual {v3, v1, v4, v5}, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView;->zoomTo(FFF)V

    .line 377
    iget-object v3, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView$ScaleListener;->this$0:Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView;

    iput v2, v3, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView;->mDoubleTapDirection:I

    .line 378
    iget-object v3, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView$ScaleListener;->this$0:Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView;

    invoke-virtual {v3}, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView;->invalidate()V

    goto :goto_0

    .line 382
    :cond_2
    iget-boolean v3, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView$ScaleListener;->mScaled:Z

    if-nez v3, :cond_0

    .line 383
    iput-boolean v2, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView$ScaleListener;->mScaled:Z

    goto :goto_0
.end method
