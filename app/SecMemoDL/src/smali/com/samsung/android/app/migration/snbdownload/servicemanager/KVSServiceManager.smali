.class public Lcom/samsung/android/app/migration/snbdownload/servicemanager/KVSServiceManager;
.super Lcom/samsung/android/app/migration/snbdownload/servicemanager/AbstractServiceManager;
.source "KVSServiceManager.java"

# interfaces
.implements Lcom/samsung/android/app/migration/snbdownload/servicemanager/IRecordServiceManager;


# instance fields
.field mKVSResponse:Lcom/samsung/android/app/migration/snbdownload/network/downsync/records/KVSResponse;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/samsung/android/app/migration/snbdownload/network/downsync/auth/AuthManager;Ljava/lang/String;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "authManager"    # Lcom/samsung/android/app/migration/snbdownload/network/downsync/auth/AuthManager;
    .param p3, "cid"    # Ljava/lang/String;

    .prologue
    .line 52
    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/android/app/migration/snbdownload/servicemanager/AbstractServiceManager;-><init>(Landroid/content/Context;Lcom/samsung/android/app/migration/snbdownload/network/downsync/auth/AuthManager;Ljava/lang/String;)V

    .line 49
    new-instance v0, Lcom/samsung/android/app/migration/snbdownload/network/downsync/records/KVSResponse;

    invoke-direct {v0}, Lcom/samsung/android/app/migration/snbdownload/network/downsync/records/KVSResponse;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/app/migration/snbdownload/servicemanager/KVSServiceManager;->mKVSResponse:Lcom/samsung/android/app/migration/snbdownload/network/downsync/records/KVSResponse;

    .line 53
    return-void
.end method


# virtual methods
.method public getItems(Ljava/util/List;)Lcom/samsung/android/app/migration/snbdownload/network/downsync/records/KVSResponse;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/samsung/android/app/migration/snbdownload/network/downsync/records/KVSResponse;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/android/app/migration/snbdownload/network/downsync/util/SyncException;
        }
    .end annotation

    .prologue
    .line 57
    .local p1, "keyIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v3, p0, Lcom/samsung/android/app/migration/snbdownload/servicemanager/KVSServiceManager;->mAuthManager:Lcom/samsung/android/app/migration/snbdownload/network/downsync/auth/AuthManager;

    invoke-virtual {v3}, Lcom/samsung/android/app/migration/snbdownload/network/downsync/auth/AuthManager;->getBaseUrl()Ljava/lang/String;

    move-result-object v1

    .line 58
    .local v1, "baseUrl":Ljava/lang/String;
    if-nez v1, :cond_0

    .line 59
    new-instance v3, Lcom/samsung/android/app/migration/snbdownload/network/downsync/util/SyncException;

    const/4 v4, 0x3

    invoke-direct {v3, v4}, Lcom/samsung/android/app/migration/snbdownload/network/downsync/util/SyncException;-><init>(I)V

    throw v3

    .line 61
    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 62
    .local v2, "url":Ljava/lang/StringBuilder;
    const-string v3, "/kvs/item?"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 63
    iget-object v3, p0, Lcom/samsung/android/app/migration/snbdownload/servicemanager/KVSServiceManager;->mAuthManager:Lcom/samsung/android/app/migration/snbdownload/network/downsync/auth/AuthManager;

    iget-object v4, p0, Lcom/samsung/android/app/migration/snbdownload/servicemanager/KVSServiceManager;->mCid:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/samsung/android/app/migration/snbdownload/network/downsync/auth/AuthManager;->getGetApiParams(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 64
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_1

    .line 68
    const/4 v3, 0x1

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/samsung/android/app/migration/snbdownload/servicemanager/KVSServiceManager;->mKVSResponse:Lcom/samsung/android/app/migration/snbdownload/network/downsync/records/KVSResponse;

    invoke-virtual {p0, v3, v4, v5}, Lcom/samsung/android/app/migration/snbdownload/servicemanager/KVSServiceManager;->handleRequest(ILjava/lang/String;Lcom/samsung/android/app/migration/snbdownload/network/downsync/records/AbstractJSON;)Lcom/samsung/android/app/migration/snbdownload/network/downsync/http/NetworkResponse;

    .line 69
    iget-object v3, p0, Lcom/samsung/android/app/migration/snbdownload/servicemanager/KVSServiceManager;->mKVSResponse:Lcom/samsung/android/app/migration/snbdownload/network/downsync/records/KVSResponse;

    return-object v3

    .line 64
    :cond_1
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 65
    .local v0, "Key":Ljava/lang/String;
    const-string v4, "&key="

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 66
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0
.end method

.method public getKeys(Ljava/lang/String;I)Lcom/samsung/android/app/migration/snbdownload/network/downsync/records/KVSResponse;
    .locals 5
    .param p1, "startKey"    # Ljava/lang/String;
    .param p2, "count"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/android/app/migration/snbdownload/network/downsync/util/SyncException;
        }
    .end annotation

    .prologue
    .line 74
    iget-object v2, p0, Lcom/samsung/android/app/migration/snbdownload/servicemanager/KVSServiceManager;->mAuthManager:Lcom/samsung/android/app/migration/snbdownload/network/downsync/auth/AuthManager;

    invoke-virtual {v2}, Lcom/samsung/android/app/migration/snbdownload/network/downsync/auth/AuthManager;->getBaseUrl()Ljava/lang/String;

    move-result-object v0

    .line 75
    .local v0, "baseUrl":Ljava/lang/String;
    if-nez v0, :cond_0

    .line 76
    new-instance v2, Lcom/samsung/android/app/migration/snbdownload/network/downsync/util/SyncException;

    const/4 v3, 0x3

    invoke-direct {v2, v3}, Lcom/samsung/android/app/migration/snbdownload/network/downsync/util/SyncException;-><init>(I)V

    throw v2

    .line 78
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 79
    .local v1, "url":Ljava/lang/StringBuilder;
    const-string v2, "/kvs/keys?"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 80
    iget-object v2, p0, Lcom/samsung/android/app/migration/snbdownload/servicemanager/KVSServiceManager;->mAuthManager:Lcom/samsung/android/app/migration/snbdownload/network/downsync/auth/AuthManager;

    iget-object v3, p0, Lcom/samsung/android/app/migration/snbdownload/servicemanager/KVSServiceManager;->mCid:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/samsung/android/app/migration/snbdownload/network/downsync/auth/AuthManager;->getGetApiParams(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 81
    const-string v2, "&start="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 82
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 83
    const-string v2, "&count="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 84
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 85
    const/4 v2, 0x1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/android/app/migration/snbdownload/servicemanager/KVSServiceManager;->mKVSResponse:Lcom/samsung/android/app/migration/snbdownload/network/downsync/records/KVSResponse;

    invoke-virtual {p0, v2, v3, v4}, Lcom/samsung/android/app/migration/snbdownload/servicemanager/KVSServiceManager;->handleRequest(ILjava/lang/String;Lcom/samsung/android/app/migration/snbdownload/network/downsync/records/AbstractJSON;)Lcom/samsung/android/app/migration/snbdownload/network/downsync/http/NetworkResponse;

    .line 86
    iget-object v2, p0, Lcom/samsung/android/app/migration/snbdownload/servicemanager/KVSServiceManager;->mKVSResponse:Lcom/samsung/android/app/migration/snbdownload/network/downsync/records/KVSResponse;

    return-object v2
.end method
