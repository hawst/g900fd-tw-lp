.class public Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback$SelectAllActionBar;
.super Ljava/lang/Object;
.source "MemoListFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "SelectAllActionBar"
.end annotation


# instance fields
.field private final mCheckBox:Landroid/widget/CheckBox;

.field private final mLayout:Landroid/widget/LinearLayout;

.field private final mTextView:Landroid/widget/TextView;

.field final synthetic this$1:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;


# direct methods
.method public constructor <init>(Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;Landroid/content/Context;Landroid/widget/LinearLayout;Landroid/widget/TextView;Landroid/widget/CheckBox;)V
    .locals 1
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "layout"    # Landroid/widget/LinearLayout;
    .param p4, "textview"    # Landroid/widget/TextView;
    .param p5, "checkBox"    # Landroid/widget/CheckBox;

    .prologue
    .line 2464
    iput-object p1, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback$SelectAllActionBar;->this$1:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2465
    iput-object p3, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback$SelectAllActionBar;->mLayout:Landroid/widget/LinearLayout;

    .line 2466
    iput-object p4, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback$SelectAllActionBar;->mTextView:Landroid/widget/TextView;

    .line 2467
    iput-object p5, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback$SelectAllActionBar;->mCheckBox:Landroid/widget/CheckBox;

    .line 2468
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback$SelectAllActionBar;->mLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2469
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 2473
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback$SelectAllActionBar;->mCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2475
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback$SelectAllActionBar;->mCheckBox:Landroid/widget/CheckBox;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 2476
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback$SelectAllActionBar;->this$1:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;

    iget-object v0, v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;->mMessageHandler:Landroid/os/Handler;

    const/16 v1, 0xc8

    const-wide/16 v2, 0xa

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 2482
    :goto_0
    return-void

    .line 2479
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback$SelectAllActionBar;->this$1:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;

    iget-object v0, v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;->mMessageHandler:Landroid/os/Handler;

    const/16 v1, 0x64

    const-wide/16 v2, 0x64

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 2480
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback$SelectAllActionBar;->mCheckBox:Landroid/widget/CheckBox;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto :goto_0
.end method

.method public setTitle(Ljava/lang/CharSequence;)V
    .locals 1
    .param p1, "title"    # Ljava/lang/CharSequence;

    .prologue
    .line 2485
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback$SelectAllActionBar;->mTextView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2486
    return-void
.end method
