.class public final Lcom/samsung/android/app/migration/snbdownload/network/downsync/records/RecordKey;
.super Lcom/samsung/android/app/migration/snbdownload/network/downsync/records/RecordBaseKeyItem;
.source "RecordKey.java"


# static fields
.field private static final SIZE:Ljava/lang/String; = "size"


# instance fields
.field mSize:J


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/Long;JZ)V
    .locals 1
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "timeStamp"    # Ljava/lang/Long;
    .param p3, "size"    # J
    .param p5, "deleted"    # Z

    .prologue
    .line 35
    invoke-direct {p0, p1, p2, p5}, Lcom/samsung/android/app/migration/snbdownload/network/downsync/records/RecordBaseKeyItem;-><init>(Ljava/lang/String;Ljava/lang/Long;Z)V

    .line 36
    iput-wide p3, p0, Lcom/samsung/android/app/migration/snbdownload/network/downsync/records/RecordKey;->mSize:J

    .line 37
    return-void
.end method


# virtual methods
.method public fromJSON(Ljava/lang/Object;)V
    .locals 4
    .param p1, "obj"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 40
    invoke-super {p0, p1}, Lcom/samsung/android/app/migration/snbdownload/network/downsync/records/RecordBaseKeyItem;->fromJSON(Ljava/lang/Object;)V

    move-object v0, p1

    .line 41
    check-cast v0, Lorg/json/JSONObject;

    .line 42
    .local v0, "json":Lorg/json/JSONObject;
    const-string v1, "size"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 43
    const-string v1, "size"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/samsung/android/app/migration/snbdownload/network/downsync/records/RecordKey;->mSize:J

    .line 44
    :cond_0
    return-void
.end method

.method public getSize()J
    .locals 2

    .prologue
    .line 31
    iget-wide v0, p0, Lcom/samsung/android/app/migration/snbdownload/network/downsync/records/RecordKey;->mSize:J

    return-wide v0
.end method

.method public toJSON()Ljava/lang/Object;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 47
    invoke-super {p0}, Lcom/samsung/android/app/migration/snbdownload/network/downsync/records/RecordBaseKeyItem;->toJSON()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/json/JSONObject;

    .line 48
    .local v0, "json":Lorg/json/JSONObject;
    const-string v1, "size"

    iget-wide v2, p0, Lcom/samsung/android/app/migration/snbdownload/network/downsync/records/RecordKey;->mSize:J

    invoke-virtual {v0, v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    .line 49
    return-object v0
.end method
