.class public Lcom/samsung/android/app/memo/share/SharePopupList;
.super Ljava/lang/Object;
.source "SharePopupList.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/app/memo/share/SharePopupList$Holder;,
        Lcom/samsung/android/app/memo/share/SharePopupList$SelectShareMenuAdapter;
    }
.end annotation


# static fields
.field private static final BAIDUCLOUD_PKG_NAME:Ljava/lang/String; = "com.baidu.netdisk_ss"

.field private static final CLOUD189_PKG_NAME:Ljava/lang/String; = "com.cn21.ecloud"

.field private static final DOCS_APP_NAME:Ljava/lang/String; = "com.google.android.apps.docs.app.SendTextToClipboardActivity"

.field private static final FLIPBRD_CHN_PKG_NAME:Ljava/lang/String; = "flipboard.cn"

.field private static final MAIL139_PKG_NAME:Ljava/lang/String; = "cn.cj.pe"

.field private static final MEMO_PKG_NAME:Ljava/lang/String; = "com.samsung.android.app.memo"

.field private static final MEMO_PKG_NAME_IN_KNOX:Ljava/lang/String; = "sec_container_1.com.samsung.android.app.memo"

.field private static final MOMENTS_PKG_NAME:Ljava/lang/String; = "im.yixin"

.field private static final QQ_PKG_NAME:Ljava/lang/String; = "com.tencent.mtt"

.field private static final WECHAT_PKG_NAME:Ljava/lang/String; = "com.tencent.mm"

.field private static final WEIBO_PKG_NAME:Ljava/lang/String; = "com.sina.weibo"


# instance fields
.field initialIntents:[Landroid/content/Intent;

.field mActivity:Landroid/app/Activity;

.field private mPm:Landroid/content/pm/PackageManager;

.field mSelectShareMenuAdapter:Lcom/samsung/android/app/memo/share/SharePopupList$SelectShareMenuAdapter;

.field mSharePopupMenu:Landroid/widget/ListPopupWindow;

.field mcontext:Landroid/content/Context;

.field public mimeType:Ljava/lang/String;

.field private rList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/content/pm/ResolveInfo;",
            ">;"
        }
    .end annotation
.end field

.field target:Landroid/content/Intent;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v5, 0x0

    .line 78
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 60
    iput-object v5, p0, Lcom/samsung/android/app/memo/share/SharePopupList;->mPm:Landroid/content/pm/PackageManager;

    .line 62
    iput-object v5, p0, Lcom/samsung/android/app/memo/share/SharePopupList;->mimeType:Ljava/lang/String;

    .line 64
    iput-object v5, p0, Lcom/samsung/android/app/memo/share/SharePopupList;->rList:Ljava/util/List;

    move-object v4, p1

    .line 80
    check-cast v4, Landroid/app/Activity;

    iput-object v4, p0, Lcom/samsung/android/app/memo/share/SharePopupList;->mActivity:Landroid/app/Activity;

    .line 81
    iput-object p1, p0, Lcom/samsung/android/app/memo/share/SharePopupList;->mcontext:Landroid/content/Context;

    .line 83
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    iput-object v4, p0, Lcom/samsung/android/app/memo/share/SharePopupList;->mPm:Landroid/content/pm/PackageManager;

    .line 84
    const-string v4, "android.intent.extra.INTENT"

    invoke-virtual {p2, v4}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v3

    .line 85
    .local v3, "targetParcelable":Landroid/os/Parcelable;
    check-cast v3, Landroid/content/Intent;

    .end local v3    # "targetParcelable":Landroid/os/Parcelable;
    iput-object v3, p0, Lcom/samsung/android/app/memo/share/SharePopupList;->target:Landroid/content/Intent;

    .line 87
    iget-object v4, p0, Lcom/samsung/android/app/memo/share/SharePopupList;->target:Landroid/content/Intent;

    invoke-virtual {v4}, Landroid/content/Intent;->getType()Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/samsung/android/app/memo/share/SharePopupList;->mimeType:Ljava/lang/String;

    .line 89
    iput-object v5, p0, Lcom/samsung/android/app/memo/share/SharePopupList;->initialIntents:[Landroid/content/Intent;

    .line 90
    const-string v4, "android.intent.extra.INITIAL_INTENTS"

    invoke-virtual {p2, v4}, Landroid/content/Intent;->getParcelableArrayExtra(Ljava/lang/String;)[Landroid/os/Parcelable;

    move-result-object v1

    .line 91
    .local v1, "pa":[Landroid/os/Parcelable;
    if-eqz v1, :cond_0

    .line 92
    array-length v4, v1

    new-array v4, v4, [Landroid/content/Intent;

    iput-object v4, p0, Lcom/samsung/android/app/memo/share/SharePopupList;->initialIntents:[Landroid/content/Intent;

    .line 93
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v4, v1

    if-lt v0, v4, :cond_1

    .line 98
    .end local v0    # "i":I
    :cond_0
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, p0, Lcom/samsung/android/app/memo/share/SharePopupList;->rList:Ljava/util/List;

    .line 99
    iget-object v4, p0, Lcom/samsung/android/app/memo/share/SharePopupList;->target:Landroid/content/Intent;

    iget-object v5, p0, Lcom/samsung/android/app/memo/share/SharePopupList;->rList:Ljava/util/List;

    const/4 v6, 0x1

    invoke-virtual {p0, v4, v5, v6}, Lcom/samsung/android/app/memo/share/SharePopupList;->filterResolveList(Landroid/content/Intent;Ljava/util/List;Z)V

    .line 101
    const/4 v0, 0x0

    .restart local v0    # "i":I
    :goto_1
    iget-object v4, p0, Lcom/samsung/android/app/memo/share/SharePopupList;->initialIntents:[Landroid/content/Intent;

    array-length v4, v4

    if-lt v0, v4, :cond_2

    .line 107
    iget-object v4, p0, Lcom/samsung/android/app/memo/share/SharePopupList;->rList:Ljava/util/List;

    new-instance v5, Lcom/samsung/android/app/memo/share/SharePopupList$1;

    invoke-direct {v5, p0}, Lcom/samsung/android/app/memo/share/SharePopupList$1;-><init>(Lcom/samsung/android/app/memo/share/SharePopupList;)V

    invoke-static {v4, v5}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 114
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/share/SharePopupList;->openPopupList()V

    .line 115
    return-void

    .line 94
    :cond_1
    iget-object v5, p0, Lcom/samsung/android/app/memo/share/SharePopupList;->initialIntents:[Landroid/content/Intent;

    aget-object v4, v1, v0

    check-cast v4, Landroid/content/Intent;

    aput-object v4, v5, v0

    .line 93
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 102
    :cond_2
    iget-object v4, p0, Lcom/samsung/android/app/memo/share/SharePopupList;->mPm:Landroid/content/pm/PackageManager;

    iget-object v5, p0, Lcom/samsung/android/app/memo/share/SharePopupList;->initialIntents:[Landroid/content/Intent;

    aget-object v5, v5, v0

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-nez v5, :cond_3

    .line 101
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 102
    :cond_3
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/pm/ResolveInfo;

    .line 103
    .local v2, "ri":Landroid/content/pm/ResolveInfo;
    iget-object v5, p0, Lcom/samsung/android/app/memo/share/SharePopupList;->rList:Ljava/util/List;

    invoke-interface {v5, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2
.end method

.method static synthetic access$0(Lcom/samsung/android/app/memo/share/SharePopupList;)Landroid/content/pm/PackageManager;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/samsung/android/app/memo/share/SharePopupList;->mPm:Landroid/content/pm/PackageManager;

    return-object v0
.end method

.method static synthetic access$1(Lcom/samsung/android/app/memo/share/SharePopupList;)Ljava/util/List;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/samsung/android/app/memo/share/SharePopupList;->rList:Ljava/util/List;

    return-object v0
.end method


# virtual methods
.method public filterResolveList(Landroid/content/Intent;Ljava/util/List;Z)V
    .locals 4
    .param p1, "intent"    # Landroid/content/Intent;
    .param p3, "skipKnownPkg"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Intent;",
            "Ljava/util/List",
            "<",
            "Landroid/content/pm/ResolveInfo;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 173
    .local p2, "list":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    iget-object v1, p0, Lcom/samsung/android/app/memo/share/SharePopupList;->mPm:Landroid/content/pm/PackageManager;

    iget-object v2, p0, Lcom/samsung/android/app/memo/share/SharePopupList;->target:Landroid/content/Intent;

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_1

    .line 202
    return-void

    .line 173
    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/ResolveInfo;

    .line 174
    .local v0, "ri":Landroid/content/pm/ResolveInfo;
    const-string v2, "com.samsung.android.app.memo"

    iget-object v3, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v3, v3, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 175
    const-string v2, "sec_container_1.com.samsung.android.app.memo"

    iget-object v3, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v3, v3, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 177
    if-eqz p3, :cond_2

    .line 178
    const-string v2, "com.google.android.apps.docs.app.SendTextToClipboardActivity"

    iget-object v3, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v3, v3, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 179
    const-string v2, "com.android.bluetooth"

    iget-object v3, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v3, v3, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 180
    const-string v2, "com.android.bluetooth.opp.BluetoothOppLauncherActivity"

    iget-object v3, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v3, v3, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "com.samsung.android.app.FileShareClient"

    .line 181
    iget-object v3, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v3, v3, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 184
    :cond_2
    iget-object v2, p0, Lcom/samsung/android/app/memo/share/SharePopupList;->target:Landroid/content/Intent;

    invoke-virtual {v2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    const-string v3, "android.intent.action.SEND_MULTIPLE"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 185
    const-string v2, "com.tencent.mm"

    iget-object v3, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v3, v3, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 186
    const-string v2, "im.yixin"

    iget-object v3, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v3, v3, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 187
    const-string v2, "cn.cj.pe"

    iget-object v3, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v3, v3, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 188
    const-string v2, "com.cn21.ecloud"

    iget-object v3, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v3, v3, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 189
    const-string v2, "flipboard.cn"

    iget-object v3, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v3, v3, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 190
    const-string v2, "com.baidu.netdisk_ss"

    iget-object v3, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v3, v3, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 191
    const-string v2, "com.tencent.mtt"

    iget-object v3, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v3, v3, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 192
    const-string v2, "com.sina.weibo"

    iget-object v3, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v3, v3, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 193
    const-string v2, "com.facebook.katana"

    iget-object v3, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v3, v3, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 194
    const-string v2, "com.samsung.android.sconnect"

    iget-object v3, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v3, v3, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 196
    :cond_3
    iget-object v2, p0, Lcom/samsung/android/app/memo/share/SharePopupList;->target:Landroid/content/Intent;

    invoke-virtual {v2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    const-string v3, "android.intent.action.SEND"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 197
    const-string v2, "com.google.android.apps.docs"

    iget-object v3, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v3, v3, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 200
    :cond_4
    invoke-interface {p2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0
.end method

.method public openPopupList()V
    .locals 4

    .prologue
    .line 119
    const/4 v0, 0x0

    .line 120
    .local v0, "shareMenuView":Landroid/view/View;
    new-instance v1, Landroid/widget/ListPopupWindow;

    iget-object v2, p0, Lcom/samsung/android/app/memo/share/SharePopupList;->mActivity:Landroid/app/Activity;

    invoke-direct {v1, v2}, Landroid/widget/ListPopupWindow;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/samsung/android/app/memo/share/SharePopupList;->mSharePopupMenu:Landroid/widget/ListPopupWindow;

    .line 122
    iget-object v1, p0, Lcom/samsung/android/app/memo/share/SharePopupList;->mSharePopupMenu:Landroid/widget/ListPopupWindow;

    const/16 v2, 0x190

    invoke-virtual {v1, v2}, Landroid/widget/ListPopupWindow;->setWidth(I)V

    .line 123
    iget-object v1, p0, Lcom/samsung/android/app/memo/share/SharePopupList;->mSharePopupMenu:Landroid/widget/ListPopupWindow;

    const/16 v2, 0x3e8

    invoke-virtual {v1, v2}, Landroid/widget/ListPopupWindow;->setHeight(I)V

    .line 125
    iget-object v1, p0, Lcom/samsung/android/app/memo/share/SharePopupList;->mActivity:Landroid/app/Activity;

    instance-of v1, v1, Lcom/samsung/android/app/memo/Main;

    if-eqz v1, :cond_1

    .line 126
    iget-object v1, p0, Lcom/samsung/android/app/memo/share/SharePopupList;->mActivity:Landroid/app/Activity;

    const v2, 0x7f0e00a0

    invoke-virtual {v1, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 130
    :goto_0
    if-eqz v0, :cond_0

    .line 131
    iget-object v1, p0, Lcom/samsung/android/app/memo/share/SharePopupList;->mSharePopupMenu:Landroid/widget/ListPopupWindow;

    invoke-virtual {v1, v0}, Landroid/widget/ListPopupWindow;->setAnchorView(Landroid/view/View;)V

    .line 133
    iget-object v1, p0, Lcom/samsung/android/app/memo/share/SharePopupList;->mSharePopupMenu:Landroid/widget/ListPopupWindow;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/widget/ListPopupWindow;->setModal(Z)V

    .line 135
    new-instance v1, Lcom/samsung/android/app/memo/share/SharePopupList$SelectShareMenuAdapter;

    iget-object v2, p0, Lcom/samsung/android/app/memo/share/SharePopupList;->mActivity:Landroid/app/Activity;

    .line 136
    const v3, 0x7f040028

    invoke-direct {v1, p0, v2, v3}, Lcom/samsung/android/app/memo/share/SharePopupList$SelectShareMenuAdapter;-><init>(Lcom/samsung/android/app/memo/share/SharePopupList;Landroid/content/Context;I)V

    .line 135
    iput-object v1, p0, Lcom/samsung/android/app/memo/share/SharePopupList;->mSelectShareMenuAdapter:Lcom/samsung/android/app/memo/share/SharePopupList$SelectShareMenuAdapter;

    .line 138
    iget-object v1, p0, Lcom/samsung/android/app/memo/share/SharePopupList;->mSelectShareMenuAdapter:Lcom/samsung/android/app/memo/share/SharePopupList$SelectShareMenuAdapter;

    iget-object v2, p0, Lcom/samsung/android/app/memo/share/SharePopupList;->rList:Ljava/util/List;

    invoke-virtual {v1, v2}, Lcom/samsung/android/app/memo/share/SharePopupList$SelectShareMenuAdapter;->setAppList(Ljava/util/List;)V

    .line 140
    iget-object v1, p0, Lcom/samsung/android/app/memo/share/SharePopupList;->mSharePopupMenu:Landroid/widget/ListPopupWindow;

    iget-object v2, p0, Lcom/samsung/android/app/memo/share/SharePopupList;->mSelectShareMenuAdapter:Lcom/samsung/android/app/memo/share/SharePopupList$SelectShareMenuAdapter;

    invoke-virtual {v1, v2}, Landroid/widget/ListPopupWindow;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 142
    iget-object v1, p0, Lcom/samsung/android/app/memo/share/SharePopupList;->mSharePopupMenu:Landroid/widget/ListPopupWindow;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/ListPopupWindow;->setVerticalOffset(I)V

    .line 146
    iget-object v1, p0, Lcom/samsung/android/app/memo/share/SharePopupList;->mSharePopupMenu:Landroid/widget/ListPopupWindow;

    new-instance v2, Lcom/samsung/android/app/memo/share/SharePopupList$2;

    invoke-direct {v2, p0}, Lcom/samsung/android/app/memo/share/SharePopupList$2;-><init>(Lcom/samsung/android/app/memo/share/SharePopupList;)V

    invoke-virtual {v1, v2}, Landroid/widget/ListPopupWindow;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 167
    iget-object v1, p0, Lcom/samsung/android/app/memo/share/SharePopupList;->mSharePopupMenu:Landroid/widget/ListPopupWindow;

    invoke-virtual {v1}, Landroid/widget/ListPopupWindow;->show()V

    .line 169
    :cond_0
    return-void

    .line 128
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/app/memo/share/SharePopupList;->mActivity:Landroid/app/Activity;

    const v2, 0x7f0e0095

    invoke-virtual {v1, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    goto :goto_0
.end method
