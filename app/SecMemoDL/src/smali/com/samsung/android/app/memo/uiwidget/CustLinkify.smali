.class public Lcom/samsung/android/app/memo/uiwidget/CustLinkify;
.super Ljava/lang/Object;
.source "CustLinkify.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/app/memo/uiwidget/CustLinkify$MatchFilter;,
        Lcom/samsung/android/app/memo/uiwidget/CustLinkify$TransformFilter;
    }
.end annotation


# static fields
.field public static final ALL:I = 0xf

.field public static final ALL_CHN:I = 0x7

.field public static final EMAIL_ADDRESSES:I = 0x2

.field public static final MAP_ADDRESSES:I = 0x8

.field public static final PHONE_NUMBERS:I = 0x4

.field private static final PHONE_NUMBER_MINIMUM_DIGITS:I = 0x5

.field public static final WEB_URLS:I = 0x1

.field public static final sPhoneNumberMatchFilter:Lcom/samsung/android/app/memo/uiwidget/CustLinkify$MatchFilter;

.field public static final sPhoneNumberTransformFilter:Lcom/samsung/android/app/memo/uiwidget/CustLinkify$TransformFilter;

.field public static final sUrlMatchFilter:Lcom/samsung/android/app/memo/uiwidget/CustLinkify$MatchFilter;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 94
    new-instance v0, Lcom/samsung/android/app/memo/uiwidget/CustLinkify$1;

    invoke-direct {v0}, Lcom/samsung/android/app/memo/uiwidget/CustLinkify$1;-><init>()V

    sput-object v0, Lcom/samsung/android/app/memo/uiwidget/CustLinkify;->sUrlMatchFilter:Lcom/samsung/android/app/memo/uiwidget/CustLinkify$MatchFilter;

    .line 112
    new-instance v0, Lcom/samsung/android/app/memo/uiwidget/CustLinkify$2;

    invoke-direct {v0}, Lcom/samsung/android/app/memo/uiwidget/CustLinkify$2;-><init>()V

    sput-object v0, Lcom/samsung/android/app/memo/uiwidget/CustLinkify;->sPhoneNumberMatchFilter:Lcom/samsung/android/app/memo/uiwidget/CustLinkify$MatchFilter;

    .line 134
    new-instance v0, Lcom/samsung/android/app/memo/uiwidget/CustLinkify$3;

    invoke-direct {v0}, Lcom/samsung/android/app/memo/uiwidget/CustLinkify$3;-><init>()V

    sput-object v0, Lcom/samsung/android/app/memo/uiwidget/CustLinkify;->sPhoneNumberTransformFilter:Lcom/samsung/android/app/memo/uiwidget/CustLinkify$TransformFilter;

    .line 138
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static final addLinkMovementMethod(Landroid/widget/TextView;)V
    .locals 2
    .param p0, "t"    # Landroid/widget/TextView;

    .prologue
    .line 269
    invoke-virtual {p0}, Landroid/widget/TextView;->getMovementMethod()Landroid/text/method/MovementMethod;

    move-result-object v0

    .line 271
    .local v0, "m":Landroid/text/method/MovementMethod;
    if-eqz v0, :cond_0

    instance-of v1, v0, Landroid/text/method/LinkMovementMethod;

    if-nez v1, :cond_1

    .line 272
    :cond_0
    invoke-virtual {p0}, Landroid/widget/TextView;->getLinksClickable()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 273
    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v1

    invoke-virtual {p0, v1}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 276
    :cond_1
    return-void
.end method

.method public static final addLinks(Landroid/widget/TextView;Ljava/util/regex/Pattern;Ljava/lang/String;)V
    .locals 1
    .param p0, "text"    # Landroid/widget/TextView;
    .param p1, "pattern"    # Ljava/util/regex/Pattern;
    .param p2, "scheme"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 291
    invoke-static {p0, p1, p2, v0, v0}, Lcom/samsung/android/app/memo/uiwidget/CustLinkify;->addLinks(Landroid/widget/TextView;Ljava/util/regex/Pattern;Ljava/lang/String;Lcom/samsung/android/app/memo/uiwidget/CustLinkify$MatchFilter;Lcom/samsung/android/app/memo/uiwidget/CustLinkify$TransformFilter;)V

    .line 292
    return-void
.end method

.method public static final addLinks(Landroid/widget/TextView;Ljava/util/regex/Pattern;Ljava/lang/String;Lcom/samsung/android/app/memo/uiwidget/CustLinkify$MatchFilter;Lcom/samsung/android/app/memo/uiwidget/CustLinkify$TransformFilter;)V
    .locals 2
    .param p0, "text"    # Landroid/widget/TextView;
    .param p1, "p"    # Ljava/util/regex/Pattern;
    .param p2, "scheme"    # Ljava/lang/String;
    .param p3, "matchFilter"    # Lcom/samsung/android/app/memo/uiwidget/CustLinkify$MatchFilter;
    .param p4, "transformFilter"    # Lcom/samsung/android/app/memo/uiwidget/CustLinkify$TransformFilter;

    .prologue
    .line 311
    invoke-virtual {p0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-static {v1}, Landroid/text/SpannableString;->valueOf(Ljava/lang/CharSequence;)Landroid/text/SpannableString;

    move-result-object v0

    .line 313
    .local v0, "s":Landroid/text/SpannableString;
    invoke-static {v0, p1, p2, p3, p4}, Lcom/samsung/android/app/memo/uiwidget/CustLinkify;->addLinks(Landroid/text/Spannable;Ljava/util/regex/Pattern;Ljava/lang/String;Lcom/samsung/android/app/memo/uiwidget/CustLinkify$MatchFilter;Lcom/samsung/android/app/memo/uiwidget/CustLinkify$TransformFilter;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 314
    invoke-virtual {p0, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 315
    invoke-static {p0}, Lcom/samsung/android/app/memo/uiwidget/CustLinkify;->addLinkMovementMethod(Landroid/widget/TextView;)V

    .line 317
    :cond_0
    return-void
.end method

.method public static final addLinks(Landroid/text/Spannable;I)Z
    .locals 11
    .param p0, "text"    # Landroid/text/Spannable;
    .param p1, "mask"    # I

    .prologue
    const/4 v5, 0x0

    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 189
    if-nez p1, :cond_0

    move v1, v9

    .line 233
    :goto_0
    return v1

    .line 193
    :cond_0
    invoke-interface {p0}, Landroid/text/Spannable;->length()I

    move-result v1

    const-class v2, Landroid/text/style/URLSpan;

    invoke-interface {p0, v9, v1, v2}, Landroid/text/Spannable;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v8

    check-cast v8, [Landroid/text/style/URLSpan;

    .line 195
    .local v8, "old":[Landroid/text/style/URLSpan;
    array-length v1, v8

    add-int/lit8 v6, v1, -0x1

    .local v6, "i":I
    :goto_1
    if-gez v6, :cond_5

    .line 199
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 201
    .local v0, "links":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/app/memo/uiwidget/MemoLinkSpec;>;"
    and-int/lit8 v1, p1, 0x1

    if-eqz v1, :cond_1

    .line 202
    sget-object v2, Landroid/util/Patterns;->WEB_URL:Ljava/util/regex/Pattern;

    const/4 v1, 0x3

    new-array v3, v1, [Ljava/lang/String;

    .line 203
    const-string v1, "http://"

    aput-object v1, v3, v9

    const-string v1, "https://"

    aput-object v1, v3, v10

    const/4 v1, 0x2

    const-string v4, "rtsp://"

    aput-object v4, v3, v1

    .line 204
    sget-object v4, Lcom/samsung/android/app/memo/uiwidget/CustLinkify;->sUrlMatchFilter:Lcom/samsung/android/app/memo/uiwidget/CustLinkify$MatchFilter;

    move-object v1, p0

    .line 202
    invoke-static/range {v0 .. v5}, Lcom/samsung/android/app/memo/uiwidget/CustLinkify;->gatherLinks(Ljava/util/ArrayList;Landroid/text/Spannable;Ljava/util/regex/Pattern;[Ljava/lang/String;Lcom/samsung/android/app/memo/uiwidget/CustLinkify$MatchFilter;Lcom/samsung/android/app/memo/uiwidget/CustLinkify$TransformFilter;)V

    .line 207
    :cond_1
    and-int/lit8 v1, p1, 0x2

    if-eqz v1, :cond_2

    .line 208
    sget-object v2, Landroid/util/Patterns;->EMAIL_ADDRESS:Ljava/util/regex/Pattern;

    new-array v3, v10, [Ljava/lang/String;

    .line 209
    const-string v1, "mailto:"

    aput-object v1, v3, v9

    move-object v1, p0

    move-object v4, v5

    .line 208
    invoke-static/range {v0 .. v5}, Lcom/samsung/android/app/memo/uiwidget/CustLinkify;->gatherLinks(Ljava/util/ArrayList;Landroid/text/Spannable;Ljava/util/regex/Pattern;[Ljava/lang/String;Lcom/samsung/android/app/memo/uiwidget/CustLinkify$MatchFilter;Lcom/samsung/android/app/memo/uiwidget/CustLinkify$TransformFilter;)V

    .line 213
    :cond_2
    and-int/lit8 v1, p1, 0x4

    if-eqz v1, :cond_3

    .line 214
    sget-object v2, Landroid/util/Patterns;->PHONE:Ljava/util/regex/Pattern;

    new-array v3, v10, [Ljava/lang/String;

    .line 215
    const-string v1, "tel:"

    aput-object v1, v3, v9

    .line 216
    sget-object v4, Lcom/samsung/android/app/memo/uiwidget/CustLinkify;->sPhoneNumberMatchFilter:Lcom/samsung/android/app/memo/uiwidget/CustLinkify$MatchFilter;

    sget-object v5, Lcom/samsung/android/app/memo/uiwidget/CustLinkify;->sPhoneNumberTransformFilter:Lcom/samsung/android/app/memo/uiwidget/CustLinkify$TransformFilter;

    move-object v1, p0

    .line 214
    invoke-static/range {v0 .. v5}, Lcom/samsung/android/app/memo/uiwidget/CustLinkify;->gatherLinks(Ljava/util/ArrayList;Landroid/text/Spannable;Ljava/util/regex/Pattern;[Ljava/lang/String;Lcom/samsung/android/app/memo/uiwidget/CustLinkify$MatchFilter;Lcom/samsung/android/app/memo/uiwidget/CustLinkify$TransformFilter;)V

    .line 219
    :cond_3
    and-int/lit8 v1, p1, 0x8

    if-eqz v1, :cond_4

    .line 220
    invoke-static {v0, p0}, Lcom/samsung/android/app/memo/uiwidget/CustLinkify;->gatherMapLinks(Ljava/util/ArrayList;Landroid/text/Spannable;)V

    .line 223
    :cond_4
    invoke-static {v0}, Lcom/samsung/android/app/memo/uiwidget/CustLinkify;->pruneOverlaps(Ljava/util/ArrayList;)V

    .line 225
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-nez v1, :cond_6

    move v1, v9

    .line 226
    goto :goto_0

    .line 196
    .end local v0    # "links":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/app/memo/uiwidget/MemoLinkSpec;>;"
    :cond_5
    aget-object v1, v8, v6

    invoke-interface {p0, v1}, Landroid/text/Spannable;->removeSpan(Ljava/lang/Object;)V

    .line 195
    add-int/lit8 v6, v6, -0x1

    goto :goto_1

    .line 229
    .restart local v0    # "links":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/app/memo/uiwidget/MemoLinkSpec;>;"
    :cond_6
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_7

    move v1, v10

    .line 233
    goto :goto_0

    .line 229
    :cond_7
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/samsung/android/app/memo/uiwidget/MemoLinkSpec;

    .line 230
    .local v7, "link":Lcom/samsung/android/app/memo/uiwidget/MemoLinkSpec;
    iget-object v2, v7, Lcom/samsung/android/app/memo/uiwidget/MemoLinkSpec;->url:Ljava/lang/String;

    iget v3, v7, Lcom/samsung/android/app/memo/uiwidget/MemoLinkSpec;->start:I

    iget v4, v7, Lcom/samsung/android/app/memo/uiwidget/MemoLinkSpec;->end:I

    invoke-static {v2, v3, v4, p0}, Lcom/samsung/android/app/memo/uiwidget/CustLinkify;->applyLink(Ljava/lang/String;IILandroid/text/Spannable;)V

    goto :goto_2
.end method

.method public static final addLinks(Landroid/text/Spannable;Ljava/util/regex/Pattern;Ljava/lang/String;)Z
    .locals 1
    .param p0, "text"    # Landroid/text/Spannable;
    .param p1, "pattern"    # Ljava/util/regex/Pattern;
    .param p2, "scheme"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 329
    invoke-static {p0, p1, p2, v0, v0}, Lcom/samsung/android/app/memo/uiwidget/CustLinkify;->addLinks(Landroid/text/Spannable;Ljava/util/regex/Pattern;Ljava/lang/String;Lcom/samsung/android/app/memo/uiwidget/CustLinkify$MatchFilter;Lcom/samsung/android/app/memo/uiwidget/CustLinkify$TransformFilter;)Z

    move-result v0

    return v0
.end method

.method public static final addLinks(Landroid/text/Spannable;Ljava/util/regex/Pattern;Ljava/lang/String;Lcom/samsung/android/app/memo/uiwidget/CustLinkify$MatchFilter;Lcom/samsung/android/app/memo/uiwidget/CustLinkify$TransformFilter;)Z
    .locals 10
    .param p0, "s"    # Landroid/text/Spannable;
    .param p1, "p"    # Ljava/util/regex/Pattern;
    .param p2, "scheme"    # Ljava/lang/String;
    .param p3, "matchFilter"    # Lcom/samsung/android/app/memo/uiwidget/CustLinkify$MatchFilter;
    .param p4, "transformFilter"    # Lcom/samsung/android/app/memo/uiwidget/CustLinkify$TransformFilter;

    .prologue
    const/4 v9, 0x0

    .line 346
    const/4 v2, 0x0

    .line 347
    .local v2, "hasMatches":Z
    if-nez p2, :cond_1

    const-string v4, ""

    .line 348
    .local v4, "prefix":Ljava/lang/String;
    :goto_0
    invoke-virtual {p1, p0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v3

    .line 350
    .local v3, "m":Ljava/util/regex/Matcher;
    :cond_0
    :goto_1
    invoke-virtual {v3}, Ljava/util/regex/Matcher;->find()Z

    move-result v7

    if-nez v7, :cond_2

    .line 369
    return v2

    .line 347
    .end local v3    # "m":Ljava/util/regex/Matcher;
    .end local v4    # "prefix":Ljava/lang/String;
    :cond_1
    sget-object v7, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {p2, v7}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v4

    goto :goto_0

    .line 351
    .restart local v3    # "m":Ljava/util/regex/Matcher;
    .restart local v4    # "prefix":Ljava/lang/String;
    :cond_2
    invoke-virtual {v3}, Ljava/util/regex/Matcher;->start()I

    move-result v5

    .line 352
    .local v5, "start":I
    invoke-virtual {v3}, Ljava/util/regex/Matcher;->end()I

    move-result v1

    .line 353
    .local v1, "end":I
    const/4 v0, 0x1

    .line 355
    .local v0, "allowed":Z
    if-eqz p3, :cond_3

    .line 356
    invoke-interface {p3, p0, v5, v1}, Lcom/samsung/android/app/memo/uiwidget/CustLinkify$MatchFilter;->acceptMatch(Ljava/lang/CharSequence;II)Z

    move-result v0

    .line 359
    :cond_3
    if-eqz v0, :cond_0

    .line 360
    invoke-virtual {v3, v9}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/String;

    .line 361
    aput-object v4, v8, v9

    .line 360
    invoke-static {v7, v8, v3, p4}, Lcom/samsung/android/app/memo/uiwidget/CustLinkify;->makeUrl(Ljava/lang/String;[Ljava/lang/String;Ljava/util/regex/Matcher;Lcom/samsung/android/app/memo/uiwidget/CustLinkify$TransformFilter;)Ljava/lang/String;

    move-result-object v6

    .line 364
    .local v6, "url":Ljava/lang/String;
    invoke-static {v6, v5, v1, p0}, Lcom/samsung/android/app/memo/uiwidget/CustLinkify;->applyLink(Ljava/lang/String;IILandroid/text/Spannable;)V

    .line 365
    const/4 v2, 0x1

    goto :goto_1
.end method

.method public static final addLinks(Landroid/widget/TextView;I)Z
    .locals 5
    .param p0, "text"    # Landroid/widget/TextView;
    .param p1, "mask"    # I

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 242
    if-nez p1, :cond_1

    .line 264
    :cond_0
    :goto_0
    return v2

    .line 246
    :cond_1
    invoke-virtual {p0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    .line 248
    .local v1, "t":Ljava/lang/CharSequence;
    instance-of v4, v1, Landroid/text/Spannable;

    if-eqz v4, :cond_2

    .line 249
    check-cast v1, Landroid/text/Spannable;

    .end local v1    # "t":Ljava/lang/CharSequence;
    invoke-static {v1, p1}, Lcom/samsung/android/app/memo/uiwidget/CustLinkify;->addLinks(Landroid/text/Spannable;I)Z

    move-result v4

    if-eqz v4, :cond_0

    move v2, v3

    .line 251
    goto :goto_0

    .line 256
    .restart local v1    # "t":Ljava/lang/CharSequence;
    :cond_2
    invoke-static {v1}, Landroid/text/SpannableString;->valueOf(Ljava/lang/CharSequence;)Landroid/text/SpannableString;

    move-result-object v0

    .line 257
    .local v0, "s":Landroid/text/SpannableString;
    invoke-static {v0, p1}, Lcom/samsung/android/app/memo/uiwidget/CustLinkify;->addLinks(Landroid/text/Spannable;I)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 259
    invoke-virtual {p0, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    move v2, v3

    .line 261
    goto :goto_0
.end method

.method private static final applyLink(Ljava/lang/String;IILandroid/text/Spannable;)V
    .locals 2
    .param p0, "url"    # Ljava/lang/String;
    .param p1, "start"    # I
    .param p2, "end"    # I
    .param p3, "text"    # Landroid/text/Spannable;

    .prologue
    .line 373
    new-instance v0, Lcom/samsung/android/app/memo/uiwidget/URLSpanNoUnderline;

    invoke-direct {v0, p0}, Lcom/samsung/android/app/memo/uiwidget/URLSpanNoUnderline;-><init>(Ljava/lang/String;)V

    .line 374
    .local v0, "span":Lcom/samsung/android/app/memo/uiwidget/URLSpanNoUnderline;
    const/16 v1, 0x21

    invoke-interface {p3, v0, p1, p2, v1}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    .line 375
    return-void
.end method

.method private static final gatherLinks(Ljava/util/ArrayList;Landroid/text/Spannable;Ljava/util/regex/Pattern;[Ljava/lang/String;Lcom/samsung/android/app/memo/uiwidget/CustLinkify$MatchFilter;Lcom/samsung/android/app/memo/uiwidget/CustLinkify$TransformFilter;)V
    .locals 6
    .param p1, "s"    # Landroid/text/Spannable;
    .param p2, "pattern"    # Ljava/util/regex/Pattern;
    .param p3, "schemes"    # [Ljava/lang/String;
    .param p4, "matchFilter"    # Lcom/samsung/android/app/memo/uiwidget/CustLinkify$MatchFilter;
    .param p5, "transformFilter"    # Lcom/samsung/android/app/memo/uiwidget/CustLinkify$TransformFilter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/app/memo/uiwidget/MemoLinkSpec;",
            ">;",
            "Landroid/text/Spannable;",
            "Ljava/util/regex/Pattern;",
            "[",
            "Ljava/lang/String;",
            "Lcom/samsung/android/app/memo/uiwidget/CustLinkify$MatchFilter;",
            "Lcom/samsung/android/app/memo/uiwidget/CustLinkify$TransformFilter;",
            ")V"
        }
    .end annotation

    .prologue
    .line 408
    .local p0, "links":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/app/memo/uiwidget/MemoLinkSpec;>;"
    invoke-virtual {p2, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v1

    .line 410
    .local v1, "m":Ljava/util/regex/Matcher;
    :cond_0
    :goto_0
    invoke-virtual {v1}, Ljava/util/regex/Matcher;->find()Z

    move-result v5

    if-nez v5, :cond_1

    .line 425
    return-void

    .line 411
    :cond_1
    invoke-virtual {v1}, Ljava/util/regex/Matcher;->start()I

    move-result v3

    .line 412
    .local v3, "start":I
    invoke-virtual {v1}, Ljava/util/regex/Matcher;->end()I

    move-result v0

    .line 414
    .local v0, "end":I
    if-eqz p4, :cond_2

    invoke-interface {p4, p1, v3, v0}, Lcom/samsung/android/app/memo/uiwidget/CustLinkify$MatchFilter;->acceptMatch(Ljava/lang/CharSequence;II)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 415
    :cond_2
    new-instance v2, Lcom/samsung/android/app/memo/uiwidget/MemoLinkSpec;

    invoke-direct {v2}, Lcom/samsung/android/app/memo/uiwidget/MemoLinkSpec;-><init>()V

    .line 416
    .local v2, "spec":Lcom/samsung/android/app/memo/uiwidget/MemoLinkSpec;
    const/4 v5, 0x0

    invoke-virtual {v1, v5}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5, p3, v1, p5}, Lcom/samsung/android/app/memo/uiwidget/CustLinkify;->makeUrl(Ljava/lang/String;[Ljava/lang/String;Ljava/util/regex/Matcher;Lcom/samsung/android/app/memo/uiwidget/CustLinkify$TransformFilter;)Ljava/lang/String;

    move-result-object v4

    .line 418
    .local v4, "url":Ljava/lang/String;
    iput-object v4, v2, Lcom/samsung/android/app/memo/uiwidget/MemoLinkSpec;->url:Ljava/lang/String;

    .line 419
    iput v3, v2, Lcom/samsung/android/app/memo/uiwidget/MemoLinkSpec;->start:I

    .line 420
    iput v0, v2, Lcom/samsung/android/app/memo/uiwidget/MemoLinkSpec;->end:I

    .line 422
    invoke-virtual {p0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method private static final gatherMapLinks(Ljava/util/ArrayList;Landroid/text/Spannable;)V
    .locals 11
    .param p1, "s"    # Landroid/text/Spannable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/app/memo/uiwidget/MemoLinkSpec;",
            ">;",
            "Landroid/text/Spannable;",
            ")V"
        }
    .end annotation

    .prologue
    .line 428
    .local p0, "links":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/app/memo/uiwidget/MemoLinkSpec;>;"
    invoke-interface {p1}, Landroid/text/Spannable;->toString()Ljava/lang/String;

    move-result-object v8

    .line 430
    .local v8, "string":Ljava/lang/String;
    const/4 v1, 0x0

    .line 432
    .local v1, "base":I
    :goto_0
    invoke-static {v8}, Landroid/webkit/WebView;->findAddress(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .local v0, "address":Ljava/lang/String;
    if-nez v0, :cond_1

    .line 459
    :cond_0
    return-void

    .line 433
    :cond_1
    invoke-virtual {v8, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v7

    .line 435
    .local v7, "start":I
    if-ltz v7, :cond_0

    .line 439
    new-instance v6, Lcom/samsung/android/app/memo/uiwidget/MemoLinkSpec;

    invoke-direct {v6}, Lcom/samsung/android/app/memo/uiwidget/MemoLinkSpec;-><init>()V

    .line 440
    .local v6, "spec":Lcom/samsung/android/app/memo/uiwidget/MemoLinkSpec;
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v5

    .line 441
    .local v5, "length":I
    add-int v4, v7, v5

    .line 443
    .local v4, "end":I
    add-int v9, v1, v7

    iput v9, v6, Lcom/samsung/android/app/memo/uiwidget/MemoLinkSpec;->start:I

    .line 444
    add-int v9, v1, v4

    iput v9, v6, Lcom/samsung/android/app/memo/uiwidget/MemoLinkSpec;->end:I

    .line 445
    invoke-virtual {v8, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v8

    .line 446
    add-int/2addr v1, v4

    .line 448
    const/4 v3, 0x0

    .line 451
    .local v3, "encodedAddress":Ljava/lang/String;
    :try_start_0
    const-string v9, "UTF-8"

    invoke-static {v0, v9}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    .line 456
    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "geo:0,0?q="

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    iput-object v9, v6, Lcom/samsung/android/app/memo/uiwidget/MemoLinkSpec;->url:Ljava/lang/String;

    .line 457
    invoke-virtual {p0, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 452
    :catch_0
    move-exception v2

    .line 453
    .local v2, "e":Ljava/io/UnsupportedEncodingException;
    goto :goto_0
.end method

.method private static final makeUrl(Ljava/lang/String;[Ljava/lang/String;Ljava/util/regex/Matcher;Lcom/samsung/android/app/memo/uiwidget/CustLinkify$TransformFilter;)Ljava/lang/String;
    .locals 9
    .param p0, "url"    # Ljava/lang/String;
    .param p1, "prefixes"    # [Ljava/lang/String;
    .param p2, "m"    # Ljava/util/regex/Matcher;
    .param p3, "filter"    # Lcom/samsung/android/app/memo/uiwidget/CustLinkify$TransformFilter;

    .prologue
    const/4 v2, 0x0

    .line 379
    if-eqz p3, :cond_0

    .line 380
    invoke-interface {p3, p2, p0}, Lcom/samsung/android/app/memo/uiwidget/CustLinkify$TransformFilter;->transformUrl(Ljava/util/regex/Matcher;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    .line 383
    :cond_0
    const/4 v7, 0x0

    .line 385
    .local v7, "hasPrefix":Z
    const/4 v8, 0x0

    .local v8, "i":I
    :goto_0
    array-length v0, p1

    if-lt v8, v0, :cond_3

    .line 398
    :cond_1
    :goto_1
    if-nez v7, :cond_2

    .line 399
    new-instance v0, Ljava/lang/StringBuilder;

    aget-object v1, p1, v2

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    .line 402
    :cond_2
    return-object p0

    .line 386
    :cond_3
    const/4 v1, 0x1

    aget-object v3, p1, v8

    aget-object v0, p1, v8

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v5

    move-object v0, p0

    move v4, v2

    invoke-virtual/range {v0 .. v5}, Ljava/lang/String;->regionMatches(ZILjava/lang/String;II)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 387
    const/4 v7, 0x1

    .line 390
    aget-object v4, p1, v8

    aget-object v0, p1, v8

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v6

    move-object v1, p0

    move v3, v2

    move v5, v2

    invoke-virtual/range {v1 .. v6}, Ljava/lang/String;->regionMatches(ZILjava/lang/String;II)Z

    move-result v0

    if-nez v0, :cond_1

    .line 391
    new-instance v0, Ljava/lang/StringBuilder;

    aget-object v1, p1, v8

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    aget-object v1, p1, v8

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    invoke-virtual {p0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    .line 394
    goto :goto_1

    .line 385
    :cond_4
    add-int/lit8 v8, v8, 0x1

    goto :goto_0
.end method

.method private static final pruneOverlaps(Ljava/util/ArrayList;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/app/memo/uiwidget/MemoLinkSpec;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 462
    .local p0, "links":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/app/memo/uiwidget/MemoLinkSpec;>;"
    new-instance v2, Lcom/samsung/android/app/memo/uiwidget/CustLinkify$4;

    invoke-direct {v2}, Lcom/samsung/android/app/memo/uiwidget/CustLinkify$4;-><init>()V

    .line 488
    .local v2, "c":Ljava/util/Comparator;, "Ljava/util/Comparator<Lcom/samsung/android/app/memo/uiwidget/MemoLinkSpec;>;"
    invoke-static {p0, v2}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 490
    invoke-virtual {p0}, Ljava/util/ArrayList;->size()I

    move-result v4

    .line 491
    .local v4, "len":I
    const/4 v3, 0x0

    .line 493
    .local v3, "i":I
    :goto_0
    add-int/lit8 v6, v4, -0x1

    if-lt v3, v6, :cond_0

    .line 517
    return-void

    .line 494
    :cond_0
    invoke-virtual {p0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/app/memo/uiwidget/MemoLinkSpec;

    .line 495
    .local v0, "a":Lcom/samsung/android/app/memo/uiwidget/MemoLinkSpec;
    add-int/lit8 v6, v3, 0x1

    invoke-virtual {p0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/app/memo/uiwidget/MemoLinkSpec;

    .line 496
    .local v1, "b":Lcom/samsung/android/app/memo/uiwidget/MemoLinkSpec;
    const/4 v5, -0x1

    .line 498
    .local v5, "remove":I
    iget v6, v0, Lcom/samsung/android/app/memo/uiwidget/MemoLinkSpec;->start:I

    iget v7, v1, Lcom/samsung/android/app/memo/uiwidget/MemoLinkSpec;->start:I

    if-gt v6, v7, :cond_4

    iget v6, v0, Lcom/samsung/android/app/memo/uiwidget/MemoLinkSpec;->end:I

    iget v7, v1, Lcom/samsung/android/app/memo/uiwidget/MemoLinkSpec;->start:I

    if-le v6, v7, :cond_4

    .line 499
    iget v6, v1, Lcom/samsung/android/app/memo/uiwidget/MemoLinkSpec;->end:I

    iget v7, v0, Lcom/samsung/android/app/memo/uiwidget/MemoLinkSpec;->end:I

    if-gt v6, v7, :cond_2

    .line 500
    add-int/lit8 v5, v3, 0x1

    .line 507
    :cond_1
    :goto_1
    const/4 v6, -0x1

    if-eq v5, v6, :cond_4

    .line 508
    invoke-virtual {p0, v5}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 509
    add-int/lit8 v4, v4, -0x1

    .line 510
    goto :goto_0

    .line 501
    :cond_2
    iget v6, v0, Lcom/samsung/android/app/memo/uiwidget/MemoLinkSpec;->end:I

    iget v7, v0, Lcom/samsung/android/app/memo/uiwidget/MemoLinkSpec;->start:I

    sub-int/2addr v6, v7

    iget v7, v1, Lcom/samsung/android/app/memo/uiwidget/MemoLinkSpec;->end:I

    iget v8, v1, Lcom/samsung/android/app/memo/uiwidget/MemoLinkSpec;->start:I

    sub-int/2addr v7, v8

    if-le v6, v7, :cond_3

    .line 502
    add-int/lit8 v5, v3, 0x1

    .line 503
    goto :goto_1

    :cond_3
    iget v6, v0, Lcom/samsung/android/app/memo/uiwidget/MemoLinkSpec;->end:I

    iget v7, v0, Lcom/samsung/android/app/memo/uiwidget/MemoLinkSpec;->start:I

    sub-int/2addr v6, v7

    iget v7, v1, Lcom/samsung/android/app/memo/uiwidget/MemoLinkSpec;->end:I

    iget v8, v1, Lcom/samsung/android/app/memo/uiwidget/MemoLinkSpec;->start:I

    sub-int/2addr v7, v8

    if-ge v6, v7, :cond_1

    .line 504
    move v5, v3

    goto :goto_1

    .line 515
    :cond_4
    add-int/lit8 v3, v3, 0x1

    goto :goto_0
.end method
