.class Lcom/samsung/android/app/memo/voice/VoiceViewLayout$13;
.super Landroid/content/BroadcastReceiver;
.source "VoiceViewLayout.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->updateKnoxSwitchReceiver(Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$13;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    .line 692
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 696
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.sec.knox.container.INTENT_KNOX_USER_CHANGED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 697
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$13;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    # getter for: Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mVoiceManger:Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;
    invoke-static {v0}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->access$9(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 699
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$13;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    # getter for: Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mVoiceManger:Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;
    invoke-static {v0}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->access$9(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->isNowRecording()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 700
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$13;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    invoke-virtual {v0}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->handleRecordingStop()V

    .line 701
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$13;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    iget-object v0, v0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mRecordListener:Lcom/samsung/android/app/memo/voice/VoiceRecorderBase$RecordListener;

    invoke-interface {v0}, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase$RecordListener;->onForceStop()V

    .line 707
    :cond_0
    :goto_0
    return-void

    .line 702
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$13;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    # getter for: Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mVoiceManger:Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;
    invoke-static {v0}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->access$9(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->isNowPlaying()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 703
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$13;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    invoke-virtual {v0}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->handleStopPlaying()V

    goto :goto_0
.end method
