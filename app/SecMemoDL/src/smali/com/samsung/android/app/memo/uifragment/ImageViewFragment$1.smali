.class Lcom/samsung/android/app/memo/uifragment/ImageViewFragment$1;
.super Landroid/os/Handler;
.source "ImageViewFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/memo/uifragment/ImageViewFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/memo/uifragment/ImageViewFragment;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/memo/uifragment/ImageViewFragment;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsung/android/app/memo/uifragment/ImageViewFragment$1;->this$0:Lcom/samsung/android/app/memo/uifragment/ImageViewFragment;

    .line 117
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 7
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 121
    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/ImageViewFragment$1;->this$0:Lcom/samsung/android/app/memo/uifragment/ImageViewFragment;

    # getter for: Lcom/samsung/android/app/memo/uifragment/ImageViewFragment;->mContentUri:Landroid/net/Uri;
    invoke-static {v1}, Lcom/samsung/android/app/memo/uifragment/ImageViewFragment;->access$0(Lcom/samsung/android/app/memo/uifragment/ImageViewFragment;)Landroid/net/Uri;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 122
    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/ImageViewFragment$1;->this$0:Lcom/samsung/android/app/memo/uifragment/ImageViewFragment;

    iget-object v2, p0, Lcom/samsung/android/app/memo/uifragment/ImageViewFragment$1;->this$0:Lcom/samsung/android/app/memo/uifragment/ImageViewFragment;

    invoke-virtual {v2}, Lcom/samsung/android/app/memo/uifragment/ImageViewFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/app/memo/uifragment/ImageViewFragment$1;->this$0:Lcom/samsung/android/app/memo/uifragment/ImageViewFragment;

    # getter for: Lcom/samsung/android/app/memo/uifragment/ImageViewFragment;->mContentUri:Landroid/net/Uri;
    invoke-static {v3}, Lcom/samsung/android/app/memo/uifragment/ImageViewFragment;->access$0(Lcom/samsung/android/app/memo/uifragment/ImageViewFragment;)Landroid/net/Uri;

    move-result-object v3

    const/4 v4, -0x1

    iget-object v5, p0, Lcom/samsung/android/app/memo/uifragment/ImageViewFragment$1;->this$0:Lcom/samsung/android/app/memo/uifragment/ImageViewFragment;

    # getter for: Lcom/samsung/android/app/memo/uifragment/ImageViewFragment;->mOrientation:I
    invoke-static {v5}, Lcom/samsung/android/app/memo/uifragment/ImageViewFragment;->access$1(Lcom/samsung/android/app/memo/uifragment/ImageViewFragment;)I

    move-result v5

    invoke-static {}, Lcom/samsung/android/app/memo/util/Utils;->getScreenWidthAbs()I

    move-result v6

    invoke-static {v2, v3, v4, v5, v6}, Lcom/samsung/android/app/memo/util/ImageUtil;->decodeImageScaledIf(Landroid/content/Context;Landroid/net/Uri;III)Landroid/graphics/Bitmap;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/android/app/memo/uifragment/ImageViewFragment;->access$2(Lcom/samsung/android/app/memo/uifragment/ImageViewFragment;Landroid/graphics/Bitmap;)V

    .line 124
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/ImageViewFragment$1;->this$0:Lcom/samsung/android/app/memo/uifragment/ImageViewFragment;

    # getter for: Lcom/samsung/android/app/memo/uifragment/ImageViewFragment;->mImageView:Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView;
    invoke-static {v1}, Lcom/samsung/android/app/memo/uifragment/ImageViewFragment;->access$3(Lcom/samsung/android/app/memo/uifragment/ImageViewFragment;)Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/app/memo/uifragment/ImageViewFragment$1;->this$0:Lcom/samsung/android/app/memo/uifragment/ImageViewFragment;

    # getter for: Lcom/samsung/android/app/memo/uifragment/ImageViewFragment;->mBitmap:Landroid/graphics/Bitmap;
    invoke-static {v2}, Lcom/samsung/android/app/memo/uifragment/ImageViewFragment;->access$4(Lcom/samsung/android/app/memo/uifragment/ImageViewFragment;)Landroid/graphics/Bitmap;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 128
    :goto_0
    return-void

    .line 125
    :catch_0
    move-exception v0

    .line 126
    .local v0, "e":Ljava/io/IOException;
    # getter for: Lcom/samsung/android/app/memo/uifragment/ImageViewFragment;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/app/memo/uifragment/ImageViewFragment;->access$5()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Failed to loaded content "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/samsung/android/app/memo/uifragment/ImageViewFragment$1;->this$0:Lcom/samsung/android/app/memo/uifragment/ImageViewFragment;

    # getter for: Lcom/samsung/android/app/memo/uifragment/ImageViewFragment;->mContentUri:Landroid/net/Uri;
    invoke-static {v3}, Lcom/samsung/android/app/memo/uifragment/ImageViewFragment;->access$0(Lcom/samsung/android/app/memo/uifragment/ImageViewFragment;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method
