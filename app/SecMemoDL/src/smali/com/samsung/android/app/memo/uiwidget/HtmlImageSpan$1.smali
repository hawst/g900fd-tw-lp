.class Lcom/samsung/android/app/memo/uiwidget/HtmlImageSpan$1;
.super Landroid/util/LruCache;
.source "HtmlImageSpan.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/memo/uiwidget/HtmlImageSpan;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/util/LruCache",
        "<",
        "Landroid/net/Uri;",
        "Landroid/graphics/Bitmap;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>(I)V
    .locals 0
    .param p1, "$anonymous0"    # I

    .prologue
    .line 102
    invoke-direct {p0, p1}, Landroid/util/LruCache;-><init>(I)V

    .line 1
    return-void
.end method


# virtual methods
.method protected entryRemoved(ZLandroid/net/Uri;Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;)V
    .locals 0
    .param p1, "evicted"    # Z
    .param p2, "key"    # Landroid/net/Uri;
    .param p3, "oldValue"    # Landroid/graphics/Bitmap;
    .param p4, "newValue"    # Landroid/graphics/Bitmap;

    .prologue
    .line 111
    invoke-super {p0, p1, p2, p3, p4}, Landroid/util/LruCache;->entryRemoved(ZLjava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 112
    if-eqz p1, :cond_0

    .line 113
    invoke-virtual {p3}, Landroid/graphics/Bitmap;->recycle()V

    .line 114
    :cond_0
    return-void
.end method

.method protected bridge synthetic entryRemoved(ZLjava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1
    check-cast p2, Landroid/net/Uri;

    check-cast p3, Landroid/graphics/Bitmap;

    check-cast p4, Landroid/graphics/Bitmap;

    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/samsung/android/app/memo/uiwidget/HtmlImageSpan$1;->entryRemoved(ZLandroid/net/Uri;Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;)V

    return-void
.end method

.method protected sizeOf(Landroid/net/Uri;Landroid/graphics/Bitmap;)I
    .locals 1
    .param p1, "key"    # Landroid/net/Uri;
    .param p2, "item"    # Landroid/graphics/Bitmap;

    .prologue
    .line 107
    invoke-virtual {p2}, Landroid/graphics/Bitmap;->getByteCount()I

    move-result v0

    return v0
.end method

.method protected bridge synthetic sizeOf(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 1
    check-cast p1, Landroid/net/Uri;

    check-cast p2, Landroid/graphics/Bitmap;

    invoke-virtual {p0, p1, p2}, Lcom/samsung/android/app/memo/uiwidget/HtmlImageSpan$1;->sizeOf(Landroid/net/Uri;Landroid/graphics/Bitmap;)I

    move-result v0

    return v0
.end method
