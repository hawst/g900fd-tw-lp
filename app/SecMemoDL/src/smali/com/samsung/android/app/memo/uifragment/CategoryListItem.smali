.class Lcom/samsung/android/app/memo/uifragment/CategoryListItem;
.super Ljava/lang/Object;
.source "CategoryManagementFragment.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private categoryName:Ljava/lang/String;

.field private category_id:J

.field private category_uuid:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 587
    const-class v0, Lcom/samsung/android/app/memo/uifragment/CategoryListItem;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/app/memo/uifragment/CategoryListItem;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(ILjava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "category_id"    # I
    .param p2, "category_uuid"    # Ljava/lang/String;
    .param p3, "categoryName"    # Ljava/lang/String;

    .prologue
    .line 595
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 596
    int-to-long v0, p1

    iput-wide v0, p0, Lcom/samsung/android/app/memo/uifragment/CategoryListItem;->category_id:J

    .line 597
    iput-object p2, p0, Lcom/samsung/android/app/memo/uifragment/CategoryListItem;->category_uuid:Ljava/lang/String;

    .line 598
    iput-object p3, p0, Lcom/samsung/android/app/memo/uifragment/CategoryListItem;->categoryName:Ljava/lang/String;

    .line 599
    return-void
.end method


# virtual methods
.method public getCategoryID()J
    .locals 2

    .prologue
    .line 602
    iget-wide v0, p0, Lcom/samsung/android/app/memo/uifragment/CategoryListItem;->category_id:J

    return-wide v0
.end method

.method public getCategoryName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 610
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/CategoryListItem;->categoryName:Ljava/lang/String;

    return-object v0
.end method

.method public getCategoryUUID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 606
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/CategoryListItem;->category_uuid:Ljava/lang/String;

    return-object v0
.end method

.method public setCategoryName(Ljava/lang/String;)V
    .locals 0
    .param p1, "categoryName"    # Ljava/lang/String;

    .prologue
    .line 614
    iput-object p1, p0, Lcom/samsung/android/app/memo/uifragment/CategoryListItem;->categoryName:Ljava/lang/String;

    .line 615
    return-void
.end method
