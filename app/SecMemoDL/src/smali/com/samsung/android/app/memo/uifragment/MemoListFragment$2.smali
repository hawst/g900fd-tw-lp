.class Lcom/samsung/android/app/memo/uifragment/MemoListFragment$2;
.super Ljava/lang/Object;
.source "MemoListFragment.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->onActivityCreated(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/widget/AdapterView$OnItemClickListener;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$2;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    .line 544
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$0(Lcom/samsung/android/app/memo/uifragment/MemoListFragment$2;)Lcom/samsung/android/app/memo/uifragment/MemoListFragment;
    .locals 1

    .prologue
    .line 544
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$2;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    return-object v0
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 6
    .param p2, "arg1"    # Landroid/view/View;
    .param p3, "arg2"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 549
    .local p1, "arg0":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 550
    .local v0, "currentTouchTime":J
    iget-object v2, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$2;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mLastMemoOpenClickTime:J
    invoke-static {v2}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->access$15(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;)J

    move-result-wide v2

    sub-long v2, v0, v2

    const-wide/16 v4, 0x2bc

    cmp-long v2, v2, v4

    if-lez v2, :cond_0

    .line 551
    iget-object v2, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$2;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mFragmentAction:Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;
    invoke-static {v2}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->access$36(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;)Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-interface {v2, p4, p5, v3, v4}, Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;->openContent(JLjava/lang/String;Z)V

    .line 553
    :cond_0
    iget-object v2, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$2;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-static {v2, v4, v5}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->access$16(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;J)V

    .line 555
    iget-object v2, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$2;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mMenuSearch:Landroid/view/MenuItem;
    invoke-static {v2}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->access$41(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;)Landroid/view/MenuItem;

    move-result-object v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$2;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mMenuSearch:Landroid/view/MenuItem;
    invoke-static {v2}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->access$41(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;)Landroid/view/MenuItem;

    move-result-object v2

    invoke-interface {v2}, Landroid/view/MenuItem;->isActionViewExpanded()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 556
    new-instance v2, Landroid/os/Handler;

    invoke-direct {v2}, Landroid/os/Handler;-><init>()V

    new-instance v3, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$2$1;

    invoke-direct {v3, p0}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$2$1;-><init>(Lcom/samsung/android/app/memo/uifragment/MemoListFragment$2;)V

    .line 561
    const-wide/16 v4, 0x190

    .line 556
    invoke-virtual {v2, v3, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 563
    :cond_1
    return-void
.end method
