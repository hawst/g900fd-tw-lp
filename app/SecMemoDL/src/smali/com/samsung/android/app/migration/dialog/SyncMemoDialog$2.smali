.class Lcom/samsung/android/app/migration/dialog/SyncMemoDialog$2;
.super Ljava/lang/Object;
.source "SyncMemoDialog.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/app/migration/dialog/SyncMemoDialog;->onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/migration/dialog/SyncMemoDialog;

.field private final synthetic val$ctx:Landroid/app/Activity;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/migration/dialog/SyncMemoDialog;Landroid/app/Activity;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsung/android/app/migration/dialog/SyncMemoDialog$2;->this$0:Lcom/samsung/android/app/migration/dialog/SyncMemoDialog;

    iput-object p2, p0, Lcom/samsung/android/app/migration/dialog/SyncMemoDialog$2;->val$ctx:Landroid/app/Activity;

    .line 84
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "id"    # I

    .prologue
    .line 87
    iget-object v0, p0, Lcom/samsung/android/app/migration/dialog/SyncMemoDialog$2;->val$ctx:Landroid/app/Activity;

    invoke-static {v0}, Lcom/samsung/android/app/memo/util/AccountHelper;->getRegisteredSamsungAccount(Landroid/content/Context;)Landroid/accounts/Account;

    move-result-object v0

    const-string v1, "com.samsung.android.memo"

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/content/ContentResolver;->setSyncAutomatically(Landroid/accounts/Account;Ljava/lang/String;Z)V

    .line 88
    iget-object v0, p0, Lcom/samsung/android/app/migration/dialog/SyncMemoDialog$2;->val$ctx:Landroid/app/Activity;

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/samsung/android/app/migration/utils/SharedPref;->setSyncMemoDialogState(Landroid/content/Context;I)V

    .line 89
    iget-object v0, p0, Lcom/samsung/android/app/migration/dialog/SyncMemoDialog$2;->this$0:Lcom/samsung/android/app/migration/dialog/SyncMemoDialog;

    iget-object v1, p0, Lcom/samsung/android/app/migration/dialog/SyncMemoDialog$2;->val$ctx:Landroid/app/Activity;

    # invokes: Lcom/samsung/android/app/migration/dialog/SyncMemoDialog;->launchTMemoImport(Landroid/app/Activity;)V
    invoke-static {v0, v1}, Lcom/samsung/android/app/migration/dialog/SyncMemoDialog;->access$0(Lcom/samsung/android/app/migration/dialog/SyncMemoDialog;Landroid/app/Activity;)V

    .line 90
    iget-object v0, p0, Lcom/samsung/android/app/migration/dialog/SyncMemoDialog$2;->this$0:Lcom/samsung/android/app/migration/dialog/SyncMemoDialog;

    invoke-virtual {v0}, Lcom/samsung/android/app/migration/dialog/SyncMemoDialog;->dismiss()V

    .line 91
    return-void
.end method
