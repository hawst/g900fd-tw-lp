.class public Lcom/samsung/android/app/memo/KillVoiceNotificationService;
.super Landroid/app/Service;
.source "KillVoiceNotificationService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/app/memo/KillVoiceNotificationService$KillBinder;
    }
.end annotation


# instance fields
.field private final mBinder:Landroid/os/IBinder;

.field private mNM:Landroid/app/NotificationManager;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 30
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 42
    new-instance v0, Lcom/samsung/android/app/memo/KillVoiceNotificationService$KillBinder;

    invoke-direct {v0, p0}, Lcom/samsung/android/app/memo/KillVoiceNotificationService$KillBinder;-><init>(Landroid/app/Service;)V

    iput-object v0, p0, Lcom/samsung/android/app/memo/KillVoiceNotificationService;->mBinder:Landroid/os/IBinder;

    .line 30
    return-void
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/samsung/android/app/memo/KillVoiceNotificationService;->mBinder:Landroid/os/IBinder;

    return-object v0
.end method

.method public onDestroy()V
    .locals 0

    .prologue
    .line 58
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 59
    return-void
.end method

.method public onUnbind(Landroid/content/Intent;)Z
    .locals 3
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v2, 0x0

    .line 50
    invoke-static {v2}, Lcom/samsung/android/app/memo/MemoApp;->setShowNotificationEnabled(Z)V

    .line 51
    const-string v0, "notification"

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/memo/KillVoiceNotificationService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    iput-object v0, p0, Lcom/samsung/android/app/memo/KillVoiceNotificationService;->mNM:Landroid/app/NotificationManager;

    .line 52
    iget-object v0, p0, Lcom/samsung/android/app/memo/KillVoiceNotificationService;->mNM:Landroid/app/NotificationManager;

    const v1, 0x5411111

    invoke-virtual {v0, v1}, Landroid/app/NotificationManager;->cancel(I)V

    .line 53
    return v2
.end method
