.class public Lcom/samsung/android/app/memo/util/SplitManager;
.super Ljava/lang/Object;
.source "SplitManager.java"


# static fields
.field private static final SPLIT_BAR_MIN_X:F = 10.0f

.field public static final SPLIT_BAR_POSITION:Ljava/lang/String; = "pref_key_splitbar_position"

.field private static final SPLIT_LAND_VALUE:I

.field private static final SPLIT_MIN_X:I = 0x5c

.field public static final SPLIT_MODE_LEFT_ONLY:I = 0x0

.field public static final SPLIT_MODE_NONE:I = -0x1

.field public static final SPLIT_MODE_RIGHT_ONLY:I = 0x1

.field public static final SPLIT_MODE_SPLITED:I = 0x2

.field private static final SPLIT_PORT_VALUE:I

.field public static final SPLIT_XPER_NOT_STORED:I = -0x1

.field private static final TAG:Ljava/lang/String; = "Memo/SplitManager"

.field private static mActivity:Landroid/app/Activity;

.field private static mSplitX:I

.field private static mXPer:F

.field private static mXPerPrev:F


# instance fields
.field private mDensity:F

.field private mFocusedBar:Landroid/view/View;

.field private mLeftView:Landroid/view/View;

.field private mMaxXPer:I

.field private mMinX:I

.field public mMinXPer:I

.field private mNormalBar:Landroid/view/View;

.field private mRightSplitBar:Landroid/view/View;

.field public mSplitBarOnTouchListener:Landroid/view/View$OnTouchListener;

.field private mSplitMode:I

.field private mWidth:I

.field private mleftSplitBar:Landroid/view/View;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 68
    const v0, 0x7f0a000c

    invoke-static {v0}, Lcom/samsung/android/app/memo/util/Utils;->getInteger(I)I

    move-result v0

    sput v0, Lcom/samsung/android/app/memo/util/SplitManager;->SPLIT_LAND_VALUE:I

    .line 69
    const v0, 0x7f0a000b

    invoke-static {v0}, Lcom/samsung/android/app/memo/util/Utils;->getInteger(I)I

    move-result v0

    sput v0, Lcom/samsung/android/app/memo/util/SplitManager;->SPLIT_PORT_VALUE:I

    .line 156
    return-void
.end method

.method public constructor <init>(Landroid/view/View;Landroid/view/View;Landroid/view/View;Landroid/view/View;Landroid/view/View;Landroid/app/Activity;)V
    .locals 3
    .param p1, "leftView"    # Landroid/view/View;
    .param p2, "leftSplitBar"    # Landroid/view/View;
    .param p3, "rightSplitBar"    # Landroid/view/View;
    .param p4, "normalBar"    # Landroid/view/View;
    .param p5, "focusBar"    # Landroid/view/View;
    .param p6, "activity"    # Landroid/app/Activity;

    .prologue
    const/4 v1, 0x0

    .line 72
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    iput-object v1, p0, Lcom/samsung/android/app/memo/util/SplitManager;->mLeftView:Landroid/view/View;

    .line 39
    iput-object v1, p0, Lcom/samsung/android/app/memo/util/SplitManager;->mleftSplitBar:Landroid/view/View;

    .line 41
    iput-object v1, p0, Lcom/samsung/android/app/memo/util/SplitManager;->mRightSplitBar:Landroid/view/View;

    .line 43
    iput-object v1, p0, Lcom/samsung/android/app/memo/util/SplitManager;->mNormalBar:Landroid/view/View;

    .line 45
    iput-object v1, p0, Lcom/samsung/android/app/memo/util/SplitManager;->mFocusedBar:Landroid/view/View;

    .line 57
    const/16 v1, 0x42

    iput v1, p0, Lcom/samsung/android/app/memo/util/SplitManager;->mMaxXPer:I

    .line 61
    const/4 v1, 0x2

    iput v1, p0, Lcom/samsung/android/app/memo/util/SplitManager;->mSplitMode:I

    .line 63
    const/high16 v1, 0x3f800000    # 1.0f

    iput v1, p0, Lcom/samsung/android/app/memo/util/SplitManager;->mDensity:F

    .line 206
    new-instance v1, Lcom/samsung/android/app/memo/util/SplitManager$1;

    invoke-direct {v1, p0}, Lcom/samsung/android/app/memo/util/SplitManager$1;-><init>(Lcom/samsung/android/app/memo/util/SplitManager;)V

    iput-object v1, p0, Lcom/samsung/android/app/memo/util/SplitManager;->mSplitBarOnTouchListener:Landroid/view/View$OnTouchListener;

    .line 74
    iput-object p1, p0, Lcom/samsung/android/app/memo/util/SplitManager;->mLeftView:Landroid/view/View;

    .line 75
    iput-object p2, p0, Lcom/samsung/android/app/memo/util/SplitManager;->mleftSplitBar:Landroid/view/View;

    .line 76
    iput-object p3, p0, Lcom/samsung/android/app/memo/util/SplitManager;->mRightSplitBar:Landroid/view/View;

    .line 77
    iput-object p4, p0, Lcom/samsung/android/app/memo/util/SplitManager;->mNormalBar:Landroid/view/View;

    .line 78
    iput-object p5, p0, Lcom/samsung/android/app/memo/util/SplitManager;->mFocusedBar:Landroid/view/View;

    .line 79
    sput-object p6, Lcom/samsung/android/app/memo/util/SplitManager;->mActivity:Landroid/app/Activity;

    .line 81
    sget-object v1, Lcom/samsung/android/app/memo/util/SplitManager;->mActivity:Landroid/app/Activity;

    if-eqz v1, :cond_0

    .line 82
    new-instance v0, Landroid/util/DisplayMetrics;

    invoke-direct {v0}, Landroid/util/DisplayMetrics;-><init>()V

    .line 83
    .local v0, "displayMetrics":Landroid/util/DisplayMetrics;
    sget-object v1, Lcom/samsung/android/app/memo/util/SplitManager;->mActivity:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v1

    invoke-interface {v1}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 84
    iget v1, v0, Landroid/util/DisplayMetrics;->density:F

    iput v1, p0, Lcom/samsung/android/app/memo/util/SplitManager;->mDensity:F

    .line 87
    .end local v0    # "displayMetrics":Landroid/util/DisplayMetrics;
    :cond_0
    const/high16 v1, 0x41200000    # 10.0f

    iget v2, p0, Lcom/samsung/android/app/memo/util/SplitManager;->mDensity:F

    mul-float/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    iput v1, p0, Lcom/samsung/android/app/memo/util/SplitManager;->mMinX:I

    .line 89
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/util/SplitManager;->SplitPositionInit()V

    .line 91
    iget-object v1, p0, Lcom/samsung/android/app/memo/util/SplitManager;->mleftSplitBar:Landroid/view/View;

    iget-object v2, p0, Lcom/samsung/android/app/memo/util/SplitManager;->mSplitBarOnTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 92
    iget-object v1, p0, Lcom/samsung/android/app/memo/util/SplitManager;->mRightSplitBar:Landroid/view/View;

    iget-object v2, p0, Lcom/samsung/android/app/memo/util/SplitManager;->mSplitBarOnTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 93
    return-void
.end method

.method static synthetic access$0(Lcom/samsung/android/app/memo/util/SplitManager;)Landroid/view/View;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/samsung/android/app/memo/util/SplitManager;->mNormalBar:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$1(Lcom/samsung/android/app/memo/util/SplitManager;)Landroid/view/View;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/samsung/android/app/memo/util/SplitManager;->mFocusedBar:Landroid/view/View;

    return-object v0
.end method

.method private checkfordefaultvalue()V
    .locals 3

    .prologue
    .line 193
    sget-object v1, Lcom/samsung/android/app/memo/util/SplitManager;->mActivity:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget v1, v1, Landroid/content/res/Configuration;->orientation:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_1

    const/4 v0, 0x1

    .line 194
    .local v0, "mIsLandscape":Z
    :goto_0
    if-eqz v0, :cond_2

    sget v1, Lcom/samsung/android/app/memo/util/SplitManager;->mXPer:F

    sget v2, Lcom/samsung/android/app/memo/util/SplitManager;->SPLIT_PORT_VALUE:I

    int-to-float v2, v2

    cmpl-float v1, v1, v2

    if-nez v1, :cond_2

    .line 195
    sget v1, Lcom/samsung/android/app/memo/util/SplitManager;->SPLIT_LAND_VALUE:I

    int-to-float v1, v1

    sput v1, Lcom/samsung/android/app/memo/util/SplitManager;->mXPer:F

    .line 199
    :cond_0
    :goto_1
    return-void

    .line 193
    .end local v0    # "mIsLandscape":Z
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 196
    .restart local v0    # "mIsLandscape":Z
    :cond_2
    if-nez v0, :cond_0

    sget v1, Lcom/samsung/android/app/memo/util/SplitManager;->mXPer:F

    sget v2, Lcom/samsung/android/app/memo/util/SplitManager;->SPLIT_LAND_VALUE:I

    int-to-float v2, v2

    cmpl-float v1, v1, v2

    if-nez v1, :cond_0

    .line 197
    sget v1, Lcom/samsung/android/app/memo/util/SplitManager;->SPLIT_PORT_VALUE:I

    int-to-float v1, v1

    sput v1, Lcom/samsung/android/app/memo/util/SplitManager;->mXPer:F

    goto :goto_1
.end method

.method public static getScreenWidth()I
    .locals 2

    .prologue
    .line 107
    sget-object v1, Lcom/samsung/android/app/memo/util/SplitManager;->mActivity:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getWidth()I

    move-result v0

    .line 109
    .local v0, "screenWidth":I
    return v0
.end method


# virtual methods
.method public SplitPositionInit()V
    .locals 3

    .prologue
    .line 142
    sget-object v1, Lcom/samsung/android/app/memo/util/SplitManager;->mActivity:Landroid/app/Activity;

    if-nez v1, :cond_1

    .line 154
    :cond_0
    :goto_0
    return-void

    .line 145
    :cond_1
    sget-object v1, Lcom/samsung/android/app/memo/util/SplitManager;->mActivity:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget v1, v1, Landroid/content/res/Configuration;->orientation:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_2

    const/4 v0, 0x1

    .line 146
    .local v0, "mIsLandscape":Z
    :goto_1
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/util/SplitManager;->readSplitPosition()V

    .line 147
    sget v1, Lcom/samsung/android/app/memo/util/SplitManager;->mXPer:F

    const/high16 v2, -0x40800000    # -1.0f

    cmpl-float v1, v1, v2

    if-nez v1, :cond_0

    .line 149
    if-eqz v0, :cond_3

    .line 150
    sget v1, Lcom/samsung/android/app/memo/util/SplitManager;->SPLIT_LAND_VALUE:I

    int-to-float v1, v1

    sput v1, Lcom/samsung/android/app/memo/util/SplitManager;->mXPer:F

    goto :goto_0

    .line 145
    .end local v0    # "mIsLandscape":Z
    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    .line 152
    .restart local v0    # "mIsLandscape":Z
    :cond_3
    sget v1, Lcom/samsung/android/app/memo/util/SplitManager;->SPLIT_PORT_VALUE:I

    int-to-float v1, v1

    sput v1, Lcom/samsung/android/app/memo/util/SplitManager;->mXPer:F

    goto :goto_0
.end method

.method public UpdateSplitPosition(I)Z
    .locals 6
    .param p1, "offset"    # I

    .prologue
    const/4 v1, 0x0

    .line 113
    iget v2, p0, Lcom/samsung/android/app/memo/util/SplitManager;->mWidth:I

    int-to-float v2, v2

    sget v3, Lcom/samsung/android/app/memo/util/SplitManager;->mXPer:F

    mul-float/2addr v2, v3

    const/high16 v3, 0x42c80000    # 100.0f

    div-float/2addr v2, v3

    float-to-int v2, v2

    add-int v0, v2, p1

    .line 115
    .local v0, "x":I
    iget v2, p0, Lcom/samsung/android/app/memo/util/SplitManager;->mMinX:I

    if-ge v0, v2, :cond_1

    .line 116
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/util/SplitManager;->refreshLayouts()Z

    move-result v1

    .line 138
    :cond_0
    :goto_0
    return v1

    .line 117
    :cond_1
    iget v2, p0, Lcom/samsung/android/app/memo/util/SplitManager;->mWidth:I

    if-gt v0, v2, :cond_0

    .line 120
    iget v2, p0, Lcom/samsung/android/app/memo/util/SplitManager;->mSplitMode:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_0

    .line 123
    int-to-float v1, v0

    float-to-double v2, v1

    const-wide/high16 v4, 0x4059000000000000L    # 100.0

    mul-double/2addr v2, v4

    iget v1, p0, Lcom/samsung/android/app/memo/util/SplitManager;->mWidth:I

    int-to-float v1, v1

    float-to-double v4, v1

    div-double/2addr v2, v4

    double-to-float v1, v2

    sput v1, Lcom/samsung/android/app/memo/util/SplitManager;->mXPer:F

    .line 125
    sget v1, Lcom/samsung/android/app/memo/util/SplitManager;->mXPer:F

    iget v2, p0, Lcom/samsung/android/app/memo/util/SplitManager;->mMaxXPer:I

    int-to-float v2, v2

    cmpl-float v1, v1, v2

    if-lez v1, :cond_2

    .line 126
    iget v1, p0, Lcom/samsung/android/app/memo/util/SplitManager;->mMaxXPer:I

    int-to-float v1, v1

    sput v1, Lcom/samsung/android/app/memo/util/SplitManager;->mXPer:F

    .line 128
    :cond_2
    sget v1, Lcom/samsung/android/app/memo/util/SplitManager;->mXPer:F

    iget v2, p0, Lcom/samsung/android/app/memo/util/SplitManager;->mMinXPer:I

    int-to-float v2, v2

    cmpg-float v1, v1, v2

    if-gez v1, :cond_3

    .line 129
    sget v1, Lcom/samsung/android/app/memo/util/SplitManager;->mXPer:F

    sget v2, Lcom/samsung/android/app/memo/util/SplitManager;->mXPerPrev:F

    cmpg-float v1, v1, v2

    if-gez v1, :cond_4

    .line 130
    const/high16 v1, 0x3f800000    # 1.0f

    sput v1, Lcom/samsung/android/app/memo/util/SplitManager;->mXPer:F

    .line 136
    :cond_3
    :goto_1
    sget v1, Lcom/samsung/android/app/memo/util/SplitManager;->mXPer:F

    sput v1, Lcom/samsung/android/app/memo/util/SplitManager;->mXPerPrev:F

    .line 138
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/util/SplitManager;->refreshLayouts()Z

    move-result v1

    goto :goto_0

    .line 132
    :cond_4
    iget v1, p0, Lcom/samsung/android/app/memo/util/SplitManager;->mMinXPer:I

    int-to-float v1, v1

    sput v1, Lcom/samsung/android/app/memo/util/SplitManager;->mXPer:F

    goto :goto_1
.end method

.method public onConfigurationChanged()V
    .locals 0

    .prologue
    .line 202
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/util/SplitManager;->setSplitWidth()V

    .line 203
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/util/SplitManager;->refreshLayouts()Z

    .line 204
    return-void
.end method

.method public readSplitPosition()V
    .locals 3

    .prologue
    .line 168
    sget-object v1, Lcom/samsung/android/app/memo/util/SplitManager;->mActivity:Landroid/app/Activity;

    if-eqz v1, :cond_0

    .line 170
    sget-object v1, Lcom/samsung/android/app/memo/util/SplitManager;->mActivity:Landroid/app/Activity;

    invoke-static {v1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 171
    .local v0, "defaultPref":Landroid/content/SharedPreferences;
    const-string v1, "pref_key_splitbar_position"

    const/high16 v2, -0x40800000    # -1.0f

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getFloat(Ljava/lang/String;F)F

    move-result v1

    sput v1, Lcom/samsung/android/app/memo/util/SplitManager;->mXPer:F

    .line 173
    .end local v0    # "defaultPref":Landroid/content/SharedPreferences;
    :cond_0
    return-void
.end method

.method public refreshLayouts()Z
    .locals 3

    .prologue
    .line 176
    const-string v1, "Memo/SplitManager"

    const-string v2, "refreshLayouts()"

    invoke-static {v1, v2}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 178
    iget v1, p0, Lcom/samsung/android/app/memo/util/SplitManager;->mSplitMode:I

    packed-switch v1, :pswitch_data_0

    .line 187
    const/4 v1, 0x0

    .line 189
    :goto_0
    return v1

    .line 180
    :pswitch_0
    invoke-direct {p0}, Lcom/samsung/android/app/memo/util/SplitManager;->checkfordefaultvalue()V

    .line 181
    iget v1, p0, Lcom/samsung/android/app/memo/util/SplitManager;->mWidth:I

    int-to-float v1, v1

    sget v2, Lcom/samsung/android/app/memo/util/SplitManager;->mXPer:F

    mul-float/2addr v1, v2

    const/high16 v2, 0x42c80000    # 100.0f

    div-float/2addr v1, v2

    float-to-int v1, v1

    sput v1, Lcom/samsung/android/app/memo/util/SplitManager;->mSplitX:I

    .line 182
    iget-object v1, p0, Lcom/samsung/android/app/memo/util/SplitManager;->mLeftView:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 183
    .local v0, "lp":Landroid/view/ViewGroup$LayoutParams;
    sget v1, Lcom/samsung/android/app/memo/util/SplitManager;->mSplitX:I

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 184
    iget-object v1, p0, Lcom/samsung/android/app/memo/util/SplitManager;->mLeftView:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 189
    const/4 v1, 0x1

    goto :goto_0

    .line 178
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
    .end packed-switch
.end method

.method public saveSplitPosition()V
    .locals 3

    .prologue
    .line 159
    sget-object v1, Lcom/samsung/android/app/memo/util/SplitManager;->mActivity:Landroid/app/Activity;

    if-eqz v1, :cond_0

    .line 161
    sget-object v1, Lcom/samsung/android/app/memo/util/SplitManager;->mActivity:Landroid/app/Activity;

    .line 160
    invoke-static {v1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 161
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 162
    .local v0, "editPrefs":Landroid/content/SharedPreferences$Editor;
    const-string v1, "pref_key_splitbar_position"

    sget v2, Lcom/samsung/android/app/memo/util/SplitManager;->mXPer:F

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putFloat(Ljava/lang/String;F)Landroid/content/SharedPreferences$Editor;

    .line 163
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 165
    .end local v0    # "editPrefs":Landroid/content/SharedPreferences$Editor;
    :cond_0
    return-void
.end method

.method public setSplitWidth()V
    .locals 3

    .prologue
    .line 96
    invoke-static {}, Lcom/samsung/android/app/memo/util/SplitManager;->getScreenWidth()I

    move-result v0

    .line 97
    .local v0, "width":I
    if-nez v0, :cond_0

    .line 98
    const/4 v0, 0x1

    .line 99
    const-string v1, "Memo/SplitManager"

    const-string v2, "width is Zero. So width set to 1"

    invoke-static {v1, v2}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 102
    :cond_0
    iput v0, p0, Lcom/samsung/android/app/memo/util/SplitManager;->mWidth:I

    .line 103
    const/16 v1, 0x23f0

    iget v2, p0, Lcom/samsung/android/app/memo/util/SplitManager;->mWidth:I

    div-int/2addr v1, v2

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/samsung/android/app/memo/util/SplitManager;->mMinXPer:I

    .line 104
    return-void
.end method
