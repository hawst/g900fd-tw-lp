.class Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;
.super Landroid/widget/CursorAdapter;
.source "MemoListFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/memo/uifragment/MemoListFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ListCursorAdapter"
.end annotation


# instance fields
.field private imageLoader:Lcom/samsung/android/app/memo/util/ImageLoader;

.field private isTablet:Z

.field private final mSelectedItemsIds:Landroid/util/SparseBooleanArray;

.field private memo_list_item_contents_height:I

.field private memo_list_item_contents_margin_left:I

.field private memo_list_item_contents_margin_right:I

.field private memo_list_item_contents_margin_right_selection_mode:I

.field private memo_list_item_contents_margin_top:I

.field private memo_list_item_date_time_compound_padding:I

.field private memo_list_item_max_lines:I

.field private memo_list_item_title_margin_left_selection_mode:I

.field private sec_roboto_light:Landroid/graphics/Typeface;

.field t:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListItemTag;

.field final synthetic this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;


# direct methods
.method public constructor <init>(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;Landroid/content/Context;Landroid/database/Cursor;ILcom/samsung/android/app/memo/util/ImageLoader;)V
    .locals 1
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "c"    # Landroid/database/Cursor;
    .param p4, "flags"    # I
    .param p5, "mImageLoader"    # Lcom/samsung/android/app/memo/util/ImageLoader;

    .prologue
    .line 1562
    iput-object p1, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    .line 1563
    invoke-direct {p0, p2, p3, p4}, Landroid/widget/CursorAdapter;-><init>(Landroid/content/Context;Landroid/database/Cursor;I)V

    .line 1509
    invoke-static {}, Lcom/samsung/android/app/memo/util/Utils;->isTablet()Z

    move-result v0

    iput-boolean v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;->isTablet:Z

    .line 1564
    iput-object p5, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;->imageLoader:Lcom/samsung/android/app/memo/util/ImageLoader;

    .line 1565
    new-instance v0, Landroid/util/SparseBooleanArray;

    invoke-direct {v0}, Landroid/util/SparseBooleanArray;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;->mSelectedItemsIds:Landroid/util/SparseBooleanArray;

    .line 1567
    invoke-direct {p0, p2}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;->initVars(Landroid/content/Context;)V

    .line 1569
    return-void
.end method

.method static synthetic access$0(Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 1504
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$1(Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;Landroid/view/View;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1903
    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;->setHoverPopupContents(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private highlightSearchResult()V
    .locals 7

    .prologue
    .line 1813
    iget-object v5, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;->t:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListItemTag;

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mStrQuery:Ljava/lang/String;
    invoke-static {v5}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->access$5(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    if-nez v5, :cond_1

    .line 1839
    :cond_0
    :goto_0
    return-void

    .line 1816
    :cond_1
    iget-object v5, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mStrQuery:Ljava/lang/String;
    invoke-static {v5}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->access$5(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;)Ljava/lang/String;

    move-result-object v2

    .line 1817
    .local v2, "queryString":Ljava/lang/String;
    iget-object v5, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;->t:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListItemTag;

    iget-object v5, v5, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListItemTag;->snippet:Landroid/widget/TextView;

    invoke-virtual {v5}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v5

    invoke-interface {v5}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1818
    .local v0, "body":Ljava/lang/String;
    iget-object v5, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;->t:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListItemTag;

    iget-object v5, v5, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListItemTag;->title:Landroid/widget/TextView;

    invoke-virtual {v5}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v5

    invoke-interface {v5}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1819
    .local v3, "title":Ljava/lang/String;
    const/4 v1, 0x0

    .line 1820
    .local v1, "bodyLoopCount":I
    iget-object v5, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;->t:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListItemTag;

    iget-object v5, v5, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListItemTag;->snippet:Landroid/widget/TextView;

    invoke-virtual {v5}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v5

    invoke-interface {v5}, Ljava/lang/CharSequence;->length()I

    move-result v5

    const/16 v6, 0x12c

    if-le v5, v6, :cond_3

    .line 1821
    const/16 v1, 0x12c

    .line 1825
    :goto_1
    const/4 v4, 0x0

    .line 1826
    .local v4, "titleLoopCount":I
    iget-object v5, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;->t:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListItemTag;

    iget-object v5, v5, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListItemTag;->title:Landroid/widget/TextView;

    if-eqz v5, :cond_4

    iget-object v5, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;->t:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListItemTag;

    iget-object v5, v5, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListItemTag;->title:Landroid/widget/TextView;

    invoke-virtual {v5}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v5

    invoke-interface {v5}, Ljava/lang/CharSequence;->length()I

    move-result v5

    const/16 v6, 0x64

    if-le v5, v6, :cond_4

    .line 1827
    const/16 v4, 0x64

    .line 1831
    :goto_2
    if-lez v1, :cond_2

    if-eqz v2, :cond_2

    .line 1832
    const/4 v5, 0x1

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-direct {p0, v0, v2, v5}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;->highlightText(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 1835
    :cond_2
    if-lez v4, :cond_0

    if-eqz v2, :cond_0

    .line 1836
    const/4 v5, 0x0

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-direct {p0, v3, v2, v5}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;->highlightText(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;)V

    goto :goto_0

    .line 1823
    .end local v4    # "titleLoopCount":I
    :cond_3
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    goto :goto_1

    .line 1829
    .restart local v4    # "titleLoopCount":I
    :cond_4
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v4

    goto :goto_2
.end method

.method private highlightText(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;)V
    .locals 27
    .param p1, "text"    # Ljava/lang/String;
    .param p2, "query"    # Ljava/lang/String;
    .param p3, "isBody"    # Ljava/lang/Boolean;

    .prologue
    .line 1842
    if-nez p2, :cond_1

    .line 1901
    :cond_0
    :goto_0
    return-void

    .line 1844
    :cond_1
    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v13

    .line 1845
    .local v13, "lowerText":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v12

    .line 1846
    .local v12, "lowerQuery":Ljava/lang/String;
    invoke-virtual {v13, v12}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v19

    .line 1847
    .local v19, "queryStart":I
    invoke-virtual/range {p2 .. p2}, Ljava/lang/String;->length()I

    move-result v24

    add-int v18, v19, v24

    .line 1848
    .local v18, "queryEnd":I
    if-ltz v19, :cond_0

    .line 1850
    new-instance v20, Landroid/text/SpannableStringBuilder;

    move-object/from16 v0, v20

    move-object/from16 v1, p1

    invoke-direct {v0, v1}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 1851
    .local v20, "sp":Landroid/text/SpannableStringBuilder;
    const/16 v23, 0x0

    .line 1852
    .local v23, "tView":Landroid/widget/TextView;
    invoke-virtual/range {p3 .. p3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v24

    if-eqz v24, :cond_3

    .line 1853
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;->t:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListItemTag;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    iget-object v0, v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListItemTag;->snippet:Landroid/widget/TextView;

    move-object/from16 v23, v0

    .line 1857
    :goto_1
    const/16 v16, 0x0

    .line 1858
    .local v16, "prefixForIndian":[C
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    move-object/from16 v24, v0

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->SEARCH_RESULT_BG_COLOR:Ljava/lang/String;
    invoke-static/range {v24 .. v24}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->access$6(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;)Ljava/lang/String;

    move-result-object v24

    invoke-static/range {v24 .. v24}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v9

    .line 1859
    .local v9, "highlightColor":I
    const/4 v11, 0x0

    .line 1860
    .local v11, "isIndianTextPresent":Z
    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->toCharArray()[C

    move-result-object v6

    .line 1861
    .local v6, "char1":[C
    array-length v0, v6

    move/from16 v24, v0

    move/from16 v0, v24

    new-array v7, v0, [C

    .line 1862
    .local v7, "charNew":[C
    const/4 v14, 0x0

    .line 1865
    .local v14, "newSize":I
    array-length v0, v6

    move/from16 v25, v0

    const/16 v24, 0x0

    :goto_2
    move/from16 v0, v24

    move/from16 v1, v25

    if-lt v0, v1, :cond_4

    .line 1875
    if-eqz v11, :cond_a

    .line 1876
    new-instance v15, Ljava/lang/String;

    const/16 v24, 0x0

    move/from16 v0, v24

    invoke-static {v7, v0, v14}, Ljava/util/Arrays;->copyOfRange([CII)[C

    move-result-object v24

    move-object/from16 v0, v24

    invoke-direct {v15, v0}, Ljava/lang/String;-><init>([C)V

    .line 1877
    .local v15, "newTopLabel":Ljava/lang/String;
    invoke-virtual/range {v23 .. v23}, Landroid/widget/TextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v24

    invoke-virtual/range {v23 .. v23}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v25

    invoke-virtual/range {p2 .. p2}, Ljava/lang/String;->toCharArray()[C

    move-result-object v26

    invoke-static/range {v24 .. v26}, Landroid/text/TextUtils;->getPrefixCharForIndian(Landroid/text/TextPaint;Ljava/lang/CharSequence;[C)[C

    move-result-object v16

    .line 1878
    if-eqz v16, :cond_8

    .line 1879
    new-instance v21, Landroid/text/SpannableStringBuilder;

    move-object/from16 v0, v21

    invoke-direct {v0, v15}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 1880
    .local v21, "spanString":Landroid/text/SpannableStringBuilder;
    new-instance v17, Ljava/lang/String;

    const/16 v24, 0x0

    move-object/from16 v0, v16

    array-length v0, v0

    move/from16 v25, v0

    move-object/from16 v0, v16

    move/from16 v1, v24

    move/from16 v2, v25

    invoke-static {v0, v1, v2}, Ljava/util/Arrays;->copyOfRange([CII)[C

    move-result-object v24

    move-object/from16 v0, v17

    move-object/from16 v1, v24

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>([C)V

    .line 1881
    .local v17, "prefixIndianString":Ljava/lang/String;
    move-object/from16 v0, v17

    invoke-virtual {v15, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v22

    .line 1882
    .local v22, "startpoint":I
    const/4 v8, 0x0

    .line 1883
    .local v8, "endpoint":I
    move-object/from16 v0, v16

    array-length v0, v0

    move/from16 v24, v0

    if-eqz v24, :cond_2

    const/16 v24, -0x1

    move/from16 v0, v22

    move/from16 v1, v24

    if-eq v0, v1, :cond_2

    .line 1884
    move-object/from16 v0, v16

    array-length v0, v0

    move/from16 v24, v0

    add-int v8, v22, v24

    .line 1885
    new-instance v24, Landroid/text/style/ForegroundColorSpan;

    move-object/from16 v0, v24

    invoke-direct {v0, v9}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    .line 1886
    const/16 v25, 0x21

    .line 1885
    move-object/from16 v0, v21

    move-object/from16 v1, v24

    move/from16 v2, v22

    move/from16 v3, v25

    invoke-virtual {v0, v1, v2, v8, v3}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 1888
    :cond_2
    move-object/from16 v0, v23

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 1855
    .end local v6    # "char1":[C
    .end local v7    # "charNew":[C
    .end local v8    # "endpoint":I
    .end local v9    # "highlightColor":I
    .end local v11    # "isIndianTextPresent":Z
    .end local v14    # "newSize":I
    .end local v15    # "newTopLabel":Ljava/lang/String;
    .end local v16    # "prefixForIndian":[C
    .end local v17    # "prefixIndianString":Ljava/lang/String;
    .end local v21    # "spanString":Landroid/text/SpannableStringBuilder;
    .end local v22    # "startpoint":I
    :cond_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;->t:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListItemTag;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    iget-object v0, v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListItemTag;->title:Landroid/widget/TextView;

    move-object/from16 v23, v0

    goto/16 :goto_1

    .line 1865
    .restart local v6    # "char1":[C
    .restart local v7    # "charNew":[C
    .restart local v9    # "highlightColor":I
    .restart local v11    # "isIndianTextPresent":Z
    .restart local v14    # "newSize":I
    .restart local v16    # "prefixForIndian":[C
    :cond_4
    aget-char v5, v6, v24

    .line 1866
    .local v5, "c":C
    move v10, v5

    .line 1867
    .local v10, "i":I
    const/16 v26, 0x200c

    move/from16 v0, v26

    if-eq v10, v0, :cond_7

    .line 1868
    aput-char v5, v7, v14

    .line 1870
    aget-char v26, v7, v14

    invoke-static/range {v26 .. v26}, Lcom/samsung/android/app/memo/util/Utils;->isIndianChar(C)Z

    move-result v26

    if-nez v26, :cond_5

    aget-char v26, v7, v14

    invoke-static/range {v26 .. v26}, Lcom/samsung/android/app/memo/util/Utils;->isKhmerChar(C)Z

    move-result v26

    if-eqz v26, :cond_6

    .line 1871
    :cond_5
    const/4 v11, 0x1

    .line 1872
    :cond_6
    add-int/lit8 v14, v14, 0x1

    .line 1865
    :cond_7
    add-int/lit8 v24, v24, 0x1

    goto/16 :goto_2

    .line 1890
    .end local v5    # "c":C
    .end local v10    # "i":I
    .restart local v15    # "newTopLabel":Ljava/lang/String;
    :cond_8
    invoke-virtual/range {v23 .. v23}, Landroid/widget/TextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v24

    invoke-virtual/range {v23 .. v23}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v25

    invoke-virtual/range {p2 .. p2}, Ljava/lang/String;->toCharArray()[C

    move-result-object v26

    invoke-static/range {v24 .. v26}, Landroid/text/TextUtils;->getPrefixCharForIndian(Landroid/text/TextPaint;Ljava/lang/CharSequence;[C)[C

    move-result-object v16

    .line 1891
    if-eqz v16, :cond_9

    .line 1892
    move-object/from16 v0, v16

    array-length v0, v0

    move/from16 v24, v0

    add-int v18, v19, v24

    .line 1893
    :cond_9
    new-instance v24, Landroid/text/style/ForegroundColorSpan;

    move-object/from16 v0, v24

    invoke-direct {v0, v9}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    const/16 v25, 0x21

    move-object/from16 v0, v20

    move-object/from16 v1, v24

    move/from16 v2, v19

    move/from16 v3, v18

    move/from16 v4, v25

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 1894
    move-object/from16 v0, v23

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 1898
    .end local v15    # "newTopLabel":Ljava/lang/String;
    :cond_a
    new-instance v24, Landroid/text/style/ForegroundColorSpan;

    move-object/from16 v0, v24

    invoke-direct {v0, v9}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    const/16 v25, 0x21

    move-object/from16 v0, v20

    move-object/from16 v1, v24

    move/from16 v2, v19

    move/from16 v3, v18

    move/from16 v4, v25

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 1899
    move-object/from16 v0, v23

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0
.end method

.method private initVars(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 1530
    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->access$1()Ljava/lang/String;

    move-result-object v0

    const-string v1, " initVars"

    invoke-static {v0, v1}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1532
    iget-boolean v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;->isTablet:Z

    if-eqz v0, :cond_0

    .line 1533
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 1534
    const v1, 0x7f09003e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    .line 1533
    iput v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;->memo_list_item_contents_margin_right_selection_mode:I

    .line 1536
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 1537
    const v1, 0x7f09003d

    .line 1536
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;->memo_list_item_contents_margin_right:I

    .line 1539
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 1540
    const v1, 0x7f090040

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    .line 1539
    iput v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;->memo_list_item_title_margin_left_selection_mode:I

    .line 1542
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 1543
    const v1, 0x7f09003c

    .line 1542
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;->memo_list_item_contents_margin_left:I

    .line 1545
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 1546
    const v1, 0x7f09014f

    .line 1545
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;->memo_list_item_contents_height:I

    .line 1549
    :cond_0
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 1550
    const v1, 0x7f09003b

    .line 1549
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;->memo_list_item_contents_margin_top:I

    .line 1552
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 1553
    const v1, 0x7f0a0004

    .line 1552
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;->memo_list_item_max_lines:I

    .line 1555
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 1556
    const v1, 0x7f090042

    .line 1555
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;->memo_list_item_date_time_compound_padding:I

    .line 1558
    const-string v0, "sec-roboto-light"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;->sec_roboto_light:Landroid/graphics/Typeface;

    .line 1560
    return-void
.end method

.method private setHoverPopupContents(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;
    .param p2, "contents"    # Ljava/lang/String;
    .param p3, "noTextContents"    # Ljava/lang/String;

    .prologue
    .line 1904
    sget-boolean v1, Lcom/samsung/android/app/memo/util/Utils;->HAVE_SEC_CUSTOM_HOVER_API:Z

    if-eqz v1, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1905
    const/4 v1, 0x2

    invoke-virtual {p1, v1}, Landroid/view/View;->setHoverPopupType(I)V

    .line 1907
    invoke-virtual {p1}, Landroid/view/View;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v0

    .line 1908
    .local v0, "hpw":Landroid/widget/HoverPopupWindow;
    invoke-virtual {v0, p2}, Landroid/widget/HoverPopupWindow;->setContent(Ljava/lang/CharSequence;)V

    .line 1909
    new-instance v1, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter$3;

    invoke-direct {v1, p0}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter$3;-><init>(Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;)V

    invoke-virtual {p1, v1}, Landroid/view/View;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    .line 1920
    .end local v0    # "hpw":Landroid/widget/HoverPopupWindow;
    :cond_0
    return-void
.end method

.method private setHoverPopupThumb(Landroid/view/View;Ljava/lang/String;)V
    .locals 5
    .param p1, "view"    # Landroid/view/View;
    .param p2, "thumbFilePath"    # Ljava/lang/String;

    .prologue
    .line 1923
    sget-boolean v2, Lcom/samsung/android/app/memo/util/Utils;->HAVE_SEC_CUSTOM_HOVER_API:Z

    if-eqz v2, :cond_1

    invoke-virtual {p1}, Landroid/view/View;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 1924
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    .line 1925
    const v3, 0x7f04001d

    const/4 v4, 0x0

    .line 1924
    invoke-static {v2, v3, v4}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 1927
    .local v1, "hoverView":Landroid/view/View;
    const v2, 0x7f0e003e

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 1928
    .local v0, "hoverImage":Landroid/widget/ImageView;
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1929
    iget-object v2, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;->imageLoader:Lcom/samsung/android/app/memo/util/ImageLoader;

    invoke-virtual {v2, p2, v0}, Lcom/samsung/android/app/memo/util/ImageLoader;->loadImage(Ljava/lang/String;Landroid/widget/ImageView;)V

    .line 1931
    :cond_0
    const/4 v2, 0x3

    invoke-virtual {p1, v2}, Landroid/view/View;->setHoverPopupType(I)V

    .line 1932
    invoke-virtual {p1}, Landroid/view/View;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v2

    new-instance v3, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter$4;

    invoke-direct {v3, p0, v1}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter$4;-><init>(Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;Landroid/view/View;)V

    invoke-virtual {v2, v3}, Landroid/widget/HoverPopupWindow;->setHoverPopupListener(Landroid/widget/HoverPopupWindow$HoverPopupListener;)V

    .line 1942
    .end local v0    # "hoverImage":Landroid/widget/ImageView;
    .end local v1    # "hoverView":Landroid/view/View;
    :cond_1
    return-void
.end method


# virtual methods
.method public bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 28
    .param p1, "view"    # Landroid/view/View;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 1577
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListItemTag;

    .line 1578
    .local v16, "t1":Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListItemTag;
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v23

    check-cast v23, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListItemTag;

    move-object/from16 v0, v23

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;->t:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListItemTag;

    .line 1579
    const/16 v23, 0x4

    move-object/from16 v0, p3

    move/from16 v1, v23

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 1580
    .local v8, "displayName":Ljava/lang/String;
    const/16 v23, 0x5

    move-object/from16 v0, p3

    move/from16 v1, v23

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 1581
    .local v6, "contents":Ljava/lang/String;
    const/16 v23, 0x6

    move-object/from16 v0, p3

    move/from16 v1, v23

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v21

    .line 1582
    .local v21, "voiceData":Ljava/lang/String;
    const/16 v23, 0x7

    move-object/from16 v0, p3

    move/from16 v1, v23

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v23

    move-object/from16 v0, p2

    move-object/from16 v1, v23

    invoke-static {v0, v1}, Lcom/samsung/android/app/memo/util/Utils;->getAbsolutePath(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v18

    .line 1583
    .local v18, "thumbData":Ljava/lang/String;
    const/16 v23, 0x1

    move-object/from16 v0, p3

    move/from16 v1, v23

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v23 .. v23}, Lcom/samsung/android/app/memo/util/FileHelper;->getPrivateFilePath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v19

    .line 1584
    .local v19, "thumbFilePath":Ljava/lang/String;
    const-string v23, " "

    const-string v24, ""

    move-object/from16 v0, v23

    move-object/from16 v1, v24

    invoke-virtual {v6, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    .line 1585
    .local v14, "noTextContents":Ljava/lang/String;
    const/16 v23, 0x2

    move-object/from16 v0, p3

    move/from16 v1, v23

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v24

    invoke-static/range {v24 .. v25}, Lcom/samsung/android/app/memo/util/Utils;->toCurrentTimeOrDate(J)Ljava/lang/String;

    move-result-object v7

    .line 1587
    .local v7, "dateTime":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;->t:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListItemTag;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget-object v0, v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListItemTag;->snippet:Landroid/widget/TextView;

    move-object/from16 v23, v0

    const/16 v24, 0x0

    invoke-virtual/range {v23 .. v24}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1588
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;->t:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListItemTag;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget-object v0, v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListItemTag;->title:Landroid/widget/TextView;

    move-object/from16 v23, v0

    const/16 v24, 0x0

    invoke-virtual/range {v23 .. v24}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1590
    const/16 v23, 0x0

    move-object/from16 v0, p3

    move/from16 v1, v23

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v12

    .line 1591
    .local v12, "memoId":J
    const-wide/16 v24, -0x1

    cmp-long v23, v12, v24

    if-lez v23, :cond_0

    .line 1592
    if-eqz v18, :cond_0

    .line 1593
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    move-object/from16 v23, v0

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mAllThumbHash:Landroid/util/SparseArray;
    invoke-static/range {v23 .. v23}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->access$2(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;)Landroid/util/SparseArray;

    move-result-object v23

    long-to-int v0, v12

    move/from16 v24, v0

    move-object/from16 v0, v23

    move/from16 v1, v24

    move-object/from16 v2, v18

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 1597
    :cond_0
    invoke-virtual {v14}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v23 .. v23}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v23

    if-eqz v23, :cond_7

    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v23

    if-eqz v23, :cond_7

    .line 1598
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;->t:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListItemTag;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget-object v0, v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListItemTag;->title:Landroid/widget/TextView;

    move-object/from16 v23, v0

    .line 1599
    invoke-virtual/range {v23 .. v23}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v20

    .line 1598
    check-cast v20, Landroid/widget/RelativeLayout$LayoutParams;

    .line 1600
    .local v20, "tp":Landroid/widget/RelativeLayout$LayoutParams;
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;->isTablet:Z

    move/from16 v23, v0

    if-eqz v23, :cond_1

    .line 1601
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    move-object/from16 v23, v0

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mIsCheckBoxMode:Z
    invoke-static/range {v23 .. v23}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->access$3(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;)Z

    move-result v23

    if-eqz v23, :cond_5

    invoke-static/range {v18 .. v18}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v23

    if-eqz v23, :cond_5

    .line 1602
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;->memo_list_item_contents_margin_right_selection_mode:I

    move/from16 v23, v0

    move/from16 v0, v23

    move-object/from16 v1, v20

    iput v0, v1, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    .line 1607
    :goto_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    move-object/from16 v23, v0

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mIsCheckBoxMode:Z
    invoke-static/range {v23 .. v23}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->access$3(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;)Z

    move-result v23

    if-eqz v23, :cond_6

    .line 1608
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;->memo_list_item_title_margin_left_selection_mode:I

    move/from16 v23, v0

    move-object/from16 v0, v20

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->setMarginStart(I)V

    .line 1614
    :cond_1
    :goto_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;->t:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListItemTag;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget-object v0, v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListItemTag;->title:Landroid/widget/TextView;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1615
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;->t:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListItemTag;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget-object v0, v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListItemTag;->title:Landroid/widget/TextView;

    move-object/from16 v23, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;->sec_roboto_light:Landroid/graphics/Typeface;

    move-object/from16 v24, v0

    invoke-virtual/range {v23 .. v24}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 1616
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;->t:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListItemTag;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget-object v0, v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListItemTag;->title:Landroid/widget/TextView;

    move-object/from16 v23, v0

    const v24, 0x7f0b003c

    invoke-virtual/range {v23 .. v24}, Landroid/widget/TextView;->setText(I)V

    .line 1617
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;->t:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListItemTag;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget-object v0, v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListItemTag;->snippet:Landroid/widget/TextView;

    move-object/from16 v23, v0

    const/16 v24, 0x8

    invoke-virtual/range {v23 .. v24}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1694
    .end local v20    # "tp":Landroid/widget/RelativeLayout$LayoutParams;
    :goto_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;->t:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListItemTag;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget-object v0, v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListItemTag;->lastModAT:Landroid/widget/TextView;

    move-object/from16 v23, v0

    invoke-virtual {v7}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v23 .. v24}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1695
    invoke-static {}, Lcom/samsung/android/app/memo/util/Utils;->isFonbletHD()Z

    move-result v23

    if-eqz v23, :cond_15

    .line 1696
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;->t:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListItemTag;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget-object v0, v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListItemTag;->lastModAT:Landroid/widget/TextView;

    move-object/from16 v24, v0

    .line 1697
    const/16 v25, 0x0

    .line 1698
    const/16 v26, 0x0

    .line 1699
    invoke-static/range {v21 .. v21}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v23

    if-eqz v23, :cond_14

    const/16 v23, 0x0

    .line 1700
    :goto_3
    const/16 v27, 0x0

    .line 1696
    move-object/from16 v0, v24

    move-object/from16 v1, v25

    move-object/from16 v2, v26

    move-object/from16 v3, v23

    move-object/from16 v4, v27

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/widget/TextView;->setCompoundDrawablesRelativeWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 1708
    :goto_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;->t:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListItemTag;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget-object v0, v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListItemTag;->lastModAT:Landroid/widget/TextView;

    move-object/from16 v23, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;->memo_list_item_date_time_compound_padding:I

    move/from16 v24, v0

    invoke-virtual/range {v23 .. v24}, Landroid/widget/TextView;->setCompoundDrawablePadding(I)V

    .line 1709
    invoke-static/range {v18 .. v18}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v23

    if-eqz v23, :cond_17

    .line 1710
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;->t:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListItemTag;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget-object v0, v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListItemTag;->thumbView:Landroid/widget/ImageView;

    move-object/from16 v23, v0

    const/16 v24, 0x8

    invoke-virtual/range {v23 .. v24}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1716
    :goto_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    move-object/from16 v23, v0

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mIsCheckBoxMode:Z
    invoke-static/range {v23 .. v23}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->access$3(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;)Z

    move-result v23

    if-eqz v23, :cond_1a

    .line 1717
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;->t:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListItemTag;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget-object v0, v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListItemTag;->checkbox_list:Landroid/widget/CheckBox;

    move-object/from16 v23, v0

    const/16 v24, 0x0

    invoke-virtual/range {v23 .. v24}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 1718
    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mCheckBoxIds:Ljava/util/ArrayList;
    invoke-static {}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->access$4()Ljava/util/ArrayList;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/util/ArrayList;->size()I

    move-result v23

    if-lez v23, :cond_19

    .line 1719
    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mCheckBoxIds:Ljava/util/ArrayList;
    invoke-static {}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->access$4()Ljava/util/ArrayList;

    move-result-object v23

    const/16 v24, 0x0

    move-object/from16 v0, p3

    move/from16 v1, v24

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v24

    invoke-static/range {v24 .. v24}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v24

    invoke-virtual/range {v23 .. v24}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v23

    if-eqz v23, :cond_18

    .line 1720
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;->t:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListItemTag;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget-object v0, v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListItemTag;->checkbox_list:Landroid/widget/CheckBox;

    move-object/from16 v23, v0

    const/16 v24, 0x1

    invoke-virtual/range {v23 .. v24}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 1733
    :goto_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    move-object/from16 v23, v0

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mStrQuery:Ljava/lang/String;
    invoke-static/range {v23 .. v23}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->access$5(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;)Ljava/lang/String;

    move-result-object v23

    if-eqz v23, :cond_2

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    move-object/from16 v23, v0

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mStrQuery:Ljava/lang/String;
    invoke-static/range {v23 .. v23}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->access$5(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;)Ljava/lang/String;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/String;->length()I

    move-result v23

    if-lez v23, :cond_2

    .line 1734
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;->highlightSearchResult()V

    .line 1736
    :cond_2
    sget-boolean v23, Lcom/samsung/android/app/memo/util/Utils;->HAVE_SEC_CUSTOM_HOVER_API:Z

    if-eqz v23, :cond_4

    .line 1737
    new-instance v23, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter$1;

    move-object/from16 v0, v23

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter$1;-><init>(Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;)V

    move-object/from16 v0, p1

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    .line 1773
    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListItemTag;->snippet:Landroid/widget/TextView;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Landroid/widget/TextView;->getLayout()Landroid/text/Layout;

    move-result-object v10

    .line 1775
    .local v10, "ly":Landroid/text/Layout;
    if-eqz v10, :cond_1c

    .line 1776
    invoke-virtual {v10}, Landroid/text/Layout;->getLineCount()I

    move-result v23

    add-int/lit8 v23, v23, -0x1

    move/from16 v0, v23

    invoke-virtual {v10, v0}, Landroid/text/Layout;->getEllipsisCount(I)I

    move-result v23

    if-lez v23, :cond_1b

    .line 1777
    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListItemTag;->snippet:Landroid/widget/TextView;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v23

    invoke-interface/range {v23 .. v23}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v17

    .line 1778
    .local v17, "tempContent":Ljava/lang/String;
    const-string v23, " "

    const-string v24, ""

    move-object/from16 v0, v17

    move-object/from16 v1, v23

    move-object/from16 v2, v24

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 1779
    .local v11, "noContents":Ljava/lang/String;
    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListItemTag;->snippet:Landroid/widget/TextView;

    move-object/from16 v23, v0

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListItemTag;->snippet:Landroid/widget/TextView;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v24

    invoke-interface/range {v24 .. v24}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, p0

    move-object/from16 v1, v23

    move-object/from16 v2, v24

    invoke-direct {v0, v1, v2, v11}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;->setHoverPopupContents(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;)V

    .line 1808
    .end local v11    # "noContents":Ljava/lang/String;
    .end local v17    # "tempContent":Ljava/lang/String;
    :cond_3
    :goto_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;->t:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListItemTag;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget-object v0, v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListItemTag;->thumbView:Landroid/widget/ImageView;

    move-object/from16 v23, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v23

    move-object/from16 v2, v19

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;->setHoverPopupThumb(Landroid/view/View;Ljava/lang/String;)V

    .line 1810
    .end local v10    # "ly":Landroid/text/Layout;
    :cond_4
    return-void

    .line 1604
    .restart local v20    # "tp":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_5
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;->memo_list_item_contents_margin_right:I

    move/from16 v23, v0

    move/from16 v0, v23

    move-object/from16 v1, v20

    iput v0, v1, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    goto/16 :goto_0

    .line 1610
    :cond_6
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;->memo_list_item_contents_margin_left:I

    move/from16 v23, v0

    move-object/from16 v0, v20

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->setMarginStart(I)V

    goto/16 :goto_1

    .line 1618
    .end local v20    # "tp":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_7
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;->isTablet:Z

    move/from16 v23, v0

    if-nez v23, :cond_e

    .line 1619
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;->t:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListItemTag;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget-object v0, v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListItemTag;->snippet:Landroid/widget/TextView;

    move-object/from16 v23, v0

    .line 1620
    invoke-virtual/range {v23 .. v23}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v9

    .line 1619
    check-cast v9, Landroid/widget/RelativeLayout$LayoutParams;

    .line 1621
    .local v9, "lp":Landroid/widget/RelativeLayout$LayoutParams;
    invoke-static {}, Lcom/samsung/android/app/memo/util/Utils;->isFonbletHD()Z

    move-result v23

    if-eqz v23, :cond_9

    .line 1622
    const/16 v23, 0x0

    move/from16 v0, v23

    iput v0, v9, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 1626
    :goto_8
    invoke-virtual {v8}, Ljava/lang/String;->isEmpty()Z

    move-result v23

    if-eqz v23, :cond_c

    .line 1627
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;->t:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListItemTag;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget-object v0, v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListItemTag;->snippet:Landroid/widget/TextView;

    move-object/from16 v23, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;->memo_list_item_max_lines:I

    move/from16 v24, v0

    add-int/lit8 v24, v24, 0x1

    invoke-virtual/range {v23 .. v24}, Landroid/widget/TextView;->setMaxLines(I)V

    .line 1628
    invoke-virtual {v14}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/String;->isEmpty()Z

    move-result v23

    if-eqz v23, :cond_b

    .line 1629
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;->t:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListItemTag;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget-object v0, v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListItemTag;->title:Landroid/widget/TextView;

    move-object/from16 v23, v0

    const v24, 0x7f0b003c

    invoke-virtual/range {v23 .. v24}, Landroid/widget/TextView;->setText(I)V

    .line 1639
    :goto_9
    invoke-virtual {v8}, Ljava/lang/String;->isEmpty()Z

    move-result v23

    if-nez v23, :cond_8

    .line 1640
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;->t:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListItemTag;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget-object v0, v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListItemTag;->title:Landroid/widget/TextView;

    move-object/from16 v23, v0

    invoke-virtual {v8}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v23 .. v24}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1642
    :cond_8
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;->t:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListItemTag;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget-object v0, v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListItemTag;->snippet:Landroid/widget/TextView;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    invoke-virtual {v0, v9}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1643
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;->t:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListItemTag;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget-object v0, v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListItemTag;->snippet:Landroid/widget/TextView;

    move-object/from16 v23, v0

    invoke-virtual {v6}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v23 .. v24}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1644
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;->t:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListItemTag;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget-object v0, v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListItemTag;->title:Landroid/widget/TextView;

    move-object/from16 v23, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;->sec_roboto_light:Landroid/graphics/Typeface;

    move-object/from16 v24, v0

    invoke-virtual/range {v23 .. v24}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    goto/16 :goto_2

    .line 1624
    :cond_9
    invoke-virtual {v8}, Ljava/lang/String;->isEmpty()Z

    move-result v23

    if-nez v23, :cond_a

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;->memo_list_item_contents_margin_top:I

    move/from16 v23, v0

    :goto_a
    move/from16 v0, v23

    iput v0, v9, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    goto/16 :goto_8

    :cond_a
    const/16 v23, 0x0

    goto :goto_a

    .line 1631
    :cond_b
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;->t:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListItemTag;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget-object v0, v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListItemTag;->title:Landroid/widget/TextView;

    move-object/from16 v23, v0

    const/16 v24, 0x8

    invoke-virtual/range {v23 .. v24}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_9

    .line 1633
    :cond_c
    invoke-virtual {v6}, Ljava/lang/String;->isEmpty()Z

    move-result v23

    if-eqz v23, :cond_d

    .line 1634
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;->t:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListItemTag;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget-object v0, v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListItemTag;->snippet:Landroid/widget/TextView;

    move-object/from16 v23, v0

    const/16 v24, 0x8

    invoke-virtual/range {v23 .. v24}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_9

    .line 1636
    :cond_d
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;->t:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListItemTag;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget-object v0, v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListItemTag;->snippet:Landroid/widget/TextView;

    move-object/from16 v23, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;->memo_list_item_max_lines:I

    move/from16 v24, v0

    invoke-virtual/range {v23 .. v24}, Landroid/widget/TextView;->setMaxLines(I)V

    goto/16 :goto_9

    .line 1647
    .end local v9    # "lp":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_e
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;->t:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListItemTag;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget-object v0, v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListItemTag;->snippet:Landroid/widget/TextView;

    move-object/from16 v23, v0

    invoke-virtual {v6}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v23 .. v24}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1648
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;->t:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListItemTag;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget-object v0, v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListItemTag;->snippet:Landroid/widget/TextView;

    move-object/from16 v23, v0

    .line 1649
    invoke-virtual/range {v23 .. v23}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v9

    .line 1648
    check-cast v9, Landroid/widget/RelativeLayout$LayoutParams;

    .line 1650
    .restart local v9    # "lp":Landroid/widget/RelativeLayout$LayoutParams;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;->t:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListItemTag;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget-object v0, v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListItemTag;->title:Landroid/widget/TextView;

    move-object/from16 v23, v0

    .line 1651
    invoke-virtual/range {v23 .. v23}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v20

    .line 1650
    check-cast v20, Landroid/widget/RelativeLayout$LayoutParams;

    .line 1652
    .restart local v20    # "tp":Landroid/widget/RelativeLayout$LayoutParams;
    invoke-virtual {v8}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v23

    const-string v24, ""

    invoke-virtual/range {v23 .. v24}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-nez v23, :cond_10

    .line 1653
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;->t:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListItemTag;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget-object v0, v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListItemTag;->title:Landroid/widget/TextView;

    move-object/from16 v23, v0

    invoke-virtual {v8}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v23 .. v24}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1654
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;->t:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListItemTag;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget-object v0, v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListItemTag;->snippet:Landroid/widget/TextView;

    move-object/from16 v23, v0

    const/16 v24, 0x3

    invoke-virtual/range {v23 .. v24}, Landroid/widget/TextView;->setMaxLines(I)V

    .line 1655
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;->t:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListItemTag;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget-object v0, v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListItemTag;->snippet:Landroid/widget/TextView;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v23

    check-cast v23, Landroid/widget/RelativeLayout$LayoutParams;

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;->memo_list_item_contents_margin_top:I

    move/from16 v24, v0

    move/from16 v0, v24

    move-object/from16 v1, v23

    iput v0, v1, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 1656
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;->t:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListItemTag;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget-object v0, v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListItemTag;->snippet:Landroid/widget/TextView;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v23

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;->memo_list_item_contents_height:I

    move/from16 v24, v0

    move/from16 v0, v24

    move-object/from16 v1, v23

    iput v0, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 1664
    :goto_b
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;->isTablet:Z

    move/from16 v23, v0

    if-eqz v23, :cond_f

    .line 1665
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    move-object/from16 v23, v0

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mIsCheckBoxMode:Z
    invoke-static/range {v23 .. v23}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->access$3(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;)Z

    move-result v23

    if-eqz v23, :cond_11

    invoke-static/range {v18 .. v18}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v23

    if-eqz v23, :cond_11

    .line 1666
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;->memo_list_item_contents_margin_right_selection_mode:I

    move/from16 v23, v0

    move/from16 v0, v23

    iput v0, v9, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    .line 1670
    :goto_c
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    move-object/from16 v23, v0

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mIsCheckBoxMode:Z
    invoke-static/range {v23 .. v23}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->access$3(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;)Z

    move-result v23

    if-eqz v23, :cond_13

    .line 1671
    invoke-virtual {v8}, Ljava/lang/String;->isEmpty()Z

    move-result v23

    if-eqz v23, :cond_12

    .line 1672
    new-instance v15, Landroid/text/SpannableStringBuilder;

    invoke-virtual {v6}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-direct {v15, v0}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 1673
    .local v15, "s":Landroid/text/SpannableStringBuilder;
    new-instance v23, Landroid/text/style/LeadingMarginSpan$Standard;

    const/16 v24, 0x21

    const/16 v25, 0x0

    invoke-direct/range {v23 .. v25}, Landroid/text/style/LeadingMarginSpan$Standard;-><init>(II)V

    const/16 v24, 0x0

    .line 1674
    const/16 v25, 0x1

    const/16 v26, 0x0

    .line 1673
    move-object/from16 v0, v23

    move/from16 v1, v24

    move/from16 v2, v25

    move/from16 v3, v26

    invoke-virtual {v15, v0, v1, v2, v3}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 1675
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;->t:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListItemTag;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget-object v0, v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListItemTag;->snippet:Landroid/widget/TextView;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    invoke-virtual {v0, v15}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1684
    .end local v15    # "s":Landroid/text/SpannableStringBuilder;
    :cond_f
    :goto_d
    iget v0, v9, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    move/from16 v23, v0

    move/from16 v0, v23

    move-object/from16 v1, v20

    iput v0, v1, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    .line 1685
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;->t:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListItemTag;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget-object v0, v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListItemTag;->title:Landroid/widget/TextView;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1686
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;->t:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListItemTag;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget-object v0, v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListItemTag;->snippet:Landroid/widget/TextView;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    invoke-virtual {v0, v9}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1687
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;->t:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListItemTag;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget-object v0, v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListItemTag;->title:Landroid/widget/TextView;

    move-object/from16 v23, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;->sec_roboto_light:Landroid/graphics/Typeface;

    move-object/from16 v24, v0

    invoke-virtual/range {v23 .. v24}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 1688
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;->t:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListItemTag;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget-object v0, v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListItemTag;->title:Landroid/widget/TextView;

    move-object/from16 v23, v0

    const/16 v24, 0x0

    invoke-virtual/range {p2 .. p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v25

    .line 1689
    const v26, 0x7f090047

    invoke-virtual/range {v25 .. v26}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v25

    .line 1688
    invoke-virtual/range {v23 .. v25}, Landroid/widget/TextView;->setTextSize(IF)V

    goto/16 :goto_2

    .line 1658
    :cond_10
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;->t:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListItemTag;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget-object v0, v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListItemTag;->title:Landroid/widget/TextView;

    move-object/from16 v23, v0

    const/16 v24, 0x8

    invoke-virtual/range {v23 .. v24}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1659
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;->t:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListItemTag;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget-object v0, v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListItemTag;->title:Landroid/widget/TextView;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v23

    check-cast v23, Landroid/widget/RelativeLayout$LayoutParams;

    move-object/from16 v0, v23

    iget v0, v0, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    move/from16 v23, v0

    move/from16 v0, v23

    iput v0, v9, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 1660
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;->t:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListItemTag;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget-object v0, v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListItemTag;->snippet:Landroid/widget/TextView;

    move-object/from16 v23, v0

    const/16 v24, 0x4

    invoke-virtual/range {v23 .. v24}, Landroid/widget/TextView;->setMaxLines(I)V

    .line 1661
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;->memo_list_item_contents_height:I

    move/from16 v23, v0

    .line 1662
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;->memo_list_item_contents_height:I

    move/from16 v24, v0

    div-int/lit8 v24, v24, 0x3

    add-int v23, v23, v24

    .line 1661
    move/from16 v0, v23

    iput v0, v9, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    goto/16 :goto_b

    .line 1668
    :cond_11
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;->memo_list_item_contents_margin_right:I

    move/from16 v23, v0

    move/from16 v0, v23

    iput v0, v9, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    goto/16 :goto_c

    .line 1677
    :cond_12
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;->memo_list_item_title_margin_left_selection_mode:I

    move/from16 v23, v0

    move-object/from16 v0, v20

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->setMarginStart(I)V

    goto/16 :goto_d

    .line 1680
    :cond_13
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;->memo_list_item_contents_margin_left:I

    move/from16 v23, v0

    move/from16 v0, v23

    invoke-virtual {v9, v0}, Landroid/widget/RelativeLayout$LayoutParams;->setMarginStart(I)V

    .line 1681
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;->memo_list_item_contents_margin_left:I

    move/from16 v23, v0

    move-object/from16 v0, v20

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->setMarginStart(I)V

    goto/16 :goto_d

    .line 1699
    .end local v9    # "lp":Landroid/widget/RelativeLayout$LayoutParams;
    .end local v20    # "tp":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_14
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v23

    .line 1700
    const v27, 0x7f02006a

    .line 1699
    move-object/from16 v0, v23

    move/from16 v1, v27

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v23

    goto/16 :goto_3

    .line 1702
    :cond_15
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;->t:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListItemTag;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget-object v0, v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListItemTag;->lastModAT:Landroid/widget/TextView;

    move-object/from16 v24, v0

    .line 1703
    const/16 v25, 0x0

    .line 1704
    const/16 v26, 0x0

    .line 1705
    invoke-static/range {v21 .. v21}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v23

    if-eqz v23, :cond_16

    const/16 v23, 0x0

    .line 1706
    :goto_e
    const/16 v27, 0x0

    .line 1702
    move-object/from16 v0, v24

    move-object/from16 v1, v25

    move-object/from16 v2, v26

    move-object/from16 v3, v23

    move-object/from16 v4, v27

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/widget/TextView;->setCompoundDrawablesRelativeWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_4

    .line 1705
    :cond_16
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v23

    .line 1706
    const v27, 0x7f020069

    .line 1705
    move-object/from16 v0, v23

    move/from16 v1, v27

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v23

    goto :goto_e

    .line 1712
    :cond_17
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;->t:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListItemTag;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget-object v0, v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListItemTag;->thumbView:Landroid/widget/ImageView;

    move-object/from16 v23, v0

    const/16 v24, 0x0

    invoke-virtual/range {v23 .. v24}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1713
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;->imageLoader:Lcom/samsung/android/app/memo/util/ImageLoader;

    move-object/from16 v23, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;->t:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListItemTag;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    iget-object v0, v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListItemTag;->thumbView:Landroid/widget/ImageView;

    move-object/from16 v24, v0

    move-object/from16 v0, v23

    move-object/from16 v1, v18

    move-object/from16 v2, v24

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/app/memo/util/ImageLoader;->loadImage(Ljava/lang/String;Landroid/widget/ImageView;)V

    goto/16 :goto_5

    .line 1722
    :cond_18
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;->t:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListItemTag;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget-object v0, v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListItemTag;->checkbox_list:Landroid/widget/CheckBox;

    move-object/from16 v23, v0

    const/16 v24, 0x0

    invoke-virtual/range {v23 .. v24}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto/16 :goto_6

    .line 1725
    :cond_19
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;->t:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListItemTag;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget-object v0, v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListItemTag;->checkbox_list:Landroid/widget/CheckBox;

    move-object/from16 v23, v0

    const/16 v24, 0x0

    invoke-virtual/range {v23 .. v24}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto/16 :goto_6

    .line 1728
    :cond_1a
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;->t:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListItemTag;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget-object v0, v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListItemTag;->checkbox_list:Landroid/widget/CheckBox;

    move-object/from16 v23, v0

    const/16 v24, 0x0

    invoke-virtual/range {v23 .. v24}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 1729
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;->t:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListItemTag;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget-object v0, v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListItemTag;->checkbox_list:Landroid/widget/CheckBox;

    move-object/from16 v23, v0

    const/16 v24, 0x8

    invoke-virtual/range {v23 .. v24}, Landroid/widget/CheckBox;->setVisibility(I)V

    goto/16 :goto_6

    .line 1781
    .restart local v10    # "ly":Landroid/text/Layout;
    :cond_1b
    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListItemTag;->mParentView:Landroid/view/View;

    move-object/from16 v23, v0

    if-eqz v23, :cond_3

    .line 1782
    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListItemTag;->mParentView:Landroid/view/View;

    move-object/from16 v23, v0

    const/16 v24, 0x0

    invoke-virtual/range {v23 .. v24}, Landroid/view/View;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    .line 1783
    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListItemTag;->mParentView:Landroid/view/View;

    move-object/from16 v23, v0

    const/16 v24, 0x0

    invoke-virtual/range {v23 .. v24}, Landroid/view/View;->setHovered(Z)V

    goto/16 :goto_7

    .line 1786
    :cond_1c
    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListItemTag;->snippet:Landroid/widget/TextView;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Landroid/widget/TextView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v22

    .line 1787
    .local v22, "vto":Landroid/view/ViewTreeObserver;
    new-instance v23, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter$2;

    move-object/from16 v0, v23

    move-object/from16 v1, p0

    move-object/from16 v2, v16

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter$2;-><init>(Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListItemTag;)V

    invoke-virtual/range {v22 .. v23}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    goto/16 :goto_7
.end method

.method public doCacheDelete(Ljava/lang/String;)V
    .locals 3
    .param p1, "data"    # Ljava/lang/String;

    .prologue
    .line 1997
    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->access$1()Ljava/lang/String;

    move-result-object v1

    const-string v2, "doCacheDelete"

    invoke-static {v1, v2}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1998
    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    invoke-virtual {v1}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    invoke-virtual {v1}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->isFinishing()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;->imageLoader:Lcom/samsung/android/app/memo/util/ImageLoader;

    if-eqz v1, :cond_0

    .line 1999
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 2000
    .local v0, "i":Landroid/content/Intent;
    const-string v1, "com.samsung.android.app.memo.UPDATE_THUMB_IMAGE"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 2001
    const-string v1, "_data"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2002
    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    invoke-virtual {v1}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/app/Activity;->sendBroadcast(Landroid/content/Intent;)V

    .line 2004
    .end local v0    # "i":Landroid/content/Intent;
    :cond_0
    return-void
.end method

.method public getSelectedIds()Landroid/util/SparseBooleanArray;
    .locals 1

    .prologue
    .line 1976
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;->mSelectedItemsIds:Landroid/util/SparseBooleanArray;

    return-object v0
.end method

.method public newView(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "cursor"    # Landroid/database/Cursor;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const/4 v3, 0x0

    .line 1946
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 1948
    .local v0, "inflater":Landroid/view/LayoutInflater;
    invoke-static {}, Lcom/samsung/android/app/memo/util/Utils;->isFonbletHD()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1949
    const v2, 0x7f04001f

    invoke-virtual {v0, v2, p3, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 1953
    .local v1, "v":Landroid/view/View;
    :goto_0
    invoke-static {v1}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListItemTag;->setTag(Landroid/view/View;)V

    .line 1954
    return-object v1

    .line 1951
    .end local v1    # "v":Landroid/view/View;
    :cond_0
    const v2, 0x7f04001e

    invoke-virtual {v0, v2, p3, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .restart local v1    # "v":Landroid/view/View;
    goto :goto_0
.end method

.method public removeDeletedMemoCache()V
    .locals 5

    .prologue
    .line 1980
    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->access$1()Ljava/lang/String;

    move-result-object v3

    const-string v4, "doCacheDelete"

    invoke-static {v3, v4}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1982
    const/4 v2, 0x0

    .line 1984
    .local v2, "path":Ljava/lang/String;
    iget-object v3, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    invoke-virtual {v3}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    invoke-virtual {v3}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/Activity;->isFinishing()Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mIDsForCacheDelete:Ljava/util/ArrayList;
    invoke-static {v3}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->access$7(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;)Ljava/util/ArrayList;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 1985
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v3, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mIDsForCacheDelete:Ljava/util/ArrayList;
    invoke-static {v3}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->access$7(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-lt v0, v3, :cond_1

    .line 1994
    .end local v0    # "i":I
    :cond_0
    return-void

    .line 1986
    .restart local v0    # "i":I
    :cond_1
    const/4 v2, 0x0

    .line 1987
    iget-object v3, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mIDsForCacheDelete:Ljava/util/ArrayList;
    invoke-static {v3}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->access$7(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 1988
    .local v1, "id":I
    const/4 v3, -0x1

    if-le v1, v3, :cond_2

    .line 1989
    iget-object v3, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mAllThumbHash:Landroid/util/SparseArray;
    invoke-static {v3}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->access$2(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;)Landroid/util/SparseArray;

    move-result-object v3

    invoke-virtual {v3, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v2

    .end local v2    # "path":Ljava/lang/String;
    check-cast v2, Ljava/lang/String;

    .line 1990
    .restart local v2    # "path":Ljava/lang/String;
    :cond_2
    if-eqz v2, :cond_3

    .line 1991
    invoke-virtual {p0, v2}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;->doCacheDelete(Ljava/lang/String;)V

    .line 1985
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public removeSelection()V
    .locals 1

    .prologue
    .line 1963
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;->mSelectedItemsIds:Landroid/util/SparseBooleanArray;

    invoke-virtual {v0}, Landroid/util/SparseBooleanArray;->clear()V

    .line 1964
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;->notifyDataSetChanged()V

    .line 1965
    return-void
.end method

.method public selectView(IZ)V
    .locals 1
    .param p1, "position"    # I
    .param p2, "value"    # Z

    .prologue
    .line 1968
    if-eqz p2, :cond_0

    .line 1969
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;->mSelectedItemsIds:Landroid/util/SparseBooleanArray;

    invoke-virtual {v0, p1, p2}, Landroid/util/SparseBooleanArray;->put(IZ)V

    .line 1973
    :goto_0
    return-void

    .line 1971
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;->mSelectedItemsIds:Landroid/util/SparseBooleanArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseBooleanArray;->delete(I)V

    goto :goto_0
.end method

.method public toggleSelection(I)V
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 1958
    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->access$1()Ljava/lang/String;

    move-result-object v0

    const-string v1, "onLoaderReset"

    invoke-static {v0, v1}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1959
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;->mSelectedItemsIds:Landroid/util/SparseBooleanArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseBooleanArray;->get(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0, p1, v0}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;->selectView(IZ)V

    .line 1960
    return-void

    .line 1959
    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method
