.class public Lcom/samsung/android/app/memo/util/ImageLoader$CacheAsyncTask;
.super Landroid/os/AsyncTask;
.source "ImageLoader.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/memo/util/ImageLoader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4
    name = "CacheAsyncTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Object;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/memo/util/ImageLoader;


# direct methods
.method protected constructor <init>(Lcom/samsung/android/app/memo/util/ImageLoader;)V
    .locals 0

    .prologue
    .line 283
    iput-object p1, p0, Lcom/samsung/android/app/memo/util/ImageLoader$CacheAsyncTask;->this$0:Lcom/samsung/android/app/memo/util/ImageLoader;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected bridge varargs synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1
    check-cast p1, [Ljava/lang/Object;

    invoke-virtual {p0, p1}, Lcom/samsung/android/app/memo/util/ImageLoader$CacheAsyncTask;->doInBackground([Ljava/lang/Object;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Object;)Ljava/lang/Void;
    .locals 1
    .param p1, "params"    # [Ljava/lang/Object;

    .prologue
    .line 287
    const/4 v0, 0x0

    aget-object v0, p1, v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 295
    :goto_0
    const/4 v0, 0x0

    return-object v0

    .line 289
    :pswitch_0
    iget-object v0, p0, Lcom/samsung/android/app/memo/util/ImageLoader$CacheAsyncTask;->this$0:Lcom/samsung/android/app/memo/util/ImageLoader;

    invoke-virtual {v0}, Lcom/samsung/android/app/memo/util/ImageLoader;->clearCacheInternal()V

    goto :goto_0

    .line 292
    :pswitch_1
    iget-object v0, p0, Lcom/samsung/android/app/memo/util/ImageLoader$CacheAsyncTask;->this$0:Lcom/samsung/android/app/memo/util/ImageLoader;

    invoke-virtual {v0}, Lcom/samsung/android/app/memo/util/ImageLoader;->closeCacheInternal()V

    goto :goto_0

    .line 287
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
