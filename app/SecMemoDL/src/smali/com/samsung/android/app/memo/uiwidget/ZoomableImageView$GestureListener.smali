.class public Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView$GestureListener;
.super Landroid/view/GestureDetector$SimpleOnGestureListener;
.source "ZoomableImageView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "GestureListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView;


# direct methods
.method public constructor <init>(Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView;)V
    .locals 0

    .prologue
    .line 278
    iput-object p1, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView$GestureListener;->this$0:Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView;

    invoke-direct {p0}, Landroid/view/GestureDetector$SimpleOnGestureListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onDoubleTap(Landroid/view/MotionEvent;)Z
    .locals 6
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    .line 292
    const-string v2, "ImageViewTouchBase"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "onDoubleTap. double tap enabled? "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView$GestureListener;->this$0:Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView;

    iget-boolean v4, v4, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView;->mDoubleTapEnabled:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 293
    iget-object v2, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView$GestureListener;->this$0:Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView;

    iget-boolean v2, v2, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView;->mDoubleTapEnabled:Z

    if-eqz v2, :cond_0

    .line 294
    iget-object v2, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView$GestureListener;->this$0:Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView;

    const/4 v3, 0x1

    iput-boolean v3, v2, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView;->mUserScaled:Z

    .line 295
    iget-object v2, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView$GestureListener;->this$0:Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView;

    invoke-virtual {v2}, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView;->getScale()F

    move-result v0

    .line 296
    .local v0, "scale":F
    move v1, v0

    .line 297
    .local v1, "targetScale":F
    iget-object v2, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView$GestureListener;->this$0:Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView;

    iget-object v3, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView$GestureListener;->this$0:Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView;

    invoke-virtual {v3}, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView;->getMaxScale()F

    move-result v3

    invoke-virtual {v2, v0, v3}, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView;->onDoubleTapPost(FF)F

    move-result v1

    .line 298
    iget-object v2, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView$GestureListener;->this$0:Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView;

    invoke-virtual {v2}, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView;->getMaxScale()F

    move-result v2

    iget-object v3, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView$GestureListener;->this$0:Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView;

    invoke-virtual {v3}, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView;->getMinScale()F

    move-result v3

    invoke-static {v1, v3}, Ljava/lang/Math;->max(FF)F

    move-result v3

    invoke-static {v2, v3}, Ljava/lang/Math;->min(FF)F

    move-result v1

    .line 299
    iget-object v2, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView$GestureListener;->this$0:Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    const/high16 v5, 0x43480000    # 200.0f

    invoke-virtual {v2, v1, v3, v4, v5}, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView;->zoomTo(FFFF)V

    .line 300
    iget-object v2, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView$GestureListener;->this$0:Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView;

    invoke-virtual {v2}, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView;->invalidate()V

    .line 303
    .end local v0    # "scale":F
    .end local v1    # "targetScale":F
    :cond_0
    iget-object v2, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView$GestureListener;->this$0:Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView;

    # getter for: Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView;->mDoubleTapListener:Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView$OnImageViewTouchDoubleTapListener;
    invoke-static {v2}, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView;->access$2(Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView;)Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView$OnImageViewTouchDoubleTapListener;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 304
    iget-object v2, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView$GestureListener;->this$0:Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView;

    # getter for: Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView;->mDoubleTapListener:Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView$OnImageViewTouchDoubleTapListener;
    invoke-static {v2}, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView;->access$2(Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView;)Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView$OnImageViewTouchDoubleTapListener;

    move-result-object v2

    invoke-interface {v2}, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView$OnImageViewTouchDoubleTapListener;->onDoubleTap()V

    .line 307
    :cond_1
    invoke-super {p0, p1}, Landroid/view/GestureDetector$SimpleOnGestureListener;->onDoubleTap(Landroid/view/MotionEvent;)Z

    move-result v2

    return v2
.end method

.method public onDown(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    .line 356
    iget-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView$GestureListener;->this$0:Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView;

    invoke-virtual {v0, p1}, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView;->onDown(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method public onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 3
    .param p1, "e1"    # Landroid/view/MotionEvent;
    .param p2, "e2"    # Landroid/view/MotionEvent;
    .param p3, "velocityX"    # F
    .param p4, "velocityY"    # F

    .prologue
    const/4 v2, 0x1

    const/4 v0, 0x0

    .line 336
    iget-object v1, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView$GestureListener;->this$0:Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView;

    iget-boolean v1, v1, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView;->mScrollEnabled:Z

    if-nez v1, :cond_1

    .line 346
    :cond_0
    :goto_0
    return v0

    .line 339
    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v1

    if-gt v1, v2, :cond_0

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v1

    if-gt v1, v2, :cond_0

    .line 341
    iget-object v1, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView$GestureListener;->this$0:Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView;

    iget-object v1, v1, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView;->mScaleDetector:Landroid/view/ScaleGestureDetector;

    invoke-virtual {v1}, Landroid/view/ScaleGestureDetector;->isInProgress()Z

    move-result v1

    if-nez v1, :cond_0

    .line 343
    iget-object v1, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView$GestureListener;->this$0:Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView;

    invoke-virtual {v1}, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView;->getScale()F

    move-result v1

    const/high16 v2, 0x3f800000    # 1.0f

    cmpl-float v1, v1, v2

    if-eqz v1, :cond_0

    .line 346
    iget-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView$GestureListener;->this$0:Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView;->onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z

    move-result v0

    goto :goto_0
.end method

.method public onLongPress(Landroid/view/MotionEvent;)V
    .locals 2
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    .line 312
    iget-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView$GestureListener;->this$0:Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView;

    invoke-virtual {v0}, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView;->isLongClickable()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 313
    iget-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView$GestureListener;->this$0:Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView;

    iget-object v0, v0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView;->mScaleDetector:Landroid/view/ScaleGestureDetector;

    invoke-virtual {v0}, Landroid/view/ScaleGestureDetector;->isInProgress()Z

    move-result v0

    if-nez v0, :cond_0

    .line 314
    iget-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView$GestureListener;->this$0:Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView;->setPressed(Z)V

    .line 315
    iget-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView$GestureListener;->this$0:Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView;

    invoke-virtual {v0}, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView;->performLongClick()Z

    .line 318
    :cond_0
    return-void
.end method

.method public onScroll(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 3
    .param p1, "e1"    # Landroid/view/MotionEvent;
    .param p2, "e2"    # Landroid/view/MotionEvent;
    .param p3, "distanceX"    # F
    .param p4, "distanceY"    # F

    .prologue
    const/4 v2, 0x1

    const/4 v0, 0x0

    .line 323
    iget-object v1, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView$GestureListener;->this$0:Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView;

    iget-boolean v1, v1, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView;->mScrollEnabled:Z

    if-nez v1, :cond_1

    .line 331
    :cond_0
    :goto_0
    return v0

    .line 325
    :cond_1
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    .line 327
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v1

    if-gt v1, v2, :cond_0

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v1

    if-gt v1, v2, :cond_0

    .line 329
    iget-object v1, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView$GestureListener;->this$0:Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView;

    iget-object v1, v1, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView;->mScaleDetector:Landroid/view/ScaleGestureDetector;

    invoke-virtual {v1}, Landroid/view/ScaleGestureDetector;->isInProgress()Z

    move-result v1

    if-nez v1, :cond_0

    .line 331
    iget-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView$GestureListener;->this$0:Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView;->onScroll(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z

    move-result v0

    goto :goto_0
.end method

.method public onSingleTapConfirmed(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    .line 283
    iget-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView$GestureListener;->this$0:Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView;

    # getter for: Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView;->mSingleTapListener:Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView$OnImageViewTouchSingleTapListener;
    invoke-static {v0}, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView;->access$1(Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView;)Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView$OnImageViewTouchSingleTapListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 284
    iget-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView$GestureListener;->this$0:Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView;

    # getter for: Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView;->mSingleTapListener:Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView$OnImageViewTouchSingleTapListener;
    invoke-static {v0}, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView;->access$1(Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView;)Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView$OnImageViewTouchSingleTapListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView$OnImageViewTouchSingleTapListener;->onSingleTapConfirmed()V

    .line 287
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView$GestureListener;->this$0:Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView;

    invoke-virtual {v0, p1}, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView;->onSingleTapConfirmed(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method public onSingleTapUp(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    .line 351
    iget-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView$GestureListener;->this$0:Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView;

    invoke-virtual {v0, p1}, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView;->onSingleTapUp(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method
