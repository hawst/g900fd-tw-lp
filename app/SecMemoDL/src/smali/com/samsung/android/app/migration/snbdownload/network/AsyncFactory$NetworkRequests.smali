.class Lcom/samsung/android/app/migration/snbdownload/network/AsyncFactory$NetworkRequests;
.super Landroid/os/AsyncTask;
.source "AsyncFactory.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/migration/snbdownload/network/AsyncFactory;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "NetworkRequests"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Lcom/samsung/android/app/migration/snbdownload/network/AsyncFactory$Request;",
        "Ljava/lang/Integer;",
        "Lcom/samsung/android/app/migration/snbdownload/network/AsyncFactory$Response;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/migration/snbdownload/network/AsyncFactory;


# direct methods
.method private constructor <init>(Lcom/samsung/android/app/migration/snbdownload/network/AsyncFactory;)V
    .locals 0

    .prologue
    .line 113
    iput-object p1, p0, Lcom/samsung/android/app/migration/snbdownload/network/AsyncFactory$NetworkRequests;->this$0:Lcom/samsung/android/app/migration/snbdownload/network/AsyncFactory;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/samsung/android/app/migration/snbdownload/network/AsyncFactory;Lcom/samsung/android/app/migration/snbdownload/network/AsyncFactory$NetworkRequests;)V
    .locals 0

    .prologue
    .line 113
    invoke-direct {p0, p1}, Lcom/samsung/android/app/migration/snbdownload/network/AsyncFactory$NetworkRequests;-><init>(Lcom/samsung/android/app/migration/snbdownload/network/AsyncFactory;)V

    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Lcom/samsung/android/app/migration/snbdownload/network/AsyncFactory$Request;)Lcom/samsung/android/app/migration/snbdownload/network/AsyncFactory$Response;
    .locals 9
    .param p1, "mRequest"    # [Lcom/samsung/android/app/migration/snbdownload/network/AsyncFactory$Request;

    .prologue
    const/4 v8, 0x1

    .line 117
    const/4 v4, 0x0

    aget-object v1, p1, v4

    .line 118
    .local v1, "req":Lcom/samsung/android/app/migration/snbdownload/network/AsyncFactory$Request;
    new-instance v3, Lcom/samsung/android/app/migration/snbdownload/network/downsync/util/SharedInfo;

    invoke-direct {v3}, Lcom/samsung/android/app/migration/snbdownload/network/downsync/util/SharedInfo;-><init>()V

    .line 119
    .local v3, "result":Lcom/samsung/android/app/migration/snbdownload/network/downsync/util/SharedInfo;
    new-instance v2, Lcom/samsung/android/app/migration/snbdownload/network/AsyncFactory$Response;

    iget v4, v1, Lcom/samsung/android/app/migration/snbdownload/network/AsyncFactory$Request;->req_type:I

    invoke-direct {v2, v3, v4}, Lcom/samsung/android/app/migration/snbdownload/network/AsyncFactory$Response;-><init>(Lcom/samsung/android/app/migration/snbdownload/network/downsync/util/SharedInfo;I)V

    .line 120
    .local v2, "resp":Lcom/samsung/android/app/migration/snbdownload/network/AsyncFactory$Response;
    iget v4, v1, Lcom/samsung/android/app/migration/snbdownload/network/AsyncFactory$Request;->mFunction:I

    packed-switch v4, :pswitch_data_0

    .line 136
    :goto_0
    return-object v2

    .line 123
    :pswitch_0
    :try_start_0
    iget-object v4, p0, Lcom/samsung/android/app/migration/snbdownload/network/AsyncFactory$NetworkRequests;->this$0:Lcom/samsung/android/app/migration/snbdownload/network/AsyncFactory;

    # getter for: Lcom/samsung/android/app/migration/snbdownload/network/AsyncFactory;->mNetworkUtil:Lcom/samsung/android/app/migration/snbdownload/network/util/NetworkUtil;
    invoke-static {v4}, Lcom/samsung/android/app/migration/snbdownload/network/AsyncFactory;->access$0(Lcom/samsung/android/app/migration/snbdownload/network/AsyncFactory;)Lcom/samsung/android/app/migration/snbdownload/network/util/NetworkUtil;

    move-result-object v4

    iget-object v5, v1, Lcom/samsung/android/app/migration/snbdownload/network/AsyncFactory$Request;->mUrl:Ljava/lang/String;

    iget-object v6, v1, Lcom/samsung/android/app/migration/snbdownload/network/AsyncFactory$Request;->mJson:Ljava/lang/String;

    iget-object v7, v1, Lcom/samsung/android/app/migration/snbdownload/network/AsyncFactory$Request;->mConetnttype:Ljava/lang/String;

    invoke-virtual {v4, v5, v6, v7, v2}, Lcom/samsung/android/app/migration/snbdownload/network/util/NetworkUtil;->post(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/samsung/android/app/migration/snbdownload/network/AsyncFactory$Response;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 124
    :catch_0
    move-exception v0

    .line 125
    .local v0, "e":Ljava/io/IOException;
    iput-boolean v8, v2, Lcom/samsung/android/app/migration/snbdownload/network/AsyncFactory$Response;->isfailed:Z

    goto :goto_0

    .line 130
    .end local v0    # "e":Ljava/io/IOException;
    :pswitch_1
    :try_start_1
    iget-object v4, p0, Lcom/samsung/android/app/migration/snbdownload/network/AsyncFactory$NetworkRequests;->this$0:Lcom/samsung/android/app/migration/snbdownload/network/AsyncFactory;

    # getter for: Lcom/samsung/android/app/migration/snbdownload/network/AsyncFactory;->mNetworkUtil:Lcom/samsung/android/app/migration/snbdownload/network/util/NetworkUtil;
    invoke-static {v4}, Lcom/samsung/android/app/migration/snbdownload/network/AsyncFactory;->access$0(Lcom/samsung/android/app/migration/snbdownload/network/AsyncFactory;)Lcom/samsung/android/app/migration/snbdownload/network/util/NetworkUtil;

    move-result-object v4

    iget-object v5, v1, Lcom/samsung/android/app/migration/snbdownload/network/AsyncFactory$Request;->mUrl:Ljava/lang/String;

    invoke-virtual {v4, v5, v2}, Lcom/samsung/android/app/migration/snbdownload/network/util/NetworkUtil;->get(Ljava/lang/String;Lcom/samsung/android/app/migration/snbdownload/network/AsyncFactory$Response;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 131
    :catch_1
    move-exception v0

    .line 132
    .restart local v0    # "e":Ljava/io/IOException;
    iput-boolean v8, v2, Lcom/samsung/android/app/migration/snbdownload/network/AsyncFactory$Response;->isfailed:Z

    goto :goto_0

    .line 120
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected bridge varargs synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1
    check-cast p1, [Lcom/samsung/android/app/migration/snbdownload/network/AsyncFactory$Request;

    invoke-virtual {p0, p1}, Lcom/samsung/android/app/migration/snbdownload/network/AsyncFactory$NetworkRequests;->doInBackground([Lcom/samsung/android/app/migration/snbdownload/network/AsyncFactory$Request;)Lcom/samsung/android/app/migration/snbdownload/network/AsyncFactory$Response;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Lcom/samsung/android/app/migration/snbdownload/network/AsyncFactory$Response;)V
    .locals 3
    .param p1, "resp"    # Lcom/samsung/android/app/migration/snbdownload/network/AsyncFactory$Response;

    .prologue
    .line 141
    invoke-virtual {p1}, Lcom/samsung/android/app/migration/snbdownload/network/AsyncFactory$Response;->getReq_type()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 147
    :goto_0
    return-void

    .line 143
    :pswitch_0
    invoke-virtual {p1}, Lcom/samsung/android/app/migration/snbdownload/network/AsyncFactory$Response;->getAuthinfo()Lcom/samsung/android/app/migration/snbdownload/network/downsync/util/SharedInfo;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/app/migration/snbdownload/network/AsyncFactory$NetworkRequests;->this$0:Lcom/samsung/android/app/migration/snbdownload/network/AsyncFactory;

    # getter for: Lcom/samsung/android/app/migration/snbdownload/network/AsyncFactory;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/samsung/android/app/migration/snbdownload/network/AsyncFactory;->access$1(Lcom/samsung/android/app/migration/snbdownload/network/AsyncFactory;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/migration/snbdownload/network/downsync/util/SharedInfo;->savePushInfo(Landroid/content/Context;)V

    .line 144
    invoke-static {}, Lcom/samsung/android/app/migration/snbdownload/network/util/RelayUtil;->getListener()Lcom/samsung/android/app/migration/snbdownload/DownloadResponseListener;

    move-result-object v0

    const/4 v1, 0x0

    iget-boolean v2, p1, Lcom/samsung/android/app/migration/snbdownload/network/AsyncFactory$Response;->isfailed:Z

    invoke-interface {v0, v1, v2}, Lcom/samsung/android/app/migration/snbdownload/DownloadResponseListener;->onResponse(IZ)V

    goto :goto_0

    .line 141
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1
    check-cast p1, Lcom/samsung/android/app/migration/snbdownload/network/AsyncFactory$Response;

    invoke-virtual {p0, p1}, Lcom/samsung/android/app/migration/snbdownload/network/AsyncFactory$NetworkRequests;->onPostExecute(Lcom/samsung/android/app/migration/snbdownload/network/AsyncFactory$Response;)V

    return-void
.end method
