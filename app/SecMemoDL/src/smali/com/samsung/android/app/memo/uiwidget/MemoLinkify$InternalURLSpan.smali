.class public Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$InternalURLSpan;
.super Landroid/text/style/ClickableSpan;
.source "MemoLinkify.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/memo/uiwidget/MemoLinkify;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "InternalURLSpan"
.end annotation


# instance fields
.field mContext:Landroid/content/Context;

.field mDesc:Ljava/lang/String;

.field mIsAllDay:Z

.field mTime:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 464
    invoke-direct {p0}, Landroid/text/style/ClickableSpan;-><init>()V

    .line 465
    iput-object p1, p0, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$InternalURLSpan;->mContext:Landroid/content/Context;

    .line 466
    return-void
.end method


# virtual methods
.method public getAllDay()Z
    .locals 1

    .prologue
    .line 484
    iget-boolean v0, p0, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$InternalURLSpan;->mIsAllDay:Z

    return v0
.end method

.method public getTime()Ljava/lang/String;
    .locals 2

    .prologue
    .line 481
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "time:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$InternalURLSpan;->mTime:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "widget"    # Landroid/view/View;

    .prologue
    .line 488
    iget-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$InternalURLSpan;->mTime:Ljava/lang/String;

    iget-boolean v1, p0, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$InternalURLSpan;->mIsAllDay:Z

    iget-object v2, p0, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$InternalURLSpan;->mDesc:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify;->showPopup(Ljava/lang/String;ZLjava/lang/String;)Landroid/app/AlertDialog;

    .line 490
    return-void
.end method

.method public setAllDay(Z)V
    .locals 0
    .param p1, "isAllDay"    # Z

    .prologue
    .line 477
    iput-boolean p1, p0, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$InternalURLSpan;->mIsAllDay:Z

    .line 478
    return-void
.end method

.method public setDescription(Ljava/lang/String;)V
    .locals 0
    .param p1, "desc"    # Ljava/lang/String;

    .prologue
    .line 473
    iput-object p1, p0, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$InternalURLSpan;->mDesc:Ljava/lang/String;

    .line 474
    return-void
.end method

.method public setTimeString(Ljava/lang/String;)V
    .locals 0
    .param p1, "time"    # Ljava/lang/String;

    .prologue
    .line 469
    iput-object p1, p0, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$InternalURLSpan;->mTime:Ljava/lang/String;

    .line 470
    return-void
.end method

.method public updateDrawState(Landroid/text/TextPaint;)V
    .locals 0
    .param p1, "ds"    # Landroid/text/TextPaint;

    .prologue
    .line 493
    invoke-super {p0, p1}, Landroid/text/style/ClickableSpan;->updateDrawState(Landroid/text/TextPaint;)V

    .line 496
    return-void
.end method
