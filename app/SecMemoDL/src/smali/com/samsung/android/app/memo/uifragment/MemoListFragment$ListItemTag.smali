.class Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListItemTag;
.super Ljava/lang/Object;
.source "MemoListFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/memo/uifragment/MemoListFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ListItemTag"
.end annotation


# instance fields
.field public checkbox_list:Landroid/widget/CheckBox;

.field public lastModAT:Landroid/widget/TextView;

.field public mParentView:Landroid/view/View;

.field public snippet:Landroid/widget/TextView;

.field public thumbView:Landroid/widget/ImageView;

.field public title:Landroid/widget/TextView;


# direct methods
.method constructor <init>(Landroid/view/View;)V
    .locals 1
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 1494
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1495
    const v0, 0x7f0e0041

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListItemTag;->title:Landroid/widget/TextView;

    .line 1496
    const v0, 0x7f0e0043

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListItemTag;->snippet:Landroid/widget/TextView;

    .line 1497
    const v0, 0x7f0e0044

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListItemTag;->lastModAT:Landroid/widget/TextView;

    .line 1498
    const v0, 0x7f0e0042

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListItemTag;->thumbView:Landroid/widget/ImageView;

    .line 1499
    const v0, 0x7f0e0040

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListItemTag;->checkbox_list:Landroid/widget/CheckBox;

    .line 1500
    iput-object p1, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListItemTag;->mParentView:Landroid/view/View;

    .line 1501
    return-void
.end method

.method static setTag(Landroid/view/View;)V
    .locals 1
    .param p0, "v"    # Landroid/view/View;

    .prologue
    .line 1478
    new-instance v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListItemTag;

    invoke-direct {v0, p0}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListItemTag;-><init>(Landroid/view/View;)V

    .line 1479
    .local v0, "tag":Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListItemTag;
    invoke-virtual {p0, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 1480
    return-void
.end method
