.class public Lcom/samsung/android/app/migration/dialog/PasswordDialog;
.super Landroid/app/DialogFragment;
.source "PasswordDialog.java"

# interfaces
.implements Landroid/content/DialogInterface$OnCancelListener;


# static fields
.field private static context:Landroid/app/Activity;

.field private static mFileName:Ljava/lang/String;

.field private static mTitle:Ljava/lang/String;


# instance fields
.field private mInputPassword:Landroid/widget/EditText;

.field private mPasswordDialog:Landroid/app/AlertDialog;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 39
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    .line 47
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/app/migration/dialog/PasswordDialog;->mPasswordDialog:Landroid/app/AlertDialog;

    .line 39
    return-void
.end method

.method static synthetic access$0(Lcom/samsung/android/app/migration/dialog/PasswordDialog;)V
    .locals 0

    .prologue
    .line 127
    invoke-direct {p0}, Lcom/samsung/android/app/migration/dialog/PasswordDialog;->hideIme()V

    return-void
.end method

.method static synthetic access$1(Lcom/samsung/android/app/migration/dialog/PasswordDialog;)Landroid/widget/EditText;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/samsung/android/app/migration/dialog/PasswordDialog;->mInputPassword:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic access$2()Landroid/app/Activity;
    .locals 1

    .prologue
    .line 41
    sget-object v0, Lcom/samsung/android/app/migration/dialog/PasswordDialog;->context:Landroid/app/Activity;

    return-object v0
.end method

.method private hideIme()V
    .locals 3

    .prologue
    .line 128
    sget-object v1, Lcom/samsung/android/app/migration/dialog/PasswordDialog;->context:Landroid/app/Activity;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/android/app/migration/dialog/PasswordDialog;->mInputPassword:Landroid/widget/EditText;

    if-eqz v1, :cond_0

    .line 129
    sget-object v1, Lcom/samsung/android/app/migration/dialog/PasswordDialog;->context:Landroid/app/Activity;

    .line 130
    const-string v2, "input_method"

    invoke-virtual {v1, v2}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 129
    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 131
    .local v0, "Imm":Landroid/view/inputmethod/InputMethodManager;
    invoke-virtual {v0}, Landroid/view/inputmethod/InputMethodManager;->isActive()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 132
    iget-object v1, p0, Lcom/samsung/android/app/migration/dialog/PasswordDialog;->mInputPassword:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 135
    .end local v0    # "Imm":Landroid/view/inputmethod/InputMethodManager;
    :cond_0
    return-void
.end method

.method public static newInstance(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;)Lcom/samsung/android/app/migration/dialog/PasswordDialog;
    .locals 1
    .param p0, "ctx"    # Landroid/app/Activity;
    .param p1, "title"    # Ljava/lang/String;
    .param p2, "fileName"    # Ljava/lang/String;

    .prologue
    .line 52
    sput-object p0, Lcom/samsung/android/app/migration/dialog/PasswordDialog;->context:Landroid/app/Activity;

    .line 53
    sput-object p1, Lcom/samsung/android/app/migration/dialog/PasswordDialog;->mTitle:Ljava/lang/String;

    .line 54
    sput-object p2, Lcom/samsung/android/app/migration/dialog/PasswordDialog;->mFileName:Ljava/lang/String;

    .line 55
    new-instance v0, Lcom/samsung/android/app/migration/dialog/PasswordDialog;

    invoke-direct {v0}, Lcom/samsung/android/app/migration/dialog/PasswordDialog;-><init>()V

    .line 56
    .local v0, "f":Lcom/samsung/android/app/migration/dialog/PasswordDialog;
    return-object v0
.end method

.method private showIme()V
    .locals 4

    .prologue
    .line 108
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v1, Lcom/samsung/android/app/migration/dialog/PasswordDialog$3;

    invoke-direct {v1, p0}, Lcom/samsung/android/app/migration/dialog/PasswordDialog$3;-><init>(Lcom/samsung/android/app/migration/dialog/PasswordDialog;)V

    .line 124
    const-wide/16 v2, 0x12c

    .line 108
    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 125
    return-void
.end method


# virtual methods
.method public onCancel(Landroid/content/DialogInterface;)V
    .locals 2
    .param p1, "dialog"    # Landroid/content/DialogInterface;

    .prologue
    .line 103
    invoke-direct {p0}, Lcom/samsung/android/app/migration/dialog/PasswordDialog;->hideIme()V

    .line 104
    invoke-static {}, Lcom/samsung/android/app/migration/Migration;->getInstance()Lcom/samsung/android/app/migration/Migration;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/migration/Migration;->setPassword(Ljava/lang/String;)V

    .line 105
    return-void
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 7
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v6, 0x1

    .line 78
    sget-object v2, Lcom/samsung/android/app/migration/dialog/PasswordDialog;->context:Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v2

    const v3, 0x7f040024

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 79
    .local v1, "v":Landroid/view/View;
    const v2, 0x7f0e001f

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 80
    .local v0, "tvMsg":Landroid/widget/TextView;
    sget-object v2, Lcom/samsung/android/app/migration/dialog/PasswordDialog;->context:Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b004c

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-array v3, v6, [Ljava/lang/Object;

    const/4 v4, 0x0

    sget-object v5, Lcom/samsung/android/app/migration/dialog/PasswordDialog;->mFileName:Ljava/lang/String;

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 81
    const v2, 0x7f0e004b

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/EditText;

    iput-object v2, p0, Lcom/samsung/android/app/migration/dialog/PasswordDialog;->mInputPassword:Landroid/widget/EditText;

    .line 82
    new-instance v2, Landroid/app/AlertDialog$Builder;

    sget-object v3, Lcom/samsung/android/app/migration/dialog/PasswordDialog;->context:Landroid/app/Activity;

    invoke-direct {v2, v3}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v2, v1}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    invoke-virtual {v2, v6}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    .line 83
    const v3, 0x7f0b0047

    new-instance v4, Lcom/samsung/android/app/migration/dialog/PasswordDialog$1;

    invoke-direct {v4, p0}, Lcom/samsung/android/app/migration/dialog/PasswordDialog$1;-><init>(Lcom/samsung/android/app/migration/dialog/PasswordDialog;)V

    invoke-virtual {v2, v3, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    .line 90
    const v3, 0x7f0b0005

    new-instance v4, Lcom/samsung/android/app/migration/dialog/PasswordDialog$2;

    invoke-direct {v4, p0}, Lcom/samsung/android/app/migration/dialog/PasswordDialog$2;-><init>(Lcom/samsung/android/app/migration/dialog/PasswordDialog;)V

    invoke-virtual {v2, v3, v4}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    .line 96
    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v2

    .line 82
    iput-object v2, p0, Lcom/samsung/android/app/migration/dialog/PasswordDialog;->mPasswordDialog:Landroid/app/AlertDialog;

    .line 97
    iget-object v2, p0, Lcom/samsung/android/app/migration/dialog/PasswordDialog;->mPasswordDialog:Landroid/app/AlertDialog;

    sget-object v3, Lcom/samsung/android/app/migration/dialog/PasswordDialog;->mTitle:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog;->setTitle(Ljava/lang/CharSequence;)V

    .line 98
    iget-object v2, p0, Lcom/samsung/android/app/migration/dialog/PasswordDialog;->mPasswordDialog:Landroid/app/AlertDialog;

    return-object v2
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 65
    invoke-super {p0}, Landroid/app/DialogFragment;->onPause()V

    .line 66
    iget-object v0, p0, Lcom/samsung/android/app/migration/dialog/PasswordDialog;->mInputPassword:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->clearFocus()V

    .line 67
    invoke-direct {p0}, Lcom/samsung/android/app/migration/dialog/PasswordDialog;->hideIme()V

    .line 68
    return-void
.end method

.method public onResume()V
    .locals 0

    .prologue
    .line 72
    invoke-super {p0}, Landroid/app/DialogFragment;->onResume()V

    .line 73
    invoke-direct {p0}, Lcom/samsung/android/app/migration/dialog/PasswordDialog;->showIme()V

    .line 74
    return-void
.end method

.method public show()V
    .locals 2

    .prologue
    .line 60
    sget-object v0, Lcom/samsung/android/app/migration/dialog/PasswordDialog;->context:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    const-string v1, "passwordDialog"

    invoke-virtual {p0, v0, v1}, Lcom/samsung/android/app/migration/dialog/PasswordDialog;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 61
    return-void
.end method
