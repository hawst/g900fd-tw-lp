.class Lcom/samsung/android/app/memo/voice/VoiceViewLayout$10;
.super Landroid/content/BroadcastReceiver;
.source "VoiceViewLayout.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->updateControlReceiver(Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$10;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    .line 498
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const v6, 0x7f0b0093

    const v5, 0x7f0e006a

    const/16 v4, 0x8

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 502
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.samsung.android.app.memo.voicerecorder.stop"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 503
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$10;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    invoke-virtual {v0}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->getState()Lcom/samsung/android/app/memo/voice/VoiceViewLayout$State;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$State;->STATE_RECORD:Lcom/samsung/android/app/memo/voice/VoiceViewLayout$State;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$10;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    invoke-virtual {v0}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->getState()Lcom/samsung/android/app/memo/voice/VoiceViewLayout$State;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$State;->STATE_RECORD_PAUSE:Lcom/samsung/android/app/memo/voice/VoiceViewLayout$State;

    if-ne v0, v1, :cond_2

    .line 504
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$10;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    const v1, 0x7f0b00ac

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->showToast(I)V

    .line 505
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$10;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    invoke-virtual {v0}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->handleRecordingStop()V

    .line 509
    :goto_0
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$10;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    invoke-virtual {v0}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->hideNotification()V

    .line 557
    :cond_1
    :goto_1
    return-void

    .line 507
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$10;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    invoke-virtual {v0}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->handleStopPlaying()V

    goto :goto_0

    .line 510
    :cond_3
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.samsung.android.app.memo.voicerecorder.pause"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 511
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$10;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    # getter for: Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mRemoteViews:Landroid/widget/RemoteViews;
    invoke-static {v0}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->access$8(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)Landroid/widget/RemoteViews;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 512
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$10;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    # getter for: Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mRemoteViews:Landroid/widget/RemoteViews;
    invoke-static {v0}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->access$8(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)Landroid/widget/RemoteViews;

    move-result-object v0

    invoke-virtual {v0, v5, v4}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 514
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$10;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    # getter for: Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mVoiceManger:Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;
    invoke-static {v0}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->access$9(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->isNowRecording()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 515
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$10;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    # getter for: Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mRemoteViews:Landroid/widget/RemoteViews;
    invoke-static {v0}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->access$8(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)Landroid/widget/RemoteViews;

    move-result-object v0

    .line 516
    const v1, 0x7f0e0068

    .line 515
    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 518
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$10;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    invoke-virtual {v0, v3}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->showVoiceNotification(Z)V

    .line 519
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$10;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    invoke-virtual {v0}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->handleRecordPause()V

    goto :goto_1

    .line 521
    :cond_4
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$10;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    # getter for: Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mVoiceManger:Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;
    invoke-static {v0}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->access$9(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->requestAudioFocus()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 522
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$10;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    invoke-virtual {v0, v6}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->showToast(I)V

    goto :goto_1

    .line 524
    :cond_5
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$10;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    # getter for: Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mRemoteViews:Landroid/widget/RemoteViews;
    invoke-static {v0}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->access$8(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)Landroid/widget/RemoteViews;

    move-result-object v0

    .line 525
    const v1, 0x7f0e0069

    .line 524
    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 526
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$10;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    invoke-virtual {v0, v3}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->showVoiceNotification(Z)V

    .line 527
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$10;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    # getter for: Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mVoiceManger:Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;
    invoke-static {v0}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->access$9(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->pauseSound(Z)V

    goto :goto_1

    .line 531
    :cond_6
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.samsung.android.app.memo.voicerecorder.resume"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 532
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$10;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    # getter for: Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mRemoteViews:Landroid/widget/RemoteViews;
    invoke-static {v0}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->access$8(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)Landroid/widget/RemoteViews;

    move-result-object v0

    if-eqz v0, :cond_7

    .line 533
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$10;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    # getter for: Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mRemoteViews:Landroid/widget/RemoteViews;
    invoke-static {v0}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->access$8(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)Landroid/widget/RemoteViews;

    move-result-object v0

    const v1, 0x7f0e0068

    invoke-virtual {v0, v1, v4}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 535
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$10;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    # getter for: Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mRemoteViews:Landroid/widget/RemoteViews;
    invoke-static {v0}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->access$8(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)Landroid/widget/RemoteViews;

    move-result-object v0

    invoke-virtual {v0, v5, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 538
    :cond_7
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$10;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    invoke-virtual {v0, v3}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->showVoiceNotification(Z)V

    .line 539
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$10;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    invoke-virtual {v0}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->handleRecordPause()V

    goto/16 :goto_1

    .line 540
    :cond_8
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.samsung.android.app.memo.voicerecorder.play"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 541
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$10;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    # getter for: Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mVoiceManger:Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;
    invoke-static {v0}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->access$9(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->requestAudioFocus()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 542
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$10;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    invoke-virtual {v0, v6}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->showToast(I)V

    goto/16 :goto_1

    .line 543
    :cond_9
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$10;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    invoke-virtual {v0}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->isNowPlaying()Z

    move-result v0

    if-nez v0, :cond_1

    .line 544
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$10;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    # getter for: Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mRemoteViews:Landroid/widget/RemoteViews;
    invoke-static {v0}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->access$8(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)Landroid/widget/RemoteViews;

    move-result-object v0

    if-eqz v0, :cond_a

    .line 545
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$10;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    # getter for: Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mRemoteViews:Landroid/widget/RemoteViews;
    invoke-static {v0}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->access$8(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)Landroid/widget/RemoteViews;

    move-result-object v0

    .line 546
    const v1, 0x7f0e0069

    .line 545
    invoke-virtual {v0, v1, v4}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 547
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$10;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    # getter for: Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mRemoteViews:Landroid/widget/RemoteViews;
    invoke-static {v0}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->access$8(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)Landroid/widget/RemoteViews;

    move-result-object v0

    invoke-virtual {v0, v5, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 550
    :cond_a
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$10;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    # getter for: Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mVoiceManger:Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;
    invoke-static {v0}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->access$9(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;

    move-result-object v0

    iput-boolean v2, v0, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->playerWasPlaying:Z

    .line 551
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$10;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    invoke-virtual {v0, v3}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->showVoiceNotification(Z)V

    .line 552
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$10;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    # getter for: Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mVoiceManger:Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;
    invoke-static {v0}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->access$9(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$10;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    # getter for: Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mVoiceinfo:Lcom/samsung/android/app/memo/voice/VoiceInfo;
    invoke-static {v1}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->access$0(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)Lcom/samsung/android/app/memo/voice/VoiceInfo;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$10;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    iget-object v2, v2, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->playerCompletionListener:Landroid/media/MediaPlayer$OnCompletionListener;

    iget-object v3, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$10;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    iget-object v3, v3, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->playerErrorListener:Landroid/media/MediaPlayer$OnErrorListener;

    .line 553
    iget-object v4, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$10;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    iget-object v4, v4, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mSoundListener:Lcom/samsung/android/app/memo/voice/VoiceRecorderBase$SoundListener;

    .line 552
    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->playSound(Lcom/samsung/android/app/memo/voice/VoiceInfo;Landroid/media/MediaPlayer$OnCompletionListener;Landroid/media/MediaPlayer$OnErrorListener;Lcom/samsung/android/app/memo/voice/VoiceRecorderBase$SoundListener;)V

    goto/16 :goto_1
.end method
