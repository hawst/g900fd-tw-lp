.class public final Lcom/samsung/android/app/migration/snbdownload/network/downsync/records/KVSResponse;
.super Lcom/samsung/android/app/migration/snbdownload/network/downsync/records/AbstractJSON;
.source "KVSResponse.java"


# static fields
.field private static final DELETED:Ljava/lang/String; = "deleted"

.field private static final KEY:Ljava/lang/String; = "key"

.field private static final LIST:Ljava/lang/String; = "list"

.field private static final MAX_TIMESTAMP:Ljava/lang/String; = "maxTimestamp"

.field private static final NEXTKEY:Ljava/lang/String; = "nextKey"

.field private static final RCODE:Ljava/lang/String; = "rcode"

.field private static final SERVER_TIMESTAMP:Ljava/lang/String; = "serverTimestamp"

.field private static final SIZE:Ljava/lang/String; = "size"

.field private static final TIMESTAMP:Ljava/lang/String; = "timestamp"

.field private static final VALUE:Ljava/lang/String; = "value"


# instance fields
.field private final mItemDetailsList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/app/migration/snbdownload/network/downsync/records/RecordItem;",
            ">;"
        }
    .end annotation
.end field

.field private final mItemResponseList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/app/migration/snbdownload/network/downsync/records/RecordItemResponse;",
            ">;"
        }
    .end annotation
.end field

.field private final mKeyDetailList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/app/migration/snbdownload/network/downsync/records/RecordKey;",
            ">;"
        }
    .end annotation
.end field

.field private mMaxTimeStamp:Ljava/lang/String;

.field private mNextKey:Ljava/lang/String;

.field private mResponseCode:I

.field private mServerTimeStamp:Ljava/lang/Long;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 28
    invoke-direct {p0}, Lcom/samsung/android/app/migration/snbdownload/network/downsync/records/AbstractJSON;-><init>()V

    .line 41
    iput-object v0, p0, Lcom/samsung/android/app/migration/snbdownload/network/downsync/records/KVSResponse;->mMaxTimeStamp:Ljava/lang/String;

    .line 42
    iput-object v0, p0, Lcom/samsung/android/app/migration/snbdownload/network/downsync/records/KVSResponse;->mNextKey:Ljava/lang/String;

    .line 45
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/app/migration/snbdownload/network/downsync/records/KVSResponse;->mItemResponseList:Ljava/util/List;

    .line 46
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/app/migration/snbdownload/network/downsync/records/KVSResponse;->mItemDetailsList:Ljava/util/List;

    .line 47
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/app/migration/snbdownload/network/downsync/records/KVSResponse;->mKeyDetailList:Ljava/util/List;

    .line 28
    return-void
.end method


# virtual methods
.method public clear()V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 51
    iput-object v0, p0, Lcom/samsung/android/app/migration/snbdownload/network/downsync/records/KVSResponse;->mMaxTimeStamp:Ljava/lang/String;

    .line 52
    iput-object v0, p0, Lcom/samsung/android/app/migration/snbdownload/network/downsync/records/KVSResponse;->mNextKey:Ljava/lang/String;

    .line 53
    const/4 v0, -0x1

    iput v0, p0, Lcom/samsung/android/app/migration/snbdownload/network/downsync/records/KVSResponse;->mResponseCode:I

    .line 54
    const-wide/16 v0, 0x0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/migration/snbdownload/network/downsync/records/KVSResponse;->mServerTimeStamp:Ljava/lang/Long;

    .line 55
    iget-object v0, p0, Lcom/samsung/android/app/migration/snbdownload/network/downsync/records/KVSResponse;->mItemResponseList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 56
    iget-object v0, p0, Lcom/samsung/android/app/migration/snbdownload/network/downsync/records/KVSResponse;->mItemDetailsList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 57
    iget-object v0, p0, Lcom/samsung/android/app/migration/snbdownload/network/downsync/records/KVSResponse;->mKeyDetailList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 58
    return-void
.end method

.method public fromJSON(Ljava/lang/Object;)V
    .locals 18
    .param p1, "json"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 90
    new-instance v10, Lorg/json/JSONObject;

    check-cast p1, Ljava/lang/String;

    .end local p1    # "json":Ljava/lang/Object;
    move-object/from16 v0, p1

    invoke-direct {v10, v0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 91
    .local v10, "jsonObj":Lorg/json/JSONObject;
    const-string v3, "rcode"

    invoke-virtual {v10, v3}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 92
    const-string v3, "rcode"

    invoke-virtual {v10, v3}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, p0

    iput v3, v0, Lcom/samsung/android/app/migration/snbdownload/network/downsync/records/KVSResponse;->mResponseCode:I

    .line 93
    :cond_0
    const-string v3, "maxTimestamp"

    invoke-virtual {v10, v3}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 94
    const-string v3, "maxTimestamp"

    invoke-virtual {v10, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/samsung/android/app/migration/snbdownload/network/downsync/records/KVSResponse;->mMaxTimeStamp:Ljava/lang/String;

    .line 95
    :cond_1
    const-string v3, "nextKey"

    invoke-virtual {v10, v3}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 96
    const-string v3, "nextKey"

    invoke-virtual {v10, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/samsung/android/app/migration/snbdownload/network/downsync/records/KVSResponse;->mNextKey:Ljava/lang/String;

    .line 97
    :cond_2
    const-string v3, "serverTimestamp"

    invoke-virtual {v10, v3}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 98
    const-string v3, "serverTimestamp"

    invoke-virtual {v10, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v16

    invoke-static/range {v16 .. v17}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/samsung/android/app/migration/snbdownload/network/downsync/records/KVSResponse;->mServerTimeStamp:Ljava/lang/Long;

    .line 99
    :cond_3
    const-string v3, "list"

    invoke-virtual {v10, v3}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 100
    const-string v3, "list"

    invoke-virtual {v10, v3}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v2

    .line 101
    .local v2, "dataitemarray":Lorg/json/JSONArray;
    invoke-virtual {v2}, Lorg/json/JSONArray;->length()I

    move-result v11

    .line 102
    .local v11, "length":I
    const/4 v9, 0x0

    .local v9, "i":I
    :goto_0
    if-lt v9, v11, :cond_5

    .line 130
    .end local v2    # "dataitemarray":Lorg/json/JSONArray;
    .end local v9    # "i":I
    .end local v11    # "length":I
    :cond_4
    return-void

    .line 103
    .restart local v2    # "dataitemarray":Lorg/json/JSONArray;
    .restart local v9    # "i":I
    .restart local v11    # "length":I
    :cond_5
    invoke-virtual {v2, v9}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v12

    .line 104
    .local v12, "obj":Lorg/json/JSONObject;
    const-string v3, "timestamp"

    invoke-virtual {v12, v3}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;)J

    move-result-wide v16

    invoke-static/range {v16 .. v17}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    .line 105
    .local v5, "timestamp":Ljava/lang/Long;
    const-string v3, "key"

    invoke-virtual {v12, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 106
    .local v4, "key":Ljava/lang/String;
    const/4 v13, 0x0

    .line 107
    .local v13, "retcode":I
    const/4 v14, 0x0

    .line 108
    .local v14, "value":Ljava/lang/String;
    const/4 v8, 0x0

    .line 109
    .local v8, "deleted":Z
    const-wide/16 v6, 0x0

    .line 111
    .local v6, "size":J
    const-string v3, "rcode"

    invoke-virtual {v12, v3}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 112
    const-string v3, "rcode"

    invoke-virtual {v12, v3}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v13

    .line 113
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/app/migration/snbdownload/network/downsync/records/KVSResponse;->mItemResponseList:Ljava/util/List;

    new-instance v15, Lcom/samsung/android/app/migration/snbdownload/network/downsync/records/RecordItemResponse;

    invoke-direct {v15, v4, v5, v13}, Lcom/samsung/android/app/migration/snbdownload/network/downsync/records/RecordItemResponse;-><init>(Ljava/lang/String;Ljava/lang/Long;I)V

    invoke-interface {v3, v15}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 116
    :cond_6
    const-string v3, "value"

    invoke-virtual {v12, v3}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 117
    const-string v3, "deleted"

    invoke-virtual {v12, v3}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;)Z

    move-result v8

    .line 118
    const-string v3, "value"

    invoke-virtual {v12, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    .line 119
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/app/migration/snbdownload/network/downsync/records/KVSResponse;->mItemDetailsList:Ljava/util/List;

    new-instance v15, Lcom/samsung/android/app/migration/snbdownload/network/downsync/records/RecordItem;

    invoke-direct {v15, v4, v5, v14, v8}, Lcom/samsung/android/app/migration/snbdownload/network/downsync/records/RecordItem;-><init>(Ljava/lang/String;Ljava/lang/Long;Ljava/lang/String;Z)V

    invoke-interface {v3, v15}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 122
    :cond_7
    const-string v3, "size"

    invoke-virtual {v12, v3}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_8

    .line 123
    const-string v3, "deleted"

    invoke-virtual {v12, v3}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;)Z

    move-result v8

    .line 124
    const-string v3, "size"

    invoke-virtual {v12, v3}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;)J

    move-result-wide v6

    .line 125
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/app/migration/snbdownload/network/downsync/records/KVSResponse;->mKeyDetailList:Ljava/util/List;

    new-instance v3, Lcom/samsung/android/app/migration/snbdownload/network/downsync/records/RecordKey;

    invoke-direct/range {v3 .. v8}, Lcom/samsung/android/app/migration/snbdownload/network/downsync/records/RecordKey;-><init>(Ljava/lang/String;Ljava/lang/Long;JZ)V

    invoke-interface {v15, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 102
    :cond_8
    add-int/lit8 v9, v9, 0x1

    goto :goto_0
.end method

.method public getItemDetailsList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/app/migration/snbdownload/network/downsync/records/RecordItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 77
    iget-object v0, p0, Lcom/samsung/android/app/migration/snbdownload/network/downsync/records/KVSResponse;->mItemDetailsList:Ljava/util/List;

    return-object v0
.end method

.method public getItemResponseList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/app/migration/snbdownload/network/downsync/records/RecordItemResponse;",
            ">;"
        }
    .end annotation

    .prologue
    .line 81
    iget-object v0, p0, Lcom/samsung/android/app/migration/snbdownload/network/downsync/records/KVSResponse;->mItemResponseList:Ljava/util/List;

    return-object v0
.end method

.method public getKeyDetailList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/app/migration/snbdownload/network/downsync/records/RecordKey;",
            ">;"
        }
    .end annotation

    .prologue
    .line 85
    iget-object v0, p0, Lcom/samsung/android/app/migration/snbdownload/network/downsync/records/KVSResponse;->mKeyDetailList:Ljava/util/List;

    return-object v0
.end method

.method public getMaxTimeStamp()Ljava/lang/String;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/samsung/android/app/migration/snbdownload/network/downsync/records/KVSResponse;->mMaxTimeStamp:Ljava/lang/String;

    return-object v0
.end method

.method public getNextKey()Ljava/lang/String;
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lcom/samsung/android/app/migration/snbdownload/network/downsync/records/KVSResponse;->mNextKey:Ljava/lang/String;

    return-object v0
.end method

.method public getResponseCode()I
    .locals 1

    .prologue
    .line 61
    iget v0, p0, Lcom/samsung/android/app/migration/snbdownload/network/downsync/records/KVSResponse;->mResponseCode:I

    return v0
.end method

.method public getServerTimeStamp()J
    .locals 2

    .prologue
    .line 65
    iget-object v0, p0, Lcom/samsung/android/app/migration/snbdownload/network/downsync/records/KVSResponse;->mServerTimeStamp:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    return-wide v0
.end method

.method public toJSON()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 134
    const/4 v0, 0x0

    return-object v0
.end method
