.class Lcom/samsung/android/app/migration/snbdownload/SNBDownloader$2;
.super Ljava/lang/Object;
.source "SNBDownloader.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/app/migration/snbdownload/SNBDownloader;->downloadTMemo(Ljava/util/List;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/migration/snbdownload/SNBDownloader;

.field private final synthetic val$keys:Ljava/util/List;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/migration/snbdownload/SNBDownloader;Ljava/util/List;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsung/android/app/migration/snbdownload/SNBDownloader$2;->this$0:Lcom/samsung/android/app/migration/snbdownload/SNBDownloader;

    iput-object p2, p0, Lcom/samsung/android/app/migration/snbdownload/SNBDownloader$2;->val$keys:Ljava/util/List;

    .line 211
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 214
    const/4 v1, 0x0

    .line 216
    .local v1, "isOk":Z
    :try_start_0
    iget-object v2, p0, Lcom/samsung/android/app/migration/snbdownload/SNBDownloader$2;->this$0:Lcom/samsung/android/app/migration/snbdownload/SNBDownloader;

    # invokes: Lcom/samsung/android/app/migration/snbdownload/SNBDownloader;->showNotification()V
    invoke-static {v2}, Lcom/samsung/android/app/migration/snbdownload/SNBDownloader;->access$1(Lcom/samsung/android/app/migration/snbdownload/SNBDownloader;)V

    .line 217
    iget-object v2, p0, Lcom/samsung/android/app/migration/snbdownload/SNBDownloader$2;->this$0:Lcom/samsung/android/app/migration/snbdownload/SNBDownloader;

    iget-object v3, p0, Lcom/samsung/android/app/migration/snbdownload/SNBDownloader$2;->val$keys:Ljava/util/List;

    # invokes: Lcom/samsung/android/app/migration/snbdownload/SNBDownloader;->getItemsFromKeys(Ljava/util/List;)V
    invoke-static {v2, v3}, Lcom/samsung/android/app/migration/snbdownload/SNBDownloader;->access$2(Lcom/samsung/android/app/migration/snbdownload/SNBDownloader;Ljava/util/List;)V

    .line 218
    iget-object v2, p0, Lcom/samsung/android/app/migration/snbdownload/SNBDownloader$2;->this$0:Lcom/samsung/android/app/migration/snbdownload/SNBDownloader;

    # invokes: Lcom/samsung/android/app/migration/snbdownload/SNBDownloader;->downloadSNoteFile()V
    invoke-static {v2}, Lcom/samsung/android/app/migration/snbdownload/SNBDownloader;->access$3(Lcom/samsung/android/app/migration/snbdownload/SNBDownloader;)V
    :try_end_0
    .catch Lcom/samsung/android/app/migration/snbdownload/network/downsync/util/SyncException; {:try_start_0 .. :try_end_0} :catch_0

    .line 219
    const/4 v1, 0x1

    .line 223
    :goto_0
    iget-object v2, p0, Lcom/samsung/android/app/migration/snbdownload/SNBDownloader$2;->this$0:Lcom/samsung/android/app/migration/snbdownload/SNBDownloader;

    # invokes: Lcom/samsung/android/app/migration/snbdownload/SNBDownloader;->hideNotification(Z)V
    invoke-static {v2, v1}, Lcom/samsung/android/app/migration/snbdownload/SNBDownloader;->access$4(Lcom/samsung/android/app/migration/snbdownload/SNBDownloader;Z)V

    .line 224
    invoke-static {}, Landroid/os/Looper;->prepare()V

    .line 225
    iget-object v2, p0, Lcom/samsung/android/app/migration/snbdownload/SNBDownloader$2;->this$0:Lcom/samsung/android/app/migration/snbdownload/SNBDownloader;

    # invokes: Lcom/samsung/android/app/migration/snbdownload/SNBDownloader;->handleResult(Z)V
    invoke-static {v2, v1}, Lcom/samsung/android/app/migration/snbdownload/SNBDownloader;->access$5(Lcom/samsung/android/app/migration/snbdownload/SNBDownloader;Z)V

    .line 226
    invoke-static {}, Landroid/os/Looper;->loop()V

    .line 227
    return-void

    .line 220
    :catch_0
    move-exception v0

    .line 221
    .local v0, "e":Lcom/samsung/android/app/migration/snbdownload/network/downsync/util/SyncException;
    const-string v2, "SNBDownloader"

    const-string v3, "Error occurred when downloading TMemos."

    invoke-static {v2, v3}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
