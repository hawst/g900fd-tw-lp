.class public Lcom/samsung/android/app/migration/utils/MemoAppInfoHelper;
.super Ljava/lang/Object;
.source "MemoAppInfoHelper.java"


# static fields
.field public static final TAG:Ljava/lang/String; = "MemoAppInfoHelper"

.field public static final TMEMO1_APPLICATION:Ljava/lang/String; = "TMemo1"

.field public static final TMEMO2_APPLICATION:Ljava/lang/String; = "TMemo2"

.field public static final TMEMO_APPLICATION:Ljava/lang/String; = "TMemo"

.field public static final TMEMO_PACKAGENAME:Ljava/lang/String; = "com.sec.android.app.memo"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getAppInfoValue(Landroid/content/Context;)Ljava/lang/String;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 43
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const-string v1, "com.sec.android.app.memo"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 44
    const-string v0, "TMemo"

    .line 47
    :goto_0
    return-object v0

    .line 45
    :catch_0
    move-exception v0

    .line 47
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static getTMemoApp(Landroid/content/Context;)Ljava/lang/String;
    .locals 6
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 51
    const/4 v0, 0x0

    .line 52
    .local v0, "app":Ljava/lang/String;
    invoke-static {p0}, Lcom/samsung/android/app/migration/utils/MemoAppInfoHelper;->getAppInfoValue(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 53
    .local v1, "appName":Ljava/lang/String;
    if-eqz v1, :cond_2

    .line 55
    const-string v4, "TMemo"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 56
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v5

    invoke-virtual {v5}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v5, "/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 57
    const-string v5, "TMemo"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 56
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 58
    .local v3, "path":Ljava/lang/String;
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 59
    .local v2, "folder":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 60
    const-string v0, "TMemo2"

    .line 67
    .end local v2    # "folder":Ljava/io/File;
    .end local v3    # "path":Ljava/lang/String;
    :cond_0
    :goto_0
    return-object v0

    .line 62
    .restart local v2    # "folder":Ljava/io/File;
    .restart local v3    # "path":Ljava/lang/String;
    :cond_1
    const-string v0, "TMemo1"

    .line 64
    goto :goto_0

    .line 65
    .end local v2    # "folder":Ljava/io/File;
    .end local v3    # "path":Ljava/lang/String;
    :cond_2
    const-string v4, "MemoAppInfoHelper"

    const-string v5, "TMemo app is not installed, migration stopped."

    invoke-static {v4, v5}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
