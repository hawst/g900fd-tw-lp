.class Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog$5;
.super Ljava/lang/Object;
.source "CategoryNamingDialog.java"

# interfaces
.implements Landroid/content/DialogInterface$OnShowListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog$5;->this$0:Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;

    .line 369
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$0(Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog$5;)Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;
    .locals 1

    .prologue
    .line 369
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog$5;->this$0:Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;

    return-object v0
.end method


# virtual methods
.method public onShow(Landroid/content/DialogInterface;)V
    .locals 6
    .param p1, "arg0"    # Landroid/content/DialogInterface;

    .prologue
    const/16 v2, 0x1e

    .line 372
    iget-object v3, p0, Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog$5;->this$0:Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;

    # getter for: Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;->mCategotyNamingDialog:Landroid/app/AlertDialog;
    invoke-static {v3}, Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;->access$3(Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;)Landroid/app/AlertDialog;

    move-result-object v3

    const/4 v4, -0x1

    invoke-virtual {v3, v4}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v1

    .line 373
    .local v1, "posBtn":Landroid/widget/Button;
    new-instance v3, Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog$5$1;

    invoke-direct {v3, p0}, Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog$5$1;-><init>(Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog$5;)V

    invoke-virtual {v1, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 382
    iget-object v3, p0, Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog$5;->this$0:Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;

    # invokes: Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;->getDefaultCategoryName()Ljava/lang/String;
    invoke-static {v3}, Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;->access$5(Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;)Ljava/lang/String;

    move-result-object v0

    .line 384
    .local v0, "defaultCategoryName":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 385
    iget-object v3, p0, Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog$5;->this$0:Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;

    # getter for: Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;->mInputText:Landroid/widget/EditText;
    invoke-static {v3}, Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;->access$6(Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;)Landroid/widget/EditText;

    move-result-object v3

    invoke-virtual {v3, v0}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 386
    iget-object v3, p0, Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog$5;->this$0:Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;

    # getter for: Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;->mInputText:Landroid/widget/EditText;
    invoke-static {v3}, Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;->access$6(Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;)Landroid/widget/EditText;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v5

    if-le v5, v2, :cond_1

    :goto_0
    invoke-virtual {v3, v4, v2}, Landroid/widget/EditText;->setSelection(II)V

    .line 389
    :cond_0
    iget-object v2, p0, Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog$5;->this$0:Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;

    iget-object v3, p0, Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog$5;->this$0:Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;

    # getter for: Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;->mInputText:Landroid/widget/EditText;
    invoke-static {v3}, Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;->access$6(Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;)Landroid/widget/EditText;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-interface {v3}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v3

    # invokes: Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;->setPositiveButtonEnabled(Ljava/lang/String;)V
    invoke-static {v2, v3}, Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;->access$2(Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;Ljava/lang/String;)V

    .line 391
    return-void

    .line 386
    :cond_1
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    goto :goto_0
.end method
