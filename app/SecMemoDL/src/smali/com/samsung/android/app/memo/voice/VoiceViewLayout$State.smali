.class public final enum Lcom/samsung/android/app/memo/voice/VoiceViewLayout$State;
.super Ljava/lang/Enum;
.source "VoiceViewLayout.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/memo/voice/VoiceViewLayout;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "State"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/samsung/android/app/memo/voice/VoiceViewLayout$State;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic ENUM$VALUES:[Lcom/samsung/android/app/memo/voice/VoiceViewLayout$State;

.field public static final enum STATE_PLAY:Lcom/samsung/android/app/memo/voice/VoiceViewLayout$State;

.field public static final enum STATE_PLAY_PAUSE:Lcom/samsung/android/app/memo/voice/VoiceViewLayout$State;

.field public static final enum STATE_PLAY_STOP:Lcom/samsung/android/app/memo/voice/VoiceViewLayout$State;

.field public static final enum STATE_RECORD:Lcom/samsung/android/app/memo/voice/VoiceViewLayout$State;

.field public static final enum STATE_RECORD_PAUSE:Lcom/samsung/android/app/memo/voice/VoiceViewLayout$State;

.field public static final enum STATE_STOP:Lcom/samsung/android/app/memo/voice/VoiceViewLayout$State;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 208
    new-instance v0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$State;

    const-string v1, "STATE_STOP"

    invoke-direct {v0, v1, v3}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$State;->STATE_STOP:Lcom/samsung/android/app/memo/voice/VoiceViewLayout$State;

    new-instance v0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$State;

    const-string v1, "STATE_RECORD"

    invoke-direct {v0, v1, v4}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$State;->STATE_RECORD:Lcom/samsung/android/app/memo/voice/VoiceViewLayout$State;

    new-instance v0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$State;

    const-string v1, "STATE_PLAY_STOP"

    invoke-direct {v0, v1, v5}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$State;->STATE_PLAY_STOP:Lcom/samsung/android/app/memo/voice/VoiceViewLayout$State;

    new-instance v0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$State;

    const-string v1, "STATE_PLAY"

    invoke-direct {v0, v1, v6}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$State;->STATE_PLAY:Lcom/samsung/android/app/memo/voice/VoiceViewLayout$State;

    new-instance v0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$State;

    const-string v1, "STATE_RECORD_PAUSE"

    invoke-direct {v0, v1, v7}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$State;->STATE_RECORD_PAUSE:Lcom/samsung/android/app/memo/voice/VoiceViewLayout$State;

    new-instance v0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$State;

    const-string v1, "STATE_PLAY_PAUSE"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$State;->STATE_PLAY_PAUSE:Lcom/samsung/android/app/memo/voice/VoiceViewLayout$State;

    .line 207
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/samsung/android/app/memo/voice/VoiceViewLayout$State;

    sget-object v1, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$State;->STATE_STOP:Lcom/samsung/android/app/memo/voice/VoiceViewLayout$State;

    aput-object v1, v0, v3

    sget-object v1, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$State;->STATE_RECORD:Lcom/samsung/android/app/memo/voice/VoiceViewLayout$State;

    aput-object v1, v0, v4

    sget-object v1, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$State;->STATE_PLAY_STOP:Lcom/samsung/android/app/memo/voice/VoiceViewLayout$State;

    aput-object v1, v0, v5

    sget-object v1, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$State;->STATE_PLAY:Lcom/samsung/android/app/memo/voice/VoiceViewLayout$State;

    aput-object v1, v0, v6

    sget-object v1, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$State;->STATE_RECORD_PAUSE:Lcom/samsung/android/app/memo/voice/VoiceViewLayout$State;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$State;->STATE_PLAY_PAUSE:Lcom/samsung/android/app/memo/voice/VoiceViewLayout$State;

    aput-object v2, v0, v1

    sput-object v0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$State;->ENUM$VALUES:[Lcom/samsung/android/app/memo/voice/VoiceViewLayout$State;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 207
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/samsung/android/app/memo/voice/VoiceViewLayout$State;
    .locals 1

    .prologue
    .line 1
    const-class v0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$State;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$State;

    return-object v0
.end method

.method public static values()[Lcom/samsung/android/app/memo/voice/VoiceViewLayout$State;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$State;->ENUM$VALUES:[Lcom/samsung/android/app/memo/voice/VoiceViewLayout$State;

    array-length v1, v0

    new-array v2, v1, [Lcom/samsung/android/app/memo/voice/VoiceViewLayout$State;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method
