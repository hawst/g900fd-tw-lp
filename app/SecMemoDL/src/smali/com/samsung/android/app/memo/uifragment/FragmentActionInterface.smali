.class public interface abstract Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;
.super Ljava/lang/Object;
.source "FragmentActionInterface.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface$ActionBarStyle;
    }
.end annotation


# virtual methods
.method public abstract closeDrawer()V
.end method

.method public abstract finishActionMode()V
.end method

.method public abstract getSession()Lcom/samsung/android/app/memo/Session;
.end method

.method public abstract hideCategoryList()V
.end method

.method public abstract initDrawerClosing()V
.end method

.method public abstract isDrawerClosing()Z
.end method

.method public abstract isNeedDrawerOpen()Z
.end method

.method public abstract onCategroyChanged(Ljava/lang/String;Ljava/lang/String;Z)V
.end method

.method public abstract onFragmentSuicide(Ljava/lang/String;)V
.end method

.method public abstract openContent(JLjava/lang/String;Z)V
.end method

.method public abstract openDrawer()V
.end method

.method public abstract sendMessage(ILjava/lang/Object;)V
.end method

.method public abstract setActionBarStyle(Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface$ActionBarStyle;)V
.end method

.method public abstract setActionBarTitle(I)V
.end method

.method public abstract setActionBarTitle(Ljava/lang/CharSequence;)V
.end method

.method public abstract setDrawerLock(Z)V
.end method

.method public abstract setNeedDrawerOpen(Z)V
.end method

.method public abstract showCategoryList()V
.end method

.method public abstract showGeneralErrorMessage()V
.end method

.method public abstract startDrawerClosing()V
.end method
