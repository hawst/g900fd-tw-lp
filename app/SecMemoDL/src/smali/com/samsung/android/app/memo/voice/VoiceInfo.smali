.class public Lcom/samsung/android/app/memo/voice/VoiceInfo;
.super Ljava/lang/Object;
.source "VoiceInfo.java"


# instance fields
.field public mCurTime:J

.field public mKeyNum:I

.field public mRecordStartTime:J

.field public mTitle:Ljava/lang/String;

.field public mTotalTime:J

.field public mUri:Landroid/net/Uri;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    return-void
.end method

.method public constructor <init>(Landroid/net/Uri;)V
    .locals 0
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    iput-object p1, p0, Lcom/samsung/android/app/memo/voice/VoiceInfo;->mUri:Landroid/net/Uri;

    .line 41
    return-void
.end method

.method public constructor <init>(Landroid/net/Uri;J)V
    .locals 0
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "totalTime"    # J

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    iput-object p1, p0, Lcom/samsung/android/app/memo/voice/VoiceInfo;->mUri:Landroid/net/Uri;

    .line 37
    iput-wide p2, p0, Lcom/samsung/android/app/memo/voice/VoiceInfo;->mTotalTime:J

    .line 38
    return-void
.end method
