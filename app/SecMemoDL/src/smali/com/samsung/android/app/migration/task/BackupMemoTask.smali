.class public Lcom/samsung/android/app/migration/task/BackupMemoTask;
.super Ljava/lang/Thread;
.source "BackupMemoTask.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "BackupMemoTask"


# instance fields
.field private context:Landroid/app/Activity;

.field private ctx:Landroid/content/Context;

.field private progressDialog:Lcom/samsung/android/app/migration/dialog/ProgressBarDialog;

.field private sessionKey:Ljava/lang/String;

.field private source:Ljava/lang/String;

.field private targetDir:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "context"    # Landroid/app/Activity;
    .param p2, "ctx"    # Landroid/content/Context;
    .param p3, "path"    # Ljava/lang/String;
    .param p4, "sourceApp"    # Ljava/lang/String;
    .param p5, "key"    # Ljava/lang/String;

    .prologue
    .line 63
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 64
    iput-object p1, p0, Lcom/samsung/android/app/migration/task/BackupMemoTask;->context:Landroid/app/Activity;

    .line 65
    iput-object p2, p0, Lcom/samsung/android/app/migration/task/BackupMemoTask;->ctx:Landroid/content/Context;

    .line 66
    iput-object p3, p0, Lcom/samsung/android/app/migration/task/BackupMemoTask;->targetDir:Ljava/lang/String;

    .line 67
    iput-object p4, p0, Lcom/samsung/android/app/migration/task/BackupMemoTask;->source:Ljava/lang/String;

    .line 68
    iput-object p5, p0, Lcom/samsung/android/app/migration/task/BackupMemoTask;->sessionKey:Ljava/lang/String;

    .line 69
    return-void
.end method

.method static synthetic access$0(Lcom/samsung/android/app/migration/task/BackupMemoTask;)Lcom/samsung/android/app/migration/dialog/ProgressBarDialog;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/samsung/android/app/migration/task/BackupMemoTask;->progressDialog:Lcom/samsung/android/app/migration/dialog/ProgressBarDialog;

    return-object v0
.end method

.method private backupMemoDB(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "targetDir"    # Ljava/lang/String;
    .param p3, "sessionKey"    # Ljava/lang/String;

    .prologue
    .line 177
    const-string v4, "BackupMemoTask"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "backupMemoDB: targetDir = "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " sessionKey = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 178
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v5

    iget-object v5, v5, Landroid/content/pm/ApplicationInfo;->dataDir:Ljava/lang/String;

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v5, "/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 179
    .local v0, "appDir":Ljava/lang/String;
    const-string v4, "/"

    invoke-virtual {p2, v4}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 180
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v5, "/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    .line 181
    :cond_0
    const/4 v3, 0x0

    .line 183
    .local v3, "res":I
    :try_start_0
    invoke-direct {p0, v0}, Lcom/samsung/android/app/migration/task/BackupMemoTask;->zipFiles(Ljava/lang/String;)V

    .line 184
    invoke-direct {p0, v0, p3}, Lcom/samsung/android/app/migration/task/BackupMemoTask;->encryptZip(Ljava/lang/String;Ljava/lang/String;)V

    .line 185
    new-instance v4, Ljava/io/File;

    invoke-direct {v4, p2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4}, Ljava/io/File;->mkdirs()Z

    .line 186
    new-instance v4, Ljava/io/File;

    const-string v5, "memo.bk"

    invoke-static {v0, v5}, Lcom/samsung/android/app/migration/utils/Utils;->concat(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 187
    new-instance v5, Ljava/io/File;

    const-string v6, "memo.bk"

    invoke-static {p2, v6}, Lcom/samsung/android/app/migration/utils/Utils;->concat(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 186
    invoke-static {v4, v5}, Lcom/samsung/android/app/memo/util/FileHelper;->copyFileItem(Ljava/io/File;Ljava/io/File;)I

    .line 188
    new-instance v1, Ljava/io/File;

    const-string v4, "memo.bk"

    invoke-static {v0, v4}, Lcom/samsung/android/app/migration/utils/Utils;->concat(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 189
    .local v1, "bk":Ljava/io/File;
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 190
    invoke-virtual {v1}, Ljava/io/File;->delete()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 197
    .end local v1    # "bk":Ljava/io/File;
    :cond_1
    :goto_0
    return v3

    .line 191
    :catch_0
    move-exception v2

    .line 192
    .local v2, "e":Ljava/lang/Exception;
    const-string v4, "BackupMemoTask"

    const-string v5, "Exception occurred when backing up memo.db."

    invoke-static {v4, v5, v2}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 193
    const/4 v3, -0x1

    .line 194
    invoke-virtual {v2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    const-string v5, "ENOSPC"

    invoke-virtual {v4, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 195
    const/4 v3, -0x4

    goto :goto_0
.end method

.method private static encryptStream(Ljava/io/OutputStream;Ljava/lang/String;)Ljava/io/OutputStream;
    .locals 9
    .param p0, "out"    # Ljava/io/OutputStream;
    .param p1, "password"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/4 v8, 0x0

    .line 158
    const-string v6, "SHA-256"

    invoke-static {v6}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v1

    .line 159
    .local v1, "digest":Ljava/security/MessageDigest;
    const-string v6, "UTF-8"

    invoke-virtual {p1, v6}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v6

    invoke-virtual {v1, v6}, Ljava/security/MessageDigest;->update([B)V

    .line 160
    const/16 v6, 0x10

    new-array v4, v6, [B

    .line 161
    .local v4, "keyBytes":[B
    invoke-virtual {v1}, Ljava/security/MessageDigest;->digest()[B

    move-result-object v6

    array-length v7, v4

    invoke-static {v6, v8, v4, v8, v7}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 162
    const-string v6, "AES/CBC/PKCS5Padding"

    invoke-static {v6}, Ljavax/crypto/Cipher;->getInstance(Ljava/lang/String;)Ljavax/crypto/Cipher;

    move-result-object v0

    .line 163
    .local v0, "cipher":Ljavax/crypto/Cipher;
    new-instance v3, Ljavax/crypto/spec/SecretKeySpec;

    const-string v6, "AES"

    invoke-direct {v3, v4, v6}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BLjava/lang/String;)V

    .line 166
    .local v3, "key":Ljavax/crypto/spec/SecretKeySpec;
    invoke-virtual {v0}, Ljavax/crypto/Cipher;->getBlockSize()I

    move-result v6

    new-array v2, v6, [B

    .line 167
    .local v2, "iv":[B
    new-instance v6, Ljava/security/SecureRandom;

    invoke-direct {v6}, Ljava/security/SecureRandom;-><init>()V

    invoke-virtual {v6, v2}, Ljava/security/SecureRandom;->nextBytes([B)V

    .line 168
    new-instance v5, Ljavax/crypto/spec/IvParameterSpec;

    invoke-direct {v5, v2}, Ljavax/crypto/spec/IvParameterSpec;-><init>([B)V

    .line 170
    .local v5, "spec":Ljava/security/spec/AlgorithmParameterSpec;
    invoke-virtual {p0, v2}, Ljava/io/OutputStream;->write([B)V

    .line 172
    const/4 v6, 0x1

    invoke-virtual {v0, v6, v3, v5}, Ljavax/crypto/Cipher;->init(ILjava/security/Key;Ljava/security/spec/AlgorithmParameterSpec;)V

    .line 173
    new-instance v6, Ljavax/crypto/CipherOutputStream;

    invoke-direct {v6, p0, v0}, Ljavax/crypto/CipherOutputStream;-><init>(Ljava/io/OutputStream;Ljavax/crypto/Cipher;)V

    return-object v6
.end method

.method private encryptZip(Ljava/lang/String;Ljava/lang/String;)V
    .locals 11
    .param p1, "appDir"    # Ljava/lang/String;
    .param p2, "sessionKey"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 131
    const/4 v2, 0x0

    .line 132
    .local v2, "fis":Ljava/io/InputStream;
    const/4 v4, 0x0

    .line 133
    .local v4, "fos":Ljava/io/OutputStream;
    const/4 v6, 0x0

    .line 135
    .local v6, "newfos":Ljava/io/OutputStream;
    :try_start_0
    new-instance v3, Ljava/io/FileInputStream;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v9, "memo.zip"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v3, v8}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 136
    .end local v2    # "fis":Ljava/io/InputStream;
    .local v3, "fis":Ljava/io/InputStream;
    :try_start_1
    new-instance v5, Ljava/io/FileOutputStream;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v9, "memo.bk"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v5, v8}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 137
    .end local v4    # "fos":Ljava/io/OutputStream;
    .local v5, "fos":Ljava/io/OutputStream;
    :try_start_2
    invoke-static {v5, p2}, Lcom/samsung/android/app/migration/task/BackupMemoTask;->encryptStream(Ljava/io/OutputStream;Ljava/lang/String;)Ljava/io/OutputStream;

    move-result-object v6

    .line 138
    const/4 v7, 0x0

    .line 139
    .local v7, "readcount":I
    const/16 v8, 0x400

    new-array v0, v8, [B

    .line 140
    .local v0, "buffer":[B
    :goto_0
    const/4 v8, 0x0

    const/16 v9, 0x400

    invoke-virtual {v3, v0, v8, v9}, Ljava/io/InputStream;->read([BII)I

    move-result v7

    const/4 v8, -0x1

    if-ne v7, v8, :cond_4

    .line 143
    new-instance v8, Ljava/io/File;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v10, "memo.zip"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8}, Ljava/io/File;->delete()Z
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 147
    if-eqz v6, :cond_0

    .line 148
    invoke-virtual {v6}, Ljava/io/OutputStream;->close()V

    .line 149
    :cond_0
    if-eqz v3, :cond_1

    .line 150
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V

    .line 151
    :cond_1
    if-eqz v5, :cond_2

    .line 152
    invoke-virtual {v5}, Ljava/io/OutputStream;->close()V

    :cond_2
    move-object v4, v5

    .end local v5    # "fos":Ljava/io/OutputStream;
    .restart local v4    # "fos":Ljava/io/OutputStream;
    move-object v2, v3

    .line 154
    .end local v0    # "buffer":[B
    .end local v3    # "fis":Ljava/io/InputStream;
    .end local v7    # "readcount":I
    .restart local v2    # "fis":Ljava/io/InputStream;
    :cond_3
    :goto_1
    return-void

    .line 141
    .end local v2    # "fis":Ljava/io/InputStream;
    .end local v4    # "fos":Ljava/io/OutputStream;
    .restart local v0    # "buffer":[B
    .restart local v3    # "fis":Ljava/io/InputStream;
    .restart local v5    # "fos":Ljava/io/OutputStream;
    .restart local v7    # "readcount":I
    :cond_4
    const/4 v8, 0x0

    :try_start_3
    invoke-virtual {v6, v0, v8, v7}, Ljava/io/OutputStream;->write([BII)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    goto :goto_0

    .line 144
    .end local v0    # "buffer":[B
    .end local v7    # "readcount":I
    :catch_0
    move-exception v1

    move-object v4, v5

    .end local v5    # "fos":Ljava/io/OutputStream;
    .restart local v4    # "fos":Ljava/io/OutputStream;
    move-object v2, v3

    .line 145
    .end local v3    # "fis":Ljava/io/InputStream;
    .local v1, "e":Ljava/lang/Exception;
    .restart local v2    # "fis":Ljava/io/InputStream;
    :goto_2
    :try_start_4
    const-string v8, "BackupMemoTask"

    const-string v9, "Exception occurred in encryptZip."

    invoke-static {v8, v9, v1}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 147
    if-eqz v6, :cond_5

    .line 148
    invoke-virtual {v6}, Ljava/io/OutputStream;->close()V

    .line 149
    :cond_5
    if-eqz v2, :cond_6

    .line 150
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V

    .line 151
    :cond_6
    if-eqz v4, :cond_3

    .line 152
    invoke-virtual {v4}, Ljava/io/OutputStream;->close()V

    goto :goto_1

    .line 146
    .end local v1    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v8

    .line 147
    :goto_3
    if-eqz v6, :cond_7

    .line 148
    invoke-virtual {v6}, Ljava/io/OutputStream;->close()V

    .line 149
    :cond_7
    if-eqz v2, :cond_8

    .line 150
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V

    .line 151
    :cond_8
    if-eqz v4, :cond_9

    .line 152
    invoke-virtual {v4}, Ljava/io/OutputStream;->close()V

    .line 153
    :cond_9
    throw v8

    .line 146
    .end local v2    # "fis":Ljava/io/InputStream;
    .restart local v3    # "fis":Ljava/io/InputStream;
    :catchall_1
    move-exception v8

    move-object v2, v3

    .end local v3    # "fis":Ljava/io/InputStream;
    .restart local v2    # "fis":Ljava/io/InputStream;
    goto :goto_3

    .end local v2    # "fis":Ljava/io/InputStream;
    .end local v4    # "fos":Ljava/io/OutputStream;
    .restart local v3    # "fis":Ljava/io/InputStream;
    .restart local v5    # "fos":Ljava/io/OutputStream;
    :catchall_2
    move-exception v8

    move-object v4, v5

    .end local v5    # "fos":Ljava/io/OutputStream;
    .restart local v4    # "fos":Ljava/io/OutputStream;
    move-object v2, v3

    .end local v3    # "fis":Ljava/io/InputStream;
    .restart local v2    # "fis":Ljava/io/InputStream;
    goto :goto_3

    .line 144
    :catch_1
    move-exception v1

    goto :goto_2

    .end local v2    # "fis":Ljava/io/InputStream;
    .restart local v3    # "fis":Ljava/io/InputStream;
    :catch_2
    move-exception v1

    move-object v2, v3

    .end local v3    # "fis":Ljava/io/InputStream;
    .restart local v2    # "fis":Ljava/io/InputStream;
    goto :goto_2
.end method

.method private isContextAlive(Landroid/app/Activity;)Z
    .locals 1
    .param p1, "context"    # Landroid/app/Activity;

    .prologue
    .line 72
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/app/Activity;->isDestroyed()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Landroid/app/Activity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 73
    const/4 v0, 0x1

    .line 75
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private sendBackupResponse(Landroid/content/Context;Ljava/lang/String;I)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "source"    # Ljava/lang/String;
    .param p3, "res"    # I

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 79
    if-nez p3, :cond_1

    .line 80
    invoke-static {p1, p2, v0, v0}, Lcom/samsung/android/app/migration/receiver/KiesReceiver;->sendBackupResponse(Landroid/content/Context;Ljava/lang/String;II)V

    .line 85
    :cond_0
    :goto_0
    return-void

    .line 81
    :cond_1
    const/4 v0, -0x1

    if-ne p3, v0, :cond_2

    .line 82
    invoke-static {p1, p2, v1, v1}, Lcom/samsung/android/app/migration/receiver/KiesReceiver;->sendBackupResponse(Landroid/content/Context;Ljava/lang/String;II)V

    goto :goto_0

    .line 83
    :cond_2
    const/4 v0, -0x4

    if-ne p3, v0, :cond_0

    .line 84
    const/4 v0, 0x2

    invoke-static {p1, p2, v1, v0}, Lcom/samsung/android/app/migration/receiver/KiesReceiver;->sendBackupResponse(Landroid/content/Context;Ljava/lang/String;II)V

    goto :goto_0
.end method

.method private zipFiles(Ljava/lang/String;)V
    .locals 7
    .param p1, "appDir"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 88
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v5, "databases/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "memo.db"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 89
    .local v0, "dbPath":Ljava/lang/String;
    new-instance v1, Ljava/io/File;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v5, "tmp/app_attach/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 90
    .local v1, "tmp":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->mkdirs()Z

    .line 92
    invoke-static {}, Lcom/samsung/android/app/memo/util/FileHelper;->getAttachFolder()Ljava/io/File;

    move-result-object v4

    invoke-static {v4, v1}, Lcom/samsung/android/app/memo/util/FileHelper;->copyFileItem(Ljava/io/File;Ljava/io/File;)I

    .line 93
    new-instance v1, Ljava/io/File;

    .end local v1    # "tmp":Ljava/io/File;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v5, "tmp/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "memo.db"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 94
    .restart local v1    # "tmp":Ljava/io/File;
    new-instance v4, Ljava/io/File;

    invoke-direct {v4, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v4, v1}, Lcom/samsung/android/app/memo/util/FileHelper;->copyFileItem(Ljava/io/File;Ljava/io/File;)I

    .line 95
    new-instance v2, Ljava/io/File;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v5, "memo.zip"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 96
    .local v2, "zip":Ljava/io/File;
    new-instance v3, Ljava/util/zip/ZipOutputStream;

    new-instance v4, Ljava/io/FileOutputStream;

    invoke-direct {v4, v2}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    invoke-direct {v3, v4}, Ljava/util/zip/ZipOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 97
    .local v3, "zos":Ljava/util/zip/ZipOutputStream;
    new-instance v1, Ljava/io/File;

    .end local v1    # "tmp":Ljava/io/File;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v5, "tmp/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 98
    .restart local v1    # "tmp":Ljava/io/File;
    invoke-direct {p0, v1, v1, v3}, Lcom/samsung/android/app/migration/task/BackupMemoTask;->zipFolder(Ljava/io/File;Ljava/io/File;Ljava/util/zip/ZipOutputStream;)V

    .line 99
    invoke-virtual {v3}, Ljava/util/zip/ZipOutputStream;->close()V

    .line 100
    new-instance v4, Ljava/io/File;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v6, "tmp/"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v4}, Lcom/samsung/android/app/memo/util/FileHelper;->deleteFileItem(Ljava/io/File;)V

    .line 101
    return-void
.end method

.method private zipFolder(Ljava/io/File;Ljava/io/File;Ljava/util/zip/ZipOutputStream;)V
    .locals 11
    .param p1, "directory"    # Ljava/io/File;
    .param p2, "base"    # Ljava/io/File;
    .param p3, "zos"    # Ljava/util/zip/ZipOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 104
    invoke-virtual {p1}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v3

    .line 105
    .local v3, "files":[Ljava/io/File;
    const/16 v9, 0x2000

    new-array v0, v9, [B

    .line 106
    .local v0, "buffer":[B
    const/4 v8, 0x0

    .line 107
    .local v8, "read":I
    const/4 v4, 0x0

    .local v4, "i":I
    array-length v7, v3

    .local v7, "n":I
    :goto_0
    if-lt v4, v7, :cond_0

    .line 128
    return-void

    .line 108
    :cond_0
    aget-object v9, v3, v4

    invoke-virtual {v9}, Ljava/io/File;->isDirectory()Z

    move-result v9

    if-eqz v9, :cond_2

    .line 109
    aget-object v9, v3, v4

    invoke-direct {p0, v9, p2, p3}, Lcom/samsung/android/app/migration/task/BackupMemoTask;->zipFolder(Ljava/io/File;Ljava/io/File;Ljava/util/zip/ZipOutputStream;)V

    .line 107
    :cond_1
    :goto_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 111
    :cond_2
    const/4 v5, 0x0

    .line 113
    .local v5, "in":Ljava/io/FileInputStream;
    :try_start_0
    new-instance v6, Ljava/io/FileInputStream;

    aget-object v9, v3, v4

    invoke-direct {v6, v9}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 114
    .end local v5    # "in":Ljava/io/FileInputStream;
    .local v6, "in":Ljava/io/FileInputStream;
    :try_start_1
    new-instance v2, Ljava/util/zip/ZipEntry;

    aget-object v9, v3, v4

    invoke-virtual {v9}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v9

    .line 115
    invoke-virtual {p2}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v10

    add-int/lit8 v10, v10, 0x1

    .line 114
    invoke-virtual {v9, v10}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v9

    invoke-direct {v2, v9}, Ljava/util/zip/ZipEntry;-><init>(Ljava/lang/String;)V

    .line 116
    .local v2, "entry":Ljava/util/zip/ZipEntry;
    invoke-virtual {p3, v2}, Ljava/util/zip/ZipOutputStream;->putNextEntry(Ljava/util/zip/ZipEntry;)V

    .line 117
    :goto_2
    const/4 v9, -0x1

    invoke-virtual {v6, v0}, Ljava/io/FileInputStream;->read([B)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v8

    if-ne v9, v8, :cond_3

    .line 123
    if-eqz v6, :cond_1

    .line 124
    invoke-virtual {v6}, Ljava/io/FileInputStream;->close()V

    goto :goto_1

    .line 118
    :cond_3
    const/4 v9, 0x0

    :try_start_2
    invoke-virtual {p3, v0, v9, v8}, Ljava/util/zip/ZipOutputStream;->write([BII)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    goto :goto_2

    .line 120
    .end local v2    # "entry":Ljava/util/zip/ZipEntry;
    :catch_0
    move-exception v1

    move-object v5, v6

    .line 121
    .end local v6    # "in":Ljava/io/FileInputStream;
    .local v1, "e":Ljava/lang/Exception;
    .restart local v5    # "in":Ljava/io/FileInputStream;
    :goto_3
    :try_start_3
    const-string v9, "BackupMemoTask"

    const-string v10, "Exception occurred in zipFolder."

    invoke-static {v9, v10, v1}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 123
    if-eqz v5, :cond_1

    .line 124
    invoke-virtual {v5}, Ljava/io/FileInputStream;->close()V

    goto :goto_1

    .line 122
    .end local v1    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v9

    .line 123
    :goto_4
    if-eqz v5, :cond_4

    .line 124
    invoke-virtual {v5}, Ljava/io/FileInputStream;->close()V

    .line 125
    :cond_4
    throw v9

    .line 122
    .end local v5    # "in":Ljava/io/FileInputStream;
    .restart local v6    # "in":Ljava/io/FileInputStream;
    :catchall_1
    move-exception v9

    move-object v5, v6

    .end local v6    # "in":Ljava/io/FileInputStream;
    .restart local v5    # "in":Ljava/io/FileInputStream;
    goto :goto_4

    .line 120
    :catch_1
    move-exception v1

    goto :goto_3
.end method


# virtual methods
.method public hideDialog(Landroid/app/Activity;)V
    .locals 1
    .param p1, "context"    # Landroid/app/Activity;

    .prologue
    .line 213
    iput-object p1, p0, Lcom/samsung/android/app/migration/task/BackupMemoTask;->context:Landroid/app/Activity;

    .line 214
    invoke-direct {p0, p1}, Lcom/samsung/android/app/migration/task/BackupMemoTask;->isContextAlive(Landroid/app/Activity;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 215
    new-instance v0, Lcom/samsung/android/app/migration/task/BackupMemoTask$2;

    invoke-direct {v0, p0}, Lcom/samsung/android/app/migration/task/BackupMemoTask$2;-><init>(Lcom/samsung/android/app/migration/task/BackupMemoTask;)V

    invoke-virtual {p1, v0}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 222
    :cond_0
    return-void
.end method

.method public run()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 226
    iget-object v1, p0, Lcom/samsung/android/app/migration/task/BackupMemoTask;->ctx:Landroid/content/Context;

    invoke-static {v1}, Lcom/samsung/android/app/migration/utils/SharedPref;->isMemoRestoringInProcess(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 227
    iget-object v1, p0, Lcom/samsung/android/app/migration/task/BackupMemoTask;->ctx:Landroid/content/Context;

    iget-object v2, p0, Lcom/samsung/android/app/migration/task/BackupMemoTask;->source:Ljava/lang/String;

    const/4 v3, -0x1

    invoke-direct {p0, v1, v2, v3}, Lcom/samsung/android/app/migration/task/BackupMemoTask;->sendBackupResponse(Landroid/content/Context;Ljava/lang/String;I)V

    .line 242
    :goto_0
    return-void

    .line 229
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/app/migration/task/BackupMemoTask;->ctx:Landroid/content/Context;

    invoke-static {v1, v4}, Lcom/samsung/android/app/migration/utils/SharedPref;->setMemoBackingUpInProcessFlag(Landroid/content/Context;Z)V

    .line 230
    const/4 v1, 0x0

    .line 231
    iget-object v2, p0, Lcom/samsung/android/app/migration/task/BackupMemoTask;->ctx:Landroid/content/Context;

    const v3, 0x7f0b0053

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 230
    invoke-static {v1, v2, v4}, Lcom/samsung/android/app/migration/dialog/ProgressBarDialog;->newInstance(Ljava/lang/String;Ljava/lang/String;Z)Lcom/samsung/android/app/migration/dialog/ProgressBarDialog;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/app/migration/task/BackupMemoTask;->progressDialog:Lcom/samsung/android/app/migration/dialog/ProgressBarDialog;

    .line 232
    iget-object v1, p0, Lcom/samsung/android/app/migration/task/BackupMemoTask;->context:Landroid/app/Activity;

    invoke-virtual {p0, v1}, Lcom/samsung/android/app/migration/task/BackupMemoTask;->showDialog(Landroid/app/Activity;)V

    .line 233
    iget-object v1, p0, Lcom/samsung/android/app/migration/task/BackupMemoTask;->ctx:Landroid/content/Context;

    invoke-static {v1}, Lcom/samsung/android/app/memo/MemoDataHandler;->isDBEmpty(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 234
    iget-object v1, p0, Lcom/samsung/android/app/migration/task/BackupMemoTask;->ctx:Landroid/content/Context;

    iget-object v2, p0, Lcom/samsung/android/app/migration/task/BackupMemoTask;->source:Ljava/lang/String;

    const/4 v3, 0x3

    invoke-static {v1, v2, v4, v3}, Lcom/samsung/android/app/migration/receiver/KiesReceiver;->sendBackupResponse(Landroid/content/Context;Ljava/lang/String;II)V

    .line 239
    :goto_1
    iget-object v1, p0, Lcom/samsung/android/app/migration/task/BackupMemoTask;->context:Landroid/app/Activity;

    invoke-virtual {p0, v1}, Lcom/samsung/android/app/migration/task/BackupMemoTask;->hideDialog(Landroid/app/Activity;)V

    .line 240
    iget-object v1, p0, Lcom/samsung/android/app/migration/task/BackupMemoTask;->ctx:Landroid/content/Context;

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/samsung/android/app/migration/utils/SharedPref;->setMemoBackingUpInProcessFlag(Landroid/content/Context;Z)V

    goto :goto_0

    .line 236
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/app/migration/task/BackupMemoTask;->ctx:Landroid/content/Context;

    iget-object v2, p0, Lcom/samsung/android/app/migration/task/BackupMemoTask;->targetDir:Ljava/lang/String;

    iget-object v3, p0, Lcom/samsung/android/app/migration/task/BackupMemoTask;->sessionKey:Ljava/lang/String;

    invoke-direct {p0, v1, v2, v3}, Lcom/samsung/android/app/migration/task/BackupMemoTask;->backupMemoDB(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 237
    .local v0, "res":I
    iget-object v1, p0, Lcom/samsung/android/app/migration/task/BackupMemoTask;->ctx:Landroid/content/Context;

    iget-object v2, p0, Lcom/samsung/android/app/migration/task/BackupMemoTask;->source:Ljava/lang/String;

    invoke-direct {p0, v1, v2, v0}, Lcom/samsung/android/app/migration/task/BackupMemoTask;->sendBackupResponse(Landroid/content/Context;Ljava/lang/String;I)V

    goto :goto_1
.end method

.method public showDialog(Landroid/app/Activity;)V
    .locals 1
    .param p1, "context"    # Landroid/app/Activity;

    .prologue
    .line 201
    iput-object p1, p0, Lcom/samsung/android/app/migration/task/BackupMemoTask;->context:Landroid/app/Activity;

    .line 202
    invoke-direct {p0, p1}, Lcom/samsung/android/app/migration/task/BackupMemoTask;->isContextAlive(Landroid/app/Activity;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Landroid/app/Activity;->isResumed()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 203
    new-instance v0, Lcom/samsung/android/app/migration/task/BackupMemoTask$1;

    invoke-direct {v0, p0, p1}, Lcom/samsung/android/app/migration/task/BackupMemoTask$1;-><init>(Lcom/samsung/android/app/migration/task/BackupMemoTask;Landroid/app/Activity;)V

    invoke-virtual {p1, v0}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 210
    :cond_0
    return-void
.end method
