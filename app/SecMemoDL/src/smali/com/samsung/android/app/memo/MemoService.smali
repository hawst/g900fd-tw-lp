.class public Lcom/samsung/android/app/memo/MemoService;
.super Landroid/app/Service;
.source "MemoService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/app/memo/MemoService$MemoServiceIFImpl;
    }
.end annotation


# static fields
.field public static final ACTION:Ljava/lang/String; = "com.samsung.android.intent.action.MEMO_SERVICE"

.field public static final CATEGORY:Ljava/lang/String; = "com.samsung.android.intent.category.APP_MEMO"

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mStartId:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 46
    const-class v0, Lcom/samsung/android/app/memo/MemoService;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/app/memo/MemoService;->TAG:Ljava/lang/String;

    .line 56
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 45
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    return-void
.end method

.method static synthetic access$0()Ljava/lang/String;
    .locals 1

    .prologue
    .line 46
    sget-object v0, Lcom/samsung/android/app/memo/MemoService;->TAG:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 2
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 221
    const-string v0, "com.samsung.android.intent.action.MEMO_SERVICE"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "MemoService"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 222
    :cond_0
    sget-object v0, Lcom/samsung/android/app/memo/MemoService;->TAG:Ljava/lang/String;

    const-string v1, "onBind()"

    invoke-static {v0, v1}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 223
    new-instance v0, Lcom/samsung/android/app/memo/MemoService$MemoServiceIFImpl;

    invoke-direct {v0, p0}, Lcom/samsung/android/app/memo/MemoService$MemoServiceIFImpl;-><init>(Landroid/content/Context;)V

    .line 225
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onCreate()V
    .locals 0

    .prologue
    .line 198
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 199
    return-void
.end method

.method public onDestroy()V
    .locals 0

    .prologue
    .line 216
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 217
    return-void
.end method

.method public onRebind(Landroid/content/Intent;)V
    .locals 0
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 230
    invoke-super {p0, p1}, Landroid/app/Service;->onRebind(Landroid/content/Intent;)V

    .line 231
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 2
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "flags"    # I
    .param p3, "startId"    # I

    .prologue
    .line 203
    iput p3, p0, Lcom/samsung/android/app/memo/MemoService;->mStartId:I

    .line 204
    if-eqz p1, :cond_0

    .line 205
    const-string v0, "com.samsung.android.intent.action.MEMO_SERVICE"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 206
    invoke-super {p0, p1, p2, p3}, Landroid/app/Service;->onStartCommand(Landroid/content/Intent;II)I

    .line 207
    const/4 v0, 0x1

    .line 211
    :goto_0
    return v0

    .line 210
    :cond_0
    iget v0, p0, Lcom/samsung/android/app/memo/MemoService;->mStartId:I

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/memo/MemoService;->stopSelfResult(I)Z

    .line 211
    const/4 v0, 0x2

    goto :goto_0
.end method

.method public onTaskRemoved(Landroid/content/Intent;)V
    .locals 0
    .param p1, "rootIntent"    # Landroid/content/Intent;

    .prologue
    .line 240
    invoke-super {p0, p1}, Landroid/app/Service;->onTaskRemoved(Landroid/content/Intent;)V

    .line 241
    return-void
.end method

.method public onUnbind(Landroid/content/Intent;)Z
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 235
    invoke-super {p0, p1}, Landroid/app/Service;->onUnbind(Landroid/content/Intent;)Z

    move-result v0

    return v0
.end method
