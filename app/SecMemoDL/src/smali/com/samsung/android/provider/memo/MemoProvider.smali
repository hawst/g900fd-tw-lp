.class public Lcom/samsung/android/provider/memo/MemoProvider;
.super Landroid/content/ContentProvider;
.source "MemoProvider.java"


# static fields
.field private static final M_CLIPBOARD:I = 0xc

.field private static final M_CTGR_ALL:I = 0x9

.field private static final M_CTGR_ID:I = 0x7

.field private static final M_CTGR_ONE:I = 0x8

.field private static final M_FILE_ALL:I = 0x6

.field private static final M_FILE_ID:I = 0x4

.field private static final M_FILE_ONE:I = 0x5

.field private static final M_GOOGLE_SEARCH:I = 0xa

.field private static final M_MEMO_ALL:I = 0x3

.field private static final M_MEMO_ID:I = 0x1

.field private static final M_MEMO_ONE:I = 0x2

.field private static final M_REGEX_SEARCH:I = 0xb

.field private static final M_SEARCH:I = 0x0

.field private static final M_SYNC_ALL:I = 0xf

.field private static final M_SYNC_ID:I = 0xd

.field private static final M_SYNC_ONE:I = 0xe

.field private static final PDM_PKG_NAME:Ljava/lang/String; = "com.samsung.ssss"

.field private static final PDM_QUERY_PARAM_NAME:Ljava/lang/String; = "caller_is_syncadapter"

.field private static final PDM_QUERY_PARAM_VALUE:Ljava/lang/String; = "true"

.field private static final PROJECTION_MIME:[Ljava/lang/String;

.field private static final PROJECTION_UUID:[Ljava/lang/String;

.field private static final TAG:Ljava/lang/String;

.field private static isFromClipBoard:Z

.field private static final sUriMatcher:Landroid/content/UriMatcher;


# instance fields
.field private mCFL:Lcom/samsung/android/app/memo/util/CFLHelper;

.field private mMemoDBHelper:Landroid/database/sqlite/SQLiteOpenHelper;

.field private mPDMUid:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 83
    const-class v0, Lcom/samsung/android/provider/memo/MemoProvider;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/provider/memo/MemoProvider;->TAG:Ljava/lang/String;

    .line 89
    new-array v0, v5, [Ljava/lang/String;

    const-string v1, "mime_type"

    aput-object v1, v0, v4

    sput-object v0, Lcom/samsung/android/provider/memo/MemoProvider;->PROJECTION_MIME:[Ljava/lang/String;

    .line 90
    new-array v0, v5, [Ljava/lang/String;

    const-string v1, "UUID"

    aput-object v1, v0, v4

    sput-object v0, Lcom/samsung/android/provider/memo/MemoProvider;->PROJECTION_UUID:[Ljava/lang/String;

    .line 173
    sput-boolean v4, Lcom/samsung/android/provider/memo/MemoProvider;->isFromClipBoard:Z

    .line 176
    new-instance v0, Landroid/content/UriMatcher;

    const/4 v1, -0x1

    invoke-direct {v0, v1}, Landroid/content/UriMatcher;-><init>(I)V

    sput-object v0, Lcom/samsung/android/provider/memo/MemoProvider;->sUriMatcher:Landroid/content/UriMatcher;

    .line 178
    sget-object v0, Lcom/samsung/android/provider/memo/MemoProvider;->sUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.samsung.android.memo"

    const-string v2, "category/#"

    const/4 v3, 0x7

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 179
    sget-object v0, Lcom/samsung/android/provider/memo/MemoProvider;->sUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.samsung.android.memo"

    const-string v2, "category"

    const/16 v3, 0x9

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 181
    sget-object v0, Lcom/samsung/android/provider/memo/MemoProvider;->sUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.samsung.android.memo"

    const-string v2, "memo/#"

    invoke-virtual {v0, v1, v2, v5}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 182
    sget-object v0, Lcom/samsung/android/provider/memo/MemoProvider;->sUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.samsung.android.memo"

    const-string v2, "memo"

    const/4 v3, 0x3

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 183
    sget-object v0, Lcom/samsung/android/provider/memo/MemoProvider;->sUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.samsung.android.memo"

    const-string v2, "#"

    invoke-virtual {v0, v1, v2, v5}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 185
    sget-object v0, Lcom/samsung/android/provider/memo/MemoProvider;->sUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.samsung.android.memo"

    const-string v2, "file/#"

    const/4 v3, 0x4

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 186
    sget-object v0, Lcom/samsung/android/provider/memo/MemoProvider;->sUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.samsung.android.memo"

    const-string v2, "file"

    const/4 v3, 0x6

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 188
    sget-object v0, Lcom/samsung/android/provider/memo/MemoProvider;->sUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.samsung.android.memo"

    const-string v2, "search/*"

    invoke-virtual {v0, v1, v2, v4}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 189
    sget-object v0, Lcom/samsung/android/provider/memo/MemoProvider;->sUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.samsung.android.memo"

    const-string v2, "search_suggest_query"

    const/16 v3, 0xa

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 190
    sget-object v0, Lcom/samsung/android/provider/memo/MemoProvider;->sUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.samsung.android.memo"

    const-string v2, "search_suggest_regex_query"

    const/16 v3, 0xb

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 191
    sget-object v0, Lcom/samsung/android/provider/memo/MemoProvider;->sUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.samsung.android.memo"

    new-instance v2, Ljava/lang/StringBuilder;

    sget-object v3, Lcom/samsung/android/provider/memo/MemoContract;->BASE_URI_CLIPBOARD:Landroid/net/Uri;

    invoke-virtual {v3}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, "/*"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/16 v3, 0xc

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 193
    sget-object v0, Lcom/samsung/android/provider/memo/MemoProvider;->sUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.samsung.android.memo"

    const-string v2, "_sync_state/#"

    const/16 v3, 0xd

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 194
    sget-object v0, Lcom/samsung/android/provider/memo/MemoProvider;->sUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.samsung.android.memo"

    const-string v2, "_sync_state"

    const/16 v3, 0xf

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 195
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 82
    invoke-direct {p0}, Landroid/content/ContentProvider;-><init>()V

    return-void
.end method

.method private deleteFilesForMemoUUIDs(Landroid/database/sqlite/SQLiteDatabase;Ljava/util/List;)I
    .locals 14
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/sqlite/SQLiteDatabase;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)I"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/database/SQLException;
        }
    .end annotation

    .prologue
    .line 1008
    .local p2, "memoUUIDs":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/4 v9, -0x1

    .line 1009
    .local v9, "ret":I
    invoke-interface/range {p2 .. p2}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_0

    .line 1010
    const/4 v0, 0x0

    .line 1035
    :goto_0
    return v0

    .line 1012
    :cond_0
    new-instance v13, Ljava/util/ArrayList;

    const/16 v0, 0x1e

    invoke-direct {v13, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 1014
    .local v13, "uuids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    new-instance v10, Ljava/lang/StringBuffer;

    const-string v0, "memoUUID"

    invoke-direct {v10, v0}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 1015
    .local v10, "sb":Ljava/lang/StringBuffer;
    const-string v0, " IN ("

    invoke-virtual {v10, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1016
    invoke-interface/range {p2 .. p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-nez v1, :cond_2

    .line 1019
    invoke-virtual {v10}, Ljava/lang/StringBuffer;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    const/16 v1, 0x29

    invoke-virtual {v10, v0, v1}, Ljava/lang/StringBuffer;->setCharAt(IC)V

    .line 1020
    invoke-virtual {v10}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1021
    .local v3, "selection":Ljava/lang/String;
    const/4 v8, 0x0

    .line 1023
    .local v8, "c":Landroid/database/Cursor;
    :try_start_0
    const-string v1, "file"

    sget-object v2, Lcom/samsung/android/provider/memo/MemoProvider;->PROJECTION_UUID:[Ljava/lang/String;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 1024
    :goto_2
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-nez v0, :cond_3

    .line 1027
    const-string v0, "file"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v3, v1}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v9

    .line 1028
    invoke-static {v13}, Lcom/samsung/android/app/memo/util/FileHelper;->deletePrivateFiles(Ljava/util/List;)J
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1033
    if-eqz v8, :cond_1

    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    :cond_1
    move v0, v9

    .line 1035
    goto :goto_0

    .line 1016
    .end local v3    # "selection":Ljava/lang/String;
    .end local v8    # "c":Landroid/database/Cursor;
    :cond_2
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Ljava/lang/String;

    .line 1017
    .local v12, "uuid":Ljava/lang/String;
    const/16 v1, 0x27

    invoke-virtual {v10, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, v12}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const/16 v2, 0x27

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    move-result-object v1

    const/16 v2, 0x2c

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    goto :goto_1

    .line 1025
    .end local v12    # "uuid":Ljava/lang/String;
    .restart local v3    # "selection":Ljava/lang/String;
    .restart local v8    # "c":Landroid/database/Cursor;
    :cond_3
    const/4 v0, 0x0

    :try_start_1
    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v13, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Landroid/database/SQLException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2

    .line 1029
    :catch_0
    move-exception v11

    .line 1030
    .local v11, "sqle":Landroid/database/SQLException;
    :try_start_2
    sget-object v0, Lcom/samsung/android/provider/memo/MemoProvider;->TAG:Ljava/lang/String;

    const-string v1, "buildFileListToBeDeleted()"

    invoke-static {v0, v1, v11}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1031
    throw v11
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1032
    .end local v11    # "sqle":Landroid/database/SQLException;
    :catchall_0
    move-exception v0

    .line 1033
    if-eqz v8, :cond_4

    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 1034
    :cond_4
    throw v0
.end method

.method private getFileUUIDs(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)Ljava/util/List;
    .locals 11
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "memoUUID"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/sqlite/SQLiteDatabase;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 629
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 630
    .local v9, "fileUUIDsList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const-string v1, "file"

    const/4 v0, 0x2

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v3, "UUID"

    aput-object v3, v2, v0

    const/4 v0, 0x1

    const-string v3, "mime_type"

    aput-object v3, v2, v0

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v3, "memoUUID=\'"

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "\'"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object v0, p1

    move-object v5, v4

    move-object v6, v4

    move-object v7, v4

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 631
    .local v8, "c":Landroid/database/Cursor;
    if-eqz v8, :cond_2

    .line 632
    invoke-interface {v8}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_1

    .line 633
    :cond_0
    :goto_0
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-nez v0, :cond_3

    .line 639
    :cond_1
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 641
    :cond_2
    return-object v9

    .line 634
    :cond_3
    const-string v0, "mime_type"

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 635
    .local v10, "mimeType":Ljava/lang/String;
    if-eqz v10, :cond_0

    const-string v0, "audio/mp4"

    invoke-virtual {v10, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 636
    const-string v0, "UUID"

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v9, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method private getMimeType(Landroid/net/Uri;I)Ljava/lang/String;
    .locals 11
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "match"    # I

    .prologue
    .line 349
    const-string v10, "vnd.android.cursor.item/vnd.samsung.memo.file"

    .line 350
    .local v10, "type":Ljava/lang/String;
    const/4 v0, 0x5

    if-ne p2, v0, :cond_2

    .line 351
    const-string v3, "UUID IS ?"

    .line 355
    .local v3, "selection":Ljava/lang/String;
    :goto_0
    const/4 v8, 0x0

    .line 357
    .local v8, "c":Landroid/database/Cursor;
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/provider/memo/MemoProvider;->mMemoDBHelper:Landroid/database/sqlite/SQLiteOpenHelper;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteOpenHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 358
    const-string v1, "file"

    sget-object v2, Lcom/samsung/android/provider/memo/MemoProvider;->PROJECTION_MIME:[Ljava/lang/String;

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 359
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 360
    const/4 v0, 0x0

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v10

    .line 365
    :cond_0
    if-eqz v8, :cond_1

    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 367
    :cond_1
    :goto_1
    return-object v10

    .line 352
    .end local v3    # "selection":Ljava/lang/String;
    .end local v8    # "c":Landroid/database/Cursor;
    :cond_2
    const-string v3, "_id IS ?"

    goto :goto_0

    .line 362
    .restart local v3    # "selection":Ljava/lang/String;
    .restart local v8    # "c":Landroid/database/Cursor;
    :catch_0
    move-exception v9

    .line 363
    .local v9, "sqle":Landroid/database/SQLException;
    :try_start_1
    sget-object v0, Lcom/samsung/android/provider/memo/MemoProvider;->TAG:Ljava/lang/String;

    const-string v1, ""

    invoke-static {v0, v1, v9}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 365
    if-eqz v8, :cond_1

    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    goto :goto_1

    .line 364
    .end local v9    # "sqle":Landroid/database/SQLException;
    :catchall_0
    move-exception v0

    .line 365
    if-eqz v8, :cond_3

    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 366
    :cond_3
    throw v0
.end method

.method private getRegexSuggestion(Landroid/net/Uri;[Ljava/lang/String;)Landroid/database/Cursor;
    .locals 18
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "selectionArgs"    # [Ljava/lang/String;

    .prologue
    .line 384
    const-string v10, ""

    .line 385
    .local v10, "selection_args":Ljava/lang/String;
    const-string v8, ""

    .line 386
    .local v8, "query":Ljava/lang/String;
    const/4 v3, 0x0

    .line 387
    .local v3, "columns":[Ljava/lang/String;
    const/4 v2, 0x0

    .line 388
    .local v2, "args":[Ljava/lang/String;
    const/4 v6, 0x0

    .line 389
    .local v6, "flag":Z
    const-string v12, "stime"

    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 390
    .local v11, "stime":Ljava/lang/String;
    const-string v12, "etime"

    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 391
    .local v5, "etime":Ljava/lang/String;
    if-eqz v11, :cond_0

    if-eqz v5, :cond_0

    .line 392
    const/4 v6, 0x1

    .line 393
    new-instance v12, Ljava/lang/StringBuilder;

    const-string v13, "lastModifiedAt >= "

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v12, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, " AND "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    .line 394
    const-string v13, "lastModifiedAt"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, " <= "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    .line 393
    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    .line 396
    :cond_0
    if-eqz p2, :cond_1

    const/4 v12, 0x0

    aget-object v12, p2, v12

    const-string v13, "[]"

    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_2

    .line 397
    :cond_1
    const/4 v2, 0x0

    .line 398
    const-string v8, ""

    .line 407
    :goto_0
    new-instance v12, Ljava/lang/StringBuilder;

    const-string v13, "\'"

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v13, Lcom/samsung/android/provider/memo/MemoContract;->AUTHORITY_URI:Landroid/net/Uri;

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "/\'||"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 408
    .local v7, "imgString":Ljava/lang/String;
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/provider/memo/MemoProvider;->getContext()Landroid/content/Context;

    move-result-object v12

    invoke-static {v12}, Lcom/samsung/android/app/memo/util/Utils;->getQueryDateFormat(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    .line 409
    .local v4, "dateFormat":Ljava/lang/String;
    const/16 v12, 0x8

    new-array v3, v12, [Ljava/lang/String;

    .end local v3    # "columns":[Ljava/lang/String;
    const/4 v12, 0x0

    .line 410
    const-string v13, "_id"

    aput-object v13, v3, v12

    const/4 v12, 0x1

    .line 411
    const-string v13, "title AS suggest_text_1"

    aput-object v13, v3, v12

    const/4 v12, 0x2

    .line 412
    const-string v13, "strippedContent AS suggest_text_2"

    aput-object v13, v3, v12

    const/4 v12, 0x3

    .line 413
    new-instance v13, Ljava/lang/StringBuilder;

    const-string v14, "CASE WHEN _data=\'\' THEN NULL ELSE "

    invoke-direct {v13, v14}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v13, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "UUID"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    .line 414
    const-string v14, " END"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, " AS "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "suggest_icon_1"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    .line 413
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    aput-object v13, v3, v12

    const/4 v12, 0x4

    .line 415
    new-instance v13, Ljava/lang/StringBuilder;

    const-string v14, "CASE WHEN lastModifiedAt/(1000*60*60*24)!="

    invoke-direct {v13, v14}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Lcom/samsung/android/app/memo/util/Utils;->getCurrentUTCTimeMillis()J

    move-result-wide v14

    const-wide/32 v16, 0x5265c00

    div-long v14, v14, v16

    invoke-virtual {v13, v14, v15}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v13

    .line 416
    const-string v14, " THEN "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    .line 417
    const-string v14, "strftime("

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "\',"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "lastModifiedAt"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "/1000 , \"unixepoch\",\"localtime\")"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    .line 418
    const-string v14, " ELSE "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    .line 419
    const-string v14, "strftime("

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "\'%H:%M"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "\',"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "lastModifiedAt"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "/1000 , \"unixepoch\",\"localtime\")"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    .line 420
    const-string v14, " END "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    .line 421
    const-string v14, " AS "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "suggest_text_3"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    .line 415
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    aput-object v13, v3, v12

    const/4 v12, 0x5

    .line 422
    const-string v13, "_id AS suggest_intent_data_id"

    aput-object v13, v3, v12

    const/4 v12, 0x6

    .line 427
    const-string v13, "title || strippedContent AS suggest_uri"

    aput-object v13, v3, v12

    const/4 v12, 0x7

    .line 429
    const-string v13, "\'text/plain\' AS suggest_mime_type"

    aput-object v13, v3, v12

    .line 432
    .restart local v3    # "columns":[Ljava/lang/String;
    if-nez v2, :cond_3

    .line 433
    move-object/from16 v0, p0

    invoke-direct {v0, v8, v3, v10, v6}, Lcom/samsung/android/provider/memo/MemoProvider;->search(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Z)Landroid/database/Cursor;

    move-result-object v12

    .line 435
    :goto_1
    return-object v12

    .line 400
    .end local v4    # "dateFormat":Ljava/lang/String;
    .end local v7    # "imgString":Ljava/lang/String;
    :cond_2
    new-instance v9, Lcom/samsung/android/provider/memo/MemoQueryParser;

    invoke-direct {v9}, Lcom/samsung/android/provider/memo/MemoQueryParser;-><init>()V

    .line 401
    .local v9, "queryParser":Lcom/samsung/android/provider/memo/MemoQueryParser;
    const/4 v12, 0x0

    aget-object v12, p2, v12

    invoke-virtual {v12}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v9, v12}, Lcom/samsung/android/provider/memo/MemoQueryParser;->regexParser(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 402
    const/4 v12, 0x0

    aget-object v8, v2, v12

    goto/16 :goto_0

    .line 435
    .end local v9    # "queryParser":Lcom/samsung/android/provider/memo/MemoQueryParser;
    .restart local v4    # "dateFormat":Ljava/lang/String;
    .restart local v7    # "imgString":Ljava/lang/String;
    :cond_3
    move-object/from16 v0, p0

    invoke-direct {v0, v2, v3, v10, v6}, Lcom/samsung/android/provider/memo/MemoProvider;->searchRegex([Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Z)Landroid/database/Cursor;

    move-result-object v12

    goto :goto_1
.end method

.method private getSelectionParams(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/String;
    .locals 4
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "selection"    # Ljava/lang/String;
    .param p3, "lastPathSeg"    # Ljava/lang/String;
    .param p4, "match"    # I

    .prologue
    const/16 v3, 0x29

    .line 1130
    packed-switch p4, :pswitch_data_0

    .line 1161
    :pswitch_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid URI: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1141
    :pswitch_1
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "UUID=\'"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1143
    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 1144
    const/16 v1, 0x27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 1145
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, ""

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 1141
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    .line 1163
    :goto_1
    :pswitch_2
    return-object p2

    .line 1145
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, " AND ("

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 1146
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1152
    :pswitch_3
    invoke-static {p3}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    .line 1153
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "_id= "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1155
    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 1156
    const/16 v1, 0x20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 1157
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, ""

    :goto_2
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 1153
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    .line 1159
    goto :goto_1

    .line 1157
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, " AND ("

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 1158
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    .line 1130
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private getSuggestions(Ljava/lang/String;)Landroid/database/Cursor;
    .locals 6
    .param p1, "query"    # Ljava/lang/String;

    .prologue
    const/4 v5, 0x0

    .line 371
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "\'"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v3, Lcom/samsung/android/provider/memo/MemoContract;->AUTHORITY_URI:Landroid/net/Uri;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/\'||"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 372
    .local v1, "imgString":Ljava/lang/String;
    const/4 v2, 0x6

    new-array v0, v2, [Ljava/lang/String;

    .line 373
    const-string v2, "_id"

    aput-object v2, v0, v5

    const/4 v2, 0x1

    .line 374
    const-string v3, "title AS suggest_text_1"

    aput-object v3, v0, v2

    const/4 v2, 0x2

    .line 375
    const-string v3, "strippedContent AS suggest_text_2_url"

    aput-object v3, v0, v2

    const/4 v2, 0x3

    .line 376
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, "UUID"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " AS "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "suggest_icon_1"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v0, v2

    const/4 v2, 0x4

    .line 377
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, "UUID"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " AS "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "suggest_icon_2"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v0, v2

    const/4 v2, 0x5

    .line 378
    const-string v3, "_id AS suggest_intent_data_id"

    aput-object v3, v0, v2

    .line 380
    .local v0, "columns":[Ljava/lang/String;
    const-string v2, ""

    invoke-direct {p0, p1, v0, v2, v5}, Lcom/samsung/android/provider/memo/MemoProvider;->search(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Z)Landroid/database/Cursor;

    move-result-object v2

    return-object v2
.end method

.method private getTableName(I)Ljava/lang/String;
    .locals 4
    .param p1, "match"    # I

    .prologue
    .line 243
    packed-switch p1, :pswitch_data_0

    .line 267
    :pswitch_0
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unknown match: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 247
    :pswitch_1
    const-string v0, "file"

    .line 269
    .local v0, "tblName":Ljava/lang/String;
    :goto_0
    sget-object v1, Lcom/samsung/android/provider/memo/MemoProvider;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "getTblName() "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 270
    return-object v0

    .line 254
    .end local v0    # "tblName":Ljava/lang/String;
    :pswitch_2
    const-string v0, "memo"

    .line 255
    .restart local v0    # "tblName":Ljava/lang/String;
    goto :goto_0

    .line 259
    .end local v0    # "tblName":Ljava/lang/String;
    :pswitch_3
    const-string v0, "category"

    .line 260
    .restart local v0    # "tblName":Ljava/lang/String;
    goto :goto_0

    .line 264
    .end local v0    # "tblName":Ljava/lang/String;
    :pswitch_4
    const-string v0, "_sync_state"

    .line 265
    .restart local v0    # "tblName":Ljava/lang/String;
    goto :goto_0

    .line 243
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_4
        :pswitch_4
        :pswitch_4
    .end packed-switch
.end method

.method private getUUIDs(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)Ljava/util/List;
    .locals 11
    .param p1, "tableName"    # Ljava/lang/String;
    .param p2, "selection"    # Ljava/lang/String;
    .param p3, "selectionArgs"    # [Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "[",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/database/SQLException;
        }
    .end annotation

    .prologue
    .line 985
    new-instance v10, Ljava/util/ArrayList;

    const/16 v1, 0x1e

    invoke-direct {v10, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 987
    .local v10, "uuids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/4 v8, 0x0

    .line 988
    .local v8, "c":Landroid/database/Cursor;
    const/4 v0, 0x0

    .line 990
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/provider/memo/MemoProvider;->mMemoDBHelper:Landroid/database/sqlite/SQLiteOpenHelper;

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteOpenHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 991
    const/4 v1, 0x1

    new-array v2, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    .line 992
    const-string v3, "UUID"

    aput-object v3, v2, v1

    .line 993
    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v1, p1

    move-object v3, p2

    move-object v4, p3

    .line 991
    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 994
    :goto_0
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-nez v1, :cond_1

    .line 1001
    if-eqz v8, :cond_0

    .line 1002
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 1004
    :cond_0
    return-object v10

    .line 995
    :cond_1
    const/4 v1, 0x0

    :try_start_1
    invoke-interface {v8, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v10, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Landroid/database/SQLException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 997
    :catch_0
    move-exception v9

    .line 998
    .local v9, "sqle":Landroid/database/SQLException;
    :try_start_2
    sget-object v1, Lcom/samsung/android/provider/memo/MemoProvider;->TAG:Ljava/lang/String;

    const-string v2, "buildFileListToBeDeleted()"

    invoke-static {v1, v2, v9}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 999
    throw v9
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1000
    .end local v9    # "sqle":Landroid/database/SQLException;
    :catchall_0
    move-exception v1

    .line 1001
    if-eqz v8, :cond_2

    .line 1002
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 1003
    :cond_2
    throw v1
.end method

.method private insertFileEntry(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 11
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "oldFileUUID"    # Ljava/lang/String;
    .param p3, "newFileUUID"    # Ljava/lang/String;
    .param p4, "newMemoUUID"    # Ljava/lang/String;

    .prologue
    const/4 v10, 0x1

    const/4 v2, 0x0

    .line 645
    const-string v1, "file"

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v3, "UUID=\'"

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "\'"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object v0, p1

    move-object v4, v2

    move-object v5, v2

    move-object v6, v2

    move-object v7, v2

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 646
    .local v8, "c":Landroid/database/Cursor;
    if-eqz v8, :cond_1

    .line 647
    invoke-interface {v8}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_0

    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 648
    new-instance v9, Landroid/content/ContentValues;

    invoke-direct {v9}, Landroid/content/ContentValues;-><init>()V

    .line 649
    .local v9, "values":Landroid/content/ContentValues;
    const-string v0, "UUID"

    invoke-virtual {v9, v0, p3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 650
    const-string v0, "isDirty"

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v9, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 651
    const-string v0, "isDeleted"

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v9, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 652
    const-string v0, "accountType"

    const-string v1, "accountType"

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v9, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 653
    const-string v0, "accountName"

    const-string v1, "accountName"

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v9, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 654
    const-string v0, "sync1"

    invoke-static {}, Lcom/samsung/android/app/memo/util/Utils;->getCorrectedTime()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v9, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 655
    const-string v0, "memoUUID"

    invoke-virtual {v9, v0, p4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 656
    const-string v0, "mime_type"

    const-string v1, "mime_type"

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v9, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 657
    const-string v0, "_display_name"

    const-string v1, "_display_name"

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v9, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 658
    const-string v0, "_size"

    const-string v1, "_size"

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v9, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 659
    const-string v0, "orientation"

    const-string v1, "orientation"

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v9, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 660
    const-string v0, "_data"

    invoke-static {p3}, Lcom/samsung/android/app/memo/util/FileHelper;->getPrivateFilePath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/samsung/android/app/memo/util/Utils;->getRelativePath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v9, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 662
    invoke-static {p2}, Lcom/samsung/android/app/memo/util/FileHelper;->getPrivateFile(Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    invoke-static {p3}, Lcom/samsung/android/app/memo/util/FileHelper;->getPrivateFile(Ljava/lang/String;)Ljava/io/File;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/app/memo/util/FileHelper;->copyFileItem(Ljava/io/File;Ljava/io/File;)I

    .line 664
    const-string v0, "file"

    invoke-virtual {p1, v0, v2, v9}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    .line 665
    invoke-virtual {p0}, Lcom/samsung/android/provider/memo/MemoProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/provider/memo/MemoContract;->BASE_URI_FILE:Landroid/net/Uri;

    invoke-virtual {v0, v1, v2, v10}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;Z)V

    .line 667
    .end local v9    # "values":Landroid/content/ContentValues;
    :cond_0
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 669
    :cond_1
    return-void
.end method

.method private isCalledByPDM(Landroid/net/Uri;)Z
    .locals 8
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    const/4 v7, -0x1

    const/4 v3, 0x0

    .line 274
    iget v4, p0, Lcom/samsung/android/provider/memo/MemoProvider;->mPDMUid:I

    if-ne v4, v7, :cond_0

    .line 275
    monitor-enter p0

    .line 277
    :try_start_0
    invoke-virtual {p0}, Lcom/samsung/android/provider/memo/MemoProvider;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    const-string v5, "com.samsung.ssss"

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v0

    .line 278
    .local v0, "appInfo":Landroid/content/pm/ApplicationInfo;
    iget v4, v0, Landroid/content/pm/ApplicationInfo;->uid:I

    iput v4, p0, Lcom/samsung/android/provider/memo/MemoProvider;->mPDMUid:I
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 275
    .end local v0    # "appInfo":Landroid/content/pm/ApplicationInfo;
    :goto_0
    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 285
    :cond_0
    iget v4, p0, Lcom/samsung/android/provider/memo/MemoProvider;->mPDMUid:I

    if-eq v4, v7, :cond_2

    .line 286
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v1

    .line 287
    .local v1, "callerUid":I
    iget v4, p0, Lcom/samsung/android/provider/memo/MemoProvider;->mPDMUid:I

    if-ne v1, v4, :cond_1

    const/4 v3, 0x1

    .line 292
    .end local v1    # "callerUid":I
    :cond_1
    :goto_1
    return v3

    .line 275
    :catchall_0
    move-exception v3

    :try_start_2
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v3

    .line 288
    :cond_2
    if-eqz p1, :cond_1

    .line 289
    const-string v3, "caller_is_syncadapter"

    invoke-virtual {p1, v3}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 290
    .local v2, "param":Ljava/lang/String;
    const-string v3, "true"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    goto :goto_1

    .line 279
    .end local v2    # "param":Ljava/lang/String;
    :catch_0
    move-exception v4

    goto :goto_0
.end method

.method private isForce(Landroid/net/Uri;)Z
    .locals 2
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 308
    const-string v0, "force"

    invoke-virtual {p1}, Landroid/net/Uri;->getEncodedFragment()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->decode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private isNeedUpsync(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)Z
    .locals 11
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "tblName"    # Ljava/lang/String;
    .param p3, "selection"    # Ljava/lang/String;
    .param p4, "selectionArgs"    # [Ljava/lang/String;

    .prologue
    .line 615
    const/4 v9, 0x0

    .line 616
    .local v9, "res":Z
    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "isNeedUpsync"

    aput-object v1, v2, v0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v0, p1

    move-object v1, p2

    move-object v3, p3

    move-object v4, p4

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 617
    .local v8, "c":Landroid/database/Cursor;
    if-eqz v8, :cond_1

    .line 618
    invoke-interface {v8}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_0

    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 619
    const-string v0, "isNeedUpsync"

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v10

    .line 620
    .local v10, "val":I
    const/4 v0, 0x1

    if-ne v10, v0, :cond_0

    .line 621
    const/4 v9, 0x1

    .line 623
    .end local v10    # "val":I
    :cond_0
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 625
    :cond_1
    return v9
.end method

.method private isSAregistered()Z
    .locals 1

    .prologue
    .line 296
    invoke-virtual {p0}, Lcom/samsung/android/provider/memo/MemoProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/android/app/memo/util/AccountHelper;->getRegisteredSamsungAccount(Landroid/content/Context;)Landroid/accounts/Account;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isUUIDExist(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 10
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "tblName"    # Ljava/lang/String;
    .param p3, "uuid"    # Ljava/lang/String;

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    const/4 v5, 0x0

    .line 601
    const/4 v9, 0x0

    .line 602
    .local v9, "res":Z
    const/4 v0, 0x3

    new-array v2, v0, [Ljava/lang/String;

    const-string v0, "UUID"

    aput-object v0, v2, v6

    const-string v0, "isDeleted"

    aput-object v0, v2, v7

    const/4 v0, 0x2

    const-string v1, "sync1"

    aput-object v1, v2, v0

    .line 603
    .local v2, "projection":[Ljava/lang/String;
    const-string v3, "UUID = ?"

    .line 604
    .local v3, "selection":Ljava/lang/String;
    new-array v4, v7, [Ljava/lang/String;

    aput-object p3, v4, v6

    .local v4, "selectionArgs":[Ljava/lang/String;
    move-object v0, p1

    move-object v1, p2

    move-object v6, v5

    move-object v7, v5

    .line 605
    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 606
    .local v8, "c":Landroid/database/Cursor;
    if-eqz v8, :cond_1

    .line 607
    invoke-interface {v8}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_0

    .line 608
    const/4 v9, 0x1

    .line 609
    :cond_0
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 611
    :cond_1
    return v9
.end method

.method private isUndo(Landroid/net/Uri;)Z
    .locals 2
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 304
    const-string v0, "undo"

    invoke-virtual {p1}, Landroid/net/Uri;->getEncodedFragment()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->decode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private isUndoable(Landroid/net/Uri;)Z
    .locals 2
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 300
    const-string v0, "undoable"

    invoke-virtual {p1}, Landroid/net/Uri;->getEncodedFragment()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->decode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private static match(Landroid/net/Uri;)I
    .locals 9
    .param p0, "uri"    # Landroid/net/Uri;

    .prologue
    const/4 v5, 0x2

    const/4 v6, -0x1

    const/4 v8, 0x1

    .line 198
    sget-object v7, Lcom/samsung/android/provider/memo/MemoProvider;->sUriMatcher:Landroid/content/UriMatcher;

    invoke-virtual {v7, p0}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v0

    .line 199
    .local v0, "mid":I
    if-eq v0, v6, :cond_0

    .line 225
    .end local v0    # "mid":I
    :goto_0
    return v0

    .line 201
    .restart local v0    # "mid":I
    :cond_0
    const/16 v7, 0xc

    if-ne v7, v0, :cond_1

    .line 202
    sput-boolean v8, Lcom/samsung/android/provider/memo/MemoProvider;->isFromClipBoard:Z

    .line 204
    :cond_1
    invoke-virtual {p0}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v1

    .line 205
    .local v1, "paths":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    .line 206
    .local v2, "size":I
    if-eq v2, v8, :cond_2

    if-ne v2, v5, :cond_7

    .line 207
    :cond_2
    const/4 v7, 0x0

    invoke-interface {v1, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 208
    .local v3, "token1":Ljava/lang/String;
    add-int/lit8 v7, v2, -0x1

    invoke-interface {v1, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 210
    .local v4, "token2":Ljava/lang/String;
    :try_start_0
    invoke-static {v4}, Lcom/samsung/android/app/memo/util/UUIDHelper;->verify(Ljava/lang/String;)V

    .line 211
    if-eq v2, v8, :cond_3

    const-string v7, "memo"

    invoke-virtual {v7, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_4

    :cond_3
    move v0, v5

    .line 212
    goto :goto_0

    .line 213
    :cond_4
    const-string v5, "file"

    invoke-virtual {v5, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 214
    const/4 v0, 0x5

    goto :goto_0

    .line 215
    :cond_5
    const-string v5, "category"

    invoke-virtual {v5, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 216
    const/16 v0, 0x8

    goto :goto_0

    .line 217
    :cond_6
    const-string v5, "_sync_state"

    invoke-virtual {v5, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v5

    if-eqz v5, :cond_7

    .line 218
    const/16 v0, 0xe

    goto :goto_0

    .line 220
    :catch_0
    move-exception v5

    .end local v3    # "token1":Ljava/lang/String;
    .end local v4    # "token2":Ljava/lang/String;
    :cond_7
    move v0, v6

    .line 225
    goto :goto_0
.end method

.method private search(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Z)Landroid/database/Cursor;
    .locals 11
    .param p1, "token"    # Ljava/lang/String;
    .param p2, "projection"    # [Ljava/lang/String;
    .param p3, "selection_args"    # Ljava/lang/String;
    .param p4, "flag"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/database/SQLException;
        }
    .end annotation

    .prologue
    .line 501
    new-instance v6, Ljava/lang/StringBuilder;

    const/16 v7, 0x200

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 502
    .local v6, "sbSQL":Ljava/lang/StringBuilder;
    const-string v7, "SELECT "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 503
    if-eqz p2, :cond_1

    .line 504
    array-length v8, p2

    const/4 v7, 0x0

    :goto_0
    if-lt v7, v8, :cond_0

    .line 509
    :goto_1
    const-string v0, ""

    .line 511
    .local v0, "_token":Ljava/lang/String;
    if-eqz p4, :cond_2

    .line 512
    new-instance v7, Ljava/lang/StringBuilder;

    const/16 v8, 0x25

    invoke-static {v8}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const/16 v8, 0x25

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 513
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->length()I

    move-result v7

    add-int/lit8 v7, v7, -0x1

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->deleteCharAt(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " FROM "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "memo"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    .line 514
    const-string v8, " WHERE ("

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "isDeleted"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " IS "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    .line 515
    const-string v8, ") AND ("

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ") AND ("

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    .line 516
    const-string v8, "title"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " LIKE ? OR "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "strippedContent"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    .line 517
    const-string v8, " LIKE ?)"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " ORDER BY "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "lastModifiedAt"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    .line 518
    const-string v8, " DESC;"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 527
    :goto_2
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 529
    .local v5, "query":Ljava/lang/String;
    const/4 v1, 0x0

    .line 530
    .local v1, "c":Landroid/database/Cursor;
    iget-object v7, p0, Lcom/samsung/android/provider/memo/MemoProvider;->mMemoDBHelper:Landroid/database/sqlite/SQLiteOpenHelper;

    invoke-virtual {v7}, Landroid/database/sqlite/SQLiteOpenHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 532
    .local v2, "db":Landroid/database/sqlite/SQLiteDatabase;
    const/4 v7, 0x2

    :try_start_0
    new-array v7, v7, [Ljava/lang/String;

    const/4 v8, 0x0

    .line 533
    aput-object v0, v7, v8

    const/4 v8, 0x1

    aput-object v0, v7, v8

    .line 532
    invoke-virtual {v2, v5, v7}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 535
    invoke-virtual {p0}, Lcom/samsung/android/provider/memo/MemoProvider;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v7

    sget-object v8, Lcom/samsung/android/provider/memo/MemoContract;->BASE_URI_MEMO:Landroid/net/Uri;

    invoke-interface {v1, v7, v8}, Landroid/database/Cursor;->setNotificationUri(Landroid/content/ContentResolver;Landroid/net/Uri;)V
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 541
    const/4 v7, 0x0

    sput-boolean v7, Lcom/samsung/android/provider/memo/MemoProvider;->isFromClipBoard:Z

    .line 544
    return-object v1

    .line 504
    .end local v0    # "_token":Ljava/lang/String;
    .end local v1    # "c":Landroid/database/Cursor;
    .end local v2    # "db":Landroid/database/sqlite/SQLiteDatabase;
    .end local v5    # "query":Ljava/lang/String;
    :cond_0
    aget-object v4, p2, v7

    .line 505
    .local v4, "field":Ljava/lang/String;
    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const/16 v10, 0x2c

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 504
    add-int/lit8 v7, v7, 0x1

    goto/16 :goto_0

    .line 507
    .end local v4    # "field":Ljava/lang/String;
    :cond_1
    const-string v7, "* "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_1

    .line 520
    .restart local v0    # "_token":Ljava/lang/String;
    :cond_2
    sget-boolean v7, Lcom/samsung/android/provider/memo/MemoProvider;->isFromClipBoard:Z

    if-eqz v7, :cond_3

    move-object v0, p1

    .line 521
    :goto_3
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->length()I

    move-result v7

    add-int/lit8 v7, v7, -0x1

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->deleteCharAt(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " FROM "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "memo"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    .line 522
    const-string v8, " WHERE ("

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "isDeleted"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " IS "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    .line 523
    const-string v8, ") AND ("

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "title"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " LIKE ? OR "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    .line 524
    const-string v8, "strippedContent"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " LIKE ?)"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " ORDER BY "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    .line 525
    const-string v8, "lastModifiedAt"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " DESC;"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_2

    .line 520
    :cond_3
    new-instance v7, Ljava/lang/StringBuilder;

    const/16 v8, 0x25

    invoke-static {v8}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const/16 v8, 0x25

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_3

    .line 536
    .restart local v1    # "c":Landroid/database/Cursor;
    .restart local v2    # "db":Landroid/database/sqlite/SQLiteDatabase;
    .restart local v5    # "query":Ljava/lang/String;
    :catch_0
    move-exception v3

    .line 537
    .local v3, "e":Landroid/database/SQLException;
    :try_start_1
    sget-object v7, Lcom/samsung/android/provider/memo/MemoProvider;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "search()"

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8, v3}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 538
    throw v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 539
    .end local v3    # "e":Landroid/database/SQLException;
    :catchall_0
    move-exception v7

    .line 541
    const/4 v8, 0x0

    sput-boolean v8, Lcom/samsung/android/provider/memo/MemoProvider;->isFromClipBoard:Z

    .line 542
    throw v7
.end method

.method private searchRegex([Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Z)Landroid/database/Cursor;
    .locals 12
    .param p1, "token"    # [Ljava/lang/String;
    .param p2, "projection"    # [Ljava/lang/String;
    .param p3, "selection_args"    # Ljava/lang/String;
    .param p4, "flag"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/database/SQLException;
        }
    .end annotation

    .prologue
    .line 549
    new-instance v7, Ljava/lang/StringBuilder;

    const/16 v8, 0x400

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 550
    .local v7, "sbSQL":Ljava/lang/StringBuilder;
    new-instance v6, Ljava/lang/StringBuilder;

    const/16 v8, 0x400

    invoke-direct {v6, v8}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 552
    .local v6, "sbCON":Ljava/lang/StringBuilder;
    const-string v8, "SELECT "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 553
    if-eqz p2, :cond_1

    .line 554
    array-length v9, p2

    const/4 v8, 0x0

    :goto_0
    if-lt v8, v9, :cond_0

    .line 559
    :goto_1
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_2
    array-length v8, p1

    add-int/lit8 v8, v8, -0x1

    if-lt v4, v8, :cond_2

    .line 564
    const-string v8, " ( "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "strippedContent"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    .line 565
    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, " LIKE \'%"

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    array-length v10, p1

    add-int/lit8 v10, v10, -0x1

    aget-object v10, p1, v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "%\'"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " OR "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    .line 566
    const-string v9, "title"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, " LIKE \'%"

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    array-length v10, p1

    add-int/lit8 v10, v10, -0x1

    aget-object v10, p1, v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "%\'"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    .line 567
    const-string v9, " ) ) "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 569
    if-eqz p4, :cond_3

    .line 570
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->length()I

    move-result v8

    add-int/lit8 v8, v8, -0x1

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->deleteCharAt(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " FROM "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "memo"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    .line 571
    const-string v9, " WHERE ("

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "isDeleted"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " IS "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    .line 572
    const-string v9, ") AND ("

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ") AND ("

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    move-result-object v8

    .line 573
    const-string v9, " ORDER BY "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "lastModifiedAt"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " DESC;"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 583
    :goto_3
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 584
    .local v5, "query":Ljava/lang/String;
    const/4 v0, 0x0

    .line 585
    .local v0, "c":Landroid/database/Cursor;
    iget-object v8, p0, Lcom/samsung/android/provider/memo/MemoProvider;->mMemoDBHelper:Landroid/database/sqlite/SQLiteOpenHelper;

    invoke-virtual {v8}, Landroid/database/sqlite/SQLiteOpenHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 587
    .local v1, "db":Landroid/database/sqlite/SQLiteDatabase;
    const/4 v8, 0x0

    :try_start_0
    invoke-virtual {v1, v5, v8}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 588
    invoke-virtual {p0}, Lcom/samsung/android/provider/memo/MemoProvider;->getContext()Landroid/content/Context;

    move-result-object v8

    invoke-virtual {v8}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v8

    sget-object v9, Lcom/samsung/android/provider/memo/MemoContract;->BASE_URI_MEMO:Landroid/net/Uri;

    invoke-interface {v0, v8, v9}, Landroid/database/Cursor;->setNotificationUri(Landroid/content/ContentResolver;Landroid/net/Uri;)V
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 594
    const/4 v8, 0x0

    sput-boolean v8, Lcom/samsung/android/provider/memo/MemoProvider;->isFromClipBoard:Z

    .line 597
    return-object v0

    .line 554
    .end local v0    # "c":Landroid/database/Cursor;
    .end local v1    # "db":Landroid/database/sqlite/SQLiteDatabase;
    .end local v4    # "i":I
    .end local v5    # "query":Ljava/lang/String;
    :cond_0
    aget-object v3, p2, v8

    .line 555
    .local v3, "field":Ljava/lang/String;
    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const/16 v11, 0x2c

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 554
    add-int/lit8 v8, v8, 0x1

    goto/16 :goto_0

    .line 557
    .end local v3    # "field":Ljava/lang/String;
    :cond_1
    const-string v8, "* "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_1

    .line 560
    .restart local v4    # "i":I
    :cond_2
    const-string v8, " ( "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "strippedContent"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    .line 561
    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, " LIKE \'%"

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    aget-object v10, p1, v4

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "%\'"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "  OR "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "title"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    .line 562
    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, " LIKE \'%"

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    aget-object v10, p1, v4

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "%\'"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "  )  AND "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 559
    add-int/lit8 v4, v4, 0x2

    goto/16 :goto_2

    .line 578
    :cond_3
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->length()I

    move-result v8

    add-int/lit8 v8, v8, -0x1

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->deleteCharAt(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " FROM "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "memo"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    .line 579
    const-string v9, " WHERE ("

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "isDeleted"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " IS "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    .line 580
    const-string v9, ") AND ("

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " ORDER BY "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    .line 581
    const-string v9, "lastModifiedAt"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " DESC;"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_3

    .line 589
    .restart local v0    # "c":Landroid/database/Cursor;
    .restart local v1    # "db":Landroid/database/sqlite/SQLiteDatabase;
    .restart local v5    # "query":Ljava/lang/String;
    :catch_0
    move-exception v2

    .line 590
    .local v2, "e":Landroid/database/SQLException;
    :try_start_1
    sget-object v8, Lcom/samsung/android/provider/memo/MemoProvider;->TAG:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "search()"

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9, v2}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 591
    throw v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 592
    .end local v2    # "e":Landroid/database/SQLException;
    :catchall_0
    move-exception v8

    .line 594
    const/4 v9, 0x0

    sput-boolean v9, Lcom/samsung/android/provider/memo/MemoProvider;->isFromClipBoard:Z

    .line 595
    throw v8
.end method

.method private updateEntries(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)V
    .locals 11
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "oldMemoUUID"    # Ljava/lang/String;
    .param p3, "newMemoUUID"    # Ljava/lang/String;
    .param p4, "values"    # Landroid/content/ContentValues;

    .prologue
    .line 672
    invoke-static {p2}, Lcom/samsung/android/app/memo/util/FileHelper;->getPrivateFile(Ljava/lang/String;)Ljava/io/File;

    move-result-object v7

    .line 673
    .local v7, "source":Ljava/io/File;
    invoke-static {p3}, Lcom/samsung/android/app/memo/util/FileHelper;->getPrivateFile(Ljava/lang/String;)Ljava/io/File;

    move-result-object v2

    .line 674
    .local v2, "dest":Ljava/io/File;
    invoke-static {v7, v2}, Lcom/samsung/android/app/memo/util/FileHelper;->copyFileItem(Ljava/io/File;Ljava/io/File;)I

    .line 676
    const-string v9, "content"

    invoke-virtual {p4, v9}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 677
    .local v0, "content":Ljava/lang/String;
    const-string v9, "vrfileuuid"

    invoke-virtual {p4, v9}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 680
    .local v8, "vrFileUUID":Ljava/lang/String;
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/provider/memo/MemoProvider;->getFileUUIDs(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)Ljava/util/List;

    move-result-object v3

    .line 681
    .local v3, "fileUUIDsList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v9

    if-nez v9, :cond_0

    .line 682
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_0
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-nez v10, :cond_2

    .line 687
    const-string v9, "content"

    invoke-virtual {p4, v9, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 689
    invoke-static {p3}, Lcom/samsung/android/app/memo/util/FileHelper;->getPrivateFilePath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/samsung/android/app/memo/util/Utils;->getRelativePath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 690
    .local v1, "data":Ljava/lang/String;
    const-string v9, "_data"

    invoke-virtual {p4, v9, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 693
    .end local v1    # "data":Ljava/lang/String;
    :cond_0
    if-eqz v8, :cond_1

    const-string v9, ""

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_1

    .line 694
    invoke-static {}, Lcom/samsung/android/app/memo/util/UUIDHelper;->newUUID()Ljava/lang/String;

    move-result-object v5

    .line 695
    .local v5, "newVrFileUUID":Ljava/lang/String;
    invoke-direct {p0, p1, v8, v5, p3}, Lcom/samsung/android/provider/memo/MemoProvider;->insertFileEntry(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 696
    move-object v8, v5

    .line 697
    const-string v9, "vrfileUUID"

    invoke-virtual {p4, v9, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 699
    .end local v5    # "newVrFileUUID":Ljava/lang/String;
    :cond_1
    return-void

    .line 682
    :cond_2
    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    .line 683
    .local v6, "oldFileUUID":Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/app/memo/util/UUIDHelper;->newUUID()Ljava/lang/String;

    move-result-object v4

    .line 684
    .local v4, "newFileUUID":Ljava/lang/String;
    invoke-virtual {v0, v6, v4}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 685
    invoke-direct {p0, p1, v6, v4, p3}, Lcom/samsung/android/provider/memo/MemoProvider;->insertFileEntry(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 20
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "selection"    # Ljava/lang/String;
    .param p3, "selectionArgs"    # [Ljava/lang/String;

    .prologue
    .line 1039
    const/4 v4, 0x0

    .line 1040
    .local v4, "count":I
    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v9

    .line 1041
    .local v9, "lastPathSeg":Ljava/lang/String;
    const/4 v14, 0x0

    .line 1042
    .local v14, "uuids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-direct/range {p0 .. p1}, Lcom/samsung/android/provider/memo/MemoProvider;->isCalledByPDM(Landroid/net/Uri;)Z

    move-result v8

    .line 1044
    .local v8, "isCalledByPDM":Z
    invoke-static/range {p1 .. p1}, Lcom/samsung/android/provider/memo/MemoProvider;->match(Landroid/net/Uri;)I

    move-result v10

    .line 1045
    .local v10, "match":I
    const/16 v16, -0x1

    move/from16 v0, v16

    if-ne v10, v0, :cond_0

    .line 1046
    new-instance v16, Ljava/lang/IllegalArgumentException;

    new-instance v17, Ljava/lang/StringBuilder;

    const-string v18, "Unknown URI: "

    invoke-direct/range {v17 .. v18}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v17

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-direct/range {v16 .. v17}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v16

    .line 1047
    :cond_0
    move-object/from16 v0, p0

    invoke-direct {v0, v10}, Lcom/samsung/android/provider/memo/MemoProvider;->getTableName(I)Ljava/lang/String;

    move-result-object v13

    .line 1049
    .local v13, "tblName":Ljava/lang/String;
    invoke-direct/range {p0 .. p1}, Lcom/samsung/android/provider/memo/MemoProvider;->isUndoable(Landroid/net/Uri;)Z

    move-result v16

    if-eqz v16, :cond_1

    .line 1050
    new-instance v15, Landroid/content/ContentValues;

    invoke-direct {v15}, Landroid/content/ContentValues;-><init>()V

    .line 1051
    .local v15, "values":Landroid/content/ContentValues;
    const-string v16, "isDeleted"

    const/16 v17, 0x1

    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v17

    invoke-virtual/range {v15 .. v17}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1052
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    invoke-virtual {v0, v1, v15, v2, v3}, Lcom/samsung/android/provider/memo/MemoProvider;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v4

    move v5, v4

    .line 1125
    .end local v4    # "count":I
    .end local v15    # "values":Landroid/content/ContentValues;
    .local v5, "count":I
    :goto_0
    return v5

    .line 1055
    .end local v5    # "count":I
    .restart local v4    # "count":I
    :cond_1
    invoke-direct/range {p0 .. p1}, Lcom/samsung/android/provider/memo/MemoProvider;->isUndo(Landroid/net/Uri;)Z

    move-result v16

    if-eqz v16, :cond_2

    .line 1056
    new-instance v15, Landroid/content/ContentValues;

    invoke-direct {v15}, Landroid/content/ContentValues;-><init>()V

    .line 1057
    .restart local v15    # "values":Landroid/content/ContentValues;
    const-string v16, "isDeleted"

    const/16 v17, 0x0

    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v17

    invoke-virtual/range {v15 .. v17}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1058
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    invoke-virtual {v0, v1, v15, v2, v3}, Lcom/samsung/android/provider/memo/MemoProvider;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v4

    move v5, v4

    .line 1060
    .end local v4    # "count":I
    .restart local v5    # "count":I
    goto :goto_0

    .line 1061
    .end local v5    # "count":I
    .end local v15    # "values":Landroid/content/ContentValues;
    .restart local v4    # "count":I
    :cond_2
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/provider/memo/MemoProvider;->isSAregistered()Z

    move-result v16

    if-eqz v16, :cond_4

    if-nez v8, :cond_4

    const/16 v16, 0xd

    move/from16 v0, v16

    if-ge v10, v0, :cond_4

    .line 1062
    const/16 v16, 0x4

    move/from16 v0, v16

    if-lt v10, v0, :cond_3

    const/16 v16, 0x6

    move/from16 v0, v16

    if-le v10, v0, :cond_4

    :cond_3
    invoke-direct/range {p0 .. p1}, Lcom/samsung/android/provider/memo/MemoProvider;->isForce(Landroid/net/Uri;)Z

    move-result v16

    if-nez v16, :cond_4

    .line 1067
    new-instance v15, Landroid/content/ContentValues;

    invoke-direct {v15}, Landroid/content/ContentValues;-><init>()V

    .line 1068
    .restart local v15    # "values":Landroid/content/ContentValues;
    const-string v16, "isDeleted"

    const/16 v17, 0x1

    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v17

    invoke-virtual/range {v15 .. v17}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1069
    const-string v16, "sync1"

    invoke-static {}, Lcom/samsung/android/app/memo/util/Utils;->getCorrectedTime()J

    move-result-wide v18

    invoke-static/range {v18 .. v19}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v17

    invoke-virtual/range {v15 .. v17}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1070
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    invoke-virtual {v0, v1, v15, v2, v3}, Lcom/samsung/android/provider/memo/MemoProvider;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v4

    .line 1072
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    invoke-direct {v0, v13, v1, v2}, Lcom/samsung/android/provider/memo/MemoProvider;->getUUIDs(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)Ljava/util/List;

    move-result-object v14

    .line 1073
    invoke-static {v14}, Lcom/samsung/android/app/memo/util/FileHelper;->deletePrivateFiles(Ljava/util/List;)J

    .line 1074
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/provider/memo/MemoProvider;->getContext()Landroid/content/Context;

    move-result-object v16

    invoke-static/range {v16 .. v16}, Lcom/samsung/android/app/memo/util/Utils;->sendUpdateAllWidgetBroadcastEvent(Landroid/content/Context;)V

    move v5, v4

    .line 1075
    .end local v4    # "count":I
    .restart local v5    # "count":I
    goto/16 :goto_0

    .line 1079
    .end local v5    # "count":I
    .end local v15    # "values":Landroid/content/ContentValues;
    .restart local v4    # "count":I
    :cond_4
    :try_start_0
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    invoke-direct {v0, v1, v2, v9, v10}, Lcom/samsung/android/provider/memo/MemoProvider;->getSelectionParams(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object p2

    .line 1080
    const/16 v16, 0xd

    move/from16 v0, v16

    if-ge v10, v0, :cond_5

    .line 1081
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    invoke-direct {v0, v13, v1, v2}, Lcom/samsung/android/provider/memo/MemoProvider;->getUUIDs(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)Ljava/util/List;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v14

    .line 1087
    :cond_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/provider/memo/MemoProvider;->mMemoDBHelper:Landroid/database/sqlite/SQLiteOpenHelper;

    move-object/from16 v17, v0

    monitor-enter v17

    .line 1088
    :try_start_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/provider/memo/MemoProvider;->mMemoDBHelper:Landroid/database/sqlite/SQLiteOpenHelper;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Landroid/database/sqlite/SQLiteOpenHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v6

    .line 1089
    .local v6, "db":Landroid/database/sqlite/SQLiteDatabase;
    invoke-virtual {v6}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 1091
    :try_start_2
    move-object/from16 v0, p2

    move-object/from16 v1, p3

    invoke-virtual {v6, v13, v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v4

    .line 1093
    const/16 v16, 0xd

    move/from16 v0, v16

    if-ge v10, v0, :cond_7

    .line 1095
    const-string v16, "category"

    move-object/from16 v0, v16

    invoke-virtual {v0, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    if-nez v16, :cond_6

    .line 1096
    invoke-static {v14}, Lcom/samsung/android/app/memo/util/FileHelper;->deletePrivateFiles(Ljava/util/List;)J

    .line 1100
    :cond_6
    const-string v16, "memo"

    move-object/from16 v0, v16

    invoke-virtual {v0, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    if-eqz v16, :cond_7

    .line 1101
    move-object/from16 v0, p0

    invoke-direct {v0, v6, v14}, Lcom/samsung/android/provider/memo/MemoProvider;->deleteFilesForMemoUUIDs(Landroid/database/sqlite/SQLiteDatabase;Ljava/util/List;)I

    .line 1105
    :cond_7
    invoke-virtual {v6}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_2
    .catch Landroid/database/SQLException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1110
    :try_start_3
    invoke-virtual {v6}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 1087
    monitor-exit v17
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 1114
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/provider/memo/MemoProvider;->getContext()Landroid/content/Context;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v11

    .line 1115
    .local v11, "resolver":Landroid/content/ContentResolver;
    const/16 v16, 0x3

    move/from16 v0, v16

    if-eq v10, v0, :cond_8

    const/16 v16, 0x6

    move/from16 v0, v16

    if-eq v10, v0, :cond_8

    const/16 v16, 0x9

    move/from16 v0, v16

    if-eq v10, v0, :cond_8

    const/16 v16, 0xf

    move/from16 v0, v16

    if-eq v10, v0, :cond_8

    .line 1117
    new-instance v16, Ljava/lang/StringBuilder;

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v17 .. v17}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v17

    invoke-direct/range {v16 .. v17}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v17, "://"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    .line 1118
    const/16 v17, 0x2f

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v16

    const/16 v18, 0x0

    move-object/from16 v0, v16

    move/from16 v1, v18

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Ljava/lang/String;

    move-object/from16 v0, v17

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    .line 1117
    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v16 .. v16}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v16

    .line 1118
    const/16 v17, 0x0

    const/16 v18, 0x0

    .line 1116
    move-object/from16 v0, v16

    move-object/from16 v1, v17

    move/from16 v2, v18

    invoke-virtual {v11, v0, v1, v2}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;Z)V

    .line 1120
    :cond_8
    if-eqz v8, :cond_9

    .line 1121
    const/16 v16, 0x0

    const/16 v17, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    move/from16 v2, v17

    invoke-virtual {v11, v0, v1, v2}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;Z)V

    .line 1124
    :goto_1
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/provider/memo/MemoProvider;->getContext()Landroid/content/Context;

    move-result-object v16

    invoke-static/range {v16 .. v16}, Lcom/samsung/android/app/memo/util/Utils;->sendUpdateAllWidgetBroadcastEvent(Landroid/content/Context;)V

    move v5, v4

    .line 1125
    .end local v4    # "count":I
    .restart local v5    # "count":I
    goto/16 :goto_0

    .line 1082
    .end local v5    # "count":I
    .end local v6    # "db":Landroid/database/sqlite/SQLiteDatabase;
    .end local v11    # "resolver":Landroid/content/ContentResolver;
    .restart local v4    # "count":I
    :catch_0
    move-exception v12

    .line 1083
    .local v12, "t":Ljava/lang/Throwable;
    sget-object v16, Lcom/samsung/android/provider/memo/MemoProvider;->TAG:Ljava/lang/String;

    new-instance v17, Ljava/lang/StringBuilder;

    const-string v18, "delete()"

    invoke-direct/range {v17 .. v18}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v17

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-static {v0, v1, v12}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1084
    new-instance v16, Landroid/database/SQLException;

    const-string v17, "error in preprocessing"

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-direct {v0, v1, v12}, Landroid/database/SQLException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v16

    .line 1106
    .end local v12    # "t":Ljava/lang/Throwable;
    .restart local v6    # "db":Landroid/database/sqlite/SQLiteDatabase;
    :catch_1
    move-exception v7

    .line 1107
    .local v7, "e":Landroid/database/SQLException;
    :try_start_4
    sget-object v16, Lcom/samsung/android/provider/memo/MemoProvider;->TAG:Ljava/lang/String;

    new-instance v18, Ljava/lang/StringBuilder;

    const-string v19, "delete()"

    invoke-direct/range {v18 .. v19}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v18

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v16

    move-object/from16 v1, v18

    invoke-static {v0, v1, v7}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1108
    throw v7
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 1109
    .end local v7    # "e":Landroid/database/SQLException;
    :catchall_0
    move-exception v16

    .line 1110
    :try_start_5
    invoke-virtual {v6}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 1111
    throw v16

    .line 1087
    .end local v6    # "db":Landroid/database/sqlite/SQLiteDatabase;
    :catchall_1
    move-exception v16

    monitor-exit v17
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    throw v16

    .line 1123
    .restart local v6    # "db":Landroid/database/sqlite/SQLiteDatabase;
    .restart local v11    # "resolver":Landroid/content/ContentResolver;
    :cond_9
    const/16 v16, 0x0

    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/provider/memo/MemoProvider;->isSAregistered()Z

    move-result v17

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    move/from16 v2, v17

    invoke-virtual {v11, v0, v1, v2}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;Z)V

    goto :goto_1
.end method

.method public getType(Landroid/net/Uri;)Ljava/lang/String;
    .locals 5
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 314
    invoke-static {p1}, Lcom/samsung/android/provider/memo/MemoProvider;->match(Landroid/net/Uri;)I

    move-result v0

    .line 315
    .local v0, "match":I
    packed-switch v0, :pswitch_data_0

    .line 341
    :pswitch_0
    const/4 v1, 0x0

    .line 343
    .local v1, "type":Ljava/lang/String;
    :goto_0
    sget-object v2, Lcom/samsung/android/provider/memo/MemoProvider;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "getType() "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 344
    return-object v1

    .line 318
    .end local v1    # "type":Ljava/lang/String;
    :pswitch_1
    invoke-direct {p0, p1, v0}, Lcom/samsung/android/provider/memo/MemoProvider;->getMimeType(Landroid/net/Uri;I)Ljava/lang/String;

    move-result-object v1

    .line 320
    .restart local v1    # "type":Ljava/lang/String;
    goto :goto_0

    .line 322
    .end local v1    # "type":Ljava/lang/String;
    :pswitch_2
    const-string v1, "vnd.android.cursor.dir/vnd.samsung.memo.file"

    .line 323
    .restart local v1    # "type":Ljava/lang/String;
    goto :goto_0

    .line 326
    .end local v1    # "type":Ljava/lang/String;
    :pswitch_3
    const-string v1, "vnd.android.cursor.item/vnd.samsung.memo"

    .line 327
    .restart local v1    # "type":Ljava/lang/String;
    goto :goto_0

    .line 331
    .end local v1    # "type":Ljava/lang/String;
    :pswitch_4
    const-string v1, "vnd.android.cursor.dir/vnd.samsung.memo"

    .line 332
    .restart local v1    # "type":Ljava/lang/String;
    goto :goto_0

    .line 335
    .end local v1    # "type":Ljava/lang/String;
    :pswitch_5
    const-string v1, "vnd.android.cursor.item/vnd.samsung.memo.category"

    .line 336
    .restart local v1    # "type":Ljava/lang/String;
    goto :goto_0

    .line 338
    .end local v1    # "type":Ljava/lang/String;
    :pswitch_6
    const-string v1, "vnd.android.cursor.dir/vnd.samsung.memo.category"

    .line 339
    .restart local v1    # "type":Ljava/lang/String;
    goto :goto_0

    .line 315
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_4
        :pswitch_3
        :pswitch_3
        :pswitch_4
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_5
        :pswitch_5
        :pswitch_6
        :pswitch_0
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method

.method public insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    .locals 34
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "values"    # Landroid/content/ContentValues;

    .prologue
    .line 703
    const/4 v11, 0x0

    .line 704
    .local v11, "insertedUri":Landroid/net/Uri;
    const/4 v12, 0x0

    .line 705
    .local v12, "insertedUri2":Landroid/net/Uri;
    invoke-direct/range {p0 .. p1}, Lcom/samsung/android/provider/memo/MemoProvider;->isCalledByPDM(Landroid/net/Uri;)Z

    move-result v13

    .line 706
    .local v13, "isCalledByPDM":Z
    const/4 v5, 0x0

    .line 707
    .local v5, "createThumbnail":Z
    const-wide/16 v18, -0x1

    .line 708
    .local v18, "rawId":J
    const-string v30, "UUID"

    move-object/from16 v0, p2

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v29

    .line 709
    .local v29, "uuid":Ljava/lang/String;
    if-eqz p2, :cond_0

    const-string v30, "title"

    move-object/from16 v0, p2

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v30

    if-eqz v30, :cond_0

    .line 710
    const-string v30, "title"

    move-object/from16 v0, p2

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v20

    .line 711
    .local v20, "s":Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/app/memo/util/Utils;->getAppContext()Landroid/content/Context;

    move-result-object v30

    invoke-virtual/range {v30 .. v30}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v30

    .line 712
    const v31, 0x7f0a0009

    invoke-virtual/range {v30 .. v31}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v28

    .line 713
    .local v28, "title_limit":I
    if-eqz v20, :cond_0

    invoke-virtual/range {v20 .. v20}, Ljava/lang/String;->length()I

    move-result v30

    move/from16 v0, v30

    move/from16 v1, v28

    if-le v0, v1, :cond_0

    .line 714
    const/16 v30, 0x0

    move-object/from16 v0, v20

    move/from16 v1, v30

    move/from16 v2, v28

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v20

    .line 715
    const-string v30, "title"

    move-object/from16 v0, p2

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->remove(Ljava/lang/String;)V

    .line 716
    const-string v30, "title"

    move-object/from16 v0, p2

    move-object/from16 v1, v30

    move-object/from16 v2, v20

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 719
    .end local v20    # "s":Ljava/lang/String;
    .end local v28    # "title_limit":I
    :cond_0
    invoke-static/range {p1 .. p1}, Lcom/samsung/android/provider/memo/MemoProvider;->match(Landroid/net/Uri;)I

    move-result v15

    .line 720
    .local v15, "match":I
    if-nez v13, :cond_1

    const/16 v30, 0xd

    move/from16 v0, v30

    if-lt v15, v0, :cond_1

    const/16 v30, 0xf

    move/from16 v0, v30

    if-gt v15, v0, :cond_1

    .line 721
    new-instance v30, Ljava/lang/IllegalArgumentException;

    const-string v31, "_sync_state table is accessed by Memo app."

    invoke-direct/range {v30 .. v31}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v30

    .line 722
    :cond_1
    if-nez v13, :cond_2

    const/16 v30, 0xd

    move/from16 v0, v30

    if-ge v15, v0, :cond_2

    .line 723
    const-string v30, "isDirty"

    const/16 v31, 0x1

    invoke-static/range {v31 .. v31}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v31

    move-object/from16 v0, p2

    move-object/from16 v1, v30

    move-object/from16 v2, v31

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 724
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/provider/memo/MemoProvider;->getContext()Landroid/content/Context;

    move-result-object v30

    invoke-static/range {v30 .. v30}, Lcom/samsung/android/app/memo/util/AccountHelper;->getRegisteredSamsungAccount(Landroid/content/Context;)Landroid/accounts/Account;

    move-result-object v4

    .line 725
    .local v4, "account":Landroid/accounts/Account;
    if-eqz v4, :cond_8

    .line 726
    const-string v30, "accountType"

    const-string v31, "com.osp.app.signin"

    move-object/from16 v0, p2

    move-object/from16 v1, v30

    move-object/from16 v2, v31

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 727
    const-string v30, "accountName"

    iget-object v0, v4, Landroid/accounts/Account;->name:Ljava/lang/String;

    move-object/from16 v31, v0

    move-object/from16 v0, p2

    move-object/from16 v1, v30

    move-object/from16 v2, v31

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 732
    :goto_0
    const-string v30, "v_skipSync1Set"

    move-object/from16 v0, p2

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v30

    if-nez v30, :cond_9

    .line 733
    const-string v30, "sync1"

    invoke-static {}, Lcom/samsung/android/app/memo/util/Utils;->getCorrectedTime()J

    move-result-wide v32

    invoke-static/range {v32 .. v33}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v31

    move-object/from16 v0, p2

    move-object/from16 v1, v30

    move-object/from16 v2, v31

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 738
    .end local v4    # "account":Landroid/accounts/Account;
    :cond_2
    :goto_1
    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lcom/samsung/android/provider/memo/MemoProvider;->getTableName(I)Ljava/lang/String;

    move-result-object v26

    .line 740
    .local v26, "tblName":Ljava/lang/String;
    sparse-switch v15, :sswitch_data_0

    .line 790
    :goto_2
    const/16 v30, 0xd

    move/from16 v0, v30

    if-ge v15, v0, :cond_3

    .line 791
    :try_start_0
    invoke-static/range {v29 .. v29}, Lcom/samsung/android/app/memo/util/UUIDHelper;->verify(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 797
    :cond_3
    const/4 v14, 0x0

    .line 798
    .local v14, "isUUIDConflict":Z
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/provider/memo/MemoProvider;->mMemoDBHelper:Landroid/database/sqlite/SQLiteOpenHelper;

    move-object/from16 v31, v0

    monitor-enter v31

    .line 799
    :try_start_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/provider/memo/MemoProvider;->mMemoDBHelper:Landroid/database/sqlite/SQLiteOpenHelper;

    move-object/from16 v30, v0

    invoke-virtual/range {v30 .. v30}, Landroid/database/sqlite/SQLiteOpenHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v8

    .line 800
    .local v8, "db":Landroid/database/sqlite/SQLiteDatabase;
    invoke-virtual {v8}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 802
    const/16 v16, 0x0

    .line 803
    .local v16, "newUUID":Ljava/lang/String;
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/provider/memo/MemoProvider;->isSAregistered()Z

    move-result v30

    if-eqz v30, :cond_4

    if-eqz v13, :cond_4

    .line 804
    const/16 v30, 0x9

    move/from16 v0, v30

    if-ne v15, v0, :cond_f

    .line 805
    move-object/from16 v0, p0

    move-object/from16 v1, v26

    move-object/from16 v2, v29

    invoke-direct {v0, v8, v1, v2}, Lcom/samsung/android/provider/memo/MemoProvider;->isUUIDExist(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v30

    if-eqz v30, :cond_4

    .line 806
    invoke-static {}, Lcom/samsung/android/app/memo/util/UUIDHelper;->newUUID()Ljava/lang/String;

    move-result-object v16

    .line 807
    const-string v30, "UUID"

    move-object/from16 v0, p2

    move-object/from16 v1, v30

    move-object/from16 v2, v16

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 808
    const-string v30, "isNeedUpsync"

    const/16 v32, 0x1

    invoke-static/range {v32 .. v32}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v32

    move-object/from16 v0, p2

    move-object/from16 v1, v30

    move-object/from16 v2, v32

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 809
    const/4 v14, 0x1

    .line 822
    :cond_4
    :goto_3
    const/16 v30, 0x0

    const/16 v32, 0x1

    :try_start_2
    move-object/from16 v0, v26

    move-object/from16 v1, v30

    move-object/from16 v2, p2

    move/from16 v3, v32

    invoke-virtual {v8, v0, v1, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->insertWithOnConflict(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;I)J

    move-result-wide v18

    .line 823
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/provider/memo/MemoProvider;->isSAregistered()Z

    move-result v30

    if-eqz v30, :cond_5

    if-eqz v13, :cond_5

    if-eqz v14, :cond_5

    const/16 v30, 0x3

    move/from16 v0, v30

    if-ne v15, v0, :cond_5

    .line 824
    new-instance v30, Ljava/lang/StringBuilder;

    const-string v32, "UUID = \'"

    move-object/from16 v0, v30

    move-object/from16 v1, v32

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v30

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    const-string v32, "\'"

    move-object/from16 v0, v30

    move-object/from16 v1, v32

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    invoke-virtual/range {v30 .. v30}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v30

    const/16 v32, 0x0

    move-object/from16 v0, v26

    move-object/from16 v1, p2

    move-object/from16 v2, v30

    move-object/from16 v3, v32

    invoke-virtual {v8, v0, v1, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 825
    invoke-static/range {v16 .. v16}, Lcom/samsung/android/app/memo/util/FileHelper;->getPrivateFile(Ljava/lang/String;)Ljava/io/File;

    move-result-object v30

    invoke-virtual/range {v30 .. v30}, Ljava/io/File;->exists()Z

    move-result v30

    if-nez v30, :cond_5

    .line 826
    invoke-static {}, Lcom/samsung/android/app/memo/util/Utils;->getAppContext()Landroid/content/Context;

    move-result-object v30

    move-object/from16 v0, v30

    move-object/from16 v1, v16

    invoke-static {v0, v1}, Lcom/samsung/android/app/memo/util/ImageUtil;->createThumbnailOnDiscreteThread(Landroid/content/Context;Ljava/lang/String;)V

    .line 828
    :cond_5
    invoke-virtual {v8}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_2
    .catch Landroid/database/SQLException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 833
    :try_start_3
    invoke-virtual {v8}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 798
    monitor-exit v31
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 837
    const-wide/16 v30, -0x1

    cmp-long v30, v18, v30

    if-lez v30, :cond_11

    .line 838
    move-wide/from16 v0, v18

    invoke-static {v11, v0, v1}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v11

    .line 839
    sget-object v30, Lcom/samsung/android/provider/memo/MemoContract;->BASE_URI_FILE:Landroid/net/Uri;

    move-object/from16 v0, v30

    move-object/from16 v1, v29

    invoke-static {v0, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v12

    .line 841
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/provider/memo/MemoProvider;->getContext()Landroid/content/Context;

    move-result-object v30

    invoke-virtual/range {v30 .. v30}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v17

    .line 842
    .local v17, "resolver":Landroid/content/ContentResolver;
    if-eqz v13, :cond_10

    .line 843
    const/16 v30, 0x0

    move-object/from16 v0, v17

    move-object/from16 v1, p1

    move-object/from16 v2, v30

    invoke-virtual {v0, v1, v2, v14}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;Z)V

    .line 846
    :goto_4
    const/16 v30, 0x0

    const/16 v31, 0x0

    move-object/from16 v0, v17

    move-object/from16 v1, v30

    move/from16 v2, v31

    invoke-virtual {v0, v11, v1, v2}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;Z)V

    .line 847
    const/16 v30, 0x0

    const/16 v31, 0x0

    move-object/from16 v0, v17

    move-object/from16 v1, v30

    move/from16 v2, v31

    invoke-virtual {v0, v12, v1, v2}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;Z)V

    .line 851
    if-eqz v5, :cond_6

    if-eqz v29, :cond_6

    invoke-static/range {v29 .. v29}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v30

    if-nez v30, :cond_6

    .line 852
    invoke-static {}, Lcom/samsung/android/app/memo/util/Utils;->getAppContext()Landroid/content/Context;

    move-result-object v30

    move-object/from16 v0, v30

    move-object/from16 v1, v29

    invoke-static {v0, v1}, Lcom/samsung/android/app/memo/util/ImageUtil;->createThumbnailOnDiscreteThread(Landroid/content/Context;Ljava/lang/String;)V

    .line 854
    :cond_6
    if-nez v13, :cond_7

    const/16 v30, 0x3

    move/from16 v0, v30

    if-ne v15, v0, :cond_7

    .line 855
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/provider/memo/MemoProvider;->mCFL:Lcom/samsung/android/app/memo/util/CFLHelper;

    move-object/from16 v30, v0

    move-object/from16 v0, v30

    invoke-virtual {v0, v11}, Lcom/samsung/android/app/memo/util/CFLHelper;->created(Landroid/net/Uri;)V

    .line 858
    :cond_7
    return-object v11

    .line 729
    .end local v8    # "db":Landroid/database/sqlite/SQLiteDatabase;
    .end local v14    # "isUUIDConflict":Z
    .end local v16    # "newUUID":Ljava/lang/String;
    .end local v17    # "resolver":Landroid/content/ContentResolver;
    .end local v26    # "tblName":Ljava/lang/String;
    .restart local v4    # "account":Landroid/accounts/Account;
    :cond_8
    const-string v30, "accountType"

    const-string v31, "local"

    move-object/from16 v0, p2

    move-object/from16 v1, v30

    move-object/from16 v2, v31

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 730
    const-string v30, "accountName"

    const-string v31, ""

    move-object/from16 v0, p2

    move-object/from16 v1, v30

    move-object/from16 v2, v31

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 735
    :cond_9
    const-string v30, "v_skipSync1Set"

    move-object/from16 v0, p2

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->remove(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 742
    .end local v4    # "account":Landroid/accounts/Account;
    .restart local v26    # "tblName":Ljava/lang/String;
    :sswitch_0
    :try_start_4
    invoke-static/range {v29 .. v29}, Lcom/samsung/android/app/memo/util/FileHelper;->getPrivateFilePath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 743
    .local v6, "data":Ljava/lang/String;
    const-string v30, "_srcUri"

    move-object/from16 v0, p2

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v21

    .line 744
    .local v21, "srcUri":Ljava/lang/String;
    const-wide/16 v22, 0x0

    .line 745
    .local v22, "size":J
    const-string v30, "_size"

    move-object/from16 v0, p2

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v30

    if-eqz v30, :cond_a

    .line 746
    const-string v30, "_size"

    move-object/from16 v0, p2

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v30

    invoke-virtual/range {v30 .. v30}, Ljava/lang/Long;->longValue()J

    move-result-wide v22

    .line 747
    :cond_a
    sget-object v30, Lcom/samsung/android/provider/memo/MemoProvider;->TAG:Ljava/lang/String;

    new-instance v31, Ljava/lang/StringBuilder;

    const-string v32, "_srcUri="

    invoke-direct/range {v31 .. v32}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v31

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    const-string v32, " _data="

    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    move-object/from16 v0, v31

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    invoke-virtual/range {v31 .. v31}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v31

    invoke-static/range {v30 .. v31}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 748
    if-eqz v21, :cond_c

    .line 749
    invoke-static/range {v21 .. v21}, Lcom/samsung/android/app/memo/util/Utils;->getRelativePath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v24

    .line 750
    .local v24, "srcUriRel":Ljava/lang/String;
    invoke-static {v6}, Lcom/samsung/android/app/memo/util/Utils;->getRelativePath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 753
    .local v7, "dataRel":Ljava/lang/String;
    move-object/from16 v0, v24

    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v30

    if-nez v30, :cond_b

    .line 754
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/provider/memo/MemoProvider;->getContext()Landroid/content/Context;

    move-result-object v30

    move-object/from16 v0, v30

    move-object/from16 v1, v21

    invoke-static {v0, v1}, Lcom/samsung/android/app/memo/util/Utils;->getAbsolutePath(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v30

    move-object/from16 v0, v30

    invoke-static {v0, v6}, Lcom/samsung/android/app/memo/util/FileHelper;->copy(Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v22

    .line 755
    sget-object v30, Lcom/samsung/android/provider/memo/MemoProvider;->TAG:Ljava/lang/String;

    const-string v31, "Copying from _srcUri to _data..."

    invoke-static/range {v30 .. v31}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 762
    .end local v7    # "dataRel":Ljava/lang/String;
    .end local v24    # "srcUriRel":Ljava/lang/String;
    :cond_b
    :goto_5
    const-string v30, "_srcUri"

    move-object/from16 v0, p2

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->remove(Ljava/lang/String;)V

    .line 763
    const-string v30, "_data"

    invoke-static {v6}, Lcom/samsung/android/app/memo/util/Utils;->getRelativePath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v31

    move-object/from16 v0, p2

    move-object/from16 v1, v30

    move-object/from16 v2, v31

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 764
    const-string v30, "_size"

    invoke-static/range {v22 .. v23}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v31

    move-object/from16 v0, p2

    move-object/from16 v1, v30

    move-object/from16 v2, v31

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 765
    sget-object v11, Lcom/samsung/android/provider/memo/MemoContract;->BASE_URI_FILE:Landroid/net/Uri;

    .line 766
    goto/16 :goto_2

    .line 758
    :cond_c
    invoke-static/range {v29 .. v29}, Lcom/samsung/android/app/memo/util/FileHelper;->createPrivateFile(Ljava/lang/String;)Z

    .line 759
    sget-object v30, Lcom/samsung/android/provider/memo/MemoProvider;->TAG:Ljava/lang/String;

    const-string v31, "_srcUri is null"

    invoke-static/range {v30 .. v31}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_0

    goto :goto_5

    .line 792
    .end local v6    # "data":Ljava/lang/String;
    .end local v21    # "srcUri":Ljava/lang/String;
    .end local v22    # "size":J
    :catch_0
    move-exception v25

    .line 793
    .local v25, "t":Ljava/lang/Throwable;
    sget-object v30, Lcom/samsung/android/provider/memo/MemoProvider;->TAG:Ljava/lang/String;

    new-instance v31, Ljava/lang/StringBuilder;

    const-string v32, "insert()"

    invoke-direct/range {v31 .. v32}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v31

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v31

    invoke-virtual/range {v31 .. v31}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v31

    move-object/from16 v0, v30

    move-object/from16 v1, v31

    move-object/from16 v2, v25

    invoke-static {v0, v1, v2}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 794
    new-instance v30, Landroid/database/SQLException;

    const-string v31, "error in preprocessing"

    move-object/from16 v0, v30

    move-object/from16 v1, v31

    move-object/from16 v2, v25

    invoke-direct {v0, v1, v2}, Landroid/database/SQLException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v30

    .line 768
    .end local v25    # "t":Ljava/lang/Throwable;
    :sswitch_1
    :try_start_5
    sget-object v11, Lcom/samsung/android/provider/memo/MemoContract;->BASE_URI_CATEGORY:Landroid/net/Uri;

    .line 769
    goto/16 :goto_2

    .line 771
    :sswitch_2
    const-string v30, "content"

    move-object/from16 v0, p2

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v30

    if-eqz v30, :cond_d

    .line 772
    const-string v30, "strippedContent"

    .line 773
    const-string v31, "content"

    move-object/from16 v0, p2

    move-object/from16 v1, v31

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v31

    invoke-static/range {v31 .. v31}, Lcom/samsung/android/app/memo/util/HtmlUtil;->stripHtmlToSingleLine(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v31

    invoke-interface/range {v31 .. v31}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v31

    invoke-virtual/range {v31 .. v31}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v31

    .line 772
    move-object/from16 v0, p2

    move-object/from16 v1, v30

    move-object/from16 v2, v31

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 774
    :cond_d
    if-eqz v13, :cond_e

    const-string v30, "title"

    move-object/from16 v0, p2

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v30

    if-eqz v30, :cond_e

    .line 775
    const-string v30, "title"

    move-object/from16 v0, p2

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v27

    .line 776
    .local v27, "title":Ljava/lang/String;
    const-string v30, ".snb"

    move-object/from16 v0, v27

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v10

    .line 777
    .local v10, "i":I
    const/16 v30, -0x1

    move/from16 v0, v30

    if-eq v10, v0, :cond_e

    .line 778
    const-string v30, "title"

    const/16 v31, 0x0

    move-object/from16 v0, v27

    move/from16 v1, v31

    invoke-virtual {v0, v1, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v31

    move-object/from16 v0, p2

    move-object/from16 v1, v30

    move-object/from16 v2, v31

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 780
    .end local v10    # "i":I
    .end local v27    # "title":Ljava/lang/String;
    :cond_e
    sget-object v11, Lcom/samsung/android/provider/memo/MemoContract;->BASE_URI_MEMO:Landroid/net/Uri;

    .line 781
    const/4 v5, 0x1

    .line 782
    goto/16 :goto_2

    .line 784
    :sswitch_3
    sget-object v11, Lcom/samsung/android/provider/memo/MemoContract;->BASE_URI_SYNCSTATE:Landroid/net/Uri;

    .line 785
    goto/16 :goto_2

    .line 788
    :sswitch_4
    new-instance v30, Ljava/lang/IllegalArgumentException;

    new-instance v31, Ljava/lang/StringBuilder;

    const-string v32, "Unknown URI: "

    invoke-direct/range {v31 .. v32}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v31

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v31

    invoke-virtual/range {v31 .. v31}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v31

    invoke-direct/range {v30 .. v31}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v30
    :try_end_5
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_5} :catch_0

    .line 811
    .restart local v8    # "db":Landroid/database/sqlite/SQLiteDatabase;
    .restart local v14    # "isUUIDConflict":Z
    .restart local v16    # "newUUID":Ljava/lang/String;
    :cond_f
    const/16 v30, 0x3

    move/from16 v0, v30

    if-ne v15, v0, :cond_4

    .line 812
    :try_start_6
    move-object/from16 v0, p0

    move-object/from16 v1, v26

    move-object/from16 v2, v29

    invoke-direct {v0, v8, v1, v2}, Lcom/samsung/android/provider/memo/MemoProvider;->isUUIDExist(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v30

    if-eqz v30, :cond_4

    .line 813
    invoke-static {}, Lcom/samsung/android/app/memo/util/UUIDHelper;->newUUID()Ljava/lang/String;

    move-result-object v16

    .line 814
    const-string v30, "UUID"

    move-object/from16 v0, p2

    move-object/from16 v1, v30

    move-object/from16 v2, v16

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 815
    const-string v30, "isNeedUpsync"

    const/16 v32, 0x1

    invoke-static/range {v32 .. v32}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v32

    move-object/from16 v0, p2

    move-object/from16 v1, v30

    move-object/from16 v2, v32

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 816
    const/4 v14, 0x1

    .line 817
    move-object/from16 v0, p0

    move-object/from16 v1, v29

    move-object/from16 v2, v16

    move-object/from16 v3, p2

    invoke-direct {v0, v8, v1, v2, v3}, Lcom/samsung/android/provider/memo/MemoProvider;->updateEntries(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)V

    goto/16 :goto_3

    .line 798
    .end local v8    # "db":Landroid/database/sqlite/SQLiteDatabase;
    .end local v16    # "newUUID":Ljava/lang/String;
    :catchall_0
    move-exception v30

    monitor-exit v31
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    throw v30

    .line 829
    .restart local v8    # "db":Landroid/database/sqlite/SQLiteDatabase;
    .restart local v16    # "newUUID":Ljava/lang/String;
    :catch_1
    move-exception v9

    .line 830
    .local v9, "e":Landroid/database/SQLException;
    :try_start_7
    sget-object v30, Lcom/samsung/android/provider/memo/MemoProvider;->TAG:Ljava/lang/String;

    new-instance v32, Ljava/lang/StringBuilder;

    const-string v33, "insert()"

    invoke-direct/range {v32 .. v33}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v32

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v32

    invoke-virtual/range {v32 .. v32}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v32

    move-object/from16 v0, v30

    move-object/from16 v1, v32

    invoke-static {v0, v1, v9}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 831
    throw v9
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    .line 832
    .end local v9    # "e":Landroid/database/SQLException;
    :catchall_1
    move-exception v30

    .line 833
    :try_start_8
    invoke-virtual {v8}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 834
    throw v30
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    .line 845
    .restart local v17    # "resolver":Landroid/content/ContentResolver;
    :cond_10
    const/16 v30, 0x0

    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/provider/memo/MemoProvider;->isSAregistered()Z

    move-result v31

    move-object/from16 v0, v17

    move-object/from16 v1, p1

    move-object/from16 v2, v30

    move/from16 v3, v31

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;Z)V

    goto/16 :goto_4

    .line 861
    .end local v17    # "resolver":Landroid/content/ContentResolver;
    :cond_11
    new-instance v30, Landroid/database/SQLException;

    new-instance v31, Ljava/lang/StringBuilder;

    const-string v32, "insertion failure for "

    invoke-direct/range {v31 .. v32}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v31

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v31

    invoke-virtual/range {v31 .. v31}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v31

    invoke-direct/range {v30 .. v31}, Landroid/database/SQLException;-><init>(Ljava/lang/String;)V

    throw v30

    .line 740
    nop

    :sswitch_data_0
    .sparse-switch
        -0x1 -> :sswitch_4
        0x3 -> :sswitch_2
        0x6 -> :sswitch_0
        0x9 -> :sswitch_1
        0xf -> :sswitch_3
    .end sparse-switch
.end method

.method public onCreate()Z
    .locals 2

    .prologue
    .line 234
    sget-object v0, Lcom/samsung/android/provider/memo/MemoProvider;->TAG:Ljava/lang/String;

    const-string v1, "onCreate()"

    invoke-static {v0, v1}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 235
    new-instance v0, Lcom/samsung/android/provider/memo/MemoDBHelper;

    invoke-virtual {p0}, Lcom/samsung/android/provider/memo/MemoProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/samsung/android/provider/memo/MemoDBHelper;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/provider/memo/MemoProvider;->mMemoDBHelper:Landroid/database/sqlite/SQLiteOpenHelper;

    .line 236
    const/4 v0, -0x1

    iput v0, p0, Lcom/samsung/android/provider/memo/MemoProvider;->mPDMUid:I

    .line 237
    new-instance v0, Lcom/samsung/android/app/memo/util/CFLHelper;

    invoke-virtual {p0}, Lcom/samsung/android/provider/memo/MemoProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/samsung/android/app/memo/util/CFLHelper;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/provider/memo/MemoProvider;->mCFL:Lcom/samsung/android/app/memo/util/CFLHelper;

    .line 238
    const/4 v0, 0x1

    return v0
.end method

.method public openFile(Landroid/net/Uri;Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;
    .locals 17
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "mode"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation

    .prologue
    .line 1168
    invoke-static/range {p1 .. p1}, Lcom/samsung/android/provider/memo/MemoProvider;->match(Landroid/net/Uri;)I

    move-result v14

    .line 1169
    .local v14, "match":I
    const/16 v16, 0x0

    .line 1170
    .local v16, "uuid":Ljava/lang/String;
    const/4 v4, 0x0

    .line 1171
    .local v4, "selection":Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v13

    .line 1173
    .local v13, "lastPathSeg":Ljava/lang/String;
    packed-switch v14, :pswitch_data_0

    .line 1185
    :pswitch_0
    :try_start_0
    new-instance v2, Ljava/io/FileNotFoundException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v5, "Invalid URI: "

    invoke-direct {v3, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p1

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    throw v2
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 1187
    :catch_0
    move-exception v15

    .line 1188
    .local v15, "t":Ljava/lang/Throwable;
    sget-object v2, Lcom/samsung/android/provider/memo/MemoProvider;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v5, "update()"

    invoke-direct {v3, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p1

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v15}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1189
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "error in preprocessing"

    invoke-direct {v2, v3, v15}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2

    .line 1176
    .end local v15    # "t":Ljava/lang/Throwable;
    :pswitch_1
    :try_start_1
    invoke-static {v13}, Lcom/samsung/android/app/memo/util/UUIDHelper;->verify(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    .line 1177
    move-object/from16 v16, v13

    .line 1192
    :goto_0
    if-nez v16, :cond_1

    if-eqz v4, :cond_1

    .line 1193
    const/4 v9, 0x0

    .line 1194
    .local v9, "c":Landroid/database/Cursor;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/provider/memo/MemoProvider;->mMemoDBHelper:Landroid/database/sqlite/SQLiteOpenHelper;

    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteOpenHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 1196
    .local v1, "db":Landroid/database/sqlite/SQLiteDatabase;
    :try_start_2
    move-object/from16 v0, p0

    invoke-direct {v0, v14}, Lcom/samsung/android/provider/memo/MemoProvider;->getTableName(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "UUID"

    aput-object v6, v3, v5

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual/range {v1 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 1197
    invoke-interface {v9}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1198
    const/4 v2, 0x0

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_2
    .catch Landroid/database/SQLException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v16

    .line 1204
    :cond_0
    if-eqz v9, :cond_1

    .line 1205
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    .line 1209
    .end local v1    # "db":Landroid/database/sqlite/SQLiteDatabase;
    .end local v9    # "c":Landroid/database/Cursor;
    :cond_1
    if-nez v16, :cond_3

    .line 1210
    new-instance v2, Ljava/io/FileNotFoundException;

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 1181
    :pswitch_2
    :try_start_3
    invoke-static {v13}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    .line 1182
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "_id="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/16 v3, 0x20

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_0

    move-result-object v4

    .line 1183
    goto :goto_0

    .line 1200
    .restart local v1    # "db":Landroid/database/sqlite/SQLiteDatabase;
    .restart local v9    # "c":Landroid/database/Cursor;
    :catch_1
    move-exception v10

    .line 1201
    .local v10, "e":Landroid/database/SQLException;
    :try_start_4
    sget-object v2, Lcom/samsung/android/provider/memo/MemoProvider;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v5, "openFile()"

    invoke-direct {v3, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p1

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v10}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1202
    throw v10
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 1203
    .end local v10    # "e":Landroid/database/SQLException;
    :catchall_0
    move-exception v2

    .line 1204
    if-eqz v9, :cond_2

    .line 1205
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    .line 1206
    :cond_2
    throw v2

    .line 1212
    .end local v1    # "db":Landroid/database/sqlite/SQLiteDatabase;
    .end local v9    # "c":Landroid/database/Cursor;
    :cond_3
    invoke-static/range {v16 .. v16}, Lcom/samsung/android/app/memo/util/FileHelper;->getPrivateFile(Ljava/lang/String;)Ljava/io/File;

    move-result-object v11

    .line 1214
    .local v11, "f":Ljava/io/File;
    const/4 v12, 0x0

    .line 1215
    .local v12, "imode":I
    if-nez p2, :cond_4

    .line 1216
    const-string p2, "r"

    .line 1217
    :cond_4
    const-string v2, "w"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 1218
    const/high16 v2, 0x20000000

    or-int/2addr v12, v2

    .line 1219
    invoke-virtual {v11}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_5

    .line 1221
    :try_start_5
    invoke-virtual {v11}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->mkdirs()Z

    .line 1222
    invoke-virtual {v11}, Ljava/io/File;->createNewFile()Z
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2

    .line 1229
    :cond_5
    const-string v2, "r"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_6

    const/high16 v2, 0x10000000

    or-int/2addr v12, v2

    .line 1230
    :cond_6
    const-string v2, "+"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_7

    const/high16 v2, 0x2000000

    or-int/2addr v12, v2

    .line 1232
    :cond_7
    invoke-static {v11, v12}, Landroid/os/ParcelFileDescriptor;->open(Ljava/io/File;I)Landroid/os/ParcelFileDescriptor;

    move-result-object v2

    return-object v2

    .line 1223
    :catch_2
    move-exception v10

    .line 1224
    .local v10, "e":Ljava/io/IOException;
    sget-object v2, Lcom/samsung/android/provider/memo/MemoProvider;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v5, "openFile()"

    invoke-direct {v3, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p1

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v10}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1225
    new-instance v2, Ljava/io/FileNotFoundException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v5, "can\'t create file for "

    invoke-direct {v3, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p1

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 1173
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 14
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "projection"    # [Ljava/lang/String;
    .param p3, "selection"    # Ljava/lang/String;
    .param p4, "selectionArgs"    # [Ljava/lang/String;
    .param p5, "sortOrder"    # Ljava/lang/String;

    .prologue
    .line 440
    const/4 v9, 0x0

    .line 441
    .local v9, "c":Landroid/database/Cursor;
    invoke-static {p1}, Lcom/samsung/android/provider/memo/MemoProvider;->match(Landroid/net/Uri;)I

    move-result v10

    .line 442
    .local v10, "matchId":I
    const/16 v3, 0xa

    if-ne v10, v3, :cond_0

    .line 443
    const/4 v3, 0x0

    aget-object v3, p4, v3

    invoke-static {v3}, Landroid/net/Uri;->decode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/samsung/android/provider/memo/MemoProvider;->getSuggestions(Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v3

    .line 497
    :goto_0
    return-object v3

    .line 444
    :cond_0
    const/16 v3, 0xb

    if-ne v10, v3, :cond_2

    .line 445
    if-nez p3, :cond_1

    if-nez p4, :cond_1

    .line 446
    const/4 v3, 0x1

    new-array v11, v3, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "[]"

    aput-object v4, v11, v3

    .line 447
    .local v11, "selection_args":[Ljava/lang/String;
    invoke-direct {p0, p1, v11}, Lcom/samsung/android/provider/memo/MemoProvider;->getRegexSuggestion(Landroid/net/Uri;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v3

    goto :goto_0

    .line 450
    .end local v11    # "selection_args":[Ljava/lang/String;
    :cond_1
    move-object/from16 v0, p4

    invoke-direct {p0, p1, v0}, Lcom/samsung/android/provider/memo/MemoProvider;->getRegexSuggestion(Landroid/net/Uri;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v3

    goto :goto_0

    .line 452
    :cond_2
    invoke-direct {p0, v10}, Lcom/samsung/android/provider/memo/MemoProvider;->getTableName(I)Ljava/lang/String;

    move-result-object v2

    .line 454
    .local v2, "tblName":Ljava/lang/String;
    packed-switch v10, :pswitch_data_0

    .line 476
    :goto_1
    :pswitch_0
    :try_start_0
    invoke-direct {p0, p1}, Lcom/samsung/android/provider/memo/MemoProvider;->isCalledByPDM(Landroid/net/Uri;)Z

    move-result v3

    if-nez v3, :cond_3

    invoke-direct {p0, p1}, Lcom/samsung/android/provider/memo/MemoProvider;->isForce(Landroid/net/Uri;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 477
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v3, "isDeleted IS 0"

    invoke-direct {v4, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 478
    invoke-static/range {p3 .. p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_6

    const-string v3, ""

    :goto_2
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 477
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object p3

    .line 485
    :cond_3
    iget-object v3, p0, Lcom/samsung/android/provider/memo/MemoProvider;->mMemoDBHelper:Landroid/database/sqlite/SQLiteOpenHelper;

    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteOpenHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 488
    .local v1, "db":Landroid/database/sqlite/SQLiteDatabase;
    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v8, p5

    :try_start_1
    invoke-virtual/range {v1 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 489
    invoke-virtual {p0}, Lcom/samsung/android/provider/memo/MemoProvider;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    invoke-interface {v9, v3, p1}, Landroid/database/Cursor;->setNotificationUri(Landroid/content/ContentResolver;Landroid/net/Uri;)V
    :try_end_1
    .catch Landroid/database/SQLException; {:try_start_1 .. :try_end_1} :catch_1

    move-object v3, v9

    .line 497
    goto :goto_0

    .line 458
    .end local v1    # "db":Landroid/database/sqlite/SQLiteDatabase;
    :pswitch_1
    :try_start_2
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "_id="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 459
    invoke-static/range {p3 .. p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_4

    const-string v3, ""

    :goto_3
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 458
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p3

    .line 460
    goto :goto_1

    .line 459
    :cond_4
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v5, " AND ("

    invoke-direct {v3, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const/16 v5, 0x29

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    goto :goto_3

    .line 464
    :pswitch_2
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "UUID IS \'"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const/16 v4, 0x27

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 465
    invoke-static/range {p3 .. p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_5

    const-string v3, ""

    :goto_4
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 464
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p3

    .line 466
    goto/16 :goto_1

    .line 465
    :cond_5
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v5, " AND ("

    invoke-direct {v3, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const/16 v5, 0x29

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    goto :goto_4

    .line 469
    :pswitch_3
    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->decode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, ""

    const/4 v5, 0x0

    move-object/from16 v0, p2

    invoke-direct {p0, v3, v0, v4, v5}, Lcom/samsung/android/provider/memo/MemoProvider;->search(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Z)Landroid/database/Cursor;

    move-result-object v3

    goto/16 :goto_0

    .line 472
    :pswitch_4
    new-instance v3, Ljava/lang/IllegalArgumentException;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Unknown URI: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_0

    .line 480
    :catch_0
    move-exception v13

    .line 481
    .local v13, "t":Ljava/lang/Throwable;
    sget-object v3, Lcom/samsung/android/provider/memo/MemoProvider;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "query()"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v13}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 482
    new-instance v3, Landroid/database/SQLException;

    const-string v4, "error in preprocessing"

    invoke-direct {v3, v4, v13}, Landroid/database/SQLException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v3

    .line 478
    .end local v13    # "t":Ljava/lang/Throwable;
    :cond_6
    :try_start_3
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v5, " AND ("

    invoke-direct {v3, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const/16 v5, 0x29

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_0

    move-result-object v3

    goto/16 :goto_2

    .line 490
    .restart local v1    # "db":Landroid/database/sqlite/SQLiteDatabase;
    :catch_1
    move-exception v12

    .line 491
    .local v12, "sqle":Landroid/database/SQLException;
    sget-object v3, Lcom/samsung/android/provider/memo/MemoProvider;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "query()"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v12}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 492
    throw v12

    .line 454
    nop

    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_4
        :pswitch_3
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method public update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 22
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "values"    # Landroid/content/ContentValues;
    .param p3, "selection"    # Ljava/lang/String;
    .param p4, "selectionArgs"    # [Ljava/lang/String;

    .prologue
    .line 866
    const-wide/16 v10, 0x0

    .line 867
    .local v10, "count":J
    invoke-direct/range {p0 .. p1}, Lcom/samsung/android/provider/memo/MemoProvider;->isCalledByPDM(Landroid/net/Uri;)Z

    move-result v14

    .line 868
    .local v14, "isCalledByPDM":Z
    const/4 v12, 0x0

    .line 869
    .local v12, "createThumbnail":Z
    const-string v6, "UUID"

    move-object/from16 v0, p2

    invoke-virtual {v0, v6}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v20

    .line 872
    .local v20, "uuid":Ljava/lang/String;
    const-string v6, "_id"

    move-object/from16 v0, p2

    invoke-virtual {v0, v6}, Landroid/content/ContentValues;->remove(Ljava/lang/String;)V

    .line 873
    const-string v6, "UUID"

    move-object/from16 v0, p2

    invoke-virtual {v0, v6}, Landroid/content/ContentValues;->remove(Ljava/lang/String;)V

    .line 875
    invoke-static/range {p1 .. p1}, Lcom/samsung/android/provider/memo/MemoProvider;->match(Landroid/net/Uri;)I

    move-result v17

    .line 876
    .local v17, "match":I
    if-nez v14, :cond_0

    const/16 v6, 0xd

    move/from16 v0, v17

    if-lt v0, v6, :cond_0

    const/16 v6, 0xf

    move/from16 v0, v17

    if-gt v0, v6, :cond_0

    .line 877
    new-instance v6, Ljava/lang/IllegalArgumentException;

    const-string v7, "_sync_state table is accessed by Memo app."

    invoke-direct {v6, v7}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 879
    :cond_0
    if-nez v14, :cond_2

    const/16 v6, 0xd

    move/from16 v0, v17

    if-ge v0, v6, :cond_2

    .line 880
    const-string v6, "v_skipDirtySet"

    move-object/from16 v0, p2

    invoke-virtual {v0, v6}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_8

    .line 881
    const-string v6, "isDirty"

    move-object/from16 v0, p2

    invoke-virtual {v0, v6}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_1

    .line 882
    const-string v6, "isDirty"

    const/4 v7, 0x1

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    move-object/from16 v0, p2

    invoke-virtual {v0, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 885
    :cond_1
    :goto_0
    const-string v6, "v_skipSync1Set"

    move-object/from16 v0, p2

    invoke-virtual {v0, v6}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_9

    .line 886
    const-string v6, "sync1"

    invoke-static {}, Lcom/samsung/android/app/memo/util/Utils;->getCorrectedTime()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    move-object/from16 v0, p2

    invoke-virtual {v0, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 891
    :cond_2
    :goto_1
    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v16

    .line 893
    .local v16, "lastPathSeg":Ljava/lang/String;
    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-direct {v0, v1}, Lcom/samsung/android/provider/memo/MemoProvider;->getTableName(I)Ljava/lang/String;

    move-result-object v5

    .line 895
    .local v5, "tblName":Ljava/lang/String;
    packed-switch v17, :pswitch_data_0

    .line 935
    :goto_2
    :pswitch_0
    const/4 v15, 0x0

    .line 936
    .local v15, "isNeedUpsync":Z
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/provider/memo/MemoProvider;->mMemoDBHelper:Landroid/database/sqlite/SQLiteOpenHelper;

    move-object/from16 v21, v0

    monitor-enter v21

    .line 937
    :try_start_0
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/android/provider/memo/MemoProvider;->mMemoDBHelper:Landroid/database/sqlite/SQLiteOpenHelper;

    invoke-virtual {v6}, Landroid/database/sqlite/SQLiteOpenHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v4

    .line 938
    .local v4, "db":Landroid/database/sqlite/SQLiteDatabase;
    invoke-virtual {v4}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 940
    if-eqz v14, :cond_3

    .line 941
    :try_start_1
    const-string v6, "isDirty"

    move-object/from16 v0, p2

    invoke-virtual {v0, v6}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 942
    const-string v6, "isDirty"

    move-object/from16 v0, p2

    invoke-virtual {v0, v6}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    if-nez v6, :cond_3

    .line 943
    move-object/from16 v0, p0

    move-object/from16 v1, p3

    move-object/from16 v2, p4

    invoke-direct {v0, v4, v5, v1, v2}, Lcom/samsung/android/provider/memo/MemoProvider;->isNeedUpsync(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 944
    const/4 v6, 0x1

    move/from16 v0, v17

    if-lt v0, v6, :cond_e

    const/4 v6, 0x3

    move/from16 v0, v17

    if-gt v0, v6, :cond_e

    .line 945
    const-string v6, "isNeedUpsync"

    const/4 v7, 0x0

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    move-object/from16 v0, p2

    invoke-virtual {v0, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 946
    const-string v6, "isDirty"

    const/4 v7, 0x1

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    move-object/from16 v0, p2

    invoke-virtual {v0, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 947
    const/4 v15, 0x1

    .line 955
    :cond_3
    :goto_3
    const/4 v9, 0x1

    move-object/from16 v6, p2

    move-object/from16 v7, p3

    move-object/from16 v8, p4

    invoke-virtual/range {v4 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->updateWithOnConflict(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;I)I

    move-result v6

    int-to-long v10, v6

    .line 956
    invoke-virtual {v4}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_1
    .catch Landroid/database/SQLException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 961
    :try_start_2
    invoke-virtual {v4}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 936
    monitor-exit v21
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 965
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/provider/memo/MemoProvider;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v18

    .line 966
    .local v18, "resolver":Landroid/content/ContentResolver;
    const/4 v6, 0x3

    move/from16 v0, v17

    if-eq v0, v6, :cond_4

    const/4 v6, 0x6

    move/from16 v0, v17

    if-eq v0, v6, :cond_4

    const/16 v6, 0x9

    move/from16 v0, v17

    if-eq v0, v6, :cond_4

    .line 967
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v7, "://"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const/16 v7, 0x2f

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v6

    const/4 v8, 0x0

    invoke-interface {v6, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object/from16 v0, v18

    invoke-virtual {v0, v6, v7, v8}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;Z)V

    .line 969
    :cond_4
    if-eqz v14, :cond_f

    .line 970
    const/4 v6, 0x0

    move-object/from16 v0, v18

    move-object/from16 v1, p1

    invoke-virtual {v0, v1, v6, v15}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;Z)V

    .line 973
    :goto_4
    if-eqz v12, :cond_5

    if-eqz v20, :cond_5

    invoke-static/range {v20 .. v20}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_5

    .line 974
    invoke-static {}, Lcom/samsung/android/app/memo/util/Utils;->getAppContext()Landroid/content/Context;

    move-result-object v6

    move-object/from16 v0, v20

    invoke-static {v6, v0}, Lcom/samsung/android/app/memo/util/ImageUtil;->createThumbnailOnDiscreteThread(Landroid/content/Context;Ljava/lang/String;)V

    .line 976
    :cond_5
    if-nez v14, :cond_7

    const/4 v6, 0x1

    move/from16 v0, v17

    if-eq v0, v6, :cond_6

    const/4 v6, 0x2

    move/from16 v0, v17

    if-ne v0, v6, :cond_7

    .line 977
    :cond_6
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/android/provider/memo/MemoProvider;->mCFL:Lcom/samsung/android/app/memo/util/CFLHelper;

    move-object/from16 v0, p1

    invoke-virtual {v6, v0}, Lcom/samsung/android/app/memo/util/CFLHelper;->modified(Landroid/net/Uri;)V

    .line 980
    :cond_7
    long-to-int v6, v10

    return v6

    .line 884
    .end local v4    # "db":Landroid/database/sqlite/SQLiteDatabase;
    .end local v5    # "tblName":Ljava/lang/String;
    .end local v15    # "isNeedUpsync":Z
    .end local v16    # "lastPathSeg":Ljava/lang/String;
    .end local v18    # "resolver":Landroid/content/ContentResolver;
    :cond_8
    const-string v6, "v_skipDirtySet"

    move-object/from16 v0, p2

    invoke-virtual {v0, v6}, Landroid/content/ContentValues;->remove(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 888
    :cond_9
    const-string v6, "v_skipSync1Set"

    move-object/from16 v0, p2

    invoke-virtual {v0, v6}, Landroid/content/ContentValues;->remove(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 897
    .restart local v5    # "tblName":Ljava/lang/String;
    .restart local v16    # "lastPathSeg":Ljava/lang/String;
    :pswitch_1
    :try_start_3
    const-string v6, "content"

    move-object/from16 v0, p2

    invoke-virtual {v0, v6}, Landroid/content/ContentValues;->remove(Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_0

    goto/16 :goto_2

    .line 930
    :catch_0
    move-exception v19

    .line 931
    .local v19, "t":Ljava/lang/Throwable;
    sget-object v6, Lcom/samsung/android/provider/memo/MemoProvider;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "update()"

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p1

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, v19

    invoke-static {v6, v7, v0}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 932
    new-instance v6, Landroid/database/SQLException;

    const-string v7, "error in preprocessing"

    move-object/from16 v0, v19

    invoke-direct {v6, v7, v0}, Landroid/database/SQLException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v6

    .line 903
    .end local v19    # "t":Ljava/lang/Throwable;
    :pswitch_2
    :try_start_4
    const-string v6, "content"

    move-object/from16 v0, p2

    invoke-virtual {v0, v6}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_a

    .line 904
    const-string v6, "strippedContent"

    .line 905
    const-string v7, "content"

    move-object/from16 v0, p2

    invoke-virtual {v0, v7}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/samsung/android/app/memo/util/HtmlUtil;->stripHtmlToSingleLine(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v7

    invoke-interface {v7}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v7

    .line 904
    move-object/from16 v0, p2

    invoke-virtual {v0, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 906
    :cond_a
    const/4 v12, 0x1

    .line 910
    :pswitch_3
    invoke-static/range {v16 .. v16}, Lcom/samsung/android/app/memo/util/UUIDHelper;->verify(Ljava/lang/String;)V

    .line 911
    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "UUID=\'"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v16

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const/16 v7, 0x27

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v7

    .line 912
    invoke-static/range {p3 .. p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_b

    const-string v6, ""

    :goto_5
    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    .line 911
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p3

    .line 913
    goto/16 :goto_2

    .line 912
    :cond_b
    new-instance v6, Ljava/lang/StringBuilder;

    const-string v8, " AND ("

    invoke-direct {v6, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p3

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const/16 v8, 0x29

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    goto :goto_5

    .line 915
    :pswitch_4
    const-string v6, "content"

    move-object/from16 v0, p2

    invoke-virtual {v0, v6}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_c

    .line 916
    const-string v6, "strippedContent"

    .line 917
    const-string v7, "content"

    move-object/from16 v0, p2

    invoke-virtual {v0, v7}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/samsung/android/app/memo/util/HtmlUtil;->stripHtmlToSingleLine(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v7

    invoke-interface {v7}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v7

    .line 916
    move-object/from16 v0, p2

    invoke-virtual {v0, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 918
    :cond_c
    const/4 v12, 0x1

    .line 922
    :pswitch_5
    invoke-static/range {v16 .. v16}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    .line 923
    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "_id= "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v16

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const/16 v7, 0x20

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v7

    .line 924
    invoke-static/range {p3 .. p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_d

    const-string v6, ""

    :goto_6
    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    .line 923
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p3

    .line 925
    goto/16 :goto_2

    .line 924
    :cond_d
    new-instance v6, Ljava/lang/StringBuilder;

    const-string v8, " AND ("

    invoke-direct {v6, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p3

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const/16 v8, 0x29

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    goto :goto_6

    .line 928
    :pswitch_6
    new-instance v6, Ljava/lang/IllegalArgumentException;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "Invalid URI: "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p1

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v6
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_0

    .line 948
    .restart local v4    # "db":Landroid/database/sqlite/SQLiteDatabase;
    .restart local v15    # "isNeedUpsync":Z
    :cond_e
    const/4 v6, 0x7

    move/from16 v0, v17

    if-lt v0, v6, :cond_3

    const/16 v6, 0x9

    move/from16 v0, v17

    if-gt v0, v6, :cond_3

    .line 949
    :try_start_5
    const-string v6, "isNeedUpsync"

    const/4 v7, 0x0

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    move-object/from16 v0, p2

    invoke-virtual {v0, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 950
    const-string v6, "isDirty"

    const/4 v7, 0x1

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    move-object/from16 v0, p2

    invoke-virtual {v0, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 951
    const-string v6, "isDeleted"

    const/4 v7, 0x1

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    move-object/from16 v0, p2

    invoke-virtual {v0, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V
    :try_end_5
    .catch Landroid/database/SQLException; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 952
    const/4 v15, 0x1

    goto/16 :goto_3

    .line 957
    :catch_1
    move-exception v13

    .line 958
    .local v13, "e":Landroid/database/SQLException;
    :try_start_6
    sget-object v6, Lcom/samsung/android/provider/memo/MemoProvider;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "update()"

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p1

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7, v13}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 959
    throw v13
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 960
    .end local v13    # "e":Landroid/database/SQLException;
    :catchall_0
    move-exception v6

    .line 961
    :try_start_7
    invoke-virtual {v4}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 962
    throw v6

    .line 936
    .end local v4    # "db":Landroid/database/sqlite/SQLiteDatabase;
    :catchall_1
    move-exception v6

    monitor-exit v21
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    throw v6

    .line 972
    .restart local v4    # "db":Landroid/database/sqlite/SQLiteDatabase;
    .restart local v18    # "resolver":Landroid/content/ContentResolver;
    :cond_f
    const/4 v6, 0x0

    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/provider/memo/MemoProvider;->isSAregistered()Z

    move-result v7

    move-object/from16 v0, v18

    move-object/from16 v1, p1

    invoke-virtual {v0, v1, v6, v7}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;Z)V

    goto/16 :goto_4

    .line 895
    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_6
        :pswitch_0
        :pswitch_4
        :pswitch_2
        :pswitch_1
        :pswitch_5
        :pswitch_3
        :pswitch_0
        :pswitch_5
        :pswitch_3
        :pswitch_0
    .end packed-switch
.end method
