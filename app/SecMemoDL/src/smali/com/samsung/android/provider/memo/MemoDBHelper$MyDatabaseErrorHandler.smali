.class Lcom/samsung/android/provider/memo/MemoDBHelper$MyDatabaseErrorHandler;
.super Ljava/lang/Object;
.source "MemoDBHelper.java"

# interfaces
.implements Landroid/database/DatabaseErrorHandler;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/provider/memo/MemoDBHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "MyDatabaseErrorHandler"
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/samsung/android/provider/memo/MemoDBHelper$MyDatabaseErrorHandler;)V
    .locals 0

    .prologue
    .line 46
    invoke-direct {p0}, Lcom/samsung/android/provider/memo/MemoDBHelper$MyDatabaseErrorHandler;-><init>()V

    return-void
.end method


# virtual methods
.method public onCorruption(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 0
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 50
    # invokes: Lcom/samsung/android/provider/memo/MemoDBHelper;->dropTables(Landroid/database/sqlite/SQLiteDatabase;)V
    invoke-static {p1}, Lcom/samsung/android/provider/memo/MemoDBHelper;->access$0(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 51
    # invokes: Lcom/samsung/android/provider/memo/MemoDBHelper;->createTables(Landroid/database/sqlite/SQLiteDatabase;)V
    invoke-static {p1}, Lcom/samsung/android/provider/memo/MemoDBHelper;->access$1(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 52
    return-void
.end method
