.class final Lcom/samsung/android/provider/memo/MemoDBHelper;
.super Landroid/database/sqlite/SQLiteOpenHelper;
.source "MemoDBHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/provider/memo/MemoDBHelper$MyDatabaseErrorHandler;
    }
.end annotation


# static fields
.field private static final DB_FILE:Ljava/lang/String; = "memo.db"

.field private static final TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 42
    const-class v0, Lcom/samsung/android/provider/memo/MemoDBHelper;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/provider/memo/MemoDBHelper;->TAG:Ljava/lang/String;

    .line 44
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v3, 0x0

    .line 279
    const-string v2, "memo.db"

    const/16 v4, 0x9

    new-instance v5, Lcom/samsung/android/provider/memo/MemoDBHelper$MyDatabaseErrorHandler;

    invoke-direct {v5, v3}, Lcom/samsung/android/provider/memo/MemoDBHelper$MyDatabaseErrorHandler;-><init>(Lcom/samsung/android/provider/memo/MemoDBHelper$MyDatabaseErrorHandler;)V

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Landroid/database/sqlite/SQLiteOpenHelper;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;ILandroid/database/DatabaseErrorHandler;)V

    .line 280
    sget-object v0, Lcom/samsung/android/provider/memo/MemoDBHelper;->TAG:Ljava/lang/String;

    const-string v1, "MemoDBHelper()"

    invoke-static {v0, v1}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 281
    return-void
.end method

.method static synthetic access$0(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 0

    .prologue
    .line 249
    invoke-static {p0}, Lcom/samsung/android/provider/memo/MemoDBHelper;->dropTables(Landroid/database/sqlite/SQLiteDatabase;)V

    return-void
.end method

.method static synthetic access$1(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 0

    .prologue
    .line 122
    invoke-static {p0}, Lcom/samsung/android/provider/memo/MemoDBHelper;->createTables(Landroid/database/sqlite/SQLiteDatabase;)V

    return-void
.end method

.method private alterDBtoVersion8(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 3
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 333
    sget-object v1, Lcom/samsung/android/provider/memo/MemoDBHelper;->TAG:Ljava/lang/String;

    const-string v2, "alterDBtoVersion8()"

    invoke-static {v1, v2}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 334
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 336
    :try_start_0
    const-string v1, "ALTER TABLE memo ADD COLUMN sync1 INTEGER"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 337
    const-string v1, "ALTER TABLE memo ADD COLUMN sync2 TEXT"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 339
    const-string v1, "ALTER TABLE file ADD COLUMN sync1 INTEGER"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 340
    const-string v1, "ALTER TABLE file ADD COLUMN sync2 TEXT"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 342
    const-string v1, "ALTER TABLE category ADD COLUMN sync1 INTEGER"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 343
    const-string v1, "ALTER TABLE category ADD COLUMN sync2 TEXT"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 344
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 349
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 350
    invoke-direct {p0, p1}, Lcom/samsung/android/provider/memo/MemoDBHelper;->updateTimestampsToUTC(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 351
    invoke-direct {p0, p1}, Lcom/samsung/android/provider/memo/MemoDBHelper;->updatePathsToRelative(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 353
    return-void

    .line 345
    :catch_0
    move-exception v0

    .line 346
    .local v0, "sqle":Landroid/database/SQLException;
    :try_start_1
    sget-object v1, Lcom/samsung/android/provider/memo/MemoDBHelper;->TAG:Ljava/lang/String;

    const-string v2, "alterDBtoVersion8()"

    invoke-static {v1, v2, v0}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 347
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 348
    .end local v0    # "sqle":Landroid/database/SQLException;
    :catchall_0
    move-exception v1

    .line 349
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 350
    invoke-direct {p0, p1}, Lcom/samsung/android/provider/memo/MemoDBHelper;->updateTimestampsToUTC(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 351
    invoke-direct {p0, p1}, Lcom/samsung/android/provider/memo/MemoDBHelper;->updatePathsToRelative(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 352
    throw v1
.end method

.method private alterDBtoVersion9(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 3
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 356
    sget-object v1, Lcom/samsung/android/provider/memo/MemoDBHelper;->TAG:Ljava/lang/String;

    const-string v2, "alterDBtoVersion9()"

    invoke-static {v1, v2}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 357
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 359
    :try_start_0
    const-string v1, "ALTER TABLE memo ADD COLUMN isNeedUpsync INTEGER"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 361
    const-string v1, "ALTER TABLE file ADD COLUMN isNeedUpsync INTEGER"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 363
    const-string v1, "ALTER TABLE category ADD COLUMN isNeedUpsync INTEGER"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 364
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 369
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 371
    return-void

    .line 365
    :catch_0
    move-exception v0

    .line 366
    .local v0, "sqle":Landroid/database/SQLException;
    :try_start_1
    sget-object v1, Lcom/samsung/android/provider/memo/MemoDBHelper;->TAG:Ljava/lang/String;

    const-string v2, "alterDBtoVersion9()"

    invoke-static {v1, v2, v0}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 367
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 368
    .end local v0    # "sqle":Landroid/database/SQLException;
    :catchall_0
    move-exception v1

    .line 369
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 370
    throw v1
.end method

.method private static createIndexesFor(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V
    .locals 7
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p1, "tableName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/database/SQLException;
        }
    .end annotation

    .prologue
    .line 56
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "CREATE INDEX IF NOT EXISTS "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "_uuid ON "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " ("

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 57
    const-string v6, "UUID"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 58
    const-string v6, ");"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 56
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 60
    .local v0, "query1":Ljava/lang/String;
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "CREATE INDEX IF NOT EXISTS "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "_sac1 ON "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " ("

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 61
    const-string v6, "isDirty"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 62
    const-string v6, ");"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 60
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 64
    .local v1, "query2":Ljava/lang/String;
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "CREATE INDEX IF NOT EXISTS "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "_sac2 ON "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " ("

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 65
    const-string v6, "isDeleted"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 66
    const-string v6, ");"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 64
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 68
    .local v2, "query3":Ljava/lang/String;
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "CREATE INDEX IF NOT EXISTS "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "_accountName ON "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " ("

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 69
    const-string v6, "accountName"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 70
    const-string v6, ");"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 68
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 72
    .local v3, "query4":Ljava/lang/String;
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "CREATE INDEX IF NOT EXISTS "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "_accountType ON "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " ("

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 73
    const-string v6, "accountType"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 74
    const-string v6, ");"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 72
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 76
    .local v4, "query5":Ljava/lang/String;
    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 77
    invoke-virtual {p0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 78
    invoke-virtual {p0, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 79
    invoke-virtual {p0, v3}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 80
    invoke-virtual {p0, v4}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 81
    return-void
.end method

.method private static createTables(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 13
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 123
    const-string v0, "_id INTEGER PRIMARY KEY AUTOINCREMENT, UUID TEXT    NOT NULL UNIQUE, isDirty INTEGER NOT NULL DEFAULT 0, isDeleted INTEGER NOT NULL DEFAULT 0, accountType TEXT, accountName TEXT DEFAULT \'\', sync1 INTEGER, sync2 TEXT, isNeedUpsync INTEGER NOT NULL DEFAULT 0, "

    .line 134
    .local v0, "common":Ljava/lang/String;
    const-string v1, "CREATE TABLE IF NOT EXISTS memo ( _id INTEGER PRIMARY KEY AUTOINCREMENT, UUID TEXT    NOT NULL UNIQUE, isDirty INTEGER NOT NULL DEFAULT 0, isDeleted INTEGER NOT NULL DEFAULT 0, accountType TEXT, accountName TEXT DEFAULT \'\', sync1 INTEGER, sync2 TEXT, isNeedUpsync INTEGER NOT NULL DEFAULT 0, createdAt INTEGER NOT NULL, lastModifiedAt INTEGER NOT NULL, categoryUUID TEXT    NOT NULL DEFAULT \'\', title TEXT    NOT NULL DEFAULT \'\', content TEXT    NOT NULL DEFAULT \'\', vrfileUUID TEXT    NOT NULL DEFAULT \'\', strippedContent TEXT    NOT NULL DEFAULT \'\', _data TEXT    NOT NULL DEFAULT \'\', _phoneNum TEXT );"

    .line 148
    .local v1, "query10":Ljava/lang/String;
    const-string v2, "CREATE INDEX IF NOT EXISTS memo_createdAt ON memo (createdAt);"

    .line 152
    .local v2, "query12":Ljava/lang/String;
    const-string v3, "CREATE INDEX IF NOT EXISTS memo_lastModifiedAt ON memo (lastModifiedAt);"

    .line 156
    .local v3, "query13":Ljava/lang/String;
    const-string v4, "CREATE INDEX IF NOT EXISTS memo_categoryUUID ON memo (categoryUUID);"

    .line 161
    .local v4, "query14":Ljava/lang/String;
    const-string v5, "CREATE TABLE IF NOT EXISTS file ( _id INTEGER PRIMARY KEY AUTOINCREMENT, UUID TEXT    NOT NULL UNIQUE, isDirty INTEGER NOT NULL DEFAULT 0, isDeleted INTEGER NOT NULL DEFAULT 0, accountType TEXT, accountName TEXT DEFAULT \'\', sync1 INTEGER, sync2 TEXT, isNeedUpsync INTEGER NOT NULL DEFAULT 0, memoUUID TEXT    NOT NULL DEFAULT \'\', mime_type TEXT    NOT NULL DEFAULT \'application/octet-stream\', _display_name TEXT    NOT NULL DEFAULT \'\', _size INTEGER NOT NULL DEFAULT 0, orientation INTEGER NOT NULL DEFAULT 0, _data TEXT    NOT NULL );"

    .line 171
    .local v5, "query20":Ljava/lang/String;
    const-string v6, "CREATE INDEX IF NOT EXISTS file_memoUUID ON file (memoUUID);"

    .line 176
    .local v6, "query21":Ljava/lang/String;
    const-string v7, "CREATE TABLE IF NOT EXISTS category ( _id INTEGER PRIMARY KEY AUTOINCREMENT, UUID TEXT    NOT NULL UNIQUE, isDirty INTEGER NOT NULL DEFAULT 0, isDeleted INTEGER NOT NULL DEFAULT 0, accountType TEXT, accountName TEXT DEFAULT \'\', sync1 INTEGER, sync2 TEXT, isNeedUpsync INTEGER NOT NULL DEFAULT 0, orderBy INTEGER NOT NULL DEFAULT 999999, _display_name TEXT    NOT NULL );"

    .line 182
    .local v7, "query30":Ljava/lang/String;
    const-string v8, "CREATE INDEX IF NOT EXISTS category_orderBy ON category (orderBy);"

    .line 187
    .local v8, "query31":Ljava/lang/String;
    const-string v9, "CREATE TABLE IF NOT EXISTS _sync_state ( _id INTEGER PRIMARY KEY AUTOINCREMENT, accountType TEXT, accountName TEXT DEFAULT \'\', data TEXT );"

    .line 195
    .local v9, "query40":Ljava/lang/String;
    sget-object v11, Lcom/samsung/android/provider/memo/MemoDBHelper;->TAG:Ljava/lang/String;

    const-string v12, "createTables()"

    invoke-static {v11, v12}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 197
    :try_start_0
    invoke-virtual {p0}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 199
    const-string v11, "CREATE TABLE IF NOT EXISTS memo ( _id INTEGER PRIMARY KEY AUTOINCREMENT, UUID TEXT    NOT NULL UNIQUE, isDirty INTEGER NOT NULL DEFAULT 0, isDeleted INTEGER NOT NULL DEFAULT 0, accountType TEXT, accountName TEXT DEFAULT \'\', sync1 INTEGER, sync2 TEXT, isNeedUpsync INTEGER NOT NULL DEFAULT 0, createdAt INTEGER NOT NULL, lastModifiedAt INTEGER NOT NULL, categoryUUID TEXT    NOT NULL DEFAULT \'\', title TEXT    NOT NULL DEFAULT \'\', content TEXT    NOT NULL DEFAULT \'\', vrfileUUID TEXT    NOT NULL DEFAULT \'\', strippedContent TEXT    NOT NULL DEFAULT \'\', _data TEXT    NOT NULL DEFAULT \'\', _phoneNum TEXT );"

    invoke-virtual {p0, v11}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 200
    const-string v11, "CREATE INDEX IF NOT EXISTS memo_createdAt ON memo (createdAt);"

    invoke-virtual {p0, v11}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 201
    const-string v11, "CREATE INDEX IF NOT EXISTS memo_lastModifiedAt ON memo (lastModifiedAt);"

    invoke-virtual {p0, v11}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 202
    const-string v11, "CREATE INDEX IF NOT EXISTS memo_categoryUUID ON memo (categoryUUID);"

    invoke-virtual {p0, v11}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 203
    const-string v11, "memo"

    invoke-static {p0, v11}, Lcom/samsung/android/provider/memo/MemoDBHelper;->createIndexesFor(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V

    .line 205
    const-string v11, "CREATE TABLE IF NOT EXISTS file ( _id INTEGER PRIMARY KEY AUTOINCREMENT, UUID TEXT    NOT NULL UNIQUE, isDirty INTEGER NOT NULL DEFAULT 0, isDeleted INTEGER NOT NULL DEFAULT 0, accountType TEXT, accountName TEXT DEFAULT \'\', sync1 INTEGER, sync2 TEXT, isNeedUpsync INTEGER NOT NULL DEFAULT 0, memoUUID TEXT    NOT NULL DEFAULT \'\', mime_type TEXT    NOT NULL DEFAULT \'application/octet-stream\', _display_name TEXT    NOT NULL DEFAULT \'\', _size INTEGER NOT NULL DEFAULT 0, orientation INTEGER NOT NULL DEFAULT 0, _data TEXT    NOT NULL );"

    invoke-virtual {p0, v11}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 206
    const-string v11, "CREATE INDEX IF NOT EXISTS file_memoUUID ON file (memoUUID);"

    invoke-virtual {p0, v11}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 207
    const-string v11, "file"

    invoke-static {p0, v11}, Lcom/samsung/android/provider/memo/MemoDBHelper;->createIndexesFor(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V

    .line 209
    const-string v11, "CREATE TABLE IF NOT EXISTS category ( _id INTEGER PRIMARY KEY AUTOINCREMENT, UUID TEXT    NOT NULL UNIQUE, isDirty INTEGER NOT NULL DEFAULT 0, isDeleted INTEGER NOT NULL DEFAULT 0, accountType TEXT, accountName TEXT DEFAULT \'\', sync1 INTEGER, sync2 TEXT, isNeedUpsync INTEGER NOT NULL DEFAULT 0, orderBy INTEGER NOT NULL DEFAULT 999999, _display_name TEXT    NOT NULL );"

    invoke-virtual {p0, v11}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 210
    const-string v11, "CREATE INDEX IF NOT EXISTS category_orderBy ON category (orderBy);"

    invoke-virtual {p0, v11}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 211
    const-string v11, "category"

    invoke-static {p0, v11}, Lcom/samsung/android/provider/memo/MemoDBHelper;->createIndexesFor(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V

    .line 213
    const-string v11, "CREATE TABLE IF NOT EXISTS _sync_state ( _id INTEGER PRIMARY KEY AUTOINCREMENT, accountType TEXT, accountName TEXT DEFAULT \'\', data TEXT );"

    invoke-virtual {p0, v11}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 215
    invoke-static {p0}, Lcom/samsung/android/provider/memo/MemoDBHelper;->createTriggers(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 217
    invoke-virtual {p0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 222
    invoke-virtual {p0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 247
    return-void

    .line 218
    :catch_0
    move-exception v10

    .line 219
    .local v10, "sqle":Landroid/database/SQLException;
    :try_start_1
    sget-object v11, Lcom/samsung/android/provider/memo/MemoDBHelper;->TAG:Ljava/lang/String;

    const-string v12, "createTables()"

    invoke-static {v11, v12, v10}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 220
    throw v10
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 221
    .end local v10    # "sqle":Landroid/database/SQLException;
    :catchall_0
    move-exception v11

    .line 222
    invoke-virtual {p0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 223
    throw v11
.end method

.method private static createTriggers(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 3
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/database/SQLException;
        }
    .end annotation

    .prologue
    .line 98
    const-string v0, "CREATE TRIGGER trg_memo_01 AFTER UPDATE OF isDeleted ON memo FOR EACH ROW BEGIN     UPDATE file SET isDirty=1, isDeleted=NEW.isDeleted WHERE memoUUID IS NEW.UUID; END; "

    .line 103
    .local v0, "query1":Ljava/lang/String;
    const-string v1, "CREATE TRIGGER trg_memo_02 AFTER UPDATE OF isDeleted ON category FOR EACH ROW WHEN new.isDeleted IS 1 BEGIN     UPDATE memo SET isDirty=1, categoryUUID=\'\' WHERE categoryUUID IS NEW.UUID; END; "

    .line 110
    .local v1, "query2":Ljava/lang/String;
    const-string v2, "CREATE TRIGGER trg_memo_01 AFTER UPDATE OF isDeleted ON memo FOR EACH ROW BEGIN     UPDATE file SET isDirty=1, isDeleted=NEW.isDeleted WHERE memoUUID IS NEW.UUID; END; "

    invoke-virtual {p0, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 111
    const-string v2, "CREATE TRIGGER trg_memo_02 AFTER UPDATE OF isDeleted ON category FOR EACH ROW WHEN new.isDeleted IS 1 BEGIN     UPDATE memo SET isDirty=1, categoryUUID=\'\' WHERE categoryUUID IS NEW.UUID; END; "

    invoke-virtual {p0, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 112
    return-void
.end method

.method private static dropIndexesFor(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V
    .locals 7
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p1, "tableName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/database/SQLException;
        }
    .end annotation

    .prologue
    .line 84
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "DROP INDEX IF EXISTS "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "_uuid;"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 85
    .local v0, "query1":Ljava/lang/String;
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "DROP INDEX IF EXISTS "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "_sac1;"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 86
    .local v1, "query2":Ljava/lang/String;
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "DROP INDEX IF EXISTS "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "_sac2;"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 87
    .local v2, "query3":Ljava/lang/String;
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "DROP INDEX IF EXISTS "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "_accountName;"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 88
    .local v3, "query4":Ljava/lang/String;
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "DROP INDEX IF EXISTS "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "_accountType;"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 90
    .local v4, "query5":Ljava/lang/String;
    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 91
    invoke-virtual {p0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 92
    invoke-virtual {p0, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 93
    invoke-virtual {p0, v3}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 94
    invoke-virtual {p0, v4}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 95
    return-void
.end method

.method private static dropTables(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 3
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 250
    sget-object v1, Lcom/samsung/android/provider/memo/MemoDBHelper;->TAG:Ljava/lang/String;

    const-string v2, "dropTables()"

    invoke-static {v1, v2}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 252
    :try_start_0
    invoke-virtual {p0}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 254
    const-string v1, "memo"

    invoke-static {p0, v1}, Lcom/samsung/android/provider/memo/MemoDBHelper;->dropIndexesFor(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V

    .line 255
    const-string v1, "DROP INDEX IF EXISTS memo_createdAt;"

    invoke-virtual {p0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 256
    const-string v1, "DROP INDEX IF EXISTS memo_lastModifiedAt;"

    invoke-virtual {p0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 257
    const-string v1, "DROP INDEX IF EXISTS memo_categoryUUID;"

    invoke-virtual {p0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 258
    const-string v1, "DROP TABLE IF EXISTS memo;"

    invoke-virtual {p0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 260
    const-string v1, "file"

    invoke-static {p0, v1}, Lcom/samsung/android/provider/memo/MemoDBHelper;->dropIndexesFor(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V

    .line 261
    const-string v1, "DROP INDEX IF EXISTS file_memoUUID;"

    invoke-virtual {p0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 262
    const-string v1, "DROP TABLE IF EXISTS file;"

    invoke-virtual {p0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 264
    const-string v1, "category"

    invoke-static {p0, v1}, Lcom/samsung/android/provider/memo/MemoDBHelper;->dropIndexesFor(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V

    .line 265
    const-string v1, "DROP INDEX IF EXISTS category_orderBy;"

    invoke-virtual {p0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 266
    const-string v1, "DROP TABLE IF EXISTS category;"

    invoke-virtual {p0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 267
    invoke-static {p0}, Lcom/samsung/android/provider/memo/MemoDBHelper;->dropTriggers(Landroid/database/sqlite/SQLiteDatabase;)V
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 273
    invoke-virtual {p0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V

    .line 274
    invoke-virtual {p0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 276
    return-void

    .line 269
    :catch_0
    move-exception v0

    .line 270
    .local v0, "sqle":Landroid/database/SQLException;
    :try_start_1
    sget-object v1, Lcom/samsung/android/provider/memo/MemoDBHelper;->TAG:Ljava/lang/String;

    const-string v2, "dropTables()"

    invoke-static {v1, v2, v0}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 271
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 272
    .end local v0    # "sqle":Landroid/database/SQLException;
    :catchall_0
    move-exception v1

    .line 273
    invoke-virtual {p0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V

    .line 274
    invoke-virtual {p0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 275
    throw v1
.end method

.method private static dropTriggers(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 3
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/database/SQLException;
        }
    .end annotation

    .prologue
    .line 115
    const-string v0, "DROP TRIGGER IF EXISTS trg_memo_01;"

    .line 116
    .local v0, "query1":Ljava/lang/String;
    const-string v1, "DROP TRIGGER IF EXISTS trg_memo_02;"

    .line 118
    .local v1, "query2":Ljava/lang/String;
    const-string v2, "DROP TRIGGER IF EXISTS trg_memo_01;"

    invoke-virtual {p0, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 119
    const-string v2, "DROP TRIGGER IF EXISTS trg_memo_02;"

    invoke-virtual {p0, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 120
    return-void
.end method

.method private updatePathsToRelative(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 14
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 404
    const/4 v11, 0x0

    .line 406
    .local v11, "memoCursor":Landroid/database/Cursor;
    :try_start_0
    const-string v1, "memo"

    const/4 v0, 0x2

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v3, "UUID"

    aput-object v3, v2, v0

    const/4 v0, 0x1

    const-string v3, "_data"

    aput-object v3, v2, v0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v11

    .line 407
    if-eqz v11, :cond_1

    invoke-interface {v11}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 409
    :cond_0
    const-string v0, "UUID"

    invoke-interface {v11, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v11, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v12

    .line 410
    .local v12, "uuid":Ljava/lang/String;
    const-string v0, "_data"

    invoke-interface {v11, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v11, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 412
    .local v8, "data":Ljava/lang/String;
    new-instance v13, Landroid/content/ContentValues;

    invoke-direct {v13}, Landroid/content/ContentValues;-><init>()V

    .line 413
    .local v13, "values":Landroid/content/ContentValues;
    const-string v0, "_data"

    invoke-static {v8}, Lcom/samsung/android/app/memo/util/Utils;->getRelativePath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v13, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 414
    const-string v0, "memo"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "UUID = \'"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {p1, v0, v13, v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 415
    invoke-interface {v11}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    .line 420
    .end local v8    # "data":Ljava/lang/String;
    .end local v12    # "uuid":Ljava/lang/String;
    .end local v13    # "values":Landroid/content/ContentValues;
    :cond_1
    if-eqz v11, :cond_2

    .line 421
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    .line 423
    :cond_2
    :goto_0
    const/4 v10, 0x0

    .line 425
    .local v10, "fileCursor":Landroid/database/Cursor;
    :try_start_1
    const-string v1, "file"

    const/4 v0, 0x2

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v3, "UUID"

    aput-object v3, v2, v0

    const/4 v0, 0x1

    const-string v3, "_data"

    aput-object v3, v2, v0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    .line 426
    if-eqz v10, :cond_4

    invoke-interface {v10}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 428
    :cond_3
    const-string v0, "UUID"

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v12

    .line 429
    .restart local v12    # "uuid":Ljava/lang/String;
    const-string v0, "_data"

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 431
    .restart local v8    # "data":Ljava/lang/String;
    new-instance v13, Landroid/content/ContentValues;

    invoke-direct {v13}, Landroid/content/ContentValues;-><init>()V

    .line 432
    .restart local v13    # "values":Landroid/content/ContentValues;
    const-string v0, "_data"

    invoke-static {v8}, Lcom/samsung/android/app/memo/util/Utils;->getRelativePath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v13, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 433
    const-string v0, "file"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "UUID = \'"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {p1, v0, v13, v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 434
    invoke-interface {v10}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_1
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v0

    if-nez v0, :cond_3

    .line 439
    .end local v8    # "data":Ljava/lang/String;
    .end local v12    # "uuid":Ljava/lang/String;
    .end local v13    # "values":Landroid/content/ContentValues;
    :cond_4
    if-eqz v10, :cond_5

    .line 440
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    .line 442
    :cond_5
    :goto_1
    return-void

    .line 417
    .end local v10    # "fileCursor":Landroid/database/Cursor;
    :catch_0
    move-exception v9

    .line 418
    .local v9, "e":Landroid/database/sqlite/SQLiteException;
    :try_start_2
    sget-object v0, Lcom/samsung/android/provider/memo/MemoDBHelper;->TAG:Ljava/lang/String;

    const-string v1, "Exception occurred in updatePathsToRelative() in memoCursor."

    invoke-static {v0, v1, v9}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 420
    if-eqz v11, :cond_2

    .line 421
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 419
    .end local v9    # "e":Landroid/database/sqlite/SQLiteException;
    :catchall_0
    move-exception v0

    .line 420
    if-eqz v11, :cond_6

    .line 421
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    .line 422
    :cond_6
    throw v0

    .line 436
    .restart local v10    # "fileCursor":Landroid/database/Cursor;
    :catch_1
    move-exception v9

    .line 437
    .restart local v9    # "e":Landroid/database/sqlite/SQLiteException;
    :try_start_3
    sget-object v0, Lcom/samsung/android/provider/memo/MemoDBHelper;->TAG:Ljava/lang/String;

    const-string v1, "Exception occurred in updatePathsToRelative() in fileCursor."

    invoke-static {v0, v1, v9}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 439
    if-eqz v10, :cond_5

    .line 440
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    goto :goto_1

    .line 438
    .end local v9    # "e":Landroid/database/sqlite/SQLiteException;
    :catchall_1
    move-exception v0

    .line 439
    if-eqz v10, :cond_7

    .line 440
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    .line 441
    :cond_7
    throw v0
.end method

.method private updateTimestampsToUTC(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 21
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 374
    const/16 v16, 0x0

    .line 376
    .local v16, "memoCursor":Landroid/database/Cursor;
    :try_start_0
    const-string v3, "memo"

    const/4 v2, 0x3

    new-array v4, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v5, "UUID"

    aput-object v5, v4, v2

    const/4 v2, 0x1

    const-string v5, "createdAt"

    aput-object v5, v4, v2

    const/4 v2, 0x2

    const-string v5, "lastModifiedAt"

    aput-object v5, v4, v2

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v16

    .line 377
    if-eqz v16, :cond_1

    invoke-interface/range {v16 .. v16}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 379
    :cond_0
    const-string v2, "UUID"

    move-object/from16 v0, v16

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, v16

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v17

    .line 380
    .local v17, "uuid":Ljava/lang/String;
    const-string v2, "createdAt"

    move-object/from16 v0, v16

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, v16

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v12

    .line 381
    .local v12, "createdAt":J
    const-string v2, "lastModifiedAt"

    move-object/from16 v0, v16

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, v16

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v14

    .line 383
    .local v14, "lastModifiedAt":J
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v10

    .line 384
    .local v10, "calInitial":Ljava/util/Calendar;
    const/16 v2, 0xf

    invoke-virtual {v10, v2}, Ljava/util/Calendar;->get(I)I

    move-result v2

    const/16 v3, 0x10

    invoke-virtual {v10, v3}, Ljava/util/Calendar;->get(I)I

    move-result v3

    add-int/2addr v2, v3

    int-to-long v0, v2

    move-wide/from16 v18, v0

    .line 386
    .local v18, "offsetInitial":J
    add-long v12, v12, v18

    .line 387
    add-long v14, v14, v18

    .line 389
    new-instance v20, Landroid/content/ContentValues;

    invoke-direct/range {v20 .. v20}, Landroid/content/ContentValues;-><init>()V

    .line 390
    .local v20, "values":Landroid/content/ContentValues;
    const-string v2, "createdAt"

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    move-object/from16 v0, v20

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 391
    const-string v2, "lastModifiedAt"

    invoke-static {v14, v15}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    move-object/from16 v0, v20

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 392
    const-string v2, "memo"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "UUID = \'"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v17

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-virtual {v0, v2, v1, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 393
    invoke-interface/range {v16 .. v16}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    if-nez v2, :cond_0

    .line 398
    .end local v10    # "calInitial":Ljava/util/Calendar;
    .end local v12    # "createdAt":J
    .end local v14    # "lastModifiedAt":J
    .end local v17    # "uuid":Ljava/lang/String;
    .end local v18    # "offsetInitial":J
    .end local v20    # "values":Landroid/content/ContentValues;
    :cond_1
    if-eqz v16, :cond_2

    .line 399
    invoke-interface/range {v16 .. v16}, Landroid/database/Cursor;->close()V

    .line 401
    :cond_2
    :goto_0
    return-void

    .line 395
    :catch_0
    move-exception v11

    .line 396
    .local v11, "e":Landroid/database/sqlite/SQLiteException;
    :try_start_1
    sget-object v2, Lcom/samsung/android/provider/memo/MemoDBHelper;->TAG:Ljava/lang/String;

    const-string v3, "Exception occurred in updateTimestampsToUTC()."

    invoke-static {v2, v3, v11}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 398
    if-eqz v16, :cond_2

    .line 399
    invoke-interface/range {v16 .. v16}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 397
    .end local v11    # "e":Landroid/database/sqlite/SQLiteException;
    :catchall_0
    move-exception v2

    .line 398
    if-eqz v16, :cond_3

    .line 399
    invoke-interface/range {v16 .. v16}, Landroid/database/Cursor;->close()V

    .line 400
    :cond_3
    throw v2
.end method


# virtual methods
.method public onCreate(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 2
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 285
    sget-object v0, Lcom/samsung/android/provider/memo/MemoDBHelper;->TAG:Ljava/lang/String;

    const-string v1, "onCreate()"

    invoke-static {v0, v1}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 286
    invoke-static {p1}, Lcom/samsung/android/provider/memo/MemoDBHelper;->createTables(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 287
    return-void
.end method

.method public onDowngrade(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 3
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "oldVersion"    # I
    .param p3, "newVersion"    # I

    .prologue
    .line 446
    sget-object v0, Lcom/samsung/android/provider/memo/MemoDBHelper;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onDowngrade() "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " to "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 447
    invoke-static {p1}, Lcom/samsung/android/provider/memo/MemoDBHelper;->dropTables(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 448
    invoke-static {p1}, Lcom/samsung/android/provider/memo/MemoDBHelper;->createTables(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 449
    return-void
.end method

.method public onUpgrade(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 4
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "oldVersion"    # I
    .param p3, "newVersion"    # I

    .prologue
    const/16 v3, 0x9

    .line 291
    sget-object v0, Lcom/samsung/android/provider/memo/MemoDBHelper;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onUpgrade() "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " to "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 292
    if-eq p3, v3, :cond_0

    .line 293
    new-instance v0, Landroid/database/sqlite/SQLiteException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "unknown database version: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/database/sqlite/SQLiteException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 295
    :cond_0
    invoke-static {}, Lcom/samsung/android/app/memo/util/Utils;->isMondrianOrPicassoInProduction()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 296
    packed-switch p2, :pswitch_data_0

    .line 318
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Converting from "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " to "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " wasn\'t implemented."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 298
    :pswitch_0
    const/4 v0, 0x6

    if-eq p3, v0, :cond_1

    const/4 v0, 0x7

    if-ne p3, v0, :cond_2

    .line 299
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Abnormal DB upgrade case. Converting from "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " to "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " should not occur."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 300
    :cond_2
    invoke-direct {p0, p1}, Lcom/samsung/android/provider/memo/MemoDBHelper;->alterDBtoVersion8(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 301
    const/16 v0, 0x8

    if-ne p3, v0, :cond_3

    .line 330
    :goto_0
    return-void

    .line 306
    :cond_3
    :pswitch_1
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Abnormal DB upgrade case. Converting from "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " to "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " should not occur."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 310
    :pswitch_2
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Abnormal DB upgrade case. Converting from "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " to "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " should not occur."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 312
    :pswitch_3
    if-ne p3, v3, :cond_4

    .line 313
    invoke-direct {p0, p1}, Lcom/samsung/android/provider/memo/MemoDBHelper;->alterDBtoVersion9(Landroid/database/sqlite/SQLiteDatabase;)V

    goto :goto_0

    .line 316
    :cond_4
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Abnormal DB upgrade case. Converting from "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " to "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " should not occur."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 321
    :cond_5
    packed-switch p2, :pswitch_data_1

    goto :goto_0

    .line 323
    :pswitch_4
    if-ne p3, v3, :cond_6

    .line 324
    invoke-direct {p0, p1}, Lcom/samsung/android/provider/memo/MemoDBHelper;->alterDBtoVersion9(Landroid/database/sqlite/SQLiteDatabase;)V

    goto :goto_0

    .line 326
    :cond_6
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Abnormal DB upgrade case. Converting from "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " to "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " should not occur."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 296
    :pswitch_data_0
    .packed-switch 0x5
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch

    .line 321
    :pswitch_data_1
    .packed-switch 0x8
        :pswitch_4
    .end packed-switch
.end method
