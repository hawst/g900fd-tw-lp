.class public final Lcom/samsung/android/provider/memo/MemoContract;
.super Ljava/lang/Object;
.source "MemoContract.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/provider/memo/MemoContract$Tbl_Base;,
        Lcom/samsung/android/provider/memo/MemoContract$Tbl_Category;,
        Lcom/samsung/android/provider/memo/MemoContract$Tbl_File;,
        Lcom/samsung/android/provider/memo/MemoContract$Tbl_Memo;,
        Lcom/samsung/android/provider/memo/MemoContract$Tbl_Search;,
        Lcom/samsung/android/provider/memo/MemoContract$Tbl_SyncState;
    }
.end annotation


# static fields
.field public static final AUTHORITY:Ljava/lang/String; = "com.samsung.android.memo"

.field public static final AUTHORITY_URI:Landroid/net/Uri;

.field public static final BASE_URI_CATEGORY:Landroid/net/Uri;

.field public static final BASE_URI_CATEGORY_UNDO:Landroid/net/Uri;

.field public static final BASE_URI_CATEGORY_UNDOABLE:Landroid/net/Uri;

.field public static final BASE_URI_CLIPBOARD:Landroid/net/Uri;

.field public static final BASE_URI_FILE:Landroid/net/Uri;

.field public static final BASE_URI_FILE_UNDO:Landroid/net/Uri;

.field public static final BASE_URI_FILE_UNDOABLE:Landroid/net/Uri;

.field public static final BASE_URI_MEMO:Landroid/net/Uri;

.field public static final BASE_URI_MEMO_UNDO:Landroid/net/Uri;

.field public static final BASE_URI_MEMO_UNDOABLE:Landroid/net/Uri;

.field public static final BASE_URI_SEARCH:Landroid/net/Uri;

.field public static final BASE_URI_SYNCSTATE:Landroid/net/Uri;

.field public static final CATEGORY_UUID_INCALL:Ljava/lang/String;

.field public static final CLIPBOARD_SAVE_MEMO:Ljava/lang/String; = "save_from_clipboard"

.field public static final DB_VERSION:I = 0x9

.field public static final DEFAULT_ORIENTATION:I = 0x0

.field public static final FRAGMENT_FORCE:Ljava/lang/String; = "force"

.field public static final FRAGMENT_UNDO:Ljava/lang/String; = "undo"

.field public static final FRAGMENT_UNDOABLE:Ljava/lang/String; = "undoable"

.field public static final GOOGLE_SEARCH_SUGGESTION_QUERY:Ljava/lang/String; = "search_suggest_query"

.field public static final ID:Ljava/lang/String; = "memo_id"

.field public static final INVALID_ID:J = -0x1L

.field public static final MIME_TYPE_MP4:Ljava/lang/String; = "audio/mp4"

.field public static final MIME_TYPE_TEXT:Ljava/lang/String; = "text/plain"

.field public static final MIME_TYPE_UNKNOWN:Ljava/lang/String; = "application/octet-stream"

.field public static final MP4_EXTENSION:Ljava/lang/String; = ".m4a"

.field public static final NO:I = 0x0

.field public static final SAMSUNG_SEARCH_REGEX_QUERY:Ljava/lang/String; = "search_suggest_regex_query"

.field private static final TAG:Ljava/lang/String;

.field public static final UUID_NONE:Ljava/lang/String; = ""

.field public static final YES:I = 0x1


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 35
    const-class v0, Lcom/samsung/android/provider/memo/MemoContract;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/provider/memo/MemoContract;->TAG:Ljava/lang/String;

    .line 94
    const-string v0, "content://com.samsung.android.memo"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/provider/memo/MemoContract;->AUTHORITY_URI:Landroid/net/Uri;

    .line 98
    sget-object v0, Lcom/samsung/android/provider/memo/MemoContract;->AUTHORITY_URI:Landroid/net/Uri;

    const-string v1, "search"

    invoke-static {v0, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/provider/memo/MemoContract;->BASE_URI_SEARCH:Landroid/net/Uri;

    .line 102
    sget-object v0, Lcom/samsung/android/provider/memo/MemoContract;->AUTHORITY_URI:Landroid/net/Uri;

    const-string v1, "memo"

    invoke-static {v0, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/provider/memo/MemoContract;->BASE_URI_MEMO:Landroid/net/Uri;

    .line 106
    sget-object v0, Lcom/samsung/android/provider/memo/MemoContract;->BASE_URI_MEMO:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "undoable"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->fragment(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/provider/memo/MemoContract;->BASE_URI_MEMO_UNDOABLE:Landroid/net/Uri;

    .line 110
    sget-object v0, Lcom/samsung/android/provider/memo/MemoContract;->BASE_URI_MEMO:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "undo"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->fragment(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/provider/memo/MemoContract;->BASE_URI_MEMO_UNDO:Landroid/net/Uri;

    .line 114
    sget-object v0, Lcom/samsung/android/provider/memo/MemoContract;->AUTHORITY_URI:Landroid/net/Uri;

    const-string v1, "file"

    invoke-static {v0, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/provider/memo/MemoContract;->BASE_URI_FILE:Landroid/net/Uri;

    .line 118
    sget-object v0, Lcom/samsung/android/provider/memo/MemoContract;->BASE_URI_FILE:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "undoable"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->fragment(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/provider/memo/MemoContract;->BASE_URI_FILE_UNDOABLE:Landroid/net/Uri;

    .line 122
    sget-object v0, Lcom/samsung/android/provider/memo/MemoContract;->BASE_URI_FILE:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "undo"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->fragment(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/provider/memo/MemoContract;->BASE_URI_FILE_UNDO:Landroid/net/Uri;

    .line 126
    sget-object v0, Lcom/samsung/android/provider/memo/MemoContract;->AUTHORITY_URI:Landroid/net/Uri;

    const-string v1, "category"

    invoke-static {v0, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/provider/memo/MemoContract;->BASE_URI_CATEGORY:Landroid/net/Uri;

    .line 130
    sget-object v0, Lcom/samsung/android/provider/memo/MemoContract;->BASE_URI_CATEGORY:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "undoable"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->fragment(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/provider/memo/MemoContract;->BASE_URI_CATEGORY_UNDOABLE:Landroid/net/Uri;

    .line 134
    sget-object v0, Lcom/samsung/android/provider/memo/MemoContract;->BASE_URI_CATEGORY:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "undo"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->fragment(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/provider/memo/MemoContract;->BASE_URI_CATEGORY_UNDO:Landroid/net/Uri;

    .line 138
    sget-object v0, Lcom/samsung/android/provider/memo/MemoContract;->AUTHORITY_URI:Landroid/net/Uri;

    const-string v1, "save_from_clipboard"

    invoke-static {v0, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/provider/memo/MemoContract;->BASE_URI_CLIPBOARD:Landroid/net/Uri;

    .line 142
    sget-object v0, Lcom/samsung/android/provider/memo/MemoContract;->AUTHORITY_URI:Landroid/net/Uri;

    const-string v1, "_sync_state"

    invoke-static {v0, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/provider/memo/MemoContract;->BASE_URI_SYNCSTATE:Landroid/net/Uri;

    .line 175
    const-wide/16 v0, 0x3

    invoke-static {v0, v1}, Lcom/samsung/android/app/memo/util/UUIDHelper;->getUUIDforDefaultCategory(J)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/provider/memo/MemoContract;->CATEGORY_UUID_INCALL:Ljava/lang/String;

    .line 204
    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    .line 433
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 434
    sget-object v0, Lcom/samsung/android/provider/memo/MemoContract;->TAG:Ljava/lang/String;

    sget-object v1, Lcom/samsung/android/provider/memo/MemoContract;->TAG:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 435
    return-void
.end method
