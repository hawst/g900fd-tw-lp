.class public interface abstract Lcom/samsung/android/provider/memo/MemoContract$Tbl_Memo;
.super Ljava/lang/Object;
.source "MemoContract.java"

# interfaces
.implements Lcom/samsung/android/provider/memo/MemoContract$Tbl_Base;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/provider/memo/MemoContract;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Tbl_Memo"
.end annotation


# static fields
.field public static final CATEGORY_UUID:Ljava/lang/String; = "categoryUUID"

.field public static final CONTENT:Ljava/lang/String; = "content"

.field public static final CREATED_AT:Ljava/lang/String; = "createdAt"

.field public static final DATA:Ljava/lang/String; = "_data"

.field public static final LAST_MODIFIED_AT:Ljava/lang/String; = "lastModifiedAt"

.field public static final MIME_TYPE_ALL:Ljava/lang/String; = "vnd.android.cursor.dir/vnd.samsung.memo"

.field public static final MIME_TYPE_ONE:Ljava/lang/String; = "vnd.android.cursor.item/vnd.samsung.memo"

.field public static final PHONE_NUM:Ljava/lang/String; = "_phoneNum"

.field public static final STRIPPED_CONTENT:Ljava/lang/String; = "strippedContent"

.field public static final TABLE_NAME:Ljava/lang/String; = "memo"

.field public static final TITLE:Ljava/lang/String; = "title"

.field public static final VR_FILE_UUID:Ljava/lang/String; = "vrfileUUID"

.field public static final VR_FILE_lower:Ljava/lang/String; = "vrfileuuid"
