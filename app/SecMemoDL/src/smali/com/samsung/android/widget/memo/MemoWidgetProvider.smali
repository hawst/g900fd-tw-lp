.class public Lcom/samsung/android/widget/memo/MemoWidgetProvider;
.super Landroid/appwidget/AppWidgetProvider;
.source "MemoWidgetProvider.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/widget/memo/MemoWidgetProvider$WidgetReceiver;
    }
.end annotation


# static fields
.field public static final ACTION_MEMO_DELETE:Ljava/lang/String; = "com.sec.android.widgetapp.MEMO_DELETE"

.field public static final ACTION_MEMO_SAVE:Ljava/lang/String; = "com.sec.android.widgetapp.MEMO_SAVE"

.field public static final ACTION_UPDATE_ALL_WIDGET:Ljava/lang/String; = "com.samsung.android.widget.memo.UPDATE_ALL_WIDGET"

.field public static final ACTION_UPDATE_SELECTED:Ljava/lang/String; = "com.samsung.android.widget.memo.UPDATE_SELECTED_WIDGET"

.field public static final ACTION_WIDGET_CLICKED:Ljava/lang/String; = "com.samsung.android.widget.APPWIDGET_CLICKED"

.field public static final ACTION_WIDGET_RESIZE:Ljava/lang/String; = "com.sec.android.widgetapp.APPWIDGET_RESIZE"

.field public static final EXTRA_ITEM:Ljava/lang/String; = "com.samsung.android.widget.EXTRA_ITEM"

.field public static final EXTRA_KEY_FROM_WIDGET:Ljava/lang/String; = "from_widget"

.field public static final EXTRA_KEY_FROM_WIDGET_ID:Ljava/lang/String; = "appWidgetId"

.field public static final KNOX_USER_CHANGED:Ljava/lang/String; = "com.sec.knox.container.INTENT_KNOX_USER_CHANGED"

.field public static final ORIENTATION_CHANGE:Ljava/lang/String; = "com.samsung.android.widget.ORIENTATION_CHANGE"

.field private static final TAG:Ljava/lang/String;

.field public static final WIDGET_MIN_HEIGHT:I = 0x64

.field public static final WIDGET_MIN_HEIGHT_FONBLET:I = 0x96

.field public static final WIDGET_PREFERENCES:Ljava/lang/String; = "widgetPref"

.field private static mIsRegisteredMemoSaveReceiver:Z


# instance fields
.field private mContext:Landroid/content/Context;

.field private wReceiver:Lcom/samsung/android/widget/memo/MemoWidgetProvider$WidgetReceiver;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 52
    const-class v0, Lcom/samsung/android/widget/memo/MemoWidgetProvider;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/widget/memo/MemoWidgetProvider;->TAG:Ljava/lang/String;

    .line 86
    const/4 v0, 0x0

    sput-boolean v0, Lcom/samsung/android/widget/memo/MemoWidgetProvider;->mIsRegisteredMemoSaveReceiver:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 50
    invoke-direct {p0}, Landroid/appwidget/AppWidgetProvider;-><init>()V

    .line 84
    new-instance v0, Lcom/samsung/android/widget/memo/MemoWidgetProvider$WidgetReceiver;

    invoke-direct {v0, p0}, Lcom/samsung/android/widget/memo/MemoWidgetProvider$WidgetReceiver;-><init>(Lcom/samsung/android/widget/memo/MemoWidgetProvider;)V

    iput-object v0, p0, Lcom/samsung/android/widget/memo/MemoWidgetProvider;->wReceiver:Lcom/samsung/android/widget/memo/MemoWidgetProvider$WidgetReceiver;

    .line 50
    return-void
.end method

.method static synthetic access$0()Ljava/lang/String;
    .locals 1

    .prologue
    .line 52
    sget-object v0, Lcom/samsung/android/widget/memo/MemoWidgetProvider;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1(Lcom/samsung/android/widget/memo/MemoWidgetProvider;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 82
    iput-object p1, p0, Lcom/samsung/android/widget/memo/MemoWidgetProvider;->mContext:Landroid/content/Context;

    return-void
.end method

.method static synthetic access$2(Lcom/samsung/android/widget/memo/MemoWidgetProvider;Ljava/lang/String;J)V
    .locals 0

    .prologue
    .line 188
    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/android/widget/memo/MemoWidgetProvider;->saveWidgetPreferences(Ljava/lang/String;J)V

    return-void
.end method

.method static synthetic access$3(Lcom/samsung/android/widget/memo/MemoWidgetProvider;Landroid/content/Context;J)V
    .locals 0

    .prologue
    .line 360
    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/android/widget/memo/MemoWidgetProvider;->updateWidget(Landroid/content/Context;J)V

    return-void
.end method

.method private checkForVoiceMemo(Landroid/widget/RemoteViews;I)V
    .locals 17
    .param p1, "rv"    # Landroid/widget/RemoteViews;
    .param p2, "mAppWidgetId"    # I

    .prologue
    .line 292
    const-wide/16 v8, -0x1

    .line 293
    .local v8, "id":J
    const/4 v10, 0x0

    .line 296
    .local v10, "mCursor":Landroid/database/Cursor;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/widget/memo/MemoWidgetProvider;->mContext:Landroid/content/Context;

    const-string v3, "widgetPref"

    .line 297
    const/4 v4, 0x0

    .line 296
    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v16

    .line 298
    .local v16, "widgetPref":Landroid/content/SharedPreferences;
    invoke-static/range {p2 .. p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    const-wide/16 v4, -0x1

    move-object/from16 v0, v16

    invoke-interface {v0, v2, v4, v5}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v8

    .line 300
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/widget/memo/MemoWidgetProvider;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Lcom/samsung/android/provider/memo/MemoContract;->BASE_URI_MEMO:Landroid/net/Uri;

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    .line 301
    const-string v6, "content"

    aput-object v6, v4, v5

    const/4 v5, 0x1

    const-string v6, "title"

    aput-object v6, v4, v5

    const/4 v5, 0x2

    const-string v6, "vrfileUUID"

    aput-object v6, v4, v5

    .line 302
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "_id="

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    .line 300
    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    .line 304
    if-eqz v10, :cond_3

    invoke-interface {v10}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 305
    const/4 v2, 0x0

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    .line 306
    .local v11, "mStrBody":Ljava/lang/String;
    const-string v2, "title"

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v13

    .line 307
    .local v13, "titlepos":I
    invoke-interface {v10, v13}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v12

    .line 308
    .local v12, "mTitle":Ljava/lang/String;
    const-string v2, "vrfileUUID"

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v15

    .line 309
    .local v15, "voiceMemopos":I
    invoke-interface {v10, v15}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v14

    .line 310
    .local v14, "voiceMemo":Ljava/lang/String;
    const-string v2, ""

    invoke-virtual {v14, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 311
    const v2, 0x7f0e008b

    const/4 v3, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 312
    invoke-virtual {v11}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {v12}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 313
    const v2, 0x7f0e008a

    const/4 v3, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 324
    .end local v11    # "mStrBody":Ljava/lang/String;
    .end local v12    # "mTitle":Ljava/lang/String;
    .end local v13    # "titlepos":I
    .end local v14    # "voiceMemo":Ljava/lang/String;
    .end local v15    # "voiceMemopos":I
    :goto_0
    if-eqz v10, :cond_0

    .line 325
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    .line 326
    :cond_0
    return-void

    .line 315
    .restart local v11    # "mStrBody":Ljava/lang/String;
    .restart local v12    # "mTitle":Ljava/lang/String;
    .restart local v13    # "titlepos":I
    .restart local v14    # "voiceMemo":Ljava/lang/String;
    .restart local v15    # "voiceMemopos":I
    :cond_1
    const v2, 0x7f0e008a

    const/16 v3, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    goto :goto_0

    .line 317
    :cond_2
    const v2, 0x7f0e008b

    const/16 v3, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 318
    const v2, 0x7f0e008a

    const/16 v3, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    goto :goto_0

    .line 321
    .end local v11    # "mStrBody":Ljava/lang/String;
    .end local v12    # "mTitle":Ljava/lang/String;
    .end local v13    # "titlepos":I
    .end local v14    # "voiceMemo":Ljava/lang/String;
    .end local v15    # "voiceMemopos":I
    :cond_3
    const v2, 0x7f0e008b

    const/16 v3, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 322
    const v2, 0x7f0e008a

    const/16 v3, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    goto :goto_0
.end method

.method private createMemoIntent(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 197
    const/4 v6, 0x1

    invoke-static {v6}, Lcom/samsung/android/app/memo/MemoApp;->setIsSaveOnDetach(Z)V

    .line 199
    const-string v6, "appWidgetId"

    const/4 v7, -0x1

    invoke-virtual {p2, v6, v7}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v6

    int-to-long v4, v6

    .line 200
    .local v4, "widgetId":J
    const-wide/16 v6, 0x0

    cmp-long v6, v4, v6

    if-gez v6, :cond_0

    .line 227
    :goto_0
    return-void

    .line 202
    :cond_0
    const-wide/16 v2, -0x1

    .line 203
    .local v2, "memoid":J
    invoke-direct {p0, v4, v5}, Lcom/samsung/android/widget/memo/MemoWidgetProvider;->getMemoIdFromPreferences(J)J

    move-result-wide v2

    .line 205
    const-wide/16 v6, 0x0

    cmp-long v6, v2, v6

    if-gez v6, :cond_2

    .line 206
    new-instance v0, Landroid/content/Intent;

    const-string v6, "android.intent.action.PICK"

    invoke-direct {v0, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 207
    .local v0, "memoIntent":Landroid/content/Intent;
    const v6, 0x10008000

    invoke-virtual {v0, v6}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 208
    sget-boolean v6, Lcom/samsung/android/app/memo/util/Utils;->IS_MULTIWINDOW_DEVICE:Z

    if-eqz v6, :cond_1

    .line 209
    new-instance v6, Lcom/samsung/android/multiwindow/MultiWindowStyle;

    const/4 v7, 0x0

    invoke-direct {v6, v7}, Lcom/samsung/android/multiwindow/MultiWindowStyle;-><init>(I)V

    invoke-virtual {v0, v6}, Landroid/content/Intent;->setMultiWindowStyle(Lcom/samsung/android/multiwindow/MultiWindowStyle;)V

    .line 210
    :cond_1
    const-string v6, "com.samsung.android.app.memo.Main"

    invoke-virtual {v0, p1, v6}, Landroid/content/Intent;->setClassName(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    .line 211
    const-string v6, "from_widget"

    const/4 v7, 0x1

    invoke-virtual {v0, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 212
    const-string v6, "appWidgetId"

    invoke-virtual {v0, v6, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 213
    const-string v6, "*/*"

    invoke-virtual {v0, v6}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 214
    invoke-virtual {p1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 216
    .end local v0    # "memoIntent":Landroid/content/Intent;
    :cond_2
    sget-object v6, Lcom/samsung/android/provider/memo/MemoContract;->BASE_URI_MEMO:Landroid/net/Uri;

    invoke-static {v6, v2, v3}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v1

    .line 217
    .local v1, "memoUri":Landroid/net/Uri;
    new-instance v0, Landroid/content/Intent;

    const-string v6, "android.intent.action.VIEW"

    invoke-direct {v0, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 218
    .restart local v0    # "memoIntent":Landroid/content/Intent;
    const v6, 0x10008000

    invoke-virtual {v0, v6}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 219
    sget-boolean v6, Lcom/samsung/android/app/memo/util/Utils;->IS_MULTIWINDOW_DEVICE:Z

    if-eqz v6, :cond_3

    .line 220
    new-instance v6, Lcom/samsung/android/multiwindow/MultiWindowStyle;

    const/4 v7, 0x0

    invoke-direct {v6, v7}, Lcom/samsung/android/multiwindow/MultiWindowStyle;-><init>(I)V

    invoke-virtual {v0, v6}, Landroid/content/Intent;->setMultiWindowStyle(Lcom/samsung/android/multiwindow/MultiWindowStyle;)V

    .line 221
    :cond_3
    const-string v6, "from_widget"

    const/4 v7, 0x1

    invoke-virtual {v0, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 222
    const-string v6, "appWidgetId"

    invoke-virtual {v0, v6, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 223
    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 224
    const-string v6, "com.samsung.android.app.memo.Main"

    invoke-virtual {v0, p1, v6}, Landroid/content/Intent;->setClassName(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    .line 225
    invoke-virtual {p1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method private getCellsNumber(I)I
    .locals 3
    .param p1, "height"    # I

    .prologue
    .line 381
    div-int/lit8 v0, p1, 0x64

    iget-object v1, p0, Lcom/samsung/android/widget/memo/MemoWidgetProvider;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0018

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    div-int/lit8 v1, v1, 0x64

    if-ne v0, v1, :cond_0

    .line 382
    const/4 v0, 0x1

    .line 394
    :goto_0
    return v0

    .line 383
    :cond_0
    div-int/lit8 v0, p1, 0x64

    iget-object v1, p0, Lcom/samsung/android/widget/memo/MemoWidgetProvider;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0019

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    div-int/lit8 v1, v1, 0x64

    if-ne v0, v1, :cond_1

    .line 384
    const/4 v0, 0x2

    goto :goto_0

    .line 385
    :cond_1
    div-int/lit8 v0, p1, 0x64

    iget-object v1, p0, Lcom/samsung/android/widget/memo/MemoWidgetProvider;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a001a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    div-int/lit8 v1, v1, 0x64

    if-ne v0, v1, :cond_2

    .line 386
    const/4 v0, 0x3

    goto :goto_0

    .line 387
    :cond_2
    div-int/lit8 v0, p1, 0x64

    iget-object v1, p0, Lcom/samsung/android/widget/memo/MemoWidgetProvider;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a001b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    div-int/lit8 v1, v1, 0x64

    if-ne v0, v1, :cond_3

    .line 388
    const/4 v0, 0x4

    goto :goto_0

    .line 389
    :cond_3
    div-int/lit8 v0, p1, 0x64

    iget-object v1, p0, Lcom/samsung/android/widget/memo/MemoWidgetProvider;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a001c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    div-int/lit8 v1, v1, 0x64

    if-ne v0, v1, :cond_4

    .line 390
    const/4 v0, 0x5

    goto :goto_0

    .line 391
    :cond_4
    div-int/lit8 v0, p1, 0x64

    iget-object v1, p0, Lcom/samsung/android/widget/memo/MemoWidgetProvider;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a001d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    div-int/lit8 v1, v1, 0x64

    if-ne v0, v1, :cond_5

    .line 392
    const/4 v0, 0x6

    goto :goto_0

    .line 394
    :cond_5
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private getMemoIdFromPreferences(J)J
    .locals 7
    .param p1, "widgetID"    # J

    .prologue
    .line 181
    iget-object v3, p0, Lcom/samsung/android/widget/memo/MemoWidgetProvider;->mContext:Landroid/content/Context;

    const-string v4, "widgetPref"

    .line 182
    const/4 v5, 0x0

    .line 181
    invoke-virtual {v3, v4, v5}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    .line 183
    .local v2, "widgetPref":Landroid/content/SharedPreferences;
    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    const-wide/16 v4, -0x1

    invoke-interface {v2, v3, v4, v5}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    .line 184
    .local v0, "id":J
    return-wide v0
.end method

.method private removeWidgetCells(J)V
    .locals 5
    .param p1, "widgetId"    # J

    .prologue
    .line 374
    iget-object v2, p0, Lcom/samsung/android/widget/memo/MemoWidgetProvider;->mContext:Landroid/content/Context;

    const-string v3, "widgetPref"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 375
    .local v1, "widgetPref":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 376
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "cells"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 377
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 378
    return-void
.end method

.method private saveWidgetCells(II)V
    .locals 5
    .param p1, "widgetId"    # I
    .param p2, "cells"    # I

    .prologue
    .line 367
    iget-object v2, p0, Lcom/samsung/android/widget/memo/MemoWidgetProvider;->mContext:Landroid/content/Context;

    const-string v3, "widgetPref"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 368
    .local v1, "widgetPref":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 369
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "cells"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2, p2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 370
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 371
    return-void
.end method

.method private saveWidgetPreferences(Ljava/lang/String;J)V
    .locals 6
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "id"    # J

    .prologue
    .line 189
    iget-object v2, p0, Lcom/samsung/android/widget/memo/MemoWidgetProvider;->mContext:Landroid/content/Context;

    const-string v3, "widgetPref"

    .line 190
    const/4 v4, 0x0

    .line 189
    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 191
    .local v1, "widgetPref":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 192
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    invoke-interface {v0, p1, p2, p3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 193
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 194
    return-void
.end method

.method private updateAllWidget(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 341
    invoke-static {p1}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v0

    .line 342
    .local v0, "manager":Landroid/appwidget/AppWidgetManager;
    new-instance v1, Landroid/content/ComponentName;

    const-class v3, Lcom/samsung/android/widget/memo/MemoWidgetProvider;

    invoke-direct {v1, p1, v3}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 343
    .local v1, "provider":Landroid/content/ComponentName;
    invoke-virtual {v0, v1}, Landroid/appwidget/AppWidgetManager;->getAppWidgetIds(Landroid/content/ComponentName;)[I

    move-result-object v2

    .line 344
    .local v2, "wIds":[I
    invoke-virtual {p0, p1, v0, v2}, Lcom/samsung/android/widget/memo/MemoWidgetProvider;->onUpdate(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;[I)V

    .line 345
    return-void
.end method

.method private updateSelectedWidgets(Landroid/content/Context;J)[I
    .locals 10
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "memoID"    # J

    .prologue
    .line 348
    invoke-static {p1}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v3

    .line 349
    .local v3, "manager":Landroid/appwidget/AppWidgetManager;
    new-instance v4, Landroid/content/ComponentName;

    const-class v7, Lcom/samsung/android/widget/memo/MemoWidgetProvider;

    invoke-direct {v4, p1, v7}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 350
    .local v4, "provider":Landroid/content/ComponentName;
    invoke-virtual {v3, v4}, Landroid/appwidget/AppWidgetManager;->getAppWidgetIds(Landroid/content/ComponentName;)[I

    move-result-object v6

    .line 351
    .local v6, "wIds":[I
    array-length v7, v6

    new-array v5, v7, [I

    .line 352
    .local v5, "updateWIds":[I
    const/4 v0, 0x0

    .line 353
    .local v0, "count":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    array-length v7, v6

    if-lt v2, v7, :cond_0

    .line 357
    return-object v5

    .line 354
    :cond_0
    aget v7, v6, v2

    int-to-long v8, v7

    invoke-direct {p0, v8, v9}, Lcom/samsung/android/widget/memo/MemoWidgetProvider;->getMemoIdFromPreferences(J)J

    move-result-wide v8

    cmp-long v7, v8, p2

    if-nez v7, :cond_1

    .line 355
    add-int/lit8 v1, v0, 0x1

    .end local v0    # "count":I
    .local v1, "count":I
    aget v7, v6, v2

    aput v7, v5, v0

    move v0, v1

    .line 353
    .end local v1    # "count":I
    .restart local v0    # "count":I
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method private updateWidget(Landroid/content/Context;J)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "widgetId"    # J

    .prologue
    .line 361
    invoke-static {p1}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v0

    .line 362
    .local v0, "manager":Landroid/appwidget/AppWidgetManager;
    const/4 v2, 0x1

    new-array v1, v2, [I

    const/4 v2, 0x0

    long-to-int v3, p2

    aput v3, v1, v2

    .line 363
    .local v1, "wIds":[I
    invoke-virtual {p0, p1, v0, v1}, Lcom/samsung/android/widget/memo/MemoWidgetProvider;->onUpdate(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;[I)V

    .line 364
    return-void
.end method


# virtual methods
.method public onAppWidgetOptionsChanged(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;ILandroid/os/Bundle;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "appWidgetManager"    # Landroid/appwidget/AppWidgetManager;
    .param p3, "appWidgetId"    # I
    .param p4, "bundle"    # Landroid/os/Bundle;

    .prologue
    .line 330
    invoke-static {p1}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v0

    .line 331
    .local v0, "manager":Landroid/appwidget/AppWidgetManager;
    invoke-virtual {v0, p3}, Landroid/appwidget/AppWidgetManager;->getAppWidgetOptions(I)Landroid/os/Bundle;

    move-result-object v2

    .line 332
    .local v2, "widgetBundle":Landroid/os/Bundle;
    if-eqz v2, :cond_0

    .line 333
    const-string v3, "appWidgetMaxHeight"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v3

    invoke-direct {p0, v3}, Lcom/samsung/android/widget/memo/MemoWidgetProvider;->getCellsNumber(I)I

    move-result v3

    invoke-direct {p0, p3, v3}, Lcom/samsung/android/widget/memo/MemoWidgetProvider;->saveWidgetCells(II)V

    .line 334
    :cond_0
    const/4 v3, 0x1

    new-array v1, v3, [I

    const/4 v3, 0x0

    aput p3, v1, v3

    .line 335
    .local v1, "wIds":[I
    invoke-virtual {p0, p1, v0, v1}, Lcom/samsung/android/widget/memo/MemoWidgetProvider;->onUpdate(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;[I)V

    .line 336
    const v3, 0x7f0e0084

    invoke-virtual {v0, v1, v3}, Landroid/appwidget/AppWidgetManager;->notifyAppWidgetViewDataChanged([II)V

    .line 337
    invoke-super {p0, p1, p2, p3, p4}, Landroid/appwidget/AppWidgetProvider;->onAppWidgetOptionsChanged(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;ILandroid/os/Bundle;)V

    .line 338
    return-void
.end method

.method public onDeleted(Landroid/content/Context;[I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "appWidgetIds"    # [I

    .prologue
    .line 119
    invoke-super {p0, p1, p2}, Landroid/appwidget/AppWidgetProvider;->onDeleted(Landroid/content/Context;[I)V

    .line 120
    return-void
.end method

.method public onDisabled(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 124
    invoke-super {p0, p1}, Landroid/appwidget/AppWidgetProvider;->onDisabled(Landroid/content/Context;)V

    .line 125
    sget-object v0, Lcom/samsung/android/widget/memo/MemoWidgetProvider;->TAG:Ljava/lang/String;

    const-string v1, "onDisabled"

    invoke-static {v0, v1}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 126
    iget-object v0, p0, Lcom/samsung/android/widget/memo/MemoWidgetProvider;->mContext:Landroid/content/Context;

    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lcom/samsung/android/widget/memo/MemoWidgetProvider;->mContext:Landroid/content/Context;

    const-class v3, Lcom/samsung/android/widget/memo/GetDeviceOrientationService;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v0, v1}, Landroid/content/Context;->stopService(Landroid/content/Intent;)Z

    .line 127
    invoke-static {p1}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/widget/memo/MemoWidgetProvider;->wReceiver:Lcom/samsung/android/widget/memo/MemoWidgetProvider$WidgetReceiver;

    invoke-virtual {v0, v1}, Landroid/support/v4/content/LocalBroadcastManager;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 128
    return-void
.end method

.method public onEnabled(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 132
    sget-object v0, Lcom/samsung/android/widget/memo/MemoWidgetProvider;->TAG:Ljava/lang/String;

    const-string v1, "onEnabled"

    invoke-static {v0, v1}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 133
    invoke-super {p0, p1}, Landroid/appwidget/AppWidgetProvider;->onEnabled(Landroid/content/Context;)V

    .line 134
    return-void
.end method

.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 13
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const v12, 0x7f0e0084

    .line 138
    sget-object v7, Lcom/samsung/android/widget/memo/MemoWidgetProvider;->TAG:Ljava/lang/String;

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "onReceive "

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v7, v10}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 140
    sget-boolean v7, Lcom/samsung/android/widget/memo/MemoWidgetProvider;->mIsRegisteredMemoSaveReceiver:Z

    if-nez v7, :cond_0

    .line 141
    sget-object v7, Lcom/samsung/android/widget/memo/MemoWidgetProvider;->TAG:Ljava/lang/String;

    const-string v10, "Local receiver registered"

    invoke-static {v7, v10}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 142
    new-instance v4, Landroid/content/IntentFilter;

    invoke-direct {v4}, Landroid/content/IntentFilter;-><init>()V

    .line 143
    .local v4, "localFilter":Landroid/content/IntentFilter;
    const-string v7, "com.sec.android.widgetapp.MEMO_SAVE"

    invoke-virtual {v4, v7}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 144
    iget-object v7, p0, Lcom/samsung/android/widget/memo/MemoWidgetProvider;->wReceiver:Lcom/samsung/android/widget/memo/MemoWidgetProvider$WidgetReceiver;

    invoke-virtual {v7, p0}, Lcom/samsung/android/widget/memo/MemoWidgetProvider$WidgetReceiver;->setProvider(Lcom/samsung/android/widget/memo/MemoWidgetProvider;)V

    .line 145
    invoke-static {p1}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v7

    iget-object v10, p0, Lcom/samsung/android/widget/memo/MemoWidgetProvider;->wReceiver:Lcom/samsung/android/widget/memo/MemoWidgetProvider$WidgetReceiver;

    invoke-virtual {v7, v10, v4}, Landroid/support/v4/content/LocalBroadcastManager;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)V

    .line 146
    const/4 v7, 0x1

    sput-boolean v7, Lcom/samsung/android/widget/memo/MemoWidgetProvider;->mIsRegisteredMemoSaveReceiver:Z

    .line 149
    .end local v4    # "localFilter":Landroid/content/IntentFilter;
    :cond_0
    iput-object p1, p0, Lcom/samsung/android/widget/memo/MemoWidgetProvider;->mContext:Landroid/content/Context;

    .line 150
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 152
    .local v0, "action":Ljava/lang/String;
    const-string v7, "com.samsung.android.widget.APPWIDGET_CLICKED"

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 153
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/widget/memo/MemoWidgetProvider;->createMemoIntent(Landroid/content/Context;Landroid/content/Intent;)V

    .line 176
    :cond_1
    :goto_0
    invoke-super {p0, p1, p2}, Landroid/appwidget/AppWidgetProvider;->onReceive(Landroid/content/Context;Landroid/content/Intent;)V

    .line 177
    return-void

    .line 154
    :cond_2
    const-string v7, "android.appwidget.action.APPWIDGET_UPDATE"

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 155
    iget-object v7, p0, Lcom/samsung/android/widget/memo/MemoWidgetProvider;->mContext:Landroid/content/Context;

    new-instance v10, Landroid/content/Intent;

    iget-object v11, p0, Lcom/samsung/android/widget/memo/MemoWidgetProvider;->mContext:Landroid/content/Context;

    const-class v12, Lcom/samsung/android/widget/memo/GetDeviceOrientationService;

    invoke-direct {v10, v11, v12}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v7, v10}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    goto :goto_0

    .line 157
    :cond_3
    const-string v7, "com.samsung.android.widget.memo.UPDATE_SELECTED_WIDGET"

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 158
    const-string v7, "memo_id"

    .line 159
    const-wide/16 v10, -0x1

    .line 158
    invoke-virtual {p2, v7, v10, v11}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v2

    .line 160
    .local v2, "id":J
    invoke-direct {p0, p1, v2, v3}, Lcom/samsung/android/widget/memo/MemoWidgetProvider;->updateSelectedWidgets(Landroid/content/Context;J)[I

    move-result-object v6

    .line 161
    .local v6, "updateWIds":[I
    invoke-static {p1}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v5

    .line 162
    .local v5, "mgr":Landroid/appwidget/AppWidgetManager;
    invoke-virtual {v5, v6, v12}, Landroid/appwidget/AppWidgetManager;->notifyAppWidgetViewDataChanged([II)V

    .line 164
    invoke-virtual {p0, p1, v5, v6}, Lcom/samsung/android/widget/memo/MemoWidgetProvider;->onUpdate(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;[I)V

    goto :goto_0

    .line 165
    .end local v2    # "id":J
    .end local v5    # "mgr":Landroid/appwidget/AppWidgetManager;
    .end local v6    # "updateWIds":[I
    :cond_4
    const-string v7, "com.samsung.android.widget.memo.UPDATE_ALL_WIDGET"

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 166
    invoke-direct {p0, p1}, Lcom/samsung/android/widget/memo/MemoWidgetProvider;->updateAllWidget(Landroid/content/Context;)V

    .line 167
    invoke-static {p1}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v5

    .line 168
    .restart local v5    # "mgr":Landroid/appwidget/AppWidgetManager;
    new-instance v1, Landroid/content/ComponentName;

    const-class v7, Lcom/samsung/android/widget/memo/MemoWidgetProvider;

    invoke-direct {v1, p1, v7}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 169
    .local v1, "cn":Landroid/content/ComponentName;
    invoke-virtual {v5, v1}, Landroid/appwidget/AppWidgetManager;->getAppWidgetIds(Landroid/content/ComponentName;)[I

    move-result-object v7

    invoke-virtual {v5, v7, v12}, Landroid/appwidget/AppWidgetManager;->notifyAppWidgetViewDataChanged([II)V

    goto :goto_0

    .line 170
    .end local v1    # "cn":Landroid/content/ComponentName;
    .end local v5    # "mgr":Landroid/appwidget/AppWidgetManager;
    :cond_5
    const-string v7, "android.appwidget.action.APPWIDGET_DELETED"

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_6

    .line 171
    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v7

    const-string v10, "appWidgetId"

    const/4 v11, 0x0

    invoke-virtual {v7, v10, v11}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v7

    int-to-long v8, v7

    .line 172
    .local v8, "widgetId":J
    invoke-direct {p0, v8, v9}, Lcom/samsung/android/widget/memo/MemoWidgetProvider;->removeWidgetCells(J)V

    goto :goto_0

    .line 173
    .end local v8    # "widgetId":J
    :cond_6
    const-string v7, "com.sec.knox.container.INTENT_KNOX_USER_CHANGED"

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_7

    const-string v7, "com.samsung.android.widget.ORIENTATION_CHANGE"

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_7

    const-string v7, "android.appwidget.action.APPWIDGET_UPDATE"

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 174
    :cond_7
    invoke-direct {p0, p1}, Lcom/samsung/android/widget/memo/MemoWidgetProvider;->updateAllWidget(Landroid/content/Context;)V

    goto/16 :goto_0
.end method

.method public onUpdate(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;[I)V
    .locals 14
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "appWidgetManager"    # Landroid/appwidget/AppWidgetManager;
    .param p3, "appWidgetIds"    # [I

    .prologue
    .line 231
    sget-object v10, Lcom/samsung/android/widget/memo/MemoWidgetProvider;->TAG:Ljava/lang/String;

    const-string v11, "onUpdate"

    invoke-static {v10, v11}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 232
    const/4 v9, 0x0

    .line 233
    .local v9, "widgetBundle":Landroid/os/Bundle;
    const/4 v4, 0x0

    .line 234
    .local v4, "minHeight":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    move-object/from16 v0, p3

    array-length v10, v0

    if-lt v2, v10, :cond_0

    .line 288
    invoke-super/range {p0 .. p3}, Landroid/appwidget/AppWidgetProvider;->onUpdate(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;[I)V

    .line 289
    return-void

    .line 235
    :cond_0
    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3}, Landroid/content/Intent;-><init>()V

    .line 236
    .local v3, "intent":Landroid/content/Intent;
    const-string v10, "com.samsung.android.widget.memo.WidgetService"

    invoke-virtual {v3, p1, v10}, Landroid/content/Intent;->setClassName(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    .line 237
    const-string v10, "appWidgetId"

    aget v11, p3, v2

    invoke-virtual {v3, v10, v11}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 238
    const/4 v10, 0x1

    invoke-virtual {v3, v10}, Landroid/content/Intent;->toUri(I)Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v10

    invoke-virtual {v3, v10}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 239
    new-instance v8, Landroid/widget/RemoteViews;

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v10

    const v11, 0x7f04002c

    invoke-direct {v8, v10, v11}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    .line 240
    .local v8, "rv":Landroid/widget/RemoteViews;
    const v10, 0x7f0e0084

    invoke-virtual {v8, v10, v3}, Landroid/widget/RemoteViews;->setRemoteAdapter(ILandroid/content/Intent;)V

    .line 241
    aget v10, p3, v2

    invoke-direct {p0, v8, v10}, Lcom/samsung/android/widget/memo/MemoWidgetProvider;->checkForVoiceMemo(Landroid/widget/RemoteViews;I)V

    .line 242
    invoke-static {}, Lcom/samsung/android/app/memo/util/Utils;->isTablet()Z

    move-result v10

    if-eqz v10, :cond_3

    .line 243
    const v10, 0x7f0e0084

    const v11, 0x7f0e0089

    invoke-virtual {v8, v10, v11}, Landroid/widget/RemoteViews;->setEmptyView(II)V

    .line 248
    :goto_1
    aget v10, p3, v2

    move-object/from16 v0, p2

    invoke-virtual {v0, v10}, Landroid/appwidget/AppWidgetManager;->getAppWidgetOptions(I)Landroid/os/Bundle;

    move-result-object v9

    .line 249
    const-string v10, "appWidgetMinHeight"

    invoke-virtual {v9, v10}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v4

    .line 250
    invoke-static {}, Lcom/samsung/android/app/memo/util/Utils;->isTablet()Z

    move-result v10

    if-nez v10, :cond_1

    .line 251
    invoke-static {}, Lcom/samsung/android/app/memo/util/Utils;->isFonbletHD()Z

    move-result v10

    if-eqz v10, :cond_5

    iget-object v10, p0, Lcom/samsung/android/widget/memo/MemoWidgetProvider;->mContext:Landroid/content/Context;

    invoke-virtual {v10}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    invoke-virtual {v10}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v10

    iget v10, v10, Landroid/content/res/Configuration;->orientation:I

    const/4 v11, 0x2

    if-ne v10, v11, :cond_5

    .line 253
    const/16 v10, 0x96

    if-ge v4, v10, :cond_4

    .line 254
    const v10, 0x7f0e0088

    const/4 v11, 0x0

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v12

    const v13, 0x7f0900b0

    invoke-virtual {v12, v13}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v12

    invoke-virtual {v8, v10, v11, v12}, Landroid/widget/RemoteViews;->setTextViewTextSize(IIF)V

    .line 269
    :cond_1
    :goto_2
    const-string v10, "appWidgetMaxHeight"

    invoke-virtual {v9, v10}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v10

    invoke-direct {p0, v10}, Lcom/samsung/android/widget/memo/MemoWidgetProvider;->getCellsNumber(I)I

    move-result v5

    .line 270
    .local v5, "nWidgetLines":I
    const/4 v10, 0x1

    if-le v5, v10, :cond_2

    .line 272
    const v10, 0x7f0e0088

    const/4 v11, 0x0

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v12

    const v13, 0x7f0900b1

    invoke-virtual {v12, v13}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v12

    invoke-virtual {v8, v10, v11, v12}, Landroid/widget/RemoteViews;->setTextViewTextSize(IIF)V

    .line 274
    :cond_2
    aget v10, p3, v2

    invoke-direct {p0, v10, v5}, Lcom/samsung/android/widget/memo/MemoWidgetProvider;->saveWidgetCells(II)V

    .line 275
    new-instance v1, Landroid/content/Intent;

    const-class v10, Lcom/samsung/android/widget/memo/MemoWidgetProvider;

    invoke-direct {v1, p1, v10}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 276
    .local v1, "clickIntent":Landroid/content/Intent;
    const-string v10, "com.samsung.android.widget.APPWIDGET_CLICKED"

    invoke-virtual {v1, v10}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 277
    const-string v10, "appWidgetId"

    aget v11, p3, v2

    invoke-virtual {v1, v10, v11}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 278
    aget v10, p3, v2

    .line 279
    const/high16 v11, 0x8000000

    .line 278
    invoke-static {p1, v10, v1, v11}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v7

    .line 280
    .local v7, "pi":Landroid/app/PendingIntent;
    const v10, 0x7f0e0081

    invoke-virtual {v8, v10, v7}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 281
    const v10, 0x7f0e008a

    invoke-virtual {v8, v10, v7}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 282
    const v10, 0x7f0e008b

    invoke-virtual {v8, v10, v7}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 283
    aget v10, p3, v2

    .line 284
    const/high16 v11, 0x8000000

    .line 283
    invoke-static {p1, v10, v1, v11}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v6

    .line 285
    .local v6, "pendingintent":Landroid/app/PendingIntent;
    const v10, 0x7f0e0084

    invoke-virtual {v8, v10, v6}, Landroid/widget/RemoteViews;->setPendingIntentTemplate(ILandroid/app/PendingIntent;)V

    .line 286
    aget v10, p3, v2

    move-object/from16 v0, p2

    invoke-virtual {v0, v10, v8}, Landroid/appwidget/AppWidgetManager;->updateAppWidget(ILandroid/widget/RemoteViews;)V

    .line 234
    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_0

    .line 245
    .end local v1    # "clickIntent":Landroid/content/Intent;
    .end local v5    # "nWidgetLines":I
    .end local v6    # "pendingintent":Landroid/app/PendingIntent;
    .end local v7    # "pi":Landroid/app/PendingIntent;
    :cond_3
    const v10, 0x7f0e0084

    const v11, 0x7f0e0085

    invoke-virtual {v8, v10, v11}, Landroid/widget/RemoteViews;->setEmptyView(II)V

    goto/16 :goto_1

    .line 257
    :cond_4
    const v10, 0x7f0e0088

    const/4 v11, 0x0

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v12

    const v13, 0x7f0900af

    invoke-virtual {v12, v13}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v12

    invoke-virtual {v8, v10, v11, v12}, Landroid/widget/RemoteViews;->setTextViewTextSize(IIF)V

    goto/16 :goto_2

    .line 262
    :cond_5
    const/16 v10, 0x64

    if-ge v4, v10, :cond_6

    .line 263
    const v10, 0x7f0e0088

    const/4 v11, 0x0

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v12

    const v13, 0x7f0900b0

    invoke-virtual {v12, v13}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v12

    invoke-virtual {v8, v10, v11, v12}, Landroid/widget/RemoteViews;->setTextViewTextSize(IIF)V

    goto/16 :goto_2

    .line 265
    :cond_6
    const v10, 0x7f0e0088

    const/4 v11, 0x0

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v12

    const v13, 0x7f0900af

    invoke-virtual {v12, v13}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v12

    invoke-virtual {v8, v10, v11, v12}, Landroid/widget/RemoteViews;->setTextViewTextSize(IIF)V

    goto/16 :goto_2
.end method
