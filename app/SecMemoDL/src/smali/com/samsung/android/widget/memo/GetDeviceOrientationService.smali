.class public Lcom/samsung/android/widget/memo/GetDeviceOrientationService;
.super Landroid/app/Service;
.source "GetDeviceOrientationService.java"


# instance fields
.field public configurationReceiver:Landroid/content/BroadcastReceiver;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 10
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 27
    new-instance v0, Lcom/samsung/android/widget/memo/GetDeviceOrientationService$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/widget/memo/GetDeviceOrientationService$1;-><init>(Lcom/samsung/android/widget/memo/GetDeviceOrientationService;)V

    iput-object v0, p0, Lcom/samsung/android/widget/memo/GetDeviceOrientationService;->configurationReceiver:Landroid/content/BroadcastReceiver;

    .line 10
    return-void
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1, "arg0"    # Landroid/content/Intent;

    .prologue
    .line 15
    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 2

    .prologue
    .line 21
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 22
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 23
    .local v0, "intentFilter":Landroid/content/IntentFilter;
    const-string v1, "android.intent.action.CONFIGURATION_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 24
    iget-object v1, p0, Lcom/samsung/android/widget/memo/GetDeviceOrientationService;->configurationReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/samsung/android/widget/memo/GetDeviceOrientationService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 25
    return-void
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 44
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 45
    iget-object v0, p0, Lcom/samsung/android/widget/memo/GetDeviceOrientationService;->configurationReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/samsung/android/widget/memo/GetDeviceOrientationService;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 46
    return-void
.end method
