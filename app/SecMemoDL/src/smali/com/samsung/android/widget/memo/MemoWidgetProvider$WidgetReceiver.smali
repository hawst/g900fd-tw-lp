.class public Lcom/samsung/android/widget/memo/MemoWidgetProvider$WidgetReceiver;
.super Landroid/content/BroadcastReceiver;
.source "MemoWidgetProvider.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/widget/memo/MemoWidgetProvider;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "WidgetReceiver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/widget/memo/MemoWidgetProvider;

.field widgetProvider:Lcom/samsung/android/widget/memo/MemoWidgetProvider;


# direct methods
.method public constructor <init>(Lcom/samsung/android/widget/memo/MemoWidgetProvider;)V
    .locals 0

    .prologue
    .line 88
    iput-object p1, p0, Lcom/samsung/android/widget/memo/MemoWidgetProvider$WidgetReceiver;->this$0:Lcom/samsung/android/widget/memo/MemoWidgetProvider;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 12
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const-wide/16 v10, -0x1

    .line 100
    # getter for: Lcom/samsung/android/widget/memo/MemoWidgetProvider;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/widget/memo/MemoWidgetProvider;->access$0()Ljava/lang/String;

    move-result-object v5

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, " Local onReceive : "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v5, v8}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 101
    iget-object v5, p0, Lcom/samsung/android/widget/memo/MemoWidgetProvider$WidgetReceiver;->this$0:Lcom/samsung/android/widget/memo/MemoWidgetProvider;

    invoke-static {v5, p1}, Lcom/samsung/android/widget/memo/MemoWidgetProvider;->access$1(Lcom/samsung/android/widget/memo/MemoWidgetProvider;Landroid/content/Context;)V

    .line 102
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 104
    .local v0, "action":Ljava/lang/String;
    const-string v5, "com.sec.android.widgetapp.MEMO_SAVE"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 105
    const-string v5, "memo_id"

    invoke-virtual {p2, v5, v10, v11}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v2

    .line 106
    .local v2, "id":J
    const-string v5, "appWidgetId"

    invoke-virtual {p2, v5, v10, v11}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v6

    .line 107
    .local v6, "widgetId":J
    iget-object v5, p0, Lcom/samsung/android/widget/memo/MemoWidgetProvider$WidgetReceiver;->this$0:Lcom/samsung/android/widget/memo/MemoWidgetProvider;

    invoke-static {v6, v7}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v8

    # invokes: Lcom/samsung/android/widget/memo/MemoWidgetProvider;->saveWidgetPreferences(Ljava/lang/String;J)V
    invoke-static {v5, v8, v2, v3}, Lcom/samsung/android/widget/memo/MemoWidgetProvider;->access$2(Lcom/samsung/android/widget/memo/MemoWidgetProvider;Ljava/lang/String;J)V

    .line 108
    invoke-static {p1}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v1

    .line 109
    .local v1, "mgr":Landroid/appwidget/AppWidgetManager;
    const/4 v5, 0x1

    new-array v4, v5, [I

    const/4 v5, 0x0

    long-to-int v8, v6

    aput v8, v4, v5

    .line 111
    .local v4, "updateWIds":[I
    const v5, 0x7f0e0084

    .line 110
    invoke-virtual {v1, v4, v5}, Landroid/appwidget/AppWidgetManager;->notifyAppWidgetViewDataChanged([II)V

    .line 112
    iget-object v5, p0, Lcom/samsung/android/widget/memo/MemoWidgetProvider$WidgetReceiver;->this$0:Lcom/samsung/android/widget/memo/MemoWidgetProvider;

    # invokes: Lcom/samsung/android/widget/memo/MemoWidgetProvider;->updateWidget(Landroid/content/Context;J)V
    invoke-static {v5, p1, v6, v7}, Lcom/samsung/android/widget/memo/MemoWidgetProvider;->access$3(Lcom/samsung/android/widget/memo/MemoWidgetProvider;Landroid/content/Context;J)V

    .line 114
    .end local v1    # "mgr":Landroid/appwidget/AppWidgetManager;
    .end local v2    # "id":J
    .end local v4    # "updateWIds":[I
    .end local v6    # "widgetId":J
    :cond_0
    return-void
.end method

.method setProvider(Lcom/samsung/android/widget/memo/MemoWidgetProvider;)V
    .locals 0
    .param p1, "inProvider"    # Lcom/samsung/android/widget/memo/MemoWidgetProvider;

    .prologue
    .line 95
    iput-object p1, p0, Lcom/samsung/android/widget/memo/MemoWidgetProvider$WidgetReceiver;->widgetProvider:Lcom/samsung/android/widget/memo/MemoWidgetProvider;

    .line 96
    return-void
.end method
