.class Lcom/samsung/android/widget/memo/MemoViewsFactory;
.super Ljava/lang/Object;
.source "WidgetService.java"

# interfaces
.implements Landroid/widget/RemoteViewsService$RemoteViewsFactory;


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final mAppWidgetId:I

.field private final mContext:Landroid/content/Context;

.field private mCursor:Landroid/database/Cursor;

.field public mStrBody:Ljava/lang/String;

.field public mTitle:Ljava/lang/String;

.field public strippedContents:Ljava/lang/String;

.field public voiceRecodData:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 60
    const-class v0, Lcom/samsung/android/widget/memo/WidgetService;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/widget/memo/MemoViewsFactory;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v0, 0x0

    .line 76
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 66
    iput-object v0, p0, Lcom/samsung/android/widget/memo/MemoViewsFactory;->mStrBody:Ljava/lang/String;

    .line 68
    iput-object v0, p0, Lcom/samsung/android/widget/memo/MemoViewsFactory;->mTitle:Ljava/lang/String;

    .line 70
    iput-object v0, p0, Lcom/samsung/android/widget/memo/MemoViewsFactory;->voiceRecodData:Ljava/lang/String;

    .line 77
    iput-object p1, p0, Lcom/samsung/android/widget/memo/MemoViewsFactory;->mContext:Landroid/content/Context;

    .line 78
    const-string v0, "appWidgetId"

    .line 79
    const/4 v1, 0x0

    .line 78
    invoke-virtual {p2, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/samsung/android/widget/memo/MemoViewsFactory;->mAppWidgetId:I

    .line 80
    return-void
.end method

.method private buildTitle(Landroid/widget/RemoteViews;Ljava/lang/String;)V
    .locals 3
    .param p1, "rootView"    # Landroid/widget/RemoteViews;
    .param p2, "mTitle"    # Ljava/lang/String;

    .prologue
    const v2, 0x7f0e004d

    .line 138
    if-eqz p2, :cond_0

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 139
    const-string v0, "setVisibility"

    .line 140
    const/4 v1, 0x0

    .line 139
    invoke-virtual {p1, v2, v0, v1}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    .line 141
    invoke-virtual {p1, v2, p2}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 146
    :goto_0
    return-void

    .line 143
    :cond_0
    const-string v0, "setVisibility"

    .line 144
    const/16 v1, 0x8

    .line 143
    invoke-virtual {p1, v2, v0, v1}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    goto :goto_0
.end method

.method private buildView(Landroid/widget/RemoteViews;Ljava/lang/String;)V
    .locals 12
    .param p1, "rootView"    # Landroid/widget/RemoteViews;
    .param p2, "content"    # Ljava/lang/String;

    .prologue
    .line 149
    const/4 v9, 0x0

    .line 150
    .local v9, "start":I
    const/4 v3, 0x0

    .line 151
    .local v3, "imageEnd":I
    const/4 v1, 0x0

    .line 152
    .local v1, "end":I
    const/4 v7, 0x0

    .line 153
    .local v7, "orientation":I
    const-string v4, "<img src="

    .line 154
    .local v4, "imageTag":Ljava/lang/String;
    const-string v8, " orientation="

    .line 155
    .local v8, "orientationTag":Ljava/lang/String;
    const-string v2, "/>"

    .line 157
    .local v2, "endTag":Ljava/lang/String;
    const/4 v0, 0x0

    .line 159
    .local v0, "count":I
    invoke-virtual {p2, v4, v9}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result v9

    .line 160
    const/16 v10, 0x8

    invoke-direct {p0, p1, v10}, Lcom/samsung/android/widget/memo/MemoViewsFactory;->setAllViewVisibility(Landroid/widget/RemoteViews;I)V

    .line 161
    if-ltz v9, :cond_5

    .line 162
    :goto_0
    if-gez v9, :cond_0

    .line 196
    :goto_1
    return-void

    .line 164
    :cond_0
    if-eqz v9, :cond_2

    if-eq v9, v1, :cond_2

    .line 165
    add-int/lit8 v10, v9, -0x1

    invoke-virtual {p2, v1, v10}, Ljava/lang/String;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 168
    .local v5, "mText":Ljava/lang/String;
    :goto_2
    const-string v10, "</p><p></p><p"

    invoke-virtual {v10, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_3

    .line 170
    const-string v5, ""

    .line 175
    :cond_1
    :goto_3
    invoke-direct {p0, p1, v5, v0}, Lcom/samsung/android/widget/memo/MemoViewsFactory;->handleText(Landroid/widget/RemoteViews;Ljava/lang/String;I)V

    .line 176
    add-int/lit8 v10, v9, 0x1

    invoke-virtual {p2, v8, v10}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result v3

    .line 179
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v10

    add-int/2addr v10, v9

    add-int/lit8 v10, v10, 0x1

    add-int/lit8 v11, v3, -0x1

    .line 178
    invoke-virtual {p2, v10, v11}, Ljava/lang/String;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v10

    check-cast v10, Ljava/lang/String;

    invoke-static {v10}, Landroid/net/Uri;->decode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    .line 180
    .local v6, "mUri":Landroid/net/Uri;
    const-string v10, " "

    add-int/lit8 v11, v3, 0x1

    invoke-virtual {p2, v10, v11}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result v9

    .line 181
    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v10

    add-int/2addr v10, v3

    add-int/lit8 v10, v10, 0x1

    add-int/lit8 v11, v9, -0x1

    invoke-virtual {p2, v10, v11}, Ljava/lang/String;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v10

    check-cast v10, Ljava/lang/String;

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/Integer;->intValue()I

    move-result v7

    .line 182
    invoke-direct {p0, p1, v6, v7, v0}, Lcom/samsung/android/widget/memo/MemoViewsFactory;->handleImage(Landroid/widget/RemoteViews;Landroid/net/Uri;II)V

    .line 183
    add-int/lit8 v10, v3, 0x1

    invoke-virtual {p2, v2, v10}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result v10

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v11

    add-int v1, v10, v11

    .line 184
    invoke-virtual {p2, v4, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result v9

    .line 185
    const/4 v10, -0x1

    if-ne v9, v10, :cond_4

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v10

    if-ge v1, v10, :cond_4

    .line 186
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v9

    .line 187
    add-int/lit8 v10, v9, -0x1

    invoke-virtual {p2, v1, v10}, Ljava/lang/String;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v5

    .end local v5    # "mText":Ljava/lang/String;
    check-cast v5, Ljava/lang/String;

    .line 188
    .restart local v5    # "mText":Ljava/lang/String;
    add-int/lit8 v10, v0, 0x1

    invoke-direct {p0, p1, v5, v10}, Lcom/samsung/android/widget/memo/MemoViewsFactory;->handleText(Landroid/widget/RemoteViews;Ljava/lang/String;I)V

    goto/16 :goto_1

    .line 167
    .end local v5    # "mText":Ljava/lang/String;
    .end local v6    # "mUri":Landroid/net/Uri;
    :cond_2
    const-string v5, ""

    .restart local v5    # "mText":Ljava/lang/String;
    goto :goto_2

    .line 172
    :cond_3
    const-string v10, "</p><p"

    invoke-virtual {v10, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1

    .line 173
    const/4 v5, 0x0

    goto :goto_3

    .line 191
    .restart local v6    # "mUri":Landroid/net/Uri;
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_0

    .line 194
    .end local v5    # "mText":Ljava/lang/String;
    .end local v6    # "mUri":Landroid/net/Uri;
    :cond_5
    invoke-direct {p0, p1, p2, v0}, Lcom/samsung/android/widget/memo/MemoViewsFactory;->handleText(Landroid/widget/RemoteViews;Ljava/lang/String;I)V

    goto/16 :goto_1
.end method

.method private handleImage(Landroid/widget/RemoteViews;Landroid/net/Uri;II)V
    .locals 9
    .param p1, "rootView"    # Landroid/widget/RemoteViews;
    .param p2, "mUri"    # Landroid/net/Uri;
    .param p3, "orientation"    # I
    .param p4, "count"    # I

    .prologue
    .line 319
    if-eqz p2, :cond_0

    .line 320
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v2

    .line 322
    .local v2, "identityToken":J
    :try_start_0
    iget-object v6, p0, Lcom/samsung/android/widget/memo/MemoViewsFactory;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0900ac

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v6

    float-to-int v5, v6

    .line 323
    .local v5, "width":I
    iget-object v6, p0, Lcom/samsung/android/widget/memo/MemoViewsFactory;->mContext:Landroid/content/Context;

    const/4 v7, -0x1

    invoke-static {v6, p2, v7, p3, v5}, Lcom/samsung/android/app/memo/util/ImageUtil;->decodeImageScaledIf(Landroid/content/Context;Landroid/net/Uri;III)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v4

    .line 324
    .local v4, "mBitmap":Landroid/graphics/Bitmap;
    packed-switch p4, :pswitch_data_0

    .line 372
    :goto_0
    invoke-static {v2, v3}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 375
    .end local v2    # "identityToken":J
    .end local v4    # "mBitmap":Landroid/graphics/Bitmap;
    .end local v5    # "width":I
    :cond_0
    :goto_1
    return-void

    .line 326
    .restart local v2    # "identityToken":J
    .restart local v4    # "mBitmap":Landroid/graphics/Bitmap;
    .restart local v5    # "width":I
    :pswitch_0
    :try_start_1
    iget-object v6, p0, Lcom/samsung/android/widget/memo/MemoViewsFactory;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0900b7

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v6

    float-to-int v1, v6

    .line 327
    .local v1, "height":I
    const v6, 0x7f0e004e

    const-string v7, "setMinHeight"

    invoke-virtual {p1, v6, v7, v1}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    .line 328
    const v6, 0x7f0e004f

    const-string v7, "setVisibility"

    const/4 v8, 0x0

    invoke-virtual {p1, v6, v7, v8}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    .line 329
    const v6, 0x7f0e004f

    invoke-virtual {p1, v6, v4}, Landroid/widget/RemoteViews;->setImageViewBitmap(ILandroid/graphics/Bitmap;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 368
    .end local v1    # "height":I
    .end local v4    # "mBitmap":Landroid/graphics/Bitmap;
    .end local v5    # "width":I
    :catch_0
    move-exception v0

    .line 369
    .local v0, "e":Ljava/io/IOException;
    :try_start_2
    sget-object v6, Lcom/samsung/android/widget/memo/MemoViewsFactory;->TAG:Ljava/lang/String;

    const-string v7, "Handle Image"

    invoke-static {v6, v7, v0}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 372
    invoke-static {v2, v3}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    goto :goto_1

    .line 332
    .end local v0    # "e":Ljava/io/IOException;
    .restart local v4    # "mBitmap":Landroid/graphics/Bitmap;
    .restart local v5    # "width":I
    :pswitch_1
    const v6, 0x7f0e0051

    :try_start_3
    const-string v7, "setVisibility"

    const/4 v8, 0x0

    invoke-virtual {p1, v6, v7, v8}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    .line 333
    const v6, 0x7f0e0051

    invoke-virtual {p1, v6, v4}, Landroid/widget/RemoteViews;->setImageViewBitmap(ILandroid/graphics/Bitmap;)V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 371
    .end local v4    # "mBitmap":Landroid/graphics/Bitmap;
    .end local v5    # "width":I
    :catchall_0
    move-exception v6

    .line 372
    invoke-static {v2, v3}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 373
    throw v6

    .line 336
    .restart local v4    # "mBitmap":Landroid/graphics/Bitmap;
    .restart local v5    # "width":I
    :pswitch_2
    const v6, 0x7f0e0053

    :try_start_4
    const-string v7, "setVisibility"

    const/4 v8, 0x0

    invoke-virtual {p1, v6, v7, v8}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    .line 337
    const v6, 0x7f0e0053

    invoke-virtual {p1, v6, v4}, Landroid/widget/RemoteViews;->setImageViewBitmap(ILandroid/graphics/Bitmap;)V

    goto :goto_0

    .line 340
    :pswitch_3
    const v6, 0x7f0e0055

    const-string v7, "setVisibility"

    const/4 v8, 0x0

    invoke-virtual {p1, v6, v7, v8}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    .line 341
    const v6, 0x7f0e0055

    invoke-virtual {p1, v6, v4}, Landroid/widget/RemoteViews;->setImageViewBitmap(ILandroid/graphics/Bitmap;)V

    goto :goto_0

    .line 344
    :pswitch_4
    const v6, 0x7f0e0057

    const-string v7, "setVisibility"

    const/4 v8, 0x0

    invoke-virtual {p1, v6, v7, v8}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    .line 345
    const v6, 0x7f0e0057

    invoke-virtual {p1, v6, v4}, Landroid/widget/RemoteViews;->setImageViewBitmap(ILandroid/graphics/Bitmap;)V

    goto :goto_0

    .line 348
    :pswitch_5
    const v6, 0x7f0e0059

    const-string v7, "setVisibility"

    const/4 v8, 0x0

    invoke-virtual {p1, v6, v7, v8}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    .line 349
    const v6, 0x7f0e0059

    invoke-virtual {p1, v6, v4}, Landroid/widget/RemoteViews;->setImageViewBitmap(ILandroid/graphics/Bitmap;)V

    goto/16 :goto_0

    .line 352
    :pswitch_6
    const v6, 0x7f0e005b

    const-string v7, "setVisibility"

    const/4 v8, 0x0

    invoke-virtual {p1, v6, v7, v8}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    .line 353
    const v6, 0x7f0e005b

    invoke-virtual {p1, v6, v4}, Landroid/widget/RemoteViews;->setImageViewBitmap(ILandroid/graphics/Bitmap;)V

    goto/16 :goto_0

    .line 356
    :pswitch_7
    const v6, 0x7f0e005d

    const-string v7, "setVisibility"

    const/4 v8, 0x0

    invoke-virtual {p1, v6, v7, v8}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    .line 357
    const v6, 0x7f0e005d

    invoke-virtual {p1, v6, v4}, Landroid/widget/RemoteViews;->setImageViewBitmap(ILandroid/graphics/Bitmap;)V

    goto/16 :goto_0

    .line 360
    :pswitch_8
    const v6, 0x7f0e005f

    const-string v7, "setVisibility"

    const/4 v8, 0x0

    invoke-virtual {p1, v6, v7, v8}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    .line 361
    const v6, 0x7f0e005f

    invoke-virtual {p1, v6, v4}, Landroid/widget/RemoteViews;->setImageViewBitmap(ILandroid/graphics/Bitmap;)V

    goto/16 :goto_0

    .line 364
    :pswitch_9
    const v6, 0x7f0e0061

    const-string v7, "setVisibility"

    const/4 v8, 0x0

    invoke-virtual {p1, v6, v7, v8}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    .line 365
    const v6, 0x7f0e0061

    invoke-virtual {p1, v6, v4}, Landroid/widget/RemoteViews;->setImageViewBitmap(ILandroid/graphics/Bitmap;)V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 324
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
    .end packed-switch
.end method

.method private handleText(Landroid/widget/RemoteViews;Ljava/lang/String;I)V
    .locals 8
    .param p1, "rootView"    # Landroid/widget/RemoteViews;
    .param p2, "content"    # Ljava/lang/String;
    .param p3, "i"    # I

    .prologue
    .line 225
    if-eqz p2, :cond_0

    .line 226
    const-string v4, "\n"

    const-string v5, ""

    invoke-virtual {p2, v4, v5}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p2

    .line 227
    iget-object v4, p0, Lcom/samsung/android/widget/memo/MemoViewsFactory;->mContext:Landroid/content/Context;

    const/4 v5, 0x0

    const-wide/16 v6, -0x1

    invoke-static {p2, v4, v5, v6, v7}, Lcom/samsung/android/app/memo/util/HtmlUtil;->fromHtml(Ljava/lang/String;Landroid/content/Context;Lcom/samsung/android/app/memo/uiwidget/ClickableSpanType$OnClickListener;J)Landroid/text/Spanned;

    move-result-object v2

    .line 228
    .local v2, "mText":Landroid/text/Spanned;
    packed-switch p3, :pswitch_data_0

    .line 315
    .end local v2    # "mText":Landroid/text/Spanned;
    :cond_0
    :goto_0
    return-void

    .line 230
    .restart local v2    # "mText":Landroid/text/Spanned;
    :pswitch_0
    iget-object v4, p0, Lcom/samsung/android/widget/memo/MemoViewsFactory;->mContext:Landroid/content/Context;

    const-string v5, "widgetPref"

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v3

    .line 231
    .local v3, "widgetPref":Landroid/content/SharedPreferences;
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "cells"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v5, p0, Lcom/samsung/android/widget/memo/MemoViewsFactory;->mAppWidgetId:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, -0x1

    invoke-interface {v3, v4, v5}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 232
    .local v0, "cells":I
    iget-object v4, p0, Lcom/samsung/android/widget/memo/MemoViewsFactory;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0900b8

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v4

    float-to-int v1, v4

    .line 233
    .local v1, "height":I
    if-lez v0, :cond_1

    .line 234
    packed-switch v0, :pswitch_data_1

    .line 255
    :cond_1
    :goto_1
    iget-object v4, p0, Lcom/samsung/android/widget/memo/MemoViewsFactory;->mTitle:Ljava/lang/String;

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/samsung/android/widget/memo/MemoViewsFactory;->mTitle:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    if-lez v4, :cond_2

    .line 256
    iget-object v4, p0, Lcom/samsung/android/widget/memo/MemoViewsFactory;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0900be

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v4

    float-to-int v4, v4

    sub-int/2addr v1, v4

    .line 257
    :cond_2
    const v4, 0x7f0e004e

    const-string v5, "setMinHeight"

    invoke-virtual {p1, v4, v5, v1}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    .line 258
    const v4, 0x7f0e004e

    const-string v5, "setVisibility"

    .line 259
    const/4 v6, 0x0

    .line 258
    invoke-virtual {p1, v4, v5, v6}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    .line 260
    const v4, 0x7f0e004e

    invoke-virtual {p1, v4, v2}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    goto :goto_0

    .line 236
    :pswitch_1
    iget-object v4, p0, Lcom/samsung/android/widget/memo/MemoViewsFactory;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0900b8

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v4

    float-to-int v1, v4

    .line 237
    goto :goto_1

    .line 239
    :pswitch_2
    iget-object v4, p0, Lcom/samsung/android/widget/memo/MemoViewsFactory;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0900b9

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v4

    float-to-int v1, v4

    .line 240
    goto :goto_1

    .line 242
    :pswitch_3
    iget-object v4, p0, Lcom/samsung/android/widget/memo/MemoViewsFactory;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0900ba

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v4

    float-to-int v1, v4

    .line 243
    goto :goto_1

    .line 245
    :pswitch_4
    iget-object v4, p0, Lcom/samsung/android/widget/memo/MemoViewsFactory;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0900bb

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v4

    float-to-int v1, v4

    .line 246
    goto :goto_1

    .line 248
    :pswitch_5
    iget-object v4, p0, Lcom/samsung/android/widget/memo/MemoViewsFactory;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0900bc

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v4

    float-to-int v1, v4

    .line 249
    goto :goto_1

    .line 251
    :pswitch_6
    iget-object v4, p0, Lcom/samsung/android/widget/memo/MemoViewsFactory;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0900bd

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v4

    float-to-int v1, v4

    goto/16 :goto_1

    .line 263
    .end local v0    # "cells":I
    .end local v1    # "height":I
    .end local v3    # "widgetPref":Landroid/content/SharedPreferences;
    :pswitch_7
    const v4, 0x7f0e0050

    const-string v5, "setVisibility"

    .line 264
    const/4 v6, 0x0

    .line 263
    invoke-virtual {p1, v4, v5, v6}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    .line 265
    const v4, 0x7f0e0050

    invoke-virtual {p1, v4, v2}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 268
    :pswitch_8
    const v4, 0x7f0e0052

    const-string v5, "setVisibility"

    .line 269
    const/4 v6, 0x0

    .line 268
    invoke-virtual {p1, v4, v5, v6}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    .line 270
    const v4, 0x7f0e0052

    invoke-virtual {p1, v4, v2}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 273
    :pswitch_9
    const v4, 0x7f0e0054

    const-string v5, "setVisibility"

    .line 274
    const/4 v6, 0x0

    .line 273
    invoke-virtual {p1, v4, v5, v6}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    .line 275
    const v4, 0x7f0e0054

    invoke-virtual {p1, v4, v2}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 278
    :pswitch_a
    const v4, 0x7f0e0056

    const-string v5, "setVisibility"

    .line 279
    const/4 v6, 0x0

    .line 278
    invoke-virtual {p1, v4, v5, v6}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    .line 280
    const v4, 0x7f0e0056

    invoke-virtual {p1, v4, v2}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 283
    :pswitch_b
    const v4, 0x7f0e0058

    const-string v5, "setVisibility"

    .line 284
    const/4 v6, 0x0

    .line 283
    invoke-virtual {p1, v4, v5, v6}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    .line 285
    const v4, 0x7f0e0058

    invoke-virtual {p1, v4, v2}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 288
    :pswitch_c
    const v4, 0x7f0e005a

    const-string v5, "setVisibility"

    .line 289
    const/4 v6, 0x0

    .line 288
    invoke-virtual {p1, v4, v5, v6}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    .line 290
    const v4, 0x7f0e005a

    invoke-virtual {p1, v4, v2}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 293
    :pswitch_d
    const v4, 0x7f0e005c

    const-string v5, "setVisibility"

    .line 294
    const/4 v6, 0x0

    .line 293
    invoke-virtual {p1, v4, v5, v6}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    .line 295
    const v4, 0x7f0e005c

    invoke-virtual {p1, v4, v2}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 298
    :pswitch_e
    const v4, 0x7f0e005e

    const-string v5, "setVisibility"

    .line 299
    const/4 v6, 0x0

    .line 298
    invoke-virtual {p1, v4, v5, v6}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    .line 300
    const v4, 0x7f0e005e

    invoke-virtual {p1, v4, v2}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 303
    :pswitch_f
    const v4, 0x7f0e0060

    const-string v5, "setVisibility"

    .line 304
    const/4 v6, 0x0

    .line 303
    invoke-virtual {p1, v4, v5, v6}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    .line 305
    const v4, 0x7f0e0060

    invoke-virtual {p1, v4, v2}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 308
    :pswitch_10
    const v4, 0x7f0e0062

    const-string v5, "setVisibility"

    .line 309
    const/4 v6, 0x0

    .line 308
    invoke-virtual {p1, v4, v5, v6}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    .line 310
    const v4, 0x7f0e0062

    invoke-virtual {p1, v4, v2}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 228
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
    .end packed-switch

    .line 234
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method private setAllViewVisibility(Landroid/widget/RemoteViews;I)V
    .locals 1
    .param p1, "rootView"    # Landroid/widget/RemoteViews;
    .param p2, "visibility"    # I

    .prologue
    .line 200
    const v0, 0x7f0e004e

    invoke-virtual {p1, v0, p2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 201
    const v0, 0x7f0e0050

    invoke-virtual {p1, v0, p2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 202
    const v0, 0x7f0e0052

    invoke-virtual {p1, v0, p2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 203
    const v0, 0x7f0e0054

    invoke-virtual {p1, v0, p2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 204
    const v0, 0x7f0e0056

    invoke-virtual {p1, v0, p2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 205
    const v0, 0x7f0e0058

    invoke-virtual {p1, v0, p2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 206
    const v0, 0x7f0e005a

    invoke-virtual {p1, v0, p2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 207
    const v0, 0x7f0e005c

    invoke-virtual {p1, v0, p2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 208
    const v0, 0x7f0e005e

    invoke-virtual {p1, v0, p2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 209
    const v0, 0x7f0e0060

    invoke-virtual {p1, v0, p2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 211
    const v0, 0x7f0e004f

    invoke-virtual {p1, v0, p2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 212
    const v0, 0x7f0e0051

    invoke-virtual {p1, v0, p2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 213
    const v0, 0x7f0e0053

    invoke-virtual {p1, v0, p2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 214
    const v0, 0x7f0e0055

    invoke-virtual {p1, v0, p2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 215
    const v0, 0x7f0e0057

    invoke-virtual {p1, v0, p2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 216
    const v0, 0x7f0e0059

    invoke-virtual {p1, v0, p2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 217
    const v0, 0x7f0e005b

    invoke-virtual {p1, v0, p2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 218
    const v0, 0x7f0e005d

    invoke-virtual {p1, v0, p2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 219
    const v0, 0x7f0e005f

    invoke-virtual {p1, v0, p2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 220
    const v0, 0x7f0e0061

    invoke-virtual {p1, v0, p2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 222
    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lcom/samsung/android/widget/memo/MemoViewsFactory;->mCursor:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/widget/memo/MemoViewsFactory;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-nez v0, :cond_0

    .line 96
    const/4 v0, 0x0

    .line 97
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 389
    int-to-long v0, p1

    return-wide v0
.end method

.method public getLoadingView()Landroid/widget/RemoteViews;
    .locals 1

    .prologue
    .line 379
    const/4 v0, 0x0

    return-object v0
.end method

.method public getViewAt(I)Landroid/widget/RemoteViews;
    .locals 7
    .param p1, "position"    # I

    .prologue
    .line 102
    const/4 v4, 0x0

    iput-object v4, p0, Lcom/samsung/android/widget/memo/MemoViewsFactory;->mStrBody:Ljava/lang/String;

    .line 103
    const/4 v2, 0x0

    .line 104
    .local v2, "noTextContents":Ljava/lang/String;
    new-instance v3, Landroid/widget/RemoteViews;

    iget-object v4, p0, Lcom/samsung/android/widget/memo/MemoViewsFactory;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v4

    .line 105
    const v5, 0x7f040026

    .line 104
    invoke-direct {v3, v4, v5}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    .line 107
    .local v3, "rv":Landroid/widget/RemoteViews;
    iget-object v4, p0, Lcom/samsung/android/widget/memo/MemoViewsFactory;->mCursor:Landroid/database/Cursor;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/samsung/android/widget/memo/MemoViewsFactory;->mCursor:Landroid/database/Cursor;

    invoke-interface {v4, p1}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v4

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/samsung/android/widget/memo/MemoViewsFactory;->mCursor:Landroid/database/Cursor;

    invoke-interface {v4}, Landroid/database/Cursor;->isClosed()Z

    move-result v4

    if-nez v4, :cond_0

    .line 108
    iget-object v4, p0, Lcom/samsung/android/widget/memo/MemoViewsFactory;->mCursor:Landroid/database/Cursor;

    const/4 v5, 0x0

    invoke-interface {v4, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/samsung/android/widget/memo/MemoViewsFactory;->mStrBody:Ljava/lang/String;

    .line 109
    iget-object v4, p0, Lcom/samsung/android/widget/memo/MemoViewsFactory;->mCursor:Landroid/database/Cursor;

    const/4 v5, 0x1

    invoke-interface {v4, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/samsung/android/widget/memo/MemoViewsFactory;->mTitle:Ljava/lang/String;

    .line 110
    iget-object v4, p0, Lcom/samsung/android/widget/memo/MemoViewsFactory;->mCursor:Landroid/database/Cursor;

    const/4 v5, 0x2

    invoke-interface {v4, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/samsung/android/widget/memo/MemoViewsFactory;->voiceRecodData:Ljava/lang/String;

    .line 111
    iget-object v4, p0, Lcom/samsung/android/widget/memo/MemoViewsFactory;->mCursor:Landroid/database/Cursor;

    const/4 v5, 0x3

    invoke-interface {v4, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/samsung/android/widget/memo/MemoViewsFactory;->strippedContents:Ljava/lang/String;

    .line 112
    iget-object v4, p0, Lcom/samsung/android/widget/memo/MemoViewsFactory;->strippedContents:Ljava/lang/String;

    if-eqz v4, :cond_0

    .line 113
    iget-object v4, p0, Lcom/samsung/android/widget/memo/MemoViewsFactory;->strippedContents:Ljava/lang/String;

    const-string v5, " "

    const-string v6, ""

    invoke-virtual {v4, v5, v6}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 114
    const-string v4, "\n\n"

    const-string v5, ""

    invoke-virtual {v2, v4, v5}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 118
    :cond_0
    iget-object v4, p0, Lcom/samsung/android/widget/memo/MemoViewsFactory;->mTitle:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/samsung/android/widget/memo/MemoViewsFactory;->voiceRecodData:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 119
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 120
    const-string v4, ""

    iput-object v4, p0, Lcom/samsung/android/widget/memo/MemoViewsFactory;->mTitle:Ljava/lang/String;

    .line 123
    :cond_1
    iget-object v4, p0, Lcom/samsung/android/widget/memo/MemoViewsFactory;->mTitle:Ljava/lang/String;

    invoke-direct {p0, v3, v4}, Lcom/samsung/android/widget/memo/MemoViewsFactory;->buildTitle(Landroid/widget/RemoteViews;Ljava/lang/String;)V

    .line 124
    iget-object v4, p0, Lcom/samsung/android/widget/memo/MemoViewsFactory;->mStrBody:Ljava/lang/String;

    if-eqz v4, :cond_2

    .line 125
    iget-object v4, p0, Lcom/samsung/android/widget/memo/MemoViewsFactory;->mStrBody:Ljava/lang/String;

    invoke-direct {p0, v3, v4}, Lcom/samsung/android/widget/memo/MemoViewsFactory;->buildView(Landroid/widget/RemoteViews;Ljava/lang/String;)V

    .line 128
    :cond_2
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 129
    .local v0, "extras":Landroid/os/Bundle;
    const-string v4, "com.samsung.android.widget.EXTRA_ITEM"

    invoke-virtual {v0, v4, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 130
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 131
    .local v1, "fillInIntent":Landroid/content/Intent;
    invoke-virtual {v1, v0}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 132
    const v4, 0x7f0e004c

    invoke-virtual {v3, v4, v1}, Landroid/widget/RemoteViews;->setOnClickFillInIntent(ILandroid/content/Intent;)V

    .line 133
    return-object v3
.end method

.method public getViewTypeCount()I
    .locals 1

    .prologue
    .line 384
    const/4 v0, 0x1

    return v0
.end method

.method public hasStableIds()Z
    .locals 1

    .prologue
    .line 394
    const/4 v0, 0x0

    return v0
.end method

.method public onCreate()V
    .locals 0

    .prologue
    .line 84
    return-void
.end method

.method public onDataSetChanged()V
    .locals 12

    .prologue
    .line 399
    sget-object v0, Lcom/samsung/android/widget/memo/MemoViewsFactory;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "mAppWidgetId : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/samsung/android/widget/memo/MemoViewsFactory;->mAppWidgetId:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 402
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v10

    .line 404
    .local v10, "identityToken":J
    const-wide/16 v8, -0x1

    .line 405
    .local v8, "id":J
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/widget/memo/MemoViewsFactory;->mContext:Landroid/content/Context;

    .line 406
    const-string v1, "widgetPref"

    const/4 v2, 0x0

    .line 405
    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v7

    .line 407
    .local v7, "widgetPref":Landroid/content/SharedPreferences;
    iget v0, p0, Lcom/samsung/android/widget/memo/MemoViewsFactory;->mAppWidgetId:I

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    .line 408
    const-wide/16 v2, -0x1

    .line 407
    invoke-interface {v7, v0, v2, v3}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v8

    .line 409
    iget-object v0, p0, Lcom/samsung/android/widget/memo/MemoViewsFactory;->mCursor:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    .line 410
    iget-object v0, p0, Lcom/samsung/android/widget/memo/MemoViewsFactory;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 412
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/widget/memo/MemoViewsFactory;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/provider/memo/MemoContract;->BASE_URI_MEMO:Landroid/net/Uri;

    .line 413
    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "content"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "title"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "vrfileUUID"

    aput-object v4, v2, v3

    const/4 v3, 0x3

    const-string v4, "strippedContent"

    aput-object v4, v2, v3

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "_id="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 414
    const/4 v4, 0x0

    const/4 v5, 0x0

    .line 412
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/widget/memo/MemoViewsFactory;->mCursor:Landroid/database/Cursor;

    .line 415
    iget-object v0, p0, Lcom/samsung/android/widget/memo/MemoViewsFactory;->mCursor:Landroid/database/Cursor;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/samsung/android/widget/memo/MemoViewsFactory;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-gtz v0, :cond_1

    .line 416
    invoke-interface {v7}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v6

    .line 417
    .local v6, "editor":Landroid/content/SharedPreferences$Editor;
    iget v0, p0, Lcom/samsung/android/widget/memo/MemoViewsFactory;->mAppWidgetId:I

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    .line 418
    const-wide/16 v2, -0x1

    .line 417
    invoke-interface {v6, v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 419
    invoke-interface {v6}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 420
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/widget/memo/MemoViewsFactory;->mTitle:Ljava/lang/String;

    .line 421
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/widget/memo/MemoViewsFactory;->mStrBody:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 428
    .end local v6    # "editor":Landroid/content/SharedPreferences$Editor;
    :cond_1
    invoke-static {v10, v11}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 430
    return-void

    .line 424
    .end local v7    # "widgetPref":Landroid/content/SharedPreferences;
    :catchall_0
    move-exception v0

    .line 428
    invoke-static {v10, v11}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 429
    throw v0
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/samsung/android/widget/memo/MemoViewsFactory;->mCursor:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    .line 89
    iget-object v0, p0, Lcom/samsung/android/widget/memo/MemoViewsFactory;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 91
    :cond_0
    return-void
.end method
