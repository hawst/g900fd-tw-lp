.class Lcom/samsung/safetyinformation/SettingsSafetyInformationActivity$InformationFileLoader;
.super Ljava/lang/Object;
.source "SettingsSafetyInformationActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/safetyinformation/SettingsSafetyInformationActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "InformationFileLoader"
.end annotation


# static fields
.field private static final INNER_TAG:Ljava/lang/String; = "SettingsSafetyInformationActivity.InformationFileLoader"

.field public static final STATUS_EMPTY_FILE:I = 0x3

.field public static final STATUS_NOT_FOUND:I = 0x1

.field public static final STATUS_OK:I = 0x0

.field public static final STATUS_READ_ERROR:I = 0x2


# instance fields
.field private mHandler:Landroid/os/Handler;


# direct methods
.method public constructor <init>(Landroid/os/Handler;)V
    .locals 0
    .param p1, "handler"    # Landroid/os/Handler;

    .prologue
    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 60
    iput-object p1, p0, Lcom/samsung/safetyinformation/SettingsSafetyInformationActivity$InformationFileLoader;->mHandler:Landroid/os/Handler;

    .line 61
    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 65
    const/4 v1, 0x0

    .line 67
    .local v1, "status":I
    iget-object v2, p0, Lcom/samsung/safetyinformation/SettingsSafetyInformationActivity$InformationFileLoader;->mHandler:Landroid/os/Handler;

    const/4 v3, 0x0

    invoke-virtual {v2, v1, v3}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 68
    .local v0, "msg":Landroid/os/Message;
    iget-object v2, p0, Lcom/samsung/safetyinformation/SettingsSafetyInformationActivity$InformationFileLoader;->mHandler:Landroid/os/Handler;

    invoke-virtual {v2, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 69
    return-void
.end method
