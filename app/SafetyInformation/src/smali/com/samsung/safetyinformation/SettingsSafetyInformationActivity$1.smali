.class Lcom/samsung/safetyinformation/SettingsSafetyInformationActivity$1;
.super Ljava/lang/Object;
.source "SettingsSafetyInformationActivity.java"

# interfaces
.implements Landroid/webkit/DownloadListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/safetyinformation/SettingsSafetyInformationActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/safetyinformation/SettingsSafetyInformationActivity;


# direct methods
.method constructor <init>(Lcom/samsung/safetyinformation/SettingsSafetyInformationActivity;)V
    .locals 0

    .prologue
    .line 88
    iput-object p1, p0, Lcom/samsung/safetyinformation/SettingsSafetyInformationActivity$1;->this$0:Lcom/samsung/safetyinformation/SettingsSafetyInformationActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDownloadStart(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;J)V
    .locals 6
    .param p1, "url"    # Ljava/lang/String;
    .param p2, "userAgent"    # Ljava/lang/String;
    .param p3, "contentDisposition"    # Ljava/lang/String;
    .param p4, "mimetype"    # Ljava/lang/String;
    .param p5, "contentLength"    # J

    .prologue
    .line 93
    const-string v4, ".pdf"

    invoke-virtual {p1, v4}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 94
    const-string v4, "/"

    invoke-virtual {p1, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 95
    .local v0, "filename":[Ljava/lang/String;
    if-eqz v0, :cond_0

    array-length v4, v0

    const/4 v5, 0x2

    if-le v4, v5, :cond_0

    .line 96
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    .line 98
    .local v3, "source":Landroid/net/Uri;
    new-instance v2, Landroid/app/DownloadManager$Request;

    invoke-direct {v2, v3}, Landroid/app/DownloadManager$Request;-><init>(Landroid/net/Uri;)V

    .line 100
    .local v2, "request":Landroid/app/DownloadManager$Request;
    sget-object v4, Landroid/os/Environment;->DIRECTORY_DOWNLOADS:Ljava/lang/String;

    array-length v5, v0

    add-int/lit8 v5, v5, -0x1

    aget-object v5, v0, v5

    invoke-virtual {v2, v4, v5}, Landroid/app/DownloadManager$Request;->setDestinationInExternalPublicDir(Ljava/lang/String;Ljava/lang/String;)Landroid/app/DownloadManager$Request;

    .line 102
    const/4 v4, 0x1

    invoke-virtual {v2, v4}, Landroid/app/DownloadManager$Request;->setNotificationVisibility(I)Landroid/app/DownloadManager$Request;

    .line 105
    iget-object v4, p0, Lcom/samsung/safetyinformation/SettingsSafetyInformationActivity$1;->this$0:Lcom/samsung/safetyinformation/SettingsSafetyInformationActivity;

    const-string v5, "download"

    invoke-virtual {v4, v5}, Lcom/samsung/safetyinformation/SettingsSafetyInformationActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/DownloadManager;

    .line 106
    .local v1, "manager":Landroid/app/DownloadManager;
    invoke-virtual {v1, v2}, Landroid/app/DownloadManager;->enqueue(Landroid/app/DownloadManager$Request;)J

    .line 109
    .end local v0    # "filename":[Ljava/lang/String;
    .end local v1    # "manager":Landroid/app/DownloadManager;
    .end local v2    # "request":Landroid/app/DownloadManager$Request;
    .end local v3    # "source":Landroid/net/Uri;
    :cond_0
    return-void
.end method
