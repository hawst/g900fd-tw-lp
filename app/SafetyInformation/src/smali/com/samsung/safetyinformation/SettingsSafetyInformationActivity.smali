.class public Lcom/samsung/safetyinformation/SettingsSafetyInformationActivity;
.super Landroid/app/Activity;
.source "SettingsSafetyInformationActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/safetyinformation/SettingsSafetyInformationActivity$InformationFileLoader;
    }
.end annotation


# static fields
.field private static final DEFAULT_SAFETYINFORMATION_PATH:Ljava/lang/String; = "file:///android_asset/safetyinformation/safetyinformation.html"

.field private static final LOGV:Z = false

.field private static final TAG:Ljava/lang/String; = "SettingsSafetyInformationActivity"


# instance fields
.field loadingFinished:Z

.field private mHandler:Landroid/os/Handler;

.field private mSpinnerDlg:Landroid/app/ProgressDialog;

.field private mWebView:Landroid/webkit/WebView;

.field redirect:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 73
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 155
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/safetyinformation/SettingsSafetyInformationActivity;->loadingFinished:Z

    .line 156
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/safetyinformation/SettingsSafetyInformationActivity;->redirect:Z

    .line 74
    iput-object v1, p0, Lcom/samsung/safetyinformation/SettingsSafetyInformationActivity;->mHandler:Landroid/os/Handler;

    .line 75
    iput-object v1, p0, Lcom/samsung/safetyinformation/SettingsSafetyInformationActivity;->mWebView:Landroid/webkit/WebView;

    .line 76
    iput-object v1, p0, Lcom/samsung/safetyinformation/SettingsSafetyInformationActivity;->mSpinnerDlg:Landroid/app/ProgressDialog;

    .line 77
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/safetyinformation/SettingsSafetyInformationActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/safetyinformation/SettingsSafetyInformationActivity;

    .prologue
    .line 38
    invoke-direct {p0}, Lcom/samsung/safetyinformation/SettingsSafetyInformationActivity;->showPageOfText()V

    return-void
.end method

.method static synthetic access$100(Lcom/samsung/safetyinformation/SettingsSafetyInformationActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/safetyinformation/SettingsSafetyInformationActivity;

    .prologue
    .line 38
    invoke-direct {p0}, Lcom/samsung/safetyinformation/SettingsSafetyInformationActivity;->showErrorAndFinish()V

    return-void
.end method

.method static synthetic access$200(Lcom/samsung/safetyinformation/SettingsSafetyInformationActivity;)Landroid/app/ProgressDialog;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/safetyinformation/SettingsSafetyInformationActivity;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/samsung/safetyinformation/SettingsSafetyInformationActivity;->mSpinnerDlg:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method private showErrorAndFinish()V
    .locals 2

    .prologue
    .line 190
    iget-object v0, p0, Lcom/samsung/safetyinformation/SettingsSafetyInformationActivity;->mSpinnerDlg:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 191
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/safetyinformation/SettingsSafetyInformationActivity;->mSpinnerDlg:Landroid/app/ProgressDialog;

    .line 192
    const v0, 0x7f030003

    const/4 v1, 0x1

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 194
    invoke-virtual {p0}, Lcom/samsung/safetyinformation/SettingsSafetyInformationActivity;->finish()V

    .line 195
    return-void
.end method

.method private showPageOfText()V
    .locals 2

    .prologue
    .line 161
    iget-object v0, p0, Lcom/samsung/safetyinformation/SettingsSafetyInformationActivity;->mWebView:Landroid/webkit/WebView;

    const-string v1, "file:///android_asset/safetyinformation/safetyinformation.html"

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    .line 162
    iget-object v0, p0, Lcom/samsung/safetyinformation/SettingsSafetyInformationActivity;->mWebView:Landroid/webkit/WebView;

    new-instance v1, Lcom/samsung/safetyinformation/SettingsSafetyInformationActivity$3;

    invoke-direct {v1, p0}, Lcom/samsung/safetyinformation/SettingsSafetyInformationActivity$3;-><init>(Lcom/samsung/safetyinformation/SettingsSafetyInformationActivity;)V

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 171
    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 7
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 81
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 83
    const/high16 v3, 0x7f020000

    invoke-virtual {p0, v3}, Lcom/samsung/safetyinformation/SettingsSafetyInformationActivity;->setContentView(I)V

    .line 84
    const/high16 v3, 0x7f050000

    invoke-virtual {p0, v3}, Lcom/samsung/safetyinformation/SettingsSafetyInformationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/webkit/WebView;

    iput-object v3, p0, Lcom/samsung/safetyinformation/SettingsSafetyInformationActivity;->mWebView:Landroid/webkit/WebView;

    .line 85
    iget-object v3, p0, Lcom/samsung/safetyinformation/SettingsSafetyInformationActivity;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v3}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v3

    invoke-virtual {v3, v6}, Landroid/webkit/WebSettings;->setJavaScriptEnabled(Z)V

    .line 86
    iget-object v3, p0, Lcom/samsung/safetyinformation/SettingsSafetyInformationActivity;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v3}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v3

    sget-object v4, Landroid/webkit/WebSettings$TextSize;->NORMAL:Landroid/webkit/WebSettings$TextSize;

    invoke-virtual {v3, v4}, Landroid/webkit/WebSettings;->setTextSize(Landroid/webkit/WebSettings$TextSize;)V

    .line 88
    iget-object v3, p0, Lcom/samsung/safetyinformation/SettingsSafetyInformationActivity;->mWebView:Landroid/webkit/WebView;

    new-instance v4, Lcom/samsung/safetyinformation/SettingsSafetyInformationActivity$1;

    invoke-direct {v4, p0}, Lcom/samsung/safetyinformation/SettingsSafetyInformationActivity$1;-><init>(Lcom/samsung/safetyinformation/SettingsSafetyInformationActivity;)V

    invoke-virtual {v3, v4}, Landroid/webkit/WebView;->setDownloadListener(Landroid/webkit/DownloadListener;)V

    .line 112
    if-eqz p1, :cond_0

    .line 113
    iget-object v3, p0, Lcom/samsung/safetyinformation/SettingsSafetyInformationActivity;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v3, p1}, Landroid/webkit/WebView;->restoreState(Landroid/os/Bundle;)Landroid/webkit/WebBackForwardList;

    .line 139
    :goto_0
    return-void

    .line 115
    :cond_0
    new-instance v3, Lcom/samsung/safetyinformation/SettingsSafetyInformationActivity$2;

    invoke-direct {v3, p0}, Lcom/samsung/safetyinformation/SettingsSafetyInformationActivity$2;-><init>(Lcom/samsung/safetyinformation/SettingsSafetyInformationActivity;)V

    iput-object v3, p0, Lcom/samsung/safetyinformation/SettingsSafetyInformationActivity;->mHandler:Landroid/os/Handler;

    .line 129
    const v3, 0x7f030001

    invoke-virtual {p0, v3}, Lcom/samsung/safetyinformation/SettingsSafetyInformationActivity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    .line 131
    .local v0, "msg":Ljava/lang/CharSequence;
    const/4 v3, 0x0

    invoke-static {p0, v3, v0, v6, v5}, Landroid/app/ProgressDialog;->show(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ZZ)Landroid/app/ProgressDialog;

    move-result-object v1

    .line 132
    .local v1, "pd":Landroid/app/ProgressDialog;
    invoke-virtual {v1, v5}, Landroid/app/ProgressDialog;->setProgressStyle(I)V

    .line 133
    iput-object v1, p0, Lcom/samsung/safetyinformation/SettingsSafetyInformationActivity;->mSpinnerDlg:Landroid/app/ProgressDialog;

    .line 136
    new-instance v2, Ljava/lang/Thread;

    new-instance v3, Lcom/samsung/safetyinformation/SettingsSafetyInformationActivity$InformationFileLoader;

    iget-object v4, p0, Lcom/samsung/safetyinformation/SettingsSafetyInformationActivity;->mHandler:Landroid/os/Handler;

    invoke-direct {v3, v4}, Lcom/samsung/safetyinformation/SettingsSafetyInformationActivity$InformationFileLoader;-><init>(Landroid/os/Handler;)V

    invoke-direct {v2, v3}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 137
    .local v2, "thread":Ljava/lang/Thread;
    invoke-virtual {v2}, Ljava/lang/Thread;->start()V

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 148
    iget-object v0, p0, Lcom/samsung/safetyinformation/SettingsSafetyInformationActivity;->mSpinnerDlg:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/safetyinformation/SettingsSafetyInformationActivity;->mSpinnerDlg:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 149
    iget-object v0, p0, Lcom/samsung/safetyinformation/SettingsSafetyInformationActivity;->mSpinnerDlg:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 151
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/safetyinformation/SettingsSafetyInformationActivity;->finish()V

    .line 152
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 153
    return-void
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 2
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 175
    const/4 v0, 0x0

    .line 178
    .local v0, "result":Z
    const/4 v1, 0x4

    if-ne p1, v1, :cond_0

    .line 179
    iget-object v1, p0, Lcom/samsung/safetyinformation/SettingsSafetyInformationActivity;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v1}, Landroid/webkit/WebView;->canGoBack()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 180
    iget-object v1, p0, Lcom/samsung/safetyinformation/SettingsSafetyInformationActivity;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v1}, Landroid/webkit/WebView;->goBack()V

    .line 181
    const/4 v0, 0x1

    .line 186
    :cond_0
    :goto_0
    return v0

    .line 183
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/safetyinformation/SettingsSafetyInformationActivity;->finish()V

    goto :goto_0
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 143
    iget-object v0, p0, Lcom/samsung/safetyinformation/SettingsSafetyInformationActivity;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v0, p1}, Landroid/webkit/WebView;->saveState(Landroid/os/Bundle;)Landroid/webkit/WebBackForwardList;

    .line 144
    return-void
.end method
