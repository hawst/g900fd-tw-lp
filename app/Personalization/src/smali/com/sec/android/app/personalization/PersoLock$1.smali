.class Lcom/sec/android/app/personalization/PersoLock$1;
.super Landroid/os/Handler;
.source "PersoLock.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/personalization/PersoLock;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/personalization/PersoLock;


# direct methods
.method constructor <init>(Lcom/sec/android/app/personalization/PersoLock;)V
    .locals 0

    .prologue
    .line 338
    iput-object p1, p0, Lcom/sec/android/app/personalization/PersoLock$1;->this$0:Lcom/sec/android/app/personalization/PersoLock;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 12
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const-wide/16 v10, 0x7d0

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 342
    iget v2, p1, Landroid/os/Message;->what:I

    const/16 v3, 0x64

    if-ne v2, v3, :cond_2

    .line 344
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Landroid/os/AsyncResult;

    .line 346
    .local v1, "res":Landroid/os/AsyncResult;
    iget-object v2, v1, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    if-eqz v2, :cond_1

    .line 349
    iget-object v2, p0, Lcom/sec/android/app/personalization/PersoLock$1;->this$0:Lcom/sec/android/app/personalization/PersoLock;

    const-string v3, "network perso lock request failure."

    # invokes: Lcom/sec/android/app/personalization/PersoLock;->log(Ljava/lang/String;)V
    invoke-static {v2, v3}, Lcom/sec/android/app/personalization/PersoLock;->access$000(Lcom/sec/android/app/personalization/PersoLock;Ljava/lang/String;)V

    .line 351
    iget-object v2, p0, Lcom/sec/android/app/personalization/PersoLock$1;->this$0:Lcom/sec/android/app/personalization/PersoLock;

    # invokes: Lcom/sec/android/app/personalization/PersoLock;->indicateError()V
    invoke-static {v2}, Lcom/sec/android/app/personalization/PersoLock;->access$100(Lcom/sec/android/app/personalization/PersoLock;)V

    .line 352
    new-instance v2, Lcom/sec/android/app/personalization/PersoLock$1$1;

    invoke-direct {v2, p0}, Lcom/sec/android/app/personalization/PersoLock$1$1;-><init>(Lcom/sec/android/app/personalization/PersoLock$1;)V

    invoke-virtual {p0, v2, v10, v11}, Lcom/sec/android/app/personalization/PersoLock$1;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 471
    .end local v1    # "res":Landroid/os/AsyncResult;
    :cond_0
    :goto_0
    return-void

    .line 367
    .restart local v1    # "res":Landroid/os/AsyncResult;
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/personalization/PersoLock$1;->this$0:Lcom/sec/android/app/personalization/PersoLock;

    const-string v3, "network perso lock SuCcess."

    # invokes: Lcom/sec/android/app/personalization/PersoLock;->log(Ljava/lang/String;)V
    invoke-static {v2, v3}, Lcom/sec/android/app/personalization/PersoLock;->access$000(Lcom/sec/android/app/personalization/PersoLock;Ljava/lang/String;)V

    .line 369
    iget-object v2, p0, Lcom/sec/android/app/personalization/PersoLock$1;->this$0:Lcom/sec/android/app/personalization/PersoLock;

    # invokes: Lcom/sec/android/app/personalization/PersoLock;->indicateSuccess()V
    invoke-static {v2}, Lcom/sec/android/app/personalization/PersoLock;->access$500(Lcom/sec/android/app/personalization/PersoLock;)V

    .line 370
    new-instance v2, Lcom/sec/android/app/personalization/PersoLock$1$2;

    invoke-direct {v2, p0}, Lcom/sec/android/app/personalization/PersoLock$1$2;-><init>(Lcom/sec/android/app/personalization/PersoLock$1;)V

    invoke-virtual {p0, v2, v10, v11}, Lcom/sec/android/app/personalization/PersoLock$1;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .line 382
    .end local v1    # "res":Landroid/os/AsyncResult;
    :cond_2
    iget v2, p1, Landroid/os/Message;->what:I

    const/16 v3, 0x65

    if-ne v2, v3, :cond_4

    .line 384
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Landroid/os/AsyncResult;

    .line 386
    .restart local v1    # "res":Landroid/os/AsyncResult;
    iget-object v2, v1, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    if-eqz v2, :cond_3

    .line 389
    iget-object v2, p0, Lcom/sec/android/app/personalization/PersoLock$1;->this$0:Lcom/sec/android/app/personalization/PersoLock;

    const-string v3, "network perso unlock request failure."

    # invokes: Lcom/sec/android/app/personalization/PersoLock;->log(Ljava/lang/String;)V
    invoke-static {v2, v3}, Lcom/sec/android/app/personalization/PersoLock;->access$000(Lcom/sec/android/app/personalization/PersoLock;Ljava/lang/String;)V

    .line 391
    iget-object v2, p0, Lcom/sec/android/app/personalization/PersoLock$1;->this$0:Lcom/sec/android/app/personalization/PersoLock;

    # invokes: Lcom/sec/android/app/personalization/PersoLock;->indicateError()V
    invoke-static {v2}, Lcom/sec/android/app/personalization/PersoLock;->access$100(Lcom/sec/android/app/personalization/PersoLock;)V

    .line 392
    new-instance v2, Lcom/sec/android/app/personalization/PersoLock$1$3;

    invoke-direct {v2, p0}, Lcom/sec/android/app/personalization/PersoLock$1$3;-><init>(Lcom/sec/android/app/personalization/PersoLock$1;)V

    const-wide/16 v4, 0xbb8

    invoke-virtual {p0, v2, v4, v5}, Lcom/sec/android/app/personalization/PersoLock$1;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .line 405
    :cond_3
    iget-object v2, p0, Lcom/sec/android/app/personalization/PersoLock$1;->this$0:Lcom/sec/android/app/personalization/PersoLock;

    const-string v3, "network perso unlock success."

    # invokes: Lcom/sec/android/app/personalization/PersoLock;->log(Ljava/lang/String;)V
    invoke-static {v2, v3}, Lcom/sec/android/app/personalization/PersoLock;->access$000(Lcom/sec/android/app/personalization/PersoLock;Ljava/lang/String;)V

    .line 407
    iget-object v2, p0, Lcom/sec/android/app/personalization/PersoLock$1;->this$0:Lcom/sec/android/app/personalization/PersoLock;

    # invokes: Lcom/sec/android/app/personalization/PersoLock;->indicateSuccess()V
    invoke-static {v2}, Lcom/sec/android/app/personalization/PersoLock;->access$500(Lcom/sec/android/app/personalization/PersoLock;)V

    .line 408
    new-instance v2, Lcom/sec/android/app/personalization/PersoLock$1$4;

    invoke-direct {v2, p0}, Lcom/sec/android/app/personalization/PersoLock$1$4;-><init>(Lcom/sec/android/app/personalization/PersoLock$1;)V

    const-wide/16 v4, 0xbb8

    invoke-virtual {p0, v2, v4, v5}, Lcom/sec/android/app/personalization/PersoLock$1;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .line 417
    .end local v1    # "res":Landroid/os/AsyncResult;
    :cond_4
    iget v2, p1, Landroid/os/Message;->what:I

    const/16 v3, 0x66

    if-ne v2, v3, :cond_0

    .line 419
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Landroid/os/AsyncResult;

    .line 421
    .restart local v1    # "res":Landroid/os/AsyncResult;
    iget-object v2, v1, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    if-eqz v2, :cond_5

    .line 424
    iget-object v2, p0, Lcom/sec/android/app/personalization/PersoLock$1;->this$0:Lcom/sec/android/app/personalization/PersoLock;

    const-string v3, "get status failure."

    # invokes: Lcom/sec/android/app/personalization/PersoLock;->log(Ljava/lang/String;)V
    invoke-static {v2, v3}, Lcom/sec/android/app/personalization/PersoLock;->access$000(Lcom/sec/android/app/personalization/PersoLock;Ljava/lang/String;)V

    goto :goto_0

    .line 429
    :cond_5
    iget-object v2, p0, Lcom/sec/android/app/personalization/PersoLock$1;->this$0:Lcom/sec/android/app/personalization/PersoLock;

    const-string v3, "get status success"

    # invokes: Lcom/sec/android/app/personalization/PersoLock;->log(Ljava/lang/String;)V
    invoke-static {v2, v3}, Lcom/sec/android/app/personalization/PersoLock;->access$000(Lcom/sec/android/app/personalization/PersoLock;Ljava/lang/String;)V

    .line 430
    iget-object v2, v1, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    check-cast v2, [B

    move-object v0, v2

    check-cast v0, [B

    .line 432
    .local v0, "Buf":[B
    if-eqz v0, :cond_a

    .line 434
    iget-object v2, p0, Lcom/sec/android/app/personalization/PersoLock$1;->this$0:Lcom/sec/android/app/personalization/PersoLock;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Buf[0] : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    aget-byte v4, v0, v6

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    # invokes: Lcom/sec/android/app/personalization/PersoLock;->log(Ljava/lang/String;)V
    invoke-static {v2, v3}, Lcom/sec/android/app/personalization/PersoLock;->access$000(Lcom/sec/android/app/personalization/PersoLock;Ljava/lang/String;)V

    .line 435
    iget-object v2, p0, Lcom/sec/android/app/personalization/PersoLock$1;->this$0:Lcom/sec/android/app/personalization/PersoLock;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Buf[1] : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    aget-byte v4, v0, v5

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    # invokes: Lcom/sec/android/app/personalization/PersoLock;->log(Ljava/lang/String;)V
    invoke-static {v2, v3}, Lcom/sec/android/app/personalization/PersoLock;->access$000(Lcom/sec/android/app/personalization/PersoLock;Ljava/lang/String;)V

    .line 436
    iget-object v2, p0, Lcom/sec/android/app/personalization/PersoLock$1;->this$0:Lcom/sec/android/app/personalization/PersoLock;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Buf[2] : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    aget-byte v4, v0, v7

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    # invokes: Lcom/sec/android/app/personalization/PersoLock;->log(Ljava/lang/String;)V
    invoke-static {v2, v3}, Lcom/sec/android/app/personalization/PersoLock;->access$000(Lcom/sec/android/app/personalization/PersoLock;Ljava/lang/String;)V

    .line 437
    iget-object v2, p0, Lcom/sec/android/app/personalization/PersoLock$1;->this$0:Lcom/sec/android/app/personalization/PersoLock;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Buf[3] : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    aget-byte v4, v0, v8

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    # invokes: Lcom/sec/android/app/personalization/PersoLock;->log(Ljava/lang/String;)V
    invoke-static {v2, v3}, Lcom/sec/android/app/personalization/PersoLock;->access$000(Lcom/sec/android/app/personalization/PersoLock;Ljava/lang/String;)V

    .line 439
    iget-object v3, p0, Lcom/sec/android/app/personalization/PersoLock$1;->this$0:Lcom/sec/android/app/personalization/PersoLock;

    iget-object v2, p0, Lcom/sec/android/app/personalization/PersoLock$1;->this$0:Lcom/sec/android/app/personalization/PersoLock;

    const v4, 0x7f05000e

    invoke-virtual {v2, v4}, Lcom/sec/android/app/personalization/PersoLock;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    # setter for: Lcom/sec/android/app/personalization/PersoLock;->mNpStatus:Landroid/widget/TextView;
    invoke-static {v3, v2}, Lcom/sec/android/app/personalization/PersoLock;->access$602(Lcom/sec/android/app/personalization/PersoLock;Landroid/widget/TextView;)Landroid/widget/TextView;

    .line 440
    iget-object v3, p0, Lcom/sec/android/app/personalization/PersoLock$1;->this$0:Lcom/sec/android/app/personalization/PersoLock;

    iget-object v2, p0, Lcom/sec/android/app/personalization/PersoLock$1;->this$0:Lcom/sec/android/app/personalization/PersoLock;

    const v4, 0x7f05000f

    invoke-virtual {v2, v4}, Lcom/sec/android/app/personalization/PersoLock;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    # setter for: Lcom/sec/android/app/personalization/PersoLock;->mNspStatus:Landroid/widget/TextView;
    invoke-static {v3, v2}, Lcom/sec/android/app/personalization/PersoLock;->access$702(Lcom/sec/android/app/personalization/PersoLock;Landroid/widget/TextView;)Landroid/widget/TextView;

    .line 441
    iget-object v3, p0, Lcom/sec/android/app/personalization/PersoLock$1;->this$0:Lcom/sec/android/app/personalization/PersoLock;

    iget-object v2, p0, Lcom/sec/android/app/personalization/PersoLock$1;->this$0:Lcom/sec/android/app/personalization/PersoLock;

    const v4, 0x7f050010

    invoke-virtual {v2, v4}, Lcom/sec/android/app/personalization/PersoLock;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    # setter for: Lcom/sec/android/app/personalization/PersoLock;->mSpStatus:Landroid/widget/TextView;
    invoke-static {v3, v2}, Lcom/sec/android/app/personalization/PersoLock;->access$802(Lcom/sec/android/app/personalization/PersoLock;Landroid/widget/TextView;)Landroid/widget/TextView;

    .line 442
    iget-object v3, p0, Lcom/sec/android/app/personalization/PersoLock$1;->this$0:Lcom/sec/android/app/personalization/PersoLock;

    iget-object v2, p0, Lcom/sec/android/app/personalization/PersoLock$1;->this$0:Lcom/sec/android/app/personalization/PersoLock;

    const v4, 0x7f050011

    invoke-virtual {v2, v4}, Lcom/sec/android/app/personalization/PersoLock;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    # setter for: Lcom/sec/android/app/personalization/PersoLock;->mCpStatus:Landroid/widget/TextView;
    invoke-static {v3, v2}, Lcom/sec/android/app/personalization/PersoLock;->access$902(Lcom/sec/android/app/personalization/PersoLock;Landroid/widget/TextView;)Landroid/widget/TextView;

    .line 444
    aget-byte v2, v0, v6

    if-ne v2, v5, :cond_6

    .line 445
    iget-object v2, p0, Lcom/sec/android/app/personalization/PersoLock$1;->this$0:Lcom/sec/android/app/personalization/PersoLock;

    # getter for: Lcom/sec/android/app/personalization/PersoLock;->mNpStatus:Landroid/widget/TextView;
    invoke-static {v2}, Lcom/sec/android/app/personalization/PersoLock;->access$600(Lcom/sec/android/app/personalization/PersoLock;)Landroid/widget/TextView;

    move-result-object v2

    const-string v3, "[ON]"

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 449
    :goto_1
    aget-byte v2, v0, v5

    if-ne v2, v5, :cond_7

    .line 450
    iget-object v2, p0, Lcom/sec/android/app/personalization/PersoLock$1;->this$0:Lcom/sec/android/app/personalization/PersoLock;

    # getter for: Lcom/sec/android/app/personalization/PersoLock;->mNspStatus:Landroid/widget/TextView;
    invoke-static {v2}, Lcom/sec/android/app/personalization/PersoLock;->access$700(Lcom/sec/android/app/personalization/PersoLock;)Landroid/widget/TextView;

    move-result-object v2

    const-string v3, "[ON]"

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 454
    :goto_2
    aget-byte v2, v0, v7

    if-ne v2, v5, :cond_8

    .line 455
    iget-object v2, p0, Lcom/sec/android/app/personalization/PersoLock$1;->this$0:Lcom/sec/android/app/personalization/PersoLock;

    # getter for: Lcom/sec/android/app/personalization/PersoLock;->mSpStatus:Landroid/widget/TextView;
    invoke-static {v2}, Lcom/sec/android/app/personalization/PersoLock;->access$800(Lcom/sec/android/app/personalization/PersoLock;)Landroid/widget/TextView;

    move-result-object v2

    const-string v3, "[ON]"

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 459
    :goto_3
    aget-byte v2, v0, v8

    if-ne v2, v5, :cond_9

    .line 460
    iget-object v2, p0, Lcom/sec/android/app/personalization/PersoLock$1;->this$0:Lcom/sec/android/app/personalization/PersoLock;

    # getter for: Lcom/sec/android/app/personalization/PersoLock;->mCpStatus:Landroid/widget/TextView;
    invoke-static {v2}, Lcom/sec/android/app/personalization/PersoLock;->access$900(Lcom/sec/android/app/personalization/PersoLock;)Landroid/widget/TextView;

    move-result-object v2

    const-string v3, "[ON]"

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 447
    :cond_6
    iget-object v2, p0, Lcom/sec/android/app/personalization/PersoLock$1;->this$0:Lcom/sec/android/app/personalization/PersoLock;

    # getter for: Lcom/sec/android/app/personalization/PersoLock;->mNpStatus:Landroid/widget/TextView;
    invoke-static {v2}, Lcom/sec/android/app/personalization/PersoLock;->access$600(Lcom/sec/android/app/personalization/PersoLock;)Landroid/widget/TextView;

    move-result-object v2

    const-string v3, "[OFF]"

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 452
    :cond_7
    iget-object v2, p0, Lcom/sec/android/app/personalization/PersoLock$1;->this$0:Lcom/sec/android/app/personalization/PersoLock;

    # getter for: Lcom/sec/android/app/personalization/PersoLock;->mNspStatus:Landroid/widget/TextView;
    invoke-static {v2}, Lcom/sec/android/app/personalization/PersoLock;->access$700(Lcom/sec/android/app/personalization/PersoLock;)Landroid/widget/TextView;

    move-result-object v2

    const-string v3, "[OFF]"

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_2

    .line 457
    :cond_8
    iget-object v2, p0, Lcom/sec/android/app/personalization/PersoLock$1;->this$0:Lcom/sec/android/app/personalization/PersoLock;

    # getter for: Lcom/sec/android/app/personalization/PersoLock;->mSpStatus:Landroid/widget/TextView;
    invoke-static {v2}, Lcom/sec/android/app/personalization/PersoLock;->access$800(Lcom/sec/android/app/personalization/PersoLock;)Landroid/widget/TextView;

    move-result-object v2

    const-string v3, "[OFF]"

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_3

    .line 462
    :cond_9
    iget-object v2, p0, Lcom/sec/android/app/personalization/PersoLock$1;->this$0:Lcom/sec/android/app/personalization/PersoLock;

    # getter for: Lcom/sec/android/app/personalization/PersoLock;->mCpStatus:Landroid/widget/TextView;
    invoke-static {v2}, Lcom/sec/android/app/personalization/PersoLock;->access$900(Lcom/sec/android/app/personalization/PersoLock;)Landroid/widget/TextView;

    move-result-object v2

    const-string v3, "[OFF]"

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 467
    :cond_a
    iget-object v2, p0, Lcom/sec/android/app/personalization/PersoLock$1;->this$0:Lcom/sec/android/app/personalization/PersoLock;

    const-string v3, "Buf is Null."

    # invokes: Lcom/sec/android/app/personalization/PersoLock;->log(Ljava/lang/String;)V
    invoke-static {v2, v3}, Lcom/sec/android/app/personalization/PersoLock;->access$000(Lcom/sec/android/app/personalization/PersoLock;Ljava/lang/String;)V

    goto/16 :goto_0
.end method
