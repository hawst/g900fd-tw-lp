.class public Lcom/sec/android/app/personalization/Personalization;
.super Landroid/app/Activity;
.source "Personalization.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method

.method private log(Ljava/lang/String;)V
    .locals 3
    .param p1, "msg"    # Ljava/lang/String;

    .prologue
    .line 120
    const-string v0, "Personalization"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[SimNetworklock] "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 121
    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 8
    .param p1, "icicle"    # Landroid/os/Bundle;

    .prologue
    .line 47
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 49
    const/4 v2, 0x0

    .line 50
    .local v2, "PersoType":I
    const/4 v0, 0x0

    .line 52
    .local v0, "Active_Lock":I
    invoke-virtual {p0}, Lcom/sec/android/app/personalization/Personalization;->getIntent()Landroid/content/Intent;

    move-result-object v3

    .line 53
    .local v3, "intent":Landroid/content/Intent;
    const-string v6, "keyString"

    invoke-virtual {v3, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 54
    .local v4, "keyString":Ljava/lang/String;
    const-string v6, "scheme"

    invoke-virtual {v3, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 56
    .local v5, "scheme":Ljava/lang/String;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "keyString is "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {p0, v6}, Lcom/sec/android/app/personalization/Personalization;->log(Ljava/lang/String;)V

    .line 57
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "scheme is "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {p0, v6}, Lcom/sec/android/app/personalization/Personalization;->log(Ljava/lang/String;)V

    .line 59
    if-eqz v4, :cond_0

    .line 60
    const-string v6, "7465625"

    invoke-virtual {v4, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 62
    const-string v6, "SEC_PRODUCT_FEATURE_RIL_USE_SECRIL_WITH_QCRIL is false"

    invoke-direct {p0, v6}, Lcom/sec/android/app/personalization/Personalization;->log(Ljava/lang/String;)V

    .line 67
    const/4 v0, 0x2

    .line 108
    :cond_0
    :goto_0
    new-instance v1, Lcom/sec/android/app/personalization/PersoLock;

    invoke-direct {v1, p0}, Lcom/sec/android/app/personalization/PersoLock;-><init>(Landroid/content/Context;)V

    .line 111
    .local v1, "Panel":Lcom/sec/android/app/personalization/PersoLock;
    iput v2, v1, Lcom/sec/android/app/personalization/PersoLock;->mPersoType:I

    .line 112
    iput v0, v1, Lcom/sec/android/app/personalization/PersoLock;->mIsLock:I

    .line 113
    iput-object p0, v1, Lcom/sec/android/app/personalization/PersoLock;->mParentActivity:Landroid/app/Activity;

    .line 115
    invoke-virtual {v1}, Lcom/sec/android/app/personalization/PersoLock;->show()V

    .line 117
    return-void

    .line 70
    .end local v1    # "Panel":Lcom/sec/android/app/personalization/PersoLock;
    :cond_1
    const-string v6, "7465625*638*"

    invoke-virtual {v4, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 72
    const/4 v2, 0x5

    .line 74
    const-string v6, "android_perso_lock_code"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 75
    const/4 v0, 0x1

    goto :goto_0

    .line 77
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 79
    :cond_3
    const-string v6, "7465625*782*"

    invoke-virtual {v4, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 81
    const/4 v2, 0x6

    .line 83
    const-string v6, "android_perso_lock_code"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 84
    const/4 v0, 0x1

    goto :goto_0

    .line 86
    :cond_4
    const/4 v0, 0x0

    goto :goto_0

    .line 89
    :cond_5
    const-string v6, "7465625*77*"

    invoke-virtual {v4, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 91
    const/4 v2, 0x7

    .line 93
    const-string v6, "android_perso_lock_code"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 94
    const/4 v0, 0x1

    goto :goto_0

    .line 96
    :cond_6
    const/4 v0, 0x0

    goto :goto_0

    .line 98
    :cond_7
    const-string v6, "7465625*27*"

    invoke-virtual {v4, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 100
    const/16 v2, 0x8

    .line 102
    const-string v6, "android_perso_lock_code"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_8

    .line 103
    const/4 v0, 0x1

    goto :goto_0

    .line 105
    :cond_8
    const/4 v0, 0x0

    goto :goto_0
.end method
