.class public Lcom/sec/android/app/personalization/PersoLock;
.super Landroid/app/Dialog;
.source "PersoLock.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/personalization/PersoLock$OemCommands;
    }
.end annotation


# instance fields
.field final REQ_GET_LOCK_STATUS:C

.field final REQ_LOCK_MODE:C

.field final REQ_UNLOCK_MODE:C

.field private mCPEntry:Landroid/widget/EditText;

.field private mCPText:Landroid/widget/TextView;

.field private mControlKeyEntry:Landroid/widget/EditText;

.field private mCpStatus:Landroid/widget/TextView;

.field private mDismissButton:Landroid/widget/Button;

.field mDismissListener:Landroid/view/View$OnClickListener;

.field private mEntryPanel:Landroid/widget/LinearLayout;

.field private mHandler:Landroid/os/Handler;

.field public mIsLock:I

.field private mMccMncEntry:Landroid/widget/EditText;

.field private mNSPEntry:Landroid/widget/EditText;

.field private mNSPText:Landroid/widget/TextView;

.field private mNpStatus:Landroid/widget/TextView;

.field private mNspStatus:Landroid/widget/TextView;

.field private mOem:Lcom/sec/android/app/personalization/PersoLock$OemCommands;

.field public mParentActivity:Landroid/app/Activity;

.field public mPersoType:I

.field private mPersoTypeText:Landroid/widget/TextView;

.field private mPhone:Lcom/android/internal/telephony/Phone;

.field private mSpStatus:Landroid/widget/TextView;

.field private mStatusPanel:Landroid/widget/LinearLayout;

.field private mStatusText:Landroid/widget/TextView;

.field private mUnlockText:Landroid/widget/TextView;

.field private mlockButton:Landroid/widget/Button;

.field mlockListener:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    .line 476
    invoke-direct {p0, p1}, Landroid/app/Dialog;-><init>(Landroid/content/Context;)V

    .line 37
    iput-char v1, p0, Lcom/sec/android/app/personalization/PersoLock;->REQ_UNLOCK_MODE:C

    .line 38
    const/4 v0, 0x1

    iput-char v0, p0, Lcom/sec/android/app/personalization/PersoLock;->REQ_LOCK_MODE:C

    .line 39
    const/4 v0, 0x2

    iput-char v0, p0, Lcom/sec/android/app/personalization/PersoLock;->REQ_GET_LOCK_STATUS:C

    .line 48
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/personalization/PersoLock;->mOem:Lcom/sec/android/app/personalization/PersoLock$OemCommands;

    .line 50
    iput v1, p0, Lcom/sec/android/app/personalization/PersoLock;->mPersoType:I

    .line 51
    iput v1, p0, Lcom/sec/android/app/personalization/PersoLock;->mIsLock:I

    .line 338
    new-instance v0, Lcom/sec/android/app/personalization/PersoLock$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/personalization/PersoLock$1;-><init>(Lcom/sec/android/app/personalization/PersoLock;)V

    iput-object v0, p0, Lcom/sec/android/app/personalization/PersoLock;->mHandler:Landroid/os/Handler;

    .line 523
    new-instance v0, Lcom/sec/android/app/personalization/PersoLock$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/personalization/PersoLock$2;-><init>(Lcom/sec/android/app/personalization/PersoLock;)V

    iput-object v0, p0, Lcom/sec/android/app/personalization/PersoLock;->mlockListener:Landroid/view/View$OnClickListener;

    .line 807
    new-instance v0, Lcom/sec/android/app/personalization/PersoLock$3;

    invoke-direct {v0, p0}, Lcom/sec/android/app/personalization/PersoLock$3;-><init>(Lcom/sec/android/app/personalization/PersoLock;)V

    iput-object v0, p0, Lcom/sec/android/app/personalization/PersoLock;->mDismissListener:Landroid/view/View$OnClickListener;

    .line 477
    return-void
.end method

.method private SendGetStatusData()V
    .locals 4

    .prologue
    .line 758
    const-string v1, "SendGetStatusData"

    invoke-direct {p0, v1}, Lcom/sec/android/app/personalization/PersoLock;->log(Ljava/lang/String;)V

    .line 759
    const/4 v0, 0x0

    .line 761
    .local v0, "data":[B
    iget-object v1, p0, Lcom/sec/android/app/personalization/PersoLock;->mOem:Lcom/sec/android/app/personalization/PersoLock$OemCommands;

    invoke-virtual {v1}, Lcom/sec/android/app/personalization/PersoLock$OemCommands;->PersoGetStatusData()[B

    move-result-object v0

    .line 763
    if-nez v0, :cond_0

    .line 765
    const-string v1, "err - data is NULL"

    invoke-direct {p0, v1}, Lcom/sec/android/app/personalization/PersoLock;->log(Ljava/lang/String;)V

    .line 770
    :goto_0
    return-void

    .line 769
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/personalization/PersoLock;->mPhone:Lcom/android/internal/telephony/Phone;

    iget-object v2, p0, Lcom/sec/android/app/personalization/PersoLock;->mHandler:Landroid/os/Handler;

    const/16 v3, 0x66

    invoke-virtual {v2, v3}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Lcom/android/internal/telephony/Phone;->invokeOemRilRequestRaw([BLandroid/os/Message;)V

    goto :goto_0
.end method

.method private SendLockData()V
    .locals 8

    .prologue
    const/4 v2, 0x1

    .line 704
    const/4 v7, 0x0

    .line 706
    .local v7, "data":[B
    iget v0, p0, Lcom/sec/android/app/personalization/PersoLock;->mPersoType:I

    packed-switch v0, :pswitch_data_0

    .line 726
    const-string v0, "err - switch;default"

    invoke-direct {p0, v0}, Lcom/sec/android/app/personalization/PersoLock;->log(Ljava/lang/String;)V

    .line 731
    :goto_0
    if-nez v7, :cond_0

    .line 733
    const-string v0, "err - data is NULL"

    invoke-direct {p0, v0}, Lcom/sec/android/app/personalization/PersoLock;->log(Ljava/lang/String;)V

    .line 738
    :goto_1
    return-void

    .line 709
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/personalization/PersoLock;->mOem:Lcom/sec/android/app/personalization/PersoLock$OemCommands;

    iget-object v1, p0, Lcom/sec/android/app/personalization/PersoLock;->mOem:Lcom/sec/android/app/personalization/PersoLock$OemCommands;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v1, 0x5

    iget-object v3, p0, Lcom/sec/android/app/personalization/PersoLock;->mOem:Lcom/sec/android/app/personalization/PersoLock$OemCommands;

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    iget-object v3, p0, Lcom/sec/android/app/personalization/PersoLock;->mMccMncEntry:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/personalization/PersoLock;->mControlKeyEntry:Landroid/widget/EditText;

    invoke-virtual {v4}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/sec/android/app/personalization/PersoLock$OemCommands;->PersoNWLockData(CCLjava/lang/String;Ljava/lang/String;)[B

    move-result-object v7

    .line 711
    goto :goto_0

    .line 713
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/personalization/PersoLock;->mOem:Lcom/sec/android/app/personalization/PersoLock$OemCommands;

    iget-object v1, p0, Lcom/sec/android/app/personalization/PersoLock;->mOem:Lcom/sec/android/app/personalization/PersoLock$OemCommands;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v1, 0x6

    iget-object v3, p0, Lcom/sec/android/app/personalization/PersoLock;->mOem:Lcom/sec/android/app/personalization/PersoLock$OemCommands;

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    iget-object v3, p0, Lcom/sec/android/app/personalization/PersoLock;->mMccMncEntry:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/personalization/PersoLock;->mNSPEntry:Landroid/widget/EditText;

    invoke-virtual {v4}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/personalization/PersoLock;->mControlKeyEntry:Landroid/widget/EditText;

    invoke-virtual {v5}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/app/personalization/PersoLock$OemCommands;->PersoSUBLockData(CCLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)[B

    move-result-object v7

    .line 715
    goto :goto_0

    .line 717
    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/app/personalization/PersoLock;->mOem:Lcom/sec/android/app/personalization/PersoLock$OemCommands;

    iget-object v1, p0, Lcom/sec/android/app/personalization/PersoLock;->mOem:Lcom/sec/android/app/personalization/PersoLock$OemCommands;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v1, 0x7

    iget-object v3, p0, Lcom/sec/android/app/personalization/PersoLock;->mOem:Lcom/sec/android/app/personalization/PersoLock$OemCommands;

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    iget-object v3, p0, Lcom/sec/android/app/personalization/PersoLock;->mMccMncEntry:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/personalization/PersoLock;->mNSPEntry:Landroid/widget/EditText;

    invoke-virtual {v4}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/personalization/PersoLock;->mControlKeyEntry:Landroid/widget/EditText;

    invoke-virtual {v5}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/app/personalization/PersoLock$OemCommands;->PersoSPLockData(CCLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)[B

    move-result-object v7

    .line 719
    goto/16 :goto_0

    .line 721
    :pswitch_3
    iget-object v0, p0, Lcom/sec/android/app/personalization/PersoLock;->mOem:Lcom/sec/android/app/personalization/PersoLock$OemCommands;

    iget-object v1, p0, Lcom/sec/android/app/personalization/PersoLock;->mOem:Lcom/sec/android/app/personalization/PersoLock$OemCommands;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/16 v1, 0x8

    iget-object v3, p0, Lcom/sec/android/app/personalization/PersoLock;->mOem:Lcom/sec/android/app/personalization/PersoLock$OemCommands;

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    iget-object v3, p0, Lcom/sec/android/app/personalization/PersoLock;->mMccMncEntry:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/personalization/PersoLock;->mNSPEntry:Landroid/widget/EditText;

    invoke-virtual {v4}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/personalization/PersoLock;->mCPEntry:Landroid/widget/EditText;

    invoke-virtual {v5}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/personalization/PersoLock;->mControlKeyEntry:Landroid/widget/EditText;

    invoke-virtual {v6}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual/range {v0 .. v6}, Lcom/sec/android/app/personalization/PersoLock$OemCommands;->PersoCPLockData(CCLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)[B

    move-result-object v7

    .line 723
    goto/16 :goto_0

    .line 737
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/personalization/PersoLock;->mPhone:Lcom/android/internal/telephony/Phone;

    iget-object v1, p0, Lcom/sec/android/app/personalization/PersoLock;->mHandler:Landroid/os/Handler;

    const/16 v2, 0x64

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-interface {v0, v7, v1}, Lcom/android/internal/telephony/Phone;->invokeOemRilRequestRaw([BLandroid/os/Message;)V

    goto/16 :goto_1

    .line 706
    :pswitch_data_0
    .packed-switch 0x5
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private SendUnLockData()V
    .locals 4

    .prologue
    .line 743
    const/4 v0, 0x0

    .line 745
    .local v0, "data":[B
    iget-object v1, p0, Lcom/sec/android/app/personalization/PersoLock;->mOem:Lcom/sec/android/app/personalization/PersoLock$OemCommands;

    iget v2, p0, Lcom/sec/android/app/personalization/PersoLock;->mPersoType:I

    int-to-char v2, v2

    iget-object v3, p0, Lcom/sec/android/app/personalization/PersoLock;->mControlKeyEntry:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/personalization/PersoLock$OemCommands;->PersoUnLockData(CLjava/lang/String;)[B

    move-result-object v0

    .line 747
    if-nez v0, :cond_0

    .line 749
    const-string v1, "err - data is NULL"

    invoke-direct {p0, v1}, Lcom/sec/android/app/personalization/PersoLock;->log(Ljava/lang/String;)V

    .line 754
    :goto_0
    return-void

    .line 753
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/personalization/PersoLock;->mPhone:Lcom/android/internal/telephony/Phone;

    iget-object v2, p0, Lcom/sec/android/app/personalization/PersoLock;->mHandler:Landroid/os/Handler;

    const/16 v3, 0x65

    invoke-virtual {v2, v3}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Lcom/android/internal/telephony/Phone;->invokeOemRilRequestRaw([BLandroid/os/Message;)V

    goto :goto_0
.end method

.method private ShowLockPanel()V
    .locals 3

    .prologue
    const/16 v2, 0x8

    .line 569
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "P type is "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/sec/android/app/personalization/PersoLock;->mPersoType:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/android/app/personalization/PersoLock;->log(Ljava/lang/String;)V

    .line 572
    const v0, 0x7f050001

    invoke-virtual {p0, v0}, Lcom/sec/android/app/personalization/PersoLock;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/personalization/PersoLock;->mPersoTypeText:Landroid/widget/TextView;

    .line 573
    const v0, 0x7f050003

    invoke-virtual {p0, v0}, Lcom/sec/android/app/personalization/PersoLock;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/personalization/PersoLock;->mNSPText:Landroid/widget/TextView;

    .line 574
    const v0, 0x7f050005

    invoke-virtual {p0, v0}, Lcom/sec/android/app/personalization/PersoLock;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/personalization/PersoLock;->mCPText:Landroid/widget/TextView;

    .line 576
    const v0, 0x7f050002

    invoke-virtual {p0, v0}, Lcom/sec/android/app/personalization/PersoLock;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/sec/android/app/personalization/PersoLock;->mMccMncEntry:Landroid/widget/EditText;

    .line 577
    iget-object v0, p0, Lcom/sec/android/app/personalization/PersoLock;->mMccMncEntry:Landroid/widget/EditText;

    invoke-static {}, Landroid/text/method/DialerKeyListener;->getInstance()Landroid/text/method/DialerKeyListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setKeyListener(Landroid/text/method/KeyListener;)V

    .line 578
    iget-object v0, p0, Lcom/sec/android/app/personalization/PersoLock;->mMccMncEntry:Landroid/widget/EditText;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setInputType(I)V

    .line 579
    iget-object v0, p0, Lcom/sec/android/app/personalization/PersoLock;->mMccMncEntry:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/sec/android/app/personalization/PersoLock;->mlockListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 581
    const v0, 0x7f050004

    invoke-virtual {p0, v0}, Lcom/sec/android/app/personalization/PersoLock;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/sec/android/app/personalization/PersoLock;->mNSPEntry:Landroid/widget/EditText;

    .line 582
    const v0, 0x7f050006

    invoke-virtual {p0, v0}, Lcom/sec/android/app/personalization/PersoLock;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/sec/android/app/personalization/PersoLock;->mCPEntry:Landroid/widget/EditText;

    .line 584
    iget v0, p0, Lcom/sec/android/app/personalization/PersoLock;->mPersoType:I

    iget-object v1, p0, Lcom/sec/android/app/personalization/PersoLock;->mOem:Lcom/sec/android/app/personalization/PersoLock$OemCommands;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v1, 0x5

    if-ne v0, v1, :cond_0

    .line 586
    iget-object v0, p0, Lcom/sec/android/app/personalization/PersoLock;->mNSPText:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 587
    iget-object v0, p0, Lcom/sec/android/app/personalization/PersoLock;->mCPText:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 588
    iget-object v0, p0, Lcom/sec/android/app/personalization/PersoLock;->mNSPEntry:Landroid/widget/EditText;

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setVisibility(I)V

    .line 589
    iget-object v0, p0, Lcom/sec/android/app/personalization/PersoLock;->mCPEntry:Landroid/widget/EditText;

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setVisibility(I)V

    .line 618
    :goto_0
    const v0, 0x7f050007

    invoke-virtual {p0, v0}, Lcom/sec/android/app/personalization/PersoLock;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/sec/android/app/personalization/PersoLock;->mControlKeyEntry:Landroid/widget/EditText;

    .line 619
    iget-object v0, p0, Lcom/sec/android/app/personalization/PersoLock;->mControlKeyEntry:Landroid/widget/EditText;

    invoke-static {}, Landroid/text/method/DialerKeyListener;->getInstance()Landroid/text/method/DialerKeyListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setKeyListener(Landroid/text/method/KeyListener;)V

    .line 622
    const/high16 v0, 0x7f050000

    invoke-virtual {p0, v0}, Lcom/sec/android/app/personalization/PersoLock;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/app/personalization/PersoLock;->mEntryPanel:Landroid/widget/LinearLayout;

    .line 624
    const v0, 0x7f050008

    invoke-virtual {p0, v0}, Lcom/sec/android/app/personalization/PersoLock;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/android/app/personalization/PersoLock;->mlockButton:Landroid/widget/Button;

    .line 625
    iget-object v0, p0, Lcom/sec/android/app/personalization/PersoLock;->mlockButton:Landroid/widget/Button;

    iget-object v1, p0, Lcom/sec/android/app/personalization/PersoLock;->mlockListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 627
    const v0, 0x7f050009

    invoke-virtual {p0, v0}, Lcom/sec/android/app/personalization/PersoLock;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/android/app/personalization/PersoLock;->mDismissButton:Landroid/widget/Button;

    .line 628
    iget-object v0, p0, Lcom/sec/android/app/personalization/PersoLock;->mDismissButton:Landroid/widget/Button;

    iget-object v1, p0, Lcom/sec/android/app/personalization/PersoLock;->mDismissListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 631
    const v0, 0x7f05000a

    invoke-virtual {p0, v0}, Lcom/sec/android/app/personalization/PersoLock;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/app/personalization/PersoLock;->mStatusPanel:Landroid/widget/LinearLayout;

    .line 632
    const v0, 0x7f05000b

    invoke-virtual {p0, v0}, Lcom/sec/android/app/personalization/PersoLock;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/personalization/PersoLock;->mStatusText:Landroid/widget/TextView;

    .line 633
    return-void

    .line 591
    :cond_0
    iget v0, p0, Lcom/sec/android/app/personalization/PersoLock;->mPersoType:I

    iget-object v1, p0, Lcom/sec/android/app/personalization/PersoLock;->mOem:Lcom/sec/android/app/personalization/PersoLock$OemCommands;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v1, 0x6

    if-ne v0, v1, :cond_1

    .line 593
    iget-object v0, p0, Lcom/sec/android/app/personalization/PersoLock;->mCPEntry:Landroid/widget/EditText;

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setVisibility(I)V

    .line 594
    iget-object v0, p0, Lcom/sec/android/app/personalization/PersoLock;->mCPText:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 596
    iget-object v0, p0, Lcom/sec/android/app/personalization/PersoLock;->mNSPText:Landroid/widget/TextView;

    const v1, 0x7f040003

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 597
    iget-object v0, p0, Lcom/sec/android/app/personalization/PersoLock;->mNSPEntry:Landroid/widget/EditText;

    invoke-static {}, Landroid/text/method/DialerKeyListener;->getInstance()Landroid/text/method/DialerKeyListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setKeyListener(Landroid/text/method/KeyListener;)V

    goto :goto_0

    .line 600
    :cond_1
    iget v0, p0, Lcom/sec/android/app/personalization/PersoLock;->mPersoType:I

    iget-object v1, p0, Lcom/sec/android/app/personalization/PersoLock;->mOem:Lcom/sec/android/app/personalization/PersoLock$OemCommands;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v1, 0x7

    if-ne v0, v1, :cond_2

    .line 602
    iget-object v0, p0, Lcom/sec/android/app/personalization/PersoLock;->mCPEntry:Landroid/widget/EditText;

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setVisibility(I)V

    .line 603
    iget-object v0, p0, Lcom/sec/android/app/personalization/PersoLock;->mCPText:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 604
    iget-object v0, p0, Lcom/sec/android/app/personalization/PersoLock;->mNSPText:Landroid/widget/TextView;

    const v1, 0x7f040004

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 605
    iget-object v0, p0, Lcom/sec/android/app/personalization/PersoLock;->mNSPEntry:Landroid/widget/EditText;

    invoke-static {}, Landroid/text/method/DialerKeyListener;->getInstance()Landroid/text/method/DialerKeyListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setKeyListener(Landroid/text/method/KeyListener;)V

    goto/16 :goto_0

    .line 607
    :cond_2
    iget v0, p0, Lcom/sec/android/app/personalization/PersoLock;->mPersoType:I

    iget-object v1, p0, Lcom/sec/android/app/personalization/PersoLock;->mOem:Lcom/sec/android/app/personalization/PersoLock$OemCommands;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    if-ne v0, v2, :cond_3

    .line 609
    iget-object v0, p0, Lcom/sec/android/app/personalization/PersoLock;->mNSPEntry:Landroid/widget/EditText;

    invoke-static {}, Landroid/text/method/DialerKeyListener;->getInstance()Landroid/text/method/DialerKeyListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setKeyListener(Landroid/text/method/KeyListener;)V

    .line 612
    iget-object v0, p0, Lcom/sec/android/app/personalization/PersoLock;->mCPEntry:Landroid/widget/EditText;

    invoke-static {}, Landroid/text/method/DialerKeyListener;->getInstance()Landroid/text/method/DialerKeyListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setKeyListener(Landroid/text/method/KeyListener;)V

    goto/16 :goto_0

    .line 616
    :cond_3
    const-string v0, "Err Unknown type"

    invoke-direct {p0, v0}, Lcom/sec/android/app/personalization/PersoLock;->log(Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method private ShowStatusPanel()V
    .locals 2

    .prologue
    .line 688
    const-string v0, "ShowStatusPanel"

    invoke-direct {p0, v0}, Lcom/sec/android/app/personalization/PersoLock;->log(Ljava/lang/String;)V

    .line 690
    const v0, 0x7f05000d

    invoke-virtual {p0, v0}, Lcom/sec/android/app/personalization/PersoLock;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/personalization/PersoLock;->mPersoTypeText:Landroid/widget/TextView;

    .line 691
    iget-object v0, p0, Lcom/sec/android/app/personalization/PersoLock;->mPersoTypeText:Landroid/widget/TextView;

    const-string v1, "Personalizatiob Status"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 693
    const v0, 0x7f050012

    invoke-virtual {p0, v0}, Lcom/sec/android/app/personalization/PersoLock;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/android/app/personalization/PersoLock;->mDismissButton:Landroid/widget/Button;

    .line 694
    iget-object v0, p0, Lcom/sec/android/app/personalization/PersoLock;->mDismissButton:Landroid/widget/Button;

    iget-object v1, p0, Lcom/sec/android/app/personalization/PersoLock;->mDismissListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 696
    invoke-direct {p0}, Lcom/sec/android/app/personalization/PersoLock;->SendGetStatusData()V

    .line 698
    return-void
.end method

.method private ShowUnLockPanel()V
    .locals 2

    .prologue
    .line 639
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "P type is "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/sec/android/app/personalization/PersoLock;->mPersoType:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/android/app/personalization/PersoLock;->log(Ljava/lang/String;)V

    .line 642
    const v0, 0x7f050014

    invoke-virtual {p0, v0}, Lcom/sec/android/app/personalization/PersoLock;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/personalization/PersoLock;->mPersoTypeText:Landroid/widget/TextView;

    .line 643
    const v0, 0x7f050015

    invoke-virtual {p0, v0}, Lcom/sec/android/app/personalization/PersoLock;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/personalization/PersoLock;->mUnlockText:Landroid/widget/TextView;

    .line 645
    iget v0, p0, Lcom/sec/android/app/personalization/PersoLock;->mPersoType:I

    iget-object v1, p0, Lcom/sec/android/app/personalization/PersoLock;->mOem:Lcom/sec/android/app/personalization/PersoLock$OemCommands;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v1, 0x5

    if-ne v0, v1, :cond_0

    .line 647
    iget-object v0, p0, Lcom/sec/android/app/personalization/PersoLock;->mPersoTypeText:Landroid/widget/TextView;

    const v1, 0x7f040012

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 648
    iget-object v0, p0, Lcom/sec/android/app/personalization/PersoLock;->mUnlockText:Landroid/widget/TextView;

    const v1, 0x7f040016

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 668
    :goto_0
    const v0, 0x7f050016

    invoke-virtual {p0, v0}, Lcom/sec/android/app/personalization/PersoLock;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/sec/android/app/personalization/PersoLock;->mControlKeyEntry:Landroid/widget/EditText;

    .line 669
    iget-object v0, p0, Lcom/sec/android/app/personalization/PersoLock;->mControlKeyEntry:Landroid/widget/EditText;

    invoke-static {}, Landroid/text/method/DialerKeyListener;->getInstance()Landroid/text/method/DialerKeyListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setKeyListener(Landroid/text/method/KeyListener;)V

    .line 671
    const v0, 0x7f050013

    invoke-virtual {p0, v0}, Lcom/sec/android/app/personalization/PersoLock;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/app/personalization/PersoLock;->mEntryPanel:Landroid/widget/LinearLayout;

    .line 673
    const v0, 0x7f050017

    invoke-virtual {p0, v0}, Lcom/sec/android/app/personalization/PersoLock;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/android/app/personalization/PersoLock;->mlockButton:Landroid/widget/Button;

    .line 674
    iget-object v0, p0, Lcom/sec/android/app/personalization/PersoLock;->mlockButton:Landroid/widget/Button;

    iget-object v1, p0, Lcom/sec/android/app/personalization/PersoLock;->mlockListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 676
    const v0, 0x7f050018

    invoke-virtual {p0, v0}, Lcom/sec/android/app/personalization/PersoLock;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/android/app/personalization/PersoLock;->mDismissButton:Landroid/widget/Button;

    .line 677
    iget-object v0, p0, Lcom/sec/android/app/personalization/PersoLock;->mDismissButton:Landroid/widget/Button;

    iget-object v1, p0, Lcom/sec/android/app/personalization/PersoLock;->mDismissListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 680
    const v0, 0x7f050019

    invoke-virtual {p0, v0}, Lcom/sec/android/app/personalization/PersoLock;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/app/personalization/PersoLock;->mStatusPanel:Landroid/widget/LinearLayout;

    .line 681
    const v0, 0x7f05001a

    invoke-virtual {p0, v0}, Lcom/sec/android/app/personalization/PersoLock;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/personalization/PersoLock;->mStatusText:Landroid/widget/TextView;

    .line 682
    return-void

    .line 650
    :cond_0
    iget v0, p0, Lcom/sec/android/app/personalization/PersoLock;->mPersoType:I

    iget-object v1, p0, Lcom/sec/android/app/personalization/PersoLock;->mOem:Lcom/sec/android/app/personalization/PersoLock$OemCommands;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v1, 0x6

    if-ne v0, v1, :cond_1

    .line 652
    iget-object v0, p0, Lcom/sec/android/app/personalization/PersoLock;->mPersoTypeText:Landroid/widget/TextView;

    const v1, 0x7f040013

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 653
    iget-object v0, p0, Lcom/sec/android/app/personalization/PersoLock;->mUnlockText:Landroid/widget/TextView;

    const v1, 0x7f040017

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    .line 655
    :cond_1
    iget v0, p0, Lcom/sec/android/app/personalization/PersoLock;->mPersoType:I

    iget-object v1, p0, Lcom/sec/android/app/personalization/PersoLock;->mOem:Lcom/sec/android/app/personalization/PersoLock$OemCommands;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v1, 0x7

    if-ne v0, v1, :cond_2

    .line 657
    iget-object v0, p0, Lcom/sec/android/app/personalization/PersoLock;->mPersoTypeText:Landroid/widget/TextView;

    const v1, 0x7f040014

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 658
    iget-object v0, p0, Lcom/sec/android/app/personalization/PersoLock;->mUnlockText:Landroid/widget/TextView;

    const v1, 0x7f040018

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto/16 :goto_0

    .line 660
    :cond_2
    iget v0, p0, Lcom/sec/android/app/personalization/PersoLock;->mPersoType:I

    iget-object v1, p0, Lcom/sec/android/app/personalization/PersoLock;->mOem:Lcom/sec/android/app/personalization/PersoLock$OemCommands;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/16 v1, 0x8

    if-ne v0, v1, :cond_3

    .line 662
    iget-object v0, p0, Lcom/sec/android/app/personalization/PersoLock;->mPersoTypeText:Landroid/widget/TextView;

    const v1, 0x7f040015

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 663
    iget-object v0, p0, Lcom/sec/android/app/personalization/PersoLock;->mUnlockText:Landroid/widget/TextView;

    const v1, 0x7f040019

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto/16 :goto_0

    .line 666
    :cond_3
    const-string v0, "Err Unknown type"

    invoke-direct {p0, v0}, Lcom/sec/android/app/personalization/PersoLock;->log(Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method static synthetic access$000(Lcom/sec/android/app/personalization/PersoLock;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/personalization/PersoLock;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 30
    invoke-direct {p0, p1}, Lcom/sec/android/app/personalization/PersoLock;->log(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/app/personalization/PersoLock;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/personalization/PersoLock;

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/sec/android/app/personalization/PersoLock;->indicateError()V

    return-void
.end method

.method static synthetic access$1100(Lcom/sec/android/app/personalization/PersoLock;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/personalization/PersoLock;

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/sec/android/app/personalization/PersoLock;->SendLockData()V

    return-void
.end method

.method static synthetic access$1200(Lcom/sec/android/app/personalization/PersoLock;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/personalization/PersoLock;

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/sec/android/app/personalization/PersoLock;->indicateBusy()V

    return-void
.end method

.method static synthetic access$1300(Lcom/sec/android/app/personalization/PersoLock;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/personalization/PersoLock;

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/sec/android/app/personalization/PersoLock;->SendUnLockData()V

    return-void
.end method

.method static synthetic access$200(Lcom/sec/android/app/personalization/PersoLock;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/personalization/PersoLock;

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/sec/android/app/personalization/PersoLock;->hideAlert()V

    return-void
.end method

.method static synthetic access$300(Lcom/sec/android/app/personalization/PersoLock;)Landroid/widget/EditText;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/personalization/PersoLock;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/sec/android/app/personalization/PersoLock;->mMccMncEntry:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/personalization/PersoLock;)Landroid/widget/EditText;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/personalization/PersoLock;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/sec/android/app/personalization/PersoLock;->mControlKeyEntry:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/personalization/PersoLock;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/personalization/PersoLock;

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/sec/android/app/personalization/PersoLock;->indicateSuccess()V

    return-void
.end method

.method static synthetic access$600(Lcom/sec/android/app/personalization/PersoLock;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/personalization/PersoLock;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/sec/android/app/personalization/PersoLock;->mNpStatus:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$602(Lcom/sec/android/app/personalization/PersoLock;Landroid/widget/TextView;)Landroid/widget/TextView;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/personalization/PersoLock;
    .param p1, "x1"    # Landroid/widget/TextView;

    .prologue
    .line 30
    iput-object p1, p0, Lcom/sec/android/app/personalization/PersoLock;->mNpStatus:Landroid/widget/TextView;

    return-object p1
.end method

.method static synthetic access$700(Lcom/sec/android/app/personalization/PersoLock;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/personalization/PersoLock;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/sec/android/app/personalization/PersoLock;->mNspStatus:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$702(Lcom/sec/android/app/personalization/PersoLock;Landroid/widget/TextView;)Landroid/widget/TextView;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/personalization/PersoLock;
    .param p1, "x1"    # Landroid/widget/TextView;

    .prologue
    .line 30
    iput-object p1, p0, Lcom/sec/android/app/personalization/PersoLock;->mNspStatus:Landroid/widget/TextView;

    return-object p1
.end method

.method static synthetic access$800(Lcom/sec/android/app/personalization/PersoLock;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/personalization/PersoLock;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/sec/android/app/personalization/PersoLock;->mSpStatus:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$802(Lcom/sec/android/app/personalization/PersoLock;Landroid/widget/TextView;)Landroid/widget/TextView;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/personalization/PersoLock;
    .param p1, "x1"    # Landroid/widget/TextView;

    .prologue
    .line 30
    iput-object p1, p0, Lcom/sec/android/app/personalization/PersoLock;->mSpStatus:Landroid/widget/TextView;

    return-object p1
.end method

.method static synthetic access$900(Lcom/sec/android/app/personalization/PersoLock;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/personalization/PersoLock;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/sec/android/app/personalization/PersoLock;->mCpStatus:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$902(Lcom/sec/android/app/personalization/PersoLock;Landroid/widget/TextView;)Landroid/widget/TextView;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/personalization/PersoLock;
    .param p1, "x1"    # Landroid/widget/TextView;

    .prologue
    .line 30
    iput-object p1, p0, Lcom/sec/android/app/personalization/PersoLock;->mCpStatus:Landroid/widget/TextView;

    return-object p1
.end method

.method private hideAlert()V
    .locals 2

    .prologue
    .line 803
    iget-object v0, p0, Lcom/sec/android/app/personalization/PersoLock;->mEntryPanel:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 804
    iget-object v0, p0, Lcom/sec/android/app/personalization/PersoLock;->mStatusPanel:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 805
    return-void
.end method

.method private indicateBusy()V
    .locals 2

    .prologue
    .line 773
    iget v0, p0, Lcom/sec/android/app/personalization/PersoLock;->mIsLock:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 774
    iget-object v0, p0, Lcom/sec/android/app/personalization/PersoLock;->mStatusText:Landroid/widget/TextView;

    const v1, 0x7f04000a

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 778
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/personalization/PersoLock;->mEntryPanel:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 779
    iget-object v0, p0, Lcom/sec/android/app/personalization/PersoLock;->mStatusPanel:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 780
    return-void

    .line 776
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/personalization/PersoLock;->mStatusText:Landroid/widget/TextView;

    const v1, 0x7f04000d

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0
.end method

.method private indicateError()V
    .locals 2

    .prologue
    .line 783
    iget v0, p0, Lcom/sec/android/app/personalization/PersoLock;->mIsLock:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 784
    iget-object v0, p0, Lcom/sec/android/app/personalization/PersoLock;->mStatusText:Landroid/widget/TextView;

    const v1, 0x7f04000b

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 788
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/personalization/PersoLock;->mEntryPanel:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 789
    iget-object v0, p0, Lcom/sec/android/app/personalization/PersoLock;->mStatusPanel:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 790
    return-void

    .line 786
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/personalization/PersoLock;->mStatusText:Landroid/widget/TextView;

    const v1, 0x7f04000e

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0
.end method

.method private indicateSuccess()V
    .locals 2

    .prologue
    .line 793
    iget v0, p0, Lcom/sec/android/app/personalization/PersoLock;->mIsLock:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 794
    iget-object v0, p0, Lcom/sec/android/app/personalization/PersoLock;->mStatusText:Landroid/widget/TextView;

    const v1, 0x7f04000c

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 798
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/personalization/PersoLock;->mEntryPanel:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 799
    iget-object v0, p0, Lcom/sec/android/app/personalization/PersoLock;->mStatusPanel:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 800
    return-void

    .line 796
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/personalization/PersoLock;->mStatusText:Landroid/widget/TextView;

    const v1, 0x7f04000f

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0
.end method

.method private log(Ljava/lang/String;)V
    .locals 3
    .param p1, "msg"    # Ljava/lang/String;

    .prologue
    .line 816
    const-string v0, "Personalization"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[SimNetworklock] "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 817
    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "icicle"    # Landroid/os/Bundle;

    .prologue
    .line 481
    invoke-super {p0, p1}, Landroid/app/Dialog;->onCreate(Landroid/os/Bundle;)V

    .line 483
    invoke-static {}, Lcom/android/internal/telephony/PhoneFactory;->getDefaultPhone()Lcom/android/internal/telephony/Phone;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/personalization/PersoLock;->mPhone:Lcom/android/internal/telephony/Phone;

    .line 484
    new-instance v0, Lcom/sec/android/app/personalization/PersoLock$OemCommands;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/personalization/PersoLock$OemCommands;-><init>(Lcom/sec/android/app/personalization/PersoLock;Lcom/sec/android/app/personalization/PersoLock$1;)V

    iput-object v0, p0, Lcom/sec/android/app/personalization/PersoLock;->mOem:Lcom/sec/android/app/personalization/PersoLock$OemCommands;

    .line 486
    iget v0, p0, Lcom/sec/android/app/personalization/PersoLock;->mIsLock:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 488
    const-string v0, "mIsLock - REQ_GET_LOCK_STATUS"

    invoke-direct {p0, v0}, Lcom/sec/android/app/personalization/PersoLock;->log(Ljava/lang/String;)V

    .line 490
    const v0, 0x7f030001

    invoke-virtual {p0, v0}, Lcom/sec/android/app/personalization/PersoLock;->setContentView(I)V

    .line 491
    invoke-direct {p0}, Lcom/sec/android/app/personalization/PersoLock;->ShowStatusPanel()V

    .line 506
    :goto_0
    return-void

    .line 494
    :cond_0
    iget v0, p0, Lcom/sec/android/app/personalization/PersoLock;->mIsLock:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 496
    const/high16 v0, 0x7f030000

    invoke-virtual {p0, v0}, Lcom/sec/android/app/personalization/PersoLock;->setContentView(I)V

    .line 497
    invoke-direct {p0}, Lcom/sec/android/app/personalization/PersoLock;->ShowLockPanel()V

    goto :goto_0

    .line 502
    :cond_1
    const v0, 0x7f030002

    invoke-virtual {p0, v0}, Lcom/sec/android/app/personalization/PersoLock;->setContentView(I)V

    .line 503
    invoke-direct {p0}, Lcom/sec/android/app/personalization/PersoLock;->ShowUnLockPanel()V

    goto :goto_0
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 515
    const/4 v0, 0x4

    if-ne p1, v0, :cond_0

    .line 516
    const/4 v0, 0x1

    .line 519
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2}, Landroid/app/Dialog;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method protected onStart()V
    .locals 0

    .prologue
    .line 510
    invoke-super {p0}, Landroid/app/Dialog;->onStart()V

    .line 511
    return-void
.end method
