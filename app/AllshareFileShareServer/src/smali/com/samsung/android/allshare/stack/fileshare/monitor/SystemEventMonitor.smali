.class public Lcom/samsung/android/allshare/stack/fileshare/monitor/SystemEventMonitor;
.super Ljava/lang/Object;
.source "SystemEventMonitor.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/allshare/stack/fileshare/monitor/SystemEventMonitor$3;
    }
.end annotation


# instance fields
.field private handler:Landroid/os/Handler;

.field private handlerThread:Landroid/os/HandlerThread;

.field private mBroadcastReceiver:Landroid/content/BroadcastReceiver;

.field private mConnectivityManager:Landroid/net/ConnectivityManager;

.field private mContext:Landroid/content/Context;

.field private mIsP2pAvailable:Z

.field private mP2pInterfaceName:Ljava/lang/String;

.field private mStarted:Z

.field private mSystemEventListeners:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/allshare/stack/fileshare/monitor/ISystemEventListener;",
            ">;"
        }
    .end annotation
.end field

.field private final mTag:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 68
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/allshare/stack/fileshare/monitor/SystemEventMonitor;->mTag:Ljava/lang/String;

    .line 51
    iput-object v1, p0, Lcom/samsung/android/allshare/stack/fileshare/monitor/SystemEventMonitor;->mContext:Landroid/content/Context;

    .line 53
    iput-object v1, p0, Lcom/samsung/android/allshare/stack/fileshare/monitor/SystemEventMonitor;->mConnectivityManager:Landroid/net/ConnectivityManager;

    .line 55
    iput-object v1, p0, Lcom/samsung/android/allshare/stack/fileshare/monitor/SystemEventMonitor;->mP2pInterfaceName:Ljava/lang/String;

    .line 57
    iput-boolean v2, p0, Lcom/samsung/android/allshare/stack/fileshare/monitor/SystemEventMonitor;->mStarted:Z

    .line 59
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/allshare/stack/fileshare/monitor/SystemEventMonitor;->mSystemEventListeners:Ljava/util/List;

    .line 61
    iput-object v1, p0, Lcom/samsung/android/allshare/stack/fileshare/monitor/SystemEventMonitor;->handlerThread:Landroid/os/HandlerThread;

    .line 63
    iput-object v1, p0, Lcom/samsung/android/allshare/stack/fileshare/monitor/SystemEventMonitor;->handler:Landroid/os/Handler;

    .line 158
    new-instance v0, Lcom/samsung/android/allshare/stack/fileshare/monitor/SystemEventMonitor$2;

    invoke-direct {v0, p0}, Lcom/samsung/android/allshare/stack/fileshare/monitor/SystemEventMonitor$2;-><init>(Lcom/samsung/android/allshare/stack/fileshare/monitor/SystemEventMonitor;)V

    iput-object v0, p0, Lcom/samsung/android/allshare/stack/fileshare/monitor/SystemEventMonitor;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    .line 221
    iput-boolean v2, p0, Lcom/samsung/android/allshare/stack/fileshare/monitor/SystemEventMonitor;->mIsP2pAvailable:Z

    .line 69
    iput-object p1, p0, Lcom/samsung/android/allshare/stack/fileshare/monitor/SystemEventMonitor;->mContext:Landroid/content/Context;

    .line 70
    invoke-direct {p0}, Lcom/samsung/android/allshare/stack/fileshare/monitor/SystemEventMonitor;->init()V

    .line 71
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/android/allshare/stack/fileshare/monitor/SystemEventMonitor;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/stack/fileshare/monitor/SystemEventMonitor;

    .prologue
    .line 48
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/fileshare/monitor/SystemEventMonitor;->mSystemEventListeners:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$100(Lcom/samsung/android/allshare/stack/fileshare/monitor/SystemEventMonitor;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/stack/fileshare/monitor/SystemEventMonitor;

    .prologue
    .line 48
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/fileshare/monitor/SystemEventMonitor;->mP2pInterfaceName:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$200(Lcom/samsung/android/allshare/stack/fileshare/monitor/SystemEventMonitor;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/stack/fileshare/monitor/SystemEventMonitor;

    .prologue
    .line 48
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/fileshare/monitor/SystemEventMonitor;->mTag:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$300(Lcom/samsung/android/allshare/stack/fileshare/monitor/SystemEventMonitor;Landroid/content/Intent;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/allshare/stack/fileshare/monitor/SystemEventMonitor;
    .param p1, "x1"    # Landroid/content/Intent;

    .prologue
    .line 48
    invoke-direct {p0, p1}, Lcom/samsung/android/allshare/stack/fileshare/monitor/SystemEventMonitor;->checkP2PState(Landroid/content/Intent;)V

    return-void
.end method

.method private checkP2PState(Landroid/content/Intent;)V
    .locals 7
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 94
    sget-object v1, Landroid/net/NetworkInfo$State;->UNKNOWN:Landroid/net/NetworkInfo$State;

    .line 95
    .local v1, "state":Landroid/net/NetworkInfo$State;
    const-string v2, "networkInfo"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/NetworkInfo;

    .line 98
    .local v0, "netInfo":Landroid/net/NetworkInfo;
    if-eqz v0, :cond_0

    .line 99
    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getState()Landroid/net/NetworkInfo$State;

    move-result-object v1

    .line 103
    :goto_0
    sget-object v2, Lcom/samsung/android/allshare/stack/fileshare/monitor/SystemEventMonitor$3;->$SwitchMap$android$net$NetworkInfo$State:[I

    invoke-virtual {v1}, Landroid/net/NetworkInfo$State;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 118
    :goto_1
    return-void

    .line 101
    :cond_0
    iget-object v2, p0, Lcom/samsung/android/allshare/stack/fileshare/monitor/SystemEventMonitor;->mConnectivityManager:Landroid/net/ConnectivityManager;

    const/16 v3, 0xd

    invoke-virtual {v2, v3}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/NetworkInfo;->getState()Landroid/net/NetworkInfo$State;

    move-result-object v1

    goto :goto_0

    .line 105
    :pswitch_0
    iget-object v2, p0, Lcom/samsung/android/allshare/stack/fileshare/monitor/SystemEventMonitor;->mTag:Ljava/lang/String;

    const-string v3, "checkP2PState"

    const-string v4, "WIFI P2P STATE: CONNECTED"

    invoke-static {v2, v3, v4}, Lcom/samsung/android/allshare/stack/fileshare/data/DLog;->v_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 106
    iput-boolean v6, p0, Lcom/samsung/android/allshare/stack/fileshare/monitor/SystemEventMonitor;->mIsP2pAvailable:Z

    .line 109
    invoke-direct {p0, v5}, Lcom/samsung/android/allshare/stack/fileshare/monitor/SystemEventMonitor;->notifyEvent(I)V

    goto :goto_1

    .line 113
    :pswitch_1
    iget-object v2, p0, Lcom/samsung/android/allshare/stack/fileshare/monitor/SystemEventMonitor;->mTag:Ljava/lang/String;

    const-string v3, "checkP2PState"

    const-string v4, "WIFI P2P STATE: DISCONNECTED"

    invoke-static {v2, v3, v4}, Lcom/samsung/android/allshare/stack/fileshare/data/DLog;->v_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 114
    iput-boolean v5, p0, Lcom/samsung/android/allshare/stack/fileshare/monitor/SystemEventMonitor;->mIsP2pAvailable:Z

    .line 115
    invoke-direct {p0, v6}, Lcom/samsung/android/allshare/stack/fileshare/monitor/SystemEventMonitor;->notifyEvent(I)V

    goto :goto_1

    .line 103
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private init()V
    .locals 3

    .prologue
    .line 77
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/fileshare/monitor/SystemEventMonitor;->mContext:Landroid/content/Context;

    if-nez v0, :cond_0

    .line 78
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/fileshare/monitor/SystemEventMonitor;->mTag:Ljava/lang/String;

    const-string v1, "init"

    const-string v2, "the context is null!"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/allshare/stack/fileshare/data/DLog;->e_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 90
    :goto_0
    return-void

    .line 82
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/fileshare/monitor/SystemEventMonitor;->mContext:Landroid/content/Context;

    const-string v1, "connectivity"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    iput-object v0, p0, Lcom/samsung/android/allshare/stack/fileshare/monitor/SystemEventMonitor;->mConnectivityManager:Landroid/net/ConnectivityManager;

    .line 85
    const-string v0, "p2p-wlan0-0"

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/allshare/stack/fileshare/monitor/SystemEventMonitor;->mP2pInterfaceName:Ljava/lang/String;

    .line 89
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/allshare/stack/fileshare/monitor/SystemEventMonitor;->mStarted:Z

    goto :goto_0
.end method

.method private notifyEvent(I)V
    .locals 3
    .param p1, "event"    # I

    .prologue
    .line 126
    iget-object v1, p0, Lcom/samsung/android/allshare/stack/fileshare/monitor/SystemEventMonitor;->handler:Landroid/os/Handler;

    if-nez v1, :cond_0

    .line 127
    new-instance v1, Landroid/os/HandlerThread;

    const-string v2, "SystemEventMonitorHandlerThread"

    invoke-direct {v1, v2}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    iput-object v1, p0, Lcom/samsung/android/allshare/stack/fileshare/monitor/SystemEventMonitor;->handlerThread:Landroid/os/HandlerThread;

    .line 128
    iget-object v1, p0, Lcom/samsung/android/allshare/stack/fileshare/monitor/SystemEventMonitor;->handlerThread:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->start()V

    .line 130
    new-instance v1, Lcom/samsung/android/allshare/stack/fileshare/monitor/SystemEventMonitor$1;

    iget-object v2, p0, Lcom/samsung/android/allshare/stack/fileshare/monitor/SystemEventMonitor;->handlerThread:Landroid/os/HandlerThread;

    invoke-virtual {v2}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, p0, v2}, Lcom/samsung/android/allshare/stack/fileshare/monitor/SystemEventMonitor$1;-><init>(Lcom/samsung/android/allshare/stack/fileshare/monitor/SystemEventMonitor;Landroid/os/Looper;)V

    iput-object v1, p0, Lcom/samsung/android/allshare/stack/fileshare/monitor/SystemEventMonitor;->handler:Landroid/os/Handler;

    .line 147
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/allshare/stack/fileshare/monitor/SystemEventMonitor;->handler:Landroid/os/Handler;

    invoke-virtual {v1}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    .line 148
    .local v0, "msg":Landroid/os/Message;
    const/4 v1, 0x1

    iput v1, v0, Landroid/os/Message;->what:I

    .line 149
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 150
    iget-object v1, p0, Lcom/samsung/android/allshare/stack/fileshare/monitor/SystemEventMonitor;->handler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 151
    return-void
.end method


# virtual methods
.method public addSystemEventListener(Lcom/samsung/android/allshare/stack/fileshare/monitor/ISystemEventListener;)V
    .locals 3
    .param p1, "listener"    # Lcom/samsung/android/allshare/stack/fileshare/monitor/ISystemEventListener;

    .prologue
    .line 231
    if-nez p1, :cond_0

    .line 232
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/fileshare/monitor/SystemEventMonitor;->mTag:Ljava/lang/String;

    const-string v1, "addSystemEventListener"

    const-string v2, "the paramter is null!"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/allshare/stack/fileshare/data/DLog;->e_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 237
    :goto_0
    return-void

    .line 236
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/fileshare/monitor/SystemEventMonitor;->mSystemEventListeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public fini()V
    .locals 2

    .prologue
    .line 180
    iget-boolean v0, p0, Lcom/samsung/android/allshare/stack/fileshare/monitor/SystemEventMonitor;->mStarted:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/allshare/stack/fileshare/monitor/SystemEventMonitor;->mContext:Landroid/content/Context;

    if-eqz v0, :cond_0

    .line 181
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/fileshare/monitor/SystemEventMonitor;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/samsung/android/allshare/stack/fileshare/monitor/SystemEventMonitor;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 182
    :cond_0
    return-void
.end method

.method public isNetworkAvailable(Ljava/lang/String;)Z
    .locals 1
    .param p1, "nic"    # Ljava/lang/String;

    .prologue
    .line 224
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/fileshare/monitor/SystemEventMonitor;->mP2pInterfaceName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 225
    iget-boolean v0, p0, Lcom/samsung/android/allshare/stack/fileshare/monitor/SystemEventMonitor;->mIsP2pAvailable:Z

    .line 227
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public startMonitor()V
    .locals 3

    .prologue
    .line 188
    iget-boolean v1, p0, Lcom/samsung/android/allshare/stack/fileshare/monitor/SystemEventMonitor;->mStarted:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/samsung/android/allshare/stack/fileshare/monitor/SystemEventMonitor;->mContext:Landroid/content/Context;

    if-nez v1, :cond_1

    .line 202
    :cond_0
    :goto_0
    return-void

    .line 191
    :cond_1
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 194
    .local v0, "filter":Landroid/content/IntentFilter;
    const-string v1, "android.net.wifi.p2p.CONNECTION_STATE_CHANGE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 195
    const-string v1, "android.net.wifi.p2p.PEERS_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 196
    const-string v1, "android.net.wifi.p2p.STATE_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 197
    const-string v1, "android.net.wifi.p2p.THIS_DEVICE_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 199
    iget-object v1, p0, Lcom/samsung/android/allshare/stack/fileshare/monitor/SystemEventMonitor;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/samsung/android/allshare/stack/fileshare/monitor/SystemEventMonitor;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 201
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/samsung/android/allshare/stack/fileshare/monitor/SystemEventMonitor;->mStarted:Z

    goto :goto_0
.end method

.method public stopMonitor()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 208
    iget-boolean v0, p0, Lcom/samsung/android/allshare/stack/fileshare/monitor/SystemEventMonitor;->mStarted:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/allshare/stack/fileshare/monitor/SystemEventMonitor;->mContext:Landroid/content/Context;

    if-nez v0, :cond_1

    .line 219
    :cond_0
    :goto_0
    return-void

    .line 211
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/fileshare/monitor/SystemEventMonitor;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/samsung/android/allshare/stack/fileshare/monitor/SystemEventMonitor;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 212
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/allshare/stack/fileshare/monitor/SystemEventMonitor;->mStarted:Z

    .line 214
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/fileshare/monitor/SystemEventMonitor;->handlerThread:Landroid/os/HandlerThread;

    if-eqz v0, :cond_0

    .line 215
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/fileshare/monitor/SystemEventMonitor;->handlerThread:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->quit()Z

    .line 216
    iput-object v2, p0, Lcom/samsung/android/allshare/stack/fileshare/monitor/SystemEventMonitor;->handlerThread:Landroid/os/HandlerThread;

    .line 217
    iput-object v2, p0, Lcom/samsung/android/allshare/stack/fileshare/monitor/SystemEventMonitor;->handler:Landroid/os/Handler;

    goto :goto_0
.end method
