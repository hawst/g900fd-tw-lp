.class public Lcom/samsung/android/allshare/stack/fileshare/data/FTDRequestSessionInfo;
.super Ljava/lang/Object;
.source "FTDRequestSessionInfo.java"


# instance fields
.field count:I

.field isHandled:Z

.field requestStatus:Lcom/samsung/android/allshare/stack/fileshare/data/FTDSessionAcceptInfo;

.field totalSize:J


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 4
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/allshare/stack/fileshare/data/FTDRequestSessionInfo;->isHandled:Z

    return-void
.end method


# virtual methods
.method public isHandled()Z
    .locals 1

    .prologue
    .line 33
    iget-boolean v0, p0, Lcom/samsung/android/allshare/stack/fileshare/data/FTDRequestSessionInfo;->isHandled:Z

    return v0
.end method

.method public setAcceptInfo(Lcom/samsung/android/allshare/stack/fileshare/data/FTDSessionAcceptInfo;)V
    .locals 0
    .param p1, "requestStatus"    # Lcom/samsung/android/allshare/stack/fileshare/data/FTDSessionAcceptInfo;

    .prologue
    .line 21
    iput-object p1, p0, Lcom/samsung/android/allshare/stack/fileshare/data/FTDRequestSessionInfo;->requestStatus:Lcom/samsung/android/allshare/stack/fileshare/data/FTDSessionAcceptInfo;

    .line 22
    return-void
.end method

.method public setCount(I)V
    .locals 0
    .param p1, "count"    # I

    .prologue
    .line 25
    iput p1, p0, Lcom/samsung/android/allshare/stack/fileshare/data/FTDRequestSessionInfo;->count:I

    .line 26
    return-void
.end method

.method public declared-synchronized setRequestStatus(Z)V
    .locals 1
    .param p1, "isAccept"    # Z

    .prologue
    .line 15
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/fileshare/data/FTDRequestSessionInfo;->requestStatus:Lcom/samsung/android/allshare/stack/fileshare/data/FTDSessionAcceptInfo;

    iput-boolean p1, v0, Lcom/samsung/android/allshare/stack/fileshare/data/FTDSessionAcceptInfo;->isAccept:Z

    .line 16
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/allshare/stack/fileshare/data/FTDRequestSessionInfo;->isHandled:Z

    .line 17
    invoke-virtual {p0}, Ljava/lang/Object;->notify()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 18
    monitor-exit p0

    return-void

    .line 15
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public setTotalSize(J)V
    .locals 1
    .param p1, "totalSize"    # J

    .prologue
    .line 29
    iput-wide p1, p0, Lcom/samsung/android/allshare/stack/fileshare/data/FTDRequestSessionInfo;->totalSize:J

    .line 30
    return-void
.end method
