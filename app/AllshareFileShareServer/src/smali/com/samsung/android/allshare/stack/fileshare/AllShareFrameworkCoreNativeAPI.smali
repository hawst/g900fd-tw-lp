.class public Lcom/samsung/android/allshare/stack/fileshare/AllShareFrameworkCoreNativeAPI;
.super Ljava/lang/Object;
.source "AllShareFrameworkCoreNativeAPI.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static native cancelFTDSession(Ljava/lang/String;)I
.end method

.method public static native getFTDFriendlyName(Ljava/lang/StringBuffer;)I
.end method

.method public static native getFTDHTTPPort(Lcom/samsung/android/allshare/stack/fileshare/data/FileShareHTTPPortInfo;)I
.end method

.method public static native getFTDSession(Ljava/lang/String;Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceSessionInfo;Ljava/util/ArrayList;)I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceSessionInfo;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceTransportInfo;",
            ">;)I"
        }
    .end annotation
.end method

.method public static native getFTDUDN(Ljava/lang/StringBuffer;)I
.end method

.method public static native initializeAllShareFrameworkCore()I
.end method

.method public static native initializeFTD(Ljava/lang/String;Ljava/lang/String;)I
.end method

.method public static native notifyNICAdded(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
.end method

.method public static native notifyNICRemoved(Ljava/lang/String;)I
.end method

.method public static native setFTDFreindlyName(Ljava/lang/String;)I
.end method

.method public static native startFTD(Ljava/lang/String;)I
.end method

.method public static native stopFTD()I
.end method

.method public static native terminateAllShareFrameworkCore()I
.end method

.method public static native terminateFTD()I
.end method
