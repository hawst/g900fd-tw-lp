.class public Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper;
.super Ljava/lang/Object;
.source "FileShareDeviceWrapper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper$ISessionEvent;,
        Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper$IDeviceEvent;,
        Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper$EventData;
    }
.end annotation


# static fields
.field private static DEVICE_DECRIPTION_FILE_NAME:Ljava/lang/String; = null

.field public static final ERROR_DEPLOY_FILE:I = -0x2

.field public static final ERROR_INIT_ASF:I = -0x1

.field public static final ERROR_INIT_FTD:I = -0x3

.field private static final EVENT_SESSION_CANCEL:I = 0x1

.field private static final EVENT_SESSION_CLOSE:I = 0x2

.field private static final EVENT_SESSION_CREATE:I = 0x3

.field private static final EVENT_TRANSFER_ERROR:I = 0x4

.field private static final EVENT_TRANSFER_FINISH:I = 0x7

.field private static final EVENT_TRANSFER_PROGRESS:I = 0x6

.field private static final EVENT_TRANSFER_REQUEST:I = 0x5

.field private static FILE_TRANSPORT_FILE_NAME:Ljava/lang/String; = null

.field private static final LOG_TAG:Ljava/lang/String; = "FileShareDeviceWrapper"

.field private static deploymentFilePath:Ljava/lang/String;

.field private static instance:Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper;


# instance fields
.field public mDeviceEventListener:Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper$IDeviceEvent;

.field private mGUIEventHandler:Landroid/os/Handler;

.field public mSessionEventListener:Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper$ISessionEvent;

.field private mSystemEventMonitor:Lcom/samsung/android/allshare/stack/fileshare/monitor/SystemEventMonitor;

.field public sessionInfoMaps:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/samsung/android/allshare/stack/fileshare/data/FTDRequestSessionInfo;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 31
    sput-object v0, Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper;->instance:Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper;

    .line 45
    sput-object v0, Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper;->deploymentFilePath:Ljava/lang/String;

    .line 47
    const-string v0, "DeviceDescription.xml"

    sput-object v0, Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper;->DEVICE_DECRIPTION_FILE_NAME:Ljava/lang/String;

    .line 49
    const-string v0, "FileTransport_1.xml"

    sput-object v0, Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper;->FILE_TRANSPORT_FILE_NAME:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper;->sessionInfoMaps:Ljava/util/HashMap;

    .line 37
    iput-object v1, p0, Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper;->mSessionEventListener:Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper$ISessionEvent;

    .line 39
    iput-object v1, p0, Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper;->mDeviceEventListener:Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper$IDeviceEvent;

    .line 41
    iput-object v1, p0, Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper;->mGUIEventHandler:Landroid/os/Handler;

    .line 43
    iput-object v1, p0, Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper;->mSystemEventMonitor:Lcom/samsung/android/allshare/stack/fileshare/monitor/SystemEventMonitor;

    .line 156
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper;Landroid/os/Message;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper;
    .param p1, "x1"    # Landroid/os/Message;

    .prologue
    .line 30
    invoke-direct {p0, p1}, Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper;->handleEventMessage(Landroid/os/Message;)V

    return-void
.end method

.method private declared-synchronized copyDeploymentFilesFromRaw(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 24
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "filePath"    # Ljava/lang/String;

    .prologue
    .line 561
    monitor-enter p0

    const/4 v6, 0x0

    .line 562
    .local v6, "deploymentFile":Ljava/io/File;
    const/4 v4, 0x0

    .line 563
    .local v4, "currentFilePath":Ljava/io/File;
    const/4 v9, 0x0

    .line 564
    .local v9, "inDeviceDescription":Ljava/io/InputStream;
    const/4 v13, 0x0

    .line 565
    .local v13, "outDeviceDescription":Ljava/io/OutputStream;
    const/4 v10, 0x0

    .line 566
    .local v10, "inFileTransport":Ljava/io/InputStream;
    const/16 v17, 0x0

    .line 569
    .local v17, "outFileTransport":Ljava/io/OutputStream;
    :try_start_0
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v19

    const/high16 v20, 0x7f040000

    invoke-virtual/range {v19 .. v20}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;

    move-result-object v9

    .line 570
    if-nez v9, :cond_4

    .line 571
    const-string v19, "FileShareDeviceWrapper"

    const-string v20, "copyDeploymentFilesFromRaw"

    const-string v21, "openRawResource devicedescription failed!"

    invoke-static/range {v19 .. v21}, Lcom/samsung/android/allshare/stack/fileshare/data/DLog;->e_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_11
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_8
    .catchall {:try_start_0 .. :try_end_0} :catchall_3

    .line 573
    const/16 v19, 0x0

    .line 652
    if-eqz v9, :cond_0

    .line 653
    :try_start_1
    invoke-virtual {v9}, Ljava/io/InputStream;->close()V

    .line 655
    :cond_0
    if-eqz v13, :cond_1

    .line 656
    invoke-virtual {v13}, Ljava/io/OutputStream;->close()V

    .line 658
    :cond_1
    if-eqz v10, :cond_2

    .line 659
    invoke-virtual {v10}, Ljava/io/InputStream;->close()V

    .line 661
    :cond_2
    if-eqz v17, :cond_3

    .line 662
    invoke-virtual/range {v17 .. v17}, Ljava/io/OutputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 669
    :cond_3
    :goto_0
    monitor-exit p0

    return v19

    .line 664
    :catch_0
    move-exception v8

    .line 665
    .local v8, "e":Ljava/io/IOException;
    :try_start_2
    const-string v20, "FileShareDeviceWrapper"

    const-string v21, "copyDeploymentFilesFromRaw"

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "IOException:"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v20 .. v22}, Lcom/samsung/android/allshare/stack/fileshare/data/DLog;->e_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 561
    .end local v8    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v19

    :goto_1
    monitor-exit p0

    throw v19

    .line 576
    :cond_4
    :try_start_3
    invoke-virtual/range {p2 .. p2}, Ljava/lang/String;->length()I

    move-result v19

    if-nez v19, :cond_9

    .line 577
    new-instance v7, Ljava/io/File;

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v20, Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper;->deploymentFilePath:Ljava/lang/String;

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "/"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    sget-object v20, Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper;->DEVICE_DECRIPTION_FILE_NAME:Ljava/lang/String;

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-direct {v7, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/io/FileNotFoundException; {:try_start_3 .. :try_end_3} :catch_11
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_8
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    .line 578
    .end local v6    # "deploymentFile":Ljava/io/File;
    .local v7, "deploymentFile":Ljava/io/File;
    :try_start_4
    new-instance v5, Ljava/io/File;

    sget-object v19, Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper;->deploymentFilePath:Ljava/lang/String;

    move-object/from16 v0, v19

    invoke-direct {v5, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V
    :try_end_4
    .catch Ljava/io/FileNotFoundException; {:try_start_4 .. :try_end_4} :catch_12
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_b
    .catchall {:try_start_4 .. :try_end_4} :catchall_4

    .line 592
    .end local v4    # "currentFilePath":Ljava/io/File;
    .local v5, "currentFilePath":Ljava/io/File;
    :cond_5
    :goto_2
    :try_start_5
    new-instance v15, Ljava/io/File;

    sget-object v19, Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper;->DEVICE_DECRIPTION_FILE_NAME:Ljava/lang/String;

    move-object/from16 v0, v19

    invoke-direct {v15, v5, v0}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 593
    .local v15, "outFile":Ljava/io/File;
    new-instance v14, Ljava/io/FileOutputStream;

    invoke-direct {v14, v15}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_5
    .catch Ljava/io/FileNotFoundException; {:try_start_5 .. :try_end_5} :catch_3
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_c
    .catchall {:try_start_5 .. :try_end_5} :catchall_5

    .line 594
    .end local v13    # "outDeviceDescription":Ljava/io/OutputStream;
    .local v14, "outDeviceDescription":Ljava/io/OutputStream;
    const/16 v19, 0x400

    :try_start_6
    move/from16 v0, v19

    new-array v2, v0, [B

    .line 596
    .local v2, "buf":[B
    :goto_3
    invoke-virtual {v9, v2}, Ljava/io/InputStream;->read([B)I

    move-result v11

    .local v11, "len":I
    if-lez v11, :cond_a

    .line 597
    const/16 v19, 0x0

    move/from16 v0, v19

    invoke-virtual {v14, v2, v0, v11}, Ljava/io/OutputStream;->write([BII)V
    :try_end_6
    .catch Ljava/io/FileNotFoundException; {:try_start_6 .. :try_end_6} :catch_1
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_d
    .catchall {:try_start_6 .. :try_end_6} :catchall_6

    goto :goto_3

    .line 641
    .end local v2    # "buf":[B
    .end local v11    # "len":I
    :catch_1
    move-exception v8

    move-object v13, v14

    .end local v14    # "outDeviceDescription":Ljava/io/OutputStream;
    .restart local v13    # "outDeviceDescription":Ljava/io/OutputStream;
    move-object v4, v5

    .end local v5    # "currentFilePath":Ljava/io/File;
    .restart local v4    # "currentFilePath":Ljava/io/File;
    move-object v6, v7

    .line 642
    .end local v7    # "deploymentFile":Ljava/io/File;
    .end local v15    # "outFile":Ljava/io/File;
    .restart local v6    # "deploymentFile":Ljava/io/File;
    .local v8, "e":Ljava/io/FileNotFoundException;
    :goto_4
    :try_start_7
    const-string v19, "FileShareDeviceWrapper"

    const-string v20, "copyDeploymentFilesFromRaw"

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "File not Found!!! FileNotFoundException:"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v19 .. v21}, Lcom/samsung/android/allshare/stack/fileshare/data/DLog;->e_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_3

    .line 644
    const/16 v19, 0x0

    .line 652
    if-eqz v9, :cond_6

    .line 653
    :try_start_8
    invoke-virtual {v9}, Ljava/io/InputStream;->close()V

    .line 655
    :cond_6
    if-eqz v13, :cond_7

    .line 656
    invoke-virtual {v13}, Ljava/io/OutputStream;->close()V

    .line 658
    :cond_7
    if-eqz v10, :cond_8

    .line 659
    invoke-virtual {v10}, Ljava/io/InputStream;->close()V

    .line 661
    :cond_8
    if-eqz v17, :cond_3

    .line 662
    invoke-virtual/range {v17 .. v17}, Ljava/io/OutputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_2
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    goto/16 :goto_0

    .line 664
    :catch_2
    move-exception v8

    .line 665
    .local v8, "e":Ljava/io/IOException;
    :try_start_9
    const-string v20, "FileShareDeviceWrapper"

    const-string v21, "copyDeploymentFilesFromRaw"

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "IOException:"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v20 .. v22}, Lcom/samsung/android/allshare/stack/fileshare/data/DLog;->e_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    goto/16 :goto_0

    .line 580
    .end local v8    # "e":Ljava/io/IOException;
    :cond_9
    :try_start_a
    new-instance v7, Ljava/io/File;

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v20, Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper;->deploymentFilePath:Ljava/lang/String;

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "/"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    move-object/from16 v1, p2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "/"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    sget-object v20, Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper;->DEVICE_DECRIPTION_FILE_NAME:Ljava/lang/String;

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-direct {v7, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V
    :try_end_a
    .catch Ljava/io/FileNotFoundException; {:try_start_a .. :try_end_a} :catch_11
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_8
    .catchall {:try_start_a .. :try_end_a} :catchall_3

    .line 582
    .end local v6    # "deploymentFile":Ljava/io/File;
    .restart local v7    # "deploymentFile":Ljava/io/File;
    :try_start_b
    new-instance v5, Ljava/io/File;

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v20, Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper;->deploymentFilePath:Ljava/lang/String;

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "/"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    move-object/from16 v1, p2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-direct {v5, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V
    :try_end_b
    .catch Ljava/io/FileNotFoundException; {:try_start_b .. :try_end_b} :catch_12
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_b
    .catchall {:try_start_b .. :try_end_b} :catchall_4

    .line 584
    .end local v4    # "currentFilePath":Ljava/io/File;
    .restart local v5    # "currentFilePath":Ljava/io/File;
    :try_start_c
    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v19

    if-nez v19, :cond_5

    .line 585
    invoke-virtual {v5}, Ljava/io/File;->mkdirs()Z

    move-result v19

    if-nez v19, :cond_5

    .line 586
    const-string v19, "FileShareDeviceWrapper"

    const-string v20, "copyDeploymentFilesFromRaw"

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "mkdirs["

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    sget-object v22, Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper;->DEVICE_DECRIPTION_FILE_NAME:Ljava/lang/String;

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v22, "] failed!"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v19 .. v21}, Lcom/samsung/android/allshare/stack/fileshare/data/DLog;->e_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_c
    .catch Ljava/io/FileNotFoundException; {:try_start_c .. :try_end_c} :catch_3
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_c
    .catchall {:try_start_c .. :try_end_c} :catchall_5

    goto/16 :goto_2

    .line 641
    :catch_3
    move-exception v8

    move-object v4, v5

    .end local v5    # "currentFilePath":Ljava/io/File;
    .restart local v4    # "currentFilePath":Ljava/io/File;
    move-object v6, v7

    .end local v7    # "deploymentFile":Ljava/io/File;
    .restart local v6    # "deploymentFile":Ljava/io/File;
    goto/16 :goto_4

    .line 600
    .end local v4    # "currentFilePath":Ljava/io/File;
    .end local v6    # "deploymentFile":Ljava/io/File;
    .end local v13    # "outDeviceDescription":Ljava/io/OutputStream;
    .restart local v2    # "buf":[B
    .restart local v5    # "currentFilePath":Ljava/io/File;
    .restart local v7    # "deploymentFile":Ljava/io/File;
    .restart local v11    # "len":I
    .restart local v14    # "outDeviceDescription":Ljava/io/OutputStream;
    .restart local v15    # "outFile":Ljava/io/File;
    :cond_a
    :try_start_d
    const-string v19, "FileShareDeviceWrapper"

    const-string v20, "copyDeploymentFilesFromRaw"

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "copy "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    sget-object v22, Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper;->DEVICE_DECRIPTION_FILE_NAME:Ljava/lang/String;

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v22, " successfully from "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v22, " to "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v19 .. v21}, Lcom/samsung/android/allshare/stack/fileshare/data/DLog;->d_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 604
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v19

    const v20, 0x7f040001

    invoke-virtual/range {v19 .. v20}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;

    move-result-object v10

    .line 605
    if-nez v10, :cond_f

    .line 606
    const-string v19, "FileShareDeviceWrapper"

    const-string v20, "copyDeploymentFilesFromRaw"

    const-string v21, "openRawResource filetransport failed!"

    invoke-static/range {v19 .. v21}, Lcom/samsung/android/allshare/stack/fileshare/data/DLog;->e_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_d
    .catch Ljava/io/FileNotFoundException; {:try_start_d .. :try_end_d} :catch_1
    .catch Ljava/io/IOException; {:try_start_d .. :try_end_d} :catch_d
    .catchall {:try_start_d .. :try_end_d} :catchall_6

    .line 608
    const/16 v19, 0x0

    .line 652
    if-eqz v9, :cond_b

    .line 653
    :try_start_e
    invoke-virtual {v9}, Ljava/io/InputStream;->close()V

    .line 655
    :cond_b
    if-eqz v14, :cond_c

    .line 656
    invoke-virtual {v14}, Ljava/io/OutputStream;->close()V

    .line 658
    :cond_c
    if-eqz v10, :cond_d

    .line 659
    invoke-virtual {v10}, Ljava/io/InputStream;->close()V

    .line 661
    :cond_d
    if-eqz v17, :cond_e

    .line 662
    invoke-virtual/range {v17 .. v17}, Ljava/io/OutputStream;->close()V
    :try_end_e
    .catch Ljava/io/IOException; {:try_start_e .. :try_end_e} :catch_4
    .catchall {:try_start_e .. :try_end_e} :catchall_1

    :cond_e
    :goto_5
    move-object v13, v14

    .end local v14    # "outDeviceDescription":Ljava/io/OutputStream;
    .restart local v13    # "outDeviceDescription":Ljava/io/OutputStream;
    move-object v4, v5

    .end local v5    # "currentFilePath":Ljava/io/File;
    .restart local v4    # "currentFilePath":Ljava/io/File;
    move-object v6, v7

    .line 666
    .end local v7    # "deploymentFile":Ljava/io/File;
    .restart local v6    # "deploymentFile":Ljava/io/File;
    goto/16 :goto_0

    .line 664
    .end local v4    # "currentFilePath":Ljava/io/File;
    .end local v6    # "deploymentFile":Ljava/io/File;
    .end local v13    # "outDeviceDescription":Ljava/io/OutputStream;
    .restart local v5    # "currentFilePath":Ljava/io/File;
    .restart local v7    # "deploymentFile":Ljava/io/File;
    .restart local v14    # "outDeviceDescription":Ljava/io/OutputStream;
    :catch_4
    move-exception v8

    .line 665
    .restart local v8    # "e":Ljava/io/IOException;
    :try_start_f
    const-string v20, "FileShareDeviceWrapper"

    const-string v21, "copyDeploymentFilesFromRaw"

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "IOException:"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v20 .. v22}, Lcom/samsung/android/allshare/stack/fileshare/data/DLog;->e_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_1

    goto :goto_5

    .line 561
    .end local v8    # "e":Ljava/io/IOException;
    :catchall_1
    move-exception v19

    move-object v13, v14

    .end local v14    # "outDeviceDescription":Ljava/io/OutputStream;
    .restart local v13    # "outDeviceDescription":Ljava/io/OutputStream;
    move-object v4, v5

    .end local v5    # "currentFilePath":Ljava/io/File;
    .restart local v4    # "currentFilePath":Ljava/io/File;
    move-object v6, v7

    .end local v7    # "deploymentFile":Ljava/io/File;
    .restart local v6    # "deploymentFile":Ljava/io/File;
    goto/16 :goto_1

    .line 611
    .end local v4    # "currentFilePath":Ljava/io/File;
    .end local v6    # "deploymentFile":Ljava/io/File;
    .end local v13    # "outDeviceDescription":Ljava/io/OutputStream;
    .restart local v5    # "currentFilePath":Ljava/io/File;
    .restart local v7    # "deploymentFile":Ljava/io/File;
    .restart local v14    # "outDeviceDescription":Ljava/io/OutputStream;
    :cond_f
    :try_start_10
    invoke-virtual/range {p2 .. p2}, Ljava/lang/String;->length()I

    move-result v19

    if-nez v19, :cond_10

    .line 612
    new-instance v6, Ljava/io/File;

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v20, Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper;->deploymentFilePath:Ljava/lang/String;

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "/"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    sget-object v20, Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper;->FILE_TRANSPORT_FILE_NAME:Ljava/lang/String;

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-direct {v6, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V
    :try_end_10
    .catch Ljava/io/FileNotFoundException; {:try_start_10 .. :try_end_10} :catch_1
    .catch Ljava/io/IOException; {:try_start_10 .. :try_end_10} :catch_d
    .catchall {:try_start_10 .. :try_end_10} :catchall_6

    .line 613
    .end local v7    # "deploymentFile":Ljava/io/File;
    .restart local v6    # "deploymentFile":Ljava/io/File;
    :try_start_11
    new-instance v4, Ljava/io/File;

    sget-object v19, Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper;->deploymentFilePath:Ljava/lang/String;

    move-object/from16 v0, v19

    invoke-direct {v4, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V
    :try_end_11
    .catch Ljava/io/FileNotFoundException; {:try_start_11 .. :try_end_11} :catch_13
    .catch Ljava/io/IOException; {:try_start_11 .. :try_end_11} :catch_e
    .catchall {:try_start_11 .. :try_end_11} :catchall_7

    .line 629
    .end local v5    # "currentFilePath":Ljava/io/File;
    .restart local v4    # "currentFilePath":Ljava/io/File;
    :goto_6
    :try_start_12
    new-instance v16, Ljava/io/File;

    sget-object v19, Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper;->FILE_TRANSPORT_FILE_NAME:Ljava/lang/String;

    move-object/from16 v0, v16

    move-object/from16 v1, v19

    invoke-direct {v0, v4, v1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 630
    .local v16, "outFile2":Ljava/io/File;
    new-instance v18, Ljava/io/FileOutputStream;

    move-object/from16 v0, v18

    move-object/from16 v1, v16

    invoke-direct {v0, v1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_12
    .catch Ljava/io/FileNotFoundException; {:try_start_12 .. :try_end_12} :catch_6
    .catch Ljava/io/IOException; {:try_start_12 .. :try_end_12} :catch_f
    .catchall {:try_start_12 .. :try_end_12} :catchall_8

    .line 631
    .end local v17    # "outFileTransport":Ljava/io/OutputStream;
    .local v18, "outFileTransport":Ljava/io/OutputStream;
    const/16 v19, 0x400

    :try_start_13
    move/from16 v0, v19

    new-array v3, v0, [B

    .line 633
    .local v3, "buf2":[B
    :goto_7
    invoke-virtual {v10, v3}, Ljava/io/InputStream;->read([B)I

    move-result v12

    .local v12, "len2":I
    if-lez v12, :cond_12

    .line 634
    const/16 v19, 0x0

    move-object/from16 v0, v18

    move/from16 v1, v19

    invoke-virtual {v0, v3, v1, v12}, Ljava/io/OutputStream;->write([BII)V
    :try_end_13
    .catch Ljava/io/FileNotFoundException; {:try_start_13 .. :try_end_13} :catch_5
    .catch Ljava/io/IOException; {:try_start_13 .. :try_end_13} :catch_10
    .catchall {:try_start_13 .. :try_end_13} :catchall_9

    goto :goto_7

    .line 641
    .end local v3    # "buf2":[B
    .end local v12    # "len2":I
    :catch_5
    move-exception v8

    move-object/from16 v17, v18

    .end local v18    # "outFileTransport":Ljava/io/OutputStream;
    .restart local v17    # "outFileTransport":Ljava/io/OutputStream;
    move-object v13, v14

    .end local v14    # "outDeviceDescription":Ljava/io/OutputStream;
    .restart local v13    # "outDeviceDescription":Ljava/io/OutputStream;
    goto/16 :goto_4

    .line 615
    .end local v4    # "currentFilePath":Ljava/io/File;
    .end local v6    # "deploymentFile":Ljava/io/File;
    .end local v13    # "outDeviceDescription":Ljava/io/OutputStream;
    .end local v16    # "outFile2":Ljava/io/File;
    .restart local v5    # "currentFilePath":Ljava/io/File;
    .restart local v7    # "deploymentFile":Ljava/io/File;
    .restart local v14    # "outDeviceDescription":Ljava/io/OutputStream;
    :cond_10
    :try_start_14
    new-instance v6, Ljava/io/File;

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v20, Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper;->deploymentFilePath:Ljava/lang/String;

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "/"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    move-object/from16 v1, p2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "/"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    sget-object v20, Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper;->DEVICE_DECRIPTION_FILE_NAME:Ljava/lang/String;

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-direct {v6, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V
    :try_end_14
    .catch Ljava/io/FileNotFoundException; {:try_start_14 .. :try_end_14} :catch_1
    .catch Ljava/io/IOException; {:try_start_14 .. :try_end_14} :catch_d
    .catchall {:try_start_14 .. :try_end_14} :catchall_6

    .line 617
    .end local v7    # "deploymentFile":Ljava/io/File;
    .restart local v6    # "deploymentFile":Ljava/io/File;
    :try_start_15
    new-instance v4, Ljava/io/File;

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v20, Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper;->deploymentFilePath:Ljava/lang/String;

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "/"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    move-object/from16 v1, p2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-direct {v4, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V
    :try_end_15
    .catch Ljava/io/FileNotFoundException; {:try_start_15 .. :try_end_15} :catch_13
    .catch Ljava/io/IOException; {:try_start_15 .. :try_end_15} :catch_e
    .catchall {:try_start_15 .. :try_end_15} :catchall_7

    .line 619
    .end local v5    # "currentFilePath":Ljava/io/File;
    .restart local v4    # "currentFilePath":Ljava/io/File;
    :try_start_16
    invoke-virtual {v4}, Ljava/io/File;->exists()Z

    move-result v19

    if-nez v19, :cond_11

    .line 620
    invoke-virtual {v4}, Ljava/io/File;->mkdirs()Z

    move-result v19

    if-nez v19, :cond_11

    .line 621
    const-string v19, "FileShareDeviceWrapper"

    const-string v20, "copyDeploymentFilesFromRaw"

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "mkdirs["

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    sget-object v22, Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper;->DEVICE_DECRIPTION_FILE_NAME:Ljava/lang/String;

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v22, "] failed!"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v19 .. v21}, Lcom/samsung/android/allshare/stack/fileshare/data/DLog;->e_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 626
    :cond_11
    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v20, Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper;->deploymentFilePath:Ljava/lang/String;

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    move-object/from16 v1, p2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    sput-object v19, Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper;->deploymentFilePath:Ljava/lang/String;
    :try_end_16
    .catch Ljava/io/FileNotFoundException; {:try_start_16 .. :try_end_16} :catch_6
    .catch Ljava/io/IOException; {:try_start_16 .. :try_end_16} :catch_f
    .catchall {:try_start_16 .. :try_end_16} :catchall_8

    goto/16 :goto_6

    .line 641
    :catch_6
    move-exception v8

    move-object v13, v14

    .end local v14    # "outDeviceDescription":Ljava/io/OutputStream;
    .restart local v13    # "outDeviceDescription":Ljava/io/OutputStream;
    goto/16 :goto_4

    .line 637
    .end local v13    # "outDeviceDescription":Ljava/io/OutputStream;
    .end local v17    # "outFileTransport":Ljava/io/OutputStream;
    .restart local v3    # "buf2":[B
    .restart local v12    # "len2":I
    .restart local v14    # "outDeviceDescription":Ljava/io/OutputStream;
    .restart local v16    # "outFile2":Ljava/io/File;
    .restart local v18    # "outFileTransport":Ljava/io/OutputStream;
    :cond_12
    :try_start_17
    const-string v19, "FileShareDeviceWrapper"

    const-string v20, "copyDeploymentFilesFromRaw"

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "copy "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    sget-object v22, Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper;->FILE_TRANSPORT_FILE_NAME:Ljava/lang/String;

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v22, " successfully from "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v22, " to "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v19 .. v21}, Lcom/samsung/android/allshare/stack/fileshare/data/DLog;->d_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_17
    .catch Ljava/io/FileNotFoundException; {:try_start_17 .. :try_end_17} :catch_5
    .catch Ljava/io/IOException; {:try_start_17 .. :try_end_17} :catch_10
    .catchall {:try_start_17 .. :try_end_17} :catchall_9

    .line 652
    if-eqz v9, :cond_13

    .line 653
    :try_start_18
    invoke-virtual {v9}, Ljava/io/InputStream;->close()V

    .line 655
    :cond_13
    if-eqz v14, :cond_14

    .line 656
    invoke-virtual {v14}, Ljava/io/OutputStream;->close()V

    .line 658
    :cond_14
    if-eqz v10, :cond_15

    .line 659
    invoke-virtual {v10}, Ljava/io/InputStream;->close()V

    .line 661
    :cond_15
    if-eqz v18, :cond_16

    .line 662
    invoke-virtual/range {v18 .. v18}, Ljava/io/OutputStream;->close()V
    :try_end_18
    .catch Ljava/io/IOException; {:try_start_18 .. :try_end_18} :catch_7
    .catchall {:try_start_18 .. :try_end_18} :catchall_2

    .line 669
    :cond_16
    :goto_8
    const/16 v19, 0x1

    move-object/from16 v17, v18

    .end local v18    # "outFileTransport":Ljava/io/OutputStream;
    .restart local v17    # "outFileTransport":Ljava/io/OutputStream;
    move-object v13, v14

    .end local v14    # "outDeviceDescription":Ljava/io/OutputStream;
    .restart local v13    # "outDeviceDescription":Ljava/io/OutputStream;
    goto/16 :goto_0

    .line 664
    .end local v13    # "outDeviceDescription":Ljava/io/OutputStream;
    .end local v17    # "outFileTransport":Ljava/io/OutputStream;
    .restart local v14    # "outDeviceDescription":Ljava/io/OutputStream;
    .restart local v18    # "outFileTransport":Ljava/io/OutputStream;
    :catch_7
    move-exception v8

    .line 665
    .restart local v8    # "e":Ljava/io/IOException;
    :try_start_19
    const-string v19, "FileShareDeviceWrapper"

    const-string v20, "copyDeploymentFilesFromRaw"

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "IOException:"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v19 .. v21}, Lcom/samsung/android/allshare/stack/fileshare/data/DLog;->e_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_19
    .catchall {:try_start_19 .. :try_end_19} :catchall_2

    goto :goto_8

    .line 561
    .end local v8    # "e":Ljava/io/IOException;
    :catchall_2
    move-exception v19

    move-object/from16 v17, v18

    .end local v18    # "outFileTransport":Ljava/io/OutputStream;
    .restart local v17    # "outFileTransport":Ljava/io/OutputStream;
    move-object v13, v14

    .end local v14    # "outDeviceDescription":Ljava/io/OutputStream;
    .restart local v13    # "outDeviceDescription":Ljava/io/OutputStream;
    goto/16 :goto_1

    .line 645
    .end local v2    # "buf":[B
    .end local v3    # "buf2":[B
    .end local v11    # "len":I
    .end local v12    # "len2":I
    .end local v15    # "outFile":Ljava/io/File;
    .end local v16    # "outFile2":Ljava/io/File;
    :catch_8
    move-exception v8

    .line 646
    .restart local v8    # "e":Ljava/io/IOException;
    :goto_9
    :try_start_1a
    const-string v19, "FileShareDeviceWrapper"

    const-string v20, "copyDeploymentFilesFromRaw"

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "IOException:"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v19 .. v21}, Lcom/samsung/android/allshare/stack/fileshare/data/DLog;->e_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 647
    invoke-virtual {v8}, Ljava/io/IOException;->printStackTrace()V
    :try_end_1a
    .catchall {:try_start_1a .. :try_end_1a} :catchall_3

    .line 648
    const/16 v19, 0x0

    .line 652
    if-eqz v9, :cond_17

    .line 653
    :try_start_1b
    invoke-virtual {v9}, Ljava/io/InputStream;->close()V

    .line 655
    :cond_17
    if-eqz v13, :cond_18

    .line 656
    invoke-virtual {v13}, Ljava/io/OutputStream;->close()V

    .line 658
    :cond_18
    if-eqz v10, :cond_19

    .line 659
    invoke-virtual {v10}, Ljava/io/InputStream;->close()V

    .line 661
    :cond_19
    if-eqz v17, :cond_3

    .line 662
    invoke-virtual/range {v17 .. v17}, Ljava/io/OutputStream;->close()V
    :try_end_1b
    .catch Ljava/io/IOException; {:try_start_1b .. :try_end_1b} :catch_9
    .catchall {:try_start_1b .. :try_end_1b} :catchall_0

    goto/16 :goto_0

    .line 664
    :catch_9
    move-exception v8

    .line 665
    :try_start_1c
    const-string v20, "FileShareDeviceWrapper"

    const-string v21, "copyDeploymentFilesFromRaw"

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "IOException:"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v20 .. v22}, Lcom/samsung/android/allshare/stack/fileshare/data/DLog;->e_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1c
    .catchall {:try_start_1c .. :try_end_1c} :catchall_0

    goto/16 :goto_0

    .line 651
    .end local v8    # "e":Ljava/io/IOException;
    :catchall_3
    move-exception v19

    .line 652
    :goto_a
    if-eqz v9, :cond_1a

    .line 653
    :try_start_1d
    invoke-virtual {v9}, Ljava/io/InputStream;->close()V

    .line 655
    :cond_1a
    if-eqz v13, :cond_1b

    .line 656
    invoke-virtual {v13}, Ljava/io/OutputStream;->close()V

    .line 658
    :cond_1b
    if-eqz v10, :cond_1c

    .line 659
    invoke-virtual {v10}, Ljava/io/InputStream;->close()V

    .line 661
    :cond_1c
    if-eqz v17, :cond_1d

    .line 662
    invoke-virtual/range {v17 .. v17}, Ljava/io/OutputStream;->close()V
    :try_end_1d
    .catch Ljava/io/IOException; {:try_start_1d .. :try_end_1d} :catch_a
    .catchall {:try_start_1d .. :try_end_1d} :catchall_0

    .line 666
    :cond_1d
    :goto_b
    :try_start_1e
    throw v19

    .line 664
    :catch_a
    move-exception v8

    .line 665
    .restart local v8    # "e":Ljava/io/IOException;
    const-string v20, "FileShareDeviceWrapper"

    const-string v21, "copyDeploymentFilesFromRaw"

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "IOException:"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v20 .. v22}, Lcom/samsung/android/allshare/stack/fileshare/data/DLog;->e_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1e
    .catchall {:try_start_1e .. :try_end_1e} :catchall_0

    goto :goto_b

    .line 651
    .end local v6    # "deploymentFile":Ljava/io/File;
    .end local v8    # "e":Ljava/io/IOException;
    .restart local v7    # "deploymentFile":Ljava/io/File;
    :catchall_4
    move-exception v19

    move-object v6, v7

    .end local v7    # "deploymentFile":Ljava/io/File;
    .restart local v6    # "deploymentFile":Ljava/io/File;
    goto :goto_a

    .end local v4    # "currentFilePath":Ljava/io/File;
    .end local v6    # "deploymentFile":Ljava/io/File;
    .restart local v5    # "currentFilePath":Ljava/io/File;
    .restart local v7    # "deploymentFile":Ljava/io/File;
    :catchall_5
    move-exception v19

    move-object v4, v5

    .end local v5    # "currentFilePath":Ljava/io/File;
    .restart local v4    # "currentFilePath":Ljava/io/File;
    move-object v6, v7

    .end local v7    # "deploymentFile":Ljava/io/File;
    .restart local v6    # "deploymentFile":Ljava/io/File;
    goto :goto_a

    .end local v4    # "currentFilePath":Ljava/io/File;
    .end local v6    # "deploymentFile":Ljava/io/File;
    .end local v13    # "outDeviceDescription":Ljava/io/OutputStream;
    .restart local v5    # "currentFilePath":Ljava/io/File;
    .restart local v7    # "deploymentFile":Ljava/io/File;
    .restart local v14    # "outDeviceDescription":Ljava/io/OutputStream;
    .restart local v15    # "outFile":Ljava/io/File;
    :catchall_6
    move-exception v19

    move-object v13, v14

    .end local v14    # "outDeviceDescription":Ljava/io/OutputStream;
    .restart local v13    # "outDeviceDescription":Ljava/io/OutputStream;
    move-object v4, v5

    .end local v5    # "currentFilePath":Ljava/io/File;
    .restart local v4    # "currentFilePath":Ljava/io/File;
    move-object v6, v7

    .end local v7    # "deploymentFile":Ljava/io/File;
    .restart local v6    # "deploymentFile":Ljava/io/File;
    goto :goto_a

    .end local v4    # "currentFilePath":Ljava/io/File;
    .end local v13    # "outDeviceDescription":Ljava/io/OutputStream;
    .restart local v2    # "buf":[B
    .restart local v5    # "currentFilePath":Ljava/io/File;
    .restart local v11    # "len":I
    .restart local v14    # "outDeviceDescription":Ljava/io/OutputStream;
    :catchall_7
    move-exception v19

    move-object v13, v14

    .end local v14    # "outDeviceDescription":Ljava/io/OutputStream;
    .restart local v13    # "outDeviceDescription":Ljava/io/OutputStream;
    move-object v4, v5

    .end local v5    # "currentFilePath":Ljava/io/File;
    .restart local v4    # "currentFilePath":Ljava/io/File;
    goto :goto_a

    .end local v13    # "outDeviceDescription":Ljava/io/OutputStream;
    .restart local v14    # "outDeviceDescription":Ljava/io/OutputStream;
    :catchall_8
    move-exception v19

    move-object v13, v14

    .end local v14    # "outDeviceDescription":Ljava/io/OutputStream;
    .restart local v13    # "outDeviceDescription":Ljava/io/OutputStream;
    goto :goto_a

    .end local v13    # "outDeviceDescription":Ljava/io/OutputStream;
    .end local v17    # "outFileTransport":Ljava/io/OutputStream;
    .restart local v14    # "outDeviceDescription":Ljava/io/OutputStream;
    .restart local v16    # "outFile2":Ljava/io/File;
    .restart local v18    # "outFileTransport":Ljava/io/OutputStream;
    :catchall_9
    move-exception v19

    move-object/from16 v17, v18

    .end local v18    # "outFileTransport":Ljava/io/OutputStream;
    .restart local v17    # "outFileTransport":Ljava/io/OutputStream;
    move-object v13, v14

    .end local v14    # "outDeviceDescription":Ljava/io/OutputStream;
    .restart local v13    # "outDeviceDescription":Ljava/io/OutputStream;
    goto :goto_a

    .line 645
    .end local v2    # "buf":[B
    .end local v6    # "deploymentFile":Ljava/io/File;
    .end local v11    # "len":I
    .end local v15    # "outFile":Ljava/io/File;
    .end local v16    # "outFile2":Ljava/io/File;
    .restart local v7    # "deploymentFile":Ljava/io/File;
    :catch_b
    move-exception v8

    move-object v6, v7

    .end local v7    # "deploymentFile":Ljava/io/File;
    .restart local v6    # "deploymentFile":Ljava/io/File;
    goto/16 :goto_9

    .end local v4    # "currentFilePath":Ljava/io/File;
    .end local v6    # "deploymentFile":Ljava/io/File;
    .restart local v5    # "currentFilePath":Ljava/io/File;
    .restart local v7    # "deploymentFile":Ljava/io/File;
    :catch_c
    move-exception v8

    move-object v4, v5

    .end local v5    # "currentFilePath":Ljava/io/File;
    .restart local v4    # "currentFilePath":Ljava/io/File;
    move-object v6, v7

    .end local v7    # "deploymentFile":Ljava/io/File;
    .restart local v6    # "deploymentFile":Ljava/io/File;
    goto/16 :goto_9

    .end local v4    # "currentFilePath":Ljava/io/File;
    .end local v6    # "deploymentFile":Ljava/io/File;
    .end local v13    # "outDeviceDescription":Ljava/io/OutputStream;
    .restart local v5    # "currentFilePath":Ljava/io/File;
    .restart local v7    # "deploymentFile":Ljava/io/File;
    .restart local v14    # "outDeviceDescription":Ljava/io/OutputStream;
    .restart local v15    # "outFile":Ljava/io/File;
    :catch_d
    move-exception v8

    move-object v13, v14

    .end local v14    # "outDeviceDescription":Ljava/io/OutputStream;
    .restart local v13    # "outDeviceDescription":Ljava/io/OutputStream;
    move-object v4, v5

    .end local v5    # "currentFilePath":Ljava/io/File;
    .restart local v4    # "currentFilePath":Ljava/io/File;
    move-object v6, v7

    .end local v7    # "deploymentFile":Ljava/io/File;
    .restart local v6    # "deploymentFile":Ljava/io/File;
    goto/16 :goto_9

    .end local v4    # "currentFilePath":Ljava/io/File;
    .end local v13    # "outDeviceDescription":Ljava/io/OutputStream;
    .restart local v2    # "buf":[B
    .restart local v5    # "currentFilePath":Ljava/io/File;
    .restart local v11    # "len":I
    .restart local v14    # "outDeviceDescription":Ljava/io/OutputStream;
    :catch_e
    move-exception v8

    move-object v13, v14

    .end local v14    # "outDeviceDescription":Ljava/io/OutputStream;
    .restart local v13    # "outDeviceDescription":Ljava/io/OutputStream;
    move-object v4, v5

    .end local v5    # "currentFilePath":Ljava/io/File;
    .restart local v4    # "currentFilePath":Ljava/io/File;
    goto/16 :goto_9

    .end local v13    # "outDeviceDescription":Ljava/io/OutputStream;
    .restart local v14    # "outDeviceDescription":Ljava/io/OutputStream;
    :catch_f
    move-exception v8

    move-object v13, v14

    .end local v14    # "outDeviceDescription":Ljava/io/OutputStream;
    .restart local v13    # "outDeviceDescription":Ljava/io/OutputStream;
    goto/16 :goto_9

    .end local v13    # "outDeviceDescription":Ljava/io/OutputStream;
    .end local v17    # "outFileTransport":Ljava/io/OutputStream;
    .restart local v14    # "outDeviceDescription":Ljava/io/OutputStream;
    .restart local v16    # "outFile2":Ljava/io/File;
    .restart local v18    # "outFileTransport":Ljava/io/OutputStream;
    :catch_10
    move-exception v8

    move-object/from16 v17, v18

    .end local v18    # "outFileTransport":Ljava/io/OutputStream;
    .restart local v17    # "outFileTransport":Ljava/io/OutputStream;
    move-object v13, v14

    .end local v14    # "outDeviceDescription":Ljava/io/OutputStream;
    .restart local v13    # "outDeviceDescription":Ljava/io/OutputStream;
    goto/16 :goto_9

    .line 641
    .end local v2    # "buf":[B
    .end local v11    # "len":I
    .end local v15    # "outFile":Ljava/io/File;
    .end local v16    # "outFile2":Ljava/io/File;
    :catch_11
    move-exception v8

    goto/16 :goto_4

    .end local v6    # "deploymentFile":Ljava/io/File;
    .restart local v7    # "deploymentFile":Ljava/io/File;
    :catch_12
    move-exception v8

    move-object v6, v7

    .end local v7    # "deploymentFile":Ljava/io/File;
    .restart local v6    # "deploymentFile":Ljava/io/File;
    goto/16 :goto_4

    .end local v4    # "currentFilePath":Ljava/io/File;
    .end local v13    # "outDeviceDescription":Ljava/io/OutputStream;
    .restart local v2    # "buf":[B
    .restart local v5    # "currentFilePath":Ljava/io/File;
    .restart local v11    # "len":I
    .restart local v14    # "outDeviceDescription":Ljava/io/OutputStream;
    .restart local v15    # "outFile":Ljava/io/File;
    :catch_13
    move-exception v8

    move-object v13, v14

    .end local v14    # "outDeviceDescription":Ljava/io/OutputStream;
    .restart local v13    # "outDeviceDescription":Ljava/io/OutputStream;
    move-object v4, v5

    .end local v5    # "currentFilePath":Ljava/io/File;
    .restart local v4    # "currentFilePath":Ljava/io/File;
    goto/16 :goto_4
.end method

.method public static getInstance()Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper;
    .locals 2

    .prologue
    .line 140
    sget-object v0, Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper;->instance:Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper;

    if-nez v0, :cond_1

    .line 141
    const-class v1, Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper;

    monitor-enter v1

    .line 142
    :try_start_0
    sget-object v0, Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper;->instance:Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper;

    if-nez v0, :cond_0

    .line 143
    new-instance v0, Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper;

    invoke-direct {v0}, Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper;-><init>()V

    sput-object v0, Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper;->instance:Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper;

    .line 145
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 147
    :cond_1
    sget-object v0, Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper;->instance:Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper;

    return-object v0

    .line 145
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method private getSessionInfo(Ljava/lang/String;)Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceSessionInfo;
    .locals 2
    .param p1, "sessionId"    # Ljava/lang/String;

    .prologue
    .line 673
    new-instance v0, Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceSessionInfo;

    invoke-direct {v0}, Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceSessionInfo;-><init>()V

    .line 674
    .local v0, "sessionInfo":Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceSessionInfo;
    const/4 v1, 0x0

    invoke-virtual {p0, p1, v0, v1}, Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper;->getSession(Ljava/lang/String;Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceSessionInfo;Ljava/util/ArrayList;)I

    .line 675
    return-object v0
.end method

.method private handleEventMessage(Landroid/os/Message;)V
    .locals 5
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 90
    iget-object v1, p0, Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper;->mSessionEventListener:Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper$ISessionEvent;

    if-nez v1, :cond_0

    .line 91
    const-string v1, "FileShareDeviceWrapper"

    const-string v2, "handleEventMessage"

    const-string v3, "mSessionEventListener is null"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/stack/fileshare/data/DLog;->e_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 137
    :goto_0
    return-void

    .line 95
    :cond_0
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper$EventData;

    .line 97
    .local v0, "mEventData":Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper$EventData;
    if-nez v0, :cond_1

    .line 98
    const-string v1, "FileShareDeviceWrapper"

    const-string v2, "handleEventMessage"

    const-string v3, "mEventData is null"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/stack/fileshare/data/DLog;->e_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 102
    :cond_1
    iget v1, p1, Landroid/os/Message;->what:I

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 112
    :pswitch_0
    const-string v1, "FileShareDeviceWrapper"

    const-string v2, "handleEventMessage"

    const-string v3, "onSessionCancel is called"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/stack/fileshare/data/DLog;->d_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 113
    iget-object v1, p0, Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper;->mSessionEventListener:Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper$ISessionEvent;

    iget-object v2, v0, Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper$EventData;->mSessData:Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceSessionInfo;

    iget-boolean v3, v0, Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper$EventData;->byClient:Z

    invoke-interface {v1, v2, v3}, Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper$ISessionEvent;->onSessionCancel(Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceSessionInfo;Z)V

    goto :goto_0

    .line 104
    :pswitch_1
    const-string v1, "FileShareDeviceWrapper"

    const-string v2, "handleEventMessage"

    const-string v3, "onNewSessionRequest is called"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/stack/fileshare/data/DLog;->d_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 105
    iget-object v1, p0, Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper;->mSessionEventListener:Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper$ISessionEvent;

    iget-object v2, v0, Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper$EventData;->mSessData:Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceSessionInfo;

    invoke-interface {v1, v2}, Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper$ISessionEvent;->onNewSessionRequest(Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceSessionInfo;)V

    goto :goto_0

    .line 108
    :pswitch_2
    const-string v1, "FileShareDeviceWrapper"

    const-string v2, "handleEventMessage"

    const-string v3, "onSessionClose is called"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/stack/fileshare/data/DLog;->d_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 109
    iget-object v1, p0, Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper;->mSessionEventListener:Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper$ISessionEvent;

    iget-object v2, v0, Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper$EventData;->mSessData:Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceSessionInfo;

    invoke-interface {v1, v2}, Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper$ISessionEvent;->onSessionClose(Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceSessionInfo;)V

    goto :goto_0

    .line 116
    :pswitch_3
    const-string v1, "FileShareDeviceWrapper"

    const-string v2, "handleEventMessage"

    const-string v3, "onSessionTransportStart is called"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/stack/fileshare/data/DLog;->d_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 117
    iget-object v1, p0, Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper;->mSessionEventListener:Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper$ISessionEvent;

    iget-object v2, v0, Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper$EventData;->mSessData:Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceSessionInfo;

    iget-object v3, v0, Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper$EventData;->mTransData:Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceTransportInfo;

    invoke-interface {v1, v2, v3}, Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper$ISessionEvent;->onSessionTransportStart(Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceSessionInfo;Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceTransportInfo;)V

    goto :goto_0

    .line 121
    :pswitch_4
    iget-object v1, p0, Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper;->mSessionEventListener:Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper$ISessionEvent;

    iget-object v2, v0, Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper$EventData;->mSessData:Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceSessionInfo;

    iget-object v3, v0, Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper$EventData;->mTransData:Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceTransportInfo;

    invoke-interface {v1, v2, v3}, Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper$ISessionEvent;->onSessionTransportProgress(Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceSessionInfo;Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceTransportInfo;)V

    goto :goto_0

    .line 125
    :pswitch_5
    const-string v1, "FileShareDeviceWrapper"

    const-string v2, "handleEventMessage"

    const-string v3, "onSessionTransportFinish is called"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/stack/fileshare/data/DLog;->d_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 126
    iget-object v1, p0, Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper;->mSessionEventListener:Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper$ISessionEvent;

    iget-object v2, v0, Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper$EventData;->mSessData:Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceSessionInfo;

    iget-object v3, v0, Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper$EventData;->mTransData:Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceTransportInfo;

    invoke-interface {v1, v2, v3}, Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper$ISessionEvent;->onSessionTransportFinish(Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceSessionInfo;Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceTransportInfo;)V

    goto :goto_0

    .line 130
    :pswitch_6
    const-string v1, "FileShareDeviceWrapper"

    const-string v2, "handleEventMessage"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onSessionTransportError is called - "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, v0, Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper$EventData;->nErrorCode:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/stack/fileshare/data/DLog;->d_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 132
    iget-object v1, p0, Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper;->mSessionEventListener:Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper$ISessionEvent;

    iget-object v2, v0, Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper$EventData;->mSessData:Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceSessionInfo;

    iget v3, v0, Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper$EventData;->nErrorCode:I

    iget-object v4, v0, Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper$EventData;->mTransData:Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceTransportInfo;

    invoke-interface {v1, v2, v3, v4}, Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper$ISessionEvent;->onSessionTransportError(Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceSessionInfo;ILcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceTransportInfo;)V

    goto/16 :goto_0

    .line 102
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_6
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method


# virtual methods
.method public acceptSession(Ljava/lang/String;)I
    .locals 3
    .param p1, "sessionId"    # Ljava/lang/String;

    .prologue
    .line 361
    iget-object v2, p0, Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper;->sessionInfoMaps:Ljava/util/HashMap;

    monitor-enter v2

    .line 362
    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper;->sessionInfoMaps:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/allshare/stack/fileshare/data/FTDRequestSessionInfo;

    .line 363
    .local v0, "sessionInfo":Lcom/samsung/android/allshare/stack/fileshare/data/FTDRequestSessionInfo;
    if-eqz v0, :cond_0

    .line 364
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/samsung/android/allshare/stack/fileshare/data/FTDRequestSessionInfo;->setRequestStatus(Z)V

    .line 365
    const/4 v1, 0x0

    monitor-exit v2

    .line 368
    :goto_0
    return v1

    .line 367
    :cond_0
    monitor-exit v2

    .line 368
    const/4 v1, -0x1

    goto :goto_0

    .line 367
    .end local v0    # "sessionInfo":Lcom/samsung/android/allshare/stack/fileshare/data/FTDRequestSessionInfo;
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public addDeviceEventListener(Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper$IDeviceEvent;)V
    .locals 0
    .param p1, "listener"    # Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper$IDeviceEvent;

    .prologue
    .line 189
    iput-object p1, p0, Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper;->mDeviceEventListener:Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper$IDeviceEvent;

    .line 190
    return-void
.end method

.method public addSessionEventListener(Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper$ISessionEvent;)V
    .locals 0
    .param p1, "listener"    # Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper$ISessionEvent;

    .prologue
    .line 197
    iput-object p1, p0, Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper;->mSessionEventListener:Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper$ISessionEvent;

    .line 198
    return-void
.end method

.method public cancelSession(Ljava/lang/String;)I
    .locals 6
    .param p1, "sessionId"    # Ljava/lang/String;

    .prologue
    .line 300
    invoke-static {p1}, Lcom/samsung/android/allshare/stack/fileshare/AllShareFrameworkCoreNativeAPI;->cancelFTDSession(Ljava/lang/String;)I

    move-result v0

    .line 301
    .local v0, "result":I
    if-eqz v0, :cond_0

    .line 302
    const-string v2, "FileShareDeviceWrapper"

    const-string v3, "cancelSession"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "cancel FTD failed! sessionId = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/samsung/android/allshare/stack/fileshare/data/DLog;->e_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 307
    :goto_0
    return v0

    .line 304
    :cond_0
    invoke-direct {p0, p1}, Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper;->getSessionInfo(Ljava/lang/String;)Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceSessionInfo;

    move-result-object v1

    .line 305
    .local v1, "sessionInfo":Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceSessionInfo;
    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper;->onCancelSessionReceived(Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceSessionInfo;Z)V

    goto :goto_0
.end method

.method public getFriendlyName(Ljava/lang/StringBuffer;)I
    .locals 5
    .param p1, "name"    # Ljava/lang/StringBuffer;

    .prologue
    .line 337
    invoke-static {p1}, Lcom/samsung/android/allshare/stack/fileshare/AllShareFrameworkCoreNativeAPI;->getFTDFriendlyName(Ljava/lang/StringBuffer;)I

    move-result v0

    .line 338
    .local v0, "result":I
    if-eqz v0, :cond_0

    .line 339
    const-string v1, "FileShareDeviceWrapper"

    const-string v2, "getFriendlyName"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "get FTD name("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ") failed!"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/stack/fileshare/data/DLog;->e_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 341
    :cond_0
    return v0
.end method

.method public getHTTPPort(Lcom/samsung/android/allshare/stack/fileshare/data/FileShareHTTPPortInfo;)I
    .locals 5
    .param p1, "port"    # Lcom/samsung/android/allshare/stack/fileshare/data/FileShareHTTPPortInfo;

    .prologue
    .line 353
    invoke-static {p1}, Lcom/samsung/android/allshare/stack/fileshare/AllShareFrameworkCoreNativeAPI;->getFTDHTTPPort(Lcom/samsung/android/allshare/stack/fileshare/data/FileShareHTTPPortInfo;)I

    move-result v0

    .line 354
    .local v0, "result":I
    if-eqz v0, :cond_0

    .line 355
    const-string v1, "FileShareDeviceWrapper"

    const-string v2, "getHTTPPort"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "get HTTP port failed!, HTTP port= "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/stack/fileshare/data/DLog;->e_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 357
    :cond_0
    return v0
.end method

.method public getSession(Ljava/lang/String;Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceSessionInfo;Ljava/util/ArrayList;)I
    .locals 5
    .param p1, "sessionId"    # Ljava/lang/String;
    .param p2, "session"    # Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceSessionInfo;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceSessionInfo;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceTransportInfo;",
            ">;)I"
        }
    .end annotation

    .prologue
    .line 312
    .local p3, "transportInfos":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceTransportInfo;>;"
    if-nez p2, :cond_0

    .line 313
    new-instance p2, Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceSessionInfo;

    .end local p2    # "session":Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceSessionInfo;
    invoke-direct {p2}, Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceSessionInfo;-><init>()V

    .line 316
    .restart local p2    # "session":Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceSessionInfo;
    :cond_0
    if-nez p3, :cond_1

    .line 317
    new-instance p3, Ljava/util/ArrayList;

    .end local p3    # "transportInfos":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceTransportInfo;>;"
    invoke-direct {p3}, Ljava/util/ArrayList;-><init>()V

    .line 320
    .restart local p3    # "transportInfos":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceTransportInfo;>;"
    :cond_1
    invoke-static {p1, p2, p3}, Lcom/samsung/android/allshare/stack/fileshare/AllShareFrameworkCoreNativeAPI;->getFTDSession(Ljava/lang/String;Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceSessionInfo;Ljava/util/ArrayList;)I

    move-result v0

    .line 322
    .local v0, "result":I
    if-eqz v0, :cond_2

    .line 323
    const-string v1, "FileShareDeviceWrapper"

    const-string v2, "getSession"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "get FTD session("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ") failed!"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/stack/fileshare/data/DLog;->e_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 325
    :cond_2
    return v0
.end method

.method public getUDN(Ljava/lang/StringBuffer;)I
    .locals 5
    .param p1, "udn"    # Ljava/lang/StringBuffer;

    .prologue
    .line 345
    invoke-static {p1}, Lcom/samsung/android/allshare/stack/fileshare/AllShareFrameworkCoreNativeAPI;->getFTDUDN(Ljava/lang/StringBuffer;)I

    move-result v0

    .line 346
    .local v0, "result":I
    if-eqz v0, :cond_0

    .line 347
    const-string v1, "FileShareDeviceWrapper"

    const-string v2, "getUDN"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "get FTD udn failed!, UDN= "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/stack/fileshare/data/DLog;->e_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 349
    :cond_0
    return v0
.end method

.method public declared-synchronized initialize(Landroid/content/Context;Ljava/lang/String;)I
    .locals 7
    .param p1, "svcCoxt"    # Landroid/content/Context;
    .param p2, "tempFilePath"    # Ljava/lang/String;

    .prologue
    .line 214
    monitor-enter p0

    :try_start_0
    invoke-static {}, Lcom/samsung/android/allshare/stack/fileshare/api/AllShareFrameworkCoreWrapper;->getInstance()Lcom/samsung/android/allshare/stack/fileshare/api/AllShareFrameworkCoreWrapper;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/android/allshare/stack/fileshare/api/AllShareFrameworkCoreWrapper;->initialize()I

    move-result v1

    .line 215
    .local v1, "result":I
    if-eqz v1, :cond_0

    .line 216
    const-string v3, "FileShareDeviceWrapper"

    const-string v4, "initialize"

    const-string v5, "initialize AllShareFrameworkCore failed !"

    invoke-static {v3, v4, v5}, Lcom/samsung/android/allshare/stack/fileshare/data/DLog;->e_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 217
    const/4 v3, -0x1

    .line 246
    :goto_0
    monitor-exit p0

    return v3

    .line 220
    :cond_0
    :try_start_1
    const-string v0, ""

    .line 221
    .local v0, "deviceConfigPath":Ljava/lang/String;
    const-string v2, ""

    .line 224
    .local v2, "serviceConfigPath":Ljava/lang/String;
    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v3

    invoke-virtual {v3}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v3

    sput-object v3, Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper;->deploymentFilePath:Ljava/lang/String;

    .line 226
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper;->copyDeploymentFilesFromRaw(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 227
    sget-object v0, Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper;->deploymentFilePath:Ljava/lang/String;

    .line 228
    sget-object v2, Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper;->deploymentFilePath:Ljava/lang/String;

    .line 235
    new-instance v3, Lcom/samsung/android/allshare/stack/fileshare/monitor/SystemEventMonitor;

    invoke-direct {v3, p1}, Lcom/samsung/android/allshare/stack/fileshare/monitor/SystemEventMonitor;-><init>(Landroid/content/Context;)V

    iput-object v3, p0, Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper;->mSystemEventMonitor:Lcom/samsung/android/allshare/stack/fileshare/monitor/SystemEventMonitor;

    .line 236
    iget-object v3, p0, Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper;->mSystemEventMonitor:Lcom/samsung/android/allshare/stack/fileshare/monitor/SystemEventMonitor;

    new-instance v4, Lcom/samsung/android/allshare/stack/fileshare/monitor/NetworkStatusListener;

    invoke-direct {v4}, Lcom/samsung/android/allshare/stack/fileshare/monitor/NetworkStatusListener;-><init>()V

    invoke-virtual {v3, v4}, Lcom/samsung/android/allshare/stack/fileshare/monitor/SystemEventMonitor;->addSystemEventListener(Lcom/samsung/android/allshare/stack/fileshare/monitor/ISystemEventListener;)V

    .line 237
    iget-object v3, p0, Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper;->mSystemEventMonitor:Lcom/samsung/android/allshare/stack/fileshare/monitor/SystemEventMonitor;

    invoke-virtual {v3}, Lcom/samsung/android/allshare/stack/fileshare/monitor/SystemEventMonitor;->startMonitor()V

    .line 240
    invoke-static {v0, v2}, Lcom/samsung/android/allshare/stack/fileshare/AllShareFrameworkCoreNativeAPI;->initializeFTD(Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    .line 241
    if-eqz v1, :cond_2

    .line 242
    const-string v3, "FileShareDeviceWrapper"

    const-string v4, "initialize"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "initialize FTD failed : deviceConfigPath = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", serviceConfigPath = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/samsung/android/allshare/stack/fileshare/data/DLog;->e_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 244
    const/4 v3, -0x3

    goto :goto_0

    .line 230
    :cond_1
    const-string v3, "FileShareDeviceWrapper"

    const-string v4, "initialize"

    const-string v5, "copyDeploymentFilesFromRaw failed !"

    invoke-static {v3, v4, v5}, Lcom/samsung/android/allshare/stack/fileshare/data/DLog;->e_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 231
    const/4 v3, -0x2

    goto :goto_0

    :cond_2
    move v3, v1

    .line 246
    goto :goto_0

    .line 214
    .end local v0    # "deviceConfigPath":Ljava/lang/String;
    .end local v1    # "result":I
    .end local v2    # "serviceConfigPath":Ljava/lang/String;
    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3
.end method

.method onCancelSessionReceived(Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceSessionInfo;Z)V
    .locals 6
    .param p1, "session"    # Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceSessionInfo;
    .param p2, "byClient"    # Z

    .prologue
    .line 440
    const-string v2, "FileShareDeviceWrapper"

    const-string v3, "onCancelSessionReceived"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, " sessionId = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p1, Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceSessionInfo;->sessionId:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", byClient = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/samsung/android/allshare/stack/fileshare/data/DLog;->d_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 443
    iget-object v2, p0, Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper;->mGUIEventHandler:Landroid/os/Handler;

    if-nez v2, :cond_0

    .line 444
    const-string v2, "FileShareDeviceWrapper"

    const-string v3, "onCancelSessionReceived"

    const-string v4, "mGUIEventHandler is null. "

    invoke-static {v2, v3, v4}, Lcom/samsung/android/allshare/stack/fileshare/data/DLog;->d_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 455
    :goto_0
    return-void

    .line 448
    :cond_0
    new-instance v1, Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper$EventData;

    invoke-direct {v1, p0}, Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper$EventData;-><init>(Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper;)V

    .line 449
    .local v1, "mMsgData":Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper$EventData;
    iput-object p1, v1, Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper$EventData;->mSessData:Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceSessionInfo;

    .line 450
    iput-boolean p2, v1, Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper$EventData;->byClient:Z

    .line 452
    iget-object v2, p0, Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper;->mGUIEventHandler:Landroid/os/Handler;

    const/4 v3, 0x1

    invoke-virtual {v2, v3, v1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 453
    .local v0, "eventMsg":Landroid/os/Message;
    iget-object v2, p0, Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper;->mGUIEventHandler:Landroid/os/Handler;

    invoke-virtual {v2, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method onCloseSessionReceived(Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceSessionInfo;)V
    .locals 5
    .param p1, "session"    # Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceSessionInfo;

    .prologue
    .line 458
    iget-object v2, p0, Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper;->mGUIEventHandler:Landroid/os/Handler;

    if-nez v2, :cond_0

    .line 459
    const-string v2, "FileShareDeviceWrapper"

    const-string v3, "onCloseSessionReceived"

    const-string v4, "mGUIEventHandler is null. "

    invoke-static {v2, v3, v4}, Lcom/samsung/android/allshare/stack/fileshare/data/DLog;->d_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 469
    :goto_0
    return-void

    .line 463
    :cond_0
    new-instance v1, Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper$EventData;

    invoke-direct {v1, p0}, Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper$EventData;-><init>(Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper;)V

    .line 464
    .local v1, "mMsgData":Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper$EventData;
    iput-object p1, v1, Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper$EventData;->mSessData:Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceSessionInfo;

    .line 466
    iget-object v2, p0, Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper;->mGUIEventHandler:Landroid/os/Handler;

    const/4 v3, 0x2

    invoke-virtual {v2, v3, v1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 467
    .local v0, "eventMsg":Landroid/os/Message;
    iget-object v2, p0, Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper;->mGUIEventHandler:Landroid/os/Handler;

    invoke-virtual {v2, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method onCreateSessionReceived(Ljava/lang/String;IJLcom/samsung/android/allshare/stack/fileshare/data/FTDSessionAcceptInfo;)V
    .locals 13
    .param p1, "sessionId"    # Ljava/lang/String;
    .param p2, "count"    # I
    .param p3, "totalSize"    # J
    .param p5, "isAccept"    # Lcom/samsung/android/allshare/stack/fileshare/data/FTDSessionAcceptInfo;

    .prologue
    .line 398
    const-string v8, "FileShareDeviceWrapper"

    const-string v9, "onCreateSessionReceived "

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "sessionId="

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ",count="

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ",totalSize="

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    move-wide/from16 v0, p3

    invoke-virtual {v10, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ",isAccept.addr="

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ",isAccept="

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    move-object/from16 v0, p5

    iget-boolean v11, v0, Lcom/samsung/android/allshare/stack/fileshare/data/FTDSessionAcceptInfo;->isAccept:Z

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v8, v9, v10}, Lcom/samsung/android/allshare/stack/fileshare/data/DLog;->d_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 402
    new-instance v4, Ljava/lang/StringBuffer;

    invoke-direct {v4, p1}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 403
    .local v4, "localSessionID":Ljava/lang/StringBuffer;
    new-instance v7, Lcom/samsung/android/allshare/stack/fileshare/data/FTDRequestSessionInfo;

    invoke-direct {v7}, Lcom/samsung/android/allshare/stack/fileshare/data/FTDRequestSessionInfo;-><init>()V

    .line 404
    .local v7, "sessionInfo":Lcom/samsung/android/allshare/stack/fileshare/data/FTDRequestSessionInfo;
    move-object/from16 v0, p5

    invoke-virtual {v7, v0}, Lcom/samsung/android/allshare/stack/fileshare/data/FTDRequestSessionInfo;->setAcceptInfo(Lcom/samsung/android/allshare/stack/fileshare/data/FTDSessionAcceptInfo;)V

    .line 405
    invoke-virtual {v7, p2}, Lcom/samsung/android/allshare/stack/fileshare/data/FTDRequestSessionInfo;->setCount(I)V

    .line 406
    move-wide/from16 v0, p3

    invoke-virtual {v7, v0, v1}, Lcom/samsung/android/allshare/stack/fileshare/data/FTDRequestSessionInfo;->setTotalSize(J)V

    .line 407
    iget-object v9, p0, Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper;->sessionInfoMaps:Ljava/util/HashMap;

    monitor-enter v9

    .line 408
    :try_start_0
    iget-object v8, p0, Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper;->sessionInfoMaps:Ljava/util/HashMap;

    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v10, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 409
    monitor-exit v9
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 411
    iget-object v8, p0, Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper;->mGUIEventHandler:Landroid/os/Handler;

    if-nez v8, :cond_0

    .line 412
    const-string v8, "FileShareDeviceWrapper"

    const-string v9, "onCreateSessionReceived"

    const-string v10, "mGUIEventHandler is null. "

    invoke-static {v8, v9, v10}, Lcom/samsung/android/allshare/stack/fileshare/data/DLog;->d_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 437
    :goto_0
    return-void

    .line 409
    :catchall_0
    move-exception v8

    :try_start_1
    monitor-exit v9
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v8

    .line 416
    :cond_0
    invoke-direct {p0, p1}, Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper;->getSessionInfo(Ljava/lang/String;)Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceSessionInfo;

    move-result-object v6

    .line 418
    .local v6, "session":Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceSessionInfo;
    new-instance v5, Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper$EventData;

    invoke-direct {v5, p0}, Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper$EventData;-><init>(Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper;)V

    .line 419
    .local v5, "mMsgData":Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper$EventData;
    iput-object v6, v5, Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper$EventData;->mSessData:Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceSessionInfo;

    .line 421
    iget-object v8, p0, Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper;->mGUIEventHandler:Landroid/os/Handler;

    const/4 v9, 0x3

    invoke-virtual {v8, v9, v5}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v3

    .line 422
    .local v3, "eventMsg":Landroid/os/Message;
    iget-object v8, p0, Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper;->mGUIEventHandler:Landroid/os/Handler;

    invoke-virtual {v8, v3}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 424
    monitor-enter v7

    .line 425
    :try_start_2
    invoke-virtual {v7}, Lcom/samsung/android/allshare/stack/fileshare/data/FTDRequestSessionInfo;->isHandled()Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    move-result v8

    if-nez v8, :cond_1

    .line 427
    const-wide/16 v8, 0x7530

    :try_start_3
    invoke-virtual {v7, v8, v9}, Ljava/lang/Object;->wait(J)V
    :try_end_3
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    .line 433
    :cond_1
    :goto_1
    :try_start_4
    monitor-exit v7
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    .line 434
    iget-object v9, p0, Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper;->sessionInfoMaps:Ljava/util/HashMap;

    monitor-enter v9

    .line 435
    :try_start_5
    iget-object v8, p0, Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper;->sessionInfoMaps:Ljava/util/HashMap;

    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 436
    monitor-exit v9

    goto :goto_0

    :catchall_1
    move-exception v8

    monitor-exit v9
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    throw v8

    .line 428
    :catch_0
    move-exception v2

    .line 429
    .local v2, "e":Ljava/lang/InterruptedException;
    :try_start_6
    const-string v8, "FileShareDeviceWrapper"

    const-string v9, "onCreateSessionReceived"

    const-string v10, "Waiting for accept timeout...."

    invoke-static {v8, v9, v10}, Lcom/samsung/android/allshare/stack/fileshare/data/DLog;->e_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 433
    .end local v2    # "e":Ljava/lang/InterruptedException;
    :catchall_2
    move-exception v8

    monitor-exit v7
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    throw v8
.end method

.method onTransferItemErrorReceived(Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceSessionInfo;ILjava/util/ArrayList;)V
    .locals 6
    .param p1, "session"    # Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceSessionInfo;
    .param p2, "errorDesc"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceSessionInfo;",
            "I",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceTransportInfo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 538
    .local p3, "transport":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceTransportInfo;>;"
    const-string v3, "FileShareDeviceWrapper"

    const-string v4, "onTransferItemErrorReceived"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, " sessionId = "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v5, p1, Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceSessionInfo;->sessionId:Ljava/lang/String;

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, ", errorDesc = "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, ", "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "transport.size() = "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p3}, Ljava/util/ArrayList;->size()I

    move-result v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    :goto_0
    invoke-static {v3, v4, v2}, Lcom/samsung/android/allshare/stack/fileshare/data/DLog;->d_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 542
    iget-object v2, p0, Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper;->mGUIEventHandler:Landroid/os/Handler;

    if-nez v2, :cond_1

    .line 543
    const-string v2, "FileShareDeviceWrapper"

    const-string v3, "onTransferItemErrorReceived"

    const-string v4, "mGUIEventHandler is null. "

    invoke-static {v2, v3, v4}, Lcom/samsung/android/allshare/stack/fileshare/data/DLog;->d_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 557
    :goto_1
    return-void

    .line 538
    :cond_0
    const-string v2, "transport is null! "

    goto :goto_0

    .line 547
    :cond_1
    new-instance v1, Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper$EventData;

    invoke-direct {v1, p0}, Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper$EventData;-><init>(Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper;)V

    .line 548
    .local v1, "mMsgData":Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper$EventData;
    iput-object p1, v1, Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper$EventData;->mSessData:Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceSessionInfo;

    .line 549
    if-eqz p3, :cond_2

    invoke-virtual {p3}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_2

    .line 550
    const/4 v2, 0x0

    invoke-virtual {p3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceTransportInfo;

    iput-object v2, v1, Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper$EventData;->mTransData:Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceTransportInfo;

    .line 552
    :cond_2
    iput p2, v1, Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper$EventData;->nErrorCode:I

    .line 554
    iget-object v2, p0, Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper;->mGUIEventHandler:Landroid/os/Handler;

    const/4 v3, 0x4

    invoke-virtual {v2, v3, v1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 555
    .local v0, "eventMsg":Landroid/os/Message;
    iget-object v2, p0, Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper;->mGUIEventHandler:Landroid/os/Handler;

    invoke-virtual {v2, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_1
.end method

.method onTransferItemFinishedReceived(Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceSessionInfo;Ljava/util/ArrayList;)V
    .locals 8
    .param p1, "session"    # Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceSessionInfo;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceSessionInfo;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceTransportInfo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 515
    .local p2, "transport":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceTransportInfo;>;"
    iget-object v2, p0, Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper;->mGUIEventHandler:Landroid/os/Handler;

    if-nez v2, :cond_0

    .line 516
    const-string v2, "FileShareDeviceWrapper"

    const-string v3, "onTransferItemFinishedReceived"

    const-string v4, "mGUIEventHandler is null. "

    invoke-static {v2, v3, v4}, Lcom/samsung/android/allshare/stack/fileshare/data/DLog;->d_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 533
    :goto_0
    return-void

    .line 520
    :cond_0
    const-string v2, "FileShareDeviceWrapper"

    const-string v3, "onTransferItemFinishedReceived"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "sessionId="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p1, Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceSessionInfo;->sessionId:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ",totalSize="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-wide v6, p1, Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceSessionInfo;->totalSize:J

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ",totalCount="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-wide v6, p1, Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceSessionInfo;->totalCount:J

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ",successCount="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-wide v6, p1, Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceSessionInfo;->successCount:J

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/samsung/android/allshare/stack/fileshare/data/DLog;->d_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 524
    new-instance v1, Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper$EventData;

    invoke-direct {v1, p0}, Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper$EventData;-><init>(Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper;)V

    .line 525
    .local v1, "mMsgData":Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper$EventData;
    iput-object p1, v1, Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper$EventData;->mSessData:Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceSessionInfo;

    .line 526
    if-eqz p2, :cond_1

    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_1

    .line 527
    const/4 v2, 0x0

    invoke-virtual {p2, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceTransportInfo;

    iput-object v2, v1, Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper$EventData;->mTransData:Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceTransportInfo;

    .line 530
    :cond_1
    iget-object v2, p0, Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper;->mGUIEventHandler:Landroid/os/Handler;

    const/4 v3, 0x7

    invoke-virtual {v2, v3, v1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 531
    .local v0, "eventMsg":Landroid/os/Message;
    iget-object v2, p0, Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper;->mGUIEventHandler:Landroid/os/Handler;

    invoke-virtual {v2, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method onTransferItemProgressReceived(Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceSessionInfo;Ljava/util/ArrayList;)V
    .locals 5
    .param p1, "session"    # Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceSessionInfo;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceSessionInfo;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceTransportInfo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 496
    .local p2, "transport":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceTransportInfo;>;"
    iget-object v2, p0, Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper;->mGUIEventHandler:Landroid/os/Handler;

    if-nez v2, :cond_0

    .line 497
    const-string v2, "FileShareDeviceWrapper"

    const-string v3, "onTransferItemProgressReceived"

    const-string v4, "mGUIEventHandler is null. "

    invoke-static {v2, v3, v4}, Lcom/samsung/android/allshare/stack/fileshare/data/DLog;->d_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 510
    :goto_0
    return-void

    .line 501
    :cond_0
    new-instance v1, Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper$EventData;

    invoke-direct {v1, p0}, Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper$EventData;-><init>(Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper;)V

    .line 502
    .local v1, "mMsgData":Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper$EventData;
    iput-object p1, v1, Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper$EventData;->mSessData:Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceSessionInfo;

    .line 503
    if-eqz p2, :cond_1

    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_1

    .line 504
    const/4 v2, 0x0

    invoke-virtual {p2, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceTransportInfo;

    iput-object v2, v1, Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper$EventData;->mTransData:Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceTransportInfo;

    .line 507
    :cond_1
    iget-object v2, p0, Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper;->mGUIEventHandler:Landroid/os/Handler;

    const/4 v3, 0x6

    invoke-virtual {v2, v3, v1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 508
    .local v0, "eventMsg":Landroid/os/Message;
    iget-object v2, p0, Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper;->mGUIEventHandler:Landroid/os/Handler;

    invoke-virtual {v2, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method onTransferItemRequestReceived(Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceSessionInfo;Ljava/util/ArrayList;)V
    .locals 6
    .param p1, "session"    # Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceSessionInfo;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceSessionInfo;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceTransportInfo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 474
    .local p2, "transport":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceTransportInfo;>;"
    const-string v2, "FileShareDeviceWrapper"

    const-string v3, "onTransferItemRequestReceived"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, " sessionId = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p1, Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceSessionInfo;->sessionId:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/samsung/android/allshare/stack/fileshare/data/DLog;->d_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 477
    iget-object v2, p0, Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper;->mGUIEventHandler:Landroid/os/Handler;

    if-nez v2, :cond_0

    .line 478
    const-string v2, "FileShareDeviceWrapper"

    const-string v3, "onTransferItemRequestReceived"

    const-string v4, " mGUIEventHandler is null. "

    invoke-static {v2, v3, v4}, Lcom/samsung/android/allshare/stack/fileshare/data/DLog;->d_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 491
    :goto_0
    return-void

    .line 482
    :cond_0
    new-instance v1, Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper$EventData;

    invoke-direct {v1, p0}, Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper$EventData;-><init>(Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper;)V

    .line 483
    .local v1, "mMsgData":Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper$EventData;
    iput-object p1, v1, Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper$EventData;->mSessData:Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceSessionInfo;

    .line 484
    if-eqz p2, :cond_1

    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_1

    .line 485
    const/4 v2, 0x0

    invoke-virtual {p2, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceTransportInfo;

    iput-object v2, v1, Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper$EventData;->mTransData:Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceTransportInfo;

    .line 488
    :cond_1
    iget-object v2, p0, Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper;->mGUIEventHandler:Landroid/os/Handler;

    const/4 v3, 0x5

    invoke-virtual {v2, v3, v1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 489
    .local v0, "eventMsg":Landroid/os/Message;
    iget-object v2, p0, Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper;->mGUIEventHandler:Landroid/os/Handler;

    invoke-virtual {v2, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method public rejectSession(Ljava/lang/String;)I
    .locals 4
    .param p1, "sessionId"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 372
    iget-object v2, p0, Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper;->sessionInfoMaps:Ljava/util/HashMap;

    monitor-enter v2

    .line 373
    :try_start_0
    iget-object v3, p0, Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper;->sessionInfoMaps:Ljava/util/HashMap;

    invoke-virtual {v3, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/allshare/stack/fileshare/data/FTDRequestSessionInfo;

    .line 374
    .local v0, "sessionInfo":Lcom/samsung/android/allshare/stack/fileshare/data/FTDRequestSessionInfo;
    if-eqz v0, :cond_0

    .line 375
    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Lcom/samsung/android/allshare/stack/fileshare/data/FTDRequestSessionInfo;->setRequestStatus(Z)V

    .line 376
    monitor-exit v2

    .line 379
    :goto_0
    return v1

    .line 378
    :cond_0
    monitor-exit v2

    .line 379
    const/4 v1, -0x1

    goto :goto_0

    .line 378
    .end local v0    # "sessionInfo":Lcom/samsung/android/allshare/stack/fileshare/data/FTDRequestSessionInfo;
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public removeDeviceEventListener(Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper$IDeviceEvent;)V
    .locals 1
    .param p1, "listener"    # Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper$IDeviceEvent;

    .prologue
    .line 193
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper;->mDeviceEventListener:Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper$IDeviceEvent;

    .line 194
    return-void
.end method

.method public removeSessionEventListener(Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper$ISessionEvent;)V
    .locals 1
    .param p1, "listener"    # Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper$ISessionEvent;

    .prologue
    .line 201
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper;->mSessionEventListener:Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper$ISessionEvent;

    .line 202
    return-void
.end method

.method public setFreindlyName(Ljava/lang/String;)I
    .locals 5
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 329
    invoke-static {p1}, Lcom/samsung/android/allshare/stack/fileshare/AllShareFrameworkCoreNativeAPI;->setFTDFreindlyName(Ljava/lang/String;)I

    move-result v0

    .line 330
    .local v0, "result":I
    if-eqz v0, :cond_0

    .line 331
    const-string v1, "FileShareDeviceWrapper"

    const-string v2, "setFreindlyName"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "set FTD name("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ") failed!"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/stack/fileshare/data/DLog;->e_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 333
    :cond_0
    return v0
.end method

.method public setGUIEventListenr(Landroid/os/Looper;)V
    .locals 1
    .param p1, "looper"    # Landroid/os/Looper;

    .prologue
    .line 388
    new-instance v0, Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper$1;

    invoke-direct {v0, p0, p1}, Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper$1;-><init>(Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper;->mGUIEventHandler:Landroid/os/Handler;

    .line 393
    return-void
.end method

.method public start(Ljava/lang/String;)I
    .locals 5
    .param p1, "downloadPath"    # Ljava/lang/String;

    .prologue
    .line 271
    invoke-static {p1}, Lcom/samsung/android/allshare/stack/fileshare/AllShareFrameworkCoreNativeAPI;->startFTD(Ljava/lang/String;)I

    move-result v0

    .line 272
    .local v0, "result":I
    if-eqz v0, :cond_0

    .line 273
    const-string v1, "FileShareDeviceWrapper"

    const-string v2, "start"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "start FTD failed : downloadPath = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/stack/fileshare/data/DLog;->e_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 275
    :cond_0
    return v0
.end method

.method public stop()I
    .locals 4

    .prologue
    .line 280
    iget-object v1, p0, Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper;->mGUIEventHandler:Landroid/os/Handler;

    if-eqz v1, :cond_0

    .line 281
    iget-object v1, p0, Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper;->mGUIEventHandler:Landroid/os/Handler;

    const/4 v2, 0x3

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 282
    iget-object v1, p0, Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper;->mGUIEventHandler:Landroid/os/Handler;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 283
    iget-object v1, p0, Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper;->mGUIEventHandler:Landroid/os/Handler;

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 284
    iget-object v1, p0, Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper;->mGUIEventHandler:Landroid/os/Handler;

    const/4 v2, 0x5

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 285
    iget-object v1, p0, Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper;->mGUIEventHandler:Landroid/os/Handler;

    const/4 v2, 0x6

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 286
    iget-object v1, p0, Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper;->mGUIEventHandler:Landroid/os/Handler;

    const/4 v2, 0x7

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 287
    iget-object v1, p0, Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper;->mGUIEventHandler:Landroid/os/Handler;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 289
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper;->mGUIEventHandler:Landroid/os/Handler;

    .line 292
    :cond_0
    invoke-static {}, Lcom/samsung/android/allshare/stack/fileshare/AllShareFrameworkCoreNativeAPI;->stopFTD()I

    move-result v0

    .line 293
    .local v0, "result":I
    if-eqz v0, :cond_1

    .line 294
    const-string v1, "FileShareDeviceWrapper"

    const-string v2, "stop"

    const-string v3, "stop FTD failed!"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/stack/fileshare/data/DLog;->e_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 296
    :cond_1
    return v0
.end method

.method public terminate()I
    .locals 4

    .prologue
    .line 251
    invoke-static {}, Lcom/samsung/android/allshare/stack/fileshare/AllShareFrameworkCoreNativeAPI;->terminateFTD()I

    move-result v0

    .line 252
    .local v0, "result":I
    if-eqz v0, :cond_0

    .line 253
    const-string v1, "FileShareDeviceWrapper"

    const-string v2, "terminate"

    const-string v3, "terminate FTD failed !"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/stack/fileshare/data/DLog;->e_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 257
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper;->mSystemEventMonitor:Lcom/samsung/android/allshare/stack/fileshare/monitor/SystemEventMonitor;

    if-eqz v1, :cond_1

    .line 258
    iget-object v1, p0, Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper;->mSystemEventMonitor:Lcom/samsung/android/allshare/stack/fileshare/monitor/SystemEventMonitor;

    invoke-virtual {v1}, Lcom/samsung/android/allshare/stack/fileshare/monitor/SystemEventMonitor;->stopMonitor()V

    .line 259
    iget-object v1, p0, Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper;->mSystemEventMonitor:Lcom/samsung/android/allshare/stack/fileshare/monitor/SystemEventMonitor;

    invoke-virtual {v1}, Lcom/samsung/android/allshare/stack/fileshare/monitor/SystemEventMonitor;->fini()V

    .line 263
    :cond_1
    invoke-static {}, Lcom/samsung/android/allshare/stack/fileshare/api/AllShareFrameworkCoreWrapper;->getInstance()Lcom/samsung/android/allshare/stack/fileshare/api/AllShareFrameworkCoreWrapper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/allshare/stack/fileshare/api/AllShareFrameworkCoreWrapper;->terminate()I

    move-result v0

    .line 264
    if-eqz v0, :cond_2

    .line 265
    const-string v1, "FileShareDeviceWrapper"

    const-string v2, "terminate"

    const-string v3, "terminate AllShareFrameworkCore failed !"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/stack/fileshare/data/DLog;->e_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 267
    :cond_2
    return v0
.end method
