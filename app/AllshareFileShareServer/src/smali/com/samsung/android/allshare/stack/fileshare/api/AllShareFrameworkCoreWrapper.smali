.class public Lcom/samsung/android/allshare/stack/fileshare/api/AllShareFrameworkCoreWrapper;
.super Ljava/lang/Object;
.source "AllShareFrameworkCoreWrapper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/allshare/stack/fileshare/api/AllShareFrameworkCoreWrapper$INetworkExceptionListener;,
        Lcom/samsung/android/allshare/stack/fileshare/api/AllShareFrameworkCoreWrapper$TypeMismatchException;,
        Lcom/samsung/android/allshare/stack/fileshare/api/AllShareFrameworkCoreWrapper$ErrorCode;
    }
.end annotation


# static fields
.field private static final LOG_TAG:Ljava/lang/String; = "AllShareFrameworkCoreWrapper"

.field private static instance:Lcom/samsung/android/allshare/stack/fileshare/api/AllShareFrameworkCoreWrapper;

.field private static libraryLoaded:Z


# instance fields
.field private isAllShareFrameworkCoreStarted:Z

.field private networkExceptionListeners:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/allshare/stack/fileshare/api/AllShareFrameworkCoreWrapper$INetworkExceptionListener;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 53
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/android/allshare/stack/fileshare/api/AllShareFrameworkCoreWrapper;->instance:Lcom/samsung/android/allshare/stack/fileshare/api/AllShareFrameworkCoreWrapper;

    .line 55
    const/4 v0, 0x0

    sput-boolean v0, Lcom/samsung/android/allshare/stack/fileshare/api/AllShareFrameworkCoreWrapper;->libraryLoaded:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/allshare/stack/fileshare/api/AllShareFrameworkCoreWrapper;->isAllShareFrameworkCoreStarted:Z

    .line 59
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/allshare/stack/fileshare/api/AllShareFrameworkCoreWrapper;->networkExceptionListeners:Ljava/util/List;

    .line 69
    return-void
.end method

.method public static declared-synchronized getInstance()Lcom/samsung/android/allshare/stack/fileshare/api/AllShareFrameworkCoreWrapper;
    .locals 2

    .prologue
    .line 88
    const-class v1, Lcom/samsung/android/allshare/stack/fileshare/api/AllShareFrameworkCoreWrapper;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/samsung/android/allshare/stack/fileshare/api/AllShareFrameworkCoreWrapper;->instance:Lcom/samsung/android/allshare/stack/fileshare/api/AllShareFrameworkCoreWrapper;

    if-nez v0, :cond_0

    .line 89
    new-instance v0, Lcom/samsung/android/allshare/stack/fileshare/api/AllShareFrameworkCoreWrapper;

    invoke-direct {v0}, Lcom/samsung/android/allshare/stack/fileshare/api/AllShareFrameworkCoreWrapper;-><init>()V

    sput-object v0, Lcom/samsung/android/allshare/stack/fileshare/api/AllShareFrameworkCoreWrapper;->instance:Lcom/samsung/android/allshare/stack/fileshare/api/AllShareFrameworkCoreWrapper;

    .line 91
    :cond_0
    sget-object v0, Lcom/samsung/android/allshare/stack/fileshare/api/AllShareFrameworkCoreWrapper;->instance:Lcom/samsung/android/allshare/stack/fileshare/api/AllShareFrameworkCoreWrapper;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 88
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public addNetworkExceptionListener(Lcom/samsung/android/allshare/stack/fileshare/api/AllShareFrameworkCoreWrapper$INetworkExceptionListener;)Z
    .locals 1
    .param p1, "listener"    # Lcom/samsung/android/allshare/stack/fileshare/api/AllShareFrameworkCoreWrapper$INetworkExceptionListener;

    .prologue
    .line 155
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/fileshare/api/AllShareFrameworkCoreWrapper;->networkExceptionListeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method initialize()I
    .locals 6

    .prologue
    const/4 v2, 0x1

    .line 101
    iget-boolean v3, p0, Lcom/samsung/android/allshare/stack/fileshare/api/AllShareFrameworkCoreWrapper;->isAllShareFrameworkCoreStarted:Z

    if-nez v3, :cond_1

    .line 105
    sget-boolean v3, Lcom/samsung/android/allshare/stack/fileshare/api/AllShareFrameworkCoreWrapper;->libraryLoaded:Z

    if-nez v3, :cond_0

    .line 109
    :try_start_0
    const-string v3, "asf_fileshareserver"

    invoke-static {v3}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 111
    const/4 v3, 0x1

    sput-boolean v3, Lcom/samsung/android/allshare/stack/fileshare/api/AllShareFrameworkCoreWrapper;->libraryLoaded:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 122
    :cond_0
    const/4 v1, 0x0

    .line 123
    .local v1, "result":I
    invoke-static {}, Lcom/samsung/android/allshare/stack/fileshare/AllShareFrameworkCoreNativeAPI;->initializeAllShareFrameworkCore()I

    move-result v1

    .line 124
    if-eqz v1, :cond_2

    .line 125
    const-string v2, "AllShareFrameworkCoreWrapper"

    const-string v3, "initialize"

    const-string v4, "Initialize AllShareFrameworkCore failed!"

    invoke-static {v2, v3, v4}, Lcom/samsung/android/allshare/stack/fileshare/data/DLog;->e_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 129
    .end local v1    # "result":I
    :goto_0
    return v1

    .line 112
    :catch_0
    move-exception v0

    .line 113
    .local v0, "ex":Ljava/lang/Exception;
    const/4 v3, 0x0

    sput-boolean v3, Lcom/samsung/android/allshare/stack/fileshare/api/AllShareFrameworkCoreWrapper;->libraryLoaded:Z

    .line 114
    const-string v3, "AllShareFrameworkCoreWrapper"

    const-string v4, "initialize"

    const-string v5, "loadLibrary failure!"

    invoke-static {v3, v4, v5, v0}, Lcom/samsung/android/allshare/stack/fileshare/data/DLog;->e_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    move v1, v2

    .line 115
    goto :goto_0

    .line 119
    .end local v0    # "ex":Ljava/lang/Exception;
    :cond_1
    const/4 v1, 0x2

    goto :goto_0

    .line 127
    .restart local v1    # "result":I
    :cond_2
    iput-boolean v2, p0, Lcom/samsung/android/allshare/stack/fileshare/api/AllShareFrameworkCoreWrapper;->isAllShareFrameworkCoreStarted:Z

    goto :goto_0
.end method

.method public notifyNICAdded(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
    .locals 1
    .param p1, "nic"    # Ljava/lang/String;
    .param p2, "ip"    # Ljava/lang/String;
    .param p3, "subnetMask"    # Ljava/lang/String;

    .prologue
    .line 184
    invoke-static {p1, p2, p3}, Lcom/samsung/android/allshare/stack/fileshare/AllShareFrameworkCoreNativeAPI;->notifyNICAdded(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public notifyNICRemoved(Ljava/lang/String;)I
    .locals 1
    .param p1, "nic"    # Ljava/lang/String;

    .prologue
    .line 188
    invoke-static {p1}, Lcom/samsung/android/allshare/stack/fileshare/AllShareFrameworkCoreNativeAPI;->notifyNICRemoved(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method protected onNetworkExceptionNotify(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "exceptionType"    # Ljava/lang/String;
    .param p2, "exceptionValue"    # Ljava/lang/String;
    .param p3, "deviceUDN"    # Ljava/lang/String;

    .prologue
    .line 177
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/samsung/android/allshare/stack/fileshare/api/AllShareFrameworkCoreWrapper;->networkExceptionListeners:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 178
    iget-object v1, p0, Lcom/samsung/android/allshare/stack/fileshare/api/AllShareFrameworkCoreWrapper;->networkExceptionListeners:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/allshare/stack/fileshare/api/AllShareFrameworkCoreWrapper$INetworkExceptionListener;

    invoke-interface {v1, p1, p2, p3}, Lcom/samsung/android/allshare/stack/fileshare/api/AllShareFrameworkCoreWrapper$INetworkExceptionListener;->onNetworkExceptionNotify(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 177
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 181
    :cond_0
    return-void
.end method

.method public removeNetworkExceptionListener(Lcom/samsung/android/allshare/stack/fileshare/api/AllShareFrameworkCoreWrapper$INetworkExceptionListener;)Z
    .locals 1
    .param p1, "listener"    # Lcom/samsung/android/allshare/stack/fileshare/api/AllShareFrameworkCoreWrapper$INetworkExceptionListener;

    .prologue
    .line 165
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/fileshare/api/AllShareFrameworkCoreWrapper;->networkExceptionListeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method terminate()I
    .locals 4

    .prologue
    .line 139
    invoke-static {}, Lcom/samsung/android/allshare/stack/fileshare/AllShareFrameworkCoreNativeAPI;->terminateAllShareFrameworkCore()I

    move-result v0

    .line 140
    .local v0, "result":I
    if-eqz v0, :cond_0

    .line 141
    const-string v1, "AllShareFrameworkCoreWrapper"

    const-string v2, "terminate"

    const-string v3, "Terminate AllShareFrameworkCore failed!"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/stack/fileshare/data/DLog;->e_service(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 143
    :cond_0
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/samsung/android/allshare/stack/fileshare/api/AllShareFrameworkCoreWrapper;->isAllShareFrameworkCoreStarted:Z

    .line 144
    iget-object v1, p0, Lcom/samsung/android/allshare/stack/fileshare/api/AllShareFrameworkCoreWrapper;->networkExceptionListeners:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 145
    return v0
.end method
