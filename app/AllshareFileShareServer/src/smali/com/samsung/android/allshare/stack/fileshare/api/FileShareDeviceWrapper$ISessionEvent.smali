.class public interface abstract Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper$ISessionEvent;
.super Ljava/lang/Object;
.source "FileShareDeviceWrapper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "ISessionEvent"
.end annotation


# virtual methods
.method public abstract onNewSessionRequest(Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceSessionInfo;)V
.end method

.method public abstract onSessionCancel(Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceSessionInfo;Z)V
.end method

.method public abstract onSessionClose(Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceSessionInfo;)V
.end method

.method public abstract onSessionTransportError(Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceSessionInfo;ILcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceTransportInfo;)V
.end method

.method public abstract onSessionTransportFinish(Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceSessionInfo;Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceTransportInfo;)V
.end method

.method public abstract onSessionTransportProgress(Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceSessionInfo;Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceTransportInfo;)V
.end method

.method public abstract onSessionTransportStart(Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceSessionInfo;Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceTransportInfo;)V
.end method
