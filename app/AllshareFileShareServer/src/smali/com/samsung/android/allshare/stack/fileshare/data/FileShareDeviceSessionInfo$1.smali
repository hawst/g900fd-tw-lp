.class Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceSessionInfo$1;
.super Ljava/lang/Object;
.source "FileShareDeviceSessionInfo.java"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceSessionInfo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceSessionInfo;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceSessionInfo;


# direct methods
.method constructor <init>(Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceSessionInfo;)V
    .locals 0

    .prologue
    .line 43
    iput-object p1, p0, Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceSessionInfo$1;->this$0:Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceSessionInfo;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public createFromParcel(Landroid/os/Parcel;)Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceSessionInfo;
    .locals 4
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 46
    new-instance v0, Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceSessionInfo;

    invoke-direct {v0}, Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceSessionInfo;-><init>()V

    .line 47
    .local v0, "session":Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceSessionInfo;
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceSessionInfo;->status:Ljava/lang/String;

    .line 48
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceSessionInfo;->sessionId:Ljava/lang/String;

    .line 49
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceSessionInfo;->friendlyName:Ljava/lang/String;

    .line 50
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceSessionInfo;->description:Ljava/lang/String;

    .line 51
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceSessionInfo;->transportDescription:Ljava/lang/String;

    .line 52
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v2

    iput-wide v2, v0, Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceSessionInfo;->totalSize:J

    .line 53
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v2

    iput-wide v2, v0, Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceSessionInfo;->totalCount:J

    .line 54
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v2

    iput-wide v2, v0, Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceSessionInfo;->successCount:J

    .line 55
    return-object v0
.end method

.method public bridge synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # Landroid/os/Parcel;

    .prologue
    .line 43
    invoke-virtual {p0, p1}, Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceSessionInfo$1;->createFromParcel(Landroid/os/Parcel;)Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceSessionInfo;

    move-result-object v0

    return-object v0
.end method

.method public newArray(I)[Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceSessionInfo;
    .locals 1
    .param p1, "size"    # I

    .prologue
    .line 60
    new-array v0, p1, [Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceSessionInfo;

    return-object v0
.end method

.method public bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # I

    .prologue
    .line 43
    invoke-virtual {p0, p1}, Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceSessionInfo$1;->newArray(I)[Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceSessionInfo;

    move-result-object v0

    return-object v0
.end method
