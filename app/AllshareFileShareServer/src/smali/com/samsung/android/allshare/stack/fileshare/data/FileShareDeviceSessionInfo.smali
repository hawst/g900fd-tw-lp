.class public Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceSessionInfo;
.super Ljava/lang/Object;
.source "FileShareDeviceSessionInfo.java"

# interfaces
.implements Landroid/os/Parcelable;


# instance fields
.field public final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceSessionInfo;",
            ">;"
        }
    .end annotation
.end field

.field public description:Ljava/lang/String;

.field public friendlyName:Ljava/lang/String;

.field public sessionId:Ljava/lang/String;

.field public status:Ljava/lang/String;

.field public successCount:J

.field public totalCount:J

.field public totalSize:J

.field public transportDescription:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 9
    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceSessionInfo;->status:Ljava/lang/String;

    .line 11
    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceSessionInfo;->sessionId:Ljava/lang/String;

    .line 13
    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceSessionInfo;->friendlyName:Ljava/lang/String;

    .line 15
    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceSessionInfo;->description:Ljava/lang/String;

    .line 17
    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceSessionInfo;->transportDescription:Ljava/lang/String;

    .line 19
    iput-wide v2, p0, Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceSessionInfo;->totalSize:J

    .line 21
    iput-wide v2, p0, Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceSessionInfo;->totalCount:J

    .line 23
    iput-wide v2, p0, Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceSessionInfo;->successCount:J

    .line 43
    new-instance v0, Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceSessionInfo$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceSessionInfo$1;-><init>(Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceSessionInfo;)V

    iput-object v0, p0, Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceSessionInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 2

    .prologue
    .line 27
    iget-object v1, p0, Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceSessionInfo;->sessionId:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 28
    .local v0, "value":I
    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2
    .param p1, "parcel"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 33
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceSessionInfo;->status:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 34
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceSessionInfo;->sessionId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 35
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceSessionInfo;->friendlyName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 36
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceSessionInfo;->description:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 37
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceSessionInfo;->transportDescription:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 38
    iget-wide v0, p0, Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceSessionInfo;->totalSize:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 39
    iget-wide v0, p0, Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceSessionInfo;->totalCount:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 40
    iget-wide v0, p0, Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceSessionInfo;->successCount:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 41
    return-void
.end method
