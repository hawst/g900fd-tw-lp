.class Lcom/samsung/android/allshare/stack/fileshare/monitor/SystemEventMonitor$1;
.super Landroid/os/Handler;
.source "SystemEventMonitor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/allshare/stack/fileshare/monitor/SystemEventMonitor;->notifyEvent(I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/allshare/stack/fileshare/monitor/SystemEventMonitor;


# direct methods
.method constructor <init>(Lcom/samsung/android/allshare/stack/fileshare/monitor/SystemEventMonitor;Landroid/os/Looper;)V
    .locals 0
    .param p2, "x0"    # Landroid/os/Looper;

    .prologue
    .line 130
    iput-object p1, p0, Lcom/samsung/android/allshare/stack/fileshare/monitor/SystemEventMonitor$1;->this$0:Lcom/samsung/android/allshare/stack/fileshare/monitor/SystemEventMonitor;

    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 133
    iget v3, p1, Landroid/os/Message;->what:I

    packed-switch v3, :pswitch_data_0

    .line 143
    :cond_0
    return-void

    .line 135
    :pswitch_0
    iget-object v3, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 137
    .local v0, "event":I
    iget-object v3, p0, Lcom/samsung/android/allshare/stack/fileshare/monitor/SystemEventMonitor$1;->this$0:Lcom/samsung/android/allshare/stack/fileshare/monitor/SystemEventMonitor;

    # getter for: Lcom/samsung/android/allshare/stack/fileshare/monitor/SystemEventMonitor;->mSystemEventListeners:Ljava/util/List;
    invoke-static {v3}, Lcom/samsung/android/allshare/stack/fileshare/monitor/SystemEventMonitor;->access$000(Lcom/samsung/android/allshare/stack/fileshare/monitor/SystemEventMonitor;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/allshare/stack/fileshare/monitor/ISystemEventListener;

    .line 138
    .local v2, "l":Lcom/samsung/android/allshare/stack/fileshare/monitor/ISystemEventListener;
    iget-object v3, p0, Lcom/samsung/android/allshare/stack/fileshare/monitor/SystemEventMonitor$1;->this$0:Lcom/samsung/android/allshare/stack/fileshare/monitor/SystemEventMonitor;

    # getter for: Lcom/samsung/android/allshare/stack/fileshare/monitor/SystemEventMonitor;->mP2pInterfaceName:Ljava/lang/String;
    invoke-static {v3}, Lcom/samsung/android/allshare/stack/fileshare/monitor/SystemEventMonitor;->access$100(Lcom/samsung/android/allshare/stack/fileshare/monitor/SystemEventMonitor;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v0, v3}, Lcom/samsung/android/allshare/stack/fileshare/monitor/ISystemEventListener;->eventNotifyReceived(ILjava/lang/String;)V

    goto :goto_0

    .line 133
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method
