.class Lcom/samsung/android/app/FileShareServer/ThumbnailManager$BitmapGetterTask;
.super Landroid/os/AsyncTask;
.source "ThumbnailManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/FileShareServer/ThumbnailManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "BitmapGetterTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Void;",
        "Landroid/graphics/Bitmap;",
        ">;"
    }
.end annotation


# instance fields
.field private final imageViewReference:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/widget/ImageView;",
            ">;"
        }
    .end annotation
.end field

.field private path:Ljava/lang/String;

.field final synthetic this$0:Lcom/samsung/android/app/FileShareServer/ThumbnailManager;


# direct methods
.method public constructor <init>(Lcom/samsung/android/app/FileShareServer/ThumbnailManager;Landroid/widget/ImageView;)V
    .locals 1
    .param p2, "imageView"    # Landroid/widget/ImageView;

    .prologue
    .line 111
    iput-object p1, p0, Lcom/samsung/android/app/FileShareServer/ThumbnailManager$BitmapGetterTask;->this$0:Lcom/samsung/android/app/FileShareServer/ThumbnailManager;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 112
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p2}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/samsung/android/app/FileShareServer/ThumbnailManager$BitmapGetterTask;->imageViewReference:Ljava/lang/ref/WeakReference;

    .line 113
    return-void
.end method

.method static synthetic access$300(Lcom/samsung/android/app/FileShareServer/ThumbnailManager$BitmapGetterTask;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/FileShareServer/ThumbnailManager$BitmapGetterTask;

    .prologue
    .line 106
    iget-object v0, p0, Lcom/samsung/android/app/FileShareServer/ThumbnailManager$BitmapGetterTask;->path:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 2
    .param p1, "params"    # [Ljava/lang/String;

    .prologue
    .line 117
    const/4 v0, 0x0

    aget-object v0, p1, v0

    iput-object v0, p0, Lcom/samsung/android/app/FileShareServer/ThumbnailManager$BitmapGetterTask;->path:Ljava/lang/String;

    .line 118
    iget-object v0, p0, Lcom/samsung/android/app/FileShareServer/ThumbnailManager$BitmapGetterTask;->this$0:Lcom/samsung/android/app/FileShareServer/ThumbnailManager;

    iget-object v1, p0, Lcom/samsung/android/app/FileShareServer/ThumbnailManager$BitmapGetterTask;->path:Ljava/lang/String;

    # invokes: Lcom/samsung/android/app/FileShareServer/ThumbnailManager;->getThumbnailBitmap(Ljava/lang/String;)Landroid/graphics/Bitmap;
    invoke-static {v0, v1}, Lcom/samsung/android/app/FileShareServer/ThumbnailManager;->access$000(Lcom/samsung/android/app/FileShareServer/ThumbnailManager;Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 106
    check-cast p1, [Ljava/lang/String;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/samsung/android/app/FileShareServer/ThumbnailManager$BitmapGetterTask;->doInBackground([Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Landroid/graphics/Bitmap;)V
    .locals 4
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 123
    invoke-virtual {p0}, Lcom/samsung/android/app/FileShareServer/ThumbnailManager$BitmapGetterTask;->isCancelled()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 124
    const/4 p1, 0x0

    .line 127
    :cond_0
    iget-object v2, p0, Lcom/samsung/android/app/FileShareServer/ThumbnailManager$BitmapGetterTask;->this$0:Lcom/samsung/android/app/FileShareServer/ThumbnailManager;

    # getter for: Lcom/samsung/android/app/FileShareServer/ThumbnailManager;->mThumbnailBuffer:Lcom/samsung/android/app/FileShareServer/ThumbnailBuffer;
    invoke-static {v2}, Lcom/samsung/android/app/FileShareServer/ThumbnailManager;->access$100(Lcom/samsung/android/app/FileShareServer/ThumbnailManager;)Lcom/samsung/android/app/FileShareServer/ThumbnailBuffer;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/app/FileShareServer/ThumbnailManager$BitmapGetterTask;->path:Ljava/lang/String;

    invoke-virtual {v2, v3, p1}, Lcom/samsung/android/app/FileShareServer/ThumbnailBuffer;->put(Ljava/lang/String;Landroid/graphics/Bitmap;)Ljava/lang/Object;

    .line 129
    iget-object v2, p0, Lcom/samsung/android/app/FileShareServer/ThumbnailManager$BitmapGetterTask;->imageViewReference:Ljava/lang/ref/WeakReference;

    if-eqz v2, :cond_1

    .line 130
    iget-object v2, p0, Lcom/samsung/android/app/FileShareServer/ThumbnailManager$BitmapGetterTask;->imageViewReference:Ljava/lang/ref/WeakReference;

    invoke-virtual {v2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 131
    .local v1, "imageView":Landroid/widget/ImageView;
    # invokes: Lcom/samsung/android/app/FileShareServer/ThumbnailManager;->getBitmapGetterTask(Landroid/widget/ImageView;)Lcom/samsung/android/app/FileShareServer/ThumbnailManager$BitmapGetterTask;
    invoke-static {v1}, Lcom/samsung/android/app/FileShareServer/ThumbnailManager;->access$200(Landroid/widget/ImageView;)Lcom/samsung/android/app/FileShareServer/ThumbnailManager$BitmapGetterTask;

    move-result-object v0

    .line 132
    .local v0, "bitmapDownloaderTask":Lcom/samsung/android/app/FileShareServer/ThumbnailManager$BitmapGetterTask;
    if-ne p0, v0, :cond_1

    .line 133
    invoke-virtual {v1, p1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 136
    .end local v0    # "bitmapDownloaderTask":Lcom/samsung/android/app/FileShareServer/ThumbnailManager$BitmapGetterTask;
    .end local v1    # "imageView":Landroid/widget/ImageView;
    :cond_1
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 106
    check-cast p1, Landroid/graphics/Bitmap;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/samsung/android/app/FileShareServer/ThumbnailManager$BitmapGetterTask;->onPostExecute(Landroid/graphics/Bitmap;)V

    return-void
.end method
