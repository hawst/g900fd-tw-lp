.class public Lcom/samsung/android/app/FileShareServer/ServerBroadcastReceiver;
.super Landroid/content/BroadcastReceiver;
.source "ServerBroadcastReceiver.java"


# static fields
.field private static final TAGClass:Ljava/lang/String; = "ServerBroadcastReceiver"


# instance fields
.field private mCurrentUser:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 11
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 15
    invoke-static {}, Landroid/app/ActivityManager;->getCurrentUser()I

    move-result v0

    iput v0, p0, Lcom/samsung/android/app/FileShareServer/ServerBroadcastReceiver;->mCurrentUser:I

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 13
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 20
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 21
    .local v0, "action":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/content/Intent;->getDataString()Ljava/lang/String;

    move-result-object v1

    .line 22
    .local v1, "data":Ljava/lang/String;
    const-string v9, "ServerBroadcastReceiver"

    const-string v10, "onReceive"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "action "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " // "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v9, v10, v11}, Lcom/samsung/android/app/FileShareServer/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 24
    const-string v9, "android.net.wifi.p2p.CONNECTION_STATE_CHANGE"

    invoke-virtual {v9, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_1

    .line 29
    invoke-static {p1}, Lcom/samsung/android/app/FileShareServer/Utils;->isWifiP2pConnected(Landroid/content/Context;)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 30
    const-string v9, "ServerBroadcastReceiver"

    const-string v10, "onReceive"

    const-string v11, "isWifiP2pConnected : true"

    invoke-static {v9, v10, v11}, Lcom/samsung/android/app/FileShareServer/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 31
    new-instance v6, Landroid/content/Intent;

    invoke-direct {v6}, Landroid/content/Intent;-><init>()V

    .line 32
    .local v6, "it":Landroid/content/Intent;
    const-class v9, Lcom/samsung/android/app/FileShareServer/ServerService;

    invoke-virtual {v6, p1, v9}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 33
    const-string v9, "com.samsung.android.app.FileShareServer.SERVICE_START"

    invoke-virtual {v6, v9}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 34
    const-string v9, "CurrentUser"

    iget v10, p0, Lcom/samsung/android/app/FileShareServer/ServerBroadcastReceiver;->mCurrentUser:I

    invoke-virtual {v6, v9, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 35
    invoke-virtual {p1, v6}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 75
    .end local v6    # "it":Landroid/content/Intent;
    :cond_0
    :goto_0
    return-void

    .line 37
    :cond_1
    const-string v9, "android.intent.action.BOOT_COMPLETED"

    invoke-virtual {v0, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    .line 38
    new-instance v2, Lcom/samsung/android/app/FileShareServer/DbAdapter;

    invoke-direct {v2, p1}, Lcom/samsung/android/app/FileShareServer/DbAdapter;-><init>(Landroid/content/Context;)V

    .line 40
    .local v2, "dbAdapter":Lcom/samsung/android/app/FileShareServer/DbAdapter;
    :try_start_0
    invoke-virtual {v2}, Lcom/samsung/android/app/FileShareServer/DbAdapter;->open()Lcom/samsung/android/app/FileShareServer/DbAdapter;

    .line 41
    invoke-virtual {v2}, Lcom/samsung/android/app/FileShareServer/DbAdapter;->clearDbTable()V

    .line 42
    invoke-virtual {v2}, Lcom/samsung/android/app/FileShareServer/DbAdapter;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 50
    const-string v9, "inboundIdPref"

    const/4 v10, 0x0

    invoke-virtual {p1, v9, v10}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v7

    .line 52
    .local v7, "prefs1":Landroid/content/SharedPreferences;
    invoke-interface {v7}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v4

    .line 53
    .local v4, "ed1":Landroid/content/SharedPreferences$Editor;
    const-string v9, "InboundId"

    const/16 v10, 0x3e8

    invoke-interface {v4, v9, v10}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 54
    invoke-interface {v4}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 56
    const-string v9, "notiCntPref"

    const/4 v10, 0x0

    invoke-virtual {p1, v9, v10}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v8

    .line 58
    .local v8, "prefs2":Landroid/content/SharedPreferences;
    invoke-interface {v8}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v5

    .line 59
    .local v5, "ed2":Landroid/content/SharedPreferences$Editor;
    const-string v9, "notiCnt"

    const/4 v10, 0x0

    invoke-interface {v5, v9, v10}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 60
    invoke-interface {v5}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto :goto_0

    .line 44
    .end local v4    # "ed1":Landroid/content/SharedPreferences$Editor;
    .end local v5    # "ed2":Landroid/content/SharedPreferences$Editor;
    .end local v7    # "prefs1":Landroid/content/SharedPreferences;
    .end local v8    # "prefs2":Landroid/content/SharedPreferences;
    :catch_0
    move-exception v3

    .line 46
    .local v3, "e":Ljava/lang/Exception;
    :try_start_1
    const-string v9, "ServerBroadcastReceiver"

    const-string v10, "onReceive"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "exception "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v9, v10, v11}, Lcom/samsung/android/app/FileShareServer/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 50
    const-string v9, "inboundIdPref"

    const/4 v10, 0x0

    invoke-virtual {p1, v9, v10}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v7

    .line 52
    .restart local v7    # "prefs1":Landroid/content/SharedPreferences;
    invoke-interface {v7}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v4

    .line 53
    .restart local v4    # "ed1":Landroid/content/SharedPreferences$Editor;
    const-string v9, "InboundId"

    const/16 v10, 0x3e8

    invoke-interface {v4, v9, v10}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 54
    invoke-interface {v4}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 56
    const-string v9, "notiCntPref"

    const/4 v10, 0x0

    invoke-virtual {p1, v9, v10}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v8

    .line 58
    .restart local v8    # "prefs2":Landroid/content/SharedPreferences;
    invoke-interface {v8}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v5

    .line 59
    .restart local v5    # "ed2":Landroid/content/SharedPreferences$Editor;
    const-string v9, "notiCnt"

    const/4 v10, 0x0

    invoke-interface {v5, v9, v10}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 60
    invoke-interface {v5}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto/16 :goto_0

    .line 50
    .end local v3    # "e":Ljava/lang/Exception;
    .end local v4    # "ed1":Landroid/content/SharedPreferences$Editor;
    .end local v5    # "ed2":Landroid/content/SharedPreferences$Editor;
    .end local v7    # "prefs1":Landroid/content/SharedPreferences;
    .end local v8    # "prefs2":Landroid/content/SharedPreferences;
    :catchall_0
    move-exception v9

    const-string v10, "inboundIdPref"

    const/4 v11, 0x0

    invoke-virtual {p1, v10, v11}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v7

    .line 52
    .restart local v7    # "prefs1":Landroid/content/SharedPreferences;
    invoke-interface {v7}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v4

    .line 53
    .restart local v4    # "ed1":Landroid/content/SharedPreferences$Editor;
    const-string v10, "InboundId"

    const/16 v11, 0x3e8

    invoke-interface {v4, v10, v11}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 54
    invoke-interface {v4}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 56
    const-string v10, "notiCntPref"

    const/4 v11, 0x0

    invoke-virtual {p1, v10, v11}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v8

    .line 58
    .restart local v8    # "prefs2":Landroid/content/SharedPreferences;
    invoke-interface {v8}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v5

    .line 59
    .restart local v5    # "ed2":Landroid/content/SharedPreferences$Editor;
    const-string v10, "notiCnt"

    const/4 v11, 0x0

    invoke-interface {v5, v10, v11}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 60
    invoke-interface {v5}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 61
    throw v9

    .line 62
    .end local v2    # "dbAdapter":Lcom/samsung/android/app/FileShareServer/DbAdapter;
    .end local v4    # "ed1":Landroid/content/SharedPreferences$Editor;
    .end local v5    # "ed2":Landroid/content/SharedPreferences$Editor;
    .end local v7    # "prefs1":Landroid/content/SharedPreferences;
    .end local v8    # "prefs2":Landroid/content/SharedPreferences;
    :cond_2
    const-string v9, "android.intent.action.PACKAGE_CHANGED"

    invoke-virtual {v0, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 63
    const-string v9, "package:com.samsung.android.app.FileShareServer"

    invoke-virtual {v1, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 65
    invoke-static {p1}, Lcom/samsung/android/app/FileShareServer/Utils;->isWifiP2pConnected(Landroid/content/Context;)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 66
    const-string v9, "ServerBroadcastReceiver"

    const-string v10, "onReceive"

    const-string v11, "isWifiP2pConnected : true"

    invoke-static {v9, v10, v11}, Lcom/samsung/android/app/FileShareServer/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 67
    new-instance v6, Landroid/content/Intent;

    invoke-direct {v6}, Landroid/content/Intent;-><init>()V

    .line 68
    .restart local v6    # "it":Landroid/content/Intent;
    const-class v9, Lcom/samsung/android/app/FileShareServer/ServerService;

    invoke-virtual {v6, p1, v9}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 69
    const-string v9, "com.samsung.android.app.FileShareServer.SERVICE_START"

    invoke-virtual {v6, v9}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 70
    const-string v9, "CurrentUser"

    iget v10, p0, Lcom/samsung/android/app/FileShareServer/ServerBroadcastReceiver;->mCurrentUser:I

    invoke-virtual {v6, v9, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 71
    invoke-virtual {p1, v6}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    goto/16 :goto_0
.end method
