.class public Lcom/samsung/android/app/FileShareServer/InboundListAdaptor;
.super Landroid/widget/BaseAdapter;
.source "InboundListAdaptor.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/app/FileShareServer/InboundListAdaptor$ViewHolder;
    }
.end annotation


# static fields
.field private static final TAGClass:Ljava/lang/String; = "InboundListAdaptor"


# instance fields
.field private mContext:Landroid/content/Context;

.field private mInboundInfo:Lcom/samsung/android/app/FileShareServer/InboundInfo;

.field private mInflator:Landroid/view/LayoutInflater;

.field private mItemList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation
.end field

.field private mThumbnailer:Lcom/samsung/android/app/FileShareServer/ThumbnailManager;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 32
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 34
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/FileShareServer/InboundListAdaptor;->mInflator:Landroid/view/LayoutInflater;

    .line 35
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/app/FileShareServer/InboundListAdaptor;->mItemList:Ljava/util/ArrayList;

    .line 36
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/samsung/android/app/FileShareServer/InboundInfo;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "inboundInfo"    # Lcom/samsung/android/app/FileShareServer/InboundInfo;

    .prologue
    .line 39
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 41
    iput-object p1, p0, Lcom/samsung/android/app/FileShareServer/InboundListAdaptor;->mContext:Landroid/content/Context;

    .line 42
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/android/app/FileShareServer/InboundListAdaptor;->mInflator:Landroid/view/LayoutInflater;

    .line 43
    iput-object p2, p0, Lcom/samsung/android/app/FileShareServer/InboundListAdaptor;->mInboundInfo:Lcom/samsung/android/app/FileShareServer/InboundInfo;

    .line 44
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/samsung/android/app/FileShareServer/InboundListAdaptor;->mItemList:Ljava/util/ArrayList;

    .line 45
    iget-object v2, p0, Lcom/samsung/android/app/FileShareServer/InboundListAdaptor;->mInboundInfo:Lcom/samsung/android/app/FileShareServer/InboundInfo;

    invoke-virtual {v2}, Lcom/samsung/android/app/FileShareServer/InboundInfo;->getFileList()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/File;

    .line 46
    .local v0, "file":Ljava/io/File;
    iget-object v2, p0, Lcom/samsung/android/app/FileShareServer/InboundListAdaptor;->mItemList:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 48
    .end local v0    # "file":Ljava/io/File;
    :cond_0
    new-instance v2, Lcom/samsung/android/app/FileShareServer/ThumbnailManager;

    iget-object v3, p0, Lcom/samsung/android/app/FileShareServer/InboundListAdaptor;->mContext:Landroid/content/Context;

    invoke-direct {v2, v3}, Lcom/samsung/android/app/FileShareServer/ThumbnailManager;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/samsung/android/app/FileShareServer/InboundListAdaptor;->mThumbnailer:Lcom/samsung/android/app/FileShareServer/ThumbnailManager;

    .line 49
    return-void
.end method

.method private getConvertSize(J)Ljava/lang/String;
    .locals 5
    .param p1, "size"    # J

    .prologue
    .line 126
    const-string v0, ""

    .line 128
    .local v0, "retSize":Ljava/lang/String;
    const-wide/16 v2, 0x0

    cmp-long v1, p1, v2

    if-gez v1, :cond_0

    .line 129
    const-string v0, "Wrong size"

    .line 133
    :goto_0
    const-string v1, "InboundListAdaptor"

    const-string v2, "getConvertSize"

    invoke-static {v1, v2, v0}, Lcom/samsung/android/app/FileShareServer/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 134
    return-object v0

    .line 131
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/app/FileShareServer/InboundListAdaptor;->mContext:Landroid/content/Context;

    invoke-static {v1, p1, p2}, Landroid/text/format/Formatter;->formatFileSize(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public addItem(Ljava/io/File;)V
    .locals 1
    .param p1, "file"    # Ljava/io/File;

    .prologue
    .line 75
    iget-object v0, p0, Lcom/samsung/android/app/FileShareServer/InboundListAdaptor;->mItemList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 76
    return-void
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/samsung/android/app/FileShareServer/InboundListAdaptor;->mItemList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1, "arg0"    # I

    .prologue
    .line 65
    const/4 v0, 0x0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 71
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 7
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const/16 v6, 0x8

    const/4 v5, 0x0

    .line 82
    iget-object v3, p0, Lcom/samsung/android/app/FileShareServer/InboundListAdaptor;->mItemList:Ljava/util/ArrayList;

    invoke-virtual {v3, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/File;

    .line 83
    .local v0, "file":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    .line 85
    .local v2, "path":Ljava/lang/String;
    if-nez p2, :cond_3

    .line 86
    new-instance v1, Lcom/samsung/android/app/FileShareServer/InboundListAdaptor$ViewHolder;

    invoke-direct {v1}, Lcom/samsung/android/app/FileShareServer/InboundListAdaptor$ViewHolder;-><init>()V

    .line 87
    .local v1, "holder":Lcom/samsung/android/app/FileShareServer/InboundListAdaptor$ViewHolder;
    iget-object v3, p0, Lcom/samsung/android/app/FileShareServer/InboundListAdaptor;->mInflator:Landroid/view/LayoutInflater;

    const v4, 0x7f030004

    invoke-virtual {v3, v4, p3, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 88
    const v3, 0x7f080016

    invoke-virtual {p2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iput-object v3, v1, Lcom/samsung/android/app/FileShareServer/InboundListAdaptor$ViewHolder;->thumbnailView:Landroid/widget/ImageView;

    .line 89
    const v3, 0x7f080018

    invoke-virtual {p2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iput-object v3, v1, Lcom/samsung/android/app/FileShareServer/InboundListAdaptor$ViewHolder;->videoPlayThumbnailView:Landroid/widget/ImageView;

    .line 91
    const v3, 0x7f080017

    invoke-virtual {p2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iput-object v3, v1, Lcom/samsung/android/app/FileShareServer/InboundListAdaptor$ViewHolder;->imageBorderView:Landroid/widget/ImageView;

    .line 92
    const v3, 0x7f08000c

    invoke-virtual {p2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, v1, Lcom/samsung/android/app/FileShareServer/InboundListAdaptor$ViewHolder;->fileName:Landroid/widget/TextView;

    .line 93
    const v3, 0x7f080019

    invoke-virtual {p2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, v1, Lcom/samsung/android/app/FileShareServer/InboundListAdaptor$ViewHolder;->fileInfo:Landroid/widget/TextView;

    .line 94
    const v3, 0x7f08001a

    invoke-virtual {p2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, v1, Lcom/samsung/android/app/FileShareServer/InboundListAdaptor$ViewHolder;->fileSize:Landroid/widget/TextView;

    .line 95
    invoke-virtual {p2, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 100
    :goto_0
    if-eqz p2, :cond_2

    .line 101
    iget-object v3, v1, Lcom/samsung/android/app/FileShareServer/InboundListAdaptor$ViewHolder;->imageBorderView:Landroid/widget/ImageView;

    invoke-virtual {v3, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 102
    iget-object v3, v1, Lcom/samsung/android/app/FileShareServer/InboundListAdaptor$ViewHolder;->videoPlayThumbnailView:Landroid/widget/ImageView;

    invoke-virtual {v3, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 104
    iget-object v3, p0, Lcom/samsung/android/app/FileShareServer/InboundListAdaptor;->mThumbnailer:Lcom/samsung/android/app/FileShareServer/ThumbnailManager;

    iget-object v4, v1, Lcom/samsung/android/app/FileShareServer/InboundListAdaptor$ViewHolder;->thumbnailView:Landroid/widget/ImageView;

    invoke-virtual {v3, v2, v4}, Lcom/samsung/android/app/FileShareServer/ThumbnailManager;->getBitmap(Ljava/lang/String;Landroid/widget/ImageView;)V

    .line 105
    iget-object v3, p0, Lcom/samsung/android/app/FileShareServer/InboundListAdaptor;->mThumbnailer:Lcom/samsung/android/app/FileShareServer/ThumbnailManager;

    invoke-virtual {v3, v2}, Lcom/samsung/android/app/FileShareServer/ThumbnailManager;->isBroken(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 106
    iget-object v3, v1, Lcom/samsung/android/app/FileShareServer/InboundListAdaptor$ViewHolder;->imageBorderView:Landroid/widget/ImageView;

    invoke-virtual {v3, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 109
    :cond_0
    invoke-static {v2}, Lcom/samsung/android/app/FileShareServer/FileTypeManager;->isVideoFileType(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 110
    iget-object v3, v1, Lcom/samsung/android/app/FileShareServer/InboundListAdaptor$ViewHolder;->videoPlayThumbnailView:Landroid/widget/ImageView;

    invoke-virtual {v3, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 112
    :cond_1
    iget-object v3, v1, Lcom/samsung/android/app/FileShareServer/InboundListAdaptor$ViewHolder;->fileName:Landroid/widget/TextView;

    invoke-virtual {v0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 113
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v3

    if-nez v3, :cond_4

    .line 114
    iget-object v3, v1, Lcom/samsung/android/app/FileShareServer/InboundListAdaptor$ViewHolder;->fileInfo:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/samsung/android/app/FileShareServer/InboundListAdaptor;->mContext:Landroid/content/Context;

    const v5, 0x7f060006

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 115
    iget-object v3, v1, Lcom/samsung/android/app/FileShareServer/InboundListAdaptor$ViewHolder;->fileSize:Landroid/widget/TextView;

    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v4

    invoke-direct {p0, v4, v5}, Lcom/samsung/android/app/FileShareServer/InboundListAdaptor;->getConvertSize(J)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 122
    :cond_2
    :goto_1
    return-object p2

    .line 97
    .end local v1    # "holder":Lcom/samsung/android/app/FileShareServer/InboundListAdaptor$ViewHolder;
    :cond_3
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/app/FileShareServer/InboundListAdaptor$ViewHolder;

    .restart local v1    # "holder":Lcom/samsung/android/app/FileShareServer/InboundListAdaptor$ViewHolder;
    goto :goto_0

    .line 117
    :cond_4
    iget-object v3, v1, Lcom/samsung/android/app/FileShareServer/InboundListAdaptor$ViewHolder;->fileInfo:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/samsung/android/app/FileShareServer/InboundListAdaptor;->mContext:Landroid/content/Context;

    const v5, 0x7f060005

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 118
    iget-object v3, v1, Lcom/samsung/android/app/FileShareServer/InboundListAdaptor$ViewHolder;->fileSize:Landroid/widget/TextView;

    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v4

    invoke-direct {p0, v4, v5}, Lcom/samsung/android/app/FileShareServer/InboundListAdaptor;->getConvertSize(J)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method
