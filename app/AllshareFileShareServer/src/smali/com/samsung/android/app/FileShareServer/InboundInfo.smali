.class public Lcom/samsung/android/app/FileShareServer/InboundInfo;
.super Ljava/lang/Object;
.source "InboundInfo.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/app/FileShareServer/InboundInfo$IInboundInfoListener;
    }
.end annotation


# static fields
.field public static final NEED_TO_REMOVE:I = 0x2

.field public static final NOT_USED:I = 0x0

.field public static final RECEIVE_PROGRESS:Ljava/lang/String; = "com.samsung.android.app.FileShareServer.RECEIVE_PROGRESS"

.field public static final RECEIVE_RESULT:Ljava/lang/String; = "com.samsung.android.app.FileShareServer.RECEIVE_RESULT"

.field public static final REMOVE_INBOUND_INFO:Ljava/lang/String; = "com.samsung.android.app.FileShareServer.REMOVE_INBOUND_INFO"

.field public static final STATUS_CANCELED:I = 0x3eb

.field public static final STATUS_COMPLETED:I = 0x3ec

.field public static final STATUS_ERROR:I = 0x3ee

.field public static final STATUS_FAILED:I = 0x3e9

.field public static final STATUS_NONE:I = 0x3e8

.field public static final STATUS_PROGRESS:I = 0x3ea

.field public static final STATUS_TRANSPORTED:I = 0x3ed

.field private static final TAGClass:Ljava/lang/String; = "InboundInfo : "

.field public static final USED:I = 0x1


# instance fields
.field private mContext:Landroid/content/Context;

.field private mCurrentCount:I

.field private mCurrentIndex:I

.field private mCurrentSize:J

.field private mCurrentStatus:I

.field private mDownloadedSize:J

.field private mFileList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation
.end field

.field private mFileName:Ljava/lang/String;

.field private mInboundID:I

.field private mListener:Lcom/samsung/android/app/FileShareServer/InboundInfo$IInboundInfoListener;

.field private mSenderName:Ljava/lang/String;

.field private mSessionID:Ljava/lang/String;

.field private mTotalCount:I

.field private mTotalFailedCount:I

.field private mTotalSavedCount:I

.field private mTotalSize:J

.field private mTotalSuccessCount:I

.field private mTransportSize:J


# direct methods
.method public constructor <init>(I)V
    .locals 1
    .param p1, "id"    # I

    .prologue
    .line 87
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 88
    invoke-virtual {p0, p1}, Lcom/samsung/android/app/FileShareServer/InboundInfo;->setInboundID(I)V

    .line 89
    const/16 v0, 0x3e8

    iput v0, p0, Lcom/samsung/android/app/FileShareServer/InboundInfo;->mCurrentStatus:I

    .line 90
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/app/FileShareServer/InboundInfo;->mFileList:Ljava/util/ArrayList;

    .line 91
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 77
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 78
    iput-object p1, p0, Lcom/samsung/android/app/FileShareServer/InboundInfo;->mContext:Landroid/content/Context;

    .line 79
    invoke-direct {p0}, Lcom/samsung/android/app/FileShareServer/InboundInfo;->getIdPreference()I

    move-result v0

    .line 80
    .local v0, "id":I
    add-int/lit8 v1, v0, 0x1

    .end local v0    # "id":I
    .local v1, "id":I
    invoke-virtual {p0, v0}, Lcom/samsung/android/app/FileShareServer/InboundInfo;->setInboundID(I)V

    .line 81
    invoke-direct {p0, v1}, Lcom/samsung/android/app/FileShareServer/InboundInfo;->setIdPreference(I)V

    .line 82
    const/16 v2, 0x3e8

    iput v2, p0, Lcom/samsung/android/app/FileShareServer/InboundInfo;->mCurrentStatus:I

    .line 83
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/samsung/android/app/FileShareServer/InboundInfo;->mFileList:Ljava/util/ArrayList;

    .line 84
    return-void
.end method

.method private getIdPreference()I
    .locals 5

    .prologue
    .line 268
    iget-object v2, p0, Lcom/samsung/android/app/FileShareServer/InboundInfo;->mContext:Landroid/content/Context;

    const-string v3, "inboundIdPref"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 270
    .local v1, "prefs":Landroid/content/SharedPreferences;
    const/4 v0, 0x0

    .line 271
    .local v0, "inboundId":I
    const-string v2, "InboundId"

    const/16 v3, 0x3e8

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 274
    return v0
.end method

.method private setIdPreference(I)V
    .locals 5
    .param p1, "id"    # I

    .prologue
    .line 260
    iget-object v2, p0, Lcom/samsung/android/app/FileShareServer/InboundInfo;->mContext:Landroid/content/Context;

    const-string v3, "inboundIdPref"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 262
    .local v1, "prefs":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 263
    .local v0, "ed":Landroid/content/SharedPreferences$Editor;
    const-string v2, "InboundId"

    invoke-interface {v0, v2, p1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 264
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 265
    return-void
.end method


# virtual methods
.method public addFile(Ljava/io/File;)V
    .locals 4
    .param p1, "file"    # Ljava/io/File;

    .prologue
    .line 223
    iget-object v0, p0, Lcom/samsung/android/app/FileShareServer/InboundInfo;->mFileList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 224
    const-string v0, "InboundInfo : "

    const-string v1, "addFile"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "(ID - "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/samsung/android/app/FileShareServer/InboundInfo;->mInboundID:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/app/FileShareServer/InboundInfo;->mFileList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", Name["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/samsung/android/app/FileShareServer/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 226
    invoke-virtual {p0}, Lcom/samsung/android/app/FileShareServer/InboundInfo;->updateFileList()V

    .line 227
    return-void
.end method

.method public getCurrentCount()I
    .locals 1

    .prologue
    .line 134
    iget v0, p0, Lcom/samsung/android/app/FileShareServer/InboundInfo;->mCurrentCount:I

    return v0
.end method

.method public getCurrentIndex()I
    .locals 1

    .prologue
    .line 199
    iget v0, p0, Lcom/samsung/android/app/FileShareServer/InboundInfo;->mCurrentIndex:I

    return v0
.end method

.method public getCurrentSize()J
    .locals 4

    .prologue
    .line 190
    invoke-virtual {p0}, Lcom/samsung/android/app/FileShareServer/InboundInfo;->getTransportSize()J

    move-result-wide v0

    invoke-virtual {p0}, Lcom/samsung/android/app/FileShareServer/InboundInfo;->getDownloadedSize()J

    move-result-wide v2

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/samsung/android/app/FileShareServer/InboundInfo;->mCurrentSize:J

    .line 191
    iget-wide v0, p0, Lcom/samsung/android/app/FileShareServer/InboundInfo;->mCurrentSize:J

    return-wide v0
.end method

.method public getCurrentStatus()I
    .locals 1

    .prologue
    .line 207
    iget v0, p0, Lcom/samsung/android/app/FileShareServer/InboundInfo;->mCurrentStatus:I

    return v0
.end method

.method public getDownloadedSize()J
    .locals 2

    .prologue
    .line 174
    iget-wide v0, p0, Lcom/samsung/android/app/FileShareServer/InboundInfo;->mDownloadedSize:J

    return-wide v0
.end method

.method public getFileList()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation

    .prologue
    .line 215
    iget-object v0, p0, Lcom/samsung/android/app/FileShareServer/InboundInfo;->mFileList:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getFileName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 110
    iget-object v0, p0, Lcom/samsung/android/app/FileShareServer/InboundInfo;->mFileName:Ljava/lang/String;

    return-object v0
.end method

.method public getInboundID()I
    .locals 1

    .prologue
    .line 94
    iget v0, p0, Lcom/samsung/android/app/FileShareServer/InboundInfo;->mInboundID:I

    return v0
.end method

.method public getSenderName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 118
    iget-object v0, p0, Lcom/samsung/android/app/FileShareServer/InboundInfo;->mSenderName:Ljava/lang/String;

    return-object v0
.end method

.method public getSessionID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 102
    iget-object v0, p0, Lcom/samsung/android/app/FileShareServer/InboundInfo;->mSessionID:Ljava/lang/String;

    return-object v0
.end method

.method public getTotalCount()I
    .locals 1

    .prologue
    .line 126
    iget v0, p0, Lcom/samsung/android/app/FileShareServer/InboundInfo;->mTotalCount:I

    return v0
.end method

.method public getTotalFailedCount()I
    .locals 1

    .prologue
    .line 150
    iget v0, p0, Lcom/samsung/android/app/FileShareServer/InboundInfo;->mTotalFailedCount:I

    return v0
.end method

.method public getTotalSavedCount()I
    .locals 1

    .prologue
    .line 158
    iget v0, p0, Lcom/samsung/android/app/FileShareServer/InboundInfo;->mTotalSavedCount:I

    return v0
.end method

.method public getTotalSize()J
    .locals 2

    .prologue
    .line 166
    iget-wide v0, p0, Lcom/samsung/android/app/FileShareServer/InboundInfo;->mTotalSize:J

    return-wide v0
.end method

.method public getTotalSuccessCount()I
    .locals 1

    .prologue
    .line 142
    iget v0, p0, Lcom/samsung/android/app/FileShareServer/InboundInfo;->mTotalSuccessCount:I

    return v0
.end method

.method public getTransportSize()J
    .locals 2

    .prologue
    .line 182
    iget-wide v0, p0, Lcom/samsung/android/app/FileShareServer/InboundInfo;->mTransportSize:J

    return-wide v0
.end method

.method public isListener()Z
    .locals 1

    .prologue
    .line 234
    iget-object v0, p0, Lcom/samsung/android/app/FileShareServer/InboundInfo;->mListener:Lcom/samsung/android/app/FileShareServer/InboundInfo$IInboundInfoListener;

    if-eqz v0, :cond_0

    .line 235
    const/4 v0, 0x1

    .line 238
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setCurrentCount(I)V
    .locals 0
    .param p1, "currentCount"    # I

    .prologue
    .line 138
    iput p1, p0, Lcom/samsung/android/app/FileShareServer/InboundInfo;->mCurrentCount:I

    .line 139
    return-void
.end method

.method public setCurrentIndex(I)V
    .locals 0
    .param p1, "currentIndex"    # I

    .prologue
    .line 203
    iput p1, p0, Lcom/samsung/android/app/FileShareServer/InboundInfo;->mCurrentIndex:I

    .line 204
    return-void
.end method

.method public setCurrentSize(J)V
    .locals 1
    .param p1, "currentSize"    # J

    .prologue
    .line 195
    iput-wide p1, p0, Lcom/samsung/android/app/FileShareServer/InboundInfo;->mCurrentSize:J

    .line 196
    return-void
.end method

.method public setCurrentStatus(I)V
    .locals 0
    .param p1, "currentStatus"    # I

    .prologue
    .line 211
    iput p1, p0, Lcom/samsung/android/app/FileShareServer/InboundInfo;->mCurrentStatus:I

    .line 212
    return-void
.end method

.method public setDownloadedSize(J)V
    .locals 1
    .param p1, "downloadedSize"    # J

    .prologue
    .line 178
    iput-wide p1, p0, Lcom/samsung/android/app/FileShareServer/InboundInfo;->mDownloadedSize:J

    .line 179
    return-void
.end method

.method public setFileList(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/io/File;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 219
    .local p1, "fileList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/io/File;>;"
    iput-object p1, p0, Lcom/samsung/android/app/FileShareServer/InboundInfo;->mFileList:Ljava/util/ArrayList;

    .line 220
    return-void
.end method

.method public setFileName(Ljava/lang/String;)V
    .locals 0
    .param p1, "fileName"    # Ljava/lang/String;

    .prologue
    .line 114
    iput-object p1, p0, Lcom/samsung/android/app/FileShareServer/InboundInfo;->mFileName:Ljava/lang/String;

    .line 115
    return-void
.end method

.method public setInboundID(I)V
    .locals 0
    .param p1, "ID"    # I

    .prologue
    .line 98
    iput p1, p0, Lcom/samsung/android/app/FileShareServer/InboundInfo;->mInboundID:I

    .line 99
    return-void
.end method

.method public setListener(Lcom/samsung/android/app/FileShareServer/InboundInfo$IInboundInfoListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/samsung/android/app/FileShareServer/InboundInfo$IInboundInfoListener;

    .prologue
    .line 230
    iput-object p1, p0, Lcom/samsung/android/app/FileShareServer/InboundInfo;->mListener:Lcom/samsung/android/app/FileShareServer/InboundInfo$IInboundInfoListener;

    .line 231
    return-void
.end method

.method public setSenderName(Ljava/lang/String;)V
    .locals 0
    .param p1, "senderName"    # Ljava/lang/String;

    .prologue
    .line 122
    iput-object p1, p0, Lcom/samsung/android/app/FileShareServer/InboundInfo;->mSenderName:Ljava/lang/String;

    .line 123
    return-void
.end method

.method public setSessionID(Ljava/lang/String;)V
    .locals 0
    .param p1, "sessionID"    # Ljava/lang/String;

    .prologue
    .line 106
    iput-object p1, p0, Lcom/samsung/android/app/FileShareServer/InboundInfo;->mSessionID:Ljava/lang/String;

    .line 107
    return-void
.end method

.method public setTotalCount(I)V
    .locals 0
    .param p1, "totalCount"    # I

    .prologue
    .line 130
    iput p1, p0, Lcom/samsung/android/app/FileShareServer/InboundInfo;->mTotalCount:I

    .line 131
    return-void
.end method

.method public setTotalFailedCount(I)V
    .locals 0
    .param p1, "TotalFailedCount"    # I

    .prologue
    .line 154
    iput p1, p0, Lcom/samsung/android/app/FileShareServer/InboundInfo;->mTotalFailedCount:I

    .line 155
    return-void
.end method

.method public setTotalSavedCount(I)V
    .locals 0
    .param p1, "TotalSavedCount"    # I

    .prologue
    .line 162
    iput p1, p0, Lcom/samsung/android/app/FileShareServer/InboundInfo;->mTotalSavedCount:I

    .line 163
    return-void
.end method

.method public setTotalSize(J)V
    .locals 1
    .param p1, "totalSize"    # J

    .prologue
    .line 170
    iput-wide p1, p0, Lcom/samsung/android/app/FileShareServer/InboundInfo;->mTotalSize:J

    .line 171
    return-void
.end method

.method public setTotalSuccessCount(I)V
    .locals 0
    .param p1, "TotalSuccessCount"    # I

    .prologue
    .line 146
    iput p1, p0, Lcom/samsung/android/app/FileShareServer/InboundInfo;->mTotalSuccessCount:I

    .line 147
    return-void
.end method

.method public setTransportSize(J)V
    .locals 1
    .param p1, "transportSize"    # J

    .prologue
    .line 186
    iput-wide p1, p0, Lcom/samsung/android/app/FileShareServer/InboundInfo;->mTransportSize:J

    .line 187
    return-void
.end method

.method public update()V
    .locals 1

    .prologue
    .line 242
    iget-object v0, p0, Lcom/samsung/android/app/FileShareServer/InboundInfo;->mListener:Lcom/samsung/android/app/FileShareServer/InboundInfo$IInboundInfoListener;

    if-eqz v0, :cond_0

    .line 243
    iget-object v0, p0, Lcom/samsung/android/app/FileShareServer/InboundInfo;->mListener:Lcom/samsung/android/app/FileShareServer/InboundInfo$IInboundInfoListener;

    invoke-interface {v0, p0}, Lcom/samsung/android/app/FileShareServer/InboundInfo$IInboundInfoListener;->updateProgress(Lcom/samsung/android/app/FileShareServer/InboundInfo;)V

    .line 245
    :cond_0
    return-void
.end method

.method public updateFileList()V
    .locals 1

    .prologue
    .line 248
    iget-object v0, p0, Lcom/samsung/android/app/FileShareServer/InboundInfo;->mListener:Lcom/samsung/android/app/FileShareServer/InboundInfo$IInboundInfoListener;

    if-eqz v0, :cond_0

    .line 249
    iget-object v0, p0, Lcom/samsung/android/app/FileShareServer/InboundInfo;->mListener:Lcom/samsung/android/app/FileShareServer/InboundInfo$IInboundInfoListener;

    invoke-interface {v0}, Lcom/samsung/android/app/FileShareServer/InboundInfo$IInboundInfoListener;->updateFileList()V

    .line 251
    :cond_0
    return-void
.end method
