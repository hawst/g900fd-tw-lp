.class Lcom/samsung/android/app/FileShareServer/InboundFragment$3;
.super Ljava/lang/Object;
.source "InboundFragment.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/FileShareServer/InboundFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/FileShareServer/InboundFragment;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/FileShareServer/InboundFragment;)V
    .locals 0

    .prologue
    .line 398
    iput-object p1, p0, Lcom/samsung/android/app/FileShareServer/InboundFragment$3;->this$0:Lcom/samsung/android/app/FileShareServer/InboundFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 5
    .param p1, "name"    # Landroid/content/ComponentName;
    .param p2, "service"    # Landroid/os/IBinder;

    .prologue
    .line 403
    const-string v1, "InboundFragment"

    const-string v2, "onServiceConnected()"

    const-string v3, ""

    invoke-static {v1, v2, v3}, Lcom/samsung/android/app/FileShareServer/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, p2

    .line 404
    check-cast v0, Lcom/samsung/android/app/FileShareServer/ServerService$ServerServiceBinder;

    .line 405
    .local v0, "binder":Lcom/samsung/android/app/FileShareServer/ServerService$ServerServiceBinder;
    iget-object v1, p0, Lcom/samsung/android/app/FileShareServer/InboundFragment$3;->this$0:Lcom/samsung/android/app/FileShareServer/InboundFragment;

    invoke-virtual {v0}, Lcom/samsung/android/app/FileShareServer/ServerService$ServerServiceBinder;->getService()Lcom/samsung/android/app/FileShareServer/ServerService;

    move-result-object v2

    # setter for: Lcom/samsung/android/app/FileShareServer/InboundFragment;->mServerService:Lcom/samsung/android/app/FileShareServer/ServerService;
    invoke-static {v1, v2}, Lcom/samsung/android/app/FileShareServer/InboundFragment;->access$302(Lcom/samsung/android/app/FileShareServer/InboundFragment;Lcom/samsung/android/app/FileShareServer/ServerService;)Lcom/samsung/android/app/FileShareServer/ServerService;

    .line 407
    iget-object v1, p0, Lcom/samsung/android/app/FileShareServer/InboundFragment$3;->this$0:Lcom/samsung/android/app/FileShareServer/InboundFragment;

    # getter for: Lcom/samsung/android/app/FileShareServer/InboundFragment;->mInboundInfo:Lcom/samsung/android/app/FileShareServer/InboundInfo;
    invoke-static {v1}, Lcom/samsung/android/app/FileShareServer/InboundFragment;->access$100(Lcom/samsung/android/app/FileShareServer/InboundFragment;)Lcom/samsung/android/app/FileShareServer/InboundInfo;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 408
    iget-object v1, p0, Lcom/samsung/android/app/FileShareServer/InboundFragment$3;->this$0:Lcom/samsung/android/app/FileShareServer/InboundFragment;

    # getter for: Lcom/samsung/android/app/FileShareServer/InboundFragment;->mInboundInfo:Lcom/samsung/android/app/FileShareServer/InboundInfo;
    invoke-static {v1}, Lcom/samsung/android/app/FileShareServer/InboundFragment;->access$100(Lcom/samsung/android/app/FileShareServer/InboundFragment;)Lcom/samsung/android/app/FileShareServer/InboundInfo;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/samsung/android/app/FileShareServer/InboundInfo;->setListener(Lcom/samsung/android/app/FileShareServer/InboundInfo$IInboundInfoListener;)V

    .line 411
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/app/FileShareServer/InboundFragment$3;->this$0:Lcom/samsung/android/app/FileShareServer/InboundFragment;

    iget-object v2, p0, Lcom/samsung/android/app/FileShareServer/InboundFragment$3;->this$0:Lcom/samsung/android/app/FileShareServer/InboundFragment;

    # getter for: Lcom/samsung/android/app/FileShareServer/InboundFragment;->mServerService:Lcom/samsung/android/app/FileShareServer/ServerService;
    invoke-static {v2}, Lcom/samsung/android/app/FileShareServer/InboundFragment;->access$300(Lcom/samsung/android/app/FileShareServer/InboundFragment;)Lcom/samsung/android/app/FileShareServer/ServerService;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/app/FileShareServer/InboundFragment$3;->this$0:Lcom/samsung/android/app/FileShareServer/InboundFragment;

    # getter for: Lcom/samsung/android/app/FileShareServer/InboundFragment;->mInboundID:I
    invoke-static {v3}, Lcom/samsung/android/app/FileShareServer/InboundFragment;->access$400(Lcom/samsung/android/app/FileShareServer/InboundFragment;)I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/samsung/android/app/FileShareServer/ServerService;->getInboundInfo(I)Lcom/samsung/android/app/FileShareServer/InboundInfo;

    move-result-object v2

    # setter for: Lcom/samsung/android/app/FileShareServer/InboundFragment;->mInboundInfo:Lcom/samsung/android/app/FileShareServer/InboundInfo;
    invoke-static {v1, v2}, Lcom/samsung/android/app/FileShareServer/InboundFragment;->access$102(Lcom/samsung/android/app/FileShareServer/InboundFragment;Lcom/samsung/android/app/FileShareServer/InboundInfo;)Lcom/samsung/android/app/FileShareServer/InboundInfo;

    .line 413
    iget-object v1, p0, Lcom/samsung/android/app/FileShareServer/InboundFragment$3;->this$0:Lcom/samsung/android/app/FileShareServer/InboundFragment;

    # getter for: Lcom/samsung/android/app/FileShareServer/InboundFragment;->mInboundInfo:Lcom/samsung/android/app/FileShareServer/InboundInfo;
    invoke-static {v1}, Lcom/samsung/android/app/FileShareServer/InboundFragment;->access$100(Lcom/samsung/android/app/FileShareServer/InboundFragment;)Lcom/samsung/android/app/FileShareServer/InboundInfo;

    move-result-object v1

    if-nez v1, :cond_2

    .line 414
    const-string v1, "InboundFragment"

    const-string v2, "========== InboundInfo.RECEIVE_RESULT and Bitmap update complete! ========="

    const-string v3, ""

    invoke-static {v1, v2, v3}, Lcom/samsung/android/app/FileShareServer/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 417
    iget-object v1, p0, Lcom/samsung/android/app/FileShareServer/InboundFragment$3;->this$0:Lcom/samsung/android/app/FileShareServer/InboundFragment;

    iget-object v2, p0, Lcom/samsung/android/app/FileShareServer/InboundFragment$3;->this$0:Lcom/samsung/android/app/FileShareServer/InboundFragment;

    iget-object v3, p0, Lcom/samsung/android/app/FileShareServer/InboundFragment$3;->this$0:Lcom/samsung/android/app/FileShareServer/InboundFragment;

    # getter for: Lcom/samsung/android/app/FileShareServer/InboundFragment;->mInboundID:I
    invoke-static {v3}, Lcom/samsung/android/app/FileShareServer/InboundFragment;->access$400(Lcom/samsung/android/app/FileShareServer/InboundFragment;)I

    move-result v3

    # invokes: Lcom/samsung/android/app/FileShareServer/InboundFragment;->getInboundInfo(I)Lcom/samsung/android/app/FileShareServer/InboundInfo;
    invoke-static {v2, v3}, Lcom/samsung/android/app/FileShareServer/InboundFragment;->access$500(Lcom/samsung/android/app/FileShareServer/InboundFragment;I)Lcom/samsung/android/app/FileShareServer/InboundInfo;

    move-result-object v2

    # setter for: Lcom/samsung/android/app/FileShareServer/InboundFragment;->mInboundInfo:Lcom/samsung/android/app/FileShareServer/InboundInfo;
    invoke-static {v1, v2}, Lcom/samsung/android/app/FileShareServer/InboundFragment;->access$102(Lcom/samsung/android/app/FileShareServer/InboundFragment;Lcom/samsung/android/app/FileShareServer/InboundInfo;)Lcom/samsung/android/app/FileShareServer/InboundInfo;

    .line 424
    :goto_0
    iget-object v1, p0, Lcom/samsung/android/app/FileShareServer/InboundFragment$3;->this$0:Lcom/samsung/android/app/FileShareServer/InboundFragment;

    # getter for: Lcom/samsung/android/app/FileShareServer/InboundFragment;->mIntent:Landroid/content/Intent;
    invoke-static {v1}, Lcom/samsung/android/app/FileShareServer/InboundFragment;->access$600(Lcom/samsung/android/app/FileShareServer/InboundFragment;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    const-string v2, "com.samsung.android.app.FileShareServer.RECEIVE_RESULT"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 425
    const-string v1, "InboundFragment"

    const-string v2, "========== InboundInfo.RECEIVE_RESULT ========="

    const-string v3, ""

    invoke-static {v1, v2, v3}, Lcom/samsung/android/app/FileShareServer/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 429
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/app/FileShareServer/InboundFragment$3;->this$0:Lcom/samsung/android/app/FileShareServer/InboundFragment;

    # getter for: Lcom/samsung/android/app/FileShareServer/InboundFragment;->mInboundInfo:Lcom/samsung/android/app/FileShareServer/InboundInfo;
    invoke-static {v1}, Lcom/samsung/android/app/FileShareServer/InboundFragment;->access$100(Lcom/samsung/android/app/FileShareServer/InboundFragment;)Lcom/samsung/android/app/FileShareServer/InboundInfo;

    move-result-object v1

    if-nez v1, :cond_3

    .line 430
    const-string v1, "InboundFragment"

    const-string v2, "onServiceConnected"

    const-string v3, "mInboundInfo is null!"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/app/FileShareServer/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 431
    iget-object v1, p0, Lcom/samsung/android/app/FileShareServer/InboundFragment$3;->this$0:Lcom/samsung/android/app/FileShareServer/InboundFragment;

    invoke-virtual {v1}, Lcom/samsung/android/app/FileShareServer/InboundFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->finish()V

    .line 440
    :goto_1
    return-void

    .line 419
    :cond_2
    const-string v1, "InboundFragment"

    const-string v2, "========== InboundInfo.RECEIVE_PROGRESS else Bitmap is not ready ========="

    const-string v3, ""

    invoke-static {v1, v2, v3}, Lcom/samsung/android/app/FileShareServer/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 434
    :cond_3
    iget-object v1, p0, Lcom/samsung/android/app/FileShareServer/InboundFragment$3;->this$0:Lcom/samsung/android/app/FileShareServer/InboundFragment;

    new-instance v2, Lcom/samsung/android/app/FileShareServer/InboundListAdaptor;

    iget-object v3, p0, Lcom/samsung/android/app/FileShareServer/InboundFragment$3;->this$0:Lcom/samsung/android/app/FileShareServer/InboundFragment;

    invoke-virtual {v3}, Lcom/samsung/android/app/FileShareServer/InboundFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/android/app/FileShareServer/InboundFragment$3;->this$0:Lcom/samsung/android/app/FileShareServer/InboundFragment;

    # getter for: Lcom/samsung/android/app/FileShareServer/InboundFragment;->mInboundInfo:Lcom/samsung/android/app/FileShareServer/InboundInfo;
    invoke-static {v4}, Lcom/samsung/android/app/FileShareServer/InboundFragment;->access$100(Lcom/samsung/android/app/FileShareServer/InboundFragment;)Lcom/samsung/android/app/FileShareServer/InboundInfo;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lcom/samsung/android/app/FileShareServer/InboundListAdaptor;-><init>(Landroid/content/Context;Lcom/samsung/android/app/FileShareServer/InboundInfo;)V

    # setter for: Lcom/samsung/android/app/FileShareServer/InboundFragment;->mCustomAdaptor:Lcom/samsung/android/app/FileShareServer/InboundListAdaptor;
    invoke-static {v1, v2}, Lcom/samsung/android/app/FileShareServer/InboundFragment;->access$202(Lcom/samsung/android/app/FileShareServer/InboundFragment;Lcom/samsung/android/app/FileShareServer/InboundListAdaptor;)Lcom/samsung/android/app/FileShareServer/InboundListAdaptor;

    .line 435
    iget-object v1, p0, Lcom/samsung/android/app/FileShareServer/InboundFragment$3;->this$0:Lcom/samsung/android/app/FileShareServer/InboundFragment;

    # getter for: Lcom/samsung/android/app/FileShareServer/InboundFragment;->mFileList:Landroid/widget/ListView;
    invoke-static {v1}, Lcom/samsung/android/app/FileShareServer/InboundFragment;->access$700(Lcom/samsung/android/app/FileShareServer/InboundFragment;)Landroid/widget/ListView;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/app/FileShareServer/InboundFragment$3;->this$0:Lcom/samsung/android/app/FileShareServer/InboundFragment;

    # getter for: Lcom/samsung/android/app/FileShareServer/InboundFragment;->mCustomAdaptor:Lcom/samsung/android/app/FileShareServer/InboundListAdaptor;
    invoke-static {v2}, Lcom/samsung/android/app/FileShareServer/InboundFragment;->access$200(Lcom/samsung/android/app/FileShareServer/InboundFragment;)Lcom/samsung/android/app/FileShareServer/InboundListAdaptor;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 436
    iget-object v1, p0, Lcom/samsung/android/app/FileShareServer/InboundFragment$3;->this$0:Lcom/samsung/android/app/FileShareServer/InboundFragment;

    # getter for: Lcom/samsung/android/app/FileShareServer/InboundFragment;->mFileList:Landroid/widget/ListView;
    invoke-static {v1}, Lcom/samsung/android/app/FileShareServer/InboundFragment;->access$700(Lcom/samsung/android/app/FileShareServer/InboundFragment;)Landroid/widget/ListView;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/app/FileShareServer/InboundFragment$3;->this$0:Lcom/samsung/android/app/FileShareServer/InboundFragment;

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 437
    iget-object v1, p0, Lcom/samsung/android/app/FileShareServer/InboundFragment$3;->this$0:Lcom/samsung/android/app/FileShareServer/InboundFragment;

    # getter for: Lcom/samsung/android/app/FileShareServer/InboundFragment;->mInboundInfo:Lcom/samsung/android/app/FileShareServer/InboundInfo;
    invoke-static {v1}, Lcom/samsung/android/app/FileShareServer/InboundFragment;->access$100(Lcom/samsung/android/app/FileShareServer/InboundFragment;)Lcom/samsung/android/app/FileShareServer/InboundInfo;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/app/FileShareServer/InboundFragment$3;->this$0:Lcom/samsung/android/app/FileShareServer/InboundFragment;

    invoke-virtual {v1, v2}, Lcom/samsung/android/app/FileShareServer/InboundInfo;->setListener(Lcom/samsung/android/app/FileShareServer/InboundInfo$IInboundInfoListener;)V

    .line 439
    iget-object v1, p0, Lcom/samsung/android/app/FileShareServer/InboundFragment$3;->this$0:Lcom/samsung/android/app/FileShareServer/InboundFragment;

    # invokes: Lcom/samsung/android/app/FileShareServer/InboundFragment;->showStatus()V
    invoke-static {v1}, Lcom/samsung/android/app/FileShareServer/InboundFragment;->access$000(Lcom/samsung/android/app/FileShareServer/InboundFragment;)V

    goto :goto_1
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 3
    .param p1, "name"    # Landroid/content/ComponentName;

    .prologue
    .line 445
    const-string v0, "InboundFragment"

    const-string v1, "onServiceDisconnected()"

    const-string v2, ""

    invoke-static {v0, v1, v2}, Lcom/samsung/android/app/FileShareServer/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 446
    iget-object v0, p0, Lcom/samsung/android/app/FileShareServer/InboundFragment$3;->this$0:Lcom/samsung/android/app/FileShareServer/InboundFragment;

    const/4 v1, 0x0

    # setter for: Lcom/samsung/android/app/FileShareServer/InboundFragment;->mServerService:Lcom/samsung/android/app/FileShareServer/ServerService;
    invoke-static {v0, v1}, Lcom/samsung/android/app/FileShareServer/InboundFragment;->access$302(Lcom/samsung/android/app/FileShareServer/InboundFragment;Lcom/samsung/android/app/FileShareServer/ServerService;)Lcom/samsung/android/app/FileShareServer/ServerService;

    .line 447
    return-void
.end method
