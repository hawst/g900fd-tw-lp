.class public Lcom/samsung/android/app/FileShareServer/FileTypeManager;
.super Ljava/lang/Object;
.source "FileTypeManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/app/FileShareServer/FileTypeManager$MediaFileType;
    }
.end annotation


# static fields
.field private static final FILE_ICON_DEFAULT:I = 0x7f020001

.field private static final FILE_TYPE_3G2_AUDIO:I = 0x12

.field private static final FILE_TYPE_3GA:I = 0x9

.field private static final FILE_TYPE_3GPP:I = 0x21

.field private static final FILE_TYPE_3GPP2:I = 0x22

.field private static final FILE_TYPE_3GPP_AUDIO:I = 0x14

.field private static final FILE_TYPE_3GP_AUDIO:I = 0x11

.field private static final FILE_TYPE_AAC:I = 0x8

.field private static final FILE_TYPE_AK3G:I = 0x2e

.field private static final FILE_TYPE_AMR:I = 0x4

.field private static final FILE_TYPE_APK:I = 0x64

.field private static final FILE_TYPE_ASC:I = 0x4e

.field private static final FILE_TYPE_ASF:I = 0x25

.field private static final FILE_TYPE_ASF_AUDIO:I = 0x13

.field private static final FILE_TYPE_AVI:I = 0x26

.field private static final FILE_TYPE_AWB:I = 0x5

.field private static final FILE_TYPE_BMP:I = 0x40

.field private static final FILE_TYPE_CSV:I = 0x50

.field private static final FILE_TYPE_DCF:I = 0x57

.field private static final FILE_TYPE_DIVX:I = 0x27

.field private static final FILE_TYPE_DOC:I = 0x52

.field private static final FILE_TYPE_EBOOK:I = 0x59

.field private static final FILE_TYPE_EML:I = 0x8e

.field private static final FILE_TYPE_FLAC:I = 0xa

.field private static final FILE_TYPE_FLV:I = 0x28

.field private static final FILE_TYPE_GIF:I = 0x3e

.field private static final FILE_TYPE_GUL:I = 0x56

.field private static final FILE_TYPE_HTML:I = 0x7e

.field private static final FILE_TYPE_HWP:I = 0x8d

.field private static final FILE_TYPE_IMY:I = 0x18

.field private static final FILE_TYPE_ISMA:I = 0xd

.field private static final FILE_TYPE_ISMV:I = 0x33

.field private static final FILE_TYPE_JAD:I = 0x6e

.field private static final FILE_TYPE_JAR:I = 0x6f

.field private static final FILE_TYPE_JPEG:I = 0x3d

.field private static final FILE_TYPE_K3G:I = 0x2d

.field private static final FILE_TYPE_M3U:I = 0x47

.field private static final FILE_TYPE_M4A:I = 0x2

.field private static final FILE_TYPE_M4B:I = 0xb

.field private static final FILE_TYPE_M4V:I = 0x20

.field private static final FILE_TYPE_MEMO:I = 0x96

.field private static final FILE_TYPE_MID:I = 0x16

.field private static final FILE_TYPE_MKV:I = 0x29

.field private static final FILE_TYPE_MOV:I = 0x2a

.field private static final FILE_TYPE_MP3:I = 0x1

.field private static final FILE_TYPE_MP4:I = 0x1f

.field private static final FILE_TYPE_MP4_AUDIO:I = 0x10

.field private static final FILE_TYPE_MPG:I = 0x24

.field private static final FILE_TYPE_ODF:I = 0x58

.field private static final FILE_TYPE_OGG:I = 0x7

.field private static final FILE_TYPE_PDF:I = 0x51

.field private static final FILE_TYPE_PLS:I = 0x48

.field private static final FILE_TYPE_PNG:I = 0x3f

.field private static final FILE_TYPE_PPS:I = 0x4f

.field private static final FILE_TYPE_PPT:I = 0x54

.field private static final FILE_TYPE_PYA:I = 0xc

.field private static final FILE_TYPE_PYV:I = 0x2b

.field private static final FILE_TYPE_QCP:I = 0x15

.field private static final FILE_TYPE_RM:I = 0x30

.field private static final FILE_TYPE_RMVB:I = 0x31

.field private static final FILE_TYPE_SASF:I = 0x91

.field private static final FILE_TYPE_SCC:I = 0x95

.field private static final FILE_TYPE_SDP:I = 0x32

.field private static final FILE_TYPE_SKM:I = 0x2c

.field private static final FILE_TYPE_SMF:I = 0x17

.field private static final FILE_TYPE_SNB:I = 0x90

.field private static final FILE_TYPE_SPD:I = 0x94

.field private static final FILE_TYPE_SSF:I = 0x92

.field private static final FILE_TYPE_SVG:I = 0x5b

.field private static final FILE_TYPE_SWF:I = 0x5a

.field private static final FILE_TYPE_TRC:I = 0x93

.field private static final FILE_TYPE_TXT:I = 0x55

.field private static final FILE_TYPE_VCF:I = 0x79

.field private static final FILE_TYPE_VCS:I = 0x78

.field private static final FILE_TYPE_VNT:I = 0x7a

.field private static final FILE_TYPE_VTS:I = 0x7b

.field private static final FILE_TYPE_WAV:I = 0x3

.field private static final FILE_TYPE_WBMP:I = 0x41

.field private static final FILE_TYPE_WEBM:I = 0x2f

.field private static final FILE_TYPE_WGT:I = 0x65

.field private static final FILE_TYPE_WMA:I = 0x6

.field private static final FILE_TYPE_WMV:I = 0x23

.field private static final FILE_TYPE_WPL:I = 0x49

.field private static final FILE_TYPE_XHTML:I = 0x80

.field private static final FILE_TYPE_XLS:I = 0x53

.field private static final FILE_TYPE_XML:I = 0x7f

.field private static final FILE_TYPE_ZIP:I = 0x8f

.field private static final FIRST_IMAGE_FILE_TYPE:I = 0x3d

.field private static final FIRST_VIDEO_FILE_TYPE:I = 0x1f

.field private static final LAST_IMAGE_FILE_TYPE:I = 0x41

.field private static final LAST_VIDEO_FILE_TYPE:I = 0x33

.field private static final MIME_UNKNOWN_APPLICATION:Ljava/lang/String; = "application/octet-stream"

.field private static final TAGClass:Ljava/lang/String; = "FileTypeManager"

.field private static sFileTypeMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/samsung/android/app/FileShareServer/FileTypeManager$MediaFileType;",
            ">;"
        }
    .end annotation
.end field

.field private static sMimeType:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static sMimeTypeMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/16 v8, 0x16

    const v7, 0x7f020001

    const v6, 0x7f020007

    const/high16 v5, 0x7f020000

    const v4, 0x7f020003

    .line 220
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/samsung/android/app/FileShareServer/FileTypeManager;->sFileTypeMap:Ljava/util/HashMap;

    .line 222
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/samsung/android/app/FileShareServer/FileTypeManager;->sMimeTypeMap:Ljava/util/HashMap;

    .line 224
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/samsung/android/app/FileShareServer/FileTypeManager;->sMimeType:Ljava/util/HashMap;

    .line 233
    const-string v0, "EML"

    const/16 v1, 0x8e

    const-string v2, "message/rfc822"

    const-string v3, "EML"

    invoke-static {v0, v1, v2, v3, v7}, Lcom/samsung/android/app/FileShareServer/FileTypeManager;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 234
    const-string v0, "MP3"

    const/4 v1, 0x1

    const-string v2, "audio/mpeg"

    const-string v3, "Mpeg"

    invoke-static {v0, v1, v2, v3, v4}, Lcom/samsung/android/app/FileShareServer/FileTypeManager;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 235
    const-string v0, "M4A"

    const/4 v1, 0x2

    const-string v2, "audio/mp4"

    const-string v3, "M4A"

    invoke-static {v0, v1, v2, v3, v4}, Lcom/samsung/android/app/FileShareServer/FileTypeManager;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 236
    const-string v0, "WAV"

    const/4 v1, 0x3

    const-string v2, "audio/x-wav"

    const-string v3, "WAVE"

    invoke-static {v0, v1, v2, v3, v4}, Lcom/samsung/android/app/FileShareServer/FileTypeManager;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 237
    const-string v0, "AMR"

    const/4 v1, 0x4

    const-string v2, "audio/amr"

    const-string v3, "AMR"

    invoke-static {v0, v1, v2, v3, v4}, Lcom/samsung/android/app/FileShareServer/FileTypeManager;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 238
    const-string v0, "AWB"

    const/4 v1, 0x5

    const-string v2, "audio/amr-wb"

    const-string v3, "AWB"

    invoke-static {v0, v1, v2, v3, v4}, Lcom/samsung/android/app/FileShareServer/FileTypeManager;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 239
    const-string v0, "WMA"

    const/4 v1, 0x6

    const-string v2, "audio/x-ms-wma"

    const-string v3, "WMA"

    invoke-static {v0, v1, v2, v3, v4}, Lcom/samsung/android/app/FileShareServer/FileTypeManager;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 240
    const-string v0, "OGG"

    const/4 v1, 0x7

    const-string v2, "audio/ogg"

    const-string v3, "OGG"

    invoke-static {v0, v1, v2, v3, v4}, Lcom/samsung/android/app/FileShareServer/FileTypeManager;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 241
    const-string v0, "OGA"

    const/4 v1, 0x7

    const-string v2, "audio/ogg"

    const-string v3, "OGA"

    invoke-static {v0, v1, v2, v3, v4}, Lcom/samsung/android/app/FileShareServer/FileTypeManager;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 242
    const-string v0, "AAC"

    const/16 v1, 0x8

    const-string v2, "audio/aac"

    const-string v3, "AAC"

    invoke-static {v0, v1, v2, v3, v4}, Lcom/samsung/android/app/FileShareServer/FileTypeManager;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 243
    const-string v0, "3GA"

    const/16 v1, 0x9

    const-string v2, "audio/3gpp"

    const-string v3, "3GA"

    invoke-static {v0, v1, v2, v3, v4}, Lcom/samsung/android/app/FileShareServer/FileTypeManager;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 244
    const-string v0, "FLAC"

    const/16 v1, 0xa

    const-string v2, "audio/flac"

    const-string v3, "FLAC"

    invoke-static {v0, v1, v2, v3, v4}, Lcom/samsung/android/app/FileShareServer/FileTypeManager;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 245
    const-string v0, "MPGA"

    const/4 v1, 0x1

    const-string v2, "audio/mpeg"

    const-string v3, "MPGA"

    invoke-static {v0, v1, v2, v3, v4}, Lcom/samsung/android/app/FileShareServer/FileTypeManager;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 246
    const-string v0, "MP4_A"

    const/16 v1, 0x10

    const-string v2, "audio/mp4"

    const-string v3, "MP4 Audio"

    invoke-static {v0, v1, v2, v3, v4}, Lcom/samsung/android/app/FileShareServer/FileTypeManager;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 248
    const-string v0, "3GP_A"

    const/16 v1, 0x11

    const-string v2, "audio/3gpp"

    const-string v3, "3GP Audio"

    invoke-static {v0, v1, v2, v3, v4}, Lcom/samsung/android/app/FileShareServer/FileTypeManager;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 250
    const-string v0, "3G2_A"

    const/16 v1, 0x12

    const-string v2, "audio/3gpp2"

    const-string v3, "3G2 Audio"

    invoke-static {v0, v1, v2, v3, v4}, Lcom/samsung/android/app/FileShareServer/FileTypeManager;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 252
    const-string v0, "ASF_A"

    const/16 v1, 0x13

    const-string v2, "audio/x-ms-asf"

    const-string v3, "ASF Audio"

    invoke-static {v0, v1, v2, v3, v4}, Lcom/samsung/android/app/FileShareServer/FileTypeManager;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 254
    const-string v0, "3GPP_A"

    const/16 v1, 0x14

    const-string v2, "audio/3gpp"

    const-string v3, "3GPP"

    invoke-static {v0, v1, v2, v3, v4}, Lcom/samsung/android/app/FileShareServer/FileTypeManager;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 256
    const-string v0, "MID"

    const-string v1, "audio/midi"

    const-string v2, "MIDI"

    invoke-static {v0, v8, v1, v2, v4}, Lcom/samsung/android/app/FileShareServer/FileTypeManager;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 257
    const-string v0, "XMF"

    const-string v1, "audio/midi"

    const-string v2, "XMF"

    invoke-static {v0, v8, v1, v2, v4}, Lcom/samsung/android/app/FileShareServer/FileTypeManager;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 258
    const-string v0, "MXMF"

    const-string v1, "audio/midi"

    const-string v2, "MXMF"

    invoke-static {v0, v8, v1, v2, v4}, Lcom/samsung/android/app/FileShareServer/FileTypeManager;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 259
    const-string v0, "RTTTL"

    const-string v1, "audio/midi"

    const-string v2, "RTTTL"

    invoke-static {v0, v8, v1, v2, v4}, Lcom/samsung/android/app/FileShareServer/FileTypeManager;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 260
    const-string v0, "SMF"

    const/16 v1, 0x17

    const-string v2, "audio/sp-midi"

    const-string v3, "SMF"

    invoke-static {v0, v1, v2, v3, v4}, Lcom/samsung/android/app/FileShareServer/FileTypeManager;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 261
    const-string v0, "IMY"

    const/16 v1, 0x18

    const-string v2, "audio/imelody"

    const-string v3, "IMY"

    invoke-static {v0, v1, v2, v3, v4}, Lcom/samsung/android/app/FileShareServer/FileTypeManager;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 262
    const-string v0, "MIDI"

    const-string v1, "audio/midi"

    const-string v2, "MIDI"

    invoke-static {v0, v8, v1, v2, v4}, Lcom/samsung/android/app/FileShareServer/FileTypeManager;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 263
    const-string v0, "RTX"

    const-string v1, "audio/midi"

    const-string v2, "MIDI"

    invoke-static {v0, v8, v1, v2, v4}, Lcom/samsung/android/app/FileShareServer/FileTypeManager;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 264
    const-string v0, "OTA"

    const-string v1, "audio/midi"

    const-string v2, "MIDI"

    invoke-static {v0, v8, v1, v2, v4}, Lcom/samsung/android/app/FileShareServer/FileTypeManager;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 265
    const-string v0, "PYA"

    const/16 v1, 0xc

    const-string v2, "audio/vnd.ms-playready.media.pya"

    const-string v3, "PYA"

    invoke-static {v0, v1, v2, v3, v4}, Lcom/samsung/android/app/FileShareServer/FileTypeManager;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 267
    const-string v0, "M4B"

    const/16 v1, 0xb

    const-string v2, "audio/mp4"

    const-string v3, "M4B"

    invoke-static {v0, v1, v2, v3, v4}, Lcom/samsung/android/app/FileShareServer/FileTypeManager;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 268
    const-string v0, "ISMA"

    const/16 v1, 0xd

    const-string v2, "audio/isma"

    const-string v3, "ISMA"

    invoke-static {v0, v1, v2, v3, v4}, Lcom/samsung/android/app/FileShareServer/FileTypeManager;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 269
    const-string v0, "QCP"

    const/16 v1, 0x15

    const-string v2, "audio/qcelp"

    const-string v3, "QCP"

    invoke-static {v0, v1, v2, v3, v4}, Lcom/samsung/android/app/FileShareServer/FileTypeManager;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 270
    const-string v0, "M3U"

    const/16 v1, 0x47

    const-string v2, "audio/x-mpegurl"

    const-string v3, "M3U"

    invoke-static {v0, v1, v2, v3, v4}, Lcom/samsung/android/app/FileShareServer/FileTypeManager;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 272
    const-string v0, "PLS"

    const/16 v1, 0x48

    const-string v2, "audio/x-scpls"

    const-string v3, "PLS"

    invoke-static {v0, v1, v2, v3, v4}, Lcom/samsung/android/app/FileShareServer/FileTypeManager;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 274
    const-string v0, "MPEG"

    const/16 v1, 0x24

    const-string v2, "video/mpeg"

    const-string v3, "MPEG"

    invoke-static {v0, v1, v2, v3, v6}, Lcom/samsung/android/app/FileShareServer/FileTypeManager;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 275
    const-string v0, "MPG"

    const/16 v1, 0x24

    const-string v2, "video/mpeg"

    const-string v3, "MPEG"

    invoke-static {v0, v1, v2, v3, v6}, Lcom/samsung/android/app/FileShareServer/FileTypeManager;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 276
    const-string v0, "MP4"

    const/16 v1, 0x1f

    const-string v2, "video/mp4"

    const-string v3, "MP4"

    invoke-static {v0, v1, v2, v3, v6}, Lcom/samsung/android/app/FileShareServer/FileTypeManager;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 277
    const-string v0, "M4V"

    const/16 v1, 0x20

    const-string v2, "video/mp4"

    const-string v3, "M4V"

    invoke-static {v0, v1, v2, v3, v6}, Lcom/samsung/android/app/FileShareServer/FileTypeManager;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 278
    const-string v0, "3GP"

    const/16 v1, 0x21

    const-string v2, "video/3gpp"

    const-string v3, "3GP"

    invoke-static {v0, v1, v2, v3, v6}, Lcom/samsung/android/app/FileShareServer/FileTypeManager;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 279
    const-string v0, "3GPP"

    const/16 v1, 0x21

    const-string v2, "video/3gpp"

    const-string v3, "3GPP"

    invoke-static {v0, v1, v2, v3, v6}, Lcom/samsung/android/app/FileShareServer/FileTypeManager;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 280
    const-string v0, "3G2"

    const/16 v1, 0x22

    const-string v2, "video/3gpp2"

    const-string v3, "3G2"

    invoke-static {v0, v1, v2, v3, v6}, Lcom/samsung/android/app/FileShareServer/FileTypeManager;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 281
    const-string v0, "3GPP2"

    const/16 v1, 0x22

    const-string v2, "video/3gpp2"

    const-string v3, "3GPP2"

    invoke-static {v0, v1, v2, v3, v6}, Lcom/samsung/android/app/FileShareServer/FileTypeManager;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 283
    const-string v0, "WMV"

    const/16 v1, 0x23

    const-string v2, "video/x-ms-wmv"

    const-string v3, "WMV"

    invoke-static {v0, v1, v2, v3, v6}, Lcom/samsung/android/app/FileShareServer/FileTypeManager;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 284
    const-string v0, "ASF"

    const/16 v1, 0x25

    const-string v2, "video/x-ms-asf"

    const-string v3, "ASF"

    invoke-static {v0, v1, v2, v3, v6}, Lcom/samsung/android/app/FileShareServer/FileTypeManager;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 285
    const-string v0, "AVI"

    const/16 v1, 0x26

    const-string v2, "video/avi"

    const-string v3, "AVI"

    invoke-static {v0, v1, v2, v3, v6}, Lcom/samsung/android/app/FileShareServer/FileTypeManager;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 286
    const-string v0, "DIVX"

    const/16 v1, 0x27

    const-string v2, "video/divx"

    const-string v3, "DIVX"

    invoke-static {v0, v1, v2, v3, v6}, Lcom/samsung/android/app/FileShareServer/FileTypeManager;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 287
    const-string v0, "FLV"

    const/16 v1, 0x28

    const-string v2, "video/flv"

    const-string v3, "FLV"

    invoke-static {v0, v1, v2, v3, v6}, Lcom/samsung/android/app/FileShareServer/FileTypeManager;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 288
    const-string v0, "MKV"

    const/16 v1, 0x29

    const-string v2, "video/mkv"

    const-string v3, "MKV"

    invoke-static {v0, v1, v2, v3, v6}, Lcom/samsung/android/app/FileShareServer/FileTypeManager;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 289
    const-string v0, "SDP"

    const/16 v1, 0x32

    const-string v2, "application/sdp"

    const-string v3, "SDP"

    invoke-static {v0, v1, v2, v3, v6}, Lcom/samsung/android/app/FileShareServer/FileTypeManager;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 291
    const-string v0, "RM"

    const/16 v1, 0x30

    const-string v2, "video/mp4"

    const-string v3, "RM"

    invoke-static {v0, v1, v2, v3, v6}, Lcom/samsung/android/app/FileShareServer/FileTypeManager;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 292
    const-string v0, "RMVB"

    const/16 v1, 0x31

    const-string v2, "video/mp4"

    const-string v3, "RMVB"

    invoke-static {v0, v1, v2, v3, v6}, Lcom/samsung/android/app/FileShareServer/FileTypeManager;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 293
    const-string v0, "MOV"

    const/16 v1, 0x2a

    const-string v2, "video/quicktime"

    const-string v3, "MOV"

    invoke-static {v0, v1, v2, v3, v6}, Lcom/samsung/android/app/FileShareServer/FileTypeManager;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 295
    const-string v0, "PYV"

    const/16 v1, 0x2b

    const-string v2, "video/vnd.ms-playready.media.pyv"

    const-string v3, "PYV"

    invoke-static {v0, v1, v2, v3, v6}, Lcom/samsung/android/app/FileShareServer/FileTypeManager;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 297
    const-string v0, "ISMV"

    const/16 v1, 0x33

    const-string v2, "video/ismv"

    const-string v3, "ISMV"

    invoke-static {v0, v1, v2, v3, v6}, Lcom/samsung/android/app/FileShareServer/FileTypeManager;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 298
    const-string v0, "WEBM"

    const/16 v1, 0x2f

    const-string v2, "video/webm"

    const-string v3, "WEBM"

    invoke-static {v0, v1, v2, v3, v6}, Lcom/samsung/android/app/FileShareServer/FileTypeManager;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 300
    const-string v0, "JPG"

    const/16 v1, 0x3d

    const-string v2, "image/jpeg"

    const-string v3, "JPEG"

    const v4, 0x7f020002

    invoke-static {v0, v1, v2, v3, v4}, Lcom/samsung/android/app/FileShareServer/FileTypeManager;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 301
    const-string v0, "JPEG"

    const/16 v1, 0x3d

    const-string v2, "image/jpeg"

    const-string v3, "JPEG"

    const v4, 0x7f020002

    invoke-static {v0, v1, v2, v3, v4}, Lcom/samsung/android/app/FileShareServer/FileTypeManager;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 302
    const-string v0, "MY5"

    const/16 v1, 0x3d

    const-string v2, "image/vnd.tmo.my5"

    const-string v3, "JPEG"

    const v4, 0x7f020002

    invoke-static {v0, v1, v2, v3, v4}, Lcom/samsung/android/app/FileShareServer/FileTypeManager;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 304
    const-string v0, "GIF"

    const/16 v1, 0x3e

    const-string v2, "image/gif"

    const-string v3, "GIF"

    const v4, 0x7f020002

    invoke-static {v0, v1, v2, v3, v4}, Lcom/samsung/android/app/FileShareServer/FileTypeManager;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 305
    const-string v0, "PNG"

    const/16 v1, 0x3f

    const-string v2, "image/png"

    const-string v3, "PNG"

    const v4, 0x7f020002

    invoke-static {v0, v1, v2, v3, v4}, Lcom/samsung/android/app/FileShareServer/FileTypeManager;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 306
    const-string v0, "BMP"

    const/16 v1, 0x40

    const-string v2, "image/x-ms-bmp"

    const-string v3, "Microsoft BMP"

    const v4, 0x7f020002

    invoke-static {v0, v1, v2, v3, v4}, Lcom/samsung/android/app/FileShareServer/FileTypeManager;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 308
    const-string v0, "WBMP"

    const/16 v1, 0x41

    const-string v2, "image/vnd.wap.wbmp"

    const-string v3, "Wireless BMP"

    const v4, 0x7f020002

    invoke-static {v0, v1, v2, v3, v4}, Lcom/samsung/android/app/FileShareServer/FileTypeManager;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 311
    const-string v0, "WPL"

    const/16 v1, 0x49

    const-string v2, "application/vnd.ms-wpl"

    const-string v3, "WPL"

    invoke-static {v0, v1, v2, v3, v7}, Lcom/samsung/android/app/FileShareServer/FileTypeManager;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 313
    const-string v0, "PDF"

    const/16 v1, 0x51

    const-string v2, "application/pdf"

    const-string v3, "Acrobat PDF"

    invoke-static {v0, v1, v2, v3, v5}, Lcom/samsung/android/app/FileShareServer/FileTypeManager;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 315
    const-string v0, "RTF"

    const/16 v1, 0x52

    const-string v2, "application/msword"

    const-string v3, "Microsoft Office WORD"

    invoke-static {v0, v1, v2, v3, v5}, Lcom/samsung/android/app/FileShareServer/FileTypeManager;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 317
    const-string v0, "DOC"

    const/16 v1, 0x52

    const-string v2, "application/msword"

    const-string v3, "Microsoft Office WORD"

    invoke-static {v0, v1, v2, v3, v5}, Lcom/samsung/android/app/FileShareServer/FileTypeManager;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 319
    const-string v0, "DOCX"

    const/16 v1, 0x52

    const-string v2, "application/vnd.openxmlformats-officedocument.wordprocessingml.document"

    const-string v3, "Microsoft Office WORD"

    invoke-static {v0, v1, v2, v3, v5}, Lcom/samsung/android/app/FileShareServer/FileTypeManager;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 322
    const-string v0, "DOT"

    const/16 v1, 0x52

    const-string v2, "application/msword"

    const-string v3, "Microsoft Office WORD"

    invoke-static {v0, v1, v2, v3, v5}, Lcom/samsung/android/app/FileShareServer/FileTypeManager;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 324
    const-string v0, "DOTX"

    const/16 v1, 0x52

    const-string v2, "application/vnd.openxmlformats-officedocument.wordprocessingml.template"

    const-string v3, "Microsoft Office WORD"

    invoke-static {v0, v1, v2, v3, v5}, Lcom/samsung/android/app/FileShareServer/FileTypeManager;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 327
    const-string v0, "CSV"

    const/16 v1, 0x50

    const-string v2, "text/comma-separated-values"

    const-string v3, "Microsoft Office Excel"

    invoke-static {v0, v1, v2, v3, v5}, Lcom/samsung/android/app/FileShareServer/FileTypeManager;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 329
    const-string v0, "XLS"

    const/16 v1, 0x53

    const-string v2, "application/vnd.ms-excel"

    const-string v3, "Microsoft Office Excel"

    invoke-static {v0, v1, v2, v3, v5}, Lcom/samsung/android/app/FileShareServer/FileTypeManager;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 331
    const-string v0, "XLSX"

    const/16 v1, 0x53

    const-string v2, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"

    const-string v3, "Microsoft Office Excel"

    invoke-static {v0, v1, v2, v3, v5}, Lcom/samsung/android/app/FileShareServer/FileTypeManager;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 334
    const-string v0, "XLT"

    const/16 v1, 0x53

    const-string v2, "application/vnd.ms-excel"

    const-string v3, "Microsoft Office Excel"

    invoke-static {v0, v1, v2, v3, v5}, Lcom/samsung/android/app/FileShareServer/FileTypeManager;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 336
    const-string v0, "XLTX"

    const/16 v1, 0x53

    const-string v2, "application/vnd.openxmlformats-officedocument.spreadsheetml.template"

    const-string v3, "Microsoft Office Excel"

    invoke-static {v0, v1, v2, v3, v5}, Lcom/samsung/android/app/FileShareServer/FileTypeManager;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 339
    const-string v0, "PPS"

    const/16 v1, 0x4f

    const-string v2, "application/vnd.ms-powerpoint"

    const-string v3, "Microsoft Office PowerPoint"

    invoke-static {v0, v1, v2, v3, v5}, Lcom/samsung/android/app/FileShareServer/FileTypeManager;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 341
    const-string v0, "PPT"

    const/16 v1, 0x54

    const-string v2, "application/vnd.ms-powerpoint"

    const-string v3, "Microsoft Office PowerPoint"

    invoke-static {v0, v1, v2, v3, v5}, Lcom/samsung/android/app/FileShareServer/FileTypeManager;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 343
    const-string v0, "PPTX"

    const/16 v1, 0x54

    const-string v2, "application/vnd.openxmlformats-officedocument.presentationml.presentation"

    const-string v3, "Microsoft Office PowerPoint"

    invoke-static {v0, v1, v2, v3, v5}, Lcom/samsung/android/app/FileShareServer/FileTypeManager;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 346
    const-string v0, "POT"

    const/16 v1, 0x54

    const-string v2, "application/vnd.ms-powerpoint"

    const-string v3, "Microsoft Office PowerPoint"

    invoke-static {v0, v1, v2, v3, v5}, Lcom/samsung/android/app/FileShareServer/FileTypeManager;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 348
    const-string v0, "POTX"

    const/16 v1, 0x54

    const-string v2, "application/vnd.openxmlformats-officedocument.presentationml.template"

    const-string v3, "Microsoft Office PowerPoint"

    invoke-static {v0, v1, v2, v3, v5}, Lcom/samsung/android/app/FileShareServer/FileTypeManager;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 351
    const-string v0, "ASC"

    const/16 v1, 0x4e

    const-string v2, "text/plain"

    const-string v3, "Text Document"

    invoke-static {v0, v1, v2, v3, v5}, Lcom/samsung/android/app/FileShareServer/FileTypeManager;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 353
    const-string v0, "TXT"

    const/16 v1, 0x55

    const-string v2, "text/plain"

    const-string v3, "Text Document"

    invoke-static {v0, v1, v2, v3, v5}, Lcom/samsung/android/app/FileShareServer/FileTypeManager;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 355
    const-string v0, "GUL"

    const/16 v1, 0x56

    const-string v2, "application/jungumword"

    const-string v3, "Jungum Word"

    invoke-static {v0, v1, v2, v3, v5}, Lcom/samsung/android/app/FileShareServer/FileTypeManager;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 357
    const-string v0, "EPUB"

    const/16 v1, 0x59

    const-string v2, "application/epub+zip"

    const-string v3, "eBookReader"

    invoke-static {v0, v1, v2, v3, v5}, Lcom/samsung/android/app/FileShareServer/FileTypeManager;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 359
    const-string v0, "ACSM"

    const/16 v1, 0x59

    const-string v2, "application/vnd.adobe.adept+xml"

    const-string v3, "eBookReader"

    invoke-static {v0, v1, v2, v3, v5}, Lcom/samsung/android/app/FileShareServer/FileTypeManager;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 361
    const-string v0, "SWF"

    const/16 v1, 0x5a

    const-string v2, "application/x-shockwave-flash"

    const-string v3, "SWF"

    invoke-static {v0, v1, v2, v3, v5}, Lcom/samsung/android/app/FileShareServer/FileTypeManager;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 363
    const-string v0, "SVG"

    const/16 v1, 0x5b

    const-string v2, "image/svg+xml"

    const-string v3, "SVG"

    invoke-static {v0, v1, v2, v3, v5}, Lcom/samsung/android/app/FileShareServer/FileTypeManager;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 364
    const-string v0, "DCF"

    const/16 v1, 0x57

    const-string v2, "application/vnd.oma.drm.content"

    const-string v3, "DRM Content"

    invoke-static {v0, v1, v2, v3, v7}, Lcom/samsung/android/app/FileShareServer/FileTypeManager;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 366
    const-string v0, "ODF"

    const/16 v1, 0x58

    const-string v2, "application/vnd.oma.drm.content"

    const-string v3, "DRM Content"

    invoke-static {v0, v1, v2, v3, v7}, Lcom/samsung/android/app/FileShareServer/FileTypeManager;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 368
    const-string v0, "APK"

    const/16 v1, 0x64

    const-string v2, "application/vnd.android.package-archive"

    const-string v3, "Android package install file"

    invoke-static {v0, v1, v2, v3, v7}, Lcom/samsung/android/app/FileShareServer/FileTypeManager;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 370
    const-string v0, "JAD"

    const/16 v1, 0x6e

    const-string v2, "text/vnd.sun.j2me.app-descriptor"

    const-string v3, "JAD"

    invoke-static {v0, v1, v2, v3, v7}, Lcom/samsung/android/app/FileShareServer/FileTypeManager;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 372
    const-string v0, "JAR"

    const/16 v1, 0x6f

    const-string v2, "application/java-archive "

    const-string v3, "JAR"

    invoke-static {v0, v1, v2, v3, v7}, Lcom/samsung/android/app/FileShareServer/FileTypeManager;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 374
    const-string v0, "VCS"

    const/16 v1, 0x78

    const-string v2, "text/x-vCalendar"

    const-string v3, "VCS"

    const v4, 0x7f020004

    invoke-static {v0, v1, v2, v3, v4}, Lcom/samsung/android/app/FileShareServer/FileTypeManager;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 376
    const-string v0, "ICS"

    const/16 v1, 0x78

    const-string v2, "text/x-vCalendar"

    const-string v3, "ICS"

    const v4, 0x7f020004

    invoke-static {v0, v1, v2, v3, v4}, Lcom/samsung/android/app/FileShareServer/FileTypeManager;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 378
    const-string v0, "VTS"

    const/16 v1, 0x7b

    const-string v2, "text/x-vtodo"

    const-string v3, "VTS"

    const v4, 0x7f020004

    invoke-static {v0, v1, v2, v3, v4}, Lcom/samsung/android/app/FileShareServer/FileTypeManager;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 380
    const-string v0, "VCF"

    const/16 v1, 0x79

    const-string v2, "text/x-vcard"

    const-string v3, "VCF"

    const v4, 0x7f020005

    invoke-static {v0, v1, v2, v3, v4}, Lcom/samsung/android/app/FileShareServer/FileTypeManager;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 381
    const-string v0, "VNT"

    const/16 v1, 0x7a

    const-string v2, "text/x-vnote"

    const-string v3, "VNT"

    invoke-static {v0, v1, v2, v3, v5}, Lcom/samsung/android/app/FileShareServer/FileTypeManager;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 382
    const-string v0, "HTML"

    const/16 v1, 0x7e

    const-string v2, "text/html"

    const-string v3, "HTML"

    invoke-static {v0, v1, v2, v3, v5}, Lcom/samsung/android/app/FileShareServer/FileTypeManager;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 383
    const-string v0, "HTM"

    const/16 v1, 0x7e

    const-string v2, "text/html"

    const-string v3, "HTML"

    invoke-static {v0, v1, v2, v3, v5}, Lcom/samsung/android/app/FileShareServer/FileTypeManager;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 384
    const-string v0, "XHTML"

    const/16 v1, 0x80

    const-string v2, "text/html"

    const-string v3, "XHTML"

    invoke-static {v0, v1, v2, v3, v5}, Lcom/samsung/android/app/FileShareServer/FileTypeManager;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 385
    const-string v0, "XML"

    const/16 v1, 0x7f

    const-string v2, "application/xhtml+xml"

    const-string v3, "XML"

    invoke-static {v0, v1, v2, v3, v5}, Lcom/samsung/android/app/FileShareServer/FileTypeManager;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 387
    const-string v0, "WGT"

    const/16 v1, 0x65

    const-string v2, "application/vnd.samsung.widget"

    const-string v3, "WGT"

    invoke-static {v0, v1, v2, v3, v5}, Lcom/samsung/android/app/FileShareServer/FileTypeManager;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 389
    const-string v0, "HWP"

    const/16 v1, 0x8d

    const-string v2, "application/x-hwp"

    const-string v3, "HWP"

    invoke-static {v0, v1, v2, v3, v5}, Lcom/samsung/android/app/FileShareServer/FileTypeManager;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 391
    const-string v0, "ZIP"

    const/16 v1, 0x8f

    const-string v2, "application/zip"

    const-string v3, "ZIP"

    invoke-static {v0, v1, v2, v3, v7}, Lcom/samsung/android/app/FileShareServer/FileTypeManager;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 392
    const-string v0, "SNB"

    const/16 v1, 0x90

    const-string v2, "application/snb"

    const-string v3, "SNB"

    invoke-static {v0, v1, v2, v3, v7}, Lcom/samsung/android/app/FileShareServer/FileTypeManager;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 393
    const-string v0, "SASF"

    const/16 v1, 0x91

    const-string v2, "application/x-sasf"

    const-string v3, "SASF"

    invoke-static {v0, v1, v2, v3, v7}, Lcom/samsung/android/app/FileShareServer/FileTypeManager;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 396
    const-string v0, "SSF"

    const/16 v1, 0x92

    const-string v2, "application/ssf"

    const-string v3, "SSF"

    invoke-static {v0, v1, v2, v3, v7}, Lcom/samsung/android/app/FileShareServer/FileTypeManager;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 399
    const-string v0, "TRC"

    const/16 v1, 0x93

    const-string v2, "application/x-toruca"

    const-string v3, "TRC"

    invoke-static {v0, v1, v2, v3, v7}, Lcom/samsung/android/app/FileShareServer/FileTypeManager;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 402
    const-string v0, "TRCS"

    const/16 v1, 0x93

    const-string v2, "application/x-storuca"

    const-string v3, "TRC"

    invoke-static {v0, v1, v2, v3, v7}, Lcom/samsung/android/app/FileShareServer/FileTypeManager;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 405
    const-string v0, "SPD"

    const/16 v1, 0x94

    const-string v2, "application/spd"

    const-string v3, "SPD"

    invoke-static {v0, v1, v2, v3, v7}, Lcom/samsung/android/app/FileShareServer/FileTypeManager;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 407
    const-string v0, "SCC"

    const/16 v1, 0x95

    const-string v2, "application/vnd.samsung.scc.storyalbum"

    const-string v3, "SCC"

    invoke-static {v0, v1, v2, v3, v7}, Lcom/samsung/android/app/FileShareServer/FileTypeManager;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 411
    const-string v0, "MEMO"

    const/16 v1, 0x96

    const-string v2, "application/vnd.samsung.android.memo"

    const-string v3, "MEMO"

    invoke-static {v0, v1, v2, v3, v5}, Lcom/samsung/android/app/FileShareServer/FileTypeManager;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 420
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 203
    return-void
.end method

.method static addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V
    .locals 2
    .param p0, "extension"    # Ljava/lang/String;
    .param p1, "fileType"    # I
    .param p2, "mimeType"    # Ljava/lang/String;
    .param p3, "desc"    # Ljava/lang/String;
    .param p4, "icon"    # I

    .prologue
    .line 227
    sget-object v0, Lcom/samsung/android/app/FileShareServer/FileTypeManager;->sFileTypeMap:Ljava/util/HashMap;

    new-instance v1, Lcom/samsung/android/app/FileShareServer/FileTypeManager$MediaFileType;

    invoke-direct {v1, p1, p2, p3, p4}, Lcom/samsung/android/app/FileShareServer/FileTypeManager$MediaFileType;-><init>(ILjava/lang/String;Ljava/lang/String;I)V

    invoke-virtual {v0, p0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 228
    sget-object v0, Lcom/samsung/android/app/FileShareServer/FileTypeManager;->sMimeTypeMap:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, p2, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 229
    sget-object v0, Lcom/samsung/android/app/FileShareServer/FileTypeManager;->sMimeType:Ljava/util/HashMap;

    invoke-virtual {v0, p0, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 230
    return-void
.end method

.method public static getDescription(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "path"    # Ljava/lang/String;

    .prologue
    .line 497
    invoke-static {p0}, Lcom/samsung/android/app/FileShareServer/FileTypeManager;->getFileType(Ljava/lang/String;)Lcom/samsung/android/app/FileShareServer/FileTypeManager$MediaFileType;

    move-result-object v0

    .line 498
    .local v0, "mediaType":Lcom/samsung/android/app/FileShareServer/FileTypeManager$MediaFileType;
    if-nez v0, :cond_0

    invoke-static {p0}, Lcom/samsung/android/app/FileShareServer/FileTypeManager;->getExtension(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    :goto_0
    return-object v1

    :cond_0
    iget-object v1, v0, Lcom/samsung/android/app/FileShareServer/FileTypeManager$MediaFileType;->description:Ljava/lang/String;

    goto :goto_0
.end method

.method public static getExtension(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p0, "path"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 502
    if-nez p0, :cond_1

    .line 507
    :cond_0
    :goto_0
    return-object v1

    .line 504
    :cond_1
    const/16 v2, 0x2e

    invoke-virtual {p0, v2}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v0

    .line 505
    .local v0, "lastDot":I
    if-ltz v0, :cond_0

    .line 507
    add-int/lit8 v1, v0, 0x1

    invoke-virtual {p0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v1, v2}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public static getFileType(Ljava/lang/String;)Lcom/samsung/android/app/FileShareServer/FileTypeManager$MediaFileType;
    .locals 3
    .param p0, "path"    # Ljava/lang/String;

    .prologue
    .line 468
    invoke-static {p0}, Lcom/samsung/android/app/FileShareServer/FileTypeManager;->getExtension(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 469
    .local v0, "ext":Ljava/lang/String;
    if-nez v0, :cond_0

    .line 470
    const/4 v1, 0x0

    .line 472
    :goto_0
    return-object v1

    .line 471
    :cond_0
    sget-object v2, Lcom/samsung/android/app/FileShareServer/FileTypeManager;->sFileTypeMap:Ljava/util/HashMap;

    invoke-virtual {v2, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/app/FileShareServer/FileTypeManager$MediaFileType;

    .line 472
    .local v1, "mediaType":Lcom/samsung/android/app/FileShareServer/FileTypeManager$MediaFileType;
    goto :goto_0
.end method

.method public static getFileTypeInt(Ljava/lang/String;)I
    .locals 2
    .param p0, "path"    # Ljava/lang/String;

    .prologue
    .line 476
    invoke-static {p0}, Lcom/samsung/android/app/FileShareServer/FileTypeManager;->getFileType(Ljava/lang/String;)Lcom/samsung/android/app/FileShareServer/FileTypeManager$MediaFileType;

    move-result-object v0

    .line 477
    .local v0, "mediaType":Lcom/samsung/android/app/FileShareServer/FileTypeManager$MediaFileType;
    if-nez v0, :cond_0

    const/4 v1, 0x0

    :goto_0
    return v1

    :cond_0
    iget v1, v0, Lcom/samsung/android/app/FileShareServer/FileTypeManager$MediaFileType;->fileType:I

    goto :goto_0
.end method

.method public static getIcon(Ljava/lang/String;)I
    .locals 2
    .param p0, "path"    # Ljava/lang/String;

    .prologue
    .line 492
    invoke-static {p0}, Lcom/samsung/android/app/FileShareServer/FileTypeManager;->getFileType(Ljava/lang/String;)Lcom/samsung/android/app/FileShareServer/FileTypeManager$MediaFileType;

    move-result-object v0

    .line 493
    .local v0, "mediaType":Lcom/samsung/android/app/FileShareServer/FileTypeManager$MediaFileType;
    if-nez v0, :cond_0

    const v1, 0x7f020001

    :goto_0
    return v1

    :cond_0
    iget v1, v0, Lcom/samsung/android/app/FileShareServer/FileTypeManager$MediaFileType;->icon:I

    goto :goto_0
.end method

.method public static getMimeType(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 481
    invoke-static {p1}, Lcom/samsung/android/app/FileShareServer/FileTypeManager;->getFileTypeInt(Ljava/lang/String;)I

    move-result v0

    .line 482
    .local v0, "drm":I
    const/16 v3, 0x57

    if-eq v0, v3, :cond_0

    const/16 v3, 0x58

    if-ne v0, v3, :cond_1

    .line 483
    :cond_0
    new-instance v1, Landroid/drm/DrmManagerClient;

    invoke-direct {v1, p0}, Landroid/drm/DrmManagerClient;-><init>(Landroid/content/Context;)V

    .line 484
    .local v1, "drmManagerClient":Landroid/drm/DrmManagerClient;
    invoke-virtual {v1, p1}, Landroid/drm/DrmManagerClient;->getOriginalMimeType(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 487
    .end local v1    # "drmManagerClient":Landroid/drm/DrmManagerClient;
    :goto_0
    return-object v3

    .line 486
    :cond_1
    invoke-static {p1}, Lcom/samsung/android/app/FileShareServer/FileTypeManager;->getFileType(Ljava/lang/String;)Lcom/samsung/android/app/FileShareServer/FileTypeManager$MediaFileType;

    move-result-object v2

    .line 487
    .local v2, "mediaType":Lcom/samsung/android/app/FileShareServer/FileTypeManager$MediaFileType;
    if-nez v2, :cond_2

    const-string v3, "application/octet-stream"

    goto :goto_0

    :cond_2
    iget-object v3, v2, Lcom/samsung/android/app/FileShareServer/FileTypeManager$MediaFileType;->mimeType:Ljava/lang/String;

    goto :goto_0
.end method

.method public static isAudioInMediaStore(Ljava/lang/String;Landroid/content/ContentResolver;)Z
    .locals 9
    .param p0, "path"    # Ljava/lang/String;
    .param p1, "cr"    # Landroid/content/ContentResolver;

    .prologue
    const/4 v2, 0x0

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 450
    sget-object v1, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    const-string v3, "_data= ? COLLATE LOCALIZED"

    new-array v4, v8, [Ljava/lang/String;

    aput-object p0, v4, v7

    move-object v0, p1

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 454
    .local v6, "c":Landroid/database/Cursor;
    if-nez v6, :cond_0

    .line 455
    const-string v0, "FileTypeManager"

    const-string v1, "isAudioInMediaStore"

    const-string v2, "Cursor is null"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/app/FileShareServer/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move v0, v7

    .line 464
    :goto_0
    return v0

    .line 458
    :cond_0
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_1

    .line 459
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    move v0, v8

    .line 460
    goto :goto_0

    .line 462
    :cond_1
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    move v0, v7

    .line 464
    goto :goto_0
.end method

.method public static isImageFileType(Ljava/lang/String;)Z
    .locals 4
    .param p0, "path"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 431
    invoke-static {p0}, Lcom/samsung/android/app/FileShareServer/FileTypeManager;->getFileType(Ljava/lang/String;)Lcom/samsung/android/app/FileShareServer/FileTypeManager$MediaFileType;

    move-result-object v1

    .line 432
    .local v1, "mediaType":Lcom/samsung/android/app/FileShareServer/FileTypeManager$MediaFileType;
    if-nez v1, :cond_1

    .line 435
    :cond_0
    :goto_0
    return v2

    .line 434
    :cond_1
    iget v0, v1, Lcom/samsung/android/app/FileShareServer/FileTypeManager$MediaFileType;->fileType:I

    .line 435
    .local v0, "fileType":I
    const/16 v3, 0x3d

    if-lt v0, v3, :cond_0

    const/16 v3, 0x41

    if-gt v0, v3, :cond_0

    const/4 v2, 0x1

    goto :goto_0
.end method

.method public static isVideoFileType(Ljava/lang/String;)Z
    .locals 4
    .param p0, "path"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 423
    invoke-static {p0}, Lcom/samsung/android/app/FileShareServer/FileTypeManager;->getFileType(Ljava/lang/String;)Lcom/samsung/android/app/FileShareServer/FileTypeManager$MediaFileType;

    move-result-object v1

    .line 424
    .local v1, "mediaType":Lcom/samsung/android/app/FileShareServer/FileTypeManager$MediaFileType;
    if-nez v1, :cond_1

    .line 427
    :cond_0
    :goto_0
    return v2

    .line 426
    :cond_1
    iget v0, v1, Lcom/samsung/android/app/FileShareServer/FileTypeManager$MediaFileType;->fileType:I

    .line 427
    .local v0, "fileType":I
    const/16 v3, 0x1f

    if-lt v0, v3, :cond_0

    const/16 v3, 0x33

    if-gt v0, v3, :cond_0

    const/4 v2, 0x1

    goto :goto_0
.end method

.method public static needToCheckMimeType(Ljava/lang/String;)Z
    .locals 3
    .param p0, "path"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 439
    invoke-static {p0}, Lcom/samsung/android/app/FileShareServer/FileTypeManager;->getExtension(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 440
    .local v0, "ext":Ljava/lang/String;
    if-nez v0, :cond_1

    .line 446
    :cond_0
    :goto_0
    return v1

    .line 442
    :cond_1
    const-string v2, "MP4"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    const-string v2, "3GP"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    const-string v2, "3G2"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    const-string v2, "ASF"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    const-string v2, "3GPP"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 444
    :cond_2
    const/4 v1, 0x1

    goto :goto_0
.end method
