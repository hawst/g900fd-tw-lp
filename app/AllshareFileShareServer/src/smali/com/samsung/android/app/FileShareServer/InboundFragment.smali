.class public Lcom/samsung/android/app/FileShareServer/InboundFragment;
.super Landroid/app/Fragment;
.source "InboundFragment.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;
.implements Lcom/samsung/android/app/FileShareServer/InboundInfo$IInboundInfoListener;


# static fields
.field private static final TAGClass:Ljava/lang/String; = "InboundFragment"


# instance fields
.field private final GONE:I

.field private final VISIBLE:I

.field mCompleteStatus:Landroid/widget/TextView;

.field private mConnection:Landroid/content/ServiceConnection;

.field private mCustomAdaptor:Lcom/samsung/android/app/FileShareServer/InboundListAdaptor;

.field private mDbAdapter:Lcom/samsung/android/app/FileShareServer/DbAdapter;

.field mDownloadCount:Landroid/widget/TextView;

.field mDownloadProgress:Landroid/widget/ProgressBar;

.field private mFileList:Landroid/widget/ListView;

.field mFileName:Landroid/widget/TextView;

.field private mInboundID:I

.field private mInboundInfo:Lcom/samsung/android/app/FileShareServer/InboundInfo;

.field private mIntent:Landroid/content/Intent;

.field private mLayout1:Landroid/widget/LinearLayout;

.field private mLayout2:Landroid/widget/LinearLayout;

.field private mMenuItem:Landroid/view/MenuItem;

.field mSenderName_1:Landroid/widget/TextView;

.field private mServerService:Lcom/samsung/android/app/FileShareServer/ServerService;

.field private mThemeContext:Landroid/content/Context;

.field mTotalSize_1:Landroid/widget/TextView;

.field mTotalSize_2:Landroid/widget/TextView;

.field mTransferInfo:Landroid/widget/TextView;

.field mTransferRate:Landroid/widget/TextView;

.field final mUiHandler:Landroid/os/Handler;

.field final mUpdateList:Ljava/lang/Runnable;

.field final mUpdateResults:Ljava/lang/Runnable;

.field private mbCompleted:Z


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 42
    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    .line 45
    iput v1, p0, Lcom/samsung/android/app/FileShareServer/InboundFragment;->VISIBLE:I

    .line 47
    const/16 v0, 0x8

    iput v0, p0, Lcom/samsung/android/app/FileShareServer/InboundFragment;->GONE:I

    .line 55
    iput-boolean v1, p0, Lcom/samsung/android/app/FileShareServer/InboundFragment;->mbCompleted:Z

    .line 65
    iput-object v2, p0, Lcom/samsung/android/app/FileShareServer/InboundFragment;->mThemeContext:Landroid/content/Context;

    .line 373
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/app/FileShareServer/InboundFragment;->mUiHandler:Landroid/os/Handler;

    .line 375
    new-instance v0, Lcom/samsung/android/app/FileShareServer/InboundFragment$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/app/FileShareServer/InboundFragment$1;-><init>(Lcom/samsung/android/app/FileShareServer/InboundFragment;)V

    iput-object v0, p0, Lcom/samsung/android/app/FileShareServer/InboundFragment;->mUpdateResults:Ljava/lang/Runnable;

    .line 383
    new-instance v0, Lcom/samsung/android/app/FileShareServer/InboundFragment$2;

    invoke-direct {v0, p0}, Lcom/samsung/android/app/FileShareServer/InboundFragment$2;-><init>(Lcom/samsung/android/app/FileShareServer/InboundFragment;)V

    iput-object v0, p0, Lcom/samsung/android/app/FileShareServer/InboundFragment;->mUpdateList:Ljava/lang/Runnable;

    .line 396
    iput-object v2, p0, Lcom/samsung/android/app/FileShareServer/InboundFragment;->mServerService:Lcom/samsung/android/app/FileShareServer/ServerService;

    .line 398
    new-instance v0, Lcom/samsung/android/app/FileShareServer/InboundFragment$3;

    invoke-direct {v0, p0}, Lcom/samsung/android/app/FileShareServer/InboundFragment$3;-><init>(Lcom/samsung/android/app/FileShareServer/InboundFragment;)V

    iput-object v0, p0, Lcom/samsung/android/app/FileShareServer/InboundFragment;->mConnection:Landroid/content/ServiceConnection;

    return-void
.end method

.method static synthetic access$000(Lcom/samsung/android/app/FileShareServer/InboundFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/FileShareServer/InboundFragment;

    .prologue
    .line 42
    invoke-direct {p0}, Lcom/samsung/android/app/FileShareServer/InboundFragment;->showStatus()V

    return-void
.end method

.method static synthetic access$100(Lcom/samsung/android/app/FileShareServer/InboundFragment;)Lcom/samsung/android/app/FileShareServer/InboundInfo;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/FileShareServer/InboundFragment;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/samsung/android/app/FileShareServer/InboundFragment;->mInboundInfo:Lcom/samsung/android/app/FileShareServer/InboundInfo;

    return-object v0
.end method

.method static synthetic access$102(Lcom/samsung/android/app/FileShareServer/InboundFragment;Lcom/samsung/android/app/FileShareServer/InboundInfo;)Lcom/samsung/android/app/FileShareServer/InboundInfo;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/FileShareServer/InboundFragment;
    .param p1, "x1"    # Lcom/samsung/android/app/FileShareServer/InboundInfo;

    .prologue
    .line 42
    iput-object p1, p0, Lcom/samsung/android/app/FileShareServer/InboundFragment;->mInboundInfo:Lcom/samsung/android/app/FileShareServer/InboundInfo;

    return-object p1
.end method

.method static synthetic access$200(Lcom/samsung/android/app/FileShareServer/InboundFragment;)Lcom/samsung/android/app/FileShareServer/InboundListAdaptor;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/FileShareServer/InboundFragment;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/samsung/android/app/FileShareServer/InboundFragment;->mCustomAdaptor:Lcom/samsung/android/app/FileShareServer/InboundListAdaptor;

    return-object v0
.end method

.method static synthetic access$202(Lcom/samsung/android/app/FileShareServer/InboundFragment;Lcom/samsung/android/app/FileShareServer/InboundListAdaptor;)Lcom/samsung/android/app/FileShareServer/InboundListAdaptor;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/FileShareServer/InboundFragment;
    .param p1, "x1"    # Lcom/samsung/android/app/FileShareServer/InboundListAdaptor;

    .prologue
    .line 42
    iput-object p1, p0, Lcom/samsung/android/app/FileShareServer/InboundFragment;->mCustomAdaptor:Lcom/samsung/android/app/FileShareServer/InboundListAdaptor;

    return-object p1
.end method

.method static synthetic access$300(Lcom/samsung/android/app/FileShareServer/InboundFragment;)Lcom/samsung/android/app/FileShareServer/ServerService;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/FileShareServer/InboundFragment;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/samsung/android/app/FileShareServer/InboundFragment;->mServerService:Lcom/samsung/android/app/FileShareServer/ServerService;

    return-object v0
.end method

.method static synthetic access$302(Lcom/samsung/android/app/FileShareServer/InboundFragment;Lcom/samsung/android/app/FileShareServer/ServerService;)Lcom/samsung/android/app/FileShareServer/ServerService;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/FileShareServer/InboundFragment;
    .param p1, "x1"    # Lcom/samsung/android/app/FileShareServer/ServerService;

    .prologue
    .line 42
    iput-object p1, p0, Lcom/samsung/android/app/FileShareServer/InboundFragment;->mServerService:Lcom/samsung/android/app/FileShareServer/ServerService;

    return-object p1
.end method

.method static synthetic access$400(Lcom/samsung/android/app/FileShareServer/InboundFragment;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/FileShareServer/InboundFragment;

    .prologue
    .line 42
    iget v0, p0, Lcom/samsung/android/app/FileShareServer/InboundFragment;->mInboundID:I

    return v0
.end method

.method static synthetic access$500(Lcom/samsung/android/app/FileShareServer/InboundFragment;I)Lcom/samsung/android/app/FileShareServer/InboundInfo;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/FileShareServer/InboundFragment;
    .param p1, "x1"    # I

    .prologue
    .line 42
    invoke-direct {p0, p1}, Lcom/samsung/android/app/FileShareServer/InboundFragment;->getInboundInfo(I)Lcom/samsung/android/app/FileShareServer/InboundInfo;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$600(Lcom/samsung/android/app/FileShareServer/InboundFragment;)Landroid/content/Intent;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/FileShareServer/InboundFragment;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/samsung/android/app/FileShareServer/InboundFragment;->mIntent:Landroid/content/Intent;

    return-object v0
.end method

.method static synthetic access$700(Lcom/samsung/android/app/FileShareServer/InboundFragment;)Landroid/widget/ListView;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/FileShareServer/InboundFragment;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/samsung/android/app/FileShareServer/InboundFragment;->mFileList:Landroid/widget/ListView;

    return-object v0
.end method

.method private bindServerService()V
    .locals 4

    .prologue
    .line 452
    const-string v1, "InboundFragment"

    const-string v2, "bindServerService()"

    const-string v3, ""

    invoke-static {v1, v2, v3}, Lcom/samsung/android/app/FileShareServer/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 453
    iget-object v1, p0, Lcom/samsung/android/app/FileShareServer/InboundFragment;->mServerService:Lcom/samsung/android/app/FileShareServer/ServerService;

    if-nez v1, :cond_0

    .line 454
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 455
    .local v0, "intent":Landroid/content/Intent;
    invoke-virtual {p0}, Lcom/samsung/android/app/FileShareServer/InboundFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/samsung/android/app/FileShareServer/ServerService;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 456
    const-string v1, "com.samsung.android.app.FileShareServer.SERVICE_START"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 457
    invoke-virtual {p0}, Lcom/samsung/android/app/FileShareServer/InboundFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/app/FileShareServer/InboundFragment;->mConnection:Landroid/content/ServiceConnection;

    const/4 v3, 0x1

    invoke-virtual {v1, v0, v2, v3}, Landroid/app/Activity;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    .line 459
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_0
    return-void
.end method

.method private getConvertSize(J)Ljava/lang/String;
    .locals 5
    .param p1, "size"    # J

    .prologue
    .line 306
    const-string v0, ""

    .line 308
    .local v0, "retSize":Ljava/lang/String;
    const-wide/16 v2, 0x0

    cmp-long v1, p1, v2

    if-gez v1, :cond_0

    .line 309
    const-string v0, "Wrong size"

    .line 313
    :goto_0
    const-string v1, "InboundFragment"

    const-string v2, "getConvertSize"

    invoke-static {v1, v2, v0}, Lcom/samsung/android/app/FileShareServer/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 314
    return-object v0

    .line 311
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/android/app/FileShareServer/InboundFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1, p1, p2}, Landroid/text/format/Formatter;->formatFileSize(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private getImageContentUri(Landroid/content/Context;Ljava/io/File;)Landroid/net/Uri;
    .locals 13
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "imageFile"    # Ljava/io/File;

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 564
    invoke-virtual {p2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v9

    .line 565
    .local v9, "filePath":Ljava/lang/String;
    const/4 v11, 0x0

    .line 566
    .local v11, "returnVal":Landroid/net/Uri;
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    new-array v2, v4, [Ljava/lang/String;

    const-string v3, "_id"

    aput-object v3, v2, v5

    const-string v3, "_data=? "

    new-array v4, v4, [Ljava/lang/String;

    aput-object v9, v4, v5

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 574
    .local v7, "cursor":Landroid/database/Cursor;
    if-eqz v7, :cond_2

    :try_start_0
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 575
    const-string v0, "_id"

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v10

    .line 576
    .local v10, "id":I
    const-string v0, "content://media/external/images/media"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    .line 577
    .local v6, "baseUri":Landroid/net/Uri;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v6, v0}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v11

    .line 588
    .end local v6    # "baseUri":Landroid/net/Uri;
    .end local v10    # "id":I
    :cond_0
    :goto_0
    if-eqz v7, :cond_1

    .line 589
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 592
    :cond_1
    :goto_1
    return-object v11

    .line 578
    :cond_2
    :try_start_1
    invoke-virtual {p2}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 579
    new-instance v12, Landroid/content/ContentValues;

    invoke-direct {v12}, Landroid/content/ContentValues;-><init>()V

    .line 580
    .local v12, "values":Landroid/content/ContentValues;
    const-string v0, "_data"

    invoke-virtual {v12, v0, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 581
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1, v12}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v11

    goto :goto_0

    .line 584
    .end local v12    # "values":Landroid/content/ContentValues;
    :catch_0
    move-exception v8

    .line 585
    .local v8, "e":Ljava/lang/Exception;
    :try_start_2
    const-string v0, "InboundFragment"

    const-string v1, "getImageContentUri"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Exception"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/samsung/android/app/FileShareServer/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 586
    invoke-virtual {v8}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 588
    if-eqz v7, :cond_1

    .line 589
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    goto :goto_1

    .line 588
    .end local v8    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v0

    if-eqz v7, :cond_3

    .line 589
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v0
.end method

.method private getInboundInfo(I)Lcom/samsung/android/app/FileShareServer/InboundInfo;
    .locals 17
    .param p1, "inboundId"    # I

    .prologue
    .line 318
    new-instance v11, Lcom/samsung/android/app/FileShareServer/InboundInfo;

    move/from16 v0, p1

    invoke-direct {v11, v0}, Lcom/samsung/android/app/FileShareServer/InboundInfo;-><init>(I)V

    .line 321
    .local v11, "retInfo":Lcom/samsung/android/app/FileShareServer/InboundInfo;
    :try_start_0
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/app/FileShareServer/InboundFragment;->mDbAdapter:Lcom/samsung/android/app/FileShareServer/DbAdapter;

    invoke-virtual {v13}, Lcom/samsung/android/app/FileShareServer/DbAdapter;->open()Lcom/samsung/android/app/FileShareServer/DbAdapter;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 326
    :goto_0
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/app/FileShareServer/InboundFragment;->mDbAdapter:Lcom/samsung/android/app/FileShareServer/DbAdapter;

    move/from16 v0, p1

    invoke-virtual {v13, v0}, Lcom/samsung/android/app/FileShareServer/DbAdapter;->queryStatusInfo(I)Landroid/database/Cursor;

    move-result-object v12

    .line 328
    .local v12, "statusCursor":Landroid/database/Cursor;
    invoke-interface {v12}, Landroid/database/Cursor;->getCount()I

    move-result v13

    if-gtz v13, :cond_0

    .line 329
    const-string v13, "InboundFragment"

    const-string v14, "getInboundInfo"

    const-string v15, "statusCursor has no result."

    invoke-static {v13, v14, v15}, Lcom/samsung/android/app/FileShareServer/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 330
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    .line 331
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/app/FileShareServer/InboundFragment;->mDbAdapter:Lcom/samsung/android/app/FileShareServer/DbAdapter;

    invoke-virtual {v13}, Lcom/samsung/android/app/FileShareServer/DbAdapter;->close()V

    .line 332
    const/4 v11, 0x0

    .line 370
    .end local v11    # "retInfo":Lcom/samsung/android/app/FileShareServer/InboundInfo;
    :goto_1
    return-object v11

    .line 322
    .end local v12    # "statusCursor":Landroid/database/Cursor;
    .restart local v11    # "retInfo":Lcom/samsung/android/app/FileShareServer/InboundInfo;
    :catch_0
    move-exception v2

    .line 323
    .local v2, "e":Ljava/lang/Exception;
    const-string v13, "InboundFragment"

    const-string v14, "getInboundInfo"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "exception "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v13, v14, v15}, Lcom/samsung/android/app/FileShareServer/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 335
    .end local v2    # "e":Ljava/lang/Exception;
    .restart local v12    # "statusCursor":Landroid/database/Cursor;
    :cond_0
    const-string v13, "sender_name"

    invoke-interface {v12, v13}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v7

    .line 336
    .local v7, "idSenderName":I
    const-string v13, "total_size"

    invoke-interface {v12, v13}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v10

    .line 337
    .local v10, "idTotalSize":I
    const-string v13, "total_count"

    invoke-interface {v12, v13}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v9

    .line 338
    .local v9, "idTotalCnt":I
    const-string v13, "fail_count"

    invoke-interface {v12, v13}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    .line 339
    .local v5, "idFailCnt":I
    const-string v13, "status"

    invoke-interface {v12, v13}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v8

    .line 341
    .local v8, "idStatus":I
    :goto_2
    invoke-interface {v12}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v13

    if-nez v13, :cond_1

    .line 342
    invoke-interface {v12, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v11, v13}, Lcom/samsung/android/app/FileShareServer/InboundInfo;->setSenderName(Ljava/lang/String;)V

    .line 343
    invoke-interface {v12, v10}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v14

    invoke-virtual {v11, v14, v15}, Lcom/samsung/android/app/FileShareServer/InboundInfo;->setTotalSize(J)V

    .line 344
    invoke-interface {v12, v9}, Landroid/database/Cursor;->getInt(I)I

    move-result v13

    invoke-virtual {v11, v13}, Lcom/samsung/android/app/FileShareServer/InboundInfo;->setTotalCount(I)V

    .line 345
    invoke-interface {v12, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v13

    invoke-virtual {v11, v13}, Lcom/samsung/android/app/FileShareServer/InboundInfo;->setTotalFailedCount(I)V

    .line 346
    invoke-virtual {v11}, Lcom/samsung/android/app/FileShareServer/InboundInfo;->getTotalCount()I

    move-result v13

    invoke-virtual {v11}, Lcom/samsung/android/app/FileShareServer/InboundInfo;->getTotalFailedCount()I

    move-result v14

    sub-int/2addr v13, v14

    invoke-virtual {v11, v13}, Lcom/samsung/android/app/FileShareServer/InboundInfo;->setTotalSuccessCount(I)V

    .line 347
    invoke-interface {v12, v8}, Landroid/database/Cursor;->getInt(I)I

    move-result v13

    invoke-virtual {v11, v13}, Lcom/samsung/android/app/FileShareServer/InboundInfo;->setCurrentStatus(I)V

    .line 348
    invoke-interface {v12}, Landroid/database/Cursor;->moveToNext()Z

    goto :goto_2

    .line 350
    :cond_1
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    .line 352
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/app/FileShareServer/InboundFragment;->mDbAdapter:Lcom/samsung/android/app/FileShareServer/DbAdapter;

    move/from16 v0, p1

    invoke-virtual {v13, v0}, Lcom/samsung/android/app/FileShareServer/DbAdapter;->queryReceivedFileInfo(I)Landroid/database/Cursor;

    move-result-object v4

    .line 354
    .local v4, "fileCursor":Landroid/database/Cursor;
    invoke-interface {v4}, Landroid/database/Cursor;->getCount()I

    move-result v13

    if-gtz v13, :cond_2

    .line 355
    const-string v13, "InboundFragment"

    const-string v14, "getInboundInfo"

    const-string v15, "fileCursor has no result."

    invoke-static {v13, v14, v15}, Lcom/samsung/android/app/FileShareServer/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 356
    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    .line 357
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/app/FileShareServer/InboundFragment;->mDbAdapter:Lcom/samsung/android/app/FileShareServer/DbAdapter;

    invoke-virtual {v13}, Lcom/samsung/android/app/FileShareServer/DbAdapter;->close()V

    .line 360
    :cond_2
    const-string v13, "file_path"

    invoke-interface {v4, v13}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    .line 362
    .local v6, "idFilePath":I
    :goto_3
    invoke-interface {v4}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v13

    if-nez v13, :cond_3

    .line 363
    new-instance v3, Ljava/io/File;

    invoke-interface {v4, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v13

    invoke-direct {v3, v13}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 364
    .local v3, "file":Ljava/io/File;
    invoke-virtual {v11, v3}, Lcom/samsung/android/app/FileShareServer/InboundInfo;->addFile(Ljava/io/File;)V

    .line 365
    invoke-interface {v4}, Landroid/database/Cursor;->moveToNext()Z

    goto :goto_3

    .line 367
    .end local v3    # "file":Ljava/io/File;
    :cond_3
    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    .line 368
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/app/FileShareServer/InboundFragment;->mDbAdapter:Lcom/samsung/android/app/FileShareServer/DbAdapter;

    invoke-virtual {v13}, Lcom/samsung/android/app/FileShareServer/DbAdapter;->close()V

    goto/16 :goto_1
.end method

.method private isContentUriMimeType(Ljava/lang/String;)Z
    .locals 1
    .param p1, "mimeType"    # Ljava/lang/String;

    .prologue
    .line 557
    const-string v0, "image/jpeg"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 558
    const/4 v0, 0x1

    .line 560
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private showStatus()V
    .locals 10

    .prologue
    const v9, 0x7f060015

    const v6, 0x7f06000a

    const/16 v3, 0x8

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 223
    iget-object v2, p0, Lcom/samsung/android/app/FileShareServer/InboundFragment;->mInboundInfo:Lcom/samsung/android/app/FileShareServer/InboundInfo;

    if-eqz v2, :cond_5

    .line 224
    iget-object v2, p0, Lcom/samsung/android/app/FileShareServer/InboundFragment;->mInboundInfo:Lcom/samsung/android/app/FileShareServer/InboundInfo;

    invoke-virtual {v2}, Lcom/samsung/android/app/FileShareServer/InboundInfo;->getCurrentStatus()I

    move-result v1

    .line 226
    .local v1, "inboundStatus":I
    packed-switch v1, :pswitch_data_0

    .line 303
    .end local v1    # "inboundStatus":I
    :cond_0
    :goto_0
    return-void

    .line 231
    .restart local v1    # "inboundStatus":I
    :pswitch_0
    iput-boolean v8, p0, Lcom/samsung/android/app/FileShareServer/InboundFragment;->mbCompleted:Z

    .line 232
    iget-object v2, p0, Lcom/samsung/android/app/FileShareServer/InboundFragment;->mLayout1:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 233
    iget-object v2, p0, Lcom/samsung/android/app/FileShareServer/InboundFragment;->mLayout2:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v7}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 235
    const/16 v2, 0x3e9

    if-eq v1, v2, :cond_1

    const/16 v2, 0x3ee

    if-ne v1, v2, :cond_2

    .line 237
    :cond_1
    iget-object v2, p0, Lcom/samsung/android/app/FileShareServer/InboundFragment;->mCompleteStatus:Landroid/widget/TextView;

    const v3, 0x7f060013

    invoke-virtual {p0, v3}, Lcom/samsung/android/app/FileShareServer/InboundFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 238
    iget-object v2, p0, Lcom/samsung/android/app/FileShareServer/InboundFragment;->mTransferInfo:Landroid/widget/TextView;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    new-array v4, v8, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/samsung/android/app/FileShareServer/InboundFragment;->mInboundInfo:Lcom/samsung/android/app/FileShareServer/InboundInfo;

    invoke-virtual {v5}, Lcom/samsung/android/app/FileShareServer/InboundInfo;->getTotalSuccessCount()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v7

    invoke-virtual {p0, v6, v4}, Lcom/samsung/android/app/FileShareServer/InboundFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " / "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const v4, 0x7f060012

    new-array v5, v8, [Ljava/lang/Object;

    iget-object v6, p0, Lcom/samsung/android/app/FileShareServer/InboundFragment;->mInboundInfo:Lcom/samsung/android/app/FileShareServer/InboundInfo;

    invoke-virtual {v6}, Lcom/samsung/android/app/FileShareServer/InboundInfo;->getTotalFailedCount()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-virtual {p0, v4, v5}, Lcom/samsung/android/app/FileShareServer/InboundFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 263
    :goto_1
    iget-object v2, p0, Lcom/samsung/android/app/FileShareServer/InboundFragment;->mTotalSize_2:Landroid/widget/TextView;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p0, v9}, Lcom/samsung/android/app/FileShareServer/InboundFragment;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/android/app/FileShareServer/InboundFragment;->mInboundInfo:Lcom/samsung/android/app/FileShareServer/InboundInfo;

    invoke-virtual {v4}, Lcom/samsung/android/app/FileShareServer/InboundInfo;->getTotalSize()J

    move-result-wide v4

    invoke-direct {p0, v4, v5}, Lcom/samsung/android/app/FileShareServer/InboundFragment;->getConvertSize(J)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 265
    iget-object v2, p0, Lcom/samsung/android/app/FileShareServer/InboundFragment;->mMenuItem:Landroid/view/MenuItem;

    if-eqz v2, :cond_0

    .line 266
    iget-object v2, p0, Lcom/samsung/android/app/FileShareServer/InboundFragment;->mMenuItem:Landroid/view/MenuItem;

    invoke-interface {v2, v7}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto/16 :goto_0

    .line 244
    :cond_2
    const/16 v2, 0x3eb

    if-ne v1, v2, :cond_3

    .line 245
    iget-object v2, p0, Lcom/samsung/android/app/FileShareServer/InboundFragment;->mCompleteStatus:Landroid/widget/TextView;

    const v3, 0x7f060009

    invoke-virtual {p0, v3}, Lcom/samsung/android/app/FileShareServer/InboundFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 246
    iget-object v2, p0, Lcom/samsung/android/app/FileShareServer/InboundFragment;->mTransferInfo:Landroid/widget/TextView;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    new-array v4, v8, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/samsung/android/app/FileShareServer/InboundFragment;->mInboundInfo:Lcom/samsung/android/app/FileShareServer/InboundInfo;

    invoke-virtual {v5}, Lcom/samsung/android/app/FileShareServer/InboundInfo;->getTotalSuccessCount()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v7

    invoke-virtual {p0, v6, v4}, Lcom/samsung/android/app/FileShareServer/InboundFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " / "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const v4, 0x7f06000b

    new-array v5, v8, [Ljava/lang/Object;

    iget-object v6, p0, Lcom/samsung/android/app/FileShareServer/InboundFragment;->mInboundInfo:Lcom/samsung/android/app/FileShareServer/InboundInfo;

    invoke-virtual {v6}, Lcom/samsung/android/app/FileShareServer/InboundInfo;->getTotalFailedCount()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-virtual {p0, v4, v5}, Lcom/samsung/android/app/FileShareServer/InboundFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 252
    :cond_3
    iget-object v2, p0, Lcom/samsung/android/app/FileShareServer/InboundFragment;->mInboundInfo:Lcom/samsung/android/app/FileShareServer/InboundInfo;

    invoke-virtual {v2}, Lcom/samsung/android/app/FileShareServer/InboundInfo;->getTotalCount()I

    move-result v2

    if-le v2, v8, :cond_4

    .line 253
    iget-object v2, p0, Lcom/samsung/android/app/FileShareServer/InboundFragment;->mCompleteStatus:Landroid/widget/TextView;

    const v3, 0x7f060018

    new-array v4, v8, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/samsung/android/app/FileShareServer/InboundFragment;->mInboundInfo:Lcom/samsung/android/app/FileShareServer/InboundInfo;

    invoke-virtual {v5}, Lcom/samsung/android/app/FileShareServer/InboundInfo;->getSenderName()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v7

    invoke-virtual {p0, v3, v4}, Lcom/samsung/android/app/FileShareServer/InboundFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 259
    :goto_2
    iget-object v2, p0, Lcom/samsung/android/app/FileShareServer/InboundFragment;->mTransferInfo:Landroid/widget/TextView;

    new-array v3, v8, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/samsung/android/app/FileShareServer/InboundFragment;->mInboundInfo:Lcom/samsung/android/app/FileShareServer/InboundInfo;

    invoke-virtual {v4}, Lcom/samsung/android/app/FileShareServer/InboundInfo;->getTotalSuccessCount()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v7

    invoke-virtual {p0, v6, v3}, Lcom/samsung/android/app/FileShareServer/InboundFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 256
    :cond_4
    iget-object v2, p0, Lcom/samsung/android/app/FileShareServer/InboundFragment;->mCompleteStatus:Landroid/widget/TextView;

    const v3, 0x7f060019

    new-array v4, v8, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/samsung/android/app/FileShareServer/InboundFragment;->mInboundInfo:Lcom/samsung/android/app/FileShareServer/InboundInfo;

    invoke-virtual {v5}, Lcom/samsung/android/app/FileShareServer/InboundInfo;->getSenderName()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v7

    invoke-virtual {p0, v3, v4}, Lcom/samsung/android/app/FileShareServer/InboundFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_2

    .line 273
    :pswitch_1
    const/4 v0, 0x0

    .line 275
    .local v0, "iDataRate":I
    iput-boolean v7, p0, Lcom/samsung/android/app/FileShareServer/InboundFragment;->mbCompleted:Z

    .line 276
    iget-object v2, p0, Lcom/samsung/android/app/FileShareServer/InboundFragment;->mLayout1:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v7}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 277
    iget-object v2, p0, Lcom/samsung/android/app/FileShareServer/InboundFragment;->mLayout2:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 279
    iget-object v2, p0, Lcom/samsung/android/app/FileShareServer/InboundFragment;->mInboundInfo:Lcom/samsung/android/app/FileShareServer/InboundInfo;

    invoke-virtual {v2}, Lcom/samsung/android/app/FileShareServer/InboundInfo;->getCurrentSize()J

    move-result-wide v2

    const-wide/16 v4, 0x64

    mul-long/2addr v2, v4

    iget-object v4, p0, Lcom/samsung/android/app/FileShareServer/InboundFragment;->mInboundInfo:Lcom/samsung/android/app/FileShareServer/InboundInfo;

    invoke-virtual {v4}, Lcom/samsung/android/app/FileShareServer/InboundInfo;->getTotalSize()J

    move-result-wide v4

    div-long/2addr v2, v4

    long-to-int v0, v2

    .line 281
    iget-object v2, p0, Lcom/samsung/android/app/FileShareServer/InboundFragment;->mDownloadProgress:Landroid/widget/ProgressBar;

    invoke-virtual {v2, v0}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 282
    iget-object v2, p0, Lcom/samsung/android/app/FileShareServer/InboundFragment;->mTransferRate:Landroid/widget/TextView;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "%d"

    new-array v5, v8, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "%"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 283
    iget-object v2, p0, Lcom/samsung/android/app/FileShareServer/InboundFragment;->mSenderName_1:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/samsung/android/app/FileShareServer/InboundFragment;->mInboundInfo:Lcom/samsung/android/app/FileShareServer/InboundInfo;

    invoke-virtual {v3}, Lcom/samsung/android/app/FileShareServer/InboundInfo;->getSenderName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 284
    iget-object v2, p0, Lcom/samsung/android/app/FileShareServer/InboundFragment;->mDownloadCount:Landroid/widget/TextView;

    const-string v3, "%d/%d"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/samsung/android/app/FileShareServer/InboundFragment;->mInboundInfo:Lcom/samsung/android/app/FileShareServer/InboundInfo;

    invoke-virtual {v5}, Lcom/samsung/android/app/FileShareServer/InboundInfo;->getCurrentIndex()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v7

    iget-object v5, p0, Lcom/samsung/android/app/FileShareServer/InboundFragment;->mInboundInfo:Lcom/samsung/android/app/FileShareServer/InboundInfo;

    invoke-virtual {v5}, Lcom/samsung/android/app/FileShareServer/InboundInfo;->getTotalCount()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v8

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 286
    iget-object v2, p0, Lcom/samsung/android/app/FileShareServer/InboundFragment;->mTotalSize_1:Landroid/widget/TextView;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p0, v9}, Lcom/samsung/android/app/FileShareServer/InboundFragment;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/android/app/FileShareServer/InboundFragment;->mInboundInfo:Lcom/samsung/android/app/FileShareServer/InboundInfo;

    invoke-virtual {v4}, Lcom/samsung/android/app/FileShareServer/InboundInfo;->getTotalSize()J

    move-result-wide v4

    invoke-direct {p0, v4, v5}, Lcom/samsung/android/app/FileShareServer/InboundFragment;->getConvertSize(J)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 288
    iget-object v2, p0, Lcom/samsung/android/app/FileShareServer/InboundFragment;->mFileName:Landroid/widget/TextView;

    const v3, 0x7f06000d

    new-array v4, v8, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/samsung/android/app/FileShareServer/InboundFragment;->mInboundInfo:Lcom/samsung/android/app/FileShareServer/InboundInfo;

    invoke-virtual {v5}, Lcom/samsung/android/app/FileShareServer/InboundInfo;->getFileName()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v7

    invoke-virtual {p0, v3, v4}, Lcom/samsung/android/app/FileShareServer/InboundFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 291
    iget-object v2, p0, Lcom/samsung/android/app/FileShareServer/InboundFragment;->mMenuItem:Landroid/view/MenuItem;

    if-eqz v2, :cond_0

    .line 292
    iget-object v2, p0, Lcom/samsung/android/app/FileShareServer/InboundFragment;->mMenuItem:Landroid/view/MenuItem;

    invoke-interface {v2, v8}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 293
    iget-object v2, p0, Lcom/samsung/android/app/FileShareServer/InboundFragment;->mMenuItem:Landroid/view/MenuItem;

    invoke-interface {v2, v8}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    goto/16 :goto_0

    .line 300
    .end local v0    # "iDataRate":I
    .end local v1    # "inboundStatus":I
    :cond_5
    iget-object v2, p0, Lcom/samsung/android/app/FileShareServer/InboundFragment;->mLayout1:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 301
    iget-object v2, p0, Lcom/samsung/android/app/FileShareServer/InboundFragment;->mLayout2:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v7}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto/16 :goto_0

    .line 226
    nop

    :pswitch_data_0
    .packed-switch 0x3e8
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private unbindServerService()V
    .locals 3

    .prologue
    .line 462
    const-string v0, "InboundFragment"

    const-string v1, "unbindServerService()"

    const-string v2, ""

    invoke-static {v0, v1, v2}, Lcom/samsung/android/app/FileShareServer/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 463
    invoke-virtual {p0}, Lcom/samsung/android/app/FileShareServer/InboundFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/app/FileShareServer/InboundFragment;->mConnection:Landroid/content/ServiceConnection;

    invoke-virtual {v0, v1}, Landroid/app/Activity;->unbindService(Landroid/content/ServiceConnection;)V

    .line 464
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/app/FileShareServer/InboundFragment;->mServerService:Lcom/samsung/android/app/FileShareServer/ServerService;

    .line 465
    return-void
.end method


# virtual methods
.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const v2, 0x7f080008

    .line 79
    invoke-super {p0, p1}, Landroid/app/Fragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 80
    new-instance v0, Lcom/samsung/android/app/FileShareServer/DbAdapter;

    invoke-virtual {p0}, Lcom/samsung/android/app/FileShareServer/InboundFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/samsung/android/app/FileShareServer/DbAdapter;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/app/FileShareServer/InboundFragment;->mDbAdapter:Lcom/samsung/android/app/FileShareServer/DbAdapter;

    .line 82
    invoke-virtual {p0}, Lcom/samsung/android/app/FileShareServer/InboundFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const v1, 0x7f080001

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/samsung/android/app/FileShareServer/InboundFragment;->mLayout1:Landroid/widget/LinearLayout;

    .line 83
    invoke-virtual {p0}, Lcom/samsung/android/app/FileShareServer/InboundFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const v1, 0x7f080002

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/samsung/android/app/FileShareServer/InboundFragment;->mLayout2:Landroid/widget/LinearLayout;

    .line 85
    iget-object v0, p0, Lcom/samsung/android/app/FileShareServer/InboundFragment;->mLayout1:Landroid/widget/LinearLayout;

    const v1, 0x7f08000a

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/samsung/android/app/FileShareServer/InboundFragment;->mSenderName_1:Landroid/widget/TextView;

    .line 86
    iget-object v0, p0, Lcom/samsung/android/app/FileShareServer/InboundFragment;->mLayout1:Landroid/widget/LinearLayout;

    const v1, 0x7f08000f

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/samsung/android/app/FileShareServer/InboundFragment;->mDownloadCount:Landroid/widget/TextView;

    .line 87
    iget-object v0, p0, Lcom/samsung/android/app/FileShareServer/InboundFragment;->mLayout1:Landroid/widget/LinearLayout;

    const v1, 0x7f08000c

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/samsung/android/app/FileShareServer/InboundFragment;->mFileName:Landroid/widget/TextView;

    .line 88
    iget-object v0, p0, Lcom/samsung/android/app/FileShareServer/InboundFragment;->mLayout1:Landroid/widget/LinearLayout;

    const v1, 0x7f08000d

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/samsung/android/app/FileShareServer/InboundFragment;->mDownloadProgress:Landroid/widget/ProgressBar;

    .line 89
    iget-object v0, p0, Lcom/samsung/android/app/FileShareServer/InboundFragment;->mLayout1:Landroid/widget/LinearLayout;

    const v1, 0x7f080010

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/samsung/android/app/FileShareServer/InboundFragment;->mTransferRate:Landroid/widget/TextView;

    .line 90
    iget-object v0, p0, Lcom/samsung/android/app/FileShareServer/InboundFragment;->mLayout1:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/samsung/android/app/FileShareServer/InboundFragment;->mTotalSize_1:Landroid/widget/TextView;

    .line 92
    iget-object v0, p0, Lcom/samsung/android/app/FileShareServer/InboundFragment;->mLayout2:Landroid/widget/LinearLayout;

    const v1, 0x7f080006

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/samsung/android/app/FileShareServer/InboundFragment;->mCompleteStatus:Landroid/widget/TextView;

    .line 93
    iget-object v0, p0, Lcom/samsung/android/app/FileShareServer/InboundFragment;->mLayout2:Landroid/widget/LinearLayout;

    const v1, 0x7f080007

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/samsung/android/app/FileShareServer/InboundFragment;->mTransferInfo:Landroid/widget/TextView;

    .line 94
    iget-object v0, p0, Lcom/samsung/android/app/FileShareServer/InboundFragment;->mLayout2:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/samsung/android/app/FileShareServer/InboundFragment;->mTotalSize_2:Landroid/widget/TextView;

    .line 96
    invoke-virtual {p0}, Lcom/samsung/android/app/FileShareServer/InboundFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const v1, 0x7f080004

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/samsung/android/app/FileShareServer/InboundFragment;->mFileList:Landroid/widget/ListView;

    .line 97
    iget-object v0, p0, Lcom/samsung/android/app/FileShareServer/InboundFragment;->mFileList:Landroid/widget/ListView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setFocusable(Z)V

    .line 98
    return-void
.end method

.method public onBackPressed()V
    .locals 3

    .prologue
    .line 177
    const-string v0, "InboundFragment"

    const-string v1, "onBackPressed()"

    const-string v2, ""

    invoke-static {v0, v1, v2}, Lcom/samsung/android/app/FileShareServer/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 178
    iget-object v0, p0, Lcom/samsung/android/app/FileShareServer/InboundFragment;->mInboundInfo:Lcom/samsung/android/app/FileShareServer/InboundInfo;

    if-eqz v0, :cond_0

    .line 179
    iget-object v0, p0, Lcom/samsung/android/app/FileShareServer/InboundFragment;->mInboundInfo:Lcom/samsung/android/app/FileShareServer/InboundInfo;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/FileShareServer/InboundInfo;->setListener(Lcom/samsung/android/app/FileShareServer/InboundInfo$IInboundInfoListener;)V

    .line 181
    :cond_0
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 3
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    const/4 v2, 0x1

    .line 185
    const-string v1, "ro.build.characteristics"

    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 186
    .local v0, "deviceType":Ljava/lang/String;
    iget v1, p1, Landroid/content/res/Configuration;->orientation:I

    if-eq v1, v2, :cond_0

    const-string v1, "tablet"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-ne v1, v2, :cond_1

    .line 188
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/app/FileShareServer/InboundFragment;->mMenuItem:Landroid/view/MenuItem;

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;

    .line 191
    :cond_1
    invoke-super {p0, p1}, Landroid/app/Fragment;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 192
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 3
    .param p1, "menu"    # Landroid/view/Menu;
    .param p2, "inflater"    # Landroid/view/MenuInflater;

    .prologue
    .line 196
    invoke-super {p0, p1, p2}, Landroid/app/Fragment;->onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V

    .line 197
    const-string v1, "ro.build.characteristics"

    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 198
    .local v0, "deviceType":Ljava/lang/String;
    const v1, 0x7f060008

    invoke-interface {p1, v1}, Landroid/view/Menu;->add(I)Landroid/view/MenuItem;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/app/FileShareServer/InboundFragment;->mMenuItem:Landroid/view/MenuItem;

    .line 203
    iget-object v1, p0, Lcom/samsung/android/app/FileShareServer/InboundFragment;->mMenuItem:Landroid/view/MenuItem;

    const/4 v2, 0x5

    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setShowAsAction(I)V

    .line 205
    iget-object v2, p0, Lcom/samsung/android/app/FileShareServer/InboundFragment;->mMenuItem:Landroid/view/MenuItem;

    iget-boolean v1, p0, Lcom/samsung/android/app/FileShareServer/InboundFragment;->mbCompleted:Z

    if-nez v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    invoke-interface {v2, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 206
    return-void

    .line 205
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 102
    const/high16 v0, 0x7f030000

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public onDestroy()V
    .locals 3

    .prologue
    .line 168
    const-string v0, "InboundFragment"

    const-string v1, "onDestroy()"

    const-string v2, ""

    invoke-static {v0, v1, v2}, Lcom/samsung/android/app/FileShareServer/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 169
    invoke-super {p0}, Landroid/app/Fragment;->onDestroy()V

    .line 170
    iget-object v0, p0, Lcom/samsung/android/app/FileShareServer/InboundFragment;->mInboundInfo:Lcom/samsung/android/app/FileShareServer/InboundInfo;

    if-eqz v0, :cond_0

    .line 171
    iget-object v0, p0, Lcom/samsung/android/app/FileShareServer/InboundFragment;->mInboundInfo:Lcom/samsung/android/app/FileShareServer/InboundInfo;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/FileShareServer/InboundInfo;->setListener(Lcom/samsung/android/app/FileShareServer/InboundInfo$IInboundInfoListener;)V

    .line 173
    :cond_0
    invoke-direct {p0}, Lcom/samsung/android/app/FileShareServer/InboundFragment;->unbindServerService()V

    .line 174
    return-void
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 10
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 508
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    :try_start_0
    iget-object v6, p0, Lcom/samsung/android/app/FileShareServer/InboundFragment;->mInboundInfo:Lcom/samsung/android/app/FileShareServer/InboundInfo;

    invoke-virtual {v6}, Lcom/samsung/android/app/FileShareServer/InboundInfo;->getFileList()Ljava/util/ArrayList;

    move-result-object v2

    .line 509
    .local v2, "fileList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/io/File;>;"
    invoke-virtual {v2, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/io/File;

    .line 511
    .local v1, "file":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v6

    if-nez v6, :cond_1

    .line 512
    new-instance v6, Landroid/view/ContextThemeWrapper;

    invoke-virtual {p0}, Lcom/samsung/android/app/FileShareServer/InboundFragment;->getActivity()Landroid/app/Activity;

    move-result-object v7

    const/high16 v8, 0x7f070000

    invoke-direct {v6, v7, v8}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    iput-object v6, p0, Lcom/samsung/android/app/FileShareServer/InboundFragment;->mThemeContext:Landroid/content/Context;

    .line 514
    iget-object v6, p0, Lcom/samsung/android/app/FileShareServer/InboundFragment;->mThemeContext:Landroid/content/Context;

    const v7, 0x7f060014

    const/4 v8, 0x0

    invoke-static {v6, v7, v8}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v6

    invoke-virtual {v6}, Landroid/widget/Toast;->show()V

    .line 554
    .end local v1    # "file":Ljava/io/File;
    .end local v2    # "fileList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/io/File;>;"
    :cond_0
    :goto_0
    return-void

    .line 519
    .restart local v1    # "file":Ljava/io/File;
    .restart local v2    # "fileList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/io/File;>;"
    :cond_1
    invoke-virtual {v1}, Ljava/io/File;->isFile()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 520
    invoke-virtual {p0}, Lcom/samsung/android/app/FileShareServer/InboundFragment;->getActivity()Landroid/app/Activity;

    move-result-object v6

    invoke-virtual {v6}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/samsung/android/app/FileShareServer/FileTypeManager;->getMimeType(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 522
    .local v4, "mimeType":Ljava/lang/String;
    const-string v6, "InboundFragment"

    const-string v7, "onItemClick"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "mime type = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v7, v8}, Lcom/samsung/android/app/FileShareServer/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 523
    new-instance v3, Landroid/content/Intent;

    const-string v6, "android.intent.action.VIEW"

    invoke-direct {v3, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 524
    .local v3, "intent":Landroid/content/Intent;
    const/high16 v6, 0x10000000

    invoke-virtual {v3, v6}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 526
    invoke-direct {p0, v4}, Lcom/samsung/android/app/FileShareServer/InboundFragment;->isContentUriMimeType(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 527
    invoke-virtual {p0}, Lcom/samsung/android/app/FileShareServer/InboundFragment;->getActivity()Landroid/app/Activity;

    move-result-object v6

    invoke-virtual {v6}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v6

    invoke-direct {p0, v6, v1}, Lcom/samsung/android/app/FileShareServer/InboundFragment;->getImageContentUri(Landroid/content/Context;Ljava/io/File;)Landroid/net/Uri;

    move-result-object v5

    .line 529
    .local v5, "uri":Landroid/net/Uri;
    if-eqz v5, :cond_2

    .line 530
    const-string v6, "InboundFragment"

    const-string v7, "onItemClick()"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "send by content uri:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v7, v8}, Lcom/samsung/android/app/FileShareServer/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 531
    invoke-virtual {v3, v5}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 532
    invoke-virtual {v3, v5, v4}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 544
    .end local v5    # "uri":Landroid/net/Uri;
    :goto_1
    :try_start_1
    invoke-virtual {p0}, Lcom/samsung/android/app/FileShareServer/InboundFragment;->getActivity()Landroid/app/Activity;

    move-result-object v6

    invoke-virtual {v6, v3}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V
    :try_end_1
    .catch Landroid/content/ActivityNotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 545
    :catch_0
    move-exception v0

    .line 546
    .local v0, "e":Landroid/content/ActivityNotFoundException;
    :try_start_2
    invoke-virtual {v0}, Landroid/content/ActivityNotFoundException;->printStackTrace()V

    .line 547
    invoke-virtual {p0}, Lcom/samsung/android/app/FileShareServer/InboundFragment;->getActivity()Landroid/app/Activity;

    move-result-object v6

    const v7, 0x7f060007

    invoke-virtual {p0, v7}, Lcom/samsung/android/app/FileShareServer/InboundFragment;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-static {v3, v7}, Landroid/content/Intent;->createChooser(Landroid/content/Intent;Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    goto/16 :goto_0

    .line 551
    .end local v0    # "e":Landroid/content/ActivityNotFoundException;
    .end local v1    # "file":Ljava/io/File;
    .end local v2    # "fileList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/io/File;>;"
    .end local v3    # "intent":Landroid/content/Intent;
    .end local v4    # "mimeType":Ljava/lang/String;
    :catch_1
    move-exception v0

    .line 552
    .local v0, "e":Ljava/lang/Exception;
    const-string v6, "InboundFragment"

    const-string v7, "onItemClick()"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Exception on itemClick"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v7, v8}, Lcom/samsung/android/app/FileShareServer/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 534
    .end local v0    # "e":Ljava/lang/Exception;
    .restart local v1    # "file":Ljava/io/File;
    .restart local v2    # "fileList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/io/File;>;"
    .restart local v3    # "intent":Landroid/content/Intent;
    .restart local v4    # "mimeType":Ljava/lang/String;
    .restart local v5    # "uri":Landroid/net/Uri;
    :cond_2
    :try_start_3
    const-string v6, "InboundFragment"

    const-string v7, "onItemClick()"

    const-string v8, "content uri FAILED:"

    invoke-static {v6, v7, v8}, Lcom/samsung/android/app/FileShareServer/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 535
    invoke-virtual {v1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    invoke-virtual {v3, v6}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 536
    invoke-static {v1}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v6

    invoke-virtual {v3, v6, v4}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_1

    .line 539
    .end local v5    # "uri":Landroid/net/Uri;
    :cond_3
    invoke-virtual {v1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    invoke-virtual {v3, v6}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 540
    invoke-static {v1}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v6

    invoke-virtual {v3, v6, v4}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_1
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 3
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 210
    iget-object v1, p0, Lcom/samsung/android/app/FileShareServer/InboundFragment;->mMenuItem:Landroid/view/MenuItem;

    if-ne p1, v1, :cond_0

    .line 212
    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/app/FileShareServer/InboundFragment;->mServerService:Lcom/samsung/android/app/FileShareServer/ServerService;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/android/app/FileShareServer/InboundFragment;->mInboundInfo:Lcom/samsung/android/app/FileShareServer/InboundInfo;

    if-eqz v1, :cond_0

    .line 213
    iget-object v1, p0, Lcom/samsung/android/app/FileShareServer/InboundFragment;->mServerService:Lcom/samsung/android/app/FileShareServer/ServerService;

    iget-object v2, p0, Lcom/samsung/android/app/FileShareServer/InboundFragment;->mInboundInfo:Lcom/samsung/android/app/FileShareServer/InboundInfo;

    invoke-virtual {v2}, Lcom/samsung/android/app/FileShareServer/InboundInfo;->getSessionID()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/samsung/android/app/FileShareServer/ServerService;->cancelSession(Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 219
    :cond_0
    :goto_0
    invoke-super {p0, p1}, Landroid/app/Fragment;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v1

    return v1

    .line 215
    :catch_0
    move-exception v0

    .line 216
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public onResume()V
    .locals 6

    .prologue
    .line 107
    invoke-super {p0}, Landroid/app/Fragment;->onResume()V

    .line 108
    const/4 v0, 0x0

    .line 110
    .local v0, "bDeleteNotification":Z
    const/4 v2, 0x1

    invoke-virtual {p0, v2}, Lcom/samsung/android/app/FileShareServer/InboundFragment;->setHasOptionsMenu(Z)V

    .line 111
    iget-object v2, p0, Lcom/samsung/android/app/FileShareServer/InboundFragment;->mIntent:Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/samsung/android/app/FileShareServer/InboundFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    if-eq v2, v3, :cond_0

    .line 112
    invoke-virtual {p0}, Lcom/samsung/android/app/FileShareServer/InboundFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/android/app/FileShareServer/InboundFragment;->mIntent:Landroid/content/Intent;

    .line 113
    iget-object v2, p0, Lcom/samsung/android/app/FileShareServer/InboundFragment;->mServerService:Lcom/samsung/android/app/FileShareServer/ServerService;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/samsung/android/app/FileShareServer/InboundFragment;->mIntent:Landroid/content/Intent;

    invoke-virtual {v2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    const-string v3, "com.samsung.android.app.FileShareServer.RECEIVE_RESULT"

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 115
    const-string v2, "InboundFragment"

    const-string v3, "onResume"

    const-string v4, "mServerService != null && mIntent.getAction().contains(InboundInfo.RECEIVE_RESULT"

    invoke-static {v2, v3, v4}, Lcom/samsung/android/app/FileShareServer/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 119
    :cond_0
    iget-object v2, p0, Lcom/samsung/android/app/FileShareServer/InboundFragment;->mIntent:Landroid/content/Intent;

    const-string v3, "inboundId"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 120
    .local v1, "inboundID":I
    const-string v2, "InboundFragment"

    const-string v3, "onResume"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "get["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "], mInboundID["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/samsung/android/app/FileShareServer/InboundFragment;->mInboundID:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "]"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/samsung/android/app/FileShareServer/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 121
    iget v2, p0, Lcom/samsung/android/app/FileShareServer/InboundFragment;->mInboundID:I

    if-eq v1, v2, :cond_5

    .line 122
    iput v1, p0, Lcom/samsung/android/app/FileShareServer/InboundFragment;->mInboundID:I

    .line 123
    iget-object v2, p0, Lcom/samsung/android/app/FileShareServer/InboundFragment;->mServerService:Lcom/samsung/android/app/FileShareServer/ServerService;

    if-eqz v2, :cond_6

    .line 125
    iget-object v2, p0, Lcom/samsung/android/app/FileShareServer/InboundFragment;->mInboundInfo:Lcom/samsung/android/app/FileShareServer/InboundInfo;

    if-eqz v2, :cond_1

    .line 126
    iget-object v2, p0, Lcom/samsung/android/app/FileShareServer/InboundFragment;->mInboundInfo:Lcom/samsung/android/app/FileShareServer/InboundInfo;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/samsung/android/app/FileShareServer/InboundInfo;->setListener(Lcom/samsung/android/app/FileShareServer/InboundInfo$IInboundInfoListener;)V

    .line 128
    :cond_1
    const-string v2, "InboundFragment"

    const-string v3, "onResume"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "get["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "], mInboundID["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/samsung/android/app/FileShareServer/InboundFragment;->mIntent:Landroid/content/Intent;

    invoke-virtual {v5}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "]"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/samsung/android/app/FileShareServer/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 132
    iget-object v2, p0, Lcom/samsung/android/app/FileShareServer/InboundFragment;->mIntent:Landroid/content/Intent;

    invoke-virtual {v2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    const-string v3, "com.samsung.android.app.FileShareServer.RECEIVE_RESULT"

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 133
    const-string v2, "InboundFragment"

    const-string v3, "onResume"

    const-string v4, "========== InboundInfo.RECEIVE_RESULT ========="

    invoke-static {v2, v3, v4}, Lcom/samsung/android/app/FileShareServer/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 134
    iget v2, p0, Lcom/samsung/android/app/FileShareServer/InboundFragment;->mInboundID:I

    invoke-direct {p0, v2}, Lcom/samsung/android/app/FileShareServer/InboundFragment;->getInboundInfo(I)Lcom/samsung/android/app/FileShareServer/InboundInfo;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/android/app/FileShareServer/InboundFragment;->mInboundInfo:Lcom/samsung/android/app/FileShareServer/InboundInfo;

    .line 140
    :goto_0
    iget-object v2, p0, Lcom/samsung/android/app/FileShareServer/InboundFragment;->mInboundInfo:Lcom/samsung/android/app/FileShareServer/InboundInfo;

    if-nez v2, :cond_4

    .line 141
    const-string v2, "InboundFragment"

    const-string v3, "onResume"

    const-string v4, "mInboundInfo is null!"

    invoke-static {v2, v3, v4}, Lcom/samsung/android/app/FileShareServer/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 142
    invoke-virtual {p0}, Lcom/samsung/android/app/FileShareServer/InboundFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->finish()V

    .line 163
    :cond_2
    :goto_1
    return-void

    .line 136
    :cond_3
    const-string v2, "InboundFragment"

    const-string v3, "onResume"

    const-string v4, "========== InboundInfo.RECEIVE_RESULT else ========="

    invoke-static {v2, v3, v4}, Lcom/samsung/android/app/FileShareServer/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 138
    iget-object v2, p0, Lcom/samsung/android/app/FileShareServer/InboundFragment;->mServerService:Lcom/samsung/android/app/FileShareServer/ServerService;

    iget v3, p0, Lcom/samsung/android/app/FileShareServer/InboundFragment;->mInboundID:I

    invoke-virtual {v2, v3}, Lcom/samsung/android/app/FileShareServer/ServerService;->getInboundInfo(I)Lcom/samsung/android/app/FileShareServer/InboundInfo;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/android/app/FileShareServer/InboundFragment;->mInboundInfo:Lcom/samsung/android/app/FileShareServer/InboundInfo;

    goto :goto_0

    .line 145
    :cond_4
    new-instance v2, Lcom/samsung/android/app/FileShareServer/InboundListAdaptor;

    invoke-virtual {p0}, Lcom/samsung/android/app/FileShareServer/InboundFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/android/app/FileShareServer/InboundFragment;->mInboundInfo:Lcom/samsung/android/app/FileShareServer/InboundInfo;

    invoke-direct {v2, v3, v4}, Lcom/samsung/android/app/FileShareServer/InboundListAdaptor;-><init>(Landroid/content/Context;Lcom/samsung/android/app/FileShareServer/InboundInfo;)V

    iput-object v2, p0, Lcom/samsung/android/app/FileShareServer/InboundFragment;->mCustomAdaptor:Lcom/samsung/android/app/FileShareServer/InboundListAdaptor;

    .line 146
    iget-object v2, p0, Lcom/samsung/android/app/FileShareServer/InboundFragment;->mFileList:Landroid/widget/ListView;

    iget-object v3, p0, Lcom/samsung/android/app/FileShareServer/InboundFragment;->mCustomAdaptor:Lcom/samsung/android/app/FileShareServer/InboundListAdaptor;

    invoke-virtual {v2, v3}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 147
    iget-object v2, p0, Lcom/samsung/android/app/FileShareServer/InboundFragment;->mFileList:Landroid/widget/ListView;

    invoke-virtual {v2, p0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 148
    iget-object v2, p0, Lcom/samsung/android/app/FileShareServer/InboundFragment;->mInboundInfo:Lcom/samsung/android/app/FileShareServer/InboundInfo;

    invoke-virtual {v2, p0}, Lcom/samsung/android/app/FileShareServer/InboundInfo;->setListener(Lcom/samsung/android/app/FileShareServer/InboundInfo$IInboundInfoListener;)V

    .line 150
    invoke-direct {p0}, Lcom/samsung/android/app/FileShareServer/InboundFragment;->showStatus()V

    .line 158
    :cond_5
    if-eqz v0, :cond_2

    .line 159
    iget-object v2, p0, Lcom/samsung/android/app/FileShareServer/InboundFragment;->mServerService:Lcom/samsung/android/app/FileShareServer/ServerService;

    invoke-virtual {v2}, Lcom/samsung/android/app/FileShareServer/ServerService;->deletedNotification()V

    .line 160
    const/4 v0, 0x0

    goto :goto_1

    .line 152
    :cond_6
    invoke-direct {p0}, Lcom/samsung/android/app/FileShareServer/InboundFragment;->bindServerService()V

    .line 153
    const/4 v0, 0x0

    .line 154
    goto :goto_1
.end method

.method public updateFileList()V
    .locals 2

    .prologue
    .line 490
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/samsung/android/app/FileShareServer/InboundFragment$5;

    invoke-direct {v1, p0}, Lcom/samsung/android/app/FileShareServer/InboundFragment$5;-><init>(Lcom/samsung/android/app/FileShareServer/InboundFragment;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 497
    .local v0, "mUiThread":Ljava/lang/Thread;
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 498
    return-void
.end method

.method public updateProgress(Lcom/samsung/android/app/FileShareServer/InboundInfo;)V
    .locals 2
    .param p1, "updateInfo"    # Lcom/samsung/android/app/FileShareServer/InboundInfo;

    .prologue
    .line 473
    iput-object p1, p0, Lcom/samsung/android/app/FileShareServer/InboundFragment;->mInboundInfo:Lcom/samsung/android/app/FileShareServer/InboundInfo;

    .line 476
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/samsung/android/app/FileShareServer/InboundFragment$4;

    invoke-direct {v1, p0}, Lcom/samsung/android/app/FileShareServer/InboundFragment$4;-><init>(Lcom/samsung/android/app/FileShareServer/InboundFragment;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 483
    .local v0, "mUiThread":Ljava/lang/Thread;
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 484
    return-void
.end method
