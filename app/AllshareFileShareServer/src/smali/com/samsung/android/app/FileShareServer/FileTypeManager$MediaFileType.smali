.class Lcom/samsung/android/app/FileShareServer/FileTypeManager$MediaFileType;
.super Ljava/lang/Object;
.source "FileTypeManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/FileShareServer/FileTypeManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "MediaFileType"
.end annotation


# instance fields
.field description:Ljava/lang/String;

.field fileType:I

.field icon:I

.field mimeType:Ljava/lang/String;


# direct methods
.method constructor <init>(ILjava/lang/String;Ljava/lang/String;I)V
    .locals 0
    .param p1, "fileType"    # I
    .param p2, "mimeType"    # Ljava/lang/String;
    .param p3, "desc"    # Ljava/lang/String;
    .param p4, "icon"    # I

    .prologue
    .line 212
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 213
    iput p1, p0, Lcom/samsung/android/app/FileShareServer/FileTypeManager$MediaFileType;->fileType:I

    .line 214
    iput-object p2, p0, Lcom/samsung/android/app/FileShareServer/FileTypeManager$MediaFileType;->mimeType:Ljava/lang/String;

    .line 215
    iput-object p3, p0, Lcom/samsung/android/app/FileShareServer/FileTypeManager$MediaFileType;->description:Ljava/lang/String;

    .line 216
    iput p4, p0, Lcom/samsung/android/app/FileShareServer/FileTypeManager$MediaFileType;->icon:I

    .line 217
    return-void
.end method
