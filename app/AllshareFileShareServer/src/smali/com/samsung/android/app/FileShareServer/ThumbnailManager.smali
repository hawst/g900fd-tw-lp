.class public Lcom/samsung/android/app/FileShareServer/ThumbnailManager;
.super Ljava/lang/Object;
.source "ThumbnailManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/app/FileShareServer/ThumbnailManager$DownloadedDrawable;,
        Lcom/samsung/android/app/FileShareServer/ThumbnailManager$BitmapGetterTask;
    }
.end annotation


# static fields
.field private static final DELAY_BEFORE_PURGE:I = 0x2710

.field private static final GETTHUMBNAILTIME:I = 0xe4e1c0

.field private static final MICRO_HEIGH:I = 0x68

.field private static final MICRO_WIDTH:I = 0x68

.field private static final TAGClass:Ljava/lang/String; = "ThumbnailManager"

.field private static final THUMBNAIL_BUFFER_MAX_SIZE:I = 0xc8

.field private static final THUMBNAIL_BUFFER_MIN_SIZE:I = 0x64


# instance fields
.field private mCache:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field

.field private mContentResolver:Landroid/content/ContentResolver;

.field private mContext:Landroid/content/Context;

.field private mDefaultBitmap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private mThumbnailBuffer:Lcom/samsung/android/app/FileShareServer/ThumbnailBuffer;

.field private final purgeHandler:Landroid/os/Handler;

.field private final purger:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 77
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 62
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/app/FileShareServer/ThumbnailManager;->mDefaultBitmap:Ljava/util/HashMap;

    .line 64
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/app/FileShareServer/ThumbnailManager;->mCache:Ljava/util/HashMap;

    .line 66
    new-instance v0, Lcom/samsung/android/app/FileShareServer/ThumbnailBuffer;

    iget-object v1, p0, Lcom/samsung/android/app/FileShareServer/ThumbnailManager;->mCache:Ljava/util/HashMap;

    const/16 v2, 0xc8

    const/16 v3, 0x64

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/android/app/FileShareServer/ThumbnailBuffer;-><init>(Ljava/util/Map;II)V

    iput-object v0, p0, Lcom/samsung/android/app/FileShareServer/ThumbnailManager;->mThumbnailBuffer:Lcom/samsung/android/app/FileShareServer/ThumbnailBuffer;

    .line 69
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/app/FileShareServer/ThumbnailManager;->purgeHandler:Landroid/os/Handler;

    .line 71
    new-instance v0, Lcom/samsung/android/app/FileShareServer/ThumbnailManager$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/app/FileShareServer/ThumbnailManager$1;-><init>(Lcom/samsung/android/app/FileShareServer/ThumbnailManager;)V

    iput-object v0, p0, Lcom/samsung/android/app/FileShareServer/ThumbnailManager;->purger:Ljava/lang/Runnable;

    .line 78
    iput-object p1, p0, Lcom/samsung/android/app/FileShareServer/ThumbnailManager;->mContext:Landroid/content/Context;

    .line 79
    iget-object v0, p0, Lcom/samsung/android/app/FileShareServer/ThumbnailManager;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/FileShareServer/ThumbnailManager;->mContentResolver:Landroid/content/ContentResolver;

    .line 80
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/android/app/FileShareServer/ThumbnailManager;Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/FileShareServer/ThumbnailManager;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 41
    invoke-direct {p0, p1}, Lcom/samsung/android/app/FileShareServer/ThumbnailManager;->getThumbnailBitmap(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$100(Lcom/samsung/android/app/FileShareServer/ThumbnailManager;)Lcom/samsung/android/app/FileShareServer/ThumbnailBuffer;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/FileShareServer/ThumbnailManager;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/samsung/android/app/FileShareServer/ThumbnailManager;->mThumbnailBuffer:Lcom/samsung/android/app/FileShareServer/ThumbnailBuffer;

    return-object v0
.end method

.method static synthetic access$200(Landroid/widget/ImageView;)Lcom/samsung/android/app/FileShareServer/ThumbnailManager$BitmapGetterTask;
    .locals 1
    .param p0, "x0"    # Landroid/widget/ImageView;

    .prologue
    .line 41
    invoke-static {p0}, Lcom/samsung/android/app/FileShareServer/ThumbnailManager;->getBitmapGetterTask(Landroid/widget/ImageView;)Lcom/samsung/android/app/FileShareServer/ThumbnailManager$BitmapGetterTask;

    move-result-object v0

    return-object v0
.end method

.method private static cancelPotentialGet(Ljava/lang/String;Landroid/widget/ImageView;)Z
    .locals 4
    .param p0, "path"    # Ljava/lang/String;
    .param p1, "imageView"    # Landroid/widget/ImageView;

    .prologue
    const/4 v2, 0x1

    .line 172
    invoke-static {p1}, Lcom/samsung/android/app/FileShareServer/ThumbnailManager;->getBitmapGetterTask(Landroid/widget/ImageView;)Lcom/samsung/android/app/FileShareServer/ThumbnailManager$BitmapGetterTask;

    move-result-object v0

    .line 174
    .local v0, "bitmapGetterTask":Lcom/samsung/android/app/FileShareServer/ThumbnailManager$BitmapGetterTask;
    if-eqz v0, :cond_1

    .line 175
    # getter for: Lcom/samsung/android/app/FileShareServer/ThumbnailManager$BitmapGetterTask;->path:Ljava/lang/String;
    invoke-static {v0}, Lcom/samsung/android/app/FileShareServer/ThumbnailManager$BitmapGetterTask;->access$300(Lcom/samsung/android/app/FileShareServer/ThumbnailManager$BitmapGetterTask;)Ljava/lang/String;

    move-result-object v1

    .line 176
    .local v1, "bitmapPath":Ljava/lang/String;
    if-eqz v1, :cond_0

    invoke-virtual {v1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 177
    :cond_0
    invoke-virtual {v0, v2}, Lcom/samsung/android/app/FileShareServer/ThumbnailManager$BitmapGetterTask;->cancel(Z)Z

    .line 182
    .end local v1    # "bitmapPath":Ljava/lang/String;
    :cond_1
    :goto_0
    return v2

    .line 179
    .restart local v1    # "bitmapPath":Ljava/lang/String;
    :cond_2
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private forceGetThumbnail(Ljava/lang/String;Landroid/widget/ImageView;)V
    .locals 4
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "imageView"    # Landroid/widget/ImageView;

    .prologue
    .line 153
    if-nez p1, :cond_1

    .line 154
    const/4 v2, 0x0

    invoke-virtual {p2, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 164
    :cond_0
    :goto_0
    return-void

    .line 158
    :cond_1
    invoke-static {p1, p2}, Lcom/samsung/android/app/FileShareServer/ThumbnailManager;->cancelPotentialGet(Ljava/lang/String;Landroid/widget/ImageView;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 159
    new-instance v1, Lcom/samsung/android/app/FileShareServer/ThumbnailManager$BitmapGetterTask;

    invoke-direct {v1, p0, p2}, Lcom/samsung/android/app/FileShareServer/ThumbnailManager$BitmapGetterTask;-><init>(Lcom/samsung/android/app/FileShareServer/ThumbnailManager;Landroid/widget/ImageView;)V

    .line 160
    .local v1, "task":Lcom/samsung/android/app/FileShareServer/ThumbnailManager$BitmapGetterTask;
    new-instance v0, Lcom/samsung/android/app/FileShareServer/ThumbnailManager$DownloadedDrawable;

    invoke-direct {v0, v1}, Lcom/samsung/android/app/FileShareServer/ThumbnailManager$DownloadedDrawable;-><init>(Lcom/samsung/android/app/FileShareServer/ThumbnailManager$BitmapGetterTask;)V

    .line 161
    .local v0, "downloadedDrawable":Lcom/samsung/android/app/FileShareServer/ThumbnailManager$DownloadedDrawable;
    invoke-virtual {p2, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 162
    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-virtual {v1, v2}, Lcom/samsung/android/app/FileShareServer/ThumbnailManager$BitmapGetterTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0
.end method

.method private static getBitmapGetterTask(Landroid/widget/ImageView;)Lcom/samsung/android/app/FileShareServer/ThumbnailManager$BitmapGetterTask;
    .locals 3
    .param p0, "imageView"    # Landroid/widget/ImageView;

    .prologue
    .line 186
    if-eqz p0, :cond_0

    .line 187
    invoke-virtual {p0}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 188
    .local v1, "drawable":Landroid/graphics/drawable/Drawable;
    instance-of v2, v1, Lcom/samsung/android/app/FileShareServer/ThumbnailManager$DownloadedDrawable;

    if-eqz v2, :cond_0

    move-object v0, v1

    .line 189
    check-cast v0, Lcom/samsung/android/app/FileShareServer/ThumbnailManager$DownloadedDrawable;

    .line 190
    .local v0, "downloadedDrawable":Lcom/samsung/android/app/FileShareServer/ThumbnailManager$DownloadedDrawable;
    invoke-virtual {v0}, Lcom/samsung/android/app/FileShareServer/ThumbnailManager$DownloadedDrawable;->getBitmapGatterTask()Lcom/samsung/android/app/FileShareServer/ThumbnailManager$BitmapGetterTask;

    move-result-object v2

    .line 193
    .end local v0    # "downloadedDrawable":Lcom/samsung/android/app/FileShareServer/ThumbnailManager$DownloadedDrawable;
    .end local v1    # "drawable":Landroid/graphics/drawable/Drawable;
    :goto_0
    return-object v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private static getExifOrientation(Ljava/lang/String;)I
    .locals 10
    .param p0, "filepath"    # Ljava/lang/String;

    .prologue
    const/4 v9, -0x1

    .line 298
    const/4 v0, 0x0

    .line 299
    .local v0, "degree":I
    const/4 v2, 0x0

    .line 301
    .local v2, "exif":Landroid/media/ExifInterface;
    :try_start_0
    new-instance v3, Landroid/media/ExifInterface;

    invoke-direct {v3, p0}, Landroid/media/ExifInterface;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .end local v2    # "exif":Landroid/media/ExifInterface;
    .local v3, "exif":Landroid/media/ExifInterface;
    move-object v2, v3

    .line 305
    .end local v3    # "exif":Landroid/media/ExifInterface;
    .restart local v2    # "exif":Landroid/media/ExifInterface;
    :goto_0
    if-eqz v2, :cond_0

    .line 306
    const-string v5, "Orientation"

    invoke-virtual {v2, v5, v9}, Landroid/media/ExifInterface;->getAttributeInt(Ljava/lang/String;I)I

    move-result v4

    .line 307
    .local v4, "orientation":I
    if-eq v4, v9, :cond_0

    .line 309
    packed-switch v4, :pswitch_data_0

    .line 324
    .end local v4    # "orientation":I
    :cond_0
    :goto_1
    :pswitch_0
    return v0

    .line 302
    :catch_0
    move-exception v1

    .line 303
    .local v1, "ex":Ljava/io/IOException;
    const-string v5, "ThumbnailManager"

    const-string v6, "getExifOrientation"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Cannot read exif"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/samsung/android/app/FileShareServer/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 311
    .end local v1    # "ex":Ljava/io/IOException;
    .restart local v4    # "orientation":I
    :pswitch_1
    const/16 v0, 0x5a

    .line 312
    goto :goto_1

    .line 314
    :pswitch_2
    const/16 v0, 0xb4

    .line 315
    goto :goto_1

    .line 317
    :pswitch_3
    const/16 v0, 0x10e

    .line 318
    goto :goto_1

    .line 309
    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method private getImageBitmap(Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 13
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 237
    const/4 v6, 0x0

    .line 238
    .local v6, "bitmap":Landroid/graphics/Bitmap;
    const/4 v7, 0x0

    .line 240
    .local v7, "cursor":Landroid/database/Cursor;
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/app/FileShareServer/ThumbnailManager;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v1, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "_data=\""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 242
    if-eqz v7, :cond_2

    .line 243
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 244
    const-string v0, "_id"

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v10

    .line 245
    .local v10, "id":J
    iget-object v0, p0, Lcom/samsung/android/app/FileShareServer/ThumbnailManager;->mContentResolver:Landroid/content/ContentResolver;

    const/4 v3, 0x3

    const/4 v4, 0x0

    invoke-static {v0, v10, v11, v3, v4}, Landroid/provider/MediaStore$Images$Thumbnails;->getThumbnail(Landroid/content/ContentResolver;JILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v6

    .line 247
    if-eqz v6, :cond_1

    .line 248
    const-string v0, "orientation"

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v12

    .line 249
    .local v12, "index2":I
    invoke-interface {v7, v12}, Landroid/database/Cursor;->getInt(I)I

    move-result v8

    .line 251
    .local v8, "degree":I
    if-nez v8, :cond_0

    .line 252
    invoke-static {p1}, Lcom/samsung/android/app/FileShareServer/ThumbnailManager;->getExifOrientation(Ljava/lang/String;)I

    move-result v8

    .line 254
    :cond_0
    if-eqz v8, :cond_1

    .line 255
    invoke-static {v6, v8}, Lcom/samsung/android/app/FileShareServer/ThumbnailManager;->rotate(Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;

    move-result-object v6

    .line 258
    .end local v8    # "degree":I
    .end local v10    # "id":J
    .end local v12    # "index2":I
    :cond_1
    invoke-interface {v7}, Landroid/database/Cursor;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 263
    :cond_2
    if-eqz v7, :cond_3

    .line 264
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 267
    :cond_3
    :goto_0
    if-nez v6, :cond_4

    .line 268
    const/4 v2, 0x0

    .line 269
    .local v2, "tempB":Landroid/graphics/Bitmap;
    new-instance v1, Landroid/graphics/Matrix;

    invoke-direct {v1}, Landroid/graphics/Matrix;-><init>()V

    .line 272
    .local v1, "m":Landroid/graphics/Matrix;
    const/16 v0, 0x100

    :try_start_1
    invoke-direct {p0, v0, p1}, Lcom/samsung/android/app/FileShareServer/ThumbnailManager;->getSampleSizeBitmap(ILjava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 273
    const/16 v3, 0x40

    const/16 v4, 0x40

    const/4 v5, 0x1

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/samsung/android/app/FileShareServer/ThumbnailManager;->transform(Landroid/graphics/Matrix;Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v6

    .line 274
    if-eqz v2, :cond_4

    .line 275
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->recycle()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 282
    .end local v1    # "m":Landroid/graphics/Matrix;
    .end local v2    # "tempB":Landroid/graphics/Bitmap;
    :cond_4
    :goto_1
    return-object v6

    .line 260
    :catch_0
    move-exception v9

    .line 261
    .local v9, "e":Ljava/lang/Exception;
    :try_start_2
    invoke-virtual {v9}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 263
    if-eqz v7, :cond_3

    .line 264
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 263
    .end local v9    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v0

    if-eqz v7, :cond_5

    .line 264
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    :cond_5
    throw v0

    .line 276
    .restart local v1    # "m":Landroid/graphics/Matrix;
    .restart local v2    # "tempB":Landroid/graphics/Bitmap;
    :catch_1
    move-exception v9

    .line 277
    .restart local v9    # "e":Ljava/lang/Exception;
    if-eqz v2, :cond_6

    .line 278
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->recycle()V

    .line 279
    :cond_6
    invoke-virtual {v9}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1
.end method

.method private getSampleSizeBitmap(ILjava/lang/String;)Landroid/graphics/Bitmap;
    .locals 10
    .param p1, "targetWidthHeight"    # I
    .param p2, "path"    # Ljava/lang/String;

    .prologue
    const/4 v9, 0x1

    .line 405
    new-instance v6, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v6}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 407
    .local v6, "options":Landroid/graphics/BitmapFactory$Options;
    iput-boolean v9, v6, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 408
    invoke-static {p2, v6}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 409
    .local v0, "bm":Landroid/graphics/Bitmap;
    const/4 v8, 0x0

    iput-boolean v8, v6, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 411
    const/4 v8, -0x1

    if-eq p1, v8, :cond_3

    .line 413
    iget v7, v6, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    .line 414
    .local v7, "w":I
    iget v5, v6, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    .line 415
    .local v5, "h":I
    div-int v3, v7, p1

    .line 416
    .local v3, "candidateW":I
    div-int v2, v5, p1

    .line 417
    .local v2, "candidateH":I
    invoke-static {v3, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 418
    .local v1, "candidate":I
    if-nez v1, :cond_0

    .line 419
    const/4 v1, 0x1

    .line 421
    :cond_0
    if-le v1, v9, :cond_1

    .line 422
    if-le v7, p1, :cond_1

    div-int v8, v7, v1

    if-ge v8, p1, :cond_1

    .line 423
    add-int/lit8 v1, v1, -0x1

    .line 426
    :cond_1
    if-le v1, v9, :cond_2

    .line 427
    if-le v5, p1, :cond_2

    div-int v8, v5, v1

    if-ge v8, p1, :cond_2

    .line 428
    add-int/lit8 v1, v1, -0x1

    .line 431
    :cond_2
    iput v1, v6, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 434
    .end local v1    # "candidate":I
    .end local v2    # "candidateH":I
    .end local v3    # "candidateW":I
    .end local v5    # "h":I
    .end local v7    # "w":I
    :cond_3
    invoke-static {p2, v6}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 436
    const/4 v4, 0x0

    .line 437
    .local v4, "degree":I
    invoke-static {p2}, Lcom/samsung/android/app/FileShareServer/ThumbnailManager;->getExifOrientation(Ljava/lang/String;)I

    move-result v4

    .line 439
    if-eqz v4, :cond_4

    .line 440
    invoke-static {v0, v4}, Lcom/samsung/android/app/FileShareServer/ThumbnailManager;->rotate(Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 442
    :cond_4
    return-object v0
.end method

.method private getThumbnailBitmap(Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 8
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 197
    const/4 v0, 0x0

    .line 199
    .local v0, "b":Landroid/graphics/Bitmap;
    invoke-static {p1}, Lcom/samsung/android/app/FileShareServer/FileTypeManager;->isImageFileType(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 200
    invoke-direct {p0, p1}, Lcom/samsung/android/app/FileShareServer/ThumbnailManager;->getImageBitmap(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 205
    :cond_0
    :goto_0
    if-nez v0, :cond_5

    .line 206
    invoke-static {p1}, Lcom/samsung/android/app/FileShareServer/FileTypeManager;->needToCheckMimeType(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 207
    const v3, 0x7f020007

    .line 208
    .local v3, "icon":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    const/4 v5, 0x3

    if-ge v2, v5, :cond_3

    .line 209
    iget-object v5, p0, Lcom/samsung/android/app/FileShareServer/ThumbnailManager;->mContentResolver:Landroid/content/ContentResolver;

    invoke-static {p1, v5}, Lcom/samsung/android/app/FileShareServer/FileTypeManager;->isAudioInMediaStore(Ljava/lang/String;Landroid/content/ContentResolver;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 210
    const v3, 0x7f020003

    .line 208
    :goto_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 201
    .end local v2    # "i":I
    .end local v3    # "icon":I
    :cond_1
    invoke-static {p1}, Lcom/samsung/android/app/FileShareServer/FileTypeManager;->isVideoFileType(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 202
    invoke-direct {p0, p1}, Lcom/samsung/android/app/FileShareServer/ThumbnailManager;->getVideoBitmap(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0

    .line 213
    .restart local v2    # "i":I
    .restart local v3    # "icon":I
    :cond_2
    const-wide/16 v6, 0x1f4

    :try_start_0
    invoke-static {v6, v7}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    .line 214
    :catch_0
    move-exception v1

    .line 216
    .local v1, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v1}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_2

    .line 220
    .end local v1    # "e":Ljava/lang/InterruptedException;
    :cond_3
    iget-object v5, p0, Lcom/samsung/android/app/FileShareServer/ThumbnailManager;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    check-cast v4, Landroid/graphics/drawable/BitmapDrawable;

    .line 222
    .local v4, "tempDrawable":Landroid/graphics/drawable/BitmapDrawable;
    invoke-virtual {v4}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 228
    .end local v2    # "i":I
    .end local v3    # "icon":I
    :goto_3
    iget-object v5, p0, Lcom/samsung/android/app/FileShareServer/ThumbnailManager;->mDefaultBitmap:Ljava/util/HashMap;

    const/4 v6, 0x1

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    invoke-virtual {v5, p1, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 233
    .end local v4    # "tempDrawable":Landroid/graphics/drawable/BitmapDrawable;
    :goto_4
    return-object v0

    .line 224
    :cond_4
    iget-object v5, p0, Lcom/samsung/android/app/FileShareServer/ThumbnailManager;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-static {p1}, Lcom/samsung/android/app/FileShareServer/FileTypeManager;->getIcon(Ljava/lang/String;)I

    move-result v6

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    check-cast v4, Landroid/graphics/drawable/BitmapDrawable;

    .line 226
    .restart local v4    # "tempDrawable":Landroid/graphics/drawable/BitmapDrawable;
    invoke-virtual {v4}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_3

    .line 230
    .end local v4    # "tempDrawable":Landroid/graphics/drawable/BitmapDrawable;
    :cond_5
    iget-object v5, p0, Lcom/samsung/android/app/FileShareServer/ThumbnailManager;->mDefaultBitmap:Ljava/util/HashMap;

    const/4 v6, 0x0

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    invoke-virtual {v5, p1, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_4
.end method

.method private getVideoBitmap(Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 3
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 286
    const/4 v0, 0x0

    .line 288
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    :try_start_0
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 289
    .local v2, "file":Ljava/io/File;
    invoke-direct {p0, v2}, Lcom/samsung/android/app/FileShareServer/ThumbnailManager;->getVideoThumbBitmap(Ljava/io/File;)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 294
    .end local v2    # "file":Ljava/io/File;
    :goto_0
    return-object v0

    .line 291
    :catch_0
    move-exception v1

    .line 292
    .local v1, "ex":Ljava/lang/OutOfMemoryError;
    invoke-virtual {v1}, Ljava/lang/OutOfMemoryError;->printStackTrace()V

    goto :goto_0
.end method

.method private getVideoThumbBitmap(Ljava/io/File;)Landroid/graphics/Bitmap;
    .locals 9
    .param p1, "mFile"    # Ljava/io/File;

    .prologue
    const/high16 v8, 0x42d00000    # 104.0f

    .line 328
    new-instance v6, Landroid/media/MediaMetadataRetriever;

    invoke-direct {v6}, Landroid/media/MediaMetadataRetriever;-><init>()V

    .line 329
    .local v6, "retriever":Landroid/media/MediaMetadataRetriever;
    const/4 v2, 0x0

    .line 331
    .local v2, "bitmap":Landroid/graphics/Bitmap;
    :try_start_0
    invoke-virtual {p1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Landroid/media/MediaMetadataRetriever;->setDataSource(Ljava/lang/String;)V

    .line 332
    const-wide/32 v4, 0xe4e1c0

    invoke-virtual {v6, v4, v5}, Landroid/media/MediaMetadataRetriever;->getFrameAtTime(J)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 333
    invoke-virtual {v6}, Landroid/media/MediaMetadataRetriever;->release()V

    .line 335
    const/4 v7, 0x0

    .line 336
    .local v7, "scale":F
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    if-ge v0, v3, :cond_1

    .line 337
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    int-to-float v0, v0

    div-float v7, v8, v0

    .line 341
    :goto_0
    new-instance v1, Landroid/graphics/Matrix;

    invoke-direct {v1}, Landroid/graphics/Matrix;-><init>()V

    .line 342
    .local v1, "matrix":Landroid/graphics/Matrix;
    invoke-virtual {v1, v7, v7}, Landroid/graphics/Matrix;->setScale(FF)V

    .line 343
    const/16 v3, 0x68

    const/16 v4, 0x68

    const/4 v5, 0x0

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/samsung/android/app/FileShareServer/ThumbnailManager;->transform(Landroid/graphics/Matrix;Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 346
    if-eqz v6, :cond_0

    .line 347
    invoke-virtual {v6}, Landroid/media/MediaMetadataRetriever;->release()V

    .line 349
    .end local v1    # "matrix":Landroid/graphics/Matrix;
    .end local v7    # "scale":F
    :cond_0
    :goto_1
    return-object v0

    .line 339
    .restart local v7    # "scale":F
    :cond_1
    :try_start_1
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    int-to-float v0, v0

    div-float v7, v8, v0

    goto :goto_0

    .line 344
    .end local v7    # "scale":F
    :catch_0
    move-exception v0

    .line 346
    if-eqz v6, :cond_2

    .line 347
    invoke-virtual {v6}, Landroid/media/MediaMetadataRetriever;->release()V

    :cond_2
    move-object v0, v2

    .line 349
    goto :goto_1

    .line 346
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_3

    .line 347
    invoke-virtual {v6}, Landroid/media/MediaMetadataRetriever;->release()V

    :cond_3
    throw v0
.end method

.method private resetPurgeTimer()V
    .locals 4

    .prologue
    .line 167
    iget-object v0, p0, Lcom/samsung/android/app/FileShareServer/ThumbnailManager;->purgeHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/samsung/android/app/FileShareServer/ThumbnailManager;->purger:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 168
    iget-object v0, p0, Lcom/samsung/android/app/FileShareServer/ThumbnailManager;->purgeHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/samsung/android/app/FileShareServer/ThumbnailManager;->purger:Ljava/lang/Runnable;

    const-wide/16 v2, 0x2710

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 169
    return-void
.end method

.method private static rotate(Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;
    .locals 9
    .param p0, "b"    # Landroid/graphics/Bitmap;
    .param p1, "degrees"    # I

    .prologue
    const/high16 v3, 0x40000000    # 2.0f

    .line 387
    if-eqz p1, :cond_0

    if-eqz p0, :cond_0

    .line 388
    new-instance v5, Landroid/graphics/Matrix;

    invoke-direct {v5}, Landroid/graphics/Matrix;-><init>()V

    .line 389
    .local v5, "m":Landroid/graphics/Matrix;
    int-to-float v0, p1

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v1, v3

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v2, v3

    invoke-virtual {v5, v0, v1, v2}, Landroid/graphics/Matrix;->setRotate(FFF)V

    .line 391
    const/4 v1, 0x0

    const/4 v2, 0x0

    :try_start_0
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    const/4 v6, 0x1

    move-object v0, p0

    invoke-static/range {v0 .. v6}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v7

    .line 392
    .local v7, "b2":Landroid/graphics/Bitmap;
    invoke-virtual {p0, v7}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 393
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->recycle()V
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    .line 394
    move-object p0, v7

    .line 401
    .end local v5    # "m":Landroid/graphics/Matrix;
    .end local v7    # "b2":Landroid/graphics/Bitmap;
    :cond_0
    :goto_0
    return-object p0

    .line 396
    .restart local v5    # "m":Landroid/graphics/Matrix;
    :catch_0
    move-exception v8

    .line 398
    .local v8, "ex":Ljava/lang/OutOfMemoryError;
    invoke-virtual {v8}, Ljava/lang/OutOfMemoryError;->printStackTrace()V

    goto :goto_0
.end method

.method private transform(Landroid/graphics/Matrix;Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;
    .locals 14
    .param p1, "scaler"    # Landroid/graphics/Matrix;
    .param p2, "source"    # Landroid/graphics/Bitmap;
    .param p3, "targetWidth"    # I
    .param p4, "targetHeight"    # I
    .param p5, "scaleUp"    # Z

    .prologue
    .line 354
    if-nez p2, :cond_1

    .line 355
    const/4 v10, 0x0

    .line 383
    :cond_0
    :goto_0
    return-object v10

    .line 358
    :cond_1
    invoke-virtual/range {p2 .. p2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    invoke-virtual/range {p2 .. p2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    if-ge v2, v3, :cond_2

    .line 359
    move/from16 v0, p3

    int-to-float v2, v0

    invoke-virtual/range {p2 .. p2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    int-to-float v3, v3

    div-float v13, v2, v3

    .line 365
    .local v13, "scale":F
    :goto_1
    if-eqz p1, :cond_3

    .line 366
    invoke-virtual {p1, v13, v13}, Landroid/graphics/Matrix;->setScale(FF)V

    .line 368
    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual/range {p2 .. p2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    invoke-virtual/range {p2 .. p2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v6

    const/4 v8, 0x1

    move-object/from16 v2, p2

    move-object v7, p1

    invoke-static/range {v2 .. v8}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v9

    .line 375
    .local v9, "b1":Landroid/graphics/Bitmap;
    :goto_2
    const/4 v2, 0x0

    invoke-virtual {v9}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    sub-int v3, v3, p3

    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    move-result v11

    .line 376
    .local v11, "dx1":I
    const/4 v2, 0x0

    invoke-virtual {v9}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    sub-int v3, v3, p4

    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    move-result v12

    .line 378
    .local v12, "dy1":I
    div-int/lit8 v2, v11, 0x2

    div-int/lit8 v3, v12, 0x2

    move/from16 v0, p3

    move/from16 v1, p4

    invoke-static {v9, v2, v3, v0, v1}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIII)Landroid/graphics/Bitmap;

    move-result-object v10

    .line 380
    .local v10, "b2":Landroid/graphics/Bitmap;
    move-object/from16 v0, p2

    invoke-virtual {v9, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 381
    invoke-virtual {v9}, Landroid/graphics/Bitmap;->recycle()V

    goto :goto_0

    .line 361
    .end local v9    # "b1":Landroid/graphics/Bitmap;
    .end local v10    # "b2":Landroid/graphics/Bitmap;
    .end local v11    # "dx1":I
    .end local v12    # "dy1":I
    .end local v13    # "scale":F
    :cond_2
    move/from16 v0, p4

    int-to-float v2, v0

    invoke-virtual/range {p2 .. p2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    int-to-float v3, v3

    div-float v13, v2, v3

    .restart local v13    # "scale":F
    goto :goto_1

    .line 371
    :cond_3
    move-object/from16 v9, p2

    .restart local v9    # "b1":Landroid/graphics/Bitmap;
    goto :goto_2
.end method


# virtual methods
.method public clearCache()V
    .locals 1

    .prologue
    .line 103
    iget-object v0, p0, Lcom/samsung/android/app/FileShareServer/ThumbnailManager;->mThumbnailBuffer:Lcom/samsung/android/app/FileShareServer/ThumbnailBuffer;

    invoke-virtual {v0}, Lcom/samsung/android/app/FileShareServer/ThumbnailBuffer;->clear()V

    .line 104
    return-void
.end method

.method public getBitmap(Ljava/lang/String;Landroid/widget/ImageView;)V
    .locals 2
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "imageView"    # Landroid/widget/ImageView;

    .prologue
    .line 83
    invoke-direct {p0}, Lcom/samsung/android/app/FileShareServer/ThumbnailManager;->resetPurgeTimer()V

    .line 84
    iget-object v1, p0, Lcom/samsung/android/app/FileShareServer/ThumbnailManager;->mThumbnailBuffer:Lcom/samsung/android/app/FileShareServer/ThumbnailBuffer;

    invoke-virtual {v1, p1}, Lcom/samsung/android/app/FileShareServer/ThumbnailBuffer;->get(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 86
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    if-nez v0, :cond_0

    .line 87
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/app/FileShareServer/ThumbnailManager;->forceGetThumbnail(Ljava/lang/String;Landroid/widget/ImageView;)V

    .line 93
    :goto_0
    return-void

    .line 89
    :cond_0
    invoke-static {p1, p2}, Lcom/samsung/android/app/FileShareServer/ThumbnailManager;->cancelPotentialGet(Ljava/lang/String;Landroid/widget/ImageView;)Z

    .line 90
    invoke-virtual {p2, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_0
.end method

.method public isBroken(Ljava/lang/String;)Z
    .locals 2
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 96
    iget-object v1, p0, Lcom/samsung/android/app/FileShareServer/ThumbnailManager;->mDefaultBitmap:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    .line 97
    .local v0, "b":Ljava/lang/Boolean;
    if-eqz v0, :cond_0

    .line 98
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    .line 99
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method
