.class public Lcom/samsung/android/app/FileShareServer/ServerService;
.super Landroid/app/Service;
.source "ServerService.java"

# interfaces
.implements Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper$IDeviceEvent;
.implements Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper$ISessionEvent;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/app/FileShareServer/ServerService$ServerServiceBinder;
    }
.end annotation


# static fields
.field private static final TAGClass:Ljava/lang/String; = "ServerService"

.field private static mWorkingList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/app/FileShareServer/InboundInfo;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private bRegisterReceiver:Z

.field private final mBinder:Landroid/os/IBinder;

.field private mBroadcastReceiver:Landroid/content/BroadcastReceiver;

.field mDbAdapter:Lcom/samsung/android/app/FileShareServer/DbAdapter;

.field private mFileShareDevice:Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper;

.field public mGetCurrenUserFromIntent:I

.field private mIsUpdate:Z

.field private mLastUpdatedTime:J

.field private mNotiManager:Landroid/app/NotificationManager;

.field private mPowerManager:Landroid/os/PowerManager;

.field private mSaveFilePath:Ljava/io/File;

.field private mThemeContext:Landroid/content/Context;

.field private mWakeLock:Landroid/os/PowerManager$WakeLock;

.field private mbForeground:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 48
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/samsung/android/app/FileShareServer/ServerService;->mWorkingList:Ljava/util/ArrayList;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v0, 0x0

    const/4 v2, 0x0

    .line 44
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 49
    iput-boolean v0, p0, Lcom/samsung/android/app/FileShareServer/ServerService;->bRegisterReceiver:Z

    .line 50
    iput-object v2, p0, Lcom/samsung/android/app/FileShareServer/ServerService;->mSaveFilePath:Ljava/io/File;

    .line 51
    iput-object v2, p0, Lcom/samsung/android/app/FileShareServer/ServerService;->mNotiManager:Landroid/app/NotificationManager;

    .line 53
    iput-boolean v0, p0, Lcom/samsung/android/app/FileShareServer/ServerService;->mbForeground:Z

    .line 54
    iput-object v2, p0, Lcom/samsung/android/app/FileShareServer/ServerService;->mThemeContext:Landroid/content/Context;

    .line 56
    iput v0, p0, Lcom/samsung/android/app/FileShareServer/ServerService;->mGetCurrenUserFromIntent:I

    .line 58
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/samsung/android/app/FileShareServer/ServerService;->mLastUpdatedTime:J

    .line 193
    new-instance v0, Lcom/samsung/android/app/FileShareServer/ServerService$ServerServiceBinder;

    invoke-direct {v0, p0}, Lcom/samsung/android/app/FileShareServer/ServerService$ServerServiceBinder;-><init>(Lcom/samsung/android/app/FileShareServer/ServerService;)V

    iput-object v0, p0, Lcom/samsung/android/app/FileShareServer/ServerService;->mBinder:Landroid/os/IBinder;

    .line 641
    iput-object v2, p0, Lcom/samsung/android/app/FileShareServer/ServerService;->mFileShareDevice:Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper;

    .line 894
    new-instance v0, Lcom/samsung/android/app/FileShareServer/ServerService$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/app/FileShareServer/ServerService$1;-><init>(Lcom/samsung/android/app/FileShareServer/ServerService;)V

    iput-object v0, p0, Lcom/samsung/android/app/FileShareServer/ServerService;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    return-void
.end method

.method private AddOngoing(Ljava/lang/String;)V
    .locals 18
    .param p1, "sessionId"    # Ljava/lang/String;

    .prologue
    .line 662
    invoke-direct/range {p0 .. p1}, Lcom/samsung/android/app/FileShareServer/ServerService;->findCurrentSession(Ljava/lang/String;)I

    move-result v5

    .line 663
    .local v5, "idx":I
    const/4 v11, -0x1

    if-ne v5, v11, :cond_0

    .line 664
    const-string v11, "ServerService"

    const-string v12, "AddOngoing"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "can\'t find "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move-object/from16 v0, p1

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v11, v12, v13}, Lcom/samsung/android/app/FileShareServer/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 724
    :goto_0
    return-void

    .line 667
    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v12

    move-object/from16 v0, p0

    iget-wide v14, v0, Lcom/samsung/android/app/FileShareServer/ServerService;->mLastUpdatedTime:J

    sub-long/2addr v12, v14

    const-wide/16 v14, 0x1f4

    cmp-long v11, v12, v14

    if-gez v11, :cond_1

    .line 668
    const-string v11, "ServerService"

    const-string v12, "AddOngoing"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "Notify: DO NOT UPDATE "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v14

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/samsung/android/app/FileShareServer/ServerService;->mLastUpdatedTime:J

    move-wide/from16 v16, v0

    sub-long v14, v14, v16

    invoke-virtual {v13, v14, v15}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v11, v12, v13}, Lcom/samsung/android/app/FileShareServer/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 672
    :cond_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v12

    move-object/from16 v0, p0

    iput-wide v12, v0, Lcom/samsung/android/app/FileShareServer/ServerService;->mLastUpdatedTime:J

    .line 674
    sget-object v11, Lcom/samsung/android/app/FileShareServer/ServerService;->mWorkingList:Ljava/util/ArrayList;

    invoke-virtual {v11, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/samsung/android/app/FileShareServer/InboundInfo;

    .line 675
    .local v6, "info":Lcom/samsung/android/app/FileShareServer/InboundInfo;
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/samsung/android/app/FileShareServer/ServerService;->mNotiManager:Landroid/app/NotificationManager;

    if-nez v11, :cond_2

    .line 676
    const-string v11, "notification"

    move-object/from16 v0, p0

    invoke-virtual {v0, v11}, Lcom/samsung/android/app/FileShareServer/ServerService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Landroid/app/NotificationManager;

    move-object/from16 v0, p0

    iput-object v11, v0, Lcom/samsung/android/app/FileShareServer/ServerService;->mNotiManager:Landroid/app/NotificationManager;

    .line 679
    :cond_2
    new-instance v2, Landroid/app/Notification$Builder;

    move-object/from16 v0, p0

    invoke-direct {v2, v0}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;)V

    .line 681
    .local v2, "builder":Landroid/app/Notification$Builder;
    move-object/from16 v0, p0

    iget-boolean v11, v0, Lcom/samsung/android/app/FileShareServer/ServerService;->mIsUpdate:Z

    if-eqz v11, :cond_4

    .line 682
    invoke-virtual {v6}, Lcom/samsung/android/app/FileShareServer/InboundInfo;->update()V

    .line 690
    :goto_1
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/app/FileShareServer/ServerService;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    const v12, 0x1060058

    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getColor(I)I

    move-result v11

    invoke-virtual {v2, v11}, Landroid/app/Notification$Builder;->setColor(I)Landroid/app/Notification$Builder;

    .line 693
    invoke-virtual {v6}, Lcom/samsung/android/app/FileShareServer/InboundInfo;->getTotalCount()I

    move-result v11

    const/4 v12, 0x1

    if-le v11, v12, :cond_5

    .line 694
    invoke-virtual {v6}, Lcom/samsung/android/app/FileShareServer/InboundInfo;->getFileName()Ljava/lang/String;

    move-result-object v4

    .line 695
    .local v4, "fileName":Ljava/lang/String;
    invoke-virtual {v6}, Lcom/samsung/android/app/FileShareServer/InboundInfo;->getFileList()Ljava/util/ArrayList;

    move-result-object v11

    if-eqz v11, :cond_3

    invoke-virtual {v6}, Lcom/samsung/android/app/FileShareServer/InboundInfo;->getFileList()Ljava/util/ArrayList;

    move-result-object v11

    invoke-virtual {v11}, Ljava/util/ArrayList;->size()I

    move-result v11

    if-lez v11, :cond_3

    .line 696
    invoke-virtual {v6}, Lcom/samsung/android/app/FileShareServer/InboundInfo;->getFileList()Ljava/util/ArrayList;

    move-result-object v11

    const/4 v12, 0x0

    invoke-virtual {v11, v12}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/io/File;

    invoke-virtual {v11}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v4

    .line 698
    :cond_3
    const v11, 0x7f060017

    const/4 v12, 0x2

    new-array v12, v12, [Ljava/lang/Object;

    const/4 v13, 0x0

    aput-object v4, v12, v13

    const/4 v13, 0x1

    invoke-virtual {v6}, Lcom/samsung/android/app/FileShareServer/InboundInfo;->getTotalCount()I

    move-result v14

    add-int/lit8 v14, v14, -0x1

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    aput-object v14, v12, v13

    move-object/from16 v0, p0

    invoke-virtual {v0, v11, v12}, Lcom/samsung/android/app/FileShareServer/ServerService;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v2, v11}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    .line 704
    .end local v4    # "fileName":Ljava/lang/String;
    :goto_2
    const/16 v8, 0x64

    .line 705
    .local v8, "max":I
    invoke-virtual {v6}, Lcom/samsung/android/app/FileShareServer/InboundInfo;->getCurrentSize()J

    move-result-wide v12

    const-wide/16 v14, 0x64

    mul-long/2addr v12, v14

    invoke-virtual {v6}, Lcom/samsung/android/app/FileShareServer/InboundInfo;->getTotalSize()J

    move-result-wide v14

    div-long/2addr v12, v14

    long-to-int v9, v12

    .line 706
    .local v9, "percent":I
    const/4 v11, 0x0

    invoke-virtual {v2, v8, v9, v11}, Landroid/app/Notification$Builder;->setProgress(IIZ)Landroid/app/Notification$Builder;

    .line 708
    const-string v11, "%d%%"

    const/4 v12, 0x1

    new-array v12, v12, [Ljava/lang/Object;

    const/4 v13, 0x0

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    aput-object v14, v12, v13

    invoke-static {v11, v12}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v2, v11}, Landroid/app/Notification$Builder;->setContentInfo(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    .line 709
    const/4 v11, 0x0

    invoke-virtual {v2, v11}, Landroid/app/Notification$Builder;->setShowWhen(Z)Landroid/app/Notification$Builder;

    .line 710
    const v11, 0x1080081

    invoke-virtual {v2, v11}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    .line 711
    const/4 v11, 0x1

    invoke-virtual {v2, v11}, Landroid/app/Notification$Builder;->setOngoing(Z)Landroid/app/Notification$Builder;

    .line 713
    new-instance v7, Landroid/content/Intent;

    const-class v11, Lcom/samsung/android/app/FileShareServer/InboundActivity;

    move-object/from16 v0, p0

    invoke-direct {v7, v0, v11}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 714
    .local v7, "it":Landroid/content/Intent;
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "com.samsung.android.app.FileShareServer.RECEIVE_PROGRESS"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v6}, Lcom/samsung/android/app/FileShareServer/InboundInfo;->getInboundID()I

    move-result v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v7, v11}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 715
    const/high16 v11, 0x10800000

    invoke-virtual {v7, v11}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 716
    const-string v11, "inboundId"

    invoke-virtual {v6}, Lcom/samsung/android/app/FileShareServer/InboundInfo;->getInboundID()I

    move-result v12

    invoke-virtual {v7, v11, v12}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 717
    invoke-virtual {v6}, Lcom/samsung/android/app/FileShareServer/InboundInfo;->getInboundID()I

    move-result v11

    const/high16 v12, 0x8000000

    move-object/from16 v0, p0

    invoke-static {v0, v11, v7, v12}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v3

    .line 719
    .local v3, "contentIntent":Landroid/app/PendingIntent;
    invoke-virtual {v2, v3}, Landroid/app/Notification$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    .line 721
    const-string v11, "ServerService"

    const-string v12, "AddOngoing"

    const-string v13, "Notify: UPDATE"

    invoke-static {v11, v12, v13}, Lcom/samsung/android/app/FileShareServer/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 722
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/samsung/android/app/FileShareServer/ServerService;->mNotiManager:Landroid/app/NotificationManager;

    const-string v12, "FileShareServer"

    invoke-virtual {v6}, Lcom/samsung/android/app/FileShareServer/InboundInfo;->getInboundID()I

    move-result v13

    mul-int/lit8 v13, v13, 0x2

    invoke-virtual {v2}, Landroid/app/Notification$Builder;->build()Landroid/app/Notification;

    move-result-object v14

    invoke-virtual {v11, v12, v13, v14}, Landroid/app/NotificationManager;->notify(Ljava/lang/String;ILandroid/app/Notification;)V

    goto/16 :goto_0

    .line 684
    .end local v3    # "contentIntent":Landroid/app/PendingIntent;
    .end local v7    # "it":Landroid/content/Intent;
    .end local v8    # "max":I
    .end local v9    # "percent":I
    :cond_4
    const-string v10, ""

    .line 685
    .local v10, "text":Ljava/lang/String;
    const v11, 0x7f060002

    const/4 v12, 0x1

    new-array v12, v12, [Ljava/lang/Object;

    const/4 v13, 0x0

    invoke-virtual {v6}, Lcom/samsung/android/app/FileShareServer/InboundInfo;->getSenderName()Ljava/lang/String;

    move-result-object v14

    aput-object v14, v12, v13

    move-object/from16 v0, p0

    invoke-virtual {v0, v11, v12}, Lcom/samsung/android/app/FileShareServer/ServerService;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    .line 686
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/samsung/android/app/FileShareServer/ServerService;->mThemeContext:Landroid/content/Context;

    const/4 v12, 0x1

    invoke-static {v11, v10, v12}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v11

    invoke-virtual {v11}, Landroid/widget/Toast;->show()V

    .line 687
    const/4 v11, 0x1

    invoke-virtual {v2, v11}, Landroid/app/Notification$Builder;->setDefaults(I)Landroid/app/Notification$Builder;

    goto/16 :goto_1

    .line 701
    .end local v10    # "text":Ljava/lang/String;
    :cond_5
    invoke-virtual {v6}, Lcom/samsung/android/app/FileShareServer/InboundInfo;->getFileName()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v2, v11}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    goto/16 :goto_2
.end method

.method private AddResultNotification(Ljava/lang/String;)V
    .locals 18
    .param p1, "sessionID"    # Ljava/lang/String;

    .prologue
    .line 727
    invoke-direct/range {p0 .. p1}, Lcom/samsung/android/app/FileShareServer/ServerService;->findCurrentSession(Ljava/lang/String;)I

    move-result v7

    .line 728
    .local v7, "idx":I
    const/4 v13, -0x1

    if-ne v7, v13, :cond_0

    .line 729
    const-string v13, "ServerService"

    const-string v14, "AddResultNotification"

    const-string v15, "findCurrentSession return -1!"

    invoke-static {v13, v14, v15}, Lcom/samsung/android/app/FileShareServer/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 825
    :goto_0
    return-void

    .line 731
    :cond_0
    sget-object v13, Lcom/samsung/android/app/FileShareServer/ServerService;->mWorkingList:Ljava/util/ArrayList;

    invoke-virtual {v13, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/samsung/android/app/FileShareServer/InboundInfo;

    .line 732
    .local v8, "info":Lcom/samsung/android/app/FileShareServer/InboundInfo;
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/app/FileShareServer/ServerService;->mNotiManager:Landroid/app/NotificationManager;

    if-nez v13, :cond_1

    .line 733
    const-string v13, "notification"

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/samsung/android/app/FileShareServer/ServerService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Landroid/app/NotificationManager;

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/samsung/android/app/FileShareServer/ServerService;->mNotiManager:Landroid/app/NotificationManager;

    .line 735
    :cond_1
    const/4 v11, 0x0

    .line 736
    .local v11, "notiTitle":Ljava/lang/CharSequence;
    const/4 v10, 0x0

    .line 737
    .local v10, "notiText":Ljava/lang/CharSequence;
    new-instance v2, Landroid/app/Notification$Builder;

    move-object/from16 v0, p0

    invoke-direct {v2, v0}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;)V

    .line 738
    .local v2, "builder":Landroid/app/Notification$Builder;
    const v6, 0x1080082

    .line 739
    .local v6, "downloadIcon":I
    invoke-virtual {v8}, Lcom/samsung/android/app/FileShareServer/InboundInfo;->getTotalFailedCount()I

    move-result v13

    if-nez v13, :cond_3

    invoke-virtual {v8}, Lcom/samsung/android/app/FileShareServer/InboundInfo;->getCurrentStatus()I

    move-result v13

    const/16 v14, 0x3ec

    if-ne v13, v14, :cond_3

    .line 741
    const-string v13, "ServerService"

    const-string v14, "AddResultNotification"

    const-string v15, "receiving_files_completed "

    invoke-static {v13, v14, v15}, Lcom/samsung/android/app/FileShareServer/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 742
    const v13, 0x7f060009

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/samsung/android/app/FileShareServer/ServerService;->getString(I)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v2, v13}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    .line 743
    invoke-virtual {v2, v6}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    .line 744
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v14

    invoke-virtual {v2, v14, v15}, Landroid/app/Notification$Builder;->setWhen(J)Landroid/app/Notification$Builder;

    .line 758
    :goto_1
    const/4 v9, 0x0

    .line 759
    .local v9, "it":Landroid/content/Intent;
    const-string v13, "ServerService"

    const-string v14, "AddResultNotification"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "getTotalFailedCount()= "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v8}, Lcom/samsung/android/app/FileShareServer/InboundInfo;->getTotalFailedCount()I

    move-result v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, " getTotalSuccessCount()= "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v8}, Lcom/samsung/android/app/FileShareServer/InboundInfo;->getTotalSuccessCount()I

    move-result v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v13, v14, v15}, Lcom/samsung/android/app/FileShareServer/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 762
    invoke-virtual {v8}, Lcom/samsung/android/app/FileShareServer/InboundInfo;->getTotalFailedCount()I

    move-result v13

    if-nez v13, :cond_5

    invoke-virtual {v8}, Lcom/samsung/android/app/FileShareServer/InboundInfo;->getTotalSuccessCount()I

    move-result v13

    const/4 v14, 0x1

    if-lt v13, v14, :cond_5

    .line 763
    const v13, 0x7f060009

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/samsung/android/app/FileShareServer/ServerService;->getString(I)Ljava/lang/String;

    move-result-object v11

    .line 765
    new-instance v9, Landroid/content/Intent;

    .end local v9    # "it":Landroid/content/Intent;
    const-class v13, Lcom/samsung/android/app/FileShareServer/InboundActivity;

    move-object/from16 v0, p0

    invoke-direct {v9, v0, v13}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 766
    .restart local v9    # "it":Landroid/content/Intent;
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "com.samsung.android.app.FileShareServer.RECEIVE_RESULT"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v8}, Lcom/samsung/android/app/FileShareServer/InboundInfo;->getInboundID()I

    move-result v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v9, v13}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 767
    const/high16 v13, 0x10800000

    invoke-virtual {v9, v13}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 794
    :goto_2
    invoke-virtual {v8}, Lcom/samsung/android/app/FileShareServer/InboundInfo;->getFileList()Ljava/util/ArrayList;

    move-result-object v13

    if-eqz v13, :cond_2

    invoke-virtual {v8}, Lcom/samsung/android/app/FileShareServer/InboundInfo;->getFileList()Ljava/util/ArrayList;

    move-result-object v13

    invoke-virtual {v13}, Ljava/util/ArrayList;->size()I

    move-result v13

    if-lez v13, :cond_2

    .line 795
    invoke-virtual {v8}, Lcom/samsung/android/app/FileShareServer/InboundInfo;->getFileList()Ljava/util/ArrayList;

    move-result-object v13

    invoke-virtual {v13}, Ljava/util/ArrayList;->size()I

    move-result v13

    const/4 v14, 0x1

    if-ne v13, v14, :cond_8

    .line 796
    invoke-virtual {v8}, Lcom/samsung/android/app/FileShareServer/InboundInfo;->getFileName()Ljava/lang/String;

    move-result-object v10

    .line 803
    :cond_2
    :goto_3
    const-string v13, "inboundId"

    invoke-virtual {v8}, Lcom/samsung/android/app/FileShareServer/InboundInfo;->getInboundID()I

    move-result v14

    invoke-virtual {v9, v13, v14}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 804
    const-string v13, "ServerService"

    const-string v14, "AddResultNotification"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "inboundId = "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v8}, Lcom/samsung/android/app/FileShareServer/InboundInfo;->getInboundID()I

    move-result v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v13, v14, v15}, Lcom/samsung/android/app/FileShareServer/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 805
    invoke-virtual {v8}, Lcom/samsung/android/app/FileShareServer/InboundInfo;->getInboundID()I

    move-result v13

    const/4 v14, 0x0

    move-object/from16 v0, p0

    invoke-static {v0, v13, v9, v14}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v4

    .line 807
    .local v4, "contentIntent":Landroid/app/PendingIntent;
    new-instance v3, Landroid/content/Intent;

    const-class v13, Lcom/samsung/android/app/FileShareServer/ServerService;

    move-object/from16 v0, p0

    invoke-direct {v3, v0, v13}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 808
    .local v3, "clearIntent":Landroid/content/Intent;
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "com.samsung.android.app.FileShareServer.REMOVE_INBOUND_INFO"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v8}, Lcom/samsung/android/app/FileShareServer/InboundInfo;->getInboundID()I

    move-result v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v3, v13}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 809
    const-string v13, "inboundId"

    invoke-virtual {v8}, Lcom/samsung/android/app/FileShareServer/InboundInfo;->getInboundID()I

    move-result v14

    invoke-virtual {v3, v13, v14}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 810
    invoke-virtual {v8}, Lcom/samsung/android/app/FileShareServer/InboundInfo;->getInboundID()I

    move-result v13

    const/4 v14, 0x0

    move-object/from16 v0, p0

    invoke-static {v0, v13, v3, v14}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v5

    .line 812
    .local v5, "deleteIntent":Landroid/app/PendingIntent;
    invoke-virtual {v2, v4}, Landroid/app/Notification$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    .line 813
    invoke-virtual {v2, v5}, Landroid/app/Notification$Builder;->setDeleteIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    .line 814
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v14

    invoke-virtual {v2, v14, v15}, Landroid/app/Notification$Builder;->setWhen(J)Landroid/app/Notification$Builder;

    .line 815
    const/4 v13, 0x0

    invoke-virtual {v2, v13}, Landroid/app/Notification$Builder;->setAutoCancel(Z)Landroid/app/Notification$Builder;

    .line 816
    invoke-virtual {v2, v11}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    .line 817
    invoke-virtual {v2, v10}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    .line 818
    const/4 v13, 0x1

    invoke-virtual {v2, v13}, Landroid/app/Notification$Builder;->setDefaults(I)Landroid/app/Notification$Builder;

    .line 819
    invoke-virtual {v2}, Landroid/app/Notification$Builder;->build()Landroid/app/Notification;

    move-result-object v12

    .line 820
    .local v12, "notification":Landroid/app/Notification;
    iget v13, v12, Landroid/app/Notification;->flags:I

    or-int/lit8 v13, v13, 0x8

    iput v13, v12, Landroid/app/Notification;->flags:I

    .line 821
    iget v13, v12, Landroid/app/Notification;->defaults:I

    or-int/lit8 v13, v13, 0x1

    iput v13, v12, Landroid/app/Notification;->defaults:I

    .line 822
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/app/FileShareServer/ServerService;->mNotiManager:Landroid/app/NotificationManager;

    const-string v14, "FileShareServer"

    invoke-virtual {v8}, Lcom/samsung/android/app/FileShareServer/InboundInfo;->getInboundID()I

    move-result v15

    mul-int/lit8 v15, v15, 0x2

    invoke-virtual {v13, v14, v15}, Landroid/app/NotificationManager;->cancel(Ljava/lang/String;I)V

    .line 823
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/app/FileShareServer/ServerService;->mNotiManager:Landroid/app/NotificationManager;

    const-string v14, "FileShareServer"

    invoke-virtual {v8}, Lcom/samsung/android/app/FileShareServer/InboundInfo;->getInboundID()I

    move-result v15

    mul-int/lit8 v15, v15, 0x2

    add-int/lit8 v15, v15, 0x1

    invoke-virtual {v13, v14, v15, v12}, Landroid/app/NotificationManager;->notify(Ljava/lang/String;ILandroid/app/Notification;)V

    goto/16 :goto_0

    .line 746
    .end local v3    # "clearIntent":Landroid/content/Intent;
    .end local v4    # "contentIntent":Landroid/app/PendingIntent;
    .end local v5    # "deleteIntent":Landroid/app/PendingIntent;
    .end local v9    # "it":Landroid/content/Intent;
    .end local v12    # "notification":Landroid/app/Notification;
    :cond_3
    invoke-virtual {v8}, Lcom/samsung/android/app/FileShareServer/InboundInfo;->getCurrentStatus()I

    move-result v13

    const/16 v14, 0x3eb

    if-ne v13, v14, :cond_4

    .line 747
    const-string v13, "ServerService"

    const-string v14, "AddResultNotification"

    const-string v15, "receiving_canceled "

    invoke-static {v13, v14, v15}, Lcom/samsung/android/app/FileShareServer/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 748
    const v13, 0x7f060010

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/samsung/android/app/FileShareServer/ServerService;->getString(I)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v2, v13}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    .line 749
    invoke-virtual {v2, v6}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    .line 750
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v14

    invoke-virtual {v2, v14, v15}, Landroid/app/Notification$Builder;->setWhen(J)Landroid/app/Notification$Builder;

    goto/16 :goto_1

    .line 752
    :cond_4
    const-string v13, "ServerService"

    const-string v14, "AddResultNotification"

    const-string v15, "receiving_failed "

    invoke-static {v13, v14, v15}, Lcom/samsung/android/app/FileShareServer/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 753
    const v13, 0x7f060013

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/samsung/android/app/FileShareServer/ServerService;->getString(I)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v2, v13}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    .line 754
    invoke-virtual {v2, v6}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    .line 755
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v14

    invoke-virtual {v2, v14, v15}, Landroid/app/Notification$Builder;->setWhen(J)Landroid/app/Notification$Builder;

    goto/16 :goto_1

    .line 769
    .restart local v9    # "it":Landroid/content/Intent;
    :cond_5
    invoke-virtual {v8}, Lcom/samsung/android/app/FileShareServer/InboundInfo;->getTotalFailedCount()I

    move-result v13

    if-lez v13, :cond_7

    .line 770
    invoke-virtual {v8}, Lcom/samsung/android/app/FileShareServer/InboundInfo;->getCurrentStatus()I

    move-result v13

    const/16 v14, 0x3eb

    if-ne v13, v14, :cond_6

    .line 771
    const v13, 0x7f060010

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/samsung/android/app/FileShareServer/ServerService;->getString(I)Ljava/lang/String;

    move-result-object v11

    .line 776
    :goto_4
    new-instance v9, Landroid/content/Intent;

    .end local v9    # "it":Landroid/content/Intent;
    const-class v13, Lcom/samsung/android/app/FileShareServer/InboundActivity;

    move-object/from16 v0, p0

    invoke-direct {v9, v0, v13}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 777
    .restart local v9    # "it":Landroid/content/Intent;
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "com.samsung.android.app.FileShareServer.RECEIVE_RESULT"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v8}, Lcom/samsung/android/app/FileShareServer/InboundInfo;->getInboundID()I

    move-result v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v9, v13}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 778
    const/high16 v13, 0x10020000

    invoke-virtual {v9, v13}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    goto/16 :goto_2

    .line 773
    :cond_6
    const v13, 0x7f060013

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/samsung/android/app/FileShareServer/ServerService;->getString(I)Ljava/lang/String;

    move-result-object v11

    goto :goto_4

    .line 780
    :cond_7
    const-string v13, "ServerService"

    const-string v14, "AddResultNotification"

    const-string v15, "Need Check Received Count, Failed Count."

    invoke-static {v13, v14, v15}, Lcom/samsung/android/app/FileShareServer/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 782
    const-string v13, "ServerService"

    const-string v14, "AddResultNotification"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "TotalSavedCount: "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v8}, Lcom/samsung/android/app/FileShareServer/InboundInfo;->getTotalSavedCount()I

    move-result v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, "TotalCount: "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v8}, Lcom/samsung/android/app/FileShareServer/InboundInfo;->getTotalCount()I

    move-result v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v13, v14, v15}, Lcom/samsung/android/app/FileShareServer/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 786
    const v13, 0x7f060009

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/samsung/android/app/FileShareServer/ServerService;->getString(I)Ljava/lang/String;

    move-result-object v11

    .line 788
    new-instance v9, Landroid/content/Intent;

    .end local v9    # "it":Landroid/content/Intent;
    const-class v13, Lcom/samsung/android/app/FileShareServer/InboundActivity;

    move-object/from16 v0, p0

    invoke-direct {v9, v0, v13}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 789
    .restart local v9    # "it":Landroid/content/Intent;
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "com.samsung.android.app.FileShareServer.RECEIVE_RESULT"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v8}, Lcom/samsung/android/app/FileShareServer/InboundInfo;->getInboundID()I

    move-result v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v9, v13}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 790
    const/high16 v13, 0x10800000

    invoke-virtual {v9, v13}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    goto/16 :goto_2

    .line 798
    :cond_8
    const v14, 0x7f060017

    const/4 v13, 0x2

    new-array v15, v13, [Ljava/lang/Object;

    const/16 v16, 0x0

    invoke-virtual {v8}, Lcom/samsung/android/app/FileShareServer/InboundInfo;->getFileList()Ljava/util/ArrayList;

    move-result-object v13

    const/16 v17, 0x0

    move/from16 v0, v17

    invoke-virtual {v13, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Ljava/io/File;

    invoke-virtual {v13}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v13

    aput-object v13, v15, v16

    const/4 v13, 0x1

    invoke-virtual {v8}, Lcom/samsung/android/app/FileShareServer/InboundInfo;->getFileList()Ljava/util/ArrayList;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/util/ArrayList;->size()I

    move-result v16

    add-int/lit8 v16, v16, -0x1

    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    aput-object v16, v15, v13

    move-object/from16 v0, p0

    invoke-virtual {v0, v14, v15}, Lcom/samsung/android/app/FileShareServer/ServerService;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    goto/16 :goto_3
.end method

.method static synthetic access$000(Lcom/samsung/android/app/FileShareServer/ServerService;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/FileShareServer/ServerService;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/samsung/android/app/FileShareServer/ServerService;->mThemeContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$100()Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 44
    sget-object v0, Lcom/samsung/android/app/FileShareServer/ServerService;->mWorkingList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$200(Lcom/samsung/android/app/FileShareServer/ServerService;)Landroid/app/NotificationManager;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/FileShareServer/ServerService;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/samsung/android/app/FileShareServer/ServerService;->mNotiManager:Landroid/app/NotificationManager;

    return-object v0
.end method

.method static synthetic access$300(Lcom/samsung/android/app/FileShareServer/ServerService;II)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/FileShareServer/ServerService;
    .param p1, "x1"    # I
    .param p2, "x2"    # I

    .prologue
    .line 44
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/app/FileShareServer/ServerService;->setErrorInboundInfo(II)V

    return-void
.end method

.method static synthetic access$400(Lcom/samsung/android/app/FileShareServer/ServerService;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/FileShareServer/ServerService;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 44
    invoke-direct {p0, p1}, Lcom/samsung/android/app/FileShareServer/ServerService;->AddResultNotification(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$500(Lcom/samsung/android/app/FileShareServer/ServerService;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/FileShareServer/ServerService;

    .prologue
    .line 44
    invoke-direct {p0}, Lcom/samsung/android/app/FileShareServer/ServerService;->releaseWakeLock()V

    return-void
.end method

.method private acquireWakeLock()V
    .locals 5

    .prologue
    .line 993
    const-string v1, "ServerService"

    const-string v2, "startInboundForeground"

    const-string v3, "acquire Wake lock"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/app/FileShareServer/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 995
    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/app/FileShareServer/ServerService;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    if-eqz v1, :cond_1

    .line 996
    iget-object v1, p0, Lcom/samsung/android/app/FileShareServer/ServerService;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 997
    iget-object v1, p0, Lcom/samsung/android/app/FileShareServer/ServerService;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 999
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/app/FileShareServer/ServerService;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->acquire()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1004
    :cond_1
    :goto_0
    return-void

    .line 1001
    :catch_0
    move-exception v0

    .line 1002
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "ServerService"

    const-string v2, "startInboundForeground"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Exception on acquireWakeLock"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/samsung/android/app/FileShareServer/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private checkForDeviceRunning()Z
    .locals 5

    .prologue
    .line 855
    sget-object v3, Lcom/samsung/android/app/FileShareServer/ServerService;->mWorkingList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-lez v3, :cond_2

    .line 856
    sget-object v3, Lcom/samsung/android/app/FileShareServer/ServerService;->mWorkingList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 857
    .local v2, "size":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v2, :cond_2

    .line 858
    sget-object v3, Lcom/samsung/android/app/FileShareServer/ServerService;->mWorkingList:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/app/FileShareServer/InboundInfo;

    .line 859
    .local v1, "info":Lcom/samsung/android/app/FileShareServer/InboundInfo;
    invoke-virtual {v1}, Lcom/samsung/android/app/FileShareServer/InboundInfo;->getCurrentStatus()I

    move-result v3

    const/16 v4, 0x3ea

    if-eq v3, v4, :cond_0

    invoke-virtual {v1}, Lcom/samsung/android/app/FileShareServer/InboundInfo;->getCurrentStatus()I

    move-result v3

    const/16 v4, 0x3ed

    if-eq v3, v4, :cond_0

    invoke-virtual {v1}, Lcom/samsung/android/app/FileShareServer/InboundInfo;->getCurrentStatus()I

    move-result v3

    const/16 v4, 0x3e8

    if-ne v3, v4, :cond_1

    .line 862
    :cond_0
    const/4 v3, 0x1

    .line 866
    .end local v0    # "i":I
    .end local v1    # "info":Lcom/samsung/android/app/FileShareServer/InboundInfo;
    .end local v2    # "size":I
    :goto_1
    return v3

    .line 857
    .restart local v0    # "i":I
    .restart local v1    # "info":Lcom/samsung/android/app/FileShareServer/InboundInfo;
    .restart local v2    # "size":I
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 866
    .end local v0    # "i":I
    .end local v1    # "info":Lcom/samsung/android/app/FileShareServer/InboundInfo;
    .end local v2    # "size":I
    :cond_2
    const/4 v3, 0x0

    goto :goto_1
.end method

.method private checkIsServiceRunning()V
    .locals 2

    .prologue
    .line 1060
    iget-object v0, p0, Lcom/samsung/android/app/FileShareServer/ServerService;->mThemeContext:Landroid/content/Context;

    sget v1, Lcom/samsung/android/app/FileShareServer/Utils;->mServerProperty:I

    invoke-static {v0, v1}, Lcom/samsung/android/app/FileShareServer/Utils;->setServerIsRunning(Landroid/content/Context;I)V

    .line 1061
    iget-object v0, p0, Lcom/samsung/android/app/FileShareServer/ServerService;->mThemeContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/samsung/android/app/FileShareServer/Utils;->getIsClientRunning(Landroid/content/Context;)I

    .line 1062
    iget-object v0, p0, Lcom/samsung/android/app/FileShareServer/ServerService;->mThemeContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/samsung/android/app/FileShareServer/Utils;->getIsQuickConnectRunning(Landroid/content/Context;)I

    .line 1063
    return-void
.end method

.method private convertInboundInfo(Ljava/lang/String;)Lcom/samsung/android/app/FileShareServer/InboundInfo;
    .locals 7
    .param p1, "sessionId"    # Ljava/lang/String;

    .prologue
    .line 644
    new-instance v0, Lcom/samsung/android/app/FileShareServer/InboundInfo;

    invoke-direct {v0, p0}, Lcom/samsung/android/app/FileShareServer/InboundInfo;-><init>(Landroid/content/Context;)V

    .line 645
    .local v0, "inboundInfo":Lcom/samsung/android/app/FileShareServer/InboundInfo;
    const-string v3, "ServerService"

    const-string v4, "convertInboundInfo"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "session ID = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/samsung/android/app/FileShareServer/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 646
    new-instance v1, Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceSessionInfo;

    invoke-direct {v1}, Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceSessionInfo;-><init>()V

    .line 647
    .local v1, "session":Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceSessionInfo;
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 648
    .local v2, "transportInfos":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceTransportInfo;>;"
    iget-object v3, p0, Lcom/samsung/android/app/FileShareServer/ServerService;->mFileShareDevice:Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper;

    invoke-virtual {v3, p1, v1, v2}, Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper;->getSession(Ljava/lang/String;Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceSessionInfo;Ljava/util/ArrayList;)I

    move-result v3

    if-eqz v3, :cond_0

    .line 649
    const-string v3, "ServerService"

    const-string v4, "convertInboundInfo"

    const-string v5, "Cannot get session info!"

    invoke-static {v3, v4, v5}, Lcom/samsung/android/app/FileShareServer/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 650
    const/4 v0, 0x0

    .line 658
    .end local v0    # "inboundInfo":Lcom/samsung/android/app/FileShareServer/InboundInfo;
    :goto_0
    return-object v0

    .line 652
    .restart local v0    # "inboundInfo":Lcom/samsung/android/app/FileShareServer/InboundInfo;
    :cond_0
    invoke-virtual {v0, p1}, Lcom/samsung/android/app/FileShareServer/InboundInfo;->setSessionID(Ljava/lang/String;)V

    .line 653
    iget-wide v4, v1, Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceSessionInfo;->totalSize:J

    invoke-virtual {v0, v4, v5}, Lcom/samsung/android/app/FileShareServer/InboundInfo;->setTotalSize(J)V

    .line 654
    iget-wide v4, v1, Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceSessionInfo;->totalCount:J

    long-to-int v3, v4

    invoke-virtual {v0, v3}, Lcom/samsung/android/app/FileShareServer/InboundInfo;->setTotalCount(I)V

    .line 655
    const/16 v3, 0x3e8

    invoke-virtual {v0, v3}, Lcom/samsung/android/app/FileShareServer/InboundInfo;->setCurrentStatus(I)V

    .line 656
    iget-object v3, v1, Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceSessionInfo;->friendlyName:Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/samsung/android/app/FileShareServer/InboundInfo;->setSenderName(Ljava/lang/String;)V

    .line 657
    iget-object v3, v1, Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceSessionInfo;->transportDescription:Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/samsung/android/app/FileShareServer/InboundInfo;->setFileName(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private findCurrentSession(Ljava/lang/String;)I
    .locals 8
    .param p1, "id"    # Ljava/lang/String;

    .prologue
    .line 839
    :try_start_0
    sget-object v4, Lcom/samsung/android/app/FileShareServer/ServerService;->mWorkingList:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 840
    .local v3, "size":I
    const/4 v1, -0x1

    .line 841
    .local v1, "idx":I
    const/4 v1, 0x0

    :goto_0
    if-ge v1, v3, :cond_1

    .line 842
    sget-object v4, Lcom/samsung/android/app/FileShareServer/ServerService;->mWorkingList:Ljava/util/ArrayList;

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/app/FileShareServer/InboundInfo;

    .line 843
    .local v2, "info":Lcom/samsung/android/app/FileShareServer/InboundInfo;
    invoke-virtual {v2}, Lcom/samsung/android/app/FileShareServer/InboundInfo;->getSessionID()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 851
    .end local v1    # "idx":I
    .end local v2    # "info":Lcom/samsung/android/app/FileShareServer/InboundInfo;
    .end local v3    # "size":I
    :goto_1
    return v1

    .line 841
    .restart local v1    # "idx":I
    .restart local v2    # "info":Lcom/samsung/android/app/FileShareServer/InboundInfo;
    .restart local v3    # "size":I
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 847
    .end local v2    # "info":Lcom/samsung/android/app/FileShareServer/InboundInfo;
    :cond_1
    const-string v4, "ServerService"

    const-string v5, "findCurrentSession"

    const-string v6, "Can\'t find session"

    invoke-static {v4, v5, v6}, Lcom/samsung/android/app/FileShareServer/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 851
    .end local v1    # "idx":I
    .end local v3    # "size":I
    :goto_2
    const/4 v1, -0x1

    goto :goto_1

    .line 848
    :catch_0
    move-exception v0

    .line 849
    .local v0, "e":Ljava/lang/Exception;
    const-string v4, "ServerService"

    const-string v5, "findCurrentSession"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Exception on findCurrentSession "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/samsung/android/app/FileShareServer/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2
.end method

.method private getNotiCntPreference()I
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1035
    const-string v2, "notiCntPref"

    invoke-virtual {p0, v2, v3}, Lcom/samsung/android/app/FileShareServer/ServerService;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 1036
    .local v1, "prefs":Landroid/content/SharedPreferences;
    const/4 v0, 0x0

    .line 1037
    .local v0, "inboundId":I
    const-string v2, "notiCnt"

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 1041
    return v0
.end method

.method private getSavePath()Ljava/lang/String;
    .locals 5

    .prologue
    .line 966
    const v1, 0x7f06001b

    invoke-virtual {p0, v1}, Lcom/samsung/android/app/FileShareServer/ServerService;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/os/Environment;->getExternalStoragePublicDirectory(Ljava/lang/String;)Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    .line 968
    .local v0, "savePath":Ljava/lang/String;
    const-string v1, "ServerService"

    const-string v2, "getSavePath"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "SavePath "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/samsung/android/app/FileShareServer/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 969
    return-object v0
.end method

.method private manageProcessForeground(Z)V
    .locals 4
    .param p1, "start"    # Z

    .prologue
    .line 1045
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    move-result-object v1

    .line 1047
    .local v1, "mAm":Landroid/app/IActivityManager;
    if-eqz v1, :cond_0

    :try_start_0
    iget-object v2, p0, Lcom/samsung/android/app/FileShareServer/ServerService;->mBinder:Landroid/os/IBinder;

    if-eqz v2, :cond_0

    .line 1048
    iget-object v2, p0, Lcom/samsung/android/app/FileShareServer/ServerService;->mBinder:Landroid/os/IBinder;

    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v3

    invoke-interface {v1, v2, v3, p1}, Landroid/app/IActivityManager;->setProcessForeground(Landroid/os/IBinder;IZ)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1053
    :cond_0
    :goto_0
    return-void

    .line 1050
    :catch_0
    move-exception v0

    .line 1051
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method private releaseWakeLock()V
    .locals 6

    .prologue
    .line 1008
    const/4 v1, 0x0

    .local v1, "idx":I
    :goto_0
    :try_start_0
    sget-object v2, Lcom/samsung/android/app/FileShareServer/ServerService;->mWorkingList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v1, v2, :cond_2

    .line 1009
    sget-object v2, Lcom/samsung/android/app/FileShareServer/ServerService;->mWorkingList:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/app/FileShareServer/InboundInfo;

    invoke-virtual {v2}, Lcom/samsung/android/app/FileShareServer/InboundInfo;->getCurrentStatus()I

    move-result v2

    const/16 v3, 0x3ea

    if-ne v2, v3, :cond_1

    .line 1010
    const-string v3, "ServerService"

    const-string v4, "releaseWakelock"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Any sessions are ongoing ["

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget-object v2, Lcom/samsung/android/app/FileShareServer/ServerService;->mWorkingList:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/app/FileShareServer/InboundInfo;

    invoke-virtual {v2}, Lcom/samsung/android/app/FileShareServer/InboundInfo;->getInboundID()I

    move-result v2

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, "]"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v4, v2}, Lcom/samsung/android/app/FileShareServer/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1025
    :cond_0
    :goto_1
    return-void

    .line 1008
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1015
    :cond_2
    iget-object v2, p0, Lcom/samsung/android/app/FileShareServer/ServerService;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/samsung/android/app/FileShareServer/ServerService;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v2}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1016
    const-string v2, "ServerService"

    const-string v3, "releaseWakelock"

    const-string v4, ""

    invoke-static {v2, v3, v4}, Lcom/samsung/android/app/FileShareServer/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1017
    iget-object v2, p0, Lcom/samsung/android/app/FileShareServer/ServerService;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v2}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 1019
    const/4 v2, 0x0

    invoke-direct {p0, v2}, Lcom/samsung/android/app/FileShareServer/ServerService;->manageProcessForeground(Z)V

    .line 1020
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/samsung/android/app/FileShareServer/ServerService;->mbForeground:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 1022
    :catch_0
    move-exception v0

    .line 1023
    .local v0, "e":Ljava/lang/Exception;
    const-string v2, "ServerService"

    const-string v3, "releaseWakelock"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Exception on releaseWakeLock"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/samsung/android/app/FileShareServer/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method private sessionTransportProgress(Ljava/lang/String;Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceTransportInfo;)V
    .locals 8
    .param p1, "sessionId"    # Ljava/lang/String;
    .param p2, "transport"    # Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceTransportInfo;

    .prologue
    .line 562
    invoke-direct {p0, p1}, Lcom/samsung/android/app/FileShareServer/ServerService;->findCurrentSession(Ljava/lang/String;)I

    move-result v0

    .line 563
    .local v0, "idx":I
    const/4 v2, -0x1

    if-ne v0, v2, :cond_1

    .line 564
    const-string v2, "ServerService"

    const-string v3, "sessionTransportProgress"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Can\'t find "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/samsung/android/app/FileShareServer/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 584
    :cond_0
    :goto_0
    return-void

    .line 566
    :cond_1
    sget-object v2, Lcom/samsung/android/app/FileShareServer/ServerService;->mWorkingList:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/app/FileShareServer/InboundInfo;

    .line 567
    .local v1, "info":Lcom/samsung/android/app/FileShareServer/InboundInfo;
    invoke-virtual {v1}, Lcom/samsung/android/app/FileShareServer/InboundInfo;->getCurrentStatus()I

    move-result v2

    const/16 v3, 0x3eb

    if-eq v2, v3, :cond_0

    invoke-virtual {v1}, Lcom/samsung/android/app/FileShareServer/InboundInfo;->getCurrentStatus()I

    move-result v2

    const/16 v3, 0x3ec

    if-eq v2, v3, :cond_0

    .line 571
    if-eqz p2, :cond_2

    .line 572
    const-string v2, "ServerService"

    const-string v3, "sessionTransportProgress"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "sessionId : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", downloadLength["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-wide v6, p2, Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceTransportInfo;->downloadLength:J

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "]"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/samsung/android/app/FileShareServer/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 574
    sget-object v2, Lcom/samsung/android/app/FileShareServer/ServerService;->mWorkingList:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/app/FileShareServer/InboundInfo;

    iget-wide v4, p2, Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceTransportInfo;->downloadLength:J

    invoke-virtual {v2, v4, v5}, Lcom/samsung/android/app/FileShareServer/InboundInfo;->setTransportSize(J)V

    .line 575
    sget-object v2, Lcom/samsung/android/app/FileShareServer/ServerService;->mWorkingList:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/app/FileShareServer/InboundInfo;

    iget-object v3, p2, Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceTransportInfo;->name:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/samsung/android/app/FileShareServer/InboundInfo;->setFileName(Ljava/lang/String;)V

    .line 580
    const/16 v2, 0x3ea

    invoke-virtual {v1, v2}, Lcom/samsung/android/app/FileShareServer/InboundInfo;->setCurrentStatus(I)V

    .line 581
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/samsung/android/app/FileShareServer/ServerService;->mIsUpdate:Z

    .line 582
    invoke-direct {p0, p1}, Lcom/samsung/android/app/FileShareServer/ServerService;->AddOngoing(Ljava/lang/String;)V

    goto :goto_0

    .line 577
    :cond_2
    const-string v2, "ServerService"

    const-string v3, "sessionTransportProgress"

    const-string v4, "Cannot get transport info!"

    invoke-static {v2, v3, v4}, Lcom/samsung/android/app/FileShareServer/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private sessionTransportStart(Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceSessionInfo;Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceTransportInfo;)V
    .locals 6
    .param p1, "session"    # Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceSessionInfo;
    .param p2, "transport"    # Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceTransportInfo;

    .prologue
    .line 539
    const-string v2, "ServerService"

    const-string v3, "sessionTransportStart()"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "SessionID is : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p1, Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceSessionInfo;->sessionId:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/samsung/android/app/FileShareServer/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 540
    iget-object v2, p1, Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceSessionInfo;->sessionId:Ljava/lang/String;

    invoke-direct {p0, v2}, Lcom/samsung/android/app/FileShareServer/ServerService;->findCurrentSession(Ljava/lang/String;)I

    move-result v0

    .line 541
    .local v0, "idx":I
    const/4 v2, -0x1

    if-ne v0, v2, :cond_0

    .line 542
    const-string v2, "ServerService"

    const-string v3, "sessionTransportStart"

    const-string v4, "findCurrentSession return -1!"

    invoke-static {v2, v3, v4}, Lcom/samsung/android/app/FileShareServer/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 557
    :goto_0
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/samsung/android/app/FileShareServer/ServerService;->mIsUpdate:Z

    .line 558
    iget-object v2, p1, Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceSessionInfo;->sessionId:Ljava/lang/String;

    invoke-direct {p0, v2}, Lcom/samsung/android/app/FileShareServer/ServerService;->AddOngoing(Ljava/lang/String;)V

    .line 559
    return-void

    .line 544
    :cond_0
    sget-object v2, Lcom/samsung/android/app/FileShareServer/ServerService;->mWorkingList:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/app/FileShareServer/InboundInfo;

    .line 545
    .local v1, "info":Lcom/samsung/android/app/FileShareServer/InboundInfo;
    iget-object v2, p1, Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceSessionInfo;->friendlyName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/samsung/android/app/FileShareServer/InboundInfo;->setSenderName(Ljava/lang/String;)V

    .line 546
    iget-wide v2, p1, Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceSessionInfo;->totalSize:J

    invoke-virtual {v1, v2, v3}, Lcom/samsung/android/app/FileShareServer/InboundInfo;->setTotalSize(J)V

    .line 547
    iget-wide v2, p1, Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceSessionInfo;->totalCount:J

    long-to-int v2, v2

    invoke-virtual {v1, v2}, Lcom/samsung/android/app/FileShareServer/InboundInfo;->setTotalCount(I)V

    .line 548
    if-eqz p2, :cond_1

    .line 549
    iget-wide v2, p2, Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceTransportInfo;->downloadLength:J

    invoke-virtual {v1, v2, v3}, Lcom/samsung/android/app/FileShareServer/InboundInfo;->setCurrentSize(J)V

    .line 550
    iget-object v2, p2, Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceTransportInfo;->name:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/samsung/android/app/FileShareServer/InboundInfo;->setFileName(Ljava/lang/String;)V

    .line 554
    :goto_1
    const/16 v2, 0x3ea

    invoke-virtual {v1, v2}, Lcom/samsung/android/app/FileShareServer/InboundInfo;->setCurrentStatus(I)V

    .line 555
    invoke-virtual {v1}, Lcom/samsung/android/app/FileShareServer/InboundInfo;->update()V

    goto :goto_0

    .line 552
    :cond_1
    const-string v2, "ServerService"

    const-string v3, "sessionTransportStart"

    const-string v4, "cannot get transport info!"

    invoke-static {v2, v3, v4}, Lcom/samsung/android/app/FileShareServer/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method private setBroadcastReceiver()V
    .locals 6

    .prologue
    .line 871
    :try_start_0
    iget-boolean v2, p0, Lcom/samsung/android/app/FileShareServer/ServerService;->bRegisterReceiver:Z

    if-nez v2, :cond_0

    .line 872
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 873
    .local v0, "DirectFilter":Landroid/content/IntentFilter;
    const-string v2, "android.net.wifi.p2p.CONNECTION_STATE_CHANGE"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 874
    iget-object v2, p0, Lcom/samsung/android/app/FileShareServer/ServerService;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v2, v0}, Lcom/samsung/android/app/FileShareServer/ServerService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 875
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/samsung/android/app/FileShareServer/ServerService;->bRegisterReceiver:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 880
    .end local v0    # "DirectFilter":Landroid/content/IntentFilter;
    :cond_0
    :goto_0
    return-void

    .line 877
    :catch_0
    move-exception v1

    .line 878
    .local v1, "e":Ljava/lang/Exception;
    const-string v2, "ServerService"

    const-string v3, "setBroadcastReceiver"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Exception on setBroadcastReceiver"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/samsung/android/app/FileShareServer/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private setErrorInboundInfo(II)V
    .locals 6
    .param p1, "index"    # I
    .param p2, "status"    # I

    .prologue
    .line 828
    sget-object v2, Lcom/samsung/android/app/FileShareServer/ServerService;->mWorkingList:Ljava/util/ArrayList;

    invoke-virtual {v2, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/app/FileShareServer/InboundInfo;

    .line 829
    .local v1, "info":Lcom/samsung/android/app/FileShareServer/InboundInfo;
    invoke-virtual {v1}, Lcom/samsung/android/app/FileShareServer/InboundInfo;->getCurrentIndex()I

    move-result v2

    add-int/lit8 v0, v2, -0x1

    .line 830
    .local v0, "downloadedCount":I
    invoke-virtual {v1, p2}, Lcom/samsung/android/app/FileShareServer/InboundInfo;->setCurrentStatus(I)V

    .line 831
    invoke-virtual {v1, v0}, Lcom/samsung/android/app/FileShareServer/InboundInfo;->setCurrentIndex(I)V

    .line 832
    invoke-virtual {v1}, Lcom/samsung/android/app/FileShareServer/InboundInfo;->getTotalCount()I

    move-result v2

    invoke-virtual {v1}, Lcom/samsung/android/app/FileShareServer/InboundInfo;->getTotalSavedCount()I

    move-result v3

    sub-int/2addr v2, v3

    invoke-virtual {v1, v2}, Lcom/samsung/android/app/FileShareServer/InboundInfo;->setTotalFailedCount(I)V

    .line 833
    const-string v2, "ServerService"

    const-string v3, "setErrorInboundInfo"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "TotalFailedCount = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Lcom/samsung/android/app/FileShareServer/InboundInfo;->getTotalFailedCount()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/samsung/android/app/FileShareServer/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 834
    invoke-virtual {v1}, Lcom/samsung/android/app/FileShareServer/InboundInfo;->update()V

    .line 835
    return-void
.end method

.method private setNotiCntPreference(I)V
    .locals 4
    .param p1, "notiCnt"    # I

    .prologue
    .line 1028
    const-string v2, "notiCntPref"

    const/4 v3, 0x0

    invoke-virtual {p0, v2, v3}, Lcom/samsung/android/app/FileShareServer/ServerService;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 1029
    .local v1, "prefs":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 1030
    .local v0, "ed":Landroid/content/SharedPreferences$Editor;
    const-string v2, "notiCnt"

    invoke-interface {v0, v2, p1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 1031
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 1032
    return-void
.end method

.method private setThemeContext()V
    .locals 2

    .prologue
    .line 1056
    new-instance v0, Landroid/view/ContextThemeWrapper;

    const/high16 v1, 0x7f070000

    invoke-direct {v0, p0, v1}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/samsung/android/app/FileShareServer/ServerService;->mThemeContext:Landroid/content/Context;

    .line 1057
    return-void
.end method

.method private startFileShareDevice()V
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/net/SocketException;
        }
    .end annotation

    .prologue
    .line 587
    invoke-direct {p0}, Lcom/samsung/android/app/FileShareServer/ServerService;->checkForDeviceRunning()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 588
    iget-object v4, p0, Lcom/samsung/android/app/FileShareServer/ServerService;->mFileShareDevice:Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper;

    if-eqz v4, :cond_0

    .line 589
    const-string v4, "ServerService"

    const-string v5, "startFileShareDevice"

    const-string v6, "There is DeviceRunning & mFileShareDevice() is not null"

    invoke-static {v4, v5, v6}, Lcom/samsung/android/app/FileShareServer/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 622
    :cond_0
    :goto_0
    return-void

    .line 594
    :cond_1
    iget-object v4, p0, Lcom/samsung/android/app/FileShareServer/ServerService;->mFileShareDevice:Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper;

    if-eqz v4, :cond_2

    .line 595
    const-string v4, "ServerService"

    const-string v5, "startFileShareDevice"

    const-string v6, "mFileShareDevice() is not null. return!"

    invoke-static {v4, v5, v6}, Lcom/samsung/android/app/FileShareServer/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 599
    :cond_2
    const-string v4, "ServerService"

    const-string v5, "startFileShareDevice"

    const-string v6, ""

    invoke-static {v4, v5, v6}, Lcom/samsung/android/app/FileShareServer/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 601
    invoke-static {}, Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper;->getInstance()Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper;

    move-result-object v4

    iput-object v4, p0, Lcom/samsung/android/app/FileShareServer/ServerService;->mFileShareDevice:Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper;

    .line 602
    const/4 v1, 0x0

    .line 603
    .local v1, "deviceName":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/samsung/android/app/FileShareServer/ServerService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string v5, "device_name"

    invoke-static {v4, v5}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 604
    if-nez v1, :cond_3

    .line 605
    sget-object v1, Landroid/os/Build;->MODEL:Ljava/lang/String;

    .line 607
    :cond_3
    invoke-virtual {p0}, Lcom/samsung/android/app/FileShareServer/ServerService;->getFilesDir()Ljava/io/File;

    move-result-object v4

    invoke-virtual {v4}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v0

    .line 608
    .local v0, "deploymentFilePath":Ljava/lang/String;
    const-string v4, "ServerService"

    const-string v5, "startFileShareDevice"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "deploymentFilePath : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/samsung/android/app/FileShareServer/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 609
    iget-object v4, p0, Lcom/samsung/android/app/FileShareServer/ServerService;->mFileShareDevice:Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper;

    invoke-virtual {v4, p0, v0}, Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper;->initialize(Landroid/content/Context;Ljava/lang/String;)I

    move-result v2

    .line 610
    .local v2, "result":I
    const/4 v4, -0x2

    if-ne v2, v4, :cond_4

    .line 611
    iget-object v4, p0, Lcom/samsung/android/app/FileShareServer/ServerService;->mThemeContext:Landroid/content/Context;

    const v5, 0x7f06000e

    const/4 v6, 0x1

    invoke-static {v4, v5, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/Toast;->show()V

    .line 613
    :cond_4
    invoke-direct {p0}, Lcom/samsung/android/app/FileShareServer/ServerService;->getSavePath()Ljava/lang/String;

    move-result-object v3

    .line 614
    .local v3, "savePath":Ljava/lang/String;
    new-instance v4, Ljava/io/File;

    invoke-direct {v4, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    iput-object v4, p0, Lcom/samsung/android/app/FileShareServer/ServerService;->mSaveFilePath:Ljava/io/File;

    .line 615
    const-string v4, "ServerService"

    const-string v5, "startFileShareDevice"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "save path : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/samsung/android/app/FileShareServer/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 616
    iget-object v4, p0, Lcom/samsung/android/app/FileShareServer/ServerService;->mFileShareDevice:Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper;

    invoke-virtual {v4, v3}, Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper;->start(Ljava/lang/String;)I

    .line 617
    iget-object v4, p0, Lcom/samsung/android/app/FileShareServer/ServerService;->mFileShareDevice:Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper;

    invoke-virtual {v4, p0}, Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper;->addDeviceEventListener(Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper$IDeviceEvent;)V

    .line 618
    iget-object v4, p0, Lcom/samsung/android/app/FileShareServer/ServerService;->mFileShareDevice:Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper;

    invoke-virtual {v4, p0}, Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper;->addSessionEventListener(Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper$ISessionEvent;)V

    .line 619
    iget-object v4, p0, Lcom/samsung/android/app/FileShareServer/ServerService;->mFileShareDevice:Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper;

    invoke-virtual {v4, v1}, Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper;->setFreindlyName(Ljava/lang/String;)I

    .line 620
    iget-object v4, p0, Lcom/samsung/android/app/FileShareServer/ServerService;->mFileShareDevice:Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper;

    invoke-virtual {p0}, Lcom/samsung/android/app/FileShareServer/ServerService;->getMainLooper()Landroid/os/Looper;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper;->setGUIEventListenr(Landroid/os/Looper;)V

    goto/16 :goto_0
.end method

.method private startInboundForeground()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 973
    iget-boolean v1, p0, Lcom/samsung/android/app/FileShareServer/ServerService;->mbForeground:Z

    if-eqz v1, :cond_0

    .line 990
    :goto_0
    return-void

    .line 977
    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/app/FileShareServer/ServerService;->mPowerManager:Landroid/os/PowerManager;

    if-nez v1, :cond_1

    .line 978
    const-string v1, "power"

    invoke-virtual {p0, v1}, Lcom/samsung/android/app/FileShareServer/ServerService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/PowerManager;

    iput-object v1, p0, Lcom/samsung/android/app/FileShareServer/ServerService;->mPowerManager:Landroid/os/PowerManager;

    .line 979
    iget-object v1, p0, Lcom/samsung/android/app/FileShareServer/ServerService;->mPowerManager:Landroid/os/PowerManager;

    const/4 v2, 0x1

    const-string v3, "ServerService Wake Lock"

    invoke-virtual {v1, v2, v3}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/app/FileShareServer/ServerService;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    .line 982
    :cond_1
    invoke-direct {p0}, Lcom/samsung/android/app/FileShareServer/ServerService;->acquireWakeLock()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 987
    :goto_1
    invoke-direct {p0, v5}, Lcom/samsung/android/app/FileShareServer/ServerService;->manageProcessForeground(Z)V

    .line 988
    iput-boolean v5, p0, Lcom/samsung/android/app/FileShareServer/ServerService;->mbForeground:Z

    .line 989
    const-string v1, "ServerService"

    const-string v2, "startInboundForeground"

    const-string v3, ""

    invoke-static {v1, v2, v3}, Lcom/samsung/android/app/FileShareServer/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 983
    :catch_0
    move-exception v0

    .line 984
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "ServerService"

    const-string v2, "startInboundForeground"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Exception on startInboundForeground "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/samsung/android/app/FileShareServer/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method private unregBroadcastReceiver()V
    .locals 5

    .prologue
    .line 885
    :try_start_0
    iget-boolean v1, p0, Lcom/samsung/android/app/FileShareServer/ServerService;->bRegisterReceiver:Z

    if-eqz v1, :cond_0

    .line 886
    iget-object v1, p0, Lcom/samsung/android/app/FileShareServer/ServerService;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1}, Lcom/samsung/android/app/FileShareServer/ServerService;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 887
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/samsung/android/app/FileShareServer/ServerService;->bRegisterReceiver:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 892
    :cond_0
    :goto_0
    return-void

    .line 889
    :catch_0
    move-exception v0

    .line 890
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "ServerService"

    const-string v2, "unregBroadcastReceiver"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Exception on unregBroadcastReceiver"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/samsung/android/app/FileShareServer/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public cancelSession(Ljava/lang/String;)V
    .locals 6
    .param p1, "sessionID"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/16 v5, 0x3eb

    .line 174
    const-string v1, "ServerService"

    const-string v2, "cancelSession"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "User canceled > "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/samsung/android/app/FileShareServer/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 175
    invoke-direct {p0, p1}, Lcom/samsung/android/app/FileShareServer/ServerService;->findCurrentSession(Ljava/lang/String;)I

    move-result v0

    .line 176
    .local v0, "idx":I
    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 177
    const-string v1, "ServerService"

    const-string v2, "cancelSession"

    const-string v3, "findCurrentSession return -1!"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/app/FileShareServer/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 191
    :goto_0
    return-void

    .line 179
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/app/FileShareServer/ServerService;->mFileShareDevice:Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper;

    if-eqz v1, :cond_1

    .line 180
    iget-object v1, p0, Lcom/samsung/android/app/FileShareServer/ServerService;->mFileShareDevice:Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper;

    invoke-virtual {v1, p1}, Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper;->cancelSession(Ljava/lang/String;)I

    .line 181
    invoke-direct {p0, v0, v5}, Lcom/samsung/android/app/FileShareServer/ServerService;->setErrorInboundInfo(II)V

    .line 186
    :goto_1
    const-string v1, "ServerService"

    const-string v2, "cancelSession"

    const-string v3, "DB - add"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/app/FileShareServer/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 187
    iget-object v2, p0, Lcom/samsung/android/app/FileShareServer/ServerService;->mDbAdapter:Lcom/samsung/android/app/FileShareServer/DbAdapter;

    sget-object v1, Lcom/samsung/android/app/FileShareServer/ServerService;->mWorkingList:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/app/FileShareServer/InboundInfo;

    invoke-virtual {v2, v1}, Lcom/samsung/android/app/FileShareServer/DbAdapter;->insertInboundInfo(Lcom/samsung/android/app/FileShareServer/InboundInfo;)V

    .line 188
    invoke-direct {p0, p1}, Lcom/samsung/android/app/FileShareServer/ServerService;->AddResultNotification(Ljava/lang/String;)V

    .line 189
    sget-object v1, Lcom/samsung/android/app/FileShareServer/ServerService;->mWorkingList:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/app/FileShareServer/InboundInfo;

    invoke-virtual {v1}, Lcom/samsung/android/app/FileShareServer/InboundInfo;->getInboundID()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/samsung/android/app/FileShareServer/ServerService;->destroyInbound(I)V

    goto :goto_0

    .line 183
    :cond_1
    const-string v1, "ServerService"

    const-string v2, "cancelSession"

    const-string v3, "mFileShareDevice is null;;;"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/app/FileShareServer/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 184
    invoke-direct {p0, v0, v5}, Lcom/samsung/android/app/FileShareServer/ServerService;->setErrorInboundInfo(II)V

    goto :goto_1
.end method

.method public deletedNotification()V
    .locals 5

    .prologue
    .line 524
    invoke-direct {p0}, Lcom/samsung/android/app/FileShareServer/ServerService;->getNotiCntPreference()I

    move-result v0

    .line 525
    .local v0, "notiCnt":I
    add-int/lit8 v0, v0, -0x1

    .line 526
    invoke-direct {p0, v0}, Lcom/samsung/android/app/FileShareServer/ServerService;->setNotiCntPreference(I)V

    .line 527
    const-string v1, "ServerService"

    const-string v2, "deletedNotification"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "NotiCnt = ["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/samsung/android/app/FileShareServer/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 528
    if-gtz v0, :cond_0

    iget-object v1, p0, Lcom/samsung/android/app/FileShareServer/ServerService;->mDbAdapter:Lcom/samsung/android/app/FileShareServer/DbAdapter;

    if-eqz v1, :cond_0

    .line 529
    const-string v1, "ServerService"

    const-string v2, "deletedNotification"

    const-string v3, "clearDbTable()"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/app/FileShareServer/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 530
    iget-object v1, p0, Lcom/samsung/android/app/FileShareServer/ServerService;->mDbAdapter:Lcom/samsung/android/app/FileShareServer/DbAdapter;

    invoke-virtual {v1}, Lcom/samsung/android/app/FileShareServer/DbAdapter;->clearDbTable()V

    .line 532
    :cond_0
    return-void
.end method

.method public destroyInbound(I)V
    .locals 7
    .param p1, "nId"    # I

    .prologue
    .line 505
    const-string v3, "ServerService"

    const-string v4, "destroyInbound"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "ID["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "]"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/samsung/android/app/FileShareServer/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 507
    sget-object v3, Lcom/samsung/android/app/FileShareServer/ServerService;->mWorkingList:Ljava/util/ArrayList;

    if-eqz v3, :cond_0

    const/4 v3, -0x1

    if-ne p1, v3, :cond_2

    .line 508
    :cond_0
    const-string v3, "ServerService"

    const-string v4, "destroyInbound"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\'s Workinglist is already Null! "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/samsung/android/app/FileShareServer/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 521
    :cond_1
    :goto_0
    return-void

    .line 512
    :cond_2
    :try_start_0
    sget-object v3, Lcom/samsung/android/app/FileShareServer/ServerService;->mWorkingList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/app/FileShareServer/InboundInfo;

    .line 513
    .local v2, "info":Lcom/samsung/android/app/FileShareServer/InboundInfo;
    invoke-virtual {v2}, Lcom/samsung/android/app/FileShareServer/InboundInfo;->getInboundID()I

    move-result v3

    if-ne v3, p1, :cond_3

    .line 514
    sget-object v3, Lcom/samsung/android/app/FileShareServer/ServerService;->mWorkingList:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 518
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v2    # "info":Lcom/samsung/android/app/FileShareServer/InboundInfo;
    :catch_0
    move-exception v0

    .line 519
    .local v0, "e":Ljava/lang/Exception;
    const-string v3, "ServerService"

    const-string v4, "destroyInbound"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Exception on destroyInbound "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/samsung/android/app/FileShareServer/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public getInboundInfo(I)Lcom/samsung/android/app/FileShareServer/InboundInfo;
    .locals 6
    .param p1, "InboundID"    # I

    .prologue
    .line 163
    const-string v2, "ServerService"

    const-string v3, "getInboundInfo"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "InboundID > "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/samsung/android/app/FileShareServer/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 164
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    sget-object v2, Lcom/samsung/android/app/FileShareServer/ServerService;->mWorkingList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v0, v2, :cond_1

    .line 165
    sget-object v2, Lcom/samsung/android/app/FileShareServer/ServerService;->mWorkingList:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/app/FileShareServer/InboundInfo;

    .line 166
    .local v1, "info":Lcom/samsung/android/app/FileShareServer/InboundInfo;
    invoke-virtual {v1}, Lcom/samsung/android/app/FileShareServer/InboundInfo;->getInboundID()I

    move-result v2

    if-ne v2, p1, :cond_0

    .line 170
    .end local v1    # "info":Lcom/samsung/android/app/FileShareServer/InboundInfo;
    :goto_1
    return-object v1

    .line 164
    .restart local v1    # "info":Lcom/samsung/android/app/FileShareServer/InboundInfo;
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 170
    .end local v1    # "info":Lcom/samsung/android/app/FileShareServer/InboundInfo;
    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 4
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 127
    const-string v0, "ServerService"

    const-string v1, "onBind"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "intent.getAction() "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/samsung/android/app/FileShareServer/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 128
    iget-object v0, p0, Lcom/samsung/android/app/FileShareServer/ServerService;->mBinder:Landroid/os/IBinder;

    return-object v0
.end method

.method public onCreate()V
    .locals 5

    .prologue
    .line 68
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 69
    invoke-direct {p0}, Lcom/samsung/android/app/FileShareServer/ServerService;->setThemeContext()V

    .line 70
    new-instance v1, Lcom/samsung/android/app/FileShareServer/DbAdapter;

    invoke-direct {v1, p0}, Lcom/samsung/android/app/FileShareServer/DbAdapter;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/samsung/android/app/FileShareServer/ServerService;->mDbAdapter:Lcom/samsung/android/app/FileShareServer/DbAdapter;

    .line 72
    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/app/FileShareServer/ServerService;->mDbAdapter:Lcom/samsung/android/app/FileShareServer/DbAdapter;

    invoke-virtual {v1}, Lcom/samsung/android/app/FileShareServer/DbAdapter;->open()Lcom/samsung/android/app/FileShareServer/DbAdapter;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 76
    :goto_0
    const-string v1, "ServerService"

    const-string v2, "onCreate"

    const-string v3, ""

    invoke-static {v1, v2, v3}, Lcom/samsung/android/app/FileShareServer/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 77
    return-void

    .line 73
    :catch_0
    move-exception v0

    .line 74
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "ServerService"

    const-string v2, "onCreate"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "exception "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/samsung/android/app/FileShareServer/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 7

    .prologue
    .line 133
    const-string v3, "ServerService"

    const-string v4, "onDestroy"

    const-string v5, "releaseWakeLock"

    invoke-static {v3, v4, v5}, Lcom/samsung/android/app/FileShareServer/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 134
    invoke-direct {p0}, Lcom/samsung/android/app/FileShareServer/ServerService;->releaseWakeLock()V

    .line 135
    invoke-direct {p0}, Lcom/samsung/android/app/FileShareServer/ServerService;->unregBroadcastReceiver()V

    .line 136
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    sget-object v3, Lcom/samsung/android/app/FileShareServer/ServerService;->mWorkingList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v1, v3, :cond_2

    .line 137
    sget-object v3, Lcom/samsung/android/app/FileShareServer/ServerService;->mWorkingList:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/app/FileShareServer/InboundInfo;

    .line 138
    .local v2, "info":Lcom/samsung/android/app/FileShareServer/InboundInfo;
    invoke-virtual {v2}, Lcom/samsung/android/app/FileShareServer/InboundInfo;->getCurrentStatus()I

    move-result v3

    const/16 v4, 0x3ea

    if-eq v3, v4, :cond_0

    invoke-virtual {v2}, Lcom/samsung/android/app/FileShareServer/InboundInfo;->getCurrentStatus()I

    move-result v3

    const/16 v4, 0x3ed

    if-eq v3, v4, :cond_0

    invoke-virtual {v2}, Lcom/samsung/android/app/FileShareServer/InboundInfo;->getCurrentStatus()I

    move-result v3

    const/16 v4, 0x3e8

    if-ne v3, v4, :cond_1

    .line 141
    :cond_0
    iget-object v3, p0, Lcom/samsung/android/app/FileShareServer/ServerService;->mNotiManager:Landroid/app/NotificationManager;

    if-eqz v3, :cond_1

    .line 142
    const-string v3, "ServerService"

    const-string v4, "onDestroy"

    const-string v5, "mNotiManager.cancel"

    invoke-static {v3, v4, v5}, Lcom/samsung/android/app/FileShareServer/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 143
    iget-object v3, p0, Lcom/samsung/android/app/FileShareServer/ServerService;->mNotiManager:Landroid/app/NotificationManager;

    const-string v4, "FileShareServer"

    invoke-virtual {v2}, Lcom/samsung/android/app/FileShareServer/InboundInfo;->getInboundID()I

    move-result v5

    mul-int/lit8 v5, v5, 0x2

    invoke-virtual {v3, v4, v5}, Landroid/app/NotificationManager;->cancel(Ljava/lang/String;I)V

    .line 144
    invoke-virtual {p0}, Lcom/samsung/android/app/FileShareServer/ServerService;->deletedNotification()V

    .line 136
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 149
    .end local v2    # "info":Lcom/samsung/android/app/FileShareServer/InboundInfo;
    :cond_2
    :try_start_0
    iget-object v3, p0, Lcom/samsung/android/app/FileShareServer/ServerService;->mDbAdapter:Lcom/samsung/android/app/FileShareServer/DbAdapter;

    invoke-virtual {v3}, Lcom/samsung/android/app/FileShareServer/DbAdapter;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 153
    :goto_1
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 154
    return-void

    .line 150
    :catch_0
    move-exception v0

    .line 151
    .local v0, "e":Ljava/lang/Exception;
    const-string v3, "ServerService"

    const-string v4, "onDestroy"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "exception "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/samsung/android/app/FileShareServer/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public onNewSessionRequest(Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceSessionInfo;)V
    .locals 18
    .param p1, "session"    # Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceSessionInfo;

    .prologue
    .line 220
    const-string v12, "ServerService"

    const-string v13, "onNewSessionRequest"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "session.sessionId :"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move-object/from16 v0, p1

    iget-object v15, v0, Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceSessionInfo;->sessionId:Ljava/lang/String;

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v12, v13, v14}, Lcom/samsung/android/app/FileShareServer/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 223
    :try_start_0
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/samsung/android/app/FileShareServer/ServerService;->mSaveFilePath:Ljava/io/File;

    invoke-virtual {v12}, Ljava/io/File;->exists()Z

    move-result v12

    if-nez v12, :cond_0

    .line 224
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/samsung/android/app/FileShareServer/ServerService;->mSaveFilePath:Ljava/io/File;

    invoke-virtual {v12}, Ljava/io/File;->mkdir()Z

    .line 226
    :cond_0
    new-instance v9, Landroid/os/StatFs;

    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/app/FileShareServer/ServerService;->getSavePath()Ljava/lang/String;

    move-result-object v12

    invoke-direct {v9, v12}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    .line 227
    .local v9, "stat":Landroid/os/StatFs;
    invoke-virtual {v9}, Landroid/os/StatFs;->getBlockSizeLong()J

    move-result-wide v4

    .line 228
    .local v4, "blockSize":J
    invoke-virtual {v9}, Landroid/os/StatFs;->getAvailableBlocksLong()J

    move-result-wide v10

    .line 229
    .local v10, "totalBlocks":J
    mul-long v2, v4, v10

    .line 230
    .local v2, "availableMemory":J
    const-string v12, "ServerService"

    const-string v13, "onNewSessionRequest"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "availableMemory :"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, ", mTotalByte is : "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move-object/from16 v0, p1

    iget-wide v0, v0, Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceSessionInfo;->totalSize:J

    move-wide/from16 v16, v0

    move-wide/from16 v0, v16

    invoke-virtual {v14, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v12, v13, v14}, Lcom/samsung/android/app/FileShareServer/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 232
    move-object/from16 v0, p1

    iget-wide v12, v0, Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceSessionInfo;->totalSize:J

    cmp-long v12, v2, v12

    if-gez v12, :cond_1

    .line 233
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/samsung/android/app/FileShareServer/ServerService;->mThemeContext:Landroid/content/Context;

    const v13, 0x7f06000e

    const/4 v14, 0x0

    invoke-static {v12, v13, v14}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v12

    invoke-virtual {v12}, Landroid/widget/Toast;->show()V

    .line 235
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/samsung/android/app/FileShareServer/ServerService;->mFileShareDevice:Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper;

    move-object/from16 v0, p1

    iget-object v13, v0, Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceSessionInfo;->sessionId:Ljava/lang/String;

    invoke-virtual {v12, v13}, Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper;->cancelSession(Ljava/lang/String;)I

    .line 268
    .end local v2    # "availableMemory":J
    .end local v4    # "blockSize":J
    .end local v9    # "stat":Landroid/os/StatFs;
    .end local v10    # "totalBlocks":J
    :goto_0
    return-void

    .line 238
    .restart local v2    # "availableMemory":J
    .restart local v4    # "blockSize":J
    .restart local v9    # "stat":Landroid/os/StatFs;
    .restart local v10    # "totalBlocks":J
    :cond_1
    move-object/from16 v0, p1

    iget-object v12, v0, Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceSessionInfo;->sessionId:Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-direct {v0, v12}, Lcom/samsung/android/app/FileShareServer/ServerService;->convertInboundInfo(Ljava/lang/String;)Lcom/samsung/android/app/FileShareServer/InboundInfo;

    move-result-object v7

    .line 239
    .local v7, "inbound":Lcom/samsung/android/app/FileShareServer/InboundInfo;
    if-eqz v7, :cond_3

    .line 240
    sget-object v12, Lcom/samsung/android/app/FileShareServer/ServerService;->mWorkingList:Ljava/util/ArrayList;

    invoke-virtual {v12, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 241
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/samsung/android/app/FileShareServer/ServerService;->mFileShareDevice:Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper;

    move-object/from16 v0, p1

    iget-object v13, v0, Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceSessionInfo;->sessionId:Ljava/lang/String;

    invoke-virtual {v12, v13}, Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper;->acceptSession(Ljava/lang/String;)I

    .line 249
    sget v12, Lcom/samsung/android/app/FileShareServer/Utils;->mServerProperty:I

    if-ltz v12, :cond_2

    .line 250
    sget v12, Lcom/samsung/android/app/FileShareServer/Utils;->mServerProperty:I

    add-int/lit8 v12, v12, 0x1

    sput v12, Lcom/samsung/android/app/FileShareServer/Utils;->mServerProperty:I

    .line 251
    const-string v12, "ServerService"

    const-string v13, "onSessionTransportStart"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "mServerProperty : "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    sget v15, Lcom/samsung/android/app/FileShareServer/Utils;->mServerProperty:I

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v12, v13, v14}, Lcom/samsung/android/app/FileShareServer/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 254
    :cond_2
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/app/FileShareServer/ServerService;->checkIsServiceRunning()V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 261
    const/4 v12, 0x0

    move-object/from16 v0, p0

    iput-boolean v12, v0, Lcom/samsung/android/app/FileShareServer/ServerService;->mIsUpdate:Z

    .line 262
    move-object/from16 v0, p1

    iget-object v12, v0, Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceSessionInfo;->sessionId:Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-direct {v0, v12}, Lcom/samsung/android/app/FileShareServer/ServerService;->AddOngoing(Ljava/lang/String;)V

    .line 263
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/app/FileShareServer/ServerService;->getNotiCntPreference()I

    move-result v8

    .line 264
    .local v8, "notiCnt":I
    add-int/lit8 v8, v8, 0x1

    .line 265
    move-object/from16 v0, p0

    invoke-direct {v0, v8}, Lcom/samsung/android/app/FileShareServer/ServerService;->setNotiCntPreference(I)V

    .line 266
    const-string v12, "ServerService"

    const-string v13, "onNewSessionRequest"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "NotiCnt = ["

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, "]"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v12, v13, v14}, Lcom/samsung/android/app/FileShareServer/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 267
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/app/FileShareServer/ServerService;->startInboundForeground()V

    goto/16 :goto_0

    .line 244
    .end local v8    # "notiCnt":I
    :cond_3
    :try_start_1
    const-string v12, "ServerService"

    const-string v13, "onNewSessionRequest"

    const-string v14, "InbountInfo is null, maybe session has already been cancelled."

    invoke-static {v12, v13, v14}, Lcom/samsung/android/app/FileShareServer/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0

    .line 255
    .end local v2    # "availableMemory":J
    .end local v4    # "blockSize":J
    .end local v7    # "inbound":Lcom/samsung/android/app/FileShareServer/InboundInfo;
    .end local v9    # "stat":Landroid/os/StatFs;
    .end local v10    # "totalBlocks":J
    :catch_0
    move-exception v6

    .line 256
    .local v6, "e":Ljava/lang/IllegalArgumentException;
    const-string v12, "ServerService"

    const-string v13, "onNewSessionRequest"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "Check Memory Full: "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v12, v13, v14}, Lcom/samsung/android/app/FileShareServer/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 257
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/samsung/android/app/FileShareServer/ServerService;->mThemeContext:Landroid/content/Context;

    const v13, 0x7f06000e

    const/4 v14, 0x0

    invoke-static {v12, v13, v14}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v12

    invoke-virtual {v12}, Landroid/widget/Toast;->show()V

    .line 258
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/samsung/android/app/FileShareServer/ServerService;->mFileShareDevice:Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper;

    move-object/from16 v0, p1

    iget-object v13, v0, Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceSessionInfo;->sessionId:Ljava/lang/String;

    invoke-virtual {v12, v13}, Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper;->cancelSession(Ljava/lang/String;)I

    goto/16 :goto_0
.end method

.method public onSessionCancel(Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceSessionInfo;Z)V
    .locals 10
    .param p1, "session"    # Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceSessionInfo;
    .param p2, "byClient"    # Z

    .prologue
    .line 276
    const-string v5, "ServerService"

    const-string v6, "onSessionCancel"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "session ID : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p1, Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceSessionInfo;->sessionId:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " byClient : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/samsung/android/app/FileShareServer/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 278
    if-eqz p2, :cond_4

    .line 279
    iget-object v5, p1, Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceSessionInfo;->sessionId:Ljava/lang/String;

    invoke-direct {p0, v5}, Lcom/samsung/android/app/FileShareServer/ServerService;->findCurrentSession(Ljava/lang/String;)I

    move-result v2

    .line 280
    .local v2, "idx":I
    const/4 v5, -0x1

    if-ne v2, v5, :cond_1

    .line 281
    const-string v5, "ServerService"

    const-string v6, "onSessionCancel"

    const-string v7, "findCurrentSession return -1!"

    invoke-static {v5, v6, v7}, Lcom/samsung/android/app/FileShareServer/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 313
    .end local v2    # "idx":I
    :goto_0
    sget v5, Lcom/samsung/android/app/FileShareServer/Utils;->mServerProperty:I

    if-lez v5, :cond_0

    .line 314
    sget v5, Lcom/samsung/android/app/FileShareServer/Utils;->mServerProperty:I

    add-int/lit8 v5, v5, -0x1

    sput v5, Lcom/samsung/android/app/FileShareServer/Utils;->mServerProperty:I

    .line 315
    const-string v5, "ServerService"

    const-string v6, "onSessionCancel"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "mServerProperty : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    sget v8, Lcom/samsung/android/app/FileShareServer/Utils;->mServerProperty:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/samsung/android/app/FileShareServer/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 317
    :cond_0
    invoke-direct {p0}, Lcom/samsung/android/app/FileShareServer/ServerService;->checkIsServiceRunning()V

    .line 319
    const/4 v5, 0x1

    invoke-direct {p0}, Lcom/samsung/android/app/FileShareServer/ServerService;->checkForDeviceRunning()Z

    move-result v6

    if-ne v5, v6, :cond_5

    .line 320
    const-string v5, "ServerService"

    const-string v6, "onSessionCancel"

    const-string v7, "There are Running Devices."

    invoke-static {v5, v6, v7}, Lcom/samsung/android/app/FileShareServer/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 325
    :goto_1
    return-void

    .line 283
    .restart local v2    # "idx":I
    :cond_1
    sget-object v5, Lcom/samsung/android/app/FileShareServer/ServerService;->mWorkingList:Ljava/util/ArrayList;

    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/android/app/FileShareServer/InboundInfo;

    .line 284
    .local v3, "info":Lcom/samsung/android/app/FileShareServer/InboundInfo;
    invoke-virtual {v3}, Lcom/samsung/android/app/FileShareServer/InboundInfo;->getCurrentIndex()I

    move-result v5

    add-int/lit8 v1, v5, -0x1

    .line 285
    .local v1, "downloadedCount":I
    const/16 v5, 0x3e9

    invoke-virtual {v3, v5}, Lcom/samsung/android/app/FileShareServer/InboundInfo;->setCurrentStatus(I)V

    .line 286
    invoke-virtual {v3, v1}, Lcom/samsung/android/app/FileShareServer/InboundInfo;->setCurrentIndex(I)V

    .line 287
    invoke-virtual {v3}, Lcom/samsung/android/app/FileShareServer/InboundInfo;->getTotalCount()I

    move-result v5

    invoke-virtual {v3}, Lcom/samsung/android/app/FileShareServer/InboundInfo;->getTotalSavedCount()I

    move-result v6

    sub-int v4, v5, v6

    .line 288
    .local v4, "workingListFailedCnt":I
    if-gez v4, :cond_3

    .line 289
    const-string v5, "ServerService"

    const-string v6, "onSessionCancel"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "totalCount["

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-wide v8, p1, Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceSessionInfo;->totalCount:J

    invoke-virtual {v7, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", successCount["

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-wide v8, p1, Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceSessionInfo;->successCount:J

    invoke-virtual {v7, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "]"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/samsung/android/app/FileShareServer/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 291
    iget-wide v6, p1, Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceSessionInfo;->totalCount:J

    iget-wide v8, p1, Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceSessionInfo;->successCount:J

    sub-long/2addr v6, v8

    long-to-int v0, v6

    .line 292
    .local v0, "SessionFailedCnt":I
    if-gez v0, :cond_2

    .line 293
    const-string v5, "ServerService"

    const-string v6, "onSessionCancel"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "FailedCount is Wrong.TotalCount: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v3}, Lcom/samsung/android/app/FileShareServer/InboundInfo;->getTotalCount()I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "TotalSavedCount: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v3}, Lcom/samsung/android/app/FileShareServer/InboundInfo;->getTotalSavedCount()I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/samsung/android/app/FileShareServer/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 302
    .end local v0    # "SessionFailedCnt":I
    :goto_2
    invoke-virtual {v3}, Lcom/samsung/android/app/FileShareServer/InboundInfo;->update()V

    .line 303
    const-string v5, "ServerService"

    const-string v6, "onSessionCancel"

    const-string v7, "DB - add"

    invoke-static {v5, v6, v7}, Lcom/samsung/android/app/FileShareServer/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 304
    iget-object v5, p0, Lcom/samsung/android/app/FileShareServer/ServerService;->mDbAdapter:Lcom/samsung/android/app/FileShareServer/DbAdapter;

    invoke-virtual {v5, v3}, Lcom/samsung/android/app/FileShareServer/DbAdapter;->insertInboundInfo(Lcom/samsung/android/app/FileShareServer/InboundInfo;)V

    .line 305
    iget-object v5, p1, Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceSessionInfo;->sessionId:Ljava/lang/String;

    invoke-direct {p0, v5}, Lcom/samsung/android/app/FileShareServer/ServerService;->AddResultNotification(Ljava/lang/String;)V

    .line 306
    invoke-virtual {v3}, Lcom/samsung/android/app/FileShareServer/InboundInfo;->getInboundID()I

    move-result v5

    invoke-virtual {p0, v5}, Lcom/samsung/android/app/FileShareServer/ServerService;->destroyInbound(I)V

    goto/16 :goto_0

    .line 297
    .restart local v0    # "SessionFailedCnt":I
    :cond_2
    invoke-virtual {v3, v0}, Lcom/samsung/android/app/FileShareServer/InboundInfo;->setTotalFailedCount(I)V

    goto :goto_2

    .line 300
    .end local v0    # "SessionFailedCnt":I
    :cond_3
    invoke-virtual {v3}, Lcom/samsung/android/app/FileShareServer/InboundInfo;->getTotalCount()I

    move-result v5

    invoke-virtual {v3}, Lcom/samsung/android/app/FileShareServer/InboundInfo;->getTotalSavedCount()I

    move-result v6

    sub-int/2addr v5, v6

    invoke-virtual {v3, v5}, Lcom/samsung/android/app/FileShareServer/InboundInfo;->setTotalFailedCount(I)V

    goto :goto_2

    .line 309
    .end local v1    # "downloadedCount":I
    .end local v2    # "idx":I
    .end local v3    # "info":Lcom/samsung/android/app/FileShareServer/InboundInfo;
    .end local v4    # "workingListFailedCnt":I
    :cond_4
    const-string v5, "ServerService"

    const-string v6, "onSessionCancel"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Cancel myself! So This Event PASS. session ID = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p1, Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceSessionInfo;->sessionId:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/samsung/android/app/FileShareServer/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 323
    :cond_5
    invoke-direct {p0}, Lcom/samsung/android/app/FileShareServer/ServerService;->releaseWakeLock()V

    goto/16 :goto_1
.end method

.method public onSessionClose(Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceSessionInfo;)V
    .locals 6
    .param p1, "session"    # Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceSessionInfo;

    .prologue
    .line 333
    const-string v2, "ServerService"

    const-string v3, "onSessionClose"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "session.sessionId "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p1, Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceSessionInfo;->sessionId:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/samsung/android/app/FileShareServer/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 334
    iget-object v2, p1, Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceSessionInfo;->sessionId:Ljava/lang/String;

    invoke-direct {p0, v2}, Lcom/samsung/android/app/FileShareServer/ServerService;->findCurrentSession(Ljava/lang/String;)I

    move-result v0

    .line 336
    .local v0, "idx":I
    sget v2, Lcom/samsung/android/app/FileShareServer/Utils;->mServerProperty:I

    if-lez v2, :cond_0

    .line 337
    sget v2, Lcom/samsung/android/app/FileShareServer/Utils;->mServerProperty:I

    add-int/lit8 v2, v2, -0x1

    sput v2, Lcom/samsung/android/app/FileShareServer/Utils;->mServerProperty:I

    .line 338
    const-string v2, "ServerService"

    const-string v3, "onSessionClose"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "mServerProperty : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget v5, Lcom/samsung/android/app/FileShareServer/Utils;->mServerProperty:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/samsung/android/app/FileShareServer/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 340
    :cond_0
    invoke-direct {p0}, Lcom/samsung/android/app/FileShareServer/ServerService;->checkIsServiceRunning()V

    .line 342
    const/4 v2, -0x1

    if-ne v0, v2, :cond_1

    .line 343
    const-string v2, "ServerService"

    const-string v3, "onSessionClose"

    const-string v4, "findCurrentSession return -1!"

    invoke-static {v2, v3, v4}, Lcom/samsung/android/app/FileShareServer/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 353
    :goto_0
    const/4 v2, 0x1

    invoke-direct {p0}, Lcom/samsung/android/app/FileShareServer/ServerService;->checkForDeviceRunning()Z

    move-result v3

    if-ne v2, v3, :cond_2

    .line 354
    const-string v2, "ServerService"

    const-string v3, "onSessionClose"

    const-string v4, "There are Running Devices."

    invoke-static {v2, v3, v4}, Lcom/samsung/android/app/FileShareServer/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 359
    :goto_1
    return-void

    .line 345
    :cond_1
    sget-object v2, Lcom/samsung/android/app/FileShareServer/ServerService;->mWorkingList:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/app/FileShareServer/InboundInfo;

    .line 346
    .local v1, "info":Lcom/samsung/android/app/FileShareServer/InboundInfo;
    const/16 v2, 0x3ec

    invoke-virtual {v1, v2}, Lcom/samsung/android/app/FileShareServer/InboundInfo;->setCurrentStatus(I)V

    .line 347
    invoke-virtual {v1}, Lcom/samsung/android/app/FileShareServer/InboundInfo;->update()V

    .line 348
    const-string v2, "ServerService"

    const-string v3, "onSessionClose"

    const-string v4, "DB - add"

    invoke-static {v2, v3, v4}, Lcom/samsung/android/app/FileShareServer/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 349
    iget-object v2, p0, Lcom/samsung/android/app/FileShareServer/ServerService;->mDbAdapter:Lcom/samsung/android/app/FileShareServer/DbAdapter;

    invoke-virtual {v2, v1}, Lcom/samsung/android/app/FileShareServer/DbAdapter;->insertInboundInfo(Lcom/samsung/android/app/FileShareServer/InboundInfo;)V

    .line 350
    iget-object v2, p1, Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceSessionInfo;->sessionId:Ljava/lang/String;

    invoke-direct {p0, v2}, Lcom/samsung/android/app/FileShareServer/ServerService;->AddResultNotification(Ljava/lang/String;)V

    .line 351
    invoke-virtual {v1}, Lcom/samsung/android/app/FileShareServer/InboundInfo;->getInboundID()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/samsung/android/app/FileShareServer/ServerService;->destroyInbound(I)V

    goto :goto_0

    .line 357
    .end local v1    # "info":Lcom/samsung/android/app/FileShareServer/InboundInfo;
    :cond_2
    invoke-direct {p0}, Lcom/samsung/android/app/FileShareServer/ServerService;->releaseWakeLock()V

    goto :goto_1
.end method

.method public onSessionTransportError(Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceSessionInfo;ILcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceTransportInfo;)V
    .locals 12
    .param p1, "session"    # Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceSessionInfo;
    .param p2, "ErrorDesc"    # I
    .param p3, "transport"    # Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceTransportInfo;

    .prologue
    const/4 v0, 0x5

    .line 461
    const-string v1, "ServerService"

    const-string v2, "onSessionTransportError"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "SessionId : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p1, Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceSessionInfo;->sessionId:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/samsung/android/app/FileShareServer/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 462
    iget-object v1, p1, Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceSessionInfo;->sessionId:Ljava/lang/String;

    invoke-direct {p0, v1}, Lcom/samsung/android/app/FileShareServer/ServerService;->findCurrentSession(Ljava/lang/String;)I

    move-result v8

    .line 464
    .local v8, "idx":I
    new-instance v10, Ljava/io/File;

    iget-object v1, p3, Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceTransportInfo;->targetURL:Ljava/lang/String;

    invoke-direct {v10, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 465
    .local v10, "receivedFile":Ljava/io/File;
    const/4 v2, 0x0

    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v3

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Receiving file "

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v10}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v5, " failed"

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    move v1, v0

    invoke-static/range {v0 .. v5}, Landroid/sec/enterprise/auditlog/AuditLog;->log(IIZILjava/lang/String;Ljava/lang/String;)V

    .line 470
    const/4 v0, -0x1

    if-ne v8, v0, :cond_0

    .line 471
    const-string v0, "ServerService"

    const-string v1, "onSessionTransportError"

    const-string v2, "Can\'t find"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/app/FileShareServer/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 496
    :goto_0
    const/4 v0, 0x1

    invoke-direct {p0}, Lcom/samsung/android/app/FileShareServer/ServerService;->checkForDeviceRunning()Z

    move-result v1

    if-ne v0, v1, :cond_3

    .line 497
    const-string v0, "ServerService"

    const-string v1, "onSessionTransportError"

    const-string v2, "There are Running Devices."

    invoke-static {v0, v1, v2}, Lcom/samsung/android/app/FileShareServer/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 502
    :goto_1
    return-void

    .line 473
    :cond_0
    sget-object v0, Lcom/samsung/android/app/FileShareServer/ServerService;->mWorkingList:Ljava/util/ArrayList;

    invoke-virtual {v0, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/samsung/android/app/FileShareServer/InboundInfo;

    .line 474
    .local v9, "info":Lcom/samsung/android/app/FileShareServer/InboundInfo;
    invoke-virtual {v9}, Lcom/samsung/android/app/FileShareServer/InboundInfo;->getCurrentIndex()I

    move-result v0

    add-int/lit8 v7, v0, -0x1

    .line 475
    .local v7, "downloadedCount":I
    const/16 v0, 0x3ee

    invoke-virtual {v9, v0}, Lcom/samsung/android/app/FileShareServer/InboundInfo;->setCurrentStatus(I)V

    .line 476
    invoke-virtual {v9, v7}, Lcom/samsung/android/app/FileShareServer/InboundInfo;->setCurrentIndex(I)V

    .line 477
    invoke-virtual {v9}, Lcom/samsung/android/app/FileShareServer/InboundInfo;->getTotalCount()I

    move-result v0

    invoke-virtual {v9}, Lcom/samsung/android/app/FileShareServer/InboundInfo;->getTotalSavedCount()I

    move-result v1

    sub-int v11, v0, v1

    .line 478
    .local v11, "workingListFailedCnt":I
    if-gez v11, :cond_2

    .line 479
    iget-wide v0, p1, Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceSessionInfo;->totalCount:J

    iget-wide v2, p1, Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceSessionInfo;->successCount:J

    sub-long/2addr v0, v2

    long-to-int v6, v0

    .line 480
    .local v6, "SessionFailedCnt":I
    if-gez v6, :cond_1

    .line 481
    const-string v0, "ServerService"

    const-string v1, "onSessionTransportError"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "FailedCount is Wrong. TotalCount: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v9}, Lcom/samsung/android/app/FileShareServer/InboundInfo;->getTotalCount()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " TotalSavedCount: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v9}, Lcom/samsung/android/app/FileShareServer/InboundInfo;->getTotalSavedCount()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/samsung/android/app/FileShareServer/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 490
    .end local v6    # "SessionFailedCnt":I
    :goto_2
    invoke-virtual {v9}, Lcom/samsung/android/app/FileShareServer/InboundInfo;->update()V

    .line 491
    const-string v0, "ServerService"

    const-string v1, "onSessionTransportError"

    const-string v2, "DB - add"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/app/FileShareServer/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 492
    iget-object v0, p0, Lcom/samsung/android/app/FileShareServer/ServerService;->mDbAdapter:Lcom/samsung/android/app/FileShareServer/DbAdapter;

    invoke-virtual {v0, v9}, Lcom/samsung/android/app/FileShareServer/DbAdapter;->insertInboundInfo(Lcom/samsung/android/app/FileShareServer/InboundInfo;)V

    .line 493
    iget-object v0, p1, Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceSessionInfo;->sessionId:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/samsung/android/app/FileShareServer/ServerService;->AddResultNotification(Ljava/lang/String;)V

    .line 494
    invoke-virtual {v9}, Lcom/samsung/android/app/FileShareServer/InboundInfo;->getInboundID()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/FileShareServer/ServerService;->destroyInbound(I)V

    goto/16 :goto_0

    .line 485
    .restart local v6    # "SessionFailedCnt":I
    :cond_1
    invoke-virtual {v9, v6}, Lcom/samsung/android/app/FileShareServer/InboundInfo;->setTotalFailedCount(I)V

    goto :goto_2

    .line 488
    .end local v6    # "SessionFailedCnt":I
    :cond_2
    invoke-virtual {v9}, Lcom/samsung/android/app/FileShareServer/InboundInfo;->getTotalCount()I

    move-result v0

    invoke-virtual {v9}, Lcom/samsung/android/app/FileShareServer/InboundInfo;->getTotalSavedCount()I

    move-result v1

    sub-int/2addr v0, v1

    invoke-virtual {v9, v0}, Lcom/samsung/android/app/FileShareServer/InboundInfo;->setTotalFailedCount(I)V

    goto :goto_2

    .line 500
    .end local v7    # "downloadedCount":I
    .end local v9    # "info":Lcom/samsung/android/app/FileShareServer/InboundInfo;
    .end local v11    # "workingListFailedCnt":I
    :cond_3
    invoke-direct {p0}, Lcom/samsung/android/app/FileShareServer/ServerService;->releaseWakeLock()V

    goto/16 :goto_1
.end method

.method public onSessionTransportFinish(Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceSessionInfo;Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceTransportInfo;)V
    .locals 16
    .param p1, "session"    # Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceSessionInfo;
    .param p2, "transport"    # Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceTransportInfo;

    .prologue
    .line 399
    :try_start_0
    const-string v2, "ServerService"

    const-string v3, "onSessionTransportFinish"

    const-string v4, ""

    invoke-static {v2, v3, v4}, Lcom/samsung/android/app/FileShareServer/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 400
    if-nez p2, :cond_0

    .line 401
    const-string v2, "ServerService"

    const-string v3, "onSessionTransportFinish"

    const-string v4, "cannot get session info!"

    invoke-static {v2, v3, v4}, Lcom/samsung/android/app/FileShareServer/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 451
    :goto_0
    return-void

    .line 404
    :cond_0
    new-instance v14, Ljava/io/File;

    move-object/from16 v0, p2

    iget-object v2, v0, Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceTransportInfo;->targetURL:Ljava/lang/String;

    invoke-direct {v14, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 405
    .local v14, "receivedFile":Ljava/io/File;
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceSessionInfo;->sessionId:Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/samsung/android/app/FileShareServer/ServerService;->findCurrentSession(Ljava/lang/String;)I

    move-result v12

    .line 407
    .local v12, "idx":I
    const/4 v2, 0x5

    const/4 v3, 0x5

    const/4 v4, 0x0

    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v5

    invoke-virtual/range {p0 .. p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "Receiving file "

    invoke-virtual {v7, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v14}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v7, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v15, " finished"

    invoke-virtual {v7, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static/range {v2 .. v7}, Landroid/sec/enterprise/auditlog/AuditLog;->log(IIZILjava/lang/String;Ljava/lang/String;)V

    .line 412
    const/4 v2, -0x1

    if-ne v12, v2, :cond_1

    .line 413
    const-string v2, "ServerService"

    const-string v3, "onSessionTransportFinish"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "can\'t find "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceSessionInfo;->sessionId:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/samsung/android/app/FileShareServer/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 448
    .end local v12    # "idx":I
    .end local v14    # "receivedFile":Ljava/io/File;
    :catch_0
    move-exception v10

    .line 449
    .local v10, "e":Ljava/lang/Exception;
    const-string v2, "ServerService"

    const-string v3, "onSessionTransportFinish"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Exception onSessionTransportFinish"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/samsung/android/app/FileShareServer/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 415
    .end local v10    # "e":Ljava/lang/Exception;
    .restart local v12    # "idx":I
    .restart local v14    # "receivedFile":Ljava/io/File;
    :cond_1
    :try_start_1
    sget-object v2, Lcom/samsung/android/app/FileShareServer/ServerService;->mWorkingList:Ljava/util/ArrayList;

    invoke-virtual {v2, v12}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lcom/samsung/android/app/FileShareServer/InboundInfo;

    .line 416
    .local v13, "info":Lcom/samsung/android/app/FileShareServer/InboundInfo;
    const-wide/16 v8, 0x0

    .line 417
    .local v8, "downloadedSize":J
    const/16 v2, 0x3ed

    invoke-virtual {v13, v2}, Lcom/samsung/android/app/FileShareServer/InboundInfo;->setCurrentStatus(I)V

    .line 418
    invoke-virtual {v13}, Lcom/samsung/android/app/FileShareServer/InboundInfo;->getTotalSuccessCount()I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {v13, v2}, Lcom/samsung/android/app/FileShareServer/InboundInfo;->setTotalSuccessCount(I)V

    .line 419
    sget-object v2, Lcom/samsung/android/app/FileShareServer/ServerService;->mWorkingList:Ljava/util/ArrayList;

    invoke-virtual {v2, v12}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/app/FileShareServer/InboundInfo;

    invoke-virtual {v2}, Lcom/samsung/android/app/FileShareServer/InboundInfo;->getDownloadedSize()J

    move-result-wide v2

    move-object/from16 v0, p2

    iget-wide v4, v0, Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceTransportInfo;->downloadLength:J

    add-long v8, v2, v4

    .line 421
    invoke-virtual {v13, v8, v9}, Lcom/samsung/android/app/FileShareServer/InboundInfo;->setDownloadedSize(J)V

    .line 422
    const-wide/16 v2, 0x0

    invoke-virtual {v13, v2, v3}, Lcom/samsung/android/app/FileShareServer/InboundInfo;->setTransportSize(J)V

    .line 423
    const-string v2, "ServerService"

    const-string v3, "onSessionTransportFinish"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceSessionInfo;->sessionId:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ") "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/samsung/android/app/FileShareServer/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 425
    invoke-virtual {v13, v14}, Lcom/samsung/android/app/FileShareServer/InboundInfo;->addFile(Ljava/io/File;)V

    .line 426
    new-instance v2, Ljava/io/File;

    invoke-virtual {v14}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v2}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v11

    .line 427
    .local v11, "fileUri":Landroid/net/Uri;
    new-instance v2, Landroid/content/Intent;

    const-string v3, "android.intent.action.MEDIA_SCANNER_SCAN_FILE"

    invoke-virtual {v11}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/samsung/android/app/FileShareServer/ServerService;->sendBroadcast(Landroid/content/Intent;)V

    .line 429
    invoke-virtual {v13}, Lcom/samsung/android/app/FileShareServer/InboundInfo;->getTotalSavedCount()I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {v13, v2}, Lcom/samsung/android/app/FileShareServer/InboundInfo;->setTotalSavedCount(I)V

    .line 430
    invoke-virtual {v13}, Lcom/samsung/android/app/FileShareServer/InboundInfo;->getTotalSavedCount()I

    move-result v2

    invoke-virtual {v13}, Lcom/samsung/android/app/FileShareServer/InboundInfo;->getTotalFailedCount()I

    move-result v3

    add-int/2addr v2, v3

    invoke-virtual {v13}, Lcom/samsung/android/app/FileShareServer/InboundInfo;->getTotalCount()I

    move-result v3

    if-ne v2, v3, :cond_3

    .line 432
    const/16 v2, 0x3ec

    invoke-virtual {v13, v2}, Lcom/samsung/android/app/FileShareServer/InboundInfo;->setCurrentStatus(I)V

    .line 433
    invoke-virtual {v13}, Lcom/samsung/android/app/FileShareServer/InboundInfo;->update()V

    .line 434
    const-string v2, "ServerService"

    const-string v3, "onSessionTransportFinish"

    const-string v4, "DB - add"

    invoke-static {v2, v3, v4}, Lcom/samsung/android/app/FileShareServer/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 435
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/app/FileShareServer/ServerService;->mDbAdapter:Lcom/samsung/android/app/FileShareServer/DbAdapter;

    invoke-virtual {v2, v13}, Lcom/samsung/android/app/FileShareServer/DbAdapter;->insertInboundInfo(Lcom/samsung/android/app/FileShareServer/InboundInfo;)V

    .line 436
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceSessionInfo;->sessionId:Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/samsung/android/app/FileShareServer/ServerService;->AddResultNotification(Ljava/lang/String;)V

    .line 437
    invoke-virtual {v13}, Lcom/samsung/android/app/FileShareServer/InboundInfo;->getInboundID()I

    move-result v2

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/samsung/android/app/FileShareServer/ServerService;->destroyInbound(I)V

    .line 438
    const/4 v2, 0x1

    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/app/FileShareServer/ServerService;->checkForDeviceRunning()Z

    move-result v3

    if-ne v2, v3, :cond_2

    .line 439
    const-string v2, "ServerService"

    const-string v3, "onSessionTransportFinish"

    const-string v4, "There are Running Devices."

    invoke-static {v2, v3, v4}, Lcom/samsung/android/app/FileShareServer/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 442
    :cond_2
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/app/FileShareServer/ServerService;->releaseWakeLock()V

    goto/16 :goto_0

    .line 445
    :cond_3
    invoke-virtual {v13}, Lcom/samsung/android/app/FileShareServer/InboundInfo;->update()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0
.end method

.method public onSessionTransportProgress(Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceSessionInfo;Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceTransportInfo;)V
    .locals 5
    .param p1, "session"    # Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceSessionInfo;
    .param p2, "transport"    # Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceTransportInfo;

    .prologue
    .line 385
    :try_start_0
    const-string v1, "ServerService"

    const-string v2, "onSessionTransportProgress"

    const-string v3, ""

    invoke-static {v1, v2, v3}, Lcom/samsung/android/app/FileShareServer/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 386
    iget-object v1, p1, Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceSessionInfo;->sessionId:Ljava/lang/String;

    invoke-direct {p0, v1, p2}, Lcom/samsung/android/app/FileShareServer/ServerService;->sessionTransportProgress(Ljava/lang/String;Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceTransportInfo;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 391
    :goto_0
    return-void

    .line 387
    :catch_0
    move-exception v0

    .line 388
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "ServerService"

    const-string v2, "onSessionTransportProgress"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Exception onSessionTransportProgress"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/samsung/android/app/FileShareServer/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onSessionTransportStart(Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceSessionInfo;Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceTransportInfo;)V
    .locals 8
    .param p1, "session"    # Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceSessionInfo;
    .param p2, "transport"    # Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceTransportInfo;

    .prologue
    .line 366
    :try_start_0
    const-string v4, "ServerService"

    const-string v5, "onSessionTransportStart"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "SessionId : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p1, Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceSessionInfo;->sessionId:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/samsung/android/app/FileShareServer/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 367
    iget-object v4, p1, Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceSessionInfo;->sessionId:Ljava/lang/String;

    invoke-direct {p0, v4}, Lcom/samsung/android/app/FileShareServer/ServerService;->findCurrentSession(Ljava/lang/String;)I

    move-result v2

    .line 368
    .local v2, "idx":I
    const/4 v4, -0x1

    if-ne v2, v4, :cond_0

    .line 369
    const-string v4, "ServerService"

    const-string v5, "onSessionTransportStart"

    const-string v6, "findCurrentSession return -1!"

    invoke-static {v4, v5, v6}, Lcom/samsung/android/app/FileShareServer/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 379
    .end local v2    # "idx":I
    :goto_0
    return-void

    .line 371
    .restart local v2    # "idx":I
    :cond_0
    sget-object v4, Lcom/samsung/android/app/FileShareServer/ServerService;->mWorkingList:Ljava/util/ArrayList;

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/android/app/FileShareServer/InboundInfo;

    .line 372
    .local v3, "info":Lcom/samsung/android/app/FileShareServer/InboundInfo;
    invoke-virtual {v3}, Lcom/samsung/android/app/FileShareServer/InboundInfo;->getCurrentIndex()I

    move-result v0

    .line 373
    .local v0, "currentIdx":I
    add-int/lit8 v0, v0, 0x1

    invoke-virtual {v3, v0}, Lcom/samsung/android/app/FileShareServer/InboundInfo;->setCurrentIndex(I)V

    .line 374
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/app/FileShareServer/ServerService;->sessionTransportStart(Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceSessionInfo;Lcom/samsung/android/allshare/stack/fileshare/data/FileShareDeviceTransportInfo;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 376
    .end local v0    # "currentIdx":I
    .end local v2    # "idx":I
    .end local v3    # "info":Lcom/samsung/android/app/FileShareServer/InboundInfo;
    :catch_0
    move-exception v1

    .line 377
    .local v1, "e":Ljava/lang/Exception;
    const-string v4, "ServerService"

    const-string v5, "onSessionTransportStart"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Exception onSessionTransportStart"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/samsung/android/app/FileShareServer/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onStart(Z)V
    .locals 4
    .param p1, "result"    # Z

    .prologue
    .line 201
    const-string v0, "ServerService"

    const-string v1, "onStart"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "result : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/samsung/android/app/FileShareServer/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 202
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 7
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "flags"    # I
    .param p3, "startId"    # I

    .prologue
    const/4 v6, 0x1

    .line 82
    if-nez p1, :cond_1

    .line 83
    const-string v2, "ServerService"

    const-string v3, "onStartCommand"

    const-string v4, "onStartCommand with null intent"

    invoke-static {v2, v3, v4}, Lcom/samsung/android/app/FileShareServer/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 85
    invoke-direct {p0}, Lcom/samsung/android/app/FileShareServer/ServerService;->setBroadcastReceiver()V

    .line 87
    :try_start_0
    invoke-direct {p0}, Lcom/samsung/android/app/FileShareServer/ServerService;->startFileShareDevice()V
    :try_end_0
    .catch Ljava/net/SocketException; {:try_start_0 .. :try_end_0} :catch_0

    .line 92
    :goto_0
    iget-object v2, p0, Lcom/samsung/android/app/FileShareServer/ServerService;->mNotiManager:Landroid/app/NotificationManager;

    if-nez v2, :cond_0

    .line 93
    const-string v2, "notification"

    invoke-virtual {p0, v2}, Lcom/samsung/android/app/FileShareServer/ServerService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/NotificationManager;

    iput-object v2, p0, Lcom/samsung/android/app/FileShareServer/ServerService;->mNotiManager:Landroid/app/NotificationManager;

    .line 122
    :cond_0
    :goto_1
    return v6

    .line 88
    :catch_0
    move-exception v0

    .line 89
    .local v0, "e":Ljava/net/SocketException;
    invoke-virtual {v0}, Ljava/net/SocketException;->printStackTrace()V

    goto :goto_0

    .line 97
    .end local v0    # "e":Ljava/net/SocketException;
    :cond_1
    const-string v2, "ServerService"

    const-string v3, "onStartCommand"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "intent.getAction() : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/samsung/android/app/FileShareServer/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 98
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    const-string v3, "com.samsung.android.app.FileShareServer.REMOVE_INBOUND_INFO"

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 100
    invoke-virtual {p0}, Lcom/samsung/android/app/FileShareServer/ServerService;->deletedNotification()V

    goto :goto_1

    .line 104
    :cond_2
    const-string v2, "CurrentUser"

    iget v3, p0, Lcom/samsung/android/app/FileShareServer/ServerService;->mGetCurrenUserFromIntent:I

    invoke-virtual {p1, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 106
    .local v1, "mCurrentUser":I
    const-string v2, "ServerService"

    const-string v3, "onStartCommand"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "mUser.getUserHandle() : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/samsung/android/app/FileShareServer/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 107
    const-string v2, "ServerService"

    const-string v3, "onStartCommand"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "mGetCurrentUserFromIntent : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/samsung/android/app/FileShareServer/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 109
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v2

    if-ne v2, v1, :cond_0

    .line 110
    invoke-direct {p0}, Lcom/samsung/android/app/FileShareServer/ServerService;->setBroadcastReceiver()V

    .line 112
    :try_start_1
    invoke-direct {p0}, Lcom/samsung/android/app/FileShareServer/ServerService;->startFileShareDevice()V
    :try_end_1
    .catch Ljava/net/SocketException; {:try_start_1 .. :try_end_1} :catch_1

    .line 117
    :goto_2
    iget-object v2, p0, Lcom/samsung/android/app/FileShareServer/ServerService;->mNotiManager:Landroid/app/NotificationManager;

    if-nez v2, :cond_0

    .line 118
    const-string v2, "notification"

    invoke-virtual {p0, v2}, Lcom/samsung/android/app/FileShareServer/ServerService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/NotificationManager;

    iput-object v2, p0, Lcom/samsung/android/app/FileShareServer/ServerService;->mNotiManager:Landroid/app/NotificationManager;

    goto/16 :goto_1

    .line 113
    :catch_1
    move-exception v0

    .line 114
    .restart local v0    # "e":Ljava/net/SocketException;
    invoke-virtual {v0}, Ljava/net/SocketException;->printStackTrace()V

    goto :goto_2
.end method

.method public onStop(Z)V
    .locals 4
    .param p1, "result"    # Z

    .prologue
    .line 207
    const-string v0, "ServerService"

    const-string v1, "onStop"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "result : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/samsung/android/app/FileShareServer/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 208
    return-void
.end method

.method public stopFileShareDevice(Z)V
    .locals 5
    .param p1, "doByeBye"    # Z

    .prologue
    const/4 v4, 0x0

    .line 625
    const-string v1, "ServerService"

    const-string v2, "stopFileShareDevice"

    const-string v3, ""

    invoke-static {v1, v2, v3}, Lcom/samsung/android/app/FileShareServer/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 627
    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/app/FileShareServer/ServerService;->mFileShareDevice:Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper;

    if-eqz v1, :cond_0

    .line 628
    iget-object v1, p0, Lcom/samsung/android/app/FileShareServer/ServerService;->mFileShareDevice:Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper;

    invoke-virtual {v1, p0}, Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper;->removeDeviceEventListener(Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper$IDeviceEvent;)V

    .line 629
    iget-object v1, p0, Lcom/samsung/android/app/FileShareServer/ServerService;->mFileShareDevice:Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper;

    invoke-virtual {v1, p0}, Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper;->removeSessionEventListener(Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper$ISessionEvent;)V

    .line 630
    iget-object v1, p0, Lcom/samsung/android/app/FileShareServer/ServerService;->mFileShareDevice:Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper;

    invoke-virtual {v1}, Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper;->stop()I

    .line 631
    iget-object v1, p0, Lcom/samsung/android/app/FileShareServer/ServerService;->mFileShareDevice:Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper;

    invoke-virtual {v1}, Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper;->terminate()I

    .line 633
    :cond_0
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/samsung/android/app/FileShareServer/ServerService;->mFileShareDevice:Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 639
    :goto_0
    return-void

    .line 634
    :catch_0
    move-exception v0

    .line 635
    .local v0, "e":Ljava/lang/Exception;
    iput-object v4, p0, Lcom/samsung/android/app/FileShareServer/ServerService;->mFileShareDevice:Lcom/samsung/android/allshare/stack/fileshare/api/FileShareDeviceWrapper;

    .line 636
    const-string v1, "ServerService"

    const-string v2, "stopFileShareDevice"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Exception on stopFileShareDevice of Device, Noti"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/samsung/android/app/FileShareServer/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
