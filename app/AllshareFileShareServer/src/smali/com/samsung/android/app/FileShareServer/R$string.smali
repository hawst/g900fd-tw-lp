.class public final Lcom/samsung/android/app/FileShareServer/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/FileShareServer/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final all_file_transfer_cancelled:I = 0x7f06000c

.field public static final app_name:I = 0x7f06001a

.field public static final byteShort:I = 0x7f06001e

.field public static final cancel:I = 0x7f060008

.field public static final file_received_ps:I = 0x7f060019

.field public static final files_received:I = 0x7f060009

.field public static final files_received_ps:I = 0x7f060018

.field public static final from:I = 0x7f06000f

.field public static final gigabyteShort:I = 0x7f060021

.field public static final inbound_transfer:I = 0x7f060000

.field public static final kilobyteShort:I = 0x7f06001f

.field public static final megabyteShort:I = 0x7f060020

.field public static final not_enough_memory:I = 0x7f06000e

.field public static final open_file:I = 0x7f060007

.field public static final pd_cancelled:I = 0x7f06000b

.field public static final pd_failed:I = 0x7f060012

.field public static final pd_more:I = 0x7f060016

.field public static final pd_received:I = 0x7f06000a

.field public static final ps_and_pd_more:I = 0x7f060017

.field public static final received_files:I = 0x7f060004

.field public static final receiving:I = 0x7f060001

.field public static final receiving_cancelled:I = 0x7f060010

.field public static final receiving_failed:I = 0x7f060013

.field public static final receiving_filename:I = 0x7f06000d

.field public static final receiving_from_devicename:I = 0x7f060011

.field public static final saved_folder:I = 0x7f06001b

.field public static final sender:I = 0x7f060003

.field public static final start_to_receive_files_from:I = 0x7f060002

.field public static final stms_appgroup:I = 0x7f06001d

.field public static final stms_version:I = 0x7f06001c

.field public static final tap_to_open:I = 0x7f060005

.field public static final terabyteShort:I = 0x7f060022

.field public static final this_file_does_not_exist:I = 0x7f060014

.field public static final total:I = 0x7f060015

.field public static final unknown_file:I = 0x7f060006


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 79
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
