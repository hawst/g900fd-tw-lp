.class public Lcom/samsung/android/app/FileShareServer/InboundActivity;
.super Landroid/app/Activity;
.source "InboundActivity.java"


# static fields
.field private static final SIDE_PADDING:I = 0x0

.field private static final SIDE_PADDING_PERCENT:F = 1.0f

.field private static final TAGClass:Ljava/lang/String; = "InboundActivity : "


# instance fields
.field blankLayout:Landroid/widget/LinearLayout;

.field center:Landroid/widget/RelativeLayout;

.field private deviceType:Ljava/lang/String;

.field left:Landroid/widget/RelativeLayout;

.field private mIsGrandeDisplay:Z

.field private mIsLandscape:Z

.field right:Landroid/widget/RelativeLayout;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 20
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 23
    iput-boolean v0, p0, Lcom/samsung/android/app/FileShareServer/InboundActivity;->mIsLandscape:Z

    .line 25
    iput-boolean v0, p0, Lcom/samsung/android/app/FileShareServer/InboundActivity;->mIsGrandeDisplay:Z

    .line 31
    const-string v0, "ro.build.characteristics"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/FileShareServer/InboundActivity;->deviceType:Ljava/lang/String;

    return-void
.end method

.method public static getDisplayWidth(Landroid/content/Context;)I
    .locals 9
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 164
    const-string v6, "window"

    invoke-virtual {p0, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/view/WindowManager;

    .line 165
    .local v5, "wm":Landroid/view/WindowManager;
    const/4 v4, 0x0

    .line 168
    .local v4, "maxWidth":I
    :try_start_0
    invoke-interface {v5}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    .line 169
    .local v0, "display":Landroid/view/Display;
    const-class v6, Landroid/view/Display;

    const-string v7, "getRawWidth"

    const/4 v8, 0x0

    new-array v8, v8, [Ljava/lang/Class;

    invoke-virtual {v6, v7, v8}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v2

    .line 171
    .local v2, "getRawW":Ljava/lang/reflect/Method;
    const/4 v6, 0x0

    new-array v6, v6, [Ljava/lang/Object;

    invoke-virtual {v2, v0, v6}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v4

    .line 179
    .end local v0    # "display":Landroid/view/Display;
    .end local v2    # "getRawW":Ljava/lang/reflect/Method;
    :goto_0
    return v4

    .line 172
    :catch_0
    move-exception v1

    .line 173
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    .line 174
    new-instance v3, Landroid/util/DisplayMetrics;

    invoke-direct {v3}, Landroid/util/DisplayMetrics;-><init>()V

    .line 175
    .local v3, "matrix":Landroid/util/DisplayMetrics;
    invoke-interface {v5}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v6

    invoke-virtual {v6, v3}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 177
    iget v4, v3, Landroid/util/DisplayMetrics;->widthPixels:I

    goto :goto_0
.end method

.method private setGrandeDisplayView(Z)V
    .locals 10
    .param p1, "land"    # Z

    .prologue
    const/4 v9, 0x0

    const/high16 v8, 0x3f800000    # 1.0f

    const/4 v7, 0x0

    const/4 v6, -0x1

    .line 113
    const-string v0, "#002835"

    .line 115
    .local v0, "backgroundcolor":Ljava/lang/String;
    const v4, 0x7f080011

    invoke-virtual {p0, v4}, Lcom/samsung/android/app/FileShareServer/InboundActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/LinearLayout;

    iput-object v4, p0, Lcom/samsung/android/app/FileShareServer/InboundActivity;->blankLayout:Landroid/widget/LinearLayout;

    .line 116
    const v4, 0x7f080012

    invoke-virtual {p0, v4}, Lcom/samsung/android/app/FileShareServer/InboundActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/RelativeLayout;

    iput-object v4, p0, Lcom/samsung/android/app/FileShareServer/InboundActivity;->left:Landroid/widget/RelativeLayout;

    .line 117
    const v4, 0x7f080015

    invoke-virtual {p0, v4}, Lcom/samsung/android/app/FileShareServer/InboundActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/RelativeLayout;

    iput-object v4, p0, Lcom/samsung/android/app/FileShareServer/InboundActivity;->right:Landroid/widget/RelativeLayout;

    .line 118
    const v4, 0x7f080013

    invoke-virtual {p0, v4}, Lcom/samsung/android/app/FileShareServer/InboundActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/RelativeLayout;

    iput-object v4, p0, Lcom/samsung/android/app/FileShareServer/InboundActivity;->center:Landroid/widget/RelativeLayout;

    .line 120
    iget-object v4, p0, Lcom/samsung/android/app/FileShareServer/InboundActivity;->blankLayout:Landroid/widget/LinearLayout;

    iget-object v5, p0, Lcom/samsung/android/app/FileShareServer/InboundActivity;->left:Landroid/widget/RelativeLayout;

    invoke-virtual {v4, v5}, Landroid/widget/LinearLayout;->removeView(Landroid/view/View;)V

    .line 121
    iget-object v4, p0, Lcom/samsung/android/app/FileShareServer/InboundActivity;->blankLayout:Landroid/widget/LinearLayout;

    iget-object v5, p0, Lcom/samsung/android/app/FileShareServer/InboundActivity;->right:Landroid/widget/RelativeLayout;

    invoke-virtual {v4, v5}, Landroid/widget/LinearLayout;->removeView(Landroid/view/View;)V

    .line 122
    iget-object v4, p0, Lcom/samsung/android/app/FileShareServer/InboundActivity;->blankLayout:Landroid/widget/LinearLayout;

    iget-object v5, p0, Lcom/samsung/android/app/FileShareServer/InboundActivity;->center:Landroid/widget/RelativeLayout;

    invoke-virtual {v4, v5}, Landroid/widget/LinearLayout;->removeView(Landroid/view/View;)V

    .line 124
    if-eqz p1, :cond_0

    .line 125
    new-instance v2, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v2, v7, v6, v8}, Landroid/widget/LinearLayout$LayoutParams;-><init>(IIF)V

    .line 127
    .local v2, "leftParams":Landroid/widget/LinearLayout$LayoutParams;
    new-instance v3, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v3, v7, v6, v8}, Landroid/widget/LinearLayout$LayoutParams;-><init>(IIF)V

    .line 129
    .local v3, "rightParams":Landroid/widget/LinearLayout$LayoutParams;
    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    const/high16 v4, 0x41000000    # 8.0f

    invoke-direct {v1, v7, v6, v4}, Landroid/widget/LinearLayout$LayoutParams;-><init>(IIF)V

    .line 132
    .local v1, "centerParams":Landroid/widget/LinearLayout$LayoutParams;
    iget-object v4, p0, Lcom/samsung/android/app/FileShareServer/InboundActivity;->left:Landroid/widget/RelativeLayout;

    invoke-virtual {p0}, Lcom/samsung/android/app/FileShareServer/InboundActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f02000e

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/RelativeLayout;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 134
    iget-object v4, p0, Lcom/samsung/android/app/FileShareServer/InboundActivity;->right:Landroid/widget/RelativeLayout;

    invoke-virtual {p0}, Lcom/samsung/android/app/FileShareServer/InboundActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f020010

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/RelativeLayout;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 137
    iget-object v4, p0, Lcom/samsung/android/app/FileShareServer/InboundActivity;->left:Landroid/widget/RelativeLayout;

    invoke-virtual {v4, v2}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 138
    iget-object v4, p0, Lcom/samsung/android/app/FileShareServer/InboundActivity;->right:Landroid/widget/RelativeLayout;

    invoke-virtual {v4, v3}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 139
    iget-object v4, p0, Lcom/samsung/android/app/FileShareServer/InboundActivity;->center:Landroid/widget/RelativeLayout;

    invoke-virtual {v4, v1}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 157
    .end local v1    # "centerParams":Landroid/widget/LinearLayout$LayoutParams;
    .end local v2    # "leftParams":Landroid/widget/LinearLayout$LayoutParams;
    .end local v3    # "rightParams":Landroid/widget/LinearLayout$LayoutParams;
    :goto_0
    iget-object v4, p0, Lcom/samsung/android/app/FileShareServer/InboundActivity;->blankLayout:Landroid/widget/LinearLayout;

    iget-object v5, p0, Lcom/samsung/android/app/FileShareServer/InboundActivity;->left:Landroid/widget/RelativeLayout;

    invoke-virtual {v4, v5}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 158
    iget-object v4, p0, Lcom/samsung/android/app/FileShareServer/InboundActivity;->blankLayout:Landroid/widget/LinearLayout;

    iget-object v5, p0, Lcom/samsung/android/app/FileShareServer/InboundActivity;->center:Landroid/widget/RelativeLayout;

    invoke-virtual {v4, v5}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 159
    iget-object v4, p0, Lcom/samsung/android/app/FileShareServer/InboundActivity;->blankLayout:Landroid/widget/LinearLayout;

    iget-object v5, p0, Lcom/samsung/android/app/FileShareServer/InboundActivity;->right:Landroid/widget/RelativeLayout;

    invoke-virtual {v4, v5}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 160
    return-void

    .line 141
    :cond_0
    new-instance v2, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v2, v7, v6}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 143
    .local v2, "leftParams":Landroid/widget/RelativeLayout$LayoutParams;
    const/16 v4, 0x9

    invoke-virtual {v2, v4, v6}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 144
    new-instance v3, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v3, v7, v6}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 146
    .local v3, "rightParams":Landroid/widget/RelativeLayout$LayoutParams;
    const/16 v4, 0xb

    invoke-virtual {v3, v4, v6}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 147
    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-static {p0}, Lcom/samsung/android/app/FileShareServer/InboundActivity;->getDisplayWidth(Landroid/content/Context;)I

    move-result v4

    add-int/lit8 v4, v4, 0x0

    invoke-direct {v1, v4, v6}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 151
    .local v1, "centerParams":Landroid/widget/RelativeLayout$LayoutParams;
    iget-object v4, p0, Lcom/samsung/android/app/FileShareServer/InboundActivity;->left:Landroid/widget/RelativeLayout;

    invoke-virtual {v4, v9}, Landroid/widget/RelativeLayout;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 152
    iget-object v4, p0, Lcom/samsung/android/app/FileShareServer/InboundActivity;->right:Landroid/widget/RelativeLayout;

    invoke-virtual {v4, v9}, Landroid/widget/RelativeLayout;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 153
    iget-object v4, p0, Lcom/samsung/android/app/FileShareServer/InboundActivity;->left:Landroid/widget/RelativeLayout;

    invoke-virtual {v4, v2}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 154
    iget-object v4, p0, Lcom/samsung/android/app/FileShareServer/InboundActivity;->right:Landroid/widget/RelativeLayout;

    invoke-virtual {v4, v3}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 155
    iget-object v4, p0, Lcom/samsung/android/app/FileShareServer/InboundActivity;->center:Landroid/widget/RelativeLayout;

    invoke-virtual {v4, v1}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0
.end method


# virtual methods
.method public onBackPressed()V
    .locals 3

    .prologue
    .line 92
    invoke-super {p0}, Landroid/app/Activity;->onBackPressed()V

    .line 93
    invoke-virtual {p0}, Lcom/samsung/android/app/FileShareServer/InboundActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const v2, 0x7f080014

    invoke-virtual {v1, v2}, Landroid/app/FragmentManager;->findFragmentById(I)Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/app/FileShareServer/InboundFragment;

    .line 95
    .local v0, "inboundFragment":Lcom/samsung/android/app/FileShareServer/InboundFragment;
    if-eqz v0, :cond_0

    .line 96
    invoke-virtual {v0}, Lcom/samsung/android/app/FileShareServer/InboundFragment;->onBackPressed()V

    .line 98
    :cond_0
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    const/4 v0, 0x1

    .line 102
    iget v1, p1, Landroid/content/res/Configuration;->orientation:I

    if-ne v1, v0, :cond_0

    const/4 v0, 0x0

    :cond_0
    iput-boolean v0, p0, Lcom/samsung/android/app/FileShareServer/InboundActivity;->mIsLandscape:Z

    .line 105
    iget-boolean v0, p0, Lcom/samsung/android/app/FileShareServer/InboundActivity;->mIsGrandeDisplay:Z

    if-eqz v0, :cond_1

    .line 106
    iget-boolean v0, p0, Lcom/samsung/android/app/FileShareServer/InboundActivity;->mIsLandscape:Z

    invoke-direct {p0, v0}, Lcom/samsung/android/app/FileShareServer/InboundActivity;->setGrandeDisplayView(Z)V

    .line 108
    :cond_1
    invoke-super {p0, p1}, Landroid/app/Activity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 109
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/16 v3, 0x400

    .line 40
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 42
    iget-object v1, p0, Lcom/samsung/android/app/FileShareServer/InboundActivity;->deviceType:Ljava/lang/String;

    const-string v2, "tablet"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 43
    invoke-virtual {p0}, Lcom/samsung/android/app/FileShareServer/InboundActivity;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1, v3, v3}, Landroid/view/Window;->setFlags(II)V

    .line 47
    :cond_0
    const v1, 0x7f030003

    invoke-virtual {p0, v1}, Lcom/samsung/android/app/FileShareServer/InboundActivity;->setContentView(I)V

    .line 48
    invoke-virtual {p0}, Lcom/samsung/android/app/FileShareServer/InboundActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    .line 49
    .local v0, "titleBar":Landroid/app/ActionBar;
    const/high16 v1, 0x7f060000

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setTitle(I)V

    .line 51
    const-string v1, "grande"

    const-string v2, "ro.build.scafe.size"

    invoke-static {v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/samsung/android/app/FileShareServer/InboundActivity;->mIsGrandeDisplay:Z

    .line 52
    invoke-virtual {p0}, Lcom/samsung/android/app/FileShareServer/InboundActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget v1, v1, Landroid/content/res/Configuration;->orientation:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_2

    const/4 v1, 0x1

    :goto_0
    iput-boolean v1, p0, Lcom/samsung/android/app/FileShareServer/InboundActivity;->mIsLandscape:Z

    .line 55
    iget-boolean v1, p0, Lcom/samsung/android/app/FileShareServer/InboundActivity;->mIsGrandeDisplay:Z

    if-eqz v1, :cond_1

    .line 56
    iget-boolean v1, p0, Lcom/samsung/android/app/FileShareServer/InboundActivity;->mIsLandscape:Z

    invoke-direct {p0, v1}, Lcom/samsung/android/app/FileShareServer/InboundActivity;->setGrandeDisplayView(Z)V

    .line 58
    :cond_1
    return-void

    .line 52
    :cond_2
    const/4 v1, 0x0

    goto :goto_0
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 7
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v6, 0x1

    .line 63
    invoke-super {p0, p1}, Landroid/app/Activity;->onNewIntent(Landroid/content/Intent;)V

    .line 64
    const-string v2, "InboundActivity : "

    const-string v3, "onNewIntent"

    const-string v4, ""

    invoke-static {v2, v3, v4}, Lcom/samsung/android/app/FileShareServer/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 66
    const/4 v1, 0x0

    .line 67
    .local v1, "oldAct":Ljava/lang/String;
    const/4 v0, 0x0

    .line 69
    .local v0, "newAct":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/samsung/android/app/FileShareServer/InboundActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 70
    invoke-virtual {p0}, Lcom/samsung/android/app/FileShareServer/InboundActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    .line 71
    const-string v2, "InboundActivity : "

    const-string v3, "onNewIntent"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "old : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/samsung/android/app/FileShareServer/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 74
    :cond_0
    if-eqz p1, :cond_3

    .line 75
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 76
    const-string v2, "InboundActivity : "

    const-string v3, "onNewIntent"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "new : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/samsung/android/app/FileShareServer/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 77
    const-string v2, "onNewIntent"

    invoke-virtual {p1, v2, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 79
    if-eqz v1, :cond_1

    const-string v2, "com.samsung.android.app.FileShareServer.RECEIVE_RESULT"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 80
    const-string v2, "bOldAct"

    invoke-virtual {p1, v2, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 82
    :cond_1
    if-eqz v0, :cond_2

    const-string v2, "com.samsung.android.app.FileShareServer.RECEIVE_RESULT"

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 83
    const-string v2, "bNewAct"

    invoke-virtual {p1, v2, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 85
    :cond_2
    invoke-virtual {p0, p1}, Lcom/samsung/android/app/FileShareServer/InboundActivity;->setIntent(Landroid/content/Intent;)V

    .line 87
    :cond_3
    return-void
.end method
