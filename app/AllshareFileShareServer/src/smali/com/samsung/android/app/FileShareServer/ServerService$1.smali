.class Lcom/samsung/android/app/FileShareServer/ServerService$1;
.super Landroid/content/BroadcastReceiver;
.source "ServerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/FileShareServer/ServerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/FileShareServer/ServerService;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/FileShareServer/ServerService;)V
    .locals 0

    .prologue
    .line 894
    iput-object p1, p0, Lcom/samsung/android/app/FileShareServer/ServerService$1;->this$0:Lcom/samsung/android/app/FileShareServer/ServerService;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 11
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 897
    const-string v7, "ServerService"

    const-string v8, "mBroadcastReceiver"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "onReceive "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v7, v8, v9}, Lcom/samsung/android/app/FileShareServer/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 899
    :try_start_0
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 900
    .local v0, "action":Ljava/lang/String;
    const-string v7, "android.net.wifi.p2p.CONNECTION_STATE_CHANGE"

    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 906
    const-string v7, "networkInfo"

    invoke-virtual {p2, v7}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v5

    check-cast v5, Landroid/net/NetworkInfo;

    .line 908
    .local v5, "netInfo":Landroid/net/NetworkInfo;
    const-string v7, "ServerService"

    const-string v8, "mBroadcastReceiver"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "onReceive state = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v5}, Landroid/net/NetworkInfo;->getDetailedState()Landroid/net/NetworkInfo$DetailedState;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v7, v8, v9}, Lcom/samsung/android/app/FileShareServer/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 910
    sget-object v7, Landroid/net/NetworkInfo$DetailedState;->DISCONNECTED:Landroid/net/NetworkInfo$DetailedState;

    invoke-virtual {v5}, Landroid/net/NetworkInfo;->getDetailedState()Landroid/net/NetworkInfo$DetailedState;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/net/NetworkInfo$DetailedState;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 912
    sget v7, Lcom/samsung/android/app/FileShareServer/Utils;->mServerProperty:I

    if-lez v7, :cond_0

    .line 913
    const/4 v7, 0x0

    sput v7, Lcom/samsung/android/app/FileShareServer/Utils;->mServerProperty:I

    .line 914
    iget-object v7, p0, Lcom/samsung/android/app/FileShareServer/ServerService$1;->this$0:Lcom/samsung/android/app/FileShareServer/ServerService;

    # getter for: Lcom/samsung/android/app/FileShareServer/ServerService;->mThemeContext:Landroid/content/Context;
    invoke-static {v7}, Lcom/samsung/android/app/FileShareServer/ServerService;->access$000(Lcom/samsung/android/app/FileShareServer/ServerService;)Landroid/content/Context;

    move-result-object v7

    sget v8, Lcom/samsung/android/app/FileShareServer/Utils;->mServerProperty:I

    invoke-static {v7, v8}, Lcom/samsung/android/app/FileShareServer/Utils;->setServerIsRunning(Landroid/content/Context;I)V

    .line 917
    :cond_0
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    # getter for: Lcom/samsung/android/app/FileShareServer/ServerService;->mWorkingList:Ljava/util/ArrayList;
    invoke-static {}, Lcom/samsung/android/app/FileShareServer/ServerService;->access$100()Ljava/util/ArrayList;

    move-result-object v7

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v7

    if-ge v3, v7, :cond_6

    .line 918
    # getter for: Lcom/samsung/android/app/FileShareServer/ServerService;->mWorkingList:Ljava/util/ArrayList;
    invoke-static {}, Lcom/samsung/android/app/FileShareServer/ServerService;->access$100()Ljava/util/ArrayList;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/android/app/FileShareServer/InboundInfo;

    .line 919
    .local v4, "info":Lcom/samsung/android/app/FileShareServer/InboundInfo;
    invoke-virtual {v4}, Lcom/samsung/android/app/FileShareServer/InboundInfo;->getTotalCount()I

    move-result v7

    invoke-virtual {v4}, Lcom/samsung/android/app/FileShareServer/InboundInfo;->getTotalSavedCount()I

    move-result v8

    sub-int/2addr v7, v8

    invoke-virtual {v4, v7}, Lcom/samsung/android/app/FileShareServer/InboundInfo;->setTotalFailedCount(I)V

    .line 921
    invoke-virtual {v4}, Lcom/samsung/android/app/FileShareServer/InboundInfo;->getCurrentStatus()I

    move-result v7

    const/16 v8, 0x3ea

    if-eq v7, v8, :cond_1

    invoke-virtual {v4}, Lcom/samsung/android/app/FileShareServer/InboundInfo;->getCurrentStatus()I

    move-result v7

    const/16 v8, 0x3ed

    if-eq v7, v8, :cond_1

    invoke-virtual {v4}, Lcom/samsung/android/app/FileShareServer/InboundInfo;->getCurrentStatus()I

    move-result v7

    const/16 v8, 0x3e8

    if-ne v7, v8, :cond_5

    .line 924
    :cond_1
    iget-object v7, p0, Lcom/samsung/android/app/FileShareServer/ServerService$1;->this$0:Lcom/samsung/android/app/FileShareServer/ServerService;

    # getter for: Lcom/samsung/android/app/FileShareServer/ServerService;->mNotiManager:Landroid/app/NotificationManager;
    invoke-static {v7}, Lcom/samsung/android/app/FileShareServer/ServerService;->access$200(Lcom/samsung/android/app/FileShareServer/ServerService;)Landroid/app/NotificationManager;

    move-result-object v7

    if-eqz v7, :cond_2

    .line 925
    const-string v7, "ServerService"

    const-string v8, "mBroadcastReceiver"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "onReceive Remove Progressing Notification. ID = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v4}, Lcom/samsung/android/app/FileShareServer/InboundInfo;->getInboundID()I

    move-result v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v7, v8, v9}, Lcom/samsung/android/app/FileShareServer/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 928
    iget-object v7, p0, Lcom/samsung/android/app/FileShareServer/ServerService$1;->this$0:Lcom/samsung/android/app/FileShareServer/ServerService;

    # getter for: Lcom/samsung/android/app/FileShareServer/ServerService;->mNotiManager:Landroid/app/NotificationManager;
    invoke-static {v7}, Lcom/samsung/android/app/FileShareServer/ServerService;->access$200(Lcom/samsung/android/app/FileShareServer/ServerService;)Landroid/app/NotificationManager;

    move-result-object v7

    const-string v8, "FileShareServer"

    invoke-virtual {v4}, Lcom/samsung/android/app/FileShareServer/InboundInfo;->getInboundID()I

    move-result v9

    mul-int/lit8 v9, v9, 0x2

    invoke-virtual {v7, v8, v9}, Landroid/app/NotificationManager;->cancel(Ljava/lang/String;I)V

    .line 930
    :cond_2
    iget-object v7, p0, Lcom/samsung/android/app/FileShareServer/ServerService$1;->this$0:Lcom/samsung/android/app/FileShareServer/ServerService;

    const/16 v8, 0x3e9

    # invokes: Lcom/samsung/android/app/FileShareServer/ServerService;->setErrorInboundInfo(II)V
    invoke-static {v7, v3, v8}, Lcom/samsung/android/app/FileShareServer/ServerService;->access$300(Lcom/samsung/android/app/FileShareServer/ServerService;II)V

    .line 931
    invoke-virtual {v4}, Lcom/samsung/android/app/FileShareServer/InboundInfo;->getSessionID()Ljava/lang/String;

    move-result-object v6

    .line 932
    .local v6, "sessionID":Ljava/lang/String;
    if-eqz v6, :cond_3

    .line 933
    const-string v7, "ServerService"

    const-string v8, "mBroadcastReceiver"

    const-string v9, "onReceive DB - add"

    invoke-static {v7, v8, v9}, Lcom/samsung/android/app/FileShareServer/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 935
    iget-object v7, p0, Lcom/samsung/android/app/FileShareServer/ServerService$1;->this$0:Lcom/samsung/android/app/FileShareServer/ServerService;

    iget-object v7, v7, Lcom/samsung/android/app/FileShareServer/ServerService;->mDbAdapter:Lcom/samsung/android/app/FileShareServer/DbAdapter;

    invoke-virtual {v7, v4}, Lcom/samsung/android/app/FileShareServer/DbAdapter;->insertInboundInfo(Lcom/samsung/android/app/FileShareServer/InboundInfo;)V

    .line 936
    iget-object v7, p0, Lcom/samsung/android/app/FileShareServer/ServerService$1;->this$0:Lcom/samsung/android/app/FileShareServer/ServerService;

    # invokes: Lcom/samsung/android/app/FileShareServer/ServerService;->AddResultNotification(Ljava/lang/String;)V
    invoke-static {v7, v6}, Lcom/samsung/android/app/FileShareServer/ServerService;->access$400(Lcom/samsung/android/app/FileShareServer/ServerService;Ljava/lang/String;)V

    .line 917
    .end local v6    # "sessionID":Ljava/lang/String;
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_0

    .line 939
    .restart local v6    # "sessionID":Ljava/lang/String;
    :cond_3
    const-string v7, "ServerService"

    const-string v8, "mBroadcastReceiver"

    const-string v9, "onReceive No RunnningSessions."

    invoke-static {v7, v8, v9}, Lcom/samsung/android/app/FileShareServer/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_1

    .line 954
    .end local v0    # "action":Ljava/lang/String;
    .end local v3    # "i":I
    .end local v4    # "info":Lcom/samsung/android/app/FileShareServer/InboundInfo;
    .end local v5    # "netInfo":Landroid/net/NetworkInfo;
    .end local v6    # "sessionID":Ljava/lang/String;
    :catch_0
    move-exception v1

    .line 955
    .local v1, "e":Landroid/content/res/Resources$NotFoundException;
    const-string v7, "ServerService"

    const-string v8, "mBroadcastReceiver"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "onReceive NotFoundException on broadcast receiver"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v7, v8, v9}, Lcom/samsung/android/app/FileShareServer/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 961
    .end local v1    # "e":Landroid/content/res/Resources$NotFoundException;
    :cond_4
    :goto_2
    return-void

    .line 943
    .restart local v0    # "action":Ljava/lang/String;
    .restart local v3    # "i":I
    .restart local v4    # "info":Lcom/samsung/android/app/FileShareServer/InboundInfo;
    .restart local v5    # "netInfo":Landroid/net/NetworkInfo;
    :cond_5
    :try_start_1
    const-string v7, "ServerService"

    const-string v8, "mBroadcastReceiver"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "onReceive getCurrentStatus()"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v4}, Lcom/samsung/android/app/FileShareServer/InboundInfo;->getCurrentStatus()I

    move-result v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v7, v8, v9}, Lcom/samsung/android/app/FileShareServer/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    .line 957
    .end local v0    # "action":Ljava/lang/String;
    .end local v3    # "i":I
    .end local v4    # "info":Lcom/samsung/android/app/FileShareServer/InboundInfo;
    .end local v5    # "netInfo":Landroid/net/NetworkInfo;
    :catch_1
    move-exception v2

    .line 958
    .local v2, "e1":Ljava/lang/Exception;
    const-string v7, "ServerService"

    const-string v8, "mBroadcastReceiver"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "onReceive Exception on broadcast receiver"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v7, v8, v9}, Lcom/samsung/android/app/FileShareServer/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 947
    .end local v2    # "e1":Ljava/lang/Exception;
    .restart local v0    # "action":Ljava/lang/String;
    .restart local v3    # "i":I
    .restart local v5    # "netInfo":Landroid/net/NetworkInfo;
    :cond_6
    :try_start_2
    iget-object v7, p0, Lcom/samsung/android/app/FileShareServer/ServerService$1;->this$0:Lcom/samsung/android/app/FileShareServer/ServerService;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Lcom/samsung/android/app/FileShareServer/ServerService;->stopFileShareDevice(Z)V

    .line 950
    # getter for: Lcom/samsung/android/app/FileShareServer/ServerService;->mWorkingList:Ljava/util/ArrayList;
    invoke-static {}, Lcom/samsung/android/app/FileShareServer/ServerService;->access$100()Ljava/util/ArrayList;

    move-result-object v7

    invoke-virtual {v7}, Ljava/util/ArrayList;->clear()V

    .line 951
    iget-object v7, p0, Lcom/samsung/android/app/FileShareServer/ServerService$1;->this$0:Lcom/samsung/android/app/FileShareServer/ServerService;

    # invokes: Lcom/samsung/android/app/FileShareServer/ServerService;->releaseWakeLock()V
    invoke-static {v7}, Lcom/samsung/android/app/FileShareServer/ServerService;->access$500(Lcom/samsung/android/app/FileShareServer/ServerService;)V
    :try_end_2
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_2
.end method
