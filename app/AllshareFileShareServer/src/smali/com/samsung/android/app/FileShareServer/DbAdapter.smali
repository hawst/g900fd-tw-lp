.class public Lcom/samsung/android/app/FileShareServer/DbAdapter;
.super Ljava/lang/Object;
.source "DbAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/app/FileShareServer/DbAdapter$DatabaseHelper;
    }
.end annotation


# static fields
.field private static final DATABASE_CREATE_1:Ljava/lang/String; = "CREATE TABLE status_tbl (_id integer primary key autoincrement,inbound_id integer,sender_name text not null,total_size long,total_count integer,fail_count integer,status integer);"

.field private static final DATABASE_CREATE_2:Ljava/lang/String; = "CREATE TABLE filelist_tbl (_id integer primary key autoincrement,inbound_id integer,file_path text not null);"

.field private static final DATABASE_NAME:Ljava/lang/String; = "inbound_transfer.db"

.field private static final DATABASE_TABLE_1:Ljava/lang/String; = "status_tbl"

.field private static final DATABASE_TABLE_2:Ljava/lang/String; = "filelist_tbl"

.field private static final DATABASE_VERSION:I = 0x1

.field public static final KEY_FAIL_COUNT:Ljava/lang/String; = "fail_count"

.field public static final KEY_FILE_PATH:Ljava/lang/String; = "file_path"

.field public static final KEY_INBOUND_ID:Ljava/lang/String; = "inbound_id"

.field public static final KEY_ROWID:Ljava/lang/String; = "_id"

.field public static final KEY_SENDER_NAME:Ljava/lang/String; = "sender_name"

.field public static final KEY_STATUS:Ljava/lang/String; = "status"

.field public static final KEY_TOTAL_COUNT:Ljava/lang/String; = "total_count"

.field public static final KEY_TOTAL_SIZE:Ljava/lang/String; = "total_size"

.field private static final TAGClass:Ljava/lang/String; = "DbAdapter"


# instance fields
.field private mContext:Landroid/content/Context;

.field private mDb:Landroid/database/sqlite/SQLiteDatabase;

.field private mDbHelper:Lcom/samsung/android/app/FileShareServer/DbAdapter$DatabaseHelper;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 85
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 86
    iput-object p1, p0, Lcom/samsung/android/app/FileShareServer/DbAdapter;->mContext:Landroid/content/Context;

    .line 87
    return-void
.end method


# virtual methods
.method public clearDbTable()V
    .locals 2

    .prologue
    .line 131
    iget-object v0, p0, Lcom/samsung/android/app/FileShareServer/DbAdapter;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "DROP TABLE IF EXISTS status_tbl"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 132
    iget-object v0, p0, Lcom/samsung/android/app/FileShareServer/DbAdapter;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "DROP TABLE IF EXISTS filelist_tbl"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 134
    iget-object v0, p0, Lcom/samsung/android/app/FileShareServer/DbAdapter;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "CREATE TABLE status_tbl (_id integer primary key autoincrement,inbound_id integer,sender_name text not null,total_size long,total_count integer,fail_count integer,status integer);"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 135
    iget-object v0, p0, Lcom/samsung/android/app/FileShareServer/DbAdapter;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "CREATE TABLE filelist_tbl (_id integer primary key autoincrement,inbound_id integer,file_path text not null);"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 136
    return-void
.end method

.method public close()V
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lcom/samsung/android/app/FileShareServer/DbAdapter;->mDbHelper:Lcom/samsung/android/app/FileShareServer/DbAdapter$DatabaseHelper;

    invoke-virtual {v0}, Lcom/samsung/android/app/FileShareServer/DbAdapter$DatabaseHelper;->close()V

    .line 97
    iget-object v0, p0, Lcom/samsung/android/app/FileShareServer/DbAdapter;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    .line 98
    return-void
.end method

.method public deleteReceivedFileInfo(J)Z
    .locals 5
    .param p1, "rowId"    # J

    .prologue
    .line 127
    iget-object v0, p0, Lcom/samsung/android/app/FileShareServer/DbAdapter;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "filelist_tbl"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "_id="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public deleteStatusInfo(J)Z
    .locals 5
    .param p1, "rowId"    # J

    .prologue
    .line 123
    iget-object v0, p0, Lcom/samsung/android/app/FileShareServer/DbAdapter;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "status_tbl"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "_id="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public insertInboundInfo(Lcom/samsung/android/app/FileShareServer/InboundInfo;)V
    .locals 7
    .param p1, "info"    # Lcom/samsung/android/app/FileShareServer/InboundInfo;

    .prologue
    const/4 v6, 0x0

    .line 101
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 103
    .local v2, "value":Landroid/content/ContentValues;
    const-string v3, "inbound_id"

    invoke-virtual {p1}, Lcom/samsung/android/app/FileShareServer/InboundInfo;->getInboundID()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 104
    const-string v3, "sender_name"

    invoke-virtual {p1}, Lcom/samsung/android/app/FileShareServer/InboundInfo;->getSenderName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 105
    const-string v3, "total_size"

    invoke-virtual {p1}, Lcom/samsung/android/app/FileShareServer/InboundInfo;->getTotalSize()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 106
    const-string v3, "total_count"

    invoke-virtual {p1}, Lcom/samsung/android/app/FileShareServer/InboundInfo;->getTotalCount()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 107
    const-string v3, "fail_count"

    invoke-virtual {p1}, Lcom/samsung/android/app/FileShareServer/InboundInfo;->getTotalFailedCount()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 108
    const-string v3, "status"

    invoke-virtual {p1}, Lcom/samsung/android/app/FileShareServer/InboundInfo;->getCurrentStatus()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 110
    iget-object v3, p0, Lcom/samsung/android/app/FileShareServer/DbAdapter;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    const-string v4, "status_tbl"

    invoke-virtual {v3, v4, v6, v2}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    .line 111
    invoke-virtual {v2}, Landroid/content/ContentValues;->clear()V

    .line 114
    invoke-virtual {p1}, Lcom/samsung/android/app/FileShareServer/InboundInfo;->getFileList()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/File;

    .line 115
    .local v0, "file":Ljava/io/File;
    const-string v3, "inbound_id"

    invoke-virtual {p1}, Lcom/samsung/android/app/FileShareServer/InboundInfo;->getInboundID()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 116
    const-string v3, "file_path"

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 117
    iget-object v3, p0, Lcom/samsung/android/app/FileShareServer/DbAdapter;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    const-string v4, "filelist_tbl"

    invoke-virtual {v3, v4, v6, v2}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    .line 118
    invoke-virtual {v2}, Landroid/content/ContentValues;->clear()V

    goto :goto_0

    .line 120
    .end local v0    # "file":Ljava/io/File;
    :cond_0
    return-void
.end method

.method public open()Lcom/samsung/android/app/FileShareServer/DbAdapter;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/database/SQLException;
        }
    .end annotation

    .prologue
    .line 90
    new-instance v0, Lcom/samsung/android/app/FileShareServer/DbAdapter$DatabaseHelper;

    iget-object v1, p0, Lcom/samsung/android/app/FileShareServer/DbAdapter;->mContext:Landroid/content/Context;

    invoke-direct {v0, p0, v1}, Lcom/samsung/android/app/FileShareServer/DbAdapter$DatabaseHelper;-><init>(Lcom/samsung/android/app/FileShareServer/DbAdapter;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/app/FileShareServer/DbAdapter;->mDbHelper:Lcom/samsung/android/app/FileShareServer/DbAdapter$DatabaseHelper;

    .line 91
    iget-object v0, p0, Lcom/samsung/android/app/FileShareServer/DbAdapter;->mDbHelper:Lcom/samsung/android/app/FileShareServer/DbAdapter$DatabaseHelper;

    invoke-virtual {v0}, Lcom/samsung/android/app/FileShareServer/DbAdapter$DatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/FileShareServer/DbAdapter;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    .line 92
    return-object p0
.end method

.method public queryReceivedFileInfo(I)Landroid/database/Cursor;
    .locals 9
    .param p1, "inboundId"    # I

    .prologue
    const/4 v2, 0x0

    .line 148
    iget-object v0, p0, Lcom/samsung/android/app/FileShareServer/DbAdapter;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "filelist_tbl"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "inbound_id="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object v4, v2

    move-object v5, v2

    move-object v6, v2

    move-object v7, v2

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 150
    .local v8, "cursor":Landroid/database/Cursor;
    if-eqz v8, :cond_0

    .line 151
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    .line 153
    :cond_0
    return-object v8
.end method

.method public queryStatusInfo(I)Landroid/database/Cursor;
    .locals 9
    .param p1, "inboundId"    # I

    .prologue
    const/4 v2, 0x0

    .line 139
    iget-object v0, p0, Lcom/samsung/android/app/FileShareServer/DbAdapter;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "status_tbl"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "inbound_id="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object v4, v2

    move-object v5, v2

    move-object v6, v2

    move-object v7, v2

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 141
    .local v8, "cursor":Landroid/database/Cursor;
    if-eqz v8, :cond_0

    .line 142
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    .line 144
    :cond_0
    return-object v8
.end method
