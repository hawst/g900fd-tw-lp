.class Lcom/sec/phone/SecPhoneServiceCPOnUPG$1;
.super Landroid/os/Handler;
.source "SecPhoneServiceCPOnUPG.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/phone/SecPhoneServiceCPOnUPG;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/phone/SecPhoneServiceCPOnUPG;


# direct methods
.method constructor <init>(Lcom/sec/phone/SecPhoneServiceCPOnUPG;)V
    .locals 0

    .prologue
    .line 163
    iput-object p1, p0, Lcom/sec/phone/SecPhoneServiceCPOnUPG$1;->this$0:Lcom/sec/phone/SecPhoneServiceCPOnUPG;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 6
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 166
    iget-object v3, p0, Lcom/sec/phone/SecPhoneServiceCPOnUPG$1;->this$0:Lcom/sec/phone/SecPhoneServiceCPOnUPG;

    iget-object v3, v3, Lcom/sec/phone/SecPhoneServiceCPOnUPG;->LOG_TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "handleMessage(): "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/telephony/Rlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 168
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v1

    .line 169
    .local v1, "reply":Landroid/os/Message;
    invoke-virtual {v1, p1}, Landroid/os/Message;->copyFrom(Landroid/os/Message;)V

    .line 171
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v3

    const-string v4, "Action"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 173
    .local v0, "action":Ljava/lang/String;
    if-eqz v0, :cond_1

    .line 174
    iget-object v3, p0, Lcom/sec/phone/SecPhoneServiceCPOnUPG$1;->this$0:Lcom/sec/phone/SecPhoneServiceCPOnUPG;

    iget-object v3, v3, Lcom/sec/phone/SecPhoneServiceCPOnUPG;->LOG_TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Messenger Name : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/telephony/Rlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 176
    iget-object v3, p0, Lcom/sec/phone/SecPhoneServiceCPOnUPG$1;->this$0:Lcom/sec/phone/SecPhoneServiceCPOnUPG;

    iget-object v3, v3, Lcom/sec/phone/SecPhoneServiceCPOnUPG;->mRegisteredSender:Ljava/util/Map;

    invoke-interface {v3, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/os/Message;

    .line 177
    .local v2, "temp":Landroid/os/Message;
    if-nez v2, :cond_0

    .line 178
    iget-object v3, p0, Lcom/sec/phone/SecPhoneServiceCPOnUPG$1;->this$0:Lcom/sec/phone/SecPhoneServiceCPOnUPG;

    iget-object v3, v3, Lcom/sec/phone/SecPhoneServiceCPOnUPG;->mRegisteredSender:Ljava/util/Map;

    invoke-interface {v3, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 187
    .end local v2    # "temp":Landroid/os/Message;
    :goto_0
    return-void

    .line 180
    .restart local v2    # "temp":Landroid/os/Message;
    :cond_0
    iget-object v3, p0, Lcom/sec/phone/SecPhoneServiceCPOnUPG$1;->this$0:Lcom/sec/phone/SecPhoneServiceCPOnUPG;

    iget-object v3, v3, Lcom/sec/phone/SecPhoneServiceCPOnUPG;->LOG_TAG:Ljava/lang/String;

    const-string v4, "action is already exist. data will be updated"

    invoke-static {v3, v4}, Landroid/telephony/Rlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 181
    iget-object v3, p0, Lcom/sec/phone/SecPhoneServiceCPOnUPG$1;->this$0:Lcom/sec/phone/SecPhoneServiceCPOnUPG;

    iget-object v3, v3, Lcom/sec/phone/SecPhoneServiceCPOnUPG;->mRegisteredSender:Ljava/util/Map;

    invoke-interface {v3, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 182
    iget-object v3, p0, Lcom/sec/phone/SecPhoneServiceCPOnUPG$1;->this$0:Lcom/sec/phone/SecPhoneServiceCPOnUPG;

    iget-object v3, v3, Lcom/sec/phone/SecPhoneServiceCPOnUPG;->mRegisteredSender:Ljava/util/Map;

    invoke-interface {v3, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 185
    .end local v2    # "temp":Landroid/os/Message;
    :cond_1
    iget-object v3, p0, Lcom/sec/phone/SecPhoneServiceCPOnUPG$1;->this$0:Lcom/sec/phone/SecPhoneServiceCPOnUPG;

    invoke-virtual {v3, v1}, Lcom/sec/phone/SecPhoneServiceCPOnUPG;->invokeOemRilRequestRaw(Landroid/os/Message;)V

    goto :goto_0
.end method
