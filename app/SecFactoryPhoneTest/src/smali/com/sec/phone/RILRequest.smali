.class Lcom/sec/phone/RILRequest;
.super Ljava/lang/Object;
.source "RILRequest.java"


# static fields
.field private static final MAX_POOL_SIZE:I = 0x4

.field static final TAG:Ljava/lang/String; = "RILS"

.field static sNextSerial:I

.field private static sPool:Lcom/sec/phone/RILRequest;

.field private static sPoolSize:I

.field private static sPoolSync:Ljava/lang/Object;

.field static sSerialMonitor:Ljava/lang/Object;


# instance fields
.field mNext:Lcom/sec/phone/RILRequest;

.field mRequest:I

.field mResult:Landroid/os/Message;

.field mSerial:I

.field mp:Landroid/os/Parcel;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 44
    sput v1, Lcom/sec/phone/RILRequest;->sNextSerial:I

    .line 45
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/sec/phone/RILRequest;->sSerialMonitor:Ljava/lang/Object;

    .line 46
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/sec/phone/RILRequest;->sPoolSync:Ljava/lang/Object;

    .line 47
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/phone/RILRequest;->sPool:Lcom/sec/phone/RILRequest;

    .line 48
    sput v1, Lcom/sec/phone/RILRequest;->sPoolSize:I

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 113
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 114
    return-void
.end method

.method static obtain(ILandroid/os/Message;)Lcom/sec/phone/RILRequest;
    .locals 4
    .param p0, "request"    # I
    .param p1, "result"    # Landroid/os/Message;

    .prologue
    .line 66
    const/4 v0, 0x0

    .line 68
    .local v0, "rr":Lcom/sec/phone/RILRequest;
    sget-object v2, Lcom/sec/phone/RILRequest;->sPoolSync:Ljava/lang/Object;

    monitor-enter v2

    .line 69
    :try_start_0
    sget-object v1, Lcom/sec/phone/RILRequest;->sPool:Lcom/sec/phone/RILRequest;

    if-eqz v1, :cond_0

    .line 70
    sget-object v0, Lcom/sec/phone/RILRequest;->sPool:Lcom/sec/phone/RILRequest;

    .line 71
    iget-object v1, v0, Lcom/sec/phone/RILRequest;->mNext:Lcom/sec/phone/RILRequest;

    sput-object v1, Lcom/sec/phone/RILRequest;->sPool:Lcom/sec/phone/RILRequest;

    .line 72
    const/4 v1, 0x0

    iput-object v1, v0, Lcom/sec/phone/RILRequest;->mNext:Lcom/sec/phone/RILRequest;

    .line 73
    sget v1, Lcom/sec/phone/RILRequest;->sPoolSize:I

    add-int/lit8 v1, v1, -0x1

    sput v1, Lcom/sec/phone/RILRequest;->sPoolSize:I

    .line 75
    :cond_0
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 77
    if-nez v0, :cond_1

    .line 78
    new-instance v0, Lcom/sec/phone/RILRequest;

    .end local v0    # "rr":Lcom/sec/phone/RILRequest;
    invoke-direct {v0}, Lcom/sec/phone/RILRequest;-><init>()V

    .line 81
    .restart local v0    # "rr":Lcom/sec/phone/RILRequest;
    :cond_1
    sget-object v2, Lcom/sec/phone/RILRequest;->sSerialMonitor:Ljava/lang/Object;

    monitor-enter v2

    .line 82
    :try_start_1
    sget v1, Lcom/sec/phone/RILRequest;->sNextSerial:I

    add-int/lit8 v3, v1, 0x1

    sput v3, Lcom/sec/phone/RILRequest;->sNextSerial:I

    iput v1, v0, Lcom/sec/phone/RILRequest;->mSerial:I

    .line 83
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 84
    iput p0, v0, Lcom/sec/phone/RILRequest;->mRequest:I

    .line 85
    iput-object p1, v0, Lcom/sec/phone/RILRequest;->mResult:Landroid/os/Message;

    .line 86
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/phone/RILRequest;->mp:Landroid/os/Parcel;

    .line 88
    if-eqz p1, :cond_2

    invoke-virtual {p1}, Landroid/os/Message;->getTarget()Landroid/os/Handler;

    move-result-object v1

    if-nez v1, :cond_2

    iget-object v1, p1, Landroid/os/Message;->replyTo:Landroid/os/Messenger;

    if-nez v1, :cond_2

    .line 89
    new-instance v1, Ljava/lang/NullPointerException;

    const-string v2, "Message target must not be null"

    invoke-direct {v1, v2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 75
    :catchall_0
    move-exception v1

    :try_start_2
    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1

    .line 83
    :catchall_1
    move-exception v1

    :try_start_3
    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v1

    .line 93
    :cond_2
    iget-object v1, v0, Lcom/sec/phone/RILRequest;->mp:Landroid/os/Parcel;

    invoke-virtual {v1, p0}, Landroid/os/Parcel;->writeInt(I)V

    .line 94
    iget-object v1, v0, Lcom/sec/phone/RILRequest;->mp:Landroid/os/Parcel;

    iget v2, v0, Lcom/sec/phone/RILRequest;->mSerial:I

    invoke-virtual {v1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 96
    return-object v0
.end method

.method static resetSerial()V
    .locals 2

    .prologue
    .line 117
    sget-object v1, Lcom/sec/phone/RILRequest;->sSerialMonitor:Ljava/lang/Object;

    monitor-enter v1

    .line 118
    const/4 v0, 0x0

    :try_start_0
    sput v0, Lcom/sec/phone/RILRequest;->sNextSerial:I

    .line 119
    monitor-exit v1

    .line 120
    return-void

    .line 119
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method


# virtual methods
.method onError(ILjava/lang/Object;)V
    .locals 4
    .param p1, "error"    # I
    .param p2, "obj"    # Ljava/lang/Object;

    .prologue
    .line 141
    const-string v1, "RILS"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/phone/RILRequest;->serialString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "< "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/phone/RILRequest;->mRequest:I

    invoke-static {v3}, Lcom/sec/phone/SecPhoneService;->requestToString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " error: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/telephony/Rlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 144
    iget-object v1, p0, Lcom/sec/phone/RILRequest;->mResult:Landroid/os/Message;

    if-eqz v1, :cond_0

    .line 145
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    .line 146
    .local v0, "reply":Landroid/os/Message;
    iget-object v1, p0, Lcom/sec/phone/RILRequest;->mResult:Landroid/os/Message;

    invoke-virtual {v0, v1}, Landroid/os/Message;->copyFrom(Landroid/os/Message;)V

    .line 147
    invoke-virtual {v0}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "error"

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 149
    :try_start_0
    iget-object v1, p0, Lcom/sec/phone/RILRequest;->mResult:Landroid/os/Message;

    iget-object v1, v1, Landroid/os/Message;->replyTo:Landroid/os/Messenger;

    invoke-virtual {v1, v0}, Landroid/os/Messenger;->send(Landroid/os/Message;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 154
    .end local v0    # "reply":Landroid/os/Message;
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/sec/phone/RILRequest;->mp:Landroid/os/Parcel;

    if-eqz v1, :cond_1

    .line 155
    iget-object v1, p0, Lcom/sec/phone/RILRequest;->mp:Landroid/os/Parcel;

    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 156
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/phone/RILRequest;->mp:Landroid/os/Parcel;

    .line 158
    :cond_1
    return-void

    .line 150
    .restart local v0    # "reply":Landroid/os/Message;
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method release()V
    .locals 3

    .prologue
    .line 104
    sget-object v1, Lcom/sec/phone/RILRequest;->sPoolSync:Ljava/lang/Object;

    monitor-enter v1

    .line 105
    :try_start_0
    sget v0, Lcom/sec/phone/RILRequest;->sPoolSize:I

    const/4 v2, 0x4

    if-ge v0, v2, :cond_0

    .line 106
    sget-object v0, Lcom/sec/phone/RILRequest;->sPool:Lcom/sec/phone/RILRequest;

    iput-object v0, p0, Lcom/sec/phone/RILRequest;->mNext:Lcom/sec/phone/RILRequest;

    .line 107
    sput-object p0, Lcom/sec/phone/RILRequest;->sPool:Lcom/sec/phone/RILRequest;

    .line 108
    sget v0, Lcom/sec/phone/RILRequest;->sPoolSize:I

    add-int/lit8 v0, v0, 0x1

    sput v0, Lcom/sec/phone/RILRequest;->sPoolSize:I

    .line 110
    :cond_0
    monitor-exit v1

    .line 111
    return-void

    .line 110
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method serialString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 124
    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v4, 0x8

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 127
    .local v2, "sb":Ljava/lang/StringBuilder;
    iget v4, p0, Lcom/sec/phone/RILRequest;->mSerial:I

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    .line 130
    .local v3, "sn":Ljava/lang/String;
    const/16 v4, 0x5b

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 131
    const/4 v0, 0x0

    .local v0, "i":I
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v1

    .local v1, "s":I
    :goto_0
    rsub-int/lit8 v4, v1, 0x4

    if-ge v0, v4, :cond_0

    .line 132
    const/16 v4, 0x30

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 131
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 135
    :cond_0
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 136
    const/16 v4, 0x5d

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 137
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    return-object v4
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 162
    const-string v1, "{ serial: %d, request: %d, result: "

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget v4, p0, Lcom/sec/phone/RILRequest;->mSerial:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget v4, p0, Lcom/sec/phone/RILRequest;->mRequest:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 163
    .local v0, "ret":Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/phone/RILRequest;->mResult:Landroid/os/Message;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 164
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", replyTo: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/phone/RILRequest;->mResult:Landroid/os/Message;

    iget-object v2, v2, Landroid/os/Message;->replyTo:Landroid/os/Messenger;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " }"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 165
    return-object v0
.end method
