.class public Lcom/sec/phone/BootCompleteReceiver;
.super Landroid/content/BroadcastReceiver;
.source "BootCompleteReceiver.java"


# static fields
.field public static final ACTION_CUSTOMER_TEST_RESPONSE:Ljava/lang/String; = "android.intent.action.CUSTOMER_TEST_RESPONSE"

.field public static final ACTION_SENIOR_BOOT_COMPLETED:Ljava/lang/String; = "android.intent.action.BOOT_COMPLETED"

.field public static final ACTION_SENIOR_FORCE_SLEEP:Ljava/lang/String; = "android.intent.action.FORCE_SLEEP"

.field private static final TAG:Ljava/lang/String; = "SecPhoneBootCompleteReceiver"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method public static hasQcril()Z
    .locals 3

    .prologue
    .line 43
    const-string v1, "rild.libpath"

    const-string v2, "none"

    invoke-static {v1, v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    .line 46
    .local v0, "strRilPath":Ljava/lang/String;
    const-string v1, "/system/lib/libril-qc-qmi-1.so"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 47
    const/4 v1, 0x1

    .line 49
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static hasSecrilDaemon(I)Z
    .locals 5
    .param p0, "index"    # I

    .prologue
    const/4 v2, 0x1

    .line 26
    const-string v0, ""

    .line 27
    .local v0, "daemonName":Ljava/lang/String;
    if-ne p0, v2, :cond_0

    .line 28
    const-string v0, "init.svc.ril-daemon"

    .line 32
    :goto_0
    const-string v3, "stopped"

    invoke-static {v0, v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    .line 35
    .local v1, "strRilDaemonStatus":Ljava/lang/String;
    const-string v3, "running"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 38
    :goto_1
    return v2

    .line 30
    .end local v1    # "strRilDaemonStatus":Ljava/lang/String;
    :cond_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "init.svc.ril-daemon"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 38
    .restart local v1    # "strRilDaemonStatus":Ljava/lang/String;
    :cond_1
    const/4 v2, 0x0

    goto :goto_1
.end method

.method private isApoMode()Z
    .locals 2

    .prologue
    .line 21
    new-instance v0, Ljava/io/File;

    const-string v1, "system/bin/at_distributor"

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 22
    .local v0, "f":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    return v1
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 55
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 56
    .local v0, "action":Ljava/lang/String;
    if-nez v0, :cond_1

    .line 57
    const-string v4, "SecPhoneBootCompleteReceiver"

    const-string v5, "onReceive : intent is null"

    invoke-static {v4, v5}, Landroid/telephony/Rlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 102
    :cond_0
    :goto_0
    return-void

    .line 61
    :cond_1
    const-string v4, "android.intent.action.BOOT_COMPLETED"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 62
    const-string v4, "SecPhoneBootCompleteReceiver"

    const-string v5, "onReceive(): android.intent.action.BOOT_COMPLETED"

    invoke-static {v4, v5}, Landroid/telephony/Rlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 64
    const/4 v4, 0x2

    invoke-static {v4}, Lcom/sec/phone/BootCompleteReceiver;->hasSecrilDaemon(I)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 65
    const-string v4, "SecPhoneBootCompleteReceiver"

    const-string v5, "init.svc.ril-daemon2 running start SecPhoneService2"

    invoke-static {v4, v5}, Landroid/telephony/Rlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 66
    new-instance v4, Landroid/content/Intent;

    const-class v5, Lcom/sec/phone/SecPhoneService2;

    invoke-direct {v4, p1, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p1, v4}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 70
    :cond_2
    invoke-direct {p0}, Lcom/sec/phone/BootCompleteReceiver;->isApoMode()Z

    move-result v4

    if-eqz v4, :cond_4

    .line 71
    const/4 v4, 0x1

    invoke-static {v4}, Lcom/sec/phone/BootCompleteReceiver;->hasSecrilDaemon(I)Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-static {}, Lcom/sec/phone/BootCompleteReceiver;->hasQcril()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 72
    const-string v4, "SecPhoneBootCompleteReceiver"

    const-string v5, "Starting SecPhoneService service"

    invoke-static {v4, v5}, Landroid/telephony/Rlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 73
    new-instance v4, Landroid/content/Intent;

    const-class v5, Lcom/sec/phone/SecPhoneService;

    invoke-direct {v4, p1, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p1, v4}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    goto :goto_0

    .line 75
    :cond_3
    const-string v4, "SecPhoneBootCompleteReceiver"

    const-string v5, "Starting RilInitTracker service"

    invoke-static {v4, v5}, Landroid/telephony/Rlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 76
    new-instance v4, Landroid/content/Intent;

    const-class v5, Lcom/sec/phone/RilTracker;

    invoke-direct {v4, p1, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p1, v4}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    goto :goto_0

    .line 79
    :cond_4
    const-string v4, "SecPhoneBootCompleteReceiver"

    const-string v5, "Starting SecPhoneServiceCPOnUPG service"

    invoke-static {v4, v5}, Landroid/telephony/Rlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 80
    new-instance v4, Landroid/content/Intent;

    const-class v5, Lcom/sec/phone/SecPhoneServiceCPOnUPG;

    invoke-direct {v4, p1, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p1, v4}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    goto :goto_0

    .line 82
    :cond_5
    const-string v4, "android.intent.action.FORCE_SLEEP"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 84
    invoke-direct {p0}, Lcom/sec/phone/BootCompleteReceiver;->isApoMode()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 85
    new-instance v4, Landroid/content/Intent;

    const-class v5, Lcom/sec/phone/SecPhoneService;

    invoke-direct {v4, p1, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v5, "MODE"

    const-string v6, "FORCE_SLEEP"

    invoke-virtual {v4, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v4

    invoke-virtual {p1, v4}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    goto/16 :goto_0

    .line 87
    :cond_6
    const-string v4, "android.intent.action.CUSTOMER_TEST_RESPONSE"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 88
    const-string v4, "command"

    invoke-virtual {p2, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 89
    .local v2, "command":Ljava/lang/String;
    const-string v4, "result"

    invoke-virtual {p2, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 90
    .local v3, "result":Ljava/lang/String;
    const-string v4, "mode"

    invoke-virtual {p2, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 92
    .local v1, "bypassMode":Ljava/lang/String;
    const-string v4, "SecPhoneBootCompleteReceiver"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "CUSTOMER_TEST_REQUEST 2: command : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", result : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", bypassMode : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/telephony/Rlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 93
    const-string v4, "bypass_ap_cp"

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_7

    .line 94
    const-string v4, "SecPhoneBootCompleteReceiver"

    const-string v5, "Not sending request to cp"

    invoke-static {v4, v5}, Landroid/telephony/Rlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 96
    :cond_7
    new-instance v4, Landroid/content/Intent;

    const-class v5, Lcom/sec/phone/SecPhoneService;

    invoke-direct {v4, p1, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v5, "MODE"

    const-string v6, "CUSTOMER_TEST_RESULT"

    invoke-virtual {v4, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v4

    const-string v5, "command"

    const-string v6, "command"

    invoke-virtual {p2, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v4

    const-string v5, "result"

    const-string v6, "result"

    invoke-virtual {p2, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v4

    invoke-virtual {p1, v4}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    goto/16 :goto_0
.end method
