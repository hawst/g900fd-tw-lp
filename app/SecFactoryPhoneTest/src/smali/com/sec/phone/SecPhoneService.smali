.class public Lcom/sec/phone/SecPhoneService;
.super Landroid/app/Service;
.source "SecPhoneService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/phone/SecPhoneService$RILReceiver;,
        Lcom/sec/phone/SecPhoneService$RILSender;
    }
.end annotation


# static fields
.field static final ANTENNA_TYPE_DUAL:I = 0x3

.field static final ANTENNA_TYPE_PRIMARY:I = 0x1

.field static final ANTENNA_TYPE_SECONDARY:I = 0x2

.field static final EVENT_SEND:I = 0x1

.field static final EVENT_WAKE_LOCK_TIMEOUT:I = 0x2

.field static final OEM_FUNCTION_ID_MISC:I = 0x11

.field static final OEM_FUNCTION_ID_SDM_IMS_SETTINGS:I = 0x22

.field static final OEM_MISC_GET_ANTENNA_TEST:I = 0x6

.field static final OEM_MISC_SET_ANTENNA_TEST:I = 0x7

.field static final OEM_SDM_IMS_GET_SETTINGS:I = 0x1

.field static final OEM_SDM_IMS_SET_SETTINGS:I = 0x2

.field static final OEM_SYSDUMP_FUNCTAG:I = 0x7

.field static final OEM_TCPDUMP_START:I = 0x15

.field static final OEM_TCPDUMP_STOP:I = 0x16

.field static final RESPONSE_SOLICITED:I = 0x0

.field static final RESPONSE_UNSOLICITED:I = 0x1

.field static final RIL_MAX_COMMAND_BYTES:I = 0x2000

.field static final SOCKET_OPEN_RETRY_MILLIS:I = 0xfa0

.field static final WAKE_LOCK_TIMEOUT:I = 0x1d4c0


# instance fields
.field public LOG_TAG:Ljava/lang/String;

.field public RilSocketName:Ljava/lang/String;

.field private final mHandler:Landroid/os/Handler;

.field private final mMessenger:Landroid/os/Messenger;

.field mQueuingMsg:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/os/Message;",
            ">;"
        }
    .end annotation
.end field

.field mReceiver:Lcom/sec/phone/SecPhoneService$RILReceiver;

.field mReceiverThread:Ljava/lang/Thread;

.field mRegisteredSender:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Landroid/os/Message;",
            ">;"
        }
    .end annotation
.end field

.field mRequestMessagesPending:I

.field mRequestsList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/phone/RILRequest;",
            ">;"
        }
    .end annotation
.end field

.field mSender:Lcom/sec/phone/SecPhoneService$RILSender;

.field mSenderThread:Landroid/os/HandlerThread;

.field mSocket:Landroid/net/LocalSocket;

.field mSupportUnsolicitedMsg:Z

.field mWakeLock:Landroid/os/PowerManager$WakeLock;

.field public sInstance:Lcom/sec/phone/SecPhoneService;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 120
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 74
    const-string v0, "RILS"

    iput-object v0, p0, Lcom/sec/phone/SecPhoneService;->LOG_TAG:Ljava/lang/String;

    .line 75
    iput-object v1, p0, Lcom/sec/phone/SecPhoneService;->sInstance:Lcom/sec/phone/SecPhoneService;

    .line 106
    const-string v0, "Multiclient"

    iput-object v0, p0, Lcom/sec/phone/SecPhoneService;->RilSocketName:Ljava/lang/String;

    .line 108
    new-instance v0, Landroid/net/LocalSocket;

    invoke-direct {v0}, Landroid/net/LocalSocket;-><init>()V

    iput-object v0, p0, Lcom/sec/phone/SecPhoneService;->mSocket:Landroid/net/LocalSocket;

    .line 116
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/phone/SecPhoneService;->mSupportUnsolicitedMsg:Z

    .line 117
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/phone/SecPhoneService;->mRequestsList:Ljava/util/ArrayList;

    .line 118
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/phone/SecPhoneService;->mQueuingMsg:Ljava/util/ArrayList;

    .line 125
    iput-object v1, p0, Lcom/sec/phone/SecPhoneService;->mRegisteredSender:Ljava/util/Map;

    .line 127
    new-instance v0, Lcom/sec/phone/SecPhoneService$1;

    invoke-direct {v0, p0}, Lcom/sec/phone/SecPhoneService$1;-><init>(Lcom/sec/phone/SecPhoneService;)V

    iput-object v0, p0, Lcom/sec/phone/SecPhoneService;->mHandler:Landroid/os/Handler;

    .line 183
    new-instance v0, Landroid/os/Messenger;

    iget-object v1, p0, Lcom/sec/phone/SecPhoneService;->mHandler:Landroid/os/Handler;

    invoke-direct {v0, v1}, Landroid/os/Messenger;-><init>(Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/sec/phone/SecPhoneService;->mMessenger:Landroid/os/Messenger;

    .line 121
    iput-object p0, p0, Lcom/sec/phone/SecPhoneService;->sInstance:Lcom/sec/phone/SecPhoneService;

    .line 122
    return-void
.end method

.method private SendQueuingMsg()V
    .locals 7

    .prologue
    .line 164
    iget-object v4, p0, Lcom/sec/phone/SecPhoneService;->mQueuingMsg:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-lez v4, :cond_1

    .line 165
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v4, p0, Lcom/sec/phone/SecPhoneService;->mQueuingMsg:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-ge v0, v4, :cond_1

    .line 166
    iget-object v4, p0, Lcom/sec/phone/SecPhoneService;->LOG_TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "SendQueuingMsg : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/telephony/Rlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 167
    iget-object v4, p0, Lcom/sec/phone/SecPhoneService;->mQueuingMsg:Ljava/util/ArrayList;

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/Message;

    .line 168
    .local v1, "queuingMsg":Landroid/os/Message;
    iget-object v4, v1, Landroid/os/Message;->replyTo:Landroid/os/Messenger;

    if-eqz v4, :cond_0

    .line 169
    invoke-virtual {p0, v1}, Lcom/sec/phone/SecPhoneService;->invokeOemRilRequestRaw(Landroid/os/Message;)V

    .line 165
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 171
    :cond_0
    const/16 v4, 0x3b

    const/4 v5, 0x0

    invoke-static {v4, v5}, Lcom/sec/phone/RILRequest;->obtain(ILandroid/os/Message;)Lcom/sec/phone/RILRequest;

    move-result-object v3

    .line 172
    .local v3, "rr":Lcom/sec/phone/RILRequest;
    invoke-virtual {v1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v2

    .line 173
    .local v2, "req":Landroid/os/Bundle;
    iget-object v4, v3, Lcom/sec/phone/RILRequest;->mp:Landroid/os/Parcel;

    const-string v5, "request"

    invoke-virtual {v2, v5}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/os/Parcel;->writeByteArray([B)V

    .line 174
    invoke-direct {p0, v3}, Lcom/sec/phone/SecPhoneService;->send(Lcom/sec/phone/RILRequest;)V

    .line 175
    iget-object v4, p0, Lcom/sec/phone/SecPhoneService;->LOG_TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3}, Lcom/sec/phone/RILRequest;->serialString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "> "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, v3, Lcom/sec/phone/RILRequest;->mRequest:I

    invoke-static {v6}, Lcom/sec/phone/SecPhoneService;->requestToString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "request"

    invoke-virtual {v2, v6}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v6

    invoke-static {v6}, Lcom/sec/phone/SecPhoneService;->bytesToHexString([B)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "]"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 180
    .end local v0    # "i":I
    .end local v1    # "queuingMsg":Landroid/os/Message;
    .end local v2    # "req":Landroid/os/Bundle;
    .end local v3    # "rr":Lcom/sec/phone/RILRequest;
    :cond_1
    iget-object v4, p0, Lcom/sec/phone/SecPhoneService;->mQueuingMsg:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->clear()V

    .line 181
    return-void
.end method

.method static synthetic access$000(Lcom/sec/phone/SecPhoneService;I)Lcom/sec/phone/RILRequest;
    .locals 1
    .param p0, "x0"    # Lcom/sec/phone/SecPhoneService;
    .param p1, "x1"    # I

    .prologue
    .line 73
    invoke-direct {p0, p1}, Lcom/sec/phone/SecPhoneService;->findAndRemoveRequestFromList(I)Lcom/sec/phone/RILRequest;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/phone/SecPhoneService;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/phone/SecPhoneService;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 73
    invoke-direct {p0, p1}, Lcom/sec/phone/SecPhoneService;->sendIntentSecPhoneReady(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$200(Lcom/sec/phone/SecPhoneService;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/phone/SecPhoneService;

    .prologue
    .line 73
    invoke-direct {p0}, Lcom/sec/phone/SecPhoneService;->sendRequestSecPhoneReady()V

    return-void
.end method

.method static synthetic access$300(Lcom/sec/phone/SecPhoneService;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/phone/SecPhoneService;

    .prologue
    .line 73
    invoke-direct {p0}, Lcom/sec/phone/SecPhoneService;->SendQueuingMsg()V

    return-void
.end method

.method static synthetic access$400(Ljava/lang/String;Ljava/io/InputStream;[B)I
    .locals 1
    .param p0, "x0"    # Ljava/lang/String;
    .param p1, "x1"    # Ljava/io/InputStream;
    .param p2, "x2"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 73
    invoke-static {p0, p1, p2}, Lcom/sec/phone/SecPhoneService;->readRilMessage(Ljava/lang/String;Ljava/io/InputStream;[B)I

    move-result v0

    return v0
.end method

.method static synthetic access$500(Lcom/sec/phone/SecPhoneService;Landroid/os/Parcel;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/phone/SecPhoneService;
    .param p1, "x1"    # Landroid/os/Parcel;

    .prologue
    .line 73
    invoke-direct {p0, p1}, Lcom/sec/phone/SecPhoneService;->processResponse(Landroid/os/Parcel;)V

    return-void
.end method

.method private acquireWakeLock()V
    .locals 6

    .prologue
    .line 857
    iget-object v2, p0, Lcom/sec/phone/SecPhoneService;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    monitor-enter v2

    .line 858
    :try_start_0
    iget-object v1, p0, Lcom/sec/phone/SecPhoneService;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 859
    iget v1, p0, Lcom/sec/phone/SecPhoneService;->mRequestMessagesPending:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/sec/phone/SecPhoneService;->mRequestMessagesPending:I

    .line 861
    iget-object v1, p0, Lcom/sec/phone/SecPhoneService;->mSender:Lcom/sec/phone/SecPhoneService$RILSender;

    const/4 v3, 0x2

    invoke-virtual {v1, v3}, Lcom/sec/phone/SecPhoneService$RILSender;->removeMessages(I)V

    .line 862
    iget-object v1, p0, Lcom/sec/phone/SecPhoneService;->mSender:Lcom/sec/phone/SecPhoneService$RILSender;

    const/4 v3, 0x2

    invoke-virtual {v1, v3}, Lcom/sec/phone/SecPhoneService$RILSender;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 863
    .local v0, "msg":Landroid/os/Message;
    iget-object v1, p0, Lcom/sec/phone/SecPhoneService;->mSender:Lcom/sec/phone/SecPhoneService$RILSender;

    const-wide/32 v4, 0x1d4c0

    invoke-virtual {v1, v0, v4, v5}, Lcom/sec/phone/SecPhoneService$RILSender;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 864
    monitor-exit v2

    .line 865
    return-void

    .line 864
    .end local v0    # "msg":Landroid/os/Message;
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public static bytesToHexString([B)Ljava/lang/String;
    .locals 4
    .param p0, "bytes"    # [B

    .prologue
    .line 1134
    if-nez p0, :cond_0

    .line 1135
    const/4 v3, 0x0

    .line 1151
    :goto_0
    return-object v3

    .line 1137
    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    array-length v3, p0

    mul-int/lit8 v3, v3, 0x2

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 1139
    .local v2, "ret":Ljava/lang/StringBuilder;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    array-length v3, p0

    if-ge v1, v3, :cond_1

    .line 1142
    aget-byte v3, p0, v1

    shr-int/lit8 v3, v3, 0x4

    and-int/lit8 v0, v3, 0xf

    .line 1144
    .local v0, "b":I
    const-string v3, "0123456789abcdef"

    invoke-virtual {v3, v0}, Ljava/lang/String;->charAt(I)C

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 1146
    aget-byte v3, p0, v1

    and-int/lit8 v0, v3, 0xf

    .line 1148
    const-string v3, "0123456789abcdef"

    invoke-virtual {v3, v0}, Ljava/lang/String;->charAt(I)C

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 1139
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1151
    .end local v0    # "b":I
    :cond_1
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    goto :goto_0
.end method

.method private checkSupportUnsolicitedMsg()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 205
    invoke-static {v2}, Lcom/sec/phone/BootCompleteReceiver;->hasSecrilDaemon(I)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/sec/phone/BootCompleteReceiver;->hasQcril()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 207
    iget-object v0, p0, Lcom/sec/phone/SecPhoneService;->LOG_TAG:Ljava/lang/String;

    const-string v1, "support Unsolicited message"

    invoke-static {v0, v1}, Landroid/telephony/Rlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 208
    iput-boolean v2, p0, Lcom/sec/phone/SecPhoneService;->mSupportUnsolicitedMsg:Z

    .line 210
    :cond_0
    return-void
.end method

.method private findAndRemoveRequestFromList(I)Lcom/sec/phone/RILRequest;
    .locals 5
    .param p1, "serial"    # I

    .prologue
    .line 913
    iget-object v4, p0, Lcom/sec/phone/SecPhoneService;->mRequestsList:Ljava/util/ArrayList;

    monitor-enter v4

    .line 914
    const/4 v0, 0x0

    .local v0, "i":I
    :try_start_0
    iget-object v3, p0, Lcom/sec/phone/SecPhoneService;->mRequestsList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v2

    .local v2, "s":I
    :goto_0
    if-ge v0, v2, :cond_1

    .line 915
    iget-object v3, p0, Lcom/sec/phone/SecPhoneService;->mRequestsList:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/phone/RILRequest;

    .line 917
    .local v1, "rr":Lcom/sec/phone/RILRequest;
    iget v3, v1, Lcom/sec/phone/RILRequest;->mSerial:I

    if-ne v3, p1, :cond_0

    .line 918
    iget-object v3, p0, Lcom/sec/phone/SecPhoneService;->mRequestsList:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 919
    monitor-exit v4

    .line 924
    .end local v1    # "rr":Lcom/sec/phone/RILRequest;
    :goto_1
    return-object v1

    .line 914
    .restart local v1    # "rr":Lcom/sec/phone/RILRequest;
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 922
    .end local v1    # "rr":Lcom/sec/phone/RILRequest;
    :cond_1
    monitor-exit v4

    .line 924
    const/4 v1, 0x0

    goto :goto_1

    .line 922
    .end local v2    # "s":I
    :catchall_0
    move-exception v3

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v3
.end method

.method private getTelephonyService()Lcom/android/internal/telephony/ITelephony;
    .locals 3

    .prologue
    .line 700
    const-string v1, "phone"

    invoke-static {v1}, Landroid/os/ServiceManager;->checkService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v1

    invoke-static {v1}, Lcom/android/internal/telephony/ITelephony$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/telephony/ITelephony;

    move-result-object v0

    .line 702
    .local v0, "telephonyService":Lcom/android/internal/telephony/ITelephony;
    if-nez v0, :cond_0

    .line 703
    iget-object v1, p0, Lcom/sec/phone/SecPhoneService;->LOG_TAG:Ljava/lang/String;

    const-string v2, "Unable to find ITelephony interface."

    invoke-static {v1, v2}, Landroid/telephony/Rlog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 705
    :cond_0
    return-object v0
.end method

.method private isSupportUnsolicitedMsg()Z
    .locals 1

    .prologue
    .line 213
    iget-boolean v0, p0, Lcom/sec/phone/SecPhoneService;->mSupportUnsolicitedMsg:Z

    return v0
.end method

.method private processResponse(Landroid/os/Parcel;)V
    .locals 3
    .param p1, "p"    # Landroid/os/Parcel;

    .prologue
    const/4 v2, 0x1

    .line 901
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 903
    .local v0, "type":I
    if-ne v0, v2, :cond_1

    invoke-direct {p0}, Lcom/sec/phone/SecPhoneService;->isSupportUnsolicitedMsg()Z

    move-result v1

    if-ne v1, v2, :cond_1

    .line 904
    invoke-direct {p0, p1}, Lcom/sec/phone/SecPhoneService;->processUnsolicited(Landroid/os/Parcel;)V

    .line 909
    :cond_0
    :goto_0
    invoke-direct {p0}, Lcom/sec/phone/SecPhoneService;->releaseWakeLockIfDone()V

    .line 910
    return-void

    .line 905
    :cond_1
    if-nez v0, :cond_0

    .line 906
    invoke-direct {p0, p1}, Lcom/sec/phone/SecPhoneService;->processSolicited(Landroid/os/Parcel;)V

    goto :goto_0
.end method

.method private processSolicited(Landroid/os/Parcel;)V
    .locals 9
    .param p1, "p"    # Landroid/os/Parcel;

    .prologue
    const/4 v8, 0x0

    .line 930
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v3

    .line 931
    .local v3, "serial":I
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 935
    .local v0, "error":I
    invoke-direct {p0, v3}, Lcom/sec/phone/SecPhoneService;->findAndRemoveRequestFromList(I)Lcom/sec/phone/RILRequest;

    move-result-object v2

    .line 937
    .local v2, "rr":Lcom/sec/phone/RILRequest;
    if-nez v2, :cond_0

    .line 938
    iget-object v5, p0, Lcom/sec/phone/SecPhoneService;->LOG_TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Unexpected solicited response! sn: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " error: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/telephony/Rlog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 990
    :goto_0
    return-void

    .line 942
    :cond_0
    iget-object v5, p0, Lcom/sec/phone/SecPhoneService;->LOG_TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "processSolicited(): mResult - "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, v2, Lcom/sec/phone/RILRequest;->mResult:Landroid/os/Message;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/telephony/Rlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 943
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v1

    .line 944
    .local v1, "reply":Landroid/os/Message;
    iget-object v5, v2, Lcom/sec/phone/RILRequest;->mResult:Landroid/os/Message;

    if-eqz v5, :cond_1

    .line 945
    iget-object v5, v2, Lcom/sec/phone/RILRequest;->mResult:Landroid/os/Message;

    invoke-virtual {v1, v5}, Landroid/os/Message;->copyFrom(Landroid/os/Message;)V

    .line 946
    :cond_1
    iget-object v5, p0, Lcom/sec/phone/SecPhoneService;->LOG_TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "processSolicited(): reply - "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/telephony/Rlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 948
    if-eqz v0, :cond_2

    invoke-virtual {p1}, Landroid/os/Parcel;->dataAvail()I

    move-result v5

    if-lez v5, :cond_3

    .line 951
    :cond_2
    :try_start_0
    iget v5, v2, Lcom/sec/phone/RILRequest;->mRequest:I

    packed-switch v5, :pswitch_data_0

    .line 956
    new-instance v5, Ljava/lang/RuntimeException;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Unrecognized solicited response: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v7, v2, Lcom/sec/phone/RILRequest;->mRequest:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v5
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 960
    :catch_0
    move-exception v4

    .line 963
    .local v4, "tr":Ljava/lang/Throwable;
    iget-object v5, p0, Lcom/sec/phone/SecPhoneService;->LOG_TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2}, Lcom/sec/phone/RILRequest;->serialString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "< "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v7, v2, Lcom/sec/phone/RILRequest;->mRequest:I

    invoke-static {v7}, Lcom/sec/phone/SecPhoneService;->requestToString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " exception, possible invalid RIL response"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6, v4}, Landroid/telephony/Rlog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 966
    const/4 v5, -0x1

    invoke-virtual {v2, v5, v8}, Lcom/sec/phone/RILRequest;->onError(ILjava/lang/Object;)V

    .line 967
    invoke-virtual {v2}, Lcom/sec/phone/RILRequest;->release()V

    goto/16 :goto_0

    .line 953
    .end local v4    # "tr":Ljava/lang/Throwable;
    :pswitch_0
    :try_start_1
    invoke-direct {p0, p1, v1}, Lcom/sec/phone/SecPhoneService;->responseRaw(Landroid/os/Parcel;Landroid/os/Message;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    .line 972
    :cond_3
    if-eqz v0, :cond_4

    .line 973
    invoke-virtual {v2, v0, v8}, Lcom/sec/phone/RILRequest;->onError(ILjava/lang/Object;)V

    .line 974
    invoke-virtual {v2}, Lcom/sec/phone/RILRequest;->release()V

    goto/16 :goto_0

    .line 978
    :cond_4
    iget-object v5, p0, Lcom/sec/phone/SecPhoneService;->LOG_TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2}, Lcom/sec/phone/RILRequest;->serialString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "< "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v7, v2, Lcom/sec/phone/RILRequest;->mRequest:I

    invoke-static {v7}, Lcom/sec/phone/SecPhoneService;->requestToString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v7, v2, Lcom/sec/phone/RILRequest;->mRequest:I

    invoke-direct {p0, v7, v1}, Lcom/sec/phone/SecPhoneService;->retToString(ILjava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/telephony/Rlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 982
    iget-object v5, v2, Lcom/sec/phone/RILRequest;->mResult:Landroid/os/Message;

    if-eqz v5, :cond_5

    .line 984
    :try_start_2
    iget-object v5, v2, Lcom/sec/phone/RILRequest;->mResult:Landroid/os/Message;

    iget-object v5, v5, Landroid/os/Message;->replyTo:Landroid/os/Messenger;

    invoke-virtual {v5, v1}, Landroid/os/Messenger;->send(Landroid/os/Message;)V
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_1

    .line 989
    :cond_5
    :goto_1
    invoke-virtual {v2}, Lcom/sec/phone/RILRequest;->release()V

    goto/16 :goto_0

    .line 985
    :catch_1
    move-exception v5

    goto :goto_1

    .line 951
    :pswitch_data_0
    .packed-switch 0x3b
        :pswitch_0
    .end packed-switch
.end method

.method private processUnsolicited(Landroid/os/Parcel;)V
    .locals 13
    .param p1, "p"    # Landroid/os/Parcel;

    .prologue
    .line 1040
    const/4 v7, 0x0

    .line 1042
    .local v7, "ret":Ljava/lang/Object;
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v6

    .line 1045
    .local v6, "response":I
    sparse-switch v6, :sswitch_data_0

    .line 1056
    :try_start_0
    new-instance v10, Ljava/lang/RuntimeException;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Unrecognized unsol response: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v10

    .line 1059
    :catch_0
    move-exception v9

    .line 1102
    .end local v7    # "ret":Ljava/lang/Object;
    :goto_0
    :sswitch_0
    return-void

    .line 1052
    .restart local v7    # "ret":Ljava/lang/Object;
    :sswitch_1
    invoke-direct {p0, p1}, Lcom/sec/phone/SecPhoneService;->responseString(Landroid/os/Parcel;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v7

    .line 1064
    .end local v7    # "ret":Ljava/lang/Object;
    :sswitch_2
    sparse-switch v6, :sswitch_data_1

    goto :goto_0

    :sswitch_3
    move-object v8, v7

    .line 1071
    check-cast v8, Ljava/lang/String;

    .line 1072
    .local v8, "str":Ljava/lang/String;
    const-string v10, " "

    invoke-virtual {v8, v10}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 1075
    .local v0, "Args":[Ljava/lang/String;
    invoke-virtual {p0, v0}, Lcom/sec/phone/SecPhoneService;->GetActionFromRilResponse([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1076
    .local v1, "action":Ljava/lang/String;
    const/4 v3, 0x0

    .line 1078
    .local v3, "registeredMessenger":Landroid/os/Message;
    iget-object v10, p0, Lcom/sec/phone/SecPhoneService;->mRegisteredSender:Ljava/util/Map;

    if-eqz v10, :cond_0

    .line 1079
    iget-object v10, p0, Lcom/sec/phone/SecPhoneService;->mRegisteredSender:Ljava/util/Map;

    invoke-interface {v10, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    .end local v3    # "registeredMessenger":Landroid/os/Message;
    check-cast v3, Landroid/os/Message;

    .line 1082
    .restart local v3    # "registeredMessenger":Landroid/os/Message;
    :cond_0
    if-nez v3, :cond_1

    .line 1083
    iget-object v10, p0, Lcom/sec/phone/SecPhoneService;->LOG_TAG:Ljava/lang/String;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Executing Am "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/telephony/Rlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1084
    invoke-static {v0}, Lcom/sec/phone/Am;->main([Ljava/lang/String;)V

    goto :goto_0

    .line 1086
    :cond_1
    iget-object v10, p0, Lcom/sec/phone/SecPhoneService;->LOG_TAG:Ljava/lang/String;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "SendToMessenger : "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/telephony/Rlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1087
    move-object v4, v3

    .line 1088
    .local v4, "reply":Landroid/os/Message;
    invoke-virtual {v4}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v5

    .line 1089
    .local v5, "req":Landroid/os/Bundle;
    const-string v10, "Command"

    invoke-virtual {v5, v10, v8}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1090
    invoke-virtual {v4, v5}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 1092
    :try_start_1
    iget-object v10, v3, Landroid/os/Message;->replyTo:Landroid/os/Messenger;

    invoke-virtual {v10, v4}, Landroid/os/Messenger;->send(Landroid/os/Message;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 1093
    :catch_1
    move-exception v2

    .line 1094
    .local v2, "e":Landroid/os/RemoteException;
    iget-object v10, p0, Lcom/sec/phone/SecPhoneService;->LOG_TAG:Ljava/lang/String;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Executing Am "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/telephony/Rlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1095
    invoke-static {v0}, Lcom/sec/phone/Am;->main([Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1045
    :sswitch_data_0
    .sparse-switch
        0x2b02 -> :sswitch_2
        0x2b12 -> :sswitch_1
    .end sparse-switch

    .line 1064
    :sswitch_data_1
    .sparse-switch
        0x2b02 -> :sswitch_0
        0x2b12 -> :sswitch_3
    .end sparse-switch
.end method

.method private static readRilMessage(Ljava/lang/String;Ljava/io/InputStream;[B)I
    .locals 7
    .param p0, "LOG_TAG"    # Ljava/lang/String;
    .param p1, "is"    # Ljava/io/InputStream;
    .param p2, "buffer"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, -0x1

    .line 436
    const/4 v2, 0x0

    .line 437
    .local v2, "offset":I
    const/4 v3, 0x4

    .line 439
    .local v3, "remaining":I
    :cond_0
    invoke-virtual {p1, p2, v2, v3}, Ljava/io/InputStream;->read([BII)I

    move-result v0

    .line 441
    .local v0, "countRead":I
    if-gez v0, :cond_1

    .line 442
    const-string v5, "Hit EOS reading message length"

    invoke-static {p0, v5}, Landroid/telephony/Rlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v1, v4

    .line 469
    :goto_0
    return v1

    .line 446
    :cond_1
    add-int/2addr v2, v0

    .line 447
    sub-int/2addr v3, v0

    .line 448
    if-gtz v3, :cond_0

    .line 450
    const/4 v5, 0x0

    aget-byte v5, p2, v5

    and-int/lit16 v5, v5, 0xff

    shl-int/lit8 v5, v5, 0x18

    const/4 v6, 0x1

    aget-byte v6, p2, v6

    and-int/lit16 v6, v6, 0xff

    shl-int/lit8 v6, v6, 0x10

    or-int/2addr v5, v6

    const/4 v6, 0x2

    aget-byte v6, p2, v6

    and-int/lit16 v6, v6, 0xff

    shl-int/lit8 v6, v6, 0x8

    or-int/2addr v5, v6

    const/4 v6, 0x3

    aget-byte v6, p2, v6

    and-int/lit16 v6, v6, 0xff

    or-int v1, v5, v6

    .line 454
    .local v1, "messageLength":I
    const/4 v2, 0x0

    .line 455
    move v3, v1

    .line 457
    :cond_2
    invoke-virtual {p1, p2, v2, v3}, Ljava/io/InputStream;->read([BII)I

    move-result v0

    .line 459
    if-gez v0, :cond_3

    .line 460
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Hit EOS reading message.  messageLength="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " remaining="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {p0, v5}, Landroid/telephony/Rlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v1, v4

    .line 462
    goto :goto_0

    .line 465
    :cond_3
    add-int/2addr v2, v0

    .line 466
    sub-int/2addr v3, v0

    .line 467
    if-gtz v3, :cond_2

    goto :goto_0
.end method

.method private releaseWakeLock()V
    .locals 2

    .prologue
    .line 882
    iget-object v1, p0, Lcom/sec/phone/SecPhoneService;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    monitor-enter v1

    .line 883
    const/4 v0, 0x0

    :try_start_0
    iput v0, p0, Lcom/sec/phone/SecPhoneService;->mRequestMessagesPending:I

    .line 884
    iget-object v0, p0, Lcom/sec/phone/SecPhoneService;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 885
    monitor-exit v1

    .line 886
    return-void

    .line 885
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private releaseWakeLockIfDone()V
    .locals 4

    .prologue
    .line 868
    iget-object v1, p0, Lcom/sec/phone/SecPhoneService;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    monitor-enter v1

    .line 869
    :try_start_0
    iget-object v0, p0, Lcom/sec/phone/SecPhoneService;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 870
    iget-object v2, p0, Lcom/sec/phone/SecPhoneService;->mRequestsList:Ljava/util/ArrayList;

    monitor-enter v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 871
    :try_start_1
    iget v0, p0, Lcom/sec/phone/SecPhoneService;->mRequestMessagesPending:I

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/phone/SecPhoneService;->mRequestsList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_0

    .line 873
    iget-object v0, p0, Lcom/sec/phone/SecPhoneService;->mSender:Lcom/sec/phone/SecPhoneService$RILSender;

    const/4 v3, 0x2

    invoke-virtual {v0, v3}, Lcom/sec/phone/SecPhoneService$RILSender;->removeMessages(I)V

    .line 874
    iget-object v0, p0, Lcom/sec/phone/SecPhoneService;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 876
    :cond_0
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 878
    :cond_1
    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 879
    return-void

    .line 876
    :catchall_0
    move-exception v0

    :try_start_3
    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v0

    .line 878
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    throw v0
.end method

.method static requestToString(I)Ljava/lang/String;
    .locals 1
    .param p0, "request"    # I

    .prologue
    .line 813
    sparse-switch p0, :sswitch_data_0

    .line 833
    const-string v0, "<unknown request>"

    :goto_0
    return-object v0

    .line 815
    :sswitch_0
    const-string v0, "OEM_HOOK_RAW"

    goto :goto_0

    .line 817
    :sswitch_1
    const-string v0, "OEM_HOOK_STRINGS"

    goto :goto_0

    .line 820
    :sswitch_2
    const-string v0, "GET_PHONEBOOK_STORAGE_INFO"

    goto :goto_0

    .line 822
    :sswitch_3
    const-string v0, "GET_PHONEBOOK_ENTRY"

    goto :goto_0

    .line 824
    :sswitch_4
    const-string v0, "ACCESS_PHONEBOOK_ENTRY"

    goto :goto_0

    .line 828
    :sswitch_5
    const-string v0, "USIM_PB_CAPA"

    goto :goto_0

    .line 831
    :sswitch_6
    const-string v0, "LOCK_INFO"

    goto :goto_0

    .line 813
    nop

    :sswitch_data_0
    .sparse-switch
        0x3b -> :sswitch_0
        0x3c -> :sswitch_1
        0x2717 -> :sswitch_2
        0x2718 -> :sswitch_3
        0x2719 -> :sswitch_4
        0x271d -> :sswitch_5
        0x271e -> :sswitch_6
    .end sparse-switch
.end method

.method private responseRaw(Landroid/os/Parcel;Landroid/os/Message;)V
    .locals 3
    .param p1, "p"    # Landroid/os/Parcel;
    .param p2, "reply"    # Landroid/os/Message;

    .prologue
    .line 1121
    invoke-virtual {p1}, Landroid/os/Parcel;->createByteArray()[B

    move-result-object v0

    .line 1122
    .local v0, "response":[B
    invoke-virtual {p2}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "response"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V

    .line 1123
    return-void
.end method

.method private responseString(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1
    .param p1, "p"    # Landroid/os/Parcel;

    .prologue
    .line 1128
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 1130
    .local v0, "response":Ljava/lang/String;
    return-object v0
.end method

.method static responseToString(I)Ljava/lang/String;
    .locals 1
    .param p0, "request"    # I

    .prologue
    .line 842
    sparse-switch p0, :sswitch_data_0

    .line 852
    const-string v0, "<unknown reponse>"

    :goto_0
    return-object v0

    .line 844
    :sswitch_0
    const-string v0, "UNSOL_OEM_HOOK_RAW"

    goto :goto_0

    .line 847
    :sswitch_1
    const-string v0, "UNSOL_DEVICE_READY_NOTI"

    goto :goto_0

    .line 849
    :sswitch_2
    const-string v0, "RIL_UNSOL_RESPONSE_NEW_CB_MSG"

    goto :goto_0

    .line 842
    nop

    :sswitch_data_0
    .sparse-switch
        0x404 -> :sswitch_0
        0x2af8 -> :sswitch_2
        0x2b00 -> :sswitch_1
    .end sparse-switch
.end method

.method private retToString(ILjava/lang/Object;)Ljava/lang/String;
    .locals 9
    .param p1, "req"    # I
    .param p2, "ret"    # Ljava/lang/Object;

    .prologue
    .line 993
    if-nez p2, :cond_0

    .line 994
    const-string v4, ""

    .line 1035
    .end local p2    # "ret":Ljava/lang/Object;
    :goto_0
    return-object v4

    .line 995
    .restart local p2    # "ret":Ljava/lang/Object;
    :cond_0
    sparse-switch p1, :sswitch_data_0

    .line 1006
    instance-of v7, p2, [I

    if-eqz v7, :cond_2

    .line 1007
    check-cast p2, [I

    .end local p2    # "ret":Ljava/lang/Object;
    move-object v2, p2

    check-cast v2, [I

    .line 1008
    .local v2, "intArray":[I
    array-length v3, v2

    .line 1009
    .local v3, "length":I
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v7, "{"

    invoke-direct {v5, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1010
    .local v5, "sb":Ljava/lang/StringBuilder;
    if-lez v3, :cond_1

    .line 1011
    const/4 v0, 0x0

    .line 1012
    .local v0, "i":I
    add-int/lit8 v1, v0, 0x1

    .end local v0    # "i":I
    .local v1, "i":I
    aget v7, v2, v0

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 1013
    :goto_1
    if-ge v1, v3, :cond_1

    .line 1014
    const-string v7, ", "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    add-int/lit8 v0, v1, 0x1

    .end local v1    # "i":I
    .restart local v0    # "i":I
    aget v8, v2, v1

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move v1, v0

    .end local v0    # "i":I
    .restart local v1    # "i":I
    goto :goto_1

    .line 1000
    .end local v1    # "i":I
    .end local v2    # "intArray":[I
    .end local v3    # "length":I
    .end local v5    # "sb":Ljava/lang/StringBuilder;
    .restart local p2    # "ret":Ljava/lang/Object;
    :sswitch_0
    const-string v4, ""

    goto :goto_0

    .line 1017
    .end local p2    # "ret":Ljava/lang/Object;
    .restart local v2    # "intArray":[I
    .restart local v3    # "length":I
    .restart local v5    # "sb":Ljava/lang/StringBuilder;
    :cond_1
    const-string v7, "}"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1018
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 1019
    .local v4, "s":Ljava/lang/String;
    goto :goto_0

    .end local v2    # "intArray":[I
    .end local v3    # "length":I
    .end local v4    # "s":Ljava/lang/String;
    .end local v5    # "sb":Ljava/lang/StringBuilder;
    .restart local p2    # "ret":Ljava/lang/Object;
    :cond_2
    instance-of v7, p2, [Ljava/lang/String;

    if-eqz v7, :cond_4

    .line 1020
    check-cast p2, [Ljava/lang/String;

    .end local p2    # "ret":Ljava/lang/Object;
    move-object v6, p2

    check-cast v6, [Ljava/lang/String;

    .line 1021
    .local v6, "strings":[Ljava/lang/String;
    array-length v3, v6

    .line 1022
    .restart local v3    # "length":I
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v7, "{"

    invoke-direct {v5, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1023
    .restart local v5    # "sb":Ljava/lang/StringBuilder;
    if-lez v3, :cond_3

    .line 1024
    const/4 v0, 0x0

    .line 1025
    .restart local v0    # "i":I
    add-int/lit8 v1, v0, 0x1

    .end local v0    # "i":I
    .restart local v1    # "i":I
    aget-object v7, v6, v0

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1026
    :goto_2
    if-ge v1, v3, :cond_3

    .line 1027
    const-string v7, ", "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    add-int/lit8 v0, v1, 0x1

    .end local v1    # "i":I
    .restart local v0    # "i":I
    aget-object v8, v6, v1

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move v1, v0

    .end local v0    # "i":I
    .restart local v1    # "i":I
    goto :goto_2

    .line 1030
    .end local v1    # "i":I
    :cond_3
    const-string v7, "}"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1031
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 1032
    .restart local v4    # "s":Ljava/lang/String;
    goto :goto_0

    .line 1033
    .end local v3    # "length":I
    .end local v4    # "s":Ljava/lang/String;
    .end local v5    # "sb":Ljava/lang/StringBuilder;
    .end local v6    # "strings":[Ljava/lang/String;
    .restart local p2    # "ret":Ljava/lang/Object;
    :cond_4
    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    .restart local v4    # "s":Ljava/lang/String;
    goto :goto_0

    .line 995
    :sswitch_data_0
    .sparse-switch
        0xb -> :sswitch_0
        0x26 -> :sswitch_0
        0x27 -> :sswitch_0
    .end sparse-switch
.end method

.method private routeDataToITelephony([B)Ljava/lang/reflect/Method;
    .locals 14
    .param p1, "data"    # [B

    .prologue
    const/4 v13, 0x2

    const/4 v12, 0x0

    const/4 v11, 0x1

    const/4 v5, 0x0

    .line 716
    const/4 v0, 0x6

    .line 717
    .local v0, "OEM_FUNCTION_ID_IMEI":B
    const/4 v1, 0x5

    .line 719
    .local v1, "OEM_IMEI_EVENT_START_IMEI":B
    if-eqz p1, :cond_0

    array-length v6, p1

    const/4 v7, 0x4

    if-ge v6, v7, :cond_2

    :cond_0
    move-object v3, v5

    .line 759
    :cond_1
    :goto_0
    return-object v3

    .line 724
    :cond_2
    const/4 v3, 0x0

    .line 725
    .local v3, "oemHookMethod":Ljava/lang/reflect/Method;
    const/4 v4, 0x0

    .line 727
    .local v4, "phoneServ":Lcom/android/internal/telephony/ITelephony;
    :try_start_0
    invoke-direct {p0}, Lcom/sec/phone/SecPhoneService;->getTelephonyService()Lcom/android/internal/telephony/ITelephony;

    move-result-object v4

    .line 728
    if-eqz v4, :cond_3

    .line 729
    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v6

    const-string v7, "sendOemRilRequestRaw"

    const/4 v8, 0x2

    new-array v8, v8, [Ljava/lang/Class;

    const/4 v9, 0x0

    const-class v10, [B

    aput-object v10, v8, v9

    const/4 v9, 0x1

    const-class v10, [B

    aput-object v10, v8, v9

    invoke-virtual {v6, v7, v8}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    .line 739
    aget-byte v6, p1, v12

    sparse-switch v6, :sswitch_data_0

    move-object v3, v5

    .line 759
    goto :goto_0

    :cond_3
    move-object v3, v5

    .line 732
    goto :goto_0

    .line 734
    :catch_0
    move-exception v2

    .local v2, "ex":Ljava/lang/NoSuchMethodException;
    move-object v3, v5

    .line 735
    goto :goto_0

    .line 741
    .end local v2    # "ex":Ljava/lang/NoSuchMethodException;
    :sswitch_0
    aget-byte v6, p1, v11

    const/4 v7, 0x6

    if-eq v6, v7, :cond_1

    aget-byte v6, p1, v11

    const/4 v7, 0x7

    if-eq v6, v7, :cond_1

    move-object v3, v5

    .line 744
    goto :goto_0

    .line 747
    :sswitch_1
    aget-byte v6, p1, v11

    if-eq v6, v11, :cond_1

    aget-byte v6, p1, v11

    if-eq v6, v13, :cond_1

    move-object v3, v5

    .line 750
    goto :goto_0

    :sswitch_2
    move-object v3, v5

    .line 753
    goto :goto_0

    :sswitch_3
    move-object v3, v5

    .line 756
    goto :goto_0

    .line 739
    nop

    :sswitch_data_0
    .sparse-switch
        0x6 -> :sswitch_2
        0x7 -> :sswitch_3
        0x11 -> :sswitch_0
        0x22 -> :sswitch_1
    .end sparse-switch
.end method

.method private send(Lcom/sec/phone/RILRequest;)V
    .locals 3
    .param p1, "rr"    # Lcom/sec/phone/RILRequest;

    .prologue
    .line 891
    iget-object v1, p0, Lcom/sec/phone/SecPhoneService;->mSender:Lcom/sec/phone/SecPhoneService$RILSender;

    const/4 v2, 0x1

    invoke-virtual {v1, v2, p1}, Lcom/sec/phone/SecPhoneService$RILSender;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 893
    .local v0, "msg":Landroid/os/Message;
    invoke-direct {p0}, Lcom/sec/phone/SecPhoneService;->acquireWakeLock()V

    .line 895
    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 896
    return-void
.end method

.method private sendIntentSecPhoneReady(Ljava/lang/String;)V
    .locals 7
    .param p1, "action"    # Ljava/lang/String;

    .prologue
    .line 672
    iget-object v4, p0, Lcom/sec/phone/SecPhoneService;->LOG_TAG:Ljava/lang/String;

    const-string v5, "intent for SECPHONE_READY"

    invoke-static {v4, v5}, Landroid/telephony/Rlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 673
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1, p1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 674
    .local v1, "i":Landroid/content/Intent;
    invoke-virtual {p0, v1}, Lcom/sec/phone/SecPhoneService;->sendBroadcast(Landroid/content/Intent;)V

    .line 677
    const/16 v4, 0x3b

    const/4 v5, 0x0

    invoke-static {v4, v5}, Lcom/sec/phone/RILRequest;->obtain(ILandroid/os/Message;)Lcom/sec/phone/RILRequest;

    move-result-object v3

    .line 678
    .local v3, "rr":Lcom/sec/phone/RILRequest;
    new-instance v2, Lcom/samsung/android/sec_platform_library/PacketBuilder;

    const/16 v4, 0x12

    const/16 v5, 0xa

    invoke-direct {v2, v4, v5}, Lcom/samsung/android/sec_platform_library/PacketBuilder;-><init>(BB)V

    .line 680
    .local v2, "packet":Lcom/samsung/android/sec_platform_library/PacketBuilder;
    invoke-virtual {v2}, Lcom/samsung/android/sec_platform_library/PacketBuilder;->getPacket()[B

    move-result-object v0

    .line 681
    .local v0, "data":[B
    iget-object v4, p0, Lcom/sec/phone/SecPhoneService;->LOG_TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3}, Lcom/sec/phone/RILRequest;->serialString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "> "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, v3, Lcom/sec/phone/RILRequest;->mRequest:I

    invoke-static {v6}, Lcom/sec/phone/SecPhoneService;->requestToString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {v0}, Lcom/sec/phone/SecPhoneService;->bytesToHexString([B)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "]"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/telephony/Rlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 683
    iget-object v4, v3, Lcom/sec/phone/RILRequest;->mp:Landroid/os/Parcel;

    invoke-virtual {v2}, Lcom/samsung/android/sec_platform_library/PacketBuilder;->getPacket()[B

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/os/Parcel;->writeByteArray([B)V

    .line 684
    invoke-direct {p0, v3}, Lcom/sec/phone/SecPhoneService;->send(Lcom/sec/phone/RILRequest;)V

    .line 685
    return-void
.end method

.method private sendRequestSecPhoneReady()V
    .locals 6

    .prologue
    .line 689
    const/16 v3, 0x3b

    const/4 v4, 0x0

    invoke-static {v3, v4}, Lcom/sec/phone/RILRequest;->obtain(ILandroid/os/Message;)Lcom/sec/phone/RILRequest;

    move-result-object v2

    .line 690
    .local v2, "rr":Lcom/sec/phone/RILRequest;
    new-instance v1, Lcom/samsung/android/sec_platform_library/PacketBuilder;

    const/16 v3, 0x12

    const/16 v4, 0xa

    invoke-direct {v1, v3, v4}, Lcom/samsung/android/sec_platform_library/PacketBuilder;-><init>(BB)V

    .line 692
    .local v1, "packet":Lcom/samsung/android/sec_platform_library/PacketBuilder;
    invoke-virtual {v1}, Lcom/samsung/android/sec_platform_library/PacketBuilder;->getPacket()[B

    move-result-object v0

    .line 693
    .local v0, "data":[B
    iget-object v3, p0, Lcom/sec/phone/SecPhoneService;->LOG_TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2}, Lcom/sec/phone/RILRequest;->serialString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "> "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, v2, Lcom/sec/phone/RILRequest;->mRequest:I

    invoke-static {v5}, Lcom/sec/phone/SecPhoneService;->requestToString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {v0}, Lcom/sec/phone/SecPhoneService;->bytesToHexString([B)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "]"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/telephony/Rlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 695
    iget-object v3, v2, Lcom/sec/phone/RILRequest;->mp:Landroid/os/Parcel;

    invoke-virtual {v1}, Lcom/samsung/android/sec_platform_library/PacketBuilder;->getPacket()[B

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/os/Parcel;->writeByteArray([B)V

    .line 696
    invoke-direct {p0, v2}, Lcom/sec/phone/SecPhoneService;->send(Lcom/sec/phone/RILRequest;)V

    .line 697
    return-void
.end method


# virtual methods
.method public GetActionFromRilResponse([Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1, "Args"    # [Ljava/lang/String;

    .prologue
    .line 1105
    const-string v2, ""

    .line 1107
    .local v2, "temp_action":Ljava/lang/String;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v3, p1

    if-ge v0, v3, :cond_0

    .line 1108
    aget-object v1, p1, v0

    .line 1109
    .local v1, "temp":Ljava/lang/String;
    const-string v3, "-a"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1110
    add-int/lit8 v3, v0, 0x1

    aget-object v2, p1, v3

    .line 1115
    .end local v1    # "temp":Ljava/lang/String;
    :cond_0
    return-object v2

    .line 1107
    .restart local v1    # "temp":Ljava/lang/String;
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public SendCustomerTestResult(Landroid/content/Intent;)V
    .locals 11
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 245
    const-string v8, "result"

    invoke-virtual {p1, v8}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 246
    .local v6, "result":Ljava/lang/String;
    iget-object v8, p0, Lcom/sec/phone/SecPhoneService;->LOG_TAG:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "result : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/telephony/Rlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 248
    if-nez v6, :cond_0

    .line 281
    :goto_0
    return-void

    .line 252
    :cond_0
    const/16 v8, 0x3b

    const/4 v9, 0x0

    invoke-static {v8, v9}, Lcom/sec/phone/RILRequest;->obtain(ILandroid/os/Message;)Lcom/sec/phone/RILRequest;

    move-result-object v7

    .line 253
    .local v7, "rr":Lcom/sec/phone/RILRequest;
    const/4 v1, 0x0

    .line 255
    .local v1, "data":[B
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 256
    .local v0, "bos":Ljava/io/ByteArrayOutputStream;
    new-instance v2, Ljava/io/DataOutputStream;

    invoke-direct {v2, v0}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 259
    .local v2, "dos":Ljava/io/DataOutputStream;
    const/16 v8, 0x11

    :try_start_0
    invoke-virtual {v2, v8}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 260
    const/16 v8, 0x61

    invoke-virtual {v2, v8}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 261
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v8

    add-int/lit8 v8, v8, 0x4

    invoke-virtual {v2, v8}, Ljava/io/DataOutputStream;->writeShort(I)V

    .line 262
    invoke-virtual {v2, v6}, Ljava/io/DataOutputStream;->writeBytes(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 267
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v1

    .line 269
    iget-object v8, p0, Lcom/sec/phone/SecPhoneService;->mSocket:Landroid/net/LocalSocket;

    if-eqz v8, :cond_1

    iget-object v8, p0, Lcom/sec/phone/SecPhoneService;->mSocket:Landroid/net/LocalSocket;

    invoke-virtual {v8}, Landroid/net/LocalSocket;->isConnected()Z

    move-result v8

    if-nez v8, :cond_1

    .line 270
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v4

    .line 271
    .local v4, "msg":Landroid/os/Message;
    invoke-virtual {v4}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v5

    .line 272
    .local v5, "req":Landroid/os/Bundle;
    const-string v8, "request"

    invoke-virtual {v5, v8, v1}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V

    .line 273
    invoke-virtual {v4, v5}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 274
    iget-object v8, p0, Lcom/sec/phone/SecPhoneService;->mQueuingMsg:Ljava/util/ArrayList;

    invoke-virtual {v8, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 263
    .end local v4    # "msg":Landroid/os/Message;
    .end local v5    # "req":Landroid/os/Bundle;
    :catch_0
    move-exception v3

    .line 264
    .local v3, "e":Ljava/io/IOException;
    iget-object v8, p0, Lcom/sec/phone/SecPhoneService;->LOG_TAG:Ljava/lang/String;

    const-string v9, "IOException in CP_BYPASS!!!"

    invoke-static {v8, v9}, Landroid/telephony/Rlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 276
    .end local v3    # "e":Ljava/io/IOException;
    :cond_1
    iget-object v8, v7, Lcom/sec/phone/RILRequest;->mp:Landroid/os/Parcel;

    invoke-virtual {v8, v1}, Landroid/os/Parcel;->writeByteArray([B)V

    .line 277
    invoke-direct {p0, v7}, Lcom/sec/phone/SecPhoneService;->send(Lcom/sec/phone/RILRequest;)V

    .line 278
    iget-object v8, p0, Lcom/sec/phone/SecPhoneService;->LOG_TAG:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7}, Lcom/sec/phone/RILRequest;->serialString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "> "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget v10, v7, Lcom/sec/phone/RILRequest;->mRequest:I

    invoke-static {v10}, Lcom/sec/phone/SecPhoneService;->requestToString(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "["

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-static {v1}, Lcom/sec/phone/SecPhoneService;->bytesToHexString([B)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "]"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method

.method public invokeOemRilRequestRaw(Landroid/os/Message;)V
    .locals 14
    .param p1, "response"    # Landroid/os/Message;

    .prologue
    const/4 v13, 0x1

    const/4 v12, 0x2

    const/4 v9, 0x0

    .line 764
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v8

    const-string v10, "request"

    invoke-virtual {v8, v10}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v0

    .line 765
    .local v0, "data":[B
    invoke-direct {p0, v0}, Lcom/sec/phone/SecPhoneService;->routeDataToITelephony([B)Ljava/lang/reflect/Method;

    move-result-object v3

    .line 766
    .local v3, "oemHookMethod":Ljava/lang/reflect/Method;
    if-nez v3, :cond_1

    .line 768
    const/16 v8, 0x3b

    invoke-static {v8, p1}, Lcom/sec/phone/RILRequest;->obtain(ILandroid/os/Message;)Lcom/sec/phone/RILRequest;

    move-result-object v7

    .line 769
    .local v7, "rr":Lcom/sec/phone/RILRequest;
    iget-object v8, p0, Lcom/sec/phone/SecPhoneService;->LOG_TAG:Ljava/lang/String;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7}, Lcom/sec/phone/RILRequest;->serialString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "> "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget v11, v7, Lcom/sec/phone/RILRequest;->mRequest:I

    invoke-static {v11}, Lcom/sec/phone/SecPhoneService;->requestToString(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "["

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-static {v0}, Lcom/sec/phone/SecPhoneService;->bytesToHexString([B)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "]"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v8, v10}, Landroid/telephony/Rlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 773
    const-string v8, "ril.dumpstate"

    invoke-static {v8, v9}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    .line 775
    .local v2, "isCPDumpState":Z
    if-eqz v2, :cond_0

    .line 776
    iget-object v8, p0, Lcom/sec/phone/SecPhoneService;->LOG_TAG:Ljava/lang/String;

    const-string v9, "Can not process Ril Request during CP dump"

    invoke-static {v8, v9}, Landroid/telephony/Rlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 777
    const/4 v8, 0x0

    invoke-virtual {v7, v13, v8}, Lcom/sec/phone/RILRequest;->onError(ILjava/lang/Object;)V

    .line 778
    invoke-virtual {v7}, Lcom/sec/phone/RILRequest;->release()V

    .line 806
    .end local v2    # "isCPDumpState":Z
    .end local v7    # "rr":Lcom/sec/phone/RILRequest;
    :goto_0
    return-void

    .line 780
    .restart local v2    # "isCPDumpState":Z
    .restart local v7    # "rr":Lcom/sec/phone/RILRequest;
    :cond_0
    iget-object v8, v7, Lcom/sec/phone/RILRequest;->mp:Landroid/os/Parcel;

    invoke-virtual {v8, v0}, Landroid/os/Parcel;->writeByteArray([B)V

    .line 781
    invoke-direct {p0, v7}, Lcom/sec/phone/SecPhoneService;->send(Lcom/sec/phone/RILRequest;)V

    goto :goto_0

    .line 785
    .end local v2    # "isCPDumpState":Z
    .end local v7    # "rr":Lcom/sec/phone/RILRequest;
    :cond_1
    const/16 v8, 0xc8

    new-array v4, v8, [B

    .line 786
    .local v4, "oemHookResp":[B
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v5

    .line 787
    .local v5, "reply":Landroid/os/Message;
    invoke-virtual {v5, p1}, Landroid/os/Message;->copyFrom(Landroid/os/Message;)V

    .line 790
    :try_start_0
    invoke-direct {p0}, Lcom/sec/phone/SecPhoneService;->getTelephonyService()Lcom/android/internal/telephony/ITelephony;

    move-result-object v8

    const/4 v10, 0x2

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    aput-object v0, v10, v11

    const/4 v11, 0x1

    aput-object v4, v10, v11

    invoke-virtual {v3, v8, v10}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/Integer;

    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v6

    .line 791
    .local v6, "ret":I
    invoke-virtual {v5}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v10

    const-string v11, "error"

    if-ltz v6, :cond_3

    move v8, v9

    :goto_1
    invoke-virtual {v10, v11, v8}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 792
    if-lez v6, :cond_2

    .line 793
    invoke-virtual {v5}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v8

    const-string v9, "response"

    const/4 v10, 0x0

    invoke-static {v4, v10, v6}, Ljava/util/Arrays;->copyOfRange([BII)[B

    move-result-object v10

    invoke-virtual {v8, v9, v10}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V
    :try_end_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_2

    .line 802
    .end local v6    # "ret":I
    :cond_2
    :goto_2
    :try_start_1
    iget-object v8, p1, Landroid/os/Message;->replyTo:Landroid/os/Messenger;

    invoke-virtual {v8, v5}, Landroid/os/Messenger;->send(Landroid/os/Message;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 803
    :catch_0
    move-exception v8

    goto :goto_0

    .restart local v6    # "ret":I
    :cond_3
    move v8, v6

    .line 791
    goto :goto_1

    .line 795
    .end local v6    # "ret":I
    :catch_1
    move-exception v1

    .line 796
    .local v1, "e":Ljava/lang/IllegalAccessException;
    invoke-virtual {v5}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v8

    const-string v9, "error"

    invoke-virtual {v8, v9, v12}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    goto :goto_2

    .line 797
    .end local v1    # "e":Ljava/lang/IllegalAccessException;
    :catch_2
    move-exception v1

    .line 798
    .local v1, "e":Ljava/lang/reflect/InvocationTargetException;
    invoke-virtual {v5}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v8

    const-string v9, "error"

    invoke-virtual {v8, v9, v12}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    goto :goto_2
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 2
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 218
    iget-object v0, p0, Lcom/sec/phone/SecPhoneService;->LOG_TAG:Ljava/lang/String;

    const-string v1, "onBind()"

    invoke-static {v0, v1}, Landroid/telephony/Rlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 219
    iget-object v0, p0, Lcom/sec/phone/SecPhoneService;->mMessenger:Landroid/os/Messenger;

    invoke-virtual {v0}, Landroid/os/Messenger;->getBinder()Landroid/os/IBinder;

    move-result-object v0

    return-object v0
.end method

.method public onCreate()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 187
    const-string v2, "power"

    invoke-virtual {p0, v2}, Lcom/sec/phone/SecPhoneService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/PowerManager;

    .line 188
    .local v1, "pm":Landroid/os/PowerManager;
    const/4 v2, 0x1

    iget-object v3, p0, Lcom/sec/phone/SecPhoneService;->LOG_TAG:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/phone/SecPhoneService;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    .line 189
    iget-object v2, p0, Lcom/sec/phone/SecPhoneService;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v2, v4}, Landroid/os/PowerManager$WakeLock;->setReferenceCounted(Z)V

    .line 190
    iput v4, p0, Lcom/sec/phone/SecPhoneService;->mRequestMessagesPending:I

    .line 192
    invoke-direct {p0}, Lcom/sec/phone/SecPhoneService;->checkSupportUnsolicitedMsg()V

    .line 193
    new-instance v2, Landroid/os/HandlerThread;

    const-string v3, "RILSender"

    invoke-direct {v2, v3}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    iput-object v2, p0, Lcom/sec/phone/SecPhoneService;->mSenderThread:Landroid/os/HandlerThread;

    .line 194
    iget-object v2, p0, Lcom/sec/phone/SecPhoneService;->mSenderThread:Landroid/os/HandlerThread;

    invoke-virtual {v2}, Landroid/os/HandlerThread;->start()V

    .line 196
    iget-object v2, p0, Lcom/sec/phone/SecPhoneService;->mSenderThread:Landroid/os/HandlerThread;

    invoke-virtual {v2}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v0

    .line 197
    .local v0, "looper":Landroid/os/Looper;
    new-instance v2, Lcom/sec/phone/SecPhoneService$RILSender;

    invoke-direct {v2, p0, v0}, Lcom/sec/phone/SecPhoneService$RILSender;-><init>(Lcom/sec/phone/SecPhoneService;Landroid/os/Looper;)V

    iput-object v2, p0, Lcom/sec/phone/SecPhoneService;->mSender:Lcom/sec/phone/SecPhoneService$RILSender;

    .line 199
    new-instance v2, Lcom/sec/phone/SecPhoneService$RILReceiver;

    invoke-direct {v2, p0}, Lcom/sec/phone/SecPhoneService$RILReceiver;-><init>(Lcom/sec/phone/SecPhoneService;)V

    iput-object v2, p0, Lcom/sec/phone/SecPhoneService;->mReceiver:Lcom/sec/phone/SecPhoneService$RILReceiver;

    .line 200
    new-instance v2, Ljava/lang/Thread;

    iget-object v3, p0, Lcom/sec/phone/SecPhoneService;->mReceiver:Lcom/sec/phone/SecPhoneService$RILReceiver;

    const-string v4, "RILReceiver"

    invoke-direct {v2, v3, v4}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V

    iput-object v2, p0, Lcom/sec/phone/SecPhoneService;->mReceiverThread:Ljava/lang/Thread;

    .line 201
    iget-object v2, p0, Lcom/sec/phone/SecPhoneService;->mReceiverThread:Ljava/lang/Thread;

    invoke-virtual {v2}, Ljava/lang/Thread;->start()V

    .line 202
    return-void
.end method

.method public onDestroy()V
    .locals 3

    .prologue
    .line 285
    iget-object v1, p0, Lcom/sec/phone/SecPhoneService;->LOG_TAG:Ljava/lang/String;

    const-string v2, "onDestroy start"

    invoke-static {v1, v2}, Landroid/telephony/Rlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 286
    iget-object v1, p0, Lcom/sec/phone/SecPhoneService;->mSenderThread:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->quit()Z

    .line 287
    iget-object v1, p0, Lcom/sec/phone/SecPhoneService;->mReceiverThread:Ljava/lang/Thread;

    invoke-virtual {v1}, Ljava/lang/Thread;->interrupt()V

    .line 290
    :try_start_0
    iget-object v1, p0, Lcom/sec/phone/SecPhoneService;->mSocket:Landroid/net/LocalSocket;

    if-eqz v1, :cond_0

    .line 291
    iget-object v1, p0, Lcom/sec/phone/SecPhoneService;->mSocket:Landroid/net/LocalSocket;

    invoke-virtual {v1}, Landroid/net/LocalSocket;->shutdownInput()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 298
    :goto_0
    invoke-direct {p0}, Lcom/sec/phone/SecPhoneService;->releaseWakeLock()V

    .line 300
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 301
    iget-object v1, p0, Lcom/sec/phone/SecPhoneService;->LOG_TAG:Ljava/lang/String;

    const-string v2, "onDestroy finish"

    invoke-static {v1, v2}, Landroid/telephony/Rlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 302
    return-void

    .line 293
    :cond_0
    :try_start_1
    iget-object v1, p0, Lcom/sec/phone/SecPhoneService;->LOG_TAG:Ljava/lang/String;

    const-string v2, "socket connection is broken"

    invoke-static {v1, v2}, Landroid/telephony/Rlog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 294
    :catch_0
    move-exception v0

    .line 295
    .local v0, "ex":Ljava/io/IOException;
    iget-object v1, p0, Lcom/sec/phone/SecPhoneService;->LOG_TAG:Ljava/lang/String;

    const-string v2, "Uncaught exception during shutdownInput"

    invoke-static {v1, v2, v0}, Landroid/telephony/Rlog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 3
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "flags"    # I
    .param p3, "startId"    # I

    .prologue
    .line 224
    iget-object v1, p0, Lcom/sec/phone/SecPhoneService;->LOG_TAG:Ljava/lang/String;

    const-string v2, "onStartCommand()"

    invoke-static {v1, v2}, Landroid/telephony/Rlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 225
    if-eqz p1, :cond_0

    .line 226
    const-string v1, "MODE"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 228
    .local v0, "mode":Ljava/lang/String;
    const-string v1, "FORCE_SLEEP"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 229
    iget-object v1, p0, Lcom/sec/phone/SecPhoneService;->LOG_TAG:Ljava/lang/String;

    const-string v2, "FORCE_SLEEP mode"

    invoke-static {v1, v2}, Landroid/telephony/Rlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 230
    const/4 v1, 0x0

    iput v1, p0, Lcom/sec/phone/SecPhoneService;->mRequestMessagesPending:I

    .line 231
    iget-object v2, p0, Lcom/sec/phone/SecPhoneService;->mRequestsList:Ljava/util/ArrayList;

    monitor-enter v2

    .line 232
    :try_start_0
    iget-object v1, p0, Lcom/sec/phone/SecPhoneService;->mRequestsList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 233
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 234
    invoke-direct {p0}, Lcom/sec/phone/SecPhoneService;->releaseWakeLockIfDone()V

    .line 241
    .end local v0    # "mode":Ljava/lang/String;
    :cond_0
    :goto_0
    const/4 v1, 0x2

    return v1

    .line 233
    .restart local v0    # "mode":Ljava/lang/String;
    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1

    .line 235
    :cond_1
    const-string v1, "CUSTOMER_TEST_RESULT"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 236
    iget-object v1, p0, Lcom/sec/phone/SecPhoneService;->LOG_TAG:Ljava/lang/String;

    const-string v2, "CUSTOMER_TEST_RESULT"

    invoke-static {v1, v2}, Landroid/telephony/Rlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 237
    invoke-virtual {p0, p1}, Lcom/sec/phone/SecPhoneService;->SendCustomerTestResult(Landroid/content/Intent;)V

    goto :goto_0
.end method
