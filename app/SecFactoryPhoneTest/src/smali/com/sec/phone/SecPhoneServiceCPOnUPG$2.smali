.class Lcom/sec/phone/SecPhoneServiceCPOnUPG$2;
.super Ljava/lang/Object;
.source "SecPhoneServiceCPOnUPG.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/phone/SecPhoneServiceCPOnUPG;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/phone/SecPhoneServiceCPOnUPG;


# direct methods
.method constructor <init>(Lcom/sec/phone/SecPhoneServiceCPOnUPG;)V
    .locals 0

    .prologue
    .line 198
    iput-object p1, p0, Lcom/sec/phone/SecPhoneServiceCPOnUPG$2;->this$0:Lcom/sec/phone/SecPhoneServiceCPOnUPG;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 2
    .param p1, "className"    # Landroid/content/ComponentName;
    .param p2, "service"    # Landroid/os/IBinder;

    .prologue
    .line 200
    iget-object v0, p0, Lcom/sec/phone/SecPhoneServiceCPOnUPG$2;->this$0:Lcom/sec/phone/SecPhoneServiceCPOnUPG;

    iget-object v0, v0, Lcom/sec/phone/SecPhoneServiceCPOnUPG;->LOG_TAG:Ljava/lang/String;

    const-string v1, "* FactoryTestClient * onServiceConnected()"

    invoke-static {v0, v1}, Landroid/telephony/Rlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 201
    iget-object v0, p0, Lcom/sec/phone/SecPhoneServiceCPOnUPG$2;->this$0:Lcom/sec/phone/SecPhoneServiceCPOnUPG;

    new-instance v1, Landroid/os/Messenger;

    invoke-direct {v1, p2}, Landroid/os/Messenger;-><init>(Landroid/os/IBinder;)V

    # setter for: Lcom/sec/phone/SecPhoneServiceCPOnUPG;->mSendServiceMessenger:Landroid/os/Messenger;
    invoke-static {v0, v1}, Lcom/sec/phone/SecPhoneServiceCPOnUPG;->access$002(Lcom/sec/phone/SecPhoneServiceCPOnUPG;Landroid/os/Messenger;)Landroid/os/Messenger;

    .line 202
    iget-object v0, p0, Lcom/sec/phone/SecPhoneServiceCPOnUPG$2;->this$0:Lcom/sec/phone/SecPhoneServiceCPOnUPG;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/sec/phone/SecPhoneServiceCPOnUPG;->m_bBindDone:Z

    .line 203
    return-void
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 2
    .param p1, "className"    # Landroid/content/ComponentName;

    .prologue
    .line 206
    iget-object v0, p0, Lcom/sec/phone/SecPhoneServiceCPOnUPG$2;->this$0:Lcom/sec/phone/SecPhoneServiceCPOnUPG;

    iget-object v0, v0, Lcom/sec/phone/SecPhoneServiceCPOnUPG;->LOG_TAG:Ljava/lang/String;

    const-string v1, "* FactoryTestClient * onServiceDisconnected()"

    invoke-static {v0, v1}, Landroid/telephony/Rlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 207
    iget-object v0, p0, Lcom/sec/phone/SecPhoneServiceCPOnUPG$2;->this$0:Lcom/sec/phone/SecPhoneServiceCPOnUPG;

    const/4 v1, 0x0

    # setter for: Lcom/sec/phone/SecPhoneServiceCPOnUPG;->mSendServiceMessenger:Landroid/os/Messenger;
    invoke-static {v0, v1}, Lcom/sec/phone/SecPhoneServiceCPOnUPG;->access$002(Lcom/sec/phone/SecPhoneServiceCPOnUPG;Landroid/os/Messenger;)Landroid/os/Messenger;

    .line 208
    iget-object v0, p0, Lcom/sec/phone/SecPhoneServiceCPOnUPG$2;->this$0:Lcom/sec/phone/SecPhoneServiceCPOnUPG;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/sec/phone/SecPhoneServiceCPOnUPG;->m_bBindDone:Z

    .line 209
    iget-object v0, p0, Lcom/sec/phone/SecPhoneServiceCPOnUPG$2;->this$0:Lcom/sec/phone/SecPhoneServiceCPOnUPG;

    # invokes: Lcom/sec/phone/SecPhoneServiceCPOnUPG;->rebindFactoryTestClientService()V
    invoke-static {v0}, Lcom/sec/phone/SecPhoneServiceCPOnUPG;->access$100(Lcom/sec/phone/SecPhoneServiceCPOnUPG;)V

    .line 210
    return-void
.end method
