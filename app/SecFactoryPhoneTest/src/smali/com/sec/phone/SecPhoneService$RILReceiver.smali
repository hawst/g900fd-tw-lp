.class Lcom/sec/phone/SecPhoneService$RILReceiver;
.super Ljava/lang/Object;
.source "SecPhoneService.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/phone/SecPhoneService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "RILReceiver"
.end annotation


# instance fields
.field buffer:[B

.field final synthetic this$0:Lcom/sec/phone/SecPhoneService;


# direct methods
.method constructor <init>(Lcom/sec/phone/SecPhoneService;)V
    .locals 1

    .prologue
    .line 475
    iput-object p1, p0, Lcom/sec/phone/SecPhoneService$RILReceiver;->this$0:Lcom/sec/phone/SecPhoneService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 476
    const/16 v0, 0x2000

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/sec/phone/SecPhoneService$RILReceiver;->buffer:[B

    .line 477
    return-void
.end method


# virtual methods
.method public WaitForDualSecRilReady()V
    .locals 7

    .prologue
    .line 480
    const-string v2, ""

    .line 481
    .local v2, "swVer":Ljava/lang/String;
    const-string v3, ""

    .line 482
    .local v3, "swVer2":Ljava/lang/String;
    const/16 v1, 0x3c

    .line 485
    .local v1, "retryCount":I
    :goto_0
    if-lez v1, :cond_2

    .line 486
    const-string v4, "ril.sw_ver"

    const-string v5, "NONE"

    invoke-static {v4, v5}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 487
    const-string v4, "ril.sw_ver2"

    const-string v5, "NONE"

    invoke-static {v4, v5}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 488
    iget-object v4, p0, Lcom/sec/phone/SecPhoneService$RILReceiver;->this$0:Lcom/sec/phone/SecPhoneService;

    iget-object v4, v4, Lcom/sec/phone/SecPhoneService;->LOG_TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "swVer : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " swVer2 : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/telephony/Rlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 490
    const-string v4, "NONE"

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    const-string v4, "NONE"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 493
    :cond_0
    const-wide/16 v4, 0x1f4

    :try_start_0
    invoke-static {v4, v5}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 497
    :goto_1
    add-int/lit8 v1, v1, -0x1

    goto :goto_0

    .line 494
    :catch_0
    move-exception v0

    .line 495
    .local v0, "er":Ljava/lang/InterruptedException;
    iget-object v4, p0, Lcom/sec/phone/SecPhoneService$RILReceiver;->this$0:Lcom/sec/phone/SecPhoneService;

    iget-object v4, v4, Lcom/sec/phone/SecPhoneService;->LOG_TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "InterruptedException : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/telephony/Rlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 499
    .end local v0    # "er":Ljava/lang/InterruptedException;
    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    .line 503
    :cond_2
    iget-object v4, p0, Lcom/sec/phone/SecPhoneService$RILReceiver;->this$0:Lcom/sec/phone/SecPhoneService;

    iget-object v4, v4, Lcom/sec/phone/SecPhoneService;->LOG_TAG:Ljava/lang/String;

    const-string v5, "Secril is ready"

    invoke-static {v4, v5}, Landroid/telephony/Rlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 504
    return-void
.end method

.method public WaitForSecRilReady()V
    .locals 8

    .prologue
    .line 507
    const-string v2, ""

    .line 508
    .local v2, "swVer":Ljava/lang/String;
    const/16 v1, 0x28

    .line 511
    .local v1, "retryCount":I
    :goto_0
    if-lez v1, :cond_1

    .line 512
    const-string v3, "ril.sw_ver"

    const-string v4, "NONE"

    invoke-static {v3, v4}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 513
    iget-object v3, p0, Lcom/sec/phone/SecPhoneService$RILReceiver;->this$0:Lcom/sec/phone/SecPhoneService;

    iget-object v3, v3, Lcom/sec/phone/SecPhoneService;->LOG_TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "swVer : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/telephony/Rlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 515
    const-string v3, "NONE"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 518
    const-wide/16 v4, 0x1f4

    :try_start_0
    invoke-static {v4, v5}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 523
    :goto_1
    add-int/lit8 v1, v1, -0x1

    goto :goto_0

    .line 519
    :catch_0
    move-exception v0

    .line 520
    .local v0, "er":Ljava/lang/InterruptedException;
    iget-object v3, p0, Lcom/sec/phone/SecPhoneService$RILReceiver;->this$0:Lcom/sec/phone/SecPhoneService;

    iget-object v3, v3, Lcom/sec/phone/SecPhoneService;->LOG_TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Thread interrupted...start to exit - "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/phone/SecPhoneService$RILReceiver;->this$0:Lcom/sec/phone/SecPhoneService;

    iget-object v5, v5, Lcom/sec/phone/SecPhoneService;->RilSocketName:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", TID: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/phone/SecPhoneService$RILReceiver;->this$0:Lcom/sec/phone/SecPhoneService;

    iget-object v5, v5, Lcom/sec/phone/SecPhoneService;->mReceiverThread:Ljava/lang/Thread;

    invoke-virtual {v5}, Ljava/lang/Thread;->getId()J

    move-result-wide v6

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/telephony/Rlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 525
    .end local v0    # "er":Ljava/lang/InterruptedException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    .line 529
    :cond_1
    iget-object v3, p0, Lcom/sec/phone/SecPhoneService$RILReceiver;->this$0:Lcom/sec/phone/SecPhoneService;

    iget-object v3, v3, Lcom/sec/phone/SecPhoneService;->LOG_TAG:Ljava/lang/String;

    const-string v4, "Secril is ready"

    invoke-static {v3, v4}, Landroid/telephony/Rlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 530
    return-void
.end method

.method public run()V
    .locals 18

    .prologue
    .line 533
    const/4 v9, 0x0

    .line 539
    .local v9, "retryCount":I
    :goto_0
    :try_start_0
    invoke-static {}, Ljava/lang/Thread;->interrupted()Z

    move-result v13

    if-eqz v13, :cond_0

    .line 540
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/phone/SecPhoneService$RILReceiver;->this$0:Lcom/sec/phone/SecPhoneService;

    iget-object v13, v13, Lcom/sec/phone/SecPhoneService;->LOG_TAG:Ljava/lang/String;

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "Thread interrupted...start to exit - "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/phone/SecPhoneService$RILReceiver;->this$0:Lcom/sec/phone/SecPhoneService;

    iget-object v15, v15, Lcom/sec/phone/SecPhoneService;->RilSocketName:Ljava/lang/String;

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, ", TID: "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/phone/SecPhoneService$RILReceiver;->this$0:Lcom/sec/phone/SecPhoneService;

    iget-object v15, v15, Lcom/sec/phone/SecPhoneService;->mReceiverThread:Ljava/lang/Thread;

    invoke-virtual {v15}, Ljava/lang/Thread;->getId()J

    move-result-wide v16

    move-wide/from16 v0, v16

    invoke-virtual {v14, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Landroid/telephony/Rlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 542
    new-instance v13, Ljava/lang/InterruptedException;

    invoke-direct {v13}, Ljava/lang/InterruptedException;-><init>()V

    throw v13
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_2

    .line 659
    :catch_0
    move-exception v2

    .line 662
    .local v2, "er":Ljava/lang/InterruptedException;
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/phone/SecPhoneService$RILReceiver;->this$0:Lcom/sec/phone/SecPhoneService;

    iget-object v13, v13, Lcom/sec/phone/SecPhoneService;->LOG_TAG:Ljava/lang/String;

    const-string v14, "Thread is terminated by InterruptedException"

    invoke-static {v13, v14}, Landroid/telephony/Rlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 668
    .end local v2    # "er":Ljava/lang/InterruptedException;
    :goto_1
    return-void

    .line 552
    :cond_0
    :try_start_1
    invoke-virtual/range {p0 .. p0}, Lcom/sec/phone/SecPhoneService$RILReceiver;->WaitForSecRilReady()V

    .line 555
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/phone/SecPhoneService$RILReceiver;->this$0:Lcom/sec/phone/SecPhoneService;

    iget-object v13, v13, Lcom/sec/phone/SecPhoneService;->LOG_TAG:Ljava/lang/String;

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "start connect to \'"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/phone/SecPhoneService$RILReceiver;->this$0:Lcom/sec/phone/SecPhoneService;

    iget-object v15, v15, Lcom/sec/phone/SecPhoneService;->RilSocketName:Ljava/lang/String;

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, "\' socket"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, ", TID: "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/phone/SecPhoneService$RILReceiver;->this$0:Lcom/sec/phone/SecPhoneService;

    iget-object v15, v15, Lcom/sec/phone/SecPhoneService;->mReceiverThread:Ljava/lang/Thread;

    invoke-virtual {v15}, Ljava/lang/Thread;->getId()J

    move-result-wide v16

    move-wide/from16 v0, v16

    invoke-virtual {v14, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Landroid/telephony/Rlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 557
    new-instance v7, Landroid/net/LocalSocketAddress;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/phone/SecPhoneService$RILReceiver;->this$0:Lcom/sec/phone/SecPhoneService;

    iget-object v13, v13, Lcom/sec/phone/SecPhoneService;->RilSocketName:Ljava/lang/String;

    invoke-direct {v7, v13}, Landroid/net/LocalSocketAddress;-><init>(Ljava/lang/String;)V

    .line 559
    .local v7, "lsa":Landroid/net/LocalSocketAddress;
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/phone/SecPhoneService$RILReceiver;->this$0:Lcom/sec/phone/SecPhoneService;

    iget-object v13, v13, Lcom/sec/phone/SecPhoneService;->mSocket:Landroid/net/LocalSocket;

    if-nez v13, :cond_1

    .line 560
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/phone/SecPhoneService$RILReceiver;->this$0:Lcom/sec/phone/SecPhoneService;

    new-instance v14, Landroid/net/LocalSocket;

    invoke-direct {v14}, Landroid/net/LocalSocket;-><init>()V

    iput-object v14, v13, Lcom/sec/phone/SecPhoneService;->mSocket:Landroid/net/LocalSocket;
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_2

    .line 563
    :cond_1
    :try_start_2
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/phone/SecPhoneService$RILReceiver;->this$0:Lcom/sec/phone/SecPhoneService;

    iget-object v13, v13, Lcom/sec/phone/SecPhoneService;->mSocket:Landroid/net/LocalSocket;

    invoke-virtual {v13, v7}, Landroid/net/LocalSocket;->connect(Landroid/net/LocalSocketAddress;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_2

    .line 596
    const/4 v9, 0x0

    .line 598
    const/4 v13, 0x1

    :try_start_3
    invoke-static {v13}, Lcom/sec/phone/BootCompleteReceiver;->hasSecrilDaemon(I)Z

    move-result v13

    if-eqz v13, :cond_5

    invoke-static {}, Lcom/sec/phone/BootCompleteReceiver;->hasQcril()Z

    move-result v13

    if-eqz v13, :cond_5

    .line 599
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/phone/SecPhoneService$RILReceiver;->this$0:Lcom/sec/phone/SecPhoneService;

    const-string v14, "android.intent.action.SECPHONE_READY"

    # invokes: Lcom/sec/phone/SecPhoneService;->sendIntentSecPhoneReady(Ljava/lang/String;)V
    invoke-static {v13, v14}, Lcom/sec/phone/SecPhoneService;->access$100(Lcom/sec/phone/SecPhoneService;Ljava/lang/String;)V

    .line 603
    :goto_2
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/phone/SecPhoneService$RILReceiver;->this$0:Lcom/sec/phone/SecPhoneService;

    # invokes: Lcom/sec/phone/SecPhoneService;->SendQueuingMsg()V
    invoke-static {v13}, Lcom/sec/phone/SecPhoneService;->access$300(Lcom/sec/phone/SecPhoneService;)V
    :try_end_3
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_2

    .line 605
    const/4 v6, 0x0

    .line 607
    .local v6, "length":I
    :try_start_4
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/phone/SecPhoneService$RILReceiver;->this$0:Lcom/sec/phone/SecPhoneService;

    iget-object v13, v13, Lcom/sec/phone/SecPhoneService;->mSocket:Landroid/net/LocalSocket;

    invoke-virtual {v13}, Landroid/net/LocalSocket;->getInputStream()Ljava/io/InputStream;

    move-result-object v5

    .line 612
    .local v5, "is":Ljava/io/InputStream;
    :goto_3
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/phone/SecPhoneService$RILReceiver;->this$0:Lcom/sec/phone/SecPhoneService;

    iget-object v13, v13, Lcom/sec/phone/SecPhoneService;->LOG_TAG:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/phone/SecPhoneService$RILReceiver;->buffer:[B

    # invokes: Lcom/sec/phone/SecPhoneService;->readRilMessage(Ljava/lang/String;Ljava/io/InputStream;[B)I
    invoke-static {v13, v5, v14}, Lcom/sec/phone/SecPhoneService;->access$400(Ljava/lang/String;Ljava/io/InputStream;[B)I
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_5
    .catch Ljava/lang/InterruptedException; {:try_start_4 .. :try_end_4} :catch_0

    move-result v6

    .line 614
    if-gez v6, :cond_6

    .line 635
    .end local v5    # "is":Ljava/io/InputStream;
    :goto_4
    :try_start_5
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/phone/SecPhoneService$RILReceiver;->this$0:Lcom/sec/phone/SecPhoneService;

    iget-object v13, v13, Lcom/sec/phone/SecPhoneService;->LOG_TAG:Ljava/lang/String;

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "Disconnected from \'"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/phone/SecPhoneService$RILReceiver;->this$0:Lcom/sec/phone/SecPhoneService;

    iget-object v15, v15, Lcom/sec/phone/SecPhoneService;->RilSocketName:Ljava/lang/String;

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, "\' socket"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Landroid/telephony/Rlog;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_5
    .catch Ljava/lang/InterruptedException; {:try_start_5 .. :try_end_5} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_5} :catch_2

    .line 638
    :try_start_6
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/phone/SecPhoneService$RILReceiver;->this$0:Lcom/sec/phone/SecPhoneService;

    iget-object v13, v13, Lcom/sec/phone/SecPhoneService;->mSocket:Landroid/net/LocalSocket;

    invoke-virtual {v13}, Landroid/net/LocalSocket;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_6
    .catch Ljava/lang/InterruptedException; {:try_start_6 .. :try_end_6} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_6 .. :try_end_6} :catch_2

    .line 642
    :goto_5
    :try_start_7
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/phone/SecPhoneService$RILReceiver;->this$0:Lcom/sec/phone/SecPhoneService;

    const/4 v14, 0x0

    iput-object v14, v13, Lcom/sec/phone/SecPhoneService;->mSocket:Landroid/net/LocalSocket;

    .line 643
    invoke-static {}, Lcom/sec/phone/RILRequest;->resetSerial()V

    .line 646
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/phone/SecPhoneService$RILReceiver;->this$0:Lcom/sec/phone/SecPhoneService;

    iget-object v14, v13, Lcom/sec/phone/SecPhoneService;->mRequestsList:Ljava/util/ArrayList;

    monitor-enter v14
    :try_end_7
    .catch Ljava/lang/InterruptedException; {:try_start_7 .. :try_end_7} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_7 .. :try_end_7} :catch_2

    .line 647
    const/4 v4, 0x0

    .local v4, "i":I
    :try_start_8
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/phone/SecPhoneService$RILReceiver;->this$0:Lcom/sec/phone/SecPhoneService;

    iget-object v13, v13, Lcom/sec/phone/SecPhoneService;->mRequestsList:Ljava/util/ArrayList;

    invoke-virtual {v13}, Ljava/util/ArrayList;->size()I

    move-result v11

    .local v11, "sz":I
    :goto_6
    if-ge v4, v11, :cond_7

    .line 648
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/phone/SecPhoneService$RILReceiver;->this$0:Lcom/sec/phone/SecPhoneService;

    iget-object v13, v13, Lcom/sec/phone/SecPhoneService;->mRequestsList:Ljava/util/ArrayList;

    invoke-virtual {v13, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/sec/phone/RILRequest;

    .line 649
    .local v10, "rr":Lcom/sec/phone/RILRequest;
    const/4 v13, 0x1

    const/4 v15, 0x0

    invoke-virtual {v10, v13, v15}, Lcom/sec/phone/RILRequest;->onError(ILjava/lang/Object;)V

    .line 650
    invoke-virtual {v10}, Lcom/sec/phone/RILRequest;->release()V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    .line 647
    add-int/lit8 v4, v4, 0x1

    goto :goto_6

    .line 564
    .end local v4    # "i":I
    .end local v6    # "length":I
    .end local v10    # "rr":Lcom/sec/phone/RILRequest;
    .end local v11    # "sz":I
    :catch_1
    move-exception v3

    .line 565
    .local v3, "ex":Ljava/io/IOException;
    :try_start_9
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/phone/SecPhoneService$RILReceiver;->this$0:Lcom/sec/phone/SecPhoneService;

    iget-object v13, v13, Lcom/sec/phone/SecPhoneService;->mSocket:Landroid/net/LocalSocket;

    invoke-virtual {v13}, Landroid/net/LocalSocket;->isConnected()Z

    move-result v13

    if-nez v13, :cond_2

    .line 572
    :cond_2
    const/16 v13, 0x8

    if-ne v9, v13, :cond_4

    .line 573
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/phone/SecPhoneService$RILReceiver;->this$0:Lcom/sec/phone/SecPhoneService;

    iget-object v13, v13, Lcom/sec/phone/SecPhoneService;->LOG_TAG:Ljava/lang/String;

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "Couldn\'t find \'"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/phone/SecPhoneService$RILReceiver;->this$0:Lcom/sec/phone/SecPhoneService;

    iget-object v15, v15, Lcom/sec/phone/SecPhoneService;->RilSocketName:Ljava/lang/String;

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, "\' socket after "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, " times, continuing to retry silently"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Landroid/telephony/Rlog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_9
    .catch Ljava/lang/InterruptedException; {:try_start_9 .. :try_end_9} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_9 .. :try_end_9} :catch_2

    .line 581
    :cond_3
    :goto_7
    const-wide/16 v14, 0xfa0

    :try_start_a
    invoke-static {v14, v15}, Ljava/lang/Thread;->sleep(J)V
    :try_end_a
    .catch Ljava/lang/InterruptedException; {:try_start_a .. :try_end_a} :catch_3
    .catch Ljava/lang/Throwable; {:try_start_a .. :try_end_a} :catch_2

    .line 592
    add-int/lit8 v9, v9, 0x1

    .line 593
    goto/16 :goto_0

    .line 575
    :cond_4
    if-lez v9, :cond_3

    const/16 v13, 0x8

    if-ge v9, v13, :cond_3

    .line 576
    :try_start_b
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/phone/SecPhoneService$RILReceiver;->this$0:Lcom/sec/phone/SecPhoneService;

    iget-object v13, v13, Lcom/sec/phone/SecPhoneService;->LOG_TAG:Ljava/lang/String;

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "Couldn\'t find \'"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/phone/SecPhoneService$RILReceiver;->this$0:Lcom/sec/phone/SecPhoneService;

    iget-object v15, v15, Lcom/sec/phone/SecPhoneService;->RilSocketName:Ljava/lang/String;

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, "\' socket; retrying after timeout"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Landroid/telephony/Rlog;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_b
    .catch Ljava/lang/InterruptedException; {:try_start_b .. :try_end_b} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_b .. :try_end_b} :catch_2

    goto :goto_7

    .line 665
    .end local v3    # "ex":Ljava/io/IOException;
    .end local v7    # "lsa":Landroid/net/LocalSocketAddress;
    :catch_2
    move-exception v12

    .line 666
    .local v12, "tr":Ljava/lang/Throwable;
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/phone/SecPhoneService$RILReceiver;->this$0:Lcom/sec/phone/SecPhoneService;

    iget-object v13, v13, Lcom/sec/phone/SecPhoneService;->LOG_TAG:Ljava/lang/String;

    const-string v14, "Uncaught exception "

    invoke-static {v13, v14, v12}, Landroid/telephony/Rlog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_1

    .line 582
    .end local v12    # "tr":Ljava/lang/Throwable;
    .restart local v3    # "ex":Ljava/io/IOException;
    .restart local v7    # "lsa":Landroid/net/LocalSocketAddress;
    :catch_3
    move-exception v2

    .line 585
    .restart local v2    # "er":Ljava/lang/InterruptedException;
    :try_start_c
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/phone/SecPhoneService$RILReceiver;->this$0:Lcom/sec/phone/SecPhoneService;

    iget-object v13, v13, Lcom/sec/phone/SecPhoneService;->LOG_TAG:Ljava/lang/String;

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "Thread interrupted...start to exit - "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/phone/SecPhoneService$RILReceiver;->this$0:Lcom/sec/phone/SecPhoneService;

    iget-object v15, v15, Lcom/sec/phone/SecPhoneService;->RilSocketName:Ljava/lang/String;

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, ", TID: "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/phone/SecPhoneService$RILReceiver;->this$0:Lcom/sec/phone/SecPhoneService;

    iget-object v15, v15, Lcom/sec/phone/SecPhoneService;->mReceiverThread:Ljava/lang/Thread;

    invoke-virtual {v15}, Ljava/lang/Thread;->getId()J

    move-result-wide v16

    move-wide/from16 v0, v16

    invoke-virtual {v14, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Landroid/telephony/Rlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 601
    .end local v2    # "er":Ljava/lang/InterruptedException;
    .end local v3    # "ex":Ljava/io/IOException;
    :cond_5
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/phone/SecPhoneService$RILReceiver;->this$0:Lcom/sec/phone/SecPhoneService;

    # invokes: Lcom/sec/phone/SecPhoneService;->sendRequestSecPhoneReady()V
    invoke-static {v13}, Lcom/sec/phone/SecPhoneService;->access$200(Lcom/sec/phone/SecPhoneService;)V
    :try_end_c
    .catch Ljava/lang/InterruptedException; {:try_start_c .. :try_end_c} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_c .. :try_end_c} :catch_2

    goto/16 :goto_2

    .line 618
    .restart local v5    # "is":Ljava/io/InputStream;
    .restart local v6    # "length":I
    :cond_6
    :try_start_d
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v8

    .line 619
    .local v8, "p":Landroid/os/Parcel;
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/phone/SecPhoneService$RILReceiver;->buffer:[B

    const/4 v14, 0x0

    invoke-virtual {v8, v13, v14, v6}, Landroid/os/Parcel;->unmarshall([BII)V

    .line 620
    const/4 v13, 0x0

    invoke-virtual {v8, v13}, Landroid/os/Parcel;->setDataPosition(I)V

    .line 625
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/phone/SecPhoneService$RILReceiver;->this$0:Lcom/sec/phone/SecPhoneService;

    # invokes: Lcom/sec/phone/SecPhoneService;->processResponse(Landroid/os/Parcel;)V
    invoke-static {v13, v8}, Lcom/sec/phone/SecPhoneService;->access$500(Lcom/sec/phone/SecPhoneService;Landroid/os/Parcel;)V

    .line 626
    invoke-virtual {v8}, Landroid/os/Parcel;->recycle()V
    :try_end_d
    .catch Ljava/io/IOException; {:try_start_d .. :try_end_d} :catch_4
    .catch Ljava/lang/Throwable; {:try_start_d .. :try_end_d} :catch_5
    .catch Ljava/lang/InterruptedException; {:try_start_d .. :try_end_d} :catch_0

    goto/16 :goto_3

    .line 628
    .end local v5    # "is":Ljava/io/InputStream;
    .end local v8    # "p":Landroid/os/Parcel;
    :catch_4
    move-exception v3

    .line 629
    .restart local v3    # "ex":Ljava/io/IOException;
    :try_start_e
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/phone/SecPhoneService$RILReceiver;->this$0:Lcom/sec/phone/SecPhoneService;

    iget-object v13, v13, Lcom/sec/phone/SecPhoneService;->LOG_TAG:Ljava/lang/String;

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "\'"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/phone/SecPhoneService$RILReceiver;->this$0:Lcom/sec/phone/SecPhoneService;

    iget-object v15, v15, Lcom/sec/phone/SecPhoneService;->RilSocketName:Ljava/lang/String;

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, "\' socket closed, "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14, v3}, Landroid/telephony/Rlog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_4

    .line 630
    .end local v3    # "ex":Ljava/io/IOException;
    :catch_5
    move-exception v12

    .line 631
    .restart local v12    # "tr":Ljava/lang/Throwable;
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/phone/SecPhoneService$RILReceiver;->this$0:Lcom/sec/phone/SecPhoneService;

    iget-object v13, v13, Lcom/sec/phone/SecPhoneService;->LOG_TAG:Ljava/lang/String;

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "Uncaught exception read length="

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, ", Exception:"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v12}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14, v12}, Landroid/telephony/Rlog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_e
    .catch Ljava/lang/InterruptedException; {:try_start_e .. :try_end_e} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_e .. :try_end_e} :catch_2

    goto/16 :goto_4

    .line 653
    .end local v12    # "tr":Ljava/lang/Throwable;
    .restart local v4    # "i":I
    .restart local v11    # "sz":I
    :cond_7
    :try_start_f
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/phone/SecPhoneService$RILReceiver;->this$0:Lcom/sec/phone/SecPhoneService;

    iget-object v13, v13, Lcom/sec/phone/SecPhoneService;->mRequestsList:Ljava/util/ArrayList;

    invoke-virtual {v13}, Ljava/util/ArrayList;->clear()V

    .line 654
    monitor-exit v14

    goto/16 :goto_0

    .end local v11    # "sz":I
    :catchall_0
    move-exception v13

    monitor-exit v14
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_0

    :try_start_10
    throw v13
    :try_end_10
    .catch Ljava/lang/InterruptedException; {:try_start_10 .. :try_end_10} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_10 .. :try_end_10} :catch_2

    .line 639
    .end local v4    # "i":I
    :catch_6
    move-exception v13

    goto/16 :goto_5
.end method
