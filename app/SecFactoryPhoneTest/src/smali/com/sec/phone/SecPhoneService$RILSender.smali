.class Lcom/sec/phone/SecPhoneService$RILSender;
.super Landroid/os/Handler;
.source "SecPhoneService.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/phone/SecPhoneService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "RILSender"
.end annotation


# instance fields
.field dataLength:[B

.field final synthetic this$0:Lcom/sec/phone/SecPhoneService;


# direct methods
.method public constructor <init>(Lcom/sec/phone/SecPhoneService;Landroid/os/Looper;)V
    .locals 1
    .param p2, "looper"    # Landroid/os/Looper;

    .prologue
    .line 305
    iput-object p1, p0, Lcom/sec/phone/SecPhoneService$RILSender;->this$0:Lcom/sec/phone/SecPhoneService;

    .line 306
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 310
    const/4 v0, 0x4

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/sec/phone/SecPhoneService$RILSender;->dataLength:[B

    .line 307
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 15
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 320
    move-object/from16 v0, p1

    iget-object v10, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v10, Lcom/sec/phone/RILRequest;

    move-object v8, v10

    check-cast v8, Lcom/sec/phone/RILRequest;

    .line 321
    .local v8, "rr":Lcom/sec/phone/RILRequest;
    const/4 v7, 0x0

    .line 323
    .local v7, "req":Lcom/sec/phone/RILRequest;
    move-object/from16 v0, p1

    iget v10, v0, Landroid/os/Message;->what:I

    packed-switch v10, :pswitch_data_0

    .line 423
    new-instance v10, Ljava/lang/RuntimeException;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Unrecognized event: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    move-object/from16 v0, p1

    iget v12, v0, Landroid/os/Message;->what:I

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v10

    .line 330
    :pswitch_0
    const/4 v1, 0x0

    .line 334
    .local v1, "alreadySubtracted":Z
    :try_start_0
    iget-object v10, p0, Lcom/sec/phone/SecPhoneService$RILSender;->this$0:Lcom/sec/phone/SecPhoneService;

    iget-object v9, v10, Lcom/sec/phone/SecPhoneService;->mSocket:Landroid/net/LocalSocket;

    .line 336
    .local v9, "s":Landroid/net/LocalSocket;
    if-nez v9, :cond_1

    .line 337
    const/4 v10, 0x1

    const/4 v11, 0x0

    invoke-virtual {v8, v10, v11}, Lcom/sec/phone/RILRequest;->onError(ILjava/lang/Object;)V

    .line 338
    invoke-virtual {v8}, Lcom/sec/phone/RILRequest;->release()V

    .line 339
    iget-object v10, p0, Lcom/sec/phone/SecPhoneService$RILSender;->this$0:Lcom/sec/phone/SecPhoneService;

    iget v11, v10, Lcom/sec/phone/SecPhoneService;->mRequestMessagesPending:I

    add-int/lit8 v11, v11, -0x1

    iput v11, v10, Lcom/sec/phone/SecPhoneService;->mRequestMessagesPending:I

    .line 340
    const/4 v1, 0x1

    .line 426
    .end local v1    # "alreadySubtracted":Z
    .end local v9    # "s":Landroid/net/LocalSocket;
    :cond_0
    :goto_0
    return-void

    .line 344
    .restart local v1    # "alreadySubtracted":Z
    .restart local v9    # "s":Landroid/net/LocalSocket;
    :cond_1
    iget-object v10, p0, Lcom/sec/phone/SecPhoneService$RILSender;->this$0:Lcom/sec/phone/SecPhoneService;

    iget-object v11, v10, Lcom/sec/phone/SecPhoneService;->mRequestsList:Ljava/util/ArrayList;

    monitor-enter v11
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_1

    .line 345
    :try_start_1
    iget-object v10, p0, Lcom/sec/phone/SecPhoneService$RILSender;->this$0:Lcom/sec/phone/SecPhoneService;

    iget-object v10, v10, Lcom/sec/phone/SecPhoneService;->mRequestsList:Ljava/util/ArrayList;

    invoke-virtual {v10, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 346
    monitor-exit v11
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 348
    :try_start_2
    iget-object v10, p0, Lcom/sec/phone/SecPhoneService$RILSender;->this$0:Lcom/sec/phone/SecPhoneService;

    iget v11, v10, Lcom/sec/phone/SecPhoneService;->mRequestMessagesPending:I

    add-int/lit8 v11, v11, -0x1

    iput v11, v10, Lcom/sec/phone/SecPhoneService;->mRequestMessagesPending:I

    .line 349
    const/4 v1, 0x1

    .line 353
    iget-object v10, v8, Lcom/sec/phone/RILRequest;->mp:Landroid/os/Parcel;

    invoke-virtual {v10}, Landroid/os/Parcel;->marshall()[B

    move-result-object v3

    .line 354
    .local v3, "data":[B
    iget-object v10, v8, Lcom/sec/phone/RILRequest;->mp:Landroid/os/Parcel;

    invoke-virtual {v10}, Landroid/os/Parcel;->recycle()V

    .line 355
    const/4 v10, 0x0

    iput-object v10, v8, Lcom/sec/phone/RILRequest;->mp:Landroid/os/Parcel;

    .line 357
    array-length v10, v3

    const/16 v11, 0x2000

    if-le v10, v11, :cond_5

    .line 358
    new-instance v10, Ljava/lang/RuntimeException;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Parcel larger than max bytes allowed! "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    array-length v12, v3

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v10
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_1

    .line 372
    .end local v3    # "data":[B
    .end local v9    # "s":Landroid/net/LocalSocket;
    :catch_0
    move-exception v4

    .line 373
    .local v4, "ex":Ljava/io/IOException;
    iget-object v10, p0, Lcom/sec/phone/SecPhoneService$RILSender;->this$0:Lcom/sec/phone/SecPhoneService;

    iget-object v10, v10, Lcom/sec/phone/SecPhoneService;->LOG_TAG:Ljava/lang/String;

    const-string v11, "IOException "

    invoke-static {v10, v11, v4}, Landroid/telephony/Rlog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 374
    iget-object v10, p0, Lcom/sec/phone/SecPhoneService$RILSender;->this$0:Lcom/sec/phone/SecPhoneService;

    iget v11, v8, Lcom/sec/phone/RILRequest;->mSerial:I

    # invokes: Lcom/sec/phone/SecPhoneService;->findAndRemoveRequestFromList(I)Lcom/sec/phone/RILRequest;
    invoke-static {v10, v11}, Lcom/sec/phone/SecPhoneService;->access$000(Lcom/sec/phone/SecPhoneService;I)Lcom/sec/phone/RILRequest;

    move-result-object v7

    .line 377
    if-nez v7, :cond_2

    if-nez v1, :cond_3

    .line 378
    :cond_2
    const/4 v10, 0x1

    const/4 v11, 0x0

    invoke-virtual {v8, v10, v11}, Lcom/sec/phone/RILRequest;->onError(ILjava/lang/Object;)V

    .line 379
    invoke-virtual {v8}, Lcom/sec/phone/RILRequest;->release()V

    .line 392
    .end local v4    # "ex":Ljava/io/IOException;
    :cond_3
    :goto_1
    if-nez v1, :cond_0

    .line 393
    iget-object v10, p0, Lcom/sec/phone/SecPhoneService$RILSender;->this$0:Lcom/sec/phone/SecPhoneService;

    iget v11, v10, Lcom/sec/phone/SecPhoneService;->mRequestMessagesPending:I

    add-int/lit8 v11, v11, -0x1

    iput v11, v10, Lcom/sec/phone/SecPhoneService;->mRequestMessagesPending:I

    goto :goto_0

    .line 346
    .restart local v9    # "s":Landroid/net/LocalSocket;
    :catchall_0
    move-exception v10

    :try_start_3
    monitor-exit v11
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v10
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_4 .. :try_end_4} :catch_1

    .line 381
    .end local v9    # "s":Landroid/net/LocalSocket;
    :catch_1
    move-exception v5

    .line 382
    .local v5, "exc":Ljava/lang/RuntimeException;
    iget-object v10, p0, Lcom/sec/phone/SecPhoneService$RILSender;->this$0:Lcom/sec/phone/SecPhoneService;

    iget-object v10, v10, Lcom/sec/phone/SecPhoneService;->LOG_TAG:Ljava/lang/String;

    const-string v11, "Uncaught exception "

    invoke-static {v10, v11, v5}, Landroid/telephony/Rlog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 383
    iget-object v10, p0, Lcom/sec/phone/SecPhoneService$RILSender;->this$0:Lcom/sec/phone/SecPhoneService;

    iget v11, v8, Lcom/sec/phone/RILRequest;->mSerial:I

    # invokes: Lcom/sec/phone/SecPhoneService;->findAndRemoveRequestFromList(I)Lcom/sec/phone/RILRequest;
    invoke-static {v10, v11}, Lcom/sec/phone/SecPhoneService;->access$000(Lcom/sec/phone/SecPhoneService;I)Lcom/sec/phone/RILRequest;

    move-result-object v7

    .line 386
    if-nez v7, :cond_4

    if-nez v1, :cond_3

    .line 387
    :cond_4
    const/4 v10, 0x2

    const/4 v11, 0x0

    invoke-virtual {v8, v10, v11}, Lcom/sec/phone/RILRequest;->onError(ILjava/lang/Object;)V

    .line 388
    invoke-virtual {v8}, Lcom/sec/phone/RILRequest;->release()V

    goto :goto_1

    .line 363
    .end local v5    # "exc":Ljava/lang/RuntimeException;
    .restart local v3    # "data":[B
    .restart local v9    # "s":Landroid/net/LocalSocket;
    :cond_5
    :try_start_5
    iget-object v10, p0, Lcom/sec/phone/SecPhoneService$RILSender;->dataLength:[B

    const/4 v11, 0x0

    iget-object v12, p0, Lcom/sec/phone/SecPhoneService$RILSender;->dataLength:[B

    const/4 v13, 0x1

    const/4 v14, 0x0

    aput-byte v14, v12, v13

    aput-byte v14, v10, v11

    .line 364
    iget-object v10, p0, Lcom/sec/phone/SecPhoneService$RILSender;->dataLength:[B

    const/4 v11, 0x2

    array-length v12, v3

    shr-int/lit8 v12, v12, 0x8

    and-int/lit16 v12, v12, 0xff

    int-to-byte v12, v12

    aput-byte v12, v10, v11

    .line 365
    iget-object v10, p0, Lcom/sec/phone/SecPhoneService$RILSender;->dataLength:[B

    const/4 v11, 0x3

    array-length v12, v3

    and-int/lit16 v12, v12, 0xff

    int-to-byte v12, v12

    aput-byte v12, v10, v11

    .line 370
    invoke-virtual {v9}, Landroid/net/LocalSocket;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v10

    iget-object v11, p0, Lcom/sec/phone/SecPhoneService$RILSender;->dataLength:[B

    invoke-virtual {v10, v11}, Ljava/io/OutputStream;->write([B)V

    .line 371
    invoke-virtual {v9}, Landroid/net/LocalSocket;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v10

    invoke-virtual {v10, v3}, Ljava/io/OutputStream;->write([B)V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_5 .. :try_end_5} :catch_1

    goto :goto_1

    .line 402
    .end local v1    # "alreadySubtracted":Z
    .end local v3    # "data":[B
    .end local v9    # "s":Landroid/net/LocalSocket;
    :pswitch_1
    iget-object v10, p0, Lcom/sec/phone/SecPhoneService$RILSender;->this$0:Lcom/sec/phone/SecPhoneService;

    iget-object v11, v10, Lcom/sec/phone/SecPhoneService;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    monitor-enter v11

    .line 403
    :try_start_6
    iget-object v10, p0, Lcom/sec/phone/SecPhoneService$RILSender;->this$0:Lcom/sec/phone/SecPhoneService;

    iget-object v10, v10, Lcom/sec/phone/SecPhoneService;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v10}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v10

    if-eqz v10, :cond_7

    .line 404
    iget-object v10, p0, Lcom/sec/phone/SecPhoneService$RILSender;->this$0:Lcom/sec/phone/SecPhoneService;

    iget-object v12, v10, Lcom/sec/phone/SecPhoneService;->mRequestsList:Ljava/util/ArrayList;

    monitor-enter v12
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 405
    :try_start_7
    iget-object v10, p0, Lcom/sec/phone/SecPhoneService$RILSender;->this$0:Lcom/sec/phone/SecPhoneService;

    iget-object v10, v10, Lcom/sec/phone/SecPhoneService;->mRequestsList:Ljava/util/ArrayList;

    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 406
    .local v2, "count":I
    iget-object v10, p0, Lcom/sec/phone/SecPhoneService$RILSender;->this$0:Lcom/sec/phone/SecPhoneService;

    iget-object v10, v10, Lcom/sec/phone/SecPhoneService;->LOG_TAG:Ljava/lang/String;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "WAKE_LOCK_TIMEOUT  mReqPending="

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    iget-object v14, p0, Lcom/sec/phone/SecPhoneService$RILSender;->this$0:Lcom/sec/phone/SecPhoneService;

    iget v14, v14, Lcom/sec/phone/SecPhoneService;->mRequestMessagesPending:I

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, " mRequestList="

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v10, v13}, Landroid/telephony/Rlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 409
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_2
    if-ge v6, v2, :cond_6

    .line 410
    iget-object v10, p0, Lcom/sec/phone/SecPhoneService$RILSender;->this$0:Lcom/sec/phone/SecPhoneService;

    iget-object v10, v10, Lcom/sec/phone/SecPhoneService;->mRequestsList:Ljava/util/ArrayList;

    invoke-virtual {v10, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    move-object v0, v10

    check-cast v0, Lcom/sec/phone/RILRequest;

    move-object v8, v0

    .line 411
    iget-object v10, p0, Lcom/sec/phone/SecPhoneService$RILSender;->this$0:Lcom/sec/phone/SecPhoneService;

    iget-object v10, v10, Lcom/sec/phone/SecPhoneService;->LOG_TAG:Ljava/lang/String;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v13, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, ": ["

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    iget v14, v8, Lcom/sec/phone/RILRequest;->mSerial:I

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "] "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    iget v14, v8, Lcom/sec/phone/RILRequest;->mRequest:I

    invoke-static {v14}, Lcom/sec/phone/SecPhoneService;->requestToString(I)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v10, v13}, Landroid/telephony/Rlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 409
    add-int/lit8 v6, v6, 0x1

    goto :goto_2

    .line 415
    :cond_6
    monitor-exit v12
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 416
    :try_start_8
    iget-object v10, p0, Lcom/sec/phone/SecPhoneService$RILSender;->this$0:Lcom/sec/phone/SecPhoneService;

    iget-object v10, v10, Lcom/sec/phone/SecPhoneService;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v10}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 418
    .end local v2    # "count":I
    .end local v6    # "i":I
    :cond_7
    monitor-exit v11

    goto/16 :goto_0

    :catchall_1
    move-exception v10

    monitor-exit v11
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    throw v10

    .line 415
    :catchall_2
    move-exception v10

    :try_start_9
    monitor-exit v12
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_2

    :try_start_a
    throw v10
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_1

    .line 323
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public run()V
    .locals 0

    .prologue
    .line 315
    return-void
.end method
