.class Lcom/sec/phone/SecPhoneService$1;
.super Landroid/os/Handler;
.source "SecPhoneService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/phone/SecPhoneService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/phone/SecPhoneService;


# direct methods
.method constructor <init>(Lcom/sec/phone/SecPhoneService;)V
    .locals 0

    .prologue
    .line 127
    iput-object p1, p0, Lcom/sec/phone/SecPhoneService$1;->this$0:Lcom/sec/phone/SecPhoneService;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 6
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 130
    iget-object v3, p0, Lcom/sec/phone/SecPhoneService$1;->this$0:Lcom/sec/phone/SecPhoneService;

    iget-object v3, v3, Lcom/sec/phone/SecPhoneService;->LOG_TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "handleMessage(): "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/telephony/Rlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 132
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v1

    .line 133
    .local v1, "reply":Landroid/os/Message;
    invoke-virtual {v1, p1}, Landroid/os/Message;->copyFrom(Landroid/os/Message;)V

    .line 135
    iget-object v3, p0, Lcom/sec/phone/SecPhoneService$1;->this$0:Lcom/sec/phone/SecPhoneService;

    iget-object v3, v3, Lcom/sec/phone/SecPhoneService;->mSocket:Landroid/net/LocalSocket;

    if-eqz v3, :cond_3

    .line 136
    iget-object v3, p0, Lcom/sec/phone/SecPhoneService$1;->this$0:Lcom/sec/phone/SecPhoneService;

    iget-object v3, v3, Lcom/sec/phone/SecPhoneService;->mSocket:Landroid/net/LocalSocket;

    invoke-virtual {v3}, Landroid/net/LocalSocket;->isConnected()Z

    move-result v3

    if-nez v3, :cond_0

    .line 137
    iget-object v3, p0, Lcom/sec/phone/SecPhoneService$1;->this$0:Lcom/sec/phone/SecPhoneService;

    iget-object v3, v3, Lcom/sec/phone/SecPhoneService;->LOG_TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Queuing Message : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/telephony/Rlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 138
    iget-object v3, p0, Lcom/sec/phone/SecPhoneService$1;->this$0:Lcom/sec/phone/SecPhoneService;

    iget-object v3, v3, Lcom/sec/phone/SecPhoneService;->mQueuingMsg:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 160
    :goto_0
    return-void

    .line 140
    :cond_0
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v3

    const-string v4, "Action"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 142
    .local v0, "action":Ljava/lang/String;
    if-eqz v0, :cond_2

    .line 143
    iget-object v3, p0, Lcom/sec/phone/SecPhoneService$1;->this$0:Lcom/sec/phone/SecPhoneService;

    iget-object v3, v3, Lcom/sec/phone/SecPhoneService;->LOG_TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Messenger Name : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/telephony/Rlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 145
    iget-object v3, p0, Lcom/sec/phone/SecPhoneService$1;->this$0:Lcom/sec/phone/SecPhoneService;

    iget-object v3, v3, Lcom/sec/phone/SecPhoneService;->mRegisteredSender:Ljava/util/Map;

    invoke-interface {v3, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/os/Message;

    .line 146
    .local v2, "temp":Landroid/os/Message;
    if-nez v2, :cond_1

    .line 147
    iget-object v3, p0, Lcom/sec/phone/SecPhoneService$1;->this$0:Lcom/sec/phone/SecPhoneService;

    iget-object v3, v3, Lcom/sec/phone/SecPhoneService;->mRegisteredSender:Ljava/util/Map;

    invoke-interface {v3, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 149
    :cond_1
    iget-object v3, p0, Lcom/sec/phone/SecPhoneService$1;->this$0:Lcom/sec/phone/SecPhoneService;

    iget-object v3, v3, Lcom/sec/phone/SecPhoneService;->LOG_TAG:Ljava/lang/String;

    const-string v4, "action is already exist. data will be updated"

    invoke-static {v3, v4}, Landroid/telephony/Rlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 150
    iget-object v3, p0, Lcom/sec/phone/SecPhoneService$1;->this$0:Lcom/sec/phone/SecPhoneService;

    iget-object v3, v3, Lcom/sec/phone/SecPhoneService;->mRegisteredSender:Ljava/util/Map;

    invoke-interface {v3, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 151
    iget-object v3, p0, Lcom/sec/phone/SecPhoneService$1;->this$0:Lcom/sec/phone/SecPhoneService;

    iget-object v3, v3, Lcom/sec/phone/SecPhoneService;->mRegisteredSender:Ljava/util/Map;

    invoke-interface {v3, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 154
    .end local v2    # "temp":Landroid/os/Message;
    :cond_2
    iget-object v3, p0, Lcom/sec/phone/SecPhoneService$1;->this$0:Lcom/sec/phone/SecPhoneService;

    invoke-virtual {v3, v1}, Lcom/sec/phone/SecPhoneService;->invokeOemRilRequestRaw(Landroid/os/Message;)V

    goto :goto_0

    .line 158
    .end local v0    # "action":Ljava/lang/String;
    :cond_3
    iget-object v3, p0, Lcom/sec/phone/SecPhoneService$1;->this$0:Lcom/sec/phone/SecPhoneService;

    iget-object v3, v3, Lcom/sec/phone/SecPhoneService;->LOG_TAG:Ljava/lang/String;

    const-string v4, "socket connection is broken"

    invoke-static {v3, v4}, Landroid/telephony/Rlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
