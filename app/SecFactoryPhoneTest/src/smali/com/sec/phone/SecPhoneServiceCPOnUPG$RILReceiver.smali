.class Lcom/sec/phone/SecPhoneServiceCPOnUPG$RILReceiver;
.super Ljava/lang/Object;
.source "SecPhoneServiceCPOnUPG.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/phone/SecPhoneServiceCPOnUPG;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "RILReceiver"
.end annotation


# instance fields
.field buffer:[B

.field final synthetic this$0:Lcom/sec/phone/SecPhoneServiceCPOnUPG;


# direct methods
.method constructor <init>(Lcom/sec/phone/SecPhoneServiceCPOnUPG;)V
    .locals 1

    .prologue
    .line 533
    iput-object p1, p0, Lcom/sec/phone/SecPhoneServiceCPOnUPG$RILReceiver;->this$0:Lcom/sec/phone/SecPhoneServiceCPOnUPG;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 534
    const/16 v0, 0x2000

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/sec/phone/SecPhoneServiceCPOnUPG$RILReceiver;->buffer:[B

    .line 535
    return-void
.end method


# virtual methods
.method public WaitForSecRilReady()V
    .locals 8

    .prologue
    .line 538
    const-string v2, ""

    .line 539
    .local v2, "swVer":Ljava/lang/String;
    const/16 v1, 0x28

    .line 542
    .local v1, "retryCount":I
    :goto_0
    if-lez v1, :cond_1

    .line 543
    const-string v3, "ril.sw_ver"

    const-string v4, "NONE"

    invoke-static {v3, v4}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 544
    iget-object v3, p0, Lcom/sec/phone/SecPhoneServiceCPOnUPG$RILReceiver;->this$0:Lcom/sec/phone/SecPhoneServiceCPOnUPG;

    iget-object v3, v3, Lcom/sec/phone/SecPhoneServiceCPOnUPG;->LOG_TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "swVer : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/telephony/Rlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 546
    const-string v3, "NONE"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 549
    const-wide/16 v4, 0x1f4

    :try_start_0
    invoke-static {v4, v5}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 554
    :goto_1
    add-int/lit8 v1, v1, -0x1

    goto :goto_0

    .line 550
    :catch_0
    move-exception v0

    .line 551
    .local v0, "er":Ljava/lang/InterruptedException;
    iget-object v3, p0, Lcom/sec/phone/SecPhoneServiceCPOnUPG$RILReceiver;->this$0:Lcom/sec/phone/SecPhoneServiceCPOnUPG;

    iget-object v3, v3, Lcom/sec/phone/SecPhoneServiceCPOnUPG;->LOG_TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Thread interrupted...start to exit - "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/phone/SecPhoneServiceCPOnUPG$RILReceiver;->this$0:Lcom/sec/phone/SecPhoneServiceCPOnUPG;

    iget-object v5, v5, Lcom/sec/phone/SecPhoneServiceCPOnUPG;->RilSocketName:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", TID: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/phone/SecPhoneServiceCPOnUPG$RILReceiver;->this$0:Lcom/sec/phone/SecPhoneServiceCPOnUPG;

    iget-object v5, v5, Lcom/sec/phone/SecPhoneServiceCPOnUPG;->mReceiverThread:Ljava/lang/Thread;

    invoke-virtual {v5}, Ljava/lang/Thread;->getId()J

    move-result-wide v6

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/telephony/Rlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 556
    .end local v0    # "er":Ljava/lang/InterruptedException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    .line 560
    :cond_1
    iget-object v3, p0, Lcom/sec/phone/SecPhoneServiceCPOnUPG$RILReceiver;->this$0:Lcom/sec/phone/SecPhoneServiceCPOnUPG;

    iget-object v3, v3, Lcom/sec/phone/SecPhoneServiceCPOnUPG;->LOG_TAG:Ljava/lang/String;

    const-string v4, "Secril is ready"

    invoke-static {v3, v4}, Landroid/telephony/Rlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 561
    return-void
.end method

.method public run()V
    .locals 18

    .prologue
    .line 564
    const/4 v9, 0x0

    .line 571
    .local v9, "retryCount":I
    :goto_0
    :try_start_0
    invoke-static {}, Ljava/lang/Thread;->interrupted()Z

    move-result v13

    if-eqz v13, :cond_0

    .line 572
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/phone/SecPhoneServiceCPOnUPG$RILReceiver;->this$0:Lcom/sec/phone/SecPhoneServiceCPOnUPG;

    iget-object v13, v13, Lcom/sec/phone/SecPhoneServiceCPOnUPG;->LOG_TAG:Ljava/lang/String;

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "Thread interrupted...start to exit - "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/phone/SecPhoneServiceCPOnUPG$RILReceiver;->this$0:Lcom/sec/phone/SecPhoneServiceCPOnUPG;

    iget-object v15, v15, Lcom/sec/phone/SecPhoneServiceCPOnUPG;->RilSocketName:Ljava/lang/String;

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, ", TID: "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/phone/SecPhoneServiceCPOnUPG$RILReceiver;->this$0:Lcom/sec/phone/SecPhoneServiceCPOnUPG;

    iget-object v15, v15, Lcom/sec/phone/SecPhoneServiceCPOnUPG;->mReceiverThread:Ljava/lang/Thread;

    invoke-virtual {v15}, Ljava/lang/Thread;->getId()J

    move-result-wide v16

    move-wide/from16 v0, v16

    invoke-virtual {v14, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Landroid/telephony/Rlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 574
    new-instance v13, Ljava/lang/InterruptedException;

    invoke-direct {v13}, Ljava/lang/InterruptedException;-><init>()V

    throw v13
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_1

    .line 688
    :catch_0
    move-exception v2

    .line 691
    .local v2, "er":Ljava/lang/InterruptedException;
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/phone/SecPhoneServiceCPOnUPG$RILReceiver;->this$0:Lcom/sec/phone/SecPhoneServiceCPOnUPG;

    iget-object v13, v13, Lcom/sec/phone/SecPhoneServiceCPOnUPG;->LOG_TAG:Ljava/lang/String;

    const-string v14, "Thread is terminated by InterruptedException"

    invoke-static {v13, v14}, Landroid/telephony/Rlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 697
    .end local v2    # "er":Ljava/lang/InterruptedException;
    :goto_1
    return-void

    .line 579
    :cond_0
    :try_start_1
    invoke-virtual/range {p0 .. p0}, Lcom/sec/phone/SecPhoneServiceCPOnUPG$RILReceiver;->WaitForSecRilReady()V

    .line 581
    # getter for: Lcom/sec/phone/SecPhoneServiceCPOnUPG;->isMarvell:Z
    invoke-static {}, Lcom/sec/phone/SecPhoneServiceCPOnUPG;->access$400()Z

    move-result v13

    if-eqz v13, :cond_1

    .line 582
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/phone/SecPhoneServiceCPOnUPG$RILReceiver;->this$0:Lcom/sec/phone/SecPhoneServiceCPOnUPG;

    iget-object v13, v13, Lcom/sec/phone/SecPhoneServiceCPOnUPG;->LOG_TAG:Ljava/lang/String;

    const-string v14, "There is no SecRil in Marvell platform. Just send SecPhoneReady intent and return!"

    invoke-static {v13, v14}, Landroid/telephony/Rlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 584
    new-instance v4, Landroid/content/Intent;

    const-string v13, "android.intent.action.SECPHONE_READY"

    invoke-direct {v4, v13}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 585
    .local v4, "i":Landroid/content/Intent;
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/phone/SecPhoneServiceCPOnUPG$RILReceiver;->this$0:Lcom/sec/phone/SecPhoneServiceCPOnUPG;

    invoke-virtual {v13, v4}, Lcom/sec/phone/SecPhoneServiceCPOnUPG;->sendBroadcast(Landroid/content/Intent;)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    .line 694
    .end local v4    # "i":Landroid/content/Intent;
    :catch_1
    move-exception v12

    .line 695
    .local v12, "tr":Ljava/lang/Throwable;
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/phone/SecPhoneServiceCPOnUPG$RILReceiver;->this$0:Lcom/sec/phone/SecPhoneServiceCPOnUPG;

    iget-object v13, v13, Lcom/sec/phone/SecPhoneServiceCPOnUPG;->LOG_TAG:Ljava/lang/String;

    const-string v14, "Uncaught exception "

    invoke-static {v13, v14, v12}, Landroid/telephony/Rlog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1

    .line 589
    .end local v12    # "tr":Ljava/lang/Throwable;
    :cond_1
    :try_start_2
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/phone/SecPhoneServiceCPOnUPG$RILReceiver;->this$0:Lcom/sec/phone/SecPhoneServiceCPOnUPG;

    iget-object v13, v13, Lcom/sec/phone/SecPhoneServiceCPOnUPG;->LOG_TAG:Ljava/lang/String;

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "start connect to \'"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/phone/SecPhoneServiceCPOnUPG$RILReceiver;->this$0:Lcom/sec/phone/SecPhoneServiceCPOnUPG;

    iget-object v15, v15, Lcom/sec/phone/SecPhoneServiceCPOnUPG;->RilSocketName:Ljava/lang/String;

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, "\' socket"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, ", TID: "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/phone/SecPhoneServiceCPOnUPG$RILReceiver;->this$0:Lcom/sec/phone/SecPhoneServiceCPOnUPG;

    iget-object v15, v15, Lcom/sec/phone/SecPhoneServiceCPOnUPG;->mReceiverThread:Ljava/lang/Thread;

    invoke-virtual {v15}, Ljava/lang/Thread;->getId()J

    move-result-wide v16

    move-wide/from16 v0, v16

    invoke-virtual {v14, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Landroid/telephony/Rlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 591
    new-instance v7, Landroid/net/LocalSocketAddress;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/phone/SecPhoneServiceCPOnUPG$RILReceiver;->this$0:Lcom/sec/phone/SecPhoneServiceCPOnUPG;

    iget-object v13, v13, Lcom/sec/phone/SecPhoneServiceCPOnUPG;->RilSocketName:Ljava/lang/String;

    invoke-direct {v7, v13}, Landroid/net/LocalSocketAddress;-><init>(Ljava/lang/String;)V

    .line 593
    .local v7, "lsa":Landroid/net/LocalSocketAddress;
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/phone/SecPhoneServiceCPOnUPG$RILReceiver;->this$0:Lcom/sec/phone/SecPhoneServiceCPOnUPG;

    iget-object v13, v13, Lcom/sec/phone/SecPhoneServiceCPOnUPG;->mSocket:Landroid/net/LocalSocket;

    if-nez v13, :cond_2

    .line 594
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/phone/SecPhoneServiceCPOnUPG$RILReceiver;->this$0:Lcom/sec/phone/SecPhoneServiceCPOnUPG;

    new-instance v14, Landroid/net/LocalSocket;

    invoke-direct {v14}, Landroid/net/LocalSocket;-><init>()V

    iput-object v14, v13, Lcom/sec/phone/SecPhoneServiceCPOnUPG;->mSocket:Landroid/net/LocalSocket;
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1

    .line 597
    :cond_2
    :try_start_3
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/phone/SecPhoneServiceCPOnUPG$RILReceiver;->this$0:Lcom/sec/phone/SecPhoneServiceCPOnUPG;

    iget-object v13, v13, Lcom/sec/phone/SecPhoneServiceCPOnUPG;->mSocket:Landroid/net/LocalSocket;

    invoke-virtual {v13, v7}, Landroid/net/LocalSocket;->connect(Landroid/net/LocalSocketAddress;)V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_1

    .line 630
    const/4 v9, 0x0

    .line 632
    :try_start_4
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/phone/SecPhoneServiceCPOnUPG$RILReceiver;->this$0:Lcom/sec/phone/SecPhoneServiceCPOnUPG;

    const-string v14, "android.intent.action.SECPHONE_READY"

    # invokes: Lcom/sec/phone/SecPhoneServiceCPOnUPG;->sendIntentSecPhoneReady(Ljava/lang/String;)V
    invoke-static {v13, v14}, Lcom/sec/phone/SecPhoneServiceCPOnUPG;->access$500(Lcom/sec/phone/SecPhoneServiceCPOnUPG;Ljava/lang/String;)V
    :try_end_4
    .catch Ljava/lang/InterruptedException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_1

    .line 634
    const/4 v6, 0x0

    .line 636
    .local v6, "length":I
    :try_start_5
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/phone/SecPhoneServiceCPOnUPG$RILReceiver;->this$0:Lcom/sec/phone/SecPhoneServiceCPOnUPG;

    iget-object v13, v13, Lcom/sec/phone/SecPhoneServiceCPOnUPG;->mSocket:Landroid/net/LocalSocket;

    invoke-virtual {v13}, Landroid/net/LocalSocket;->getInputStream()Ljava/io/InputStream;

    move-result-object v5

    .line 641
    .local v5, "is":Ljava/io/InputStream;
    :goto_2
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/phone/SecPhoneServiceCPOnUPG$RILReceiver;->this$0:Lcom/sec/phone/SecPhoneServiceCPOnUPG;

    iget-object v13, v13, Lcom/sec/phone/SecPhoneServiceCPOnUPG;->LOG_TAG:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/phone/SecPhoneServiceCPOnUPG$RILReceiver;->buffer:[B

    # invokes: Lcom/sec/phone/SecPhoneServiceCPOnUPG;->readRilMessage(Ljava/lang/String;Ljava/io/InputStream;[B)I
    invoke-static {v13, v5, v14}, Lcom/sec/phone/SecPhoneServiceCPOnUPG;->access$600(Ljava/lang/String;Ljava/io/InputStream;[B)I
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_4
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_5} :catch_5
    .catch Ljava/lang/InterruptedException; {:try_start_5 .. :try_end_5} :catch_0

    move-result v6

    .line 643
    if-gez v6, :cond_6

    .line 664
    .end local v5    # "is":Ljava/io/InputStream;
    :goto_3
    :try_start_6
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/phone/SecPhoneServiceCPOnUPG$RILReceiver;->this$0:Lcom/sec/phone/SecPhoneServiceCPOnUPG;

    iget-object v13, v13, Lcom/sec/phone/SecPhoneServiceCPOnUPG;->LOG_TAG:Ljava/lang/String;

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "Disconnected from \'"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/phone/SecPhoneServiceCPOnUPG$RILReceiver;->this$0:Lcom/sec/phone/SecPhoneServiceCPOnUPG;

    iget-object v15, v15, Lcom/sec/phone/SecPhoneServiceCPOnUPG;->RilSocketName:Ljava/lang/String;

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, "\' socket"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Landroid/telephony/Rlog;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_6
    .catch Ljava/lang/InterruptedException; {:try_start_6 .. :try_end_6} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_6 .. :try_end_6} :catch_1

    .line 667
    :try_start_7
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/phone/SecPhoneServiceCPOnUPG$RILReceiver;->this$0:Lcom/sec/phone/SecPhoneServiceCPOnUPG;

    iget-object v13, v13, Lcom/sec/phone/SecPhoneServiceCPOnUPG;->mSocket:Landroid/net/LocalSocket;

    invoke-virtual {v13}, Landroid/net/LocalSocket;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_6
    .catch Ljava/lang/InterruptedException; {:try_start_7 .. :try_end_7} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_7 .. :try_end_7} :catch_1

    .line 671
    :goto_4
    :try_start_8
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/phone/SecPhoneServiceCPOnUPG$RILReceiver;->this$0:Lcom/sec/phone/SecPhoneServiceCPOnUPG;

    const/4 v14, 0x0

    iput-object v14, v13, Lcom/sec/phone/SecPhoneServiceCPOnUPG;->mSocket:Landroid/net/LocalSocket;

    .line 672
    invoke-static {}, Lcom/sec/phone/RILRequest;->resetSerial()V

    .line 675
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/phone/SecPhoneServiceCPOnUPG$RILReceiver;->this$0:Lcom/sec/phone/SecPhoneServiceCPOnUPG;

    iget-object v14, v13, Lcom/sec/phone/SecPhoneServiceCPOnUPG;->mRequestsList:Ljava/util/ArrayList;

    monitor-enter v14
    :try_end_8
    .catch Ljava/lang/InterruptedException; {:try_start_8 .. :try_end_8} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_8 .. :try_end_8} :catch_1

    .line 676
    const/4 v4, 0x0

    .local v4, "i":I
    :try_start_9
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/phone/SecPhoneServiceCPOnUPG$RILReceiver;->this$0:Lcom/sec/phone/SecPhoneServiceCPOnUPG;

    iget-object v13, v13, Lcom/sec/phone/SecPhoneServiceCPOnUPG;->mRequestsList:Ljava/util/ArrayList;

    invoke-virtual {v13}, Ljava/util/ArrayList;->size()I

    move-result v11

    .local v11, "sz":I
    :goto_5
    if-ge v4, v11, :cond_7

    .line 677
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/phone/SecPhoneServiceCPOnUPG$RILReceiver;->this$0:Lcom/sec/phone/SecPhoneServiceCPOnUPG;

    iget-object v13, v13, Lcom/sec/phone/SecPhoneServiceCPOnUPG;->mRequestsList:Ljava/util/ArrayList;

    invoke-virtual {v13, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/sec/phone/RILRequest;

    .line 678
    .local v10, "rr":Lcom/sec/phone/RILRequest;
    const/4 v13, 0x1

    const/4 v15, 0x0

    invoke-virtual {v10, v13, v15}, Lcom/sec/phone/RILRequest;->onError(ILjava/lang/Object;)V

    .line 679
    invoke-virtual {v10}, Lcom/sec/phone/RILRequest;->release()V
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    .line 676
    add-int/lit8 v4, v4, 0x1

    goto :goto_5

    .line 598
    .end local v4    # "i":I
    .end local v6    # "length":I
    .end local v10    # "rr":Lcom/sec/phone/RILRequest;
    .end local v11    # "sz":I
    :catch_2
    move-exception v3

    .line 599
    .local v3, "ex":Ljava/io/IOException;
    :try_start_a
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/phone/SecPhoneServiceCPOnUPG$RILReceiver;->this$0:Lcom/sec/phone/SecPhoneServiceCPOnUPG;

    iget-object v13, v13, Lcom/sec/phone/SecPhoneServiceCPOnUPG;->mSocket:Landroid/net/LocalSocket;

    invoke-virtual {v13}, Landroid/net/LocalSocket;->isConnected()Z

    move-result v13

    if-nez v13, :cond_3

    .line 606
    :cond_3
    const/16 v13, 0x8

    if-ne v9, v13, :cond_5

    .line 607
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/phone/SecPhoneServiceCPOnUPG$RILReceiver;->this$0:Lcom/sec/phone/SecPhoneServiceCPOnUPG;

    iget-object v13, v13, Lcom/sec/phone/SecPhoneServiceCPOnUPG;->LOG_TAG:Ljava/lang/String;

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "Couldn\'t find \'"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/phone/SecPhoneServiceCPOnUPG$RILReceiver;->this$0:Lcom/sec/phone/SecPhoneServiceCPOnUPG;

    iget-object v15, v15, Lcom/sec/phone/SecPhoneServiceCPOnUPG;->RilSocketName:Ljava/lang/String;

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, "\' socket after "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, " times, continuing to retry silently"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Landroid/telephony/Rlog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_a
    .catch Ljava/lang/InterruptedException; {:try_start_a .. :try_end_a} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_a .. :try_end_a} :catch_1

    .line 615
    :cond_4
    :goto_6
    const-wide/16 v14, 0xfa0

    :try_start_b
    invoke-static {v14, v15}, Ljava/lang/Thread;->sleep(J)V
    :try_end_b
    .catch Ljava/lang/InterruptedException; {:try_start_b .. :try_end_b} :catch_3
    .catch Ljava/lang/Throwable; {:try_start_b .. :try_end_b} :catch_1

    .line 626
    add-int/lit8 v9, v9, 0x1

    .line 627
    goto/16 :goto_0

    .line 609
    :cond_5
    if-lez v9, :cond_4

    const/16 v13, 0x8

    if-ge v9, v13, :cond_4

    .line 610
    :try_start_c
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/phone/SecPhoneServiceCPOnUPG$RILReceiver;->this$0:Lcom/sec/phone/SecPhoneServiceCPOnUPG;

    iget-object v13, v13, Lcom/sec/phone/SecPhoneServiceCPOnUPG;->LOG_TAG:Ljava/lang/String;

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "Couldn\'t find \'"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/phone/SecPhoneServiceCPOnUPG$RILReceiver;->this$0:Lcom/sec/phone/SecPhoneServiceCPOnUPG;

    iget-object v15, v15, Lcom/sec/phone/SecPhoneServiceCPOnUPG;->RilSocketName:Ljava/lang/String;

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, "\' socket; retrying after timeout"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Landroid/telephony/Rlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_6

    .line 616
    :catch_3
    move-exception v2

    .line 619
    .restart local v2    # "er":Ljava/lang/InterruptedException;
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/phone/SecPhoneServiceCPOnUPG$RILReceiver;->this$0:Lcom/sec/phone/SecPhoneServiceCPOnUPG;

    iget-object v13, v13, Lcom/sec/phone/SecPhoneServiceCPOnUPG;->LOG_TAG:Ljava/lang/String;

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "Thread interrupted...start to exit - "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/phone/SecPhoneServiceCPOnUPG$RILReceiver;->this$0:Lcom/sec/phone/SecPhoneServiceCPOnUPG;

    iget-object v15, v15, Lcom/sec/phone/SecPhoneServiceCPOnUPG;->RilSocketName:Ljava/lang/String;

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, ", TID: "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/phone/SecPhoneServiceCPOnUPG$RILReceiver;->this$0:Lcom/sec/phone/SecPhoneServiceCPOnUPG;

    iget-object v15, v15, Lcom/sec/phone/SecPhoneServiceCPOnUPG;->mReceiverThread:Ljava/lang/Thread;

    invoke-virtual {v15}, Ljava/lang/Thread;->getId()J

    move-result-wide v16

    move-wide/from16 v0, v16

    invoke-virtual {v14, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Landroid/telephony/Rlog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_c
    .catch Ljava/lang/InterruptedException; {:try_start_c .. :try_end_c} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_c .. :try_end_c} :catch_1

    goto/16 :goto_1

    .line 647
    .end local v2    # "er":Ljava/lang/InterruptedException;
    .end local v3    # "ex":Ljava/io/IOException;
    .restart local v5    # "is":Ljava/io/InputStream;
    .restart local v6    # "length":I
    :cond_6
    :try_start_d
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v8

    .line 648
    .local v8, "p":Landroid/os/Parcel;
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/phone/SecPhoneServiceCPOnUPG$RILReceiver;->buffer:[B

    const/4 v14, 0x0

    invoke-virtual {v8, v13, v14, v6}, Landroid/os/Parcel;->unmarshall([BII)V

    .line 649
    const/4 v13, 0x0

    invoke-virtual {v8, v13}, Landroid/os/Parcel;->setDataPosition(I)V

    .line 654
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/phone/SecPhoneServiceCPOnUPG$RILReceiver;->this$0:Lcom/sec/phone/SecPhoneServiceCPOnUPG;

    # invokes: Lcom/sec/phone/SecPhoneServiceCPOnUPG;->processResponse(Landroid/os/Parcel;)V
    invoke-static {v13, v8}, Lcom/sec/phone/SecPhoneServiceCPOnUPG;->access$700(Lcom/sec/phone/SecPhoneServiceCPOnUPG;Landroid/os/Parcel;)V

    .line 655
    invoke-virtual {v8}, Landroid/os/Parcel;->recycle()V
    :try_end_d
    .catch Ljava/io/IOException; {:try_start_d .. :try_end_d} :catch_4
    .catch Ljava/lang/Throwable; {:try_start_d .. :try_end_d} :catch_5
    .catch Ljava/lang/InterruptedException; {:try_start_d .. :try_end_d} :catch_0

    goto/16 :goto_2

    .line 657
    .end local v5    # "is":Ljava/io/InputStream;
    .end local v8    # "p":Landroid/os/Parcel;
    :catch_4
    move-exception v3

    .line 658
    .restart local v3    # "ex":Ljava/io/IOException;
    :try_start_e
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/phone/SecPhoneServiceCPOnUPG$RILReceiver;->this$0:Lcom/sec/phone/SecPhoneServiceCPOnUPG;

    iget-object v13, v13, Lcom/sec/phone/SecPhoneServiceCPOnUPG;->LOG_TAG:Ljava/lang/String;

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "\'"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/phone/SecPhoneServiceCPOnUPG$RILReceiver;->this$0:Lcom/sec/phone/SecPhoneServiceCPOnUPG;

    iget-object v15, v15, Lcom/sec/phone/SecPhoneServiceCPOnUPG;->RilSocketName:Ljava/lang/String;

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, "\' socket closed, "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14, v3}, Landroid/telephony/Rlog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_3

    .line 659
    .end local v3    # "ex":Ljava/io/IOException;
    :catch_5
    move-exception v12

    .line 660
    .restart local v12    # "tr":Ljava/lang/Throwable;
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/phone/SecPhoneServiceCPOnUPG$RILReceiver;->this$0:Lcom/sec/phone/SecPhoneServiceCPOnUPG;

    iget-object v13, v13, Lcom/sec/phone/SecPhoneServiceCPOnUPG;->LOG_TAG:Ljava/lang/String;

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "Uncaught exception read length="

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, ", Exception:"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v12}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14, v12}, Landroid/telephony/Rlog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_e
    .catch Ljava/lang/InterruptedException; {:try_start_e .. :try_end_e} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_e .. :try_end_e} :catch_1

    goto/16 :goto_3

    .line 682
    .end local v12    # "tr":Ljava/lang/Throwable;
    .restart local v4    # "i":I
    .restart local v11    # "sz":I
    :cond_7
    :try_start_f
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/phone/SecPhoneServiceCPOnUPG$RILReceiver;->this$0:Lcom/sec/phone/SecPhoneServiceCPOnUPG;

    iget-object v13, v13, Lcom/sec/phone/SecPhoneServiceCPOnUPG;->mRequestsList:Ljava/util/ArrayList;

    invoke-virtual {v13}, Ljava/util/ArrayList;->clear()V

    .line 683
    monitor-exit v14

    goto/16 :goto_0

    .end local v11    # "sz":I
    :catchall_0
    move-exception v13

    monitor-exit v14
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_0

    :try_start_10
    throw v13
    :try_end_10
    .catch Ljava/lang/InterruptedException; {:try_start_10 .. :try_end_10} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_10 .. :try_end_10} :catch_1

    .line 668
    .end local v4    # "i":I
    :catch_6
    move-exception v13

    goto/16 :goto_4
.end method
