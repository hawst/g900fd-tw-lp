.class Lcom/sec/phone/SecPhoneServiceCPOnUPG$RILSender;
.super Landroid/os/Handler;
.source "SecPhoneServiceCPOnUPG.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/phone/SecPhoneServiceCPOnUPG;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "RILSender"
.end annotation


# instance fields
.field dataLength:[B

.field final synthetic this$0:Lcom/sec/phone/SecPhoneServiceCPOnUPG;


# direct methods
.method public constructor <init>(Lcom/sec/phone/SecPhoneServiceCPOnUPG;Landroid/os/Looper;)V
    .locals 1
    .param p2, "looper"    # Landroid/os/Looper;

    .prologue
    .line 367
    iput-object p1, p0, Lcom/sec/phone/SecPhoneServiceCPOnUPG$RILSender;->this$0:Lcom/sec/phone/SecPhoneServiceCPOnUPG;

    .line 368
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 372
    const/4 v0, 0x4

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/sec/phone/SecPhoneServiceCPOnUPG$RILSender;->dataLength:[B

    .line 369
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 15
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 382
    move-object/from16 v0, p1

    iget-object v10, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v10, Lcom/sec/phone/RILRequest;

    move-object v8, v10

    check-cast v8, Lcom/sec/phone/RILRequest;

    .line 383
    .local v8, "rr":Lcom/sec/phone/RILRequest;
    const/4 v7, 0x0

    .line 385
    .local v7, "req":Lcom/sec/phone/RILRequest;
    move-object/from16 v0, p1

    iget v10, v0, Landroid/os/Message;->what:I

    packed-switch v10, :pswitch_data_0

    .line 484
    :cond_0
    :goto_0
    return-void

    .line 392
    :pswitch_0
    const/4 v1, 0x0

    .line 396
    .local v1, "alreadySubtracted":Z
    :try_start_0
    iget-object v10, p0, Lcom/sec/phone/SecPhoneServiceCPOnUPG$RILSender;->this$0:Lcom/sec/phone/SecPhoneServiceCPOnUPG;

    iget-object v9, v10, Lcom/sec/phone/SecPhoneServiceCPOnUPG;->mSocket:Landroid/net/LocalSocket;

    .line 398
    .local v9, "s":Landroid/net/LocalSocket;
    if-nez v9, :cond_1

    .line 399
    const/4 v10, 0x1

    const/4 v11, 0x0

    invoke-virtual {v8, v10, v11}, Lcom/sec/phone/RILRequest;->onError(ILjava/lang/Object;)V

    .line 400
    invoke-virtual {v8}, Lcom/sec/phone/RILRequest;->release()V

    .line 401
    iget-object v10, p0, Lcom/sec/phone/SecPhoneServiceCPOnUPG$RILSender;->this$0:Lcom/sec/phone/SecPhoneServiceCPOnUPG;

    iget v11, v10, Lcom/sec/phone/SecPhoneServiceCPOnUPG;->mRequestMessagesPending:I

    add-int/lit8 v11, v11, -0x1

    iput v11, v10, Lcom/sec/phone/SecPhoneServiceCPOnUPG;->mRequestMessagesPending:I

    .line 402
    const/4 v1, 0x1

    goto :goto_0

    .line 406
    :cond_1
    iget-object v10, p0, Lcom/sec/phone/SecPhoneServiceCPOnUPG$RILSender;->this$0:Lcom/sec/phone/SecPhoneServiceCPOnUPG;

    iget-object v11, v10, Lcom/sec/phone/SecPhoneServiceCPOnUPG;->mRequestsList:Ljava/util/ArrayList;

    monitor-enter v11
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_1

    .line 407
    :try_start_1
    iget-object v10, p0, Lcom/sec/phone/SecPhoneServiceCPOnUPG$RILSender;->this$0:Lcom/sec/phone/SecPhoneServiceCPOnUPG;

    iget-object v10, v10, Lcom/sec/phone/SecPhoneServiceCPOnUPG;->mRequestsList:Ljava/util/ArrayList;

    invoke-virtual {v10, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 408
    monitor-exit v11
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 410
    :try_start_2
    iget-object v10, p0, Lcom/sec/phone/SecPhoneServiceCPOnUPG$RILSender;->this$0:Lcom/sec/phone/SecPhoneServiceCPOnUPG;

    iget v11, v10, Lcom/sec/phone/SecPhoneServiceCPOnUPG;->mRequestMessagesPending:I

    add-int/lit8 v11, v11, -0x1

    iput v11, v10, Lcom/sec/phone/SecPhoneServiceCPOnUPG;->mRequestMessagesPending:I

    .line 411
    const/4 v1, 0x1

    .line 415
    iget-object v10, v8, Lcom/sec/phone/RILRequest;->mp:Landroid/os/Parcel;

    invoke-virtual {v10}, Landroid/os/Parcel;->marshall()[B

    move-result-object v3

    .line 416
    .local v3, "data":[B
    iget-object v10, v8, Lcom/sec/phone/RILRequest;->mp:Landroid/os/Parcel;

    invoke-virtual {v10}, Landroid/os/Parcel;->recycle()V

    .line 417
    const/4 v10, 0x0

    iput-object v10, v8, Lcom/sec/phone/RILRequest;->mp:Landroid/os/Parcel;

    .line 419
    array-length v10, v3

    const/16 v11, 0x2000

    if-le v10, v11, :cond_5

    .line 420
    new-instance v10, Ljava/lang/RuntimeException;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Parcel larger than max bytes allowed! "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    array-length v12, v3

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v10
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_1

    .line 434
    .end local v3    # "data":[B
    .end local v9    # "s":Landroid/net/LocalSocket;
    :catch_0
    move-exception v4

    .line 435
    .local v4, "ex":Ljava/io/IOException;
    iget-object v10, p0, Lcom/sec/phone/SecPhoneServiceCPOnUPG$RILSender;->this$0:Lcom/sec/phone/SecPhoneServiceCPOnUPG;

    iget-object v10, v10, Lcom/sec/phone/SecPhoneServiceCPOnUPG;->LOG_TAG:Ljava/lang/String;

    const-string v11, "IOException "

    invoke-static {v10, v11, v4}, Landroid/telephony/Rlog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 436
    iget-object v10, p0, Lcom/sec/phone/SecPhoneServiceCPOnUPG$RILSender;->this$0:Lcom/sec/phone/SecPhoneServiceCPOnUPG;

    iget v11, v8, Lcom/sec/phone/RILRequest;->mSerial:I

    # invokes: Lcom/sec/phone/SecPhoneServiceCPOnUPG;->findAndRemoveRequestFromList(I)Lcom/sec/phone/RILRequest;
    invoke-static {v10, v11}, Lcom/sec/phone/SecPhoneServiceCPOnUPG;->access$300(Lcom/sec/phone/SecPhoneServiceCPOnUPG;I)Lcom/sec/phone/RILRequest;

    move-result-object v7

    .line 439
    if-nez v7, :cond_2

    if-nez v1, :cond_3

    .line 440
    :cond_2
    const/4 v10, 0x1

    const/4 v11, 0x0

    invoke-virtual {v8, v10, v11}, Lcom/sec/phone/RILRequest;->onError(ILjava/lang/Object;)V

    .line 441
    invoke-virtual {v8}, Lcom/sec/phone/RILRequest;->release()V

    .line 454
    .end local v4    # "ex":Ljava/io/IOException;
    :cond_3
    :goto_1
    if-nez v1, :cond_0

    .line 455
    iget-object v10, p0, Lcom/sec/phone/SecPhoneServiceCPOnUPG$RILSender;->this$0:Lcom/sec/phone/SecPhoneServiceCPOnUPG;

    iget v11, v10, Lcom/sec/phone/SecPhoneServiceCPOnUPG;->mRequestMessagesPending:I

    add-int/lit8 v11, v11, -0x1

    iput v11, v10, Lcom/sec/phone/SecPhoneServiceCPOnUPG;->mRequestMessagesPending:I

    goto/16 :goto_0

    .line 408
    .restart local v9    # "s":Landroid/net/LocalSocket;
    :catchall_0
    move-exception v10

    :try_start_3
    monitor-exit v11
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v10
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_4 .. :try_end_4} :catch_1

    .line 443
    .end local v9    # "s":Landroid/net/LocalSocket;
    :catch_1
    move-exception v5

    .line 444
    .local v5, "exc":Ljava/lang/RuntimeException;
    iget-object v10, p0, Lcom/sec/phone/SecPhoneServiceCPOnUPG$RILSender;->this$0:Lcom/sec/phone/SecPhoneServiceCPOnUPG;

    iget-object v10, v10, Lcom/sec/phone/SecPhoneServiceCPOnUPG;->LOG_TAG:Ljava/lang/String;

    const-string v11, "Uncaught exception "

    invoke-static {v10, v11, v5}, Landroid/telephony/Rlog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 445
    iget-object v10, p0, Lcom/sec/phone/SecPhoneServiceCPOnUPG$RILSender;->this$0:Lcom/sec/phone/SecPhoneServiceCPOnUPG;

    iget v11, v8, Lcom/sec/phone/RILRequest;->mSerial:I

    # invokes: Lcom/sec/phone/SecPhoneServiceCPOnUPG;->findAndRemoveRequestFromList(I)Lcom/sec/phone/RILRequest;
    invoke-static {v10, v11}, Lcom/sec/phone/SecPhoneServiceCPOnUPG;->access$300(Lcom/sec/phone/SecPhoneServiceCPOnUPG;I)Lcom/sec/phone/RILRequest;

    move-result-object v7

    .line 448
    if-nez v7, :cond_4

    if-nez v1, :cond_3

    .line 449
    :cond_4
    const/4 v10, 0x2

    const/4 v11, 0x0

    invoke-virtual {v8, v10, v11}, Lcom/sec/phone/RILRequest;->onError(ILjava/lang/Object;)V

    .line 450
    invoke-virtual {v8}, Lcom/sec/phone/RILRequest;->release()V

    goto :goto_1

    .line 425
    .end local v5    # "exc":Ljava/lang/RuntimeException;
    .restart local v3    # "data":[B
    .restart local v9    # "s":Landroid/net/LocalSocket;
    :cond_5
    :try_start_5
    iget-object v10, p0, Lcom/sec/phone/SecPhoneServiceCPOnUPG$RILSender;->dataLength:[B

    const/4 v11, 0x0

    iget-object v12, p0, Lcom/sec/phone/SecPhoneServiceCPOnUPG$RILSender;->dataLength:[B

    const/4 v13, 0x1

    const/4 v14, 0x0

    aput-byte v14, v12, v13

    aput-byte v14, v10, v11

    .line 426
    iget-object v10, p0, Lcom/sec/phone/SecPhoneServiceCPOnUPG$RILSender;->dataLength:[B

    const/4 v11, 0x2

    array-length v12, v3

    shr-int/lit8 v12, v12, 0x8

    and-int/lit16 v12, v12, 0xff

    int-to-byte v12, v12

    aput-byte v12, v10, v11

    .line 427
    iget-object v10, p0, Lcom/sec/phone/SecPhoneServiceCPOnUPG$RILSender;->dataLength:[B

    const/4 v11, 0x3

    array-length v12, v3

    and-int/lit16 v12, v12, 0xff

    int-to-byte v12, v12

    aput-byte v12, v10, v11

    .line 432
    invoke-virtual {v9}, Landroid/net/LocalSocket;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v10

    iget-object v11, p0, Lcom/sec/phone/SecPhoneServiceCPOnUPG$RILSender;->dataLength:[B

    invoke-virtual {v10, v11}, Ljava/io/OutputStream;->write([B)V

    .line 433
    invoke-virtual {v9}, Landroid/net/LocalSocket;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v10

    invoke-virtual {v10, v3}, Ljava/io/OutputStream;->write([B)V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_5 .. :try_end_5} :catch_1

    goto :goto_1

    .line 464
    .end local v1    # "alreadySubtracted":Z
    .end local v3    # "data":[B
    .end local v9    # "s":Landroid/net/LocalSocket;
    :pswitch_1
    iget-object v10, p0, Lcom/sec/phone/SecPhoneServiceCPOnUPG$RILSender;->this$0:Lcom/sec/phone/SecPhoneServiceCPOnUPG;

    iget-object v11, v10, Lcom/sec/phone/SecPhoneServiceCPOnUPG;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    monitor-enter v11

    .line 465
    :try_start_6
    iget-object v10, p0, Lcom/sec/phone/SecPhoneServiceCPOnUPG$RILSender;->this$0:Lcom/sec/phone/SecPhoneServiceCPOnUPG;

    iget-object v10, v10, Lcom/sec/phone/SecPhoneServiceCPOnUPG;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v10}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v10

    if-eqz v10, :cond_7

    .line 466
    iget-object v10, p0, Lcom/sec/phone/SecPhoneServiceCPOnUPG$RILSender;->this$0:Lcom/sec/phone/SecPhoneServiceCPOnUPG;

    iget-object v12, v10, Lcom/sec/phone/SecPhoneServiceCPOnUPG;->mRequestsList:Ljava/util/ArrayList;

    monitor-enter v12
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 467
    :try_start_7
    iget-object v10, p0, Lcom/sec/phone/SecPhoneServiceCPOnUPG$RILSender;->this$0:Lcom/sec/phone/SecPhoneServiceCPOnUPG;

    iget-object v10, v10, Lcom/sec/phone/SecPhoneServiceCPOnUPG;->mRequestsList:Ljava/util/ArrayList;

    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 468
    .local v2, "count":I
    iget-object v10, p0, Lcom/sec/phone/SecPhoneServiceCPOnUPG$RILSender;->this$0:Lcom/sec/phone/SecPhoneServiceCPOnUPG;

    iget-object v10, v10, Lcom/sec/phone/SecPhoneServiceCPOnUPG;->LOG_TAG:Ljava/lang/String;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "WAKE_LOCK_TIMEOUT  mReqPending="

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    iget-object v14, p0, Lcom/sec/phone/SecPhoneServiceCPOnUPG$RILSender;->this$0:Lcom/sec/phone/SecPhoneServiceCPOnUPG;

    iget v14, v14, Lcom/sec/phone/SecPhoneServiceCPOnUPG;->mRequestMessagesPending:I

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, " mRequestList="

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v10, v13}, Landroid/telephony/Rlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 471
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_2
    if-ge v6, v2, :cond_6

    .line 472
    iget-object v10, p0, Lcom/sec/phone/SecPhoneServiceCPOnUPG$RILSender;->this$0:Lcom/sec/phone/SecPhoneServiceCPOnUPG;

    iget-object v10, v10, Lcom/sec/phone/SecPhoneServiceCPOnUPG;->mRequestsList:Ljava/util/ArrayList;

    invoke-virtual {v10, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    move-object v0, v10

    check-cast v0, Lcom/sec/phone/RILRequest;

    move-object v8, v0

    .line 473
    iget-object v10, p0, Lcom/sec/phone/SecPhoneServiceCPOnUPG$RILSender;->this$0:Lcom/sec/phone/SecPhoneServiceCPOnUPG;

    iget-object v10, v10, Lcom/sec/phone/SecPhoneServiceCPOnUPG;->LOG_TAG:Ljava/lang/String;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v13, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, ": ["

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    iget v14, v8, Lcom/sec/phone/RILRequest;->mSerial:I

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "] "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    iget v14, v8, Lcom/sec/phone/RILRequest;->mRequest:I

    invoke-static {v14}, Lcom/sec/phone/SecPhoneServiceCPOnUPG;->requestToString(I)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v10, v13}, Landroid/telephony/Rlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 471
    add-int/lit8 v6, v6, 0x1

    goto :goto_2

    .line 477
    :cond_6
    monitor-exit v12
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 478
    :try_start_8
    iget-object v10, p0, Lcom/sec/phone/SecPhoneServiceCPOnUPG$RILSender;->this$0:Lcom/sec/phone/SecPhoneServiceCPOnUPG;

    iget-object v10, v10, Lcom/sec/phone/SecPhoneServiceCPOnUPG;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v10}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 480
    .end local v2    # "count":I
    .end local v6    # "i":I
    :cond_7
    monitor-exit v11

    goto/16 :goto_0

    :catchall_1
    move-exception v10

    monitor-exit v11
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    throw v10

    .line 477
    :catchall_2
    move-exception v10

    :try_start_9
    monitor-exit v12
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_2

    :try_start_a
    throw v10
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_1

    .line 385
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public run()V
    .locals 0

    .prologue
    .line 377
    return-void
.end method
