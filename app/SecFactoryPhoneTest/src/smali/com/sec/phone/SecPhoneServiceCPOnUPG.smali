.class public Lcom/sec/phone/SecPhoneServiceCPOnUPG;
.super Landroid/app/Service;
.source "SecPhoneServiceCPOnUPG.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/phone/SecPhoneServiceCPOnUPG$RILReceiver;,
        Lcom/sec/phone/SecPhoneServiceCPOnUPG$RILSender;
    }
.end annotation


# static fields
.field static final ACTION_OTHERS:Ljava/lang/String; = "com.samsung.intent.action"

.field static final ANTENNA_TYPE_DUAL:I = 0x3

.field static final ANTENNA_TYPE_PRIMARY:I = 0x1

.field static final ANTENNA_TYPE_SECONDARY:I = 0x2

.field static final EVENT_GET_ANTENNA_TEST_DONE:I = 0x100

.field static final EVENT_SEND:I = 0x1

.field static final EVENT_SET_ANTENNA_TEST_DONE:I = 0x101

.field static final EVENT_WAKE_LOCK_TIMEOUT:I = 0x2

.field static final LIST_BLOCK_ACTIONS:[Ljava/lang/String;

.field static final OEM_FUNCTION_ID_MISC:I = 0x11

.field static final OEM_FUNCTION_ID_SDM_IMS_SETTINGS:I = 0x22

.field static final OEM_MISC_GET_ANTENNA_TEST:I = 0x6

.field static final OEM_MISC_SET_ANTENNA_TEST:I = 0x7

.field static final OEM_SDM_IMS_GET_SETTINGS:I = 0x1

.field static final OEM_SDM_IMS_SET_SETTINGS:I = 0x2

.field static final OEM_SYSDUMP_FUNCTAG:I = 0x7

.field static final OEM_TCPDUMP_START:I = 0x15

.field static final OEM_TCPDUMP_STOP:I = 0x16

.field static final RESPONSE_SOLICITED:I = 0x0

.field static final RESPONSE_UNSOLICITED:I = 0x1

.field static final RIL_MAX_COMMAND_BYTES:I = 0x2000

.field static final SOCKET_OPEN_RETRY_MILLIS:I = 0xfa0

.field static final SOCKET_PORT:I = 0x1e61

.field static final WAKE_LOCK_TIMEOUT:I = 0x1388

.field private static final isMarvell:Z


# instance fields
.field public FtClientCPOName:Ljava/lang/String;

.field public LOG_TAG:Ljava/lang/String;

.field public RilSocketName:Ljava/lang/String;

.field protected mFTCServiceConnection:Landroid/content/ServiceConnection;

.field private final mHandler:Landroid/os/Handler;

.field private final mMessenger:Landroid/os/Messenger;

.field mReceiver:Lcom/sec/phone/SecPhoneServiceCPOnUPG$RILReceiver;

.field mReceiverThread:Ljava/lang/Thread;

.field mRegisteredSender:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Landroid/os/Message;",
            ">;"
        }
    .end annotation
.end field

.field mRequestMessagesPending:I

.field mRequestsList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/phone/RILRequest;",
            ">;"
        }
    .end annotation
.end field

.field private mSendServiceMessenger:Landroid/os/Messenger;

.field mSender:Lcom/sec/phone/SecPhoneServiceCPOnUPG$RILSender;

.field mSenderThread:Landroid/os/HandlerThread;

.field mSocket:Landroid/net/LocalSocket;

.field mWakeLock:Landroid/os/PowerManager$WakeLock;

.field m_bBindDone:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 126
    const-string v0, "mrvl"

    const-string v1, "ro.board.platform"

    const-string v2, "Unknown"

    invoke-static {v1, v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/sec/phone/SecPhoneServiceCPOnUPG;->isMarvell:Z

    .line 131
    const/4 v0, 0x7

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "PB_SYNC"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "PB2_SYNC"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "SlotSwitched"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "Slot1SwitchCompleted"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "Slot2SwitchCompleted"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "Slot1OffCompleted"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "Slot2OffCompleted"

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/phone/SecPhoneServiceCPOnUPG;->LIST_BLOCK_ACTIONS:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 153
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 90
    const-string v0, "RILS_CPO"

    iput-object v0, p0, Lcom/sec/phone/SecPhoneServiceCPOnUPG;->LOG_TAG:Ljava/lang/String;

    .line 91
    const-string v0, "FtClientCPO"

    iput-object v0, p0, Lcom/sec/phone/SecPhoneServiceCPOnUPG;->FtClientCPOName:Ljava/lang/String;

    .line 128
    const-string v0, "Multiclient"

    iput-object v0, p0, Lcom/sec/phone/SecPhoneServiceCPOnUPG;->RilSocketName:Ljava/lang/String;

    .line 141
    new-instance v0, Landroid/net/LocalSocket;

    invoke-direct {v0}, Landroid/net/LocalSocket;-><init>()V

    iput-object v0, p0, Lcom/sec/phone/SecPhoneServiceCPOnUPG;->mSocket:Landroid/net/LocalSocket;

    .line 149
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/phone/SecPhoneServiceCPOnUPG;->m_bBindDone:Z

    .line 151
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/phone/SecPhoneServiceCPOnUPG;->mRequestsList:Ljava/util/ArrayList;

    .line 158
    iput-object v1, p0, Lcom/sec/phone/SecPhoneServiceCPOnUPG;->mSendServiceMessenger:Landroid/os/Messenger;

    .line 161
    iput-object v1, p0, Lcom/sec/phone/SecPhoneServiceCPOnUPG;->mRegisteredSender:Ljava/util/Map;

    .line 163
    new-instance v0, Lcom/sec/phone/SecPhoneServiceCPOnUPG$1;

    invoke-direct {v0, p0}, Lcom/sec/phone/SecPhoneServiceCPOnUPG$1;-><init>(Lcom/sec/phone/SecPhoneServiceCPOnUPG;)V

    iput-object v0, p0, Lcom/sec/phone/SecPhoneServiceCPOnUPG;->mHandler:Landroid/os/Handler;

    .line 190
    new-instance v0, Landroid/os/Messenger;

    iget-object v1, p0, Lcom/sec/phone/SecPhoneServiceCPOnUPG;->mHandler:Landroid/os/Handler;

    invoke-direct {v0, v1}, Landroid/os/Messenger;-><init>(Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/sec/phone/SecPhoneServiceCPOnUPG;->mMessenger:Landroid/os/Messenger;

    .line 198
    new-instance v0, Lcom/sec/phone/SecPhoneServiceCPOnUPG$2;

    invoke-direct {v0, p0}, Lcom/sec/phone/SecPhoneServiceCPOnUPG$2;-><init>(Lcom/sec/phone/SecPhoneServiceCPOnUPG;)V

    iput-object v0, p0, Lcom/sec/phone/SecPhoneServiceCPOnUPG;->mFTCServiceConnection:Landroid/content/ServiceConnection;

    .line 155
    return-void
.end method

.method static synthetic access$000(Lcom/sec/phone/SecPhoneServiceCPOnUPG;)Landroid/os/Messenger;
    .locals 1
    .param p0, "x0"    # Lcom/sec/phone/SecPhoneServiceCPOnUPG;

    .prologue
    .line 89
    iget-object v0, p0, Lcom/sec/phone/SecPhoneServiceCPOnUPG;->mSendServiceMessenger:Landroid/os/Messenger;

    return-object v0
.end method

.method static synthetic access$002(Lcom/sec/phone/SecPhoneServiceCPOnUPG;Landroid/os/Messenger;)Landroid/os/Messenger;
    .locals 0
    .param p0, "x0"    # Lcom/sec/phone/SecPhoneServiceCPOnUPG;
    .param p1, "x1"    # Landroid/os/Messenger;

    .prologue
    .line 89
    iput-object p1, p0, Lcom/sec/phone/SecPhoneServiceCPOnUPG;->mSendServiceMessenger:Landroid/os/Messenger;

    return-object p1
.end method

.method static synthetic access$100(Lcom/sec/phone/SecPhoneServiceCPOnUPG;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/phone/SecPhoneServiceCPOnUPG;

    .prologue
    .line 89
    invoke-direct {p0}, Lcom/sec/phone/SecPhoneServiceCPOnUPG;->rebindFactoryTestClientService()V

    return-void
.end method

.method static synthetic access$200(Lcom/sec/phone/SecPhoneServiceCPOnUPG;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/phone/SecPhoneServiceCPOnUPG;

    .prologue
    .line 89
    invoke-direct {p0}, Lcom/sec/phone/SecPhoneServiceCPOnUPG;->connectToFactoryTestClientServiceCPO()V

    return-void
.end method

.method static synthetic access$300(Lcom/sec/phone/SecPhoneServiceCPOnUPG;I)Lcom/sec/phone/RILRequest;
    .locals 1
    .param p0, "x0"    # Lcom/sec/phone/SecPhoneServiceCPOnUPG;
    .param p1, "x1"    # I

    .prologue
    .line 89
    invoke-direct {p0, p1}, Lcom/sec/phone/SecPhoneServiceCPOnUPG;->findAndRemoveRequestFromList(I)Lcom/sec/phone/RILRequest;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$400()Z
    .locals 1

    .prologue
    .line 89
    sget-boolean v0, Lcom/sec/phone/SecPhoneServiceCPOnUPG;->isMarvell:Z

    return v0
.end method

.method static synthetic access$500(Lcom/sec/phone/SecPhoneServiceCPOnUPG;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/phone/SecPhoneServiceCPOnUPG;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 89
    invoke-direct {p0, p1}, Lcom/sec/phone/SecPhoneServiceCPOnUPG;->sendIntentSecPhoneReady(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$600(Ljava/lang/String;Ljava/io/InputStream;[B)I
    .locals 1
    .param p0, "x0"    # Ljava/lang/String;
    .param p1, "x1"    # Ljava/io/InputStream;
    .param p2, "x2"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 89
    invoke-static {p0, p1, p2}, Lcom/sec/phone/SecPhoneServiceCPOnUPG;->readRilMessage(Ljava/lang/String;Ljava/io/InputStream;[B)I

    move-result v0

    return v0
.end method

.method static synthetic access$700(Lcom/sec/phone/SecPhoneServiceCPOnUPG;Landroid/os/Parcel;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/phone/SecPhoneServiceCPOnUPG;
    .param p1, "x1"    # Landroid/os/Parcel;

    .prologue
    .line 89
    invoke-direct {p0, p1}, Lcom/sec/phone/SecPhoneServiceCPOnUPG;->processResponse(Landroid/os/Parcel;)V

    return-void
.end method

.method private acquireWakeLock()V
    .locals 6

    .prologue
    .line 1172
    iget-object v2, p0, Lcom/sec/phone/SecPhoneServiceCPOnUPG;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    monitor-enter v2

    .line 1173
    :try_start_0
    iget-object v1, p0, Lcom/sec/phone/SecPhoneServiceCPOnUPG;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 1174
    iget v1, p0, Lcom/sec/phone/SecPhoneServiceCPOnUPG;->mRequestMessagesPending:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/sec/phone/SecPhoneServiceCPOnUPG;->mRequestMessagesPending:I

    .line 1176
    iget-object v1, p0, Lcom/sec/phone/SecPhoneServiceCPOnUPG;->mSender:Lcom/sec/phone/SecPhoneServiceCPOnUPG$RILSender;

    const/4 v3, 0x2

    invoke-virtual {v1, v3}, Lcom/sec/phone/SecPhoneServiceCPOnUPG$RILSender;->removeMessages(I)V

    .line 1177
    iget-object v1, p0, Lcom/sec/phone/SecPhoneServiceCPOnUPG;->mSender:Lcom/sec/phone/SecPhoneServiceCPOnUPG$RILSender;

    const/4 v3, 0x2

    invoke-virtual {v1, v3}, Lcom/sec/phone/SecPhoneServiceCPOnUPG$RILSender;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 1178
    .local v0, "msg":Landroid/os/Message;
    iget-object v1, p0, Lcom/sec/phone/SecPhoneServiceCPOnUPG;->mSender:Lcom/sec/phone/SecPhoneServiceCPOnUPG$RILSender;

    const-wide/16 v4, 0x1388

    invoke-virtual {v1, v0, v4, v5}, Lcom/sec/phone/SecPhoneServiceCPOnUPG$RILSender;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 1179
    monitor-exit v2

    .line 1180
    return-void

    .line 1179
    .end local v0    # "msg":Landroid/os/Message;
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public static bytesToHexString([B)Ljava/lang/String;
    .locals 4
    .param p0, "bytes"    # [B

    .prologue
    .line 1462
    if-nez p0, :cond_0

    .line 1463
    const/4 v3, 0x0

    .line 1479
    :goto_0
    return-object v3

    .line 1465
    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    array-length v3, p0

    mul-int/lit8 v3, v3, 0x2

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 1467
    .local v2, "ret":Ljava/lang/StringBuilder;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    array-length v3, p0

    if-ge v1, v3, :cond_1

    .line 1470
    aget-byte v3, p0, v1

    shr-int/lit8 v3, v3, 0x4

    and-int/lit8 v0, v3, 0xf

    .line 1472
    .local v0, "b":I
    const-string v3, "0123456789abcdef"

    invoke-virtual {v3, v0}, Ljava/lang/String;->charAt(I)C

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 1474
    aget-byte v3, p0, v1

    and-int/lit8 v0, v3, 0xf

    .line 1476
    const-string v3, "0123456789abcdef"

    invoke-virtual {v3, v0}, Ljava/lang/String;->charAt(I)C

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 1467
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1479
    .end local v0    # "b":I
    :cond_1
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    goto :goto_0
.end method

.method private connectToFactoryTestClientServiceCPO()V
    .locals 4

    .prologue
    .line 236
    iget-object v1, p0, Lcom/sec/phone/SecPhoneServiceCPOnUPG;->LOG_TAG:Ljava/lang/String;

    const-string v2, "connect To FactoryTestClient service for CPO"

    invoke-static {v1, v2}, Landroid/telephony/Rlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 237
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 238
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "com.sec.factory"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "com.sec.factory.cporiented."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/phone/SecPhoneServiceCPOnUPG;->FtClientCPOName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 239
    const/16 v1, 0x20

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 241
    iget-object v1, p0, Lcom/sec/phone/SecPhoneServiceCPOnUPG;->mFTCServiceConnection:Landroid/content/ServiceConnection;

    const/4 v2, 0x1

    invoke-virtual {p0, v0, v1, v2}, Lcom/sec/phone/SecPhoneServiceCPOnUPG;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    .line 242
    return-void
.end method

.method private findAndRemoveRequestFromList(I)Lcom/sec/phone/RILRequest;
    .locals 5
    .param p1, "serial"    # I

    .prologue
    .line 1216
    iget-object v4, p0, Lcom/sec/phone/SecPhoneServiceCPOnUPG;->mRequestsList:Ljava/util/ArrayList;

    monitor-enter v4

    .line 1217
    const/4 v0, 0x0

    .local v0, "i":I
    :try_start_0
    iget-object v3, p0, Lcom/sec/phone/SecPhoneServiceCPOnUPG;->mRequestsList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v2

    .local v2, "s":I
    :goto_0
    if-ge v0, v2, :cond_1

    .line 1218
    iget-object v3, p0, Lcom/sec/phone/SecPhoneServiceCPOnUPG;->mRequestsList:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/phone/RILRequest;

    .line 1220
    .local v1, "rr":Lcom/sec/phone/RILRequest;
    iget v3, v1, Lcom/sec/phone/RILRequest;->mSerial:I

    if-ne v3, p1, :cond_0

    .line 1221
    iget-object v3, p0, Lcom/sec/phone/SecPhoneServiceCPOnUPG;->mRequestsList:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 1222
    monitor-exit v4

    .line 1227
    .end local v1    # "rr":Lcom/sec/phone/RILRequest;
    :goto_1
    return-object v1

    .line 1217
    .restart local v1    # "rr":Lcom/sec/phone/RILRequest;
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1225
    .end local v1    # "rr":Lcom/sec/phone/RILRequest;
    :cond_1
    monitor-exit v4

    .line 1227
    const/4 v1, 0x0

    goto :goto_1

    .line 1225
    .end local v2    # "s":I
    :catchall_0
    move-exception v3

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v3
.end method

.method private getTelephonyService()Lcom/android/internal/telephony/ITelephony;
    .locals 3

    .prologue
    .line 717
    const-string v1, "phone"

    invoke-static {v1}, Landroid/os/ServiceManager;->checkService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v1

    invoke-static {v1}, Lcom/android/internal/telephony/ITelephony$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/telephony/ITelephony;

    move-result-object v0

    .line 719
    .local v0, "telephonyService":Lcom/android/internal/telephony/ITelephony;
    if-nez v0, :cond_0

    .line 720
    iget-object v1, p0, Lcom/sec/phone/SecPhoneServiceCPOnUPG;->LOG_TAG:Ljava/lang/String;

    const-string v2, "Unable to find ITelephony interface."

    invoke-static {v1, v2}, Landroid/telephony/Rlog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 722
    :cond_0
    return-object v0
.end method

.method private isBlockedAction(Ljava/lang/String;)Z
    .locals 1
    .param p1, "action"    # Ljava/lang/String;

    .prologue
    .line 193
    const/4 v0, 0x0

    .line 195
    .local v0, "bRet":Z
    return v0
.end method

.method private processResponse(Landroid/os/Parcel;)V
    .locals 2
    .param p1, "p"    # Landroid/os/Parcel;

    .prologue
    .line 1204
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 1206
    .local v0, "type":I
    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 1207
    invoke-direct {p0, p1}, Lcom/sec/phone/SecPhoneServiceCPOnUPG;->processUnsolicited(Landroid/os/Parcel;)V

    .line 1212
    :cond_0
    :goto_0
    invoke-direct {p0}, Lcom/sec/phone/SecPhoneServiceCPOnUPG;->releaseWakeLockIfDone()V

    .line 1213
    return-void

    .line 1208
    :cond_1
    if-nez v0, :cond_0

    .line 1209
    invoke-direct {p0, p1}, Lcom/sec/phone/SecPhoneServiceCPOnUPG;->processSolicited(Landroid/os/Parcel;)V

    goto :goto_0
.end method

.method private processSolicited(Landroid/os/Parcel;)V
    .locals 9
    .param p1, "p"    # Landroid/os/Parcel;

    .prologue
    const/4 v8, 0x0

    .line 1233
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v3

    .line 1234
    .local v3, "serial":I
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 1238
    .local v0, "error":I
    invoke-direct {p0, v3}, Lcom/sec/phone/SecPhoneServiceCPOnUPG;->findAndRemoveRequestFromList(I)Lcom/sec/phone/RILRequest;

    move-result-object v2

    .line 1240
    .local v2, "rr":Lcom/sec/phone/RILRequest;
    if-nez v2, :cond_0

    .line 1241
    iget-object v5, p0, Lcom/sec/phone/SecPhoneServiceCPOnUPG;->LOG_TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Unexpected solicited response! sn: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " error: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/telephony/Rlog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1298
    :goto_0
    return-void

    .line 1245
    :cond_0
    iget-object v5, p0, Lcom/sec/phone/SecPhoneServiceCPOnUPG;->LOG_TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "processSolicited(): mResult - "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, v2, Lcom/sec/phone/RILRequest;->mResult:Landroid/os/Message;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/telephony/Rlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1246
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v1

    .line 1247
    .local v1, "reply":Landroid/os/Message;
    iget-object v5, v2, Lcom/sec/phone/RILRequest;->mResult:Landroid/os/Message;

    if-eqz v5, :cond_1

    .line 1248
    iget-object v5, v2, Lcom/sec/phone/RILRequest;->mResult:Landroid/os/Message;

    invoke-virtual {v1, v5}, Landroid/os/Message;->copyFrom(Landroid/os/Message;)V

    .line 1249
    :cond_1
    iget-object v5, p0, Lcom/sec/phone/SecPhoneServiceCPOnUPG;->LOG_TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "processSolicited(): reply - "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/telephony/Rlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1251
    if-eqz v0, :cond_2

    invoke-virtual {p1}, Landroid/os/Parcel;->dataAvail()I

    move-result v5

    if-lez v5, :cond_3

    .line 1254
    :cond_2
    :try_start_0
    iget v5, v2, Lcom/sec/phone/RILRequest;->mRequest:I

    packed-switch v5, :pswitch_data_0

    .line 1264
    new-instance v5, Ljava/lang/RuntimeException;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Unrecognized solicited response: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v7, v2, Lcom/sec/phone/RILRequest;->mRequest:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v5
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 1268
    :catch_0
    move-exception v4

    .line 1271
    .local v4, "tr":Ljava/lang/Throwable;
    iget-object v5, p0, Lcom/sec/phone/SecPhoneServiceCPOnUPG;->LOG_TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2}, Lcom/sec/phone/RILRequest;->serialString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "< "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v7, v2, Lcom/sec/phone/RILRequest;->mRequest:I

    invoke-static {v7}, Lcom/sec/phone/SecPhoneServiceCPOnUPG;->requestToString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " exception, possible invalid RIL response"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6, v4}, Landroid/telephony/Rlog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1274
    const/4 v5, -0x1

    invoke-virtual {v2, v5, v8}, Lcom/sec/phone/RILRequest;->onError(ILjava/lang/Object;)V

    .line 1275
    invoke-virtual {v2}, Lcom/sec/phone/RILRequest;->release()V

    goto/16 :goto_0

    .line 1261
    .end local v4    # "tr":Ljava/lang/Throwable;
    :pswitch_0
    :try_start_1
    invoke-direct {p0, p1, v1}, Lcom/sec/phone/SecPhoneServiceCPOnUPG;->responseRaw(Landroid/os/Parcel;Landroid/os/Message;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    .line 1280
    :cond_3
    if-eqz v0, :cond_4

    .line 1281
    invoke-virtual {v2, v0, v8}, Lcom/sec/phone/RILRequest;->onError(ILjava/lang/Object;)V

    .line 1282
    invoke-virtual {v2}, Lcom/sec/phone/RILRequest;->release()V

    goto/16 :goto_0

    .line 1286
    :cond_4
    iget-object v5, p0, Lcom/sec/phone/SecPhoneServiceCPOnUPG;->LOG_TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2}, Lcom/sec/phone/RILRequest;->serialString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "< "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v7, v2, Lcom/sec/phone/RILRequest;->mRequest:I

    invoke-static {v7}, Lcom/sec/phone/SecPhoneServiceCPOnUPG;->requestToString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v7, v2, Lcom/sec/phone/RILRequest;->mRequest:I

    invoke-direct {p0, v7, v1}, Lcom/sec/phone/SecPhoneServiceCPOnUPG;->retToString(ILjava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/telephony/Rlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1290
    iget-object v5, v2, Lcom/sec/phone/RILRequest;->mResult:Landroid/os/Message;

    if-eqz v5, :cond_5

    .line 1292
    :try_start_2
    iget-object v5, v2, Lcom/sec/phone/RILRequest;->mResult:Landroid/os/Message;

    iget-object v5, v5, Landroid/os/Message;->replyTo:Landroid/os/Messenger;

    invoke-virtual {v5, v1}, Landroid/os/Messenger;->send(Landroid/os/Message;)V
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_1

    .line 1297
    :cond_5
    :goto_1
    invoke-virtual {v2}, Lcom/sec/phone/RILRequest;->release()V

    goto/16 :goto_0

    .line 1293
    :catch_1
    move-exception v5

    goto :goto_1

    .line 1254
    :pswitch_data_0
    .packed-switch 0x3b
        :pswitch_0
    .end packed-switch
.end method

.method private processUnsolicited(Landroid/os/Parcel;)V
    .locals 17
    .param p1, "p"    # Landroid/os/Parcel;

    .prologue
    .line 1348
    const/4 v11, 0x0

    .line 1350
    .local v11, "ret":Ljava/lang/Object;
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    move-result v10

    .line 1353
    .local v10, "response":I
    sparse-switch v10, :sswitch_data_0

    .line 1364
    :try_start_0
    new-instance v14, Ljava/lang/RuntimeException;

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "Unrecognized unsol response: "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-direct {v14, v15}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v14

    .line 1367
    :catch_0
    move-exception v13

    .line 1430
    .end local v11    # "ret":Ljava/lang/Object;
    :cond_0
    :goto_0
    :sswitch_0
    return-void

    .line 1360
    .restart local v11    # "ret":Ljava/lang/Object;
    :sswitch_1
    invoke-direct/range {p0 .. p1}, Lcom/sec/phone/SecPhoneServiceCPOnUPG;->responseString(Landroid/os/Parcel;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v11

    .line 1372
    .end local v11    # "ret":Ljava/lang/Object;
    :sswitch_2
    sparse-switch v10, :sswitch_data_1

    goto :goto_0

    :sswitch_3
    move-object v12, v11

    .line 1379
    check-cast v12, Ljava/lang/String;

    .line 1380
    .local v12, "str":Ljava/lang/String;
    const-string v14, " "

    invoke-virtual {v12, v14}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 1383
    .local v1, "Args":[Ljava/lang/String;
    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/sec/phone/SecPhoneServiceCPOnUPG;->GetActionFromRilResponse([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1384
    .local v2, "action":Ljava/lang/String;
    const/4 v7, 0x0

    .line 1386
    .local v7, "registeredMessenger":Landroid/os/Message;
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/phone/SecPhoneServiceCPOnUPG;->mRegisteredSender:Ljava/util/Map;

    if-eqz v14, :cond_1

    .line 1387
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/phone/SecPhoneServiceCPOnUPG;->mRegisteredSender:Ljava/util/Map;

    invoke-interface {v14, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    .end local v7    # "registeredMessenger":Landroid/os/Message;
    check-cast v7, Landroid/os/Message;

    .line 1390
    .restart local v7    # "registeredMessenger":Landroid/os/Message;
    :cond_1
    if-nez v7, :cond_6

    .line 1392
    new-instance v3, Ljava/lang/String;

    const-string v14, "com.android.samsungtest.RilDFTCommand"

    invoke-direct {v3, v14}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    .line 1393
    .local v3, "action1":Ljava/lang/String;
    new-instance v4, Ljava/lang/String;

    const-string v14, "com.android.samsungtest.RilCommand"

    invoke-direct {v4, v14}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    .line 1394
    .local v4, "action2":Ljava/lang/String;
    new-instance v5, Ljava/lang/String;

    const-string v14, "com.android.samsungtest.CPOChnaged"

    invoke-direct {v5, v14}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    .line 1395
    .local v5, "action3":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/phone/SecPhoneServiceCPOnUPG;->mSendServiceMessenger:Landroid/os/Messenger;

    if-eqz v14, :cond_2

    const/4 v14, 0x2

    aget-object v14, v1, v14

    invoke-virtual {v14, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-nez v14, :cond_3

    :cond_2
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/phone/SecPhoneServiceCPOnUPG;->mSendServiceMessenger:Landroid/os/Messenger;

    if-eqz v14, :cond_4

    const/4 v14, 0x2

    aget-object v14, v1, v14

    invoke-virtual {v14, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_4

    .line 1397
    :cond_3
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/phone/SecPhoneServiceCPOnUPG;->LOG_TAG:Ljava/lang/String;

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "* FactoryClient *"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/telephony/Rlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1398
    move-object/from16 v0, p0

    invoke-direct {v0, v12}, Lcom/sec/phone/SecPhoneServiceCPOnUPG;->sendMessage(Ljava/lang/String;)V

    goto :goto_0

    .line 1399
    :cond_4
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/phone/SecPhoneServiceCPOnUPG;->mSendServiceMessenger:Landroid/os/Messenger;

    if-nez v14, :cond_5

    const/4 v14, 0x2

    aget-object v14, v1, v14

    invoke-virtual {v14, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_5

    .line 1400
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/phone/SecPhoneServiceCPOnUPG;->LOG_TAG:Ljava/lang/String;

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "* Change APO -> CPO *"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/telephony/Rlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1401
    invoke-direct/range {p0 .. p0}, Lcom/sec/phone/SecPhoneServiceCPOnUPG;->connectToFactoryTestClientServiceCPO()V

    goto/16 :goto_0

    .line 1406
    :cond_5
    const/4 v14, 0x2

    aget-object v14, v1, v14

    move-object/from16 v0, p0

    invoke-direct {v0, v14}, Lcom/sec/phone/SecPhoneServiceCPOnUPG;->isBlockedAction(Ljava/lang/String;)Z

    move-result v14

    if-nez v14, :cond_0

    .line 1407
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/phone/SecPhoneServiceCPOnUPG;->LOG_TAG:Ljava/lang/String;

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "Executing Am "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/telephony/Rlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1408
    invoke-static {v1}, Lcom/sec/phone/Am;->main([Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1412
    .end local v3    # "action1":Ljava/lang/String;
    .end local v4    # "action2":Ljava/lang/String;
    .end local v5    # "action3":Ljava/lang/String;
    :cond_6
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/phone/SecPhoneServiceCPOnUPG;->LOG_TAG:Ljava/lang/String;

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "SendToMessenger : "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/telephony/Rlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1413
    move-object v8, v7

    .line 1414
    .local v8, "reply":Landroid/os/Message;
    invoke-virtual {v8}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v9

    .line 1415
    .local v9, "req":Landroid/os/Bundle;
    const-string v14, "Command"

    invoke-virtual {v9, v14, v12}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1416
    invoke-virtual {v8, v9}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 1418
    :try_start_1
    iget-object v14, v7, Landroid/os/Message;->replyTo:Landroid/os/Messenger;

    invoke-virtual {v14, v8}, Landroid/os/Messenger;->send(Landroid/os/Message;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1

    goto/16 :goto_0

    .line 1419
    :catch_1
    move-exception v6

    .line 1420
    .local v6, "e":Landroid/os/RemoteException;
    const/4 v14, 0x2

    aget-object v14, v1, v14

    move-object/from16 v0, p0

    invoke-direct {v0, v14}, Lcom/sec/phone/SecPhoneServiceCPOnUPG;->isBlockedAction(Ljava/lang/String;)Z

    move-result v14

    if-nez v14, :cond_0

    .line 1421
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/phone/SecPhoneServiceCPOnUPG;->LOG_TAG:Ljava/lang/String;

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "Executing Am "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/telephony/Rlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1422
    invoke-static {v1}, Lcom/sec/phone/Am;->main([Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1353
    nop

    :sswitch_data_0
    .sparse-switch
        0x2b02 -> :sswitch_2
        0x2b12 -> :sswitch_1
    .end sparse-switch

    .line 1372
    :sswitch_data_1
    .sparse-switch
        0x2b02 -> :sswitch_0
        0x2b12 -> :sswitch_3
    .end sparse-switch
.end method

.method private static readRilMessage(Ljava/lang/String;Ljava/io/InputStream;[B)I
    .locals 7
    .param p0, "LOG_TAG"    # Ljava/lang/String;
    .param p1, "is"    # Ljava/io/InputStream;
    .param p2, "buffer"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, -0x1

    .line 494
    const/4 v2, 0x0

    .line 495
    .local v2, "offset":I
    const/4 v3, 0x4

    .line 497
    .local v3, "remaining":I
    :cond_0
    invoke-virtual {p1, p2, v2, v3}, Ljava/io/InputStream;->read([BII)I

    move-result v0

    .line 499
    .local v0, "countRead":I
    if-gez v0, :cond_1

    .line 500
    const-string v5, "Hit EOS reading message length"

    invoke-static {p0, v5}, Landroid/telephony/Rlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v1, v4

    .line 527
    :goto_0
    return v1

    .line 504
    :cond_1
    add-int/2addr v2, v0

    .line 505
    sub-int/2addr v3, v0

    .line 506
    if-gtz v3, :cond_0

    .line 508
    const/4 v5, 0x0

    aget-byte v5, p2, v5

    and-int/lit16 v5, v5, 0xff

    shl-int/lit8 v5, v5, 0x18

    const/4 v6, 0x1

    aget-byte v6, p2, v6

    and-int/lit16 v6, v6, 0xff

    shl-int/lit8 v6, v6, 0x10

    or-int/2addr v5, v6

    const/4 v6, 0x2

    aget-byte v6, p2, v6

    and-int/lit16 v6, v6, 0xff

    shl-int/lit8 v6, v6, 0x8

    or-int/2addr v5, v6

    const/4 v6, 0x3

    aget-byte v6, p2, v6

    and-int/lit16 v6, v6, 0xff

    or-int v1, v5, v6

    .line 512
    .local v1, "messageLength":I
    const/4 v2, 0x0

    .line 513
    move v3, v1

    .line 515
    :cond_2
    invoke-virtual {p1, p2, v2, v3}, Ljava/io/InputStream;->read([BII)I

    move-result v0

    .line 517
    if-gez v0, :cond_3

    .line 518
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Hit EOS reading message.  messageLength="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " remaining="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {p0, v5}, Landroid/telephony/Rlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v1, v4

    .line 520
    goto :goto_0

    .line 523
    :cond_3
    add-int/2addr v2, v0

    .line 524
    sub-int/2addr v3, v0

    .line 525
    if-gtz v3, :cond_2

    goto :goto_0
.end method

.method private rebindFactoryTestClientService()V
    .locals 1

    .prologue
    .line 218
    iget-object v0, p0, Lcom/sec/phone/SecPhoneServiceCPOnUPG;->mSendServiceMessenger:Landroid/os/Messenger;

    if-eqz v0, :cond_0

    .line 233
    :goto_0
    return-void

    .line 221
    :cond_0
    new-instance v0, Lcom/sec/phone/SecPhoneServiceCPOnUPG$3;

    invoke-direct {v0, p0}, Lcom/sec/phone/SecPhoneServiceCPOnUPG$3;-><init>(Lcom/sec/phone/SecPhoneServiceCPOnUPG;)V

    invoke-virtual {v0}, Lcom/sec/phone/SecPhoneServiceCPOnUPG$3;->start()V

    goto :goto_0
.end method

.method private releaseWakeLockIfDone()V
    .locals 3

    .prologue
    .line 1183
    iget-object v1, p0, Lcom/sec/phone/SecPhoneServiceCPOnUPG;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    monitor-enter v1

    .line 1184
    :try_start_0
    iget-object v0, p0, Lcom/sec/phone/SecPhoneServiceCPOnUPG;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/sec/phone/SecPhoneServiceCPOnUPG;->mRequestMessagesPending:I

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/phone/SecPhoneServiceCPOnUPG;->mRequestsList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_0

    .line 1185
    iget-object v0, p0, Lcom/sec/phone/SecPhoneServiceCPOnUPG;->mSender:Lcom/sec/phone/SecPhoneServiceCPOnUPG$RILSender;

    const/4 v2, 0x2

    invoke-virtual {v0, v2}, Lcom/sec/phone/SecPhoneServiceCPOnUPG$RILSender;->removeMessages(I)V

    .line 1186
    iget-object v0, p0, Lcom/sec/phone/SecPhoneServiceCPOnUPG;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 1188
    :cond_0
    monitor-exit v1

    .line 1189
    return-void

    .line 1188
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method static requestToString(I)Ljava/lang/String;
    .locals 1
    .param p0, "request"    # I

    .prologue
    .line 840
    sparse-switch p0, :sswitch_data_0

    .line 1082
    const-string v0, "<unknown request>"

    :goto_0
    return-object v0

    .line 842
    :sswitch_0
    const-string v0, "GET_SIM_STATUS"

    goto :goto_0

    .line 844
    :sswitch_1
    const-string v0, "ENTER_SIM_PIN"

    goto :goto_0

    .line 846
    :sswitch_2
    const-string v0, "ENTER_SIM_PUK"

    goto :goto_0

    .line 848
    :sswitch_3
    const-string v0, "ENTER_SIM_PIN2"

    goto :goto_0

    .line 850
    :sswitch_4
    const-string v0, "ENTER_SIM_PUK2"

    goto :goto_0

    .line 852
    :sswitch_5
    const-string v0, "CHANGE_SIM_PIN"

    goto :goto_0

    .line 854
    :sswitch_6
    const-string v0, "CHANGE_SIM_PIN2"

    goto :goto_0

    .line 856
    :sswitch_7
    const-string v0, "ENTER_NETWORK_DEPERSONALIZATION"

    goto :goto_0

    .line 858
    :sswitch_8
    const-string v0, "GET_CURRENT_CALLS"

    goto :goto_0

    .line 860
    :sswitch_9
    const-string v0, "DIAL"

    goto :goto_0

    .line 864
    :sswitch_a
    const-string v0, "DIAL_VIDEO_CALL"

    goto :goto_0

    .line 867
    :sswitch_b
    const-string v0, "GET_IMSI"

    goto :goto_0

    .line 869
    :sswitch_c
    const-string v0, "HANGUP"

    goto :goto_0

    .line 871
    :sswitch_d
    const-string v0, "HANGUP_WAITING_OR_BACKGROUND"

    goto :goto_0

    .line 873
    :sswitch_e
    const-string v0, "HANGUP_FOREGROUND_RESUME_BACKGROUND"

    goto :goto_0

    .line 875
    :sswitch_f
    const-string v0, "REQUEST_SWITCH_WAITING_OR_HOLDING_AND_ACTIVE"

    goto :goto_0

    .line 877
    :sswitch_10
    const-string v0, "CONFERENCE"

    goto :goto_0

    .line 879
    :sswitch_11
    const-string v0, "DEFLECT"

    goto :goto_0

    .line 881
    :sswitch_12
    const-string v0, "UDUB"

    goto :goto_0

    .line 883
    :sswitch_13
    const-string v0, "LAST_CALL_FAIL_CAUSE"

    goto :goto_0

    .line 885
    :sswitch_14
    const-string v0, "SIGNAL_STRENGTH"

    goto :goto_0

    .line 887
    :sswitch_15
    const-string v0, "REGISTRATION_STATE"

    goto :goto_0

    .line 889
    :sswitch_16
    const-string v0, "GPRS_REGISTRATION_STATE"

    goto :goto_0

    .line 891
    :sswitch_17
    const-string v0, "OPERATOR"

    goto :goto_0

    .line 893
    :sswitch_18
    const-string v0, "RADIO_POWER"

    goto :goto_0

    .line 895
    :sswitch_19
    const-string v0, "DTMF"

    goto :goto_0

    .line 897
    :sswitch_1a
    const-string v0, "SEND_SMS"

    goto :goto_0

    .line 899
    :sswitch_1b
    const-string v0, "SEND_SMS_EXPECT_MORE"

    goto :goto_0

    .line 901
    :sswitch_1c
    const-string v0, "SETUP_DATA_CALL"

    goto :goto_0

    .line 903
    :sswitch_1d
    const-string v0, "SIM_IO"

    goto :goto_0

    .line 905
    :sswitch_1e
    const-string v0, "SEND_USSD"

    goto :goto_0

    .line 907
    :sswitch_1f
    const-string v0, "SEND_ENCODED_USSD"

    goto :goto_0

    .line 909
    :sswitch_20
    const-string v0, "CANCEL_USSD"

    goto :goto_0

    .line 911
    :sswitch_21
    const-string v0, "GET_CLIR"

    goto :goto_0

    .line 913
    :sswitch_22
    const-string v0, "SET_CLIR"

    goto :goto_0

    .line 915
    :sswitch_23
    const-string v0, "QUERY_CALL_FORWARD_STATUS"

    goto :goto_0

    .line 917
    :sswitch_24
    const-string v0, "SET_CALL_FORWARD"

    goto :goto_0

    .line 919
    :sswitch_25
    const-string v0, "QUERY_CALL_WAITING"

    goto :goto_0

    .line 921
    :sswitch_26
    const-string v0, "SET_CALL_WAITING"

    goto :goto_0

    .line 923
    :sswitch_27
    const-string v0, "SMS_ACKNOWLEDGE"

    goto :goto_0

    .line 925
    :sswitch_28
    const-string v0, "GET_IMEI"

    goto :goto_0

    .line 927
    :sswitch_29
    const-string v0, "GET_IMEISV"

    goto :goto_0

    .line 929
    :sswitch_2a
    const-string v0, "ANSWER"

    goto/16 :goto_0

    .line 931
    :sswitch_2b
    const-string v0, "DEACTIVATE_DATA_CALL"

    goto/16 :goto_0

    .line 933
    :sswitch_2c
    const-string v0, "QUERY_FACILITY_LOCK"

    goto/16 :goto_0

    .line 935
    :sswitch_2d
    const-string v0, "SET_FACILITY_LOCK"

    goto/16 :goto_0

    .line 937
    :sswitch_2e
    const-string v0, "CHANGE_BARRING_PASSWORD"

    goto/16 :goto_0

    .line 939
    :sswitch_2f
    const-string v0, "QUERY_NETWORK_SELECTION_MODE"

    goto/16 :goto_0

    .line 941
    :sswitch_30
    const-string v0, "SET_NETWORK_SELECTION_AUTOMATIC"

    goto/16 :goto_0

    .line 943
    :sswitch_31
    const-string v0, "SET_NETWORK_SELECTION_MANUAL"

    goto/16 :goto_0

    .line 945
    :sswitch_32
    const-string v0, "QUERY_AVAILABLE_NETWORKS "

    goto/16 :goto_0

    .line 947
    :sswitch_33
    const-string v0, "DTMF_START"

    goto/16 :goto_0

    .line 949
    :sswitch_34
    const-string v0, "DTMF_STOP"

    goto/16 :goto_0

    .line 951
    :sswitch_35
    const-string v0, "BASEBAND_VERSION"

    goto/16 :goto_0

    .line 953
    :sswitch_36
    const-string v0, "SEPARATE_CONNECTION"

    goto/16 :goto_0

    .line 955
    :sswitch_37
    const-string v0, "SET_MUTE"

    goto/16 :goto_0

    .line 957
    :sswitch_38
    const-string v0, "GET_MUTE"

    goto/16 :goto_0

    .line 959
    :sswitch_39
    const-string v0, "QUERY_CLIP"

    goto/16 :goto_0

    .line 961
    :sswitch_3a
    const-string v0, "LAST_DATA_CALL_FAIL_CAUSE"

    goto/16 :goto_0

    .line 963
    :sswitch_3b
    const-string v0, "DATA_CALL_LIST"

    goto/16 :goto_0

    .line 965
    :sswitch_3c
    const-string v0, "RESET_RADIO"

    goto/16 :goto_0

    .line 967
    :sswitch_3d
    const-string v0, "OEM_HOOK_RAW"

    goto/16 :goto_0

    .line 969
    :sswitch_3e
    const-string v0, "OEM_HOOK_STRINGS"

    goto/16 :goto_0

    .line 971
    :sswitch_3f
    const-string v0, "SCREEN_STATE"

    goto/16 :goto_0

    .line 973
    :sswitch_40
    const-string v0, "SET_SUPP_SVC_NOTIFICATION"

    goto/16 :goto_0

    .line 975
    :sswitch_41
    const-string v0, "WRITE_SMS_TO_SIM"

    goto/16 :goto_0

    .line 978
    :sswitch_42
    const-string v0, "READ_SMS_FROM_SIM"

    goto/16 :goto_0

    .line 981
    :sswitch_43
    const-string v0, "DELETE_SMS_ON_SIM"

    goto/16 :goto_0

    .line 983
    :sswitch_44
    const-string v0, "SET_BAND_MODE"

    goto/16 :goto_0

    .line 985
    :sswitch_45
    const-string v0, "QUERY_AVAILABLE_BAND_MODE"

    goto/16 :goto_0

    .line 987
    :sswitch_46
    const-string v0, "REQUEST_STK_GET_PROFILE"

    goto/16 :goto_0

    .line 989
    :sswitch_47
    const-string v0, "REQUEST_STK_SET_PROFILE"

    goto/16 :goto_0

    .line 991
    :sswitch_48
    const-string v0, "REQUEST_STK_SEND_ENVELOPE_COMMAND"

    goto/16 :goto_0

    .line 993
    :sswitch_49
    const-string v0, "REQUEST_STK_SEND_TERMINAL_RESPONSE"

    goto/16 :goto_0

    .line 995
    :sswitch_4a
    const-string v0, "REQUEST_STK_HANDLE_CALL_SETUP_REQUESTED_FROM_SIM"

    goto/16 :goto_0

    .line 997
    :sswitch_4b
    const-string v0, "REQUEST_EXPLICIT_CALL_TRANSFER"

    goto/16 :goto_0

    .line 999
    :sswitch_4c
    const-string v0, "REQUEST_SET_PREFERRED_NETWORK_TYPE"

    goto/16 :goto_0

    .line 1001
    :sswitch_4d
    const-string v0, "REQUEST_GET_PREFERRED_NETWORK_TYPE"

    goto/16 :goto_0

    .line 1003
    :sswitch_4e
    const-string v0, "REQUEST_GET_NEIGHBORING_CELL_IDS"

    goto/16 :goto_0

    .line 1005
    :sswitch_4f
    const-string v0, "REQUEST_SET_LOCATION_UPDATES"

    goto/16 :goto_0

    .line 1007
    :sswitch_50
    const-string v0, "RIL_REQUEST_CDMA_SET_SUBSCRIPTION"

    goto/16 :goto_0

    .line 1009
    :sswitch_51
    const-string v0, "RIL_REQUEST_CDMA_SET_ROAMING_PREFERENCE"

    goto/16 :goto_0

    .line 1011
    :sswitch_52
    const-string v0, "RIL_REQUEST_CDMA_QUERY_ROAMING_PREFERENCE"

    goto/16 :goto_0

    .line 1013
    :sswitch_53
    const-string v0, "RIL_REQUEST_SET_TTY_MODE"

    goto/16 :goto_0

    .line 1015
    :sswitch_54
    const-string v0, "RIL_REQUEST_QUERY_TTY_MODE"

    goto/16 :goto_0

    .line 1017
    :sswitch_55
    const-string v0, "RIL_REQUEST_CDMA_SET_PREFERRED_VOICE_PRIVACY_MODE"

    goto/16 :goto_0

    .line 1019
    :sswitch_56
    const-string v0, "RIL_REQUEST_CDMA_QUERY_PREFERRED_VOICE_PRIVACY_MODE"

    goto/16 :goto_0

    .line 1021
    :sswitch_57
    const-string v0, "RIL_REQUEST_CDMA_FLASH"

    goto/16 :goto_0

    .line 1023
    :sswitch_58
    const-string v0, "RIL_REQUEST_CDMA_BURST_DTMF"

    goto/16 :goto_0

    .line 1025
    :sswitch_59
    const-string v0, "RIL_REQUEST_CDMA_SEND_SMS"

    goto/16 :goto_0

    .line 1027
    :sswitch_5a
    const-string v0, "RIL_REQUEST_CDMA_SMS_ACKNOWLEDGE"

    goto/16 :goto_0

    .line 1034
    :sswitch_5b
    const-string v0, "RIL_REQUEST_SET_CELL_BROADCAST_CONFIG"

    goto/16 :goto_0

    .line 1036
    :sswitch_5c
    const-string v0, "RIL_REQUEST_GET_CELL_BROADCAST_CONFIG"

    goto/16 :goto_0

    .line 1039
    :sswitch_5d
    const-string v0, "RIL_REQUEST_CDMA_GET_BROADCAST_CONFIG"

    goto/16 :goto_0

    .line 1041
    :sswitch_5e
    const-string v0, "RIL_REQUEST_CDMA_SET_BROADCAST_CONFIG"

    goto/16 :goto_0

    .line 1043
    :sswitch_5f
    const-string v0, "RIL_REQUEST_GSM_BROADCAST_ACTIVATION"

    goto/16 :goto_0

    .line 1045
    :sswitch_60
    const-string v0, "RIL_REQUEST_CDMA_VALIDATE_AND_WRITE_AKEY"

    goto/16 :goto_0

    .line 1047
    :sswitch_61
    const-string v0, "RIL_REQUEST_CDMA_BROADCAST_ACTIVATION"

    goto/16 :goto_0

    .line 1049
    :sswitch_62
    const-string v0, "RIL_REQUEST_CDMA_SUBSCRIPTION"

    goto/16 :goto_0

    .line 1051
    :sswitch_63
    const-string v0, "RIL_REQUEST_CDMA_WRITE_SMS_TO_RUIM"

    goto/16 :goto_0

    .line 1053
    :sswitch_64
    const-string v0, "RIL_REQUEST_CDMA_DELETE_SMS_ON_RUIM"

    goto/16 :goto_0

    .line 1055
    :sswitch_65
    const-string v0, "RIL_REQUEST_DEVICE_IDENTITY"

    goto/16 :goto_0

    .line 1057
    :sswitch_66
    const-string v0, "RIL_REQUEST_GET_SMSC_ADDRESS"

    goto/16 :goto_0

    .line 1059
    :sswitch_67
    const-string v0, "RIL_REQUEST_SET_SMSC_ADDRESS"

    goto/16 :goto_0

    .line 1061
    :sswitch_68
    const-string v0, "REQUEST_EXIT_EMERGENCY_CALLBACK_MODE"

    goto/16 :goto_0

    .line 1063
    :sswitch_69
    const-string v0, "RIL_REQUEST_REPORT_SMS_MEMORY_STATUS"

    goto/16 :goto_0

    .line 1065
    :sswitch_6a
    const-string v0, "RIL_REQUEST_REPORT_STK_SERVICE_IS_RUNNING"

    goto/16 :goto_0

    .line 1069
    :sswitch_6b
    const-string v0, "GET_PHONEBOOK_STORAGE_INFO"

    goto/16 :goto_0

    .line 1071
    :sswitch_6c
    const-string v0, "GET_PHONEBOOK_ENTRY"

    goto/16 :goto_0

    .line 1073
    :sswitch_6d
    const-string v0, "ACCESS_PHONEBOOK_ENTRY"

    goto/16 :goto_0

    .line 1077
    :sswitch_6e
    const-string v0, "USIM_PB_CAPA"

    goto/16 :goto_0

    .line 1080
    :sswitch_6f
    const-string v0, "LOCK_INFO"

    goto/16 :goto_0

    .line 840
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_1
        0x3 -> :sswitch_2
        0x4 -> :sswitch_3
        0x5 -> :sswitch_4
        0x6 -> :sswitch_5
        0x7 -> :sswitch_6
        0x8 -> :sswitch_7
        0x9 -> :sswitch_8
        0xa -> :sswitch_9
        0xb -> :sswitch_b
        0xc -> :sswitch_c
        0xd -> :sswitch_d
        0xe -> :sswitch_e
        0xf -> :sswitch_f
        0x10 -> :sswitch_10
        0x11 -> :sswitch_12
        0x12 -> :sswitch_13
        0x13 -> :sswitch_14
        0x14 -> :sswitch_15
        0x15 -> :sswitch_16
        0x16 -> :sswitch_17
        0x17 -> :sswitch_18
        0x18 -> :sswitch_19
        0x19 -> :sswitch_1a
        0x1a -> :sswitch_1b
        0x1b -> :sswitch_1c
        0x1c -> :sswitch_1d
        0x1d -> :sswitch_1e
        0x1e -> :sswitch_20
        0x1f -> :sswitch_21
        0x20 -> :sswitch_22
        0x21 -> :sswitch_23
        0x22 -> :sswitch_24
        0x23 -> :sswitch_25
        0x24 -> :sswitch_26
        0x25 -> :sswitch_27
        0x26 -> :sswitch_28
        0x27 -> :sswitch_29
        0x28 -> :sswitch_2a
        0x29 -> :sswitch_2b
        0x2a -> :sswitch_2c
        0x2b -> :sswitch_2d
        0x2c -> :sswitch_2e
        0x2d -> :sswitch_2f
        0x2e -> :sswitch_30
        0x2f -> :sswitch_31
        0x30 -> :sswitch_32
        0x31 -> :sswitch_33
        0x32 -> :sswitch_34
        0x33 -> :sswitch_35
        0x34 -> :sswitch_36
        0x35 -> :sswitch_37
        0x36 -> :sswitch_38
        0x37 -> :sswitch_39
        0x38 -> :sswitch_3a
        0x39 -> :sswitch_3b
        0x3a -> :sswitch_3c
        0x3b -> :sswitch_3d
        0x3c -> :sswitch_3e
        0x3d -> :sswitch_3f
        0x3e -> :sswitch_40
        0x3f -> :sswitch_41
        0x40 -> :sswitch_43
        0x41 -> :sswitch_44
        0x42 -> :sswitch_45
        0x43 -> :sswitch_46
        0x44 -> :sswitch_47
        0x45 -> :sswitch_48
        0x46 -> :sswitch_49
        0x47 -> :sswitch_4a
        0x48 -> :sswitch_4b
        0x49 -> :sswitch_4c
        0x4a -> :sswitch_4d
        0x4b -> :sswitch_4e
        0x4c -> :sswitch_4f
        0x4d -> :sswitch_50
        0x4e -> :sswitch_51
        0x4f -> :sswitch_52
        0x50 -> :sswitch_53
        0x51 -> :sswitch_54
        0x52 -> :sswitch_55
        0x53 -> :sswitch_56
        0x54 -> :sswitch_57
        0x55 -> :sswitch_58
        0x56 -> :sswitch_60
        0x57 -> :sswitch_59
        0x58 -> :sswitch_5a
        0x5b -> :sswitch_5f
        0x5c -> :sswitch_5d
        0x5d -> :sswitch_5e
        0x5e -> :sswitch_61
        0x5f -> :sswitch_62
        0x60 -> :sswitch_63
        0x61 -> :sswitch_64
        0x62 -> :sswitch_65
        0x63 -> :sswitch_68
        0x64 -> :sswitch_66
        0x65 -> :sswitch_67
        0x66 -> :sswitch_69
        0x67 -> :sswitch_6a
        0x2711 -> :sswitch_5b
        0x2712 -> :sswitch_5c
        0x2715 -> :sswitch_1f
        0x2717 -> :sswitch_6b
        0x2718 -> :sswitch_6c
        0x2719 -> :sswitch_6d
        0x271a -> :sswitch_a
        0x271b -> :sswitch_11
        0x271c -> :sswitch_42
        0x271d -> :sswitch_6e
        0x271e -> :sswitch_6f
    .end sparse-switch
.end method

.method private responseRaw(Landroid/os/Parcel;Landroid/os/Message;)V
    .locals 3
    .param p1, "p"    # Landroid/os/Parcel;
    .param p2, "reply"    # Landroid/os/Message;

    .prologue
    .line 1449
    invoke-virtual {p1}, Landroid/os/Parcel;->createByteArray()[B

    move-result-object v0

    .line 1450
    .local v0, "response":[B
    invoke-virtual {p2}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "response"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V

    .line 1451
    return-void
.end method

.method private responseString(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1
    .param p1, "p"    # Landroid/os/Parcel;

    .prologue
    .line 1456
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 1458
    .local v0, "response":Ljava/lang/String;
    return-object v0
.end method

.method static responseToString(I)Ljava/lang/String;
    .locals 1
    .param p0, "request"    # I

    .prologue
    .line 1091
    sparse-switch p0, :sswitch_data_0

    .line 1167
    const-string v0, "<unknown reponse>"

    :goto_0
    return-object v0

    .line 1093
    :sswitch_0
    const-string v0, "UNSOL_RESPONSE_RADIO_STATE_CHANGED"

    goto :goto_0

    .line 1095
    :sswitch_1
    const-string v0, "UNSOL_RESPONSE_CALL_STATE_CHANGED"

    goto :goto_0

    .line 1097
    :sswitch_2
    const-string v0, "UNSOL_RESPONSE_NETWORK_STATE_CHANGED"

    goto :goto_0

    .line 1099
    :sswitch_3
    const-string v0, "UNSOL_RESPONSE_NEW_SMS"

    goto :goto_0

    .line 1101
    :sswitch_4
    const-string v0, "UNSOL_RESPONSE_NEW_SMS_STATUS_REPORT"

    goto :goto_0

    .line 1103
    :sswitch_5
    const-string v0, "UNSOL_RESPONSE_NEW_SMS_ON_SIM"

    goto :goto_0

    .line 1105
    :sswitch_6
    const-string v0, "UNSOL_ON_USSD"

    goto :goto_0

    .line 1107
    :sswitch_7
    const-string v0, "UNSOL_ON_USSD_REQUEST"

    goto :goto_0

    .line 1109
    :sswitch_8
    const-string v0, "UNSOL_NITZ_TIME_RECEIVED"

    goto :goto_0

    .line 1111
    :sswitch_9
    const-string v0, "UNSOL_SIGNAL_STRENGTH"

    goto :goto_0

    .line 1113
    :sswitch_a
    const-string v0, "UNSOL_DATA_CALL_LIST_CHANGED"

    goto :goto_0

    .line 1115
    :sswitch_b
    const-string v0, "UNSOL_SUPP_SVC_NOTIFICATION"

    goto :goto_0

    .line 1117
    :sswitch_c
    const-string v0, "UNSOL_STK_SESSION_END"

    goto :goto_0

    .line 1119
    :sswitch_d
    const-string v0, "UNSOL_STK_PROACTIVE_COMMAND"

    goto :goto_0

    .line 1121
    :sswitch_e
    const-string v0, "UNSOL_STK_EVENT_NOTIFY"

    goto :goto_0

    .line 1123
    :sswitch_f
    const-string v0, "UNSOL_STK_CALL_SETUP"

    goto :goto_0

    .line 1125
    :sswitch_10
    const-string v0, "UNSOL_SIM_SMS_STORAGE_FULL"

    goto :goto_0

    .line 1127
    :sswitch_11
    const-string v0, "UNSOL_SIM_REFRESH"

    goto :goto_0

    .line 1129
    :sswitch_12
    const-string v0, "UNSOL_CALL_RING"

    goto :goto_0

    .line 1131
    :sswitch_13
    const-string v0, "UNSOL_RESPONSE_SIM_STATUS_CHANGED"

    goto :goto_0

    .line 1133
    :sswitch_14
    const-string v0, "UNSOL_RESPONSE_CDMA_NEW_SMS"

    goto :goto_0

    .line 1135
    :sswitch_15
    const-string v0, "UNSOL_RESPONSE_NEW_BROADCAST_SMS"

    goto :goto_0

    .line 1137
    :sswitch_16
    const-string v0, "UNSOL_CDMA_RUIM_SMS_STORAGE_FULL"

    goto :goto_0

    .line 1139
    :sswitch_17
    const-string v0, "UNSOL_RESTRICTED_STATE_CHANGED"

    goto :goto_0

    .line 1141
    :sswitch_18
    const-string v0, "UNSOL_ENTER_EMERGENCY_CALLBACK_MODE"

    goto :goto_0

    .line 1143
    :sswitch_19
    const-string v0, "UNSOL_CDMA_CALL_WAITING"

    goto :goto_0

    .line 1145
    :sswitch_1a
    const-string v0, "UNSOL_CDMA_OTA_PROVISION_STATUS"

    goto :goto_0

    .line 1147
    :sswitch_1b
    const-string v0, "UNSOL_CDMA_INFO_REC"

    goto :goto_0

    .line 1149
    :sswitch_1c
    const-string v0, "UNSOL_OEM_HOOK_RAW"

    goto :goto_0

    .line 1151
    :sswitch_1d
    const-string v0, "UNSOL_RINGBACK_TONG"

    goto :goto_0

    .line 1153
    :sswitch_1e
    const-string v0, "UNSOL_RELEASE_COMPLETE_MESSAGE"

    goto :goto_0

    .line 1155
    :sswitch_1f
    const-string v0, "UNSOL_STK_SEND_SMS_RESULT"

    goto :goto_0

    .line 1157
    :sswitch_20
    const-string v0, "UNSOL_STK_CALL_CONTROL_RESULT"

    goto :goto_0

    .line 1159
    :sswitch_21
    const-string v0, "UNSOL_O2_HOME_ZONE_INFO"

    goto :goto_0

    .line 1162
    :sswitch_22
    const-string v0, "UNSOL_DEVICE_READY_NOTI"

    goto :goto_0

    .line 1164
    :sswitch_23
    const-string v0, "RIL_UNSOL_RESPONSE_NEW_CB_MSG"

    goto :goto_0

    .line 1091
    :sswitch_data_0
    .sparse-switch
        0x3e8 -> :sswitch_0
        0x3e9 -> :sswitch_1
        0x3ea -> :sswitch_2
        0x3eb -> :sswitch_3
        0x3ec -> :sswitch_4
        0x3ed -> :sswitch_5
        0x3ee -> :sswitch_6
        0x3ef -> :sswitch_7
        0x3f0 -> :sswitch_8
        0x3f1 -> :sswitch_9
        0x3f2 -> :sswitch_a
        0x3f3 -> :sswitch_b
        0x3f4 -> :sswitch_c
        0x3f5 -> :sswitch_d
        0x3f6 -> :sswitch_e
        0x3f7 -> :sswitch_f
        0x3f8 -> :sswitch_10
        0x3f9 -> :sswitch_11
        0x3fa -> :sswitch_12
        0x3fb -> :sswitch_13
        0x3fc -> :sswitch_14
        0x3fd -> :sswitch_15
        0x3fe -> :sswitch_16
        0x3ff -> :sswitch_17
        0x400 -> :sswitch_18
        0x401 -> :sswitch_19
        0x402 -> :sswitch_1a
        0x403 -> :sswitch_1b
        0x404 -> :sswitch_1c
        0x405 -> :sswitch_1d
        0x2af8 -> :sswitch_23
        0x2af9 -> :sswitch_1e
        0x2afa -> :sswitch_1f
        0x2afb -> :sswitch_20
        0x2aff -> :sswitch_21
        0x2b00 -> :sswitch_22
    .end sparse-switch
.end method

.method private retToString(ILjava/lang/Object;)Ljava/lang/String;
    .locals 9
    .param p1, "req"    # I
    .param p2, "ret"    # Ljava/lang/Object;

    .prologue
    .line 1301
    if-nez p2, :cond_0

    .line 1302
    const-string v4, ""

    .line 1343
    .end local p2    # "ret":Ljava/lang/Object;
    :goto_0
    return-object v4

    .line 1303
    .restart local p2    # "ret":Ljava/lang/Object;
    :cond_0
    sparse-switch p1, :sswitch_data_0

    .line 1314
    instance-of v7, p2, [I

    if-eqz v7, :cond_2

    .line 1315
    check-cast p2, [I

    .end local p2    # "ret":Ljava/lang/Object;
    move-object v2, p2

    check-cast v2, [I

    .line 1316
    .local v2, "intArray":[I
    array-length v3, v2

    .line 1317
    .local v3, "length":I
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v7, "{"

    invoke-direct {v5, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1318
    .local v5, "sb":Ljava/lang/StringBuilder;
    if-lez v3, :cond_1

    .line 1319
    const/4 v0, 0x0

    .line 1320
    .local v0, "i":I
    add-int/lit8 v1, v0, 0x1

    .end local v0    # "i":I
    .local v1, "i":I
    aget v7, v2, v0

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 1321
    :goto_1
    if-ge v1, v3, :cond_1

    .line 1322
    const-string v7, ", "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    add-int/lit8 v0, v1, 0x1

    .end local v1    # "i":I
    .restart local v0    # "i":I
    aget v8, v2, v1

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move v1, v0

    .end local v0    # "i":I
    .restart local v1    # "i":I
    goto :goto_1

    .line 1308
    .end local v1    # "i":I
    .end local v2    # "intArray":[I
    .end local v3    # "length":I
    .end local v5    # "sb":Ljava/lang/StringBuilder;
    .restart local p2    # "ret":Ljava/lang/Object;
    :sswitch_0
    const-string v4, ""

    goto :goto_0

    .line 1325
    .end local p2    # "ret":Ljava/lang/Object;
    .restart local v2    # "intArray":[I
    .restart local v3    # "length":I
    .restart local v5    # "sb":Ljava/lang/StringBuilder;
    :cond_1
    const-string v7, "}"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1326
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 1327
    .local v4, "s":Ljava/lang/String;
    goto :goto_0

    .end local v2    # "intArray":[I
    .end local v3    # "length":I
    .end local v4    # "s":Ljava/lang/String;
    .end local v5    # "sb":Ljava/lang/StringBuilder;
    .restart local p2    # "ret":Ljava/lang/Object;
    :cond_2
    instance-of v7, p2, [Ljava/lang/String;

    if-eqz v7, :cond_4

    .line 1328
    check-cast p2, [Ljava/lang/String;

    .end local p2    # "ret":Ljava/lang/Object;
    move-object v6, p2

    check-cast v6, [Ljava/lang/String;

    .line 1329
    .local v6, "strings":[Ljava/lang/String;
    array-length v3, v6

    .line 1330
    .restart local v3    # "length":I
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v7, "{"

    invoke-direct {v5, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1331
    .restart local v5    # "sb":Ljava/lang/StringBuilder;
    if-lez v3, :cond_3

    .line 1332
    const/4 v0, 0x0

    .line 1333
    .restart local v0    # "i":I
    add-int/lit8 v1, v0, 0x1

    .end local v0    # "i":I
    .restart local v1    # "i":I
    aget-object v7, v6, v0

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1334
    :goto_2
    if-ge v1, v3, :cond_3

    .line 1335
    const-string v7, ", "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    add-int/lit8 v0, v1, 0x1

    .end local v1    # "i":I
    .restart local v0    # "i":I
    aget-object v8, v6, v1

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move v1, v0

    .end local v0    # "i":I
    .restart local v1    # "i":I
    goto :goto_2

    .line 1338
    .end local v1    # "i":I
    :cond_3
    const-string v7, "}"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1339
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 1340
    .restart local v4    # "s":Ljava/lang/String;
    goto :goto_0

    .line 1341
    .end local v3    # "length":I
    .end local v4    # "s":Ljava/lang/String;
    .end local v5    # "sb":Ljava/lang/StringBuilder;
    .end local v6    # "strings":[Ljava/lang/String;
    .restart local p2    # "ret":Ljava/lang/Object;
    :cond_4
    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    .restart local v4    # "s":Ljava/lang/String;
    goto :goto_0

    .line 1303
    :sswitch_data_0
    .sparse-switch
        0xb -> :sswitch_0
        0x26 -> :sswitch_0
        0x27 -> :sswitch_0
    .end sparse-switch
.end method

.method private routeDataToITelephony([B)Ljava/lang/reflect/Method;
    .locals 14
    .param p1, "data"    # [B

    .prologue
    const/4 v13, 0x2

    const/4 v12, 0x0

    const/4 v5, 0x0

    const/4 v11, 0x1

    .line 733
    const/4 v0, 0x6

    .line 734
    .local v0, "OEM_FUNCTION_ID_IMEI":B
    const/4 v1, 0x5

    .line 736
    .local v1, "OEM_IMEI_EVENT_START_IMEI":B
    if-eqz p1, :cond_0

    array-length v6, p1

    const/4 v7, 0x4

    if-ge v6, v7, :cond_2

    :cond_0
    move-object v3, v5

    .line 786
    :cond_1
    :goto_0
    return-object v3

    .line 741
    :cond_2
    const/4 v3, 0x0

    .line 742
    .local v3, "oemHookMethod":Ljava/lang/reflect/Method;
    const/4 v4, 0x0

    .line 744
    .local v4, "phoneServ":Lcom/android/internal/telephony/ITelephony;
    :try_start_0
    invoke-direct {p0}, Lcom/sec/phone/SecPhoneServiceCPOnUPG;->getTelephonyService()Lcom/android/internal/telephony/ITelephony;

    move-result-object v4

    .line 745
    if-eqz v4, :cond_3

    .line 746
    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v6

    const-string v7, "sendOemRilRequestRaw"

    const/4 v8, 0x2

    new-array v8, v8, [Ljava/lang/Class;

    const/4 v9, 0x0

    const-class v10, [B

    aput-object v10, v8, v9

    const/4 v9, 0x1

    const-class v10, [B

    aput-object v10, v8, v9

    invoke-virtual {v6, v7, v8}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    .line 756
    aget-byte v6, p1, v12

    sparse-switch v6, :sswitch_data_0

    move-object v3, v5

    .line 786
    goto :goto_0

    :cond_3
    move-object v3, v5

    .line 749
    goto :goto_0

    .line 751
    :catch_0
    move-exception v2

    .local v2, "ex":Ljava/lang/NoSuchMethodException;
    move-object v3, v5

    .line 752
    goto :goto_0

    .line 758
    .end local v2    # "ex":Ljava/lang/NoSuchMethodException;
    :sswitch_0
    aget-byte v6, p1, v11

    const/4 v7, 0x6

    if-eq v6, v7, :cond_1

    aget-byte v6, p1, v11

    const/4 v7, 0x7

    if-eq v6, v7, :cond_1

    move-object v3, v5

    .line 761
    goto :goto_0

    .line 764
    :sswitch_1
    aget-byte v6, p1, v11

    if-eq v6, v11, :cond_1

    aget-byte v6, p1, v11

    if-eq v6, v13, :cond_1

    move-object v3, v5

    .line 767
    goto :goto_0

    .line 770
    :sswitch_2
    sget-boolean v6, Lcom/sec/phone/SecPhoneServiceCPOnUPG;->isMarvell:Z

    if-eqz v6, :cond_4

    .line 771
    aget-byte v6, p1, v11

    const/4 v7, 0x5

    if-eq v6, v7, :cond_1

    :cond_4
    move-object v3, v5

    .line 775
    goto :goto_0

    .line 778
    :sswitch_3
    sget-boolean v6, Lcom/sec/phone/SecPhoneServiceCPOnUPG;->isMarvell:Z

    if-eqz v6, :cond_5

    .line 779
    aget-byte v6, p1, v11

    const/16 v7, 0x15

    if-eq v6, v7, :cond_1

    aget-byte v6, p1, v11

    const/16 v7, 0x16

    if-eq v6, v7, :cond_1

    :cond_5
    move-object v3, v5

    .line 783
    goto :goto_0

    .line 756
    :sswitch_data_0
    .sparse-switch
        0x6 -> :sswitch_2
        0x7 -> :sswitch_3
        0x11 -> :sswitch_0
        0x22 -> :sswitch_1
    .end sparse-switch
.end method

.method private send(Lcom/sec/phone/RILRequest;)V
    .locals 3
    .param p1, "rr"    # Lcom/sec/phone/RILRequest;

    .prologue
    .line 1194
    iget-object v1, p0, Lcom/sec/phone/SecPhoneServiceCPOnUPG;->mSender:Lcom/sec/phone/SecPhoneServiceCPOnUPG$RILSender;

    const/4 v2, 0x1

    invoke-virtual {v1, v2, p1}, Lcom/sec/phone/SecPhoneServiceCPOnUPG$RILSender;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 1196
    .local v0, "msg":Landroid/os/Message;
    invoke-direct {p0}, Lcom/sec/phone/SecPhoneServiceCPOnUPG;->acquireWakeLock()V

    .line 1198
    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 1199
    return-void
.end method

.method private sendIntentSecPhoneReady(Ljava/lang/String;)V
    .locals 7
    .param p1, "action"    # Ljava/lang/String;

    .prologue
    .line 701
    iget-object v4, p0, Lcom/sec/phone/SecPhoneServiceCPOnUPG;->LOG_TAG:Ljava/lang/String;

    const-string v5, "intent for SECPHONE_READY"

    invoke-static {v4, v5}, Landroid/telephony/Rlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 702
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1, p1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 703
    .local v1, "i":Landroid/content/Intent;
    invoke-virtual {p0, v1}, Lcom/sec/phone/SecPhoneServiceCPOnUPG;->sendBroadcast(Landroid/content/Intent;)V

    .line 706
    const/16 v4, 0x3b

    const/4 v5, 0x0

    invoke-static {v4, v5}, Lcom/sec/phone/RILRequest;->obtain(ILandroid/os/Message;)Lcom/sec/phone/RILRequest;

    move-result-object v3

    .line 707
    .local v3, "rr":Lcom/sec/phone/RILRequest;
    new-instance v2, Lcom/samsung/android/sec_platform_library/PacketBuilder;

    const/16 v4, 0x12

    const/16 v5, 0xa

    invoke-direct {v2, v4, v5}, Lcom/samsung/android/sec_platform_library/PacketBuilder;-><init>(BB)V

    .line 709
    .local v2, "packet":Lcom/samsung/android/sec_platform_library/PacketBuilder;
    invoke-virtual {v2}, Lcom/samsung/android/sec_platform_library/PacketBuilder;->getPacket()[B

    move-result-object v0

    .line 710
    .local v0, "data":[B
    iget-object v4, p0, Lcom/sec/phone/SecPhoneServiceCPOnUPG;->LOG_TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3}, Lcom/sec/phone/RILRequest;->serialString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "> "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, v3, Lcom/sec/phone/RILRequest;->mRequest:I

    invoke-static {v6}, Lcom/sec/phone/SecPhoneServiceCPOnUPG;->requestToString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {v0}, Lcom/sec/phone/SecPhoneServiceCPOnUPG;->bytesToHexString([B)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "]"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/telephony/Rlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 712
    iget-object v4, v3, Lcom/sec/phone/RILRequest;->mp:Landroid/os/Parcel;

    invoke-virtual {v2}, Lcom/samsung/android/sec_platform_library/PacketBuilder;->getPacket()[B

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/os/Parcel;->writeByteArray([B)V

    .line 713
    invoke-direct {p0, v3}, Lcom/sec/phone/SecPhoneServiceCPOnUPG;->send(Lcom/sec/phone/RILRequest;)V

    .line 714
    return-void
.end method

.method private sendMessage(Ljava/lang/String;)V
    .locals 8
    .param p1, "data"    # Ljava/lang/String;

    .prologue
    const/4 v7, 0x5

    .line 245
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 246
    .local v2, "req":Landroid/os/Bundle;
    const-string v4, " "

    invoke-virtual {p1, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 247
    .local v0, "Args":[Ljava/lang/String;
    const-string v4, "action"

    const/4 v5, 0x2

    aget-object v5, v0, v5

    invoke-virtual {v2, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 248
    const-string v4, "COMMAND"

    aget-object v5, v0, v7

    invoke-virtual {v2, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 250
    iget-object v4, p0, Lcom/sec/phone/SecPhoneServiceCPOnUPG;->LOG_TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "RilDFTCommand & RilCommand sendMessage COMMAND = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    aget-object v6, v0, v7

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/telephony/Rlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 251
    iget-object v4, p0, Lcom/sec/phone/SecPhoneServiceCPOnUPG;->LOG_TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Full CMD : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/telephony/Rlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 252
    new-instance v3, Landroid/os/Message;

    invoke-direct {v3}, Landroid/os/Message;-><init>()V

    .line 254
    .local v3, "response":Landroid/os/Message;
    invoke-virtual {v3, v2}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 257
    :try_start_0
    iget-object v4, p0, Lcom/sec/phone/SecPhoneServiceCPOnUPG;->mSendServiceMessenger:Landroid/os/Messenger;

    if-eqz v4, :cond_0

    .line 258
    iget-object v4, p0, Lcom/sec/phone/SecPhoneServiceCPOnUPG;->mSendServiceMessenger:Landroid/os/Messenger;

    invoke-virtual {v4, v3}, Landroid/os/Messenger;->send(Landroid/os/Message;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 263
    :cond_0
    :goto_0
    return-void

    .line 260
    :catch_0
    move-exception v1

    .line 261
    .local v1, "e":Landroid/os/RemoteException;
    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method


# virtual methods
.method public GetActionFromRilResponse([Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1, "Args"    # [Ljava/lang/String;

    .prologue
    .line 1433
    const-string v2, ""

    .line 1435
    .local v2, "temp_action":Ljava/lang/String;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v3, p1

    if-ge v0, v3, :cond_0

    .line 1436
    aget-object v1, p1, v0

    .line 1437
    .local v1, "temp":Ljava/lang/String;
    const-string v3, "-a"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1438
    add-int/lit8 v3, v0, 0x1

    aget-object v2, p1, v3

    .line 1443
    .end local v1    # "temp":Ljava/lang/String;
    :cond_0
    return-object v2

    .line 1435
    .restart local v1    # "temp":Ljava/lang/String;
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public invokeOemRilRequestRaw(Landroid/os/Message;)V
    .locals 14
    .param p1, "response"    # Landroid/os/Message;

    .prologue
    const/4 v13, 0x1

    const/4 v12, 0x2

    const/4 v9, 0x0

    .line 791
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v8

    const-string v10, "request"

    invoke-virtual {v8, v10}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v0

    .line 792
    .local v0, "data":[B
    invoke-direct {p0, v0}, Lcom/sec/phone/SecPhoneServiceCPOnUPG;->routeDataToITelephony([B)Ljava/lang/reflect/Method;

    move-result-object v3

    .line 793
    .local v3, "oemHookMethod":Ljava/lang/reflect/Method;
    if-nez v3, :cond_1

    .line 795
    const/16 v8, 0x3b

    invoke-static {v8, p1}, Lcom/sec/phone/RILRequest;->obtain(ILandroid/os/Message;)Lcom/sec/phone/RILRequest;

    move-result-object v7

    .line 796
    .local v7, "rr":Lcom/sec/phone/RILRequest;
    iget-object v8, p0, Lcom/sec/phone/SecPhoneServiceCPOnUPG;->LOG_TAG:Ljava/lang/String;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7}, Lcom/sec/phone/RILRequest;->serialString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "> "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget v11, v7, Lcom/sec/phone/RILRequest;->mRequest:I

    invoke-static {v11}, Lcom/sec/phone/SecPhoneServiceCPOnUPG;->requestToString(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "["

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-static {v0}, Lcom/sec/phone/SecPhoneServiceCPOnUPG;->bytesToHexString([B)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "]"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v8, v10}, Landroid/telephony/Rlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 800
    const-string v8, "ril.dumpstate"

    invoke-static {v8, v9}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    .line 802
    .local v2, "isCPDumpState":Z
    if-eqz v2, :cond_0

    .line 803
    iget-object v8, p0, Lcom/sec/phone/SecPhoneServiceCPOnUPG;->LOG_TAG:Ljava/lang/String;

    const-string v9, "Can not process Ril Request during CP dump"

    invoke-static {v8, v9}, Landroid/telephony/Rlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 804
    const/4 v8, 0x0

    invoke-virtual {v7, v13, v8}, Lcom/sec/phone/RILRequest;->onError(ILjava/lang/Object;)V

    .line 805
    invoke-virtual {v7}, Lcom/sec/phone/RILRequest;->release()V

    .line 833
    .end local v2    # "isCPDumpState":Z
    .end local v7    # "rr":Lcom/sec/phone/RILRequest;
    :goto_0
    return-void

    .line 807
    .restart local v2    # "isCPDumpState":Z
    .restart local v7    # "rr":Lcom/sec/phone/RILRequest;
    :cond_0
    iget-object v8, v7, Lcom/sec/phone/RILRequest;->mp:Landroid/os/Parcel;

    invoke-virtual {v8, v0}, Landroid/os/Parcel;->writeByteArray([B)V

    .line 808
    invoke-direct {p0, v7}, Lcom/sec/phone/SecPhoneServiceCPOnUPG;->send(Lcom/sec/phone/RILRequest;)V

    goto :goto_0

    .line 812
    .end local v2    # "isCPDumpState":Z
    .end local v7    # "rr":Lcom/sec/phone/RILRequest;
    :cond_1
    const/16 v8, 0xc8

    new-array v4, v8, [B

    .line 813
    .local v4, "oemHookResp":[B
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v5

    .line 814
    .local v5, "reply":Landroid/os/Message;
    invoke-virtual {v5, p1}, Landroid/os/Message;->copyFrom(Landroid/os/Message;)V

    .line 817
    :try_start_0
    invoke-direct {p0}, Lcom/sec/phone/SecPhoneServiceCPOnUPG;->getTelephonyService()Lcom/android/internal/telephony/ITelephony;

    move-result-object v8

    const/4 v10, 0x2

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    aput-object v0, v10, v11

    const/4 v11, 0x1

    aput-object v4, v10, v11

    invoke-virtual {v3, v8, v10}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/Integer;

    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v6

    .line 818
    .local v6, "ret":I
    invoke-virtual {v5}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v10

    const-string v11, "error"

    if-ltz v6, :cond_3

    move v8, v9

    :goto_1
    invoke-virtual {v10, v11, v8}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 819
    if-lez v6, :cond_2

    .line 820
    invoke-virtual {v5}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v8

    const-string v9, "response"

    const/4 v10, 0x0

    invoke-static {v4, v10, v6}, Ljava/util/Arrays;->copyOfRange([BII)[B

    move-result-object v10

    invoke-virtual {v8, v9, v10}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V
    :try_end_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_2

    .line 829
    .end local v6    # "ret":I
    :cond_2
    :goto_2
    :try_start_1
    iget-object v8, p1, Landroid/os/Message;->replyTo:Landroid/os/Messenger;

    invoke-virtual {v8, v5}, Landroid/os/Messenger;->send(Landroid/os/Message;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 830
    :catch_0
    move-exception v8

    goto :goto_0

    .restart local v6    # "ret":I
    :cond_3
    move v8, v6

    .line 818
    goto :goto_1

    .line 822
    .end local v6    # "ret":I
    :catch_1
    move-exception v1

    .line 823
    .local v1, "e":Ljava/lang/IllegalAccessException;
    invoke-virtual {v5}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v8

    const-string v9, "error"

    invoke-virtual {v8, v9, v12}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    goto :goto_2

    .line 824
    .end local v1    # "e":Ljava/lang/IllegalAccessException;
    :catch_2
    move-exception v1

    .line 825
    .local v1, "e":Ljava/lang/reflect/InvocationTargetException;
    invoke-virtual {v5}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v8

    const-string v9, "error"

    invoke-virtual {v8, v9, v12}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    goto :goto_2
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 2
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 333
    iget-object v0, p0, Lcom/sec/phone/SecPhoneServiceCPOnUPG;->LOG_TAG:Ljava/lang/String;

    const-string v1, "onBind()"

    invoke-static {v0, v1}, Landroid/telephony/Rlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 334
    iget-object v0, p0, Lcom/sec/phone/SecPhoneServiceCPOnUPG;->mMessenger:Landroid/os/Messenger;

    invoke-virtual {v0}, Landroid/os/Messenger;->getBinder()Landroid/os/IBinder;

    move-result-object v0

    return-object v0
.end method

.method public onCreate()V
    .locals 10

    .prologue
    const/4 v9, 0x0

    .line 269
    const-string v7, "power"

    invoke-virtual {p0, v7}, Lcom/sec/phone/SecPhoneServiceCPOnUPG;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/os/PowerManager;

    .line 270
    .local v6, "pm":Landroid/os/PowerManager;
    const/4 v7, 0x1

    iget-object v8, p0, Lcom/sec/phone/SecPhoneServiceCPOnUPG;->LOG_TAG:Ljava/lang/String;

    invoke-virtual {v6, v7, v8}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v7

    iput-object v7, p0, Lcom/sec/phone/SecPhoneServiceCPOnUPG;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    .line 271
    iget-object v7, p0, Lcom/sec/phone/SecPhoneServiceCPOnUPG;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v7, v9}, Landroid/os/PowerManager$WakeLock;->setReferenceCounted(Z)V

    .line 272
    iput v9, p0, Lcom/sec/phone/SecPhoneServiceCPOnUPG;->mRequestMessagesPending:I

    .line 274
    new-instance v7, Landroid/os/HandlerThread;

    const-string v8, "RILSender"

    invoke-direct {v7, v8}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    iput-object v7, p0, Lcom/sec/phone/SecPhoneServiceCPOnUPG;->mSenderThread:Landroid/os/HandlerThread;

    .line 275
    iget-object v7, p0, Lcom/sec/phone/SecPhoneServiceCPOnUPG;->mSenderThread:Landroid/os/HandlerThread;

    invoke-virtual {v7}, Landroid/os/HandlerThread;->start()V

    .line 277
    iget-object v7, p0, Lcom/sec/phone/SecPhoneServiceCPOnUPG;->mSenderThread:Landroid/os/HandlerThread;

    invoke-virtual {v7}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v5

    .line 278
    .local v5, "looper":Landroid/os/Looper;
    new-instance v7, Lcom/sec/phone/SecPhoneServiceCPOnUPG$RILSender;

    invoke-direct {v7, p0, v5}, Lcom/sec/phone/SecPhoneServiceCPOnUPG$RILSender;-><init>(Lcom/sec/phone/SecPhoneServiceCPOnUPG;Landroid/os/Looper;)V

    iput-object v7, p0, Lcom/sec/phone/SecPhoneServiceCPOnUPG;->mSender:Lcom/sec/phone/SecPhoneServiceCPOnUPG$RILSender;

    .line 280
    new-instance v7, Lcom/sec/phone/SecPhoneServiceCPOnUPG$RILReceiver;

    invoke-direct {v7, p0}, Lcom/sec/phone/SecPhoneServiceCPOnUPG$RILReceiver;-><init>(Lcom/sec/phone/SecPhoneServiceCPOnUPG;)V

    iput-object v7, p0, Lcom/sec/phone/SecPhoneServiceCPOnUPG;->mReceiver:Lcom/sec/phone/SecPhoneServiceCPOnUPG$RILReceiver;

    .line 281
    new-instance v7, Ljava/lang/Thread;

    iget-object v8, p0, Lcom/sec/phone/SecPhoneServiceCPOnUPG;->mReceiver:Lcom/sec/phone/SecPhoneServiceCPOnUPG$RILReceiver;

    const-string v9, "RILReceiver"

    invoke-direct {v7, v8, v9}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V

    iput-object v7, p0, Lcom/sec/phone/SecPhoneServiceCPOnUPG;->mReceiverThread:Ljava/lang/Thread;

    .line 282
    iget-object v7, p0, Lcom/sec/phone/SecPhoneServiceCPOnUPG;->mReceiverThread:Ljava/lang/Thread;

    invoke-virtual {v7}, Ljava/lang/Thread;->start()V

    .line 288
    const/4 v4, 0x0

    .line 297
    .local v4, "isAPO":Z
    sget-boolean v7, Lcom/sec/phone/SecPhoneServiceCPOnUPG;->isMarvell:Z

    if-eqz v7, :cond_1

    .line 298
    const/4 v4, 0x1

    .line 329
    :cond_0
    :goto_0
    return-void

    .line 301
    :cond_1
    const/4 v2, 0x0

    .line 304
    .local v2, "fr":Ljava/io/FileReader;
    :try_start_0
    new-instance v3, Ljava/io/FileReader;

    const-string v7, "system/bin/at_distributor"

    invoke-direct {v3, v7}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 305
    .end local v2    # "fr":Ljava/io/FileReader;
    .local v3, "fr":Ljava/io/FileReader;
    const/4 v4, 0x1

    .line 306
    :try_start_1
    const-string v7, "APO/CPO"

    const-string v8, "Found the at_distributor. Decided APO mode."

    invoke-static {v7, v8}, Landroid/telephony/Rlog;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_7
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_6
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 313
    if-eqz v3, :cond_4

    .line 315
    :try_start_2
    invoke-virtual {v3}, Ljava/io/FileReader;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    move-object v2, v3

    .line 322
    .end local v3    # "fr":Ljava/io/FileReader;
    .restart local v2    # "fr":Ljava/io/FileReader;
    :cond_2
    :goto_1
    if-nez v4, :cond_0

    .line 323
    invoke-direct {p0}, Lcom/sec/phone/SecPhoneServiceCPOnUPG;->connectToFactoryTestClientServiceCPO()V

    goto :goto_0

    .line 316
    .end local v2    # "fr":Ljava/io/FileReader;
    .restart local v3    # "fr":Ljava/io/FileReader;
    :catch_0
    move-exception v0

    .line 317
    .local v0, "e":Ljava/io/IOException;
    const-string v7, "APO/CPO"

    const-string v8, "File Close error"

    invoke-static {v7, v8}, Landroid/telephony/Rlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    move-object v2, v3

    .line 318
    .end local v3    # "fr":Ljava/io/FileReader;
    .restart local v2    # "fr":Ljava/io/FileReader;
    goto :goto_1

    .line 307
    .end local v0    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v1

    .line 308
    .local v1, "ex":Ljava/io/FileNotFoundException;
    :goto_2
    const/4 v4, 0x0

    .line 309
    :try_start_3
    const-string v7, "APO/CPO"

    const-string v8, "Can not find at_distributor. Decided CPO mode"

    invoke-static {v7, v8}, Landroid/telephony/Rlog;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 313
    if-eqz v2, :cond_2

    .line 315
    :try_start_4
    invoke-virtual {v2}, Ljava/io/FileReader;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_1

    .line 316
    :catch_2
    move-exception v0

    .line 317
    .restart local v0    # "e":Ljava/io/IOException;
    const-string v7, "APO/CPO"

    const-string v8, "File Close error"

    invoke-static {v7, v8}, Landroid/telephony/Rlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 310
    .end local v0    # "e":Ljava/io/IOException;
    .end local v1    # "ex":Ljava/io/FileNotFoundException;
    :catch_3
    move-exception v0

    .line 311
    .local v0, "e":Ljava/lang/Exception;
    :goto_3
    :try_start_5
    const-string v7, "APO/CPO"

    const-string v8, "Can not decide because of error"

    invoke-static {v7, v8}, Landroid/telephony/Rlog;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 313
    if-eqz v2, :cond_2

    .line 315
    :try_start_6
    invoke-virtual {v2}, Ljava/io/FileReader;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_4

    goto :goto_1

    .line 316
    :catch_4
    move-exception v0

    .line 317
    .local v0, "e":Ljava/io/IOException;
    const-string v7, "APO/CPO"

    const-string v8, "File Close error"

    invoke-static {v7, v8}, Landroid/telephony/Rlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 313
    .end local v0    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v7

    :goto_4
    if-eqz v2, :cond_3

    .line 315
    :try_start_7
    invoke-virtual {v2}, Ljava/io/FileReader;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_5

    .line 318
    :cond_3
    :goto_5
    throw v7

    .line 316
    :catch_5
    move-exception v0

    .line 317
    .restart local v0    # "e":Ljava/io/IOException;
    const-string v8, "APO/CPO"

    const-string v9, "File Close error"

    invoke-static {v8, v9}, Landroid/telephony/Rlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_5

    .line 313
    .end local v0    # "e":Ljava/io/IOException;
    .end local v2    # "fr":Ljava/io/FileReader;
    .restart local v3    # "fr":Ljava/io/FileReader;
    :catchall_1
    move-exception v7

    move-object v2, v3

    .end local v3    # "fr":Ljava/io/FileReader;
    .restart local v2    # "fr":Ljava/io/FileReader;
    goto :goto_4

    .line 310
    .end local v2    # "fr":Ljava/io/FileReader;
    .restart local v3    # "fr":Ljava/io/FileReader;
    :catch_6
    move-exception v0

    move-object v2, v3

    .end local v3    # "fr":Ljava/io/FileReader;
    .restart local v2    # "fr":Ljava/io/FileReader;
    goto :goto_3

    .line 307
    .end local v2    # "fr":Ljava/io/FileReader;
    .restart local v3    # "fr":Ljava/io/FileReader;
    :catch_7
    move-exception v1

    move-object v2, v3

    .end local v3    # "fr":Ljava/io/FileReader;
    .restart local v2    # "fr":Ljava/io/FileReader;
    goto :goto_2

    .end local v2    # "fr":Ljava/io/FileReader;
    .restart local v3    # "fr":Ljava/io/FileReader;
    :cond_4
    move-object v2, v3

    .end local v3    # "fr":Ljava/io/FileReader;
    .restart local v2    # "fr":Ljava/io/FileReader;
    goto :goto_1
.end method

.method public onDestroy()V
    .locals 3

    .prologue
    .line 345
    iget-object v1, p0, Lcom/sec/phone/SecPhoneServiceCPOnUPG;->LOG_TAG:Ljava/lang/String;

    const-string v2, "onDestroy()"

    invoke-static {v1, v2}, Landroid/telephony/Rlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 346
    iget-object v1, p0, Lcom/sec/phone/SecPhoneServiceCPOnUPG;->mSenderThread:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->quit()Z

    .line 348
    iget-object v1, p0, Lcom/sec/phone/SecPhoneServiceCPOnUPG;->mReceiverThread:Ljava/lang/Thread;

    invoke-virtual {v1}, Ljava/lang/Thread;->interrupt()V

    .line 351
    :try_start_0
    iget-object v1, p0, Lcom/sec/phone/SecPhoneServiceCPOnUPG;->mSocket:Landroid/net/LocalSocket;

    invoke-virtual {v1}, Landroid/net/LocalSocket;->shutdownInput()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 356
    :goto_0
    invoke-direct {p0}, Lcom/sec/phone/SecPhoneServiceCPOnUPG;->releaseWakeLockIfDone()V

    .line 358
    iget-object v1, p0, Lcom/sec/phone/SecPhoneServiceCPOnUPG;->mFTCServiceConnection:Landroid/content/ServiceConnection;

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/sec/phone/SecPhoneServiceCPOnUPG;->m_bBindDone:Z

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 360
    iget-object v1, p0, Lcom/sec/phone/SecPhoneServiceCPOnUPG;->mFTCServiceConnection:Landroid/content/ServiceConnection;

    invoke-virtual {p0, v1}, Lcom/sec/phone/SecPhoneServiceCPOnUPG;->unbindService(Landroid/content/ServiceConnection;)V

    .line 361
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/sec/phone/SecPhoneServiceCPOnUPG;->m_bBindDone:Z

    .line 363
    :cond_0
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 364
    return-void

    .line 352
    :catch_0
    move-exception v0

    .line 353
    .local v0, "ex":Ljava/io/IOException;
    iget-object v1, p0, Lcom/sec/phone/SecPhoneServiceCPOnUPG;->LOG_TAG:Ljava/lang/String;

    const-string v2, "Uncaught exception during shutdownInput"

    invoke-static {v1, v2, v0}, Landroid/telephony/Rlog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 2
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "flags"    # I
    .param p3, "startId"    # I

    .prologue
    .line 339
    iget-object v0, p0, Lcom/sec/phone/SecPhoneServiceCPOnUPG;->LOG_TAG:Ljava/lang/String;

    const-string v1, "onStartCommand()"

    invoke-static {v0, v1}, Landroid/telephony/Rlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 340
    const/4 v0, 0x2

    return v0
.end method
