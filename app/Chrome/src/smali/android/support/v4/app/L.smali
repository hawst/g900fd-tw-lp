.class public final Landroid/support/v4/app/L;
.super Ljava/lang/Object;
.source "NotificationCompat.java"


# instance fields
.field a:Landroid/content/Context;

.field b:Ljava/lang/CharSequence;

.field c:Ljava/lang/CharSequence;

.field d:Landroid/app/PendingIntent;

.field e:Ljava/lang/CharSequence;

.field f:I

.field g:Z

.field h:Landroid/support/v4/app/V;

.field i:I

.field j:I

.field k:Z

.field l:Ljava/util/ArrayList;

.field m:Z

.field n:I

.field o:I

.field p:Landroid/app/Notification;

.field public q:Ljava/util/ArrayList;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 876
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 844
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v4/app/L;->g:Z

    .line 854
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/support/v4/app/L;->l:Ljava/util/ArrayList;

    .line 855
    iput-boolean v4, p0, Landroid/support/v4/app/L;->m:Z

    .line 858
    iput v4, p0, Landroid/support/v4/app/L;->n:I

    .line 859
    iput v4, p0, Landroid/support/v4/app/L;->o:I

    .line 862
    new-instance v0, Landroid/app/Notification;

    invoke-direct {v0}, Landroid/app/Notification;-><init>()V

    iput-object v0, p0, Landroid/support/v4/app/L;->p:Landroid/app/Notification;

    .line 877
    iput-object p1, p0, Landroid/support/v4/app/L;->a:Landroid/content/Context;

    .line 880
    iget-object v0, p0, Landroid/support/v4/app/L;->p:Landroid/app/Notification;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iput-wide v2, v0, Landroid/app/Notification;->when:J

    .line 881
    iget-object v0, p0, Landroid/support/v4/app/L;->p:Landroid/app/Notification;

    const/4 v1, -0x1

    iput v1, v0, Landroid/app/Notification;->audioStreamType:I

    .line 882
    iput v4, p0, Landroid/support/v4/app/L;->f:I

    .line 883
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/support/v4/app/L;->q:Ljava/util/ArrayList;

    .line 884
    return-void
.end method

.method private a(IZ)V
    .locals 3

    .prologue
    .line 1232
    if-eqz p2, :cond_0

    .line 1233
    iget-object v0, p0, Landroid/support/v4/app/L;->p:Landroid/app/Notification;

    iget v1, v0, Landroid/app/Notification;->flags:I

    or-int/2addr v1, p1

    iput v1, v0, Landroid/app/Notification;->flags:I

    .line 1237
    :goto_0
    return-void

    .line 1235
    :cond_0
    iget-object v0, p0, Landroid/support/v4/app/L;->p:Landroid/app/Notification;

    iget v1, v0, Landroid/app/Notification;->flags:I

    xor-int/lit8 v2, p1, -0x1

    and-int/2addr v1, v2

    iput v1, v0, Landroid/app/Notification;->flags:I

    goto :goto_0
.end method

.method protected static e(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;
    .locals 2

    .prologue
    const/16 v1, 0x1400

    .line 1489
    if-nez p0, :cond_1

    .line 1493
    :cond_0
    :goto_0
    return-object p0

    .line 1490
    :cond_1
    invoke-interface {p0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-le v0, v1, :cond_0

    .line 1491
    const/4 v0, 0x0

    invoke-interface {p0, v0, v1}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object p0

    goto :goto_0
.end method


# virtual methods
.method public final a()Landroid/app/Notification;
    .locals 1

    .prologue
    .line 1485
    invoke-static {}, Landroid/support/v4/app/H;->a()Landroid/support/v4/app/N;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/support/v4/app/N;->a(Landroid/support/v4/app/L;)Landroid/app/Notification;

    move-result-object v0

    return-object v0
.end method

.method public final a(I)Landroid/support/v4/app/L;
    .locals 1

    .prologue
    .line 928
    iget-object v0, p0, Landroid/support/v4/app/L;->p:Landroid/app/Notification;

    iput p1, v0, Landroid/app/Notification;->icon:I

    .line 929
    return-object p0
.end method

.method public final a(IIZ)Landroid/support/v4/app/L;
    .locals 1

    .prologue
    .line 1001
    const/16 v0, 0x64

    iput v0, p0, Landroid/support/v4/app/L;->i:I

    .line 1002
    iput p2, p0, Landroid/support/v4/app/L;->j:I

    .line 1003
    iput-boolean p3, p0, Landroid/support/v4/app/L;->k:Z

    .line 1004
    return-object p0
.end method

.method public final a(J)Landroid/support/v4/app/L;
    .locals 1

    .prologue
    .line 891
    iget-object v0, p0, Landroid/support/v4/app/L;->p:Landroid/app/Notification;

    iput-wide p1, v0, Landroid/app/Notification;->when:J

    .line 892
    return-object p0
.end method

.method public final a(Landroid/app/PendingIntent;)Landroid/support/v4/app/L;
    .locals 0

    .prologue
    .line 1024
    iput-object p1, p0, Landroid/support/v4/app/L;->d:Landroid/app/PendingIntent;

    .line 1025
    return-object p0
.end method

.method public final a(Ljava/lang/CharSequence;)Landroid/support/v4/app/L;
    .locals 1

    .prologue
    .line 952
    invoke-static {p1}, Landroid/support/v4/app/L;->e(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v4/app/L;->b:Ljava/lang/CharSequence;

    .line 953
    return-object p0
.end method

.method public final a(Z)Landroid/support/v4/app/L;
    .locals 1

    .prologue
    .line 1166
    const/4 v0, 0x2

    invoke-direct {p0, v0, p1}, Landroid/support/v4/app/L;->a(IZ)V

    .line 1167
    return-object p0
.end method

.method public final b(Ljava/lang/CharSequence;)Landroid/support/v4/app/L;
    .locals 1

    .prologue
    .line 960
    invoke-static {p1}, Landroid/support/v4/app/L;->e(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v4/app/L;->c:Ljava/lang/CharSequence;

    .line 961
    return-object p0
.end method

.method public final b(Z)Landroid/support/v4/app/L;
    .locals 1

    .prologue
    .line 1186
    const/16 v0, 0x10

    invoke-direct {p0, v0, p1}, Landroid/support/v4/app/L;->a(IZ)V

    .line 1187
    return-object p0
.end method

.method public final c(Ljava/lang/CharSequence;)Landroid/support/v4/app/L;
    .locals 1

    .prologue
    .line 992
    invoke-static {p1}, Landroid/support/v4/app/L;->e(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v4/app/L;->e:Ljava/lang/CharSequence;

    .line 993
    return-object p0
.end method

.method public final c(Z)Landroid/support/v4/app/L;
    .locals 0

    .prologue
    .line 1197
    iput-boolean p1, p0, Landroid/support/v4/app/L;->m:Z

    .line 1198
    return-object p0
.end method

.method public final d(Ljava/lang/CharSequence;)Landroid/support/v4/app/L;
    .locals 2

    .prologue
    .line 1069
    iget-object v0, p0, Landroid/support/v4/app/L;->p:Landroid/app/Notification;

    invoke-static {p1}, Landroid/support/v4/app/L;->e(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v1

    iput-object v1, v0, Landroid/app/Notification;->tickerText:Ljava/lang/CharSequence;

    .line 1070
    return-object p0
.end method
