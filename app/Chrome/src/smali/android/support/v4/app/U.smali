.class Landroid/support/v4/app/U;
.super Landroid/support/v4/app/T;
.source "NotificationCompat.java"


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 621
    invoke-direct {p0}, Landroid/support/v4/app/T;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/support/v4/app/L;)Landroid/app/Notification;
    .locals 25

    .prologue
    .line 624
    new-instance v1, Landroid/support/v4/app/ac;

    move-object/from16 v0, p1

    iget-object v2, v0, Landroid/support/v4/app/L;->a:Landroid/content/Context;

    move-object/from16 v0, p1

    iget-object v3, v0, Landroid/support/v4/app/L;->p:Landroid/app/Notification;

    move-object/from16 v0, p1

    iget-object v4, v0, Landroid/support/v4/app/L;->b:Ljava/lang/CharSequence;

    move-object/from16 v0, p1

    iget-object v5, v0, Landroid/support/v4/app/L;->c:Ljava/lang/CharSequence;

    move-object/from16 v0, p1

    iget-object v6, v0, Landroid/support/v4/app/L;->e:Ljava/lang/CharSequence;

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object/from16 v0, p1

    iget-object v9, v0, Landroid/support/v4/app/L;->d:Landroid/app/PendingIntent;

    const/4 v10, 0x0

    const/4 v11, 0x0

    move-object/from16 v0, p1

    iget v12, v0, Landroid/support/v4/app/L;->i:I

    move-object/from16 v0, p1

    iget v13, v0, Landroid/support/v4/app/L;->j:I

    move-object/from16 v0, p1

    iget-boolean v14, v0, Landroid/support/v4/app/L;->k:Z

    move-object/from16 v0, p1

    iget-boolean v15, v0, Landroid/support/v4/app/L;->g:Z

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v18, 0x0

    move-object/from16 v0, p1

    iget-boolean v0, v0, Landroid/support/v4/app/L;->m:Z

    move/from16 v19, v0

    move-object/from16 v0, p1

    iget-object v0, v0, Landroid/support/v4/app/L;->q:Ljava/util/ArrayList;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    const/16 v22, 0x0

    const/16 v23, 0x0

    const/16 v24, 0x0

    invoke-direct/range {v1 .. v24}, Landroid/support/v4/app/ac;-><init>(Landroid/content/Context;Landroid/app/Notification;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/widget/RemoteViews;ILandroid/app/PendingIntent;Landroid/app/PendingIntent;Landroid/graphics/Bitmap;IIZZZILjava/lang/CharSequence;ZLjava/util/ArrayList;Landroid/os/Bundle;Ljava/lang/String;ZLjava/lang/String;)V

    .line 630
    move-object/from16 v0, p1

    iget-object v2, v0, Landroid/support/v4/app/L;->l:Ljava/util/ArrayList;

    invoke-static {v1, v2}, Landroid/support/v4/app/H;->a(Landroid/support/v4/app/F;Ljava/util/ArrayList;)V

    .line 631
    move-object/from16 v0, p1

    iget-object v2, v0, Landroid/support/v4/app/L;->h:Landroid/support/v4/app/V;

    invoke-static {v1, v2}, Landroid/support/v4/app/H;->a(Landroid/support/v4/app/G;Landroid/support/v4/app/V;)V

    .line 632
    invoke-virtual {v1}, Landroid/support/v4/app/ac;->b()Landroid/app/Notification;

    move-result-object v1

    return-object v1
.end method
