.class Landroid/support/v4/app/T;
.super Landroid/support/v4/app/N;
.source "NotificationCompat.java"


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 557
    invoke-direct {p0}, Landroid/support/v4/app/N;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/support/v4/app/L;)Landroid/app/Notification;
    .locals 23

    .prologue
    .line 560
    new-instance v1, Landroid/support/v4/app/ab;

    move-object/from16 v0, p1

    iget-object v2, v0, Landroid/support/v4/app/L;->a:Landroid/content/Context;

    move-object/from16 v0, p1

    iget-object v3, v0, Landroid/support/v4/app/L;->p:Landroid/app/Notification;

    move-object/from16 v0, p1

    iget-object v4, v0, Landroid/support/v4/app/L;->b:Ljava/lang/CharSequence;

    move-object/from16 v0, p1

    iget-object v5, v0, Landroid/support/v4/app/L;->c:Ljava/lang/CharSequence;

    move-object/from16 v0, p1

    iget-object v6, v0, Landroid/support/v4/app/L;->e:Ljava/lang/CharSequence;

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object/from16 v0, p1

    iget-object v9, v0, Landroid/support/v4/app/L;->d:Landroid/app/PendingIntent;

    const/4 v10, 0x0

    const/4 v11, 0x0

    move-object/from16 v0, p1

    iget v12, v0, Landroid/support/v4/app/L;->i:I

    move-object/from16 v0, p1

    iget v13, v0, Landroid/support/v4/app/L;->j:I

    move-object/from16 v0, p1

    iget-boolean v14, v0, Landroid/support/v4/app/L;->k:Z

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    move-object/from16 v0, p1

    iget-boolean v0, v0, Landroid/support/v4/app/L;->m:Z

    move/from16 v18, v0

    const/16 v19, 0x0

    const/16 v20, 0x0

    const/16 v21, 0x0

    const/16 v22, 0x0

    invoke-direct/range {v1 .. v22}, Landroid/support/v4/app/ab;-><init>(Landroid/content/Context;Landroid/app/Notification;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/widget/RemoteViews;ILandroid/app/PendingIntent;Landroid/app/PendingIntent;Landroid/graphics/Bitmap;IIZZILjava/lang/CharSequence;ZLandroid/os/Bundle;Ljava/lang/String;ZLjava/lang/String;)V

    .line 566
    move-object/from16 v0, p1

    iget-object v2, v0, Landroid/support/v4/app/L;->l:Ljava/util/ArrayList;

    invoke-static {v1, v2}, Landroid/support/v4/app/H;->a(Landroid/support/v4/app/F;Ljava/util/ArrayList;)V

    .line 567
    move-object/from16 v0, p1

    iget-object v2, v0, Landroid/support/v4/app/L;->h:Landroid/support/v4/app/V;

    invoke-static {v1, v2}, Landroid/support/v4/app/H;->a(Landroid/support/v4/app/G;Landroid/support/v4/app/V;)V

    .line 568
    invoke-virtual {v1}, Landroid/support/v4/app/ab;->b()Landroid/app/Notification;

    move-result-object v1

    return-object v1
.end method
