.class public final Landroid/support/v4/app/H;
.super Ljava/lang/Object;
.source "NotificationCompat.java"


# static fields
.field private static final a:Landroid/support/v4/app/N;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 785
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_0

    .line 786
    new-instance v0, Landroid/support/v4/app/P;

    invoke-direct {v0}, Landroid/support/v4/app/P;-><init>()V

    sput-object v0, Landroid/support/v4/app/H;->a:Landroid/support/v4/app/N;

    .line 802
    :goto_0
    return-void

    .line 787
    :cond_0
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x14

    if-lt v0, v1, :cond_1

    .line 788
    new-instance v0, Landroid/support/v4/app/O;

    invoke-direct {v0}, Landroid/support/v4/app/O;-><init>()V

    sput-object v0, Landroid/support/v4/app/H;->a:Landroid/support/v4/app/N;

    goto :goto_0

    .line 789
    :cond_1
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-lt v0, v1, :cond_2

    .line 790
    new-instance v0, Landroid/support/v4/app/U;

    invoke-direct {v0}, Landroid/support/v4/app/U;-><init>()V

    sput-object v0, Landroid/support/v4/app/H;->a:Landroid/support/v4/app/N;

    goto :goto_0

    .line 791
    :cond_2
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_3

    .line 792
    new-instance v0, Landroid/support/v4/app/T;

    invoke-direct {v0}, Landroid/support/v4/app/T;-><init>()V

    sput-object v0, Landroid/support/v4/app/H;->a:Landroid/support/v4/app/N;

    goto :goto_0

    .line 793
    :cond_3
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-lt v0, v1, :cond_4

    .line 794
    new-instance v0, Landroid/support/v4/app/S;

    invoke-direct {v0}, Landroid/support/v4/app/S;-><init>()V

    sput-object v0, Landroid/support/v4/app/H;->a:Landroid/support/v4/app/N;

    goto :goto_0

    .line 795
    :cond_4
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_5

    .line 796
    new-instance v0, Landroid/support/v4/app/R;

    invoke-direct {v0}, Landroid/support/v4/app/R;-><init>()V

    sput-object v0, Landroid/support/v4/app/H;->a:Landroid/support/v4/app/N;

    goto :goto_0

    .line 797
    :cond_5
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x9

    if-lt v0, v1, :cond_6

    .line 798
    new-instance v0, Landroid/support/v4/app/Q;

    invoke-direct {v0}, Landroid/support/v4/app/Q;-><init>()V

    sput-object v0, Landroid/support/v4/app/H;->a:Landroid/support/v4/app/N;

    goto :goto_0

    .line 800
    :cond_6
    new-instance v0, Landroid/support/v4/app/N;

    invoke-direct {v0}, Landroid/support/v4/app/N;-><init>()V

    sput-object v0, Landroid/support/v4/app/H;->a:Landroid/support/v4/app/N;

    goto :goto_0
.end method

.method static synthetic a()Landroid/support/v4/app/N;
    .locals 1

    .prologue
    .line 41
    sget-object v0, Landroid/support/v4/app/H;->a:Landroid/support/v4/app/N;

    return-object v0
.end method

.method static synthetic a(Landroid/support/v4/app/F;Ljava/util/ArrayList;)V
    .locals 2

    .prologue
    .line 41
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/I;

    invoke-interface {p0, v0}, Landroid/support/v4/app/F;->a(Landroid/support/v4/app/Y;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method static synthetic a(Landroid/support/v4/app/G;Landroid/support/v4/app/V;)V
    .locals 7

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 41
    if-eqz p1, :cond_0

    instance-of v0, p1, Landroid/support/v4/app/K;

    if-eqz v0, :cond_1

    check-cast p1, Landroid/support/v4/app/K;

    iget-object v0, p1, Landroid/support/v4/app/K;->a:Ljava/lang/CharSequence;

    invoke-static {p0, v1, v2, v1, v0}, Landroid/support/v4/app/aa;->a(Landroid/support/v4/app/G;Ljava/lang/CharSequence;ZLjava/lang/CharSequence;Ljava/lang/CharSequence;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    instance-of v0, p1, Landroid/support/v4/app/M;

    if-eqz v0, :cond_2

    check-cast p1, Landroid/support/v4/app/M;

    iget-object v0, p1, Landroid/support/v4/app/M;->a:Ljava/util/ArrayList;

    invoke-static {p0, v1, v2, v1, v0}, Landroid/support/v4/app/aa;->a(Landroid/support/v4/app/G;Ljava/lang/CharSequence;ZLjava/lang/CharSequence;Ljava/util/ArrayList;)V

    goto :goto_0

    :cond_2
    instance-of v0, p1, Landroid/support/v4/app/J;

    if-eqz v0, :cond_0

    move-object v0, p0

    move-object v3, v1

    move-object v4, v1

    move-object v5, v1

    move v6, v2

    invoke-static/range {v0 .. v6}, Landroid/support/v4/app/aa;->a(Landroid/support/v4/app/G;Ljava/lang/CharSequence;ZLjava/lang/CharSequence;Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;Z)V

    goto :goto_0
.end method
