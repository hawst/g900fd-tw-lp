.class public abstract Landroid/support/v4/app/V;
.super Ljava/lang/Object;
.source "NotificationCompat.java"


# instance fields
.field private a:Landroid/support/v4/app/L;

.field b:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1504
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1508
    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/support/v4/app/V;->b:Z

    return-void
.end method


# virtual methods
.method public final a()Landroid/app/Notification;
    .locals 2

    .prologue
    .line 1520
    const/4 v0, 0x0

    .line 1521
    iget-object v1, p0, Landroid/support/v4/app/V;->a:Landroid/support/v4/app/L;

    if-eqz v1, :cond_0

    .line 1522
    iget-object v0, p0, Landroid/support/v4/app/V;->a:Landroid/support/v4/app/L;

    invoke-virtual {v0}, Landroid/support/v4/app/L;->a()Landroid/app/Notification;

    move-result-object v0

    .line 1524
    :cond_0
    return-object v0
.end method

.method public final a(Landroid/support/v4/app/L;)V
    .locals 2

    .prologue
    .line 1511
    iget-object v0, p0, Landroid/support/v4/app/V;->a:Landroid/support/v4/app/L;

    if-eq v0, p1, :cond_0

    .line 1512
    iput-object p1, p0, Landroid/support/v4/app/V;->a:Landroid/support/v4/app/L;

    .line 1513
    iget-object v0, p0, Landroid/support/v4/app/V;->a:Landroid/support/v4/app/L;

    if-eqz v0, :cond_0

    .line 1514
    iget-object v0, p0, Landroid/support/v4/app/V;->a:Landroid/support/v4/app/L;

    iget-object v1, v0, Landroid/support/v4/app/L;->h:Landroid/support/v4/app/V;

    if-eq v1, p0, :cond_0

    iput-object p0, v0, Landroid/support/v4/app/L;->h:Landroid/support/v4/app/V;

    iget-object v1, v0, Landroid/support/v4/app/L;->h:Landroid/support/v4/app/V;

    if-eqz v1, :cond_0

    iget-object v1, v0, Landroid/support/v4/app/L;->h:Landroid/support/v4/app/V;

    invoke-virtual {v1, v0}, Landroid/support/v4/app/V;->a(Landroid/support/v4/app/L;)V

    .line 1517
    :cond_0
    return-void
.end method
