.class Landroid/support/v4/media/f;
.super Ljava/lang/Object;
.source "TransportMediator.java"


# instance fields
.field final synthetic a:Landroid/support/v4/media/d;


# direct methods
.method constructor <init>(Landroid/support/v4/media/d;)V
    .locals 0

    .prologue
    .line 58
    iput-object p1, p0, Landroid/support/v4/media/f;->a:Landroid/support/v4/media/d;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()J
    .locals 2

    .prologue
    .line 70
    iget-object v0, p0, Landroid/support/v4/media/f;->a:Landroid/support/v4/media/d;

    iget-object v0, v0, Landroid/support/v4/media/d;->a:Landroid/support/v4/media/l;

    invoke-virtual {v0}, Landroid/support/v4/media/l;->onGetCurrentPosition()J

    move-result-wide v0

    return-wide v0
.end method

.method public a(I)V
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Landroid/support/v4/media/f;->a:Landroid/support/v4/media/d;

    iget-object v0, v0, Landroid/support/v4/media/d;->a:Landroid/support/v4/media/l;

    invoke-virtual {v0, p1}, Landroid/support/v4/media/l;->onAudioFocusChange(I)V

    .line 66
    return-void
.end method

.method public a(J)V
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Landroid/support/v4/media/f;->a:Landroid/support/v4/media/d;

    iget-object v0, v0, Landroid/support/v4/media/d;->a:Landroid/support/v4/media/l;

    invoke-virtual {v0, p1, p2}, Landroid/support/v4/media/l;->onSeekTo(J)V

    .line 76
    return-void
.end method

.method public a(Landroid/view/KeyEvent;)V
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Landroid/support/v4/media/f;->a:Landroid/support/v4/media/d;

    iget-object v0, v0, Landroid/support/v4/media/d;->b:Landroid/view/KeyEvent$Callback;

    invoke-virtual {p1, v0}, Landroid/view/KeyEvent;->dispatch(Landroid/view/KeyEvent$Callback;)Z

    .line 62
    return-void
.end method
