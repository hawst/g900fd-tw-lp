.class public abstract Landroid/support/v4/media/n;
.super Ljava/lang/Object;
.source "ActionProvider.java"


# instance fields
.field private final a:Landroid/content/Context;

.field private b:Landroid/support/v4/view/j;


# virtual methods
.method public a()Landroid/content/Context;
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Landroid/support/v4/media/n;->a:Landroid/content/Context;

    return-object v0
.end method

.method public a(Landroid/support/v4/view/j;)V
    .locals 3

    .prologue
    .line 236
    iget-object v0, p0, Landroid/support/v4/media/n;->b:Landroid/support/v4/view/j;

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    .line 237
    const-string/jumbo v0, "ActionProvider(support)"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "setVisibilityListener: Setting a new ActionProvider.VisibilityListener when one is already set. Are you reusing this "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " instance while it is still in use somewhere else?"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 241
    :cond_0
    iput-object p1, p0, Landroid/support/v4/media/n;->b:Landroid/support/v4/view/j;

    .line 242
    return-void
.end method

.method public abstract b()Landroid/view/View;
.end method

.method public c()Landroid/view/View;
    .locals 1

    .prologue
    .line 109
    invoke-virtual {p0}, Landroid/support/v4/media/n;->b()Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public d()V
    .locals 1

    .prologue
    .line 145
    iget-object v0, p0, Landroid/support/v4/media/n;->b:Landroid/support/v4/view/j;

    .line 146
    return-void
.end method
