.class public final Landroid/support/v4/view/ViewCompat;
.super Ljava/lang/Object;
.source "ViewCompat.java"


# static fields
.field private static a:Landroid/support/v4/view/U;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1151
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    .line 1152
    const/16 v1, 0x15

    if-lt v0, v1, :cond_0

    .line 1153
    new-instance v0, Landroid/support/v4/view/M;

    invoke-direct {v0}, Landroid/support/v4/view/M;-><init>()V

    sput-object v0, Landroid/support/v4/view/ViewCompat;->a:Landroid/support/v4/view/U;

    .line 1171
    :goto_0
    return-void

    .line 1154
    :cond_0
    const/16 v1, 0x13

    if-lt v0, v1, :cond_1

    .line 1155
    new-instance v0, Landroid/support/v4/view/T;

    invoke-direct {v0}, Landroid/support/v4/view/T;-><init>()V

    sput-object v0, Landroid/support/v4/view/ViewCompat;->a:Landroid/support/v4/view/U;

    goto :goto_0

    .line 1156
    :cond_1
    const/16 v1, 0x11

    if-lt v0, v1, :cond_2

    .line 1157
    new-instance v0, Landroid/support/v4/view/S;

    invoke-direct {v0}, Landroid/support/v4/view/S;-><init>()V

    sput-object v0, Landroid/support/v4/view/ViewCompat;->a:Landroid/support/v4/view/U;

    goto :goto_0

    .line 1158
    :cond_2
    const/16 v1, 0x10

    if-lt v0, v1, :cond_3

    .line 1159
    new-instance v0, Landroid/support/v4/view/R;

    invoke-direct {v0}, Landroid/support/v4/view/R;-><init>()V

    sput-object v0, Landroid/support/v4/view/ViewCompat;->a:Landroid/support/v4/view/U;

    goto :goto_0

    .line 1160
    :cond_3
    const/16 v1, 0xe

    if-lt v0, v1, :cond_4

    .line 1161
    new-instance v0, Landroid/support/v4/view/Q;

    invoke-direct {v0}, Landroid/support/v4/view/Q;-><init>()V

    sput-object v0, Landroid/support/v4/view/ViewCompat;->a:Landroid/support/v4/view/U;

    goto :goto_0

    .line 1162
    :cond_4
    const/16 v1, 0xb

    if-lt v0, v1, :cond_5

    .line 1163
    new-instance v0, Landroid/support/v4/view/P;

    invoke-direct {v0}, Landroid/support/v4/view/P;-><init>()V

    sput-object v0, Landroid/support/v4/view/ViewCompat;->a:Landroid/support/v4/view/U;

    goto :goto_0

    .line 1164
    :cond_5
    const/16 v1, 0x9

    if-lt v0, v1, :cond_6

    .line 1165
    new-instance v0, Landroid/support/v4/view/O;

    invoke-direct {v0}, Landroid/support/v4/view/O;-><init>()V

    sput-object v0, Landroid/support/v4/view/ViewCompat;->a:Landroid/support/v4/view/U;

    goto :goto_0

    .line 1166
    :cond_6
    const/4 v1, 0x7

    if-lt v0, v1, :cond_7

    .line 1167
    new-instance v0, Landroid/support/v4/view/N;

    invoke-direct {v0}, Landroid/support/v4/view/N;-><init>()V

    sput-object v0, Landroid/support/v4/view/ViewCompat;->a:Landroid/support/v4/view/U;

    goto :goto_0

    .line 1169
    :cond_7
    new-instance v0, Landroid/support/v4/view/U;

    invoke-direct {v0}, Landroid/support/v4/view/U;-><init>()V

    sput-object v0, Landroid/support/v4/view/ViewCompat;->a:Landroid/support/v4/view/U;

    goto :goto_0
.end method

.method public static a(III)I
    .locals 1

    .prologue
    .line 1733
    sget-object v0, Landroid/support/v4/view/ViewCompat;->a:Landroid/support/v4/view/U;

    invoke-virtual {v0, p0, p1, p2}, Landroid/support/v4/view/U;->a(III)I

    move-result v0

    return v0
.end method

.method public static a(Landroid/view/View;)I
    .locals 1

    .prologue
    .line 1206
    sget-object v0, Landroid/support/v4/view/ViewCompat;->a:Landroid/support/v4/view/U;

    invoke-virtual {v0, p0}, Landroid/support/v4/view/U;->c(Landroid/view/View;)I

    move-result v0

    return v0
.end method

.method public static a(Landroid/view/View;F)V
    .locals 1

    .prologue
    .line 1946
    sget-object v0, Landroid/support/v4/view/ViewCompat;->a:Landroid/support/v4/view/U;

    invoke-virtual {v0, p0, p1}, Landroid/support/v4/view/U;->b(Landroid/view/View;F)V

    .line 1947
    return-void
.end method

.method public static a(Landroid/view/View;IIII)V
    .locals 6

    .prologue
    .line 1407
    sget-object v0, Landroid/support/v4/view/ViewCompat;->a:Landroid/support/v4/view/U;

    move-object v1, p0

    move v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    invoke-virtual/range {v0 .. v5}, Landroid/support/v4/view/U;->a(Landroid/view/View;IIII)V

    .line 1408
    return-void
.end method

.method public static a(Landroid/view/View;ILandroid/graphics/Paint;)V
    .locals 1

    .prologue
    .line 1579
    sget-object v0, Landroid/support/v4/view/ViewCompat;->a:Landroid/support/v4/view/U;

    invoke-virtual {v0, p0, p1, p2}, Landroid/support/v4/view/U;->a(Landroid/view/View;ILandroid/graphics/Paint;)V

    .line 1580
    return-void
.end method

.method public static a(Landroid/view/View;Landroid/graphics/Paint;)V
    .locals 1

    .prologue
    .line 1656
    sget-object v0, Landroid/support/v4/view/ViewCompat;->a:Landroid/support/v4/view/U;

    invoke-virtual {v0, p0, p1}, Landroid/support/v4/view/U;->a(Landroid/view/View;Landroid/graphics/Paint;)V

    .line 1657
    return-void
.end method

.method public static a(Landroid/view/View;Landroid/support/v4/view/G;)V
    .locals 1

    .prologue
    .line 2262
    sget-object v0, Landroid/support/v4/view/ViewCompat;->a:Landroid/support/v4/view/U;

    invoke-virtual {v0, p0, p1}, Landroid/support/v4/view/U;->a(Landroid/view/View;Landroid/support/v4/view/G;)V

    .line 2263
    return-void
.end method

.method public static a(Landroid/view/View;Landroid/support/v4/view/a;)V
    .locals 1

    .prologue
    .line 1343
    sget-object v0, Landroid/support/v4/view/ViewCompat;->a:Landroid/support/v4/view/U;

    invoke-virtual {v0, p0, p1}, Landroid/support/v4/view/U;->a(Landroid/view/View;Landroid/support/v4/view/a;)V

    .line 1344
    return-void
.end method

.method public static a(Landroid/view/View;Ljava/lang/Runnable;)V
    .locals 1

    .prologue
    .line 1421
    sget-object v0, Landroid/support/v4/view/ViewCompat;->a:Landroid/support/v4/view/U;

    invoke-virtual {v0, p0, p1}, Landroid/support/v4/view/U;->a(Landroid/view/View;Ljava/lang/Runnable;)V

    .line 1422
    return-void
.end method

.method public static a(Landroid/view/View;Ljava/lang/Runnable;J)V
    .locals 2

    .prologue
    .line 1438
    sget-object v0, Landroid/support/v4/view/ViewCompat;->a:Landroid/support/v4/view/U;

    invoke-virtual {v0, p0, p1, p2, p3}, Landroid/support/v4/view/U;->a(Landroid/view/View;Ljava/lang/Runnable;J)V

    .line 1439
    return-void
.end method

.method public static a(Landroid/view/View;I)Z
    .locals 1

    .prologue
    .line 1181
    sget-object v0, Landroid/support/v4/view/ViewCompat;->a:Landroid/support/v4/view/U;

    invoke-virtual {v0, p0, p1}, Landroid/support/v4/view/U;->a(Landroid/view/View;I)Z

    move-result v0

    return v0
.end method

.method public static b(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 1389
    sget-object v0, Landroid/support/v4/view/ViewCompat;->a:Landroid/support/v4/view/U;

    invoke-virtual {v0, p0}, Landroid/support/v4/view/U;->i(Landroid/view/View;)V

    .line 1390
    return-void
.end method

.method public static b(Landroid/view/View;F)V
    .locals 1

    .prologue
    .line 1962
    sget-object v0, Landroid/support/v4/view/ViewCompat;->a:Landroid/support/v4/view/U;

    invoke-virtual {v0, p0, p1}, Landroid/support/v4/view/U;->c(Landroid/view/View;F)V

    .line 1963
    return-void
.end method

.method public static b(Landroid/view/View;I)V
    .locals 1

    .prologue
    .line 1480
    sget-object v0, Landroid/support/v4/view/ViewCompat;->a:Landroid/support/v4/view/U;

    invoke-virtual {v0, p0, p1}, Landroid/support/v4/view/U;->b(Landroid/view/View;I)V

    .line 1481
    return-void
.end method

.method public static c(Landroid/view/View;)I
    .locals 1

    .prologue
    .line 1456
    sget-object v0, Landroid/support/v4/view/ViewCompat;->a:Landroid/support/v4/view/U;

    invoke-virtual {v0, p0}, Landroid/support/v4/view/U;->j(Landroid/view/View;)I

    move-result v0

    return v0
.end method

.method public static c(Landroid/view/View;F)V
    .locals 1

    .prologue
    .line 1978
    sget-object v0, Landroid/support/v4/view/ViewCompat;->a:Landroid/support/v4/view/U;

    invoke-virtual {v0, p0, p1}, Landroid/support/v4/view/U;->d(Landroid/view/View;F)V

    .line 1979
    return-void
.end method

.method public static d(Landroid/view/View;)I
    .locals 1

    .prologue
    .line 1600
    sget-object v0, Landroid/support/v4/view/ViewCompat;->a:Landroid/support/v4/view/U;

    invoke-virtual {v0, p0}, Landroid/support/v4/view/U;->d(Landroid/view/View;)I

    move-result v0

    return v0
.end method

.method public static d(Landroid/view/View;F)V
    .locals 2

    .prologue
    .line 2068
    sget-object v0, Landroid/support/v4/view/ViewCompat;->a:Landroid/support/v4/view/U;

    const/4 v1, 0x0

    invoke-virtual {v0, p0, v1}, Landroid/support/v4/view/U;->e(Landroid/view/View;F)V

    .line 2069
    return-void
.end method

.method public static e(Landroid/view/View;)I
    .locals 1

    .prologue
    .line 1671
    sget-object v0, Landroid/support/v4/view/ViewCompat;->a:Landroid/support/v4/view/U;

    invoke-virtual {v0, p0}, Landroid/support/v4/view/U;->n(Landroid/view/View;)I

    move-result v0

    return v0
.end method

.method public static e(Landroid/view/View;F)V
    .locals 1

    .prologue
    .line 2156
    sget-object v0, Landroid/support/v4/view/ViewCompat;->a:Landroid/support/v4/view/U;

    invoke-virtual {v0, p0, p1}, Landroid/support/v4/view/U;->a(Landroid/view/View;F)V

    .line 2157
    return-void
.end method

.method public static f(Landroid/view/View;)Landroid/view/ViewParent;
    .locals 1

    .prologue
    .line 1703
    sget-object v0, Landroid/support/v4/view/ViewCompat;->a:Landroid/support/v4/view/U;

    invoke-virtual {v0, p0}, Landroid/support/v4/view/U;->k(Landroid/view/View;)Landroid/view/ViewParent;

    move-result-object v0

    return-object v0
.end method

.method public static g(Landroid/view/View;)Z
    .locals 1

    .prologue
    .line 1716
    sget-object v0, Landroid/support/v4/view/ViewCompat;->a:Landroid/support/v4/view/U;

    invoke-virtual {v0, p0}, Landroid/support/v4/view/U;->b(Landroid/view/View;)Z

    move-result v0

    return v0
.end method

.method public static h(Landroid/view/View;)I
    .locals 1

    .prologue
    .line 1772
    sget-object v0, Landroid/support/v4/view/ViewCompat;->a:Landroid/support/v4/view/U;

    invoke-virtual {v0, p0}, Landroid/support/v4/view/U;->e(Landroid/view/View;)I

    move-result v0

    return v0
.end method

.method public static i(Landroid/view/View;)F
    .locals 1

    .prologue
    .line 1898
    sget-object v0, Landroid/support/v4/view/ViewCompat;->a:Landroid/support/v4/view/U;

    invoke-virtual {v0, p0}, Landroid/support/v4/view/U;->f(Landroid/view/View;)F

    move-result v0

    return v0
.end method

.method public static j(Landroid/view/View;)I
    .locals 1

    .prologue
    .line 1920
    sget-object v0, Landroid/support/v4/view/ViewCompat;->a:Landroid/support/v4/view/U;

    invoke-virtual {v0, p0}, Landroid/support/v4/view/U;->l(Landroid/view/View;)I

    move-result v0

    return v0
.end method

.method public static k(Landroid/view/View;)Landroid/support/v4/view/ar;
    .locals 1

    .prologue
    .line 1932
    sget-object v0, Landroid/support/v4/view/ViewCompat;->a:Landroid/support/v4/view/U;

    invoke-virtual {v0, p0}, Landroid/support/v4/view/U;->h(Landroid/view/View;)Landroid/support/v4/view/ar;

    move-result-object v0

    return-object v0
.end method

.method public static l(Landroid/view/View;)I
    .locals 1

    .prologue
    .line 2213
    sget-object v0, Landroid/support/v4/view/ViewCompat;->a:Landroid/support/v4/view/U;

    invoke-virtual {v0, p0}, Landroid/support/v4/view/U;->o(Landroid/view/View;)I

    move-result v0

    return v0
.end method

.method public static m(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 2221
    sget-object v0, Landroid/support/v4/view/ViewCompat;->a:Landroid/support/v4/view/U;

    invoke-virtual {v0, p0}, Landroid/support/v4/view/U;->a(Landroid/view/View;)V

    .line 2222
    return-void
.end method

.method public static n(Landroid/view/View;)Z
    .locals 1

    .prologue
    .line 2242
    sget-object v0, Landroid/support/v4/view/ViewCompat;->a:Landroid/support/v4/view/U;

    invoke-virtual {v0, p0}, Landroid/support/v4/view/U;->m(Landroid/view/View;)Z

    move-result v0

    return v0
.end method

.method public static o(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 2253
    sget-object v0, Landroid/support/v4/view/ViewCompat;->a:Landroid/support/v4/view/U;

    invoke-virtual {v0, p0}, Landroid/support/v4/view/U;->g(Landroid/view/View;)V

    .line 2254
    return-void
.end method
