.class final Landroid/support/v4/view/av;
.super Ljava/lang/Object;
.source "ViewPropertyAnimatorCompat.java"

# interfaces
.implements Landroid/support/v4/view/aC;


# instance fields
.field private synthetic a:Landroid/support/v4/view/au;


# direct methods
.method constructor <init>(Landroid/support/v4/view/au;)V
    .locals 0

    .prologue
    .line 508
    iput-object p1, p0, Landroid/support/v4/view/av;->a:Landroid/support/v4/view/au;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 511
    iget-object v0, p0, Landroid/support/v4/view/av;->a:Landroid/support/v4/view/au;

    .line 512
    iget-object v0, p0, Landroid/support/v4/view/av;->a:Landroid/support/v4/view/au;

    .line 516
    iget-object v0, p0, Landroid/support/v4/view/av;->a:Landroid/support/v4/view/au;

    iget-object v0, v0, Landroid/support/v4/view/au;->c:Ljava/util/WeakHashMap;

    if-eqz v0, :cond_1

    iget-object v0, p0, Landroid/support/v4/view/av;->a:Landroid/support/v4/view/au;

    iget-object v0, v0, Landroid/support/v4/view/au;->c:Ljava/util/WeakHashMap;

    invoke-virtual {v0, p1}, Ljava/util/WeakHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/aC;

    .line 522
    :goto_0
    if-eqz v0, :cond_0

    .line 523
    invoke-interface {v0, p1}, Landroid/support/v4/view/aC;->a(Landroid/view/View;)V

    .line 525
    :cond_0
    return-void

    .line 516
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 528
    iget-object v0, p0, Landroid/support/v4/view/av;->a:Landroid/support/v4/view/au;

    .line 529
    iget-object v0, p0, Landroid/support/v4/view/av;->a:Landroid/support/v4/view/au;

    iget-object v0, v0, Landroid/support/v4/view/au;->c:Ljava/util/WeakHashMap;

    if-eqz v0, :cond_1

    iget-object v0, p0, Landroid/support/v4/view/av;->a:Landroid/support/v4/view/au;

    iget-object v0, v0, Landroid/support/v4/view/au;->c:Ljava/util/WeakHashMap;

    invoke-virtual {v0, p1}, Ljava/util/WeakHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/aC;

    .line 535
    :goto_0
    if-eqz v0, :cond_0

    .line 536
    invoke-interface {v0, p1}, Landroid/support/v4/view/aC;->b(Landroid/view/View;)V

    .line 538
    :cond_0
    iget-object v0, p0, Landroid/support/v4/view/av;->a:Landroid/support/v4/view/au;

    .line 539
    return-void

    .line 529
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 547
    iget-object v0, p0, Landroid/support/v4/view/av;->a:Landroid/support/v4/view/au;

    iget-object v0, v0, Landroid/support/v4/view/au;->c:Ljava/util/WeakHashMap;

    if-eqz v0, :cond_1

    iget-object v0, p0, Landroid/support/v4/view/av;->a:Landroid/support/v4/view/au;

    iget-object v0, v0, Landroid/support/v4/view/au;->c:Ljava/util/WeakHashMap;

    invoke-virtual {v0, p1}, Ljava/util/WeakHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/aC;

    .line 549
    :goto_0
    if-eqz v0, :cond_0

    .line 550
    invoke-interface {v0, p1}, Landroid/support/v4/view/aC;->c(Landroid/view/View;)V

    .line 552
    :cond_0
    return-void

    .line 547
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
