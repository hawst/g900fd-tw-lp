.class public final Landroid/support/v4/widget/z;
.super Ljava/lang/Object;
.source "ScrollerCompat.java"


# instance fields
.field a:Ljava/lang/Object;

.field b:Landroid/support/v4/widget/A;


# direct methods
.method private constructor <init>(ILandroid/content/Context;Landroid/view/animation/Interpolator;)V
    .locals 1

    .prologue
    .line 262
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 263
    const/16 v0, 0xe

    if-lt p1, v0, :cond_0

    .line 264
    new-instance v0, Landroid/support/v4/widget/D;

    invoke-direct {v0}, Landroid/support/v4/widget/D;-><init>()V

    iput-object v0, p0, Landroid/support/v4/widget/z;->b:Landroid/support/v4/widget/A;

    .line 270
    :goto_0
    iget-object v0, p0, Landroid/support/v4/widget/z;->b:Landroid/support/v4/widget/A;

    invoke-interface {v0, p2, p3}, Landroid/support/v4/widget/A;->a(Landroid/content/Context;Landroid/view/animation/Interpolator;)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v4/widget/z;->a:Ljava/lang/Object;

    .line 271
    return-void

    .line 265
    :cond_0
    const/16 v0, 0x9

    if-lt p1, v0, :cond_1

    .line 266
    new-instance v0, Landroid/support/v4/widget/C;

    invoke-direct {v0}, Landroid/support/v4/widget/C;-><init>()V

    iput-object v0, p0, Landroid/support/v4/widget/z;->b:Landroid/support/v4/widget/A;

    goto :goto_0

    .line 268
    :cond_1
    new-instance v0, Landroid/support/v4/widget/B;

    invoke-direct {v0}, Landroid/support/v4/widget/B;-><init>()V

    iput-object v0, p0, Landroid/support/v4/widget/z;->b:Landroid/support/v4/widget/A;

    goto :goto_0
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/view/animation/Interpolator;)V
    .locals 1

    .prologue
    .line 254
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    invoke-direct {p0, v0, p1, p2}, Landroid/support/v4/widget/z;-><init>(ILandroid/content/Context;Landroid/view/animation/Interpolator;)V

    .line 256
    return-void
.end method

.method public static a(Landroid/content/Context;)Landroid/support/v4/widget/z;
    .locals 1

    .prologue
    .line 246
    const/4 v0, 0x0

    invoke-static {p0, v0}, Landroid/support/v4/widget/z;->a(Landroid/content/Context;Landroid/view/animation/Interpolator;)Landroid/support/v4/widget/z;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/content/Context;Landroid/view/animation/Interpolator;)Landroid/support/v4/widget/z;
    .locals 1

    .prologue
    .line 250
    new-instance v0, Landroid/support/v4/widget/z;

    invoke-direct {v0, p0, p1}, Landroid/support/v4/widget/z;-><init>(Landroid/content/Context;Landroid/view/animation/Interpolator;)V

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 2

    .prologue
    .line 288
    iget-object v0, p0, Landroid/support/v4/widget/z;->b:Landroid/support/v4/widget/A;

    iget-object v1, p0, Landroid/support/v4/widget/z;->a:Ljava/lang/Object;

    invoke-interface {v0, v1}, Landroid/support/v4/widget/A;->a(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final a(IIIIIIII)V
    .locals 10

    .prologue
    .line 393
    iget-object v0, p0, Landroid/support/v4/widget/z;->b:Landroid/support/v4/widget/A;

    iget-object v1, p0, Landroid/support/v4/widget/z;->a:Ljava/lang/Object;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/high16 v8, -0x80000000

    const v9, 0x7fffffff

    move v5, p4

    invoke-interface/range {v0 .. v9}, Landroid/support/v4/widget/A;->a(Ljava/lang/Object;IIIIIIII)V

    .line 394
    return-void
.end method

.method public final b()I
    .locals 2

    .prologue
    .line 297
    iget-object v0, p0, Landroid/support/v4/widget/z;->b:Landroid/support/v4/widget/A;

    iget-object v1, p0, Landroid/support/v4/widget/z;->a:Ljava/lang/Object;

    invoke-interface {v0, v1}, Landroid/support/v4/widget/A;->b(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final c()I
    .locals 2

    .prologue
    .line 311
    iget-object v0, p0, Landroid/support/v4/widget/z;->b:Landroid/support/v4/widget/A;

    iget-object v1, p0, Landroid/support/v4/widget/z;->a:Ljava/lang/Object;

    invoke-interface {v0, v1}, Landroid/support/v4/widget/A;->f(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 430
    iget-object v0, p0, Landroid/support/v4/widget/z;->b:Landroid/support/v4/widget/A;

    iget-object v1, p0, Landroid/support/v4/widget/z;->a:Ljava/lang/Object;

    invoke-interface {v0, v1}, Landroid/support/v4/widget/A;->d(Ljava/lang/Object;)V

    .line 431
    return-void
.end method
