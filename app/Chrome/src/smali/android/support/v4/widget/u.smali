.class final Landroid/support/v4/widget/u;
.super Ljava/lang/Object;
.source "MaterialProgressDrawable.java"


# instance fields
.field private final a:Landroid/graphics/RectF;

.field private final b:Landroid/graphics/Paint;

.field private final c:Landroid/graphics/Paint;

.field private final d:Landroid/graphics/drawable/Drawable$Callback;

.field private e:F

.field private f:F

.field private g:F

.field private h:I

.field private i:F

.field private j:F

.field private k:F

.field private l:Z

.field private m:Landroid/graphics/Path;

.field private n:F

.field private o:I

.field private final p:Landroid/graphics/Paint;


# direct methods
.method private f()V
    .locals 2

    .prologue
    .line 700
    iget-object v0, p0, Landroid/support/v4/widget/u;->d:Landroid/graphics/drawable/Drawable$Callback;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/graphics/drawable/Drawable$Callback;->invalidateDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 701
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 564
    iget v0, p0, Landroid/support/v4/widget/u;->o:I

    return v0
.end method

.method public final a(F)V
    .locals 0

    .prologue
    .line 583
    iput p1, p0, Landroid/support/v4/widget/u;->e:F

    .line 584
    invoke-direct {p0}, Landroid/support/v4/widget/u;->f()V

    .line 585
    return-void
.end method

.method public final a(I)V
    .locals 1

    .prologue
    .line 537
    const/4 v0, 0x0

    iput v0, p0, Landroid/support/v4/widget/u;->h:I

    .line 538
    return-void
.end method

.method public final a(Landroid/graphics/Canvas;Landroid/graphics/Rect;)V
    .locals 9

    .prologue
    .line 467
    iget-object v1, p0, Landroid/support/v4/widget/u;->a:Landroid/graphics/RectF;

    .line 468
    invoke-virtual {v1, p2}, Landroid/graphics/RectF;->set(Landroid/graphics/Rect;)V

    .line 469
    const/4 v0, 0x0

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/graphics/RectF;->inset(FF)V

    .line 471
    iget v0, p0, Landroid/support/v4/widget/u;->e:F

    iget v2, p0, Landroid/support/v4/widget/u;->g:F

    add-float/2addr v0, v2

    const/high16 v2, 0x43b40000    # 360.0f

    mul-float/2addr v2, v0

    .line 472
    iget v0, p0, Landroid/support/v4/widget/u;->f:F

    iget v3, p0, Landroid/support/v4/widget/u;->g:F

    add-float/2addr v0, v3

    const/high16 v3, 0x43b40000    # 360.0f

    mul-float/2addr v0, v3

    .line 473
    sub-float v3, v0, v2

    .line 475
    iget-object v0, p0, Landroid/support/v4/widget/u;->b:Landroid/graphics/Paint;

    const/4 v4, 0x0

    iget v5, p0, Landroid/support/v4/widget/u;->h:I

    aget v4, v4, v5

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setColor(I)V

    .line 476
    const/4 v4, 0x0

    iget-object v5, p0, Landroid/support/v4/widget/u;->b:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawArc(Landroid/graphics/RectF;FFZLandroid/graphics/Paint;)V

    .line 478
    iget-boolean v0, p0, Landroid/support/v4/widget/u;->l:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v4/widget/u;->m:Landroid/graphics/Path;

    if-nez v0, :cond_2

    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Landroid/support/v4/widget/u;->m:Landroid/graphics/Path;

    iget-object v0, p0, Landroid/support/v4/widget/u;->m:Landroid/graphics/Path;

    sget-object v1, Landroid/graphics/Path$FillType;->EVEN_ODD:Landroid/graphics/Path$FillType;

    invoke-virtual {v0, v1}, Landroid/graphics/Path;->setFillType(Landroid/graphics/Path$FillType;)V

    :goto_0
    const/4 v0, 0x0

    iget v1, p0, Landroid/support/v4/widget/u;->n:F

    mul-float/2addr v0, v1

    const-wide/16 v4, 0x0

    const-wide/16 v6, 0x0

    invoke-static {v6, v7}, Ljava/lang/Math;->cos(D)D

    move-result-wide v6

    mul-double/2addr v4, v6

    invoke-virtual {p2}, Landroid/graphics/Rect;->exactCenterX()F

    move-result v1

    float-to-double v6, v1

    add-double/2addr v4, v6

    double-to-float v1, v4

    const-wide/16 v4, 0x0

    const-wide/16 v6, 0x0

    invoke-static {v6, v7}, Ljava/lang/Math;->sin(D)D

    move-result-wide v6

    mul-double/2addr v4, v6

    invoke-virtual {p2}, Landroid/graphics/Rect;->exactCenterY()F

    move-result v6

    float-to-double v6, v6

    add-double/2addr v4, v6

    double-to-float v4, v4

    iget-object v5, p0, Landroid/support/v4/widget/u;->m:Landroid/graphics/Path;

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual {v5, v6, v7}, Landroid/graphics/Path;->moveTo(FF)V

    iget-object v5, p0, Landroid/support/v4/widget/u;->m:Landroid/graphics/Path;

    const/4 v6, 0x0

    iget v7, p0, Landroid/support/v4/widget/u;->n:F

    mul-float/2addr v6, v7

    const/4 v7, 0x0

    invoke-virtual {v5, v6, v7}, Landroid/graphics/Path;->lineTo(FF)V

    iget-object v5, p0, Landroid/support/v4/widget/u;->m:Landroid/graphics/Path;

    const/4 v6, 0x0

    iget v7, p0, Landroid/support/v4/widget/u;->n:F

    mul-float/2addr v6, v7

    const/high16 v7, 0x40000000    # 2.0f

    div-float/2addr v6, v7

    const/4 v7, 0x0

    iget v8, p0, Landroid/support/v4/widget/u;->n:F

    mul-float/2addr v7, v8

    invoke-virtual {v5, v6, v7}, Landroid/graphics/Path;->lineTo(FF)V

    iget-object v5, p0, Landroid/support/v4/widget/u;->m:Landroid/graphics/Path;

    sub-float v0, v1, v0

    invoke-virtual {v5, v0, v4}, Landroid/graphics/Path;->offset(FF)V

    iget-object v0, p0, Landroid/support/v4/widget/u;->m:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->close()V

    iget-object v0, p0, Landroid/support/v4/widget/u;->c:Landroid/graphics/Paint;

    const/4 v1, 0x0

    iget v4, p0, Landroid/support/v4/widget/u;->h:I

    aget v1, v1, v4

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    add-float v0, v2, v3

    const/high16 v1, 0x40a00000    # 5.0f

    sub-float/2addr v0, v1

    invoke-virtual {p2}, Landroid/graphics/Rect;->exactCenterX()F

    move-result v1

    invoke-virtual {p2}, Landroid/graphics/Rect;->exactCenterY()F

    move-result v2

    invoke-virtual {p1, v0, v1, v2}, Landroid/graphics/Canvas;->rotate(FFF)V

    iget-object v0, p0, Landroid/support/v4/widget/u;->m:Landroid/graphics/Path;

    iget-object v1, p0, Landroid/support/v4/widget/u;->c:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 480
    :cond_0
    iget v0, p0, Landroid/support/v4/widget/u;->o:I

    const/16 v1, 0xff

    if-ge v0, v1, :cond_1

    .line 481
    iget-object v0, p0, Landroid/support/v4/widget/u;->p:Landroid/graphics/Paint;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 482
    iget-object v0, p0, Landroid/support/v4/widget/u;->p:Landroid/graphics/Paint;

    iget v1, p0, Landroid/support/v4/widget/u;->o:I

    rsub-int v1, v1, 0xff

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 483
    invoke-virtual {p2}, Landroid/graphics/Rect;->exactCenterX()F

    move-result v0

    invoke-virtual {p2}, Landroid/graphics/Rect;->exactCenterY()F

    move-result v1

    invoke-virtual {p2}, Landroid/graphics/Rect;->width()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    iget-object v3, p0, Landroid/support/v4/widget/u;->p:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 486
    :cond_1
    return-void

    .line 478
    :cond_2
    iget-object v0, p0, Landroid/support/v4/widget/u;->m:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->reset()V

    goto/16 :goto_0
.end method

.method public final a(Landroid/graphics/ColorFilter;)V
    .locals 1

    .prologue
    .line 549
    iget-object v0, p0, Landroid/support/v4/widget/u;->b:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColorFilter(Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;

    .line 550
    invoke-direct {p0}, Landroid/support/v4/widget/u;->f()V

    .line 551
    return-void
.end method

.method public final a(Z)V
    .locals 1

    .prologue
    .line 654
    iget-boolean v0, p0, Landroid/support/v4/widget/u;->l:Z

    if-eqz v0, :cond_0

    .line 655
    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/support/v4/widget/u;->l:Z

    .line 656
    invoke-direct {p0}, Landroid/support/v4/widget/u;->f()V

    .line 658
    :cond_0
    return-void
.end method

.method public final b()F
    .locals 1

    .prologue
    .line 589
    iget v0, p0, Landroid/support/v4/widget/u;->e:F

    return v0
.end method

.method public final b(F)V
    .locals 0

    .prologue
    .line 613
    iput p1, p0, Landroid/support/v4/widget/u;->g:F

    .line 614
    invoke-direct {p0}, Landroid/support/v4/widget/u;->f()V

    .line 615
    return-void
.end method

.method public final b(I)V
    .locals 0

    .prologue
    .line 557
    iput p1, p0, Landroid/support/v4/widget/u;->o:I

    .line 558
    return-void
.end method

.method public final c()F
    .locals 1

    .prologue
    .line 608
    iget v0, p0, Landroid/support/v4/widget/u;->f:F

    return v0
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 682
    iget v0, p0, Landroid/support/v4/widget/u;->e:F

    iput v0, p0, Landroid/support/v4/widget/u;->i:F

    .line 683
    iget v0, p0, Landroid/support/v4/widget/u;->f:F

    iput v0, p0, Landroid/support/v4/widget/u;->j:F

    .line 684
    iget v0, p0, Landroid/support/v4/widget/u;->g:F

    iput v0, p0, Landroid/support/v4/widget/u;->k:F

    .line 685
    return-void
.end method

.method public final e()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 691
    iput v0, p0, Landroid/support/v4/widget/u;->i:F

    .line 692
    iput v0, p0, Landroid/support/v4/widget/u;->j:F

    .line 693
    iput v0, p0, Landroid/support/v4/widget/u;->k:F

    .line 694
    invoke-virtual {p0, v0}, Landroid/support/v4/widget/u;->a(F)V

    .line 695
    iput v0, p0, Landroid/support/v4/widget/u;->f:F

    invoke-direct {p0}, Landroid/support/v4/widget/u;->f()V

    .line 696
    invoke-virtual {p0, v0}, Landroid/support/v4/widget/u;->b(F)V

    .line 697
    return-void
.end method
