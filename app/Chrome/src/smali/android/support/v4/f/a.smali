.class public Landroid/support/v4/f/a;
.super Landroid/support/v4/f/l;
.source "ArrayMap.java"

# interfaces
.implements Ljava/util/Map;


# instance fields
.field private c:Landroid/support/v4/f/f;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 54
    invoke-direct {p0}, Landroid/support/v4/f/l;-><init>()V

    .line 55
    return-void
.end method

.method private a()Landroid/support/v4/f/f;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Landroid/support/v4/f/a;->c:Landroid/support/v4/f/f;

    if-nez v0, :cond_0

    .line 73
    new-instance v0, Landroid/support/v4/f/b;

    invoke-direct {v0, p0}, Landroid/support/v4/f/b;-><init>(Landroid/support/v4/f/a;)V

    iput-object v0, p0, Landroid/support/v4/f/a;->c:Landroid/support/v4/f/f;

    .line 120
    :cond_0
    iget-object v0, p0, Landroid/support/v4/f/a;->c:Landroid/support/v4/f/f;

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/util/Collection;)Z
    .locals 1

    .prologue
    .line 161
    invoke-static {p0, p1}, Landroid/support/v4/f/f;->a(Ljava/util/Map;Ljava/util/Collection;)Z

    move-result v0

    return v0
.end method

.method public entrySet()Ljava/util/Set;
    .locals 2

    .prologue
    .line 179
    invoke-direct {p0}, Landroid/support/v4/f/a;->a()Landroid/support/v4/f/f;

    move-result-object v0

    iget-object v1, v0, Landroid/support/v4/f/f;->a:Landroid/support/v4/f/h;

    if-nez v1, :cond_0

    new-instance v1, Landroid/support/v4/f/h;

    invoke-direct {v1, v0}, Landroid/support/v4/f/h;-><init>(Landroid/support/v4/f/f;)V

    iput-object v1, v0, Landroid/support/v4/f/f;->a:Landroid/support/v4/f/h;

    :cond_0
    iget-object v0, v0, Landroid/support/v4/f/f;->a:Landroid/support/v4/f/h;

    return-object v0
.end method

.method public keySet()Ljava/util/Set;
    .locals 2

    .prologue
    .line 191
    invoke-direct {p0}, Landroid/support/v4/f/a;->a()Landroid/support/v4/f/f;

    move-result-object v0

    iget-object v1, v0, Landroid/support/v4/f/f;->b:Landroid/support/v4/f/i;

    if-nez v1, :cond_0

    new-instance v1, Landroid/support/v4/f/i;

    invoke-direct {v1, v0}, Landroid/support/v4/f/i;-><init>(Landroid/support/v4/f/f;)V

    iput-object v1, v0, Landroid/support/v4/f/f;->b:Landroid/support/v4/f/i;

    :cond_0
    iget-object v0, v0, Landroid/support/v4/f/f;->b:Landroid/support/v4/f/i;

    return-object v0
.end method

.method public putAll(Ljava/util/Map;)V
    .locals 3

    .prologue
    .line 139
    iget v0, p0, Landroid/support/v4/f/a;->b:I

    invoke-interface {p1}, Ljava/util/Map;->size()I

    move-result v1

    add-int/2addr v0, v1

    invoke-virtual {p0, v0}, Landroid/support/v4/f/a;->a(I)V

    .line 140
    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 141
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p0, v2, v0}, Landroid/support/v4/f/a;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 143
    :cond_0
    return-void
.end method

.method public values()Ljava/util/Collection;
    .locals 2

    .prologue
    .line 203
    invoke-direct {p0}, Landroid/support/v4/f/a;->a()Landroid/support/v4/f/f;

    move-result-object v0

    iget-object v1, v0, Landroid/support/v4/f/f;->c:Landroid/support/v4/f/k;

    if-nez v1, :cond_0

    new-instance v1, Landroid/support/v4/f/k;

    invoke-direct {v1, v0}, Landroid/support/v4/f/k;-><init>(Landroid/support/v4/f/f;)V

    iput-object v1, v0, Landroid/support/v4/f/f;->c:Landroid/support/v4/f/k;

    :cond_0
    iget-object v0, v0, Landroid/support/v4/f/f;->c:Landroid/support/v4/f/k;

    return-object v0
.end method
