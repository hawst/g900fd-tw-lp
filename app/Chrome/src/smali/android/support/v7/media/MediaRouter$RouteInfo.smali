.class public final Landroid/support/v7/media/MediaRouter$RouteInfo;
.super Ljava/lang/Object;
.source "MediaRouter.java"


# instance fields
.field private final a:Landroid/support/v7/media/m;

.field private final b:Ljava/lang/String;

.field private final c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:Z

.field private g:Z

.field private final h:Ljava/util/ArrayList;

.field private i:I

.field private j:I

.field private k:I

.field private l:I

.field private m:I

.field private n:I

.field private o:Landroid/os/Bundle;

.field private p:Landroid/support/v7/media/b;


# direct methods
.method constructor <init>(Landroid/support/v7/media/m;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 763
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 703
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/support/v7/media/MediaRouter$RouteInfo;->h:Ljava/util/ArrayList;

    .line 710
    const/4 v0, -0x1

    iput v0, p0, Landroid/support/v7/media/MediaRouter$RouteInfo;->n:I

    .line 764
    iput-object p1, p0, Landroid/support/v7/media/MediaRouter$RouteInfo;->a:Landroid/support/v7/media/m;

    .line 765
    iput-object p2, p0, Landroid/support/v7/media/MediaRouter$RouteInfo;->b:Ljava/lang/String;

    .line 766
    iput-object p3, p0, Landroid/support/v7/media/MediaRouter$RouteInfo;->c:Ljava/lang/String;

    .line 767
    return-void
.end method

.method static synthetic a(Landroid/support/v7/media/MediaRouter$RouteInfo;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 695
    iget-object v0, p0, Landroid/support/v7/media/MediaRouter$RouteInfo;->b:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic b(Landroid/support/v7/media/MediaRouter$RouteInfo;)Z
    .locals 1

    .prologue
    .line 695
    iget-boolean v0, p0, Landroid/support/v7/media/MediaRouter$RouteInfo;->f:Z

    return v0
.end method

.method static synthetic c(Landroid/support/v7/media/MediaRouter$RouteInfo;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 695
    iget-object v0, p0, Landroid/support/v7/media/MediaRouter$RouteInfo;->c:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic d(Landroid/support/v7/media/MediaRouter$RouteInfo;)Landroid/support/v7/media/b;
    .locals 1

    .prologue
    .line 695
    iget-object v0, p0, Landroid/support/v7/media/MediaRouter$RouteInfo;->p:Landroid/support/v7/media/b;

    return-object v0
.end method


# virtual methods
.method final a(Landroid/support/v7/media/b;)I
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 1178
    const/4 v0, 0x0

    .line 1179
    iget-object v2, p0, Landroid/support/v7/media/MediaRouter$RouteInfo;->p:Landroid/support/v7/media/b;

    if-eq v2, p1, :cond_a

    .line 1180
    iput-object p1, p0, Landroid/support/v7/media/MediaRouter$RouteInfo;->p:Landroid/support/v7/media/b;

    .line 1181
    if-eqz p1, :cond_a

    .line 1182
    iget-object v2, p0, Landroid/support/v7/media/MediaRouter$RouteInfo;->d:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/support/v7/media/b;->b()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/support/v7/media/MediaRouter;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1183
    invoke-virtual {p1}, Landroid/support/v7/media/b;->b()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v7/media/MediaRouter$RouteInfo;->d:Ljava/lang/String;

    move v0, v1

    .line 1186
    :cond_0
    iget-object v2, p0, Landroid/support/v7/media/MediaRouter$RouteInfo;->e:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/support/v7/media/b;->c()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/support/v7/media/MediaRouter;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 1187
    invoke-virtual {p1}, Landroid/support/v7/media/b;->c()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v7/media/MediaRouter$RouteInfo;->e:Ljava/lang/String;

    move v0, v1

    .line 1190
    :cond_1
    iget-boolean v2, p0, Landroid/support/v7/media/MediaRouter$RouteInfo;->f:Z

    invoke-virtual {p1}, Landroid/support/v7/media/b;->d()Z

    move-result v3

    if-eq v2, v3, :cond_c

    .line 1191
    invoke-virtual {p1}, Landroid/support/v7/media/b;->d()Z

    move-result v0

    iput-boolean v0, p0, Landroid/support/v7/media/MediaRouter$RouteInfo;->f:Z

    .line 1194
    :goto_0
    iget-boolean v0, p0, Landroid/support/v7/media/MediaRouter$RouteInfo;->g:Z

    invoke-virtual {p1}, Landroid/support/v7/media/b;->e()Z

    move-result v2

    if-eq v0, v2, :cond_2

    .line 1195
    invoke-virtual {p1}, Landroid/support/v7/media/b;->e()Z

    move-result v0

    iput-boolean v0, p0, Landroid/support/v7/media/MediaRouter$RouteInfo;->g:Z

    .line 1196
    or-int/lit8 v1, v1, 0x1

    .line 1198
    :cond_2
    iget-object v0, p0, Landroid/support/v7/media/MediaRouter$RouteInfo;->h:Ljava/util/ArrayList;

    invoke-virtual {p1}, Landroid/support/v7/media/b;->f()Ljava/util/List;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 1199
    iget-object v0, p0, Landroid/support/v7/media/MediaRouter$RouteInfo;->h:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 1200
    iget-object v0, p0, Landroid/support/v7/media/MediaRouter$RouteInfo;->h:Ljava/util/ArrayList;

    invoke-virtual {p1}, Landroid/support/v7/media/b;->f()Ljava/util/List;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 1201
    or-int/lit8 v1, v1, 0x1

    .line 1203
    :cond_3
    iget v0, p0, Landroid/support/v7/media/MediaRouter$RouteInfo;->i:I

    invoke-virtual {p1}, Landroid/support/v7/media/b;->g()I

    move-result v2

    if-eq v0, v2, :cond_4

    .line 1204
    invoke-virtual {p1}, Landroid/support/v7/media/b;->g()I

    move-result v0

    iput v0, p0, Landroid/support/v7/media/MediaRouter$RouteInfo;->i:I

    .line 1205
    or-int/lit8 v1, v1, 0x1

    .line 1207
    :cond_4
    iget v0, p0, Landroid/support/v7/media/MediaRouter$RouteInfo;->j:I

    invoke-virtual {p1}, Landroid/support/v7/media/b;->h()I

    move-result v2

    if-eq v0, v2, :cond_5

    .line 1208
    invoke-virtual {p1}, Landroid/support/v7/media/b;->h()I

    move-result v0

    iput v0, p0, Landroid/support/v7/media/MediaRouter$RouteInfo;->j:I

    .line 1209
    or-int/lit8 v1, v1, 0x1

    .line 1211
    :cond_5
    iget v0, p0, Landroid/support/v7/media/MediaRouter$RouteInfo;->k:I

    invoke-virtual {p1}, Landroid/support/v7/media/b;->k()I

    move-result v2

    if-eq v0, v2, :cond_6

    .line 1212
    invoke-virtual {p1}, Landroid/support/v7/media/b;->k()I

    move-result v0

    iput v0, p0, Landroid/support/v7/media/MediaRouter$RouteInfo;->k:I

    .line 1213
    or-int/lit8 v1, v1, 0x3

    .line 1215
    :cond_6
    iget v0, p0, Landroid/support/v7/media/MediaRouter$RouteInfo;->l:I

    invoke-virtual {p1}, Landroid/support/v7/media/b;->i()I

    move-result v2

    if-eq v0, v2, :cond_7

    .line 1216
    invoke-virtual {p1}, Landroid/support/v7/media/b;->i()I

    move-result v0

    iput v0, p0, Landroid/support/v7/media/MediaRouter$RouteInfo;->l:I

    .line 1217
    or-int/lit8 v1, v1, 0x3

    .line 1219
    :cond_7
    iget v0, p0, Landroid/support/v7/media/MediaRouter$RouteInfo;->m:I

    invoke-virtual {p1}, Landroid/support/v7/media/b;->j()I

    move-result v2

    if-eq v0, v2, :cond_8

    .line 1220
    invoke-virtual {p1}, Landroid/support/v7/media/b;->j()I

    move-result v0

    iput v0, p0, Landroid/support/v7/media/MediaRouter$RouteInfo;->m:I

    .line 1221
    or-int/lit8 v1, v1, 0x3

    .line 1223
    :cond_8
    iget v0, p0, Landroid/support/v7/media/MediaRouter$RouteInfo;->n:I

    invoke-virtual {p1}, Landroid/support/v7/media/b;->l()I

    move-result v2

    if-eq v0, v2, :cond_9

    .line 1224
    invoke-virtual {p1}, Landroid/support/v7/media/b;->l()I

    move-result v0

    iput v0, p0, Landroid/support/v7/media/MediaRouter$RouteInfo;->n:I

    .line 1225
    or-int/lit8 v1, v1, 0x5

    .line 1228
    :cond_9
    iget-object v0, p0, Landroid/support/v7/media/MediaRouter$RouteInfo;->o:Landroid/os/Bundle;

    invoke-virtual {p1}, Landroid/support/v7/media/b;->m()Landroid/os/Bundle;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/support/v7/media/MediaRouter;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_b

    .line 1229
    invoke-virtual {p1}, Landroid/support/v7/media/b;->m()Landroid/os/Bundle;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v7/media/MediaRouter$RouteInfo;->o:Landroid/os/Bundle;

    .line 1230
    or-int/lit8 v0, v1, 0x1

    .line 1234
    :cond_a
    :goto_1
    return v0

    :cond_b
    move v0, v1

    goto :goto_1

    :cond_c
    move v1, v0

    goto/16 :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 788
    iget-object v0, p0, Landroid/support/v7/media/MediaRouter$RouteInfo;->c:Ljava/lang/String;

    return-object v0
.end method

.method public final a(I)V
    .locals 3

    .prologue
    .line 1084
    invoke-static {}, Landroid/support/v7/media/MediaRouter;->d()V

    .line 1085
    sget-object v0, Landroid/support/v7/media/MediaRouter;->a:Landroid/support/v7/media/j;

    iget v1, p0, Landroid/support/v7/media/MediaRouter$RouteInfo;->m:I

    const/4 v2, 0x0

    invoke-static {v2, p1}, Ljava/lang/Math;->max(II)I

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    invoke-virtual {v0, p0, v1}, Landroid/support/v7/media/j;->a(Landroid/support/v7/media/MediaRouter$RouteInfo;I)V

    .line 1086
    return-void
.end method

.method public final a(Landroid/content/Intent;Landroid/support/v7/media/i;)V
    .locals 2

    .prologue
    .line 1015
    if-nez p1, :cond_0

    .line 1016
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "intent must not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1018
    :cond_0
    invoke-static {}, Landroid/support/v7/media/MediaRouter;->d()V

    .line 1020
    sget-object v0, Landroid/support/v7/media/MediaRouter;->a:Landroid/support/v7/media/j;

    invoke-virtual {v0, p0, p1, p2}, Landroid/support/v7/media/j;->a(Landroid/support/v7/media/MediaRouter$RouteInfo;Landroid/content/Intent;Landroid/support/v7/media/i;)V

    .line 1021
    return-void
.end method

.method public final a(Landroid/content/Intent;)Z
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 981
    if-nez p1, :cond_0

    .line 982
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "intent must not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 984
    :cond_0
    invoke-static {}, Landroid/support/v7/media/MediaRouter;->d()V

    .line 986
    sget-object v0, Landroid/support/v7/media/MediaRouter;->a:Landroid/support/v7/media/j;

    invoke-virtual {v0}, Landroid/support/v7/media/j;->b()Landroid/content/ContentResolver;

    move-result-object v4

    .line 987
    iget-object v0, p0, Landroid/support/v7/media/MediaRouter$RouteInfo;->h:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v5

    move v3, v2

    .line 988
    :goto_0
    if-ge v3, v5, :cond_2

    .line 989
    iget-object v0, p0, Landroid/support/v7/media/MediaRouter$RouteInfo;->h:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/IntentFilter;

    const-string/jumbo v6, "MediaRouter"

    invoke-virtual {v0, v4, p1, v1, v6}, Landroid/content/IntentFilter;->match(Landroid/content/ContentResolver;Landroid/content/Intent;ZLjava/lang/String;)I

    move-result v0

    if-ltz v0, :cond_1

    move v0, v1

    .line 993
    :goto_1
    return v0

    .line 988
    :cond_1
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    :cond_2
    move v0, v2

    .line 993
    goto :goto_1
.end method

.method public final a(Landroid/support/v7/media/e;)Z
    .locals 2

    .prologue
    .line 887
    if-nez p1, :cond_0

    .line 888
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "selector must not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 890
    :cond_0
    invoke-static {}, Landroid/support/v7/media/MediaRouter;->d()V

    .line 891
    iget-object v0, p0, Landroid/support/v7/media/MediaRouter$RouteInfo;->h:Ljava/util/ArrayList;

    invoke-virtual {p1, v0}, Landroid/support/v7/media/e;->a(Ljava/util/List;)Z

    move-result v0

    return v0
.end method

.method public final a(Ljava/lang/String;)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 913
    if-nez p1, :cond_0

    .line 914
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "category must not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 916
    :cond_0
    invoke-static {}, Landroid/support/v7/media/MediaRouter;->d()V

    .line 918
    iget-object v0, p0, Landroid/support/v7/media/MediaRouter$RouteInfo;->h:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    move v2, v1

    .line 919
    :goto_0
    if-ge v2, v3, :cond_2

    .line 920
    iget-object v0, p0, Landroid/support/v7/media/MediaRouter$RouteInfo;->h:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/IntentFilter;

    invoke-virtual {v0, p1}, Landroid/content/IntentFilter;->hasCategory(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 921
    const/4 v0, 0x1

    .line 924
    :goto_1
    return v0

    .line 919
    :cond_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_2
    move v0, v1

    .line 924
    goto :goto_1
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 802
    iget-object v0, p0, Landroid/support/v7/media/MediaRouter$RouteInfo;->d:Ljava/lang/String;

    return-object v0
.end method

.method public final b(I)V
    .locals 1

    .prologue
    .line 1098
    invoke-static {}, Landroid/support/v7/media/MediaRouter;->d()V

    .line 1099
    if-eqz p1, :cond_0

    .line 1100
    sget-object v0, Landroid/support/v7/media/MediaRouter;->a:Landroid/support/v7/media/j;

    invoke-virtual {v0, p0, p1}, Landroid/support/v7/media/j;->b(Landroid/support/v7/media/MediaRouter$RouteInfo;I)V

    .line 1102
    :cond_0
    return-void
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 816
    iget-object v0, p0, Landroid/support/v7/media/MediaRouter$RouteInfo;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 825
    iget-boolean v0, p0, Landroid/support/v7/media/MediaRouter$RouteInfo;->f:Z

    return v0
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 835
    iget-boolean v0, p0, Landroid/support/v7/media/MediaRouter$RouteInfo;->g:Z

    return v0
.end method

.method public final f()Z
    .locals 1

    .prologue
    .line 846
    invoke-static {}, Landroid/support/v7/media/MediaRouter;->d()V

    .line 847
    sget-object v0, Landroid/support/v7/media/MediaRouter;->a:Landroid/support/v7/media/j;

    invoke-virtual {v0}, Landroid/support/v7/media/j;->e()Landroid/support/v7/media/MediaRouter$RouteInfo;

    move-result-object v0

    if-ne v0, p0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final g()Z
    .locals 1

    .prologue
    .line 858
    invoke-static {}, Landroid/support/v7/media/MediaRouter;->d()V

    .line 859
    sget-object v0, Landroid/support/v7/media/MediaRouter;->a:Landroid/support/v7/media/j;

    invoke-virtual {v0}, Landroid/support/v7/media/j;->d()Landroid/support/v7/media/MediaRouter$RouteInfo;

    move-result-object v0

    if-ne v0, p0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final h()I
    .locals 1

    .prologue
    .line 1031
    iget v0, p0, Landroid/support/v7/media/MediaRouter$RouteInfo;->i:I

    return v0
.end method

.method public final i()I
    .locals 1

    .prologue
    .line 1040
    iget v0, p0, Landroid/support/v7/media/MediaRouter$RouteInfo;->j:I

    return v0
.end method

.method public final j()I
    .locals 1

    .prologue
    .line 1051
    iget v0, p0, Landroid/support/v7/media/MediaRouter$RouteInfo;->k:I

    return v0
.end method

.method public final k()I
    .locals 1

    .prologue
    .line 1061
    iget v0, p0, Landroid/support/v7/media/MediaRouter$RouteInfo;->l:I

    return v0
.end method

.method public final l()I
    .locals 1

    .prologue
    .line 1071
    iget v0, p0, Landroid/support/v7/media/MediaRouter$RouteInfo;->m:I

    return v0
.end method

.method public final m()Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 1148
    iget-object v0, p0, Landroid/support/v7/media/MediaRouter$RouteInfo;->o:Landroid/os/Bundle;

    return-object v0
.end method

.method public final n()V
    .locals 1

    .prologue
    .line 1155
    invoke-static {}, Landroid/support/v7/media/MediaRouter;->d()V

    .line 1156
    sget-object v0, Landroid/support/v7/media/MediaRouter;->a:Landroid/support/v7/media/j;

    invoke-virtual {v0, p0}, Landroid/support/v7/media/j;->a(Landroid/support/v7/media/MediaRouter$RouteInfo;)V

    .line 1157
    return-void
.end method

.method final o()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1238
    iget-object v0, p0, Landroid/support/v7/media/MediaRouter$RouteInfo;->b:Ljava/lang/String;

    return-object v0
.end method

.method final p()Landroid/support/v7/media/MediaRouteProvider;
    .locals 1

    .prologue
    .line 1242
    iget-object v0, p0, Landroid/support/v7/media/MediaRouter$RouteInfo;->a:Landroid/support/v7/media/m;

    invoke-virtual {v0}, Landroid/support/v7/media/m;->a()Landroid/support/v7/media/MediaRouteProvider;

    move-result-object v0

    return-object v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1161
    new-instance v0, Ljava/lang/StringBuilder;

    const-string/jumbo v1, "MediaRouter.RouteInfo{ uniqueId="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Landroid/support/v7/media/MediaRouter$RouteInfo;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Landroid/support/v7/media/MediaRouter$RouteInfo;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", description="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Landroid/support/v7/media/MediaRouter$RouteInfo;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", enabled="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Landroid/support/v7/media/MediaRouter$RouteInfo;->f:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", connecting="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Landroid/support/v7/media/MediaRouter$RouteInfo;->g:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", playbackType="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Landroid/support/v7/media/MediaRouter$RouteInfo;->i:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", playbackStream="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Landroid/support/v7/media/MediaRouter$RouteInfo;->j:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", volumeHandling="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Landroid/support/v7/media/MediaRouter$RouteInfo;->k:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", volume="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Landroid/support/v7/media/MediaRouter$RouteInfo;->l:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", volumeMax="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Landroid/support/v7/media/MediaRouter$RouteInfo;->m:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", presentationDisplayId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Landroid/support/v7/media/MediaRouter$RouteInfo;->n:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", extras="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Landroid/support/v7/media/MediaRouter$RouteInfo;->o:Landroid/os/Bundle;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", providerPackageName="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Landroid/support/v7/media/MediaRouter$RouteInfo;->a:Landroid/support/v7/media/m;

    invoke-virtual {v1}, Landroid/support/v7/media/m;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " }"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
