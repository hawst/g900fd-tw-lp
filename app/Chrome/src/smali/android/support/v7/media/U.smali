.class Landroid/support/v7/media/U;
.super Landroid/support/v7/media/Q;
.source "SystemMediaRouteProvider.java"

# interfaces
.implements Landroid/support/v7/media/u;


# instance fields
.field private g:Landroid/support/v7/media/t;

.field private h:Landroid/support/v7/media/w;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/support/v7/media/Z;)V
    .locals 0

    .prologue
    .line 716
    invoke-direct {p0, p1, p2}, Landroid/support/v7/media/Q;-><init>(Landroid/content/Context;Landroid/support/v7/media/Z;)V

    .line 717
    return-void
.end method


# virtual methods
.method protected a(Landroid/support/v7/media/S;Landroid/support/v7/media/c;)V
    .locals 1

    .prologue
    .line 742
    invoke-super {p0, p1, p2}, Landroid/support/v7/media/Q;->a(Landroid/support/v7/media/S;Landroid/support/v7/media/c;)V

    .line 744
    iget-object v0, p1, Landroid/support/v7/media/S;->a:Ljava/lang/Object;

    check-cast v0, Landroid/media/MediaRouter$RouteInfo;

    invoke-virtual {v0}, Landroid/media/MediaRouter$RouteInfo;->isEnabled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 745
    const/4 v0, 0x0

    invoke-virtual {p2, v0}, Landroid/support/v7/media/c;->a(Z)Landroid/support/v7/media/c;

    .line 748
    :cond_0
    invoke-virtual {p0, p1}, Landroid/support/v7/media/U;->a(Landroid/support/v7/media/S;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 749
    const/4 v0, 0x1

    invoke-virtual {p2, v0}, Landroid/support/v7/media/c;->b(Z)Landroid/support/v7/media/c;

    .line 752
    :cond_1
    iget-object v0, p1, Landroid/support/v7/media/S;->a:Ljava/lang/Object;

    check-cast v0, Landroid/media/MediaRouter$RouteInfo;

    invoke-virtual {v0}, Landroid/media/MediaRouter$RouteInfo;->getPresentationDisplay()Landroid/view/Display;

    move-result-object v0

    .line 754
    if-eqz v0, :cond_2

    .line 755
    invoke-virtual {v0}, Landroid/view/Display;->getDisplayId()I

    move-result v0

    invoke-virtual {p2, v0}, Landroid/support/v7/media/c;->f(I)Landroid/support/v7/media/c;

    .line 757
    :cond_2
    return-void
.end method

.method protected a(Landroid/support/v7/media/S;)Z
    .locals 2

    .prologue
    .line 776
    iget-object v0, p0, Landroid/support/v7/media/U;->h:Landroid/support/v7/media/w;

    if-nez v0, :cond_0

    .line 777
    new-instance v0, Landroid/support/v7/media/w;

    invoke-direct {v0}, Landroid/support/v7/media/w;-><init>()V

    iput-object v0, p0, Landroid/support/v7/media/U;->h:Landroid/support/v7/media/w;

    .line 779
    :cond_0
    iget-object v0, p0, Landroid/support/v7/media/U;->h:Landroid/support/v7/media/w;

    iget-object v1, p1, Landroid/support/v7/media/S;->a:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Landroid/support/v7/media/w;->a(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method protected b()V
    .locals 3

    .prologue
    .line 761
    invoke-super {p0}, Landroid/support/v7/media/Q;->b()V

    .line 763
    iget-object v0, p0, Landroid/support/v7/media/U;->g:Landroid/support/v7/media/t;

    if-nez v0, :cond_0

    .line 764
    new-instance v0, Landroid/support/v7/media/t;

    invoke-virtual {p0}, Landroid/support/v7/media/U;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p0}, Landroid/support/v7/media/U;->getHandler()Landroid/os/Handler;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/support/v7/media/t;-><init>(Landroid/content/Context;Landroid/os/Handler;)V

    iput-object v0, p0, Landroid/support/v7/media/U;->g:Landroid/support/v7/media/t;

    .line 767
    :cond_0
    iget-object v1, p0, Landroid/support/v7/media/U;->g:Landroid/support/v7/media/t;

    iget-boolean v0, p0, Landroid/support/v7/media/U;->d:Z

    if-eqz v0, :cond_1

    iget v0, p0, Landroid/support/v7/media/U;->c:I

    :goto_0
    invoke-virtual {v1, v0}, Landroid/support/v7/media/t;->a(I)V

    .line 768
    return-void

    .line 767
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final c()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 772
    new-instance v0, Landroid/support/v7/media/v;

    invoke-direct {v0, p0}, Landroid/support/v7/media/v;-><init>(Landroid/support/v7/media/u;)V

    return-object v0
.end method

.method public final f(Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 721
    invoke-virtual {p0, p1}, Landroid/support/v7/media/U;->g(Ljava/lang/Object;)I

    move-result v0

    .line 722
    if-ltz v0, :cond_0

    .line 723
    iget-object v1, p0, Landroid/support/v7/media/U;->f:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/media/S;

    .line 724
    check-cast p1, Landroid/media/MediaRouter$RouteInfo;

    invoke-virtual {p1}, Landroid/media/MediaRouter$RouteInfo;->getPresentationDisplay()Landroid/view/Display;

    move-result-object v1

    .line 726
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Landroid/view/Display;->getDisplayId()I

    move-result v1

    .line 728
    :goto_0
    iget-object v2, v0, Landroid/support/v7/media/S;->c:Landroid/support/v7/media/b;

    invoke-virtual {v2}, Landroid/support/v7/media/b;->l()I

    move-result v2

    if-eq v1, v2, :cond_0

    .line 730
    new-instance v2, Landroid/support/v7/media/c;

    iget-object v3, v0, Landroid/support/v7/media/S;->c:Landroid/support/v7/media/b;

    invoke-direct {v2, v3}, Landroid/support/v7/media/c;-><init>(Landroid/support/v7/media/b;)V

    invoke-virtual {v2, v1}, Landroid/support/v7/media/c;->f(I)Landroid/support/v7/media/c;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v7/media/c;->a()Landroid/support/v7/media/b;

    move-result-object v1

    iput-object v1, v0, Landroid/support/v7/media/S;->c:Landroid/support/v7/media/b;

    .line 734
    invoke-virtual {p0}, Landroid/support/v7/media/U;->a()V

    .line 737
    :cond_0
    return-void

    .line 726
    :cond_1
    const/4 v1, -0x1

    goto :goto_0
.end method
