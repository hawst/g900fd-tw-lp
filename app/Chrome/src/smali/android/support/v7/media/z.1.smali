.class final Landroid/support/v7/media/z;
.super Ljava/lang/Object;
.source "RegisteredMediaRouteProvider.java"

# interfaces
.implements Landroid/os/IBinder$DeathRecipient;


# instance fields
.field final synthetic a:Landroid/support/v7/media/y;

.field private final b:Landroid/os/Messenger;

.field private final c:Landroid/support/v7/media/E;

.field private final d:Landroid/os/Messenger;

.field private e:I

.field private f:I

.field private g:I

.field private h:I

.field private final i:Landroid/util/SparseArray;


# direct methods
.method public constructor <init>(Landroid/support/v7/media/y;Landroid/os/Messenger;)V
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 394
    iput-object p1, p0, Landroid/support/v7/media/z;->a:Landroid/support/v7/media/y;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 386
    iput v0, p0, Landroid/support/v7/media/z;->e:I

    .line 387
    iput v0, p0, Landroid/support/v7/media/z;->f:I

    .line 391
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Landroid/support/v7/media/z;->i:Landroid/util/SparseArray;

    .line 395
    iput-object p2, p0, Landroid/support/v7/media/z;->b:Landroid/os/Messenger;

    .line 396
    new-instance v0, Landroid/support/v7/media/E;

    invoke-direct {v0, p0}, Landroid/support/v7/media/E;-><init>(Landroid/support/v7/media/z;)V

    iput-object v0, p0, Landroid/support/v7/media/z;->c:Landroid/support/v7/media/E;

    .line 397
    new-instance v0, Landroid/os/Messenger;

    iget-object v1, p0, Landroid/support/v7/media/z;->c:Landroid/support/v7/media/E;

    invoke-direct {v0, v1}, Landroid/os/Messenger;-><init>(Landroid/os/Handler;)V

    iput-object v0, p0, Landroid/support/v7/media/z;->d:Landroid/os/Messenger;

    .line 398
    return-void
.end method

.method static synthetic a(Landroid/support/v7/media/z;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 381
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Landroid/support/v7/media/z;->i:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/media/z;->i:Landroid/util/SparseArray;

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/media/i;

    invoke-virtual {v0, v2, v2}, Landroid/support/v7/media/i;->onError(Ljava/lang/String;Landroid/os/Bundle;)V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Landroid/support/v7/media/z;->i:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->clear()V

    return-void
.end method

.method private a(IIILjava/lang/Object;Landroid/os/Bundle;)Z
    .locals 3

    .prologue
    .line 566
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    .line 567
    iput p1, v0, Landroid/os/Message;->what:I

    .line 568
    iput p2, v0, Landroid/os/Message;->arg1:I

    .line 569
    iput p3, v0, Landroid/os/Message;->arg2:I

    .line 570
    iput-object p4, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 571
    invoke-virtual {v0, p5}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 572
    iget-object v1, p0, Landroid/support/v7/media/z;->d:Landroid/os/Messenger;

    iput-object v1, v0, Landroid/os/Message;->replyTo:Landroid/os/Messenger;

    .line 574
    :try_start_0
    iget-object v1, p0, Landroid/support/v7/media/z;->b:Landroid/os/Messenger;

    invoke-virtual {v1, v0}, Landroid/os/Messenger;->send(Landroid/os/Message;)V
    :try_end_0
    .catch Landroid/os/DeadObjectException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 575
    const/4 v0, 0x1

    .line 583
    :goto_0
    return v0

    .line 578
    :catch_0
    move-exception v0

    .line 579
    const/4 v1, 0x2

    if-eq p1, v1, :cond_0

    .line 580
    const-string/jumbo v1, "MediaRouteProviderProxy"

    const-string/jumbo v2, "Could not send message to service."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 583
    :cond_0
    :goto_1
    const/4 v0, 0x0

    goto :goto_0

    .line 576
    :catch_1
    move-exception v0

    goto :goto_1
.end method

.method public static c()Z
    .locals 1

    .prologue
    .line 452
    const/4 v0, 0x1

    return v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)I
    .locals 6

    .prologue
    .line 510
    iget v3, p0, Landroid/support/v7/media/z;->f:I

    add-int/lit8 v0, v3, 0x1

    iput v0, p0, Landroid/support/v7/media/z;->f:I

    .line 511
    new-instance v5, Landroid/os/Bundle;

    invoke-direct {v5}, Landroid/os/Bundle;-><init>()V

    .line 512
    const-string/jumbo v0, "routeId"

    invoke-virtual {v5, v0, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 513
    const/4 v1, 0x3

    iget v2, p0, Landroid/support/v7/media/z;->e:I

    add-int/lit8 v0, v2, 0x1

    iput v0, p0, Landroid/support/v7/media/z;->e:I

    const/4 v4, 0x0

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Landroid/support/v7/media/z;->a(IIILjava/lang/Object;Landroid/os/Bundle;)Z

    .line 515
    return v3
.end method

.method public final a(II)V
    .locals 6

    .prologue
    .line 534
    new-instance v5, Landroid/os/Bundle;

    invoke-direct {v5}, Landroid/os/Bundle;-><init>()V

    .line 535
    const-string/jumbo v0, "volume"

    invoke-virtual {v5, v0, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 536
    const/4 v1, 0x7

    iget v2, p0, Landroid/support/v7/media/z;->e:I

    add-int/lit8 v0, v2, 0x1

    iput v0, p0, Landroid/support/v7/media/z;->e:I

    const/4 v4, 0x0

    move-object v0, p0

    move v3, p1

    invoke-direct/range {v0 .. v5}, Landroid/support/v7/media/z;->a(IIILjava/lang/Object;Landroid/os/Bundle;)Z

    .line 538
    return-void
.end method

.method public final a(Landroid/support/v7/media/d;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 561
    const/16 v1, 0xa

    iget v2, p0, Landroid/support/v7/media/z;->e:I

    add-int/lit8 v0, v2, 0x1

    iput v0, p0, Landroid/support/v7/media/z;->e:I

    const/4 v3, 0x0

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/support/v7/media/d;->d()Landroid/os/Bundle;

    move-result-object v4

    :goto_0
    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Landroid/support/v7/media/z;->a(IIILjava/lang/Object;Landroid/os/Bundle;)Z

    .line 563
    return-void

    :cond_0
    move-object v4, v5

    .line 561
    goto :goto_0
.end method

.method public final a()Z
    .locals 7

    .prologue
    const/4 v4, 0x0

    const/4 v1, 0x1

    const/4 v6, 0x0

    .line 401
    iget v0, p0, Landroid/support/v7/media/z;->e:I

    add-int/lit8 v2, v0, 0x1

    iput v2, p0, Landroid/support/v7/media/z;->e:I

    iput v0, p0, Landroid/support/v7/media/z;->h:I

    .line 402
    iget v2, p0, Landroid/support/v7/media/z;->h:I

    move-object v0, p0

    move v3, v1

    move-object v5, v4

    invoke-direct/range {v0 .. v5}, Landroid/support/v7/media/z;->a(IIILjava/lang/Object;Landroid/os/Bundle;)Z

    move-result v0

    if-nez v0, :cond_0

    move v1, v6

    .line 414
    :goto_0
    return v1

    .line 409
    :cond_0
    :try_start_0
    iget-object v0, p0, Landroid/support/v7/media/z;->b:Landroid/os/Messenger;

    invoke-virtual {v0}, Landroid/os/Messenger;->getBinder()Landroid/os/IBinder;

    move-result-object v0

    const/4 v2, 0x0

    invoke-interface {v0, p0, v2}, Landroid/os/IBinder;->linkToDeath(Landroid/os/IBinder$DeathRecipient;I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 412
    :catch_0
    move-exception v0

    invoke-virtual {p0}, Landroid/support/v7/media/z;->binderDied()V

    move v1, v6

    .line 414
    goto :goto_0
.end method

.method public final a(I)Z
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 439
    iget v0, p0, Landroid/support/v7/media/z;->h:I

    if-ne p1, v0, :cond_0

    .line 440
    const/4 v0, 0x0

    iput v0, p0, Landroid/support/v7/media/z;->h:I

    .line 441
    iget-object v0, p0, Landroid/support/v7/media/z;->a:Landroid/support/v7/media/y;

    const-string/jumbo v1, "Registation failed"

    invoke-static {v0, p0, v1}, Landroid/support/v7/media/y;->a(Landroid/support/v7/media/y;Landroid/support/v7/media/z;Ljava/lang/String;)V

    .line 443
    :cond_0
    iget-object v0, p0, Landroid/support/v7/media/z;->i:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/media/i;

    .line 444
    if-eqz v0, :cond_1

    .line 445
    iget-object v1, p0, Landroid/support/v7/media/z;->i:Landroid/util/SparseArray;

    invoke-virtual {v1, p1}, Landroid/util/SparseArray;->remove(I)V

    .line 446
    invoke-virtual {v0, v2, v2}, Landroid/support/v7/media/i;->onError(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 448
    :cond_1
    const/4 v0, 0x1

    return v0
.end method

.method public final a(IILandroid/os/Bundle;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 457
    iget v1, p0, Landroid/support/v7/media/z;->g:I

    if-nez v1, :cond_0

    iget v1, p0, Landroid/support/v7/media/z;->h:I

    if-ne p1, v1, :cond_0

    if-lez p2, :cond_0

    .line 460
    iput v0, p0, Landroid/support/v7/media/z;->h:I

    .line 461
    iput p2, p0, Landroid/support/v7/media/z;->g:I

    .line 462
    iget-object v0, p0, Landroid/support/v7/media/z;->a:Landroid/support/v7/media/y;

    invoke-static {p3}, Landroid/support/v7/media/MediaRouteProviderDescriptor;->fromBundle(Landroid/os/Bundle;)Landroid/support/v7/media/MediaRouteProviderDescriptor;

    move-result-object v1

    invoke-static {v0, p0, v1}, Landroid/support/v7/media/y;->a(Landroid/support/v7/media/y;Landroid/support/v7/media/z;Landroid/support/v7/media/MediaRouteProviderDescriptor;)V

    .line 464
    iget-object v0, p0, Landroid/support/v7/media/z;->a:Landroid/support/v7/media/y;

    invoke-static {v0, p0}, Landroid/support/v7/media/y;->a(Landroid/support/v7/media/y;Landroid/support/v7/media/z;)V

    .line 465
    const/4 v0, 0x1

    .line 467
    :cond_0
    return v0
.end method

.method public final a(ILandroid/content/Intent;Landroid/support/v7/media/i;)Z
    .locals 6

    .prologue
    .line 549
    iget v2, p0, Landroid/support/v7/media/z;->e:I

    add-int/lit8 v0, v2, 0x1

    iput v0, p0, Landroid/support/v7/media/z;->e:I

    .line 550
    const/16 v1, 0x9

    const/4 v5, 0x0

    move-object v0, p0

    move v3, p1

    move-object v4, p2

    invoke-direct/range {v0 .. v5}, Landroid/support/v7/media/z;->a(IIILjava/lang/Object;Landroid/os/Bundle;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 552
    if-eqz p3, :cond_0

    .line 553
    iget-object v0, p0, Landroid/support/v7/media/z;->i:Landroid/util/SparseArray;

    invoke-virtual {v0, v2, p3}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 555
    :cond_0
    const/4 v0, 0x1

    .line 557
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(ILandroid/os/Bundle;)Z
    .locals 2

    .prologue
    .line 480
    iget-object v0, p0, Landroid/support/v7/media/z;->i:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/media/i;

    .line 481
    if-eqz v0, :cond_0

    .line 482
    iget-object v1, p0, Landroid/support/v7/media/z;->i:Landroid/util/SparseArray;

    invoke-virtual {v1, p1}, Landroid/util/SparseArray;->remove(I)V

    .line 483
    invoke-virtual {v0, p2}, Landroid/support/v7/media/i;->onResult(Landroid/os/Bundle;)V

    .line 484
    const/4 v0, 0x1

    .line 486
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(ILjava/lang/String;Landroid/os/Bundle;)Z
    .locals 2

    .prologue
    .line 490
    iget-object v0, p0, Landroid/support/v7/media/z;->i:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/media/i;

    .line 491
    if-eqz v0, :cond_0

    .line 492
    iget-object v1, p0, Landroid/support/v7/media/z;->i:Landroid/util/SparseArray;

    invoke-virtual {v1, p1}, Landroid/util/SparseArray;->remove(I)V

    .line 493
    invoke-virtual {v0, p2, p3}, Landroid/support/v7/media/i;->onError(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 494
    const/4 v0, 0x1

    .line 496
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Landroid/os/Bundle;)Z
    .locals 2

    .prologue
    .line 471
    iget v0, p0, Landroid/support/v7/media/z;->g:I

    if-eqz v0, :cond_0

    .line 472
    iget-object v0, p0, Landroid/support/v7/media/z;->a:Landroid/support/v7/media/y;

    invoke-static {p1}, Landroid/support/v7/media/MediaRouteProviderDescriptor;->fromBundle(Landroid/os/Bundle;)Landroid/support/v7/media/MediaRouteProviderDescriptor;

    move-result-object v1

    invoke-static {v0, p0, v1}, Landroid/support/v7/media/y;->a(Landroid/support/v7/media/y;Landroid/support/v7/media/z;Landroid/support/v7/media/MediaRouteProviderDescriptor;)V

    .line 474
    const/4 v0, 0x1

    .line 476
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()V
    .locals 6

    .prologue
    const/4 v4, 0x0

    const/4 v2, 0x0

    .line 418
    const/4 v1, 0x2

    move-object v0, p0

    move v3, v2

    move-object v5, v4

    invoke-direct/range {v0 .. v5}, Landroid/support/v7/media/z;->a(IIILjava/lang/Object;Landroid/os/Bundle;)Z

    .line 419
    iget-object v0, p0, Landroid/support/v7/media/z;->c:Landroid/support/v7/media/E;

    invoke-virtual {v0}, Landroid/support/v7/media/E;->a()V

    .line 420
    iget-object v0, p0, Landroid/support/v7/media/z;->b:Landroid/os/Messenger;

    invoke-virtual {v0}, Landroid/os/Messenger;->getBinder()Landroid/os/IBinder;

    move-result-object v0

    invoke-interface {v0, p0, v2}, Landroid/os/IBinder;->unlinkToDeath(Landroid/os/IBinder$DeathRecipient;I)Z

    .line 422
    iget-object v0, p0, Landroid/support/v7/media/z;->a:Landroid/support/v7/media/y;

    invoke-static {v0}, Landroid/support/v7/media/y;->a(Landroid/support/v7/media/y;)Landroid/support/v7/media/D;

    move-result-object v0

    new-instance v1, Landroid/support/v7/media/A;

    invoke-direct {v1, p0}, Landroid/support/v7/media/A;-><init>(Landroid/support/v7/media/z;)V

    invoke-virtual {v0, v1}, Landroid/support/v7/media/D;->post(Ljava/lang/Runnable;)Z

    .line 428
    return-void
.end method

.method public final b(I)V
    .locals 6

    .prologue
    const/4 v4, 0x0

    .line 519
    const/4 v1, 0x4

    iget v2, p0, Landroid/support/v7/media/z;->e:I

    add-int/lit8 v0, v2, 0x1

    iput v0, p0, Landroid/support/v7/media/z;->e:I

    move-object v0, p0

    move v3, p1

    move-object v5, v4

    invoke-direct/range {v0 .. v5}, Landroid/support/v7/media/z;->a(IIILjava/lang/Object;Landroid/os/Bundle;)Z

    .line 521
    return-void
.end method

.method public final b(II)V
    .locals 6

    .prologue
    .line 541
    new-instance v5, Landroid/os/Bundle;

    invoke-direct {v5}, Landroid/os/Bundle;-><init>()V

    .line 542
    const-string/jumbo v0, "volume"

    invoke-virtual {v5, v0, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 543
    const/16 v1, 0x8

    iget v2, p0, Landroid/support/v7/media/z;->e:I

    add-int/lit8 v0, v2, 0x1

    iput v0, p0, Landroid/support/v7/media/z;->e:I

    const/4 v4, 0x0

    move-object v0, p0

    move v3, p1

    invoke-direct/range {v0 .. v5}, Landroid/support/v7/media/z;->a(IIILjava/lang/Object;Landroid/os/Bundle;)Z

    .line 545
    return-void
.end method

.method public final binderDied()V
    .locals 2

    .prologue
    .line 501
    iget-object v0, p0, Landroid/support/v7/media/z;->a:Landroid/support/v7/media/y;

    invoke-static {v0}, Landroid/support/v7/media/y;->a(Landroid/support/v7/media/y;)Landroid/support/v7/media/D;

    move-result-object v0

    new-instance v1, Landroid/support/v7/media/B;

    invoke-direct {v1, p0}, Landroid/support/v7/media/B;-><init>(Landroid/support/v7/media/z;)V

    invoke-virtual {v0, v1}, Landroid/support/v7/media/D;->post(Ljava/lang/Runnable;)Z

    .line 507
    return-void
.end method

.method public final c(I)V
    .locals 6

    .prologue
    const/4 v4, 0x0

    .line 524
    const/4 v1, 0x5

    iget v2, p0, Landroid/support/v7/media/z;->e:I

    add-int/lit8 v0, v2, 0x1

    iput v0, p0, Landroid/support/v7/media/z;->e:I

    move-object v0, p0

    move v3, p1

    move-object v5, v4

    invoke-direct/range {v0 .. v5}, Landroid/support/v7/media/z;->a(IIILjava/lang/Object;Landroid/os/Bundle;)Z

    .line 526
    return-void
.end method

.method public final d(I)V
    .locals 6

    .prologue
    const/4 v4, 0x0

    .line 529
    const/4 v1, 0x6

    iget v2, p0, Landroid/support/v7/media/z;->e:I

    add-int/lit8 v0, v2, 0x1

    iput v0, p0, Landroid/support/v7/media/z;->e:I

    move-object v0, p0

    move v3, p1

    move-object v5, v4

    invoke-direct/range {v0 .. v5}, Landroid/support/v7/media/z;->a(IIILjava/lang/Object;Landroid/os/Bundle;)Z

    .line 531
    return-void
.end method
