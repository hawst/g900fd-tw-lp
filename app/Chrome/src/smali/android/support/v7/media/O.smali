.class public Landroid/support/v7/media/O;
.super Ljava/lang/Object;
.source "MediaRouter.java"


# instance fields
.field final synthetic a:Landroid/support/v7/media/j;

.field private final b:Landroid/support/v7/media/J;

.field private c:Z


# direct methods
.method public constructor <init>(Landroid/support/v7/media/j;Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 2167
    iput-object p1, p0, Landroid/support/v7/media/O;->a:Landroid/support/v7/media/j;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2168
    invoke-static {p1}, Landroid/support/v7/media/j;->c(Landroid/support/v7/media/j;)Landroid/content/Context;

    move-result-object v1

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x10

    if-lt v0, v2, :cond_0

    new-instance v0, Landroid/support/v7/media/K;

    invoke-direct {v0, v1, p2}, Landroid/support/v7/media/K;-><init>(Landroid/content/Context;Ljava/lang/Object;)V

    :goto_0
    iput-object v0, p0, Landroid/support/v7/media/O;->b:Landroid/support/v7/media/J;

    .line 2169
    iget-object v0, p0, Landroid/support/v7/media/O;->b:Landroid/support/v7/media/J;

    invoke-virtual {v0, p0}, Landroid/support/v7/media/J;->a(Landroid/support/v7/media/O;)V

    .line 2170
    invoke-virtual {p0}, Landroid/support/v7/media/O;->c()V

    .line 2171
    return-void

    .line 2168
    :cond_0
    new-instance v0, Landroid/support/v7/media/M;

    invoke-direct {v0, v1, p2}, Landroid/support/v7/media/M;-><init>(Landroid/content/Context;Ljava/lang/Object;)V

    goto :goto_0
.end method


# virtual methods
.method public a()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2174
    iget-object v0, p0, Landroid/support/v7/media/O;->b:Landroid/support/v7/media/J;

    invoke-virtual {v0}, Landroid/support/v7/media/J;->a()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public a(I)V
    .locals 1

    .prologue
    .line 2195
    iget-boolean v0, p0, Landroid/support/v7/media/O;->c:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/media/O;->a:Landroid/support/v7/media/j;

    invoke-static {v0}, Landroid/support/v7/media/j;->a(Landroid/support/v7/media/j;)Landroid/support/v7/media/MediaRouter$RouteInfo;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2196
    iget-object v0, p0, Landroid/support/v7/media/O;->a:Landroid/support/v7/media/j;

    invoke-static {v0}, Landroid/support/v7/media/j;->a(Landroid/support/v7/media/j;)Landroid/support/v7/media/MediaRouter$RouteInfo;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/support/v7/media/MediaRouter$RouteInfo;->b(I)V

    .line 2198
    :cond_0
    return-void
.end method

.method public b()V
    .locals 2

    .prologue
    .line 2178
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v7/media/O;->c:Z

    .line 2179
    iget-object v0, p0, Landroid/support/v7/media/O;->b:Landroid/support/v7/media/J;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v7/media/J;->a(Landroid/support/v7/media/O;)V

    .line 2180
    return-void
.end method

.method public b(I)V
    .locals 1

    .prologue
    .line 2188
    iget-boolean v0, p0, Landroid/support/v7/media/O;->c:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/media/O;->a:Landroid/support/v7/media/j;

    invoke-static {v0}, Landroid/support/v7/media/j;->a(Landroid/support/v7/media/j;)Landroid/support/v7/media/MediaRouter$RouteInfo;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2189
    iget-object v0, p0, Landroid/support/v7/media/O;->a:Landroid/support/v7/media/j;

    invoke-static {v0}, Landroid/support/v7/media/j;->a(Landroid/support/v7/media/j;)Landroid/support/v7/media/MediaRouter$RouteInfo;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/support/v7/media/MediaRouter$RouteInfo;->a(I)V

    .line 2191
    :cond_0
    return-void
.end method

.method public c()V
    .locals 2

    .prologue
    .line 2183
    iget-object v0, p0, Landroid/support/v7/media/O;->b:Landroid/support/v7/media/J;

    iget-object v1, p0, Landroid/support/v7/media/O;->a:Landroid/support/v7/media/j;

    invoke-static {v1}, Landroid/support/v7/media/j;->b(Landroid/support/v7/media/j;)Landroid/support/v7/media/N;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v7/media/J;->a(Landroid/support/v7/media/N;)V

    .line 2184
    return-void
.end method
