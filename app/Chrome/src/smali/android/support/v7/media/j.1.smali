.class final Landroid/support/v7/media/j;
.super Ljava/lang/Object;
.source "MediaRouter.java"

# interfaces
.implements Landroid/support/v7/media/I;
.implements Landroid/support/v7/media/Z;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Ljava/util/ArrayList;

.field private final c:Ljava/util/ArrayList;

.field private final d:Ljava/util/ArrayList;

.field private final e:Ljava/util/ArrayList;

.field private final f:Landroid/support/v7/media/N;

.field private final g:Landroid/support/v7/media/l;

.field private final h:Landroid/support/v7/media/k;

.field private final i:Landroid/support/v7/media/P;

.field private final j:Z

.field private k:Landroid/support/v7/media/F;

.field private l:Landroid/support/v7/media/MediaRouter$RouteInfo;

.field private m:Landroid/support/v7/media/MediaRouter$RouteInfo;

.field private n:Landroid/support/v7/media/MediaRouteProvider$RouteController;

.field private o:Landroid/support/v7/media/d;


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1525
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1503
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/support/v7/media/j;->b:Ljava/util/ArrayList;

    .line 1505
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/support/v7/media/j;->c:Ljava/util/ArrayList;

    .line 1506
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/support/v7/media/j;->d:Ljava/util/ArrayList;

    .line 1508
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/support/v7/media/j;->e:Ljava/util/ArrayList;

    .line 1510
    new-instance v0, Landroid/support/v7/media/N;

    invoke-direct {v0}, Landroid/support/v7/media/N;-><init>()V

    iput-object v0, p0, Landroid/support/v7/media/j;->f:Landroid/support/v7/media/N;

    .line 1512
    new-instance v0, Landroid/support/v7/media/l;

    invoke-direct {v0, p0, v1}, Landroid/support/v7/media/l;-><init>(Landroid/support/v7/media/j;B)V

    iput-object v0, p0, Landroid/support/v7/media/j;->g:Landroid/support/v7/media/l;

    .line 1513
    new-instance v0, Landroid/support/v7/media/k;

    invoke-direct {v0, p0, v1}, Landroid/support/v7/media/k;-><init>(Landroid/support/v7/media/j;B)V

    iput-object v0, p0, Landroid/support/v7/media/j;->h:Landroid/support/v7/media/k;

    .line 1526
    iput-object p1, p0, Landroid/support/v7/media/j;->a:Landroid/content/Context;

    .line 1527
    invoke-static {p1}, Landroid/support/v4/b/a/a;->a(Landroid/content/Context;)Landroid/support/v4/b/a/a;

    .line 1528
    const-string/jumbo v0, "activity"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    invoke-static {v0}, Landroid/support/v4/app/b;->a(Landroid/app/ActivityManager;)Z

    move-result v0

    iput-boolean v0, p0, Landroid/support/v7/media/j;->j:Z

    .line 1535
    invoke-static {p1, p0}, Landroid/support/v7/media/P;->a(Landroid/content/Context;Landroid/support/v7/media/Z;)Landroid/support/v7/media/P;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v7/media/j;->i:Landroid/support/v7/media/P;

    .line 1536
    iget-object v0, p0, Landroid/support/v7/media/j;->i:Landroid/support/v7/media/P;

    invoke-virtual {p0, v0}, Landroid/support/v7/media/j;->a(Landroid/support/v7/media/MediaRouteProvider;)V

    .line 1537
    return-void
.end method

.method static synthetic a(Landroid/support/v7/media/j;)Landroid/support/v7/media/MediaRouter$RouteInfo;
    .locals 1

    .prologue
    .line 1499
    iget-object v0, p0, Landroid/support/v7/media/j;->m:Landroid/support/v7/media/MediaRouter$RouteInfo;

    return-object v0
.end method

.method static synthetic a(Landroid/support/v7/media/j;Landroid/support/v7/media/MediaRouteProvider;Landroid/support/v7/media/MediaRouteProviderDescriptor;)V
    .locals 2

    .prologue
    .line 1499
    invoke-direct {p0, p1}, Landroid/support/v7/media/j;->c(Landroid/support/v7/media/MediaRouteProvider;)I

    move-result v0

    if-ltz v0, :cond_0

    iget-object v1, p0, Landroid/support/v7/media/j;->d:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/media/m;

    invoke-direct {p0, v0, p2}, Landroid/support/v7/media/j;->a(Landroid/support/v7/media/m;Landroid/support/v7/media/MediaRouteProviderDescriptor;)V

    :cond_0
    return-void
.end method

.method private a(Landroid/support/v7/media/m;Landroid/support/v7/media/MediaRouteProviderDescriptor;)V
    .locals 15

    .prologue
    .line 1802
    invoke-virtual/range {p1 .. p2}, Landroid/support/v7/media/m;->a(Landroid/support/v7/media/MediaRouteProviderDescriptor;)Z

    move-result v1

    if-eqz v1, :cond_11

    .line 1805
    const/4 v6, 0x0

    .line 1806
    const/4 v5, 0x0

    .line 1807
    if-eqz p2, :cond_c

    .line 1808
    invoke-virtual/range {p2 .. p2}, Landroid/support/v7/media/MediaRouteProviderDescriptor;->isValid()Z

    move-result v1

    if-eqz v1, :cond_b

    .line 1809
    invoke-virtual/range {p2 .. p2}, Landroid/support/v7/media/MediaRouteProviderDescriptor;->getRoutes()Ljava/util/List;

    move-result-object v8

    .line 1811
    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v9

    .line 1812
    const/4 v1, 0x0

    move v7, v1

    :goto_0
    if-ge v7, v9, :cond_c

    .line 1813
    invoke-interface {v8, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/support/v7/media/b;

    .line 1814
    invoke-virtual {v1}, Landroid/support/v7/media/b;->a()Ljava/lang/String;

    move-result-object v10

    .line 1815
    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/support/v7/media/m;->a(Ljava/lang/String;)I

    move-result v4

    .line 1816
    if-gez v4, :cond_3

    .line 1818
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {p1 .. p1}, Landroid/support/v7/media/m;->c()Landroid/content/ComponentName;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/ComponentName;->flattenToShortString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ":"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v3}, Landroid/support/v7/media/j;->b(Ljava/lang/String;)I

    move-result v2

    if-gez v2, :cond_1

    move-object v2, v3

    .line 1819
    :goto_1
    new-instance v3, Landroid/support/v7/media/MediaRouter$RouteInfo;

    move-object/from16 v0, p1

    invoke-direct {v3, v0, v10, v2}, Landroid/support/v7/media/MediaRouter$RouteInfo;-><init>(Landroid/support/v7/media/m;Ljava/lang/String;Ljava/lang/String;)V

    .line 1820
    invoke-static/range {p1 .. p1}, Landroid/support/v7/media/m;->b(Landroid/support/v7/media/m;)Ljava/util/ArrayList;

    move-result-object v4

    add-int/lit8 v2, v6, 0x1

    invoke-virtual {v4, v6, v3}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 1821
    iget-object v4, p0, Landroid/support/v7/media/j;->c:Ljava/util/ArrayList;

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1823
    invoke-virtual {v3, v1}, Landroid/support/v7/media/MediaRouter$RouteInfo;->a(Landroid/support/v7/media/b;)I

    .line 1825
    invoke-static {}, Landroid/support/v7/media/MediaRouter;->e()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1826
    const-string/jumbo v1, "MediaRouter"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v6, "Route added: "

    invoke-direct {v4, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1828
    :cond_0
    iget-object v1, p0, Landroid/support/v7/media/j;->h:Landroid/support/v7/media/k;

    const/16 v4, 0x101

    invoke-virtual {v1, v4, v3}, Landroid/support/v7/media/k;->a(ILjava/lang/Object;)V

    move v1, v5

    .line 1812
    :goto_2
    add-int/lit8 v3, v7, 0x1

    move v7, v3

    move v5, v1

    move v6, v2

    goto :goto_0

    .line 1818
    :cond_1
    const/4 v2, 0x2

    :goto_3
    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string/jumbo v11, "%s_%d"

    const/4 v12, 0x2

    new-array v12, v12, [Ljava/lang/Object;

    const/4 v13, 0x0

    aput-object v3, v12, v13

    const/4 v13, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    aput-object v14, v12, v13

    invoke-static {v4, v11, v12}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v4}, Landroid/support/v7/media/j;->b(Ljava/lang/String;)I

    move-result v11

    if-gez v11, :cond_2

    move-object v2, v4

    goto :goto_1

    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 1829
    :cond_3
    if-ge v4, v6, :cond_4

    .line 1830
    const-string/jumbo v2, "MediaRouter"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "Ignoring route descriptor with duplicate id: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move v1, v5

    move v2, v6

    goto :goto_2

    .line 1834
    :cond_4
    invoke-static/range {p1 .. p1}, Landroid/support/v7/media/m;->b(Landroid/support/v7/media/m;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/support/v7/media/MediaRouter$RouteInfo;

    .line 1835
    invoke-static/range {p1 .. p1}, Landroid/support/v7/media/m;->b(Landroid/support/v7/media/m;)Ljava/util/ArrayList;

    move-result-object v10

    add-int/lit8 v3, v6, 0x1

    invoke-static {v10, v4, v6}, Ljava/util/Collections;->swap(Ljava/util/List;II)V

    .line 1838
    invoke-virtual {v2, v1}, Landroid/support/v7/media/MediaRouter$RouteInfo;->a(Landroid/support/v7/media/b;)I

    move-result v1

    .line 1840
    if-eqz v1, :cond_12

    .line 1841
    and-int/lit8 v4, v1, 0x1

    if-eqz v4, :cond_6

    .line 1842
    invoke-static {}, Landroid/support/v7/media/MediaRouter;->e()Z

    move-result v4

    if-eqz v4, :cond_5

    .line 1843
    const-string/jumbo v4, "MediaRouter"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string/jumbo v10, "Route changed: "

    invoke-direct {v6, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1845
    :cond_5
    iget-object v4, p0, Landroid/support/v7/media/j;->h:Landroid/support/v7/media/k;

    const/16 v6, 0x103

    invoke-virtual {v4, v6, v2}, Landroid/support/v7/media/k;->a(ILjava/lang/Object;)V

    .line 1848
    :cond_6
    and-int/lit8 v4, v1, 0x2

    if-eqz v4, :cond_8

    .line 1849
    invoke-static {}, Landroid/support/v7/media/MediaRouter;->e()Z

    move-result v4

    if-eqz v4, :cond_7

    .line 1850
    const-string/jumbo v4, "MediaRouter"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string/jumbo v10, "Route volume changed: "

    invoke-direct {v6, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1852
    :cond_7
    iget-object v4, p0, Landroid/support/v7/media/j;->h:Landroid/support/v7/media/k;

    const/16 v6, 0x104

    invoke-virtual {v4, v6, v2}, Landroid/support/v7/media/k;->a(ILjava/lang/Object;)V

    .line 1855
    :cond_8
    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_a

    .line 1856
    invoke-static {}, Landroid/support/v7/media/MediaRouter;->e()Z

    move-result v1

    if-eqz v1, :cond_9

    .line 1857
    const-string/jumbo v1, "MediaRouter"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v6, "Route presentation display changed: "

    invoke-direct {v4, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1860
    :cond_9
    iget-object v1, p0, Landroid/support/v7/media/j;->h:Landroid/support/v7/media/k;

    const/16 v4, 0x105

    invoke-virtual {v1, v4, v2}, Landroid/support/v7/media/k;->a(ILjava/lang/Object;)V

    .line 1863
    :cond_a
    iget-object v1, p0, Landroid/support/v7/media/j;->m:Landroid/support/v7/media/MediaRouter$RouteInfo;

    if-ne v2, v1, :cond_12

    .line 1864
    const/4 v1, 0x1

    move v2, v3

    goto/16 :goto_2

    .line 1870
    :cond_b
    const-string/jumbo v1, "MediaRouter"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "Ignoring invalid provider descriptor: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1875
    :cond_c
    invoke-static/range {p1 .. p1}, Landroid/support/v7/media/m;->b(Landroid/support/v7/media/m;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    move v2, v1

    :goto_4
    if-lt v2, v6, :cond_d

    .line 1877
    invoke-static/range {p1 .. p1}, Landroid/support/v7/media/m;->b(Landroid/support/v7/media/m;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/support/v7/media/MediaRouter$RouteInfo;

    .line 1878
    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Landroid/support/v7/media/MediaRouter$RouteInfo;->a(Landroid/support/v7/media/b;)I

    .line 1880
    iget-object v3, p0, Landroid/support/v7/media/j;->c:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 1875
    add-int/lit8 v1, v2, -0x1

    move v2, v1

    goto :goto_4

    .line 1884
    :cond_d
    invoke-direct {p0, v5}, Landroid/support/v7/media/j;->a(Z)V

    .line 1891
    invoke-static/range {p1 .. p1}, Landroid/support/v7/media/m;->b(Landroid/support/v7/media/m;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    move v2, v1

    :goto_5
    if-lt v2, v6, :cond_f

    .line 1892
    invoke-static/range {p1 .. p1}, Landroid/support/v7/media/m;->b(Landroid/support/v7/media/m;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/support/v7/media/MediaRouter$RouteInfo;

    .line 1893
    invoke-static {}, Landroid/support/v7/media/MediaRouter;->e()Z

    move-result v3

    if-eqz v3, :cond_e

    .line 1894
    const-string/jumbo v3, "MediaRouter"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "Route removed: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1896
    :cond_e
    iget-object v3, p0, Landroid/support/v7/media/j;->h:Landroid/support/v7/media/k;

    const/16 v4, 0x102

    invoke-virtual {v3, v4, v1}, Landroid/support/v7/media/k;->a(ILjava/lang/Object;)V

    .line 1891
    add-int/lit8 v1, v2, -0x1

    move v2, v1

    goto :goto_5

    .line 1900
    :cond_f
    invoke-static {}, Landroid/support/v7/media/MediaRouter;->e()Z

    move-result v1

    if-eqz v1, :cond_10

    .line 1901
    const-string/jumbo v1, "MediaRouter"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "Provider changed: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1903
    :cond_10
    iget-object v1, p0, Landroid/support/v7/media/j;->h:Landroid/support/v7/media/k;

    const/16 v2, 0x203

    move-object/from16 v0, p1

    invoke-virtual {v1, v2, v0}, Landroid/support/v7/media/k;->a(ILjava/lang/Object;)V

    .line 1905
    :cond_11
    return-void

    :cond_12
    move v1, v5

    move v2, v3

    goto/16 :goto_2
.end method

.method private a(Z)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1936
    iget-object v0, p0, Landroid/support/v7/media/j;->l:Landroid/support/v7/media/MediaRouter$RouteInfo;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/media/j;->l:Landroid/support/v7/media/MediaRouter$RouteInfo;

    invoke-static {v0}, Landroid/support/v7/media/j;->b(Landroid/support/v7/media/MediaRouter$RouteInfo;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1937
    const-string/jumbo v0, "MediaRouter"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "Clearing the default route because it is no longer selectable: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Landroid/support/v7/media/j;->l:Landroid/support/v7/media/MediaRouter$RouteInfo;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1939
    iput-object v4, p0, Landroid/support/v7/media/j;->l:Landroid/support/v7/media/MediaRouter$RouteInfo;

    .line 1941
    :cond_0
    iget-object v0, p0, Landroid/support/v7/media/j;->l:Landroid/support/v7/media/MediaRouter$RouteInfo;

    if-nez v0, :cond_2

    iget-object v0, p0, Landroid/support/v7/media/j;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 1942
    iget-object v0, p0, Landroid/support/v7/media/j;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/media/MediaRouter$RouteInfo;

    .line 1943
    invoke-virtual {v0}, Landroid/support/v7/media/MediaRouter$RouteInfo;->p()Landroid/support/v7/media/MediaRouteProvider;

    move-result-object v1

    iget-object v3, p0, Landroid/support/v7/media/j;->i:Landroid/support/v7/media/P;

    if-ne v1, v3, :cond_5

    invoke-static {v0}, Landroid/support/v7/media/MediaRouter$RouteInfo;->a(Landroid/support/v7/media/MediaRouter$RouteInfo;)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v3, "DEFAULT_ROUTE"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    const/4 v1, 0x1

    :goto_0
    if-eqz v1, :cond_1

    invoke-static {v0}, Landroid/support/v7/media/j;->b(Landroid/support/v7/media/MediaRouter$RouteInfo;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1944
    iput-object v0, p0, Landroid/support/v7/media/j;->l:Landroid/support/v7/media/MediaRouter$RouteInfo;

    .line 1945
    const-string/jumbo v0, "MediaRouter"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "Found default route: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Landroid/support/v7/media/j;->l:Landroid/support/v7/media/MediaRouter$RouteInfo;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1952
    :cond_2
    iget-object v0, p0, Landroid/support/v7/media/j;->m:Landroid/support/v7/media/MediaRouter$RouteInfo;

    if-eqz v0, :cond_3

    iget-object v0, p0, Landroid/support/v7/media/j;->m:Landroid/support/v7/media/MediaRouter$RouteInfo;

    invoke-static {v0}, Landroid/support/v7/media/j;->b(Landroid/support/v7/media/MediaRouter$RouteInfo;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 1953
    const-string/jumbo v0, "MediaRouter"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "Unselecting the current route because it is no longer selectable: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Landroid/support/v7/media/j;->m:Landroid/support/v7/media/MediaRouter$RouteInfo;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1955
    invoke-direct {p0, v4}, Landroid/support/v7/media/j;->c(Landroid/support/v7/media/MediaRouter$RouteInfo;)V

    .line 1957
    :cond_3
    iget-object v0, p0, Landroid/support/v7/media/j;->m:Landroid/support/v7/media/MediaRouter$RouteInfo;

    if-nez v0, :cond_6

    .line 1961
    invoke-direct {p0}, Landroid/support/v7/media/j;->g()Landroid/support/v7/media/MediaRouter$RouteInfo;

    move-result-object v0

    invoke-direct {p0, v0}, Landroid/support/v7/media/j;->c(Landroid/support/v7/media/MediaRouter$RouteInfo;)V

    .line 1966
    :cond_4
    :goto_1
    return-void

    .line 1943
    :cond_5
    const/4 v1, 0x0

    goto :goto_0

    .line 1962
    :cond_6
    if-eqz p1, :cond_4

    .line 1964
    invoke-direct {p0}, Landroid/support/v7/media/j;->h()V

    goto :goto_1
.end method

.method private b(Ljava/lang/String;)I
    .locals 3

    .prologue
    .line 1925
    iget-object v0, p0, Landroid/support/v7/media/j;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 1926
    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_1

    .line 1927
    iget-object v0, p0, Landroid/support/v7/media/j;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/media/MediaRouter$RouteInfo;

    invoke-static {v0}, Landroid/support/v7/media/MediaRouter$RouteInfo;->c(Landroid/support/v7/media/MediaRouter$RouteInfo;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    .line 1931
    :goto_1
    return v0

    .line 1926
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1931
    :cond_1
    const/4 v0, -0x1

    goto :goto_1
.end method

.method static synthetic b(Landroid/support/v7/media/j;)Landroid/support/v7/media/N;
    .locals 1

    .prologue
    .line 1499
    iget-object v0, p0, Landroid/support/v7/media/j;->f:Landroid/support/v7/media/N;

    return-object v0
.end method

.method private static b(Landroid/support/v7/media/MediaRouter$RouteInfo;)Z
    .locals 1

    .prologue
    .line 1992
    invoke-static {p0}, Landroid/support/v7/media/MediaRouter$RouteInfo;->d(Landroid/support/v7/media/MediaRouter$RouteInfo;)Landroid/support/v7/media/b;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {p0}, Landroid/support/v7/media/MediaRouter$RouteInfo;->b(Landroid/support/v7/media/MediaRouter$RouteInfo;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private c(Landroid/support/v7/media/MediaRouteProvider;)I
    .locals 3

    .prologue
    .line 1791
    iget-object v0, p0, Landroid/support/v7/media/j;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 1792
    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_1

    .line 1793
    iget-object v0, p0, Landroid/support/v7/media/j;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/media/m;

    invoke-static {v0}, Landroid/support/v7/media/m;->a(Landroid/support/v7/media/m;)Landroid/support/v7/media/MediaRouteProvider;

    move-result-object v0

    if-ne v0, p1, :cond_0

    move v0, v1

    .line 1797
    :goto_1
    return v0

    .line 1792
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1797
    :cond_1
    const/4 v0, -0x1

    goto :goto_1
.end method

.method private c(Ljava/lang/Object;)I
    .locals 3

    .prologue
    .line 2075
    iget-object v0, p0, Landroid/support/v7/media/j;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 2076
    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_1

    .line 2077
    iget-object v0, p0, Landroid/support/v7/media/j;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/media/O;

    .line 2078
    invoke-virtual {v0}, Landroid/support/v7/media/O;->a()Ljava/lang/Object;

    move-result-object v0

    if-ne v0, p1, :cond_0

    move v0, v1

    .line 2082
    :goto_1
    return v0

    .line 2076
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2082
    :cond_1
    const/4 v0, -0x1

    goto :goto_1
.end method

.method static synthetic c(Landroid/support/v7/media/j;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 1499
    iget-object v0, p0, Landroid/support/v7/media/j;->a:Landroid/content/Context;

    return-object v0
.end method

.method private c(Landroid/support/v7/media/MediaRouter$RouteInfo;)V
    .locals 3

    .prologue
    .line 2002
    iget-object v0, p0, Landroid/support/v7/media/j;->m:Landroid/support/v7/media/MediaRouter$RouteInfo;

    if-eq v0, p1, :cond_5

    .line 2003
    iget-object v0, p0, Landroid/support/v7/media/j;->m:Landroid/support/v7/media/MediaRouter$RouteInfo;

    if-eqz v0, :cond_1

    .line 2004
    invoke-static {}, Landroid/support/v7/media/MediaRouter;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2005
    const-string/jumbo v0, "MediaRouter"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "Route unselected: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Landroid/support/v7/media/j;->m:Landroid/support/v7/media/MediaRouter$RouteInfo;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2007
    :cond_0
    iget-object v0, p0, Landroid/support/v7/media/j;->h:Landroid/support/v7/media/k;

    const/16 v1, 0x107

    iget-object v2, p0, Landroid/support/v7/media/j;->m:Landroid/support/v7/media/MediaRouter$RouteInfo;

    invoke-virtual {v0, v1, v2}, Landroid/support/v7/media/k;->a(ILjava/lang/Object;)V

    .line 2008
    iget-object v0, p0, Landroid/support/v7/media/j;->n:Landroid/support/v7/media/MediaRouteProvider$RouteController;

    if-eqz v0, :cond_1

    .line 2009
    iget-object v0, p0, Landroid/support/v7/media/j;->n:Landroid/support/v7/media/MediaRouteProvider$RouteController;

    invoke-virtual {v0}, Landroid/support/v7/media/MediaRouteProvider$RouteController;->onUnselect()V

    .line 2010
    iget-object v0, p0, Landroid/support/v7/media/j;->n:Landroid/support/v7/media/MediaRouteProvider$RouteController;

    invoke-virtual {v0}, Landroid/support/v7/media/MediaRouteProvider$RouteController;->onRelease()V

    .line 2011
    const/4 v0, 0x0

    iput-object v0, p0, Landroid/support/v7/media/j;->n:Landroid/support/v7/media/MediaRouteProvider$RouteController;

    .line 2015
    :cond_1
    iput-object p1, p0, Landroid/support/v7/media/j;->m:Landroid/support/v7/media/MediaRouter$RouteInfo;

    .line 2017
    iget-object v0, p0, Landroid/support/v7/media/j;->m:Landroid/support/v7/media/MediaRouter$RouteInfo;

    if-eqz v0, :cond_4

    .line 2018
    invoke-virtual {p1}, Landroid/support/v7/media/MediaRouter$RouteInfo;->p()Landroid/support/v7/media/MediaRouteProvider;

    move-result-object v0

    invoke-static {p1}, Landroid/support/v7/media/MediaRouter$RouteInfo;->a(Landroid/support/v7/media/MediaRouter$RouteInfo;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v7/media/MediaRouteProvider;->onCreateRouteController(Ljava/lang/String;)Landroid/support/v7/media/MediaRouteProvider$RouteController;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v7/media/j;->n:Landroid/support/v7/media/MediaRouteProvider$RouteController;

    .line 2020
    iget-object v0, p0, Landroid/support/v7/media/j;->n:Landroid/support/v7/media/MediaRouteProvider$RouteController;

    if-eqz v0, :cond_2

    .line 2021
    iget-object v0, p0, Landroid/support/v7/media/j;->n:Landroid/support/v7/media/MediaRouteProvider$RouteController;

    invoke-virtual {v0}, Landroid/support/v7/media/MediaRouteProvider$RouteController;->onSelect()V

    .line 2023
    :cond_2
    invoke-static {}, Landroid/support/v7/media/MediaRouter;->e()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2024
    const-string/jumbo v0, "MediaRouter"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "Route selected: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Landroid/support/v7/media/j;->m:Landroid/support/v7/media/MediaRouter$RouteInfo;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2026
    :cond_3
    iget-object v0, p0, Landroid/support/v7/media/j;->h:Landroid/support/v7/media/k;

    const/16 v1, 0x106

    iget-object v2, p0, Landroid/support/v7/media/j;->m:Landroid/support/v7/media/MediaRouter$RouteInfo;

    invoke-virtual {v0, v1, v2}, Landroid/support/v7/media/k;->a(ILjava/lang/Object;)V

    .line 2029
    :cond_4
    invoke-direct {p0}, Landroid/support/v7/media/j;->h()V

    .line 2031
    :cond_5
    return-void
.end method

.method static synthetic d(Landroid/support/v7/media/j;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 1499
    iget-object v0, p0, Landroid/support/v7/media/j;->b:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic e(Landroid/support/v7/media/j;)Landroid/support/v7/media/P;
    .locals 1

    .prologue
    .line 1499
    iget-object v0, p0, Landroid/support/v7/media/j;->i:Landroid/support/v7/media/P;

    return-object v0
.end method

.method private g()Landroid/support/v7/media/MediaRouter$RouteInfo;
    .locals 4

    .prologue
    .line 1973
    iget-object v0, p0, Landroid/support/v7/media/j;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/media/MediaRouter$RouteInfo;

    .line 1974
    iget-object v1, p0, Landroid/support/v7/media/j;->l:Landroid/support/v7/media/MediaRouter$RouteInfo;

    if-eq v0, v1, :cond_0

    invoke-virtual {v0}, Landroid/support/v7/media/MediaRouter$RouteInfo;->p()Landroid/support/v7/media/MediaRouteProvider;

    move-result-object v1

    iget-object v3, p0, Landroid/support/v7/media/j;->i:Landroid/support/v7/media/P;

    if-ne v1, v3, :cond_1

    const-string/jumbo v1, "android.media.intent.category.LIVE_AUDIO"

    invoke-virtual {v0, v1}, Landroid/support/v7/media/MediaRouter$RouteInfo;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    const-string/jumbo v1, "android.media.intent.category.LIVE_VIDEO"

    invoke-virtual {v0, v1}, Landroid/support/v7/media/MediaRouter$RouteInfo;->a(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    const/4 v1, 0x1

    :goto_0
    if-eqz v1, :cond_0

    invoke-static {v0}, Landroid/support/v7/media/j;->b(Landroid/support/v7/media/MediaRouter$RouteInfo;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1980
    :goto_1
    return-object v0

    .line 1974
    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    .line 1980
    :cond_2
    iget-object v0, p0, Landroid/support/v7/media/j;->l:Landroid/support/v7/media/MediaRouter$RouteInfo;

    goto :goto_1
.end method

.method private h()V
    .locals 3

    .prologue
    .line 2086
    iget-object v0, p0, Landroid/support/v7/media/j;->m:Landroid/support/v7/media/MediaRouter$RouteInfo;

    if-eqz v0, :cond_0

    .line 2087
    iget-object v0, p0, Landroid/support/v7/media/j;->f:Landroid/support/v7/media/N;

    iget-object v1, p0, Landroid/support/v7/media/j;->m:Landroid/support/v7/media/MediaRouter$RouteInfo;

    invoke-virtual {v1}, Landroid/support/v7/media/MediaRouter$RouteInfo;->k()I

    move-result v1

    iput v1, v0, Landroid/support/v7/media/N;->a:I

    .line 2088
    iget-object v0, p0, Landroid/support/v7/media/j;->f:Landroid/support/v7/media/N;

    iget-object v1, p0, Landroid/support/v7/media/j;->m:Landroid/support/v7/media/MediaRouter$RouteInfo;

    invoke-virtual {v1}, Landroid/support/v7/media/MediaRouter$RouteInfo;->l()I

    move-result v1

    iput v1, v0, Landroid/support/v7/media/N;->b:I

    .line 2089
    iget-object v0, p0, Landroid/support/v7/media/j;->f:Landroid/support/v7/media/N;

    iget-object v1, p0, Landroid/support/v7/media/j;->m:Landroid/support/v7/media/MediaRouter$RouteInfo;

    invoke-virtual {v1}, Landroid/support/v7/media/MediaRouter$RouteInfo;->j()I

    move-result v1

    iput v1, v0, Landroid/support/v7/media/N;->c:I

    .line 2090
    iget-object v0, p0, Landroid/support/v7/media/j;->f:Landroid/support/v7/media/N;

    iget-object v1, p0, Landroid/support/v7/media/j;->m:Landroid/support/v7/media/MediaRouter$RouteInfo;

    invoke-virtual {v1}, Landroid/support/v7/media/MediaRouter$RouteInfo;->i()I

    move-result v1

    iput v1, v0, Landroid/support/v7/media/N;->d:I

    .line 2091
    iget-object v0, p0, Landroid/support/v7/media/j;->f:Landroid/support/v7/media/N;

    iget-object v1, p0, Landroid/support/v7/media/j;->m:Landroid/support/v7/media/MediaRouter$RouteInfo;

    invoke-virtual {v1}, Landroid/support/v7/media/MediaRouter$RouteInfo;->h()I

    move-result v1

    iput v1, v0, Landroid/support/v7/media/N;->e:I

    .line 2093
    iget-object v0, p0, Landroid/support/v7/media/j;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 2094
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    .line 2095
    iget-object v0, p0, Landroid/support/v7/media/j;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/media/O;

    .line 2096
    invoke-virtual {v0}, Landroid/support/v7/media/O;->c()V

    .line 2094
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2098
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Landroid/support/v7/media/MediaRouter$RouteInfo;
    .locals 2

    .prologue
    .line 2035
    iget-object v0, p0, Landroid/support/v7/media/j;->i:Landroid/support/v7/media/P;

    invoke-direct {p0, v0}, Landroid/support/v7/media/j;->c(Landroid/support/v7/media/MediaRouteProvider;)I

    move-result v0

    .line 2036
    if-ltz v0, :cond_0

    .line 2037
    iget-object v1, p0, Landroid/support/v7/media/j;->d:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/media/m;

    .line 2038
    invoke-virtual {v0, p1}, Landroid/support/v7/media/m;->a(Ljava/lang/String;)I

    move-result v1

    .line 2039
    if-ltz v1, :cond_0

    .line 2040
    invoke-static {v0}, Landroid/support/v7/media/m;->b(Landroid/support/v7/media/m;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/media/MediaRouter$RouteInfo;

    .line 2043
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Landroid/content/Context;)Landroid/support/v7/media/MediaRouter;
    .locals 3

    .prologue
    .line 1549
    iget-object v0, p0, Landroid/support/v7/media/j;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    :goto_0
    add-int/lit8 v1, v0, -0x1

    if-ltz v1, :cond_1

    .line 1550
    iget-object v0, p0, Landroid/support/v7/media/j;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/media/MediaRouter;

    .line 1551
    if-nez v0, :cond_0

    .line 1552
    iget-object v0, p0, Landroid/support/v7/media/j;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move v0, v1

    goto :goto_0

    .line 1553
    :cond_0
    iget-object v2, v0, Landroid/support/v7/media/MediaRouter;->b:Landroid/content/Context;

    if-ne v2, p1, :cond_2

    .line 1559
    :goto_1
    return-object v0

    .line 1557
    :cond_1
    new-instance v0, Landroid/support/v7/media/MediaRouter;

    invoke-direct {v0, p1}, Landroid/support/v7/media/MediaRouter;-><init>(Landroid/content/Context;)V

    .line 1558
    iget-object v1, p0, Landroid/support/v7/media/j;->b:Ljava/util/ArrayList;

    new-instance v2, Ljava/lang/ref/WeakReference;

    invoke-direct {v2, v0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public final a()V
    .locals 2

    .prologue
    .line 1542
    new-instance v0, Landroid/support/v7/media/F;

    iget-object v1, p0, Landroid/support/v7/media/j;->a:Landroid/content/Context;

    invoke-direct {v0, v1, p0}, Landroid/support/v7/media/F;-><init>(Landroid/content/Context;Landroid/support/v7/media/I;)V

    iput-object v0, p0, Landroid/support/v7/media/j;->k:Landroid/support/v7/media/F;

    .line 1544
    iget-object v0, p0, Landroid/support/v7/media/j;->k:Landroid/support/v7/media/F;

    invoke-virtual {v0}, Landroid/support/v7/media/F;->a()V

    .line 1545
    return-void
.end method

.method public final a(Landroid/support/v7/media/MediaRouteProvider;)V
    .locals 4

    .prologue
    .line 1742
    invoke-direct {p0, p1}, Landroid/support/v7/media/j;->c(Landroid/support/v7/media/MediaRouteProvider;)I

    move-result v0

    .line 1743
    if-gez v0, :cond_1

    .line 1745
    new-instance v0, Landroid/support/v7/media/m;

    invoke-direct {v0, p1}, Landroid/support/v7/media/m;-><init>(Landroid/support/v7/media/MediaRouteProvider;)V

    .line 1746
    iget-object v1, p0, Landroid/support/v7/media/j;->d:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1747
    invoke-static {}, Landroid/support/v7/media/MediaRouter;->e()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1748
    const-string/jumbo v1, "MediaRouter"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "Provider added: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1750
    :cond_0
    iget-object v1, p0, Landroid/support/v7/media/j;->h:Landroid/support/v7/media/k;

    const/16 v2, 0x201

    invoke-virtual {v1, v2, v0}, Landroid/support/v7/media/k;->a(ILjava/lang/Object;)V

    .line 1752
    invoke-virtual {p1}, Landroid/support/v7/media/MediaRouteProvider;->getDescriptor()Landroid/support/v7/media/MediaRouteProviderDescriptor;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Landroid/support/v7/media/j;->a(Landroid/support/v7/media/m;Landroid/support/v7/media/MediaRouteProviderDescriptor;)V

    .line 1754
    iget-object v0, p0, Landroid/support/v7/media/j;->g:Landroid/support/v7/media/l;

    invoke-virtual {p1, v0}, Landroid/support/v7/media/MediaRouteProvider;->setCallback(Landroid/support/v7/media/MediaRouteProvider$Callback;)V

    .line 1756
    iget-object v0, p0, Landroid/support/v7/media/j;->o:Landroid/support/v7/media/d;

    invoke-virtual {p1, v0}, Landroid/support/v7/media/MediaRouteProvider;->setDiscoveryRequest(Landroid/support/v7/media/d;)V

    .line 1758
    :cond_1
    return-void
.end method

.method public final a(Landroid/support/v7/media/MediaRouter$RouteInfo;)V
    .locals 3

    .prologue
    .line 1637
    iget-object v0, p0, Landroid/support/v7/media/j;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1638
    const-string/jumbo v0, "MediaRouter"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "Ignoring attempt to select removed route: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1647
    :goto_0
    return-void

    .line 1641
    :cond_0
    invoke-static {p1}, Landroid/support/v7/media/MediaRouter$RouteInfo;->b(Landroid/support/v7/media/MediaRouter$RouteInfo;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1642
    const-string/jumbo v0, "MediaRouter"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "Ignoring attempt to select disabled route: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1646
    :cond_1
    invoke-direct {p0, p1}, Landroid/support/v7/media/j;->c(Landroid/support/v7/media/MediaRouter$RouteInfo;)V

    goto :goto_0
.end method

.method public final a(Landroid/support/v7/media/MediaRouter$RouteInfo;I)V
    .locals 1

    .prologue
    .line 1595
    iget-object v0, p0, Landroid/support/v7/media/j;->m:Landroid/support/v7/media/MediaRouter$RouteInfo;

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/media/j;->n:Landroid/support/v7/media/MediaRouteProvider$RouteController;

    if-eqz v0, :cond_0

    .line 1596
    iget-object v0, p0, Landroid/support/v7/media/j;->n:Landroid/support/v7/media/MediaRouteProvider$RouteController;

    invoke-virtual {v0, p2}, Landroid/support/v7/media/MediaRouteProvider$RouteController;->onSetVolume(I)V

    .line 1598
    :cond_0
    return-void
.end method

.method public final a(Landroid/support/v7/media/MediaRouter$RouteInfo;Landroid/content/Intent;Landroid/support/v7/media/i;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1584
    iget-object v0, p0, Landroid/support/v7/media/j;->m:Landroid/support/v7/media/MediaRouter$RouteInfo;

    if-ne p1, v0, :cond_1

    iget-object v0, p0, Landroid/support/v7/media/j;->n:Landroid/support/v7/media/MediaRouteProvider$RouteController;

    if-eqz v0, :cond_1

    .line 1585
    iget-object v0, p0, Landroid/support/v7/media/j;->n:Landroid/support/v7/media/MediaRouteProvider$RouteController;

    invoke-virtual {v0, p2, p3}, Landroid/support/v7/media/MediaRouteProvider$RouteController;->onControlRequest(Landroid/content/Intent;Landroid/support/v7/media/i;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1592
    :cond_0
    :goto_0
    return-void

    .line 1589
    :cond_1
    if-eqz p3, :cond_0

    .line 1590
    invoke-virtual {p3, v1, v1}, Landroid/support/v7/media/i;->onError(Ljava/lang/String;Landroid/os/Bundle;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 2047
    invoke-direct {p0, p1}, Landroid/support/v7/media/j;->c(Ljava/lang/Object;)I

    move-result v0

    .line 2048
    if-gez v0, :cond_0

    .line 2049
    new-instance v0, Landroid/support/v7/media/O;

    invoke-direct {v0, p0, p1}, Landroid/support/v7/media/O;-><init>(Landroid/support/v7/media/j;Ljava/lang/Object;)V

    .line 2050
    iget-object v1, p0, Landroid/support/v7/media/j;->e:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2052
    :cond_0
    return-void
.end method

.method public final a(Landroid/support/v7/media/e;I)Z
    .locals 6

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1650
    invoke-virtual {p1}, Landroid/support/v7/media/e;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    .line 1673
    :goto_0
    return v0

    .line 1655
    :cond_0
    and-int/lit8 v0, p2, 0x2

    if-nez v0, :cond_1

    iget-boolean v0, p0, Landroid/support/v7/media/j;->j:Z

    if-eqz v0, :cond_1

    move v0, v2

    .line 1656
    goto :goto_0

    .line 1660
    :cond_1
    iget-object v0, p0, Landroid/support/v7/media/j;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v4

    move v3, v1

    .line 1661
    :goto_1
    if-ge v3, v4, :cond_4

    .line 1662
    iget-object v0, p0, Landroid/support/v7/media/j;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/media/MediaRouter$RouteInfo;

    .line 1663
    and-int/lit8 v5, p2, 0x1

    if-eqz v5, :cond_2

    invoke-virtual {v0}, Landroid/support/v7/media/MediaRouter$RouteInfo;->g()Z

    move-result v5

    if-nez v5, :cond_3

    .line 1665
    :cond_2
    invoke-virtual {v0, p1}, Landroid/support/v7/media/MediaRouter$RouteInfo;->a(Landroid/support/v7/media/e;)Z

    move-result v0

    if-eqz v0, :cond_3

    move v0, v2

    .line 1668
    goto :goto_0

    .line 1661
    :cond_3
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1

    :cond_4
    move v0, v1

    .line 1673
    goto :goto_0
.end method

.method public final b()Landroid/content/ContentResolver;
    .locals 1

    .prologue
    .line 1563
    iget-object v0, p0, Landroid/support/v7/media/j;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    return-object v0
.end method

.method public final b(Landroid/support/v7/media/MediaRouteProvider;)V
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 1762
    invoke-direct {p0, p1}, Landroid/support/v7/media/j;->c(Landroid/support/v7/media/MediaRouteProvider;)I

    move-result v1

    .line 1763
    if-ltz v1, :cond_1

    .line 1765
    invoke-virtual {p1, v2}, Landroid/support/v7/media/MediaRouteProvider;->setCallback(Landroid/support/v7/media/MediaRouteProvider$Callback;)V

    .line 1767
    invoke-virtual {p1, v2}, Landroid/support/v7/media/MediaRouteProvider;->setDiscoveryRequest(Landroid/support/v7/media/d;)V

    .line 1769
    iget-object v0, p0, Landroid/support/v7/media/j;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/media/m;

    .line 1770
    invoke-direct {p0, v0, v2}, Landroid/support/v7/media/j;->a(Landroid/support/v7/media/m;Landroid/support/v7/media/MediaRouteProviderDescriptor;)V

    .line 1772
    invoke-static {}, Landroid/support/v7/media/MediaRouter;->e()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1773
    const-string/jumbo v2, "MediaRouter"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "Provider removed: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1775
    :cond_0
    iget-object v2, p0, Landroid/support/v7/media/j;->h:Landroid/support/v7/media/k;

    const/16 v3, 0x202

    invoke-virtual {v2, v3, v0}, Landroid/support/v7/media/k;->a(ILjava/lang/Object;)V

    .line 1776
    iget-object v0, p0, Landroid/support/v7/media/j;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 1778
    :cond_1
    return-void
.end method

.method public final b(Landroid/support/v7/media/MediaRouter$RouteInfo;I)V
    .locals 1

    .prologue
    .line 1601
    iget-object v0, p0, Landroid/support/v7/media/j;->m:Landroid/support/v7/media/MediaRouter$RouteInfo;

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/media/j;->n:Landroid/support/v7/media/MediaRouteProvider$RouteController;

    if-eqz v0, :cond_0

    .line 1602
    iget-object v0, p0, Landroid/support/v7/media/j;->n:Landroid/support/v7/media/MediaRouteProvider$RouteController;

    invoke-virtual {v0, p2}, Landroid/support/v7/media/MediaRouteProvider$RouteController;->onUpdateVolume(I)V

    .line 1604
    :cond_0
    return-void
.end method

.method public final b(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 2055
    invoke-direct {p0, p1}, Landroid/support/v7/media/j;->c(Ljava/lang/Object;)I

    move-result v0

    .line 2056
    if-ltz v0, :cond_0

    .line 2057
    iget-object v1, p0, Landroid/support/v7/media/j;->e:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/media/O;

    .line 2058
    invoke-virtual {v0}, Landroid/support/v7/media/O;->b()V

    .line 2060
    :cond_0
    return-void
.end method

.method public final c()Ljava/util/List;
    .locals 1

    .prologue
    .line 1607
    iget-object v0, p0, Landroid/support/v7/media/j;->c:Ljava/util/ArrayList;

    return-object v0
.end method

.method public final d()Landroid/support/v7/media/MediaRouter$RouteInfo;
    .locals 2

    .prologue
    .line 1615
    iget-object v0, p0, Landroid/support/v7/media/j;->l:Landroid/support/v7/media/MediaRouter$RouteInfo;

    if-nez v0, :cond_0

    .line 1619
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "There is no default route.  The media router has not yet been fully initialized."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1622
    :cond_0
    iget-object v0, p0, Landroid/support/v7/media/j;->l:Landroid/support/v7/media/MediaRouter$RouteInfo;

    return-object v0
.end method

.method public final e()Landroid/support/v7/media/MediaRouter$RouteInfo;
    .locals 2

    .prologue
    .line 1626
    iget-object v0, p0, Landroid/support/v7/media/j;->m:Landroid/support/v7/media/MediaRouter$RouteInfo;

    if-nez v0, :cond_0

    .line 1630
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "There is no currently selected route.  The media router has not yet been fully initialized."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1633
    :cond_0
    iget-object v0, p0, Landroid/support/v7/media/j;->m:Landroid/support/v7/media/MediaRouter$RouteInfo;

    return-object v0
.end method

.method public final f()V
    .locals 11

    .prologue
    const/4 v3, 0x1

    const/4 v5, 0x0

    .line 1678
    .line 1680
    new-instance v8, Landroid/support/v7/media/f;

    invoke-direct {v8}, Landroid/support/v7/media/f;-><init>()V

    .line 1681
    iget-object v0, p0, Landroid/support/v7/media/j;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    move v2, v5

    move v4, v5

    :goto_0
    add-int/lit8 v7, v0, -0x1

    if-ltz v7, :cond_5

    .line 1682
    iget-object v0, p0, Landroid/support/v7/media/j;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/media/MediaRouter;

    .line 1683
    if-nez v0, :cond_0

    .line 1684
    iget-object v0, p0, Landroid/support/v7/media/j;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move v0, v7

    goto :goto_0

    .line 1686
    :cond_0
    iget-object v1, v0, Landroid/support/v7/media/MediaRouter;->c:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v9

    move v6, v5

    .line 1687
    :goto_1
    if-ge v6, v9, :cond_4

    .line 1688
    iget-object v1, v0, Landroid/support/v7/media/MediaRouter;->c:Ljava/util/ArrayList;

    invoke-virtual {v1, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/support/v7/media/h;

    .line 1689
    iget-object v10, v1, Landroid/support/v7/media/h;->c:Landroid/support/v7/media/e;

    invoke-virtual {v8, v10}, Landroid/support/v7/media/f;->a(Landroid/support/v7/media/e;)Landroid/support/v7/media/f;

    .line 1690
    iget v10, v1, Landroid/support/v7/media/h;->d:I

    and-int/lit8 v10, v10, 0x1

    if-eqz v10, :cond_1

    move v2, v3

    move v4, v3

    .line 1694
    :cond_1
    iget v10, v1, Landroid/support/v7/media/h;->d:I

    and-int/lit8 v10, v10, 0x4

    if-eqz v10, :cond_2

    .line 1695
    iget-boolean v10, p0, Landroid/support/v7/media/j;->j:Z

    if-nez v10, :cond_2

    move v4, v3

    .line 1699
    :cond_2
    iget v1, v1, Landroid/support/v7/media/h;->d:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_3

    move v4, v3

    .line 1687
    :cond_3
    add-int/lit8 v1, v6, 0x1

    move v6, v1

    goto :goto_1

    :cond_4
    move v0, v7

    .line 1704
    goto :goto_0

    .line 1705
    :cond_5
    if-eqz v4, :cond_7

    invoke-virtual {v8}, Landroid/support/v7/media/f;->a()Landroid/support/v7/media/e;

    move-result-object v0

    .line 1708
    :goto_2
    iget-object v1, p0, Landroid/support/v7/media/j;->o:Landroid/support/v7/media/d;

    if-eqz v1, :cond_8

    iget-object v1, p0, Landroid/support/v7/media/j;->o:Landroid/support/v7/media/d;

    invoke-virtual {v1}, Landroid/support/v7/media/d;->a()Landroid/support/v7/media/e;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/support/v7/media/e;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    iget-object v1, p0, Landroid/support/v7/media/j;->o:Landroid/support/v7/media/d;

    invoke-virtual {v1}, Landroid/support/v7/media/d;->b()Z

    move-result v1

    if-ne v1, v2, :cond_8

    .line 1738
    :cond_6
    return-void

    .line 1705
    :cond_7
    sget-object v0, Landroid/support/v7/media/e;->a:Landroid/support/v7/media/e;

    goto :goto_2

    .line 1713
    :cond_8
    invoke-virtual {v0}, Landroid/support/v7/media/e;->b()Z

    move-result v1

    if-eqz v1, :cond_b

    if-nez v2, :cond_b

    .line 1715
    iget-object v0, p0, Landroid/support/v7/media/j;->o:Landroid/support/v7/media/d;

    if-eqz v0, :cond_6

    .line 1718
    const/4 v0, 0x0

    iput-object v0, p0, Landroid/support/v7/media/j;->o:Landroid/support/v7/media/d;

    .line 1723
    :goto_3
    invoke-static {}, Landroid/support/v7/media/MediaRouter;->e()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 1724
    const-string/jumbo v0, "MediaRouter"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "Updated discovery request: "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Landroid/support/v7/media/j;->o:Landroid/support/v7/media/d;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1726
    :cond_9
    if-eqz v4, :cond_a

    if-nez v2, :cond_a

    iget-boolean v0, p0, Landroid/support/v7/media/j;->j:Z

    if-eqz v0, :cond_a

    .line 1727
    const-string/jumbo v0, "MediaRouter"

    const-string/jumbo v1, "Forcing passive route discovery on a low-RAM device, system performance may be affected.  Please consider using CALLBACK_FLAG_REQUEST_DISCOVERY instead of CALLBACK_FLAG_FORCE_DISCOVERY."

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1734
    :cond_a
    iget-object v0, p0, Landroid/support/v7/media/j;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    move v1, v5

    .line 1735
    :goto_4
    if-ge v1, v2, :cond_6

    .line 1736
    iget-object v0, p0, Landroid/support/v7/media/j;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/media/m;

    invoke-static {v0}, Landroid/support/v7/media/m;->a(Landroid/support/v7/media/m;)Landroid/support/v7/media/MediaRouteProvider;

    move-result-object v0

    iget-object v3, p0, Landroid/support/v7/media/j;->o:Landroid/support/v7/media/d;

    invoke-virtual {v0, v3}, Landroid/support/v7/media/MediaRouteProvider;->setDiscoveryRequest(Landroid/support/v7/media/d;)V

    .line 1735
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_4

    .line 1721
    :cond_b
    new-instance v1, Landroid/support/v7/media/d;

    invoke-direct {v1, v0, v2}, Landroid/support/v7/media/d;-><init>(Landroid/support/v7/media/e;Z)V

    iput-object v1, p0, Landroid/support/v7/media/j;->o:Landroid/support/v7/media/d;

    goto :goto_3
.end method
