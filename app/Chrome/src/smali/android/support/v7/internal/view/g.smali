.class public final Landroid/support/v7/internal/view/g;
.super Ljava/lang/Object;
.source "ViewPropertyAnimatorCompatSet.java"


# instance fields
.field private final a:Ljava/util/ArrayList;

.field private b:J

.field private c:Landroid/view/animation/Interpolator;

.field private d:Landroid/support/v4/view/aC;

.field private e:Z

.field private final f:Landroid/support/v4/view/aD;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Landroid/support/v7/internal/view/g;->b:J

    .line 107
    new-instance v0, Landroid/support/v7/internal/view/h;

    invoke-direct {v0, p0}, Landroid/support/v7/internal/view/h;-><init>(Landroid/support/v7/internal/view/g;)V

    iput-object v0, p0, Landroid/support/v7/internal/view/g;->f:Landroid/support/v4/view/aD;

    .line 44
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/support/v7/internal/view/g;->a:Ljava/util/ArrayList;

    .line 45
    return-void
.end method

.method static synthetic a(Landroid/support/v7/internal/view/g;)Landroid/support/v4/view/aC;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Landroid/support/v7/internal/view/g;->d:Landroid/support/v4/view/aC;

    return-object v0
.end method

.method static synthetic b(Landroid/support/v7/internal/view/g;)V
    .locals 1

    .prologue
    .line 33
    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/support/v7/internal/view/g;->e:Z

    return-void
.end method

.method static synthetic c(Landroid/support/v7/internal/view/g;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Landroid/support/v7/internal/view/g;->a:Ljava/util/ArrayList;

    return-object v0
.end method


# virtual methods
.method public final a(J)Landroid/support/v7/internal/view/g;
    .locals 2

    .prologue
    .line 87
    iget-boolean v0, p0, Landroid/support/v7/internal/view/g;->e:Z

    if-nez v0, :cond_0

    .line 88
    const-wide/16 v0, 0xfa

    iput-wide v0, p0, Landroid/support/v7/internal/view/g;->b:J

    .line 90
    :cond_0
    return-object p0
.end method

.method public final a(Landroid/support/v4/view/aC;)Landroid/support/v7/internal/view/g;
    .locals 1

    .prologue
    .line 101
    iget-boolean v0, p0, Landroid/support/v7/internal/view/g;->e:Z

    if-nez v0, :cond_0

    .line 102
    iput-object p1, p0, Landroid/support/v7/internal/view/g;->d:Landroid/support/v4/view/aC;

    .line 104
    :cond_0
    return-object p0
.end method

.method public final a(Landroid/support/v4/view/ar;)Landroid/support/v7/internal/view/g;
    .locals 1

    .prologue
    .line 48
    iget-boolean v0, p0, Landroid/support/v7/internal/view/g;->e:Z

    if-nez v0, :cond_0

    .line 49
    iget-object v0, p0, Landroid/support/v7/internal/view/g;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 51
    :cond_0
    return-object p0
.end method

.method public final a(Landroid/view/animation/Interpolator;)Landroid/support/v7/internal/view/g;
    .locals 1

    .prologue
    .line 94
    iget-boolean v0, p0, Landroid/support/v7/internal/view/g;->e:Z

    if-nez v0, :cond_0

    .line 95
    iput-object p1, p0, Landroid/support/v7/internal/view/g;->c:Landroid/view/animation/Interpolator;

    .line 97
    :cond_0
    return-object p0
.end method

.method public final a()V
    .locals 6

    .prologue
    .line 55
    iget-boolean v0, p0, Landroid/support/v7/internal/view/g;->e:Z

    if-eqz v0, :cond_0

    .line 70
    :goto_0
    return-void

    .line 56
    :cond_0
    iget-object v0, p0, Landroid/support/v7/internal/view/g;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/ar;

    .line 57
    iget-wide v2, p0, Landroid/support/v7/internal/view/g;->b:J

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-ltz v2, :cond_1

    .line 58
    iget-wide v2, p0, Landroid/support/v7/internal/view/g;->b:J

    invoke-virtual {v0, v2, v3}, Landroid/support/v4/view/ar;->a(J)Landroid/support/v4/view/ar;

    .line 60
    :cond_1
    iget-object v2, p0, Landroid/support/v7/internal/view/g;->c:Landroid/view/animation/Interpolator;

    if-eqz v2, :cond_2

    .line 61
    iget-object v2, p0, Landroid/support/v7/internal/view/g;->c:Landroid/view/animation/Interpolator;

    invoke-virtual {v0, v2}, Landroid/support/v4/view/ar;->a(Landroid/view/animation/Interpolator;)Landroid/support/v4/view/ar;

    .line 63
    :cond_2
    iget-object v2, p0, Landroid/support/v7/internal/view/g;->d:Landroid/support/v4/view/aC;

    if-eqz v2, :cond_3

    .line 64
    iget-object v2, p0, Landroid/support/v7/internal/view/g;->f:Landroid/support/v4/view/aD;

    invoke-virtual {v0, v2}, Landroid/support/v4/view/ar;->a(Landroid/support/v4/view/aC;)Landroid/support/v4/view/ar;

    .line 66
    :cond_3
    invoke-virtual {v0}, Landroid/support/v4/view/ar;->b()V

    goto :goto_1

    .line 69
    :cond_4
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v7/internal/view/g;->e:Z

    goto :goto_0
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 77
    iget-boolean v0, p0, Landroid/support/v7/internal/view/g;->e:Z

    if-nez v0, :cond_0

    .line 84
    :goto_0
    return-void

    .line 80
    :cond_0
    iget-object v0, p0, Landroid/support/v7/internal/view/g;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/ar;

    .line 81
    invoke-virtual {v0}, Landroid/support/v4/view/ar;->a()V

    goto :goto_1

    .line 83
    :cond_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/support/v7/internal/view/g;->e:Z

    goto :goto_0
.end method
