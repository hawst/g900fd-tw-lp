.class public final Landroid/support/v7/internal/widget/U;
.super Landroid/widget/EditText;
.source "TintEditText.java"


# static fields
.field private static final a:[I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 31
    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v1, 0x0

    const v2, 0x10100d4

    aput v2, v0, v1

    sput-object v0, Landroid/support/v7/internal/widget/U;->a:[I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 40
    const v0, 0x101006e

    invoke-direct {p0, p1, p2, v0}, Landroid/support/v7/internal/widget/U;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 41
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    .prologue
    const v2, 0x101006e

    const/4 v1, 0x0

    .line 44
    invoke-direct {p0, p1, p2, v2}, Landroid/widget/EditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 46
    sget-object v0, Landroid/support/v7/internal/widget/U;->a:[I

    invoke-static {p1, p2, v0, v2, v1}, Landroid/support/v7/internal/widget/aa;->a(Landroid/content/Context;Landroid/util/AttributeSet;[III)Landroid/support/v7/internal/widget/aa;

    move-result-object v0

    .line 48
    invoke-virtual {v0, v1}, Landroid/support/v7/internal/widget/aa;->a(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {p0, v1}, Landroid/support/v7/internal/widget/U;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 49
    invoke-virtual {v0}, Landroid/support/v7/internal/widget/aa;->b()V

    .line 50
    return-void
.end method
