.class public final Landroid/support/v7/internal/widget/X;
.super Landroid/widget/RadioButton;
.source "TintRadioButton.java"


# static fields
.field private static final a:[I


# instance fields
.field private final b:Landroid/support/v7/internal/widget/V;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 31
    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v1, 0x0

    const v2, 0x1010107

    aput v2, v0, v1

    sput-object v0, Landroid/support/v7/internal/widget/X;->a:[I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 42
    const v0, 0x101007e

    invoke-direct {p0, p1, p2, v0}, Landroid/support/v7/internal/widget/X;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 43
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    .prologue
    const v2, 0x101007e

    const/4 v1, 0x0

    .line 46
    invoke-direct {p0, p1, p2, v2}, Landroid/widget/RadioButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 48
    sget-object v0, Landroid/support/v7/internal/widget/X;->a:[I

    invoke-static {p1, p2, v0, v2, v1}, Landroid/support/v7/internal/widget/aa;->a(Landroid/content/Context;Landroid/util/AttributeSet;[III)Landroid/support/v7/internal/widget/aa;

    move-result-object v0

    .line 50
    invoke-virtual {v0, v1}, Landroid/support/v7/internal/widget/aa;->a(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {p0, v1}, Landroid/support/v7/internal/widget/X;->setButtonDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 51
    invoke-virtual {v0}, Landroid/support/v7/internal/widget/aa;->b()V

    .line 53
    invoke-virtual {v0}, Landroid/support/v7/internal/widget/aa;->c()Landroid/support/v7/internal/widget/V;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v7/internal/widget/X;->b:Landroid/support/v7/internal/widget/V;

    .line 54
    return-void
.end method


# virtual methods
.method public final setButtonDrawable(I)V
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Landroid/support/v7/internal/widget/X;->b:Landroid/support/v7/internal/widget/V;

    invoke-virtual {v0, p1}, Landroid/support/v7/internal/widget/V;->a(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/support/v7/internal/widget/X;->setButtonDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 59
    return-void
.end method
