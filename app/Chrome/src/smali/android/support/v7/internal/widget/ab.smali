.class public final Landroid/support/v7/internal/widget/ab;
.super Ljava/lang/Object;
.source "ToolbarWidgetWrapper.java"

# interfaces
.implements Landroid/support/v7/internal/widget/u;


# instance fields
.field private a:Landroid/support/v7/widget/Toolbar;

.field private b:I

.field private c:Landroid/view/View;

.field private d:Landroid/view/View;

.field private e:Landroid/graphics/drawable/Drawable;

.field private f:Landroid/graphics/drawable/Drawable;

.field private g:Landroid/graphics/drawable/Drawable;

.field private h:Z

.field private i:Ljava/lang/CharSequence;

.field private j:Ljava/lang/CharSequence;

.field private k:Ljava/lang/CharSequence;

.field private l:Landroid/support/v7/internal/a/a;

.field private m:Z

.field private n:Landroid/support/v7/widget/ActionMenuPresenter;

.field private o:I

.field private final p:Landroid/support/v7/internal/widget/V;

.field private q:I

.field private r:Landroid/graphics/drawable/Drawable;


# direct methods
.method public constructor <init>(Landroid/support/v7/widget/Toolbar;Z)V
    .locals 3

    .prologue
    .line 88
    const/4 v0, 0x1

    const v1, 0x7f0a0050

    const v2, 0x7f02000e

    invoke-direct {p0, p1, v0, v1, v2}, Landroid/support/v7/internal/widget/ab;-><init>(Landroid/support/v7/widget/Toolbar;ZII)V

    .line 90
    return-void
.end method

.method private constructor <init>(Landroid/support/v7/widget/Toolbar;ZII)V
    .locals 8

    .prologue
    const/4 v3, 0x0

    const/4 v7, -0x1

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 93
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 81
    iput v2, p0, Landroid/support/v7/internal/widget/ab;->o:I

    .line 84
    iput v2, p0, Landroid/support/v7/internal/widget/ab;->q:I

    .line 94
    iput-object p1, p0, Landroid/support/v7/internal/widget/ab;->a:Landroid/support/v7/widget/Toolbar;

    .line 95
    invoke-virtual {p1}, Landroid/support/v7/widget/Toolbar;->getTitle()Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v7/internal/widget/ab;->i:Ljava/lang/CharSequence;

    .line 96
    invoke-virtual {p1}, Landroid/support/v7/widget/Toolbar;->getSubtitle()Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v7/internal/widget/ab;->j:Ljava/lang/CharSequence;

    .line 97
    iget-object v0, p0, Landroid/support/v7/internal/widget/ab;->i:Ljava/lang/CharSequence;

    if-eqz v0, :cond_10

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Landroid/support/v7/internal/widget/ab;->h:Z

    .line 99
    if-eqz p2, :cond_11

    .line 100
    invoke-virtual {p1}, Landroid/support/v7/widget/Toolbar;->getContext()Landroid/content/Context;

    move-result-object v0

    sget-object v4, Landroid/support/v7/a/a;->a:[I

    const v5, 0x7f010053

    invoke-static {v0, v3, v4, v5, v2}, Landroid/support/v7/internal/widget/aa;->a(Landroid/content/Context;Landroid/util/AttributeSet;[III)Landroid/support/v7/internal/widget/aa;

    move-result-object v0

    .line 103
    invoke-virtual {v0, v2}, Landroid/support/v7/internal/widget/aa;->b(I)Ljava/lang/CharSequence;

    move-result-object v4

    .line 104
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 105
    iput-boolean v1, p0, Landroid/support/v7/internal/widget/ab;->h:Z

    invoke-direct {p0, v4}, Landroid/support/v7/internal/widget/ab;->b(Ljava/lang/CharSequence;)V

    .line 108
    :cond_0
    const/4 v4, 0x5

    invoke-virtual {v0, v4}, Landroid/support/v7/internal/widget/aa;->b(I)Ljava/lang/CharSequence;

    move-result-object v4

    .line 109
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 110
    iput-object v4, p0, Landroid/support/v7/internal/widget/ab;->j:Ljava/lang/CharSequence;

    iget v5, p0, Landroid/support/v7/internal/widget/ab;->b:I

    and-int/lit8 v5, v5, 0x8

    if-eqz v5, :cond_1

    iget-object v5, p0, Landroid/support/v7/internal/widget/ab;->a:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v5, v4}, Landroid/support/v7/widget/Toolbar;->setSubtitle(Ljava/lang/CharSequence;)V

    .line 113
    :cond_1
    const/16 v4, 0x9

    invoke-virtual {v0, v4}, Landroid/support/v7/internal/widget/aa;->a(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    .line 114
    if-eqz v4, :cond_2

    .line 115
    iput-object v4, p0, Landroid/support/v7/internal/widget/ab;->f:Landroid/graphics/drawable/Drawable;

    invoke-direct {p0}, Landroid/support/v7/internal/widget/ab;->p()V

    .line 118
    :cond_2
    const/16 v4, 0x8

    invoke-virtual {v0, v4}, Landroid/support/v7/internal/widget/aa;->a(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    .line 119
    if-eqz v4, :cond_3

    .line 120
    iput-object v4, p0, Landroid/support/v7/internal/widget/ab;->e:Landroid/graphics/drawable/Drawable;

    invoke-direct {p0}, Landroid/support/v7/internal/widget/ab;->p()V

    .line 123
    :cond_3
    const/4 v4, 0x2

    invoke-virtual {v0, v4}, Landroid/support/v7/internal/widget/aa;->a(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    .line 124
    if-eqz v4, :cond_4

    .line 125
    iput-object v4, p0, Landroid/support/v7/internal/widget/ab;->g:Landroid/graphics/drawable/Drawable;

    invoke-direct {p0}, Landroid/support/v7/internal/widget/ab;->r()V

    .line 128
    :cond_4
    const/4 v4, 0x4

    invoke-virtual {v0, v4, v2}, Landroid/support/v7/internal/widget/aa;->a(II)I

    move-result v4

    invoke-virtual {p0, v4}, Landroid/support/v7/internal/widget/ab;->a(I)V

    .line 130
    const/16 v4, 0xe

    invoke-virtual {v0, v4, v2}, Landroid/support/v7/internal/widget/aa;->f(II)I

    move-result v4

    .line 132
    if-eqz v4, :cond_7

    .line 133
    iget-object v5, p0, Landroid/support/v7/internal/widget/ab;->a:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v5}, Landroid/support/v7/widget/Toolbar;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v5

    iget-object v6, p0, Landroid/support/v7/internal/widget/ab;->a:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v5, v4, v6, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v4

    iget-object v5, p0, Landroid/support/v7/internal/widget/ab;->d:Landroid/view/View;

    if-eqz v5, :cond_5

    iget v5, p0, Landroid/support/v7/internal/widget/ab;->b:I

    and-int/lit8 v5, v5, 0x10

    if-eqz v5, :cond_5

    iget-object v5, p0, Landroid/support/v7/internal/widget/ab;->a:Landroid/support/v7/widget/Toolbar;

    iget-object v6, p0, Landroid/support/v7/internal/widget/ab;->d:Landroid/view/View;

    invoke-virtual {v5, v6}, Landroid/support/v7/widget/Toolbar;->removeView(Landroid/view/View;)V

    :cond_5
    iput-object v4, p0, Landroid/support/v7/internal/widget/ab;->d:Landroid/view/View;

    if-eqz v4, :cond_6

    iget v4, p0, Landroid/support/v7/internal/widget/ab;->b:I

    and-int/lit8 v4, v4, 0x10

    if-eqz v4, :cond_6

    iget-object v4, p0, Landroid/support/v7/internal/widget/ab;->a:Landroid/support/v7/widget/Toolbar;

    iget-object v5, p0, Landroid/support/v7/internal/widget/ab;->d:Landroid/view/View;

    invoke-virtual {v4, v5}, Landroid/support/v7/widget/Toolbar;->addView(Landroid/view/View;)V

    .line 135
    :cond_6
    iget v4, p0, Landroid/support/v7/internal/widget/ab;->b:I

    or-int/lit8 v4, v4, 0x10

    invoke-virtual {p0, v4}, Landroid/support/v7/internal/widget/ab;->a(I)V

    .line 138
    :cond_7
    invoke-virtual {v0, v1, v2}, Landroid/support/v7/internal/widget/aa;->e(II)I

    move-result v1

    .line 139
    if-lez v1, :cond_8

    .line 140
    iget-object v4, p0, Landroid/support/v7/internal/widget/ab;->a:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v4}, Landroid/support/v7/widget/Toolbar;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    .line 141
    iput v1, v4, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 142
    iget-object v1, p0, Landroid/support/v7/internal/widget/ab;->a:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v1, v4}, Landroid/support/v7/widget/Toolbar;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 145
    :cond_8
    const/16 v1, 0x15

    invoke-virtual {v0, v1, v7}, Landroid/support/v7/internal/widget/aa;->c(II)I

    move-result v1

    .line 147
    const/16 v4, 0x16

    invoke-virtual {v0, v4, v7}, Landroid/support/v7/internal/widget/aa;->c(II)I

    move-result v4

    .line 149
    if-gez v1, :cond_9

    if-ltz v4, :cond_a

    .line 150
    :cond_9
    iget-object v5, p0, Landroid/support/v7/internal/widget/ab;->a:Landroid/support/v7/widget/Toolbar;

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    invoke-static {v4, v2}, Ljava/lang/Math;->max(II)I

    move-result v4

    invoke-virtual {v5, v1, v4}, Landroid/support/v7/widget/Toolbar;->setContentInsetsRelative(II)V

    .line 154
    :cond_a
    const/4 v1, 0x6

    invoke-virtual {v0, v1, v2}, Landroid/support/v7/internal/widget/aa;->f(II)I

    move-result v1

    .line 155
    if-eqz v1, :cond_b

    .line 156
    iget-object v4, p0, Landroid/support/v7/internal/widget/ab;->a:Landroid/support/v7/widget/Toolbar;

    iget-object v5, p0, Landroid/support/v7/internal/widget/ab;->a:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v5}, Landroid/support/v7/widget/Toolbar;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v4, v5, v1}, Landroid/support/v7/widget/Toolbar;->setTitleTextAppearance(Landroid/content/Context;I)V

    .line 159
    :cond_b
    const/4 v1, 0x7

    invoke-virtual {v0, v1, v2}, Landroid/support/v7/internal/widget/aa;->f(II)I

    move-result v1

    .line 161
    if-eqz v1, :cond_c

    .line 162
    iget-object v4, p0, Landroid/support/v7/internal/widget/ab;->a:Landroid/support/v7/widget/Toolbar;

    iget-object v5, p0, Landroid/support/v7/internal/widget/ab;->a:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v5}, Landroid/support/v7/widget/Toolbar;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v4, v5, v1}, Landroid/support/v7/widget/Toolbar;->setSubtitleTextAppearance(Landroid/content/Context;I)V

    .line 165
    :cond_c
    const/16 v1, 0x1a

    invoke-virtual {v0, v1, v2}, Landroid/support/v7/internal/widget/aa;->f(II)I

    move-result v1

    .line 166
    if-eqz v1, :cond_d

    .line 167
    iget-object v2, p0, Landroid/support/v7/internal/widget/ab;->a:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v2, v1}, Landroid/support/v7/widget/Toolbar;->setPopupTheme(I)V

    .line 170
    :cond_d
    invoke-virtual {v0}, Landroid/support/v7/internal/widget/aa;->b()V

    .line 172
    invoke-virtual {v0}, Landroid/support/v7/internal/widget/aa;->c()Landroid/support/v7/internal/widget/V;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v7/internal/widget/ab;->p:Landroid/support/v7/internal/widget/V;

    .line 179
    :goto_1
    iget v0, p0, Landroid/support/v7/internal/widget/ab;->q:I

    if-eq p3, v0, :cond_e

    iput p3, p0, Landroid/support/v7/internal/widget/ab;->q:I

    iget-object v0, p0, Landroid/support/v7/internal/widget/ab;->a:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v0}, Landroid/support/v7/widget/Toolbar;->getNavigationContentDescription()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_e

    iget v0, p0, Landroid/support/v7/internal/widget/ab;->q:I

    if-nez v0, :cond_13

    move-object v0, v3

    :goto_2
    iput-object v0, p0, Landroid/support/v7/internal/widget/ab;->k:Ljava/lang/CharSequence;

    invoke-direct {p0}, Landroid/support/v7/internal/widget/ab;->q()V

    .line 180
    :cond_e
    iget-object v0, p0, Landroid/support/v7/internal/widget/ab;->a:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v0}, Landroid/support/v7/widget/Toolbar;->getNavigationContentDescription()Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v7/internal/widget/ab;->k:Ljava/lang/CharSequence;

    .line 182
    iget-object v0, p0, Landroid/support/v7/internal/widget/ab;->p:Landroid/support/v7/internal/widget/V;

    invoke-virtual {v0, p4}, Landroid/support/v7/internal/widget/V;->a(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iget-object v1, p0, Landroid/support/v7/internal/widget/ab;->r:Landroid/graphics/drawable/Drawable;

    if-eq v1, v0, :cond_f

    iput-object v0, p0, Landroid/support/v7/internal/widget/ab;->r:Landroid/graphics/drawable/Drawable;

    invoke-direct {p0}, Landroid/support/v7/internal/widget/ab;->r()V

    .line 184
    :cond_f
    iget-object v0, p0, Landroid/support/v7/internal/widget/ab;->a:Landroid/support/v7/widget/Toolbar;

    new-instance v1, Landroid/support/v7/internal/widget/ac;

    invoke-direct {v1, p0}, Landroid/support/v7/internal/widget/ac;-><init>(Landroid/support/v7/internal/widget/ab;)V

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/Toolbar;->setNavigationOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 194
    return-void

    :cond_10
    move v0, v2

    .line 97
    goto/16 :goto_0

    .line 174
    :cond_11
    const/16 v0, 0xb

    iget-object v1, p0, Landroid/support/v7/internal/widget/ab;->a:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v1}, Landroid/support/v7/widget/Toolbar;->getNavigationIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    if-eqz v1, :cond_12

    const/16 v0, 0xf

    :cond_12
    iput v0, p0, Landroid/support/v7/internal/widget/ab;->b:I

    .line 176
    new-instance v0, Landroid/support/v7/internal/widget/V;

    invoke-virtual {p1}, Landroid/support/v7/widget/Toolbar;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/support/v7/internal/widget/V;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Landroid/support/v7/internal/widget/ab;->p:Landroid/support/v7/internal/widget/V;

    goto :goto_1

    .line 179
    :cond_13
    iget-object v1, p0, Landroid/support/v7/internal/widget/ab;->a:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v1}, Landroid/support/v7/widget/Toolbar;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_2
.end method

.method static synthetic a(Landroid/support/v7/internal/widget/ab;)Landroid/support/v7/widget/Toolbar;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Landroid/support/v7/internal/widget/ab;->a:Landroid/support/v7/widget/Toolbar;

    return-object v0
.end method

.method static synthetic b(Landroid/support/v7/internal/widget/ab;)Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Landroid/support/v7/internal/widget/ab;->i:Ljava/lang/CharSequence;

    return-object v0
.end method

.method private b(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 283
    iput-object p1, p0, Landroid/support/v7/internal/widget/ab;->i:Ljava/lang/CharSequence;

    .line 284
    iget v0, p0, Landroid/support/v7/internal/widget/ab;->b:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_0

    .line 285
    iget-object v0, p0, Landroid/support/v7/internal/widget/ab;->a:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v0, p1}, Landroid/support/v7/widget/Toolbar;->setTitle(Ljava/lang/CharSequence;)V

    .line 287
    :cond_0
    return-void
.end method

.method static synthetic c(Landroid/support/v7/internal/widget/ab;)Landroid/support/v7/internal/a/a;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Landroid/support/v7/internal/widget/ab;->l:Landroid/support/v7/internal/a/a;

    return-object v0
.end method

.method static synthetic d(Landroid/support/v7/internal/widget/ab;)Z
    .locals 1

    .prologue
    .line 55
    iget-boolean v0, p0, Landroid/support/v7/internal/widget/ab;->m:Z

    return v0
.end method

.method private p()V
    .locals 2

    .prologue
    .line 366
    const/4 v0, 0x0

    .line 367
    iget v1, p0, Landroid/support/v7/internal/widget/ab;->b:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_0

    .line 368
    iget v0, p0, Landroid/support/v7/internal/widget/ab;->b:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_2

    .line 369
    iget-object v0, p0, Landroid/support/v7/internal/widget/ab;->f:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_1

    iget-object v0, p0, Landroid/support/v7/internal/widget/ab;->f:Landroid/graphics/drawable/Drawable;

    .line 374
    :cond_0
    :goto_0
    iget-object v1, p0, Landroid/support/v7/internal/widget/ab;->a:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v1, v0}, Landroid/support/v7/widget/Toolbar;->setLogo(Landroid/graphics/drawable/Drawable;)V

    .line 375
    return-void

    .line 369
    :cond_1
    iget-object v0, p0, Landroid/support/v7/internal/widget/ab;->e:Landroid/graphics/drawable/Drawable;

    goto :goto_0

    .line 371
    :cond_2
    iget-object v0, p0, Landroid/support/v7/internal/widget/ab;->e:Landroid/graphics/drawable/Drawable;

    goto :goto_0
.end method

.method private q()V
    .locals 2

    .prologue
    .line 654
    iget v0, p0, Landroid/support/v7/internal/widget/ab;->b:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_0

    .line 655
    iget-object v0, p0, Landroid/support/v7/internal/widget/ab;->k:Ljava/lang/CharSequence;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 656
    iget-object v0, p0, Landroid/support/v7/internal/widget/ab;->a:Landroid/support/v7/widget/Toolbar;

    iget v1, p0, Landroid/support/v7/internal/widget/ab;->q:I

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/Toolbar;->setNavigationContentDescription(I)V

    .line 661
    :cond_0
    :goto_0
    return-void

    .line 658
    :cond_1
    iget-object v0, p0, Landroid/support/v7/internal/widget/ab;->a:Landroid/support/v7/widget/Toolbar;

    iget-object v1, p0, Landroid/support/v7/internal/widget/ab;->k:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/Toolbar;->setNavigationContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private r()V
    .locals 2

    .prologue
    .line 664
    iget v0, p0, Landroid/support/v7/internal/widget/ab;->b:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_0

    .line 665
    iget-object v1, p0, Landroid/support/v7/internal/widget/ab;->a:Landroid/support/v7/widget/Toolbar;

    iget-object v0, p0, Landroid/support/v7/internal/widget/ab;->g:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_1

    iget-object v0, p0, Landroid/support/v7/internal/widget/ab;->g:Landroid/graphics/drawable/Drawable;

    :goto_0
    invoke-virtual {v1, v0}, Landroid/support/v7/widget/Toolbar;->setNavigationIcon(Landroid/graphics/drawable/Drawable;)V

    .line 667
    :cond_0
    return-void

    .line 665
    :cond_1
    iget-object v0, p0, Landroid/support/v7/internal/widget/ab;->r:Landroid/graphics/drawable/Drawable;

    goto :goto_0
.end method


# virtual methods
.method public final a()Landroid/view/ViewGroup;
    .locals 1

    .prologue
    .line 235
    iget-object v0, p0, Landroid/support/v7/internal/widget/ab;->a:Landroid/support/v7/widget/Toolbar;

    return-object v0
.end method

.method public final a(I)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 429
    iget v0, p0, Landroid/support/v7/internal/widget/ab;->b:I

    .line 430
    xor-int/2addr v0, p1

    .line 431
    iput p1, p0, Landroid/support/v7/internal/widget/ab;->b:I

    .line 432
    if-eqz v0, :cond_3

    .line 433
    and-int/lit8 v1, v0, 0x4

    if-eqz v1, :cond_0

    .line 434
    and-int/lit8 v1, p1, 0x4

    if-eqz v1, :cond_4

    .line 435
    invoke-direct {p0}, Landroid/support/v7/internal/widget/ab;->r()V

    .line 436
    invoke-direct {p0}, Landroid/support/v7/internal/widget/ab;->q()V

    .line 442
    :cond_0
    :goto_0
    and-int/lit8 v1, v0, 0x3

    if-eqz v1, :cond_1

    .line 443
    invoke-direct {p0}, Landroid/support/v7/internal/widget/ab;->p()V

    .line 446
    :cond_1
    and-int/lit8 v1, v0, 0x8

    if-eqz v1, :cond_2

    .line 447
    and-int/lit8 v1, p1, 0x8

    if-eqz v1, :cond_5

    .line 448
    iget-object v1, p0, Landroid/support/v7/internal/widget/ab;->a:Landroid/support/v7/widget/Toolbar;

    iget-object v2, p0, Landroid/support/v7/internal/widget/ab;->i:Ljava/lang/CharSequence;

    invoke-virtual {v1, v2}, Landroid/support/v7/widget/Toolbar;->setTitle(Ljava/lang/CharSequence;)V

    .line 449
    iget-object v1, p0, Landroid/support/v7/internal/widget/ab;->a:Landroid/support/v7/widget/Toolbar;

    iget-object v2, p0, Landroid/support/v7/internal/widget/ab;->j:Ljava/lang/CharSequence;

    invoke-virtual {v1, v2}, Landroid/support/v7/widget/Toolbar;->setSubtitle(Ljava/lang/CharSequence;)V

    .line 456
    :cond_2
    :goto_1
    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_3

    iget-object v0, p0, Landroid/support/v7/internal/widget/ab;->d:Landroid/view/View;

    if-eqz v0, :cond_3

    .line 457
    and-int/lit8 v0, p1, 0x10

    if-eqz v0, :cond_6

    .line 458
    iget-object v0, p0, Landroid/support/v7/internal/widget/ab;->a:Landroid/support/v7/widget/Toolbar;

    iget-object v1, p0, Landroid/support/v7/internal/widget/ab;->d:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/Toolbar;->addView(Landroid/view/View;)V

    .line 464
    :cond_3
    :goto_2
    return-void

    .line 438
    :cond_4
    iget-object v1, p0, Landroid/support/v7/internal/widget/ab;->a:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v1, v2}, Landroid/support/v7/widget/Toolbar;->setNavigationIcon(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 451
    :cond_5
    iget-object v1, p0, Landroid/support/v7/internal/widget/ab;->a:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v1, v2}, Landroid/support/v7/widget/Toolbar;->setTitle(Ljava/lang/CharSequence;)V

    .line 452
    iget-object v1, p0, Landroid/support/v7/internal/widget/ab;->a:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v1, v2}, Landroid/support/v7/widget/Toolbar;->setSubtitle(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 460
    :cond_6
    iget-object v0, p0, Landroid/support/v7/internal/widget/ab;->a:Landroid/support/v7/widget/Toolbar;

    iget-object v1, p0, Landroid/support/v7/internal/widget/ab;->d:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/Toolbar;->removeView(Landroid/view/View;)V

    goto :goto_2
.end method

.method public final a(Landroid/support/v7/internal/a/a;)V
    .locals 0

    .prologue
    .line 260
    iput-object p1, p0, Landroid/support/v7/internal/widget/ab;->l:Landroid/support/v7/internal/a/a;

    .line 261
    return-void
.end method

.method public final a(Landroid/support/v7/internal/widget/E;)V
    .locals 2

    .prologue
    .line 468
    iget-object v0, p0, Landroid/support/v7/internal/widget/ab;->c:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/internal/widget/ab;->c:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    iget-object v1, p0, Landroid/support/v7/internal/widget/ab;->a:Landroid/support/v7/widget/Toolbar;

    if-ne v0, v1, :cond_0

    .line 469
    iget-object v0, p0, Landroid/support/v7/internal/widget/ab;->a:Landroid/support/v7/widget/Toolbar;

    iget-object v1, p0, Landroid/support/v7/internal/widget/ab;->c:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/Toolbar;->removeView(Landroid/view/View;)V

    .line 471
    :cond_0
    iput-object p1, p0, Landroid/support/v7/internal/widget/ab;->c:Landroid/view/View;

    .line 472
    return-void
.end method

.method public final a(Landroid/view/Menu;Landroid/support/v7/internal/view/menu/y;)V
    .locals 2

    .prologue
    .line 409
    iget-object v0, p0, Landroid/support/v7/internal/widget/ab;->n:Landroid/support/v7/widget/ActionMenuPresenter;

    if-nez v0, :cond_0

    .line 410
    new-instance v0, Landroid/support/v7/widget/ActionMenuPresenter;

    iget-object v1, p0, Landroid/support/v7/internal/widget/ab;->a:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v1}, Landroid/support/v7/widget/Toolbar;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/support/v7/widget/ActionMenuPresenter;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Landroid/support/v7/internal/widget/ab;->n:Landroid/support/v7/widget/ActionMenuPresenter;

    .line 411
    iget-object v0, p0, Landroid/support/v7/internal/widget/ab;->n:Landroid/support/v7/widget/ActionMenuPresenter;

    .line 413
    :cond_0
    iget-object v0, p0, Landroid/support/v7/internal/widget/ab;->n:Landroid/support/v7/widget/ActionMenuPresenter;

    invoke-virtual {v0, p2}, Landroid/support/v7/widget/ActionMenuPresenter;->a(Landroid/support/v7/internal/view/menu/y;)V

    .line 414
    iget-object v0, p0, Landroid/support/v7/internal/widget/ab;->a:Landroid/support/v7/widget/Toolbar;

    check-cast p1, Landroid/support/v7/internal/view/menu/i;

    iget-object v1, p0, Landroid/support/v7/internal/widget/ab;->n:Landroid/support/v7/widget/ActionMenuPresenter;

    invoke-virtual {v0, p1, v1}, Landroid/support/v7/widget/Toolbar;->setMenu(Landroid/support/v7/internal/view/menu/i;Landroid/support/v7/widget/ActionMenuPresenter;)V

    .line 415
    return-void
.end method

.method public final a(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 266
    iget-boolean v0, p0, Landroid/support/v7/internal/widget/ab;->h:Z

    if-nez v0, :cond_0

    .line 267
    invoke-direct {p0, p1}, Landroid/support/v7/internal/widget/ab;->b(Ljava/lang/CharSequence;)V

    .line 269
    :cond_0
    return-void
.end method

.method public final a(Z)V
    .locals 1

    .prologue
    .line 494
    iget-object v0, p0, Landroid/support/v7/internal/widget/ab;->a:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v0, p1}, Landroid/support/v7/widget/Toolbar;->setCollapsible(Z)V

    .line 495
    return-void
.end method

.method public final b()Landroid/content/Context;
    .locals 1

    .prologue
    .line 240
    iget-object v0, p0, Landroid/support/v7/internal/widget/ab;->a:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v0}, Landroid/support/v7/widget/Toolbar;->getContext()Landroid/content/Context;

    move-result-object v0

    return-object v0
.end method

.method public final b(I)V
    .locals 2

    .prologue
    .line 602
    const/16 v0, 0x8

    if-ne p1, v0, :cond_1

    .line 603
    iget-object v0, p0, Landroid/support/v7/internal/widget/ab;->a:Landroid/support/v7/widget/Toolbar;

    invoke-static {v0}, Landroid/support/v4/view/ViewCompat;->k(Landroid/view/View;)Landroid/support/v4/view/ar;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ar;->a(F)Landroid/support/v4/view/ar;

    move-result-object v0

    new-instance v1, Landroid/support/v7/internal/widget/ad;

    invoke-direct {v1, p0}, Landroid/support/v7/internal/widget/ad;-><init>(Landroid/support/v7/internal/widget/ab;)V

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ar;->a(Landroid/support/v4/view/aC;)Landroid/support/v4/view/ar;

    .line 627
    :cond_0
    :goto_0
    return-void

    .line 618
    :cond_1
    if-nez p1, :cond_0

    .line 619
    iget-object v0, p0, Landroid/support/v7/internal/widget/ab;->a:Landroid/support/v7/widget/Toolbar;

    invoke-static {v0}, Landroid/support/v4/view/ViewCompat;->k(Landroid/view/View;)Landroid/support/v4/view/ar;

    move-result-object v0

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ar;->a(F)Landroid/support/v4/view/ar;

    move-result-object v0

    new-instance v1, Landroid/support/v7/internal/widget/ae;

    invoke-direct {v1, p0}, Landroid/support/v7/internal/widget/ae;-><init>(Landroid/support/v7/internal/widget/ab;)V

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ar;->a(Landroid/support/v4/view/aC;)Landroid/support/v4/view/ar;

    goto :goto_0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 250
    iget-object v0, p0, Landroid/support/v7/internal/widget/ab;->a:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v0}, Landroid/support/v7/widget/Toolbar;->hasExpandedActionView()Z

    move-result v0

    return v0
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 255
    iget-object v0, p0, Landroid/support/v7/internal/widget/ab;->a:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v0}, Landroid/support/v7/widget/Toolbar;->collapseActionView()V

    .line 256
    return-void
.end method

.method public final e()V
    .locals 2

    .prologue
    .line 304
    const-string/jumbo v0, "ToolbarWidgetWrapper"

    const-string/jumbo v1, "Progress display unsupported"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 305
    return-void
.end method

.method public final f()V
    .locals 2

    .prologue
    .line 309
    const-string/jumbo v0, "ToolbarWidgetWrapper"

    const-string/jumbo v1, "Progress display unsupported"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 310
    return-void
.end method

.method public final g()Z
    .locals 1

    .prologue
    .line 379
    iget-object v0, p0, Landroid/support/v7/internal/widget/ab;->a:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v0}, Landroid/support/v7/widget/Toolbar;->canShowOverflowMenu()Z

    move-result v0

    return v0
.end method

.method public final h()Z
    .locals 1

    .prologue
    .line 384
    iget-object v0, p0, Landroid/support/v7/internal/widget/ab;->a:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v0}, Landroid/support/v7/widget/Toolbar;->isOverflowMenuShowing()Z

    move-result v0

    return v0
.end method

.method public final i()Z
    .locals 1

    .prologue
    .line 389
    iget-object v0, p0, Landroid/support/v7/internal/widget/ab;->a:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v0}, Landroid/support/v7/widget/Toolbar;->isOverflowMenuShowPending()Z

    move-result v0

    return v0
.end method

.method public final j()Z
    .locals 1

    .prologue
    .line 394
    iget-object v0, p0, Landroid/support/v7/internal/widget/ab;->a:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v0}, Landroid/support/v7/widget/Toolbar;->showOverflowMenu()Z

    move-result v0

    return v0
.end method

.method public final k()Z
    .locals 1

    .prologue
    .line 399
    iget-object v0, p0, Landroid/support/v7/internal/widget/ab;->a:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v0}, Landroid/support/v7/widget/Toolbar;->hideOverflowMenu()Z

    move-result v0

    return v0
.end method

.method public final l()V
    .locals 1

    .prologue
    .line 404
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v7/internal/widget/ab;->m:Z

    .line 405
    return-void
.end method

.method public final m()V
    .locals 1

    .prologue
    .line 419
    iget-object v0, p0, Landroid/support/v7/internal/widget/ab;->a:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v0}, Landroid/support/v7/widget/Toolbar;->dismissPopupMenus()V

    .line 420
    return-void
.end method

.method public final n()I
    .locals 1

    .prologue
    .line 424
    iget v0, p0, Landroid/support/v7/internal/widget/ab;->b:I

    return v0
.end method

.method public final o()I
    .locals 1

    .prologue
    .line 504
    const/4 v0, 0x0

    return v0
.end method
