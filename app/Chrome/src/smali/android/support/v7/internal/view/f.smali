.class final Landroid/support/v7/internal/view/f;
.super Ljava/lang/Object;
.source "SupportMenuInflater.java"


# instance fields
.field private a:Landroid/view/Menu;

.field private b:I

.field private c:I

.field private d:I

.field private e:I

.field private f:Z

.field private g:Z

.field private h:Z

.field private i:I

.field private j:I

.field private k:Ljava/lang/CharSequence;

.field private l:Ljava/lang/CharSequence;

.field private m:I

.field private n:C

.field private o:C

.field private p:I

.field private q:Z

.field private r:Z

.field private s:Z

.field private t:I

.field private u:I

.field private v:Ljava/lang/String;

.field private w:Ljava/lang/String;

.field private x:Ljava/lang/String;

.field private y:Landroid/support/v4/media/n;

.field private synthetic z:Landroid/support/v7/internal/view/d;


# direct methods
.method public constructor <init>(Landroid/support/v7/internal/view/d;Landroid/view/Menu;)V
    .locals 0

    .prologue
    .line 329
    iput-object p1, p0, Landroid/support/v7/internal/view/f;->z:Landroid/support/v7/internal/view/d;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 330
    iput-object p2, p0, Landroid/support/v7/internal/view/f;->a:Landroid/view/Menu;

    .line 332
    invoke-virtual {p0}, Landroid/support/v7/internal/view/f;->a()V

    .line 333
    return-void
.end method

.method private static a(Ljava/lang/String;)C
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 417
    if-nez p0, :cond_0

    .line 420
    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    move-result v0

    goto :goto_0
.end method

.method static synthetic a(Landroid/support/v7/internal/view/f;)Landroid/support/v4/media/n;
    .locals 1

    .prologue
    .line 270
    iget-object v0, p0, Landroid/support/v7/internal/view/f;->y:Landroid/support/v4/media/n;

    return-object v0
.end method

.method private a(Ljava/lang/String;[Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 497
    :try_start_0
    iget-object v0, p0, Landroid/support/v7/internal/view/f;->z:Landroid/support/v7/internal/view/d;

    invoke-static {v0}, Landroid/support/v7/internal/view/d;->a(Landroid/support/v7/internal/view/d;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/ClassLoader;->loadClass(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    .line 498
    invoke-virtual {v0, p2}, Ljava/lang/Class;->getConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v0

    .line 499
    invoke-virtual {v0, p3}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 503
    :goto_0
    return-object v0

    .line 500
    :catch_0
    move-exception v0

    .line 501
    const-string/jumbo v1, "SupportMenuInflater"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "Cannot instantiate class: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 503
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Landroid/view/MenuItem;)V
    .locals 5

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 425
    iget-boolean v0, p0, Landroid/support/v7/internal/view/f;->q:Z

    invoke-interface {p1, v0}, Landroid/view/MenuItem;->setChecked(Z)Landroid/view/MenuItem;

    move-result-object v0

    iget-boolean v3, p0, Landroid/support/v7/internal/view/f;->r:Z

    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    move-result-object v0

    iget-boolean v3, p0, Landroid/support/v7/internal/view/f;->s:Z

    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    move-result-object v3

    iget v0, p0, Landroid/support/v7/internal/view/f;->p:I

    if-lez v0, :cond_1

    move v0, v1

    :goto_0
    invoke-interface {v3, v0}, Landroid/view/MenuItem;->setCheckable(Z)Landroid/view/MenuItem;

    move-result-object v0

    iget-object v3, p0, Landroid/support/v7/internal/view/f;->l:Ljava/lang/CharSequence;

    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setTitleCondensed(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v0

    iget v3, p0, Landroid/support/v7/internal/view/f;->m:I

    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    move-result-object v0

    iget-char v3, p0, Landroid/support/v7/internal/view/f;->n:C

    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setAlphabeticShortcut(C)Landroid/view/MenuItem;

    move-result-object v0

    iget-char v3, p0, Landroid/support/v7/internal/view/f;->o:C

    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setNumericShortcut(C)Landroid/view/MenuItem;

    .line 434
    iget v0, p0, Landroid/support/v7/internal/view/f;->t:I

    if-ltz v0, :cond_0

    .line 435
    iget v0, p0, Landroid/support/v7/internal/view/f;->t:I

    invoke-static {p1, v0}, Landroid/support/v4/view/w;->a(Landroid/view/MenuItem;I)V

    .line 438
    :cond_0
    iget-object v0, p0, Landroid/support/v7/internal/view/f;->x:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 439
    iget-object v0, p0, Landroid/support/v7/internal/view/f;->z:Landroid/support/v7/internal/view/d;

    invoke-static {v0}, Landroid/support/v7/internal/view/d;->a(Landroid/support/v7/internal/view/d;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->isRestricted()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 440
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "The android:onClick attribute cannot be used within a restricted context"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    move v0, v2

    .line 425
    goto :goto_0

    .line 443
    :cond_2
    new-instance v0, Landroid/support/v7/internal/view/e;

    iget-object v3, p0, Landroid/support/v7/internal/view/f;->z:Landroid/support/v7/internal/view/d;

    invoke-static {v3}, Landroid/support/v7/internal/view/d;->c(Landroid/support/v7/internal/view/d;)Ljava/lang/Object;

    move-result-object v3

    iget-object v4, p0, Landroid/support/v7/internal/view/f;->x:Ljava/lang/String;

    invoke-direct {v0, v3, v4}, Landroid/support/v7/internal/view/e;-><init>(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p1, v0}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 447
    :cond_3
    iget v0, p0, Landroid/support/v7/internal/view/f;->p:I

    const/4 v3, 0x2

    if-lt v0, v3, :cond_4

    .line 449
    instance-of v0, p1, Landroid/support/v7/internal/view/menu/m;

    if-eqz v0, :cond_7

    move-object v0, p1

    .line 450
    check-cast v0, Landroid/support/v7/internal/view/menu/m;

    invoke-virtual {v0, v1}, Landroid/support/v7/internal/view/menu/m;->a(Z)V

    .line 457
    :cond_4
    :goto_1
    iget-object v0, p0, Landroid/support/v7/internal/view/f;->v:Ljava/lang/String;

    if-eqz v0, :cond_9

    .line 458
    iget-object v0, p0, Landroid/support/v7/internal/view/f;->v:Ljava/lang/String;

    invoke-static {}, Landroid/support/v7/internal/view/d;->b()[Ljava/lang/Class;

    move-result-object v2

    iget-object v3, p0, Landroid/support/v7/internal/view/f;->z:Landroid/support/v7/internal/view/d;

    invoke-static {v3}, Landroid/support/v7/internal/view/d;->d(Landroid/support/v7/internal/view/d;)[Ljava/lang/Object;

    move-result-object v3

    invoke-direct {p0, v0, v2, v3}, Landroid/support/v7/internal/view/f;->a(Ljava/lang/String;[Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 460
    invoke-static {p1, v0}, Landroid/support/v4/view/w;->a(Landroid/view/MenuItem;Landroid/view/View;)Landroid/view/MenuItem;

    .line 463
    :goto_2
    iget v0, p0, Landroid/support/v7/internal/view/f;->u:I

    if-lez v0, :cond_5

    .line 464
    if-nez v1, :cond_8

    .line 465
    iget v0, p0, Landroid/support/v7/internal/view/f;->u:I

    invoke-static {p1, v0}, Landroid/support/v4/view/w;->b(Landroid/view/MenuItem;I)Landroid/view/MenuItem;

    .line 472
    :cond_5
    :goto_3
    iget-object v0, p0, Landroid/support/v7/internal/view/f;->y:Landroid/support/v4/media/n;

    if-eqz v0, :cond_6

    .line 473
    iget-object v0, p0, Landroid/support/v7/internal/view/f;->y:Landroid/support/v4/media/n;

    invoke-static {p1, v0}, Landroid/support/v4/view/w;->a(Landroid/view/MenuItem;Landroid/support/v4/media/n;)Landroid/view/MenuItem;

    .line 475
    :cond_6
    return-void

    .line 451
    :cond_7
    instance-of v0, p1, Landroid/support/v7/internal/view/menu/o;

    if-eqz v0, :cond_4

    move-object v0, p1

    .line 452
    check-cast v0, Landroid/support/v7/internal/view/menu/o;

    invoke-virtual {v0, v1}, Landroid/support/v7/internal/view/menu/o;->a(Z)V

    goto :goto_1

    .line 468
    :cond_8
    const-string/jumbo v0, "SupportMenuInflater"

    const-string/jumbo v1, "Ignoring attribute \'itemActionViewLayout\'. Action view already specified."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3

    :cond_9
    move v1, v2

    goto :goto_2
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 336
    iput v0, p0, Landroid/support/v7/internal/view/f;->b:I

    .line 337
    iput v0, p0, Landroid/support/v7/internal/view/f;->c:I

    .line 338
    iput v0, p0, Landroid/support/v7/internal/view/f;->d:I

    .line 339
    iput v0, p0, Landroid/support/v7/internal/view/f;->e:I

    .line 340
    iput-boolean v1, p0, Landroid/support/v7/internal/view/f;->f:Z

    .line 341
    iput-boolean v1, p0, Landroid/support/v7/internal/view/f;->g:Z

    .line 342
    return-void
.end method

.method public final a(Landroid/util/AttributeSet;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 348
    iget-object v0, p0, Landroid/support/v7/internal/view/f;->z:Landroid/support/v7/internal/view/d;

    invoke-static {v0}, Landroid/support/v7/internal/view/d;->a(Landroid/support/v7/internal/view/d;)Landroid/content/Context;

    move-result-object v0

    sget-object v1, Landroid/support/v7/a/a;->i:[I

    invoke-virtual {v0, p1, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 350
    invoke-virtual {v0, v3, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    iput v1, p0, Landroid/support/v7/internal/view/f;->b:I

    .line 351
    const/4 v1, 0x3

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v1

    iput v1, p0, Landroid/support/v7/internal/view/f;->c:I

    .line 353
    const/4 v1, 0x4

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v1

    iput v1, p0, Landroid/support/v7/internal/view/f;->d:I

    .line 354
    const/4 v1, 0x5

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v1

    iput v1, p0, Landroid/support/v7/internal/view/f;->e:I

    .line 356
    const/4 v1, 0x2

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, Landroid/support/v7/internal/view/f;->f:Z

    .line 357
    invoke-virtual {v0, v2, v3}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, Landroid/support/v7/internal/view/f;->g:Z

    .line 359
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 360
    return-void
.end method

.method public final b()V
    .locals 5

    .prologue
    .line 478
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v7/internal/view/f;->h:Z

    .line 479
    iget-object v0, p0, Landroid/support/v7/internal/view/f;->a:Landroid/view/Menu;

    iget v1, p0, Landroid/support/v7/internal/view/f;->b:I

    iget v2, p0, Landroid/support/v7/internal/view/f;->i:I

    iget v3, p0, Landroid/support/v7/internal/view/f;->j:I

    iget-object v4, p0, Landroid/support/v7/internal/view/f;->k:Ljava/lang/CharSequence;

    invoke-interface {v0, v1, v2, v3, v4}, Landroid/view/Menu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v0

    invoke-direct {p0, v0}, Landroid/support/v7/internal/view/f;->a(Landroid/view/MenuItem;)V

    .line 480
    return-void
.end method

.method public final b(Landroid/util/AttributeSet;)V
    .locals 7

    .prologue
    const/16 v6, 0xb

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 366
    iget-object v0, p0, Landroid/support/v7/internal/view/f;->z:Landroid/support/v7/internal/view/d;

    invoke-static {v0}, Landroid/support/v7/internal/view/d;->a(Landroid/support/v7/internal/view/d;)Landroid/content/Context;

    move-result-object v0

    sget-object v3, Landroid/support/v7/a/a;->j:[I

    invoke-virtual {v0, p1, v3}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v3

    .line 369
    const/4 v0, 0x2

    invoke-virtual {v3, v0, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v0

    iput v0, p0, Landroid/support/v7/internal/view/f;->i:I

    .line 370
    const/4 v0, 0x5

    iget v4, p0, Landroid/support/v7/internal/view/f;->c:I

    invoke-virtual {v3, v0, v4}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v0

    .line 371
    const/4 v4, 0x6

    iget v5, p0, Landroid/support/v7/internal/view/f;->d:I

    invoke-virtual {v3, v4, v5}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v4

    .line 372
    const/high16 v5, -0x10000

    and-int/2addr v0, v5

    const v5, 0xffff

    and-int/2addr v4, v5

    or-int/2addr v0, v4

    iput v0, p0, Landroid/support/v7/internal/view/f;->j:I

    .line 374
    const/4 v0, 0x7

    invoke-virtual {v3, v0}, Landroid/content/res/TypedArray;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v7/internal/view/f;->k:Ljava/lang/CharSequence;

    .line 375
    const/16 v0, 0x8

    invoke-virtual {v3, v0}, Landroid/content/res/TypedArray;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v7/internal/view/f;->l:Ljava/lang/CharSequence;

    .line 376
    invoke-virtual {v3, v2, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v0

    iput v0, p0, Landroid/support/v7/internal/view/f;->m:I

    .line 377
    const/16 v0, 0x9

    invoke-virtual {v3, v0}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/support/v7/internal/view/f;->a(Ljava/lang/String;)C

    move-result v0

    iput-char v0, p0, Landroid/support/v7/internal/view/f;->n:C

    .line 379
    const/16 v0, 0xa

    invoke-virtual {v3, v0}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/support/v7/internal/view/f;->a(Ljava/lang/String;)C

    move-result v0

    iput-char v0, p0, Landroid/support/v7/internal/view/f;->o:C

    .line 381
    invoke-virtual {v3, v6}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 383
    invoke-virtual {v3, v6, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    iput v0, p0, Landroid/support/v7/internal/view/f;->p:I

    .line 389
    :goto_1
    const/4 v0, 0x3

    invoke-virtual {v3, v0, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v0

    iput-boolean v0, p0, Landroid/support/v7/internal/view/f;->q:Z

    .line 390
    const/4 v0, 0x4

    iget-boolean v4, p0, Landroid/support/v7/internal/view/f;->f:Z

    invoke-virtual {v3, v0, v4}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v0

    iput-boolean v0, p0, Landroid/support/v7/internal/view/f;->r:Z

    .line 391
    iget-boolean v0, p0, Landroid/support/v7/internal/view/f;->g:Z

    invoke-virtual {v3, v1, v0}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v0

    iput-boolean v0, p0, Landroid/support/v7/internal/view/f;->s:Z

    .line 392
    const/16 v0, 0xd

    const/4 v4, -0x1

    invoke-virtual {v3, v0, v4}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v0

    iput v0, p0, Landroid/support/v7/internal/view/f;->t:I

    .line 393
    const/16 v0, 0xc

    invoke-virtual {v3, v0}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v7/internal/view/f;->x:Ljava/lang/String;

    .line 394
    const/16 v0, 0xe

    invoke-virtual {v3, v0, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v0

    iput v0, p0, Landroid/support/v7/internal/view/f;->u:I

    .line 395
    const/16 v0, 0xf

    invoke-virtual {v3, v0}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v7/internal/view/f;->v:Ljava/lang/String;

    .line 396
    const/16 v0, 0x10

    invoke-virtual {v3, v0}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v7/internal/view/f;->w:Ljava/lang/String;

    .line 398
    iget-object v0, p0, Landroid/support/v7/internal/view/f;->w:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 399
    :goto_2
    if-eqz v1, :cond_3

    iget v0, p0, Landroid/support/v7/internal/view/f;->u:I

    if-nez v0, :cond_3

    iget-object v0, p0, Landroid/support/v7/internal/view/f;->v:Ljava/lang/String;

    if-nez v0, :cond_3

    .line 400
    iget-object v0, p0, Landroid/support/v7/internal/view/f;->w:Ljava/lang/String;

    invoke-static {}, Landroid/support/v7/internal/view/d;->a()[Ljava/lang/Class;

    move-result-object v1

    iget-object v4, p0, Landroid/support/v7/internal/view/f;->z:Landroid/support/v7/internal/view/d;

    invoke-static {v4}, Landroid/support/v7/internal/view/d;->b(Landroid/support/v7/internal/view/d;)[Ljava/lang/Object;

    move-result-object v4

    invoke-direct {p0, v0, v1, v4}, Landroid/support/v7/internal/view/f;->a(Ljava/lang/String;[Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/media/n;

    iput-object v0, p0, Landroid/support/v7/internal/view/f;->y:Landroid/support/v4/media/n;

    .line 411
    :goto_3
    invoke-virtual {v3}, Landroid/content/res/TypedArray;->recycle()V

    .line 413
    iput-boolean v2, p0, Landroid/support/v7/internal/view/f;->h:Z

    .line 414
    return-void

    :cond_0
    move v0, v2

    .line 383
    goto :goto_0

    .line 387
    :cond_1
    iget v0, p0, Landroid/support/v7/internal/view/f;->e:I

    iput v0, p0, Landroid/support/v7/internal/view/f;->p:I

    goto :goto_1

    :cond_2
    move v1, v2

    .line 398
    goto :goto_2

    .line 404
    :cond_3
    if-eqz v1, :cond_4

    .line 405
    const-string/jumbo v0, "SupportMenuInflater"

    const-string/jumbo v1, "Ignoring attribute \'actionProviderClass\'. Action view already specified."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 408
    :cond_4
    const/4 v0, 0x0

    iput-object v0, p0, Landroid/support/v7/internal/view/f;->y:Landroid/support/v4/media/n;

    goto :goto_3
.end method

.method public final c()Landroid/view/SubMenu;
    .locals 5

    .prologue
    .line 483
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v7/internal/view/f;->h:Z

    .line 484
    iget-object v0, p0, Landroid/support/v7/internal/view/f;->a:Landroid/view/Menu;

    iget v1, p0, Landroid/support/v7/internal/view/f;->b:I

    iget v2, p0, Landroid/support/v7/internal/view/f;->i:I

    iget v3, p0, Landroid/support/v7/internal/view/f;->j:I

    iget-object v4, p0, Landroid/support/v7/internal/view/f;->k:Ljava/lang/CharSequence;

    invoke-interface {v0, v1, v2, v3, v4}, Landroid/view/Menu;->addSubMenu(IIILjava/lang/CharSequence;)Landroid/view/SubMenu;

    move-result-object v0

    .line 485
    invoke-interface {v0}, Landroid/view/SubMenu;->getItem()Landroid/view/MenuItem;

    move-result-object v1

    invoke-direct {p0, v1}, Landroid/support/v7/internal/view/f;->a(Landroid/view/MenuItem;)V

    .line 486
    return-object v0
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 490
    iget-boolean v0, p0, Landroid/support/v7/internal/view/f;->h:Z

    return v0
.end method
