.class public Landroid/support/v7/internal/widget/b;
.super Ljava/lang/Object;
.source "ProgressBarCompat.java"

# interfaces
.implements Landroid/support/v4/view/aC;


# instance fields
.field private a:Z

.field private b:I

.field private synthetic c:Landroid/support/v7/internal/widget/a;


# direct methods
.method protected constructor <init>(Landroid/support/v7/internal/widget/a;)V
    .locals 1

    .prologue
    .line 255
    iput-object p1, p0, Landroid/support/v7/internal/widget/b;->c:Landroid/support/v7/internal/widget/a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 256
    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/support/v7/internal/widget/b;->a:Z

    return-void
.end method


# virtual methods
.method public final a(Landroid/support/v4/view/ar;I)Landroid/support/v7/internal/widget/b;
    .locals 1

    .prologue
    .line 261
    iget-object v0, p0, Landroid/support/v7/internal/widget/b;->c:Landroid/support/v7/internal/widget/a;

    iput-object p1, v0, Landroid/support/v7/internal/widget/a;->e:Landroid/support/v4/view/ar;

    .line 262
    iput p2, p0, Landroid/support/v7/internal/widget/b;->b:I

    .line 263
    return-object p0
.end method

.method public final a(Landroid/view/View;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 268
    iget-object v0, p0, Landroid/support/v7/internal/widget/b;->c:Landroid/support/v7/internal/widget/a;

    invoke-virtual {v0, v1}, Landroid/support/v7/internal/widget/a;->setVisibility(I)V

    .line 269
    iput-boolean v1, p0, Landroid/support/v7/internal/widget/b;->a:Z

    .line 270
    return-void
.end method

.method public final b(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 274
    iget-boolean v0, p0, Landroid/support/v7/internal/widget/b;->a:Z

    if-eqz v0, :cond_0

    .line 279
    :goto_0
    return-void

    .line 276
    :cond_0
    iget-object v0, p0, Landroid/support/v7/internal/widget/b;->c:Landroid/support/v7/internal/widget/a;

    const/4 v1, 0x0

    iput-object v1, v0, Landroid/support/v7/internal/widget/a;->e:Landroid/support/v4/view/ar;

    .line 277
    iget-object v0, p0, Landroid/support/v7/internal/widget/b;->c:Landroid/support/v7/internal/widget/a;

    iget v1, p0, Landroid/support/v7/internal/widget/b;->b:I

    invoke-virtual {v0, v1}, Landroid/support/v7/internal/widget/a;->setVisibility(I)V

    .line 278
    iget-object v0, p0, Landroid/support/v7/internal/widget/b;->c:Landroid/support/v7/internal/widget/a;

    goto :goto_0
.end method

.method public final c(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 285
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v7/internal/widget/b;->a:Z

    .line 286
    return-void
.end method
