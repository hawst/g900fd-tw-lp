.class public Landroid/support/v7/internal/widget/V;
.super Ljava/lang/Object;
.source "TintManager.java"


# static fields
.field static final a:Landroid/graphics/PorterDuff$Mode;

.field private static final b:Landroid/support/v7/internal/widget/W;

.field private static final c:[I

.field private static final d:[I

.field private static final e:[I

.field private static final f:[I

.field private static final g:[I


# instance fields
.field private final h:Landroid/content/Context;

.field private final i:Landroid/content/res/Resources;

.field private final j:Landroid/util/TypedValue;

.field private k:Landroid/content/res/ColorStateList;

.field private l:Landroid/content/res/ColorStateList;

.field private m:Landroid/content/res/ColorStateList;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x6

    const/4 v1, 0x3

    .line 37
    const-class v0, Landroid/support/v7/internal/widget/V;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    .line 40
    sget-object v0, Landroid/graphics/PorterDuff$Mode;->SRC_IN:Landroid/graphics/PorterDuff$Mode;

    sput-object v0, Landroid/support/v7/internal/widget/V;->a:Landroid/graphics/PorterDuff$Mode;

    .line 42
    new-instance v0, Landroid/support/v7/internal/widget/W;

    invoke-direct {v0, v2}, Landroid/support/v7/internal/widget/W;-><init>(I)V

    sput-object v0, Landroid/support/v7/internal/widget/V;->b:Landroid/support/v7/internal/widget/W;

    .line 48
    const/16 v0, 0xe

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Landroid/support/v7/internal/widget/V;->c:[I

    .line 69
    new-array v0, v1, [I

    fill-array-data v0, :array_1

    sput-object v0, Landroid/support/v7/internal/widget/V;->d:[I

    .line 79
    new-array v0, v1, [I

    fill-array-data v0, :array_2

    sput-object v0, Landroid/support/v7/internal/widget/V;->e:[I

    .line 89
    new-array v0, v2, [I

    fill-array-data v0, :array_3

    sput-object v0, Landroid/support/v7/internal/widget/V;->f:[I

    .line 102
    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v1, 0x0

    const v2, 0x7f02000b

    aput v2, v0, v1

    sput-object v0, Landroid/support/v7/internal/widget/V;->g:[I

    return-void

    .line 48
    :array_0
    .array-data 4
        0x7f02000e
        0x7f020011
        0x7f020018
        0x7f020010
        0x7f02000f
        0x7f020017
        0x7f020012
        0x7f020013
        0x7f020016
        0x7f020015
        0x7f020014
        0x7f020019
        0x7f020031
        0x7f02002f
    .end array-data

    .line 69
    :array_1
    .array-data 4
        0x7f02002e
        0x7f020030
        0x7f02000c
    .end array-data

    .line 79
    :array_2
    .array-data 4
        0x7f020028
        0x7f02000a
        0x7f020027
    .end array-data

    .line 89
    :array_3
    .array-data 4
        0x7f02000d
        0x7f02002c
        0x7f020032
        0x7f020029
        0x7f020002
        0x7f020005
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 126
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 127
    iput-object p1, p0, Landroid/support/v7/internal/widget/V;->h:Landroid/content/Context;

    .line 128
    new-instance v0, Landroid/support/v7/internal/widget/Y;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-direct {v0, v1, p0}, Landroid/support/v7/internal/widget/Y;-><init>(Landroid/content/res/Resources;Landroid/support/v7/internal/widget/V;)V

    iput-object v0, p0, Landroid/support/v7/internal/widget/V;->i:Landroid/content/res/Resources;

    .line 129
    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    iput-object v0, p0, Landroid/support/v7/internal/widget/V;->j:Landroid/util/TypedValue;

    .line 130
    return-void
.end method

.method private a(IF)I
    .locals 3

    .prologue
    .line 332
    invoke-direct {p0, p1}, Landroid/support/v7/internal/widget/V;->b(I)I

    move-result v0

    .line 333
    invoke-static {v0}, Landroid/graphics/Color;->alpha(I)I

    move-result v1

    .line 336
    const v2, 0xffffff

    and-int/2addr v0, v2

    int-to-float v1, v1

    mul-float/2addr v1, p2

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    shl-int/lit8 v1, v1, 0x18

    or-int/2addr v0, v1

    return v0
.end method

.method public static a(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 119
    sget-object v0, Landroid/support/v7/internal/widget/V;->e:[I

    invoke-static {v0, p1}, Landroid/support/v7/internal/widget/V;->a([II)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Landroid/support/v7/internal/widget/V;->c:[I

    invoke-static {v0, p1}, Landroid/support/v7/internal/widget/V;->a([II)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Landroid/support/v7/internal/widget/V;->d:[I

    invoke-static {v0, p1}, Landroid/support/v7/internal/widget/V;->a([II)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Landroid/support/v7/internal/widget/V;->f:[I

    invoke-static {v0, p1}, Landroid/support/v7/internal/widget/V;->a([II)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Landroid/support/v7/internal/widget/V;->g:[I

    invoke-static {v0, p1}, Landroid/support/v7/internal/widget/V;->a([II)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_2

    .line 120
    new-instance v0, Landroid/support/v7/internal/widget/V;

    invoke-direct {v0, p0}, Landroid/support/v7/internal/widget/V;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, p1}, Landroid/support/v7/internal/widget/V;->a(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 122
    :goto_1
    return-object v0

    .line 119
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 122
    :cond_2
    invoke-static {p0, p1}, Landroid/support/v4/content/a;->a(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_1
.end method

.method private static a([II)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 204
    array-length v2, p0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    aget v3, p0, v1

    .line 205
    if-ne v3, p1, :cond_1

    .line 206
    const/4 v0, 0x1

    .line 209
    :cond_0
    return v0

    .line 204
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method private b(I)I
    .locals 3

    .prologue
    .line 320
    iget-object v0, p0, Landroid/support/v7/internal/widget/V;->h:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v0

    iget-object v1, p0, Landroid/support/v7/internal/widget/V;->j:Landroid/util/TypedValue;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v1, v2}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 321
    iget-object v0, p0, Landroid/support/v7/internal/widget/V;->j:Landroid/util/TypedValue;

    iget v0, v0, Landroid/util/TypedValue;->type:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_0

    iget-object v0, p0, Landroid/support/v7/internal/widget/V;->j:Landroid/util/TypedValue;

    iget v0, v0, Landroid/util/TypedValue;->type:I

    const/16 v1, 0x1f

    if-gt v0, v1, :cond_0

    .line 323
    iget-object v0, p0, Landroid/support/v7/internal/widget/V;->j:Landroid/util/TypedValue;

    iget v0, v0, Landroid/util/TypedValue;->data:I

    .line 328
    :goto_0
    return v0

    .line 324
    :cond_0
    iget-object v0, p0, Landroid/support/v7/internal/widget/V;->j:Landroid/util/TypedValue;

    iget v0, v0, Landroid/util/TypedValue;->type:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_1

    .line 325
    iget-object v0, p0, Landroid/support/v7/internal/widget/V;->i:Landroid/content/res/Resources;

    iget-object v1, p0, Landroid/support/v7/internal/widget/V;->j:Landroid/util/TypedValue;

    iget v1, v1, Landroid/util/TypedValue;->resourceId:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    goto :goto_0

    .line 328
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private c(I)I
    .locals 4

    .prologue
    .line 341
    iget-object v0, p0, Landroid/support/v7/internal/widget/V;->h:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v0

    const v1, 0x1010033

    iget-object v2, p0, Landroid/support/v7/internal/widget/V;->j:Landroid/util/TypedValue;

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    .line 342
    iget-object v0, p0, Landroid/support/v7/internal/widget/V;->j:Landroid/util/TypedValue;

    invoke-virtual {v0}, Landroid/util/TypedValue;->getFloat()F

    move-result v0

    .line 344
    invoke-direct {p0, p1, v0}, Landroid/support/v7/internal/widget/V;->a(IF)I

    move-result v0

    return v0
.end method


# virtual methods
.method public final a(I)Landroid/graphics/drawable/Drawable;
    .locals 12

    .prologue
    const v7, -0x101009e

    const/4 v11, 0x3

    const/4 v8, 0x2

    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 133
    iget-object v0, p0, Landroid/support/v7/internal/widget/V;->h:Landroid/content/Context;

    invoke-static {v0, p1}, Landroid/support/v4/content/a;->a(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 135
    if-eqz v1, :cond_7

    .line 136
    sget-object v0, Landroid/support/v7/internal/widget/V;->f:[I

    invoke-static {v0, p1}, Landroid/support/v7/internal/widget/V;->a([II)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 137
    new-instance v0, Landroid/support/v7/internal/widget/T;

    iget-object v2, p0, Landroid/support/v7/internal/widget/V;->k:Landroid/content/res/ColorStateList;

    if-nez v2, :cond_0

    const v2, 0x7f010093

    invoke-direct {p0, v2}, Landroid/support/v7/internal/widget/V;->b(I)I

    move-result v2

    const v3, 0x7f010094

    invoke-direct {p0, v3}, Landroid/support/v7/internal/widget/V;->b(I)I

    move-result v3

    const/4 v4, 0x7

    new-array v4, v4, [[I

    const/4 v5, 0x7

    new-array v5, v5, [I

    new-array v6, v10, [I

    aput v7, v6, v9

    aput-object v6, v4, v9

    const v6, 0x7f010093

    invoke-direct {p0, v6}, Landroid/support/v7/internal/widget/V;->c(I)I

    move-result v6

    aput v6, v5, v9

    new-array v6, v10, [I

    const v7, 0x101009c

    aput v7, v6, v9

    aput-object v6, v4, v10

    aput v3, v5, v10

    new-array v6, v10, [I

    const v7, 0x10102fe

    aput v7, v6, v9

    aput-object v6, v4, v8

    aput v3, v5, v8

    new-array v6, v10, [I

    const v7, 0x10100a7

    aput v7, v6, v9

    aput-object v6, v4, v11

    aput v3, v5, v11

    const/4 v6, 0x4

    new-array v7, v10, [I

    const v8, 0x10100a0

    aput v8, v7, v9

    aput-object v7, v4, v6

    const/4 v6, 0x4

    aput v3, v5, v6

    const/4 v6, 0x5

    new-array v7, v10, [I

    const v8, 0x10100a1

    aput v8, v7, v9

    aput-object v7, v4, v6

    const/4 v6, 0x5

    aput v3, v5, v6

    const/4 v3, 0x6

    new-array v6, v9, [I

    aput-object v6, v4, v3

    const/4 v3, 0x6

    aput v2, v5, v3

    new-instance v2, Landroid/content/res/ColorStateList;

    invoke-direct {v2, v4, v5}, Landroid/content/res/ColorStateList;-><init>([[I[I)V

    iput-object v2, p0, Landroid/support/v7/internal/widget/V;->k:Landroid/content/res/ColorStateList;

    :cond_0
    iget-object v2, p0, Landroid/support/v7/internal/widget/V;->k:Landroid/content/res/ColorStateList;

    invoke-direct {v0, v1, v2}, Landroid/support/v7/internal/widget/T;-><init>(Landroid/graphics/drawable/Drawable;Landroid/content/res/ColorStateList;)V

    .line 149
    :goto_0
    return-object v0

    .line 138
    :cond_1
    const v0, 0x7f02002b

    if-ne p1, v0, :cond_3

    .line 139
    new-instance v0, Landroid/support/v7/internal/widget/T;

    iget-object v2, p0, Landroid/support/v7/internal/widget/V;->m:Landroid/content/res/ColorStateList;

    if-nez v2, :cond_2

    new-array v2, v11, [[I

    new-array v3, v11, [I

    new-array v4, v10, [I

    aput v7, v4, v9

    aput-object v4, v2, v9

    const v4, 0x1010030

    const v5, 0x3dcccccd    # 0.1f

    invoke-direct {p0, v4, v5}, Landroid/support/v7/internal/widget/V;->a(IF)I

    move-result v4

    aput v4, v3, v9

    new-array v4, v10, [I

    const v5, 0x10100a0

    aput v5, v4, v9

    aput-object v4, v2, v10

    const v4, 0x7f010094

    const v5, 0x3e99999a    # 0.3f

    invoke-direct {p0, v4, v5}, Landroid/support/v7/internal/widget/V;->a(IF)I

    move-result v4

    aput v4, v3, v10

    new-array v4, v9, [I

    aput-object v4, v2, v8

    const v4, 0x1010030

    const v5, 0x3e99999a    # 0.3f

    invoke-direct {p0, v4, v5}, Landroid/support/v7/internal/widget/V;->a(IF)I

    move-result v4

    aput v4, v3, v8

    new-instance v4, Landroid/content/res/ColorStateList;

    invoke-direct {v4, v2, v3}, Landroid/content/res/ColorStateList;-><init>([[I[I)V

    iput-object v4, p0, Landroid/support/v7/internal/widget/V;->m:Landroid/content/res/ColorStateList;

    :cond_2
    iget-object v2, p0, Landroid/support/v7/internal/widget/V;->m:Landroid/content/res/ColorStateList;

    invoke-direct {v0, v1, v2}, Landroid/support/v7/internal/widget/T;-><init>(Landroid/graphics/drawable/Drawable;Landroid/content/res/ColorStateList;)V

    goto :goto_0

    .line 140
    :cond_3
    const v0, 0x7f02002a

    if-ne p1, v0, :cond_5

    .line 141
    new-instance v0, Landroid/support/v7/internal/widget/T;

    iget-object v2, p0, Landroid/support/v7/internal/widget/V;->l:Landroid/content/res/ColorStateList;

    if-nez v2, :cond_4

    new-array v2, v11, [[I

    new-array v3, v11, [I

    new-array v4, v10, [I

    aput v7, v4, v9

    aput-object v4, v2, v9

    const v4, 0x7f010097

    invoke-direct {p0, v4}, Landroid/support/v7/internal/widget/V;->c(I)I

    move-result v4

    aput v4, v3, v9

    new-array v4, v10, [I

    const v5, 0x10100a0

    aput v5, v4, v9

    aput-object v4, v2, v10

    const v4, 0x7f010094

    invoke-direct {p0, v4}, Landroid/support/v7/internal/widget/V;->b(I)I

    move-result v4

    aput v4, v3, v10

    new-array v4, v9, [I

    aput-object v4, v2, v8

    const v4, 0x7f010097

    invoke-direct {p0, v4}, Landroid/support/v7/internal/widget/V;->b(I)I

    move-result v4

    aput v4, v3, v8

    new-instance v4, Landroid/content/res/ColorStateList;

    invoke-direct {v4, v2, v3}, Landroid/content/res/ColorStateList;-><init>([[I[I)V

    iput-object v4, p0, Landroid/support/v7/internal/widget/V;->l:Landroid/content/res/ColorStateList;

    :cond_4
    iget-object v2, p0, Landroid/support/v7/internal/widget/V;->l:Landroid/content/res/ColorStateList;

    sget-object v3, Landroid/graphics/PorterDuff$Mode;->MULTIPLY:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v0, v1, v2, v3}, Landroid/support/v7/internal/widget/T;-><init>(Landroid/graphics/drawable/Drawable;Landroid/content/res/ColorStateList;Landroid/graphics/PorterDuff$Mode;)V

    goto/16 :goto_0

    .line 143
    :cond_5
    sget-object v0, Landroid/support/v7/internal/widget/V;->g:[I

    invoke-static {v0, p1}, Landroid/support/v7/internal/widget/V;->a([II)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 144
    iget-object v0, p0, Landroid/support/v7/internal/widget/V;->i:Landroid/content/res/Resources;

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto/16 :goto_0

    .line 146
    :cond_6
    invoke-virtual {p0, p1, v1}, Landroid/support/v7/internal/widget/V;->a(ILandroid/graphics/drawable/Drawable;)V

    :cond_7
    move-object v0, v1

    goto/16 :goto_0
.end method

.method final a(ILandroid/graphics/drawable/Drawable;)V
    .locals 7

    .prologue
    const/4 v0, 0x0

    const/4 v3, -0x1

    const/4 v2, 0x1

    .line 153
    const/4 v4, 0x0

    .line 158
    sget-object v1, Landroid/support/v7/internal/widget/V;->c:[I

    invoke-static {v1, p1}, Landroid/support/v7/internal/widget/V;->a([II)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 159
    const v0, 0x7f010093

    move v1, v0

    move-object v0, v4

    move v4, v2

    move v2, v3

    .line 174
    :goto_0
    if-eqz v4, :cond_1

    .line 175
    if-nez v0, :cond_0

    .line 176
    sget-object v0, Landroid/support/v7/internal/widget/V;->a:Landroid/graphics/PorterDuff$Mode;

    .line 178
    :cond_0
    invoke-direct {p0, v1}, Landroid/support/v7/internal/widget/V;->b(I)I

    move-result v4

    .line 181
    sget-object v1, Landroid/support/v7/internal/widget/V;->b:Landroid/support/v7/internal/widget/W;

    invoke-virtual {v1, v4, v0}, Landroid/support/v7/internal/widget/W;->a(ILandroid/graphics/PorterDuff$Mode;)Landroid/graphics/PorterDuffColorFilter;

    move-result-object v1

    .line 183
    if-nez v1, :cond_5

    .line 185
    new-instance v1, Landroid/graphics/PorterDuffColorFilter;

    invoke-direct {v1, v4, v0}, Landroid/graphics/PorterDuffColorFilter;-><init>(ILandroid/graphics/PorterDuff$Mode;)V

    .line 186
    sget-object v5, Landroid/support/v7/internal/widget/V;->b:Landroid/support/v7/internal/widget/W;

    invoke-virtual {v5, v4, v0, v1}, Landroid/support/v7/internal/widget/W;->a(ILandroid/graphics/PorterDuff$Mode;Landroid/graphics/PorterDuffColorFilter;)Landroid/graphics/PorterDuffColorFilter;

    move-object v0, v1

    .line 190
    :goto_1
    invoke-virtual {p2, v0}, Landroid/graphics/drawable/Drawable;->setColorFilter(Landroid/graphics/ColorFilter;)V

    .line 192
    if-eq v2, v3, :cond_1

    .line 193
    invoke-virtual {p2, v2}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 201
    :cond_1
    return-void

    .line 161
    :cond_2
    sget-object v1, Landroid/support/v7/internal/widget/V;->d:[I

    invoke-static {v1, p1}, Landroid/support/v7/internal/widget/V;->a([II)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 162
    const v0, 0x7f010094

    move v1, v0

    move-object v0, v4

    move v4, v2

    move v2, v3

    .line 163
    goto :goto_0

    .line 164
    :cond_3
    sget-object v1, Landroid/support/v7/internal/widget/V;->e:[I

    invoke-static {v1, p1}, Landroid/support/v7/internal/widget/V;->a([II)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 165
    const v0, 0x1010031

    .line 167
    sget-object v1, Landroid/graphics/PorterDuff$Mode;->MULTIPLY:Landroid/graphics/PorterDuff$Mode;

    move v4, v2

    move v2, v3

    move v6, v0

    move-object v0, v1

    move v1, v6

    goto :goto_0

    .line 168
    :cond_4
    const v1, 0x7f02001c

    if-ne p1, v1, :cond_6

    .line 169
    const v1, 0x1010030

    .line 171
    const v0, 0x42233333    # 40.8f

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    move v6, v0

    move-object v0, v4

    move v4, v2

    move v2, v6

    goto :goto_0

    :cond_5
    move-object v0, v1

    goto :goto_1

    :cond_6
    move v2, v3

    move v1, v0

    move v6, v0

    move-object v0, v4

    move v4, v6

    goto :goto_0
.end method
