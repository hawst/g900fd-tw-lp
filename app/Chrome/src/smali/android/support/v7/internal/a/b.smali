.class public Landroid/support/v7/internal/a/b;
.super Landroid/support/v7/app/ActionBar;
.source "WindowDecorActionBar.java"

# interfaces
.implements Landroid/support/v7/internal/widget/l;


# static fields
.field private static final e:Z


# instance fields
.field private A:Landroid/support/v4/view/aC;

.field private B:Landroid/support/v4/view/aE;

.field a:Landroid/support/v7/internal/a/f;

.field b:Landroid/support/v7/b/a;

.field c:Landroid/support/v7/b/b;

.field d:Z

.field private f:Landroid/content/Context;

.field private g:Landroid/content/Context;

.field private h:Landroid/support/v7/internal/widget/ActionBarOverlayLayout;

.field private i:Landroid/support/v7/internal/widget/ActionBarContainer;

.field private j:Landroid/support/v7/internal/widget/u;

.field private k:Landroid/support/v7/internal/widget/ActionBarContextView;

.field private l:Landroid/support/v7/internal/widget/ActionBarContainer;

.field private m:Landroid/view/View;

.field private n:Z

.field private o:Z

.field private p:Ljava/util/ArrayList;

.field private q:I

.field private r:Z

.field private s:I

.field private t:Z

.field private u:Z

.field private v:Z

.field private w:Z

.field private x:Landroid/support/v7/internal/view/g;

.field private y:Z

.field private z:Landroid/support/v4/view/aC;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 75
    const-class v0, Landroid/support/v7/internal/a/b;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    .line 82
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Landroid/support/v7/internal/a/b;->e:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Landroid/support/v7/app/c;Z)V
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 172
    invoke-direct {p0}, Landroid/support/v7/app/ActionBar;-><init>()V

    .line 97
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 100
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/support/v7/internal/a/b;->p:Ljava/util/ArrayList;

    .line 120
    iput v2, p0, Landroid/support/v7/internal/a/b;->s:I

    .line 122
    iput-boolean v1, p0, Landroid/support/v7/internal/a/b;->t:Z

    .line 127
    iput-boolean v1, p0, Landroid/support/v7/internal/a/b;->w:Z

    .line 135
    new-instance v0, Landroid/support/v7/internal/a/c;

    invoke-direct {v0, p0}, Landroid/support/v7/internal/a/c;-><init>(Landroid/support/v7/internal/a/b;)V

    iput-object v0, p0, Landroid/support/v7/internal/a/b;->z:Landroid/support/v4/view/aC;

    .line 155
    new-instance v0, Landroid/support/v7/internal/a/d;

    invoke-direct {v0, p0}, Landroid/support/v7/internal/a/d;-><init>(Landroid/support/v7/internal/a/b;)V

    iput-object v0, p0, Landroid/support/v7/internal/a/b;->A:Landroid/support/v4/view/aC;

    .line 163
    new-instance v0, Landroid/support/v7/internal/a/e;

    invoke-direct {v0, p0}, Landroid/support/v7/internal/a/e;-><init>(Landroid/support/v7/internal/a/b;)V

    iput-object v0, p0, Landroid/support/v7/internal/a/b;->B:Landroid/support/v4/view/aE;

    .line 173
    invoke-virtual {p1}, Landroid/support/v7/app/c;->getWindow()Landroid/view/Window;

    move-result-object v0

    .line 175
    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v3

    .line 176
    const v0, 0x7f0f0069

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/internal/widget/ActionBarOverlayLayout;

    iput-object v0, p0, Landroid/support/v7/internal/a/b;->h:Landroid/support/v7/internal/widget/ActionBarOverlayLayout;

    iget-object v0, p0, Landroid/support/v7/internal/a/b;->h:Landroid/support/v7/internal/widget/ActionBarOverlayLayout;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/internal/a/b;->h:Landroid/support/v7/internal/widget/ActionBarOverlayLayout;

    invoke-virtual {v0, p0}, Landroid/support/v7/internal/widget/ActionBarOverlayLayout;->a(Landroid/support/v7/internal/widget/l;)V

    :cond_0
    const v0, 0x7f0f006b

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    instance-of v4, v0, Landroid/support/v7/internal/widget/u;

    if-eqz v4, :cond_2

    check-cast v0, Landroid/support/v7/internal/widget/u;

    :goto_0
    iput-object v0, p0, Landroid/support/v7/internal/a/b;->j:Landroid/support/v7/internal/widget/u;

    const v0, 0x7f0f006c

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/internal/widget/ActionBarContextView;

    iput-object v0, p0, Landroid/support/v7/internal/a/b;->k:Landroid/support/v7/internal/widget/ActionBarContextView;

    const v0, 0x7f0f006a

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/internal/widget/ActionBarContainer;

    iput-object v0, p0, Landroid/support/v7/internal/a/b;->i:Landroid/support/v7/internal/widget/ActionBarContainer;

    const v0, 0x7f0f0004

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/internal/widget/ActionBarContainer;

    iput-object v0, p0, Landroid/support/v7/internal/a/b;->l:Landroid/support/v7/internal/widget/ActionBarContainer;

    iget-object v0, p0, Landroid/support/v7/internal/a/b;->j:Landroid/support/v7/internal/widget/u;

    if-eqz v0, :cond_1

    iget-object v0, p0, Landroid/support/v7/internal/a/b;->k:Landroid/support/v7/internal/widget/ActionBarContextView;

    if-eqz v0, :cond_1

    iget-object v0, p0, Landroid/support/v7/internal/a/b;->i:Landroid/support/v7/internal/widget/ActionBarContainer;

    if-nez v0, :cond_4

    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " can only be used with a compatible window decor layout"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    instance-of v4, v0, Landroid/support/v7/widget/Toolbar;

    if-eqz v4, :cond_3

    check-cast v0, Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v0}, Landroid/support/v7/widget/Toolbar;->getWrapper()Landroid/support/v7/internal/widget/u;

    move-result-object v0

    goto :goto_0

    :cond_3
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "Can\'t make a decor toolbar out of "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_4
    iget-object v0, p0, Landroid/support/v7/internal/a/b;->j:Landroid/support/v7/internal/widget/u;

    invoke-interface {v0}, Landroid/support/v7/internal/widget/u;->b()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v7/internal/a/b;->f:Landroid/content/Context;

    iget-object v0, p0, Landroid/support/v7/internal/a/b;->j:Landroid/support/v7/internal/widget/u;

    iput v2, p0, Landroid/support/v7/internal/a/b;->q:I

    iget-object v0, p0, Landroid/support/v7/internal/a/b;->j:Landroid/support/v7/internal/widget/u;

    invoke-interface {v0}, Landroid/support/v7/internal/widget/u;->n()I

    move-result v0

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_7

    move v0, v1

    :goto_1
    if-eqz v0, :cond_5

    iput-boolean v1, p0, Landroid/support/v7/internal/a/b;->n:Z

    :cond_5
    iget-object v4, p0, Landroid/support/v7/internal/a/b;->f:Landroid/content/Context;

    invoke-static {v4}, Landroid/support/v4/d/a;->get$30f4eda9(Landroid/content/Context;)Landroid/support/v4/d/a;

    move-result-object v4

    invoke-virtual {v4}, Landroid/support/v4/d/a;->enableHomeButtonByDefault()Z

    move-result v5

    if-nez v5, :cond_6

    if-eqz v0, :cond_6

    :cond_6
    iget-object v0, p0, Landroid/support/v7/internal/a/b;->j:Landroid/support/v7/internal/widget/u;

    invoke-virtual {v4}, Landroid/support/v4/d/a;->hasEmbeddedTabs()Z

    move-result v0

    invoke-direct {p0, v0}, Landroid/support/v7/internal/a/b;->f(Z)V

    iget-object v0, p0, Landroid/support/v7/internal/a/b;->f:Landroid/content/Context;

    const/4 v4, 0x0

    sget-object v5, Landroid/support/v7/a/a;->a:[I

    const v6, 0x7f010053

    invoke-virtual {v0, v4, v5, v6, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    const/16 v4, 0x14

    invoke-virtual {v0, v4, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v4

    if-eqz v4, :cond_9

    iget-object v4, p0, Landroid/support/v7/internal/a/b;->h:Landroid/support/v7/internal/widget/ActionBarOverlayLayout;

    invoke-virtual {v4}, Landroid/support/v7/internal/widget/ActionBarOverlayLayout;->a()Z

    move-result v4

    if-nez v4, :cond_8

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "Action bar must be in overlay mode (Window.FEATURE_OVERLAY_ACTION_BAR) to enable hide on content scroll"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_7
    move v0, v2

    goto :goto_1

    :cond_8
    iput-boolean v1, p0, Landroid/support/v7/internal/a/b;->d:Z

    iget-object v4, p0, Landroid/support/v7/internal/a/b;->h:Landroid/support/v7/internal/widget/ActionBarOverlayLayout;

    invoke-virtual {v4, v1}, Landroid/support/v7/internal/widget/ActionBarOverlayLayout;->b(Z)V

    :cond_9
    const/16 v1, 0x19

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    if-eqz v1, :cond_a

    int-to-float v1, v1

    iget-object v2, p0, Landroid/support/v7/internal/a/b;->i:Landroid/support/v7/internal/widget/ActionBarContainer;

    invoke-static {v2, v1}, Landroid/support/v4/view/ViewCompat;->e(Landroid/view/View;F)V

    iget-object v2, p0, Landroid/support/v7/internal/a/b;->l:Landroid/support/v7/internal/widget/ActionBarContainer;

    if-eqz v2, :cond_a

    iget-object v2, p0, Landroid/support/v7/internal/a/b;->l:Landroid/support/v7/internal/widget/ActionBarContainer;

    invoke-static {v2, v1}, Landroid/support/v4/view/ViewCompat;->e(Landroid/view/View;F)V

    :cond_a
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 177
    if-nez p2, :cond_b

    .line 178
    const v0, 0x1020002

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v7/internal/a/b;->m:Landroid/view/View;

    .line 180
    :cond_b
    return-void
.end method

.method static synthetic a(Landroid/support/v7/internal/a/b;Landroid/support/v7/internal/view/g;)Landroid/support/v7/internal/view/g;
    .locals 1

    .prologue
    .line 75
    const/4 v0, 0x0

    iput-object v0, p0, Landroid/support/v7/internal/a/b;->x:Landroid/support/v7/internal/view/g;

    return-object v0
.end method

.method static synthetic a(Landroid/support/v7/internal/a/b;)Z
    .locals 1

    .prologue
    .line 75
    iget-boolean v0, p0, Landroid/support/v7/internal/a/b;->t:Z

    return v0
.end method

.method static synthetic a(ZZZ)Z
    .locals 1

    .prologue
    .line 75
    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Landroid/support/v7/internal/a/b;->b(ZZZ)Z

    move-result v0

    return v0
.end method

.method static synthetic b(Landroid/support/v7/internal/a/b;)Landroid/view/View;
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Landroid/support/v7/internal/a/b;->m:Landroid/view/View;

    return-object v0
.end method

.method private static b(ZZZ)Z
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 738
    if-eqz p2, :cond_1

    .line 743
    :cond_0
    :goto_0
    return v0

    .line 740
    :cond_1
    if-nez p0, :cond_2

    if-eqz p1, :cond_0

    .line 741
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic c(Landroid/support/v7/internal/a/b;)Landroid/support/v7/internal/widget/ActionBarContainer;
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Landroid/support/v7/internal/a/b;->i:Landroid/support/v7/internal/widget/ActionBarContainer;

    return-object v0
.end method

.method static synthetic d(Landroid/support/v7/internal/a/b;)Landroid/support/v7/internal/widget/ActionBarContainer;
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Landroid/support/v7/internal/a/b;->l:Landroid/support/v7/internal/widget/ActionBarContainer;

    return-object v0
.end method

.method static synthetic e(Landroid/support/v7/internal/a/b;)I
    .locals 1

    .prologue
    .line 75
    iget v0, p0, Landroid/support/v7/internal/a/b;->q:I

    return v0
.end method

.method static synthetic f(Landroid/support/v7/internal/a/b;)Landroid/support/v7/internal/widget/ActionBarOverlayLayout;
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Landroid/support/v7/internal/a/b;->h:Landroid/support/v7/internal/widget/ActionBarOverlayLayout;

    return-object v0
.end method

.method private f(Z)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v3, 0x0

    .line 271
    iput-boolean p1, p0, Landroid/support/v7/internal/a/b;->r:Z

    .line 273
    iget-boolean v0, p0, Landroid/support/v7/internal/a/b;->r:Z

    if-nez v0, :cond_0

    .line 274
    iget-object v0, p0, Landroid/support/v7/internal/a/b;->j:Landroid/support/v7/internal/widget/u;

    invoke-interface {v0, v3}, Landroid/support/v7/internal/widget/u;->a(Landroid/support/v7/internal/widget/E;)V

    .line 275
    iget-object v0, p0, Landroid/support/v7/internal/a/b;->i:Landroid/support/v7/internal/widget/ActionBarContainer;

    invoke-virtual {v0, v3}, Landroid/support/v7/internal/widget/ActionBarContainer;->a(Landroid/support/v7/internal/widget/E;)V

    .line 280
    :goto_0
    iget-object v0, p0, Landroid/support/v7/internal/a/b;->j:Landroid/support/v7/internal/widget/u;

    invoke-interface {v0}, Landroid/support/v7/internal/widget/u;->o()I

    move-result v0

    const/4 v3, 0x2

    if-ne v0, v3, :cond_1

    move v0, v1

    .line 281
    :goto_1
    iget-object v4, p0, Landroid/support/v7/internal/a/b;->j:Landroid/support/v7/internal/widget/u;

    iget-boolean v3, p0, Landroid/support/v7/internal/a/b;->r:Z

    if-nez v3, :cond_2

    if-eqz v0, :cond_2

    move v3, v1

    :goto_2
    invoke-interface {v4, v3}, Landroid/support/v7/internal/widget/u;->a(Z)V

    .line 292
    iget-object v3, p0, Landroid/support/v7/internal/a/b;->h:Landroid/support/v7/internal/widget/ActionBarOverlayLayout;

    iget-boolean v4, p0, Landroid/support/v7/internal/a/b;->r:Z

    if-nez v4, :cond_3

    if-eqz v0, :cond_3

    :goto_3
    invoke-virtual {v3, v1}, Landroid/support/v7/internal/widget/ActionBarOverlayLayout;->a(Z)V

    .line 293
    return-void

    .line 277
    :cond_0
    iget-object v0, p0, Landroid/support/v7/internal/a/b;->i:Landroid/support/v7/internal/widget/ActionBarContainer;

    invoke-virtual {v0, v3}, Landroid/support/v7/internal/widget/ActionBarContainer;->a(Landroid/support/v7/internal/widget/E;)V

    .line 278
    iget-object v0, p0, Landroid/support/v7/internal/a/b;->j:Landroid/support/v7/internal/widget/u;

    invoke-interface {v0, v3}, Landroid/support/v7/internal/widget/u;->a(Landroid/support/v7/internal/widget/E;)V

    goto :goto_0

    :cond_1
    move v0, v2

    .line 280
    goto :goto_1

    :cond_2
    move v3, v2

    .line 281
    goto :goto_2

    :cond_3
    move v1, v2

    .line 292
    goto :goto_3
.end method

.method private g(Z)V
    .locals 8

    .prologue
    const/4 v2, 0x2

    const/high16 v7, 0x3f800000    # 1.0f

    const/4 v6, 0x0

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 749
    iget-boolean v0, p0, Landroid/support/v7/internal/a/b;->u:Z

    iget-boolean v1, p0, Landroid/support/v7/internal/a/b;->v:Z

    invoke-static {v6, v0, v1}, Landroid/support/v7/internal/a/b;->b(ZZZ)Z

    move-result v0

    .line 752
    if-eqz v0, :cond_9

    .line 753
    iget-boolean v0, p0, Landroid/support/v7/internal/a/b;->w:Z

    if-nez v0, :cond_5

    .line 754
    iput-boolean v5, p0, Landroid/support/v7/internal/a/b;->w:Z

    .line 755
    iget-object v0, p0, Landroid/support/v7/internal/a/b;->x:Landroid/support/v7/internal/view/g;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/internal/a/b;->x:Landroid/support/v7/internal/view/g;

    invoke-virtual {v0}, Landroid/support/v7/internal/view/g;->b()V

    :cond_0
    iget-object v0, p0, Landroid/support/v7/internal/a/b;->i:Landroid/support/v7/internal/widget/ActionBarContainer;

    invoke-virtual {v0, v6}, Landroid/support/v7/internal/widget/ActionBarContainer;->setVisibility(I)V

    iget v0, p0, Landroid/support/v7/internal/a/b;->s:I

    if-nez v0, :cond_6

    sget-boolean v0, Landroid/support/v7/internal/a/b;->e:Z

    if-eqz v0, :cond_6

    iget-boolean v0, p0, Landroid/support/v7/internal/a/b;->y:Z

    if-nez v0, :cond_1

    if-eqz p1, :cond_6

    :cond_1
    iget-object v0, p0, Landroid/support/v7/internal/a/b;->i:Landroid/support/v7/internal/widget/ActionBarContainer;

    invoke-static {v0, v4}, Landroid/support/v4/view/ViewCompat;->b(Landroid/view/View;F)V

    iget-object v0, p0, Landroid/support/v7/internal/a/b;->i:Landroid/support/v7/internal/widget/ActionBarContainer;

    invoke-virtual {v0}, Landroid/support/v7/internal/widget/ActionBarContainer;->getHeight()I

    move-result v0

    neg-int v0, v0

    int-to-float v0, v0

    if-eqz p1, :cond_2

    new-array v1, v2, [I

    fill-array-data v1, :array_0

    iget-object v2, p0, Landroid/support/v7/internal/a/b;->i:Landroid/support/v7/internal/widget/ActionBarContainer;

    invoke-virtual {v2, v1}, Landroid/support/v7/internal/widget/ActionBarContainer;->getLocationInWindow([I)V

    aget v1, v1, v5

    int-to-float v1, v1

    sub-float/2addr v0, v1

    :cond_2
    iget-object v1, p0, Landroid/support/v7/internal/a/b;->i:Landroid/support/v7/internal/widget/ActionBarContainer;

    invoke-static {v1, v0}, Landroid/support/v4/view/ViewCompat;->b(Landroid/view/View;F)V

    new-instance v1, Landroid/support/v7/internal/view/g;

    invoke-direct {v1}, Landroid/support/v7/internal/view/g;-><init>()V

    iget-object v2, p0, Landroid/support/v7/internal/a/b;->i:Landroid/support/v7/internal/widget/ActionBarContainer;

    invoke-static {v2}, Landroid/support/v4/view/ViewCompat;->k(Landroid/view/View;)Landroid/support/v4/view/ar;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/support/v4/view/ar;->c(F)Landroid/support/v4/view/ar;

    move-result-object v2

    iget-object v3, p0, Landroid/support/v7/internal/a/b;->B:Landroid/support/v4/view/aE;

    invoke-virtual {v2, v3}, Landroid/support/v4/view/ar;->a(Landroid/support/v4/view/aE;)Landroid/support/v4/view/ar;

    invoke-virtual {v1, v2}, Landroid/support/v7/internal/view/g;->a(Landroid/support/v4/view/ar;)Landroid/support/v7/internal/view/g;

    iget-boolean v2, p0, Landroid/support/v7/internal/a/b;->t:Z

    if-eqz v2, :cond_3

    iget-object v2, p0, Landroid/support/v7/internal/a/b;->m:Landroid/view/View;

    if-eqz v2, :cond_3

    iget-object v2, p0, Landroid/support/v7/internal/a/b;->m:Landroid/view/View;

    invoke-static {v2, v0}, Landroid/support/v4/view/ViewCompat;->b(Landroid/view/View;F)V

    iget-object v0, p0, Landroid/support/v7/internal/a/b;->m:Landroid/view/View;

    invoke-static {v0}, Landroid/support/v4/view/ViewCompat;->k(Landroid/view/View;)Landroid/support/v4/view/ar;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/support/v4/view/ar;->c(F)Landroid/support/v4/view/ar;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/support/v7/internal/view/g;->a(Landroid/support/v4/view/ar;)Landroid/support/v7/internal/view/g;

    :cond_3
    iget-object v0, p0, Landroid/support/v7/internal/a/b;->l:Landroid/support/v7/internal/widget/ActionBarContainer;

    if-eqz v0, :cond_4

    iget v0, p0, Landroid/support/v7/internal/a/b;->q:I

    if-ne v0, v5, :cond_4

    iget-object v0, p0, Landroid/support/v7/internal/a/b;->l:Landroid/support/v7/internal/widget/ActionBarContainer;

    iget-object v2, p0, Landroid/support/v7/internal/a/b;->l:Landroid/support/v7/internal/widget/ActionBarContainer;

    invoke-virtual {v2}, Landroid/support/v7/internal/widget/ActionBarContainer;->getHeight()I

    move-result v2

    int-to-float v2, v2

    invoke-static {v0, v2}, Landroid/support/v4/view/ViewCompat;->b(Landroid/view/View;F)V

    iget-object v0, p0, Landroid/support/v7/internal/a/b;->l:Landroid/support/v7/internal/widget/ActionBarContainer;

    invoke-virtual {v0, v6}, Landroid/support/v7/internal/widget/ActionBarContainer;->setVisibility(I)V

    iget-object v0, p0, Landroid/support/v7/internal/a/b;->l:Landroid/support/v7/internal/widget/ActionBarContainer;

    invoke-static {v0}, Landroid/support/v4/view/ViewCompat;->k(Landroid/view/View;)Landroid/support/v4/view/ar;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/support/v4/view/ar;->c(F)Landroid/support/v4/view/ar;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/support/v7/internal/view/g;->a(Landroid/support/v4/view/ar;)Landroid/support/v7/internal/view/g;

    :cond_4
    iget-object v0, p0, Landroid/support/v7/internal/a/b;->f:Landroid/content/Context;

    const v2, 0x10a0006

    invoke-static {v0, v2}, Landroid/view/animation/AnimationUtils;->loadInterpolator(Landroid/content/Context;I)Landroid/view/animation/Interpolator;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/support/v7/internal/view/g;->a(Landroid/view/animation/Interpolator;)Landroid/support/v7/internal/view/g;

    const-wide/16 v2, 0xfa

    invoke-virtual {v1, v2, v3}, Landroid/support/v7/internal/view/g;->a(J)Landroid/support/v7/internal/view/g;

    iget-object v0, p0, Landroid/support/v7/internal/a/b;->A:Landroid/support/v4/view/aC;

    invoke-virtual {v1, v0}, Landroid/support/v7/internal/view/g;->a(Landroid/support/v4/view/aC;)Landroid/support/v7/internal/view/g;

    iput-object v1, p0, Landroid/support/v7/internal/a/b;->x:Landroid/support/v7/internal/view/g;

    invoke-virtual {v1}, Landroid/support/v7/internal/view/g;->a()V

    :goto_0
    iget-object v0, p0, Landroid/support/v7/internal/a/b;->h:Landroid/support/v7/internal/widget/ActionBarOverlayLayout;

    if-eqz v0, :cond_5

    iget-object v0, p0, Landroid/support/v7/internal/a/b;->h:Landroid/support/v7/internal/widget/ActionBarOverlayLayout;

    invoke-static {v0}, Landroid/support/v4/view/ViewCompat;->m(Landroid/view/View;)V

    .line 763
    :cond_5
    :goto_1
    return-void

    .line 755
    :cond_6
    iget-object v0, p0, Landroid/support/v7/internal/a/b;->i:Landroid/support/v7/internal/widget/ActionBarContainer;

    invoke-static {v0, v7}, Landroid/support/v4/view/ViewCompat;->c(Landroid/view/View;F)V

    iget-object v0, p0, Landroid/support/v7/internal/a/b;->i:Landroid/support/v7/internal/widget/ActionBarContainer;

    invoke-static {v0, v4}, Landroid/support/v4/view/ViewCompat;->b(Landroid/view/View;F)V

    iget-boolean v0, p0, Landroid/support/v7/internal/a/b;->t:Z

    if-eqz v0, :cond_7

    iget-object v0, p0, Landroid/support/v7/internal/a/b;->m:Landroid/view/View;

    if-eqz v0, :cond_7

    iget-object v0, p0, Landroid/support/v7/internal/a/b;->m:Landroid/view/View;

    invoke-static {v0, v4}, Landroid/support/v4/view/ViewCompat;->b(Landroid/view/View;F)V

    :cond_7
    iget-object v0, p0, Landroid/support/v7/internal/a/b;->l:Landroid/support/v7/internal/widget/ActionBarContainer;

    if-eqz v0, :cond_8

    iget v0, p0, Landroid/support/v7/internal/a/b;->q:I

    if-ne v0, v5, :cond_8

    iget-object v0, p0, Landroid/support/v7/internal/a/b;->l:Landroid/support/v7/internal/widget/ActionBarContainer;

    invoke-static {v0, v7}, Landroid/support/v4/view/ViewCompat;->c(Landroid/view/View;F)V

    iget-object v0, p0, Landroid/support/v7/internal/a/b;->l:Landroid/support/v7/internal/widget/ActionBarContainer;

    invoke-static {v0, v4}, Landroid/support/v4/view/ViewCompat;->b(Landroid/view/View;F)V

    iget-object v0, p0, Landroid/support/v7/internal/a/b;->l:Landroid/support/v7/internal/widget/ActionBarContainer;

    invoke-virtual {v0, v6}, Landroid/support/v7/internal/widget/ActionBarContainer;->setVisibility(I)V

    :cond_8
    iget-object v0, p0, Landroid/support/v7/internal/a/b;->A:Landroid/support/v4/view/aC;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/support/v4/view/aC;->b(Landroid/view/View;)V

    goto :goto_0

    .line 758
    :cond_9
    iget-boolean v0, p0, Landroid/support/v7/internal/a/b;->w:Z

    if-eqz v0, :cond_5

    .line 759
    iput-boolean v6, p0, Landroid/support/v7/internal/a/b;->w:Z

    .line 760
    iget-object v0, p0, Landroid/support/v7/internal/a/b;->x:Landroid/support/v7/internal/view/g;

    if-eqz v0, :cond_a

    iget-object v0, p0, Landroid/support/v7/internal/a/b;->x:Landroid/support/v7/internal/view/g;

    invoke-virtual {v0}, Landroid/support/v7/internal/view/g;->b()V

    :cond_a
    iget v0, p0, Landroid/support/v7/internal/a/b;->s:I

    if-nez v0, :cond_f

    sget-boolean v0, Landroid/support/v7/internal/a/b;->e:Z

    if-eqz v0, :cond_f

    iget-boolean v0, p0, Landroid/support/v7/internal/a/b;->y:Z

    if-nez v0, :cond_b

    if-eqz p1, :cond_f

    :cond_b
    iget-object v0, p0, Landroid/support/v7/internal/a/b;->i:Landroid/support/v7/internal/widget/ActionBarContainer;

    invoke-static {v0, v7}, Landroid/support/v4/view/ViewCompat;->c(Landroid/view/View;F)V

    iget-object v0, p0, Landroid/support/v7/internal/a/b;->i:Landroid/support/v7/internal/widget/ActionBarContainer;

    invoke-virtual {v0, v5}, Landroid/support/v7/internal/widget/ActionBarContainer;->a(Z)V

    new-instance v1, Landroid/support/v7/internal/view/g;

    invoke-direct {v1}, Landroid/support/v7/internal/view/g;-><init>()V

    iget-object v0, p0, Landroid/support/v7/internal/a/b;->i:Landroid/support/v7/internal/widget/ActionBarContainer;

    invoke-virtual {v0}, Landroid/support/v7/internal/widget/ActionBarContainer;->getHeight()I

    move-result v0

    neg-int v0, v0

    int-to-float v0, v0

    if-eqz p1, :cond_c

    new-array v2, v2, [I

    fill-array-data v2, :array_1

    iget-object v3, p0, Landroid/support/v7/internal/a/b;->i:Landroid/support/v7/internal/widget/ActionBarContainer;

    invoke-virtual {v3, v2}, Landroid/support/v7/internal/widget/ActionBarContainer;->getLocationInWindow([I)V

    aget v2, v2, v5

    int-to-float v2, v2

    sub-float/2addr v0, v2

    :cond_c
    iget-object v2, p0, Landroid/support/v7/internal/a/b;->i:Landroid/support/v7/internal/widget/ActionBarContainer;

    invoke-static {v2}, Landroid/support/v4/view/ViewCompat;->k(Landroid/view/View;)Landroid/support/v4/view/ar;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/support/v4/view/ar;->c(F)Landroid/support/v4/view/ar;

    move-result-object v2

    iget-object v3, p0, Landroid/support/v7/internal/a/b;->B:Landroid/support/v4/view/aE;

    invoke-virtual {v2, v3}, Landroid/support/v4/view/ar;->a(Landroid/support/v4/view/aE;)Landroid/support/v4/view/ar;

    invoke-virtual {v1, v2}, Landroid/support/v7/internal/view/g;->a(Landroid/support/v4/view/ar;)Landroid/support/v7/internal/view/g;

    iget-boolean v2, p0, Landroid/support/v7/internal/a/b;->t:Z

    if-eqz v2, :cond_d

    iget-object v2, p0, Landroid/support/v7/internal/a/b;->m:Landroid/view/View;

    if-eqz v2, :cond_d

    iget-object v2, p0, Landroid/support/v7/internal/a/b;->m:Landroid/view/View;

    invoke-static {v2}, Landroid/support/v4/view/ViewCompat;->k(Landroid/view/View;)Landroid/support/v4/view/ar;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/support/v4/view/ar;->c(F)Landroid/support/v4/view/ar;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/support/v7/internal/view/g;->a(Landroid/support/v4/view/ar;)Landroid/support/v7/internal/view/g;

    :cond_d
    iget-object v0, p0, Landroid/support/v7/internal/a/b;->l:Landroid/support/v7/internal/widget/ActionBarContainer;

    if-eqz v0, :cond_e

    iget-object v0, p0, Landroid/support/v7/internal/a/b;->l:Landroid/support/v7/internal/widget/ActionBarContainer;

    invoke-virtual {v0}, Landroid/support/v7/internal/widget/ActionBarContainer;->getVisibility()I

    move-result v0

    if-nez v0, :cond_e

    iget-object v0, p0, Landroid/support/v7/internal/a/b;->l:Landroid/support/v7/internal/widget/ActionBarContainer;

    invoke-static {v0, v7}, Landroid/support/v4/view/ViewCompat;->c(Landroid/view/View;F)V

    iget-object v0, p0, Landroid/support/v7/internal/a/b;->l:Landroid/support/v7/internal/widget/ActionBarContainer;

    invoke-static {v0}, Landroid/support/v4/view/ViewCompat;->k(Landroid/view/View;)Landroid/support/v4/view/ar;

    move-result-object v0

    iget-object v2, p0, Landroid/support/v7/internal/a/b;->l:Landroid/support/v7/internal/widget/ActionBarContainer;

    invoke-virtual {v2}, Landroid/support/v7/internal/widget/ActionBarContainer;->getHeight()I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {v0, v2}, Landroid/support/v4/view/ar;->c(F)Landroid/support/v4/view/ar;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/support/v7/internal/view/g;->a(Landroid/support/v4/view/ar;)Landroid/support/v7/internal/view/g;

    :cond_e
    iget-object v0, p0, Landroid/support/v7/internal/a/b;->f:Landroid/content/Context;

    const v2, 0x10a0005

    invoke-static {v0, v2}, Landroid/view/animation/AnimationUtils;->loadInterpolator(Landroid/content/Context;I)Landroid/view/animation/Interpolator;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/support/v7/internal/view/g;->a(Landroid/view/animation/Interpolator;)Landroid/support/v7/internal/view/g;

    const-wide/16 v2, 0xfa

    invoke-virtual {v1, v2, v3}, Landroid/support/v7/internal/view/g;->a(J)Landroid/support/v7/internal/view/g;

    iget-object v0, p0, Landroid/support/v7/internal/a/b;->z:Landroid/support/v4/view/aC;

    invoke-virtual {v1, v0}, Landroid/support/v7/internal/view/g;->a(Landroid/support/v4/view/aC;)Landroid/support/v7/internal/view/g;

    iput-object v1, p0, Landroid/support/v7/internal/a/b;->x:Landroid/support/v7/internal/view/g;

    invoke-virtual {v1}, Landroid/support/v7/internal/view/g;->a()V

    goto/16 :goto_1

    :cond_f
    iget-object v0, p0, Landroid/support/v7/internal/a/b;->z:Landroid/support/v4/view/aC;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/support/v4/view/aC;->b(Landroid/view/View;)V

    goto/16 :goto_1

    .line 755
    :array_0
    .array-data 4
        0x0
        0x0
    .end array-data

    .line 760
    :array_1
    .array-data 4
        0x0
        0x0
    .end array-data
.end method

.method static synthetic g(Landroid/support/v7/internal/a/b;)Z
    .locals 1

    .prologue
    .line 75
    const/4 v0, 0x0

    return v0
.end method

.method static synthetic h(Landroid/support/v7/internal/a/b;)Z
    .locals 1

    .prologue
    .line 75
    iget-boolean v0, p0, Landroid/support/v7/internal/a/b;->u:Z

    return v0
.end method

.method static synthetic i(Landroid/support/v7/internal/a/b;)Landroid/support/v7/internal/widget/ActionBarContextView;
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Landroid/support/v7/internal/a/b;->k:Landroid/support/v7/internal/widget/ActionBarContextView;

    return-object v0
.end method

.method static synthetic j(Landroid/support/v7/internal/a/b;)Landroid/support/v7/internal/widget/u;
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Landroid/support/v7/internal/a/b;->j:Landroid/support/v7/internal/widget/u;

    return-object v0
.end method

.method static synthetic k(Landroid/support/v7/internal/a/b;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Landroid/support/v7/internal/a/b;->f:Landroid/content/Context;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 499
    iget-object v0, p0, Landroid/support/v7/internal/a/b;->j:Landroid/support/v7/internal/widget/u;

    invoke-interface {v0}, Landroid/support/v7/internal/widget/u;->n()I

    move-result v0

    return v0
.end method

.method public final a(Landroid/support/v7/b/b;)Landroid/support/v7/b/a;
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 503
    iget-object v0, p0, Landroid/support/v7/internal/a/b;->a:Landroid/support/v7/internal/a/f;

    if-eqz v0, :cond_0

    .line 504
    iget-object v0, p0, Landroid/support/v7/internal/a/b;->a:Landroid/support/v7/internal/a/f;

    invoke-virtual {v0}, Landroid/support/v7/internal/a/f;->b()V

    .line 507
    :cond_0
    iget-object v0, p0, Landroid/support/v7/internal/a/b;->h:Landroid/support/v7/internal/widget/ActionBarOverlayLayout;

    invoke-virtual {v0, v2}, Landroid/support/v7/internal/widget/ActionBarOverlayLayout;->b(Z)V

    .line 508
    iget-object v0, p0, Landroid/support/v7/internal/a/b;->k:Landroid/support/v7/internal/widget/ActionBarContextView;

    invoke-virtual {v0}, Landroid/support/v7/internal/widget/ActionBarContextView;->e()V

    .line 509
    new-instance v0, Landroid/support/v7/internal/a/f;

    invoke-direct {v0, p0, p1}, Landroid/support/v7/internal/a/f;-><init>(Landroid/support/v7/internal/a/b;Landroid/support/v7/b/b;)V

    .line 510
    invoke-virtual {v0}, Landroid/support/v7/internal/a/f;->d()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 511
    invoke-virtual {v0}, Landroid/support/v7/internal/a/f;->c()V

    .line 512
    iget-object v1, p0, Landroid/support/v7/internal/a/b;->k:Landroid/support/v7/internal/widget/ActionBarContextView;

    invoke-virtual {v1, v0}, Landroid/support/v7/internal/widget/ActionBarContextView;->a(Landroid/support/v7/b/a;)V

    .line 513
    invoke-virtual {p0, v3}, Landroid/support/v7/internal/a/b;->e(Z)V

    .line 514
    iget-object v1, p0, Landroid/support/v7/internal/a/b;->l:Landroid/support/v7/internal/widget/ActionBarContainer;

    if-eqz v1, :cond_1

    iget v1, p0, Landroid/support/v7/internal/a/b;->q:I

    if-ne v1, v3, :cond_1

    .line 516
    iget-object v1, p0, Landroid/support/v7/internal/a/b;->l:Landroid/support/v7/internal/widget/ActionBarContainer;

    invoke-virtual {v1}, Landroid/support/v7/internal/widget/ActionBarContainer;->getVisibility()I

    move-result v1

    if-eqz v1, :cond_1

    .line 517
    iget-object v1, p0, Landroid/support/v7/internal/a/b;->l:Landroid/support/v7/internal/widget/ActionBarContainer;

    invoke-virtual {v1, v2}, Landroid/support/v7/internal/widget/ActionBarContainer;->setVisibility(I)V

    .line 518
    iget-object v1, p0, Landroid/support/v7/internal/a/b;->h:Landroid/support/v7/internal/widget/ActionBarOverlayLayout;

    if-eqz v1, :cond_1

    .line 519
    iget-object v1, p0, Landroid/support/v7/internal/a/b;->h:Landroid/support/v7/internal/widget/ActionBarOverlayLayout;

    invoke-static {v1}, Landroid/support/v4/view/ViewCompat;->m(Landroid/view/View;)V

    .line 523
    :cond_1
    iget-object v1, p0, Landroid/support/v7/internal/a/b;->k:Landroid/support/v7/internal/widget/ActionBarContextView;

    const/16 v2, 0x20

    invoke-virtual {v1, v2}, Landroid/support/v7/internal/widget/ActionBarContextView;->sendAccessibilityEvent(I)V

    .line 524
    iput-object v0, p0, Landroid/support/v7/internal/a/b;->a:Landroid/support/v7/internal/a/f;

    .line 527
    :goto_0
    return-object v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(I)V
    .locals 0

    .prologue
    .line 328
    iput p1, p0, Landroid/support/v7/internal/a/b;->s:I

    .line 329
    return-void
.end method

.method public final a(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 446
    iget-object v0, p0, Landroid/support/v7/internal/a/b;->j:Landroid/support/v7/internal/widget/u;

    invoke-interface {v0, p1}, Landroid/support/v7/internal/widget/u;->a(Ljava/lang/CharSequence;)V

    .line 447
    return-void
.end method

.method public final a(Z)V
    .locals 3

    .prologue
    .line 1341
    iget-boolean v0, p0, Landroid/support/v7/internal/a/b;->n:Z

    if-nez v0, :cond_0

    .line 1342
    if-eqz p1, :cond_1

    const/4 v0, 0x4

    :goto_0
    iget-object v1, p0, Landroid/support/v7/internal/a/b;->j:Landroid/support/v7/internal/widget/u;

    invoke-interface {v1}, Landroid/support/v7/internal/widget/u;->n()I

    move-result v1

    const/4 v2, 0x1

    iput-boolean v2, p0, Landroid/support/v7/internal/a/b;->n:Z

    iget-object v2, p0, Landroid/support/v7/internal/a/b;->j:Landroid/support/v7/internal/widget/u;

    and-int/lit8 v0, v0, 0x4

    and-int/lit8 v1, v1, -0x5

    or-int/2addr v0, v1

    invoke-interface {v2, v0}, Landroid/support/v7/internal/widget/u;->a(I)V

    .line 1344
    :cond_0
    return-void

    .line 1342
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()Landroid/content/Context;
    .locals 4

    .prologue
    .line 882
    iget-object v0, p0, Landroid/support/v7/internal/a/b;->g:Landroid/content/Context;

    if-nez v0, :cond_0

    .line 883
    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    .line 884
    iget-object v1, p0, Landroid/support/v7/internal/a/b;->f:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v1

    .line 885
    const v2, 0x7f010056

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v0, v3}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    .line 886
    iget v0, v0, Landroid/util/TypedValue;->resourceId:I

    .line 888
    if-eqz v0, :cond_1

    .line 889
    new-instance v1, Landroid/view/ContextThemeWrapper;

    iget-object v2, p0, Landroid/support/v7/internal/a/b;->f:Landroid/content/Context;

    invoke-direct {v1, v2, v0}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    iput-object v1, p0, Landroid/support/v7/internal/a/b;->g:Landroid/content/Context;

    .line 894
    :cond_0
    :goto_0
    iget-object v0, p0, Landroid/support/v7/internal/a/b;->g:Landroid/content/Context;

    return-object v0

    .line 891
    :cond_1
    iget-object v0, p0, Landroid/support/v7/internal/a/b;->f:Landroid/content/Context;

    iput-object v0, p0, Landroid/support/v7/internal/a/b;->g:Landroid/content/Context;

    goto :goto_0
.end method

.method public final b(Z)V
    .locals 1

    .prologue
    .line 339
    iput-boolean p1, p0, Landroid/support/v7/internal/a/b;->y:Z

    .line 340
    if-nez p1, :cond_0

    iget-object v0, p0, Landroid/support/v7/internal/a/b;->x:Landroid/support/v7/internal/view/g;

    if-eqz v0, :cond_0

    .line 341
    iget-object v0, p0, Landroid/support/v7/internal/a/b;->x:Landroid/support/v7/internal/view/g;

    invoke-virtual {v0}, Landroid/support/v7/internal/view/g;->b()V

    .line 343
    :cond_0
    return-void
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 267
    iget-object v0, p0, Landroid/support/v7/internal/a/b;->f:Landroid/content/Context;

    invoke-static {v0}, Landroid/support/v4/d/a;->get$30f4eda9(Landroid/content/Context;)Landroid/support/v4/d/a;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/d/a;->hasEmbeddedTabs()Z

    move-result v0

    invoke-direct {p0, v0}, Landroid/support/v7/internal/a/b;->f(Z)V

    .line 268
    return-void
.end method

.method public final c(Z)V
    .locals 3

    .prologue
    .line 354
    iget-boolean v0, p0, Landroid/support/v7/internal/a/b;->o:Z

    if-ne p1, v0, :cond_1

    .line 363
    :cond_0
    return-void

    .line 357
    :cond_1
    iput-boolean p1, p0, Landroid/support/v7/internal/a/b;->o:Z

    .line 359
    iget-object v0, p0, Landroid/support/v7/internal/a/b;->p:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    .line 360
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    .line 361
    iget-object v2, p0, Landroid/support/v7/internal/a/b;->p:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    .line 360
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public final d(Z)V
    .locals 0

    .prologue
    .line 654
    iput-boolean p1, p0, Landroid/support/v7/internal/a/b;->t:Z

    .line 655
    return-void
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 936
    iget-object v0, p0, Landroid/support/v7/internal/a/b;->j:Landroid/support/v7/internal/widget/u;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/internal/a/b;->j:Landroid/support/v7/internal/widget/u;

    invoke-interface {v0}, Landroid/support/v7/internal/widget/u;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 937
    iget-object v0, p0, Landroid/support/v7/internal/a/b;->j:Landroid/support/v7/internal/widget/u;

    invoke-interface {v0}, Landroid/support/v7/internal/widget/u;->d()V

    .line 938
    const/4 v0, 0x1

    .line 940
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method final e()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 320
    iget-object v0, p0, Landroid/support/v7/internal/a/b;->c:Landroid/support/v7/b/b;

    if-eqz v0, :cond_0

    .line 321
    iget-object v0, p0, Landroid/support/v7/internal/a/b;->c:Landroid/support/v7/b/b;

    iget-object v1, p0, Landroid/support/v7/internal/a/b;->b:Landroid/support/v7/b/a;

    invoke-interface {v0, v1}, Landroid/support/v7/b/b;->a(Landroid/support/v7/b/a;)V

    .line 322
    iput-object v2, p0, Landroid/support/v7/internal/a/b;->b:Landroid/support/v7/b/a;

    .line 323
    iput-object v2, p0, Landroid/support/v7/internal/a/b;->c:Landroid/support/v7/b/b;

    .line 325
    :cond_0
    return-void
.end method

.method public final e(Z)V
    .locals 4

    .prologue
    const/16 v1, 0x8

    const/4 v2, 0x0

    .line 870
    if-eqz p1, :cond_2

    .line 871
    iget-boolean v0, p0, Landroid/support/v7/internal/a/b;->v:Z

    if-nez v0, :cond_1

    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v7/internal/a/b;->v:Z

    iget-object v0, p0, Landroid/support/v7/internal/a/b;->h:Landroid/support/v7/internal/widget/ActionBarOverlayLayout;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/internal/a/b;->h:Landroid/support/v7/internal/widget/ActionBarOverlayLayout;

    invoke-static {}, Landroid/support/v7/internal/widget/ActionBarOverlayLayout;->b()V

    :cond_0
    invoke-direct {p0, v2}, Landroid/support/v7/internal/a/b;->g(Z)V

    .line 876
    :cond_1
    :goto_0
    iget-object v3, p0, Landroid/support/v7/internal/a/b;->j:Landroid/support/v7/internal/widget/u;

    if-eqz p1, :cond_4

    move v0, v1

    :goto_1
    invoke-interface {v3, v0}, Landroid/support/v7/internal/widget/u;->b(I)V

    .line 877
    iget-object v0, p0, Landroid/support/v7/internal/a/b;->k:Landroid/support/v7/internal/widget/ActionBarContextView;

    if-eqz p1, :cond_5

    :goto_2
    invoke-virtual {v0, v2}, Landroid/support/v7/internal/widget/ActionBarContextView;->b(I)V

    .line 879
    return-void

    .line 873
    :cond_2
    iget-boolean v0, p0, Landroid/support/v7/internal/a/b;->v:Z

    if-eqz v0, :cond_1

    iput-boolean v2, p0, Landroid/support/v7/internal/a/b;->v:Z

    iget-object v0, p0, Landroid/support/v7/internal/a/b;->h:Landroid/support/v7/internal/widget/ActionBarOverlayLayout;

    if-eqz v0, :cond_3

    iget-object v0, p0, Landroid/support/v7/internal/a/b;->h:Landroid/support/v7/internal/widget/ActionBarOverlayLayout;

    invoke-static {}, Landroid/support/v7/internal/widget/ActionBarOverlayLayout;->b()V

    :cond_3
    invoke-direct {p0, v2}, Landroid/support/v7/internal/a/b;->g(Z)V

    goto :goto_0

    :cond_4
    move v0, v2

    .line 876
    goto :goto_1

    :cond_5
    move v2, v1

    .line 877
    goto :goto_2
.end method

.method public final f()V
    .locals 1

    .prologue
    .line 676
    iget-boolean v0, p0, Landroid/support/v7/internal/a/b;->u:Z

    if-eqz v0, :cond_0

    .line 677
    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/support/v7/internal/a/b;->u:Z

    .line 678
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Landroid/support/v7/internal/a/b;->g(Z)V

    .line 680
    :cond_0
    return-void
.end method

.method public final g()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 701
    iget-boolean v0, p0, Landroid/support/v7/internal/a/b;->u:Z

    if-nez v0, :cond_0

    .line 702
    iput-boolean v1, p0, Landroid/support/v7/internal/a/b;->u:Z

    .line 703
    invoke-direct {p0, v1}, Landroid/support/v7/internal/a/b;->g(Z)V

    .line 705
    :cond_0
    return-void
.end method

.method public final h()V
    .locals 1

    .prologue
    .line 924
    iget-object v0, p0, Landroid/support/v7/internal/a/b;->x:Landroid/support/v7/internal/view/g;

    if-eqz v0, :cond_0

    .line 925
    iget-object v0, p0, Landroid/support/v7/internal/a/b;->x:Landroid/support/v7/internal/view/g;

    invoke-virtual {v0}, Landroid/support/v7/internal/view/g;->b()V

    .line 926
    const/4 v0, 0x0

    iput-object v0, p0, Landroid/support/v7/internal/a/b;->x:Landroid/support/v7/internal/view/g;

    .line 928
    :cond_0
    return-void
.end method
