.class public final Landroid/support/v7/internal/widget/ProgressBarCompat;
.super Landroid/view/View;
.source "ProgressBarCompat.java"


# instance fields
.field private a:I

.field private b:I

.field private c:Landroid/support/v7/internal/widget/B;

.field private d:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 61
    const/16 v0, 0xe

    new-array v0, v0, [I

    const/4 v1, 0x0

    const v2, 0x1010136

    aput v2, v0, v1

    const/4 v1, 0x1

    const v2, 0x1010137

    aput v2, v0, v1

    const/4 v1, 0x2

    const v2, 0x1010138

    aput v2, v0, v1

    const/4 v1, 0x3

    const v2, 0x1010139

    aput v2, v0, v1

    const/4 v1, 0x4

    const v2, 0x101013a

    aput v2, v0, v1

    const/4 v1, 0x5

    const v2, 0x101013b

    aput v2, v0, v1

    const/4 v1, 0x6

    const v2, 0x101013c

    aput v2, v0, v1

    const/4 v1, 0x7

    const v2, 0x101013d

    aput v2, v0, v1

    const/16 v1, 0x8

    const v2, 0x101013e

    aput v2, v0, v1

    const/16 v1, 0x9

    const v2, 0x101013f

    aput v2, v0, v1

    const/16 v1, 0xa

    const v2, 0x101011f

    aput v2, v0, v1

    const/16 v1, 0xb

    const v2, 0x1010140

    aput v2, v0, v1

    const/16 v1, 0xc

    const v2, 0x1010120

    aput v2, v0, v1

    const/16 v1, 0xd

    const v2, 0x1010141

    aput v2, v0, v1

    return-void
.end method

.method static synthetic a(Landroid/support/v7/internal/widget/ProgressBarCompat;Landroid/support/v7/internal/widget/B;)Landroid/support/v7/internal/widget/B;
    .locals 0

    .prologue
    .line 53
    iput-object p1, p0, Landroid/support/v7/internal/widget/ProgressBarCompat;->c:Landroid/support/v7/internal/widget/B;

    return-object p1
.end method

.method private declared-synchronized a(I)V
    .locals 1

    .prologue
    .line 466
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    invoke-direct {p0, p1, v0}, Landroid/support/v7/internal/widget/ProgressBarCompat;->a(IZ)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 467
    monitor-exit p0

    return-void

    .line 466
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized a(II)V
    .locals 1

    .prologue
    .line 419
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Landroid/support/v7/internal/widget/ProgressBarCompat;->invalidate()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 433
    monitor-exit p0

    return-void

    .line 419
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized a(IIZ)V
    .locals 4

    .prologue
    .line 436
    monitor-enter p0

    const-wide/16 v0, 0x0

    :try_start_0
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Thread;->getId()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 437
    invoke-direct {p0, p1, p2}, Landroid/support/v7/internal/widget/ProgressBarCompat;->a(II)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 452
    :goto_0
    monitor-exit p0

    return-void

    .line 440
    :cond_0
    :try_start_1
    iget-object v0, p0, Landroid/support/v7/internal/widget/ProgressBarCompat;->c:Landroid/support/v7/internal/widget/B;

    if-eqz v0, :cond_1

    .line 442
    iget-object v0, p0, Landroid/support/v7/internal/widget/ProgressBarCompat;->c:Landroid/support/v7/internal/widget/B;

    .line 444
    const/4 v1, 0x0

    iput-object v1, p0, Landroid/support/v7/internal/widget/ProgressBarCompat;->c:Landroid/support/v7/internal/widget/B;

    .line 445
    invoke-virtual {v0, p1, p2, p3}, Landroid/support/v7/internal/widget/B;->a(IIZ)V

    .line 450
    :goto_1
    invoke-virtual {p0, v0}, Landroid/support/v7/internal/widget/ProgressBarCompat;->post(Ljava/lang/Runnable;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 436
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 448
    :cond_1
    :try_start_2
    new-instance v0, Landroid/support/v7/internal/widget/B;

    invoke-direct {v0, p0, p1, p2, p3}, Landroid/support/v7/internal/widget/B;-><init>(Landroid/support/v7/internal/widget/ProgressBarCompat;IIZ)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1
.end method

.method private declared-synchronized a(IZ)V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 470
    monitor-enter p0

    if-gez p1, :cond_2

    move v1, v0

    .line 478
    :goto_0
    if-lez v1, :cond_1

    .line 482
    :goto_1
    :try_start_0
    iget v1, p0, Landroid/support/v7/internal/widget/ProgressBarCompat;->a:I

    if-eq v0, v1, :cond_0

    .line 483
    iput v0, p0, Landroid/support/v7/internal/widget/ProgressBarCompat;->a:I

    .line 484
    const v0, 0x102000d

    iget v1, p0, Landroid/support/v7/internal/widget/ProgressBarCompat;->a:I

    const/4 v2, 0x0

    invoke-direct {p0, v0, v1, v2}, Landroid/support/v7/internal/widget/ProgressBarCompat;->a(IIZ)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 486
    :cond_0
    monitor-exit p0

    return-void

    .line 470
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_1
    move v0, v1

    goto :goto_1

    :cond_2
    move v1, p1

    goto :goto_0
.end method

.method static synthetic a(Landroid/support/v7/internal/widget/ProgressBarCompat;IIZZ)V
    .locals 0

    .prologue
    .line 53
    invoke-direct {p0, p1, p2}, Landroid/support/v7/internal/widget/ProgressBarCompat;->a(II)V

    return-void
.end method

.method private declared-synchronized b(I)V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 501
    monitor-enter p0

    if-gez p1, :cond_2

    move v1, v0

    .line 509
    :goto_0
    if-lez v1, :cond_1

    .line 513
    :goto_1
    :try_start_0
    iget v1, p0, Landroid/support/v7/internal/widget/ProgressBarCompat;->b:I

    if-eq v0, v1, :cond_0

    .line 514
    iput v0, p0, Landroid/support/v7/internal/widget/ProgressBarCompat;->b:I

    .line 515
    const v0, 0x102000f

    iget v1, p0, Landroid/support/v7/internal/widget/ProgressBarCompat;->b:I

    const/4 v2, 0x0

    invoke-direct {p0, v0, v1, v2}, Landroid/support/v7/internal/widget/ProgressBarCompat;->a(IIZ)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 517
    :cond_0
    monitor-exit p0

    return-void

    .line 501
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_1
    move v0, v1

    goto :goto_1

    :cond_2
    move v1, p1

    goto :goto_0
.end method


# virtual methods
.method protected final drawableStateChanged()V
    .locals 0

    .prologue
    .line 825
    invoke-super {p0}, Landroid/view/View;->drawableStateChanged()V

    .line 826
    invoke-virtual {p0}, Landroid/support/v7/internal/widget/ProgressBarCompat;->getDrawableState()[I

    .line 827
    return-void
.end method

.method public final invalidateDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 6

    .prologue
    .line 714
    iget-boolean v0, p0, Landroid/support/v7/internal/widget/ProgressBarCompat;->d:Z

    if-nez v0, :cond_0

    .line 715
    invoke-virtual {p0, p1}, Landroid/support/v7/internal/widget/ProgressBarCompat;->verifyDrawable(Landroid/graphics/drawable/Drawable;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 716
    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v0

    .line 717
    invoke-virtual {p0}, Landroid/support/v7/internal/widget/ProgressBarCompat;->getScrollX()I

    move-result v1

    invoke-virtual {p0}, Landroid/support/v7/internal/widget/ProgressBarCompat;->getPaddingLeft()I

    move-result v2

    add-int/2addr v1, v2

    .line 718
    invoke-virtual {p0}, Landroid/support/v7/internal/widget/ProgressBarCompat;->getScrollY()I

    move-result v2

    invoke-virtual {p0}, Landroid/support/v7/internal/widget/ProgressBarCompat;->getPaddingTop()I

    move-result v3

    add-int/2addr v2, v3

    .line 720
    iget v3, v0, Landroid/graphics/Rect;->left:I

    add-int/2addr v3, v1

    iget v4, v0, Landroid/graphics/Rect;->top:I

    add-int/2addr v4, v2

    iget v5, v0, Landroid/graphics/Rect;->right:I

    add-int/2addr v1, v5

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v0, v2

    invoke-virtual {p0, v3, v4, v1, v0}, Landroid/support/v7/internal/widget/ProgressBarCompat;->invalidate(IIII)V

    .line 726
    :cond_0
    :goto_0
    return-void

    .line 723
    :cond_1
    invoke-super {p0, p1}, Landroid/view/View;->invalidateDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method

.method protected final onAttachedToWindow()V
    .locals 0

    .prologue
    .line 903
    invoke-super {p0}, Landroid/view/View;->onAttachedToWindow()V

    .line 904
    return-void
.end method

.method protected final onDetachedFromWindow()V
    .locals 1

    .prologue
    .line 911
    iget-object v0, p0, Landroid/support/v7/internal/widget/ProgressBarCompat;->c:Landroid/support/v7/internal/widget/B;

    if-eqz v0, :cond_0

    .line 915
    iget-object v0, p0, Landroid/support/v7/internal/widget/ProgressBarCompat;->c:Landroid/support/v7/internal/widget/B;

    invoke-virtual {p0, v0}, Landroid/support/v7/internal/widget/ProgressBarCompat;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 920
    :cond_0
    invoke-super {p0}, Landroid/view/View;->onDetachedFromWindow()V

    .line 921
    return-void
.end method

.method protected final declared-synchronized onDraw(Landroid/graphics/Canvas;)V
    .locals 1

    .prologue
    .line 773
    monitor-enter p0

    :try_start_0
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 775
    monitor-exit p0

    return-void

    .line 773
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected final declared-synchronized onMeasure(II)V
    .locals 3

    .prologue
    .line 807
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Landroid/support/v7/internal/widget/ProgressBarCompat;->getDrawableState()[I

    .line 816
    invoke-virtual {p0}, Landroid/support/v7/internal/widget/ProgressBarCompat;->getPaddingLeft()I

    move-result v0

    invoke-virtual {p0}, Landroid/support/v7/internal/widget/ProgressBarCompat;->getPaddingRight()I

    move-result v1

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x0

    .line 817
    invoke-virtual {p0}, Landroid/support/v7/internal/widget/ProgressBarCompat;->getPaddingTop()I

    move-result v1

    invoke-virtual {p0}, Landroid/support/v7/internal/widget/ProgressBarCompat;->getPaddingBottom()I

    move-result v2

    add-int/2addr v1, v2

    add-int/lit8 v1, v1, 0x0

    .line 819
    invoke-static {v0, p1}, Landroid/support/v7/internal/widget/ProgressBarCompat;->resolveSize(II)I

    move-result v0

    invoke-static {v1, p2}, Landroid/support/v7/internal/widget/ProgressBarCompat;->resolveSize(II)I

    move-result v1

    invoke-virtual {p0, v0, v1}, Landroid/support/v7/internal/widget/ProgressBarCompat;->setMeasuredDimension(II)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 821
    monitor-exit p0

    return-void

    .line 807
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 1

    .prologue
    .line 894
    check-cast p1, Landroid/support/v7/internal/widget/ProgressBarCompat$SavedState;

    .line 895
    invoke-virtual {p1}, Landroid/support/v7/internal/widget/ProgressBarCompat$SavedState;->getSuperState()Landroid/os/Parcelable;

    move-result-object v0

    invoke-super {p0, v0}, Landroid/view/View;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 897
    iget v0, p1, Landroid/support/v7/internal/widget/ProgressBarCompat$SavedState;->a:I

    invoke-direct {p0, v0}, Landroid/support/v7/internal/widget/ProgressBarCompat;->a(I)V

    .line 898
    iget v0, p1, Landroid/support/v7/internal/widget/ProgressBarCompat$SavedState;->b:I

    invoke-direct {p0, v0}, Landroid/support/v7/internal/widget/ProgressBarCompat;->b(I)V

    .line 899
    return-void
.end method

.method public final onSaveInstanceState()Landroid/os/Parcelable;
    .locals 2

    .prologue
    .line 883
    invoke-super {p0}, Landroid/view/View;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v0

    .line 884
    new-instance v1, Landroid/support/v7/internal/widget/ProgressBarCompat$SavedState;

    invoke-direct {v1, v0}, Landroid/support/v7/internal/widget/ProgressBarCompat$SavedState;-><init>(Landroid/os/Parcelable;)V

    .line 886
    iget v0, p0, Landroid/support/v7/internal/widget/ProgressBarCompat;->a:I

    iput v0, v1, Landroid/support/v7/internal/widget/ProgressBarCompat$SavedState;->a:I

    .line 887
    iget v0, p0, Landroid/support/v7/internal/widget/ProgressBarCompat;->b:I

    iput v0, v1, Landroid/support/v7/internal/widget/ProgressBarCompat$SavedState;->b:I

    .line 889
    return-object v1
.end method

.method protected final onSizeChanged(IIII)V
    .locals 0

    .prologue
    .line 730
    invoke-virtual {p0}, Landroid/support/v7/internal/widget/ProgressBarCompat;->getPaddingRight()I

    invoke-virtual {p0}, Landroid/support/v7/internal/widget/ProgressBarCompat;->getPaddingLeft()I

    invoke-virtual {p0}, Landroid/support/v7/internal/widget/ProgressBarCompat;->getPaddingBottom()I

    invoke-virtual {p0}, Landroid/support/v7/internal/widget/ProgressBarCompat;->getPaddingTop()I

    .line 731
    return-void
.end method

.method protected final onVisibilityChanged(Landroid/view/View;I)V
    .locals 2

    .prologue
    .line 698
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x8

    if-lt v0, v1, :cond_0

    .line 699
    invoke-super {p0, p1, p2}, Landroid/view/View;->onVisibilityChanged(Landroid/view/View;I)V

    .line 702
    :cond_0
    return-void
.end method

.method public final postInvalidate()V
    .locals 0

    .prologue
    .line 386
    invoke-super {p0}, Landroid/view/View;->postInvalidate()V

    .line 389
    return-void
.end method

.method public final setVisibility(I)V
    .locals 1

    .prologue
    .line 682
    invoke-virtual {p0}, Landroid/support/v7/internal/widget/ProgressBarCompat;->getVisibility()I

    move-result v0

    if-eq v0, p1, :cond_0

    .line 683
    invoke-super {p0, p1}, Landroid/view/View;->setVisibility(I)V

    .line 685
    :cond_0
    return-void
.end method

.method protected final verifyDrawable(Landroid/graphics/drawable/Drawable;)Z
    .locals 1

    .prologue
    .line 380
    if-eqz p1, :cond_0

    if-eqz p1, :cond_0

    invoke-super {p0, p1}, Landroid/view/View;->verifyDrawable(Landroid/graphics/drawable/Drawable;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
