.class public final Landroid/support/v7/internal/view/c;
.super Ljava/lang/Object;
.source "SupportActionModeWrapper.java"

# interfaces
.implements Landroid/support/v7/b/b;


# instance fields
.field private a:Landroid/view/ActionMode$Callback;

.field private b:Landroid/content/Context;

.field private c:Landroid/support/v4/f/l;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/view/ActionMode$Callback;)V
    .locals 1

    .prologue
    .line 143
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 144
    iput-object p1, p0, Landroid/support/v7/internal/view/c;->b:Landroid/content/Context;

    .line 145
    iput-object p2, p0, Landroid/support/v7/internal/view/c;->a:Landroid/view/ActionMode$Callback;

    .line 146
    new-instance v0, Landroid/support/v4/f/l;

    invoke-direct {v0}, Landroid/support/v4/f/l;-><init>()V

    iput-object v0, p0, Landroid/support/v7/internal/view/c;->c:Landroid/support/v4/f/l;

    .line 147
    return-void
.end method

.method private b(Landroid/support/v7/b/a;)Landroid/view/ActionMode;
    .locals 2

    .prologue
    .line 175
    iget-object v0, p0, Landroid/support/v7/internal/view/c;->c:Landroid/support/v4/f/l;

    invoke-virtual {v0, p1}, Landroid/support/v4/f/l;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/internal/view/b;

    .line 176
    if-eqz v0, :cond_0

    .line 184
    :goto_0
    return-object v0

    .line 182
    :cond_0
    new-instance v0, Landroid/support/v7/internal/view/b;

    iget-object v1, p0, Landroid/support/v7/internal/view/c;->b:Landroid/content/Context;

    invoke-direct {v0, v1, p1}, Landroid/support/v7/internal/view/b;-><init>(Landroid/content/Context;Landroid/support/v7/b/a;)V

    .line 183
    iget-object v1, p0, Landroid/support/v7/internal/view/c;->c:Landroid/support/v4/f/l;

    invoke-virtual {v1, p1, v0}, Landroid/support/v4/f/l;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/support/v7/b/a;)V
    .locals 2

    .prologue
    .line 170
    iget-object v0, p0, Landroid/support/v7/internal/view/c;->a:Landroid/view/ActionMode$Callback;

    invoke-direct {p0, p1}, Landroid/support/v7/internal/view/c;->b(Landroid/support/v7/b/a;)Landroid/view/ActionMode;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/view/ActionMode$Callback;->onDestroyActionMode(Landroid/view/ActionMode;)V

    .line 171
    return-void
.end method

.method public final a(Landroid/support/v7/b/a;Landroid/view/Menu;)Z
    .locals 3

    .prologue
    .line 151
    iget-object v0, p0, Landroid/support/v7/internal/view/c;->a:Landroid/view/ActionMode$Callback;

    invoke-direct {p0, p1}, Landroid/support/v7/internal/view/c;->b(Landroid/support/v7/b/a;)Landroid/view/ActionMode;

    move-result-object v1

    invoke-static {p2}, Landroid/support/v7/internal/view/menu/f;->a(Landroid/view/Menu;)Landroid/view/Menu;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/view/ActionMode$Callback;->onCreateActionMode(Landroid/view/ActionMode;Landroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method public final a(Landroid/support/v7/b/a;Landroid/view/MenuItem;)Z
    .locals 3

    .prologue
    .line 164
    iget-object v0, p0, Landroid/support/v7/internal/view/c;->a:Landroid/view/ActionMode$Callback;

    invoke-direct {p0, p1}, Landroid/support/v7/internal/view/c;->b(Landroid/support/v7/b/a;)Landroid/view/ActionMode;

    move-result-object v1

    invoke-static {p2}, Landroid/support/v7/internal/view/menu/f;->b(Landroid/view/MenuItem;)Landroid/view/MenuItem;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/view/ActionMode$Callback;->onActionItemClicked(Landroid/view/ActionMode;Landroid/view/MenuItem;)Z

    move-result v0

    return v0
.end method

.method public final b(Landroid/support/v7/b/a;Landroid/view/Menu;)Z
    .locals 3

    .prologue
    .line 157
    iget-object v0, p0, Landroid/support/v7/internal/view/c;->a:Landroid/view/ActionMode$Callback;

    invoke-direct {p0, p1}, Landroid/support/v7/internal/view/c;->b(Landroid/support/v7/b/a;)Landroid/view/ActionMode;

    move-result-object v1

    invoke-static {p2}, Landroid/support/v7/internal/view/menu/f;->a(Landroid/view/Menu;)Landroid/view/Menu;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/view/ActionMode$Callback;->onPrepareActionMode(Landroid/view/ActionMode;Landroid/view/Menu;)Z

    move-result v0

    return v0
.end method
