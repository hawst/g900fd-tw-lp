.class public final Landroid/support/v7/internal/a/f;
.super Landroid/support/v7/b/a;
.source "WindowDecorActionBar.java"

# interfaces
.implements Landroid/support/v7/internal/view/menu/j;


# instance fields
.field private a:Landroid/support/v7/b/b;

.field private b:Landroid/support/v7/internal/view/menu/i;

.field private c:Ljava/lang/ref/WeakReference;

.field private synthetic d:Landroid/support/v7/internal/a/b;


# direct methods
.method public constructor <init>(Landroid/support/v7/internal/a/b;Landroid/support/v7/b/b;)V
    .locals 2

    .prologue
    .line 951
    iput-object p1, p0, Landroid/support/v7/internal/a/f;->d:Landroid/support/v7/internal/a/b;

    invoke-direct {p0}, Landroid/support/v7/b/a;-><init>()V

    .line 952
    iput-object p2, p0, Landroid/support/v7/internal/a/f;->a:Landroid/support/v7/b/b;

    .line 953
    new-instance v0, Landroid/support/v7/internal/view/menu/i;

    invoke-virtual {p1}, Landroid/support/v7/internal/a/b;->b()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/support/v7/internal/view/menu/i;-><init>(Landroid/content/Context;)V

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/support/v7/internal/view/menu/i;->a(I)Landroid/support/v7/internal/view/menu/i;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v7/internal/a/f;->b:Landroid/support/v7/internal/view/menu/i;

    .line 955
    iget-object v0, p0, Landroid/support/v7/internal/a/f;->b:Landroid/support/v7/internal/view/menu/i;

    invoke-virtual {v0, p0}, Landroid/support/v7/internal/view/menu/i;->a(Landroid/support/v7/internal/view/menu/j;)V

    .line 956
    return-void
.end method


# virtual methods
.method public final a()Landroid/view/Menu;
    .locals 1

    .prologue
    .line 965
    iget-object v0, p0, Landroid/support/v7/internal/a/f;->b:Landroid/support/v7/internal/view/menu/i;

    return-object v0
.end method

.method public final a(I)V
    .locals 1

    .prologue
    .line 1036
    iget-object v0, p0, Landroid/support/v7/internal/a/f;->d:Landroid/support/v7/internal/a/b;

    invoke-static {v0}, Landroid/support/v7/internal/a/b;->k(Landroid/support/v7/internal/a/b;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/support/v7/internal/a/f;->b(Ljava/lang/CharSequence;)V

    .line 1037
    return-void
.end method

.method public final a(Landroid/support/v7/internal/view/menu/i;)V
    .locals 1

    .prologue
    .line 1098
    iget-object v0, p0, Landroid/support/v7/internal/a/f;->a:Landroid/support/v7/b/b;

    if-nez v0, :cond_0

    .line 1103
    :goto_0
    return-void

    .line 1101
    :cond_0
    invoke-virtual {p0}, Landroid/support/v7/internal/a/f;->c()V

    .line 1102
    iget-object v0, p0, Landroid/support/v7/internal/a/f;->d:Landroid/support/v7/internal/a/b;

    invoke-static {v0}, Landroid/support/v7/internal/a/b;->i(Landroid/support/v7/internal/a/b;)Landroid/support/v7/internal/widget/ActionBarContextView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/internal/widget/ActionBarContextView;->a()Z

    goto :goto_0
.end method

.method public final a(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 1020
    iget-object v0, p0, Landroid/support/v7/internal/a/f;->d:Landroid/support/v7/internal/a/b;

    invoke-static {v0}, Landroid/support/v7/internal/a/b;->i(Landroid/support/v7/internal/a/b;)Landroid/support/v7/internal/widget/ActionBarContextView;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/support/v7/internal/widget/ActionBarContextView;->d(Landroid/view/View;)V

    .line 1021
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Landroid/support/v7/internal/a/f;->c:Ljava/lang/ref/WeakReference;

    .line 1022
    return-void
.end method

.method public final a(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 1026
    iget-object v0, p0, Landroid/support/v7/internal/a/f;->d:Landroid/support/v7/internal/a/b;

    invoke-static {v0}, Landroid/support/v7/internal/a/b;->i(Landroid/support/v7/internal/a/b;)Landroid/support/v7/internal/widget/ActionBarContextView;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/support/v7/internal/widget/ActionBarContextView;->b(Ljava/lang/CharSequence;)V

    .line 1027
    return-void
.end method

.method public final a(Z)V
    .locals 1

    .prologue
    .line 1056
    invoke-super {p0, p1}, Landroid/support/v7/b/a;->a(Z)V

    .line 1057
    iget-object v0, p0, Landroid/support/v7/internal/a/f;->d:Landroid/support/v7/internal/a/b;

    invoke-static {v0}, Landroid/support/v7/internal/a/b;->i(Landroid/support/v7/internal/a/b;)Landroid/support/v7/internal/widget/ActionBarContextView;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/support/v7/internal/widget/ActionBarContextView;->a(Z)V

    .line 1058
    return-void
.end method

.method public final a(Landroid/support/v7/internal/view/menu/i;Landroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 1071
    iget-object v0, p0, Landroid/support/v7/internal/a/f;->a:Landroid/support/v7/b/b;

    if-eqz v0, :cond_0

    .line 1072
    iget-object v0, p0, Landroid/support/v7/internal/a/f;->a:Landroid/support/v7/b/b;

    invoke-interface {v0, p0, p2}, Landroid/support/v7/b/b;->a(Landroid/support/v7/b/a;Landroid/view/MenuItem;)Z

    move-result v0

    .line 1074
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 970
    iget-object v0, p0, Landroid/support/v7/internal/a/f;->d:Landroid/support/v7/internal/a/b;

    iget-object v0, v0, Landroid/support/v7/internal/a/b;->a:Landroid/support/v7/internal/a/f;

    if-eq v0, p0, :cond_0

    .line 997
    :goto_0
    return-void

    .line 979
    :cond_0
    iget-object v0, p0, Landroid/support/v7/internal/a/f;->d:Landroid/support/v7/internal/a/b;

    invoke-static {v0}, Landroid/support/v7/internal/a/b;->g(Landroid/support/v7/internal/a/b;)Z

    move-result v0

    iget-object v1, p0, Landroid/support/v7/internal/a/f;->d:Landroid/support/v7/internal/a/b;

    invoke-static {v1}, Landroid/support/v7/internal/a/b;->h(Landroid/support/v7/internal/a/b;)Z

    move-result v1

    invoke-static {v0, v1, v2}, Landroid/support/v7/internal/a/b;->a(ZZZ)Z

    move-result v0

    if-nez v0, :cond_1

    .line 982
    iget-object v0, p0, Landroid/support/v7/internal/a/f;->d:Landroid/support/v7/internal/a/b;

    iput-object p0, v0, Landroid/support/v7/internal/a/b;->b:Landroid/support/v7/b/a;

    .line 983
    iget-object v0, p0, Landroid/support/v7/internal/a/f;->d:Landroid/support/v7/internal/a/b;

    iget-object v1, p0, Landroid/support/v7/internal/a/f;->a:Landroid/support/v7/b/b;

    iput-object v1, v0, Landroid/support/v7/internal/a/b;->c:Landroid/support/v7/b/b;

    .line 987
    :goto_1
    iput-object v3, p0, Landroid/support/v7/internal/a/f;->a:Landroid/support/v7/b/b;

    .line 988
    iget-object v0, p0, Landroid/support/v7/internal/a/f;->d:Landroid/support/v7/internal/a/b;

    invoke-virtual {v0, v2}, Landroid/support/v7/internal/a/b;->e(Z)V

    .line 991
    iget-object v0, p0, Landroid/support/v7/internal/a/f;->d:Landroid/support/v7/internal/a/b;

    invoke-static {v0}, Landroid/support/v7/internal/a/b;->i(Landroid/support/v7/internal/a/b;)Landroid/support/v7/internal/widget/ActionBarContextView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/internal/widget/ActionBarContextView;->d()V

    .line 992
    iget-object v0, p0, Landroid/support/v7/internal/a/f;->d:Landroid/support/v7/internal/a/b;

    invoke-static {v0}, Landroid/support/v7/internal/a/b;->j(Landroid/support/v7/internal/a/b;)Landroid/support/v7/internal/widget/u;

    move-result-object v0

    invoke-interface {v0}, Landroid/support/v7/internal/widget/u;->a()Landroid/view/ViewGroup;

    move-result-object v0

    const/16 v1, 0x20

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->sendAccessibilityEvent(I)V

    .line 994
    iget-object v0, p0, Landroid/support/v7/internal/a/f;->d:Landroid/support/v7/internal/a/b;

    invoke-static {v0}, Landroid/support/v7/internal/a/b;->f(Landroid/support/v7/internal/a/b;)Landroid/support/v7/internal/widget/ActionBarOverlayLayout;

    move-result-object v0

    iget-object v1, p0, Landroid/support/v7/internal/a/f;->d:Landroid/support/v7/internal/a/b;

    iget-boolean v1, v1, Landroid/support/v7/internal/a/b;->d:Z

    invoke-virtual {v0, v1}, Landroid/support/v7/internal/widget/ActionBarOverlayLayout;->b(Z)V

    .line 996
    iget-object v0, p0, Landroid/support/v7/internal/a/f;->d:Landroid/support/v7/internal/a/b;

    iput-object v3, v0, Landroid/support/v7/internal/a/b;->a:Landroid/support/v7/internal/a/f;

    goto :goto_0

    .line 985
    :cond_1
    iget-object v0, p0, Landroid/support/v7/internal/a/f;->a:Landroid/support/v7/b/b;

    invoke-interface {v0, p0}, Landroid/support/v7/b/b;->a(Landroid/support/v7/b/a;)V

    goto :goto_1
.end method

.method public final b(I)V
    .locals 1

    .prologue
    .line 1041
    iget-object v0, p0, Landroid/support/v7/internal/a/f;->d:Landroid/support/v7/internal/a/b;

    invoke-static {v0}, Landroid/support/v7/internal/a/b;->k(Landroid/support/v7/internal/a/b;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/support/v7/internal/a/f;->a(Ljava/lang/CharSequence;)V

    .line 1042
    return-void
.end method

.method public final b(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 1031
    iget-object v0, p0, Landroid/support/v7/internal/a/f;->d:Landroid/support/v7/internal/a/b;

    invoke-static {v0}, Landroid/support/v7/internal/a/b;->i(Landroid/support/v7/internal/a/b;)Landroid/support/v7/internal/widget/ActionBarContextView;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/support/v7/internal/widget/ActionBarContextView;->a(Ljava/lang/CharSequence;)V

    .line 1032
    return-void
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 1001
    iget-object v0, p0, Landroid/support/v7/internal/a/f;->b:Landroid/support/v7/internal/view/menu/i;

    invoke-virtual {v0}, Landroid/support/v7/internal/view/menu/i;->f()V

    .line 1003
    :try_start_0
    iget-object v0, p0, Landroid/support/v7/internal/a/f;->a:Landroid/support/v7/b/b;

    iget-object v1, p0, Landroid/support/v7/internal/a/f;->b:Landroid/support/v7/internal/view/menu/i;

    invoke-interface {v0, p0, v1}, Landroid/support/v7/b/b;->b(Landroid/support/v7/b/a;Landroid/view/Menu;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1005
    iget-object v0, p0, Landroid/support/v7/internal/a/f;->b:Landroid/support/v7/internal/view/menu/i;

    invoke-virtual {v0}, Landroid/support/v7/internal/view/menu/i;->g()V

    .line 1006
    return-void

    .line 1005
    :catchall_0
    move-exception v0

    iget-object v1, p0, Landroid/support/v7/internal/a/f;->b:Landroid/support/v7/internal/view/menu/i;

    invoke-virtual {v1}, Landroid/support/v7/internal/view/menu/i;->g()V

    throw v0
.end method

.method public final d()Z
    .locals 2

    .prologue
    .line 1010
    iget-object v0, p0, Landroid/support/v7/internal/a/f;->b:Landroid/support/v7/internal/view/menu/i;

    invoke-virtual {v0}, Landroid/support/v7/internal/view/menu/i;->f()V

    .line 1012
    :try_start_0
    iget-object v0, p0, Landroid/support/v7/internal/a/f;->a:Landroid/support/v7/b/b;

    iget-object v1, p0, Landroid/support/v7/internal/a/f;->b:Landroid/support/v7/internal/view/menu/i;

    invoke-interface {v0, p0, v1}, Landroid/support/v7/b/b;->a(Landroid/support/v7/b/a;Landroid/view/Menu;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 1014
    iget-object v1, p0, Landroid/support/v7/internal/a/f;->b:Landroid/support/v7/internal/view/menu/i;

    invoke-virtual {v1}, Landroid/support/v7/internal/view/menu/i;->g()V

    return v0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Landroid/support/v7/internal/a/f;->b:Landroid/support/v7/internal/view/menu/i;

    invoke-virtual {v1}, Landroid/support/v7/internal/view/menu/i;->g()V

    throw v0
.end method

.method public final e()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 1046
    iget-object v0, p0, Landroid/support/v7/internal/a/f;->d:Landroid/support/v7/internal/a/b;

    invoke-static {v0}, Landroid/support/v7/internal/a/b;->i(Landroid/support/v7/internal/a/b;)Landroid/support/v7/internal/widget/ActionBarContextView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/internal/widget/ActionBarContextView;->b()Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public final f()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 1051
    iget-object v0, p0, Landroid/support/v7/internal/a/f;->d:Landroid/support/v7/internal/a/b;

    invoke-static {v0}, Landroid/support/v7/internal/a/b;->i(Landroid/support/v7/internal/a/b;)Landroid/support/v7/internal/widget/ActionBarContextView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/internal/widget/ActionBarContextView;->c()Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public final g()Z
    .locals 1

    .prologue
    .line 1062
    iget-object v0, p0, Landroid/support/v7/internal/a/f;->d:Landroid/support/v7/internal/a/b;

    invoke-static {v0}, Landroid/support/v7/internal/a/b;->i(Landroid/support/v7/internal/a/b;)Landroid/support/v7/internal/widget/ActionBarContextView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/internal/widget/ActionBarContextView;->f()Z

    move-result v0

    return v0
.end method

.method public final h()Landroid/view/View;
    .locals 1

    .prologue
    .line 1067
    iget-object v0, p0, Landroid/support/v7/internal/a/f;->c:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/internal/a/f;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
