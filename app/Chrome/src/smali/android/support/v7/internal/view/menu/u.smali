.class final Landroid/support/v7/internal/view/menu/u;
.super Landroid/support/v7/internal/view/menu/p;
.source "MenuItemWrapperJB.java"

# interfaces
.implements Landroid/support/v4/view/j;


# instance fields
.field private c:Landroid/view/ActionProvider$VisibilityListener;


# direct methods
.method public constructor <init>(Landroid/support/v7/internal/view/menu/t;Landroid/support/v4/media/n;)V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0, p1, p2}, Landroid/support/v7/internal/view/menu/p;-><init>(Landroid/support/v7/internal/view/menu/o;Landroid/support/v4/media/n;)V

    .line 40
    return-void
.end method


# virtual methods
.method public final isVisible()Z
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/u;->a:Landroid/support/v4/media/n;

    const/4 v0, 0x1

    return v0
.end method

.method public final onCreateActionView(Landroid/view/MenuItem;)Landroid/view/View;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/u;->a:Landroid/support/v4/media/n;

    invoke-virtual {v0}, Landroid/support/v4/media/n;->c()Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final overridesItemVisibility()Z
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/u;->a:Landroid/support/v4/media/n;

    const/4 v0, 0x0

    return v0
.end method

.method public final refreshVisibility()V
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/u;->a:Landroid/support/v4/media/n;

    invoke-virtual {v0}, Landroid/support/v4/media/n;->d()V

    .line 60
    return-void
.end method

.method public final setVisibilityListener(Landroid/view/ActionProvider$VisibilityListener;)V
    .locals 1

    .prologue
    .line 65
    iput-object p1, p0, Landroid/support/v7/internal/view/menu/u;->c:Landroid/view/ActionProvider$VisibilityListener;

    .line 66
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/u;->a:Landroid/support/v4/media/n;

    if-eqz p1, :cond_0

    :goto_0
    invoke-virtual {v0, p0}, Landroid/support/v4/media/n;->a(Landroid/support/v4/view/j;)V

    .line 67
    return-void

    .line 66
    :cond_0
    const/4 p0, 0x0

    goto :goto_0
.end method
