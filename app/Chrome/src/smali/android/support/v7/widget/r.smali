.class final Landroid/support/v7/widget/r;
.super Landroid/support/v7/internal/widget/y;
.source "ListPopupWindow.java"


# instance fields
.field private a:Z

.field private b:Z

.field private c:Z

.field private d:Landroid/support/v4/widget/s;


# direct methods
.method public constructor <init>(Landroid/content/Context;Z)V
    .locals 2

    .prologue
    .line 1535
    const/4 v0, 0x0

    const v1, 0x7f010088

    invoke-direct {p0, p1, v0, v1}, Landroid/support/v7/internal/widget/y;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1536
    iput-boolean p2, p0, Landroid/support/v7/widget/r;->b:Z

    .line 1537
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/r;->setCacheColorHint(I)V

    .line 1538
    return-void
.end method

.method static synthetic a(Landroid/support/v7/widget/r;Z)Z
    .locals 0

    .prologue
    .line 1483
    iput-boolean p1, p0, Landroid/support/v7/widget/r;->a:Z

    return p1
.end method


# virtual methods
.method protected final a()Z
    .locals 1

    .prologue
    .line 1647
    iget-boolean v0, p0, Landroid/support/v7/widget/r;->c:Z

    if-nez v0, :cond_0

    invoke-super {p0}, Landroid/support/v7/internal/widget/y;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Landroid/view/MotionEvent;I)Z
    .locals 8

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1547
    .line 1550
    invoke-static {p1}, Landroid/support/v4/view/C;->a(Landroid/view/MotionEvent;)I

    move-result v3

    .line 1551
    packed-switch v3, :pswitch_data_0

    :cond_0
    :goto_0
    move v0, v1

    move v3, v2

    .line 1584
    :goto_1
    if-eqz v3, :cond_1

    if-eqz v0, :cond_2

    .line 1585
    :cond_1
    iput-boolean v1, p0, Landroid/support/v7/widget/r;->c:Z

    invoke-virtual {p0, v1}, Landroid/support/v7/widget/r;->setPressed(Z)V

    invoke-virtual {p0}, Landroid/support/v7/widget/r;->drawableStateChanged()V

    .line 1589
    :cond_2
    if-eqz v3, :cond_7

    .line 1590
    iget-object v0, p0, Landroid/support/v7/widget/r;->d:Landroid/support/v4/widget/s;

    if-nez v0, :cond_3

    .line 1591
    new-instance v0, Landroid/support/v4/widget/s;

    invoke-direct {v0, p0}, Landroid/support/v4/widget/s;-><init>(Landroid/widget/ListView;)V

    iput-object v0, p0, Landroid/support/v7/widget/r;->d:Landroid/support/v4/widget/s;

    .line 1593
    :cond_3
    iget-object v0, p0, Landroid/support/v7/widget/r;->d:Landroid/support/v4/widget/s;

    invoke-virtual {v0, v2}, Landroid/support/v4/widget/s;->a(Z)Landroid/support/v4/widget/a;

    .line 1594
    iget-object v0, p0, Landroid/support/v7/widget/r;->d:Landroid/support/v4/widget/s;

    invoke-virtual {v0, p0, p1}, Landroid/support/v4/widget/s;->onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z

    .line 1599
    :cond_4
    :goto_2
    return v3

    :pswitch_0
    move v0, v1

    move v3, v1

    .line 1554
    goto :goto_1

    :pswitch_1
    move v0, v1

    .line 1559
    :goto_3
    invoke-virtual {p1, p2}, Landroid/view/MotionEvent;->findPointerIndex(I)I

    move-result v4

    .line 1560
    if-gez v4, :cond_5

    move v0, v1

    move v3, v1

    .line 1562
    goto :goto_1

    .line 1565
    :cond_5
    invoke-virtual {p1, v4}, Landroid/view/MotionEvent;->getX(I)F

    move-result v5

    float-to-int v5, v5

    .line 1566
    invoke-virtual {p1, v4}, Landroid/view/MotionEvent;->getY(I)F

    move-result v4

    float-to-int v4, v4

    .line 1567
    invoke-virtual {p0, v5, v4}, Landroid/support/v7/widget/r;->pointToPosition(II)I

    move-result v6

    .line 1568
    const/4 v7, -0x1

    if-ne v6, v7, :cond_6

    move v3, v0

    move v0, v2

    .line 1570
    goto :goto_1

    .line 1573
    :cond_6
    invoke-virtual {p0}, Landroid/support/v7/widget/r;->getFirstVisiblePosition()I

    move-result v0

    sub-int v0, v6, v0

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/r;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 1574
    int-to-float v5, v5

    int-to-float v4, v4

    iput-boolean v2, p0, Landroid/support/v7/widget/r;->c:Z

    invoke-virtual {p0, v2}, Landroid/support/v7/widget/r;->setPressed(Z)V

    invoke-virtual {p0}, Landroid/support/v7/widget/r;->layoutChildren()V

    invoke-virtual {p0, v6}, Landroid/support/v7/widget/r;->setSelection(I)V

    invoke-virtual {p0, v6, v0, v5, v4}, Landroid/support/v7/widget/r;->a(ILandroid/view/View;FF)V

    invoke-virtual {p0, v1}, Landroid/support/v7/widget/r;->a(Z)V

    invoke-virtual {p0}, Landroid/support/v7/widget/r;->refreshDrawableState()V

    .line 1577
    if-ne v3, v2, :cond_0

    .line 1578
    invoke-virtual {p0, v6}, Landroid/support/v7/widget/r;->getItemIdAtPosition(I)J

    move-result-wide v4

    invoke-virtual {p0, v0, v6, v4, v5}, Landroid/support/v7/widget/r;->performItemClick(Landroid/view/View;IJ)Z

    goto :goto_0

    .line 1595
    :cond_7
    iget-object v0, p0, Landroid/support/v7/widget/r;->d:Landroid/support/v4/widget/s;

    if-eqz v0, :cond_4

    .line 1596
    iget-object v0, p0, Landroid/support/v7/widget/r;->d:Landroid/support/v4/widget/s;

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/s;->a(Z)Landroid/support/v4/widget/a;

    goto :goto_2

    :pswitch_2
    move v0, v2

    goto :goto_3

    .line 1551
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
    .end packed-switch
.end method

.method public final hasFocus()Z
    .locals 1

    .prologue
    .line 1683
    iget-boolean v0, p0, Landroid/support/v7/widget/r;->b:Z

    if-nez v0, :cond_0

    invoke-super {p0}, Landroid/support/v7/internal/widget/y;->hasFocus()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasWindowFocus()Z
    .locals 1

    .prologue
    .line 1663
    iget-boolean v0, p0, Landroid/support/v7/widget/r;->b:Z

    if-nez v0, :cond_0

    invoke-super {p0}, Landroid/support/v7/internal/widget/y;->hasWindowFocus()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isFocused()Z
    .locals 1

    .prologue
    .line 1673
    iget-boolean v0, p0, Landroid/support/v7/widget/r;->b:Z

    if-nez v0, :cond_0

    invoke-super {p0}, Landroid/support/v7/internal/widget/y;->isFocused()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInTouchMode()Z
    .locals 1

    .prologue
    .line 1653
    iget-boolean v0, p0, Landroid/support/v7/widget/r;->b:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Landroid/support/v7/widget/r;->a:Z

    if-nez v0, :cond_1

    :cond_0
    invoke-super {p0}, Landroid/support/v7/internal/widget/y;->isInTouchMode()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method
