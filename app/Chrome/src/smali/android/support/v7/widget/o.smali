.class public Landroid/support/v7/widget/o;
.super Ljava/lang/Object;
.source "ListPopupWindow.java"


# static fields
.field private static a:Ljava/lang/reflect/Method;


# instance fields
.field b:I

.field private c:Landroid/content/Context;

.field private d:Landroid/widget/PopupWindow;

.field private e:Landroid/widget/ListAdapter;

.field private f:Landroid/support/v7/widget/r;

.field private g:I

.field private h:I

.field private i:I

.field private j:I

.field private k:Z

.field private l:I

.field private m:Z

.field private n:Z

.field private o:I

.field private p:Landroid/database/DataSetObserver;

.field private q:Landroid/view/View;

.field private r:Landroid/widget/AdapterView$OnItemClickListener;

.field private final s:Landroid/support/v7/widget/z;

.field private final t:Landroid/support/v7/widget/y;

.field private final u:Landroid/support/v7/widget/x;

.field private final v:Landroid/support/v7/widget/v;

.field private w:Landroid/os/Handler;

.field private x:Landroid/graphics/Rect;

.field private y:Z


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    .line 80
    :try_start_0
    const-class v0, Landroid/widget/PopupWindow;

    const-string/jumbo v1, "setClipToScreenEnabled"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Class;

    const/4 v3, 0x0

    sget-object v4, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    sput-object v0, Landroid/support/v7/widget/o;->a:Ljava/lang/reflect/Method;
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_0

    .line 85
    :goto_0
    return-void

    .line 83
    :catch_0
    move-exception v0

    const-string/jumbo v0, "ListPopupWindow"

    const-string/jumbo v1, "Could not find method setClipToScreenEnabled() on PopupWindow. Oh well."

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4

    .prologue
    const/4 v0, -0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 214
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 92
    iput v0, p0, Landroid/support/v7/widget/o;->g:I

    .line 93
    iput v0, p0, Landroid/support/v7/widget/o;->h:I

    .line 98
    iput v2, p0, Landroid/support/v7/widget/o;->l:I

    .line 100
    iput-boolean v2, p0, Landroid/support/v7/widget/o;->m:Z

    .line 101
    iput-boolean v2, p0, Landroid/support/v7/widget/o;->n:Z

    .line 102
    const v0, 0x7fffffff

    iput v0, p0, Landroid/support/v7/widget/o;->b:I

    .line 105
    iput v2, p0, Landroid/support/v7/widget/o;->o:I

    .line 116
    new-instance v0, Landroid/support/v7/widget/z;

    invoke-direct {v0, p0, v2}, Landroid/support/v7/widget/z;-><init>(Landroid/support/v7/widget/o;B)V

    iput-object v0, p0, Landroid/support/v7/widget/o;->s:Landroid/support/v7/widget/z;

    .line 117
    new-instance v0, Landroid/support/v7/widget/y;

    invoke-direct {v0, p0, v2}, Landroid/support/v7/widget/y;-><init>(Landroid/support/v7/widget/o;B)V

    iput-object v0, p0, Landroid/support/v7/widget/o;->t:Landroid/support/v7/widget/y;

    .line 118
    new-instance v0, Landroid/support/v7/widget/x;

    invoke-direct {v0, p0, v2}, Landroid/support/v7/widget/x;-><init>(Landroid/support/v7/widget/o;B)V

    iput-object v0, p0, Landroid/support/v7/widget/o;->u:Landroid/support/v7/widget/x;

    .line 119
    new-instance v0, Landroid/support/v7/widget/v;

    invoke-direct {v0, p0, v2}, Landroid/support/v7/widget/v;-><init>(Landroid/support/v7/widget/o;B)V

    iput-object v0, p0, Landroid/support/v7/widget/o;->v:Landroid/support/v7/widget/v;

    .line 122
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Landroid/support/v7/widget/o;->w:Landroid/os/Handler;

    .line 124
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Landroid/support/v7/widget/o;->x:Landroid/graphics/Rect;

    .line 215
    iput-object p1, p0, Landroid/support/v7/widget/o;->c:Landroid/content/Context;

    .line 217
    sget-object v0, Landroid/support/v7/a/a;->h:[I

    invoke-virtual {p1, p2, v0, p3, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 219
    invoke-virtual {v0, v2, v2}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    move-result v1

    iput v1, p0, Landroid/support/v7/widget/o;->i:I

    .line 221
    invoke-virtual {v0, v3, v2}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    move-result v1

    iput v1, p0, Landroid/support/v7/widget/o;->j:I

    .line 223
    iget v1, p0, Landroid/support/v7/widget/o;->j:I

    if-eqz v1, :cond_0

    .line 224
    iput-boolean v3, p0, Landroid/support/v7/widget/o;->k:Z

    .line 226
    :cond_0
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 228
    new-instance v0, Landroid/support/v7/internal/widget/r;

    invoke-direct {v0, p1, p2, p3}, Landroid/support/v7/internal/widget/r;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-object v0, p0, Landroid/support/v7/widget/o;->d:Landroid/widget/PopupWindow;

    .line 229
    iget-object v0, p0, Landroid/support/v7/widget/o;->d:Landroid/widget/PopupWindow;

    invoke-virtual {v0, v3}, Landroid/widget/PopupWindow;->setInputMethodMode(I)V

    .line 232
    iget-object v0, p0, Landroid/support/v7/widget/o;->c:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget-object v0, v0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    .line 233
    invoke-static {v0}, Landroid/support/v4/e/f;->a(Ljava/util/Locale;)I

    .line 234
    return-void
.end method

.method static synthetic a(Landroid/support/v7/widget/o;)Landroid/support/v7/widget/r;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Landroid/support/v7/widget/o;->f:Landroid/support/v7/widget/r;

    return-object v0
.end method

.method static synthetic b(Landroid/support/v7/widget/o;)Landroid/widget/PopupWindow;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Landroid/support/v7/widget/o;->d:Landroid/widget/PopupWindow;

    return-object v0
.end method

.method static synthetic c(Landroid/support/v7/widget/o;)Landroid/support/v7/widget/z;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Landroid/support/v7/widget/o;->s:Landroid/support/v7/widget/z;

    return-object v0
.end method

.method static synthetic d(Landroid/support/v7/widget/o;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Landroid/support/v7/widget/o;->w:Landroid/os/Handler;

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 658
    iget-object v0, p0, Landroid/support/v7/widget/o;->d:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->dismiss()V

    .line 660
    iget-object v0, p0, Landroid/support/v7/widget/o;->d:Landroid/widget/PopupWindow;

    invoke-virtual {v0, v1}, Landroid/widget/PopupWindow;->setContentView(Landroid/view/View;)V

    .line 661
    iput-object v1, p0, Landroid/support/v7/widget/o;->f:Landroid/support/v7/widget/r;

    .line 662
    iget-object v0, p0, Landroid/support/v7/widget/o;->w:Landroid/os/Handler;

    iget-object v1, p0, Landroid/support/v7/widget/o;->s:Landroid/support/v7/widget/z;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 663
    return-void
.end method

.method public final a(I)V
    .locals 1

    .prologue
    .line 268
    const/4 v0, 0x0

    iput v0, p0, Landroid/support/v7/widget/o;->o:I

    .line 269
    return-void
.end method

.method public final a(Landroid/graphics/drawable/Drawable;)V
    .locals 1

    .prologue
    .line 385
    iget-object v0, p0, Landroid/support/v7/widget/o;->d:Landroid/widget/PopupWindow;

    invoke-virtual {v0, p1}, Landroid/widget/PopupWindow;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 386
    return-void
.end method

.method public final a(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 422
    iput-object p1, p0, Landroid/support/v7/widget/o;->q:Landroid/view/View;

    .line 423
    return-void
.end method

.method public final a(Landroid/widget/AdapterView$OnItemClickListener;)V
    .locals 0

    .prologue
    .line 528
    iput-object p1, p0, Landroid/support/v7/widget/o;->r:Landroid/widget/AdapterView$OnItemClickListener;

    .line 529
    return-void
.end method

.method public a(Landroid/widget/ListAdapter;)V
    .locals 2

    .prologue
    .line 243
    iget-object v0, p0, Landroid/support/v7/widget/o;->p:Landroid/database/DataSetObserver;

    if-nez v0, :cond_3

    .line 244
    new-instance v0, Landroid/support/v7/widget/w;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Landroid/support/v7/widget/w;-><init>(Landroid/support/v7/widget/o;B)V

    iput-object v0, p0, Landroid/support/v7/widget/o;->p:Landroid/database/DataSetObserver;

    .line 248
    :cond_0
    :goto_0
    iput-object p1, p0, Landroid/support/v7/widget/o;->e:Landroid/widget/ListAdapter;

    .line 249
    iget-object v0, p0, Landroid/support/v7/widget/o;->e:Landroid/widget/ListAdapter;

    if-eqz v0, :cond_1

    .line 250
    iget-object v0, p0, Landroid/support/v7/widget/o;->p:Landroid/database/DataSetObserver;

    invoke-interface {p1, v0}, Landroid/widget/ListAdapter;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 253
    :cond_1
    iget-object v0, p0, Landroid/support/v7/widget/o;->f:Landroid/support/v7/widget/r;

    if-eqz v0, :cond_2

    .line 254
    iget-object v0, p0, Landroid/support/v7/widget/o;->f:Landroid/support/v7/widget/r;

    iget-object v1, p0, Landroid/support/v7/widget/o;->e:Landroid/widget/ListAdapter;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/r;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 256
    :cond_2
    return-void

    .line 245
    :cond_3
    iget-object v0, p0, Landroid/support/v7/widget/o;->e:Landroid/widget/ListAdapter;

    if-eqz v0, :cond_0

    .line 246
    iget-object v0, p0, Landroid/support/v7/widget/o;->e:Landroid/widget/ListAdapter;

    iget-object v1, p0, Landroid/support/v7/widget/o;->p:Landroid/database/DataSetObserver;

    invoke-interface {v0, v1}, Landroid/widget/ListAdapter;->unregisterDataSetObserver(Landroid/database/DataSetObserver;)V

    goto :goto_0
.end method

.method public final a(Landroid/widget/PopupWindow$OnDismissListener;)V
    .locals 1

    .prologue
    .line 671
    iget-object v0, p0, Landroid/support/v7/widget/o;->d:Landroid/widget/PopupWindow;

    invoke-virtual {v0, p1}, Landroid/widget/PopupWindow;->setOnDismissListener(Landroid/widget/PopupWindow$OnDismissListener;)V

    .line 672
    return-void
.end method

.method public final a(Z)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 291
    iput-boolean v1, p0, Landroid/support/v7/widget/o;->y:Z

    .line 292
    iget-object v0, p0, Landroid/support/v7/widget/o;->d:Landroid/widget/PopupWindow;

    invoke-virtual {v0, v1}, Landroid/widget/PopupWindow;->setFocusable(Z)V

    .line 293
    return-void
.end method

.method public final b(I)V
    .locals 0

    .prologue
    .line 468
    iput p1, p0, Landroid/support/v7/widget/o;->l:I

    .line 469
    return-void
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 747
    iget-object v0, p0, Landroid/support/v7/widget/o;->d:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->isShowing()Z

    move-result v0

    return v0
.end method

.method public final c()V
    .locals 10

    .prologue
    const/high16 v9, 0x40000000    # 2.0f

    const/4 v8, -0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v5, -0x1

    .line 571
    iget-object v0, p0, Landroid/support/v7/widget/o;->f:Landroid/support/v7/widget/r;

    if-nez v0, :cond_3

    iget-object v3, p0, Landroid/support/v7/widget/o;->c:Landroid/content/Context;

    new-instance v0, Landroid/support/v7/widget/p;

    invoke-direct {v0, p0}, Landroid/support/v7/widget/p;-><init>(Landroid/support/v7/widget/o;)V

    new-instance v4, Landroid/support/v7/widget/r;

    iget-boolean v0, p0, Landroid/support/v7/widget/o;->y:Z

    if-nez v0, :cond_2

    move v0, v1

    :goto_0
    invoke-direct {v4, v3, v0}, Landroid/support/v7/widget/r;-><init>(Landroid/content/Context;Z)V

    iput-object v4, p0, Landroid/support/v7/widget/o;->f:Landroid/support/v7/widget/r;

    iget-object v0, p0, Landroid/support/v7/widget/o;->f:Landroid/support/v7/widget/r;

    iget-object v3, p0, Landroid/support/v7/widget/o;->e:Landroid/widget/ListAdapter;

    invoke-virtual {v0, v3}, Landroid/support/v7/widget/r;->setAdapter(Landroid/widget/ListAdapter;)V

    iget-object v0, p0, Landroid/support/v7/widget/o;->f:Landroid/support/v7/widget/r;

    iget-object v3, p0, Landroid/support/v7/widget/o;->r:Landroid/widget/AdapterView$OnItemClickListener;

    invoke-virtual {v0, v3}, Landroid/support/v7/widget/r;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    iget-object v0, p0, Landroid/support/v7/widget/o;->f:Landroid/support/v7/widget/r;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/r;->setFocusable(Z)V

    iget-object v0, p0, Landroid/support/v7/widget/o;->f:Landroid/support/v7/widget/r;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/r;->setFocusableInTouchMode(Z)V

    iget-object v0, p0, Landroid/support/v7/widget/o;->f:Landroid/support/v7/widget/r;

    new-instance v3, Landroid/support/v7/widget/q;

    invoke-direct {v3, p0}, Landroid/support/v7/widget/q;-><init>(Landroid/support/v7/widget/o;)V

    invoke-virtual {v0, v3}, Landroid/support/v7/widget/r;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    iget-object v0, p0, Landroid/support/v7/widget/o;->f:Landroid/support/v7/widget/r;

    iget-object v3, p0, Landroid/support/v7/widget/o;->u:Landroid/support/v7/widget/x;

    invoke-virtual {v0, v3}, Landroid/support/v7/widget/r;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    iget-object v0, p0, Landroid/support/v7/widget/o;->f:Landroid/support/v7/widget/r;

    iget-object v3, p0, Landroid/support/v7/widget/o;->d:Landroid/widget/PopupWindow;

    invoke-virtual {v3, v0}, Landroid/widget/PopupWindow;->setContentView(Landroid/view/View;)V

    :goto_1
    iget-object v0, p0, Landroid/support/v7/widget/o;->d:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-eqz v0, :cond_4

    iget-object v3, p0, Landroid/support/v7/widget/o;->x:Landroid/graphics/Rect;

    invoke-virtual {v0, v3}, Landroid/graphics/drawable/Drawable;->getPadding(Landroid/graphics/Rect;)Z

    iget-object v0, p0, Landroid/support/v7/widget/o;->x:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->top:I

    iget-object v3, p0, Landroid/support/v7/widget/o;->x:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v0, v3

    iget-boolean v3, p0, Landroid/support/v7/widget/o;->k:Z

    if-nez v3, :cond_0

    iget-object v3, p0, Landroid/support/v7/widget/o;->x:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->top:I

    neg-int v3, v3

    iput v3, p0, Landroid/support/v7/widget/o;->j:I

    :cond_0
    :goto_2
    iget-object v3, p0, Landroid/support/v7/widget/o;->d:Landroid/widget/PopupWindow;

    invoke-virtual {v3}, Landroid/widget/PopupWindow;->getInputMethodMode()I

    iget-object v3, p0, Landroid/support/v7/widget/o;->d:Landroid/widget/PopupWindow;

    iget-object v4, p0, Landroid/support/v7/widget/o;->q:Landroid/view/View;

    iget v6, p0, Landroid/support/v7/widget/o;->j:I

    invoke-virtual {v3, v4, v6}, Landroid/widget/PopupWindow;->getMaxAvailableHeight(Landroid/view/View;I)I

    move-result v4

    iget v3, p0, Landroid/support/v7/widget/o;->g:I

    if-ne v3, v5, :cond_5

    add-int/2addr v0, v4

    .line 576
    :goto_3
    invoke-virtual {p0}, Landroid/support/v7/widget/o;->f()Z

    move-result v3

    .line 578
    iget-object v4, p0, Landroid/support/v7/widget/o;->d:Landroid/widget/PopupWindow;

    invoke-virtual {v4}, Landroid/widget/PopupWindow;->isShowing()Z

    move-result v4

    if-eqz v4, :cond_e

    .line 579
    iget v4, p0, Landroid/support/v7/widget/o;->h:I

    if-ne v4, v5, :cond_6

    move v4, v5

    .line 589
    :goto_4
    iget v6, p0, Landroid/support/v7/widget/o;->g:I

    if-ne v6, v5, :cond_c

    .line 592
    if-eqz v3, :cond_8

    .line 593
    :goto_5
    if-eqz v3, :cond_a

    .line 594
    iget-object v3, p0, Landroid/support/v7/widget/o;->d:Landroid/widget/PopupWindow;

    iget v6, p0, Landroid/support/v7/widget/o;->h:I

    if-ne v6, v5, :cond_9

    :goto_6
    invoke-virtual {v3, v5, v2}, Landroid/widget/PopupWindow;->setWindowLayoutMode(II)V

    move v5, v0

    .line 609
    :goto_7
    iget-object v0, p0, Landroid/support/v7/widget/o;->d:Landroid/widget/PopupWindow;

    invoke-virtual {v0, v1}, Landroid/widget/PopupWindow;->setOutsideTouchable(Z)V

    .line 611
    iget-object v0, p0, Landroid/support/v7/widget/o;->d:Landroid/widget/PopupWindow;

    iget-object v1, p0, Landroid/support/v7/widget/o;->q:Landroid/view/View;

    iget v2, p0, Landroid/support/v7/widget/o;->i:I

    iget v3, p0, Landroid/support/v7/widget/o;->j:I

    invoke-virtual/range {v0 .. v5}, Landroid/widget/PopupWindow;->update(Landroid/view/View;IIII)V

    .line 652
    :cond_1
    :goto_8
    return-void

    :cond_2
    move v0, v2

    .line 571
    goto/16 :goto_0

    :cond_3
    iget-object v0, p0, Landroid/support/v7/widget/o;->d:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->getContentView()Landroid/view/View;

    goto :goto_1

    :cond_4
    iget-object v0, p0, Landroid/support/v7/widget/o;->x:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->setEmpty()V

    move v0, v2

    goto :goto_2

    :cond_5
    iget v3, p0, Landroid/support/v7/widget/o;->h:I

    packed-switch v3, :pswitch_data_0

    iget v3, p0, Landroid/support/v7/widget/o;->h:I

    invoke-static {v3, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    :goto_9
    iget-object v6, p0, Landroid/support/v7/widget/o;->f:Landroid/support/v7/widget/r;

    invoke-virtual {v6, v3, v4, v5}, Landroid/support/v7/widget/r;->a(III)I

    move-result v3

    if-lez v3, :cond_16

    add-int/lit8 v0, v0, 0x0

    :goto_a
    add-int/2addr v0, v3

    goto :goto_3

    :pswitch_0
    iget-object v3, p0, Landroid/support/v7/widget/o;->c:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    iget v3, v3, Landroid/util/DisplayMetrics;->widthPixels:I

    iget-object v6, p0, Landroid/support/v7/widget/o;->x:Landroid/graphics/Rect;

    iget v6, v6, Landroid/graphics/Rect;->left:I

    iget-object v7, p0, Landroid/support/v7/widget/o;->x:Landroid/graphics/Rect;

    iget v7, v7, Landroid/graphics/Rect;->right:I

    add-int/2addr v6, v7

    sub-int/2addr v3, v6

    const/high16 v6, -0x80000000

    invoke-static {v3, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    goto :goto_9

    :pswitch_1
    iget-object v3, p0, Landroid/support/v7/widget/o;->c:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    iget v3, v3, Landroid/util/DisplayMetrics;->widthPixels:I

    iget-object v6, p0, Landroid/support/v7/widget/o;->x:Landroid/graphics/Rect;

    iget v6, v6, Landroid/graphics/Rect;->left:I

    iget-object v7, p0, Landroid/support/v7/widget/o;->x:Landroid/graphics/Rect;

    iget v7, v7, Landroid/graphics/Rect;->right:I

    add-int/2addr v6, v7

    sub-int/2addr v3, v6

    invoke-static {v3, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    goto :goto_9

    .line 583
    :cond_6
    iget v4, p0, Landroid/support/v7/widget/o;->h:I

    if-ne v4, v8, :cond_7

    .line 584
    iget-object v4, p0, Landroid/support/v7/widget/o;->q:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getWidth()I

    move-result v4

    goto/16 :goto_4

    .line 586
    :cond_7
    iget v4, p0, Landroid/support/v7/widget/o;->h:I

    goto/16 :goto_4

    :cond_8
    move v0, v5

    .line 592
    goto/16 :goto_5

    :cond_9
    move v5, v2

    .line 594
    goto/16 :goto_6

    .line 598
    :cond_a
    iget-object v3, p0, Landroid/support/v7/widget/o;->d:Landroid/widget/PopupWindow;

    iget v6, p0, Landroid/support/v7/widget/o;->h:I

    if-ne v6, v5, :cond_b

    move v2, v5

    :cond_b
    invoke-virtual {v3, v2, v5}, Landroid/widget/PopupWindow;->setWindowLayoutMode(II)V

    move v5, v0

    goto/16 :goto_7

    .line 603
    :cond_c
    iget v2, p0, Landroid/support/v7/widget/o;->g:I

    if-ne v2, v8, :cond_d

    move v5, v0

    .line 604
    goto/16 :goto_7

    .line 606
    :cond_d
    iget v5, p0, Landroid/support/v7/widget/o;->g:I

    goto/16 :goto_7

    .line 614
    :cond_e
    iget v3, p0, Landroid/support/v7/widget/o;->h:I

    if-ne v3, v5, :cond_12

    move v3, v5

    .line 624
    :goto_b
    iget v4, p0, Landroid/support/v7/widget/o;->g:I

    if-ne v4, v5, :cond_14

    move v2, v5

    .line 634
    :goto_c
    iget-object v0, p0, Landroid/support/v7/widget/o;->d:Landroid/widget/PopupWindow;

    invoke-virtual {v0, v3, v2}, Landroid/widget/PopupWindow;->setWindowLayoutMode(II)V

    .line 635
    sget-object v0, Landroid/support/v7/widget/o;->a:Ljava/lang/reflect/Method;

    if-eqz v0, :cond_f

    :try_start_0
    sget-object v0, Landroid/support/v7/widget/o;->a:Ljava/lang/reflect/Method;

    iget-object v2, p0, Landroid/support/v7/widget/o;->d:Landroid/widget/PopupWindow;

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    const/4 v6, 0x1

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    aput-object v6, v3, v4

    invoke-virtual {v0, v2, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 639
    :cond_f
    :goto_d
    iget-object v0, p0, Landroid/support/v7/widget/o;->d:Landroid/widget/PopupWindow;

    invoke-virtual {v0, v1}, Landroid/widget/PopupWindow;->setOutsideTouchable(Z)V

    .line 640
    iget-object v0, p0, Landroid/support/v7/widget/o;->d:Landroid/widget/PopupWindow;

    iget-object v1, p0, Landroid/support/v7/widget/o;->t:Landroid/support/v7/widget/y;

    invoke-virtual {v0, v1}, Landroid/widget/PopupWindow;->setTouchInterceptor(Landroid/view/View$OnTouchListener;)V

    .line 641
    iget-object v0, p0, Landroid/support/v7/widget/o;->d:Landroid/widget/PopupWindow;

    iget-object v1, p0, Landroid/support/v7/widget/o;->q:Landroid/view/View;

    iget v2, p0, Landroid/support/v7/widget/o;->i:I

    iget v3, p0, Landroid/support/v7/widget/o;->j:I

    iget v4, p0, Landroid/support/v7/widget/o;->l:I

    invoke-static {v0, v1, v2, v3, v4}, Landroid/support/v4/widget/w;->a(Landroid/widget/PopupWindow;Landroid/view/View;III)V

    .line 643
    iget-object v0, p0, Landroid/support/v7/widget/o;->f:Landroid/support/v7/widget/r;

    invoke-virtual {v0, v5}, Landroid/support/v7/widget/r;->setSelection(I)V

    .line 645
    iget-boolean v0, p0, Landroid/support/v7/widget/o;->y:Z

    if-eqz v0, :cond_10

    iget-object v0, p0, Landroid/support/v7/widget/o;->f:Landroid/support/v7/widget/r;

    invoke-virtual {v0}, Landroid/support/v7/widget/r;->isInTouchMode()Z

    move-result v0

    if-eqz v0, :cond_11

    .line 646
    :cond_10
    invoke-virtual {p0}, Landroid/support/v7/widget/o;->e()V

    .line 648
    :cond_11
    iget-boolean v0, p0, Landroid/support/v7/widget/o;->y:Z

    if-nez v0, :cond_1

    .line 649
    iget-object v0, p0, Landroid/support/v7/widget/o;->w:Landroid/os/Handler;

    iget-object v1, p0, Landroid/support/v7/widget/o;->v:Landroid/support/v7/widget/v;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto/16 :goto_8

    .line 617
    :cond_12
    iget v3, p0, Landroid/support/v7/widget/o;->h:I

    if-ne v3, v8, :cond_13

    .line 618
    iget-object v3, p0, Landroid/support/v7/widget/o;->d:Landroid/widget/PopupWindow;

    iget-object v4, p0, Landroid/support/v7/widget/o;->q:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getWidth()I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/widget/PopupWindow;->setWidth(I)V

    move v3, v2

    goto :goto_b

    .line 620
    :cond_13
    iget-object v3, p0, Landroid/support/v7/widget/o;->d:Landroid/widget/PopupWindow;

    iget v4, p0, Landroid/support/v7/widget/o;->h:I

    invoke-virtual {v3, v4}, Landroid/widget/PopupWindow;->setWidth(I)V

    move v3, v2

    goto :goto_b

    .line 627
    :cond_14
    iget v4, p0, Landroid/support/v7/widget/o;->g:I

    if-ne v4, v8, :cond_15

    .line 628
    iget-object v4, p0, Landroid/support/v7/widget/o;->d:Landroid/widget/PopupWindow;

    invoke-virtual {v4, v0}, Landroid/widget/PopupWindow;->setHeight(I)V

    goto :goto_c

    .line 630
    :cond_15
    iget-object v0, p0, Landroid/support/v7/widget/o;->d:Landroid/widget/PopupWindow;

    iget v4, p0, Landroid/support/v7/widget/o;->g:I

    invoke-virtual {v0, v4}, Landroid/widget/PopupWindow;->setHeight(I)V

    goto :goto_c

    .line 635
    :catch_0
    move-exception v0

    const-string/jumbo v0, "ListPopupWindow"

    const-string/jumbo v2, "Could not call setClipToScreenEnabled() on PopupWindow. Oh well."

    invoke-static {v0, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_d

    :cond_16
    move v0, v2

    goto/16 :goto_a

    .line 571
    :pswitch_data_0
    .packed-switch -0x2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final c(I)V
    .locals 2

    .prologue
    .line 495
    iget-object v0, p0, Landroid/support/v7/widget/o;->d:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 496
    if-eqz v0, :cond_0

    .line 497
    iget-object v1, p0, Landroid/support/v7/widget/o;->x:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->getPadding(Landroid/graphics/Rect;)Z

    .line 498
    iget-object v0, p0, Landroid/support/v7/widget/o;->x:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    iget-object v1, p0, Landroid/support/v7/widget/o;->x:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->right:I

    add-int/2addr v0, v1

    add-int/2addr v0, p1

    iput v0, p0, Landroid/support/v7/widget/o;->h:I

    .line 502
    :goto_0
    return-void

    .line 500
    :cond_0
    iput p1, p0, Landroid/support/v7/widget/o;->h:I

    goto :goto_0
.end method

.method public final d()Landroid/view/View;
    .locals 1

    .prologue
    .line 412
    iget-object v0, p0, Landroid/support/v7/widget/o;->q:Landroid/view/View;

    return-object v0
.end method

.method public final d(I)V
    .locals 2

    .prologue
    .line 697
    iget-object v0, p0, Landroid/support/v7/widget/o;->d:Landroid/widget/PopupWindow;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/widget/PopupWindow;->setInputMethodMode(I)V

    .line 698
    return-void
.end method

.method public final e()V
    .locals 2

    .prologue
    .line 734
    iget-object v0, p0, Landroid/support/v7/widget/o;->f:Landroid/support/v7/widget/r;

    .line 735
    if-eqz v0, :cond_0

    .line 737
    const/4 v1, 0x1

    invoke-static {v0, v1}, Landroid/support/v7/widget/r;->a(Landroid/support/v7/widget/r;Z)Z

    .line 739
    invoke-virtual {v0}, Landroid/support/v7/widget/r;->requestLayout()V

    .line 741
    :cond_0
    return-void
.end method

.method public final f()Z
    .locals 2

    .prologue
    .line 755
    iget-object v0, p0, Landroid/support/v7/widget/o;->d:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->getInputMethodMode()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final g()Landroid/widget/ListView;
    .locals 1

    .prologue
    .line 832
    iget-object v0, p0, Landroid/support/v7/widget/o;->f:Landroid/support/v7/widget/r;

    return-object v0
.end method
