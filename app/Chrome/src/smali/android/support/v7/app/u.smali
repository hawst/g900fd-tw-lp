.class public Landroid/support/v7/app/u;
.super Landroid/support/v4/app/DialogFragment;
.source "MediaRouteChooserDialogFragment.java"


# instance fields
.field private final ARGUMENT_SELECTOR:Ljava/lang/String;

.field private mSelector:Landroid/support/v7/media/e;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 43
    invoke-direct {p0}, Landroid/support/v4/app/DialogFragment;-><init>()V

    .line 33
    const-string/jumbo v0, "selector"

    iput-object v0, p0, Landroid/support/v7/app/u;->ARGUMENT_SELECTOR:Ljava/lang/String;

    .line 44
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Landroid/support/v7/app/u;->setCancelable(Z)V

    .line 45
    return-void
.end method

.method private ensureRouteSelector()V
    .locals 2

    .prologue
    .line 58
    iget-object v0, p0, Landroid/support/v7/app/u;->mSelector:Landroid/support/v7/media/e;

    if-nez v0, :cond_1

    .line 59
    invoke-virtual {p0}, Landroid/support/v7/app/u;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 60
    if-eqz v0, :cond_0

    .line 61
    const-string/jumbo v1, "selector"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    invoke-static {v0}, Landroid/support/v7/media/e;->a(Landroid/os/Bundle;)Landroid/support/v7/media/e;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v7/app/u;->mSelector:Landroid/support/v7/media/e;

    .line 63
    :cond_0
    iget-object v0, p0, Landroid/support/v7/app/u;->mSelector:Landroid/support/v7/media/e;

    if-nez v0, :cond_1

    .line 64
    sget-object v0, Landroid/support/v7/media/e;->a:Landroid/support/v7/media/e;

    iput-object v0, p0, Landroid/support/v7/app/u;->mSelector:Landroid/support/v7/media/e;

    .line 67
    :cond_1
    return-void
.end method


# virtual methods
.method public getRouteSelector()Landroid/support/v7/media/e;
    .locals 1

    .prologue
    .line 53
    invoke-direct {p0}, Landroid/support/v7/app/u;->ensureRouteSelector()V

    .line 54
    iget-object v0, p0, Landroid/support/v7/app/u;->mSelector:Landroid/support/v7/media/e;

    return-object v0
.end method

.method public onCreateChooserDialog(Landroid/content/Context;Landroid/os/Bundle;)Landroid/support/v7/app/q;
    .locals 1

    .prologue
    .line 106
    new-instance v0, Landroid/support/v7/app/q;

    invoke-direct {v0, p1}, Landroid/support/v7/app/q;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 2

    .prologue
    .line 111
    invoke-virtual {p0}, Landroid/support/v7/app/u;->getActivity()Landroid/support/v4/app/k;

    move-result-object v0

    invoke-virtual {p0, v0, p1}, Landroid/support/v7/app/u;->onCreateChooserDialog(Landroid/content/Context;Landroid/os/Bundle;)Landroid/support/v7/app/q;

    move-result-object v0

    .line 112
    invoke-virtual {p0}, Landroid/support/v7/app/u;->getRouteSelector()Landroid/support/v7/media/e;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v7/app/q;->setRouteSelector(Landroid/support/v7/media/e;)V

    .line 113
    return-object v0
.end method

.method public setRouteSelector(Landroid/support/v7/media/e;)V
    .locals 3

    .prologue
    .line 76
    if-nez p1, :cond_0

    .line 77
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "selector must not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 80
    :cond_0
    invoke-direct {p0}, Landroid/support/v7/app/u;->ensureRouteSelector()V

    .line 81
    iget-object v0, p0, Landroid/support/v7/app/u;->mSelector:Landroid/support/v7/media/e;

    invoke-virtual {v0, p1}, Landroid/support/v7/media/e;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 82
    iput-object p1, p0, Landroid/support/v7/app/u;->mSelector:Landroid/support/v7/media/e;

    .line 84
    invoke-virtual {p0}, Landroid/support/v7/app/u;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 85
    if-nez v0, :cond_1

    .line 86
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 88
    :cond_1
    const-string/jumbo v1, "selector"

    invoke-virtual {p1}, Landroid/support/v7/media/e;->d()Landroid/os/Bundle;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 89
    invoke-virtual {p0, v0}, Landroid/support/v7/app/u;->setArguments(Landroid/os/Bundle;)V

    .line 91
    invoke-virtual {p0}, Landroid/support/v7/app/u;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    check-cast v0, Landroid/support/v7/app/q;

    .line 92
    if-eqz v0, :cond_2

    .line 93
    invoke-virtual {v0, p1}, Landroid/support/v7/app/q;->setRouteSelector(Landroid/support/v7/media/e;)V

    .line 96
    :cond_2
    return-void
.end method
