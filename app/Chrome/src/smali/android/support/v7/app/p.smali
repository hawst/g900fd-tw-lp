.class final Landroid/support/v7/app/p;
.super Landroid/support/v7/media/g;
.source "MediaRouteButton.java"


# instance fields
.field private synthetic a:Landroid/support/v7/app/o;


# direct methods
.method private constructor <init>(Landroid/support/v7/app/o;)V
    .locals 0

    .prologue
    .line 512
    iput-object p1, p0, Landroid/support/v7/app/p;->a:Landroid/support/v7/app/o;

    invoke-direct {p0}, Landroid/support/v7/media/g;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Landroid/support/v7/app/o;B)V
    .locals 0

    .prologue
    .line 512
    invoke-direct {p0, p1}, Landroid/support/v7/app/p;-><init>(Landroid/support/v7/app/o;)V

    return-void
.end method


# virtual methods
.method public final onProviderAdded(Landroid/support/v7/media/MediaRouter;Landroid/support/v7/media/m;)V
    .locals 1

    .prologue
    .line 540
    iget-object v0, p0, Landroid/support/v7/app/p;->a:Landroid/support/v7/app/o;

    # invokes: Landroid/support/v7/app/o;->refreshRoute()V
    invoke-static {v0}, Landroid/support/v7/app/o;->access$100(Landroid/support/v7/app/o;)V

    .line 541
    return-void
.end method

.method public final onProviderChanged(Landroid/support/v7/media/MediaRouter;Landroid/support/v7/media/m;)V
    .locals 1

    .prologue
    .line 550
    iget-object v0, p0, Landroid/support/v7/app/p;->a:Landroid/support/v7/app/o;

    # invokes: Landroid/support/v7/app/o;->refreshRoute()V
    invoke-static {v0}, Landroid/support/v7/app/o;->access$100(Landroid/support/v7/app/o;)V

    .line 551
    return-void
.end method

.method public final onProviderRemoved(Landroid/support/v7/media/MediaRouter;Landroid/support/v7/media/m;)V
    .locals 1

    .prologue
    .line 545
    iget-object v0, p0, Landroid/support/v7/app/p;->a:Landroid/support/v7/app/o;

    # invokes: Landroid/support/v7/app/o;->refreshRoute()V
    invoke-static {v0}, Landroid/support/v7/app/o;->access$100(Landroid/support/v7/app/o;)V

    .line 546
    return-void
.end method

.method public final onRouteAdded(Landroid/support/v7/media/MediaRouter;Landroid/support/v7/media/MediaRouter$RouteInfo;)V
    .locals 1

    .prologue
    .line 515
    iget-object v0, p0, Landroid/support/v7/app/p;->a:Landroid/support/v7/app/o;

    # invokes: Landroid/support/v7/app/o;->refreshRoute()V
    invoke-static {v0}, Landroid/support/v7/app/o;->access$100(Landroid/support/v7/app/o;)V

    .line 516
    return-void
.end method

.method public final onRouteChanged(Landroid/support/v7/media/MediaRouter;Landroid/support/v7/media/MediaRouter$RouteInfo;)V
    .locals 1

    .prologue
    .line 525
    iget-object v0, p0, Landroid/support/v7/app/p;->a:Landroid/support/v7/app/o;

    # invokes: Landroid/support/v7/app/o;->refreshRoute()V
    invoke-static {v0}, Landroid/support/v7/app/o;->access$100(Landroid/support/v7/app/o;)V

    .line 526
    return-void
.end method

.method public final onRouteRemoved(Landroid/support/v7/media/MediaRouter;Landroid/support/v7/media/MediaRouter$RouteInfo;)V
    .locals 1

    .prologue
    .line 520
    iget-object v0, p0, Landroid/support/v7/app/p;->a:Landroid/support/v7/app/o;

    # invokes: Landroid/support/v7/app/o;->refreshRoute()V
    invoke-static {v0}, Landroid/support/v7/app/o;->access$100(Landroid/support/v7/app/o;)V

    .line 521
    return-void
.end method

.method public final onRouteSelected(Landroid/support/v7/media/MediaRouter;Landroid/support/v7/media/MediaRouter$RouteInfo;)V
    .locals 1

    .prologue
    .line 530
    iget-object v0, p0, Landroid/support/v7/app/p;->a:Landroid/support/v7/app/o;

    # invokes: Landroid/support/v7/app/o;->refreshRoute()V
    invoke-static {v0}, Landroid/support/v7/app/o;->access$100(Landroid/support/v7/app/o;)V

    .line 531
    return-void
.end method

.method public final onRouteUnselected(Landroid/support/v7/media/MediaRouter;Landroid/support/v7/media/MediaRouter$RouteInfo;)V
    .locals 1

    .prologue
    .line 535
    iget-object v0, p0, Landroid/support/v7/app/p;->a:Landroid/support/v7/app/o;

    # invokes: Landroid/support/v7/app/o;->refreshRoute()V
    invoke-static {v0}, Landroid/support/v7/app/o;->access$100(Landroid/support/v7/app/o;)V

    .line 536
    return-void
.end method
