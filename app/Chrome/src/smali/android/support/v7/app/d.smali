.class abstract Landroid/support/v7/app/d;
.super Ljava/lang/Object;
.source "ActionBarActivityDelegate.java"


# instance fields
.field final a:Landroid/support/v7/app/c;

.field b:Z

.field c:Z

.field d:Z

.field e:Z

.field private f:Landroid/support/v7/app/ActionBar;

.field private g:Landroid/view/MenuInflater;

.field private h:Landroid/support/v7/internal/a/a;

.field private i:Landroid/support/v7/internal/a/a;

.field private j:Z


# direct methods
.method constructor <init>(Landroid/support/v7/app/c;)V
    .locals 1

    .prologue
    .line 111
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 71
    new-instance v0, Landroid/support/v7/app/e;

    invoke-direct {v0, p0}, Landroid/support/v7/app/e;-><init>(Landroid/support/v7/app/d;)V

    iput-object v0, p0, Landroid/support/v7/app/d;->h:Landroid/support/v7/internal/a/a;

    .line 112
    iput-object p1, p0, Landroid/support/v7/app/d;->a:Landroid/support/v7/app/c;

    .line 113
    iget-object v0, p0, Landroid/support/v7/app/d;->h:Landroid/support/v7/internal/a/a;

    iput-object v0, p0, Landroid/support/v7/app/d;->i:Landroid/support/v7/internal/a/a;

    .line 114
    return-void
.end method


# virtual methods
.method abstract a()Landroid/support/v7/app/ActionBar;
.end method

.method abstract a(Ljava/lang/String;Landroid/util/AttributeSet;)Landroid/view/View;
.end method

.method abstract a(I)V
.end method

.method abstract a(ILandroid/view/Menu;)V
.end method

.method abstract a(Landroid/content/res/Configuration;)V
.end method

.method a(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 143
    iget-object v0, p0, Landroid/support/v7/app/d;->a:Landroid/support/v7/app/c;

    sget-object v1, Landroid/support/v7/a/a;->n:[I

    invoke-virtual {v0, v1}, Landroid/support/v7/app/c;->obtainStyledAttributes([I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 145
    invoke-virtual {v0, v3}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 146
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 147
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "You need to use a Theme.AppCompat theme (or descendant) with this activity."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 151
    :cond_0
    invoke-virtual {v0, v3, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, Landroid/support/v7/app/d;->b:Z

    .line 152
    const/4 v1, 0x2

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, Landroid/support/v7/app/d;->c:Z

    .line 153
    const/4 v1, 0x3

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, Landroid/support/v7/app/d;->d:Z

    .line 154
    invoke-virtual {v0, v2, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, Landroid/support/v7/app/d;->e:Z

    .line 155
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 156
    return-void
.end method

.method abstract a(Landroid/view/View;)V
.end method

.method abstract a(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
.end method

.method abstract a(Ljava/lang/CharSequence;)V
.end method

.method a(ILandroid/view/KeyEvent;)Z
    .locals 1

    .prologue
    .line 212
    const/4 v0, 0x0

    return v0
.end method

.method abstract a(ILandroid/view/View;Landroid/view/Menu;)Z
.end method

.method abstract a(Landroid/view/KeyEvent;)Z
.end method

.method final b()Landroid/support/v7/app/ActionBar;
    .locals 1

    .prologue
    .line 121
    iget-boolean v0, p0, Landroid/support/v7/app/d;->b:Z

    if-eqz v0, :cond_0

    .line 122
    iget-object v0, p0, Landroid/support/v7/app/d;->f:Landroid/support/v7/app/ActionBar;

    if-nez v0, :cond_0

    .line 123
    invoke-virtual {p0}, Landroid/support/v7/app/d;->a()Landroid/support/v7/app/ActionBar;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v7/app/d;->f:Landroid/support/v7/app/ActionBar;

    .line 126
    :cond_0
    iget-object v0, p0, Landroid/support/v7/app/d;->f:Landroid/support/v7/app/ActionBar;

    return-object v0
.end method

.method abstract b(I)Landroid/view/View;
.end method

.method abstract b(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
.end method

.method abstract b(ILandroid/view/Menu;)Z
.end method

.method final c()Landroid/view/MenuInflater;
    .locals 2

    .prologue
    .line 136
    iget-object v0, p0, Landroid/support/v7/app/d;->g:Landroid/view/MenuInflater;

    if-nez v0, :cond_0

    .line 137
    new-instance v0, Landroid/support/v7/internal/view/d;

    invoke-virtual {p0}, Landroid/support/v7/app/d;->h()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/support/v7/internal/view/d;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Landroid/support/v7/app/d;->g:Landroid/view/MenuInflater;

    .line 139
    :cond_0
    iget-object v0, p0, Landroid/support/v7/app/d;->g:Landroid/view/MenuInflater;

    return-object v0
.end method

.method abstract c(ILandroid/view/Menu;)Z
.end method

.method abstract d()V
.end method

.method abstract e()V
.end method

.method abstract f()V
.end method

.method abstract g()Z
.end method

.method protected final h()Landroid/content/Context;
    .locals 2

    .prologue
    .line 248
    const/4 v0, 0x0

    .line 251
    invoke-virtual {p0}, Landroid/support/v7/app/d;->b()Landroid/support/v7/app/ActionBar;

    move-result-object v1

    .line 252
    if-eqz v1, :cond_0

    .line 253
    invoke-virtual {v1}, Landroid/support/v7/app/ActionBar;->b()Landroid/content/Context;

    move-result-object v0

    .line 256
    :cond_0
    if-nez v0, :cond_1

    .line 257
    iget-object v0, p0, Landroid/support/v7/app/d;->a:Landroid/support/v7/app/c;

    .line 259
    :cond_1
    return-object v0
.end method

.method final i()Landroid/support/v7/internal/a/a;
    .locals 1

    .prologue
    .line 310
    iget-object v0, p0, Landroid/support/v7/app/d;->i:Landroid/support/v7/internal/a/a;

    return-object v0
.end method

.method final j()V
    .locals 1

    .prologue
    .line 314
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v7/app/d;->j:Z

    .line 315
    return-void
.end method

.method final k()Z
    .locals 1

    .prologue
    .line 318
    iget-boolean v0, p0, Landroid/support/v7/app/d;->j:Z

    return v0
.end method
