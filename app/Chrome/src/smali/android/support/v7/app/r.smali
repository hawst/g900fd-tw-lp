.class final Landroid/support/v7/app/r;
.super Landroid/support/v7/media/g;
.source "MediaRouteChooserDialog.java"


# instance fields
.field private synthetic a:Landroid/support/v7/app/q;


# direct methods
.method private constructor <init>(Landroid/support/v7/app/q;)V
    .locals 0

    .prologue
    .line 240
    iput-object p1, p0, Landroid/support/v7/app/r;->a:Landroid/support/v7/app/q;

    invoke-direct {p0}, Landroid/support/v7/media/g;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Landroid/support/v7/app/q;B)V
    .locals 0

    .prologue
    .line 240
    invoke-direct {p0, p1}, Landroid/support/v7/app/r;-><init>(Landroid/support/v7/app/q;)V

    return-void
.end method


# virtual methods
.method public final onRouteAdded(Landroid/support/v7/media/MediaRouter;Landroid/support/v7/media/MediaRouter$RouteInfo;)V
    .locals 1

    .prologue
    .line 243
    iget-object v0, p0, Landroid/support/v7/app/r;->a:Landroid/support/v7/app/q;

    invoke-virtual {v0}, Landroid/support/v7/app/q;->refreshRoutes()V

    .line 244
    return-void
.end method

.method public final onRouteChanged(Landroid/support/v7/media/MediaRouter;Landroid/support/v7/media/MediaRouter$RouteInfo;)V
    .locals 1

    .prologue
    .line 253
    iget-object v0, p0, Landroid/support/v7/app/r;->a:Landroid/support/v7/app/q;

    invoke-virtual {v0}, Landroid/support/v7/app/q;->refreshRoutes()V

    .line 254
    return-void
.end method

.method public final onRouteRemoved(Landroid/support/v7/media/MediaRouter;Landroid/support/v7/media/MediaRouter$RouteInfo;)V
    .locals 1

    .prologue
    .line 248
    iget-object v0, p0, Landroid/support/v7/app/r;->a:Landroid/support/v7/app/q;

    invoke-virtual {v0}, Landroid/support/v7/app/q;->refreshRoutes()V

    .line 249
    return-void
.end method

.method public final onRouteSelected(Landroid/support/v7/media/MediaRouter;Landroid/support/v7/media/MediaRouter$RouteInfo;)V
    .locals 1

    .prologue
    .line 258
    iget-object v0, p0, Landroid/support/v7/app/r;->a:Landroid/support/v7/app/q;

    invoke-virtual {v0}, Landroid/support/v7/app/q;->dismiss()V

    .line 259
    return-void
.end method
