.class Landroid/support/v7/app/e;
.super Ljava/lang/Object;
.source "MediaRouterThemeHelper.java"

# interfaces
.implements Landroid/support/v7/internal/a/a;


# instance fields
.field private synthetic a:Landroid/support/v7/app/d;


# direct methods
.method constructor <init>(Landroid/support/v7/app/d;)V
    .locals 0

    .prologue
    .line 71
    iput-object p1, p0, Landroid/support/v7/app/e;->a:Landroid/support/v7/app/d;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;I)I
    .locals 3

    .prologue
    .line 40
    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    .line 41
    invoke-virtual {p0}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, p1, v0, v2}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    iget v0, v0, Landroid/util/TypedValue;->resourceId:I

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Z)Landroid/content/Context;
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 30
    new-instance v2, Landroid/util/TypedValue;

    invoke-direct {v2}, Landroid/util/TypedValue;-><init>()V

    invoke-virtual {p0}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v3

    sget v4, Landroid/support/v7/mediarouter/R$attr;->isLightTheme:I

    invoke-virtual {v3, v4, v2, v1}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    move-result v3

    if-eqz v3, :cond_0

    iget v2, v2, Landroid/util/TypedValue;->data:I

    if-eqz v2, :cond_0

    .line 31
    :goto_0
    if-eqz v1, :cond_2

    if-eqz p1, :cond_2

    .line 32
    new-instance v1, Landroid/view/ContextThemeWrapper;

    sget v2, Landroid/support/v7/mediarouter/R$style;->Theme_AppCompat:I

    invoke-direct {v1, p0, v2}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    move-object p0, v1

    .line 35
    :goto_1
    new-instance v1, Landroid/view/ContextThemeWrapper;

    if-eqz v0, :cond_1

    sget v0, Landroid/support/v7/mediarouter/R$style;->Theme_MediaRouter_Light:I

    :goto_2
    invoke-direct {v1, p0, v0}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    return-object v1

    :cond_0
    move v1, v0

    .line 30
    goto :goto_0

    .line 35
    :cond_1
    sget v0, Landroid/support/v7/mediarouter/R$style;->Theme_MediaRouter:I

    goto :goto_2

    :cond_2
    move v0, v1

    goto :goto_1
.end method

.method public static b(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;
    .locals 2

    .prologue
    .line 45
    invoke-static {p0, p1}, Landroid/support/v7/app/e;->a(Landroid/content/Context;I)I

    move-result v0

    .line 46
    if-eqz v0, :cond_0

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(ILandroid/view/Menu;)Z
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Landroid/support/v7/app/e;->a:Landroid/support/v7/app/d;

    iget-object v0, v0, Landroid/support/v7/app/d;->a:Landroid/support/v7/app/c;

    invoke-virtual {v0, p1, p2}, Landroid/support/v7/app/c;->a(ILandroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method public final a(ILandroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Landroid/support/v7/app/e;->a:Landroid/support/v7/app/d;

    iget-object v0, v0, Landroid/support/v7/app/d;->a:Landroid/support/v7/app/c;

    invoke-virtual {v0, p1, p2}, Landroid/support/v7/app/c;->onMenuItemSelected(ILandroid/view/MenuItem;)Z

    move-result v0

    return v0
.end method

.method public final a(ILandroid/view/View;Landroid/view/Menu;)Z
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Landroid/support/v7/app/e;->a:Landroid/support/v7/app/d;

    iget-object v0, v0, Landroid/support/v7/app/d;->a:Landroid/support/v7/app/c;

    invoke-virtual {v0, p1, p2, p3}, Landroid/support/v7/app/c;->a(ILandroid/view/View;Landroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method public final b(ILandroid/view/Menu;)V
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Landroid/support/v7/app/e;->a:Landroid/support/v7/app/d;

    iget-object v0, v0, Landroid/support/v7/app/d;->a:Landroid/support/v7/app/c;

    invoke-virtual {v0, p1, p2}, Landroid/support/v7/app/c;->onPanelClosed(ILandroid/view/Menu;)V

    .line 90
    return-void
.end method

.method public final c(ILandroid/view/Menu;)Z
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Landroid/support/v7/app/e;->a:Landroid/support/v7/app/d;

    iget-object v0, v0, Landroid/support/v7/app/d;->a:Landroid/support/v7/app/c;

    invoke-virtual {v0, p1, p2}, Landroid/support/v7/app/c;->onMenuOpened(ILandroid/view/Menu;)Z

    move-result v0

    return v0
.end method
