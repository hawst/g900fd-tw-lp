.class final Landroid/support/v7/app/w;
.super Ljava/lang/Object;
.source "MediaRouteControllerDialog.java"

# interfaces
.implements Landroid/widget/SeekBar$OnSeekBarChangeListener;


# instance fields
.field final synthetic a:Landroid/support/v7/app/v;

.field private final b:Ljava/lang/Runnable;


# direct methods
.method constructor <init>(Landroid/support/v7/app/v;)V
    .locals 1

    .prologue
    .line 144
    iput-object p1, p0, Landroid/support/v7/app/w;->a:Landroid/support/v7/app/v;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 145
    new-instance v0, Landroid/support/v7/app/x;

    invoke-direct {v0, p0}, Landroid/support/v7/app/x;-><init>(Landroid/support/v7/app/w;)V

    iput-object v0, p0, Landroid/support/v7/app/w;->b:Ljava/lang/Runnable;

    return-void
.end method


# virtual methods
.method public final onProgressChanged(Landroid/widget/SeekBar;IZ)V
    .locals 1

    .prologue
    .line 174
    if-eqz p3, :cond_0

    .line 175
    iget-object v0, p0, Landroid/support/v7/app/w;->a:Landroid/support/v7/app/v;

    invoke-static {v0}, Landroid/support/v7/app/v;->d(Landroid/support/v7/app/v;)Landroid/support/v7/media/MediaRouter$RouteInfo;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/support/v7/media/MediaRouter$RouteInfo;->a(I)V

    .line 177
    :cond_0
    return-void
.end method

.method public final onStartTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 2

    .prologue
    .line 157
    iget-object v0, p0, Landroid/support/v7/app/w;->a:Landroid/support/v7/app/v;

    invoke-static {v0}, Landroid/support/v7/app/v;->a(Landroid/support/v7/app/v;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 158
    iget-object v0, p0, Landroid/support/v7/app/w;->a:Landroid/support/v7/app/v;

    invoke-static {v0}, Landroid/support/v7/app/v;->c(Landroid/support/v7/app/v;)Landroid/widget/SeekBar;

    move-result-object v0

    iget-object v1, p0, Landroid/support/v7/app/w;->b:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 162
    :goto_0
    return-void

    .line 160
    :cond_0
    iget-object v0, p0, Landroid/support/v7/app/w;->a:Landroid/support/v7/app/v;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Landroid/support/v7/app/v;->a(Landroid/support/v7/app/v;Z)Z

    goto :goto_0
.end method

.method public final onStopTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 4

    .prologue
    .line 169
    iget-object v0, p0, Landroid/support/v7/app/w;->a:Landroid/support/v7/app/v;

    invoke-static {v0}, Landroid/support/v7/app/v;->c(Landroid/support/v7/app/v;)Landroid/widget/SeekBar;

    move-result-object v0

    iget-object v1, p0, Landroid/support/v7/app/w;->b:Ljava/lang/Runnable;

    const-wide/16 v2, 0xfa

    invoke-virtual {v0, v1, v2, v3}, Landroid/widget/SeekBar;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 170
    return-void
.end method
