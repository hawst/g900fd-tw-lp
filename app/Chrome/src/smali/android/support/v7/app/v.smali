.class public final Landroid/support/v7/app/v;
.super Landroid/app/Dialog;
.source "MediaRouteControllerDialog.java"


# instance fields
.field private final a:Landroid/support/v7/media/MediaRouter;

.field private final b:Landroid/support/v7/app/z;

.field private final c:Landroid/support/v7/media/MediaRouter$RouteInfo;

.field private d:Landroid/graphics/drawable/Drawable;

.field private e:Landroid/graphics/drawable/Drawable;

.field private f:Landroid/graphics/drawable/Drawable;

.field private g:Z

.field private h:Landroid/widget/LinearLayout;

.field private i:Landroid/widget/SeekBar;

.field private j:Z

.field private k:Landroid/view/View;

.field private l:Landroid/widget/Button;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 70
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Landroid/support/v7/app/v;-><init>(Landroid/content/Context;I)V

    .line 71
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;I)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 74
    invoke-static {p1, v2}, Landroid/support/v7/app/e;->a(Landroid/content/Context;Z)Landroid/content/Context;

    move-result-object v0

    invoke-direct {p0, v0, v1}, Landroid/app/Dialog;-><init>(Landroid/content/Context;I)V

    .line 60
    iput-boolean v2, p0, Landroid/support/v7/app/v;->g:Z

    .line 75
    invoke-virtual {p0}, Landroid/support/v7/app/v;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 77
    invoke-static {v0}, Landroid/support/v7/media/MediaRouter;->a(Landroid/content/Context;)Landroid/support/v7/media/MediaRouter;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v7/app/v;->a:Landroid/support/v7/media/MediaRouter;

    .line 78
    new-instance v0, Landroid/support/v7/app/z;

    invoke-direct {v0, p0, v1}, Landroid/support/v7/app/z;-><init>(Landroid/support/v7/app/v;B)V

    iput-object v0, p0, Landroid/support/v7/app/v;->b:Landroid/support/v7/app/z;

    .line 79
    iget-object v0, p0, Landroid/support/v7/app/v;->a:Landroid/support/v7/media/MediaRouter;

    invoke-static {}, Landroid/support/v7/media/MediaRouter;->c()Landroid/support/v7/media/MediaRouter$RouteInfo;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v7/app/v;->c:Landroid/support/v7/media/MediaRouter$RouteInfo;

    .line 80
    return-void
.end method

.method private a()Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 242
    iget-object v0, p0, Landroid/support/v7/app/v;->c:Landroid/support/v7/media/MediaRouter$RouteInfo;

    invoke-virtual {v0}, Landroid/support/v7/media/MediaRouter$RouteInfo;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/app/v;->c:Landroid/support/v7/media/MediaRouter$RouteInfo;

    invoke-virtual {v0}, Landroid/support/v7/media/MediaRouter$RouteInfo;->g()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 243
    :cond_0
    invoke-virtual {p0}, Landroid/support/v7/app/v;->dismiss()V

    move v0, v2

    .line 261
    :goto_0
    return v0

    .line 247
    :cond_1
    iget-object v0, p0, Landroid/support/v7/app/v;->c:Landroid/support/v7/media/MediaRouter$RouteInfo;

    invoke-virtual {v0}, Landroid/support/v7/media/MediaRouter$RouteInfo;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/support/v7/app/v;->setTitle(Ljava/lang/CharSequence;)V

    .line 248
    invoke-direct {p0}, Landroid/support/v7/app/v;->b()V

    .line 250
    iget-object v0, p0, Landroid/support/v7/app/v;->c:Landroid/support/v7/media/MediaRouter$RouteInfo;

    invoke-virtual {v0}, Landroid/support/v7/media/MediaRouter$RouteInfo;->e()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Landroid/support/v7/app/v;->d:Landroid/graphics/drawable/Drawable;

    if-nez v0, :cond_2

    invoke-virtual {p0}, Landroid/support/v7/app/v;->getContext()Landroid/content/Context;

    move-result-object v0

    sget v3, Landroid/support/v7/mediarouter/R$attr;->mediaRouteConnectingDrawable:I

    invoke-static {v0, v3}, Landroid/support/v7/app/e;->b(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v7/app/v;->d:Landroid/graphics/drawable/Drawable;

    :cond_2
    iget-object v0, p0, Landroid/support/v7/app/v;->d:Landroid/graphics/drawable/Drawable;

    .line 251
    :goto_1
    iget-object v3, p0, Landroid/support/v7/app/v;->f:Landroid/graphics/drawable/Drawable;

    if-eq v0, v3, :cond_3

    .line 252
    iput-object v0, p0, Landroid/support/v7/app/v;->f:Landroid/graphics/drawable/Drawable;

    .line 258
    invoke-virtual {v0, v2, v1}, Landroid/graphics/drawable/Drawable;->setVisible(ZZ)Z

    .line 259
    invoke-virtual {p0}, Landroid/support/v7/app/v;->getWindow()Landroid/view/Window;

    move-result-object v2

    const/4 v3, 0x3

    invoke-virtual {v2, v3, v0}, Landroid/view/Window;->setFeatureDrawable(ILandroid/graphics/drawable/Drawable;)V

    :cond_3
    move v0, v1

    .line 261
    goto :goto_0

    .line 250
    :cond_4
    iget-object v0, p0, Landroid/support/v7/app/v;->e:Landroid/graphics/drawable/Drawable;

    if-nez v0, :cond_5

    invoke-virtual {p0}, Landroid/support/v7/app/v;->getContext()Landroid/content/Context;

    move-result-object v0

    sget v3, Landroid/support/v7/mediarouter/R$attr;->mediaRouteOnDrawable:I

    invoke-static {v0, v3}, Landroid/support/v7/app/e;->b(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v7/app/v;->e:Landroid/graphics/drawable/Drawable;

    :cond_5
    iget-object v0, p0, Landroid/support/v7/app/v;->e:Landroid/graphics/drawable/Drawable;

    goto :goto_1
.end method

.method static synthetic a(Landroid/support/v7/app/v;)Z
    .locals 1

    .prologue
    .line 43
    iget-boolean v0, p0, Landroid/support/v7/app/v;->j:Z

    return v0
.end method

.method static synthetic a(Landroid/support/v7/app/v;Z)Z
    .locals 0

    .prologue
    .line 43
    iput-boolean p1, p0, Landroid/support/v7/app/v;->j:Z

    return p1
.end method

.method private b()V
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 281
    iget-boolean v2, p0, Landroid/support/v7/app/v;->j:Z

    if-nez v2, :cond_0

    .line 282
    iget-boolean v2, p0, Landroid/support/v7/app/v;->g:Z

    if-eqz v2, :cond_1

    iget-object v2, p0, Landroid/support/v7/app/v;->c:Landroid/support/v7/media/MediaRouter$RouteInfo;

    invoke-virtual {v2}, Landroid/support/v7/media/MediaRouter$RouteInfo;->j()I

    move-result v2

    if-ne v2, v0, :cond_1

    :goto_0
    if-eqz v0, :cond_2

    .line 283
    iget-object v0, p0, Landroid/support/v7/app/v;->h:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 284
    iget-object v0, p0, Landroid/support/v7/app/v;->i:Landroid/widget/SeekBar;

    iget-object v1, p0, Landroid/support/v7/app/v;->c:Landroid/support/v7/media/MediaRouter$RouteInfo;

    invoke-virtual {v1}, Landroid/support/v7/media/MediaRouter$RouteInfo;->l()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setMax(I)V

    .line 285
    iget-object v0, p0, Landroid/support/v7/app/v;->i:Landroid/widget/SeekBar;

    iget-object v1, p0, Landroid/support/v7/app/v;->c:Landroid/support/v7/media/MediaRouter$RouteInfo;

    invoke-virtual {v1}, Landroid/support/v7/media/MediaRouter$RouteInfo;->k()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 290
    :cond_0
    :goto_1
    return-void

    :cond_1
    move v0, v1

    .line 282
    goto :goto_0

    .line 287
    :cond_2
    iget-object v0, p0, Landroid/support/v7/app/v;->h:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_1
.end method

.method static synthetic b(Landroid/support/v7/app/v;)V
    .locals 0

    .prologue
    .line 43
    invoke-direct {p0}, Landroid/support/v7/app/v;->b()V

    return-void
.end method

.method static synthetic c(Landroid/support/v7/app/v;)Landroid/widget/SeekBar;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Landroid/support/v7/app/v;->i:Landroid/widget/SeekBar;

    return-object v0
.end method

.method static synthetic d(Landroid/support/v7/app/v;)Landroid/support/v7/media/MediaRouter$RouteInfo;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Landroid/support/v7/app/v;->c:Landroid/support/v7/media/MediaRouter$RouteInfo;

    return-object v0
.end method

.method static synthetic e(Landroid/support/v7/app/v;)Landroid/support/v7/media/MediaRouter;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Landroid/support/v7/app/v;->a:Landroid/support/v7/media/MediaRouter;

    return-object v0
.end method

.method static synthetic f(Landroid/support/v7/app/v;)Z
    .locals 1

    .prologue
    .line 43
    invoke-direct {p0}, Landroid/support/v7/app/v;->a()Z

    move-result v0

    return v0
.end method


# virtual methods
.method public final onAttachedToWindow()V
    .locals 4

    .prologue
    .line 208
    invoke-super {p0}, Landroid/app/Dialog;->onAttachedToWindow()V

    .line 210
    iget-object v0, p0, Landroid/support/v7/app/v;->a:Landroid/support/v7/media/MediaRouter;

    sget-object v1, Landroid/support/v7/media/e;->a:Landroid/support/v7/media/e;

    iget-object v2, p0, Landroid/support/v7/app/v;->b:Landroid/support/v7/app/z;

    const/4 v3, 0x2

    invoke-virtual {v0, v1, v2, v3}, Landroid/support/v7/media/MediaRouter;->a(Landroid/support/v7/media/e;Landroid/support/v7/media/g;I)V

    .line 212
    invoke-direct {p0}, Landroid/support/v7/app/v;->a()Z

    .line 213
    return-void
.end method

.method protected final onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 136
    invoke-super {p0, p1}, Landroid/app/Dialog;->onCreate(Landroid/os/Bundle;)V

    .line 138
    invoke-virtual {p0}, Landroid/support/v7/app/v;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/view/Window;->requestFeature(I)Z

    .line 140
    sget v0, Landroid/support/v7/mediarouter/R$layout;->mr_media_route_controller_dialog:I

    invoke-virtual {p0, v0}, Landroid/support/v7/app/v;->setContentView(I)V

    .line 142
    sget v0, Landroid/support/v7/mediarouter/R$id;->media_route_volume_layout:I

    invoke-virtual {p0, v0}, Landroid/support/v7/app/v;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Landroid/support/v7/app/v;->h:Landroid/widget/LinearLayout;

    .line 143
    sget v0, Landroid/support/v7/mediarouter/R$id;->media_route_volume_slider:I

    invoke-virtual {p0, v0}, Landroid/support/v7/app/v;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/SeekBar;

    iput-object v0, p0, Landroid/support/v7/app/v;->i:Landroid/widget/SeekBar;

    .line 144
    iget-object v0, p0, Landroid/support/v7/app/v;->i:Landroid/widget/SeekBar;

    new-instance v1, Landroid/support/v7/app/w;

    invoke-direct {v1, p0}, Landroid/support/v7/app/w;-><init>(Landroid/support/v7/app/v;)V

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    .line 180
    sget v0, Landroid/support/v7/mediarouter/R$id;->media_route_disconnect_button:I

    invoke-virtual {p0, v0}, Landroid/support/v7/app/v;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Landroid/support/v7/app/v;->l:Landroid/widget/Button;

    .line 181
    iget-object v0, p0, Landroid/support/v7/app/v;->l:Landroid/widget/Button;

    new-instance v1, Landroid/support/v7/app/y;

    invoke-direct {v1, p0}, Landroid/support/v7/app/y;-><init>(Landroid/support/v7/app/v;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 191
    invoke-direct {p0}, Landroid/support/v7/app/v;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 193
    const/4 v0, 0x0

    iput-object v0, p0, Landroid/support/v7/app/v;->k:Landroid/view/View;

    .line 194
    sget v0, Landroid/support/v7/mediarouter/R$id;->media_route_control_frame:I

    invoke-virtual {p0, v0}, Landroid/support/v7/app/v;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    .line 196
    iget-object v1, p0, Landroid/support/v7/app/v;->k:Landroid/view/View;

    if-eqz v1, :cond_1

    .line 197
    iget-object v1, p0, Landroid/support/v7/app/v;->k:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 198
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 203
    :cond_0
    :goto_0
    return-void

    .line 200
    :cond_1
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    goto :goto_0
.end method

.method public final onDetachedFromWindow()V
    .locals 2

    .prologue
    .line 217
    iget-object v0, p0, Landroid/support/v7/app/v;->a:Landroid/support/v7/media/MediaRouter;

    iget-object v1, p0, Landroid/support/v7/app/v;->b:Landroid/support/v7/app/z;

    invoke-virtual {v0, v1}, Landroid/support/v7/media/MediaRouter;->a(Landroid/support/v7/media/g;)V

    .line 219
    invoke-super {p0}, Landroid/app/Dialog;->onDetachedFromWindow()V

    .line 220
    return-void
.end method

.method public final onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 4

    .prologue
    const/16 v3, 0x19

    const/4 v1, 0x1

    .line 224
    if-eq p1, v3, :cond_0

    const/16 v0, 0x18

    if-ne p1, v0, :cond_2

    .line 226
    :cond_0
    iget-object v2, p0, Landroid/support/v7/app/v;->c:Landroid/support/v7/media/MediaRouter$RouteInfo;

    if-ne p1, v3, :cond_1

    const/4 v0, -0x1

    :goto_0
    invoke-virtual {v2, v0}, Landroid/support/v7/media/MediaRouter$RouteInfo;->b(I)V

    .line 229
    :goto_1
    return v1

    :cond_1
    move v0, v1

    .line 226
    goto :goto_0

    .line 229
    :cond_2
    invoke-super {p0, p1, p2}, Landroid/app/Dialog;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v1

    goto :goto_1
.end method

.method public final onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 1

    .prologue
    .line 234
    const/16 v0, 0x19

    if-eq p1, v0, :cond_0

    const/16 v0, 0x18

    if-ne p1, v0, :cond_1

    .line 236
    :cond_0
    const/4 v0, 0x1

    .line 238
    :goto_0
    return v0

    :cond_1
    invoke-super {p0, p1, p2}, Landroid/app/Dialog;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method
