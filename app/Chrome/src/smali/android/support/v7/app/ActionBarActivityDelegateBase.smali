.class Landroid/support/v7/app/ActionBarActivityDelegateBase;
.super Landroid/support/v7/app/d;
.source "ActionBarActivityDelegateBase.java"

# interfaces
.implements Landroid/support/v7/internal/view/menu/j;


# instance fields
.field f:Landroid/support/v7/b/a;

.field g:Landroid/support/v7/internal/widget/ActionBarContextView;

.field h:Landroid/widget/PopupWindow;

.field i:Ljava/lang/Runnable;

.field private j:Landroid/support/v7/internal/widget/t;

.field private k:Landroid/support/v7/app/j;

.field private l:Landroid/support/v7/app/m;

.field private m:Z

.field private n:Landroid/view/ViewGroup;

.field private o:Landroid/view/ViewGroup;

.field private p:Landroid/view/View;

.field private q:Ljava/lang/CharSequence;

.field private r:Z

.field private s:[Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;

.field private t:Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;

.field private u:Z

.field private v:I

.field private final w:Ljava/lang/Runnable;

.field private x:Z

.field private y:Landroid/graphics/Rect;

.field private z:Landroid/graphics/Rect;


# direct methods
.method constructor <init>(Landroid/support/v7/app/c;)V
    .locals 1

    .prologue
    .line 134
    invoke-direct {p0, p1}, Landroid/support/v7/app/d;-><init>(Landroid/support/v7/app/c;)V

    .line 112
    new-instance v0, Landroid/support/v7/app/f;

    invoke-direct {v0, p0}, Landroid/support/v7/app/f;-><init>(Landroid/support/v7/app/ActionBarActivityDelegateBase;)V

    iput-object v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->w:Ljava/lang/Runnable;

    .line 135
    return-void
.end method

.method static synthetic a(Landroid/support/v7/app/ActionBarActivityDelegateBase;)I
    .locals 1

    .prologue
    .line 80
    iget v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->v:I

    return v0
.end method

.method static synthetic a(Landroid/support/v7/app/ActionBarActivityDelegateBase;Landroid/view/Menu;)Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;
    .locals 1

    .prologue
    .line 80
    invoke-direct {p0, p1}, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a(Landroid/view/Menu;)Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;

    move-result-object v0

    return-object v0
.end method

.method private a(Landroid/view/Menu;)Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 1172
    iget-object v3, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->s:[Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;

    .line 1173
    if-eqz v3, :cond_0

    array-length v0, v3

    :goto_0
    move v2, v1

    .line 1174
    :goto_1
    if-ge v2, v0, :cond_2

    .line 1175
    aget-object v1, v3, v2

    .line 1176
    if-eqz v1, :cond_1

    iget-object v4, v1, Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;->d:Landroid/support/v7/internal/view/menu/i;

    if-ne v4, p1, :cond_1

    move-object v0, v1

    .line 1180
    :goto_2
    return-object v0

    :cond_0
    move v0, v1

    .line 1173
    goto :goto_0

    .line 1174
    :cond_1
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_1

    .line 1180
    :cond_2
    const/4 v0, 0x0

    goto :goto_2
.end method

.method private a(ILandroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;Landroid/view/Menu;)V
    .locals 1

    .prologue
    .line 1150
    if-nez p3, :cond_1

    .line 1152
    if-nez p2, :cond_0

    .line 1153
    if-ltz p1, :cond_0

    iget-object v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->s:[Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;

    array-length v0, v0

    if-ge p1, v0, :cond_0

    .line 1154
    iget-object v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->s:[Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;

    aget-object p2, v0, p1

    .line 1158
    :cond_0
    if-eqz p2, :cond_1

    .line 1160
    iget-object p3, p2, Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;->d:Landroid/support/v7/internal/view/menu/i;

    .line 1165
    :cond_1
    if-eqz p2, :cond_2

    iget-boolean v0, p2, Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;->h:Z

    if-nez v0, :cond_2

    .line 1169
    :goto_0
    return-void

    .line 1168
    :cond_2
    invoke-virtual {p0}, Landroid/support/v7/app/ActionBarActivityDelegateBase;->i()Landroid/support/v7/internal/a/a;

    move-result-object v0

    invoke-interface {v0, p1, p3}, Landroid/support/v7/internal/a/a;->b(ILandroid/view/Menu;)V

    goto :goto_0
.end method

.method private a(Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;Landroid/view/KeyEvent;)V
    .locals 7

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 822
    iget-boolean v0, p1, Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;->h:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Landroid/support/v7/app/ActionBarActivityDelegateBase;->k()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 864
    :cond_0
    :goto_0
    return-void

    .line 828
    :cond_1
    iget v0, p1, Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;->a:I

    if-nez v0, :cond_2

    .line 829
    iget-object v4, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:Landroid/support/v7/app/c;

    .line 830
    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    .line 831
    iget v0, v0, Landroid/content/res/Configuration;->screenLayout:I

    and-int/lit8 v0, v0, 0xf

    const/4 v5, 0x4

    if-ne v0, v5, :cond_3

    move v0, v1

    .line 833
    :goto_1
    invoke-virtual {v4}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v4

    iget v4, v4, Landroid/content/pm/ApplicationInfo;->targetSdkVersion:I

    const/16 v5, 0xb

    if-lt v4, v5, :cond_4

    move v4, v1

    .line 836
    :goto_2
    if-eqz v0, :cond_2

    if-nez v4, :cond_0

    .line 841
    :cond_2
    invoke-virtual {p0}, Landroid/support/v7/app/ActionBarActivityDelegateBase;->i()Landroid/support/v7/internal/a/a;

    move-result-object v0

    .line 842
    if-eqz v0, :cond_5

    iget v4, p1, Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;->a:I

    iget-object v5, p1, Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;->d:Landroid/support/v7/internal/view/menu/i;

    invoke-interface {v0, v4, v5}, Landroid/support/v7/internal/a/a;->c(ILandroid/view/Menu;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 844
    invoke-direct {p0, p1, v1}, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a(Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;Z)V

    goto :goto_0

    :cond_3
    move v0, v2

    .line 831
    goto :goto_1

    :cond_4
    move v4, v2

    .line 833
    goto :goto_2

    .line 849
    :cond_5
    invoke-direct {p0, p1, v3}, Landroid/support/v7/app/ActionBarActivityDelegateBase;->b(Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;Landroid/view/KeyEvent;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 853
    iget-object v0, p1, Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;->b:Landroid/view/ViewGroup;

    if-eqz v0, :cond_6

    iget-boolean v0, p1, Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;->j:Z

    if-eqz v0, :cond_8

    .line 854
    :cond_6
    iget-object v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->n:Landroid/view/ViewGroup;

    iput-object v0, p1, Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;->b:Landroid/view/ViewGroup;

    invoke-virtual {p0}, Landroid/support/v7/app/ActionBarActivityDelegateBase;->h()Landroid/content/Context;

    move-result-object v0

    new-instance v4, Landroid/util/TypedValue;

    invoke-direct {v4}, Landroid/util/TypedValue;-><init>()V

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/res/Resources;->newTheme()Landroid/content/res/Resources$Theme;

    move-result-object v5

    invoke-virtual {v0}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/content/res/Resources$Theme;->setTo(Landroid/content/res/Resources$Theme;)V

    const v6, 0x7f010052

    invoke-virtual {v5, v6, v4, v1}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    iget v6, v4, Landroid/util/TypedValue;->resourceId:I

    if-eqz v6, :cond_7

    iget v6, v4, Landroid/util/TypedValue;->resourceId:I

    invoke-virtual {v5, v6, v1}, Landroid/content/res/Resources$Theme;->applyStyle(IZ)V

    :cond_7
    const v6, 0x7f01008e

    invoke-virtual {v5, v6, v4, v1}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    iget v6, v4, Landroid/util/TypedValue;->resourceId:I

    if-eqz v6, :cond_b

    iget v4, v4, Landroid/util/TypedValue;->resourceId:I

    invoke-virtual {v5, v4, v1}, Landroid/content/res/Resources$Theme;->applyStyle(IZ)V

    :goto_3
    new-instance v4, Landroid/view/ContextThemeWrapper;

    invoke-direct {v4, v0, v2}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    invoke-virtual {v4}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/content/res/Resources$Theme;->setTo(Landroid/content/res/Resources$Theme;)V

    iput-object v4, p1, Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;->f:Landroid/content/Context;

    .line 858
    :cond_8
    iget-object v0, p1, Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;->d:Landroid/support/v7/internal/view/menu/i;

    if-eqz v0, :cond_e

    iget-object v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->l:Landroid/support/v7/app/m;

    if-nez v0, :cond_9

    new-instance v0, Landroid/support/v7/app/m;

    invoke-direct {v0, p0, v2}, Landroid/support/v7/app/m;-><init>(Landroid/support/v7/app/ActionBarActivityDelegateBase;B)V

    iput-object v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->l:Landroid/support/v7/app/m;

    :cond_9
    iget-object v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->l:Landroid/support/v7/app/m;

    iget-object v4, p1, Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;->d:Landroid/support/v7/internal/view/menu/i;

    if-nez v4, :cond_c

    move-object v0, v3

    :goto_4
    check-cast v0, Landroid/view/View;

    iput-object v0, p1, Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;->c:Landroid/view/View;

    iget-object v0, p1, Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;->c:Landroid/view/View;

    if-eqz v0, :cond_e

    move v0, v1

    :goto_5
    if-eqz v0, :cond_0

    iget-object v0, p1, Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;->c:Landroid/view/View;

    if-eqz v0, :cond_a

    iget-object v0, p1, Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;->e:Landroid/support/v7/internal/view/menu/g;

    invoke-virtual {v0}, Landroid/support/v7/internal/view/menu/g;->a()Landroid/widget/ListAdapter;

    move-result-object v0

    invoke-interface {v0}, Landroid/widget/ListAdapter;->getCount()I

    move-result v0

    if-lez v0, :cond_a

    move v2, v1

    :cond_a
    if-eqz v2, :cond_0

    .line 862
    iput-boolean v1, p1, Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;->h:Z

    goto/16 :goto_0

    .line 854
    :cond_b
    const v4, 0x7f0900cc

    invoke-virtual {v5, v4, v1}, Landroid/content/res/Resources$Theme;->applyStyle(IZ)V

    goto :goto_3

    .line 858
    :cond_c
    iget-object v3, p1, Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;->e:Landroid/support/v7/internal/view/menu/g;

    if-nez v3, :cond_d

    new-instance v3, Landroid/support/v7/internal/view/menu/g;

    iget-object v4, p1, Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;->f:Landroid/content/Context;

    const v5, 0x7f04000d

    invoke-direct {v3, v4, v5}, Landroid/support/v7/internal/view/menu/g;-><init>(Landroid/content/Context;I)V

    iput-object v3, p1, Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;->e:Landroid/support/v7/internal/view/menu/g;

    iget-object v3, p1, Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;->e:Landroid/support/v7/internal/view/menu/g;

    invoke-virtual {v3, v0}, Landroid/support/v7/internal/view/menu/g;->a(Landroid/support/v7/internal/view/menu/y;)V

    iget-object v0, p1, Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;->d:Landroid/support/v7/internal/view/menu/i;

    iget-object v3, p1, Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;->e:Landroid/support/v7/internal/view/menu/g;

    invoke-virtual {v0, v3}, Landroid/support/v7/internal/view/menu/i;->a(Landroid/support/v7/internal/view/menu/x;)V

    :cond_d
    iget-object v0, p1, Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;->e:Landroid/support/v7/internal/view/menu/g;

    iget-object v3, p1, Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;->b:Landroid/view/ViewGroup;

    invoke-virtual {v0, v3}, Landroid/support/v7/internal/view/menu/g;->a(Landroid/view/ViewGroup;)Landroid/support/v7/internal/view/menu/z;

    move-result-object v0

    goto :goto_4

    :cond_e
    move v0, v2

    goto :goto_5
.end method

.method private a(Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;Z)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 1120
    if-eqz p2, :cond_1

    iget v0, p1, Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;->a:I

    if-nez v0, :cond_1

    iget-object v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->j:Landroid/support/v7/internal/widget/t;

    if-eqz v0, :cond_1

    iget-object v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->j:Landroid/support/v7/internal/widget/t;

    invoke-interface {v0}, Landroid/support/v7/internal/widget/t;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1122
    iget-object v0, p1, Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;->d:Landroid/support/v7/internal/view/menu/i;

    invoke-direct {p0, v0}, Landroid/support/v7/app/ActionBarActivityDelegateBase;->b(Landroid/support/v7/internal/view/menu/i;)V

    .line 1146
    :cond_0
    :goto_0
    return-void

    .line 1126
    :cond_1
    iget-boolean v0, p1, Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;->h:Z

    if-eqz v0, :cond_2

    .line 1127
    if-eqz p2, :cond_2

    .line 1128
    iget v0, p1, Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;->a:I

    invoke-direct {p0, v0, p1, v1}, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a(ILandroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;Landroid/view/Menu;)V

    .line 1132
    :cond_2
    iput-boolean v2, p1, Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;->g:Z

    .line 1133
    iput-boolean v2, p1, Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;->h:Z

    .line 1137
    iput-object v1, p1, Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;->c:Landroid/view/View;

    .line 1141
    const/4 v0, 0x1

    iput-boolean v0, p1, Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;->j:Z

    .line 1143
    iget-object v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->t:Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;

    if-ne v0, p1, :cond_0

    .line 1144
    iput-object v1, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->t:Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;

    goto :goto_0
.end method

.method static synthetic a(Landroid/support/v7/app/ActionBarActivityDelegateBase;I)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 80
    invoke-direct {p0, p1}, Landroid/support/v7/app/ActionBarActivityDelegateBase;->c(I)Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;

    move-result-object v0

    iget-object v1, v0, Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;->d:Landroid/support/v7/internal/view/menu/i;

    if-eqz v1, :cond_1

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    iget-object v2, v0, Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;->d:Landroid/support/v7/internal/view/menu/i;

    invoke-virtual {v2, v1}, Landroid/support/v7/internal/view/menu/i;->a(Landroid/os/Bundle;)V

    invoke-virtual {v1}, Landroid/os/Bundle;->size()I

    move-result v2

    if-lez v2, :cond_0

    iput-object v1, v0, Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;->l:Landroid/os/Bundle;

    :cond_0
    iget-object v1, v0, Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;->d:Landroid/support/v7/internal/view/menu/i;

    invoke-virtual {v1}, Landroid/support/v7/internal/view/menu/i;->f()V

    iget-object v1, v0, Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;->d:Landroid/support/v7/internal/view/menu/i;

    invoke-virtual {v1}, Landroid/support/v7/internal/view/menu/i;->clear()V

    :cond_1
    iput-boolean v4, v0, Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;->k:Z

    iput-boolean v4, v0, Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;->j:Z

    const/16 v0, 0x8

    if-eq p1, v0, :cond_2

    if-nez p1, :cond_3

    :cond_2
    iget-object v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->j:Landroid/support/v7/internal/widget/t;

    if-eqz v0, :cond_3

    invoke-direct {p0, v3}, Landroid/support/v7/app/ActionBarActivityDelegateBase;->c(I)Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;

    move-result-object v0

    if-eqz v0, :cond_3

    iput-boolean v3, v0, Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;->g:Z

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Landroid/support/v7/app/ActionBarActivityDelegateBase;->b(Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;Landroid/view/KeyEvent;)Z

    :cond_3
    return-void
.end method

.method static synthetic a(Landroid/support/v7/app/ActionBarActivityDelegateBase;ILandroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;Landroid/view/Menu;)V
    .locals 0

    .prologue
    .line 80
    invoke-direct {p0, p1, p2, p3}, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a(ILandroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;Landroid/view/Menu;)V

    return-void
.end method

.method static synthetic a(Landroid/support/v7/app/ActionBarActivityDelegateBase;Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;Z)V
    .locals 0

    .prologue
    .line 80
    invoke-direct {p0, p1, p2}, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a(Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;Z)V

    return-void
.end method

.method static synthetic a(Landroid/support/v7/app/ActionBarActivityDelegateBase;Landroid/support/v7/internal/view/menu/i;)V
    .locals 0

    .prologue
    .line 80
    invoke-direct {p0, p1}, Landroid/support/v7/app/ActionBarActivityDelegateBase;->b(Landroid/support/v7/internal/view/menu/i;)V

    return-void
.end method

.method private a(Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;ILandroid/view/KeyEvent;I)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1202
    invoke-virtual {p3}, Landroid/view/KeyEvent;->isSystem()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1215
    :cond_0
    :goto_0
    return v0

    .line 1210
    :cond_1
    iget-boolean v1, p1, Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;->g:Z

    if-nez v1, :cond_2

    invoke-direct {p0, p1, p3}, Landroid/support/v7/app/ActionBarActivityDelegateBase;->b(Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;Landroid/view/KeyEvent;)Z

    move-result v1

    if-eqz v1, :cond_0

    :cond_2
    iget-object v1, p1, Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;->d:Landroid/support/v7/internal/view/menu/i;

    if-eqz v1, :cond_0

    .line 1212
    iget-object v0, p1, Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;->d:Landroid/support/v7/internal/view/menu/i;

    const/4 v1, 0x1

    invoke-virtual {v0, p2, p3, v1}, Landroid/support/v7/internal/view/menu/i;->performShortcut(ILandroid/view/KeyEvent;I)Z

    move-result v0

    goto :goto_0
.end method

.method static synthetic a(Landroid/support/v7/app/ActionBarActivityDelegateBase;Z)Z
    .locals 1

    .prologue
    .line 80
    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->u:Z

    return v0
.end method

.method static synthetic b(Landroid/support/v7/app/ActionBarActivityDelegateBase;I)I
    .locals 1

    .prologue
    .line 80
    const/4 v0, 0x0

    iput v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->v:I

    return v0
.end method

.method private b(Landroid/support/v7/internal/view/menu/i;)V
    .locals 2

    .prologue
    .line 1106
    iget-boolean v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->r:Z

    if-eqz v0, :cond_0

    .line 1117
    :goto_0
    return-void

    .line 1110
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->r:Z

    .line 1111
    iget-object v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->j:Landroid/support/v7/internal/widget/t;

    invoke-interface {v0}, Landroid/support/v7/internal/widget/t;->i()V

    .line 1112
    invoke-virtual {p0}, Landroid/support/v7/app/ActionBarActivityDelegateBase;->i()Landroid/support/v7/internal/a/a;

    move-result-object v0

    .line 1113
    if-eqz v0, :cond_1

    invoke-virtual {p0}, Landroid/support/v7/app/ActionBarActivityDelegateBase;->k()Z

    move-result v1

    if-nez v1, :cond_1

    .line 1114
    const/16 v1, 0x8

    invoke-interface {v0, v1, p1}, Landroid/support/v7/internal/a/a;->b(ILandroid/view/Menu;)V

    .line 1116
    :cond_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->r:Z

    goto :goto_0
.end method

.method private b(Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;Landroid/view/KeyEvent;)Z
    .locals 10

    .prologue
    const v9, 0x7f010056

    const/16 v5, 0x8

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 1012
    invoke-virtual {p0}, Landroid/support/v7/app/ActionBarActivityDelegateBase;->k()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1102
    :cond_0
    :goto_0
    return v4

    .line 1017
    :cond_1
    iget-boolean v0, p1, Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;->g:Z

    if-eqz v0, :cond_2

    move v4, v3

    .line 1018
    goto :goto_0

    .line 1021
    :cond_2
    iget-object v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->t:Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;

    if-eqz v0, :cond_3

    iget-object v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->t:Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;

    if-eq v0, p1, :cond_3

    .line 1023
    iget-object v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->t:Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;

    invoke-direct {p0, v0, v4}, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a(Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;Z)V

    .line 1026
    :cond_3
    iget v0, p1, Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;->a:I

    if-eqz v0, :cond_4

    iget v0, p1, Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;->a:I

    if-ne v0, v5, :cond_d

    :cond_4
    move v6, v3

    .line 1029
    :goto_1
    if-eqz v6, :cond_5

    iget-object v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->j:Landroid/support/v7/internal/widget/t;

    if-eqz v0, :cond_5

    .line 1032
    iget-object v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->j:Landroid/support/v7/internal/widget/t;

    invoke-interface {v0}, Landroid/support/v7/internal/widget/t;->h()V

    .line 1036
    :cond_5
    iget-object v0, p1, Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;->d:Landroid/support/v7/internal/view/menu/i;

    if-eqz v0, :cond_6

    iget-boolean v0, p1, Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;->k:Z

    if-eqz v0, :cond_10

    .line 1037
    :cond_6
    iget-object v0, p1, Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;->d:Landroid/support/v7/internal/view/menu/i;

    if-nez v0, :cond_a

    .line 1038
    iget-object v2, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:Landroid/support/v7/app/c;

    iget v0, p1, Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;->a:I

    if-eqz v0, :cond_7

    iget v0, p1, Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;->a:I

    if-ne v0, v5, :cond_16

    :cond_7
    iget-object v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->j:Landroid/support/v7/internal/widget/t;

    if-eqz v0, :cond_16

    new-instance v5, Landroid/util/TypedValue;

    invoke-direct {v5}, Landroid/util/TypedValue;-><init>()V

    invoke-virtual {v2}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v7

    const v0, 0x7f010055

    invoke-virtual {v7, v0, v5, v3}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    iget v0, v5, Landroid/util/TypedValue;->resourceId:I

    if-eqz v0, :cond_e

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->newTheme()Landroid/content/res/Resources$Theme;

    move-result-object v0

    invoke-virtual {v0, v7}, Landroid/content/res/Resources$Theme;->setTo(Landroid/content/res/Resources$Theme;)V

    iget v8, v5, Landroid/util/TypedValue;->resourceId:I

    invoke-virtual {v0, v8, v3}, Landroid/content/res/Resources$Theme;->applyStyle(IZ)V

    invoke-virtual {v0, v9, v5, v3}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    :goto_2
    iget v8, v5, Landroid/util/TypedValue;->resourceId:I

    if-eqz v8, :cond_9

    if-nez v0, :cond_8

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->newTheme()Landroid/content/res/Resources$Theme;

    move-result-object v0

    invoke-virtual {v0, v7}, Landroid/content/res/Resources$Theme;->setTo(Landroid/content/res/Resources$Theme;)V

    :cond_8
    iget v5, v5, Landroid/util/TypedValue;->resourceId:I

    invoke-virtual {v0, v5, v3}, Landroid/content/res/Resources$Theme;->applyStyle(IZ)V

    :cond_9
    move-object v5, v0

    if-eqz v5, :cond_16

    new-instance v0, Landroid/view/ContextThemeWrapper;

    invoke-direct {v0, v2, v4}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    invoke-virtual {v0}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/content/res/Resources$Theme;->setTo(Landroid/content/res/Resources$Theme;)V

    :goto_3
    new-instance v2, Landroid/support/v7/internal/view/menu/i;

    invoke-direct {v2, v0}, Landroid/support/v7/internal/view/menu/i;-><init>(Landroid/content/Context;)V

    invoke-virtual {v2, p0}, Landroid/support/v7/internal/view/menu/i;->a(Landroid/support/v7/internal/view/menu/j;)V

    invoke-virtual {p1, v2}, Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;->a(Landroid/support/v7/internal/view/menu/i;)V

    iget-object v0, p1, Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;->d:Landroid/support/v7/internal/view/menu/i;

    if-eqz v0, :cond_0

    .line 1043
    :cond_a
    if-eqz v6, :cond_c

    iget-object v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->j:Landroid/support/v7/internal/widget/t;

    if-eqz v0, :cond_c

    .line 1044
    iget-object v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->k:Landroid/support/v7/app/j;

    if-nez v0, :cond_b

    .line 1045
    new-instance v0, Landroid/support/v7/app/j;

    invoke-direct {v0, p0, v4}, Landroid/support/v7/app/j;-><init>(Landroid/support/v7/app/ActionBarActivityDelegateBase;B)V

    iput-object v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->k:Landroid/support/v7/app/j;

    .line 1047
    :cond_b
    iget-object v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->j:Landroid/support/v7/internal/widget/t;

    iget-object v2, p1, Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;->d:Landroid/support/v7/internal/view/menu/i;

    iget-object v5, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->k:Landroid/support/v7/app/j;

    invoke-interface {v0, v2, v5}, Landroid/support/v7/internal/widget/t;->a(Landroid/view/Menu;Landroid/support/v7/internal/view/menu/y;)V

    .line 1052
    :cond_c
    iget-object v0, p1, Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;->d:Landroid/support/v7/internal/view/menu/i;

    invoke-virtual {v0}, Landroid/support/v7/internal/view/menu/i;->f()V

    .line 1053
    invoke-virtual {p0}, Landroid/support/v7/app/ActionBarActivityDelegateBase;->i()Landroid/support/v7/internal/a/a;

    move-result-object v0

    iget v2, p1, Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;->a:I

    iget-object v5, p1, Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;->d:Landroid/support/v7/internal/view/menu/i;

    invoke-interface {v0, v2, v5}, Landroid/support/v7/internal/a/a;->a(ILandroid/view/Menu;)Z

    move-result v0

    if-nez v0, :cond_f

    .line 1055
    invoke-virtual {p1, v1}, Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;->a(Landroid/support/v7/internal/view/menu/i;)V

    .line 1057
    if-eqz v6, :cond_0

    iget-object v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->j:Landroid/support/v7/internal/widget/t;

    if-eqz v0, :cond_0

    .line 1059
    iget-object v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->j:Landroid/support/v7/internal/widget/t;

    iget-object v2, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->k:Landroid/support/v7/app/j;

    invoke-interface {v0, v1, v2}, Landroid/support/v7/internal/widget/t;->a(Landroid/view/Menu;Landroid/support/v7/internal/view/menu/y;)V

    goto/16 :goto_0

    :cond_d
    move v6, v4

    .line 1026
    goto/16 :goto_1

    .line 1038
    :cond_e
    invoke-virtual {v7, v9, v5, v3}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    move-object v0, v1

    goto :goto_2

    .line 1065
    :cond_f
    iput-boolean v4, p1, Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;->k:Z

    .line 1070
    :cond_10
    iget-object v0, p1, Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;->d:Landroid/support/v7/internal/view/menu/i;

    invoke-virtual {v0}, Landroid/support/v7/internal/view/menu/i;->f()V

    .line 1074
    iget-object v0, p1, Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;->l:Landroid/os/Bundle;

    if-eqz v0, :cond_11

    .line 1075
    iget-object v0, p1, Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;->d:Landroid/support/v7/internal/view/menu/i;

    iget-object v2, p1, Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;->l:Landroid/os/Bundle;

    invoke-virtual {v0, v2}, Landroid/support/v7/internal/view/menu/i;->b(Landroid/os/Bundle;)V

    .line 1076
    iput-object v1, p1, Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;->l:Landroid/os/Bundle;

    .line 1080
    :cond_11
    invoke-virtual {p0}, Landroid/support/v7/app/ActionBarActivityDelegateBase;->i()Landroid/support/v7/internal/a/a;

    move-result-object v0

    iget-object v2, p1, Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;->d:Landroid/support/v7/internal/view/menu/i;

    invoke-interface {v0, v4, v1, v2}, Landroid/support/v7/internal/a/a;->a(ILandroid/view/View;Landroid/view/Menu;)Z

    move-result v0

    if-nez v0, :cond_13

    .line 1081
    if-eqz v6, :cond_12

    iget-object v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->j:Landroid/support/v7/internal/widget/t;

    if-eqz v0, :cond_12

    .line 1084
    iget-object v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->j:Landroid/support/v7/internal/widget/t;

    iget-object v2, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->k:Landroid/support/v7/app/j;

    invoke-interface {v0, v1, v2}, Landroid/support/v7/internal/widget/t;->a(Landroid/view/Menu;Landroid/support/v7/internal/view/menu/y;)V

    .line 1086
    :cond_12
    iget-object v0, p1, Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;->d:Landroid/support/v7/internal/view/menu/i;

    invoke-virtual {v0}, Landroid/support/v7/internal/view/menu/i;->g()V

    goto/16 :goto_0

    .line 1091
    :cond_13
    if-eqz p2, :cond_14

    invoke-virtual {p2}, Landroid/view/KeyEvent;->getDeviceId()I

    move-result v0

    :goto_4
    invoke-static {v0}, Landroid/view/KeyCharacterMap;->load(I)Landroid/view/KeyCharacterMap;

    move-result-object v0

    .line 1093
    invoke-virtual {v0}, Landroid/view/KeyCharacterMap;->getKeyboardType()I

    move-result v0

    if-eq v0, v3, :cond_15

    move v0, v3

    :goto_5
    iput-boolean v0, p1, Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;->i:Z

    .line 1094
    iget-object v0, p1, Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;->d:Landroid/support/v7/internal/view/menu/i;

    iget-boolean v1, p1, Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;->i:Z

    invoke-virtual {v0, v1}, Landroid/support/v7/internal/view/menu/i;->setQwertyMode(Z)V

    .line 1095
    iget-object v0, p1, Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;->d:Landroid/support/v7/internal/view/menu/i;

    invoke-virtual {v0}, Landroid/support/v7/internal/view/menu/i;->g()V

    .line 1098
    iput-boolean v3, p1, Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;->g:Z

    .line 1099
    iput-object p1, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->t:Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;

    move v4, v3

    .line 1102
    goto/16 :goto_0

    .line 1091
    :cond_14
    const/4 v0, -0x1

    goto :goto_4

    :cond_15
    move v0, v4

    .line 1093
    goto :goto_5

    :cond_16
    move-object v0, v2

    goto/16 :goto_3
.end method

.method static synthetic c(Landroid/support/v7/app/ActionBarActivityDelegateBase;I)I
    .locals 8

    .prologue
    const/4 v6, -0x1

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 80
    iget-object v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->g:Landroid/support/v7/internal/widget/ActionBarContextView;

    if-eqz v0, :cond_b

    iget-object v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->g:Landroid/support/v7/internal/widget/ActionBarContextView;

    invoke-virtual {v0}, Landroid/support/v7/internal/widget/ActionBarContextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    instance-of v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;

    if-eqz v0, :cond_b

    iget-object v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->g:Landroid/support/v7/internal/widget/ActionBarContextView;

    invoke-virtual {v0}, Landroid/support/v7/internal/widget/ActionBarContextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    iget-object v1, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->g:Landroid/support/v7/internal/widget/ActionBarContextView;

    invoke-virtual {v1}, Landroid/support/v7/internal/widget/ActionBarContextView;->isShown()Z

    move-result v1

    if-eqz v1, :cond_7

    iget-object v1, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->y:Landroid/graphics/Rect;

    if-nez v1, :cond_0

    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    iput-object v1, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->y:Landroid/graphics/Rect;

    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    iput-object v1, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->z:Landroid/graphics/Rect;

    :cond_0
    iget-object v1, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->y:Landroid/graphics/Rect;

    iget-object v4, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->z:Landroid/graphics/Rect;

    invoke-virtual {v1, v2, p1, v2, v2}, Landroid/graphics/Rect;->set(IIII)V

    iget-object v5, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->o:Landroid/view/ViewGroup;

    invoke-static {v5, v1, v4}, Landroid/support/v7/internal/widget/af;->a(Landroid/view/View;Landroid/graphics/Rect;Landroid/graphics/Rect;)V

    iget v1, v4, Landroid/graphics/Rect;->top:I

    if-nez v1, :cond_4

    move v1, p1

    :goto_0
    iget v4, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    if-eq v4, v1, :cond_a

    iput p1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    iget-object v1, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->p:Landroid/view/View;

    if-nez v1, :cond_5

    new-instance v1, Landroid/view/View;

    iget-object v4, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:Landroid/support/v7/app/c;

    invoke-direct {v1, v4}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->p:Landroid/view/View;

    iget-object v1, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->p:Landroid/view/View;

    iget-object v4, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:Landroid/support/v7/app/c;

    invoke-virtual {v4}, Landroid/support/v7/app/c;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0b001a

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {v1, v4}, Landroid/view/View;->setBackgroundColor(I)V

    iget-object v1, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->o:Landroid/view/ViewGroup;

    iget-object v4, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->p:Landroid/view/View;

    new-instance v5, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v5, v6, p1}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v1, v4, v6, v5}, Landroid/view/ViewGroup;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    move v1, v3

    :goto_1
    iget-boolean v4, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->d:Z

    if-nez v4, :cond_1

    move p1, v2

    :cond_1
    move v7, v1

    move v1, v3

    move v3, v7

    :goto_2
    if-eqz v3, :cond_2

    iget-object v3, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->g:Landroid/support/v7/internal/widget/ActionBarContextView;

    invoke-virtual {v3, v0}, Landroid/support/v7/internal/widget/ActionBarContextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    :cond_2
    move v0, v1

    :goto_3
    iget-object v1, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->p:Landroid/view/View;

    if-eqz v1, :cond_3

    iget-object v1, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->p:Landroid/view/View;

    if-eqz v0, :cond_8

    :goto_4
    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    :cond_3
    return p1

    :cond_4
    move v1, v2

    goto :goto_0

    :cond_5
    iget-object v1, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->p:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    iget v4, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    if-eq v4, p1, :cond_6

    iput p1, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    iget-object v4, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->p:Landroid/view/View;

    invoke-virtual {v4, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    :cond_6
    move v1, v3

    goto :goto_1

    :cond_7
    iget v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    if-eqz v1, :cond_9

    iput v2, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    move v1, v2

    goto :goto_2

    :cond_8
    const/16 v2, 0x8

    goto :goto_4

    :cond_9
    move v3, v2

    move v1, v2

    goto :goto_2

    :cond_a
    move v1, v2

    goto :goto_1

    :cond_b
    move v0, v2

    goto :goto_3
.end method

.method private c(I)Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1185
    iget-object v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->s:[Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;

    if-eqz v0, :cond_0

    array-length v1, v0

    if-gt v1, p1, :cond_2

    .line 1186
    :cond_0
    add-int/lit8 v1, p1, 0x1

    new-array v1, v1, [Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;

    .line 1187
    if-eqz v0, :cond_1

    .line 1188
    array-length v2, v0

    invoke-static {v0, v3, v1, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1190
    :cond_1
    iput-object v1, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->s:[Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;

    move-object v0, v1

    .line 1193
    :cond_2
    aget-object v1, v0, p1

    .line 1194
    if-nez v1, :cond_3

    .line 1195
    new-instance v1, Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;

    invoke-direct {v1, p1}, Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;-><init>(I)V

    aput-object v1, v0, p1

    move-object v0, v1

    .line 1197
    :goto_0
    return-object v0

    :cond_3
    move-object v0, v1

    goto :goto_0
.end method

.method private d(I)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 1226
    iget v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->v:I

    shl-int v1, v2, p1

    or-int/2addr v0, v1

    iput v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->v:I

    .line 1228
    iget-boolean v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->u:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->n:Landroid/view/ViewGroup;

    if-eqz v0, :cond_0

    .line 1229
    iget-object v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->n:Landroid/view/ViewGroup;

    iget-object v1, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->w:Ljava/lang/Runnable;

    invoke-static {v0, v1}, Landroid/support/v4/view/ViewCompat;->a(Landroid/view/View;Ljava/lang/Runnable;)V

    .line 1230
    iput-boolean v2, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->u:Z

    .line 1232
    :cond_0
    return-void
.end method

.method private m()V
    .locals 12

    .prologue
    const/4 v6, 0x1

    const/4 v11, 0x6

    const/4 v10, 0x5

    const/4 v4, -0x1

    const/4 v1, 0x0

    .line 255
    iget-boolean v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->m:Z

    if-nez v0, :cond_7

    .line 256
    iget-boolean v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->b:Z

    if-eqz v0, :cond_9

    .line 262
    new-instance v2, Landroid/util/TypedValue;

    invoke-direct {v2}, Landroid/util/TypedValue;-><init>()V

    .line 263
    iget-object v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:Landroid/support/v7/app/c;

    invoke-virtual {v0}, Landroid/support/v7/app/c;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v0

    const v3, 0x7f010055

    invoke-virtual {v0, v3, v2, v6}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    .line 266
    iget v0, v2, Landroid/util/TypedValue;->resourceId:I

    if-eqz v0, :cond_8

    .line 267
    new-instance v0, Landroid/view/ContextThemeWrapper;

    iget-object v3, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:Landroid/support/v7/app/c;

    iget v2, v2, Landroid/util/TypedValue;->resourceId:I

    invoke-direct {v0, v3, v2}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    .line 273
    :goto_0
    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v2, 0x7f040013

    invoke-virtual {v0, v2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->o:Landroid/view/ViewGroup;

    .line 276
    iget-object v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->o:Landroid/view/ViewGroup;

    const v2, 0x7f0f0069

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/internal/widget/t;

    iput-object v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->j:Landroid/support/v7/internal/widget/t;

    .line 278
    iget-object v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->j:Landroid/support/v7/internal/widget/t;

    invoke-virtual {p0}, Landroid/support/v7/app/ActionBarActivityDelegateBase;->i()Landroid/support/v7/internal/a/a;

    move-result-object v2

    invoke-interface {v0, v2}, Landroid/support/v7/internal/widget/t;->a(Landroid/support/v7/internal/a/a;)V

    .line 283
    iget-boolean v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->c:Z

    if-eqz v0, :cond_0

    .line 284
    iget-object v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->j:Landroid/support/v7/internal/widget/t;

    const/16 v2, 0x9

    invoke-interface {v0, v2}, Landroid/support/v7/internal/widget/t;->a(I)V

    .line 336
    :cond_0
    :goto_1
    iget-object v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->o:Landroid/view/ViewGroup;

    invoke-static {v0}, Landroid/support/v7/internal/widget/af;->b(Landroid/view/View;)V

    .line 339
    iget-object v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:Landroid/support/v7/app/c;

    iget-object v2, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->o:Landroid/view/ViewGroup;

    invoke-virtual {v0, v2}, Landroid/support/v7/app/c;->a(Landroid/view/View;)V

    .line 343
    iget-object v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:Landroid/support/v7/app/c;

    const v2, 0x1020002

    invoke-virtual {v0, v2}, Landroid/support/v7/app/c;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 344
    invoke-virtual {v0, v4}, Landroid/view/View;->setId(I)V

    .line 345
    iget-object v2, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:Landroid/support/v7/app/c;

    const v3, 0x7f0f0003

    invoke-virtual {v2, v3}, Landroid/support/v7/app/c;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 346
    const v3, 0x1020002

    invoke-virtual {v2, v3}, Landroid/view/View;->setId(I)V

    .line 350
    instance-of v2, v0, Landroid/widget/FrameLayout;

    if-eqz v2, :cond_1

    .line 351
    check-cast v0, Landroid/widget/FrameLayout;

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setForeground(Landroid/graphics/drawable/Drawable;)V

    .line 355
    :cond_1
    iget-object v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->q:Ljava/lang/CharSequence;

    if-eqz v0, :cond_2

    iget-object v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->j:Landroid/support/v7/internal/widget/t;

    if-eqz v0, :cond_2

    .line 356
    iget-object v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->j:Landroid/support/v7/internal/widget/t;

    iget-object v2, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->q:Ljava/lang/CharSequence;

    invoke-interface {v0, v2}, Landroid/support/v7/internal/widget/t;->a(Ljava/lang/CharSequence;)V

    .line 357
    iput-object v1, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->q:Ljava/lang/CharSequence;

    .line 360
    :cond_2
    iget-object v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:Landroid/support/v7/app/c;

    sget-object v2, Landroid/support/v7/a/a;->n:[I

    invoke-virtual {v0, v2}, Landroid/support/v7/app/c;->obtainStyledAttributes([I)Landroid/content/res/TypedArray;

    move-result-object v7

    const/4 v0, 0x4

    invoke-virtual {v7, v0}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v0

    if-eqz v0, :cond_15

    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    const/4 v2, 0x4

    invoke-virtual {v7, v2, v0}, Landroid/content/res/TypedArray;->getValue(ILandroid/util/TypedValue;)Z

    :goto_2
    invoke-virtual {v7, v11}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v2

    if-eqz v2, :cond_14

    new-instance v2, Landroid/util/TypedValue;

    invoke-direct {v2}, Landroid/util/TypedValue;-><init>()V

    invoke-virtual {v7, v11, v2}, Landroid/content/res/TypedArray;->getValue(ILandroid/util/TypedValue;)Z

    :goto_3
    const/4 v3, 0x7

    invoke-virtual {v7, v3}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v3

    if-eqz v3, :cond_13

    new-instance v3, Landroid/util/TypedValue;

    invoke-direct {v3}, Landroid/util/TypedValue;-><init>()V

    const/4 v5, 0x7

    invoke-virtual {v7, v5, v3}, Landroid/content/res/TypedArray;->getValue(ILandroid/util/TypedValue;)Z

    :goto_4
    invoke-virtual {v7, v10}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v5

    if-eqz v5, :cond_3

    new-instance v1, Landroid/util/TypedValue;

    invoke-direct {v1}, Landroid/util/TypedValue;-><init>()V

    invoke-virtual {v7, v10, v1}, Landroid/content/res/TypedArray;->getValue(ILandroid/util/TypedValue;)Z

    :cond_3
    iget-object v5, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:Landroid/support/v7/app/c;

    invoke-virtual {v5}, Landroid/support/v7/app/c;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v8

    iget v5, v8, Landroid/util/DisplayMetrics;->widthPixels:I

    iget v9, v8, Landroid/util/DisplayMetrics;->heightPixels:I

    if-ge v5, v9, :cond_c

    move v5, v6

    :goto_5
    if-eqz v5, :cond_d

    :goto_6
    if-eqz v2, :cond_12

    iget v0, v2, Landroid/util/TypedValue;->type:I

    if-eqz v0, :cond_12

    iget v0, v2, Landroid/util/TypedValue;->type:I

    if-ne v0, v10, :cond_e

    invoke-virtual {v2, v8}, Landroid/util/TypedValue;->getDimension(Landroid/util/DisplayMetrics;)F

    move-result v0

    float-to-int v0, v0

    move v2, v0

    :goto_7
    if-eqz v5, :cond_f

    :goto_8
    if-eqz v3, :cond_11

    iget v0, v3, Landroid/util/TypedValue;->type:I

    if-eqz v0, :cond_11

    iget v0, v3, Landroid/util/TypedValue;->type:I

    if-ne v0, v10, :cond_10

    invoke-virtual {v3, v8}, Landroid/util/TypedValue;->getDimension(Landroid/util/DisplayMetrics;)F

    move-result v0

    float-to-int v0, v0

    :goto_9
    if-ne v2, v4, :cond_4

    if-eq v0, v4, :cond_5

    :cond_4
    iget-object v1, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:Landroid/support/v7/app/c;

    invoke-virtual {v1}, Landroid/support/v7/app/c;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1, v2, v0}, Landroid/view/Window;->setLayout(II)V

    :cond_5
    invoke-virtual {v7}, Landroid/content/res/TypedArray;->recycle()V

    .line 362
    invoke-virtual {p0}, Landroid/support/v7/app/ActionBarActivityDelegateBase;->l()V

    .line 364
    iput-boolean v6, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->m:Z

    .line 371
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Landroid/support/v7/app/ActionBarActivityDelegateBase;->c(I)Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;

    move-result-object v0

    .line 372
    invoke-virtual {p0}, Landroid/support/v7/app/ActionBarActivityDelegateBase;->k()Z

    move-result v1

    if-nez v1, :cond_7

    if-eqz v0, :cond_6

    iget-object v0, v0, Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;->d:Landroid/support/v7/internal/view/menu/i;

    if-nez v0, :cond_7

    .line 373
    :cond_6
    const/16 v0, 0x8

    invoke-direct {p0, v0}, Landroid/support/v7/app/ActionBarActivityDelegateBase;->d(I)V

    .line 376
    :cond_7
    return-void

    .line 269
    :cond_8
    iget-object v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:Landroid/support/v7/app/c;

    goto/16 :goto_0

    .line 293
    :cond_9
    iget-boolean v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->d:Z

    if-eqz v0, :cond_a

    .line 294
    iget-object v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:Landroid/support/v7/app/c;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v2, 0x7f040012

    invoke-virtual {v0, v2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->o:Landroid/view/ViewGroup;

    .line 301
    :goto_a
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x15

    if-lt v0, v2, :cond_b

    .line 304
    iget-object v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->o:Landroid/view/ViewGroup;

    new-instance v2, Landroid/support/v7/app/g;

    invoke-direct {v2, p0}, Landroid/support/v7/app/g;-><init>(Landroid/support/v7/app/ActionBarActivityDelegateBase;)V

    invoke-static {v0, v2}, Landroid/support/v4/view/ViewCompat;->a(Landroid/view/View;Landroid/support/v4/view/G;)V

    goto/16 :goto_1

    .line 297
    :cond_a
    iget-object v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:Landroid/support/v7/app/c;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v2, 0x7f040011

    invoke-virtual {v0, v2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->o:Landroid/view/ViewGroup;

    goto :goto_a

    .line 325
    :cond_b
    iget-object v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->o:Landroid/view/ViewGroup;

    check-cast v0, Landroid/support/v7/internal/widget/w;

    new-instance v2, Landroid/support/v7/app/h;

    invoke-direct {v2, p0}, Landroid/support/v7/app/h;-><init>(Landroid/support/v7/app/ActionBarActivityDelegateBase;)V

    invoke-interface {v0, v2}, Landroid/support/v7/internal/widget/w;->a(Landroid/support/v7/internal/widget/x;)V

    goto/16 :goto_1

    .line 360
    :cond_c
    const/4 v5, 0x0

    goto/16 :goto_5

    :cond_d
    move-object v2, v0

    goto/16 :goto_6

    :cond_e
    iget v0, v2, Landroid/util/TypedValue;->type:I

    if-ne v0, v11, :cond_12

    iget v0, v8, Landroid/util/DisplayMetrics;->widthPixels:I

    int-to-float v0, v0

    iget v9, v8, Landroid/util/DisplayMetrics;->widthPixels:I

    int-to-float v9, v9

    invoke-virtual {v2, v0, v9}, Landroid/util/TypedValue;->getFraction(FF)F

    move-result v0

    float-to-int v0, v0

    move v2, v0

    goto/16 :goto_7

    :cond_f
    move-object v3, v1

    goto/16 :goto_8

    :cond_10
    iget v0, v3, Landroid/util/TypedValue;->type:I

    if-ne v0, v11, :cond_11

    iget v0, v8, Landroid/util/DisplayMetrics;->heightPixels:I

    int-to-float v0, v0

    iget v1, v8, Landroid/util/DisplayMetrics;->heightPixels:I

    int-to-float v1, v1

    invoke-virtual {v3, v0, v1}, Landroid/util/TypedValue;->getFraction(FF)F

    move-result v0

    float-to-int v0, v0

    goto/16 :goto_9

    :cond_11
    move v0, v4

    goto/16 :goto_9

    :cond_12
    move v2, v4

    goto/16 :goto_7

    :cond_13
    move-object v3, v1

    goto/16 :goto_4

    :cond_14
    move-object v2, v1

    goto/16 :goto_3

    :cond_15
    move-object v0, v1

    goto/16 :goto_2
.end method


# virtual methods
.method public final a()Landroid/support/v7/app/ActionBar;
    .locals 3

    .prologue
    .line 155
    invoke-direct {p0}, Landroid/support/v7/app/ActionBarActivityDelegateBase;->m()V

    .line 156
    new-instance v0, Landroid/support/v7/internal/a/b;

    iget-object v1, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:Landroid/support/v7/app/c;

    iget-boolean v2, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->c:Z

    invoke-direct {v0, v1, v2}, Landroid/support/v7/internal/a/b;-><init>(Landroid/support/v7/app/c;Z)V

    .line 157
    iget-boolean v1, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->x:Z

    invoke-virtual {v0, v1}, Landroid/support/v7/app/ActionBar;->a(Z)V

    .line 158
    return-object v0
.end method

.method public final a(Landroid/support/v7/b/b;)Landroid/support/v7/b/a;
    .locals 9

    .prologue
    const/4 v8, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 561
    if-nez p1, :cond_0

    .line 562
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "ActionMode callback can not be null."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 565
    :cond_0
    iget-object v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->f:Landroid/support/v7/b/a;

    if-eqz v0, :cond_1

    .line 566
    iget-object v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->f:Landroid/support/v7/b/a;

    invoke-virtual {v0}, Landroid/support/v7/b/a;->b()V

    .line 569
    :cond_1
    new-instance v3, Landroid/support/v7/app/k;

    invoke-direct {v3, p0, p1}, Landroid/support/v7/app/k;-><init>(Landroid/support/v7/app/ActionBarActivityDelegateBase;Landroid/support/v7/b/b;)V

    .line 571
    invoke-virtual {p0}, Landroid/support/v7/app/ActionBarActivityDelegateBase;->b()Landroid/support/v7/app/ActionBar;

    move-result-object v0

    .line 572
    if-eqz v0, :cond_2

    .line 573
    invoke-virtual {v0, v3}, Landroid/support/v7/app/ActionBar;->a(Landroid/support/v7/b/b;)Landroid/support/v7/b/a;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->f:Landroid/support/v7/b/a;

    .line 574
    iget-object v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->f:Landroid/support/v7/b/a;

    if-eqz v0, :cond_2

    .line 575
    iget-object v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:Landroid/support/v7/app/c;

    iget-object v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->f:Landroid/support/v7/b/a;

    invoke-static {}, Landroid/support/v7/app/c;->b()V

    .line 579
    :cond_2
    iget-object v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->f:Landroid/support/v7/b/a;

    if-nez v0, :cond_8

    .line 581
    iget-object v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->f:Landroid/support/v7/b/a;

    if-eqz v0, :cond_3

    iget-object v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->f:Landroid/support/v7/b/a;

    invoke-virtual {v0}, Landroid/support/v7/b/a;->b()V

    :cond_3
    new-instance v4, Landroid/support/v7/app/k;

    invoke-direct {v4, p0, v3}, Landroid/support/v7/app/k;-><init>(Landroid/support/v7/app/ActionBarActivityDelegateBase;Landroid/support/v7/b/b;)V

    invoke-virtual {p0}, Landroid/support/v7/app/ActionBarActivityDelegateBase;->h()Landroid/content/Context;

    move-result-object v5

    iget-object v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->g:Landroid/support/v7/internal/widget/ActionBarContextView;

    if-nez v0, :cond_4

    iget-boolean v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->e:Z

    if-eqz v0, :cond_9

    new-instance v0, Landroid/support/v7/internal/widget/ActionBarContextView;

    invoke-direct {v0, v5}, Landroid/support/v7/internal/widget/ActionBarContextView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->g:Landroid/support/v7/internal/widget/ActionBarContextView;

    new-instance v0, Landroid/widget/PopupWindow;

    const v6, 0x7f010068

    invoke-direct {v0, v5, v8, v6}, Landroid/widget/PopupWindow;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-object v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->h:Landroid/widget/PopupWindow;

    iget-object v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->h:Landroid/widget/PopupWindow;

    iget-object v6, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->g:Landroid/support/v7/internal/widget/ActionBarContextView;

    invoke-virtual {v0, v6}, Landroid/widget/PopupWindow;->setContentView(Landroid/view/View;)V

    iget-object v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->h:Landroid/widget/PopupWindow;

    const/4 v6, -0x1

    invoke-virtual {v0, v6}, Landroid/widget/PopupWindow;->setWidth(I)V

    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    iget-object v6, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:Landroid/support/v7/app/c;

    invoke-virtual {v6}, Landroid/support/v7/app/c;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v6

    const v7, 0x7f010057

    invoke-virtual {v6, v7, v0, v1}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    iget v0, v0, Landroid/util/TypedValue;->data:I

    iget-object v6, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:Landroid/support/v7/app/c;

    invoke-virtual {v6}, Landroid/support/v7/app/c;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v6

    invoke-static {v0, v6}, Landroid/util/TypedValue;->complexToDimensionPixelSize(ILandroid/util/DisplayMetrics;)I

    move-result v0

    iget-object v6, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->g:Landroid/support/v7/internal/widget/ActionBarContextView;

    invoke-virtual {v6, v0}, Landroid/support/v7/internal/widget/ActionBarContextView;->a(I)V

    iget-object v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->h:Landroid/widget/PopupWindow;

    const/4 v6, -0x2

    invoke-virtual {v0, v6}, Landroid/widget/PopupWindow;->setHeight(I)V

    new-instance v0, Landroid/support/v7/app/i;

    invoke-direct {v0, p0}, Landroid/support/v7/app/i;-><init>(Landroid/support/v7/app/ActionBarActivityDelegateBase;)V

    iput-object v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->i:Ljava/lang/Runnable;

    :cond_4
    :goto_0
    iget-object v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->g:Landroid/support/v7/internal/widget/ActionBarContextView;

    if-eqz v0, :cond_6

    iget-object v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->g:Landroid/support/v7/internal/widget/ActionBarContextView;

    invoke-virtual {v0}, Landroid/support/v7/internal/widget/ActionBarContextView;->e()V

    new-instance v6, Landroid/support/v7/internal/view/a;

    iget-object v7, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->g:Landroid/support/v7/internal/widget/ActionBarContextView;

    iget-object v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->h:Landroid/widget/PopupWindow;

    if-nez v0, :cond_a

    move v0, v1

    :goto_1
    invoke-direct {v6, v5, v7, v4, v0}, Landroid/support/v7/internal/view/a;-><init>(Landroid/content/Context;Landroid/support/v7/internal/widget/ActionBarContextView;Landroid/support/v7/b/b;Z)V

    invoke-virtual {v6}, Landroid/support/v7/b/a;->a()Landroid/view/Menu;

    move-result-object v0

    invoke-interface {v3, v6, v0}, Landroid/support/v7/b/b;->a(Landroid/support/v7/b/a;Landroid/view/Menu;)Z

    move-result v0

    if-eqz v0, :cond_b

    invoke-virtual {v6}, Landroid/support/v7/b/a;->c()V

    iget-object v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->g:Landroid/support/v7/internal/widget/ActionBarContextView;

    invoke-virtual {v0, v6}, Landroid/support/v7/internal/widget/ActionBarContextView;->a(Landroid/support/v7/b/a;)V

    iget-object v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->g:Landroid/support/v7/internal/widget/ActionBarContextView;

    invoke-virtual {v0, v2}, Landroid/support/v7/internal/widget/ActionBarContextView;->setVisibility(I)V

    iput-object v6, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->f:Landroid/support/v7/b/a;

    iget-object v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->h:Landroid/widget/PopupWindow;

    if-eqz v0, :cond_5

    iget-object v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:Landroid/support/v7/app/c;

    invoke-virtual {v0}, Landroid/support/v7/app/c;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->i:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    :cond_5
    iget-object v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->g:Landroid/support/v7/internal/widget/ActionBarContextView;

    const/16 v1, 0x20

    invoke-virtual {v0, v1}, Landroid/support/v7/internal/widget/ActionBarContextView;->sendAccessibilityEvent(I)V

    iget-object v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->g:Landroid/support/v7/internal/widget/ActionBarContextView;

    invoke-virtual {v0}, Landroid/support/v7/internal/widget/ActionBarContextView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-eqz v0, :cond_6

    iget-object v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->g:Landroid/support/v7/internal/widget/ActionBarContextView;

    invoke-virtual {v0}, Landroid/support/v7/internal/widget/ActionBarContextView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-static {v0}, Landroid/support/v4/view/ViewCompat;->m(Landroid/view/View;)V

    :cond_6
    :goto_2
    iget-object v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->f:Landroid/support/v7/b/a;

    if-eqz v0, :cond_7

    iget-object v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:Landroid/support/v7/app/c;

    if-eqz v0, :cond_7

    iget-object v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:Landroid/support/v7/app/c;

    iget-object v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->f:Landroid/support/v7/b/a;

    invoke-static {}, Landroid/support/v7/app/c;->b()V

    :cond_7
    iget-object v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->f:Landroid/support/v7/b/a;

    iput-object v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->f:Landroid/support/v7/b/a;

    .line 584
    :cond_8
    iget-object v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->f:Landroid/support/v7/b/a;

    return-object v0

    .line 581
    :cond_9
    iget-object v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:Landroid/support/v7/app/c;

    const v6, 0x7f0f0067

    invoke-virtual {v0, v6}, Landroid/support/v7/app/c;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/internal/widget/ViewStubCompat;

    if-eqz v0, :cond_4

    invoke-static {v5}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v6

    invoke-virtual {v0, v6}, Landroid/support/v7/internal/widget/ViewStubCompat;->a(Landroid/view/LayoutInflater;)V

    invoke-virtual {v0}, Landroid/support/v7/internal/widget/ViewStubCompat;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/internal/widget/ActionBarContextView;

    iput-object v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->g:Landroid/support/v7/internal/widget/ActionBarContextView;

    goto/16 :goto_0

    :cond_a
    move v0, v2

    goto :goto_1

    :cond_b
    iput-object v8, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->f:Landroid/support/v7/b/a;

    goto :goto_2
.end method

.method final a(Ljava/lang/String;Landroid/util/AttributeSet;)Landroid/view/View;
    .locals 2

    .prologue
    .line 749
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-ge v0, v1, :cond_1

    .line 752
    const/4 v0, -0x1

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    :cond_0
    :goto_0
    packed-switch v0, :pswitch_data_0

    .line 765
    :cond_1
    const/4 v0, 0x0

    :goto_1
    return-object v0

    .line 752
    :sswitch_0
    const-string/jumbo v1, "EditText"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :sswitch_1
    const-string/jumbo v1, "Spinner"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :sswitch_2
    const-string/jumbo v1, "CheckBox"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x2

    goto :goto_0

    :sswitch_3
    const-string/jumbo v1, "RadioButton"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x3

    goto :goto_0

    :sswitch_4
    const-string/jumbo v1, "CheckedTextView"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x4

    goto :goto_0

    .line 754
    :pswitch_0
    new-instance v0, Landroid/support/v7/internal/widget/U;

    iget-object v1, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:Landroid/support/v7/app/c;

    invoke-direct {v0, v1, p2}, Landroid/support/v7/internal/widget/U;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    goto :goto_1

    .line 756
    :pswitch_1
    new-instance v0, Landroid/support/v7/internal/widget/Z;

    iget-object v1, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:Landroid/support/v7/app/c;

    invoke-direct {v0, v1, p2}, Landroid/support/v7/internal/widget/Z;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    goto :goto_1

    .line 758
    :pswitch_2
    new-instance v0, Landroid/support/v7/internal/widget/R;

    iget-object v1, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:Landroid/support/v7/app/c;

    invoke-direct {v0, v1, p2}, Landroid/support/v7/internal/widget/R;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    goto :goto_1

    .line 760
    :pswitch_3
    new-instance v0, Landroid/support/v7/internal/widget/X;

    iget-object v1, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:Landroid/support/v7/app/c;

    invoke-direct {v0, v1, p2}, Landroid/support/v7/internal/widget/X;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    goto :goto_1

    .line 762
    :pswitch_4
    new-instance v0, Landroid/support/v7/internal/widget/S;

    iget-object v1, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:Landroid/support/v7/app/c;

    invoke-direct {v0, v1, p2}, Landroid/support/v7/internal/widget/S;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    goto :goto_1

    .line 752
    :sswitch_data_0
    .sparse-switch
        -0x56c015e7 -> :sswitch_4
        -0x1440b607 -> :sswitch_1
        0x2e46a6ed -> :sswitch_3
        0x5f7507c3 -> :sswitch_2
        0x63577677 -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public final a(I)V
    .locals 2

    .prologue
    .line 225
    invoke-direct {p0}, Landroid/support/v7/app/ActionBarActivityDelegateBase;->m()V

    .line 226
    iget-object v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:Landroid/support/v7/app/c;

    const v1, 0x1020002

    invoke-virtual {v0, v1}, Landroid/support/v7/app/c;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 227
    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 228
    iget-object v1, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:Landroid/support/v7/app/c;

    invoke-virtual {v1}, Landroid/support/v7/app/c;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v1

    invoke-virtual {v1, p1, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 229
    iget-object v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:Landroid/support/v7/app/c;

    invoke-static {}, Landroid/support/v7/app/c;->d()V

    .line 230
    return-void
.end method

.method public final a(ILandroid/view/Menu;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 511
    invoke-direct {p0, p1}, Landroid/support/v7/app/ActionBarActivityDelegateBase;->c(I)Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;

    move-result-object v0

    .line 512
    if-eqz v0, :cond_0

    .line 514
    invoke-direct {p0, v0, v1}, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a(Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;Z)V

    .line 517
    :cond_0
    const/16 v0, 0x8

    if-ne p1, v0, :cond_2

    .line 518
    invoke-virtual {p0}, Landroid/support/v7/app/ActionBarActivityDelegateBase;->b()Landroid/support/v7/app/ActionBar;

    move-result-object v0

    .line 519
    if-eqz v0, :cond_1

    .line 520
    invoke-virtual {v0, v1}, Landroid/support/v7/app/ActionBar;->c(Z)V

    .line 527
    :cond_1
    :goto_0
    return-void

    .line 522
    :cond_2
    invoke-virtual {p0}, Landroid/support/v7/app/ActionBarActivityDelegateBase;->k()Z

    move-result v0

    if-nez v0, :cond_1

    .line 525
    iget-object v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:Landroid/support/v7/app/c;

    invoke-virtual {v0, p1, p2}, Landroid/support/v7/app/c;->b(ILandroid/view/Menu;)V

    goto :goto_0
.end method

.method public final a(Landroid/content/res/Configuration;)V
    .locals 1

    .prologue
    .line 188
    iget-boolean v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->b:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->m:Z

    if-eqz v0, :cond_0

    .line 191
    invoke-virtual {p0}, Landroid/support/v7/app/ActionBarActivityDelegateBase;->b()Landroid/support/v7/app/ActionBar;

    move-result-object v0

    .line 192
    if-eqz v0, :cond_0

    .line 193
    invoke-virtual {v0}, Landroid/support/v7/app/ActionBar;->c()V

    .line 196
    :cond_0
    return-void
.end method

.method final a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 139
    invoke-super {p0, p1}, Landroid/support/v7/app/d;->a(Landroid/os/Bundle;)V

    .line 141
    iget-object v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:Landroid/support/v7/app/c;

    invoke-virtual {v0}, Landroid/support/v7/app/c;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->n:Landroid/view/ViewGroup;

    .line 143
    iget-object v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:Landroid/support/v7/app/c;

    invoke-static {v0}, Landroid/support/v4/app/B;->b(Landroid/app/Activity;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 144
    invoke-virtual {p0}, Landroid/support/v7/app/ActionBarActivityDelegateBase;->b()Landroid/support/v7/app/ActionBar;

    move-result-object v0

    .line 145
    if-nez v0, :cond_1

    .line 146
    iput-boolean v1, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->x:Z

    .line 151
    :cond_0
    :goto_0
    return-void

    .line 148
    :cond_1
    invoke-virtual {v0, v1}, Landroid/support/v7/app/ActionBar;->a(Z)V

    goto :goto_0
.end method

.method public final a(Landroid/support/v7/internal/view/menu/i;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/16 v4, 0x8

    const/4 v3, 0x0

    .line 556
    iget-object v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->j:Landroid/support/v7/internal/widget/t;

    if-eqz v0, :cond_4

    iget-object v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->j:Landroid/support/v7/internal/widget/t;

    invoke-interface {v0}, Landroid/support/v7/internal/widget/t;->c()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:Landroid/support/v7/app/c;

    invoke-static {v0}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    invoke-static {v0}, Landroid/support/v4/view/W;->b(Landroid/view/ViewConfiguration;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->j:Landroid/support/v7/internal/widget/t;

    invoke-interface {v0}, Landroid/support/v7/internal/widget/t;->e()Z

    move-result v0

    if-eqz v0, :cond_4

    :cond_0
    invoke-virtual {p0}, Landroid/support/v7/app/ActionBarActivityDelegateBase;->i()Landroid/support/v7/internal/a/a;

    move-result-object v0

    iget-object v1, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->j:Landroid/support/v7/internal/widget/t;

    invoke-interface {v1}, Landroid/support/v7/internal/widget/t;->d()Z

    move-result v1

    if-nez v1, :cond_3

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Landroid/support/v7/app/ActionBarActivityDelegateBase;->k()Z

    move-result v1

    if-nez v1, :cond_2

    iget-boolean v1, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->u:Z

    if-eqz v1, :cond_1

    iget v1, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->v:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_1

    iget-object v1, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->n:Landroid/view/ViewGroup;

    iget-object v2, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->w:Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->removeCallbacks(Ljava/lang/Runnable;)Z

    iget-object v1, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->w:Ljava/lang/Runnable;

    invoke-interface {v1}, Ljava/lang/Runnable;->run()V

    :cond_1
    invoke-direct {p0, v3}, Landroid/support/v7/app/ActionBarActivityDelegateBase;->c(I)Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;

    move-result-object v1

    iget-object v2, v1, Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;->d:Landroid/support/v7/internal/view/menu/i;

    if-eqz v2, :cond_2

    iget-boolean v2, v1, Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;->k:Z

    if-nez v2, :cond_2

    iget-object v2, v1, Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;->d:Landroid/support/v7/internal/view/menu/i;

    invoke-interface {v0, v3, v5, v2}, Landroid/support/v7/internal/a/a;->a(ILandroid/view/View;Landroid/view/Menu;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v1, v1, Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;->d:Landroid/support/v7/internal/view/menu/i;

    invoke-interface {v0, v4, v1}, Landroid/support/v7/internal/a/a;->c(ILandroid/view/Menu;)Z

    iget-object v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->j:Landroid/support/v7/internal/widget/t;

    invoke-interface {v0}, Landroid/support/v7/internal/widget/t;->f()Z

    .line 557
    :cond_2
    :goto_0
    return-void

    .line 556
    :cond_3
    iget-object v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->j:Landroid/support/v7/internal/widget/t;

    invoke-interface {v0}, Landroid/support/v7/internal/widget/t;->g()Z

    invoke-virtual {p0}, Landroid/support/v7/app/ActionBarActivityDelegateBase;->k()Z

    move-result v0

    if-nez v0, :cond_2

    invoke-direct {p0, v3}, Landroid/support/v7/app/ActionBarActivityDelegateBase;->c(I)Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;

    move-result-object v0

    iget-object v1, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:Landroid/support/v7/app/c;

    iget-object v0, v0, Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;->d:Landroid/support/v7/internal/view/menu/i;

    invoke-virtual {v1, v4, v0}, Landroid/support/v7/app/c;->onPanelClosed(ILandroid/view/Menu;)V

    goto :goto_0

    :cond_4
    invoke-direct {p0, v3}, Landroid/support/v7/app/ActionBarActivityDelegateBase;->c(I)Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;

    move-result-object v0

    const/4 v1, 0x1

    iput-boolean v1, v0, Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;->j:Z

    invoke-direct {p0, v0, v3}, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a(Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;Z)V

    invoke-direct {p0, v0, v5}, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a(Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;Landroid/view/KeyEvent;)V

    goto :goto_0
.end method

.method public final a(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 216
    invoke-direct {p0}, Landroid/support/v7/app/ActionBarActivityDelegateBase;->m()V

    .line 217
    iget-object v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:Landroid/support/v7/app/c;

    const v1, 0x1020002

    invoke-virtual {v0, v1}, Landroid/support/v7/app/c;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 218
    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 219
    invoke-virtual {v0, p1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 220
    iget-object v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:Landroid/support/v7/app/c;

    invoke-static {}, Landroid/support/v7/app/c;->d()V

    .line 221
    return-void
.end method

.method public final a(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    .locals 2

    .prologue
    .line 234
    invoke-direct {p0}, Landroid/support/v7/app/ActionBarActivityDelegateBase;->m()V

    .line 235
    iget-object v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:Landroid/support/v7/app/c;

    const v1, 0x1020002

    invoke-virtual {v0, v1}, Landroid/support/v7/app/c;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 236
    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 237
    invoke-virtual {v0, p1, p2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 238
    iget-object v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:Landroid/support/v7/app/c;

    invoke-static {}, Landroid/support/v7/app/c;->d()V

    .line 239
    return-void
.end method

.method public final a(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 460
    iget-object v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->j:Landroid/support/v7/internal/widget/t;

    if-eqz v0, :cond_0

    .line 461
    iget-object v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->j:Landroid/support/v7/internal/widget/t;

    invoke-interface {v0, p1}, Landroid/support/v7/internal/widget/t;->a(Ljava/lang/CharSequence;)V

    .line 467
    :goto_0
    return-void

    .line 462
    :cond_0
    invoke-virtual {p0}, Landroid/support/v7/app/ActionBarActivityDelegateBase;->b()Landroid/support/v7/app/ActionBar;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 463
    invoke-virtual {p0}, Landroid/support/v7/app/ActionBarActivityDelegateBase;->b()Landroid/support/v7/app/ActionBar;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/support/v7/app/ActionBar;->a(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 465
    :cond_1
    iput-object p1, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->q:Ljava/lang/CharSequence;

    goto :goto_0
.end method

.method a(ILandroid/view/KeyEvent;)Z
    .locals 1

    .prologue
    .line 744
    invoke-virtual {p0, p2}, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a(Landroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method public final a(ILandroid/view/View;Landroid/view/Menu;)Z
    .locals 1

    .prologue
    .line 503
    if-eqz p1, :cond_0

    .line 504
    invoke-virtual {p0}, Landroid/support/v7/app/ActionBarActivityDelegateBase;->i()Landroid/support/v7/internal/a/a;

    move-result-object v0

    invoke-interface {v0, p1, p2, p3}, Landroid/support/v7/internal/a/a;->a(ILandroid/view/View;Landroid/view/Menu;)Z

    move-result v0

    .line 506
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Landroid/support/v7/internal/view/menu/i;Landroid/view/MenuItem;)Z
    .locals 2

    .prologue
    .line 544
    invoke-virtual {p0}, Landroid/support/v7/app/ActionBarActivityDelegateBase;->i()Landroid/support/v7/internal/a/a;

    move-result-object v0

    .line 545
    if-eqz v0, :cond_0

    invoke-virtual {p0}, Landroid/support/v7/app/ActionBarActivityDelegateBase;->k()Z

    move-result v1

    if-nez v1, :cond_0

    .line 546
    invoke-virtual {p1}, Landroid/support/v7/internal/view/menu/i;->q()Landroid/support/v7/internal/view/menu/i;

    move-result-object v1

    invoke-direct {p0, v1}, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a(Landroid/view/Menu;)Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;

    move-result-object v1

    .line 547
    if-eqz v1, :cond_0

    .line 548
    iget v1, v1, Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;->a:I

    invoke-interface {v0, v1, p2}, Landroid/support/v7/internal/a/a;->a(ILandroid/view/MenuItem;)Z

    move-result v0

    .line 551
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method final a(Landroid/view/KeyEvent;)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 713
    iget-object v2, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->t:Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;

    if-eqz v2, :cond_1

    .line 714
    iget-object v2, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->t:Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v3

    invoke-direct {p0, v2, v3, p1, v0}, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a(Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;ILandroid/view/KeyEvent;I)Z

    move-result v2

    .line 716
    if-eqz v2, :cond_1

    .line 717
    iget-object v1, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->t:Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;

    if-eqz v1, :cond_0

    .line 718
    iget-object v1, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->t:Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;

    .line 737
    :cond_0
    :goto_0
    return v0

    .line 728
    :cond_1
    iget-object v2, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->t:Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;

    if-nez v2, :cond_2

    .line 729
    invoke-direct {p0, v1}, Landroid/support/v7/app/ActionBarActivityDelegateBase;->c(I)Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;

    move-result-object v2

    .line 730
    invoke-direct {p0, v2, p1}, Landroid/support/v7/app/ActionBarActivityDelegateBase;->b(Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;Landroid/view/KeyEvent;)Z

    .line 731
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v3

    invoke-direct {p0, v2, v3, p1, v0}, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a(Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;ILandroid/view/KeyEvent;I)Z

    move-result v3

    .line 732
    iput-boolean v1, v2, Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;->g:Z

    .line 733
    if-nez v3, :cond_0

    :cond_2
    move v0, v1

    .line 737
    goto :goto_0
.end method

.method public final b(I)Landroid/view/View;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 471
    .line 474
    iget-object v1, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->f:Landroid/support/v7/b/a;

    if-nez v1, :cond_1

    .line 476
    invoke-virtual {p0}, Landroid/support/v7/app/ActionBarActivityDelegateBase;->i()Landroid/support/v7/internal/a/a;

    move-result-object v1

    .line 477
    if-eqz v1, :cond_0

    .line 481
    :cond_0
    invoke-direct {p0, p1}, Landroid/support/v7/app/ActionBarActivityDelegateBase;->c(I)Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;

    move-result-object v1

    .line 484
    invoke-direct {p0, v1, v0}, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a(Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;Landroid/view/KeyEvent;)V

    .line 485
    iget-boolean v2, v1, Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;->h:Z

    if-eqz v2, :cond_1

    .line 486
    iget-object v0, v1, Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;->c:Landroid/view/View;

    .line 490
    :cond_1
    return-object v0
.end method

.method public final b(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    .locals 2

    .prologue
    .line 243
    invoke-direct {p0}, Landroid/support/v7/app/ActionBarActivityDelegateBase;->m()V

    .line 244
    iget-object v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:Landroid/support/v7/app/c;

    const v1, 0x1020002

    invoke-virtual {v0, v1}, Landroid/support/v7/app/c;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 245
    invoke-virtual {v0, p1, p2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 246
    iget-object v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:Landroid/support/v7/app/c;

    invoke-static {}, Landroid/support/v7/app/c;->d()V

    .line 247
    return-void
.end method

.method final b(ILandroid/view/Menu;)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 531
    const/16 v1, 0x8

    if-ne p1, v1, :cond_1

    .line 532
    invoke-virtual {p0}, Landroid/support/v7/app/ActionBarActivityDelegateBase;->b()Landroid/support/v7/app/ActionBar;

    move-result-object v1

    .line 533
    if-eqz v1, :cond_0

    .line 534
    invoke-virtual {v1, v0}, Landroid/support/v7/app/ActionBar;->c(Z)V

    .line 538
    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:Landroid/support/v7/app/c;

    invoke-virtual {v0, p1, p2}, Landroid/support/v7/app/c;->c(ILandroid/view/Menu;)Z

    move-result v0

    goto :goto_0
.end method

.method public final c(ILandroid/view/Menu;)Z
    .locals 1

    .prologue
    .line 495
    if-eqz p1, :cond_0

    .line 496
    invoke-virtual {p0}, Landroid/support/v7/app/ActionBarActivityDelegateBase;->i()Landroid/support/v7/internal/a/a;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Landroid/support/v7/internal/a/a;->a(ILandroid/view/Menu;)Z

    move-result v0

    .line 498
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 200
    invoke-virtual {p0}, Landroid/support/v7/app/ActionBarActivityDelegateBase;->b()Landroid/support/v7/app/ActionBar;

    move-result-object v0

    .line 201
    if-eqz v0, :cond_0

    .line 202
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v7/app/ActionBar;->b(Z)V

    .line 204
    :cond_0
    return-void
.end method

.method public final e()V
    .locals 2

    .prologue
    .line 208
    invoke-virtual {p0}, Landroid/support/v7/app/ActionBarActivityDelegateBase;->b()Landroid/support/v7/app/ActionBar;

    move-result-object v0

    .line 209
    if-eqz v0, :cond_0

    .line 210
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/support/v7/app/ActionBar;->b(Z)V

    .line 212
    :cond_0
    return-void
.end method

.method public final f()V
    .locals 1

    .prologue
    .line 589
    invoke-virtual {p0}, Landroid/support/v7/app/ActionBarActivityDelegateBase;->b()Landroid/support/v7/app/ActionBar;

    .line 590
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Landroid/support/v7/app/ActionBarActivityDelegateBase;->d(I)V

    .line 593
    return-void
.end method

.method public final g()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 667
    iget-object v1, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->f:Landroid/support/v7/b/a;

    if-eqz v1, :cond_1

    .line 668
    iget-object v1, p0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->f:Landroid/support/v7/b/a;

    invoke-virtual {v1}, Landroid/support/v7/b/a;->b()V

    .line 678
    :cond_0
    :goto_0
    return v0

    .line 673
    :cond_1
    invoke-virtual {p0}, Landroid/support/v7/app/ActionBarActivityDelegateBase;->b()Landroid/support/v7/app/ActionBar;

    move-result-object v1

    .line 674
    if-eqz v1, :cond_2

    invoke-virtual {v1}, Landroid/support/v7/app/ActionBar;->d()Z

    move-result v1

    if-nez v1, :cond_0

    .line 678
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method l()V
    .locals 0

    .prologue
    .line 378
    return-void
.end method
