.class final Landroid/support/v7/app/z;
.super Landroid/support/v7/media/g;
.source "MediaRouteControllerDialog.java"


# instance fields
.field private synthetic a:Landroid/support/v7/app/v;


# direct methods
.method private constructor <init>(Landroid/support/v7/app/v;)V
    .locals 0

    .prologue
    .line 297
    iput-object p1, p0, Landroid/support/v7/app/z;->a:Landroid/support/v7/app/v;

    invoke-direct {p0}, Landroid/support/v7/media/g;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Landroid/support/v7/app/v;B)V
    .locals 0

    .prologue
    .line 297
    invoke-direct {p0, p1}, Landroid/support/v7/app/z;-><init>(Landroid/support/v7/app/v;)V

    return-void
.end method


# virtual methods
.method public final onRouteChanged(Landroid/support/v7/media/MediaRouter;Landroid/support/v7/media/MediaRouter$RouteInfo;)V
    .locals 1

    .prologue
    .line 305
    iget-object v0, p0, Landroid/support/v7/app/z;->a:Landroid/support/v7/app/v;

    invoke-static {v0}, Landroid/support/v7/app/v;->f(Landroid/support/v7/app/v;)Z

    .line 306
    return-void
.end method

.method public final onRouteUnselected(Landroid/support/v7/media/MediaRouter;Landroid/support/v7/media/MediaRouter$RouteInfo;)V
    .locals 1

    .prologue
    .line 300
    iget-object v0, p0, Landroid/support/v7/app/z;->a:Landroid/support/v7/app/v;

    invoke-static {v0}, Landroid/support/v7/app/v;->f(Landroid/support/v7/app/v;)Z

    .line 301
    return-void
.end method

.method public final onRouteVolumeChanged(Landroid/support/v7/media/MediaRouter;Landroid/support/v7/media/MediaRouter$RouteInfo;)V
    .locals 1

    .prologue
    .line 310
    iget-object v0, p0, Landroid/support/v7/app/z;->a:Landroid/support/v7/app/v;

    invoke-static {v0}, Landroid/support/v7/app/v;->d(Landroid/support/v7/app/v;)Landroid/support/v7/media/MediaRouter$RouteInfo;

    move-result-object v0

    if-ne p2, v0, :cond_0

    .line 311
    iget-object v0, p0, Landroid/support/v7/app/z;->a:Landroid/support/v7/app/v;

    invoke-static {v0}, Landroid/support/v7/app/v;->b(Landroid/support/v7/app/v;)V

    .line 313
    :cond_0
    return-void
.end method
