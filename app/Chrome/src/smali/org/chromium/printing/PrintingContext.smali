.class public Lorg/chromium/printing/PrintingContext;
.super Ljava/lang/Object;
.source "PrintingContext.java"

# interfaces
.implements Lorg/chromium/printing/PrintingContextInterface;


# annotations
.annotation runtime Lorg/chromium/base/JNINamespace;
.end annotation


# static fields
.field private static final PRINTING_CONTEXT_MAP:Landroid/util/SparseArray;


# instance fields
.field private final mController:Lorg/chromium/printing/PrintingController;

.field private final mNativeObject:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 29
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    sput-object v0, Lorg/chromium/printing/PrintingContext;->PRINTING_CONTEXT_MAP:Landroid/util/SparseArray;

    return-void
.end method

.method private constructor <init>(J)V
    .locals 1

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    invoke-static {}, Lorg/chromium/printing/PrintingControllerImpl;->getInstance()Lorg/chromium/printing/PrintingController;

    move-result-object v0

    iput-object v0, p0, Lorg/chromium/printing/PrintingContext;->mController:Lorg/chromium/printing/PrintingController;

    .line 40
    iput-wide p1, p0, Lorg/chromium/printing/PrintingContext;->mNativeObject:J

    .line 41
    return-void
.end method

.method public static create(J)Lorg/chromium/printing/PrintingContext;
    .locals 2

    .prologue
    .line 71
    invoke-static {}, Lorg/chromium/base/ThreadUtils;->assertOnUiThread()V

    .line 72
    new-instance v0, Lorg/chromium/printing/PrintingContext;

    invoke-direct {v0, p0, p1}, Lorg/chromium/printing/PrintingContext;-><init>(J)V

    return-object v0
.end method

.method private native nativeAskUserForSettingsReply(JZ)V
.end method

.method public static pdfWritingDone(IZ)V
    .locals 1

    .prologue
    .line 101
    invoke-static {}, Lorg/chromium/base/ThreadUtils;->assertOnUiThread()V

    .line 103
    sget-object v0, Lorg/chromium/printing/PrintingContext;->PRINTING_CONTEXT_MAP:Landroid/util/SparseArray;

    invoke-virtual {v0, p0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 104
    invoke-static {}, Lorg/chromium/base/ThreadUtils;->assertOnUiThread()V

    .line 105
    sget-object v0, Lorg/chromium/printing/PrintingContext;->PRINTING_CONTEXT_MAP:Landroid/util/SparseArray;

    invoke-virtual {v0, p0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/chromium/printing/PrintingContext;

    .line 106
    iget-object v0, v0, Lorg/chromium/printing/PrintingContext;->mController:Lorg/chromium/printing/PrintingController;

    invoke-interface {v0, p1}, Lorg/chromium/printing/PrintingController;->pdfWritingDone(Z)V

    .line 107
    sget-object v0, Lorg/chromium/printing/PrintingContext;->PRINTING_CONTEXT_MAP:Landroid/util/SparseArray;

    invoke-virtual {v0, p0}, Landroid/util/SparseArray;->remove(I)V

    .line 109
    :cond_0
    return-void
.end method


# virtual methods
.method public askUserForSettingsReply(Z)V
    .locals 2

    .prologue
    .line 66
    iget-wide v0, p0, Lorg/chromium/printing/PrintingContext;->mNativeObject:J

    invoke-direct {p0, v0, v1, p1}, Lorg/chromium/printing/PrintingContext;->nativeAskUserForSettingsReply(JZ)V

    .line 67
    return-void
.end method

.method public getDpi()I
    .locals 1

    .prologue
    .line 83
    invoke-static {}, Lorg/chromium/base/ThreadUtils;->assertOnUiThread()V

    .line 84
    iget-object v0, p0, Lorg/chromium/printing/PrintingContext;->mController:Lorg/chromium/printing/PrintingController;

    invoke-interface {v0}, Lorg/chromium/printing/PrintingController;->getDpi()I

    move-result v0

    return v0
.end method

.method public getFileDescriptor()I
    .locals 1

    .prologue
    .line 77
    invoke-static {}, Lorg/chromium/base/ThreadUtils;->assertOnUiThread()V

    .line 78
    iget-object v0, p0, Lorg/chromium/printing/PrintingContext;->mController:Lorg/chromium/printing/PrintingController;

    invoke-interface {v0}, Lorg/chromium/printing/PrintingController;->getFileDescriptor()I

    move-result v0

    return v0
.end method

.method public getHeight()I
    .locals 1

    .prologue
    .line 95
    invoke-static {}, Lorg/chromium/base/ThreadUtils;->assertOnUiThread()V

    .line 96
    iget-object v0, p0, Lorg/chromium/printing/PrintingContext;->mController:Lorg/chromium/printing/PrintingController;

    invoke-interface {v0}, Lorg/chromium/printing/PrintingController;->getPageHeight()I

    move-result v0

    return v0
.end method

.method public getPages()[I
    .locals 1

    .prologue
    .line 113
    invoke-static {}, Lorg/chromium/base/ThreadUtils;->assertOnUiThread()V

    .line 114
    iget-object v0, p0, Lorg/chromium/printing/PrintingContext;->mController:Lorg/chromium/printing/PrintingController;

    invoke-interface {v0}, Lorg/chromium/printing/PrintingController;->getPageNumbers()[I

    move-result-object v0

    return-object v0
.end method

.method public getWidth()I
    .locals 1

    .prologue
    .line 89
    invoke-static {}, Lorg/chromium/base/ThreadUtils;->assertOnUiThread()V

    .line 90
    iget-object v0, p0, Lorg/chromium/printing/PrintingContext;->mController:Lorg/chromium/printing/PrintingController;

    invoke-interface {v0}, Lorg/chromium/printing/PrintingController;->getPageWidth()I

    move-result v0

    return v0
.end method

.method public pageCountEstimationDone(I)V
    .locals 3

    .prologue
    .line 119
    invoke-static {}, Lorg/chromium/base/ThreadUtils;->assertOnUiThread()V

    .line 121
    iget-object v0, p0, Lorg/chromium/printing/PrintingContext;->mController:Lorg/chromium/printing/PrintingController;

    invoke-interface {v0}, Lorg/chromium/printing/PrintingController;->hasPrintingFinished()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 125
    iget-wide v0, p0, Lorg/chromium/printing/PrintingContext;->mNativeObject:J

    const/4 v2, 0x0

    invoke-direct {p0, v0, v1, v2}, Lorg/chromium/printing/PrintingContext;->nativeAskUserForSettingsReply(JZ)V

    .line 130
    :goto_0
    return-void

    .line 127
    :cond_0
    iget-object v0, p0, Lorg/chromium/printing/PrintingContext;->mController:Lorg/chromium/printing/PrintingController;

    invoke-interface {v0, p0}, Lorg/chromium/printing/PrintingController;->setPrintingContext(Lorg/chromium/printing/PrintingContextInterface;)V

    .line 128
    iget-object v0, p0, Lorg/chromium/printing/PrintingContext;->mController:Lorg/chromium/printing/PrintingController;

    invoke-interface {v0, p1}, Lorg/chromium/printing/PrintingController;->pageCountEstimationDone(I)V

    goto :goto_0
.end method

.method public updatePrintingContextMap(IZ)V
    .locals 1

    .prologue
    .line 51
    invoke-static {}, Lorg/chromium/base/ThreadUtils;->assertOnUiThread()V

    .line 52
    if-eqz p2, :cond_0

    .line 53
    sget-object v0, Lorg/chromium/printing/PrintingContext;->PRINTING_CONTEXT_MAP:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->remove(I)V

    .line 57
    :goto_0
    return-void

    .line 55
    :cond_0
    sget-object v0, Lorg/chromium/printing/PrintingContext;->PRINTING_CONTEXT_MAP:Landroid/util/SparseArray;

    invoke-virtual {v0, p1, p0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    goto :goto_0
.end method
