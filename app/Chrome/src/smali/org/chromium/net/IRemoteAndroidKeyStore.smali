.class public interface abstract Lorg/chromium/net/IRemoteAndroidKeyStore;
.super Ljava/lang/Object;
.source "IRemoteAndroidKeyStore.java"

# interfaces
.implements Landroid/os/IInterface;


# virtual methods
.method public abstract getClientCertificateAlias()Ljava/lang/String;
.end method

.method public abstract getDSAKeyParamQ(I)[B
.end method

.method public abstract getECKeyOrder(I)[B
.end method

.method public abstract getEncodedCertificateChain(Ljava/lang/String;)[B
.end method

.method public abstract getPrivateKeyEncodedBytes(I)[B
.end method

.method public abstract getPrivateKeyHandle(Ljava/lang/String;)I
.end method

.method public abstract getPrivateKeyType(I)I
.end method

.method public abstract getRSAKeyModulus(I)[B
.end method

.method public abstract rawSignDigestWithPrivateKey(I[B)[B
.end method

.method public abstract releaseKey(I)V
.end method

.method public abstract setClientCallbacks(Lorg/chromium/net/IRemoteAndroidKeyStoreCallbacks;)V
.end method
