.class public final Lorg/chromium/components/dom_distiller/core/DomDistillerUrlUtils;
.super Ljava/lang/Object;
.source "DomDistillerUrlUtils.java"


# annotations
.annotation runtime Lorg/chromium/base/JNINamespace;
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    return-void
.end method

.method public static getIsDistillableJs()Ljava/lang/String;
    .locals 1

    .prologue
    .line 55
    invoke-static {}, Lorg/chromium/components/dom_distiller/core/DomDistillerUrlUtils;->nativeGetIsDistillableJs()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getOriginalUrlFromDistillerUrl(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 35
    invoke-static {p0}, Lorg/chromium/components/dom_distiller/core/DomDistillerUrlUtils;->nativeGetOriginalUrlFromDistillerUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getValueForKeyInUrl(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 59
    invoke-static {p0, p1}, Lorg/chromium/components/dom_distiller/core/DomDistillerUrlUtils;->nativeGetValueForKeyInUrl(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static isDistilledPage(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 45
    invoke-static {p0}, Lorg/chromium/components/dom_distiller/core/DomDistillerUrlUtils;->nativeIsDistilledPage(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static isUrlDistillable(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 49
    invoke-static {p0}, Lorg/chromium/components/dom_distiller/core/DomDistillerUrlUtils;->nativeIsUrlDistillable(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method private static native nativeGetDistillerViewUrlFromUrl(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
.end method

.method private static native nativeGetIsDistillableJs()Ljava/lang/String;
.end method

.method private static native nativeGetOriginalUrlFromDistillerUrl(Ljava/lang/String;)Ljava/lang/String;
.end method

.method private static native nativeGetValueForKeyInUrl(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
.end method

.method private static native nativeIsDistilledPage(Ljava/lang/String;)Z
.end method

.method private static native nativeIsUrlDistillable(Ljava/lang/String;)Z
.end method
