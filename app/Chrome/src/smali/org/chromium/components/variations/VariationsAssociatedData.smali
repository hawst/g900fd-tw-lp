.class public final Lorg/chromium/components/variations/VariationsAssociatedData;
.super Ljava/lang/Object;
.source "VariationsAssociatedData.java"


# annotations
.annotation runtime Lorg/chromium/base/JNINamespace;
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    return-void
.end method

.method public static getVariationParamValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 25
    invoke-static {p0, p1}, Lorg/chromium/components/variations/VariationsAssociatedData;->nativeGetVariationParamValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static native nativeGetVariationParamValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
.end method
