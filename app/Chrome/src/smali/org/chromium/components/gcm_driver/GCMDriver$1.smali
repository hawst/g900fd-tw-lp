.class Lorg/chromium/components/gcm_driver/GCMDriver$1;
.super Landroid/os/AsyncTask;
.source "GCMDriver.java"


# instance fields
.field final synthetic this$0:Lorg/chromium/components/gcm_driver/GCMDriver;

.field final synthetic val$appId:Ljava/lang/String;

.field final synthetic val$senderIds:[Ljava/lang/String;


# direct methods
.method constructor <init>(Lorg/chromium/components/gcm_driver/GCMDriver;[Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 80
    iput-object p1, p0, Lorg/chromium/components/gcm_driver/GCMDriver$1;->this$0:Lorg/chromium/components/gcm_driver/GCMDriver;

    iput-object p2, p0, Lorg/chromium/components/gcm_driver/GCMDriver$1;->val$senderIds:[Ljava/lang/String;

    iput-object p3, p0, Lorg/chromium/components/gcm_driver/GCMDriver$1;->val$appId:Ljava/lang/String;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 80
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lorg/chromium/components/gcm_driver/GCMDriver$1;->doInBackground([Ljava/lang/Void;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 84
    :try_start_0
    iget-object v0, p0, Lorg/chromium/components/gcm_driver/GCMDriver$1;->this$0:Lorg/chromium/components/gcm_driver/GCMDriver;

    # getter for: Lorg/chromium/components/gcm_driver/GCMDriver;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lorg/chromium/components/gcm_driver/GCMDriver;->access$000(Lorg/chromium/components/gcm_driver/GCMDriver;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/a/c;->a(Landroid/content/Context;)V
    :try_end_0
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_0 .. :try_end_0} :catch_0

    .line 89
    iget-object v0, p0, Lorg/chromium/components/gcm_driver/GCMDriver$1;->this$0:Lorg/chromium/components/gcm_driver/GCMDriver;

    # getter for: Lorg/chromium/components/gcm_driver/GCMDriver;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lorg/chromium/components/gcm_driver/GCMDriver;->access$000(Lorg/chromium/components/gcm_driver/GCMDriver;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/a/c;->b(Landroid/content/Context;)V

    .line 90
    iget-object v0, p0, Lorg/chromium/components/gcm_driver/GCMDriver$1;->this$0:Lorg/chromium/components/gcm_driver/GCMDriver;

    # getter for: Lorg/chromium/components/gcm_driver/GCMDriver;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lorg/chromium/components/gcm_driver/GCMDriver;->access$000(Lorg/chromium/components/gcm_driver/GCMDriver;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/a/c;->f(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 91
    const-string/jumbo v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 95
    iget-object v0, p0, Lorg/chromium/components/gcm_driver/GCMDriver$1;->this$0:Lorg/chromium/components/gcm_driver/GCMDriver;

    # getter for: Lorg/chromium/components/gcm_driver/GCMDriver;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lorg/chromium/components/gcm_driver/GCMDriver;->access$000(Lorg/chromium/components/gcm_driver/GCMDriver;)Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lorg/chromium/components/gcm_driver/GCMDriver$1;->val$senderIds:[Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/google/android/a/c;->a(Landroid/content/Context;[Ljava/lang/String;)V

    .line 96
    const/4 v0, 0x0

    .line 99
    :goto_0
    return-object v0

    .line 86
    :catch_0
    move-exception v0

    const-string/jumbo v0, ""

    goto :goto_0

    .line 98
    :cond_0
    const-string/jumbo v1, "GCMDriver"

    const-string/jumbo v2, "Re-using existing registration ID"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 80
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lorg/chromium/components/gcm_driver/GCMDriver$1;->onPostExecute(Ljava/lang/String;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/String;)V
    .locals 7

    .prologue
    .line 104
    if-nez p1, :cond_0

    .line 109
    :goto_0
    return-void

    .line 107
    :cond_0
    iget-object v1, p0, Lorg/chromium/components/gcm_driver/GCMDriver$1;->this$0:Lorg/chromium/components/gcm_driver/GCMDriver;

    iget-object v0, p0, Lorg/chromium/components/gcm_driver/GCMDriver$1;->this$0:Lorg/chromium/components/gcm_driver/GCMDriver;

    # getter for: Lorg/chromium/components/gcm_driver/GCMDriver;->mNativeGCMDriverAndroid:J
    invoke-static {v0}, Lorg/chromium/components/gcm_driver/GCMDriver;->access$100(Lorg/chromium/components/gcm_driver/GCMDriver;)J

    move-result-wide v2

    iget-object v4, p0, Lorg/chromium/components/gcm_driver/GCMDriver$1;->val$appId:Ljava/lang/String;

    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v6, 0x1

    :goto_1
    move-object v5, p1

    # invokes: Lorg/chromium/components/gcm_driver/GCMDriver;->nativeOnRegisterFinished(JLjava/lang/String;Ljava/lang/String;Z)V
    invoke-static/range {v1 .. v6}, Lorg/chromium/components/gcm_driver/GCMDriver;->access$200(Lorg/chromium/components/gcm_driver/GCMDriver;JLjava/lang/String;Ljava/lang/String;Z)V

    goto :goto_0

    :cond_1
    const/4 v6, 0x0

    goto :goto_1
.end method
