.class public Lorg/chromium/components/gcm_driver/GCMListener;
.super Lcom/google/ipc/invalidation/external/client/contrib/MultiplexingGcmListener$AbstractListener;
.source "GCMListener.java"


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 37
    const-string/jumbo v0, "GCMListener"

    invoke-direct {p0, v0}, Lcom/google/ipc/invalidation/external/client/contrib/MultiplexingGcmListener$AbstractListener;-><init>(Ljava/lang/String;)V

    .line 38
    return-void
.end method


# virtual methods
.method protected onDeletedMessages(I)V
    .locals 1

    .prologue
    .line 70
    new-instance v0, Lorg/chromium/components/gcm_driver/GCMListener$4;

    invoke-direct {v0, p0}, Lorg/chromium/components/gcm_driver/GCMListener$4;-><init>(Lorg/chromium/components/gcm_driver/GCMListener;)V

    invoke-static {v0}, Lorg/chromium/base/ThreadUtils;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 75
    return-void
.end method

.method protected onMessage(Landroid/content/Intent;)V
    .locals 1

    .prologue
    .line 60
    new-instance v0, Lorg/chromium/components/gcm_driver/GCMListener$3;

    invoke-direct {v0, p0, p1}, Lorg/chromium/components/gcm_driver/GCMListener$3;-><init>(Lorg/chromium/components/gcm_driver/GCMListener;Landroid/content/Intent;)V

    invoke-static {v0}, Lorg/chromium/base/ThreadUtils;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 66
    return-void
.end method

.method protected onRegistered(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 42
    new-instance v0, Lorg/chromium/components/gcm_driver/GCMListener$1;

    invoke-direct {v0, p0, p1}, Lorg/chromium/components/gcm_driver/GCMListener$1;-><init>(Lorg/chromium/components/gcm_driver/GCMListener;Ljava/lang/String;)V

    invoke-static {v0}, Lorg/chromium/base/ThreadUtils;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 47
    return-void
.end method

.method protected onUnregistered(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 51
    new-instance v0, Lorg/chromium/components/gcm_driver/GCMListener$2;

    invoke-direct {v0, p0}, Lorg/chromium/components/gcm_driver/GCMListener$2;-><init>(Lorg/chromium/components/gcm_driver/GCMListener;)V

    invoke-static {v0}, Lorg/chromium/base/ThreadUtils;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 56
    return-void
.end method
