.class public Lorg/chromium/components/gcm_driver/GCMDriver;
.super Ljava/lang/Object;
.source "GCMDriver.java"


# annotations
.annotation runtime Lorg/chromium/base/JNINamespace;
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z

.field private static sInstance:Lorg/chromium/components/gcm_driver/GCMDriver;


# instance fields
.field private final mContext:Landroid/content/Context;

.field private mNativeGCMDriverAndroid:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 32
    const-class v0, Lorg/chromium/components/gcm_driver/GCMDriver;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/chromium/components/gcm_driver/GCMDriver;->$assertionsDisabled:Z

    .line 39
    const/4 v0, 0x0

    sput-object v0, Lorg/chromium/components/gcm_driver/GCMDriver;->sInstance:Lorg/chromium/components/gcm_driver/GCMDriver;

    return-void

    .line 32
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private constructor <init>(JLandroid/content/Context;)V
    .locals 1

    .prologue
    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    iput-wide p1, p0, Lorg/chromium/components/gcm_driver/GCMDriver;->mNativeGCMDriverAndroid:J

    .line 46
    iput-object p3, p0, Lorg/chromium/components/gcm_driver/GCMDriver;->mContext:Landroid/content/Context;

    .line 47
    return-void
.end method

.method static synthetic access$000(Lorg/chromium/components/gcm_driver/GCMDriver;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lorg/chromium/components/gcm_driver/GCMDriver;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$100(Lorg/chromium/components/gcm_driver/GCMDriver;)J
    .locals 2

    .prologue
    .line 33
    iget-wide v0, p0, Lorg/chromium/components/gcm_driver/GCMDriver;->mNativeGCMDriverAndroid:J

    return-wide v0
.end method

.method static synthetic access$200(Lorg/chromium/components/gcm_driver/GCMDriver;JLjava/lang/String;Ljava/lang/String;Z)V
    .locals 1

    .prologue
    .line 33
    invoke-direct/range {p0 .. p5}, Lorg/chromium/components/gcm_driver/GCMDriver;->nativeOnRegisterFinished(JLjava/lang/String;Ljava/lang/String;Z)V

    return-void
.end method

.method static synthetic access$300(Lorg/chromium/components/gcm_driver/GCMDriver;JLjava/lang/String;Z)V
    .locals 1

    .prologue
    .line 33
    invoke-direct {p0, p1, p2, p3, p4}, Lorg/chromium/components/gcm_driver/GCMDriver;->nativeOnUnregisterFinished(JLjava/lang/String;Z)V

    return-void
.end method

.method static synthetic access$400()Lorg/chromium/components/gcm_driver/GCMDriver;
    .locals 1

    .prologue
    .line 33
    sget-object v0, Lorg/chromium/components/gcm_driver/GCMDriver;->sInstance:Lorg/chromium/components/gcm_driver/GCMDriver;

    return-object v0
.end method

.method static synthetic access$500()Ljava/lang/String;
    .locals 1

    .prologue
    .line 33
    invoke-static {}, Lorg/chromium/components/gcm_driver/GCMDriver;->getLastAppId()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$600(Lorg/chromium/components/gcm_driver/GCMDriver;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 33
    invoke-direct/range {p0 .. p6}, Lorg/chromium/components/gcm_driver/GCMDriver;->nativeOnMessageReceived(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$700(Lorg/chromium/components/gcm_driver/GCMDriver;JLjava/lang/String;)V
    .locals 1

    .prologue
    .line 33
    invoke-direct {p0, p1, p2, p3}, Lorg/chromium/components/gcm_driver/GCMDriver;->nativeOnMessagesDeleted(JLjava/lang/String;)V

    return-void
.end method

.method private static create(JLandroid/content/Context;)Lorg/chromium/components/gcm_driver/GCMDriver;
    .locals 2

    .prologue
    .line 59
    sget-object v0, Lorg/chromium/components/gcm_driver/GCMDriver;->sInstance:Lorg/chromium/components/gcm_driver/GCMDriver;

    if-eqz v0, :cond_0

    .line 60
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "Already instantiated"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 62
    :cond_0
    new-instance v0, Lorg/chromium/components/gcm_driver/GCMDriver;

    invoke-direct {v0, p0, p1, p2}, Lorg/chromium/components/gcm_driver/GCMDriver;-><init>(JLandroid/content/Context;)V

    .line 63
    sput-object v0, Lorg/chromium/components/gcm_driver/GCMDriver;->sInstance:Lorg/chromium/components/gcm_driver/GCMDriver;

    return-object v0
.end method

.method private destroy()V
    .locals 2

    .prologue
    .line 72
    sget-boolean v0, Lorg/chromium/components/gcm_driver/GCMDriver;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    sget-object v0, Lorg/chromium/components/gcm_driver/GCMDriver;->sInstance:Lorg/chromium/components/gcm_driver/GCMDriver;

    if-eq v0, p0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 73
    :cond_0
    const/4 v0, 0x0

    sput-object v0, Lorg/chromium/components/gcm_driver/GCMDriver;->sInstance:Lorg/chromium/components/gcm_driver/GCMDriver;

    .line 74
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lorg/chromium/components/gcm_driver/GCMDriver;->mNativeGCMDriverAndroid:J

    .line 75
    return-void
.end method

.method private static getLastAppId()Ljava/lang/String;
    .locals 3

    .prologue
    .line 229
    sget-object v0, Lorg/chromium/components/gcm_driver/GCMDriver;->sInstance:Lorg/chromium/components/gcm_driver/GCMDriver;

    iget-object v0, v0, Lorg/chromium/components/gcm_driver/GCMDriver;->mContext:Landroid/content/Context;

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 231
    const-string/jumbo v1, "last_gcm_app_id"

    const-string/jumbo v2, "push#unknown_app_id#0"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static launchNativeThen(Landroid/content/Context;Ljava/lang/Runnable;)V
    .locals 3

    .prologue
    .line 243
    sget-object v0, Lorg/chromium/components/gcm_driver/GCMDriver;->sInstance:Lorg/chromium/components/gcm_driver/GCMDriver;

    if-eqz v0, :cond_0

    .line 244
    invoke-interface {p1}, Ljava/lang/Runnable;->run()V

    .line 264
    :goto_0
    return-void

    .line 252
    :cond_0
    :try_start_0
    invoke-static {p0}, Lorg/chromium/content/browser/BrowserStartupController;->get(Landroid/content/Context;)Lorg/chromium/content/browser/BrowserStartupController;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lorg/chromium/content/browser/BrowserStartupController;->startBrowserProcessesSync(Z)V

    .line 253
    sget-object v0, Lorg/chromium/components/gcm_driver/GCMDriver;->sInstance:Lorg/chromium/components/gcm_driver/GCMDriver;

    if-eqz v0, :cond_1

    .line 254
    invoke-interface {p1}, Ljava/lang/Runnable;->run()V
    :try_end_0
    .catch Lorg/chromium/base/library_loader/ProcessInitException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 258
    :catch_0
    move-exception v0

    .line 259
    const-string/jumbo v1, "GCMDriver"

    const-string/jumbo v2, "Failed to start browser process."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 260
    const/4 v0, -0x1

    invoke-static {v0}, Ljava/lang/System;->exit(I)V

    goto :goto_0

    .line 256
    :cond_1
    :try_start_1
    const-string/jumbo v0, "GCMDriver"

    const-string/jumbo v1, "Started browser process, but failed to instantiate GCMDriver."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Lorg/chromium/base/library_loader/ProcessInitException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method

.method private native nativeOnMessageReceived(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V
.end method

.method private native nativeOnMessagesDeleted(JLjava/lang/String;)V
.end method

.method private native nativeOnRegisterFinished(JLjava/lang/String;Ljava/lang/String;Z)V
.end method

.method private native nativeOnUnregisterFinished(JLjava/lang/String;Z)V
.end method

.method static onMessageReceived(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 165
    const-string/jumbo v0, "data"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 206
    :goto_0
    return-void

    .line 181
    :cond_0
    invoke-static {}, Lorg/chromium/base/ThreadUtils;->assertOnUiThread()V

    .line 182
    new-instance v0, Lorg/chromium/components/gcm_driver/GCMDriver$3;

    invoke-direct {v0, p2}, Lorg/chromium/components/gcm_driver/GCMDriver$3;-><init>(Landroid/os/Bundle;)V

    invoke-static {p0, v0}, Lorg/chromium/components/gcm_driver/GCMDriver;->launchNativeThen(Landroid/content/Context;Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method static onMessagesDeleted(Landroid/content/Context;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 210
    invoke-static {}, Lorg/chromium/base/ThreadUtils;->assertOnUiThread()V

    .line 211
    new-instance v0, Lorg/chromium/components/gcm_driver/GCMDriver$4;

    invoke-direct {v0}, Lorg/chromium/components/gcm_driver/GCMDriver$4;-><init>()V

    invoke-static {p0, v0}, Lorg/chromium/components/gcm_driver/GCMDriver;->launchNativeThen(Landroid/content/Context;Ljava/lang/Runnable;)V

    .line 217
    return-void
.end method

.method static onRegisterFinished(Ljava/lang/String;Ljava/lang/String;)V
    .locals 7

    .prologue
    .line 145
    invoke-static {}, Lorg/chromium/base/ThreadUtils;->assertOnUiThread()V

    .line 148
    sget-object v0, Lorg/chromium/components/gcm_driver/GCMDriver;->sInstance:Lorg/chromium/components/gcm_driver/GCMDriver;

    if-eqz v0, :cond_0

    .line 149
    sget-object v1, Lorg/chromium/components/gcm_driver/GCMDriver;->sInstance:Lorg/chromium/components/gcm_driver/GCMDriver;

    sget-object v0, Lorg/chromium/components/gcm_driver/GCMDriver;->sInstance:Lorg/chromium/components/gcm_driver/GCMDriver;

    iget-wide v2, v0, Lorg/chromium/components/gcm_driver/GCMDriver;->mNativeGCMDriverAndroid:J

    invoke-static {}, Lorg/chromium/components/gcm_driver/GCMDriver;->getLastAppId()Ljava/lang/String;

    move-result-object v4

    const/4 v6, 0x1

    move-object v5, p1

    invoke-direct/range {v1 .. v6}, Lorg/chromium/components/gcm_driver/GCMDriver;->nativeOnRegisterFinished(JLjava/lang/String;Ljava/lang/String;Z)V

    .line 152
    :cond_0
    return-void
.end method

.method static onUnregisterFinished(Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 155
    invoke-static {}, Lorg/chromium/base/ThreadUtils;->assertOnUiThread()V

    .line 158
    sget-object v0, Lorg/chromium/components/gcm_driver/GCMDriver;->sInstance:Lorg/chromium/components/gcm_driver/GCMDriver;

    if-eqz v0, :cond_0

    .line 159
    sget-object v0, Lorg/chromium/components/gcm_driver/GCMDriver;->sInstance:Lorg/chromium/components/gcm_driver/GCMDriver;

    sget-object v1, Lorg/chromium/components/gcm_driver/GCMDriver;->sInstance:Lorg/chromium/components/gcm_driver/GCMDriver;

    iget-wide v2, v1, Lorg/chromium/components/gcm_driver/GCMDriver;->mNativeGCMDriverAndroid:J

    invoke-static {}, Lorg/chromium/components/gcm_driver/GCMDriver;->getLastAppId()Ljava/lang/String;

    move-result-object v1

    const/4 v4, 0x1

    invoke-direct {v0, v2, v3, v1, v4}, Lorg/chromium/components/gcm_driver/GCMDriver;->nativeOnUnregisterFinished(JLjava/lang/String;Z)V

    .line 162
    :cond_0
    return-void
.end method

.method private register(Ljava/lang/String;[Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 79
    invoke-static {p1}, Lorg/chromium/components/gcm_driver/GCMDriver;->setLastAppId(Ljava/lang/String;)V

    .line 80
    new-instance v0, Lorg/chromium/components/gcm_driver/GCMDriver$1;

    invoke-direct {v0, p0, p2, p1}, Lorg/chromium/components/gcm_driver/GCMDriver$1;-><init>(Lorg/chromium/components/gcm_driver/GCMDriver;[Ljava/lang/String;Ljava/lang/String;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lorg/chromium/components/gcm_driver/GCMDriver$1;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 111
    return-void
.end method

.method private static setLastAppId(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 235
    sget-object v0, Lorg/chromium/components/gcm_driver/GCMDriver;->sInstance:Lorg/chromium/components/gcm_driver/GCMDriver;

    iget-object v0, v0, Lorg/chromium/components/gcm_driver/GCMDriver;->mContext:Landroid/content/Context;

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 237
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 238
    const-string/jumbo v1, "last_gcm_app_id"

    invoke-interface {v0, v1, p0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 239
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 240
    return-void
.end method

.method private unregister(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 117
    new-instance v0, Lorg/chromium/components/gcm_driver/GCMDriver$2;

    invoke-direct {v0, p0, p1}, Lorg/chromium/components/gcm_driver/GCMDriver$2;-><init>(Lorg/chromium/components/gcm_driver/GCMDriver;Ljava/lang/String;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lorg/chromium/components/gcm_driver/GCMDriver$2;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 142
    return-void
.end method
