.class final Lorg/chromium/components/gcm_driver/GCMDriver$3;
.super Ljava/lang/Object;
.source "GCMDriver.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic val$extras:Landroid/os/Bundle;


# direct methods
.method constructor <init>(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 182
    iput-object p1, p0, Lorg/chromium/components/gcm_driver/GCMDriver$3;->val$extras:Landroid/os/Bundle;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 8

    .prologue
    .line 184
    iget-object v0, p0, Lorg/chromium/components/gcm_driver/GCMDriver$3;->val$extras:Landroid/os/Bundle;

    const-string/jumbo v1, "from"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 189
    iget-object v0, p0, Lorg/chromium/components/gcm_driver/GCMDriver$3;->val$extras:Landroid/os/Bundle;

    const-string/jumbo v1, "collapse_key"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 191
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 192
    iget-object v0, p0, Lorg/chromium/components/gcm_driver/GCMDriver$3;->val$extras:Landroid/os/Bundle;

    invoke-virtual {v0}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 194
    const-string/jumbo v2, "from"

    if-eq v0, v2, :cond_0

    const-string/jumbo v2, "collapse_key"

    if-eq v0, v2, :cond_0

    const-string/jumbo v2, "com.google.ipc.invalidation.gcmmplex."

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 196
    invoke-interface {v7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 198
    iget-object v2, p0, Lorg/chromium/components/gcm_driver/GCMDriver$3;->val$extras:Landroid/os/Bundle;

    invoke-virtual {v2, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 201
    :cond_1
    # getter for: Lorg/chromium/components/gcm_driver/GCMDriver;->sInstance:Lorg/chromium/components/gcm_driver/GCMDriver;
    invoke-static {}, Lorg/chromium/components/gcm_driver/GCMDriver;->access$400()Lorg/chromium/components/gcm_driver/GCMDriver;

    move-result-object v1

    # getter for: Lorg/chromium/components/gcm_driver/GCMDriver;->sInstance:Lorg/chromium/components/gcm_driver/GCMDriver;
    invoke-static {}, Lorg/chromium/components/gcm_driver/GCMDriver;->access$400()Lorg/chromium/components/gcm_driver/GCMDriver;

    move-result-object v0

    # getter for: Lorg/chromium/components/gcm_driver/GCMDriver;->mNativeGCMDriverAndroid:J
    invoke-static {v0}, Lorg/chromium/components/gcm_driver/GCMDriver;->access$100(Lorg/chromium/components/gcm_driver/GCMDriver;)J

    move-result-wide v2

    # invokes: Lorg/chromium/components/gcm_driver/GCMDriver;->getLastAppId()Ljava/lang/String;
    invoke-static {}, Lorg/chromium/components/gcm_driver/GCMDriver;->access$500()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/String;

    invoke-interface {v7, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v7

    check-cast v7, [Ljava/lang/String;

    # invokes: Lorg/chromium/components/gcm_driver/GCMDriver;->nativeOnMessageReceived(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V
    invoke-static/range {v1 .. v7}, Lorg/chromium/components/gcm_driver/GCMDriver;->access$600(Lorg/chromium/components/gcm_driver/GCMDriver;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V

    .line 204
    return-void
.end method
