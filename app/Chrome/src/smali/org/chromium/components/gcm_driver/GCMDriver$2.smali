.class Lorg/chromium/components/gcm_driver/GCMDriver$2;
.super Landroid/os/AsyncTask;
.source "GCMDriver.java"


# instance fields
.field final synthetic this$0:Lorg/chromium/components/gcm_driver/GCMDriver;

.field final synthetic val$appId:Ljava/lang/String;


# direct methods
.method constructor <init>(Lorg/chromium/components/gcm_driver/GCMDriver;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 117
    iput-object p1, p0, Lorg/chromium/components/gcm_driver/GCMDriver$2;->this$0:Lorg/chromium/components/gcm_driver/GCMDriver;

    iput-object p2, p0, Lorg/chromium/components/gcm_driver/GCMDriver$2;->val$appId:Ljava/lang/String;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 117
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lorg/chromium/components/gcm_driver/GCMDriver$2;->doInBackground([Ljava/lang/Void;)Lorg/chromium/components/gcm_driver/GCMDriver$UnregisterResult;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Lorg/chromium/components/gcm_driver/GCMDriver$UnregisterResult;
    .locals 1

    .prologue
    .line 121
    :try_start_0
    iget-object v0, p0, Lorg/chromium/components/gcm_driver/GCMDriver$2;->this$0:Lorg/chromium/components/gcm_driver/GCMDriver;

    # getter for: Lorg/chromium/components/gcm_driver/GCMDriver;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lorg/chromium/components/gcm_driver/GCMDriver;->access$000(Lorg/chromium/components/gcm_driver/GCMDriver;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/a/c;->a(Landroid/content/Context;)V
    :try_end_0
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_0 .. :try_end_0} :catch_0

    .line 125
    iget-object v0, p0, Lorg/chromium/components/gcm_driver/GCMDriver$2;->this$0:Lorg/chromium/components/gcm_driver/GCMDriver;

    # getter for: Lorg/chromium/components/gcm_driver/GCMDriver;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lorg/chromium/components/gcm_driver/GCMDriver;->access$000(Lorg/chromium/components/gcm_driver/GCMDriver;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/a/c;->g(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 126
    sget-object v0, Lorg/chromium/components/gcm_driver/GCMDriver$UnregisterResult;->SUCCESS:Lorg/chromium/components/gcm_driver/GCMDriver$UnregisterResult;

    .line 130
    :goto_0
    return-object v0

    .line 123
    :catch_0
    move-exception v0

    sget-object v0, Lorg/chromium/components/gcm_driver/GCMDriver$UnregisterResult;->FAILED:Lorg/chromium/components/gcm_driver/GCMDriver$UnregisterResult;

    goto :goto_0

    .line 129
    :cond_0
    iget-object v0, p0, Lorg/chromium/components/gcm_driver/GCMDriver$2;->this$0:Lorg/chromium/components/gcm_driver/GCMDriver;

    # getter for: Lorg/chromium/components/gcm_driver/GCMDriver;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lorg/chromium/components/gcm_driver/GCMDriver;->access$000(Lorg/chromium/components/gcm_driver/GCMDriver;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/a/c;->c(Landroid/content/Context;)V

    .line 130
    sget-object v0, Lorg/chromium/components/gcm_driver/GCMDriver$UnregisterResult;->PENDING:Lorg/chromium/components/gcm_driver/GCMDriver$UnregisterResult;

    goto :goto_0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 117
    check-cast p1, Lorg/chromium/components/gcm_driver/GCMDriver$UnregisterResult;

    invoke-virtual {p0, p1}, Lorg/chromium/components/gcm_driver/GCMDriver$2;->onPostExecute(Lorg/chromium/components/gcm_driver/GCMDriver$UnregisterResult;)V

    return-void
.end method

.method protected onPostExecute(Lorg/chromium/components/gcm_driver/GCMDriver$UnregisterResult;)V
    .locals 5

    .prologue
    .line 135
    sget-object v0, Lorg/chromium/components/gcm_driver/GCMDriver$UnregisterResult;->PENDING:Lorg/chromium/components/gcm_driver/GCMDriver$UnregisterResult;

    if-ne p1, v0, :cond_0

    .line 140
    :goto_0
    return-void

    .line 138
    :cond_0
    iget-object v1, p0, Lorg/chromium/components/gcm_driver/GCMDriver$2;->this$0:Lorg/chromium/components/gcm_driver/GCMDriver;

    iget-object v0, p0, Lorg/chromium/components/gcm_driver/GCMDriver$2;->this$0:Lorg/chromium/components/gcm_driver/GCMDriver;

    # getter for: Lorg/chromium/components/gcm_driver/GCMDriver;->mNativeGCMDriverAndroid:J
    invoke-static {v0}, Lorg/chromium/components/gcm_driver/GCMDriver;->access$100(Lorg/chromium/components/gcm_driver/GCMDriver;)J

    move-result-wide v2

    iget-object v4, p0, Lorg/chromium/components/gcm_driver/GCMDriver$2;->val$appId:Ljava/lang/String;

    sget-object v0, Lorg/chromium/components/gcm_driver/GCMDriver$UnregisterResult;->SUCCESS:Lorg/chromium/components/gcm_driver/GCMDriver$UnregisterResult;

    if-ne p1, v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    # invokes: Lorg/chromium/components/gcm_driver/GCMDriver;->nativeOnUnregisterFinished(JLjava/lang/String;Z)V
    invoke-static {v1, v2, v3, v4, v0}, Lorg/chromium/components/gcm_driver/GCMDriver;->access$300(Lorg/chromium/components/gcm_driver/GCMDriver;JLjava/lang/String;Z)V

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method
