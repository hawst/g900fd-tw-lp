.class Lorg/chromium/media/VideoCaptureAndroid$BuggyDeviceHack;
.super Ljava/lang/Object;
.source "VideoCaptureAndroid.java"


# static fields
.field private static final CAPTURESIZE_BUGGY_DEVICE_LIST:[Lorg/chromium/media/VideoCaptureAndroid$BuggyDeviceHack$IdAndSizes;

.field private static final COLORSPACE_BUGGY_DEVICE_LIST:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 42
    new-array v0, v7, [Lorg/chromium/media/VideoCaptureAndroid$BuggyDeviceHack$IdAndSizes;

    new-instance v1, Lorg/chromium/media/VideoCaptureAndroid$BuggyDeviceHack$IdAndSizes;

    const-string/jumbo v2, "Nexus 7"

    const-string/jumbo v3, "flo"

    const/16 v4, 0x280

    const/16 v5, 0x1e0

    invoke-direct {v1, v2, v3, v4, v5}, Lorg/chromium/media/VideoCaptureAndroid$BuggyDeviceHack$IdAndSizes;-><init>(Ljava/lang/String;Ljava/lang/String;II)V

    aput-object v1, v0, v6

    sput-object v0, Lorg/chromium/media/VideoCaptureAndroid$BuggyDeviceHack;->CAPTURESIZE_BUGGY_DEVICE_LIST:[Lorg/chromium/media/VideoCaptureAndroid$BuggyDeviceHack$IdAndSizes;

    .line 46
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const-string/jumbo v1, "SAMSUNG-SGH-I747"

    aput-object v1, v0, v6

    const-string/jumbo v1, "ODROID-U2"

    aput-object v1, v0, v7

    sput-object v0, Lorg/chromium/media/VideoCaptureAndroid$BuggyDeviceHack;->COLORSPACE_BUGGY_DEVICE_LIST:[Ljava/lang/String;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    return-void
.end method

.method static applyMinDimensions(Lorg/chromium/media/VideoCapture$CaptureFormat;)V
    .locals 6

    .prologue
    .line 53
    sget-object v2, Lorg/chromium/media/VideoCaptureAndroid$BuggyDeviceHack;->CAPTURESIZE_BUGGY_DEVICE_LIST:[Lorg/chromium/media/VideoCaptureAndroid$BuggyDeviceHack$IdAndSizes;

    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_3

    aget-object v4, v2, v1

    .line 54
    iget-object v0, v4, Lorg/chromium/media/VideoCaptureAndroid$BuggyDeviceHack$IdAndSizes;->mModel:Ljava/lang/String;

    sget-object v5, Landroid/os/Build;->MODEL:Ljava/lang/String;

    invoke-virtual {v0, v5}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, v4, Lorg/chromium/media/VideoCaptureAndroid$BuggyDeviceHack$IdAndSizes;->mDevice:Ljava/lang/String;

    sget-object v5, Landroid/os/Build;->DEVICE:Ljava/lang/String;

    invoke-virtual {v0, v5}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 56
    iget v0, v4, Lorg/chromium/media/VideoCaptureAndroid$BuggyDeviceHack$IdAndSizes;->mMinWidth:I

    iget v5, p0, Lorg/chromium/media/VideoCapture$CaptureFormat;->mWidth:I

    if-le v0, v5, :cond_1

    iget v0, v4, Lorg/chromium/media/VideoCaptureAndroid$BuggyDeviceHack$IdAndSizes;->mMinWidth:I

    :goto_1
    iput v0, p0, Lorg/chromium/media/VideoCapture$CaptureFormat;->mWidth:I

    .line 58
    iget v0, v4, Lorg/chromium/media/VideoCaptureAndroid$BuggyDeviceHack$IdAndSizes;->mMinHeight:I

    iget v5, p0, Lorg/chromium/media/VideoCapture$CaptureFormat;->mHeight:I

    if-le v0, v5, :cond_2

    iget v0, v4, Lorg/chromium/media/VideoCaptureAndroid$BuggyDeviceHack$IdAndSizes;->mMinHeight:I

    :goto_2
    iput v0, p0, Lorg/chromium/media/VideoCapture$CaptureFormat;->mHeight:I

    .line 53
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 56
    :cond_1
    iget v0, p0, Lorg/chromium/media/VideoCapture$CaptureFormat;->mWidth:I

    goto :goto_1

    .line 58
    :cond_2
    iget v0, p0, Lorg/chromium/media/VideoCapture$CaptureFormat;->mHeight:I

    goto :goto_2

    .line 62
    :cond_3
    return-void
.end method

.method static getImageFormat()I
    .locals 6

    .prologue
    const/16 v0, 0x11

    .line 65
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x10

    if-ge v1, v2, :cond_1

    .line 74
    :cond_0
    :goto_0
    return v0

    .line 69
    :cond_1
    sget-object v2, Lorg/chromium/media/VideoCaptureAndroid$BuggyDeviceHack;->COLORSPACE_BUGGY_DEVICE_LIST:[Ljava/lang/String;

    array-length v3, v2

    const/4 v1, 0x0

    :goto_1
    if-ge v1, v3, :cond_2

    aget-object v4, v2, v1

    .line 70
    sget-object v5, Landroid/os/Build;->MODEL:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 69
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 74
    :cond_2
    const v0, 0x32315659

    goto :goto_0
.end method
