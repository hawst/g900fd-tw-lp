.class Lorg/chromium/media/MediaDrmBridge$MediaDrmListener;
.super Ljava/lang/Object;
.source "MediaDrmBridge.java"

# interfaces
.implements Landroid/media/MediaDrm$OnEventListener;


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field final synthetic this$0:Lorg/chromium/media/MediaDrmBridge;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 775
    const-class v0, Lorg/chromium/media/MediaDrmBridge;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/chromium/media/MediaDrmBridge$MediaDrmListener;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private constructor <init>(Lorg/chromium/media/MediaDrmBridge;)V
    .locals 0

    .prologue
    .line 775
    iput-object p1, p0, Lorg/chromium/media/MediaDrmBridge$MediaDrmListener;->this$0:Lorg/chromium/media/MediaDrmBridge;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lorg/chromium/media/MediaDrmBridge;Lorg/chromium/media/MediaDrmBridge$1;)V
    .locals 0

    .prologue
    .line 775
    invoke-direct {p0, p1}, Lorg/chromium/media/MediaDrmBridge$MediaDrmListener;-><init>(Lorg/chromium/media/MediaDrmBridge;)V

    return-void
.end method


# virtual methods
.method public onEvent(Landroid/media/MediaDrm;[BII[B)V
    .locals 4

    .prologue
    .line 779
    if-nez p2, :cond_1

    .line 780
    const-string/jumbo v0, "MediaDrmBridge"

    const-string/jumbo v1, "MediaDrmListener: Null session."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 829
    :cond_0
    :goto_0
    return-void

    .line 783
    :cond_1
    invoke-static {p2}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v2

    .line 784
    iget-object v0, p0, Lorg/chromium/media/MediaDrmBridge$MediaDrmListener;->this$0:Lorg/chromium/media/MediaDrmBridge;

    # invokes: Lorg/chromium/media/MediaDrmBridge;->sessionExists(Ljava/nio/ByteBuffer;)Z
    invoke-static {v0, v2}, Lorg/chromium/media/MediaDrmBridge;->access$1200(Lorg/chromium/media/MediaDrmBridge;Ljava/nio/ByteBuffer;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 785
    const-string/jumbo v0, "MediaDrmBridge"

    const-string/jumbo v1, "MediaDrmListener: Invalid session."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 788
    :cond_2
    iget-object v0, p0, Lorg/chromium/media/MediaDrmBridge$MediaDrmListener;->this$0:Lorg/chromium/media/MediaDrmBridge;

    # getter for: Lorg/chromium/media/MediaDrmBridge;->mSessionIds:Ljava/util/HashMap;
    invoke-static {v0}, Lorg/chromium/media/MediaDrmBridge;->access$1300(Lorg/chromium/media/MediaDrmBridge;)Ljava/util/HashMap;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 789
    if-eqz v0, :cond_3

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-nez v1, :cond_4

    .line 790
    :cond_3
    const-string/jumbo v0, "MediaDrmBridge"

    const-string/jumbo v1, "MediaDrmListener: Invalid session ID."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 793
    :cond_4
    packed-switch p3, :pswitch_data_0

    .line 826
    const-string/jumbo v0, "MediaDrmBridge"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "Invalid DRM event "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 795
    :pswitch_0
    const-string/jumbo v0, "MediaDrmBridge"

    const-string/jumbo v1, "MediaDrm.EVENT_PROVISION_REQUIRED"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 798
    :pswitch_1
    const-string/jumbo v1, "MediaDrmBridge"

    const-string/jumbo v3, "MediaDrm.EVENT_KEY_REQUIRED"

    invoke-static {v1, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 799
    iget-object v1, p0, Lorg/chromium/media/MediaDrmBridge$MediaDrmListener;->this$0:Lorg/chromium/media/MediaDrmBridge;

    # getter for: Lorg/chromium/media/MediaDrmBridge;->mProvisioningPending:Z
    invoke-static {v1}, Lorg/chromium/media/MediaDrmBridge;->access$1400(Lorg/chromium/media/MediaDrmBridge;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 802
    iget-object v1, p0, Lorg/chromium/media/MediaDrmBridge$MediaDrmListener;->this$0:Lorg/chromium/media/MediaDrmBridge;

    # getter for: Lorg/chromium/media/MediaDrmBridge;->mSessionMimeTypes:Ljava/util/HashMap;
    invoke-static {v1}, Lorg/chromium/media/MediaDrmBridge;->access$1500(Lorg/chromium/media/MediaDrmBridge;)Ljava/util/HashMap;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 803
    :try_start_0
    iget-object v3, p0, Lorg/chromium/media/MediaDrmBridge$MediaDrmListener;->this$0:Lorg/chromium/media/MediaDrmBridge;

    # invokes: Lorg/chromium/media/MediaDrmBridge;->getKeyRequest(Ljava/nio/ByteBuffer;[BLjava/lang/String;)Landroid/media/MediaDrm$KeyRequest;
    invoke-static {v3, v2, p5, v1}, Lorg/chromium/media/MediaDrmBridge;->access$1600(Lorg/chromium/media/MediaDrmBridge;Ljava/nio/ByteBuffer;[BLjava/lang/String;)Landroid/media/MediaDrm$KeyRequest;
    :try_end_0
    .catch Landroid/media/NotProvisionedException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 811
    if-eqz v1, :cond_5

    .line 812
    iget-object v2, p0, Lorg/chromium/media/MediaDrmBridge$MediaDrmListener;->this$0:Lorg/chromium/media/MediaDrmBridge;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    # invokes: Lorg/chromium/media/MediaDrmBridge;->onSessionMessage(ILandroid/media/MediaDrm$KeyRequest;)V
    invoke-static {v2, v0, v1}, Lorg/chromium/media/MediaDrmBridge;->access$1800(Lorg/chromium/media/MediaDrmBridge;ILandroid/media/MediaDrm$KeyRequest;)V

    goto/16 :goto_0

    .line 806
    :catch_0
    move-exception v0

    .line 807
    const-string/jumbo v1, "MediaDrmBridge"

    const-string/jumbo v2, "Device not provisioned"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 808
    iget-object v0, p0, Lorg/chromium/media/MediaDrmBridge$MediaDrmListener;->this$0:Lorg/chromium/media/MediaDrmBridge;

    # invokes: Lorg/chromium/media/MediaDrmBridge;->startProvisioning()V
    invoke-static {v0}, Lorg/chromium/media/MediaDrmBridge;->access$1700(Lorg/chromium/media/MediaDrmBridge;)V

    goto/16 :goto_0

    .line 814
    :cond_5
    iget-object v1, p0, Lorg/chromium/media/MediaDrmBridge$MediaDrmListener;->this$0:Lorg/chromium/media/MediaDrmBridge;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    # invokes: Lorg/chromium/media/MediaDrmBridge;->onSessionError(I)V
    invoke-static {v1, v0}, Lorg/chromium/media/MediaDrmBridge;->access$1900(Lorg/chromium/media/MediaDrmBridge;I)V

    goto/16 :goto_0

    .line 818
    :pswitch_2
    const-string/jumbo v1, "MediaDrmBridge"

    const-string/jumbo v2, "MediaDrm.EVENT_KEY_EXPIRED"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 819
    iget-object v1, p0, Lorg/chromium/media/MediaDrmBridge$MediaDrmListener;->this$0:Lorg/chromium/media/MediaDrmBridge;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    # invokes: Lorg/chromium/media/MediaDrmBridge;->onSessionError(I)V
    invoke-static {v1, v0}, Lorg/chromium/media/MediaDrmBridge;->access$1900(Lorg/chromium/media/MediaDrmBridge;I)V

    goto/16 :goto_0

    .line 822
    :pswitch_3
    const-string/jumbo v0, "MediaDrmBridge"

    const-string/jumbo v1, "MediaDrm.EVENT_VENDOR_DEFINED"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 823
    sget-boolean v0, Lorg/chromium/media/MediaDrmBridge$MediaDrmListener;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 793
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
