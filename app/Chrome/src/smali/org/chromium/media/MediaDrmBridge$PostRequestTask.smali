.class Lorg/chromium/media/MediaDrmBridge$PostRequestTask;
.super Landroid/os/AsyncTask;
.source "MediaDrmBridge.java"


# instance fields
.field private mDrmRequest:[B

.field private mResponseBody:[B

.field final synthetic this$0:Lorg/chromium/media/MediaDrmBridge;


# direct methods
.method public constructor <init>(Lorg/chromium/media/MediaDrmBridge;[B)V
    .locals 0

    .prologue
    .line 838
    iput-object p1, p0, Lorg/chromium/media/MediaDrmBridge$PostRequestTask;->this$0:Lorg/chromium/media/MediaDrmBridge;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 839
    iput-object p2, p0, Lorg/chromium/media/MediaDrmBridge$PostRequestTask;->mDrmRequest:[B

    .line 840
    return-void
.end method

.method private postRequest(Ljava/lang/String;[B)[B
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 852
    new-instance v1, Lorg/apache/http/impl/client/DefaultHttpClient;

    invoke-direct {v1}, Lorg/apache/http/impl/client/DefaultHttpClient;-><init>()V

    .line 853
    new-instance v2, Lorg/apache/http/client/methods/HttpPost;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "&signedRequest="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    new-instance v4, Ljava/lang/String;

    invoke-direct {v4, p2}, Ljava/lang/String;-><init>([B)V

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lorg/apache/http/client/methods/HttpPost;-><init>(Ljava/lang/String;)V

    .line 855
    const-string/jumbo v3, "PostRequestTask"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "PostRequest:"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Lorg/apache/http/client/methods/HttpPost;->getRequestLine()Lorg/apache/http/RequestLine;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 858
    :try_start_0
    const-string/jumbo v3, "Accept"

    const-string/jumbo v4, "*/*"

    invoke-virtual {v2, v3, v4}, Lorg/apache/http/client/methods/HttpPost;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 859
    const-string/jumbo v3, "User-Agent"

    const-string/jumbo v4, "Widevine CDM v1.0"

    invoke-virtual {v2, v3, v4}, Lorg/apache/http/client/methods/HttpPost;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 860
    const-string/jumbo v3, "Content-Type"

    const-string/jumbo v4, "application/json"

    invoke-virtual {v2, v3, v4}, Lorg/apache/http/client/methods/HttpPost;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 863
    invoke-interface {v1, v2}, Lorg/apache/http/client/HttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v1

    .line 866
    invoke-interface {v1}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v2

    invoke-interface {v2}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v2

    .line 867
    const/16 v3, 0xc8

    if-ne v2, v3, :cond_0

    .line 868
    invoke-interface {v1}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v1

    invoke-static {v1}, Lorg/apache/http/util/EntityUtils;->toByteArray(Lorg/apache/http/HttpEntity;)[B

    move-result-object v0

    .line 879
    :goto_0
    return-object v0

    .line 870
    :cond_0
    const-string/jumbo v1, "PostRequestTask"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "Server returned HTTP error code "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Lorg/apache/http/client/ClientProtocolException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 874
    :catch_0
    move-exception v1

    .line 875
    invoke-virtual {v1}, Lorg/apache/http/client/ClientProtocolException;->printStackTrace()V

    goto :goto_0

    .line 876
    :catch_1
    move-exception v1

    .line 877
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 832
    check-cast p1, [Ljava/lang/String;

    invoke-virtual {p0, p1}, Lorg/chromium/media/MediaDrmBridge$PostRequestTask;->doInBackground([Ljava/lang/String;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/String;)Ljava/lang/Void;
    .locals 3

    .prologue
    .line 844
    const/4 v0, 0x0

    aget-object v0, p1, v0

    iget-object v1, p0, Lorg/chromium/media/MediaDrmBridge$PostRequestTask;->mDrmRequest:[B

    invoke-direct {p0, v0, v1}, Lorg/chromium/media/MediaDrmBridge$PostRequestTask;->postRequest(Ljava/lang/String;[B)[B

    move-result-object v0

    iput-object v0, p0, Lorg/chromium/media/MediaDrmBridge$PostRequestTask;->mResponseBody:[B

    .line 845
    iget-object v0, p0, Lorg/chromium/media/MediaDrmBridge$PostRequestTask;->mResponseBody:[B

    if-eqz v0, :cond_0

    .line 846
    const-string/jumbo v0, "PostRequestTask"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "response length="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lorg/chromium/media/MediaDrmBridge$PostRequestTask;->mResponseBody:[B

    array-length v2, v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 848
    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 832
    check-cast p1, Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lorg/chromium/media/MediaDrmBridge$PostRequestTask;->onPostExecute(Ljava/lang/Void;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/Void;)V
    .locals 2

    .prologue
    .line 884
    iget-object v0, p0, Lorg/chromium/media/MediaDrmBridge$PostRequestTask;->this$0:Lorg/chromium/media/MediaDrmBridge;

    iget-object v1, p0, Lorg/chromium/media/MediaDrmBridge$PostRequestTask;->mResponseBody:[B

    # invokes: Lorg/chromium/media/MediaDrmBridge;->onProvisionResponse([B)V
    invoke-static {v0, v1}, Lorg/chromium/media/MediaDrmBridge;->access$2000(Lorg/chromium/media/MediaDrmBridge;[B)V

    .line 885
    return-void
.end method
