.class public abstract Lorg/chromium/media/VideoCapture;
.super Ljava/lang/Object;
.source "VideoCapture.java"

# interfaces
.implements Landroid/hardware/Camera$PreviewCallback;


# annotations
.annotation runtime Lorg/chromium/base/JNINamespace;
.end annotation


# static fields
.field protected static final GL_TEXTURE_EXTERNAL_OES:I = 0x8d65


# instance fields
.field protected mCamera:Landroid/hardware/Camera;

.field protected mCameraFacing:I

.field protected mCameraOrientation:I

.field protected mCaptureFormat:Lorg/chromium/media/VideoCapture$CaptureFormat;

.field protected mContext:Landroid/content/Context;

.field protected mDeviceOrientation:I

.field protected mGlTextures:[I

.field protected mId:I

.field protected mIsRunning:Z

.field protected mNativeVideoCaptureDeviceAndroid:J

.field protected mPreviewBufferLock:Ljava/util/concurrent/locks/ReentrantLock;

.field protected mSurfaceTexture:Landroid/graphics/SurfaceTexture;


# direct methods
.method constructor <init>(Landroid/content/Context;IJ)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 82
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 61
    iput-object v1, p0, Lorg/chromium/media/VideoCapture;->mCaptureFormat:Lorg/chromium/media/VideoCapture$CaptureFormat;

    .line 63
    new-instance v0, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    iput-object v0, p0, Lorg/chromium/media/VideoCapture;->mPreviewBufferLock:Ljava/util/concurrent/locks/ReentrantLock;

    .line 64
    iput-object v1, p0, Lorg/chromium/media/VideoCapture;->mContext:Landroid/content/Context;

    .line 66
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/chromium/media/VideoCapture;->mIsRunning:Z

    .line 71
    iput-object v1, p0, Lorg/chromium/media/VideoCapture;->mGlTextures:[I

    .line 72
    iput-object v1, p0, Lorg/chromium/media/VideoCapture;->mSurfaceTexture:Landroid/graphics/SurfaceTexture;

    .line 83
    iput-object p1, p0, Lorg/chromium/media/VideoCapture;->mContext:Landroid/content/Context;

    .line 84
    iput p2, p0, Lorg/chromium/media/VideoCapture;->mId:I

    .line 85
    iput-wide p3, p0, Lorg/chromium/media/VideoCapture;->mNativeVideoCaptureDeviceAndroid:J

    .line 86
    return-void
.end method

.method private getCameraInfo(I)Landroid/hardware/Camera$CameraInfo;
    .locals 4

    .prologue
    .line 372
    new-instance v0, Landroid/hardware/Camera$CameraInfo;

    invoke-direct {v0}, Landroid/hardware/Camera$CameraInfo;-><init>()V

    .line 374
    :try_start_0
    invoke-static {p1, v0}, Landroid/hardware/Camera;->getCameraInfo(ILandroid/hardware/Camera$CameraInfo;)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 379
    :goto_0
    return-object v0

    .line 375
    :catch_0
    move-exception v0

    .line 376
    const-string/jumbo v1, "VideoCapture"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "getCameraInfo: android.hardware.Camera.getCameraInfo: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 377
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected static getCameraParameters(Landroid/hardware/Camera;)Landroid/hardware/Camera$Parameters;
    .locals 4

    .prologue
    .line 362
    :try_start_0
    invoke-virtual {p0}, Landroid/hardware/Camera;->getParameters()Landroid/hardware/Camera$Parameters;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 368
    :goto_0
    return-object v0

    .line 363
    :catch_0
    move-exception v0

    .line 364
    const-string/jumbo v1, "VideoCapture"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "getCameraParameters: android.hardware.Camera.getParameters: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 365
    invoke-virtual {p0}, Landroid/hardware/Camera;->release()V

    .line 366
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method allocate(III)Z
    .locals 11

    .prologue
    .line 90
    const-string/jumbo v0, "VideoCapture"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "allocate: requested ("

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "x"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ")@"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "fps"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 93
    :try_start_0
    iget v0, p0, Lorg/chromium/media/VideoCapture;->mId:I

    invoke-static {v0}, Landroid/hardware/Camera;->open(I)Landroid/hardware/Camera;

    move-result-object v0

    iput-object v0, p0, Lorg/chromium/media/VideoCapture;->mCamera:Landroid/hardware/Camera;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 99
    iget v0, p0, Lorg/chromium/media/VideoCapture;->mId:I

    invoke-direct {p0, v0}, Lorg/chromium/media/VideoCapture;->getCameraInfo(I)Landroid/hardware/Camera$CameraInfo;

    move-result-object v0

    .line 100
    if-nez v0, :cond_0

    .line 101
    iget-object v0, p0, Lorg/chromium/media/VideoCapture;->mCamera:Landroid/hardware/Camera;

    invoke-virtual {v0}, Landroid/hardware/Camera;->release()V

    .line 102
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/chromium/media/VideoCapture;->mCamera:Landroid/hardware/Camera;

    .line 103
    const/4 v0, 0x0

    .line 213
    :goto_0
    return v0

    .line 94
    :catch_0
    move-exception v0

    .line 95
    const-string/jumbo v1, "VideoCapture"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "allocate: Camera.open: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 96
    const/4 v0, 0x0

    goto :goto_0

    .line 106
    :cond_0
    iget v1, v0, Landroid/hardware/Camera$CameraInfo;->orientation:I

    iput v1, p0, Lorg/chromium/media/VideoCapture;->mCameraOrientation:I

    .line 107
    iget v0, v0, Landroid/hardware/Camera$CameraInfo;->facing:I

    iput v0, p0, Lorg/chromium/media/VideoCapture;->mCameraFacing:I

    .line 108
    invoke-virtual {p0}, Lorg/chromium/media/VideoCapture;->getDeviceOrientation()I

    move-result v0

    iput v0, p0, Lorg/chromium/media/VideoCapture;->mDeviceOrientation:I

    .line 109
    const-string/jumbo v0, "VideoCapture"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "allocate: orientation dev="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lorg/chromium/media/VideoCapture;->mDeviceOrientation:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ", cam="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lorg/chromium/media/VideoCapture;->mCameraOrientation:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ", facing="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lorg/chromium/media/VideoCapture;->mCameraFacing:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 112
    iget-object v0, p0, Lorg/chromium/media/VideoCapture;->mCamera:Landroid/hardware/Camera;

    invoke-static {v0}, Lorg/chromium/media/VideoCapture;->getCameraParameters(Landroid/hardware/Camera;)Landroid/hardware/Camera$Parameters;

    move-result-object v6

    .line 113
    if-nez v6, :cond_1

    .line 114
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/chromium/media/VideoCapture;->mCamera:Landroid/hardware/Camera;

    .line 115
    const/4 v0, 0x0

    goto :goto_0

    .line 120
    :cond_1
    invoke-virtual {v6}, Landroid/hardware/Camera$Parameters;->getSupportedPreviewFpsRange()Ljava/util/List;

    move-result-object v1

    .line 121
    if-eqz v1, :cond_2

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_3

    .line 122
    :cond_2
    const-string/jumbo v0, "VideoCapture"

    const-string/jumbo v1, "allocate: no fps range found"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 123
    const/4 v0, 0x0

    goto :goto_0

    .line 125
    :cond_3
    mul-int/lit16 v3, p3, 0x3e8

    .line 127
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [I

    .line 128
    const/4 v2, 0x0

    aget v2, v0, v2

    add-int/lit16 v2, v2, 0x3e7

    div-int/lit16 v2, v2, 0x3e8

    .line 129
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_4
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_9

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [I

    .line 130
    const/4 v5, 0x0

    aget v5, v1, v5

    if-gt v5, v3, :cond_4

    const/4 v5, 0x1

    aget v5, v1, v5

    if-gt v3, v5, :cond_4

    .line 137
    :goto_1
    const-string/jumbo v0, "VideoCapture"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "allocate: fps set to "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 140
    invoke-virtual {v6}, Landroid/hardware/Camera$Parameters;->getSupportedPreviewSizes()Ljava/util/List;

    move-result-object v2

    .line 142
    const v0, 0x7fffffff

    .line 145
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    move v2, v0

    move v3, p2

    move v4, p1

    :goto_2
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/Camera$Size;

    .line 146
    iget v5, v0, Landroid/hardware/Camera$Size;->width:I

    sub-int/2addr v5, p1

    invoke-static {v5}, Ljava/lang/Math;->abs(I)I

    move-result v5

    iget v8, v0, Landroid/hardware/Camera$Size;->height:I

    sub-int/2addr v8, p2

    invoke-static {v8}, Ljava/lang/Math;->abs(I)I

    move-result v8

    add-int/2addr v5, v8

    .line 148
    const-string/jumbo v8, "VideoCapture"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string/jumbo v10, "allocate: supported ("

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v10, v0, Landroid/hardware/Camera$Size;->width:I

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string/jumbo v10, ", "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget v10, v0, Landroid/hardware/Camera$Size;->height:I

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string/jumbo v10, "), diff="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 154
    if-ge v5, v2, :cond_8

    iget v8, v0, Landroid/hardware/Camera$Size;->width:I

    rem-int/lit8 v8, v8, 0x20

    if-nez v8, :cond_8

    .line 156
    iget v4, v0, Landroid/hardware/Camera$Size;->width:I

    .line 157
    iget v3, v0, Landroid/hardware/Camera$Size;->height:I

    move v0, v5

    move v2, v3

    move v3, v4

    :goto_3
    move v4, v3

    move v3, v2

    move v2, v0

    .line 159
    goto :goto_2

    .line 160
    :cond_5
    const v0, 0x7fffffff

    if-ne v2, v0, :cond_6

    .line 161
    const-string/jumbo v0, "VideoCapture"

    const-string/jumbo v1, "allocate: can not find a multiple-of-32 resolution"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 162
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 164
    :cond_6
    const-string/jumbo v0, "VideoCapture"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "allocate: matched ("

    invoke-direct {v2, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v5, "x"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v5, ")"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 166
    invoke-virtual {v6}, Landroid/hardware/Camera$Parameters;->isVideoStabilizationSupported()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 167
    const-string/jumbo v0, "VideoCapture"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "Image stabilization supported, currently: "

    invoke-direct {v2, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6}, Landroid/hardware/Camera$Parameters;->getVideoStabilization()Z

    move-result v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v5, ", setting it."

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 169
    const/4 v0, 0x1

    invoke-virtual {v6, v0}, Landroid/hardware/Camera$Parameters;->setVideoStabilization(Z)V

    .line 174
    :goto_4
    invoke-virtual {p0, v4, v3, p3, v6}, Lorg/chromium/media/VideoCapture;->setCaptureParameters(IIILandroid/hardware/Camera$Parameters;)V

    .line 175
    iget-object v0, p0, Lorg/chromium/media/VideoCapture;->mCaptureFormat:Lorg/chromium/media/VideoCapture$CaptureFormat;

    iget v0, v0, Lorg/chromium/media/VideoCapture$CaptureFormat;->mWidth:I

    iget-object v2, p0, Lorg/chromium/media/VideoCapture;->mCaptureFormat:Lorg/chromium/media/VideoCapture$CaptureFormat;

    iget v2, v2, Lorg/chromium/media/VideoCapture$CaptureFormat;->mHeight:I

    invoke-virtual {v6, v0, v2}, Landroid/hardware/Camera$Parameters;->setPreviewSize(II)V

    .line 177
    const/4 v0, 0x0

    aget v0, v1, v0

    const/4 v2, 0x1

    aget v1, v1, v2

    invoke-virtual {v6, v0, v1}, Landroid/hardware/Camera$Parameters;->setPreviewFpsRange(II)V

    .line 178
    iget-object v0, p0, Lorg/chromium/media/VideoCapture;->mCaptureFormat:Lorg/chromium/media/VideoCapture$CaptureFormat;

    iget v0, v0, Lorg/chromium/media/VideoCapture$CaptureFormat;->mPixelFormat:I

    invoke-virtual {v6, v0}, Landroid/hardware/Camera$Parameters;->setPreviewFormat(I)V

    .line 180
    :try_start_1
    iget-object v0, p0, Lorg/chromium/media/VideoCapture;->mCamera:Landroid/hardware/Camera;

    invoke-virtual {v0, v6}, Landroid/hardware/Camera;->setParameters(Landroid/hardware/Camera$Parameters;)V
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_1

    .line 188
    const/4 v0, 0x1

    new-array v0, v0, [I

    iput-object v0, p0, Lorg/chromium/media/VideoCapture;->mGlTextures:[I

    .line 190
    const/4 v0, 0x1

    iget-object v1, p0, Lorg/chromium/media/VideoCapture;->mGlTextures:[I

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/opengl/GLES20;->glGenTextures(I[II)V

    .line 191
    const v0, 0x8d65

    iget-object v1, p0, Lorg/chromium/media/VideoCapture;->mGlTextures:[I

    const/4 v2, 0x0

    aget v1, v1, v2

    invoke-static {v0, v1}, Landroid/opengl/GLES20;->glBindTexture(II)V

    .line 193
    const v0, 0x8d65

    const/16 v1, 0x2801

    const v2, 0x46180400    # 9729.0f

    invoke-static {v0, v1, v2}, Landroid/opengl/GLES20;->glTexParameterf(IIF)V

    .line 195
    const v0, 0x8d65

    const/16 v1, 0x2800

    const v2, 0x46180400    # 9729.0f

    invoke-static {v0, v1, v2}, Landroid/opengl/GLES20;->glTexParameterf(IIF)V

    .line 198
    const v0, 0x8d65

    const/16 v1, 0x2802

    const v2, 0x812f

    invoke-static {v0, v1, v2}, Landroid/opengl/GLES20;->glTexParameteri(III)V

    .line 200
    const v0, 0x8d65

    const/16 v1, 0x2803

    const v2, 0x812f

    invoke-static {v0, v1, v2}, Landroid/opengl/GLES20;->glTexParameteri(III)V

    .line 203
    new-instance v0, Landroid/graphics/SurfaceTexture;

    iget-object v1, p0, Lorg/chromium/media/VideoCapture;->mGlTextures:[I

    const/4 v2, 0x0

    aget v1, v1, v2

    invoke-direct {v0, v1}, Landroid/graphics/SurfaceTexture;-><init>(I)V

    iput-object v0, p0, Lorg/chromium/media/VideoCapture;->mSurfaceTexture:Landroid/graphics/SurfaceTexture;

    .line 204
    iget-object v0, p0, Lorg/chromium/media/VideoCapture;->mSurfaceTexture:Landroid/graphics/SurfaceTexture;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/graphics/SurfaceTexture;->setOnFrameAvailableListener(Landroid/graphics/SurfaceTexture$OnFrameAvailableListener;)V

    .line 206
    :try_start_2
    iget-object v0, p0, Lorg/chromium/media/VideoCapture;->mCamera:Landroid/hardware/Camera;

    iget-object v1, p0, Lorg/chromium/media/VideoCapture;->mSurfaceTexture:Landroid/graphics/SurfaceTexture;

    invoke-virtual {v0, v1}, Landroid/hardware/Camera;->setPreviewTexture(Landroid/graphics/SurfaceTexture;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    .line 212
    invoke-virtual {p0}, Lorg/chromium/media/VideoCapture;->allocateBuffers()V

    .line 213
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 171
    :cond_7
    const-string/jumbo v0, "VideoCapture"

    const-string/jumbo v2, "Image stabilization not supported."

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_4

    .line 181
    :catch_1
    move-exception v0

    .line 182
    const-string/jumbo v1, "VideoCapture"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "setParameters: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 183
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 207
    :catch_2
    move-exception v0

    .line 208
    const-string/jumbo v1, "VideoCapture"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "allocate: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 209
    const/4 v0, 0x0

    goto/16 :goto_0

    :cond_8
    move v0, v2

    move v2, v3

    move v3, v4

    goto/16 :goto_3

    :cond_9
    move p3, v2

    move-object v1, v0

    goto/16 :goto_1
.end method

.method abstract allocateBuffers()V
.end method

.method public deallocate()V
    .locals 4

    .prologue
    .line 266
    iget-object v0, p0, Lorg/chromium/media/VideoCapture;->mCamera:Landroid/hardware/Camera;

    if-nez v0, :cond_0

    .line 280
    :goto_0
    return-void

    .line 269
    :cond_0
    invoke-virtual {p0}, Lorg/chromium/media/VideoCapture;->stopCapture()I

    .line 271
    :try_start_0
    iget-object v0, p0, Lorg/chromium/media/VideoCapture;->mCamera:Landroid/hardware/Camera;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/hardware/Camera;->setPreviewTexture(Landroid/graphics/SurfaceTexture;)V

    .line 272
    iget-object v0, p0, Lorg/chromium/media/VideoCapture;->mGlTextures:[I

    if-eqz v0, :cond_1

    .line 273
    const/4 v0, 0x1

    iget-object v1, p0, Lorg/chromium/media/VideoCapture;->mGlTextures:[I

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/opengl/GLES20;->glDeleteTextures(I[II)V

    .line 274
    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/chromium/media/VideoCapture;->mCaptureFormat:Lorg/chromium/media/VideoCapture$CaptureFormat;

    .line 275
    iget-object v0, p0, Lorg/chromium/media/VideoCapture;->mCamera:Landroid/hardware/Camera;

    invoke-virtual {v0}, Landroid/hardware/Camera;->release()V

    .line 276
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/chromium/media/VideoCapture;->mCamera:Landroid/hardware/Camera;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 277
    :catch_0
    move-exception v0

    .line 278
    const-string/jumbo v1, "VideoCapture"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "deallocate: failed to deallocate camera, "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public getColorspace()I
    .locals 1

    .prologue
    .line 316
    iget-object v0, p0, Lorg/chromium/media/VideoCapture;->mCaptureFormat:Lorg/chromium/media/VideoCapture$CaptureFormat;

    iget v0, v0, Lorg/chromium/media/VideoCapture$CaptureFormat;->mPixelFormat:I

    sparse-switch v0, :sswitch_data_0

    .line 323
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 318
    :sswitch_0
    const v0, 0x32315659

    goto :goto_0

    .line 320
    :sswitch_1
    const/16 v0, 0x11

    goto :goto_0

    .line 316
    :sswitch_data_0
    .sparse-switch
        0x11 -> :sswitch_1
        0x32315659 -> :sswitch_0
    .end sparse-switch
.end method

.method protected getDeviceOrientation()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 328
    .line 329
    iget-object v0, p0, Lorg/chromium/media/VideoCapture;->mContext:Landroid/content/Context;

    if-eqz v0, :cond_0

    .line 330
    iget-object v0, p0, Lorg/chromium/media/VideoCapture;->mContext:Landroid/content/Context;

    const-string/jumbo v2, "window"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    .line 332
    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Display;->getRotation()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    move v0, v1

    .line 348
    :goto_0
    return v0

    .line 334
    :pswitch_0
    const/16 v0, 0x5a

    .line 335
    goto :goto_0

    .line 337
    :pswitch_1
    const/16 v0, 0xb4

    .line 338
    goto :goto_0

    .line 340
    :pswitch_2
    const/16 v0, 0x10e

    .line 341
    goto :goto_0

    :cond_0
    move v0, v1

    goto :goto_0

    .line 332
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public native nativeOnFrameAvailable(J[BII)V
.end method

.method public queryFrameRate()I
    .locals 1

    .prologue
    .line 311
    iget-object v0, p0, Lorg/chromium/media/VideoCapture;->mCaptureFormat:Lorg/chromium/media/VideoCapture$CaptureFormat;

    iget v0, v0, Lorg/chromium/media/VideoCapture$CaptureFormat;->mFramerate:I

    return v0
.end method

.method public queryHeight()I
    .locals 1

    .prologue
    .line 306
    iget-object v0, p0, Lorg/chromium/media/VideoCapture;->mCaptureFormat:Lorg/chromium/media/VideoCapture$CaptureFormat;

    iget v0, v0, Lorg/chromium/media/VideoCapture$CaptureFormat;->mHeight:I

    return v0
.end method

.method public queryWidth()I
    .locals 1

    .prologue
    .line 301
    iget-object v0, p0, Lorg/chromium/media/VideoCapture;->mCaptureFormat:Lorg/chromium/media/VideoCapture$CaptureFormat;

    iget v0, v0, Lorg/chromium/media/VideoCapture$CaptureFormat;->mWidth:I

    return v0
.end method

.method abstract setCaptureParameters(IIILandroid/hardware/Camera$Parameters;)V
.end method

.method abstract setPreviewCallback(Landroid/hardware/Camera$PreviewCallback;)V
.end method

.method public startCapture()I
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/4 v0, -0x1

    .line 218
    iget-object v2, p0, Lorg/chromium/media/VideoCapture;->mCamera:Landroid/hardware/Camera;

    if-nez v2, :cond_0

    .line 219
    const-string/jumbo v1, "VideoCapture"

    const-string/jumbo v2, "startCapture: camera is null"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 239
    :goto_0
    return v0

    .line 223
    :cond_0
    iget-object v2, p0, Lorg/chromium/media/VideoCapture;->mPreviewBufferLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v2}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 225
    :try_start_0
    iget-boolean v2, p0, Lorg/chromium/media/VideoCapture;->mIsRunning:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v2, :cond_1

    .line 226
    iget-object v0, p0, Lorg/chromium/media/VideoCapture;->mPreviewBufferLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    move v0, v1

    goto :goto_0

    .line 228
    :cond_1
    const/4 v2, 0x1

    :try_start_1
    iput-boolean v2, p0, Lorg/chromium/media/VideoCapture;->mIsRunning:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 230
    iget-object v2, p0, Lorg/chromium/media/VideoCapture;->mPreviewBufferLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v2}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 232
    invoke-virtual {p0, p0}, Lorg/chromium/media/VideoCapture;->setPreviewCallback(Landroid/hardware/Camera$PreviewCallback;)V

    .line 234
    :try_start_2
    iget-object v2, p0, Lorg/chromium/media/VideoCapture;->mCamera:Landroid/hardware/Camera;

    invoke-virtual {v2}, Landroid/hardware/Camera;->startPreview()V
    :try_end_2
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_0

    move v0, v1

    .line 239
    goto :goto_0

    .line 230
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lorg/chromium/media/VideoCapture;->mPreviewBufferLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0

    .line 235
    :catch_0
    move-exception v1

    .line 236
    const-string/jumbo v2, "VideoCapture"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "startCapture: Camera.startPreview: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public stopCapture()I
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 244
    iget-object v0, p0, Lorg/chromium/media/VideoCapture;->mCamera:Landroid/hardware/Camera;

    if-nez v0, :cond_0

    .line 245
    const-string/jumbo v0, "VideoCapture"

    const-string/jumbo v1, "stopCapture: camera is null"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 261
    :goto_0
    return v2

    .line 249
    :cond_0
    iget-object v0, p0, Lorg/chromium/media/VideoCapture;->mPreviewBufferLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 251
    :try_start_0
    iget-boolean v0, p0, Lorg/chromium/media/VideoCapture;->mIsRunning:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_1

    .line 252
    iget-object v0, p0, Lorg/chromium/media/VideoCapture;->mPreviewBufferLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    goto :goto_0

    .line 254
    :cond_1
    const/4 v0, 0x0

    :try_start_1
    iput-boolean v0, p0, Lorg/chromium/media/VideoCapture;->mIsRunning:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 256
    iget-object v0, p0, Lorg/chromium/media/VideoCapture;->mPreviewBufferLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 259
    iget-object v0, p0, Lorg/chromium/media/VideoCapture;->mCamera:Landroid/hardware/Camera;

    invoke-virtual {v0}, Landroid/hardware/Camera;->stopPreview()V

    .line 260
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lorg/chromium/media/VideoCapture;->setPreviewCallback(Landroid/hardware/Camera$PreviewCallback;)V

    goto :goto_0

    .line 256
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lorg/chromium/media/VideoCapture;->mPreviewBufferLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0
.end method
