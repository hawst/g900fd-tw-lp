.class Lorg/chromium/media/MediaCodecBridge$CodecInfo;
.super Ljava/lang/Object;
.source "MediaCodecBridge.java"


# instance fields
.field private final mCodecName:Ljava/lang/String;

.field private final mCodecType:Ljava/lang/String;

.field private final mDirection:I


# direct methods
.method private constructor <init>(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 100
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 101
    iput-object p1, p0, Lorg/chromium/media/MediaCodecBridge$CodecInfo;->mCodecType:Ljava/lang/String;

    .line 102
    iput-object p2, p0, Lorg/chromium/media/MediaCodecBridge$CodecInfo;->mCodecName:Ljava/lang/String;

    .line 103
    iput p3, p0, Lorg/chromium/media/MediaCodecBridge$CodecInfo;->mDirection:I

    .line 104
    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;Ljava/lang/String;ILorg/chromium/media/MediaCodecBridge$1;)V
    .locals 0

    .prologue
    .line 94
    invoke-direct {p0, p1, p2, p3}, Lorg/chromium/media/MediaCodecBridge$CodecInfo;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    return-void
.end method

.method private codecName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 110
    iget-object v0, p0, Lorg/chromium/media/MediaCodecBridge$CodecInfo;->mCodecName:Ljava/lang/String;

    return-object v0
.end method

.method private codecType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 107
    iget-object v0, p0, Lorg/chromium/media/MediaCodecBridge$CodecInfo;->mCodecType:Ljava/lang/String;

    return-object v0
.end method

.method private direction()I
    .locals 1

    .prologue
    .line 113
    iget v0, p0, Lorg/chromium/media/MediaCodecBridge$CodecInfo;->mDirection:I

    return v0
.end method
