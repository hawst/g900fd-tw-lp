.class Lorg/chromium/media/WebAudioMediaCodecBridge;
.super Ljava/lang/Object;
.source "WebAudioMediaCodecBridge.java"


# annotations
.annotation runtime Lorg/chromium/base/JNINamespace;
.end annotation


# static fields
.field static final LOG_TAG:Ljava/lang/String; = "WebAudioMediaCodec"

.field static final TIMEOUT_MICROSECONDS:J = 0x1f4L


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static createTempFile(Landroid/content/Context;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 29
    invoke-virtual {p0}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v0

    .line 30
    const-string/jumbo v1, "webaudio"

    const-string/jumbo v2, ".dat"

    invoke-static {v1, v2, v0}, Ljava/io/File;->createTempFile(Ljava/lang/String;Ljava/lang/String;Ljava/io/File;)Ljava/io/File;

    move-result-object v0

    .line 31
    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static decodeAudioFile(Landroid/content/Context;JIJ)Z
    .locals 26

    .prologue
    .line 41
    const-wide/16 v2, 0x0

    cmp-long v2, p4, v2

    if-ltz v2, :cond_0

    const-wide/32 v2, 0x7fffffff

    cmp-long v2, p4, v2

    if-lez v2, :cond_1

    .line 42
    :cond_0
    const/4 v2, 0x0

    .line 196
    :goto_0
    return v2

    .line 44
    :cond_1
    new-instance v2, Landroid/media/MediaExtractor;

    invoke-direct {v2}, Landroid/media/MediaExtractor;-><init>()V

    .line 47
    invoke-static/range {p3 .. p3}, Landroid/os/ParcelFileDescriptor;->adoptFd(I)Landroid/os/ParcelFileDescriptor;

    move-result-object v20

    .line 49
    :try_start_0
    invoke-virtual/range {v20 .. v20}, Landroid/os/ParcelFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    move-result-object v3

    const-wide/16 v4, 0x0

    move-wide/from16 v6, p4

    invoke-virtual/range {v2 .. v7}, Landroid/media/MediaExtractor;->setDataSource(Ljava/io/FileDescriptor;JJ)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 56
    invoke-virtual {v2}, Landroid/media/MediaExtractor;->getTrackCount()I

    move-result v3

    if-gtz v3, :cond_2

    .line 57
    invoke-virtual/range {v20 .. v20}, Landroid/os/ParcelFileDescriptor;->detachFd()I

    .line 58
    const/4 v2, 0x0

    goto :goto_0

    .line 50
    :catch_0
    move-exception v2

    .line 51
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    .line 52
    invoke-virtual/range {v20 .. v20}, Landroid/os/ParcelFileDescriptor;->detachFd()I

    .line 53
    const/4 v2, 0x0

    goto :goto_0

    .line 61
    :cond_2
    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/media/MediaExtractor;->getTrackFormat(I)Landroid/media/MediaFormat;

    move-result-object v3

    .line 64
    const-string/jumbo v4, "channel-count"

    invoke-virtual {v3, v4}, Landroid/media/MediaFormat;->getInteger(Ljava/lang/String;)I

    move-result v19

    .line 71
    const-string/jumbo v4, "sample-rate"

    invoke-virtual {v3, v4}, Landroid/media/MediaFormat;->getInteger(Ljava/lang/String;)I

    move-result v15

    .line 72
    const-string/jumbo v4, "mime"

    invoke-virtual {v3, v4}, Landroid/media/MediaFormat;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v21

    .line 74
    const-wide/16 v4, 0x0

    .line 75
    const-string/jumbo v6, "durationUs"

    invoke-virtual {v3, v6}, Landroid/media/MediaFormat;->containsKey(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 77
    :try_start_1
    const-string/jumbo v6, "durationUs"

    invoke-virtual {v3, v6}, Landroid/media/MediaFormat;->getLong(Ljava/lang/String;)J
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-result-wide v4

    .line 87
    :cond_3
    :goto_1
    const-wide/32 v6, 0x7fffffff

    cmp-long v6, v4, v6

    if-lez v6, :cond_e

    .line 88
    const-wide/16 v4, 0x0

    move-wide v12, v4

    .line 91
    :goto_2
    const-string/jumbo v4, "WebAudioMediaCodec"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string/jumbo v6, "Initial: Tracks: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Landroid/media/MediaExtractor;->getTrackCount()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, " Format: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 97
    :try_start_2
    invoke-static/range {v21 .. v21}, Landroid/media/MediaCodec;->createDecoderByType(Ljava/lang/String;)Landroid/media/MediaCodec;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    move-result-object v4

    .line 104
    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual {v4, v3, v5, v6, v7}, Landroid/media/MediaCodec;->configure(Landroid/media/MediaFormat;Landroid/view/Surface;Landroid/media/MediaCrypto;I)V

    .line 105
    invoke-virtual {v4}, Landroid/media/MediaCodec;->start()V

    .line 107
    invoke-virtual {v4}, Landroid/media/MediaCodec;->getInputBuffers()[Ljava/nio/ByteBuffer;

    move-result-object v22

    .line 108
    invoke-virtual {v4}, Landroid/media/MediaCodec;->getOutputBuffers()[Ljava/nio/ByteBuffer;

    move-result-object v5

    .line 111
    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/media/MediaExtractor;->selectTrack(I)V

    .line 113
    const/4 v3, 0x0

    .line 114
    const/4 v14, 0x0

    .line 115
    const/4 v11, 0x0

    move-object/from16 v18, v5

    move/from16 v16, v19

    .line 118
    :goto_3
    if-nez v14, :cond_a

    .line 119
    if-nez v3, :cond_4

    .line 121
    const-wide/16 v6, 0x1f4

    invoke-virtual {v4, v6, v7}, Landroid/media/MediaCodec;->dequeueInputBuffer(J)I

    move-result v5

    .line 123
    if-ltz v5, :cond_4

    .line 124
    aget-object v6, v22, v5

    .line 125
    const/4 v7, 0x0

    invoke-virtual {v2, v6, v7}, Landroid/media/MediaExtractor;->readSampleData(Ljava/nio/ByteBuffer;I)I

    move-result v7

    .line 126
    const-wide/16 v8, 0x0

    .line 128
    if-gez v7, :cond_6

    .line 129
    const/4 v3, 0x1

    .line 130
    const/4 v7, 0x0

    .line 135
    :goto_4
    const/4 v6, 0x0

    if-eqz v3, :cond_7

    const/4 v10, 0x4

    :goto_5
    invoke-virtual/range {v4 .. v10}, Landroid/media/MediaCodec;->queueInputBuffer(IIIJI)V

    .line 141
    if-nez v3, :cond_4

    .line 142
    invoke-virtual {v2}, Landroid/media/MediaExtractor;->advance()Z

    .line 148
    :cond_4
    new-instance v23, Landroid/media/MediaCodec$BufferInfo;

    invoke-direct/range {v23 .. v23}, Landroid/media/MediaCodec$BufferInfo;-><init>()V

    .line 149
    const-wide/16 v6, 0x1f4

    move-object/from16 v0, v23

    invoke-virtual {v4, v0, v6, v7}, Landroid/media/MediaCodec;->dequeueOutputBuffer(Landroid/media/MediaCodec$BufferInfo;J)I

    move-result v24

    .line 151
    if-ltz v24, :cond_8

    .line 152
    aget-object v25, v18, v24

    .line 154
    if-nez v11, :cond_d

    .line 159
    const-string/jumbo v5, "WebAudioMediaCodec"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string/jumbo v7, "Final:  Rate: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, " Channels: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move/from16 v0, v19

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, " Mime: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, v21

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, " Duration: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, " microsec"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move-wide/from16 v6, p1

    move/from16 v8, v19

    move v9, v15

    move-wide v10, v12

    .line 164
    invoke-static/range {v6 .. v11}, Lorg/chromium/media/WebAudioMediaCodecBridge;->nativeInitializeDestination(JIIJ)V

    .line 168
    const/4 v5, 0x1

    move/from16 v17, v5

    .line 171
    :goto_6
    if-eqz v17, :cond_5

    move-object/from16 v0, v23

    iget v5, v0, Landroid/media/MediaCodec$BufferInfo;->size:I

    if-lez v5, :cond_5

    .line 172
    move-object/from16 v0, v23

    iget v9, v0, Landroid/media/MediaCodec$BufferInfo;->size:I

    move-wide/from16 v6, p1

    move-object/from16 v8, v25

    move/from16 v10, v19

    move/from16 v11, v16

    invoke-static/range {v6 .. v11}, Lorg/chromium/media/WebAudioMediaCodecBridge;->nativeOnChunkDecoded(JLjava/nio/ByteBuffer;III)V

    .line 176
    :cond_5
    invoke-virtual/range {v25 .. v25}, Ljava/nio/ByteBuffer;->clear()Ljava/nio/Buffer;

    .line 177
    const/4 v5, 0x0

    move/from16 v0, v24

    invoke-virtual {v4, v0, v5}, Landroid/media/MediaCodec;->releaseOutputBuffer(IZ)V

    .line 179
    move-object/from16 v0, v23

    iget v5, v0, Landroid/media/MediaCodec$BufferInfo;->flags:I

    and-int/lit8 v5, v5, 0x4

    if-eqz v5, :cond_c

    .line 180
    const/4 v5, 0x1

    :goto_7
    move/from16 v11, v17

    move v14, v5

    .line 182
    goto/16 :goto_3

    .line 79
    :catch_1
    move-exception v6

    const-string/jumbo v6, "WebAudioMediaCodec"

    const-string/jumbo v7, "Cannot get duration"

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 99
    :catch_2
    move-exception v2

    const-string/jumbo v2, "WebAudioMediaCodec"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "Failed to create MediaCodec for mime type: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v21

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 100
    invoke-virtual/range {v20 .. v20}, Landroid/os/ParcelFileDescriptor;->detachFd()I

    .line 101
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 132
    :cond_6
    invoke-virtual {v2}, Landroid/media/MediaExtractor;->getSampleTime()J

    move-result-wide v8

    goto/16 :goto_4

    .line 135
    :cond_7
    const/4 v10, 0x0

    goto/16 :goto_5

    .line 182
    :cond_8
    const/4 v5, -0x3

    move/from16 v0, v24

    if-ne v0, v5, :cond_9

    .line 183
    invoke-virtual {v4}, Landroid/media/MediaCodec;->getOutputBuffers()[Ljava/nio/ByteBuffer;

    move-result-object v5

    move-object/from16 v18, v5

    goto/16 :goto_3

    .line 184
    :cond_9
    const/4 v5, -0x2

    move/from16 v0, v24

    if-ne v0, v5, :cond_b

    .line 185
    invoke-virtual {v4}, Landroid/media/MediaCodec;->getOutputFormat()Landroid/media/MediaFormat;

    move-result-object v7

    .line 186
    const-string/jumbo v5, "channel-count"

    invoke-virtual {v7, v5}, Landroid/media/MediaFormat;->getInteger(Ljava/lang/String;)I

    move-result v6

    .line 187
    const-string/jumbo v5, "sample-rate"

    invoke-virtual {v7, v5}, Landroid/media/MediaFormat;->getInteger(Ljava/lang/String;)I

    move-result v5

    .line 188
    const-string/jumbo v8, "WebAudioMediaCodec"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string/jumbo v10, "output format changed to "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v8, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_8
    move v15, v5

    move/from16 v16, v6

    .line 190
    goto/16 :goto_3

    .line 192
    :cond_a
    invoke-virtual/range {v20 .. v20}, Landroid/os/ParcelFileDescriptor;->detachFd()I

    .line 194
    invoke-virtual {v4}, Landroid/media/MediaCodec;->stop()V

    .line 195
    invoke-virtual {v4}, Landroid/media/MediaCodec;->release()V

    .line 196
    const/4 v2, 0x1

    goto/16 :goto_0

    :cond_b
    move v5, v15

    move/from16 v6, v16

    goto :goto_8

    :cond_c
    move v5, v14

    goto/16 :goto_7

    :cond_d
    move/from16 v17, v11

    goto/16 :goto_6

    :cond_e
    move-wide v12, v4

    goto/16 :goto_2
.end method

.method private static native nativeInitializeDestination(JIIJ)V
.end method

.method private static native nativeOnChunkDecoded(JLjava/nio/ByteBuffer;III)V
.end method
