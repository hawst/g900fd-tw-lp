.class public final enum Lorg/chromium/sync/internal_api/pub/base/ModelType;
.super Ljava/lang/Enum;
.source "ModelType.java"


# static fields
.field private static final synthetic $VALUES:[Lorg/chromium/sync/internal_api/pub/base/ModelType;

.field static final synthetic $assertionsDisabled:Z

.field public static final ALL_TYPES_TYPE:Ljava/lang/String; = "ALL_TYPES"

.field public static final enum AUTOFILL:Lorg/chromium/sync/internal_api/pub/base/ModelType;

.field public static final enum AUTOFILL_PROFILE:Lorg/chromium/sync/internal_api/pub/base/ModelType;

.field public static final enum BOOKMARK:Lorg/chromium/sync/internal_api/pub/base/ModelType;

.field public static final enum DEVICE_INFO:Lorg/chromium/sync/internal_api/pub/base/ModelType;

.field public static final enum EXPERIMENTS:Lorg/chromium/sync/internal_api/pub/base/ModelType;

.field public static final enum FAVICON_IMAGE:Lorg/chromium/sync/internal_api/pub/base/ModelType;

.field public static final enum FAVICON_TRACKING:Lorg/chromium/sync/internal_api/pub/base/ModelType;

.field public static final enum HISTORY_DELETE_DIRECTIVE:Lorg/chromium/sync/internal_api/pub/base/ModelType;

.field public static final enum MANAGED_USER_SETTING:Lorg/chromium/sync/internal_api/pub/base/ModelType;

.field public static final enum NIGORI:Lorg/chromium/sync/internal_api/pub/base/ModelType;

.field public static final enum PASSWORD:Lorg/chromium/sync/internal_api/pub/base/ModelType;

.field public static final enum PROXY_TABS:Lorg/chromium/sync/internal_api/pub/base/ModelType;

.field public static final enum SESSION:Lorg/chromium/sync/internal_api/pub/base/ModelType;

.field public static final enum TYPED_URL:Lorg/chromium/sync/internal_api/pub/base/ModelType;


# instance fields
.field private final mModelType:Ljava/lang/String;

.field private final mNonInvalidationType:Z


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 24
    const-class v0, Lorg/chromium/sync/internal_api/pub/base/ModelType;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    sput-boolean v0, Lorg/chromium/sync/internal_api/pub/base/ModelType;->$assertionsDisabled:Z

    .line 28
    new-instance v0, Lorg/chromium/sync/internal_api/pub/base/ModelType;

    const-string/jumbo v3, "AUTOFILL"

    const-string/jumbo v4, "AUTOFILL"

    invoke-direct {v0, v3, v2, v4}, Lorg/chromium/sync/internal_api/pub/base/ModelType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/chromium/sync/internal_api/pub/base/ModelType;->AUTOFILL:Lorg/chromium/sync/internal_api/pub/base/ModelType;

    .line 32
    new-instance v0, Lorg/chromium/sync/internal_api/pub/base/ModelType;

    const-string/jumbo v3, "AUTOFILL_PROFILE"

    const-string/jumbo v4, "AUTOFILL_PROFILE"

    invoke-direct {v0, v3, v1, v4}, Lorg/chromium/sync/internal_api/pub/base/ModelType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/chromium/sync/internal_api/pub/base/ModelType;->AUTOFILL_PROFILE:Lorg/chromium/sync/internal_api/pub/base/ModelType;

    .line 36
    new-instance v0, Lorg/chromium/sync/internal_api/pub/base/ModelType;

    const-string/jumbo v3, "BOOKMARK"

    const-string/jumbo v4, "BOOKMARK"

    invoke-direct {v0, v3, v6, v4}, Lorg/chromium/sync/internal_api/pub/base/ModelType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/chromium/sync/internal_api/pub/base/ModelType;->BOOKMARK:Lorg/chromium/sync/internal_api/pub/base/ModelType;

    .line 40
    new-instance v0, Lorg/chromium/sync/internal_api/pub/base/ModelType;

    const-string/jumbo v3, "EXPERIMENTS"

    const-string/jumbo v4, "EXPERIMENTS"

    invoke-direct {v0, v3, v7, v4}, Lorg/chromium/sync/internal_api/pub/base/ModelType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/chromium/sync/internal_api/pub/base/ModelType;->EXPERIMENTS:Lorg/chromium/sync/internal_api/pub/base/ModelType;

    .line 44
    new-instance v0, Lorg/chromium/sync/internal_api/pub/base/ModelType;

    const-string/jumbo v3, "NIGORI"

    const-string/jumbo v4, "NIGORI"

    invoke-direct {v0, v3, v8, v4}, Lorg/chromium/sync/internal_api/pub/base/ModelType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/chromium/sync/internal_api/pub/base/ModelType;->NIGORI:Lorg/chromium/sync/internal_api/pub/base/ModelType;

    .line 48
    new-instance v0, Lorg/chromium/sync/internal_api/pub/base/ModelType;

    const-string/jumbo v3, "PASSWORD"

    const/4 v4, 0x5

    const-string/jumbo v5, "PASSWORD"

    invoke-direct {v0, v3, v4, v5}, Lorg/chromium/sync/internal_api/pub/base/ModelType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/chromium/sync/internal_api/pub/base/ModelType;->PASSWORD:Lorg/chromium/sync/internal_api/pub/base/ModelType;

    .line 52
    new-instance v0, Lorg/chromium/sync/internal_api/pub/base/ModelType;

    const-string/jumbo v3, "SESSION"

    const/4 v4, 0x6

    const-string/jumbo v5, "SESSION"

    invoke-direct {v0, v3, v4, v5}, Lorg/chromium/sync/internal_api/pub/base/ModelType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/chromium/sync/internal_api/pub/base/ModelType;->SESSION:Lorg/chromium/sync/internal_api/pub/base/ModelType;

    .line 56
    new-instance v0, Lorg/chromium/sync/internal_api/pub/base/ModelType;

    const-string/jumbo v3, "TYPED_URL"

    const/4 v4, 0x7

    const-string/jumbo v5, "TYPED_URL"

    invoke-direct {v0, v3, v4, v5}, Lorg/chromium/sync/internal_api/pub/base/ModelType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/chromium/sync/internal_api/pub/base/ModelType;->TYPED_URL:Lorg/chromium/sync/internal_api/pub/base/ModelType;

    .line 60
    new-instance v0, Lorg/chromium/sync/internal_api/pub/base/ModelType;

    const-string/jumbo v3, "HISTORY_DELETE_DIRECTIVE"

    const/16 v4, 0x8

    const-string/jumbo v5, "HISTORY_DELETE_DIRECTIVE"

    invoke-direct {v0, v3, v4, v5}, Lorg/chromium/sync/internal_api/pub/base/ModelType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/chromium/sync/internal_api/pub/base/ModelType;->HISTORY_DELETE_DIRECTIVE:Lorg/chromium/sync/internal_api/pub/base/ModelType;

    .line 64
    new-instance v0, Lorg/chromium/sync/internal_api/pub/base/ModelType;

    const-string/jumbo v3, "DEVICE_INFO"

    const/16 v4, 0x9

    const-string/jumbo v5, "DEVICE_INFO"

    invoke-direct {v0, v3, v4, v5}, Lorg/chromium/sync/internal_api/pub/base/ModelType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/chromium/sync/internal_api/pub/base/ModelType;->DEVICE_INFO:Lorg/chromium/sync/internal_api/pub/base/ModelType;

    .line 68
    new-instance v0, Lorg/chromium/sync/internal_api/pub/base/ModelType;

    const-string/jumbo v3, "PROXY_TABS"

    const/16 v4, 0xa

    const-string/jumbo v5, "NULL"

    invoke-direct {v0, v3, v4, v5, v1}, Lorg/chromium/sync/internal_api/pub/base/ModelType;-><init>(Ljava/lang/String;ILjava/lang/String;Z)V

    sput-object v0, Lorg/chromium/sync/internal_api/pub/base/ModelType;->PROXY_TABS:Lorg/chromium/sync/internal_api/pub/base/ModelType;

    .line 72
    new-instance v0, Lorg/chromium/sync/internal_api/pub/base/ModelType;

    const-string/jumbo v3, "FAVICON_IMAGE"

    const/16 v4, 0xb

    const-string/jumbo v5, "FAVICON_IMAGE"

    invoke-direct {v0, v3, v4, v5}, Lorg/chromium/sync/internal_api/pub/base/ModelType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/chromium/sync/internal_api/pub/base/ModelType;->FAVICON_IMAGE:Lorg/chromium/sync/internal_api/pub/base/ModelType;

    .line 76
    new-instance v0, Lorg/chromium/sync/internal_api/pub/base/ModelType;

    const-string/jumbo v3, "FAVICON_TRACKING"

    const/16 v4, 0xc

    const-string/jumbo v5, "FAVICON_TRACKING"

    invoke-direct {v0, v3, v4, v5}, Lorg/chromium/sync/internal_api/pub/base/ModelType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/chromium/sync/internal_api/pub/base/ModelType;->FAVICON_TRACKING:Lorg/chromium/sync/internal_api/pub/base/ModelType;

    .line 81
    new-instance v0, Lorg/chromium/sync/internal_api/pub/base/ModelType;

    const-string/jumbo v3, "MANAGED_USER_SETTING"

    const/16 v4, 0xd

    const-string/jumbo v5, "MANAGED_USER_SETTING"

    invoke-direct {v0, v3, v4, v5}, Lorg/chromium/sync/internal_api/pub/base/ModelType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/chromium/sync/internal_api/pub/base/ModelType;->MANAGED_USER_SETTING:Lorg/chromium/sync/internal_api/pub/base/ModelType;

    .line 24
    const/16 v0, 0xe

    new-array v0, v0, [Lorg/chromium/sync/internal_api/pub/base/ModelType;

    sget-object v3, Lorg/chromium/sync/internal_api/pub/base/ModelType;->AUTOFILL:Lorg/chromium/sync/internal_api/pub/base/ModelType;

    aput-object v3, v0, v2

    sget-object v2, Lorg/chromium/sync/internal_api/pub/base/ModelType;->AUTOFILL_PROFILE:Lorg/chromium/sync/internal_api/pub/base/ModelType;

    aput-object v2, v0, v1

    sget-object v1, Lorg/chromium/sync/internal_api/pub/base/ModelType;->BOOKMARK:Lorg/chromium/sync/internal_api/pub/base/ModelType;

    aput-object v1, v0, v6

    sget-object v1, Lorg/chromium/sync/internal_api/pub/base/ModelType;->EXPERIMENTS:Lorg/chromium/sync/internal_api/pub/base/ModelType;

    aput-object v1, v0, v7

    sget-object v1, Lorg/chromium/sync/internal_api/pub/base/ModelType;->NIGORI:Lorg/chromium/sync/internal_api/pub/base/ModelType;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lorg/chromium/sync/internal_api/pub/base/ModelType;->PASSWORD:Lorg/chromium/sync/internal_api/pub/base/ModelType;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lorg/chromium/sync/internal_api/pub/base/ModelType;->SESSION:Lorg/chromium/sync/internal_api/pub/base/ModelType;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lorg/chromium/sync/internal_api/pub/base/ModelType;->TYPED_URL:Lorg/chromium/sync/internal_api/pub/base/ModelType;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lorg/chromium/sync/internal_api/pub/base/ModelType;->HISTORY_DELETE_DIRECTIVE:Lorg/chromium/sync/internal_api/pub/base/ModelType;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lorg/chromium/sync/internal_api/pub/base/ModelType;->DEVICE_INFO:Lorg/chromium/sync/internal_api/pub/base/ModelType;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lorg/chromium/sync/internal_api/pub/base/ModelType;->PROXY_TABS:Lorg/chromium/sync/internal_api/pub/base/ModelType;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lorg/chromium/sync/internal_api/pub/base/ModelType;->FAVICON_IMAGE:Lorg/chromium/sync/internal_api/pub/base/ModelType;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lorg/chromium/sync/internal_api/pub/base/ModelType;->FAVICON_TRACKING:Lorg/chromium/sync/internal_api/pub/base/ModelType;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lorg/chromium/sync/internal_api/pub/base/ModelType;->MANAGED_USER_SETTING:Lorg/chromium/sync/internal_api/pub/base/ModelType;

    aput-object v2, v0, v1

    sput-object v0, Lorg/chromium/sync/internal_api/pub/base/ModelType;->$VALUES:[Lorg/chromium/sync/internal_api/pub/base/ModelType;

    return-void

    :cond_0
    move v0, v2

    goto/16 :goto_0
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 1

    .prologue
    .line 99
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lorg/chromium/sync/internal_api/pub/base/ModelType;-><init>(Ljava/lang/String;ILjava/lang/String;Z)V

    .line 100
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;Z)V
    .locals 1

    .prologue
    .line 92
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 93
    sget-boolean v0, Lorg/chromium/sync/internal_api/pub/base/ModelType;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    if-nez p4, :cond_0

    invoke-virtual {p0}, Lorg/chromium/sync/internal_api/pub/base/ModelType;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 94
    :cond_0
    iput-object p3, p0, Lorg/chromium/sync/internal_api/pub/base/ModelType;->mModelType:Ljava/lang/String;

    .line 95
    iput-boolean p4, p0, Lorg/chromium/sync/internal_api/pub/base/ModelType;->mNonInvalidationType:Z

    .line 96
    return-void
.end method

.method public static filterOutNonInvalidationTypes(Ljava/util/Set;)Ljava/util/Set;
    .locals 4

    .prologue
    .line 193
    new-instance v1, Ljava/util/HashSet;

    invoke-interface {p0}, Ljava/util/Set;->size()I

    move-result v0

    invoke-direct {v1, v0}, Ljava/util/HashSet;-><init>(I)V

    .line 194
    invoke-interface {p0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/chromium/sync/internal_api/pub/base/ModelType;

    .line 195
    invoke-direct {v0}, Lorg/chromium/sync/internal_api/pub/base/ModelType;->isNonInvalidationType()Z

    move-result v3

    if-nez v3, :cond_0

    .line 196
    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 199
    :cond_1
    return-object v1
.end method

.method private isNonInvalidationType()Z
    .locals 2

    .prologue
    .line 103
    sget-object v0, Lorg/chromium/sync/internal_api/pub/base/ModelType;->SESSION:Lorg/chromium/sync/internal_api/pub/base/ModelType;

    if-eq p0, v0, :cond_0

    sget-object v0, Lorg/chromium/sync/internal_api/pub/base/ModelType;->FAVICON_TRACKING:Lorg/chromium/sync/internal_api/pub/base/ModelType;

    if-ne p0, v0, :cond_1

    :cond_0
    invoke-static {}, Lorg/chromium/base/library_loader/LibraryLoader;->isInitialized()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 104
    const-string/jumbo v0, "AndroidSessionNotifications"

    invoke-static {v0}, Lorg/chromium/base/FieldTrialList;->findFullName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "Disabled"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 108
    :goto_0
    return v0

    :cond_1
    iget-boolean v0, p0, Lorg/chromium/sync/internal_api/pub/base/ModelType;->mNonInvalidationType:Z

    goto :goto_0
.end method

.method public static modelTypesToObjectIds(Ljava/util/Set;)Ljava/util/Set;
    .locals 3

    .prologue
    .line 173
    invoke-static {p0}, Lorg/chromium/sync/internal_api/pub/base/ModelType;->filterOutNonInvalidationTypes(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    .line 174
    new-instance v1, Ljava/util/HashSet;

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/HashSet;-><init>(I)V

    .line 175
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/chromium/sync/internal_api/pub/base/ModelType;

    .line 176
    invoke-virtual {v0}, Lorg/chromium/sync/internal_api/pub/base/ModelType;->toObjectId()Lcom/google/ipc/invalidation/external/client/types/ObjectId;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 178
    :cond_0
    return-object v1
.end method

.method public static syncTypesToModelTypes(Ljava/util/Collection;)Ljava/util/Set;
    .locals 6

    .prologue
    .line 142
    const-string/jumbo v0, "ALL_TYPES"

    invoke-interface {p0, v0}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 143
    const-class v0, Lorg/chromium/sync/internal_api/pub/base/ModelType;

    invoke-static {v0}, Ljava/util/EnumSet;->allOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object v0

    .line 154
    :goto_0
    return-object v0

    .line 145
    :cond_0
    new-instance v1, Ljava/util/HashSet;

    invoke-interface {p0}, Ljava/util/Collection;->size()I

    move-result v0

    invoke-direct {v1, v0}, Ljava/util/HashSet;-><init>(I)V

    .line 146
    invoke-interface {p0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 148
    :try_start_0
    invoke-static {v0}, Lorg/chromium/sync/internal_api/pub/base/ModelType;->valueOf(Ljava/lang/String;)Lorg/chromium/sync/internal_api/pub/base/ModelType;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 151
    :catch_0
    move-exception v3

    const-string/jumbo v3, "ModelType"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "Could not translate sync type to model type: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :cond_1
    move-object v0, v1

    .line 154
    goto :goto_0
.end method

.method public static syncTypesToObjectIds(Ljava/util/Collection;)Ljava/util/Set;
    .locals 1

    .prologue
    .line 164
    invoke-static {p0}, Lorg/chromium/sync/internal_api/pub/base/ModelType;->syncTypesToModelTypes(Ljava/util/Collection;)Ljava/util/Set;

    move-result-object v0

    invoke-static {v0}, Lorg/chromium/sync/internal_api/pub/base/ModelType;->modelTypesToObjectIds(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)Lorg/chromium/sync/internal_api/pub/base/ModelType;
    .locals 1

    .prologue
    .line 24
    const-class v0, Lorg/chromium/sync/internal_api/pub/base/ModelType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lorg/chromium/sync/internal_api/pub/base/ModelType;

    return-object v0
.end method

.method public static values()[Lorg/chromium/sync/internal_api/pub/base/ModelType;
    .locals 1

    .prologue
    .line 24
    sget-object v0, Lorg/chromium/sync/internal_api/pub/base/ModelType;->$VALUES:[Lorg/chromium/sync/internal_api/pub/base/ModelType;

    invoke-virtual {v0}, [Lorg/chromium/sync/internal_api/pub/base/ModelType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lorg/chromium/sync/internal_api/pub/base/ModelType;

    return-object v0
.end method


# virtual methods
.method public final toObjectId()Lcom/google/ipc/invalidation/external/client/types/ObjectId;
    .locals 2

    .prologue
    .line 120
    const/16 v0, 0x3ec

    iget-object v1, p0, Lorg/chromium/sync/internal_api/pub/base/ModelType;->mModelType:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->getBytes()[B

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/ipc/invalidation/external/client/types/ObjectId;->newInstance(I[B)Lcom/google/ipc/invalidation/external/client/types/ObjectId;

    move-result-object v0

    return-object v0
.end method
