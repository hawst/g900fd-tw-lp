.class final Lorg/chromium/sync/internal_api/pub/SyncDecryptionPassphraseType$1;
.super Ljava/lang/Object;
.source "SyncDecryptionPassphraseType.java"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final bridge synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 29
    invoke-virtual {p0, p1}, Lorg/chromium/sync/internal_api/pub/SyncDecryptionPassphraseType$1;->createFromParcel(Landroid/os/Parcel;)Lorg/chromium/sync/internal_api/pub/SyncDecryptionPassphraseType;

    move-result-object v0

    return-object v0
.end method

.method public final createFromParcel(Landroid/os/Parcel;)Lorg/chromium/sync/internal_api/pub/SyncDecryptionPassphraseType;
    .locals 1

    .prologue
    .line 32
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    invoke-static {v0}, Lorg/chromium/sync/internal_api/pub/SyncDecryptionPassphraseType;->fromInternalValue(I)Lorg/chromium/sync/internal_api/pub/SyncDecryptionPassphraseType;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 29
    invoke-virtual {p0, p1}, Lorg/chromium/sync/internal_api/pub/SyncDecryptionPassphraseType$1;->newArray(I)[Lorg/chromium/sync/internal_api/pub/SyncDecryptionPassphraseType;

    move-result-object v0

    return-object v0
.end method

.method public final newArray(I)[Lorg/chromium/sync/internal_api/pub/SyncDecryptionPassphraseType;
    .locals 1

    .prologue
    .line 37
    new-array v0, p1, [Lorg/chromium/sync/internal_api/pub/SyncDecryptionPassphraseType;

    return-object v0
.end method
