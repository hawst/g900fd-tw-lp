.class public Lorg/chromium/sync/signin/ChromeSigninController;
.super Ljava/lang/Object;
.source "ChromeSigninController.java"


# static fields
.field private static final LOCK:Ljava/lang/Object;

.field public static final SIGNED_IN_ACCOUNT_KEY:Ljava/lang/String; = "google.services.username"

.field public static final TAG:Ljava/lang/String; = "ChromeSigninController"

.field private static sChromeSigninController:Lorg/chromium/sync/signin/ChromeSigninController;


# instance fields
.field private final mApplicationContext:Landroid/content/Context;

.field private mGcmInitialized:Z

.field private final mListeners:Lorg/chromium/base/ObserverList;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 33
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lorg/chromium/sync/signin/ChromeSigninController;->LOCK:Ljava/lang/Object;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    new-instance v0, Lorg/chromium/base/ObserverList;

    invoke-direct {v0}, Lorg/chromium/base/ObserverList;-><init>()V

    iput-object v0, p0, Lorg/chromium/sync/signin/ChromeSigninController;->mListeners:Lorg/chromium/base/ObserverList;

    .line 44
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lorg/chromium/sync/signin/ChromeSigninController;->mApplicationContext:Landroid/content/Context;

    .line 45
    return-void
.end method

.method static synthetic access$000(Lorg/chromium/sync/signin/ChromeSigninController;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lorg/chromium/sync/signin/ChromeSigninController;->mApplicationContext:Landroid/content/Context;

    return-object v0
.end method

.method public static get(Landroid/content/Context;)Lorg/chromium/sync/signin/ChromeSigninController;
    .locals 2

    .prologue
    .line 54
    sget-object v1, Lorg/chromium/sync/signin/ChromeSigninController;->LOCK:Ljava/lang/Object;

    monitor-enter v1

    .line 55
    :try_start_0
    sget-object v0, Lorg/chromium/sync/signin/ChromeSigninController;->sChromeSigninController:Lorg/chromium/sync/signin/ChromeSigninController;

    if-nez v0, :cond_0

    .line 56
    new-instance v0, Lorg/chromium/sync/signin/ChromeSigninController;

    invoke-direct {v0, p0}, Lorg/chromium/sync/signin/ChromeSigninController;-><init>(Landroid/content/Context;)V

    sput-object v0, Lorg/chromium/sync/signin/ChromeSigninController;->sChromeSigninController:Lorg/chromium/sync/signin/ChromeSigninController;

    .line 58
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 59
    sget-object v0, Lorg/chromium/sync/signin/ChromeSigninController;->sChromeSigninController:Lorg/chromium/sync/signin/ChromeSigninController;

    return-object v0

    .line 58
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public addListener(Lorg/chromium/sync/signin/ChromeSigninController$Listener;)V
    .locals 1

    .prologue
    .line 99
    iget-object v0, p0, Lorg/chromium/sync/signin/ChromeSigninController;->mListeners:Lorg/chromium/base/ObserverList;

    invoke-virtual {v0, p1}, Lorg/chromium/base/ObserverList;->addObserver(Ljava/lang/Object;)Z

    .line 100
    return-void
.end method

.method public clearSignedInUser()V
    .locals 2

    .prologue
    .line 81
    const-string/jumbo v0, "ChromeSigninController"

    const-string/jumbo v1, "Clearing user signed in to Chrome"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 82
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lorg/chromium/sync/signin/ChromeSigninController;->setSignedInAccountName(Ljava/lang/String;)V

    .line 84
    iget-object v0, p0, Lorg/chromium/sync/signin/ChromeSigninController;->mListeners:Lorg/chromium/base/ObserverList;

    invoke-virtual {v0}, Lorg/chromium/base/ObserverList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/chromium/sync/signin/ChromeSigninController$Listener;

    .line 85
    invoke-interface {v0}, Lorg/chromium/sync/signin/ChromeSigninController$Listener;->onClearSignedInUser()V

    goto :goto_0

    .line 87
    :cond_0
    return-void
.end method

.method public ensureGcmIsInitialized()V
    .locals 3

    .prologue
    .line 114
    iget-boolean v0, p0, Lorg/chromium/sync/signin/ChromeSigninController;->mGcmInitialized:Z

    if-eqz v0, :cond_0

    .line 138
    :goto_0
    return-void

    .line 115
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/chromium/sync/signin/ChromeSigninController;->mGcmInitialized:Z

    .line 116
    new-instance v0, Lorg/chromium/sync/signin/ChromeSigninController$1;

    invoke-direct {v0, p0}, Lorg/chromium/sync/signin/ChromeSigninController$1;-><init>(Lorg/chromium/sync/signin/ChromeSigninController;)V

    sget-object v1, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Void;

    invoke-virtual {v0, v1, v2}, Lorg/chromium/sync/signin/ChromeSigninController$1;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0
.end method

.method public getSignedInAccountName()Ljava/lang/String;
    .locals 3

    .prologue
    .line 90
    iget-object v0, p0, Lorg/chromium/sync/signin/ChromeSigninController;->mApplicationContext:Landroid/content/Context;

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string/jumbo v1, "google.services.username"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getSignedInUser()Landroid/accounts/Account;
    .locals 1

    .prologue
    .line 63
    invoke-virtual {p0}, Lorg/chromium/sync/signin/ChromeSigninController;->getSignedInAccountName()Ljava/lang/String;

    move-result-object v0

    .line 64
    if-nez v0, :cond_0

    .line 65
    const/4 v0, 0x0

    .line 67
    :goto_0
    return-object v0

    :cond_0
    invoke-static {v0}, Lorg/chromium/sync/signin/AccountManagerHelper;->createAccountFromName(Ljava/lang/String;)Landroid/accounts/Account;

    move-result-object v0

    goto :goto_0
.end method

.method public isSignedIn()Z
    .locals 1

    .prologue
    .line 71
    invoke-virtual {p0}, Lorg/chromium/sync/signin/ChromeSigninController;->getSignedInAccountName()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setSignedInAccountName(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 75
    iget-object v0, p0, Lorg/chromium/sync/signin/ChromeSigninController;->mApplicationContext:Landroid/content/Context;

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string/jumbo v1, "google.services.username"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 78
    return-void
.end method
