.class public Lorg/chromium/sync/signin/AccountManagerHelper;
.super Ljava/lang/Object;
.source "AccountManagerHelper.java"


# static fields
.field public static final GOOGLE_ACCOUNT_TYPE:Ljava/lang/String; = "com.google"

.field private static sAccountManagerHelper:Lorg/chromium/sync/signin/AccountManagerHelper;

.field private static final sLock:Ljava/lang/Object;


# instance fields
.field private final mAccountManager:Lorg/chromium/sync/signin/AccountManagerDelegate;

.field private mApplicationContext:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 44
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lorg/chromium/sync/signin/AccountManagerHelper;->sLock:Ljava/lang/Object;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Lorg/chromium/sync/signin/AccountManagerDelegate;)V
    .locals 1

    .prologue
    .line 68
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 69
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lorg/chromium/sync/signin/AccountManagerHelper;->mApplicationContext:Landroid/content/Context;

    .line 70
    iput-object p2, p0, Lorg/chromium/sync/signin/AccountManagerHelper;->mAccountManager:Lorg/chromium/sync/signin/AccountManagerDelegate;

    .line 71
    return-void
.end method

.method static synthetic access$000(Lorg/chromium/sync/signin/AccountManagerHelper;Landroid/app/Activity;Landroid/accounts/Account;Ljava/lang/String;Lorg/chromium/sync/signin/AccountManagerHelper$GetAuthTokenCallback;Ljava/util/concurrent/atomic/AtomicInteger;Ljava/util/concurrent/atomic/AtomicBoolean;Lorg/chromium/sync/signin/AccountManagerHelper$ConnectionRetry;)V
    .locals 0

    .prologue
    .line 38
    invoke-direct/range {p0 .. p7}, Lorg/chromium/sync/signin/AccountManagerHelper;->getAuthTokenAsynchronously(Landroid/app/Activity;Landroid/accounts/Account;Ljava/lang/String;Lorg/chromium/sync/signin/AccountManagerHelper$GetAuthTokenCallback;Ljava/util/concurrent/atomic/AtomicInteger;Ljava/util/concurrent/atomic/AtomicBoolean;Lorg/chromium/sync/signin/AccountManagerHelper$ConnectionRetry;)V

    return-void
.end method

.method static synthetic access$100(Lorg/chromium/sync/signin/AccountManagerHelper;Landroid/accounts/AccountManagerFuture;Ljava/util/concurrent/atomic/AtomicBoolean;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 38
    invoke-direct {p0, p1, p2}, Lorg/chromium/sync/signin/AccountManagerHelper;->getAuthTokenInner(Landroid/accounts/AccountManagerFuture;Ljava/util/concurrent/atomic/AtomicBoolean;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$200(Lorg/chromium/sync/signin/AccountManagerHelper;Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;Lorg/chromium/sync/signin/AccountManagerHelper$GetAuthTokenCallback;Ljava/util/concurrent/atomic/AtomicInteger;Ljava/util/concurrent/atomic/AtomicBoolean;Lorg/chromium/sync/signin/AccountManagerHelper$ConnectionRetry;)V
    .locals 0

    .prologue
    .line 38
    invoke-direct/range {p0 .. p7}, Lorg/chromium/sync/signin/AccountManagerHelper;->onGotAuthTokenResult(Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;Lorg/chromium/sync/signin/AccountManagerHelper$GetAuthTokenCallback;Ljava/util/concurrent/atomic/AtomicInteger;Ljava/util/concurrent/atomic/AtomicBoolean;Lorg/chromium/sync/signin/AccountManagerHelper$ConnectionRetry;)V

    return-void
.end method

.method public static createAccountFromName(Ljava/lang/String;)Landroid/accounts/Account;
    .locals 2

    .prologue
    .line 105
    new-instance v0, Landroid/accounts/Account;

    const-string/jumbo v1, "com.google"

    invoke-direct {v0, p0, v1}, Landroid/accounts/Account;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public static get(Landroid/content/Context;)Lorg/chromium/sync/signin/AccountManagerHelper;
    .locals 3

    .prologue
    .line 84
    sget-object v1, Lorg/chromium/sync/signin/AccountManagerHelper;->sLock:Ljava/lang/Object;

    monitor-enter v1

    .line 85
    :try_start_0
    sget-object v0, Lorg/chromium/sync/signin/AccountManagerHelper;->sAccountManagerHelper:Lorg/chromium/sync/signin/AccountManagerHelper;

    if-nez v0, :cond_0

    .line 86
    new-instance v0, Lorg/chromium/sync/signin/AccountManagerHelper;

    new-instance v2, Lorg/chromium/sync/signin/SystemAccountManagerDelegate;

    invoke-direct {v2, p0}, Lorg/chromium/sync/signin/SystemAccountManagerDelegate;-><init>(Landroid/content/Context;)V

    invoke-direct {v0, p0, v2}, Lorg/chromium/sync/signin/AccountManagerHelper;-><init>(Landroid/content/Context;Lorg/chromium/sync/signin/AccountManagerDelegate;)V

    sput-object v0, Lorg/chromium/sync/signin/AccountManagerHelper;->sAccountManagerHelper:Lorg/chromium/sync/signin/AccountManagerHelper;

    .line 89
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 90
    sget-object v0, Lorg/chromium/sync/signin/AccountManagerHelper;->sAccountManagerHelper:Lorg/chromium/sync/signin/AccountManagerHelper;

    return-object v0

    .line 89
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private getAuthTokenAsynchronously(Landroid/app/Activity;Landroid/accounts/Account;Ljava/lang/String;Lorg/chromium/sync/signin/AccountManagerHelper$GetAuthTokenCallback;Ljava/util/concurrent/atomic/AtomicInteger;Ljava/util/concurrent/atomic/AtomicBoolean;Lorg/chromium/sync/signin/AccountManagerHelper$ConnectionRetry;)V
    .locals 9

    .prologue
    .line 252
    invoke-virtual {p5}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v0

    if-nez v0, :cond_0

    if-eqz p1, :cond_0

    .line 253
    iget-object v0, p0, Lorg/chromium/sync/signin/AccountManagerHelper;->mAccountManager:Lorg/chromium/sync/signin/AccountManagerDelegate;

    const/4 v3, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object v1, p2

    move-object v2, p3

    move-object v4, p1

    invoke-interface/range {v0 .. v6}, Lorg/chromium/sync/signin/AccountManagerDelegate;->getAuthToken(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;Landroid/app/Activity;Landroid/accounts/AccountManagerCallback;Landroid/os/Handler;)Landroid/accounts/AccountManagerFuture;

    move-result-object v2

    .line 260
    :goto_0
    const/4 v0, 0x0

    invoke-virtual {p6, v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 264
    invoke-static {}, Lorg/chromium/base/ThreadUtils;->runningOnUiThread()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 265
    new-instance v0, Lorg/chromium/sync/signin/AccountManagerHelper$1;

    move-object v1, p0

    move-object v3, p6

    move-object v4, p2

    move-object v5, p3

    move-object v6, p4

    move-object v7, p5

    move-object/from16 v8, p7

    invoke-direct/range {v0 .. v8}, Lorg/chromium/sync/signin/AccountManagerHelper$1;-><init>(Lorg/chromium/sync/signin/AccountManagerHelper;Landroid/accounts/AccountManagerFuture;Ljava/util/concurrent/atomic/AtomicBoolean;Landroid/accounts/Account;Ljava/lang/String;Lorg/chromium/sync/signin/AccountManagerHelper$GetAuthTokenCallback;Ljava/util/concurrent/atomic/AtomicInteger;Lorg/chromium/sync/signin/AccountManagerHelper$ConnectionRetry;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lorg/chromium/sync/signin/AccountManagerHelper$1;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 281
    :goto_1
    return-void

    .line 256
    :cond_0
    iget-object v0, p0, Lorg/chromium/sync/signin/AccountManagerHelper;->mAccountManager:Lorg/chromium/sync/signin/AccountManagerDelegate;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v1, p2

    move-object v2, p3

    invoke-interface/range {v0 .. v5}, Lorg/chromium/sync/signin/AccountManagerDelegate;->getAuthToken(Landroid/accounts/Account;Ljava/lang/String;ZLandroid/accounts/AccountManagerCallback;Landroid/os/Handler;)Landroid/accounts/AccountManagerFuture;

    move-result-object v2

    goto :goto_0

    .line 277
    :cond_1
    invoke-direct {p0, v2, p6}, Lorg/chromium/sync/signin/AccountManagerHelper;->getAuthTokenInner(Landroid/accounts/AccountManagerFuture;Ljava/util/concurrent/atomic/AtomicBoolean;)Ljava/lang/String;

    move-result-object v3

    move-object v0, p0

    move-object v1, p2

    move-object v2, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    move-object/from16 v7, p7

    .line 278
    invoke-direct/range {v0 .. v7}, Lorg/chromium/sync/signin/AccountManagerHelper;->onGotAuthTokenResult(Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;Lorg/chromium/sync/signin/AccountManagerHelper$GetAuthTokenCallback;Ljava/util/concurrent/atomic/AtomicInteger;Ljava/util/concurrent/atomic/AtomicBoolean;Lorg/chromium/sync/signin/AccountManagerHelper$ConnectionRetry;)V

    goto :goto_1
.end method

.method private getAuthTokenInner(Landroid/accounts/AccountManagerFuture;Ljava/util/concurrent/atomic/AtomicBoolean;)Ljava/lang/String;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 220
    :try_start_0
    invoke-interface {p1}, Landroid/accounts/AccountManagerFuture;->getResult()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    .line 221
    if-eqz v0, :cond_1

    .line 222
    const-string/jumbo v2, "intent"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 223
    const-string/jumbo v2, "AccountManagerHelper"

    const-string/jumbo v3, "Starting intent to get auth credentials"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 225
    const-string/jumbo v2, "intent"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    .line 226
    invoke-virtual {v0}, Landroid/content/Intent;->getFlags()I

    move-result v2

    .line 227
    const/high16 v3, 0x10000000

    or-int/2addr v2, v3

    .line 228
    invoke-virtual {v0, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 229
    iget-object v2, p0, Lorg/chromium/sync/signin/AccountManagerHelper;->mApplicationContext:Landroid/content/Context;

    invoke-virtual {v2, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    move-object v0, v1

    .line 244
    :goto_0
    return-object v0

    .line 232
    :cond_0
    const-string/jumbo v2, "authtoken"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 234
    :cond_1
    const-string/jumbo v0, "AccountManagerHelper"

    const-string/jumbo v2, "Auth token - getAuthToken returned null"

    invoke-static {v0, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/accounts/OperationCanceledException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/accounts/AuthenticatorException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2

    :goto_1
    move-object v0, v1

    .line 244
    goto :goto_0

    .line 236
    :catch_0
    move-exception v0

    .line 237
    const-string/jumbo v2, "AccountManagerHelper"

    const-string/jumbo v3, "Auth token - operation cancelled"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1

    .line 238
    :catch_1
    move-exception v0

    .line 239
    const-string/jumbo v2, "AccountManagerHelper"

    const-string/jumbo v3, "Auth token - authenticator exception"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1

    .line 240
    :catch_2
    move-exception v0

    .line 241
    const-string/jumbo v2, "AccountManagerHelper"

    const-string/jumbo v3, "Auth token - IO exception"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 242
    const/4 v0, 0x1

    invoke-virtual {p2, v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    goto :goto_1
.end method

.method private onGotAuthTokenResult(Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;Lorg/chromium/sync/signin/AccountManagerHelper$GetAuthTokenCallback;Ljava/util/concurrent/atomic/AtomicInteger;Ljava/util/concurrent/atomic/AtomicBoolean;Lorg/chromium/sync/signin/AccountManagerHelper$ConnectionRetry;)V
    .locals 7

    .prologue
    .line 286
    if-nez p3, :cond_0

    invoke-virtual {p6}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p5}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    move-result v0

    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    invoke-static {}, Lorg/chromium/net/NetworkChangeNotifier;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_1

    .line 289
    :cond_0
    invoke-interface {p4, p3}, Lorg/chromium/sync/signin/AccountManagerHelper$GetAuthTokenCallback;->tokenAvailable(Ljava/lang/String;)V

    .line 299
    :goto_0
    return-void

    .line 292
    :cond_1
    if-nez p7, :cond_2

    .line 293
    new-instance v0, Lorg/chromium/sync/signin/AccountManagerHelper$ConnectionRetry;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    invoke-direct/range {v0 .. v6}, Lorg/chromium/sync/signin/AccountManagerHelper$ConnectionRetry;-><init>(Lorg/chromium/sync/signin/AccountManagerHelper;Landroid/accounts/Account;Ljava/lang/String;Lorg/chromium/sync/signin/AccountManagerHelper$GetAuthTokenCallback;Ljava/util/concurrent/atomic/AtomicInteger;Ljava/util/concurrent/atomic/AtomicBoolean;)V

    .line 295
    invoke-static {v0}, Lorg/chromium/net/NetworkChangeNotifier;->addConnectionTypeObserver(Lorg/chromium/net/NetworkChangeNotifier$ConnectionTypeObserver;)V

    goto :goto_0

    .line 297
    :cond_2
    invoke-static {p7}, Lorg/chromium/net/NetworkChangeNotifier;->addConnectionTypeObserver(Lorg/chromium/net/NetworkChangeNotifier$ConnectionTypeObserver;)V

    goto :goto_0
.end method

.method public static overrideAccountManagerHelperForTests(Landroid/content/Context;Lorg/chromium/sync/signin/AccountManagerDelegate;)V
    .locals 2

    .prologue
    .line 96
    sget-object v1, Lorg/chromium/sync/signin/AccountManagerHelper;->sLock:Ljava/lang/Object;

    monitor-enter v1

    .line 97
    :try_start_0
    new-instance v0, Lorg/chromium/sync/signin/AccountManagerHelper;

    invoke-direct {v0, p0, p1}, Lorg/chromium/sync/signin/AccountManagerHelper;-><init>(Landroid/content/Context;Lorg/chromium/sync/signin/AccountManagerDelegate;)V

    sput-object v0, Lorg/chromium/sync/signin/AccountManagerHelper;->sAccountManagerHelper:Lorg/chromium/sync/signin/AccountManagerHelper;

    .line 98
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public getAccountFromName(Ljava/lang/String;)Landroid/accounts/Account;
    .locals 5

    .prologue
    .line 129
    iget-object v0, p0, Lorg/chromium/sync/signin/AccountManagerHelper;->mAccountManager:Lorg/chromium/sync/signin/AccountManagerDelegate;

    const-string/jumbo v1, "com.google"

    invoke-interface {v0, v1}, Lorg/chromium/sync/signin/AccountManagerDelegate;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v2

    .line 130
    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v0, v2, v1

    .line 131
    iget-object v4, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v4, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 135
    :goto_1
    return-object v0

    .line 130
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 135
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public getAuthTokenFromBackground(Landroid/accounts/Account;Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 164
    iget-object v0, p0, Lorg/chromium/sync/signin/AccountManagerHelper;->mAccountManager:Lorg/chromium/sync/signin/AccountManagerDelegate;

    move-object v1, p1

    move-object v2, p2

    move-object v5, v4

    invoke-interface/range {v0 .. v5}, Lorg/chromium/sync/signin/AccountManagerDelegate;->getAuthToken(Landroid/accounts/Account;Ljava/lang/String;ZLandroid/accounts/AccountManagerCallback;Landroid/os/Handler;)Landroid/accounts/AccountManagerFuture;

    move-result-object v0

    .line 166
    new-instance v1, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v1, v3}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    .line 167
    invoke-direct {p0, v0, v1}, Lorg/chromium/sync/signin/AccountManagerHelper;->getAuthTokenInner(Landroid/accounts/AccountManagerFuture;Ljava/util/concurrent/atomic/AtomicBoolean;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getAuthTokenFromForeground(Landroid/app/Activity;Landroid/accounts/Account;Ljava/lang/String;Lorg/chromium/sync/signin/AccountManagerHelper$GetAuthTokenCallback;)V
    .locals 8

    .prologue
    const/4 v0, 0x0

    .line 179
    new-instance v5, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v5, v0}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    .line 180
    new-instance v6, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v6, v0}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    .line 181
    const/4 v7, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v7}, Lorg/chromium/sync/signin/AccountManagerHelper;->getAuthTokenAsynchronously(Landroid/app/Activity;Landroid/accounts/Account;Ljava/lang/String;Lorg/chromium/sync/signin/AccountManagerHelper$GetAuthTokenCallback;Ljava/util/concurrent/atomic/AtomicInteger;Ljava/util/concurrent/atomic/AtomicBoolean;Lorg/chromium/sync/signin/AccountManagerHelper$ConnectionRetry;)V

    .line 183
    return-void
.end method

.method public getGoogleAccountNames()Ljava/util/List;
    .locals 5

    .prologue
    .line 109
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 110
    iget-object v0, p0, Lorg/chromium/sync/signin/AccountManagerHelper;->mAccountManager:Lorg/chromium/sync/signin/AccountManagerDelegate;

    const-string/jumbo v2, "com.google"

    invoke-interface {v0, v2}, Lorg/chromium/sync/signin/AccountManagerDelegate;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v2

    .line 111
    array-length v3, v2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_0

    aget-object v4, v2, v0

    .line 112
    iget-object v4, v4, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 111
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 114
    :cond_0
    return-object v1
.end method

.method public getGoogleAccounts()[Landroid/accounts/Account;
    .locals 2

    .prologue
    .line 118
    iget-object v0, p0, Lorg/chromium/sync/signin/AccountManagerHelper;->mAccountManager:Lorg/chromium/sync/signin/AccountManagerDelegate;

    const-string/jumbo v1, "com.google"

    invoke-interface {v0, v1}, Lorg/chromium/sync/signin/AccountManagerDelegate;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v0

    return-object v0
.end method

.method public getNewAuthToken(Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 314
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 315
    iget-object v0, p0, Lorg/chromium/sync/signin/AccountManagerHelper;->mAccountManager:Lorg/chromium/sync/signin/AccountManagerDelegate;

    const-string/jumbo v1, "com.google"

    invoke-interface {v0, v1, p2}, Lorg/chromium/sync/signin/AccountManagerDelegate;->invalidateAuthToken(Ljava/lang/String;Ljava/lang/String;)V

    .line 319
    :cond_0
    :try_start_0
    iget-object v0, p0, Lorg/chromium/sync/signin/AccountManagerHelper;->mAccountManager:Lorg/chromium/sync/signin/AccountManagerDelegate;

    const/4 v1, 0x1

    invoke-interface {v0, p1, p3, v1}, Lorg/chromium/sync/signin/AccountManagerDelegate;->blockingGetAuthToken(Landroid/accounts/Account;Ljava/lang/String;Z)Ljava/lang/String;
    :try_end_0
    .catch Landroid/accounts/OperationCanceledException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/accounts/AuthenticatorException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2

    move-result-object v0

    .line 327
    :goto_0
    return-object v0

    .line 320
    :catch_0
    move-exception v0

    .line 321
    const-string/jumbo v1, "AccountManagerHelper"

    const-string/jumbo v2, "Auth token - operation cancelled"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 327
    :goto_1
    const/4 v0, 0x0

    goto :goto_0

    .line 322
    :catch_1
    move-exception v0

    .line 323
    const-string/jumbo v1, "AccountManagerHelper"

    const-string/jumbo v2, "Auth token - authenticator exception"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1

    .line 324
    :catch_2
    move-exception v0

    .line 325
    const-string/jumbo v1, "AccountManagerHelper"

    const-string/jumbo v2, "Auth token - IO exception"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method public getNewAuthTokenFromForeground(Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;Lorg/chromium/sync/signin/AccountManagerHelper$GetAuthTokenCallback;)V
    .locals 8

    .prologue
    const/4 v1, 0x0

    const/4 v3, 0x0

    .line 337
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 338
    iget-object v0, p0, Lorg/chromium/sync/signin/AccountManagerHelper;->mAccountManager:Lorg/chromium/sync/signin/AccountManagerDelegate;

    const-string/jumbo v2, "com.google"

    invoke-interface {v0, v2, p2}, Lorg/chromium/sync/signin/AccountManagerDelegate;->invalidateAuthToken(Ljava/lang/String;Ljava/lang/String;)V

    .line 340
    :cond_0
    new-instance v5, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v5, v3}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    .line 341
    new-instance v6, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v6, v3}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    move-object v0, p0

    move-object v2, p1

    move-object v3, p3

    move-object v4, p4

    move-object v7, v1

    .line 342
    invoke-direct/range {v0 .. v7}, Lorg/chromium/sync/signin/AccountManagerHelper;->getAuthTokenAsynchronously(Landroid/app/Activity;Landroid/accounts/Account;Ljava/lang/String;Lorg/chromium/sync/signin/AccountManagerHelper$GetAuthTokenCallback;Ljava/util/concurrent/atomic/AtomicInteger;Ljava/util/concurrent/atomic/AtomicBoolean;Lorg/chromium/sync/signin/AccountManagerHelper$ConnectionRetry;)V

    .line 344
    return-void
.end method

.method public hasAccountForName(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 142
    invoke-virtual {p0, p1}, Lorg/chromium/sync/signin/AccountManagerHelper;->getAccountFromName(Ljava/lang/String;)Landroid/accounts/Account;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasGoogleAccountAuthenticator()Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 149
    iget-object v1, p0, Lorg/chromium/sync/signin/AccountManagerHelper;->mAccountManager:Lorg/chromium/sync/signin/AccountManagerDelegate;

    invoke-interface {v1}, Lorg/chromium/sync/signin/AccountManagerDelegate;->getAuthenticatorTypes()[Landroid/accounts/AuthenticatorDescription;

    move-result-object v2

    .line 150
    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v4, v2, v1

    .line 151
    const-string/jumbo v5, "com.google"

    iget-object v4, v4, Landroid/accounts/AuthenticatorDescription;->type:Ljava/lang/String;

    invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    const/4 v0, 0x1

    .line 153
    :cond_0
    return v0

    .line 150
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public hasGoogleAccounts()Z
    .locals 1

    .prologue
    .line 122
    invoke-virtual {p0}, Lorg/chromium/sync/signin/AccountManagerHelper;->getGoogleAccounts()[Landroid/accounts/Account;

    move-result-object v0

    array-length v0, v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public invalidateAuthToken(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 350
    iget-object v0, p0, Lorg/chromium/sync/signin/AccountManagerHelper;->mAccountManager:Lorg/chromium/sync/signin/AccountManagerDelegate;

    const-string/jumbo v1, "com.google"

    invoke-interface {v0, v1, p1}, Lorg/chromium/sync/signin/AccountManagerDelegate;->invalidateAuthToken(Ljava/lang/String;Ljava/lang/String;)V

    .line 351
    return-void
.end method
