.class Lorg/chromium/sync/signin/AccountManagerHelper$1;
.super Landroid/os/AsyncTask;
.source "AccountManagerHelper.java"


# instance fields
.field final synthetic this$0:Lorg/chromium/sync/signin/AccountManagerHelper;

.field final synthetic val$account:Landroid/accounts/Account;

.field final synthetic val$authTokenType:Ljava/lang/String;

.field final synthetic val$callback:Lorg/chromium/sync/signin/AccountManagerHelper$GetAuthTokenCallback;

.field final synthetic val$errorEncountered:Ljava/util/concurrent/atomic/AtomicBoolean;

.field final synthetic val$finalFuture:Landroid/accounts/AccountManagerFuture;

.field final synthetic val$numTries:Ljava/util/concurrent/atomic/AtomicInteger;

.field final synthetic val$retry:Lorg/chromium/sync/signin/AccountManagerHelper$ConnectionRetry;


# direct methods
.method constructor <init>(Lorg/chromium/sync/signin/AccountManagerHelper;Landroid/accounts/AccountManagerFuture;Ljava/util/concurrent/atomic/AtomicBoolean;Landroid/accounts/Account;Ljava/lang/String;Lorg/chromium/sync/signin/AccountManagerHelper$GetAuthTokenCallback;Ljava/util/concurrent/atomic/AtomicInteger;Lorg/chromium/sync/signin/AccountManagerHelper$ConnectionRetry;)V
    .locals 0

    .prologue
    .line 265
    iput-object p1, p0, Lorg/chromium/sync/signin/AccountManagerHelper$1;->this$0:Lorg/chromium/sync/signin/AccountManagerHelper;

    iput-object p2, p0, Lorg/chromium/sync/signin/AccountManagerHelper$1;->val$finalFuture:Landroid/accounts/AccountManagerFuture;

    iput-object p3, p0, Lorg/chromium/sync/signin/AccountManagerHelper$1;->val$errorEncountered:Ljava/util/concurrent/atomic/AtomicBoolean;

    iput-object p4, p0, Lorg/chromium/sync/signin/AccountManagerHelper$1;->val$account:Landroid/accounts/Account;

    iput-object p5, p0, Lorg/chromium/sync/signin/AccountManagerHelper$1;->val$authTokenType:Ljava/lang/String;

    iput-object p6, p0, Lorg/chromium/sync/signin/AccountManagerHelper$1;->val$callback:Lorg/chromium/sync/signin/AccountManagerHelper$GetAuthTokenCallback;

    iput-object p7, p0, Lorg/chromium/sync/signin/AccountManagerHelper$1;->val$numTries:Ljava/util/concurrent/atomic/AtomicInteger;

    iput-object p8, p0, Lorg/chromium/sync/signin/AccountManagerHelper$1;->val$retry:Lorg/chromium/sync/signin/AccountManagerHelper$ConnectionRetry;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 265
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lorg/chromium/sync/signin/AccountManagerHelper$1;->doInBackground([Ljava/lang/Void;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public varargs doInBackground([Ljava/lang/Void;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 268
    iget-object v0, p0, Lorg/chromium/sync/signin/AccountManagerHelper$1;->this$0:Lorg/chromium/sync/signin/AccountManagerHelper;

    iget-object v1, p0, Lorg/chromium/sync/signin/AccountManagerHelper$1;->val$finalFuture:Landroid/accounts/AccountManagerFuture;

    iget-object v2, p0, Lorg/chromium/sync/signin/AccountManagerHelper$1;->val$errorEncountered:Ljava/util/concurrent/atomic/AtomicBoolean;

    # invokes: Lorg/chromium/sync/signin/AccountManagerHelper;->getAuthTokenInner(Landroid/accounts/AccountManagerFuture;Ljava/util/concurrent/atomic/AtomicBoolean;)Ljava/lang/String;
    invoke-static {v0, v1, v2}, Lorg/chromium/sync/signin/AccountManagerHelper;->access$100(Lorg/chromium/sync/signin/AccountManagerHelper;Landroid/accounts/AccountManagerFuture;Ljava/util/concurrent/atomic/AtomicBoolean;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 265
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lorg/chromium/sync/signin/AccountManagerHelper$1;->onPostExecute(Ljava/lang/String;)V

    return-void
.end method

.method public onPostExecute(Ljava/lang/String;)V
    .locals 8

    .prologue
    .line 272
    iget-object v0, p0, Lorg/chromium/sync/signin/AccountManagerHelper$1;->this$0:Lorg/chromium/sync/signin/AccountManagerHelper;

    iget-object v1, p0, Lorg/chromium/sync/signin/AccountManagerHelper$1;->val$account:Landroid/accounts/Account;

    iget-object v2, p0, Lorg/chromium/sync/signin/AccountManagerHelper$1;->val$authTokenType:Ljava/lang/String;

    iget-object v4, p0, Lorg/chromium/sync/signin/AccountManagerHelper$1;->val$callback:Lorg/chromium/sync/signin/AccountManagerHelper$GetAuthTokenCallback;

    iget-object v5, p0, Lorg/chromium/sync/signin/AccountManagerHelper$1;->val$numTries:Ljava/util/concurrent/atomic/AtomicInteger;

    iget-object v6, p0, Lorg/chromium/sync/signin/AccountManagerHelper$1;->val$errorEncountered:Ljava/util/concurrent/atomic/AtomicBoolean;

    iget-object v7, p0, Lorg/chromium/sync/signin/AccountManagerHelper$1;->val$retry:Lorg/chromium/sync/signin/AccountManagerHelper$ConnectionRetry;

    move-object v3, p1

    # invokes: Lorg/chromium/sync/signin/AccountManagerHelper;->onGotAuthTokenResult(Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;Lorg/chromium/sync/signin/AccountManagerHelper$GetAuthTokenCallback;Ljava/util/concurrent/atomic/AtomicInteger;Ljava/util/concurrent/atomic/AtomicBoolean;Lorg/chromium/sync/signin/AccountManagerHelper$ConnectionRetry;)V
    invoke-static/range {v0 .. v7}, Lorg/chromium/sync/signin/AccountManagerHelper;->access$200(Lorg/chromium/sync/signin/AccountManagerHelper;Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;Lorg/chromium/sync/signin/AccountManagerHelper$GetAuthTokenCallback;Ljava/util/concurrent/atomic/AtomicInteger;Ljava/util/concurrent/atomic/AtomicBoolean;Lorg/chromium/sync/signin/AccountManagerHelper$ConnectionRetry;)V

    .line 274
    return-void
.end method
