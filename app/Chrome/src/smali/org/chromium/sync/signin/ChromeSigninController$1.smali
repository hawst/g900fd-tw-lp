.class Lorg/chromium/sync/signin/ChromeSigninController$1;
.super Landroid/os/AsyncTask;
.source "ChromeSigninController.java"


# instance fields
.field final synthetic this$0:Lorg/chromium/sync/signin/ChromeSigninController;


# direct methods
.method constructor <init>(Lorg/chromium/sync/signin/ChromeSigninController;)V
    .locals 0

    .prologue
    .line 116
    iput-object p1, p0, Lorg/chromium/sync/signin/ChromeSigninController$1;->this$0:Lorg/chromium/sync/signin/ChromeSigninController;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 116
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lorg/chromium/sync/signin/ChromeSigninController$1;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 119
    invoke-static {}, Lorg/chromium/base/CommandLine;->getInstance()Lorg/chromium/base/CommandLine;

    move-result-object v0

    const-string/jumbo v1, "disable-sync-gcm-in-order-to-try-push-api"

    invoke-virtual {v0, v1}, Lorg/chromium/base/CommandLine;->hasSwitch(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 121
    const-string/jumbo v0, "ChromeSigninController"

    const-string/jumbo v1, "Sync GCM notifications disabled in order to try Push API!"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 135
    :cond_0
    :goto_0
    return-object v3

    .line 125
    :cond_1
    :try_start_0
    iget-object v0, p0, Lorg/chromium/sync/signin/ChromeSigninController$1;->this$0:Lorg/chromium/sync/signin/ChromeSigninController;

    # getter for: Lorg/chromium/sync/signin/ChromeSigninController;->mApplicationContext:Landroid/content/Context;
    invoke-static {v0}, Lorg/chromium/sync/signin/ChromeSigninController;->access$000(Lorg/chromium/sync/signin/ChromeSigninController;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/ipc/invalidation/external/client/contrib/MultiplexingGcmListener;->initializeGcm(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 126
    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 127
    const-string/jumbo v0, "ChromeSigninController"

    const-string/jumbo v1, "Already registered with GCM"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 128
    :catch_0
    move-exception v0

    .line 129
    const-string/jumbo v1, "ChromeSigninController"

    const-string/jumbo v2, "Application manifest does not correctly configure GCM; sync notifications will not work"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 131
    :catch_1
    move-exception v0

    .line 132
    const-string/jumbo v1, "ChromeSigninController"

    const-string/jumbo v2, "Device does not support GCM; sync notifications will not work"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method
