.class public Lorg/chromium/sync/notifier/InvalidationService;
.super Lcom/google/ipc/invalidation/external/client/contrib/AndroidListener;
.source "InvalidationService.java"


# static fields
.field static final CLIENT_TYPE:I = 0x3fa

.field private static final RANDOM:Ljava/util/Random;

.field private static sClientId:[B
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private static sIsClientStarted:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 62
    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    sput-object v0, Lorg/chromium/sync/notifier/InvalidationService;->RANDOM:Ljava/util/Random;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 53
    invoke-direct {p0}, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListener;-><init>()V

    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 53
    invoke-static {}, Lorg/chromium/sync/notifier/InvalidationService;->getOAuth2ScopeWithType()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static computeRegistrationOps(Ljava/util/Set;Ljava/util/Set;Ljava/util/Set;Ljava/util/Set;)V
    .locals 0

    .prologue
    .line 412
    invoke-interface {p2, p1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 413
    invoke-interface {p2, p0}, Ljava/util/Set;->removeAll(Ljava/util/Collection;)Z

    .line 416
    invoke-interface {p3, p0}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 417
    invoke-interface {p3, p1}, Ljava/util/Set;->removeAll(Ljava/util/Collection;)Z

    .line 418
    return-void
.end method

.method private ensureAccount(Landroid/accounts/Account;)V
    .locals 1

    .prologue
    .line 250
    if-nez p1, :cond_1

    .line 260
    :cond_0
    :goto_0
    return-void

    .line 253
    :cond_1
    new-instance v0, Lorg/chromium/sync/notifier/InvalidationPreferences;

    invoke-direct {v0, p0}, Lorg/chromium/sync/notifier/InvalidationPreferences;-><init>(Landroid/content/Context;)V

    .line 254
    invoke-virtual {v0}, Lorg/chromium/sync/notifier/InvalidationPreferences;->getSavedSyncedAccount()Landroid/accounts/Account;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/accounts/Account;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 255
    sget-boolean v0, Lorg/chromium/sync/notifier/InvalidationService;->sIsClientStarted:Z

    if-eqz v0, :cond_2

    .line 256
    invoke-direct {p0}, Lorg/chromium/sync/notifier/InvalidationService;->stopClient()V

    .line 258
    :cond_2
    invoke-direct {p0, p1}, Lorg/chromium/sync/notifier/InvalidationService;->setAccount(Landroid/accounts/Account;)V

    goto :goto_0
.end method

.method private ensureClientStartState()V
    .locals 2

    .prologue
    .line 235
    invoke-virtual {p0}, Lorg/chromium/sync/notifier/InvalidationService;->shouldClientBeRunning()Z

    move-result v0

    .line 236
    if-nez v0, :cond_1

    sget-boolean v1, Lorg/chromium/sync/notifier/InvalidationService;->sIsClientStarted:Z

    if-eqz v1, :cond_1

    .line 238
    invoke-direct {p0}, Lorg/chromium/sync/notifier/InvalidationService;->stopClient()V

    .line 243
    :cond_0
    :goto_0
    return-void

    .line 239
    :cond_1
    if-eqz v0, :cond_0

    sget-boolean v0, Lorg/chromium/sync/notifier/InvalidationService;->sIsClientStarted:Z

    if-nez v0, :cond_0

    .line 241
    invoke-direct {p0}, Lorg/chromium/sync/notifier/InvalidationService;->startClient()V

    goto :goto_0
.end method

.method static getClientIdForTest()[B
    .locals 1
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 493
    sget-object v0, Lorg/chromium/sync/notifier/InvalidationService;->sClientId:[B

    return-object v0
.end method

.method static getIsClientStartedForTest()Z
    .locals 1

    .prologue
    .line 487
    sget-boolean v0, Lorg/chromium/sync/notifier/InvalidationService;->sIsClientStarted:Z

    return v0
.end method

.method private static getOAuth2ScopeWithType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 497
    const-string/jumbo v0, "oauth2:https://www.googleapis.com/auth/chromesync"

    return-object v0
.end method

.method private static joinRegistrations(Ljava/util/Set;Ljava/util/Set;)Ljava/util/Set;
    .locals 3

    .prologue
    .line 325
    invoke-interface {p1}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 335
    :goto_0
    return-object p0

    .line 328
    :cond_0
    invoke-interface {p0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    move-object p0, p1

    .line 329
    goto :goto_0

    .line 331
    :cond_1
    new-instance v0, Ljava/util/HashSet;

    invoke-interface {p0}, Ljava/util/Set;->size()I

    move-result v1

    invoke-interface {p1}, Ljava/util/Set;->size()I

    move-result v2

    add-int/2addr v1, v2

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(I)V

    .line 333
    invoke-interface {v0, p0}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 334
    invoke-interface {v0, p1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    move-object p0, v0

    .line 335
    goto :goto_0
.end method

.method private readNonSyncRegistrationsFromPrefs()Ljava/util/Set;
    .locals 1

    .prologue
    .line 304
    new-instance v0, Lorg/chromium/sync/notifier/InvalidationPreferences;

    invoke-direct {v0, p0}, Lorg/chromium/sync/notifier/InvalidationPreferences;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0}, Lorg/chromium/sync/notifier/InvalidationPreferences;->getSavedObjectIds()Ljava/util/Set;

    move-result-object v0

    .line 305
    if-nez v0, :cond_0

    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;

    move-result-object v0

    .line 306
    :cond_0
    return-object v0
.end method

.method private readSyncRegistrationsFromPrefs()Ljava/util/Set;
    .locals 1

    .prologue
    .line 294
    new-instance v0, Lorg/chromium/sync/notifier/InvalidationPreferences;

    invoke-direct {v0, p0}, Lorg/chromium/sync/notifier/InvalidationPreferences;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0}, Lorg/chromium/sync/notifier/InvalidationPreferences;->getSavedSyncedTypes()Ljava/util/Set;

    move-result-object v0

    .line 295
    if-nez v0, :cond_0

    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;

    move-result-object v0

    .line 296
    :goto_0
    return-object v0

    :cond_0
    invoke-static {v0}, Lorg/chromium/sync/internal_api/pub/base/ModelType;->syncTypesToObjectIds(Ljava/util/Collection;)Ljava/util/Set;

    move-result-object v0

    goto :goto_0
.end method

.method private requestSync(Lcom/google/ipc/invalidation/external/client/types/ObjectId;Ljava/lang/Long;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 430
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 431
    if-nez p1, :cond_0

    if-nez p2, :cond_0

    if-eqz p3, :cond_3

    .line 434
    :cond_0
    if-eqz p1, :cond_1

    .line 435
    const-string/jumbo v0, "objectSource"

    invoke-virtual {p1}, Lcom/google/ipc/invalidation/external/client/types/ObjectId;->getSource()I

    move-result v1

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 436
    const-string/jumbo v0, "objectId"

    new-instance v1, Ljava/lang/String;

    invoke-virtual {p1}, Lcom/google/ipc/invalidation/external/client/types/ObjectId;->getName()[B

    move-result-object v3

    invoke-direct {v1, v3}, Ljava/lang/String;-><init>([B)V

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 441
    :cond_1
    const-string/jumbo v3, "version"

    if-nez p2, :cond_4

    const-wide/16 v0, 0x0

    :goto_0
    invoke-virtual {v2, v3, v0, v1}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 442
    const-string/jumbo v0, "payload"

    if-nez p3, :cond_2

    const-string/jumbo p3, ""

    :cond_2
    invoke-virtual {v2, v0, p3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 444
    :cond_3
    invoke-static {p0}, Lorg/chromium/sync/signin/ChromeSigninController;->get(Landroid/content/Context;)Lorg/chromium/sync/signin/ChromeSigninController;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/sync/signin/ChromeSigninController;->getSignedInUser()Landroid/accounts/Account;

    move-result-object v0

    .line 445
    invoke-static {p0}, Lorg/chromium/sync/notifier/SyncStatusHelper;->get(Landroid/content/Context;)Lorg/chromium/sync/notifier/SyncStatusHelper;

    move-result-object v1

    invoke-virtual {v1}, Lorg/chromium/sync/notifier/SyncStatusHelper;->getContractAuthority()Ljava/lang/String;

    move-result-object v1

    .line 446
    invoke-virtual {p0, v2, v0, v1}, Lorg/chromium/sync/notifier/InvalidationService;->requestSyncFromContentResolver(Landroid/os/Bundle;Landroid/accounts/Account;Ljava/lang/String;)V

    .line 447
    return-void

    .line 441
    :cond_4
    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    goto :goto_0
.end method

.method private setAccount(Landroid/accounts/Account;)V
    .locals 2

    .prologue
    .line 283
    new-instance v0, Lorg/chromium/sync/notifier/InvalidationPreferences;

    invoke-direct {v0, p0}, Lorg/chromium/sync/notifier/InvalidationPreferences;-><init>(Landroid/content/Context;)V

    .line 284
    invoke-virtual {v0}, Lorg/chromium/sync/notifier/InvalidationPreferences;->edit()Lorg/chromium/sync/notifier/InvalidationPreferences$EditContext;

    move-result-object v1

    .line 285
    invoke-virtual {v0, v1, p1}, Lorg/chromium/sync/notifier/InvalidationPreferences;->setAccount(Lorg/chromium/sync/notifier/InvalidationPreferences$EditContext;Landroid/accounts/Account;)V

    .line 286
    invoke-virtual {v0, v1}, Lorg/chromium/sync/notifier/InvalidationPreferences;->commit(Lorg/chromium/sync/notifier/InvalidationPreferences$EditContext;)Z

    .line 287
    return-void
.end method

.method private setRegisteredTypes(Ljava/util/Set;Ljava/util/Set;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 352
    sget-object v0, Lorg/chromium/sync/notifier/InvalidationService;->sClientId:[B

    if-nez v0, :cond_2

    move-object v0, v1

    .line 354
    :goto_0
    sget-object v2, Lorg/chromium/sync/notifier/InvalidationService;->sClientId:[B

    if-nez v2, :cond_3

    .line 360
    :goto_1
    new-instance v2, Lorg/chromium/sync/notifier/InvalidationPreferences;

    invoke-direct {v2, p0}, Lorg/chromium/sync/notifier/InvalidationPreferences;-><init>(Landroid/content/Context;)V

    .line 361
    invoke-virtual {v2}, Lorg/chromium/sync/notifier/InvalidationPreferences;->edit()Lorg/chromium/sync/notifier/InvalidationPreferences$EditContext;

    move-result-object v3

    .line 362
    if-eqz p1, :cond_0

    .line 363
    invoke-virtual {v2, v3, p1}, Lorg/chromium/sync/notifier/InvalidationPreferences;->setSyncTypes(Lorg/chromium/sync/notifier/InvalidationPreferences$EditContext;Ljava/util/Collection;)V

    .line 365
    :cond_0
    if-eqz p2, :cond_1

    .line 366
    invoke-virtual {v2, v3, p2}, Lorg/chromium/sync/notifier/InvalidationPreferences;->setObjectIds(Lorg/chromium/sync/notifier/InvalidationPreferences$EditContext;Ljava/util/Collection;)V

    .line 368
    :cond_1
    invoke-virtual {v2, v3}, Lorg/chromium/sync/notifier/InvalidationPreferences;->commit(Lorg/chromium/sync/notifier/InvalidationPreferences$EditContext;)Z

    .line 372
    sget-object v2, Lorg/chromium/sync/notifier/InvalidationService;->sClientId:[B

    if-nez v2, :cond_4

    .line 398
    :goto_2
    return-void

    .line 352
    :cond_2
    invoke-direct {p0}, Lorg/chromium/sync/notifier/InvalidationService;->readSyncRegistrationsFromPrefs()Ljava/util/Set;

    move-result-object v0

    goto :goto_0

    .line 354
    :cond_3
    invoke-direct {p0}, Lorg/chromium/sync/notifier/InvalidationService;->readNonSyncRegistrationsFromPrefs()Ljava/util/Set;

    move-result-object v1

    goto :goto_1

    .line 383
    :cond_4
    if-eqz p1, :cond_5

    invoke-static {p1}, Lorg/chromium/sync/internal_api/pub/base/ModelType;->syncTypesToObjectIds(Ljava/util/Collection;)Ljava/util/Set;

    move-result-object v2

    .line 385
    :goto_3
    if-eqz p2, :cond_6

    .line 387
    :goto_4
    invoke-static {p2, v2}, Lorg/chromium/sync/notifier/InvalidationService;->joinRegistrations(Ljava/util/Set;Ljava/util/Set;)Ljava/util/Set;

    move-result-object v2

    .line 389
    invoke-static {v1, v0}, Lorg/chromium/sync/notifier/InvalidationService;->joinRegistrations(Ljava/util/Set;Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    .line 392
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 393
    new-instance v3, Ljava/util/HashSet;

    invoke-direct {v3}, Ljava/util/HashSet;-><init>()V

    .line 394
    invoke-static {v0, v2, v3, v1}, Lorg/chromium/sync/notifier/InvalidationService;->computeRegistrationOps(Ljava/util/Set;Ljava/util/Set;Ljava/util/Set;Ljava/util/Set;)V

    .line 396
    sget-object v0, Lorg/chromium/sync/notifier/InvalidationService;->sClientId:[B

    invoke-virtual {p0, v0, v1}, Lorg/chromium/sync/notifier/InvalidationService;->unregister([BLjava/lang/Iterable;)V

    .line 397
    sget-object v0, Lorg/chromium/sync/notifier/InvalidationService;->sClientId:[B

    invoke-virtual {p0, v0, v3}, Lorg/chromium/sync/notifier/InvalidationService;->register([BLjava/lang/Iterable;)V

    goto :goto_2

    :cond_5
    move-object v2, v0

    .line 383
    goto :goto_3

    :cond_6
    move-object p2, v1

    .line 385
    goto :goto_4
.end method

.method private startClient()V
    .locals 2

    .prologue
    .line 268
    invoke-static {}, Lorg/chromium/sync/notifier/InvalidationClientNameProvider;->get()Lorg/chromium/sync/notifier/InvalidationClientNameProvider;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/sync/notifier/InvalidationClientNameProvider;->getInvalidatorClientName()[B

    move-result-object v0

    .line 269
    const/16 v1, 0x3fa

    invoke-static {p0, v1, v0}, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListener;->createStartIntent(Landroid/content/Context;I[B)Landroid/content/Intent;

    move-result-object v0

    .line 270
    invoke-virtual {p0, v0}, Lorg/chromium/sync/notifier/InvalidationService;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 271
    const/4 v0, 0x1

    sput-boolean v0, Lorg/chromium/sync/notifier/InvalidationService;->sIsClientStarted:Z

    .line 272
    return-void
.end method

.method private stopClient()V
    .locals 1

    .prologue
    .line 276
    invoke-static {p0}, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListener;->createStopIntent(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lorg/chromium/sync/notifier/InvalidationService;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 277
    const/4 v0, 0x0

    sput-boolean v0, Lorg/chromium/sync/notifier/InvalidationService;->sIsClientStarted:Z

    .line 278
    const/4 v0, 0x0

    sput-object v0, Lorg/chromium/sync/notifier/InvalidationService;->sClientId:[B

    .line 279
    return-void
.end method


# virtual methods
.method public informError(Lcom/google/ipc/invalidation/external/client/types/ErrorInfo;)V
    .locals 3

    .prologue
    .line 165
    const-string/jumbo v0, "InvalidationService"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "Invalidation client error:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 166
    invoke-virtual {p1}, Lcom/google/ipc/invalidation/external/client/types/ErrorInfo;->isTransient()Z

    move-result v0

    if-nez v0, :cond_0

    sget-boolean v0, Lorg/chromium/sync/notifier/InvalidationService;->sIsClientStarted:Z

    if-eqz v0, :cond_0

    .line 170
    invoke-direct {p0}, Lorg/chromium/sync/notifier/InvalidationService;->stopClient()V

    .line 172
    :cond_0
    return-void
.end method

.method public informRegistrationFailure([BLcom/google/ipc/invalidation/external/client/types/ObjectId;ZLjava/lang/String;)V
    .locals 3

    .prologue
    .line 130
    const-string/jumbo v0, "InvalidationService"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "Registration failure on "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " ; transient = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ": "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 132
    if-eqz p3, :cond_0

    .line 135
    const/4 v0, 0x1

    new-array v0, v0, [Lcom/google/ipc/invalidation/external/client/types/ObjectId;

    const/4 v1, 0x0

    aput-object p2, v0, v1

    invoke-static {v0}, Lorg/chromium/base/CollectionUtil;->newArrayList([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v0

    .line 136
    invoke-virtual {p0}, Lorg/chromium/sync/notifier/InvalidationService;->readRegistrationsFromPrefs()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1, p2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 137
    invoke-virtual {p0, p1, v0}, Lorg/chromium/sync/notifier/InvalidationService;->register([BLjava/lang/Iterable;)V

    .line 142
    :cond_0
    :goto_0
    return-void

    .line 139
    :cond_1
    invoke-virtual {p0, p1, v0}, Lorg/chromium/sync/notifier/InvalidationService;->unregister([BLjava/lang/Iterable;)V

    goto :goto_0
.end method

.method public informRegistrationStatus([BLcom/google/ipc/invalidation/external/client/types/ObjectId;Lcom/google/ipc/invalidation/external/client/InvalidationListener$RegistrationState;)V
    .locals 3

    .prologue
    .line 147
    const-string/jumbo v0, "InvalidationService"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "Registration status for "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ": "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 148
    const/4 v0, 0x1

    new-array v0, v0, [Lcom/google/ipc/invalidation/external/client/types/ObjectId;

    const/4 v1, 0x0

    aput-object p2, v0, v1

    invoke-static {v0}, Lorg/chromium/base/CollectionUtil;->newArrayList([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v0

    .line 149
    invoke-virtual {p0}, Lorg/chromium/sync/notifier/InvalidationService;->readRegistrationsFromPrefs()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1, p2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    .line 150
    sget-object v2, Lcom/google/ipc/invalidation/external/client/InvalidationListener$RegistrationState;->REGISTERED:Lcom/google/ipc/invalidation/external/client/InvalidationListener$RegistrationState;

    if-ne p3, v2, :cond_1

    .line 151
    if-nez v1, :cond_0

    .line 152
    const-string/jumbo v1, "InvalidationService"

    const-string/jumbo v2, "Unregistering for object we\'re no longer interested in"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 153
    invoke-virtual {p0, p1, v0}, Lorg/chromium/sync/notifier/InvalidationService;->unregister([BLjava/lang/Iterable;)V

    .line 161
    :cond_0
    :goto_0
    return-void

    .line 156
    :cond_1
    if-eqz v1, :cond_0

    .line 157
    const-string/jumbo v1, "InvalidationService"

    const-string/jumbo v2, "Registering for an object"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 158
    invoke-virtual {p0, p1, v0}, Lorg/chromium/sync/notifier/InvalidationService;->register([BLjava/lang/Iterable;)V

    goto :goto_0
.end method

.method public invalidate(Lcom/google/ipc/invalidation/external/client/types/Invalidation;[B)V
    .locals 4

    .prologue
    .line 109
    invoke-virtual {p1}, Lcom/google/ipc/invalidation/external/client/types/Invalidation;->getPayload()[B

    move-result-object v1

    .line 110
    if-nez v1, :cond_0

    const/4 v0, 0x0

    .line 111
    :goto_0
    invoke-virtual {p1}, Lcom/google/ipc/invalidation/external/client/types/Invalidation;->getObjectId()Lcom/google/ipc/invalidation/external/client/types/ObjectId;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/ipc/invalidation/external/client/types/Invalidation;->getVersion()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-direct {p0, v1, v2, v0}, Lorg/chromium/sync/notifier/InvalidationService;->requestSync(Lcom/google/ipc/invalidation/external/client/types/ObjectId;Ljava/lang/Long;Ljava/lang/String;)V

    .line 112
    invoke-virtual {p0, p2}, Lorg/chromium/sync/notifier/InvalidationService;->acknowledge([B)V

    .line 113
    return-void

    .line 110
    :cond_0
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>([B)V

    goto :goto_0
.end method

.method public invalidateAll([B)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 123
    invoke-direct {p0, v0, v0, v0}, Lorg/chromium/sync/notifier/InvalidationService;->requestSync(Lcom/google/ipc/invalidation/external/client/types/ObjectId;Ljava/lang/Long;Ljava/lang/String;)V

    .line 124
    invoke-virtual {p0, p1}, Lorg/chromium/sync/notifier/InvalidationService;->acknowledge([B)V

    .line 125
    return-void
.end method

.method public invalidateUnknownVersion(Lcom/google/ipc/invalidation/external/client/types/ObjectId;[B)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 117
    invoke-direct {p0, p1, v0, v0}, Lorg/chromium/sync/notifier/InvalidationService;->requestSync(Lcom/google/ipc/invalidation/external/client/types/ObjectId;Ljava/lang/Long;Ljava/lang/String;)V

    .line 118
    invoke-virtual {p0, p2}, Lorg/chromium/sync/notifier/InvalidationService;->acknowledge([B)V

    .line 119
    return-void
.end method

.method isChromeInForeground()Z
    .locals 1

    .prologue
    .line 481
    invoke-static {}, Lorg/chromium/base/ApplicationStatus;->hasVisibleActivities()Z

    move-result v0

    return v0
.end method

.method isSyncEnabled()Z
    .locals 1

    .prologue
    .line 473
    invoke-virtual {p0}, Lorg/chromium/sync/notifier/InvalidationService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lorg/chromium/sync/notifier/SyncStatusHelper;->get(Landroid/content/Context;)Lorg/chromium/sync/notifier/SyncStatusHelper;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/sync/notifier/SyncStatusHelper;->isSyncEnabled()Z

    move-result v0

    return v0
.end method

.method public onHandleIntent(Landroid/content/Intent;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 84
    const-string/jumbo v0, "account"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "account"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/accounts/Account;

    .line 88
    :goto_0
    invoke-direct {p0, v0}, Lorg/chromium/sync/notifier/InvalidationService;->ensureAccount(Landroid/accounts/Account;)V

    .line 89
    invoke-direct {p0}, Lorg/chromium/sync/notifier/InvalidationService;->ensureClientStartState()V

    .line 92
    invoke-static {p1}, Lorg/chromium/sync/notifier/InvalidationIntentProtocol;->isStop(Landroid/content/Intent;)Z

    move-result v0

    if-eqz v0, :cond_1

    sget-boolean v0, Lorg/chromium/sync/notifier/InvalidationService;->sIsClientStarted:Z

    if-eqz v0, :cond_1

    .line 94
    invoke-direct {p0}, Lorg/chromium/sync/notifier/InvalidationService;->stopClient()V

    .line 105
    :goto_1
    return-void

    :cond_0
    move-object v0, v1

    .line 84
    goto :goto_0

    .line 95
    :cond_1
    invoke-static {p1}, Lorg/chromium/sync/notifier/InvalidationIntentProtocol;->isRegisteredTypesChange(Landroid/content/Intent;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 97
    const-string/jumbo v0, "registered_types"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    .line 99
    if-eqz v0, :cond_2

    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1, v0}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    :cond_2
    invoke-static {p1}, Lorg/chromium/sync/notifier/InvalidationIntentProtocol;->getRegisteredObjectIds(Landroid/content/Intent;)Ljava/util/Set;

    move-result-object v0

    invoke-direct {p0, v1, v0}, Lorg/chromium/sync/notifier/InvalidationService;->setRegisteredTypes(Ljava/util/Set;Ljava/util/Set;)V

    goto :goto_1

    .line 103
    :cond_3
    invoke-super {p0, p1}, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListener;->onHandleIntent(Landroid/content/Intent;)V

    goto :goto_1
.end method

.method readRegistrationsFromPrefs()Ljava/util/Set;
    .locals 2

    .prologue
    .line 315
    invoke-direct {p0}, Lorg/chromium/sync/notifier/InvalidationService;->readSyncRegistrationsFromPrefs()Ljava/util/Set;

    move-result-object v0

    invoke-direct {p0}, Lorg/chromium/sync/notifier/InvalidationService;->readNonSyncRegistrationsFromPrefs()Ljava/util/Set;

    move-result-object v1

    invoke-static {v0, v1}, Lorg/chromium/sync/notifier/InvalidationService;->joinRegistrations(Ljava/util/Set;Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public readState()[B
    .locals 1
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 227
    new-instance v0, Lorg/chromium/sync/notifier/InvalidationPreferences;

    invoke-direct {v0, p0}, Lorg/chromium/sync/notifier/InvalidationPreferences;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0}, Lorg/chromium/sync/notifier/InvalidationPreferences;->getInternalNotificationClientState()[B

    move-result-object v0

    return-object v0
.end method

.method public ready([B)V
    .locals 0

    .prologue
    .line 176
    sput-object p1, Lorg/chromium/sync/notifier/InvalidationService;->sClientId:[B

    .line 180
    invoke-virtual {p0, p1}, Lorg/chromium/sync/notifier/InvalidationService;->reissueRegistrations([B)V

    .line 181
    return-void
.end method

.method public reissueRegistrations([B)V
    .locals 2

    .prologue
    .line 185
    invoke-virtual {p0}, Lorg/chromium/sync/notifier/InvalidationService;->readRegistrationsFromPrefs()Ljava/util/Set;

    move-result-object v0

    .line 186
    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 187
    invoke-virtual {p0, p1, v0}, Lorg/chromium/sync/notifier/InvalidationService;->register([BLjava/lang/Iterable;)V

    .line 189
    :cond_0
    return-void
.end method

.method public requestAuthToken(Landroid/app/PendingIntent;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 194
    invoke-static {p0}, Lorg/chromium/sync/signin/ChromeSigninController;->get(Landroid/content/Context;)Lorg/chromium/sync/signin/ChromeSigninController;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/sync/signin/ChromeSigninController;->getSignedInUser()Landroid/accounts/Account;

    move-result-object v0

    .line 195
    if-nez v0, :cond_0

    .line 198
    const-string/jumbo v0, "InvalidationService"

    const-string/jumbo v1, "No signed-in user; cannot send message to data center"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 215
    :goto_0
    return-void

    .line 204
    :cond_0
    invoke-static {p0}, Lorg/chromium/sync/signin/AccountManagerHelper;->get(Landroid/content/Context;)Lorg/chromium/sync/signin/AccountManagerHelper;

    move-result-object v1

    invoke-static {}, Lorg/chromium/sync/notifier/InvalidationService;->getOAuth2ScopeWithType()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lorg/chromium/sync/notifier/InvalidationService$1;

    invoke-direct {v3, p0, p1}, Lorg/chromium/sync/notifier/InvalidationService$1;-><init>(Lorg/chromium/sync/notifier/InvalidationService;Landroid/app/PendingIntent;)V

    invoke-virtual {v1, v0, p2, v2, v3}, Lorg/chromium/sync/signin/AccountManagerHelper;->getNewAuthTokenFromForeground(Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;Lorg/chromium/sync/signin/AccountManagerHelper$GetAuthTokenCallback;)V

    goto :goto_0
.end method

.method requestSyncFromContentResolver(Landroid/os/Bundle;Landroid/accounts/Account;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 456
    const-string/jumbo v0, "InvalidationService"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "Request sync: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " / "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " / "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 458
    invoke-static {p2, p3, p1}, Landroid/content/ContentResolver;->requestSync(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 459
    return-void
.end method

.method shouldClientBeRunning()Z
    .locals 1

    .prologue
    .line 467
    invoke-virtual {p0}, Lorg/chromium/sync/notifier/InvalidationService;->isSyncEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lorg/chromium/sync/notifier/InvalidationService;->isChromeInForeground()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public writeState([B)V
    .locals 2

    .prologue
    .line 219
    new-instance v0, Lorg/chromium/sync/notifier/InvalidationPreferences;

    invoke-direct {v0, p0}, Lorg/chromium/sync/notifier/InvalidationPreferences;-><init>(Landroid/content/Context;)V

    .line 220
    invoke-virtual {v0}, Lorg/chromium/sync/notifier/InvalidationPreferences;->edit()Lorg/chromium/sync/notifier/InvalidationPreferences$EditContext;

    move-result-object v1

    .line 221
    invoke-virtual {v0, v1, p1}, Lorg/chromium/sync/notifier/InvalidationPreferences;->setInternalNotificationClientState(Lorg/chromium/sync/notifier/InvalidationPreferences$EditContext;[B)V

    .line 222
    invoke-virtual {v0, v1}, Lorg/chromium/sync/notifier/InvalidationPreferences;->commit(Lorg/chromium/sync/notifier/InvalidationPreferences$EditContext;)Z

    .line 223
    return-void
.end method
