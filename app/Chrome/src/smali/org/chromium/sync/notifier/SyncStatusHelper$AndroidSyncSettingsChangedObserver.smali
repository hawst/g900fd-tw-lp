.class Lorg/chromium/sync/notifier/SyncStatusHelper$AndroidSyncSettingsChangedObserver;
.super Ljava/lang/Object;
.source "SyncStatusHelper.java"

# interfaces
.implements Landroid/content/SyncStatusObserver;


# instance fields
.field final synthetic this$0:Lorg/chromium/sync/notifier/SyncStatusHelper;


# direct methods
.method private constructor <init>(Lorg/chromium/sync/notifier/SyncStatusHelper;)V
    .locals 0

    .prologue
    .line 395
    iput-object p1, p0, Lorg/chromium/sync/notifier/SyncStatusHelper$AndroidSyncSettingsChangedObserver;->this$0:Lorg/chromium/sync/notifier/SyncStatusHelper;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lorg/chromium/sync/notifier/SyncStatusHelper;Lorg/chromium/sync/notifier/SyncStatusHelper$1;)V
    .locals 0

    .prologue
    .line 395
    invoke-direct {p0, p1}, Lorg/chromium/sync/notifier/SyncStatusHelper$AndroidSyncSettingsChangedObserver;-><init>(Lorg/chromium/sync/notifier/SyncStatusHelper;)V

    return-void
.end method


# virtual methods
.method public onStatusChanged(I)V
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 398
    if-ne v0, p1, :cond_1

    .line 400
    iget-object v1, p0, Lorg/chromium/sync/notifier/SyncStatusHelper$AndroidSyncSettingsChangedObserver;->this$0:Lorg/chromium/sync/notifier/SyncStatusHelper;

    # getter for: Lorg/chromium/sync/notifier/SyncStatusHelper;->mCachedSettings:Lorg/chromium/sync/notifier/SyncStatusHelper$CachedAccountSyncSettings;
    invoke-static {v1}, Lorg/chromium/sync/notifier/SyncStatusHelper;->access$100(Lorg/chromium/sync/notifier/SyncStatusHelper;)Lorg/chromium/sync/notifier/SyncStatusHelper$CachedAccountSyncSettings;

    move-result-object v1

    monitor-enter v1

    .line 401
    :try_start_0
    iget-object v2, p0, Lorg/chromium/sync/notifier/SyncStatusHelper$AndroidSyncSettingsChangedObserver;->this$0:Lorg/chromium/sync/notifier/SyncStatusHelper;

    # getter for: Lorg/chromium/sync/notifier/SyncStatusHelper;->mCachedSettings:Lorg/chromium/sync/notifier/SyncStatusHelper$CachedAccountSyncSettings;
    invoke-static {v2}, Lorg/chromium/sync/notifier/SyncStatusHelper;->access$100(Lorg/chromium/sync/notifier/SyncStatusHelper;)Lorg/chromium/sync/notifier/SyncStatusHelper$CachedAccountSyncSettings;

    move-result-object v2

    iget-object v3, p0, Lorg/chromium/sync/notifier/SyncStatusHelper$AndroidSyncSettingsChangedObserver;->this$0:Lorg/chromium/sync/notifier/SyncStatusHelper;

    # getter for: Lorg/chromium/sync/notifier/SyncStatusHelper;->mApplicationContext:Landroid/content/Context;
    invoke-static {v3}, Lorg/chromium/sync/notifier/SyncStatusHelper;->access$200(Lorg/chromium/sync/notifier/SyncStatusHelper;)Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lorg/chromium/sync/signin/ChromeSigninController;->get(Landroid/content/Context;)Lorg/chromium/sync/signin/ChromeSigninController;

    move-result-object v3

    invoke-virtual {v3}, Lorg/chromium/sync/signin/ChromeSigninController;->getSignedInUser()Landroid/accounts/Account;

    move-result-object v3

    invoke-virtual {v2, v3}, Lorg/chromium/sync/notifier/SyncStatusHelper$CachedAccountSyncSettings;->updateSyncSettingsForAccount(Landroid/accounts/Account;)V

    .line 403
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 405
    iget-object v1, p0, Lorg/chromium/sync/notifier/SyncStatusHelper$AndroidSyncSettingsChangedObserver;->this$0:Lorg/chromium/sync/notifier/SyncStatusHelper;

    invoke-virtual {v1}, Lorg/chromium/sync/notifier/SyncStatusHelper;->isMasterSyncAutomaticallyEnabled()Z

    move-result v1

    .line 406
    iget-object v2, p0, Lorg/chromium/sync/notifier/SyncStatusHelper$AndroidSyncSettingsChangedObserver;->this$0:Lorg/chromium/sync/notifier/SyncStatusHelper;

    # invokes: Lorg/chromium/sync/notifier/SyncStatusHelper;->updateMasterSyncAutomaticallySetting()V
    invoke-static {v2}, Lorg/chromium/sync/notifier/SyncStatusHelper;->access$300(Lorg/chromium/sync/notifier/SyncStatusHelper;)V

    .line 407
    iget-object v2, p0, Lorg/chromium/sync/notifier/SyncStatusHelper$AndroidSyncSettingsChangedObserver;->this$0:Lorg/chromium/sync/notifier/SyncStatusHelper;

    invoke-virtual {v2}, Lorg/chromium/sync/notifier/SyncStatusHelper;->isMasterSyncAutomaticallyEnabled()Z

    move-result v2

    if-eq v1, v2, :cond_2

    .line 410
    :goto_0
    if-nez v0, :cond_0

    iget-object v0, p0, Lorg/chromium/sync/notifier/SyncStatusHelper$AndroidSyncSettingsChangedObserver;->this$0:Lorg/chromium/sync/notifier/SyncStatusHelper;

    # invokes: Lorg/chromium/sync/notifier/SyncStatusHelper;->getAndClearDidUpdateStatus()Z
    invoke-static {v0}, Lorg/chromium/sync/notifier/SyncStatusHelper;->access$400(Lorg/chromium/sync/notifier/SyncStatusHelper;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 411
    :cond_0
    iget-object v0, p0, Lorg/chromium/sync/notifier/SyncStatusHelper$AndroidSyncSettingsChangedObserver;->this$0:Lorg/chromium/sync/notifier/SyncStatusHelper;

    # invokes: Lorg/chromium/sync/notifier/SyncStatusHelper;->notifyObservers()V
    invoke-static {v0}, Lorg/chromium/sync/notifier/SyncStatusHelper;->access$500(Lorg/chromium/sync/notifier/SyncStatusHelper;)V

    .line 413
    :cond_1
    return-void

    .line 403
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 407
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method
