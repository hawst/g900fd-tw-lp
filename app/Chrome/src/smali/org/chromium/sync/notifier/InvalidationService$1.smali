.class Lorg/chromium/sync/notifier/InvalidationService$1;
.super Ljava/lang/Object;
.source "InvalidationService.java"

# interfaces
.implements Lorg/chromium/sync/signin/AccountManagerHelper$GetAuthTokenCallback;


# instance fields
.field final synthetic this$0:Lorg/chromium/sync/notifier/InvalidationService;

.field final synthetic val$pendingIntent:Landroid/app/PendingIntent;


# direct methods
.method constructor <init>(Lorg/chromium/sync/notifier/InvalidationService;Landroid/app/PendingIntent;)V
    .locals 0

    .prologue
    .line 206
    iput-object p1, p0, Lorg/chromium/sync/notifier/InvalidationService$1;->this$0:Lorg/chromium/sync/notifier/InvalidationService;

    iput-object p2, p0, Lorg/chromium/sync/notifier/InvalidationService$1;->val$pendingIntent:Landroid/app/PendingIntent;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public tokenAvailable(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 209
    if-eqz p1, :cond_0

    .line 210
    iget-object v0, p0, Lorg/chromium/sync/notifier/InvalidationService$1;->this$0:Lorg/chromium/sync/notifier/InvalidationService;

    invoke-virtual {v0}, Lorg/chromium/sync/notifier/InvalidationService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lorg/chromium/sync/notifier/InvalidationService$1;->val$pendingIntent:Landroid/app/PendingIntent;

    # invokes: Lorg/chromium/sync/notifier/InvalidationService;->getOAuth2ScopeWithType()Ljava/lang/String;
    invoke-static {}, Lorg/chromium/sync/notifier/InvalidationService;->access$000()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, p1, v2}, Lcom/google/ipc/invalidation/external/client/contrib/AndroidListener;->setAuthToken(Landroid/content/Context;Landroid/app/PendingIntent;Ljava/lang/String;Ljava/lang/String;)V

    .line 213
    :cond_0
    return-void
.end method
