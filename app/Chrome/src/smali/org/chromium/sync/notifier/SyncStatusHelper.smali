.class public Lorg/chromium/sync/notifier/SyncStatusHelper;
.super Ljava/lang/Object;
.source "SyncStatusHelper.java"


# static fields
.field public static final CHROME_SYNC_OAUTH2_SCOPE:Ljava/lang/String; = "https://www.googleapis.com/auth/chromesync"

.field private static final INSTANCE_LOCK:Ljava/lang/Object;

.field public static final TAG:Ljava/lang/String; = "SyncStatusHelper"

.field private static sSyncStatusHelper:Lorg/chromium/sync/notifier/SyncStatusHelper;


# instance fields
.field private final mApplicationContext:Landroid/content/Context;

.field private mCachedMasterSyncAutomatically:Z

.field private mCachedSettings:Lorg/chromium/sync/notifier/SyncStatusHelper$CachedAccountSyncSettings;

.field private final mContractAuthority:Ljava/lang/String;

.field private final mObservers:Lorg/chromium/base/ObserverList;

.field private final mSyncContentResolverDelegate:Lorg/chromium/sync/notifier/SyncContentResolverDelegate;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 143
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lorg/chromium/sync/notifier/SyncStatusHelper;->INSTANCE_LOCK:Ljava/lang/Object;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Lorg/chromium/sync/notifier/SyncContentResolverDelegate;Lorg/chromium/sync/notifier/SyncStatusHelper$CachedAccountSyncSettings;)V
    .locals 4

    .prologue
    .line 174
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 158
    new-instance v0, Lorg/chromium/base/ObserverList;

    invoke-direct {v0}, Lorg/chromium/base/ObserverList;-><init>()V

    iput-object v0, p0, Lorg/chromium/sync/notifier/SyncStatusHelper;->mObservers:Lorg/chromium/base/ObserverList;

    .line 175
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lorg/chromium/sync/notifier/SyncStatusHelper;->mApplicationContext:Landroid/content/Context;

    .line 176
    iput-object p2, p0, Lorg/chromium/sync/notifier/SyncStatusHelper;->mSyncContentResolverDelegate:Lorg/chromium/sync/notifier/SyncContentResolverDelegate;

    .line 177
    invoke-virtual {p0}, Lorg/chromium/sync/notifier/SyncStatusHelper;->getContractAuthority()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/chromium/sync/notifier/SyncStatusHelper;->mContractAuthority:Ljava/lang/String;

    .line 178
    iput-object p3, p0, Lorg/chromium/sync/notifier/SyncStatusHelper;->mCachedSettings:Lorg/chromium/sync/notifier/SyncStatusHelper$CachedAccountSyncSettings;

    .line 180
    invoke-direct {p0}, Lorg/chromium/sync/notifier/SyncStatusHelper;->updateMasterSyncAutomaticallySetting()V

    .line 182
    iget-object v0, p0, Lorg/chromium/sync/notifier/SyncStatusHelper;->mSyncContentResolverDelegate:Lorg/chromium/sync/notifier/SyncContentResolverDelegate;

    const/4 v1, 0x1

    new-instance v2, Lorg/chromium/sync/notifier/SyncStatusHelper$AndroidSyncSettingsChangedObserver;

    const/4 v3, 0x0

    invoke-direct {v2, p0, v3}, Lorg/chromium/sync/notifier/SyncStatusHelper$AndroidSyncSettingsChangedObserver;-><init>(Lorg/chromium/sync/notifier/SyncStatusHelper;Lorg/chromium/sync/notifier/SyncStatusHelper$1;)V

    invoke-interface {v0, v1, v2}, Lorg/chromium/sync/notifier/SyncContentResolverDelegate;->addStatusChangeListener(ILandroid/content/SyncStatusObserver;)Ljava/lang/Object;

    .line 185
    return-void
.end method

.method static synthetic access$100(Lorg/chromium/sync/notifier/SyncStatusHelper;)Lorg/chromium/sync/notifier/SyncStatusHelper$CachedAccountSyncSettings;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lorg/chromium/sync/notifier/SyncStatusHelper;->mCachedSettings:Lorg/chromium/sync/notifier/SyncStatusHelper$CachedAccountSyncSettings;

    return-object v0
.end method

.method static synthetic access$200(Lorg/chromium/sync/notifier/SyncStatusHelper;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lorg/chromium/sync/notifier/SyncStatusHelper;->mApplicationContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$300(Lorg/chromium/sync/notifier/SyncStatusHelper;)V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Lorg/chromium/sync/notifier/SyncStatusHelper;->updateMasterSyncAutomaticallySetting()V

    return-void
.end method

.method static synthetic access$400(Lorg/chromium/sync/notifier/SyncStatusHelper;)Z
    .locals 1

    .prologue
    .line 31
    invoke-direct {p0}, Lorg/chromium/sync/notifier/SyncStatusHelper;->getAndClearDidUpdateStatus()Z

    move-result v0

    return v0
.end method

.method static synthetic access$500(Lorg/chromium/sync/notifier/SyncStatusHelper;)V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Lorg/chromium/sync/notifier/SyncStatusHelper;->notifyObservers()V

    return-void
.end method

.method public static get(Landroid/content/Context;)Lorg/chromium/sync/notifier/SyncStatusHelper;
    .locals 4

    .prologue
    .line 207
    sget-object v1, Lorg/chromium/sync/notifier/SyncStatusHelper;->INSTANCE_LOCK:Ljava/lang/Object;

    monitor-enter v1

    .line 208
    :try_start_0
    sget-object v0, Lorg/chromium/sync/notifier/SyncStatusHelper;->sSyncStatusHelper:Lorg/chromium/sync/notifier/SyncStatusHelper;

    if-nez v0, :cond_0

    .line 209
    new-instance v0, Lorg/chromium/sync/notifier/SystemSyncContentResolverDelegate;

    invoke-direct {v0}, Lorg/chromium/sync/notifier/SystemSyncContentResolverDelegate;-><init>()V

    .line 211
    new-instance v2, Lorg/chromium/sync/notifier/SyncStatusHelper$CachedAccountSyncSettings;

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3, v0}, Lorg/chromium/sync/notifier/SyncStatusHelper$CachedAccountSyncSettings;-><init>(Ljava/lang/String;Lorg/chromium/sync/notifier/SyncContentResolverDelegate;)V

    .line 213
    new-instance v3, Lorg/chromium/sync/notifier/SyncStatusHelper;

    invoke-direct {v3, p0, v0, v2}, Lorg/chromium/sync/notifier/SyncStatusHelper;-><init>(Landroid/content/Context;Lorg/chromium/sync/notifier/SyncContentResolverDelegate;Lorg/chromium/sync/notifier/SyncStatusHelper$CachedAccountSyncSettings;)V

    sput-object v3, Lorg/chromium/sync/notifier/SyncStatusHelper;->sSyncStatusHelper:Lorg/chromium/sync/notifier/SyncStatusHelper;

    .line 215
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 216
    sget-object v0, Lorg/chromium/sync/notifier/SyncStatusHelper;->sSyncStatusHelper:Lorg/chromium/sync/notifier/SyncStatusHelper;

    return-object v0

    .line 215
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private getAndClearDidUpdateStatus()Z
    .locals 3

    .prologue
    .line 418
    iget-object v1, p0, Lorg/chromium/sync/notifier/SyncStatusHelper;->mCachedSettings:Lorg/chromium/sync/notifier/SyncStatusHelper$CachedAccountSyncSettings;

    monitor-enter v1

    .line 419
    :try_start_0
    iget-object v0, p0, Lorg/chromium/sync/notifier/SyncStatusHelper;->mCachedSettings:Lorg/chromium/sync/notifier/SyncStatusHelper$CachedAccountSyncSettings;

    invoke-virtual {v0}, Lorg/chromium/sync/notifier/SyncStatusHelper$CachedAccountSyncSettings;->getDidUpdateStatus()Z

    move-result v0

    .line 420
    iget-object v2, p0, Lorg/chromium/sync/notifier/SyncStatusHelper;->mCachedSettings:Lorg/chromium/sync/notifier/SyncStatusHelper$CachedAccountSyncSettings;

    invoke-virtual {v2}, Lorg/chromium/sync/notifier/SyncStatusHelper$CachedAccountSyncSettings;->clearUpdateStatus()V

    .line 421
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 422
    return v0

    .line 421
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private makeSyncable(Landroid/accounts/Account;)V
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 369
    iget-object v2, p0, Lorg/chromium/sync/notifier/SyncStatusHelper;->mCachedSettings:Lorg/chromium/sync/notifier/SyncStatusHelper$CachedAccountSyncSettings;

    monitor-enter v2

    .line 370
    :try_start_0
    iget-object v0, p0, Lorg/chromium/sync/notifier/SyncStatusHelper;->mCachedSettings:Lorg/chromium/sync/notifier/SyncStatusHelper$CachedAccountSyncSettings;

    invoke-virtual {v0, p1}, Lorg/chromium/sync/notifier/SyncStatusHelper$CachedAccountSyncSettings;->setIsSyncable(Landroid/accounts/Account;)V

    .line 371
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 373
    invoke-static {}, Landroid/os/StrictMode;->allowThreadDiskWrites()Landroid/os/StrictMode$ThreadPolicy;

    move-result-object v2

    .line 377
    iget-object v0, p0, Lorg/chromium/sync/notifier/SyncStatusHelper;->mApplicationContext:Landroid/content/Context;

    invoke-static {v0}, Lorg/chromium/sync/signin/AccountManagerHelper;->get(Landroid/content/Context;)Lorg/chromium/sync/signin/AccountManagerHelper;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/sync/signin/AccountManagerHelper;->getGoogleAccounts()[Landroid/accounts/Account;

    move-result-object v3

    .line 379
    array-length v4, v3

    move v0, v1

    :goto_0
    if-ge v0, v4, :cond_1

    aget-object v5, v3, v0

    .line 380
    invoke-virtual {v5, p1}, Landroid/accounts/Account;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_0

    iget-object v6, p0, Lorg/chromium/sync/notifier/SyncStatusHelper;->mSyncContentResolverDelegate:Lorg/chromium/sync/notifier/SyncContentResolverDelegate;

    iget-object v7, p0, Lorg/chromium/sync/notifier/SyncStatusHelper;->mContractAuthority:Ljava/lang/String;

    invoke-interface {v6, v5, v7}, Lorg/chromium/sync/notifier/SyncContentResolverDelegate;->getIsSyncable(Landroid/accounts/Account;Ljava/lang/String;)I

    move-result v6

    if-lez v6, :cond_0

    .line 383
    iget-object v6, p0, Lorg/chromium/sync/notifier/SyncStatusHelper;->mSyncContentResolverDelegate:Lorg/chromium/sync/notifier/SyncContentResolverDelegate;

    iget-object v7, p0, Lorg/chromium/sync/notifier/SyncStatusHelper;->mContractAuthority:Ljava/lang/String;

    invoke-interface {v6, v5, v7, v1}, Lorg/chromium/sync/notifier/SyncContentResolverDelegate;->setIsSyncable(Landroid/accounts/Account;Ljava/lang/String;I)V

    .line 379
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 371
    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0

    .line 387
    :cond_1
    invoke-static {v2}, Landroid/os/StrictMode;->setThreadPolicy(Landroid/os/StrictMode$ThreadPolicy;)V

    .line 388
    return-void
.end method

.method private notifyObservers()V
    .locals 2

    .prologue
    .line 432
    iget-object v0, p0, Lorg/chromium/sync/notifier/SyncStatusHelper;->mObservers:Lorg/chromium/base/ObserverList;

    invoke-virtual {v0}, Lorg/chromium/base/ObserverList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/chromium/sync/notifier/SyncStatusHelper$SyncSettingsChangedObserver;

    .line 433
    invoke-interface {v0}, Lorg/chromium/sync/notifier/SyncStatusHelper$SyncSettingsChangedObserver;->syncSettingsChanged()V

    goto :goto_0

    .line 435
    :cond_0
    return-void
.end method

.method private notifyObserversIfAccountSettingsChanged()V
    .locals 1

    .prologue
    .line 426
    invoke-direct {p0}, Lorg/chromium/sync/notifier/SyncStatusHelper;->getAndClearDidUpdateStatus()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 427
    invoke-direct {p0}, Lorg/chromium/sync/notifier/SyncStatusHelper;->notifyObservers()V

    .line 429
    :cond_0
    return-void
.end method

.method public static overrideSyncStatusHelperForTests(Landroid/content/Context;Lorg/chromium/sync/notifier/SyncContentResolverDelegate;)V
    .locals 2

    .prologue
    .line 242
    new-instance v0, Lorg/chromium/sync/notifier/SyncStatusHelper$CachedAccountSyncSettings;

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Lorg/chromium/sync/notifier/SyncStatusHelper$CachedAccountSyncSettings;-><init>(Ljava/lang/String;Lorg/chromium/sync/notifier/SyncContentResolverDelegate;)V

    .line 244
    invoke-static {p0, p1, v0}, Lorg/chromium/sync/notifier/SyncStatusHelper;->overrideSyncStatusHelperForTests(Landroid/content/Context;Lorg/chromium/sync/notifier/SyncContentResolverDelegate;Lorg/chromium/sync/notifier/SyncStatusHelper$CachedAccountSyncSettings;)V

    .line 246
    return-void
.end method

.method public static overrideSyncStatusHelperForTests(Landroid/content/Context;Lorg/chromium/sync/notifier/SyncContentResolverDelegate;Lorg/chromium/sync/notifier/SyncStatusHelper$CachedAccountSyncSettings;)V
    .locals 3

    .prologue
    .line 230
    sget-object v1, Lorg/chromium/sync/notifier/SyncStatusHelper;->INSTANCE_LOCK:Ljava/lang/Object;

    monitor-enter v1

    .line 231
    :try_start_0
    sget-object v0, Lorg/chromium/sync/notifier/SyncStatusHelper;->sSyncStatusHelper:Lorg/chromium/sync/notifier/SyncStatusHelper;

    if-eqz v0, :cond_0

    .line 232
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v2, "SyncStatusHelper already exists"

    invoke-direct {v0, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 236
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 234
    :cond_0
    :try_start_1
    new-instance v0, Lorg/chromium/sync/notifier/SyncStatusHelper;

    invoke-direct {v0, p0, p1, p2}, Lorg/chromium/sync/notifier/SyncStatusHelper;-><init>(Landroid/content/Context;Lorg/chromium/sync/notifier/SyncContentResolverDelegate;Lorg/chromium/sync/notifier/SyncStatusHelper$CachedAccountSyncSettings;)V

    sput-object v0, Lorg/chromium/sync/notifier/SyncStatusHelper;->sSyncStatusHelper:Lorg/chromium/sync/notifier/SyncStatusHelper;

    .line 236
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method private updateMasterSyncAutomaticallySetting()V
    .locals 3

    .prologue
    .line 188
    invoke-static {}, Landroid/os/StrictMode;->allowThreadDiskWrites()Landroid/os/StrictMode$ThreadPolicy;

    move-result-object v0

    .line 189
    iget-object v1, p0, Lorg/chromium/sync/notifier/SyncStatusHelper;->mCachedSettings:Lorg/chromium/sync/notifier/SyncStatusHelper$CachedAccountSyncSettings;

    monitor-enter v1

    .line 190
    :try_start_0
    iget-object v2, p0, Lorg/chromium/sync/notifier/SyncStatusHelper;->mSyncContentResolverDelegate:Lorg/chromium/sync/notifier/SyncContentResolverDelegate;

    invoke-interface {v2}, Lorg/chromium/sync/notifier/SyncContentResolverDelegate;->getMasterSyncAutomatically()Z

    move-result v2

    iput-boolean v2, p0, Lorg/chromium/sync/notifier/SyncStatusHelper;->mCachedMasterSyncAutomatically:Z

    .line 192
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 193
    invoke-static {v0}, Landroid/os/StrictMode;->setThreadPolicy(Landroid/os/StrictMode$ThreadPolicy;)V

    .line 194
    return-void

    .line 192
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public disableAndroidSync(Landroid/accounts/Account;)V
    .locals 3

    .prologue
    .line 355
    iget-object v1, p0, Lorg/chromium/sync/notifier/SyncStatusHelper;->mCachedSettings:Lorg/chromium/sync/notifier/SyncStatusHelper$CachedAccountSyncSettings;

    monitor-enter v1

    .line 356
    :try_start_0
    iget-object v0, p0, Lorg/chromium/sync/notifier/SyncStatusHelper;->mCachedSettings:Lorg/chromium/sync/notifier/SyncStatusHelper$CachedAccountSyncSettings;

    const/4 v2, 0x0

    invoke-virtual {v0, p1, v2}, Lorg/chromium/sync/notifier/SyncStatusHelper$CachedAccountSyncSettings;->setSyncAutomatically(Landroid/accounts/Account;Z)V

    .line 357
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 359
    invoke-direct {p0}, Lorg/chromium/sync/notifier/SyncStatusHelper;->notifyObserversIfAccountSettingsChanged()V

    .line 360
    return-void

    .line 357
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public enableAndroidSync(Landroid/accounts/Account;)V
    .locals 3

    .prologue
    .line 340
    invoke-direct {p0, p1}, Lorg/chromium/sync/notifier/SyncStatusHelper;->makeSyncable(Landroid/accounts/Account;)V

    .line 342
    iget-object v1, p0, Lorg/chromium/sync/notifier/SyncStatusHelper;->mCachedSettings:Lorg/chromium/sync/notifier/SyncStatusHelper$CachedAccountSyncSettings;

    monitor-enter v1

    .line 343
    :try_start_0
    iget-object v0, p0, Lorg/chromium/sync/notifier/SyncStatusHelper;->mCachedSettings:Lorg/chromium/sync/notifier/SyncStatusHelper$CachedAccountSyncSettings;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2}, Lorg/chromium/sync/notifier/SyncStatusHelper$CachedAccountSyncSettings;->setSyncAutomatically(Landroid/accounts/Account;Z)V

    .line 344
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 346
    invoke-direct {p0}, Lorg/chromium/sync/notifier/SyncStatusHelper;->notifyObserversIfAccountSettingsChanged()V

    .line 347
    return-void

    .line 344
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public getContractAuthority()Ljava/lang/String;
    .locals 1

    .prologue
    .line 252
    iget-object v0, p0, Lorg/chromium/sync/notifier/SyncStatusHelper;->mApplicationContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public isMasterSyncAutomaticallyEnabled()Z
    .locals 2

    .prologue
    .line 329
    iget-object v1, p0, Lorg/chromium/sync/notifier/SyncStatusHelper;->mCachedSettings:Lorg/chromium/sync/notifier/SyncStatusHelper$CachedAccountSyncSettings;

    monitor-enter v1

    .line 330
    :try_start_0
    iget-boolean v0, p0, Lorg/chromium/sync/notifier/SyncStatusHelper;->mCachedMasterSyncAutomatically:Z

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v0

    .line 331
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public isSyncEnabled()Z
    .locals 1

    .prologue
    .line 299
    iget-object v0, p0, Lorg/chromium/sync/notifier/SyncStatusHelper;->mApplicationContext:Landroid/content/Context;

    invoke-static {v0}, Lorg/chromium/sync/signin/ChromeSigninController;->get(Landroid/content/Context;)Lorg/chromium/sync/signin/ChromeSigninController;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/sync/signin/ChromeSigninController;->getSignedInUser()Landroid/accounts/Account;

    move-result-object v0

    invoke-virtual {p0, v0}, Lorg/chromium/sync/notifier/SyncStatusHelper;->isSyncEnabled(Landroid/accounts/Account;)Z

    move-result v0

    return v0
.end method

.method public isSyncEnabled(Landroid/accounts/Account;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 279
    if-nez p1, :cond_0

    .line 287
    :goto_0
    return v0

    .line 281
    :cond_0
    iget-object v1, p0, Lorg/chromium/sync/notifier/SyncStatusHelper;->mCachedSettings:Lorg/chromium/sync/notifier/SyncStatusHelper$CachedAccountSyncSettings;

    monitor-enter v1

    .line 282
    :try_start_0
    iget-boolean v2, p0, Lorg/chromium/sync/notifier/SyncStatusHelper;->mCachedMasterSyncAutomatically:Z

    if-eqz v2, :cond_1

    iget-object v2, p0, Lorg/chromium/sync/notifier/SyncStatusHelper;->mCachedSettings:Lorg/chromium/sync/notifier/SyncStatusHelper$CachedAccountSyncSettings;

    invoke-virtual {v2, p1}, Lorg/chromium/sync/notifier/SyncStatusHelper$CachedAccountSyncSettings;->getSyncAutomatically(Landroid/accounts/Account;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v0, 0x1

    .line 284
    :cond_1
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 286
    invoke-direct {p0}, Lorg/chromium/sync/notifier/SyncStatusHelper;->notifyObserversIfAccountSettingsChanged()V

    goto :goto_0

    .line 284
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public isSyncEnabledForChrome(Landroid/accounts/Account;)Z
    .locals 2

    .prologue
    .line 312
    if-nez p1, :cond_0

    const/4 v0, 0x0

    .line 320
    :goto_0
    return v0

    .line 315
    :cond_0
    iget-object v1, p0, Lorg/chromium/sync/notifier/SyncStatusHelper;->mCachedSettings:Lorg/chromium/sync/notifier/SyncStatusHelper$CachedAccountSyncSettings;

    monitor-enter v1

    .line 316
    :try_start_0
    iget-object v0, p0, Lorg/chromium/sync/notifier/SyncStatusHelper;->mCachedSettings:Lorg/chromium/sync/notifier/SyncStatusHelper$CachedAccountSyncSettings;

    invoke-virtual {v0, p1}, Lorg/chromium/sync/notifier/SyncStatusHelper$CachedAccountSyncSettings;->getSyncAutomatically(Landroid/accounts/Account;)Z

    move-result v0

    .line 317
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 319
    invoke-direct {p0}, Lorg/chromium/sync/notifier/SyncStatusHelper;->notifyObserversIfAccountSettingsChanged()V

    goto :goto_0

    .line 317
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public registerSyncSettingsChangedObserver(Lorg/chromium/sync/notifier/SyncStatusHelper$SyncSettingsChangedObserver;)V
    .locals 1

    .prologue
    .line 260
    iget-object v0, p0, Lorg/chromium/sync/notifier/SyncStatusHelper;->mObservers:Lorg/chromium/base/ObserverList;

    invoke-virtual {v0, p1}, Lorg/chromium/base/ObserverList;->addObserver(Ljava/lang/Object;)Z

    .line 261
    return-void
.end method

.method public unregisterSyncSettingsChangedObserver(Lorg/chromium/sync/notifier/SyncStatusHelper$SyncSettingsChangedObserver;)V
    .locals 1

    .prologue
    .line 267
    iget-object v0, p0, Lorg/chromium/sync/notifier/SyncStatusHelper;->mObservers:Lorg/chromium/base/ObserverList;

    invoke-virtual {v0, p1}, Lorg/chromium/base/ObserverList;->removeObserver(Ljava/lang/Object;)Z

    .line 268
    return-void
.end method
