.class public Lorg/chromium/sync/notifier/InvalidationPreferences;
.super Ljava/lang/Object;
.source "InvalidationPreferences.java"


# instance fields
.field private final mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 81
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 82
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 83
    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    const-string/jumbo v1, "Unable to get application context"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 84
    :cond_0
    iput-object v0, p0, Lorg/chromium/sync/notifier/InvalidationPreferences;->mContext:Landroid/content/Context;

    .line 85
    return-void
.end method

.method static synthetic access$000(Lorg/chromium/sync/notifier/InvalidationPreferences;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lorg/chromium/sync/notifier/InvalidationPreferences;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method private getObjectId(Ljava/lang/String;)Lcom/google/ipc/invalidation/external/client/types/ObjectId;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 190
    const/16 v1, 0x3a

    invoke-virtual {p1, v1}, Ljava/lang/String;->indexOf(I)I

    move-result v1

    .line 192
    if-lez v1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    if-ne v1, v2, :cond_1

    .line 202
    :cond_0
    :goto_0
    return-object v0

    .line 197
    :cond_1
    const/4 v2, 0x0

    :try_start_0
    invoke-virtual {p1, v2, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 201
    add-int/lit8 v1, v1, 0x1

    invoke-virtual {p1, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->getBytes()[B

    move-result-object v1

    .line 202
    invoke-static {v0, v1}, Lcom/google/ipc/invalidation/external/client/types/ObjectId;->newInstance(I[B)Lcom/google/ipc/invalidation/external/client/types/ObjectId;

    move-result-object v0

    goto :goto_0

    .line 199
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method private getObjectIdString(Lcom/google/ipc/invalidation/external/client/types/ObjectId;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 182
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Lcom/google/ipc/invalidation/external/client/types/ObjectId;->getSource()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    new-instance v1, Ljava/lang/String;

    invoke-virtual {p1}, Lcom/google/ipc/invalidation/external/client/types/ObjectId;->getName()[B

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/String;-><init>([B)V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public commit(Lorg/chromium/sync/notifier/InvalidationPreferences$EditContext;)Z
    .locals 2

    .prologue
    .line 99
    # getter for: Lorg/chromium/sync/notifier/InvalidationPreferences$EditContext;->mEditor:Landroid/content/SharedPreferences$Editor;
    invoke-static {p1}, Lorg/chromium/sync/notifier/InvalidationPreferences$EditContext;->access$100(Lorg/chromium/sync/notifier/InvalidationPreferences$EditContext;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    move-result v0

    if-nez v0, :cond_0

    .line 100
    const-string/jumbo v0, "InvalidationPreferences"

    const-string/jumbo v1, "Failed to commit invalidation preferences"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 101
    const/4 v0, 0x0

    .line 103
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public edit()Lorg/chromium/sync/notifier/InvalidationPreferences$EditContext;
    .locals 1

    .prologue
    .line 89
    new-instance v0, Lorg/chromium/sync/notifier/InvalidationPreferences$EditContext;

    invoke-direct {v0, p0}, Lorg/chromium/sync/notifier/InvalidationPreferences$EditContext;-><init>(Lorg/chromium/sync/notifier/InvalidationPreferences;)V

    return-object v0
.end method

.method public getInternalNotificationClientState()[B
    .locals 3
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 166
    iget-object v1, p0, Lorg/chromium/sync/notifier/InvalidationPreferences;->mContext:Landroid/content/Context;

    invoke-static {v1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 167
    const-string/jumbo v2, "sync_tango_internal_state"

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 168
    if-nez v1, :cond_0

    .line 171
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    invoke-static {v1, v0}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v0

    goto :goto_0
.end method

.method public getSavedObjectIds()Ljava/util/Set;
    .locals 3
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 122
    iget-object v1, p0, Lorg/chromium/sync/notifier/InvalidationPreferences;->mContext:Landroid/content/Context;

    invoke-static {v1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 123
    const-string/jumbo v2, "tango_object_ids"

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences;->getStringSet(Ljava/lang/String;Ljava/util/Set;)Ljava/util/Set;

    move-result-object v2

    .line 124
    if-nez v2, :cond_0

    .line 134
    :goto_0
    return-object v0

    .line 127
    :cond_0
    new-instance v1, Ljava/util/HashSet;

    invoke-interface {v2}, Ljava/util/Set;->size()I

    move-result v0

    invoke-direct {v1, v0}, Ljava/util/HashSet;-><init>(I)V

    .line 128
    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 129
    invoke-direct {p0, v0}, Lorg/chromium/sync/notifier/InvalidationPreferences;->getObjectId(Ljava/lang/String;)Lcom/google/ipc/invalidation/external/client/types/ObjectId;

    move-result-object v0

    .line 130
    if-eqz v0, :cond_1

    .line 131
    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_2
    move-object v0, v1

    .line 134
    goto :goto_0
.end method

.method public getSavedSyncedAccount()Landroid/accounts/Account;
    .locals 4
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 149
    iget-object v1, p0, Lorg/chromium/sync/notifier/InvalidationPreferences;->mContext:Landroid/content/Context;

    invoke-static {v1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 150
    const-string/jumbo v2, "sync_acct_name"

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 151
    const-string/jumbo v3, "sync_acct_type"

    invoke-interface {v1, v3, v0}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 152
    if-eqz v2, :cond_0

    if-nez v1, :cond_1

    .line 155
    :cond_0
    :goto_0
    return-object v0

    :cond_1
    new-instance v0, Landroid/accounts/Account;

    invoke-direct {v0, v2, v1}, Landroid/accounts/Account;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public getSavedSyncedTypes()Ljava/util/Set;
    .locals 3
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 108
    iget-object v0, p0, Lorg/chromium/sync/notifier/InvalidationPreferences;->mContext:Landroid/content/Context;

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 109
    const-string/jumbo v1, "sync_tango_types"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getStringSet(Ljava/lang/String;Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public setAccount(Lorg/chromium/sync/notifier/InvalidationPreferences$EditContext;Landroid/accounts/Account;)V
    .locals 3

    .prologue
    .line 160
    # getter for: Lorg/chromium/sync/notifier/InvalidationPreferences$EditContext;->mEditor:Landroid/content/SharedPreferences$Editor;
    invoke-static {p1}, Lorg/chromium/sync/notifier/InvalidationPreferences$EditContext;->access$100(Lorg/chromium/sync/notifier/InvalidationPreferences$EditContext;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string/jumbo v1, "sync_acct_name"

    iget-object v2, p2, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 161
    # getter for: Lorg/chromium/sync/notifier/InvalidationPreferences$EditContext;->mEditor:Landroid/content/SharedPreferences$Editor;
    invoke-static {p1}, Lorg/chromium/sync/notifier/InvalidationPreferences$EditContext;->access$100(Lorg/chromium/sync/notifier/InvalidationPreferences$EditContext;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string/jumbo v1, "sync_acct_type"

    iget-object v2, p2, Landroid/accounts/Account;->type:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 162
    return-void
.end method

.method public setInternalNotificationClientState(Lorg/chromium/sync/notifier/InvalidationPreferences$EditContext;[B)V
    .locals 3

    .prologue
    .line 176
    # getter for: Lorg/chromium/sync/notifier/InvalidationPreferences$EditContext;->mEditor:Landroid/content/SharedPreferences$Editor;
    invoke-static {p1}, Lorg/chromium/sync/notifier/InvalidationPreferences$EditContext;->access$100(Lorg/chromium/sync/notifier/InvalidationPreferences$EditContext;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string/jumbo v1, "sync_tango_internal_state"

    const/4 v2, 0x0

    invoke-static {p2, v2}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 178
    return-void
.end method

.method public setObjectIds(Lorg/chromium/sync/notifier/InvalidationPreferences$EditContext;Ljava/util/Collection;)V
    .locals 3

    .prologue
    .line 139
    if-nez p2, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    const-string/jumbo v1, "objectIds is null."

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 140
    :cond_0
    new-instance v1, Ljava/util/HashSet;

    invoke-interface {p2}, Ljava/util/Collection;->size()I

    move-result v0

    invoke-direct {v1, v0}, Ljava/util/HashSet;-><init>(I)V

    .line 141
    invoke-interface {p2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/ipc/invalidation/external/client/types/ObjectId;

    .line 142
    invoke-direct {p0, v0}, Lorg/chromium/sync/notifier/InvalidationPreferences;->getObjectIdString(Lcom/google/ipc/invalidation/external/client/types/ObjectId;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 144
    :cond_1
    # getter for: Lorg/chromium/sync/notifier/InvalidationPreferences$EditContext;->mEditor:Landroid/content/SharedPreferences$Editor;
    invoke-static {p1}, Lorg/chromium/sync/notifier/InvalidationPreferences$EditContext;->access$100(Lorg/chromium/sync/notifier/InvalidationPreferences$EditContext;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string/jumbo v2, "tango_object_ids"

    invoke-interface {v0, v2, v1}, Landroid/content/SharedPreferences$Editor;->putStringSet(Ljava/lang/String;Ljava/util/Set;)Landroid/content/SharedPreferences$Editor;

    .line 145
    return-void
.end method

.method public setSyncTypes(Lorg/chromium/sync/notifier/InvalidationPreferences$EditContext;Ljava/util/Collection;)V
    .locals 3

    .prologue
    .line 114
    if-nez p2, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    const-string/jumbo v1, "syncTypes is null."

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 115
    :cond_0
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0, p2}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    .line 116
    # getter for: Lorg/chromium/sync/notifier/InvalidationPreferences$EditContext;->mEditor:Landroid/content/SharedPreferences$Editor;
    invoke-static {p1}, Lorg/chromium/sync/notifier/InvalidationPreferences$EditContext;->access$100(Lorg/chromium/sync/notifier/InvalidationPreferences$EditContext;)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string/jumbo v2, "sync_tango_types"

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences$Editor;->putStringSet(Ljava/lang/String;Ljava/util/Set;)Landroid/content/SharedPreferences$Editor;

    .line 117
    return-void
.end method
