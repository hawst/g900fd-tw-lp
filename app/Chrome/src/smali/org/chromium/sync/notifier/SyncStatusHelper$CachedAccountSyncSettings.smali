.class public Lorg/chromium/sync/notifier/SyncStatusHelper$CachedAccountSyncSettings;
.super Ljava/lang/Object;
.source "SyncStatusHelper.java"


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private mAccount:Landroid/accounts/Account;

.field private final mContractAuthority:Ljava/lang/String;

.field private mDidUpdate:Z

.field private mIsSyncable:I

.field private mSyncAutomatically:Z

.field private final mSyncContentResolverDelegate:Lorg/chromium/sync/notifier/SyncContentResolverDelegate;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 40
    const-class v0, Lorg/chromium/sync/notifier/SyncStatusHelper;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/chromium/sync/notifier/SyncStatusHelper$CachedAccountSyncSettings;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Ljava/lang/String;Lorg/chromium/sync/notifier/SyncContentResolverDelegate;)V
    .locals 0

    .prologue
    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    iput-object p1, p0, Lorg/chromium/sync/notifier/SyncStatusHelper$CachedAccountSyncSettings;->mContractAuthority:Ljava/lang/String;

    .line 53
    iput-object p2, p0, Lorg/chromium/sync/notifier/SyncStatusHelper$CachedAccountSyncSettings;->mSyncContentResolverDelegate:Lorg/chromium/sync/notifier/SyncContentResolverDelegate;

    .line 54
    return-void
.end method

.method private ensureSettingsAreForAccount(Landroid/accounts/Account;)V
    .locals 1

    .prologue
    .line 57
    sget-boolean v0, Lorg/chromium/sync/notifier/SyncStatusHelper$CachedAccountSyncSettings;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 58
    :cond_0
    iget-object v0, p0, Lorg/chromium/sync/notifier/SyncStatusHelper$CachedAccountSyncSettings;->mAccount:Landroid/accounts/Account;

    invoke-virtual {p1, v0}, Landroid/accounts/Account;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 61
    :goto_0
    return-void

    .line 59
    :cond_1
    invoke-virtual {p0, p1}, Lorg/chromium/sync/notifier/SyncStatusHelper$CachedAccountSyncSettings;->updateSyncSettingsForAccount(Landroid/accounts/Account;)V

    .line 60
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/chromium/sync/notifier/SyncStatusHelper$CachedAccountSyncSettings;->mDidUpdate:Z

    goto :goto_0
.end method


# virtual methods
.method public clearUpdateStatus()V
    .locals 1

    .prologue
    .line 64
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/chromium/sync/notifier/SyncStatusHelper$CachedAccountSyncSettings;->mDidUpdate:Z

    .line 65
    return-void
.end method

.method public getDidUpdateStatus()Z
    .locals 1

    .prologue
    .line 68
    iget-boolean v0, p0, Lorg/chromium/sync/notifier/SyncStatusHelper$CachedAccountSyncSettings;->mDidUpdate:Z

    return v0
.end method

.method public getSyncAutomatically(Landroid/accounts/Account;)Z
    .locals 1

    .prologue
    .line 73
    invoke-direct {p0, p1}, Lorg/chromium/sync/notifier/SyncStatusHelper$CachedAccountSyncSettings;->ensureSettingsAreForAccount(Landroid/accounts/Account;)V

    .line 74
    iget-boolean v0, p0, Lorg/chromium/sync/notifier/SyncStatusHelper$CachedAccountSyncSettings;->mSyncAutomatically:Z

    return v0
.end method

.method public setIsSyncable(Landroid/accounts/Account;)V
    .locals 2

    .prologue
    .line 83
    invoke-direct {p0, p1}, Lorg/chromium/sync/notifier/SyncStatusHelper$CachedAccountSyncSettings;->ensureSettingsAreForAccount(Landroid/accounts/Account;)V

    .line 84
    iget v0, p0, Lorg/chromium/sync/notifier/SyncStatusHelper$CachedAccountSyncSettings;->mIsSyncable:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 86
    :goto_0
    return-void

    .line 85
    :cond_0
    invoke-virtual {p0, p1}, Lorg/chromium/sync/notifier/SyncStatusHelper$CachedAccountSyncSettings;->setIsSyncableInternal(Landroid/accounts/Account;)V

    goto :goto_0
.end method

.method protected setIsSyncableInternal(Landroid/accounts/Account;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 117
    iput v3, p0, Lorg/chromium/sync/notifier/SyncStatusHelper$CachedAccountSyncSettings;->mIsSyncable:I

    .line 118
    invoke-static {}, Landroid/os/StrictMode;->allowThreadDiskWrites()Landroid/os/StrictMode$ThreadPolicy;

    move-result-object v0

    .line 119
    iget-object v1, p0, Lorg/chromium/sync/notifier/SyncStatusHelper$CachedAccountSyncSettings;->mSyncContentResolverDelegate:Lorg/chromium/sync/notifier/SyncContentResolverDelegate;

    iget-object v2, p0, Lorg/chromium/sync/notifier/SyncStatusHelper$CachedAccountSyncSettings;->mContractAuthority:Ljava/lang/String;

    invoke-interface {v1, p1, v2, v3}, Lorg/chromium/sync/notifier/SyncContentResolverDelegate;->setIsSyncable(Landroid/accounts/Account;Ljava/lang/String;I)V

    .line 120
    invoke-static {v0}, Landroid/os/StrictMode;->setThreadPolicy(Landroid/os/StrictMode$ThreadPolicy;)V

    .line 121
    iput-boolean v3, p0, Lorg/chromium/sync/notifier/SyncStatusHelper$CachedAccountSyncSettings;->mDidUpdate:Z

    .line 122
    return-void
.end method

.method public setSyncAutomatically(Landroid/accounts/Account;Z)V
    .locals 1

    .prologue
    .line 89
    invoke-direct {p0, p1}, Lorg/chromium/sync/notifier/SyncStatusHelper$CachedAccountSyncSettings;->ensureSettingsAreForAccount(Landroid/accounts/Account;)V

    .line 90
    iget-boolean v0, p0, Lorg/chromium/sync/notifier/SyncStatusHelper$CachedAccountSyncSettings;->mSyncAutomatically:Z

    if-ne v0, p2, :cond_0

    .line 92
    :goto_0
    return-void

    .line 91
    :cond_0
    invoke-virtual {p0, p1, p2}, Lorg/chromium/sync/notifier/SyncStatusHelper$CachedAccountSyncSettings;->setSyncAutomaticallyInternal(Landroid/accounts/Account;Z)V

    goto :goto_0
.end method

.method protected setSyncAutomaticallyInternal(Landroid/accounts/Account;Z)V
    .locals 3

    .prologue
    .line 126
    iput-boolean p2, p0, Lorg/chromium/sync/notifier/SyncStatusHelper$CachedAccountSyncSettings;->mSyncAutomatically:Z

    .line 127
    invoke-static {}, Landroid/os/StrictMode;->allowThreadDiskWrites()Landroid/os/StrictMode$ThreadPolicy;

    move-result-object v0

    .line 128
    iget-object v1, p0, Lorg/chromium/sync/notifier/SyncStatusHelper$CachedAccountSyncSettings;->mSyncContentResolverDelegate:Lorg/chromium/sync/notifier/SyncContentResolverDelegate;

    iget-object v2, p0, Lorg/chromium/sync/notifier/SyncStatusHelper$CachedAccountSyncSettings;->mContractAuthority:Ljava/lang/String;

    invoke-interface {v1, p1, v2, p2}, Lorg/chromium/sync/notifier/SyncContentResolverDelegate;->setSyncAutomatically(Landroid/accounts/Account;Ljava/lang/String;Z)V

    .line 129
    invoke-static {v0}, Landroid/os/StrictMode;->setThreadPolicy(Landroid/os/StrictMode$ThreadPolicy;)V

    .line 130
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/chromium/sync/notifier/SyncStatusHelper$CachedAccountSyncSettings;->mDidUpdate:Z

    .line 131
    return-void
.end method

.method public updateSyncSettingsForAccount(Landroid/accounts/Account;)V
    .locals 0

    .prologue
    .line 78
    if-nez p1, :cond_0

    .line 80
    :goto_0
    return-void

    .line 79
    :cond_0
    invoke-virtual {p0, p1}, Lorg/chromium/sync/notifier/SyncStatusHelper$CachedAccountSyncSettings;->updateSyncSettingsForAccountInternal(Landroid/accounts/Account;)V

    goto :goto_0
.end method

.method protected updateSyncSettingsForAccountInternal(Landroid/accounts/Account;)V
    .locals 6

    .prologue
    .line 97
    if-nez p1, :cond_0

    .line 113
    :goto_0
    return-void

    .line 99
    :cond_0
    iget-boolean v0, p0, Lorg/chromium/sync/notifier/SyncStatusHelper$CachedAccountSyncSettings;->mSyncAutomatically:Z

    .line 100
    iget v1, p0, Lorg/chromium/sync/notifier/SyncStatusHelper$CachedAccountSyncSettings;->mIsSyncable:I

    .line 101
    iget-object v2, p0, Lorg/chromium/sync/notifier/SyncStatusHelper$CachedAccountSyncSettings;->mAccount:Landroid/accounts/Account;

    .line 103
    iput-object p1, p0, Lorg/chromium/sync/notifier/SyncStatusHelper$CachedAccountSyncSettings;->mAccount:Landroid/accounts/Account;

    .line 105
    invoke-static {}, Landroid/os/StrictMode;->allowThreadDiskWrites()Landroid/os/StrictMode$ThreadPolicy;

    move-result-object v3

    .line 106
    iget-object v4, p0, Lorg/chromium/sync/notifier/SyncStatusHelper$CachedAccountSyncSettings;->mSyncContentResolverDelegate:Lorg/chromium/sync/notifier/SyncContentResolverDelegate;

    iget-object v5, p0, Lorg/chromium/sync/notifier/SyncStatusHelper$CachedAccountSyncSettings;->mContractAuthority:Ljava/lang/String;

    invoke-interface {v4, p1, v5}, Lorg/chromium/sync/notifier/SyncContentResolverDelegate;->getSyncAutomatically(Landroid/accounts/Account;Ljava/lang/String;)Z

    move-result v4

    iput-boolean v4, p0, Lorg/chromium/sync/notifier/SyncStatusHelper$CachedAccountSyncSettings;->mSyncAutomatically:Z

    .line 108
    iget-object v4, p0, Lorg/chromium/sync/notifier/SyncStatusHelper$CachedAccountSyncSettings;->mSyncContentResolverDelegate:Lorg/chromium/sync/notifier/SyncContentResolverDelegate;

    iget-object v5, p0, Lorg/chromium/sync/notifier/SyncStatusHelper$CachedAccountSyncSettings;->mContractAuthority:Ljava/lang/String;

    invoke-interface {v4, p1, v5}, Lorg/chromium/sync/notifier/SyncContentResolverDelegate;->getIsSyncable(Landroid/accounts/Account;Ljava/lang/String;)I

    move-result v4

    iput v4, p0, Lorg/chromium/sync/notifier/SyncStatusHelper$CachedAccountSyncSettings;->mIsSyncable:I

    .line 109
    invoke-static {v3}, Landroid/os/StrictMode;->setThreadPolicy(Landroid/os/StrictMode$ThreadPolicy;)V

    .line 110
    iget v3, p0, Lorg/chromium/sync/notifier/SyncStatusHelper$CachedAccountSyncSettings;->mIsSyncable:I

    if-ne v1, v3, :cond_1

    iget-boolean v1, p0, Lorg/chromium/sync/notifier/SyncStatusHelper$CachedAccountSyncSettings;->mSyncAutomatically:Z

    if-ne v0, v1, :cond_1

    invoke-virtual {p1, v2}, Landroid/accounts/Account;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_1
    iput-boolean v0, p0, Lorg/chromium/sync/notifier/SyncStatusHelper$CachedAccountSyncSettings;->mDidUpdate:Z

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method
