.class Lorg/chromium/base/test/util/TestThread$1;
.super Ljava/lang/Object;
.source "TestThread.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic this$0:Lorg/chromium/base/test/util/TestThread;


# direct methods
.method constructor <init>(Lorg/chromium/base/test/util/TestThread;)V
    .locals 0

    .prologue
    .line 62
    iput-object p1, p0, Lorg/chromium/base/test/util/TestThread$1;->this$0:Lorg/chromium/base/test/util/TestThread;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 65
    iget-object v0, p0, Lorg/chromium/base/test/util/TestThread$1;->this$0:Lorg/chromium/base/test/util/TestThread;

    # getter for: Lorg/chromium/base/test/util/TestThread;->mThreadReadyLock:Ljava/lang/Object;
    invoke-static {v0}, Lorg/chromium/base/test/util/TestThread;->access$000(Lorg/chromium/base/test/util/TestThread;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 66
    :try_start_0
    iget-object v0, p0, Lorg/chromium/base/test/util/TestThread$1;->this$0:Lorg/chromium/base/test/util/TestThread;

    # getter for: Lorg/chromium/base/test/util/TestThread;->mThreadReady:Ljava/util/concurrent/atomic/AtomicBoolean;
    invoke-static {v0}, Lorg/chromium/base/test/util/TestThread;->access$100(Lorg/chromium/base/test/util/TestThread;)Ljava/util/concurrent/atomic/AtomicBoolean;

    move-result-object v0

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 67
    iget-object v0, p0, Lorg/chromium/base/test/util/TestThread$1;->this$0:Lorg/chromium/base/test/util/TestThread;

    # getter for: Lorg/chromium/base/test/util/TestThread;->mThreadReadyLock:Ljava/lang/Object;
    invoke-static {v0}, Lorg/chromium/base/test/util/TestThread;->access$000(Lorg/chromium/base/test/util/TestThread;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->notify()V

    .line 68
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
