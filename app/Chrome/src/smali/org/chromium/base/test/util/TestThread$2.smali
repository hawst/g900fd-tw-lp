.class Lorg/chromium/base/test/util/TestThread$2;
.super Ljava/lang/Object;
.source "TestThread.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic this$0:Lorg/chromium/base/test/util/TestThread;

.field final synthetic val$lock:Ljava/lang/Object;

.field final synthetic val$r:Ljava/lang/Runnable;

.field final synthetic val$taskExecuted:Ljava/util/concurrent/atomic/AtomicBoolean;


# direct methods
.method constructor <init>(Lorg/chromium/base/test/util/TestThread;Ljava/lang/Runnable;Ljava/lang/Object;Ljava/util/concurrent/atomic/AtomicBoolean;)V
    .locals 0

    .prologue
    .line 119
    iput-object p1, p0, Lorg/chromium/base/test/util/TestThread$2;->this$0:Lorg/chromium/base/test/util/TestThread;

    iput-object p2, p0, Lorg/chromium/base/test/util/TestThread$2;->val$r:Ljava/lang/Runnable;

    iput-object p3, p0, Lorg/chromium/base/test/util/TestThread$2;->val$lock:Ljava/lang/Object;

    iput-object p4, p0, Lorg/chromium/base/test/util/TestThread$2;->val$taskExecuted:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 122
    iget-object v0, p0, Lorg/chromium/base/test/util/TestThread$2;->val$r:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/chromium/base/test/util/TestThread$2;->val$r:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 123
    :cond_0
    iget-object v1, p0, Lorg/chromium/base/test/util/TestThread$2;->val$lock:Ljava/lang/Object;

    monitor-enter v1

    .line 124
    :try_start_0
    iget-object v0, p0, Lorg/chromium/base/test/util/TestThread$2;->val$taskExecuted:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 125
    iget-object v0, p0, Lorg/chromium/base/test/util/TestThread$2;->val$lock:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->notify()V

    .line 126
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
