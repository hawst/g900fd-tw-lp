.class public interface abstract annotation Lorg/chromium/base/test/util/Restriction;
.super Ljava/lang/Object;
.source "Restriction.java"

# interfaces
.implements Ljava/lang/annotation/Annotation;


# annotations
.annotation runtime Ljava/lang/annotation/Retention;
    value = .enum Ljava/lang/annotation/RetentionPolicy;->RUNTIME:Ljava/lang/annotation/RetentionPolicy;
.end annotation

.annotation runtime Ljava/lang/annotation/Target;
    value = {
        .enum Ljava/lang/annotation/ElementType;->METHOD:Ljava/lang/annotation/ElementType;
    }
.end annotation


# static fields
.field public static final RESTRICTION_TYPE_LOW_END_DEVICE:Ljava/lang/String; = "Low_End_Device"

.field public static final RESTRICTION_TYPE_NON_LOW_END_DEVICE:Ljava/lang/String; = "Non_Low_End_Device"

.field public static final RESTRICTION_TYPE_PHONE:Ljava/lang/String; = "Phone"

.field public static final RESTRICTION_TYPE_TABLET:Ljava/lang/String; = "Tablet"


# virtual methods
.method public abstract value()[Ljava/lang/String;
.end method
