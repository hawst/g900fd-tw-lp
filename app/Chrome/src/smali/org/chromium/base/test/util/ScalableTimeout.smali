.class public Lorg/chromium/base/test/util/ScalableTimeout;
.super Ljava/lang/Object;
.source "ScalableTimeout.java"


# static fields
.field private static final PROPERTY_FILE:Ljava/lang/String; = "/data/local/tmp/chrome_timeout_scale"

.field private static sTimeoutScale:Ljava/lang/Double;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 13
    const/4 v0, 0x0

    sput-object v0, Lorg/chromium/base/test/util/ScalableTimeout;->sTimeoutScale:Ljava/lang/Double;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static scaleTimeout(J)J
    .locals 4

    .prologue
    .line 17
    sget-object v0, Lorg/chromium/base/test/util/ScalableTimeout;->sTimeoutScale:Ljava/lang/Double;

    if-nez v0, :cond_0

    .line 19
    :try_start_0
    const-string/jumbo v0, "/data/local/tmp/chrome_timeout_scale"

    const/16 v1, 0x20

    invoke-static {v0, v1}, Lorg/chromium/base/test/util/TestFileUtil;->readUtf8File(Ljava/lang/String;I)[C

    move-result-object v0

    .line 20
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>([C)V

    invoke-static {v1}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    sput-object v0, Lorg/chromium/base/test/util/ScalableTimeout;->sTimeoutScale:Ljava/lang/Double;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 26
    :cond_0
    :goto_0
    long-to-double v0, p0

    sget-object v2, Lorg/chromium/base/test/util/ScalableTimeout;->sTimeoutScale:Ljava/lang/Double;

    invoke-virtual {v2}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v2

    mul-double/2addr v0, v2

    double-to-long v0, v0

    return-wide v0

    .line 23
    :catch_0
    move-exception v0

    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    sput-object v0, Lorg/chromium/base/test/util/ScalableTimeout;->sTimeoutScale:Ljava/lang/Double;

    goto :goto_0
.end method
