.class public Lorg/chromium/base/test/util/TestThread;
.super Ljava/lang/Thread;
.source "TestThread.java"


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private mMainThreadHandler:Landroid/os/Handler;

.field private mTestThreadHandler:Landroid/os/Handler;

.field private mThreadReady:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private mThreadReadyLock:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 45
    const-class v0, Lorg/chromium/base/test/util/TestThread;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/chromium/base/test/util/TestThread;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 51
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 52
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lorg/chromium/base/test/util/TestThread;->mMainThreadHandler:Landroid/os/Handler;

    .line 54
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lorg/chromium/base/test/util/TestThread;->mThreadReadyLock:Ljava/lang/Object;

    .line 55
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>()V

    iput-object v0, p0, Lorg/chromium/base/test/util/TestThread;->mThreadReady:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 56
    return-void
.end method

.method static synthetic access$000(Lorg/chromium/base/test/util/TestThread;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lorg/chromium/base/test/util/TestThread;->mThreadReadyLock:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$100(Lorg/chromium/base/test/util/TestThread;)Ljava/util/concurrent/atomic/AtomicBoolean;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lorg/chromium/base/test/util/TestThread;->mThreadReady:Ljava/util/concurrent/atomic/AtomicBoolean;

    return-object v0
.end method

.method private checkOnMainThread()V
    .locals 2

    .prologue
    .line 141
    sget-boolean v0, Lorg/chromium/base/test/util/TestThread;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    iget-object v1, p0, Lorg/chromium/base/test/util/TestThread;->mMainThreadHandler:Landroid/os/Handler;

    invoke-virtual {v1}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v1

    if-eq v0, v1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 142
    :cond_0
    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 60
    invoke-static {}, Landroid/os/Looper;->prepare()V

    .line 61
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lorg/chromium/base/test/util/TestThread;->mTestThreadHandler:Landroid/os/Handler;

    .line 62
    iget-object v0, p0, Lorg/chromium/base/test/util/TestThread;->mTestThreadHandler:Landroid/os/Handler;

    new-instance v1, Lorg/chromium/base/test/util/TestThread$1;

    invoke-direct {v1, p0}, Lorg/chromium/base/test/util/TestThread$1;-><init>(Lorg/chromium/base/test/util/TestThread;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 71
    invoke-static {}, Landroid/os/Looper;->loop()V

    .line 72
    return-void
.end method

.method public runOnTestThreadSync(Ljava/lang/Runnable;)V
    .locals 4

    .prologue
    .line 114
    invoke-direct {p0}, Lorg/chromium/base/test/util/TestThread;->checkOnMainThread()V

    .line 115
    new-instance v1, Ljava/lang/Object;

    invoke-direct {v1}, Ljava/lang/Object;-><init>()V

    .line 118
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>()V

    .line 119
    iget-object v2, p0, Lorg/chromium/base/test/util/TestThread;->mTestThreadHandler:Landroid/os/Handler;

    new-instance v3, Lorg/chromium/base/test/util/TestThread$2;

    invoke-direct {v3, p0, p1, v1, v0}, Lorg/chromium/base/test/util/TestThread$2;-><init>(Lorg/chromium/base/test/util/TestThread;Ljava/lang/Runnable;Ljava/lang/Object;Ljava/util/concurrent/atomic/AtomicBoolean;)V

    invoke-virtual {v2, v3}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 129
    monitor-enter v1

    .line 131
    :goto_0
    :try_start_0
    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v2

    if-nez v2, :cond_0

    .line 132
    invoke-virtual {v1}, Ljava/lang/Object;->wait()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 134
    :catch_0
    move-exception v0

    .line 135
    :try_start_1
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    .line 137
    :cond_0
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public runOnTestThreadSyncAndProcessPendingTasks(Ljava/lang/Runnable;)V
    .locals 1

    .prologue
    .line 100
    invoke-direct {p0}, Lorg/chromium/base/test/util/TestThread;->checkOnMainThread()V

    .line 102
    invoke-virtual {p0, p1}, Lorg/chromium/base/test/util/TestThread;->runOnTestThreadSync(Ljava/lang/Runnable;)V

    .line 105
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lorg/chromium/base/test/util/TestThread;->runOnTestThreadSync(Ljava/lang/Runnable;)V

    .line 106
    return-void
.end method

.method public startAndWaitForReadyState()V
    .locals 4

    .prologue
    .line 78
    invoke-direct {p0}, Lorg/chromium/base/test/util/TestThread;->checkOnMainThread()V

    .line 79
    invoke-virtual {p0}, Lorg/chromium/base/test/util/TestThread;->start()V

    .line 80
    iget-object v1, p0, Lorg/chromium/base/test/util/TestThread;->mThreadReadyLock:Ljava/lang/Object;

    monitor-enter v1

    .line 84
    :goto_0
    :try_start_0
    iget-object v0, p0, Lorg/chromium/base/test/util/TestThread;->mThreadReady:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-nez v0, :cond_0

    .line 85
    iget-object v0, p0, Lorg/chromium/base/test/util/TestThread;->mThreadReadyLock:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->wait()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 87
    :catch_0
    move-exception v0

    .line 88
    :try_start_1
    sget-object v2, Ljava/lang/System;->err:Ljava/io/PrintStream;

    const-string/jumbo v3, "Error starting TestThread."

    invoke-virtual {v2, v3}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 89
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    .line 91
    :cond_0
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
