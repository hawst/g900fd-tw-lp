.class public Lorg/chromium/base/test/util/AdvancedMockContext;
.super Landroid/content/ContextWrapper;
.source "AdvancedMockContext.java"


# instance fields
.field private final mFlags:Ljava/util/Map;

.field private final mMockContentResolver:Landroid/test/mock/MockContentResolver;

.field private final mSharedPreferences:Ljava/util/Map;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 35
    new-instance v0, Landroid/test/mock/MockContext;

    invoke-direct {v0}, Landroid/test/mock/MockContext;-><init>()V

    invoke-direct {p0, v0}, Landroid/content/ContextWrapper;-><init>(Landroid/content/Context;)V

    .line 23
    new-instance v0, Landroid/test/mock/MockContentResolver;

    invoke-direct {v0}, Landroid/test/mock/MockContentResolver;-><init>()V

    iput-object v0, p0, Lorg/chromium/base/test/util/AdvancedMockContext;->mMockContentResolver:Landroid/test/mock/MockContentResolver;

    .line 25
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lorg/chromium/base/test/util/AdvancedMockContext;->mSharedPreferences:Ljava/util/Map;

    .line 28
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lorg/chromium/base/test/util/AdvancedMockContext;->mFlags:Ljava/util/Map;

    .line 36
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 31
    invoke-direct {p0, p1}, Landroid/content/ContextWrapper;-><init>(Landroid/content/Context;)V

    .line 23
    new-instance v0, Landroid/test/mock/MockContentResolver;

    invoke-direct {v0}, Landroid/test/mock/MockContentResolver;-><init>()V

    iput-object v0, p0, Lorg/chromium/base/test/util/AdvancedMockContext;->mMockContentResolver:Landroid/test/mock/MockContentResolver;

    .line 25
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lorg/chromium/base/test/util/AdvancedMockContext;->mSharedPreferences:Ljava/util/Map;

    .line 28
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lorg/chromium/base/test/util/AdvancedMockContext;->mFlags:Ljava/util/Map;

    .line 32
    return-void
.end method


# virtual methods
.method public addSharedPreferences(Ljava/lang/String;Ljava/util/Map;)V
    .locals 3

    .prologue
    .line 79
    iget-object v1, p0, Lorg/chromium/base/test/util/AdvancedMockContext;->mSharedPreferences:Ljava/util/Map;

    monitor-enter v1

    .line 80
    :try_start_0
    iget-object v0, p0, Lorg/chromium/base/test/util/AdvancedMockContext;->mSharedPreferences:Ljava/util/Map;

    new-instance v2, Lorg/chromium/base/test/util/InMemorySharedPreferences;

    invoke-direct {v2, p2}, Lorg/chromium/base/test/util/InMemorySharedPreferences;-><init>(Ljava/util/Map;)V

    invoke-interface {v0, p1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 81
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public clearFlag(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Lorg/chromium/base/test/util/AdvancedMockContext;->mFlags:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 90
    return-void
.end method

.method public getApplicationContext()Landroid/content/Context;
    .locals 0

    .prologue
    .line 45
    return-object p0
.end method

.method public getContentResolver()Landroid/content/ContentResolver;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lorg/chromium/base/test/util/AdvancedMockContext;->mMockContentResolver:Landroid/test/mock/MockContentResolver;

    return-object v0
.end method

.method public getMockContentResolver()Landroid/test/mock/MockContentResolver;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lorg/chromium/base/test/util/AdvancedMockContext;->mMockContentResolver:Landroid/test/mock/MockContentResolver;

    return-object v0
.end method

.method public getPackageName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 40
    invoke-virtual {p0}, Lorg/chromium/base/test/util/AdvancedMockContext;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;
    .locals 3

    .prologue
    .line 59
    iget-object v1, p0, Lorg/chromium/base/test/util/AdvancedMockContext;->mSharedPreferences:Ljava/util/Map;

    monitor-enter v1

    .line 60
    :try_start_0
    iget-object v0, p0, Lorg/chromium/base/test/util/AdvancedMockContext;->mSharedPreferences:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 62
    iget-object v0, p0, Lorg/chromium/base/test/util/AdvancedMockContext;->mSharedPreferences:Ljava/util/Map;

    new-instance v2, Lorg/chromium/base/test/util/InMemorySharedPreferences;

    invoke-direct {v2}, Lorg/chromium/base/test/util/InMemorySharedPreferences;-><init>()V

    invoke-interface {v0, p1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 64
    :cond_0
    iget-object v0, p0, Lorg/chromium/base/test/util/AdvancedMockContext;->mSharedPreferences:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/SharedPreferences;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    .line 65
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public isFlagSet(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lorg/chromium/base/test/util/AdvancedMockContext;->mFlags:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/chromium/base/test/util/AdvancedMockContext;->mFlags:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public registerComponentCallbacks(Landroid/content/ComponentCallbacks;)V
    .locals 1

    .prologue
    .line 70
    invoke-virtual {p0}, Lorg/chromium/base/test/util/AdvancedMockContext;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/Context;->registerComponentCallbacks(Landroid/content/ComponentCallbacks;)V

    .line 71
    return-void
.end method

.method public setFlag(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 85
    iget-object v0, p0, Lorg/chromium/base/test/util/AdvancedMockContext;->mFlags:Ljava/util/Map;

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 86
    return-void
.end method

.method public unregisterComponentCallbacks(Landroid/content/ComponentCallbacks;)V
    .locals 1

    .prologue
    .line 75
    invoke-virtual {p0}, Lorg/chromium/base/test/util/AdvancedMockContext;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/Context;->unregisterComponentCallbacks(Landroid/content/ComponentCallbacks;)V

    .line 76
    return-void
.end method
