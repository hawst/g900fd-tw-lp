.class public Lorg/chromium/base/test/util/AdvancedMockContext$MapBuilder;
.super Ljava/lang/Object;
.source "AdvancedMockContext.java"


# instance fields
.field private final mData:Ljava/util/Map;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 100
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 102
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lorg/chromium/base/test/util/AdvancedMockContext$MapBuilder;->mData:Ljava/util/Map;

    return-void
.end method

.method public static create()Lorg/chromium/base/test/util/AdvancedMockContext$MapBuilder;
    .locals 1

    .prologue
    .line 105
    new-instance v0, Lorg/chromium/base/test/util/AdvancedMockContext$MapBuilder;

    invoke-direct {v0}, Lorg/chromium/base/test/util/AdvancedMockContext$MapBuilder;-><init>()V

    return-object v0
.end method


# virtual methods
.method public add(Ljava/lang/String;Ljava/lang/Object;)Lorg/chromium/base/test/util/AdvancedMockContext$MapBuilder;
    .locals 1

    .prologue
    .line 109
    iget-object v0, p0, Lorg/chromium/base/test/util/AdvancedMockContext$MapBuilder;->mData:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 110
    return-object p0
.end method

.method public build()Ljava/util/Map;
    .locals 1

    .prologue
    .line 114
    iget-object v0, p0, Lorg/chromium/base/test/util/AdvancedMockContext$MapBuilder;->mData:Ljava/util/Map;

    return-object v0
.end method
