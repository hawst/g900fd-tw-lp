.class final Lorg/chromium/base/MemoryPressureListener$1;
.super Ljava/lang/Object;
.source "MemoryPressureListener.java"

# interfaces
.implements Landroid/content/ComponentCallbacks2;


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0

    .prologue
    .line 61
    return-void
.end method

.method public final onLowMemory()V
    .locals 1

    .prologue
    .line 56
    const/4 v0, 0x2

    # invokes: Lorg/chromium/base/MemoryPressureListener;->nativeOnMemoryPressure(I)V
    invoke-static {v0}, Lorg/chromium/base/MemoryPressureListener;->access$000(I)V

    .line 57
    return-void
.end method

.method public final onTrimMemory(I)V
    .locals 0

    .prologue
    .line 51
    invoke-static {p1}, Lorg/chromium/base/MemoryPressureListener;->maybeNotifyMemoryPresure(I)V

    .line 52
    return-void
.end method
