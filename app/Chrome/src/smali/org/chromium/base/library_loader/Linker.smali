.class public Lorg/chromium/base/library_loader/Linker;
.super Ljava/lang/Object;
.source "Linker.java"


# static fields
.field static final synthetic $assertionsDisabled:Z

.field public static final BROWSER_SHARED_RELRO_CONFIG:I = 0x1

.field public static final BROWSER_SHARED_RELRO_CONFIG_ALWAYS:I = 0x2

.field public static final BROWSER_SHARED_RELRO_CONFIG_LOW_RAM_ONLY:I = 0x1

.field public static final BROWSER_SHARED_RELRO_CONFIG_NEVER:I = 0x0

.field public static final EXTRA_LINKER_SHARED_RELROS:Ljava/lang/String; = "org.chromium.base.android.linker.shared_relros"

.field public static final MEMORY_DEVICE_CONFIG_INIT:I = 0x0

.field public static final MEMORY_DEVICE_CONFIG_LOW:I = 0x1

.field public static final MEMORY_DEVICE_CONFIG_NORMAL:I = 0x2

.field private static sBaseLoadAddress:J

.field private static sBrowserUsesSharedRelro:Z

.field private static sCurrentLoadAddress:J

.field private static sInBrowserProcess:Z

.field private static sInitialized:Z

.field private static sLoadAtFixedAddressFailed:Z

.field private static sLoadedLibraries:Ljava/util/HashMap;

.field private static sMemoryDeviceConfig:I

.field private static sPrepareLibraryLoadCalled:Z

.field private static sRelroSharingSupported:Z

.field private static sSharedRelros:Landroid/os/Bundle;

.field static sTestRunnerClassName:Ljava/lang/String;

.field private static sWaitForSharedRelros:Z


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const/4 v1, 0x1

    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 155
    const-class v0, Lorg/chromium/base/library_loader/Linker;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    sput-boolean v0, Lorg/chromium/base/library_loader/Linker;->$assertionsDisabled:Z

    .line 190
    sput v2, Lorg/chromium/base/library_loader/Linker;->sMemoryDeviceConfig:I

    .line 193
    sput-boolean v2, Lorg/chromium/base/library_loader/Linker;->sInitialized:Z

    .line 196
    sput-boolean v2, Lorg/chromium/base/library_loader/Linker;->sRelroSharingSupported:Z

    .line 199
    sput-boolean v1, Lorg/chromium/base/library_loader/Linker;->sInBrowserProcess:Z

    .line 203
    sput-boolean v2, Lorg/chromium/base/library_loader/Linker;->sWaitForSharedRelros:Z

    .line 207
    sput-boolean v2, Lorg/chromium/base/library_loader/Linker;->sBrowserUsesSharedRelro:Z

    .line 210
    sput-object v3, Lorg/chromium/base/library_loader/Linker;->sSharedRelros:Landroid/os/Bundle;

    .line 213
    sput-wide v4, Lorg/chromium/base/library_loader/Linker;->sBaseLoadAddress:J

    .line 216
    sput-wide v4, Lorg/chromium/base/library_loader/Linker;->sCurrentLoadAddress:J

    .line 219
    sput-boolean v2, Lorg/chromium/base/library_loader/Linker;->sLoadAtFixedAddressFailed:Z

    .line 222
    sput-boolean v2, Lorg/chromium/base/library_loader/Linker;->sPrepareLibraryLoadCalled:Z

    .line 298
    sput-object v3, Lorg/chromium/base/library_loader/Linker;->sTestRunnerClassName:Ljava/lang/String;

    .line 1065
    sput-object v3, Lorg/chromium/base/library_loader/Linker;->sLoadedLibraries:Ljava/util/HashMap;

    return-void

    :cond_0
    move v0, v2

    .line 155
    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 155
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 942
    return-void
.end method

.method static synthetic access$000(J)V
    .locals 0

    .prologue
    .line 155
    invoke-static {p0, p1}, Lorg/chromium/base/library_loader/Linker;->nativeRunCallbackOnUiThread(J)V

    return-void
.end method

.method private static closeLibInfoMap(Ljava/util/HashMap;)V
    .locals 2

    .prologue
    .line 1059
    invoke-virtual {p0}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 1060
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/chromium/base/library_loader/Linker$LibInfo;

    invoke-virtual {v0}, Lorg/chromium/base/library_loader/Linker$LibInfo;->close()V

    goto :goto_0

    .line 1062
    :cond_0
    return-void
.end method

.method private static computeRandomBaseLoadAddress()J
    .locals 2

    .prologue
    .line 640
    const-wide/32 v0, 0xc000000

    invoke-static {v0, v1}, Lorg/chromium/base/library_loader/Linker;->nativeGetRandomBaseLoadAddress(J)J

    move-result-wide v0

    .line 646
    return-wide v0
.end method

.method private static createBundleFromLibInfoMap(Ljava/util/HashMap;)Landroid/os/Bundle;
    .locals 4

    .prologue
    .line 1039
    new-instance v2, Landroid/os/Bundle;

    invoke-virtual {p0}, Ljava/util/HashMap;->size()I

    move-result v0

    invoke-direct {v2, v0}, Landroid/os/Bundle;-><init>(I)V

    .line 1040
    invoke-virtual {p0}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 1041
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Parcelable;

    invoke-virtual {v2, v1, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    goto :goto_0

    .line 1044
    :cond_0
    return-object v2
.end method

.method private static createLibInfoMapFromBundle(Landroid/os/Bundle;)Ljava/util/HashMap;
    .locals 4

    .prologue
    .line 1049
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 1050
    invoke-virtual {p0}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1051
    invoke-virtual {p0, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lorg/chromium/base/library_loader/Linker$LibInfo;

    .line 1052
    invoke-virtual {v2, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 1054
    :cond_0
    return-object v2
.end method

.method public static disableSharedRelros()V
    .locals 2

    .prologue
    .line 549
    const-class v1, Lorg/chromium/base/library_loader/Linker;

    monitor-enter v1

    .line 550
    const/4 v0, 0x0

    :try_start_0
    sput-boolean v0, Lorg/chromium/base/library_loader/Linker;->sInBrowserProcess:Z

    .line 551
    const/4 v0, 0x0

    sput-boolean v0, Lorg/chromium/base/library_loader/Linker;->sWaitForSharedRelros:Z

    .line 552
    const/4 v0, 0x0

    sput-boolean v0, Lorg/chromium/base/library_loader/Linker;->sBrowserUsesSharedRelro:Z

    .line 553
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private static ensureInitializedLocked()V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 226
    sget-boolean v0, Lorg/chromium/base/library_loader/Linker;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    const-class v0, Lorg/chromium/base/library_loader/Linker;

    invoke-static {v0}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 228
    :cond_0
    sget-boolean v0, Lorg/chromium/base/library_loader/Linker;->sInitialized:Z

    if-nez v0, :cond_5

    .line 229
    sput-boolean v2, Lorg/chromium/base/library_loader/Linker;->sRelroSharingSupported:Z

    .line 230
    sget-boolean v0, Lorg/chromium/base/library_loader/NativeLibraries;->USE_LINKER:Z

    if-eqz v0, :cond_3

    .line 233
    :try_start_0
    const-string/jumbo v0, "chromium_android_linker"

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/UnsatisfiedLinkError; {:try_start_0 .. :try_end_0} :catch_0

    .line 238
    :goto_0
    invoke-static {}, Lorg/chromium/base/library_loader/Linker;->nativeCanUseSharedRelro()Z

    move-result v0

    .line 239
    sput-boolean v0, Lorg/chromium/base/library_loader/Linker;->sRelroSharingSupported:Z

    if-nez v0, :cond_1

    .line 240
    const-string/jumbo v0, "chromium_android_linker"

    const-string/jumbo v3, "This system cannot safely share RELRO sections"

    invoke-static {v0, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 245
    :cond_1
    sget v0, Lorg/chromium/base/library_loader/Linker;->sMemoryDeviceConfig:I

    if-nez v0, :cond_2

    .line 246
    invoke-static {}, Lorg/chromium/base/SysUtils;->isLowEndDevice()Z

    move-result v0

    if-eqz v0, :cond_6

    move v0, v1

    :goto_1
    sput v0, Lorg/chromium/base/library_loader/Linker;->sMemoryDeviceConfig:I

    .line 250
    :cond_2
    sget v0, Lorg/chromium/base/library_loader/Linker;->sMemoryDeviceConfig:I

    if-ne v0, v1, :cond_7

    move v0, v1

    .line 257
    :goto_2
    sput-boolean v0, Lorg/chromium/base/library_loader/Linker;->sBrowserUsesSharedRelro:Z

    if-eqz v0, :cond_3

    .line 258
    const-string/jumbo v0, "chromium_android_linker"

    const-string/jumbo v3, "Low-memory device: shared RELROs used in all processes"

    invoke-static {v0, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 261
    :cond_3
    sget-boolean v0, Lorg/chromium/base/library_loader/Linker;->sRelroSharingSupported:Z

    if-nez v0, :cond_4

    .line 274
    sput-boolean v2, Lorg/chromium/base/library_loader/Linker;->sBrowserUsesSharedRelro:Z

    .line 275
    sput-boolean v2, Lorg/chromium/base/library_loader/Linker;->sWaitForSharedRelros:Z

    .line 278
    :cond_4
    sput-boolean v1, Lorg/chromium/base/library_loader/Linker;->sInitialized:Z

    .line 280
    :cond_5
    return-void

    .line 236
    :catch_0
    move-exception v0

    const-string/jumbo v0, "chromium_android_linker.cr"

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    goto :goto_0

    .line 246
    :cond_6
    const/4 v0, 0x2

    goto :goto_1

    :cond_7
    move v0, v2

    .line 250
    goto :goto_2
.end method

.method public static finishLibraryLoad()V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 418
    const-class v2, Lorg/chromium/base/library_loader/Linker;

    monitor-enter v2

    .line 426
    :try_start_0
    sget-object v0, Lorg/chromium/base/library_loader/Linker;->sLoadedLibraries:Ljava/util/HashMap;

    if-eqz v0, :cond_3

    .line 429
    sget-boolean v0, Lorg/chromium/base/library_loader/Linker;->sInBrowserProcess:Z

    if-eqz v0, :cond_0

    .line 432
    sget-object v0, Lorg/chromium/base/library_loader/Linker;->sLoadedLibraries:Ljava/util/HashMap;

    invoke-static {v0}, Lorg/chromium/base/library_loader/Linker;->createBundleFromLibInfoMap(Ljava/util/HashMap;)Landroid/os/Bundle;

    move-result-object v0

    sput-object v0, Lorg/chromium/base/library_loader/Linker;->sSharedRelros:Landroid/os/Bundle;

    .line 438
    sget-boolean v0, Lorg/chromium/base/library_loader/Linker;->sBrowserUsesSharedRelro:Z

    if-eqz v0, :cond_0

    .line 439
    sget-object v0, Lorg/chromium/base/library_loader/Linker;->sSharedRelros:Landroid/os/Bundle;

    invoke-static {v0}, Lorg/chromium/base/library_loader/Linker;->useSharedRelrosLocked(Landroid/os/Bundle;)V

    .line 443
    :cond_0
    sget-boolean v0, Lorg/chromium/base/library_loader/Linker;->sWaitForSharedRelros:Z

    if-eqz v0, :cond_3

    .line 444
    sget-boolean v0, Lorg/chromium/base/library_loader/Linker;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    sget-boolean v0, Lorg/chromium/base/library_loader/Linker;->sInBrowserProcess:Z

    if-eqz v0, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 483
    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0

    .line 447
    :cond_1
    :goto_0
    :try_start_1
    sget-object v0, Lorg/chromium/base/library_loader/Linker;->sSharedRelros:Landroid/os/Bundle;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-nez v0, :cond_2

    .line 449
    :try_start_2
    const-class v0, Lorg/chromium/base/library_loader/Linker;

    invoke-virtual {v0}, Ljava/lang/Object;->wait()V
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 452
    :catch_0
    move-exception v0

    goto :goto_0

    .line 454
    :cond_2
    :try_start_3
    sget-object v0, Lorg/chromium/base/library_loader/Linker;->sSharedRelros:Landroid/os/Bundle;

    invoke-static {v0}, Lorg/chromium/base/library_loader/Linker;->useSharedRelrosLocked(Landroid/os/Bundle;)V

    .line 456
    sget-object v0, Lorg/chromium/base/library_loader/Linker;->sSharedRelros:Landroid/os/Bundle;

    invoke-virtual {v0}, Landroid/os/Bundle;->clear()V

    .line 457
    const/4 v0, 0x0

    sput-object v0, Lorg/chromium/base/library_loader/Linker;->sSharedRelros:Landroid/os/Bundle;

    .line 461
    :cond_3
    sget-boolean v0, Lorg/chromium/base/library_loader/NativeLibraries;->ENABLE_LINKER_TESTS:Z

    if-eqz v0, :cond_5

    sget-object v0, Lorg/chromium/base/library_loader/Linker;->sTestRunnerClassName:Ljava/lang/String;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    if-eqz v0, :cond_5

    .line 466
    :try_start_4
    sget-object v0, Lorg/chromium/base/library_loader/Linker;->sTestRunnerClassName:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/chromium/base/library_loader/Linker$TestRunner;
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 474
    :goto_1
    if-eqz v0, :cond_5

    .line 475
    :try_start_5
    sget v1, Lorg/chromium/base/library_loader/Linker;->sMemoryDeviceConfig:I

    sget-boolean v3, Lorg/chromium/base/library_loader/Linker;->sInBrowserProcess:Z

    invoke-interface {v0, v1, v3}, Lorg/chromium/base/library_loader/Linker$TestRunner;->runChecks(IZ)Z

    move-result v0

    if-nez v0, :cond_4

    .line 476
    const-string/jumbo v0, "chromium_android_linker"

    const-string/jumbo v1, "Linker runtime tests failed in this process!!"

    invoke-static {v0, v1}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)I

    .line 477
    sget-boolean v0, Lorg/chromium/base/library_loader/Linker;->$assertionsDisabled:Z

    if-nez v0, :cond_5

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 470
    :catch_1
    move-exception v0

    .line 471
    const-string/jumbo v3, "chromium_android_linker"

    const-string/jumbo v4, "Could not extract test runner class name"

    invoke-static {v3, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move-object v0, v1

    .line 472
    goto :goto_1

    .line 479
    :cond_4
    const-string/jumbo v0, "chromium_android_linker"

    const-string/jumbo v1, "All linker tests passed!"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 483
    :cond_5
    monitor-exit v2
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    return-void
.end method

.method public static getBaseLoadAddress()J
    .locals 3

    .prologue
    .line 587
    const-class v2, Lorg/chromium/base/library_loader/Linker;

    monitor-enter v2

    .line 588
    :try_start_0
    invoke-static {}, Lorg/chromium/base/library_loader/Linker;->ensureInitializedLocked()V

    .line 589
    sget-boolean v0, Lorg/chromium/base/library_loader/Linker;->sInBrowserProcess:Z

    if-nez v0, :cond_0

    .line 590
    const-string/jumbo v0, "chromium_android_linker"

    const-string/jumbo v1, "Shared RELRO sections are disabled in this process!"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 591
    const-wide/16 v0, 0x0

    monitor-exit v2

    .line 597
    :goto_0
    return-wide v0

    .line 594
    :cond_0
    invoke-static {}, Lorg/chromium/base/library_loader/Linker;->setupBaseLoadAddressLocked()V

    .line 597
    sget-wide v0, Lorg/chromium/base/library_loader/Linker;->sBaseLoadAddress:J

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 598
    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0
.end method

.method public static getSharedRelros()Landroid/os/Bundle;
    .locals 2

    .prologue
    .line 530
    const-class v1, Lorg/chromium/base/library_loader/Linker;

    monitor-enter v1

    .line 531
    :try_start_0
    sget-boolean v0, Lorg/chromium/base/library_loader/Linker;->sInBrowserProcess:Z

    if-nez v0, :cond_0

    .line 533
    const/4 v0, 0x0

    monitor-exit v1

    .line 538
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lorg/chromium/base/library_loader/Linker;->sSharedRelros:Landroid/os/Bundle;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 539
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static getTestRunnerClassName()Ljava/lang/String;
    .locals 2

    .prologue
    .line 328
    const-class v1, Lorg/chromium/base/library_loader/Linker;

    monitor-enter v1

    .line 329
    :try_start_0
    sget-object v0, Lorg/chromium/base/library_loader/Linker;->sTestRunnerClassName:Ljava/lang/String;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    .line 330
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static initServiceProcess(J)V
    .locals 2

    .prologue
    .line 567
    const-class v1, Lorg/chromium/base/library_loader/Linker;

    monitor-enter v1

    .line 568
    :try_start_0
    invoke-static {}, Lorg/chromium/base/library_loader/Linker;->ensureInitializedLocked()V

    .line 569
    const/4 v0, 0x0

    sput-boolean v0, Lorg/chromium/base/library_loader/Linker;->sInBrowserProcess:Z

    .line 570
    const/4 v0, 0x0

    sput-boolean v0, Lorg/chromium/base/library_loader/Linker;->sBrowserUsesSharedRelro:Z

    .line 571
    sget-boolean v0, Lorg/chromium/base/library_loader/Linker;->sRelroSharingSupported:Z

    if-eqz v0, :cond_0

    .line 572
    const/4 v0, 0x1

    sput-boolean v0, Lorg/chromium/base/library_loader/Linker;->sWaitForSharedRelros:Z

    .line 573
    sput-wide p0, Lorg/chromium/base/library_loader/Linker;->sBaseLoadAddress:J

    .line 574
    sput-wide p0, Lorg/chromium/base/library_loader/Linker;->sCurrentLoadAddress:J

    .line 576
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static isInZipFile()Z
    .locals 1

    .prologue
    .line 392
    sget-boolean v0, Lorg/chromium/base/library_loader/NativeLibraries;->USE_LIBRARY_IN_ZIP_FILE:Z

    return v0
.end method

.method public static isUsed()Z
    .locals 2

    .prologue
    .line 365
    sget-boolean v0, Lorg/chromium/base/library_loader/NativeLibraries;->USE_LINKER:Z

    if-nez v0, :cond_0

    .line 366
    const/4 v0, 0x0

    .line 372
    :goto_0
    return v0

    .line 368
    :cond_0
    const-class v1, Lorg/chromium/base/library_loader/Linker;

    monitor-enter v1

    .line 369
    :try_start_0
    invoke-static {}, Lorg/chromium/base/library_loader/Linker;->ensureInitializedLocked()V

    .line 372
    sget-boolean v0, Lorg/chromium/base/library_loader/Linker;->sRelroSharingSupported:Z

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 373
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static isUsingBrowserSharedRelros()Z
    .locals 2

    .prologue
    .line 381
    const-class v1, Lorg/chromium/base/library_loader/Linker;

    monitor-enter v1

    .line 382
    :try_start_0
    invoke-static {}, Lorg/chromium/base/library_loader/Linker;->ensureInitializedLocked()V

    .line 383
    sget-boolean v0, Lorg/chromium/base/library_loader/Linker;->sBrowserUsesSharedRelro:Z

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v0

    .line 384
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static loadAtFixedAddressFailed()Z
    .locals 1

    .prologue
    .line 708
    sget-boolean v0, Lorg/chromium/base/library_loader/Linker;->sLoadAtFixedAddressFailed:Z

    return v0
.end method

.method public static loadLibrary(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 730
    const/4 v0, 0x0

    invoke-static {v0, p0}, Lorg/chromium/base/library_loader/Linker;->loadLibraryMaybeInZipFile(Ljava/lang/String;Ljava/lang/String;)V

    .line 731
    return-void
.end method

.method public static loadLibraryInZipFile(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 721
    invoke-static {p0, p1}, Lorg/chromium/base/library_loader/Linker;->loadLibraryMaybeInZipFile(Ljava/lang/String;Ljava/lang/String;)V

    .line 722
    return-void
.end method

.method private static loadLibraryMaybeInZipFile(Ljava/lang/String;Ljava/lang/String;)V
    .locals 12

    .prologue
    const-wide/16 v4, 0x0

    .line 741
    const-string/jumbo v0, "chromium_android_linker"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string/jumbo v0, "chromium_android_linker.cr"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 836
    :cond_0
    :goto_0
    return-void

    .line 746
    :cond_1
    const-class v6, Lorg/chromium/base/library_loader/Linker;

    monitor-enter v6

    .line 747
    :try_start_0
    invoke-static {}, Lorg/chromium/base/library_loader/Linker;->ensureInitializedLocked()V

    .line 753
    sget-boolean v0, Lorg/chromium/base/library_loader/Linker;->$assertionsDisabled:Z

    if-nez v0, :cond_2

    sget-boolean v0, Lorg/chromium/base/library_loader/Linker;->sPrepareLibraryLoadCalled:Z

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 836
    :catchall_0
    move-exception v0

    monitor-exit v6

    throw v0

    .line 755
    :cond_2
    :try_start_1
    invoke-static {p1}, Ljava/lang/System;->mapLibraryName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 757
    sget-object v0, Lorg/chromium/base/library_loader/Linker;->sLoadedLibraries:Ljava/util/HashMap;

    if-nez v0, :cond_3

    .line 758
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lorg/chromium/base/library_loader/Linker;->sLoadedLibraries:Ljava/util/HashMap;

    .line 760
    :cond_3
    sget-object v0, Lorg/chromium/base/library_loader/Linker;->sLoadedLibraries:Ljava/util/HashMap;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 762
    monitor-exit v6

    goto :goto_0

    .line 765
    :cond_4
    new-instance v7, Lorg/chromium/base/library_loader/Linker$LibInfo;

    invoke-direct {v7}, Lorg/chromium/base/library_loader/Linker$LibInfo;-><init>()V

    .line 767
    sget-boolean v0, Lorg/chromium/base/library_loader/Linker;->sInBrowserProcess:Z

    if-eqz v0, :cond_5

    sget-boolean v0, Lorg/chromium/base/library_loader/Linker;->sBrowserUsesSharedRelro:Z

    if-nez v0, :cond_6

    :cond_5
    sget-boolean v0, Lorg/chromium/base/library_loader/Linker;->sWaitForSharedRelros:Z

    if-eqz v0, :cond_f

    .line 769
    :cond_6
    sget-wide v2, Lorg/chromium/base/library_loader/Linker;->sCurrentLoadAddress:J

    .line 773
    :goto_1
    if-eqz p0, :cond_7

    .line 774
    invoke-static {p0, v1, v2, v3, v7}, Lorg/chromium/base/library_loader/Linker;->nativeLoadLibraryInZipFile(Ljava/lang/String;Ljava/lang/String;JLorg/chromium/base/library_loader/Linker$LibInfo;)Z

    move-result v0

    if-nez v0, :cond_9

    .line 776
    new-instance v0, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "Unable to load library: "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " in: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 779
    const-string/jumbo v1, "chromium_android_linker"

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 780
    new-instance v1, Ljava/lang/UnsatisfiedLinkError;

    invoke-direct {v1, v0}, Ljava/lang/UnsatisfiedLinkError;-><init>(Ljava/lang/String;)V

    throw v1

    .line 784
    :cond_7
    invoke-static {v1, v2, v3, v7}, Lorg/chromium/base/library_loader/Linker;->nativeLoadLibrary(Ljava/lang/String;JLorg/chromium/base/library_loader/Linker$LibInfo;)Z

    move-result v0

    if-nez v0, :cond_8

    .line 785
    new-instance v0, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "Unable to load library: "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 786
    const-string/jumbo v1, "chromium_android_linker"

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 787
    new-instance v1, Ljava/lang/UnsatisfiedLinkError;

    invoke-direct {v1, v0}, Ljava/lang/UnsatisfiedLinkError;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_8
    move-object p0, v1

    .line 791
    :cond_9
    cmp-long v0, v2, v4

    if-eqz v0, :cond_a

    iget-wide v8, v7, Lorg/chromium/base/library_loader/Linker$LibInfo;->mLoadAddress:J

    cmp-long v0, v2, v8

    if-eqz v0, :cond_a

    .line 792
    const/4 v0, 0x1

    sput-boolean v0, Lorg/chromium/base/library_loader/Linker;->sLoadAtFixedAddressFailed:Z

    .line 800
    :cond_a
    sget-boolean v0, Lorg/chromium/base/library_loader/NativeLibraries;->ENABLE_LINKER_TESTS:Z

    if-eqz v0, :cond_b

    .line 801
    const-string/jumbo v2, "chromium_android_linker"

    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string/jumbo v8, "%s_LIBRARY_ADDRESS: %s %x"

    const/4 v0, 0x3

    new-array v9, v0, [Ljava/lang/Object;

    const/4 v10, 0x0

    sget-boolean v0, Lorg/chromium/base/library_loader/Linker;->sInBrowserProcess:Z

    if-eqz v0, :cond_e

    const-string/jumbo v0, "BROWSER"

    :goto_2
    aput-object v0, v9, v10

    const/4 v0, 0x1

    aput-object v1, v9, v0

    const/4 v0, 0x2

    iget-wide v10, v7, Lorg/chromium/base/library_loader/Linker$LibInfo;->mLoadAddress:J

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    aput-object v10, v9, v0

    invoke-static {v3, v8, v9}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 809
    :cond_b
    sget-boolean v0, Lorg/chromium/base/library_loader/Linker;->sInBrowserProcess:Z

    if-eqz v0, :cond_c

    .line 811
    sget-wide v2, Lorg/chromium/base/library_loader/Linker;->sCurrentLoadAddress:J

    invoke-static {p0, v2, v3, v7}, Lorg/chromium/base/library_loader/Linker;->nativeCreateSharedRelro(Ljava/lang/String;JLorg/chromium/base/library_loader/Linker$LibInfo;)Z

    move-result v0

    if-nez v0, :cond_c

    .line 812
    const-string/jumbo v0, "chromium_android_linker"

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string/jumbo v3, "Could not create shared RELRO for %s at %x"

    const/4 v8, 0x2

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    aput-object v1, v8, v9

    const/4 v9, 0x1

    sget-wide v10, Lorg/chromium/base/library_loader/Linker;->sCurrentLoadAddress:J

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-static {v2, v3, v8}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 826
    :cond_c
    sget-wide v2, Lorg/chromium/base/library_loader/Linker;->sCurrentLoadAddress:J

    cmp-long v0, v2, v4

    if-eqz v0, :cond_d

    .line 831
    iget-wide v2, v7, Lorg/chromium/base/library_loader/Linker$LibInfo;->mLoadAddress:J

    iget-wide v4, v7, Lorg/chromium/base/library_loader/Linker$LibInfo;->mLoadSize:J

    add-long/2addr v2, v4

    sput-wide v2, Lorg/chromium/base/library_loader/Linker;->sCurrentLoadAddress:J

    .line 834
    :cond_d
    sget-object v0, Lorg/chromium/base/library_loader/Linker;->sLoadedLibraries:Ljava/util/HashMap;

    invoke-virtual {v0, v1, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 836
    monitor-exit v6

    goto/16 :goto_0

    .line 801
    :cond_e
    const-string/jumbo v0, "RENDERER"
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2

    :cond_f
    move-wide v2, v4

    goto/16 :goto_1
.end method

.method private static native nativeCanUseSharedRelro()Z
.end method

.method private static native nativeCreateSharedRelro(Ljava/lang/String;JLorg/chromium/base/library_loader/Linker$LibInfo;)Z
.end method

.method private static native nativeGetRandomBaseLoadAddress(J)J
.end method

.method private static native nativeLoadLibrary(Ljava/lang/String;JLorg/chromium/base/library_loader/Linker$LibInfo;)Z
.end method

.method private static native nativeLoadLibraryInZipFile(Ljava/lang/String;Ljava/lang/String;JLorg/chromium/base/library_loader/Linker$LibInfo;)Z
.end method

.method private static native nativeRunCallbackOnUiThread(J)V
.end method

.method private static native nativeUseSharedRelro(Ljava/lang/String;Lorg/chromium/base/library_loader/Linker$LibInfo;)Z
.end method

.method public static postCallbackOnMainThread(J)V
    .locals 2

    .prologue
    .line 848
    new-instance v0, Lorg/chromium/base/library_loader/Linker$1;

    invoke-direct {v0, p0, p1}, Lorg/chromium/base/library_loader/Linker$1;-><init>(J)V

    invoke-static {v0}, Lorg/chromium/base/ThreadUtils;->postOnUiThread(Ljava/lang/Runnable;)V

    .line 854
    return-void
.end method

.method public static prepareLibraryLoad()V
    .locals 2

    .prologue
    .line 400
    const-class v1, Lorg/chromium/base/library_loader/Linker;

    monitor-enter v1

    .line 401
    const/4 v0, 0x1

    :try_start_0
    sput-boolean v0, Lorg/chromium/base/library_loader/Linker;->sPrepareLibraryLoadCalled:Z

    .line 403
    sget-boolean v0, Lorg/chromium/base/library_loader/Linker;->sInBrowserProcess:Z

    if-eqz v0, :cond_0

    .line 406
    invoke-static {}, Lorg/chromium/base/library_loader/Linker;->setupBaseLoadAddressLocked()V

    .line 408
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static setTestRunnerClassName(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 309
    sget-boolean v0, Lorg/chromium/base/library_loader/NativeLibraries;->ENABLE_LINKER_TESTS:Z

    if-nez v0, :cond_0

    .line 317
    :goto_0
    return-void

    .line 314
    :cond_0
    const-class v1, Lorg/chromium/base/library_loader/Linker;

    monitor-enter v1

    .line 315
    :try_start_0
    sget-boolean v0, Lorg/chromium/base/library_loader/Linker;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    sget-object v0, Lorg/chromium/base/library_loader/Linker;->sTestRunnerClassName:Ljava/lang/String;

    if-eqz v0, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 317
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 316
    :cond_1
    :try_start_1
    sput-object p0, Lorg/chromium/base/library_loader/Linker;->sTestRunnerClassName:Ljava/lang/String;

    .line 317
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method

.method private static setupBaseLoadAddressLocked()V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const/4 v2, 0x0

    .line 603
    sget-boolean v0, Lorg/chromium/base/library_loader/Linker;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    const-class v0, Lorg/chromium/base/library_loader/Linker;

    invoke-static {v0}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 604
    :cond_0
    sget-wide v0, Lorg/chromium/base/library_loader/Linker;->sBaseLoadAddress:J

    cmp-long v0, v0, v4

    if-nez v0, :cond_1

    .line 605
    invoke-static {}, Lorg/chromium/base/library_loader/Linker;->computeRandomBaseLoadAddress()J

    move-result-wide v0

    .line 606
    sput-wide v0, Lorg/chromium/base/library_loader/Linker;->sBaseLoadAddress:J

    .line 607
    sput-wide v0, Lorg/chromium/base/library_loader/Linker;->sCurrentLoadAddress:J

    .line 608
    cmp-long v0, v0, v4

    if-nez v0, :cond_1

    .line 611
    const-string/jumbo v0, "chromium_android_linker"

    const-string/jumbo v1, "Disabling shared RELROs due address space pressure"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 612
    sput-boolean v2, Lorg/chromium/base/library_loader/Linker;->sBrowserUsesSharedRelro:Z

    .line 613
    sput-boolean v2, Lorg/chromium/base/library_loader/Linker;->sWaitForSharedRelros:Z

    .line 616
    :cond_1
    return-void
.end method

.method public static useSharedRelros(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 499
    const/4 v0, 0x0

    .line 500
    if-eqz p0, :cond_0

    .line 501
    const-class v0, Lorg/chromium/base/library_loader/Linker$LibInfo;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V

    .line 502
    new-instance v0, Landroid/os/Bundle;

    const-class v1, Lorg/chromium/base/library_loader/Linker$LibInfo;

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Bundle;-><init>(Ljava/lang/ClassLoader;)V

    .line 503
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 504
    invoke-virtual {p0, v1, v2}, Landroid/os/Bundle;->writeToParcel(Landroid/os/Parcel;I)V

    .line 505
    invoke-virtual {v1, v2}, Landroid/os/Parcel;->setDataPosition(I)V

    .line 506
    invoke-virtual {v0, v1}, Landroid/os/Bundle;->readFromParcel(Landroid/os/Parcel;)V

    .line 507
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 513
    :cond_0
    const-class v1, Lorg/chromium/base/library_loader/Linker;

    monitor-enter v1

    .line 516
    :try_start_0
    sput-object v0, Lorg/chromium/base/library_loader/Linker;->sSharedRelros:Landroid/os/Bundle;

    .line 518
    const-class v0, Lorg/chromium/base/library_loader/Linker;

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    .line 519
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private static useSharedRelrosLocked(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    .line 662
    sget-boolean v0, Lorg/chromium/base/library_loader/Linker;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    const-class v0, Lorg/chromium/base/library_loader/Linker;

    invoke-static {v0}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 666
    :cond_0
    if-nez p0, :cond_2

    .line 700
    :cond_1
    :goto_0
    return-void

    .line 671
    :cond_2
    sget-boolean v0, Lorg/chromium/base/library_loader/Linker;->sRelroSharingSupported:Z

    if-eqz v0, :cond_1

    .line 676
    sget-object v0, Lorg/chromium/base/library_loader/Linker;->sLoadedLibraries:Ljava/util/HashMap;

    if-eqz v0, :cond_1

    .line 682
    invoke-static {p0}, Lorg/chromium/base/library_loader/Linker;->createLibInfoMapFromBundle(Landroid/os/Bundle;)Ljava/util/HashMap;

    move-result-object v2

    .line 685
    invoke-virtual {v2}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_3
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 686
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 687
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/chromium/base/library_loader/Linker$LibInfo;

    .line 688
    invoke-static {v1, v0}, Lorg/chromium/base/library_loader/Linker;->nativeUseSharedRelro(Ljava/lang/String;Lorg/chromium/base/library_loader/Linker$LibInfo;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 689
    const-string/jumbo v0, "chromium_android_linker"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "Could not use shared RELRO section for "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 696
    :cond_4
    sget-boolean v0, Lorg/chromium/base/library_loader/Linker;->sInBrowserProcess:Z

    if-nez v0, :cond_1

    .line 697
    invoke-static {v2}, Lorg/chromium/base/library_loader/Linker;->closeLibInfoMap(Ljava/util/HashMap;)V

    goto :goto_0
.end method
