.class public Lorg/chromium/base/library_loader/LibraryLoader;
.super Ljava/lang/Object;
.source "LibraryLoader.java"


# annotations
.annotation runtime Lorg/chromium/base/JNINamespace;
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z

.field private static sCommandLineSwitched:Z

.field private static sInitialized:Z

.field private static sLoaded:Z

.field private static final sLock:Ljava/lang/Object;

.field private static sNativeLibraryHackWasUsed:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 30
    const-class v0, Lorg/chromium/base/library_loader/LibraryLoader;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/chromium/base/library_loader/LibraryLoader;->$assertionsDisabled:Z

    .line 35
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lorg/chromium/base/library_loader/LibraryLoader;->sLock:Ljava/lang/Object;

    .line 38
    sput-boolean v1, Lorg/chromium/base/library_loader/LibraryLoader;->sLoaded:Z

    .line 42
    sput-boolean v1, Lorg/chromium/base/library_loader/LibraryLoader;->sCommandLineSwitched:Z

    .line 47
    sput-boolean v1, Lorg/chromium/base/library_loader/LibraryLoader;->sInitialized:Z

    .line 52
    sput-boolean v1, Lorg/chromium/base/library_loader/LibraryLoader;->sNativeLibraryHackWasUsed:Z

    return-void

    :cond_0
    move v0, v1

    .line 30
    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static ensureInitialized(Landroid/content/Context;Z)V
    .locals 2

    .prologue
    .line 84
    sget-object v1, Lorg/chromium/base/library_loader/LibraryLoader;->sLock:Ljava/lang/Object;

    monitor-enter v1

    .line 85
    :try_start_0
    sget-boolean v0, Lorg/chromium/base/library_loader/LibraryLoader;->sInitialized:Z

    if-eqz v0, :cond_0

    .line 87
    monitor-exit v1

    .line 91
    :goto_0
    return-void

    .line 89
    :cond_0
    invoke-static {p0, p1}, Lorg/chromium/base/library_loader/LibraryLoader;->loadAlreadyLocked(Landroid/content/Context;Z)V

    .line 90
    invoke-static {}, Lorg/chromium/base/library_loader/LibraryLoader;->initializeAlreadyLocked()V

    .line 91
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static initialize()V
    .locals 2

    .prologue
    .line 139
    sget-object v1, Lorg/chromium/base/library_loader/LibraryLoader;->sLock:Ljava/lang/Object;

    monitor-enter v1

    .line 140
    :try_start_0
    invoke-static {}, Lorg/chromium/base/library_loader/LibraryLoader;->initializeAlreadyLocked()V

    .line 141
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private static initializeAlreadyLocked()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 263
    sget-boolean v0, Lorg/chromium/base/library_loader/LibraryLoader;->sInitialized:Z

    if-eqz v0, :cond_0

    .line 290
    :goto_0
    return-void

    .line 268
    :cond_0
    sget-boolean v0, Lorg/chromium/base/library_loader/LibraryLoader;->sCommandLineSwitched:Z

    if-nez v0, :cond_1

    .line 269
    invoke-static {}, Lorg/chromium/base/CommandLine;->getJavaSwitchesOrNull()[Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lorg/chromium/base/library_loader/LibraryLoader;->nativeInitCommandLine([Ljava/lang/String;)V

    .line 272
    :cond_1
    invoke-static {}, Lorg/chromium/base/library_loader/LibraryLoader;->nativeLibraryLoaded()Z

    move-result v0

    if-nez v0, :cond_2

    .line 273
    const-string/jumbo v0, "LibraryLoader"

    const-string/jumbo v1, "error calling nativeLibraryLoaded"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 274
    new-instance v0, Lorg/chromium/base/library_loader/ProcessInitException;

    invoke-direct {v0, v2}, Lorg/chromium/base/library_loader/ProcessInitException;-><init>(I)V

    throw v0

    .line 279
    :cond_2
    sput-boolean v2, Lorg/chromium/base/library_loader/LibraryLoader;->sInitialized:Z

    .line 283
    sget-boolean v0, Lorg/chromium/base/library_loader/LibraryLoader;->sCommandLineSwitched:Z

    if-nez v0, :cond_3

    .line 284
    invoke-static {}, Lorg/chromium/base/CommandLine;->enableNativeProxy()V

    .line 285
    sput-boolean v2, Lorg/chromium/base/library_loader/LibraryLoader;->sCommandLineSwitched:Z

    .line 289
    :cond_3
    invoke-static {}, Lorg/chromium/base/TraceEvent;->registerNativeEnabledObserver()V

    goto :goto_0
.end method

.method public static isInitialized()Z
    .locals 2

    .prologue
    .line 98
    sget-object v1, Lorg/chromium/base/library_loader/LibraryLoader;->sLock:Ljava/lang/Object;

    monitor-enter v1

    .line 99
    :try_start_0
    sget-boolean v0, Lorg/chromium/base/library_loader/LibraryLoader;->sInitialized:Z

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v0

    .line 100
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private static loadAlreadyLocked(Landroid/content/Context;Z)V
    .locals 14

    .prologue
    const/4 v13, 0x3

    const/4 v12, 0x2

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 149
    :try_start_0
    sget-boolean v0, Lorg/chromium/base/library_loader/LibraryLoader;->sLoaded:Z

    if-nez v0, :cond_8

    .line 150
    sget-boolean v0, Lorg/chromium/base/library_loader/LibraryLoader;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    sget-boolean v0, Lorg/chromium/base/library_loader/LibraryLoader;->sInitialized:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0
    :try_end_0
    .catch Ljava/lang/UnsatisfiedLinkError; {:try_start_0 .. :try_end_0} :catch_0

    .line 225
    :catch_0
    move-exception v0

    .line 226
    new-instance v1, Lorg/chromium/base/library_loader/ProcessInitException;

    invoke-direct {v1, v12, v0}, Lorg/chromium/base/library_loader/ProcessInitException;-><init>(ILjava/lang/Throwable;)V

    throw v1

    .line 152
    :cond_0
    :try_start_1
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v6

    .line 153
    invoke-static {}, Lorg/chromium/base/library_loader/Linker;->isUsed()Z

    move-result v0

    .line 155
    if-eqz v0, :cond_9

    .line 157
    invoke-static {}, Lorg/chromium/base/library_loader/Linker;->prepareLibraryLoad()V

    .line 159
    sget-object v5, Lorg/chromium/base/library_loader/NativeLibraries;->LIBRARIES:[Ljava/lang/String;

    array-length v8, v5

    move v4, v1

    :goto_0
    if-ge v4, v8, :cond_5

    aget-object v9, v5, v4

    .line 160
    const/4 v0, 0x0

    .line 161
    invoke-static {}, Lorg/chromium/base/library_loader/Linker;->isInZipFile()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 162
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v0

    iget-object v0, v0, Landroid/content/pm/ApplicationInfo;->sourceDir:Ljava/lang/String;

    .line 163
    const-string/jumbo v3, "LibraryLoader"

    new-instance v10, Ljava/lang/StringBuilder;

    const-string/jumbo v11, "Loading "

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string/jumbo v11, " from within "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v3, v10}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    move-object v3, v0

    .line 169
    :goto_1
    invoke-static {}, Lorg/chromium/base/library_loader/Linker;->isUsingBrowserSharedRelros()Z
    :try_end_1
    .catch Ljava/lang/UnsatisfiedLinkError; {:try_start_1 .. :try_end_1} :catch_0

    move-result v0

    if-eqz v0, :cond_c

    .line 171
    if-eqz v3, :cond_3

    .line 172
    :try_start_2
    invoke-static {v3, v9}, Lorg/chromium/base/library_loader/Linker;->loadLibraryInZipFile(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/UnsatisfiedLinkError; {:try_start_2 .. :try_end_2} :catch_1

    :goto_2
    move v0, v2

    .line 183
    :goto_3
    if-nez v0, :cond_1

    .line 184
    if-eqz v3, :cond_4

    .line 185
    :try_start_3
    invoke-static {v3, v9}, Lorg/chromium/base/library_loader/Linker;->loadLibraryInZipFile(Ljava/lang/String;Ljava/lang/String;)V

    .line 159
    :cond_1
    :goto_4
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_0

    .line 165
    :cond_2
    const-string/jumbo v3, "LibraryLoader"

    new-instance v10, Ljava/lang/StringBuilder;

    const-string/jumbo v11, "Loading: "

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v3, v10}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catch Ljava/lang/UnsatisfiedLinkError; {:try_start_3 .. :try_end_3} :catch_0

    move-object v3, v0

    goto :goto_1

    .line 174
    :cond_3
    :try_start_4
    invoke-static {v9}, Lorg/chromium/base/library_loader/Linker;->loadLibrary(Ljava/lang/String;)V
    :try_end_4
    .catch Ljava/lang/UnsatisfiedLinkError; {:try_start_4 .. :try_end_4} :catch_1

    goto :goto_2

    .line 178
    :catch_1
    move-exception v0

    :try_start_5
    const-string/jumbo v0, "LibraryLoader"

    const-string/jumbo v10, "Failed to load native library with shared RELRO, retrying without"

    invoke-static {v0, v10}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v1

    .line 180
    invoke-static {}, Lorg/chromium/base/library_loader/Linker;->disableSharedRelros()V

    goto :goto_3

    .line 187
    :cond_4
    invoke-static {v9}, Lorg/chromium/base/library_loader/Linker;->loadLibrary(Ljava/lang/String;)V

    goto :goto_4

    .line 192
    :cond_5
    invoke-static {}, Lorg/chromium/base/library_loader/Linker;->finishLibraryLoad()V

    .line 210
    :cond_6
    if-eqz p0, :cond_7

    if-eqz p1, :cond_7

    sget-boolean v0, Lorg/chromium/base/library_loader/LibraryLoader;->sNativeLibraryHackWasUsed:Z

    if-nez v0, :cond_7

    .line 213
    invoke-static {p0}, Lorg/chromium/base/library_loader/LibraryLoaderHelper;->deleteWorkaroundLibrariesAsynchronously(Landroid/content/Context;)V

    .line 217
    :cond_7
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v4

    .line 218
    const-string/jumbo v0, "LibraryLoader"

    const-string/jumbo v3, "Time to load native libraries: %d ms (timestamps %d-%d)"

    const/4 v8, 0x3

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    sub-long v10, v4, v6

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    aput-object v10, v8, v9

    const/4 v9, 0x1

    const-wide/16 v10, 0x2710

    rem-long/2addr v6, v10

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v8, v9

    const/4 v6, 0x2

    const-wide/16 v10, 0x2710

    rem-long/2addr v4, v10

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v8, v6

    invoke-static {v3, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 223
    const/4 v0, 0x1

    sput-boolean v0, Lorg/chromium/base/library_loader/LibraryLoader;->sLoaded:Z
    :try_end_5
    .catch Ljava/lang/UnsatisfiedLinkError; {:try_start_5 .. :try_end_5} :catch_0

    .line 229
    :cond_8
    const-string/jumbo v0, "LibraryLoader"

    const-string/jumbo v3, "Expected native library version number \"%s\",actual native library version number \"%s\""

    new-array v4, v12, [Ljava/lang/Object;

    sget-object v5, Lorg/chromium/base/library_loader/NativeLibraries;->VERSION_NUMBER:Ljava/lang/String;

    aput-object v5, v4, v1

    invoke-static {}, Lorg/chromium/base/library_loader/LibraryLoader;->nativeGetVersionNumber()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v2

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 234
    sget-object v0, Lorg/chromium/base/library_loader/NativeLibraries;->VERSION_NUMBER:Ljava/lang/String;

    invoke-static {}, Lorg/chromium/base/library_loader/LibraryLoader;->nativeGetVersionNumber()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_b

    .line 235
    new-instance v0, Lorg/chromium/base/library_loader/ProcessInitException;

    invoke-direct {v0, v13}, Lorg/chromium/base/library_loader/ProcessInitException;-><init>(I)V

    throw v0

    .line 195
    :cond_9
    :try_start_6
    sget-object v3, Lorg/chromium/base/library_loader/NativeLibraries;->LIBRARIES:[Ljava/lang/String;

    array-length v4, v3

    move v0, v1

    :goto_5
    if-ge v0, v4, :cond_6

    aget-object v5, v3, v0
    :try_end_6
    .catch Ljava/lang/UnsatisfiedLinkError; {:try_start_6 .. :try_end_6} :catch_0

    .line 197
    :try_start_7
    invoke-static {v5}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V
    :try_end_7
    .catch Ljava/lang/UnsatisfiedLinkError; {:try_start_7 .. :try_end_7} :catch_2

    .line 195
    :goto_6
    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    .line 198
    :catch_2
    move-exception v8

    .line 199
    if-eqz p0, :cond_a

    :try_start_8
    invoke-static {p0, v5}, Lorg/chromium/base/library_loader/LibraryLoaderHelper;->tryLoadLibraryUsingWorkaround(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_a

    .line 202
    const/4 v5, 0x1

    sput-boolean v5, Lorg/chromium/base/library_loader/LibraryLoader;->sNativeLibraryHackWasUsed:Z

    goto :goto_6

    .line 204
    :cond_a
    throw v8
    :try_end_8
    .catch Ljava/lang/UnsatisfiedLinkError; {:try_start_8 .. :try_end_8} :catch_0

    .line 237
    :cond_b
    return-void

    :cond_c
    move v0, v1

    goto/16 :goto_3
.end method

.method public static loadNow(Landroid/content/Context;Z)V
    .locals 2

    .prologue
    .line 128
    sget-object v1, Lorg/chromium/base/library_loader/LibraryLoader;->sLock:Ljava/lang/Object;

    monitor-enter v1

    .line 129
    :try_start_0
    invoke-static {p0, p1}, Lorg/chromium/base/library_loader/LibraryLoader;->loadAlreadyLocked(Landroid/content/Context;Z)V

    .line 130
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private static native nativeGetVersionNumber()Ljava/lang/String;
.end method

.method private static native nativeInitCommandLine([Ljava/lang/String;)V
.end method

.method private static native nativeLibraryLoaded()Z
.end method

.method private static native nativeRecordChromiumAndroidLinkerHistogram(ZZ)V
.end method

.method private static native nativeRecordNativeLibraryHack(Z)V
.end method

.method public static onNativeInitializationComplete()V
    .locals 2

    .prologue
    .line 295
    invoke-static {}, Lorg/chromium/base/library_loader/Linker;->isUsed()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 296
    invoke-static {}, Lorg/chromium/base/library_loader/Linker;->loadAtFixedAddressFailed()Z

    move-result v0

    invoke-static {}, Lorg/chromium/base/SysUtils;->isLowEndDevice()Z

    move-result v1

    invoke-static {v0, v1}, Lorg/chromium/base/library_loader/LibraryLoader;->nativeRecordChromiumAndroidLinkerHistogram(ZZ)V

    .line 300
    :cond_0
    sget-boolean v0, Lorg/chromium/base/library_loader/LibraryLoader;->sNativeLibraryHackWasUsed:Z

    invoke-static {v0}, Lorg/chromium/base/library_loader/LibraryLoader;->nativeRecordNativeLibraryHack(Z)V

    .line 301
    return-void
.end method
