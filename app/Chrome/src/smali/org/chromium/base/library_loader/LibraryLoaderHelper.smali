.class public Lorg/chromium/base/library_loader/LibraryLoaderHelper;
.super Ljava/lang/Object;
.source "LibraryLoaderHelper.java"


# static fields
.field static final synthetic $assertionsDisabled:Z

.field private static sLibrariesWereUnpacked:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 27
    const-class v0, Lorg/chromium/base/library_loader/LibraryLoaderHelper;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/chromium/base/library_loader/LibraryLoaderHelper;->$assertionsDisabled:Z

    .line 36
    sput-boolean v1, Lorg/chromium/base/library_loader/LibraryLoaderHelper;->sLibrariesWereUnpacked:Z

    return-void

    :cond_0
    move v0, v1

    .line 27
    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static deleteDirectorySync(Ljava/io/File;)V
    .locals 7

    .prologue
    .line 237
    :try_start_0
    invoke-virtual {p0}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v1

    .line 238
    if-eqz v1, :cond_1

    .line 239
    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 240
    invoke-virtual {v3}, Ljava/io/File;->delete()Z

    move-result v4

    if-nez v4, :cond_0

    .line 241
    const-string/jumbo v4, "LibraryLoaderHelper"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string/jumbo v6, "Failed to remove "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v4, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 239
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 245
    :cond_1
    invoke-virtual {p0}, Ljava/io/File;->delete()Z

    move-result v0

    if-nez v0, :cond_2

    .line 246
    const-string/jumbo v0, "LibraryLoaderHelper"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "Failed to remove "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 252
    :cond_2
    :goto_1
    return-void

    .line 249
    :catch_0
    move-exception v0

    .line 250
    const-string/jumbo v1, "LibraryLoaderHelper"

    const-string/jumbo v2, "Failed to remove old libs, "

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method static deleteWorkaroundLibrariesAsynchronously(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 211
    new-instance v0, Lorg/chromium/base/library_loader/LibraryLoaderHelper$1;

    invoke-direct {v0, p0}, Lorg/chromium/base/library_loader/LibraryLoaderHelper$1;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0}, Lorg/chromium/base/library_loader/LibraryLoaderHelper$1;->start()V

    .line 217
    return-void
.end method

.method public static deleteWorkaroundLibrariesSynchronously(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 225
    invoke-static {p0}, Lorg/chromium/base/library_loader/LibraryLoaderHelper;->getWorkaroundLibDir(Landroid/content/Context;)Ljava/io/File;

    move-result-object v0

    .line 226
    invoke-static {v0}, Lorg/chromium/base/library_loader/LibraryLoaderHelper;->deleteDirectorySync(Ljava/io/File;)V

    .line 227
    return-void
.end method

.method private static getJniNameInApk(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 232
    new-instance v0, Ljava/lang/StringBuilder;

    const-string/jumbo v1, "lib/"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v1, Landroid/os/Build;->CPU_ABI:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p0}, Ljava/lang/System;->mapLibraryName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getWorkaroundLibDir(Landroid/content/Context;)Ljava/io/File;
    .locals 2

    .prologue
    .line 112
    const-string/jumbo v0, "lib"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Landroid/content/Context;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v0

    return-object v0
.end method

.method private static getWorkaroundLibFile(Landroid/content/Context;Ljava/lang/String;)Ljava/io/File;
    .locals 3

    .prologue
    .line 116
    invoke-static {p1}, Ljava/lang/System;->mapLibraryName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 117
    new-instance v1, Ljava/io/File;

    invoke-static {p0}, Lorg/chromium/base/library_loader/LibraryLoaderHelper;->getWorkaroundLibDir(Landroid/content/Context;)Ljava/io/File;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    return-object v1
.end method

.method static tryLoadLibraryUsingWorkaround(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 91
    sget-boolean v1, Lorg/chromium/base/library_loader/LibraryLoaderHelper;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    if-nez p0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 92
    :cond_0
    invoke-static {p0, p1}, Lorg/chromium/base/library_loader/LibraryLoaderHelper;->getWorkaroundLibFile(Landroid/content/Context;Ljava/lang/String;)Ljava/io/File;

    move-result-object v1

    .line 93
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_1

    invoke-static {p0}, Lorg/chromium/base/library_loader/LibraryLoaderHelper;->unpackLibrariesOnce(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 100
    :goto_0
    return v0

    .line 97
    :cond_1
    :try_start_0
    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/System;->load(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/UnsatisfiedLinkError; {:try_start_0 .. :try_end_0} :catch_0

    .line 98
    const/4 v0, 0x1

    goto :goto_0

    .line 100
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method private static unpackLibrariesOnce(Landroid/content/Context;)Z
    .locals 15

    .prologue
    const/4 v3, 0x0

    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 130
    sget-boolean v2, Lorg/chromium/base/library_loader/LibraryLoaderHelper;->sLibrariesWereUnpacked:Z

    if-eqz v2, :cond_0

    .line 199
    :goto_0
    return v0

    .line 133
    :cond_0
    sput-boolean v1, Lorg/chromium/base/library_loader/LibraryLoaderHelper;->sLibrariesWereUnpacked:Z

    .line 135
    invoke-static {p0}, Lorg/chromium/base/library_loader/LibraryLoaderHelper;->getWorkaroundLibDir(Landroid/content/Context;)Ljava/io/File;

    move-result-object v6

    .line 136
    invoke-static {v6}, Lorg/chromium/base/library_loader/LibraryLoaderHelper;->deleteDirectorySync(Ljava/io/File;)V

    .line 139
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v7

    .line 140
    new-instance v8, Ljava/util/zip/ZipFile;

    new-instance v2, Ljava/io/File;

    iget-object v4, v7, Landroid/content/pm/ApplicationInfo;->sourceDir:Ljava/lang/String;

    invoke-direct {v2, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    const/4 v4, 0x1

    invoke-direct {v8, v2, v4}, Ljava/util/zip/ZipFile;-><init>(Ljava/io/File;I)V

    .line 141
    sget-object v9, Lorg/chromium/base/library_loader/NativeLibraries;->LIBRARIES:[Ljava/lang/String;

    array-length v10, v9

    move v5, v0

    :goto_1
    if-ge v5, v10, :cond_a

    aget-object v2, v9, v5

    .line 142
    invoke-static {v2}, Lorg/chromium/base/library_loader/LibraryLoaderHelper;->getJniNameInApk(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 144
    invoke-virtual {v8, v4}, Ljava/util/zip/ZipFile;->getEntry(Ljava/lang/String;)Ljava/util/zip/ZipEntry;

    move-result-object v11

    .line 145
    if-nez v11, :cond_1

    .line 146
    const-string/jumbo v1, "LibraryLoaderHelper"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, v7, Landroid/content/pm/ApplicationInfo;->sourceDir:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " doesn\'t have file "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 147
    invoke-virtual {v8}, Ljava/util/zip/ZipFile;->close()V

    .line 148
    invoke-static {v6}, Lorg/chromium/base/library_loader/LibraryLoaderHelper;->deleteDirectorySync(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 196
    :catch_0
    move-exception v1

    .line 197
    const-string/jumbo v2, "LibraryLoaderHelper"

    const-string/jumbo v3, "Failed to unpack native libraries"

    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 198
    invoke-static {v6}, Lorg/chromium/base/library_loader/LibraryLoaderHelper;->deleteDirectorySync(Ljava/io/File;)V

    goto :goto_0

    .line 152
    :cond_1
    :try_start_1
    invoke-static {p0, v2}, Lorg/chromium/base/library_loader/LibraryLoaderHelper;->getWorkaroundLibFile(Landroid/content/Context;Ljava/lang/String;)Ljava/io/File;

    move-result-object v12

    .line 154
    const-string/jumbo v2, "LibraryLoaderHelper"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v13, "Extracting native libraries into "

    invoke-direct {v4, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v12}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v4, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 156
    sget-boolean v2, Lorg/chromium/base/library_loader/LibraryLoaderHelper;->$assertionsDisabled:Z

    if-nez v2, :cond_2

    invoke-virtual {v12}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_2

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 159
    :cond_2
    :try_start_2
    invoke-virtual {v12}, Ljava/io/File;->createNewFile()Z

    move-result v2

    if-nez v2, :cond_4

    .line 160
    new-instance v1, Ljava/io/IOException;

    invoke-direct {v1}, Ljava/io/IOException;-><init>()V

    throw v1
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    .line 184
    :catch_1
    move-exception v1

    .line 185
    :try_start_3
    invoke-virtual {v12}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 186
    invoke-virtual {v12}, Ljava/io/File;->delete()Z

    move-result v2

    if-nez v2, :cond_3

    .line 187
    const-string/jumbo v2, "LibraryLoaderHelper"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "Failed to delete "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v12}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 190
    :cond_3
    invoke-virtual {v8}, Ljava/util/zip/ZipFile;->close()V

    .line 191
    throw v1
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    .line 166
    :cond_4
    :try_start_4
    invoke-virtual {v8, v11}, Ljava/util/zip/ZipFile;->getInputStream(Ljava/util/zip/ZipEntry;)Ljava/io/InputStream;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_3

    move-result-object v4

    .line 167
    :try_start_5
    new-instance v2, Ljava/io/FileOutputStream;

    invoke-direct {v2, v12}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_4

    .line 169
    const/16 v11, 0x4000

    :try_start_6
    new-array v11, v11, [B

    .line 170
    :goto_2
    invoke-virtual {v4, v11}, Ljava/io/InputStream;->read([B)I

    move-result v13

    if-lez v13, :cond_7

    .line 171
    const/4 v14, 0x0

    invoke-virtual {v2, v11, v14, v13}, Ljava/io/FileOutputStream;->write([BII)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto :goto_2

    .line 179
    :catchall_0
    move-exception v1

    move-object v3, v4

    .line 175
    :goto_3
    if-eqz v3, :cond_5

    :try_start_7
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 177
    :cond_5
    if-eqz v2, :cond_6

    :try_start_8
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V

    :cond_6
    throw v1
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_1

    .line 175
    :cond_7
    if-eqz v4, :cond_8

    :try_start_9
    invoke-virtual {v4}, Ljava/io/InputStream;->close()V
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    .line 177
    :cond_8
    :try_start_a
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V

    .line 181
    const/4 v2, 0x1

    const/4 v4, 0x0

    invoke-virtual {v12, v2, v4}, Ljava/io/File;->setReadable(ZZ)Z

    .line 182
    const/4 v2, 0x1

    const/4 v4, 0x0

    invoke-virtual {v12, v2, v4}, Ljava/io/File;->setExecutable(ZZ)Z

    .line 183
    const/4 v2, 0x1

    invoke-virtual {v12, v2}, Ljava/io/File;->setWritable(Z)Z

    .line 141
    add-int/lit8 v2, v5, 0x1

    move v5, v2

    goto/16 :goto_1

    .line 177
    :catchall_1
    move-exception v1

    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V

    throw v1

    :catchall_2
    move-exception v1

    if-eqz v2, :cond_9

    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V

    :cond_9
    throw v1
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_1

    .line 194
    :cond_a
    :try_start_b
    invoke-virtual {v8}, Ljava/util/zip/ZipFile;->close()V
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_0

    move v0, v1

    .line 195
    goto/16 :goto_0

    .line 179
    :catchall_3
    move-exception v1

    move-object v2, v3

    goto :goto_3

    :catchall_4
    move-exception v1

    move-object v2, v3

    move-object v3, v4

    goto :goto_3
.end method
