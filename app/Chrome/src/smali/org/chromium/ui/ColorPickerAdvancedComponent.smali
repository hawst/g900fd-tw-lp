.class public Lorg/chromium/ui/ColorPickerAdvancedComponent;
.super Ljava/lang/Object;
.source "ColorPickerAdvancedComponent.java"


# instance fields
.field private mGradientColors:[I

.field private mGradientDrawable:Landroid/graphics/drawable/GradientDrawable;

.field private final mGradientView:Landroid/view/View;

.field private final mSeekBar:Landroid/widget/SeekBar;

.field private final mText:Landroid/widget/TextView;


# direct methods
.method constructor <init>(Landroid/view/View;IILandroid/widget/SeekBar$OnSeekBarChangeListener;)V
    .locals 3

    .prologue
    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    sget v0, Lorg/chromium/ui/R$id;->gradient:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lorg/chromium/ui/ColorPickerAdvancedComponent;->mGradientView:Landroid/view/View;

    .line 49
    sget v0, Lorg/chromium/ui/R$id;->text:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lorg/chromium/ui/ColorPickerAdvancedComponent;->mText:Landroid/widget/TextView;

    .line 50
    iget-object v0, p0, Lorg/chromium/ui/ColorPickerAdvancedComponent;->mText:Landroid/widget/TextView;

    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setText(I)V

    .line 51
    new-instance v0, Landroid/graphics/drawable/GradientDrawable;

    sget-object v1, Landroid/graphics/drawable/GradientDrawable$Orientation;->LEFT_RIGHT:Landroid/graphics/drawable/GradientDrawable$Orientation;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Landroid/graphics/drawable/GradientDrawable;-><init>(Landroid/graphics/drawable/GradientDrawable$Orientation;[I)V

    iput-object v0, p0, Lorg/chromium/ui/ColorPickerAdvancedComponent;->mGradientDrawable:Landroid/graphics/drawable/GradientDrawable;

    .line 52
    sget v0, Lorg/chromium/ui/R$id;->seek_bar:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/SeekBar;

    iput-object v0, p0, Lorg/chromium/ui/ColorPickerAdvancedComponent;->mSeekBar:Landroid/widget/SeekBar;

    .line 53
    iget-object v0, p0, Lorg/chromium/ui/ColorPickerAdvancedComponent;->mSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v0, p4}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    .line 54
    iget-object v0, p0, Lorg/chromium/ui/ColorPickerAdvancedComponent;->mSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v0, p3}, Landroid/widget/SeekBar;->setMax(I)V

    .line 57
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 58
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lorg/chromium/ui/R$drawable;->color_picker_advanced_select_handle:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    .line 61
    iget-object v1, p0, Lorg/chromium/ui/ColorPickerAdvancedComponent;->mSeekBar:Landroid/widget/SeekBar;

    div-int/lit8 v0, v0, 0x2

    invoke-virtual {v1, v0}, Landroid/widget/SeekBar;->setThumbOffset(I)V

    .line 62
    return-void
.end method


# virtual methods
.method public getValue()F
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lorg/chromium/ui/ColorPickerAdvancedComponent;->mSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v0}, Landroid/widget/SeekBar;->getProgress()I

    move-result v0

    int-to-float v0, v0

    return v0
.end method

.method public setGradientColors([I)V
    .locals 3

    .prologue
    .line 86
    invoke-virtual {p1}, [I->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [I

    iput-object v0, p0, Lorg/chromium/ui/ColorPickerAdvancedComponent;->mGradientColors:[I

    .line 87
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-ge v0, v1, :cond_0

    .line 88
    sget-object v0, Landroid/graphics/drawable/GradientDrawable$Orientation;->LEFT_RIGHT:Landroid/graphics/drawable/GradientDrawable$Orientation;

    .line 89
    new-instance v1, Landroid/graphics/drawable/GradientDrawable;

    iget-object v2, p0, Lorg/chromium/ui/ColorPickerAdvancedComponent;->mGradientColors:[I

    invoke-direct {v1, v0, v2}, Landroid/graphics/drawable/GradientDrawable;-><init>(Landroid/graphics/drawable/GradientDrawable$Orientation;[I)V

    iput-object v1, p0, Lorg/chromium/ui/ColorPickerAdvancedComponent;->mGradientDrawable:Landroid/graphics/drawable/GradientDrawable;

    .line 93
    :goto_0
    iget-object v0, p0, Lorg/chromium/ui/ColorPickerAdvancedComponent;->mGradientView:Landroid/view/View;

    iget-object v1, p0, Lorg/chromium/ui/ColorPickerAdvancedComponent;->mGradientDrawable:Landroid/graphics/drawable/GradientDrawable;

    invoke-static {v0, v1}, Lorg/chromium/base/ApiCompatibilityUtils;->setBackgroundForView(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 94
    return-void

    .line 91
    :cond_0
    iget-object v0, p0, Lorg/chromium/ui/ColorPickerAdvancedComponent;->mGradientDrawable:Landroid/graphics/drawable/GradientDrawable;

    iget-object v1, p0, Lorg/chromium/ui/ColorPickerAdvancedComponent;->mGradientColors:[I

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/GradientDrawable;->setColors([I)V

    goto :goto_0
.end method

.method public setValue(F)V
    .locals 2

    .prologue
    .line 77
    iget-object v0, p0, Lorg/chromium/ui/ColorPickerAdvancedComponent;->mSeekBar:Landroid/widget/SeekBar;

    float-to-int v1, p1

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 78
    return-void
.end method
