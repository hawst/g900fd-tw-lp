.class public final Lorg/chromium/ui/R$drawable;
.super Ljava/lang/Object;
.source "R.java"


# static fields
.field public static final abc_ab_share_pack_holo_dark:I = 0x7f020000

.field public static final abc_ab_share_pack_holo_light:I = 0x7f020001

.field public static final abc_btn_check_material:I = 0x7f020002

.field public static final abc_btn_check_to_on_mtrl_000:I = 0x7f020003

.field public static final abc_btn_check_to_on_mtrl_015:I = 0x7f020004

.field public static final abc_btn_radio_material:I = 0x7f020005

.field public static final abc_btn_radio_to_on_mtrl_000:I = 0x7f020006

.field public static final abc_btn_radio_to_on_mtrl_015:I = 0x7f020007

.field public static final abc_btn_switch_to_on_mtrl_00001:I = 0x7f020008

.field public static final abc_btn_switch_to_on_mtrl_00012:I = 0x7f020009

.field public static final abc_cab_background_internal_bg:I = 0x7f02000a

.field public static final abc_cab_background_top_material:I = 0x7f02000b

.field public static final abc_cab_background_top_mtrl_alpha:I = 0x7f02000c

.field public static final abc_edit_text_material:I = 0x7f02000d

.field public static final abc_ic_ab_back_mtrl_am_alpha:I = 0x7f02000e

.field public static final abc_ic_clear_mtrl_alpha:I = 0x7f02000f

.field public static final abc_ic_commit_search_api_mtrl_alpha:I = 0x7f020010

.field public static final abc_ic_go_search_api_mtrl_alpha:I = 0x7f020011

.field public static final abc_ic_menu_copy_mtrl_am_alpha:I = 0x7f020012

.field public static final abc_ic_menu_cut_mtrl_alpha:I = 0x7f020013

.field public static final abc_ic_menu_moreoverflow_mtrl_alpha:I = 0x7f020014

.field public static final abc_ic_menu_paste_mtrl_am_alpha:I = 0x7f020015

.field public static final abc_ic_menu_selectall_mtrl_alpha:I = 0x7f020016

.field public static final abc_ic_menu_share_mtrl_alpha:I = 0x7f020017

.field public static final abc_ic_search_api_mtrl_alpha:I = 0x7f020018

.field public static final abc_ic_voice_search_api_mtrl_alpha:I = 0x7f020019

.field public static final abc_item_background_holo_dark:I = 0x7f02001a

.field public static final abc_item_background_holo_light:I = 0x7f02001b

.field public static final abc_list_divider_mtrl_alpha:I = 0x7f02001c

.field public static final abc_list_focused_holo:I = 0x7f02001d

.field public static final abc_list_longpressed_holo:I = 0x7f02001e

.field public static final abc_list_pressed_holo_dark:I = 0x7f02001f

.field public static final abc_list_pressed_holo_light:I = 0x7f020020

.field public static final abc_list_selector_background_transition_holo_dark:I = 0x7f020021

.field public static final abc_list_selector_background_transition_holo_light:I = 0x7f020022

.field public static final abc_list_selector_disabled_holo_dark:I = 0x7f020023

.field public static final abc_list_selector_disabled_holo_light:I = 0x7f020024

.field public static final abc_list_selector_holo_dark:I = 0x7f020025

.field public static final abc_list_selector_holo_light:I = 0x7f020026

.field public static final abc_menu_hardkey_panel_mtrl_mult:I = 0x7f020027

.field public static final abc_popup_background_mtrl_mult:I = 0x7f020028

.field public static final abc_spinner_mtrl_am_alpha:I = 0x7f020029

.field public static final abc_switch_thumb_material:I = 0x7f02002a

.field public static final abc_switch_track_mtrl_alpha:I = 0x7f02002b

.field public static final abc_tab_indicator_material:I = 0x7f02002c

.field public static final abc_tab_indicator_mtrl_alpha:I = 0x7f02002d

.field public static final abc_textfield_activated_mtrl_alpha:I = 0x7f02002e

.field public static final abc_textfield_default_mtrl_alpha:I = 0x7f02002f

.field public static final abc_textfield_search_activated_mtrl_alpha:I = 0x7f020030

.field public static final abc_textfield_search_default_mtrl_alpha:I = 0x7f020031

.field public static final abc_textfield_search_material:I = 0x7f020032

.field public static final accessibility_tab_switcher_divider:I = 0x7f020033

.field public static final account_management_incognito:I = 0x7f020034

.field public static final account_management_no_picture:I = 0x7f020035

.field public static final account_management_plus:I = 0x7f020036

.field public static final app_banner_button_close:I = 0x7f020037

.field public static final app_banner_button_install:I = 0x7f020038

.field public static final app_banner_button_open:I = 0x7f020039

.field public static final bg_bookmarks_widget_holo:I = 0x7f02003a

.field public static final bg_find_toolbar_popup:I = 0x7f02003b

.field public static final bg_ntp_search_box:I = 0x7f02003c

.field public static final bg_tabstrip_background_tab:I = 0x7f02003d

.field public static final bg_tabstrip_fader_transition:I = 0x7f02003e

.field public static final bg_tabstrip_incognito_background_tab:I = 0x7f02003f

.field public static final bg_tabstrip_incognito_tab:I = 0x7f020040

.field public static final bg_tabstrip_tab:I = 0x7f020041

.field public static final bookmark_widget_preview:I = 0x7f020042

.field public static final bookmark_widget_thumb_selector:I = 0x7f020043

.field public static final bookmarks_folder_icon:I = 0x7f020044

.field public static final bookmarks_widget_thumb_selector_pressed:I = 0x7f020045

.field public static final border_thumb_bookmarks_widget_holo:I = 0x7f020046

.field public static final breadcrumb_arrow:I = 0x7f020047

.field public static final browser_thumbnail:I = 0x7f020048

.field public static final btn_back:I = 0x7f020049

.field public static final btn_back_disabled:I = 0x7f02004a

.field public static final btn_back_normal:I = 0x7f02004b

.field public static final btn_back_pressed:I = 0x7f02004c

.field public static final btn_back_white:I = 0x7f02004d

.field public static final btn_back_white_disabled:I = 0x7f02004e

.field public static final btn_back_white_normal:I = 0x7f02004f

.field public static final btn_back_white_pressed:I = 0x7f020050

.field public static final btn_delete_url:I = 0x7f020051

.field public static final btn_delete_url_normal:I = 0x7f020052

.field public static final btn_delete_url_pressed:I = 0x7f020053

.field public static final btn_delete_url_white:I = 0x7f020054

.field public static final btn_delete_url_white_normal:I = 0x7f020055

.field public static final btn_delete_url_white_pressed:I = 0x7f020056

.field public static final btn_enhanced_bookmark_auto_tag:I = 0x7f020057

.field public static final btn_find_next:I = 0x7f020058

.field public static final btn_find_next_disabled:I = 0x7f020059

.field public static final btn_find_next_normal:I = 0x7f02005a

.field public static final btn_find_next_pressed:I = 0x7f02005b

.field public static final btn_find_next_white:I = 0x7f02005c

.field public static final btn_find_next_white_disabled:I = 0x7f02005d

.field public static final btn_find_next_white_normal:I = 0x7f02005e

.field public static final btn_find_next_white_pressed:I = 0x7f02005f

.field public static final btn_find_prev:I = 0x7f020060

.field public static final btn_find_prev_disabled:I = 0x7f020061

.field public static final btn_find_prev_normal:I = 0x7f020062

.field public static final btn_find_prev_pressed:I = 0x7f020063

.field public static final btn_find_prev_white:I = 0x7f020064

.field public static final btn_find_prev_white_disabled:I = 0x7f020065

.field public static final btn_find_prev_white_normal:I = 0x7f020066

.field public static final btn_find_prev_white_pressed:I = 0x7f020067

.field public static final btn_flywheel_promo_close:I = 0x7f020068

.field public static final btn_forward:I = 0x7f020069

.field public static final btn_forward_disabled:I = 0x7f02006a

.field public static final btn_forward_normal:I = 0x7f02006b

.field public static final btn_forward_pressed:I = 0x7f02006c

.field public static final btn_forward_white:I = 0x7f02006d

.field public static final btn_forward_white_disabled:I = 0x7f02006e

.field public static final btn_forward_white_normal:I = 0x7f02006f

.field public static final btn_forward_white_pressed:I = 0x7f020070

.field public static final btn_grey:I = 0x7f020071

.field public static final btn_grey_normal:I = 0x7f020072

.field public static final btn_grey_pressed:I = 0x7f020073

.field public static final btn_hotdogs:I = 0x7f020074

.field public static final btn_infobar_blue:I = 0x7f020075

.field public static final btn_menu:I = 0x7f020076

.field public static final btn_menu_disabled:I = 0x7f020077

.field public static final btn_menu_normal:I = 0x7f020078

.field public static final btn_menu_pressed:I = 0x7f020079

.field public static final btn_menu_white:I = 0x7f02007a

.field public static final btn_menu_white_disabled:I = 0x7f02007b

.field public static final btn_menu_white_normal:I = 0x7f02007c

.field public static final btn_menu_white_pressed:I = 0x7f02007d

.field public static final btn_new_tab_incognito:I = 0x7f02007e

.field public static final btn_new_tab_incognito_normal:I = 0x7f02007f

.field public static final btn_new_tab_incognito_pressed:I = 0x7f020080

.field public static final btn_new_tab_white:I = 0x7f020081

.field public static final btn_new_tab_white_normal:I = 0x7f020082

.field public static final btn_new_tab_white_pressed:I = 0x7f020083

.field public static final btn_omnibox_mic:I = 0x7f020084

.field public static final btn_omnibox_mic_normal:I = 0x7f020085

.field public static final btn_omnibox_mic_pressed:I = 0x7f020086

.field public static final btn_omnibox_mic_white:I = 0x7f020087

.field public static final btn_omnibox_mic_white_normal:I = 0x7f020088

.field public static final btn_omnibox_mic_white_pressed:I = 0x7f020089

.field public static final btn_reader_mode:I = 0x7f02008a

.field public static final btn_reader_mode_active:I = 0x7f02008b

.field public static final btn_reader_mode_normal:I = 0x7f02008c

.field public static final btn_recents:I = 0x7f02008d

.field public static final btn_recents_normal:I = 0x7f02008e

.field public static final btn_recents_pressed:I = 0x7f02008f

.field public static final btn_reload_stop:I = 0x7f020090

.field public static final btn_star:I = 0x7f020091

.field public static final btn_star_empty:I = 0x7f020092

.field public static final btn_star_filled:I = 0x7f020093

.field public static final btn_star_full:I = 0x7f020094

.field public static final btn_star_half:I = 0x7f020095

.field public static final btn_star_normal:I = 0x7f020096

.field public static final btn_star_pressed:I = 0x7f020097

.field public static final btn_star_white:I = 0x7f020098

.field public static final btn_star_white_filled:I = 0x7f020099

.field public static final btn_star_white_normal:I = 0x7f02009a

.field public static final btn_star_white_pressed:I = 0x7f02009b

.field public static final btn_suggestion_refine:I = 0x7f02009c

.field public static final btn_suggestion_refine_normal:I = 0x7f02009d

.field public static final btn_suggestion_refine_pressed:I = 0x7f02009e

.field public static final btn_suggestion_refine_white:I = 0x7f02009f

.field public static final btn_suggestion_refine_white_normal:I = 0x7f0200a0

.field public static final btn_suggestion_refine_white_pressed:I = 0x7f0200a1

.field public static final btn_tab_close:I = 0x7f0200a2

.field public static final btn_tab_close_normal:I = 0x7f0200a3

.field public static final btn_tab_close_pressed:I = 0x7f0200a4

.field public static final btn_tab_close_white:I = 0x7f0200a5

.field public static final btn_tab_close_white_normal:I = 0x7f0200a6

.field public static final btn_tab_close_white_pressed:I = 0x7f0200a7

.field public static final btn_tabstrip_incognito_switch:I = 0x7f0200a8

.field public static final btn_tabstrip_incognito_switch_incognito:I = 0x7f0200a9

.field public static final btn_tabstrip_new_incognito_tab:I = 0x7f0200aa

.field public static final btn_tabstrip_new_incognito_tab_normal:I = 0x7f0200ab

.field public static final btn_tabstrip_new_tab:I = 0x7f0200ac

.field public static final btn_tabstrip_new_tab_normal:I = 0x7f0200ad

.field public static final btn_tabstrip_new_tab_pressed:I = 0x7f0200ae

.field public static final btn_tabstrip_switch_incognito:I = 0x7f0200af

.field public static final btn_tabstrip_switch_normal:I = 0x7f0200b0

.field public static final btn_tabswitcher_single:I = 0x7f0200b1

.field public static final btn_tabswitcher_single_disabled:I = 0x7f0200b2

.field public static final btn_tabswitcher_single_normal:I = 0x7f0200b3

.field public static final btn_tabswitcher_single_pressed:I = 0x7f0200b4

.field public static final btn_tabswitcher_single_white:I = 0x7f0200b5

.field public static final btn_tabswitcher_single_white_disabled:I = 0x7f0200b6

.field public static final btn_tabswitcher_single_white_normal:I = 0x7f0200b7

.field public static final btn_tabswitcher_single_white_pressed:I = 0x7f0200b8

.field public static final btn_toolbar_home:I = 0x7f0200b9

.field public static final btn_toolbar_home_disabled:I = 0x7f0200ba

.field public static final btn_toolbar_home_normal:I = 0x7f0200bb

.field public static final btn_toolbar_home_pressed:I = 0x7f0200bc

.field public static final btn_toolbar_home_white:I = 0x7f0200bd

.field public static final btn_toolbar_home_white_disabled:I = 0x7f0200be

.field public static final btn_toolbar_home_white_normal:I = 0x7f0200bf

.field public static final btn_toolbar_home_white_pressed:I = 0x7f0200c0

.field public static final btn_toolbar_reload:I = 0x7f0200c1

.field public static final btn_toolbar_reload_disabled:I = 0x7f0200c2

.field public static final btn_toolbar_reload_normal:I = 0x7f0200c3

.field public static final btn_toolbar_reload_pressed:I = 0x7f0200c4

.field public static final btn_toolbar_reload_white:I = 0x7f0200c5

.field public static final btn_toolbar_reload_white_disabled:I = 0x7f0200c6

.field public static final btn_toolbar_reload_white_normal:I = 0x7f0200c7

.field public static final btn_toolbar_reload_white_pressed:I = 0x7f0200c8

.field public static final btn_toolbar_stop_loading:I = 0x7f0200c9

.field public static final btn_toolbar_stop_loading_disabled:I = 0x7f0200ca

.field public static final btn_toolbar_stop_loading_normal:I = 0x7f0200cb

.field public static final btn_toolbar_stop_loading_pressed:I = 0x7f0200cc

.field public static final btn_toolbar_stop_loading_white:I = 0x7f0200cd

.field public static final btn_toolbar_stop_loading_white_disabled:I = 0x7f0200ce

.field public static final btn_toolbar_stop_loading_white_normal:I = 0x7f0200cf

.field public static final btn_toolbar_stop_loading_white_pressed:I = 0x7f0200d0

.field public static final bubble:I = 0x7f0200d1

.field public static final bubble_arrow_up:I = 0x7f0200d2

.field public static final bubble_point_white:I = 0x7f0200d3

.field public static final bubble_white:I = 0x7f0200d4

.field public static final card_background_default:I = 0x7f0200d5

.field public static final chrome_logo:I = 0x7f0200d6

.field public static final close_button:I = 0x7f0200d7

.field public static final close_button_pressed:I = 0x7f0200d8

.field public static final color_button_background:I = 0x7f0200d9

.field public static final color_picker_advanced_select_handle:I = 0x7f0200da

.field public static final color_picker_border:I = 0x7f0200db

.field public static final contextual_search_bar_background:I = 0x7f0200dc

.field public static final contextual_search_icon:I = 0x7f0200dd

.field public static final contextual_search_provider_icon:I = 0x7f0200de

.field public static final contextual_search_scroll_shadow:I = 0x7f0200df

.field public static final controlled_setting_mandatory:I = 0x7f0200e0

.field public static final data_grid:I = 0x7f0200e1

.field public static final data_grid_border:I = 0x7f0200e2

.field public static final data_reduction_proxy_unreachable_warning:I = 0x7f0200e3

.field public static final default_favicon:I = 0x7f0200e4

.field public static final default_favicon_white:I = 0x7f0200e5

.field public static final distillation_quality_answer_no:I = 0x7f0200e6

.field public static final distillation_quality_answer_no_faded:I = 0x7f0200e7

.field public static final distillation_quality_answer_no_pressed:I = 0x7f0200e8

.field public static final distillation_quality_answer_yes:I = 0x7f0200e9

.field public static final distillation_quality_answer_yes_faded:I = 0x7f0200ea

.field public static final distillation_quality_answer_yes_pressed:I = 0x7f0200eb

.field public static final distilled_page_pref_background:I = 0x7f0200ec

.field public static final distilled_page_prefs_button_bg:I = 0x7f0200ed

.field public static final doc_omnibox:I = 0x7f0200ee

.field public static final doc_omnibox_active:I = 0x7f0200ef

.field public static final doc_omnibox_https_valid:I = 0x7f0200f0

.field public static final doc_omnibox_https_warning:I = 0x7f0200f1

.field public static final doc_omnibox_transparent:I = 0x7f0200f2

.field public static final dropdown_popup_background:I = 0x7f0200f3

.field public static final dropdown_popup_background_down:I = 0x7f0200f4

.field public static final dropdown_popup_background_up:I = 0x7f0200f5

.field public static final eb_add_folder:I = 0x7f0200f6

.field public static final eb_back_normal:I = 0x7f0200f7

.field public static final eb_cancel_active:I = 0x7f0200f8

.field public static final eb_cancel_normal:I = 0x7f0200f9

.field public static final eb_check_blue:I = 0x7f0200fa

.field public static final eb_check_gray:I = 0x7f0200fb

.field public static final eb_check_white:I = 0x7f0200fc

.field public static final eb_delete_normal:I = 0x7f0200fd

.field public static final eb_delete_white:I = 0x7f0200fe

.field public static final eb_drawer_list_text_color:I = 0x7f0200ff

.field public static final eb_edit_normal:I = 0x7f020100

.field public static final eb_filter_normal:I = 0x7f020101

.field public static final eb_filter_pressed:I = 0x7f020102

.field public static final eb_folder_normal:I = 0x7f020103

.field public static final eb_folder_pressed:I = 0x7f020104

.field public static final eb_item_more:I = 0x7f020105

.field public static final eb_item_tile:I = 0x7f020106

.field public static final eb_menu_normal:I = 0x7f020107

.field public static final eb_navigation_item_filter:I = 0x7f020108

.field public static final eb_navigation_item_folder:I = 0x7f020109

.field public static final eb_navigation_item_star:I = 0x7f02010a

.field public static final eb_navigation_item_uncategorized:I = 0x7f02010b

.field public static final eb_search_normal:I = 0x7f02010c

.field public static final eb_star_normal:I = 0x7f02010d

.field public static final eb_star_pressed:I = 0x7f02010e

.field public static final eb_title_bar_shadow:I = 0x7f02010f

.field public static final eb_uncategorized_normal:I = 0x7f020110

.field public static final eb_uncategorized_pressed:I = 0x7f020111

.field public static final edge_menu_bg:I = 0x7f020112

.field public static final folder_icon:I = 0x7f020113

.field public static final folder_icon_normal:I = 0x7f020114

.field public static final folder_icon_pressed:I = 0x7f020115

.field public static final font_preview_background:I = 0x7f020116

.field public static final fre_checkmark:I = 0x7f020117

.field public static final fre_chrome_logo:I = 0x7f020118

.field public static final fre_new_recents_menu:I = 0x7f020119

.field public static final fre_new_recents_menu_loupe:I = 0x7f02011a

.field public static final fre_placeholder:I = 0x7f02011b

.field public static final globe_favicon:I = 0x7f02011c

.field public static final globe_incognito_favicon:I = 0x7f02011d

.field public static final google_logo:I = 0x7f02011e

.field public static final google_play_logo:I = 0x7f02011f

.field public static final graph_illustration:I = 0x7f020120

.field public static final history_favicon:I = 0x7f020121

.field public static final ic_bookmark_widget_bookmark_holo_dark:I = 0x7f020122

.field public static final ic_cast_dark:I = 0x7f020123

.field public static final ic_cast_dark_off:I = 0x7f020124

.field public static final ic_cast_dark_on:I = 0x7f020125

.field public static final ic_list_data_empty:I = 0x7f020126

.field public static final ic_list_data_large:I = 0x7f020127

.field public static final ic_list_data_small:I = 0x7f020128

.field public static final ic_location_allowed:I = 0x7f020129

.field public static final ic_location_denied:I = 0x7f02012a

.field public static final ic_menu_copy_holo_light:I = 0x7f02012b

.field public static final ic_menu_cut_holo_light:I = 0x7f02012c

.field public static final ic_menu_paste_holo_light:I = 0x7f02012d

.field public static final ic_menu_revert_holo_dark:I = 0x7f02012e

.field public static final ic_menu_search_holo_light:I = 0x7f02012f

.field public static final ic_menu_selectall_holo_light:I = 0x7f020130

.field public static final ic_menu_share_holo_light:I = 0x7f020131

.field public static final ic_midi_allowed:I = 0x7f020132

.field public static final ic_midi_denied:I = 0x7f020133

.field public static final ic_most_visited_placeholder:I = 0x7f020134

.field public static final ic_notification_media_route:I = 0x7f020135

.field public static final ic_omnibox_incognito_badge:I = 0x7f020136

.field public static final ic_omnibox_magnifier:I = 0x7f020137

.field public static final ic_omnibox_page:I = 0x7f020138

.field public static final ic_stat_notify:I = 0x7f020139

.field public static final ic_suggestion_bookmark:I = 0x7f02013a

.field public static final ic_suggestion_bookmark_white:I = 0x7f02013b

.field public static final ic_suggestion_history:I = 0x7f02013c

.field public static final ic_suggestion_history_white:I = 0x7f02013d

.field public static final ic_suggestion_magnifier:I = 0x7f02013e

.field public static final ic_suggestion_magnifier_white:I = 0x7f02013f

.field public static final ic_suggestion_page:I = 0x7f020140

.field public static final ic_suggestion_page_white:I = 0x7f020141

.field public static final ic_uca_indicator:I = 0x7f020142

.field public static final ic_vidcontrol_pause:I = 0x7f020143

.field public static final ic_vidcontrol_play:I = 0x7f020144

.field public static final ic_vidcontrol_stop:I = 0x7f020145

.field public static final incognito_splash:I = 0x7f020146

.field public static final incognito_statusbar:I = 0x7f020147

.field public static final infobar_autofill_cc:I = 0x7f020148

.field public static final infobar_blocked_popups:I = 0x7f020149

.field public static final infobar_camera:I = 0x7f02014a

.field public static final infobar_close_button:I = 0x7f02014b

.field public static final infobar_downloading:I = 0x7f02014c

.field public static final infobar_geolocation:I = 0x7f02014d

.field public static final infobar_microphone:I = 0x7f02014e

.field public static final infobar_midi:I = 0x7f02014f

.field public static final infobar_multiple_downloads:I = 0x7f020150

.field public static final infobar_protected_media_identifier:I = 0x7f020151

.field public static final infobar_savepassword_autologin:I = 0x7f020152

.field public static final infobar_translate:I = 0x7f020153

.field public static final infobar_update_uma:I = 0x7f020154

.field public static final infobar_warning:I = 0x7f020155

.field public static final logo_card_back:I = 0x7f020156

.field public static final managed_folder_icon:I = 0x7f020157

.field public static final managed_folder_icon_normal:I = 0x7f020158

.field public static final managed_folder_icon_pressed:I = 0x7f020159

.field public static final menu_bg:I = 0x7f02015a

.field public static final mic_off:I = 0x7f02015b

.field public static final mic_on:I = 0x7f02015c

.field public static final missing:I = 0x7f02015d

.field public static final more_horiz:I = 0x7f02015e

.field public static final most_visited_item_bg:I = 0x7f02015f

.field public static final most_visited_item_empty:I = 0x7f020160

.field public static final most_visited_thumbnail_placeholder:I = 0x7f020161

.field public static final mr_ic_audio_vol:I = 0x7f020162

.field public static final mr_ic_media_route_connecting_holo_dark:I = 0x7f020163

.field public static final mr_ic_media_route_connecting_holo_light:I = 0x7f020164

.field public static final mr_ic_media_route_disabled_holo_dark:I = 0x7f020165

.field public static final mr_ic_media_route_disabled_holo_light:I = 0x7f020166

.field public static final mr_ic_media_route_holo_dark:I = 0x7f020167

.field public static final mr_ic_media_route_holo_light:I = 0x7f020168

.field public static final mr_ic_media_route_off_holo_dark:I = 0x7f020169

.field public static final mr_ic_media_route_off_holo_light:I = 0x7f02016a

.field public static final mr_ic_media_route_on_0_holo_dark:I = 0x7f02016b

.field public static final mr_ic_media_route_on_0_holo_light:I = 0x7f02016c

.field public static final mr_ic_media_route_on_1_holo_dark:I = 0x7f02016d

.field public static final mr_ic_media_route_on_1_holo_light:I = 0x7f02016e

.field public static final mr_ic_media_route_on_2_holo_dark:I = 0x7f02016f

.field public static final mr_ic_media_route_on_2_holo_light:I = 0x7f020170

.field public static final mr_ic_media_route_on_holo_dark:I = 0x7f020171

.field public static final mr_ic_media_route_on_holo_light:I = 0x7f020172

.field public static final notification_icon_bg:I = 0x7f0201ce

.field public static final ntp_chrome_to_mobile_group_icon:I = 0x7f020173

.field public static final ntp_toolbar_button_background:I = 0x7f020174

.field public static final ntp_toolbar_button_background_pressed_img:I = 0x7f020175

.field public static final ntp_toolbar_button_background_selected:I = 0x7f020176

.field public static final ntp_toolbar_button_background_selected_img:I = 0x7f020177

.field public static final ntp_toolbar_button_background_selected_pressed_img:I = 0x7f020178

.field public static final omnibox_https_invalid:I = 0x7f020179

.field public static final omnibox_https_valid:I = 0x7f02017a

.field public static final omnibox_https_warning:I = 0x7f02017b

.field public static final ondemand_overlay:I = 0x7f02017c

.field public static final overlay_url_bookmark_widget_holo:I = 0x7f02017d

.field public static final pageinfo_bad:I = 0x7f02017e

.field public static final pageinfo_enterprise_managed:I = 0x7f02017f

.field public static final pageinfo_good:I = 0x7f020180

.field public static final pageinfo_info:I = 0x7f020181

.field public static final pageinfo_warning_major:I = 0x7f020182

.field public static final pageinfo_warning_minor:I = 0x7f020183

.field public static final paw:I = 0x7f020184

.field public static final progress_bar:I = 0x7f020185

.field public static final progress_bar_background:I = 0x7f020186

.field public static final progress_bar_background_white:I = 0x7f020187

.field public static final progress_bar_foreground:I = 0x7f020188

.field public static final progress_bar_white:I = 0x7f020189

.field public static final reader_mode_prefs_icon:I = 0x7f02018a

.field public static final recent_arrow:I = 0x7f02018b

.field public static final recent_arrow_normal:I = 0x7f02018c

.field public static final recent_arrow_pressed:I = 0x7f02018d

.field public static final recent_laptop:I = 0x7f02018e

.field public static final recent_laptop_normal:I = 0x7f02018f

.field public static final recent_laptop_pressed:I = 0x7f020190

.field public static final recent_phone:I = 0x7f020191

.field public static final recent_phone_normal:I = 0x7f020192

.field public static final recent_phone_pressed:I = 0x7f020193

.field public static final recent_recently_closed:I = 0x7f020194

.field public static final recent_recently_closed_normal:I = 0x7f020195

.field public static final recent_recently_closed_pressed:I = 0x7f020196

.field public static final recent_tablet:I = 0x7f020197

.field public static final recent_tablet_normal:I = 0x7f020198

.field public static final recent_tablet_pressed:I = 0x7f020199

.field public static final sad_tab:I = 0x7f02019a

.field public static final sad_tab_background:I = 0x7f02019b

.field public static final sad_tab_background_inner:I = 0x7f02019c

.field public static final sad_tab_lower_panel_background:I = 0x7f02019d

.field public static final sad_tab_reload_button:I = 0x7f02019e

.field public static final sad_tab_reload_button_normal:I = 0x7f02019f

.field public static final sad_tab_reload_button_pressed:I = 0x7f0201a0

.field public static final search_google_logo:I = 0x7f0201a1

.field public static final signin_promo_illustration:I = 0x7f0201a2

.field public static final snapshot_prefix_background:I = 0x7f0201a3

.field public static final spinner:I = 0x7f0201a4

.field public static final spinner_white:I = 0x7f0201a5

.field public static final tab_close_white:I = 0x7f0201a6

.field public static final tab_close_white_active:I = 0x7f0201a7

.field public static final tabstrip_background_incognito_tab_text_truncator:I = 0x7f0201a8

.field public static final tabstrip_background_tab_text_truncator:I = 0x7f0201a9

.field public static final tabstrip_foreground_incognito_tab_text_truncator:I = 0x7f0201aa

.field public static final tabstrip_foreground_tab_text_truncator:I = 0x7f0201ab

.field public static final tabstrip_incognito_switch_incognito:I = 0x7f0201ac

.field public static final tabstrip_incognito_switch_incognito_active:I = 0x7f0201ad

.field public static final tabstrip_incognito_switch_normal:I = 0x7f0201ae

.field public static final tabstrip_incognito_switch_normal_active:I = 0x7f0201af

.field public static final tabswitcher_border_frame:I = 0x7f0201b0

.field public static final tabswitcher_border_frame_decoration:I = 0x7f0201b1

.field public static final tabswitcher_border_frame_incognito:I = 0x7f0201b2

.field public static final tabswitcher_border_frame_shadow:I = 0x7f0201b3

.field public static final textbox:I = 0x7f0201b4

.field public static final throbber_animation:I = 0x7f0201b5

.field public static final thumb_bookmark_widget_folder_back_holo:I = 0x7f0201b6

.field public static final thumb_bookmark_widget_folder_holo:I = 0x7f0201b7

.field public static final thumbnail_bookmarks_widget_no_bookmark_holo:I = 0x7f0201b8

.field public static final toolbar:I = 0x7f0201b9

.field public static final toolbar_background:I = 0x7f0201ba

.field public static final toolbar_incognito:I = 0x7f0201bb

.field public static final toolbar_normal:I = 0x7f0201bc

.field public static final toolbar_shadow:I = 0x7f0201bd

.field public static final toolbar_shadow_focused:I = 0x7f0201be

.field public static final toolbar_shadow_normal:I = 0x7f0201bf

.field public static final webrtc_audio:I = 0x7f0201c0

.field public static final webrtc_audio_settings:I = 0x7f0201c1

.field public static final webrtc_audio_settings_denied:I = 0x7f0201c2

.field public static final webrtc_video:I = 0x7f0201c3

.field public static final webrtc_video_settings:I = 0x7f0201c4

.field public static final webrtc_video_settings_denied:I = 0x7f0201c5

.field public static final website_location:I = 0x7f0201c6

.field public static final website_midi:I = 0x7f0201c7

.field public static final website_popups:I = 0x7f0201c8

.field public static final website_protected_media_identifier:I = 0x7f0201c9

.field public static final website_settings_reset_cert_decisions:I = 0x7f0201ca

.field public static final website_storage_usage:I = 0x7f0201cb

.field public static final website_voice_and_video_capture:I = 0x7f0201cc

.field public static final window_background:I = 0x7f0201cd


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2948
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
