.class public Lorg/chromium/ui/autofill/AutofillPopup;
.super Lorg/chromium/ui/DropdownPopupWindow;
.source "AutofillPopup.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;
.implements Landroid/widget/PopupWindow$OnDismissListener;


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private final mAutofillCallback:Lorg/chromium/ui/autofill/AutofillPopup$AutofillPopupDelegate;

.field private final mContext:Landroid/content/Context;

.field private mSuggestions:Ljava/util/List;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 30
    const-class v0, Lorg/chromium/ui/autofill/AutofillPopup;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/chromium/ui/autofill/AutofillPopup;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;Lorg/chromium/ui/base/ViewAndroidDelegate;Lorg/chromium/ui/autofill/AutofillPopup$AutofillPopupDelegate;)V
    .locals 0

    .prologue
    .line 68
    invoke-direct {p0, p1, p2}, Lorg/chromium/ui/DropdownPopupWindow;-><init>(Landroid/content/Context;Lorg/chromium/ui/base/ViewAndroidDelegate;)V

    .line 69
    iput-object p1, p0, Lorg/chromium/ui/autofill/AutofillPopup;->mContext:Landroid/content/Context;

    .line 70
    iput-object p3, p0, Lorg/chromium/ui/autofill/AutofillPopup;->mAutofillCallback:Lorg/chromium/ui/autofill/AutofillPopup$AutofillPopupDelegate;

    .line 72
    invoke-virtual {p0, p0}, Lorg/chromium/ui/autofill/AutofillPopup;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 73
    invoke-virtual {p0, p0}, Lorg/chromium/ui/autofill/AutofillPopup;->setOnDismissListener(Landroid/widget/PopupWindow$OnDismissListener;)V

    .line 74
    return-void
.end method


# virtual methods
.method public filterAndShow([Lorg/chromium/ui/autofill/AutofillSuggestion;Z)V
    .locals 7

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 82
    new-instance v0, Ljava/util/ArrayList;

    invoke-static {p1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v3

    invoke-direct {v0, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lorg/chromium/ui/autofill/AutofillPopup;->mSuggestions:Ljava/util/List;

    .line 84
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 85
    new-instance v4, Ljava/util/HashSet;

    invoke-direct {v4}, Ljava/util/HashSet;-><init>()V

    move v0, v1

    .line 86
    :goto_0
    array-length v5, p1

    if-ge v0, v5, :cond_1

    .line 87
    aget-object v5, p1, v0

    invoke-virtual {v5}, Lorg/chromium/ui/autofill/AutofillSuggestion;->getSuggestionId()I

    move-result v5

    .line 88
    const/4 v6, -0x3

    if-ne v5, v6, :cond_0

    .line 89
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 86
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 91
    :cond_0
    aget-object v5, p1, v0

    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 95
    :cond_1
    new-instance v0, Lorg/chromium/ui/DropdownAdapter;

    iget-object v5, p0, Lorg/chromium/ui/autofill/AutofillPopup;->mContext:Landroid/content/Context;

    invoke-direct {v0, v5, v3, v4}, Lorg/chromium/ui/DropdownAdapter;-><init>(Landroid/content/Context;Ljava/util/List;Ljava/util/Set;)V

    invoke-virtual {p0, v0}, Lorg/chromium/ui/autofill/AutofillPopup;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 96
    invoke-virtual {p0}, Lorg/chromium/ui/autofill/AutofillPopup;->show()V

    .line 97
    invoke-virtual {p0}, Lorg/chromium/ui/autofill/AutofillPopup;->getListView()Landroid/widget/ListView;

    move-result-object v0

    if-eqz p2, :cond_2

    move v1, v2

    :cond_2
    invoke-static {v0, v1}, Lorg/chromium/base/ApiCompatibilityUtils;->setLayoutDirection(Landroid/view/View;I)V

    .line 105
    :try_start_0
    const-class v0, Landroid/widget/ListPopupWindow;

    const-string/jumbo v1, "setForceIgnoreOutsideTouch"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Class;

    const/4 v3, 0x0

    sget-object v4, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    .line 107
    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {v0, p0, v1}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 113
    :goto_2
    return-void

    .line 108
    :catch_0
    move-exception v0

    .line 109
    const-string/jumbo v1, "AutofillPopup"

    const-string/jumbo v2, "ListPopupWindow.setForceIgnoreOutsideTouch not found"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_2
.end method

.method public hide()V
    .locals 0

    .prologue
    .line 119
    invoke-virtual {p0}, Lorg/chromium/ui/autofill/AutofillPopup;->dismiss()V

    .line 120
    return-void
.end method

.method public onDismiss()V
    .locals 1

    .prologue
    .line 132
    iget-object v0, p0, Lorg/chromium/ui/autofill/AutofillPopup;->mAutofillCallback:Lorg/chromium/ui/autofill/AutofillPopup$AutofillPopupDelegate;

    invoke-interface {v0}, Lorg/chromium/ui/autofill/AutofillPopup$AutofillPopupDelegate;->dismissed()V

    .line 133
    return-void
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 2

    .prologue
    .line 124
    invoke-virtual {p1}, Landroid/widget/AdapterView;->getAdapter()Landroid/widget/Adapter;

    move-result-object v0

    check-cast v0, Lorg/chromium/ui/DropdownAdapter;

    .line 125
    iget-object v1, p0, Lorg/chromium/ui/autofill/AutofillPopup;->mSuggestions:Ljava/util/List;

    invoke-virtual {v0, p3}, Lorg/chromium/ui/DropdownAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v0

    .line 126
    sget-boolean v1, Lorg/chromium/ui/autofill/AutofillPopup;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    if-gez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 127
    :cond_0
    iget-object v1, p0, Lorg/chromium/ui/autofill/AutofillPopup;->mAutofillCallback:Lorg/chromium/ui/autofill/AutofillPopup$AutofillPopupDelegate;

    invoke-interface {v1, v0}, Lorg/chromium/ui/autofill/AutofillPopup$AutofillPopupDelegate;->suggestionSelected(I)V

    .line 128
    return-void
.end method
