.class public interface abstract Lorg/chromium/content/common/IChildProcessCallback;
.super Ljava/lang/Object;
.source "IChildProcessCallback.java"

# interfaces
.implements Landroid/os/IInterface;


# virtual methods
.method public abstract establishSurfacePeer(ILandroid/view/Surface;II)V
.end method

.method public abstract getSurfaceTextureSurface(II)Lorg/chromium/content/common/SurfaceWrapper;
.end method

.method public abstract getViewSurface(I)Lorg/chromium/content/common/SurfaceWrapper;
.end method
