.class public final Lorg/chromium/content/R$id;
.super Ljava/lang/Object;
.source "R.java"


# static fields
.field public static final ACCOUNT_MANAGEMENT_MENU:I = 0x7f0f0249

.field public static final CONTEXTUAL_SEARCH_MENU:I = 0x7f0f024e

.field public static final DEFAULT_MENU:I = 0x7f0f0238

.field public static final DO_NOT_TRACK_MENU:I = 0x7f0f024c

.field public static final OVERVIEW_MODE_MENU:I = 0x7f0f0234

.field public static final PAGE_MENU:I = 0x7f0f021f

.field public static final PRIVACY_MENU:I = 0x7f0f0242

.field public static final PROTECTED_CONTENT_MENU:I = 0x7f0f023d

.field public static final SYNC_ACCOUNT_MENU:I = 0x7f0f0245

.field public static final SYNC_MENU:I = 0x7f0f024a

.field public static final TABLET_EMPTY_MODE_MENU:I = 0x7f0f0237

.field public static final TOP_LEVEL_MENU:I = 0x7f0f023a

.field public static final TRANSLATE_MENU:I = 0x7f0f023f

.field public static final accessibility:I = 0x7f0f0205

.field public static final account_custodian_accounts:I = 0x7f0f0092

.field public static final account_custodian_information:I = 0x7f0f0091

.field public static final account_management_uca_content_text:I = 0x7f0f0093

.field public static final account_management_uca_safe_search_text:I = 0x7f0f0094

.field public static final account_name:I = 0x7f0f0086

.field public static final account_not_you:I = 0x7f0f008e

.field public static final account_summary:I = 0x7f0f0087

.field public static final accounts_list:I = 0x7f0f0095

.field public static final accounts_list_ll:I = 0x7f0f008d

.field public static final accounts_when_signed_in:I = 0x7f0f008a

.field public static final accounts_when_signed_out:I = 0x7f0f0089

.field public static final action_bar:I = 0x7f0f006b

.field public static final action_bar_activity_content:I = 0x7f0f0003

.field public static final action_bar_black_background:I = 0x7f0f0171

.field public static final action_bar_container:I = 0x7f0f006a

.field public static final action_bar_root:I = 0x7f0f0066

.field public static final action_bar_spinner:I = 0x7f0f0002

.field public static final action_bar_subtitle:I = 0x7f0f0059

.field public static final action_bar_title:I = 0x7f0f0058

.field public static final action_context_bar:I = 0x7f0f006c

.field public static final action_menu_divider:I = 0x7f0f0005

.field public static final action_menu_presenter:I = 0x7f0f0006

.field public static final action_mode_bar:I = 0x7f0f0068

.field public static final action_mode_bar_stub:I = 0x7f0f0067

.field public static final action_mode_close_button:I = 0x7f0f005a

.field public static final activity_chooser_view_content:I = 0x7f0f005b

.field public static final add_to_homescreen_id:I = 0x7f0f022e

.field public static final address_edit_text:I = 0x7f0f00a2

.field public static final address_layout:I = 0x7f0f00a3

.field public static final address_spinner:I = 0x7f0f00a4

.field public static final address_text_view:I = 0x7f0f00a5

.field public static final all_bookmarks_menu_id:I = 0x7f0f0226

.field public static final always:I = 0x7f0f004c

.field public static final ampm:I = 0x7f0f0190

.field public static final animation_holder:I = 0x7f0f0102

.field public static final app_banner_view:I = 0x7f0f00a6

.field public static final app_icon:I = 0x7f0f00a7

.field public static final app_install_button:I = 0x7f0f00a9

.field public static final app_rating:I = 0x7f0f00ab

.field public static final app_shortcut:I = 0x7f0f00bc

.field public static final app_title:I = 0x7f0f00a8

.field public static final arrow_image:I = 0x7f0f01f0

.field public static final autofill_credit_card_cancel:I = 0x7f0f00b3

.field public static final autofill_credit_card_delete:I = 0x7f0f00b2

.field public static final autofill_credit_card_editor_month_spinner:I = 0x7f0f00b0

.field public static final autofill_credit_card_editor_name_edit:I = 0x7f0f00ae

.field public static final autofill_credit_card_editor_number_edit:I = 0x7f0f00af

.field public static final autofill_credit_card_editor_year_spinner:I = 0x7f0f00b1

.field public static final autofill_credit_card_save:I = 0x7f0f00b4

.field public static final autofill_profile_cancel:I = 0x7f0f00b9

.field public static final autofill_profile_delete:I = 0x7f0f00b8

.field public static final autofill_profile_editor_email_address_edit:I = 0x7f0f00b7

.field public static final autofill_profile_editor_phone_number_edit:I = 0x7f0f00b6

.field public static final autofill_profile_save:I = 0x7f0f00ba

.field public static final autofill_profile_widget_root:I = 0x7f0f00b5

.field public static final autofill_settings:I = 0x7f0f0201

.field public static final back:I = 0x7f0f010c

.field public static final back_button:I = 0x7f0f01e1

.field public static final background_drawable:I = 0x7f0f0012

.field public static final background_salient_image:I = 0x7f0f012d

.field public static final background_solid_color:I = 0x7f0f012c

.field public static final banner_highlight:I = 0x7f0f00ad

.field public static final beginning:I = 0x7f0f0053

.field public static final book_now:I = 0x7f0f003e

.field public static final bookmark_action_title:I = 0x7f0f0096

.field public static final bookmark_button:I = 0x7f0f016e

.field public static final bookmark_fields:I = 0x7f0f0098

.field public static final bookmark_folder_label:I = 0x7f0f009d

.field public static final bookmark_folder_list:I = 0x7f0f01c7

.field public static final bookmark_folder_select:I = 0x7f0f009e

.field public static final bookmark_folder_structure:I = 0x7f0f00c2

.field public static final bookmark_this_page_id:I = 0x7f0f0222

.field public static final bookmark_title_input:I = 0x7f0f009a

.field public static final bookmark_title_label:I = 0x7f0f0099

.field public static final bookmark_url_input:I = 0x7f0f009c

.field public static final bookmark_url_label:I = 0x7f0f009b

.field public static final bookmarks_button:I = 0x7f0f01a2

.field public static final bookmarks_content:I = 0x7f0f00c0

.field public static final bookmarks_empty_view:I = 0x7f0f00c4

.field public static final bookmarks_list:I = 0x7f0f00bb

.field public static final bookmarks_list_view:I = 0x7f0f00c3

.field public static final bottom:I = 0x7f0f0056

.field public static final bottom_panel:I = 0x7f0f014f

.field public static final bottom_stroke:I = 0x7f0f01d5

.field public static final button:I = 0x7f0f01dc

.field public static final button_bar:I = 0x7f0f0149

.field public static final button_bar_separator:I = 0x7f0f0148

.field public static final button_four:I = 0x7f0f0141

.field public static final button_one:I = 0x7f0f013e

.field public static final button_primary:I = 0x7f0f000d

.field public static final button_secondary:I = 0x7f0f000e

.field public static final button_tertiary:I = 0x7f0f000f

.field public static final button_three:I = 0x7f0f0140

.field public static final button_two:I = 0x7f0f013f

.field public static final button_wrapper:I = 0x7f0f007b

.field public static final buyButton:I = 0x7f0f003a

.field public static final buy_now:I = 0x7f0f003f

.field public static final buy_with_google:I = 0x7f0f0040

.field public static final cancel:I = 0x7f0f009f

.field public static final cast_background_image:I = 0x7f0f0135

.field public static final cast_controller_media_route_button:I = 0x7f0f00c5

.field public static final cast_frame_layout:I = 0x7f0f0134

.field public static final cast_media_controller:I = 0x7f0f0137

.field public static final cast_screen_title:I = 0x7f0f0136

.field public static final chart:I = 0x7f0f00e6

.field public static final checkbox:I = 0x7f0f0063

.field public static final child_account_indicator:I = 0x7f0f01b0

.field public static final choose_folder_title:I = 0x7f0f01c5

.field public static final classic:I = 0x7f0f0041

.field public static final close_all_incognito_tabs_menu_id:I = 0x7f0f0236

.field public static final close_all_tabs_menu_id:I = 0x7f0f0235

.field public static final close_btn:I = 0x7f0f0082

.field public static final close_button:I = 0x7f0f00ac

.field public static final close_button_front:I = 0x7f0f00db

.field public static final close_find_button:I = 0x7f0f013d

.field public static final close_menu_id:I = 0x7f0f021e

.field public static final collapseActionView:I = 0x7f0f004d

.field public static final color_button_swatch:I = 0x7f0f0208

.field public static final color_picker_advanced:I = 0x7f0f00ca

.field public static final color_picker_simple:I = 0x7f0f00cb

.field public static final compositor_view_holder:I = 0x7f0f0170

.field public static final confirmMessage:I = 0x7f0f00d0

.field public static final confirm_passphrase:I = 0x7f0f01ca

.field public static final contact:I = 0x7f0f0024

.field public static final content:I = 0x7f0f01d0

.field public static final content_container:I = 0x7f0f016f

.field public static final contextmenu_copy_email_address:I = 0x7f0f020f

.field public static final contextmenu_copy_image:I = 0x7f0f0218

.field public static final contextmenu_copy_image_url:I = 0x7f0f0219

.field public static final contextmenu_copy_link_address_text:I = 0x7f0f020e

.field public static final contextmenu_copy_link_text:I = 0x7f0f0210

.field public static final contextmenu_group_anchor:I = 0x7f0f020b

.field public static final contextmenu_group_image:I = 0x7f0f0212

.field public static final contextmenu_group_video:I = 0x7f0f021a

.field public static final contextmenu_open_image:I = 0x7f0f0214

.field public static final contextmenu_open_image_in_new_tab:I = 0x7f0f0215

.field public static final contextmenu_open_in_incognito_tab:I = 0x7f0f020d

.field public static final contextmenu_open_in_new_tab:I = 0x7f0f020c

.field public static final contextmenu_open_original_image_in_new_tab:I = 0x7f0f0216

.field public static final contextmenu_save_image:I = 0x7f0f0213

.field public static final contextmenu_save_link_as:I = 0x7f0f0211

.field public static final contextmenu_save_video:I = 0x7f0f021b

.field public static final contextmenu_search_by_image:I = 0x7f0f0217

.field public static final contextmenu_webapp_open_in_chrome:I = 0x7f0f0018

.field public static final contextual_search:I = 0x7f0f0206

.field public static final contextual_search_bar_text:I = 0x7f0f00d4

.field public static final contextual_search_panel:I = 0x7f0f00d2

.field public static final contextual_search_view:I = 0x7f0f00d1

.field public static final control_container:I = 0x7f0f00f6

.field public static final copy_url:I = 0x7f0f0256

.field public static final corpus_chip_text_view:I = 0x7f0f0167

.field public static final country_item:I = 0x7f0f00d8

.field public static final country_text:I = 0x7f0f00d9

.field public static final custom_uri:I = 0x7f0f0156

.field public static final dark_mode:I = 0x7f0f00f1

.field public static final data_reduction_compressed_size:I = 0x7f0f00e4

.field public static final data_reduction_date_range:I = 0x7f0f00e1

.field public static final data_reduction_invitation_text:I = 0x7f0f00dd

.field public static final data_reduction_original_size:I = 0x7f0f00e3

.field public static final data_reduction_percent:I = 0x7f0f00e2

.field public static final data_reduction_proxy_unreachable:I = 0x7f0f00e5

.field public static final data_reduction_statistics_container:I = 0x7f0f00e0

.field public static final data_usage_statistics_category:I = 0x7f0f01fe

.field public static final date_picker:I = 0x7f0f00ea

.field public static final date_time_suggestion:I = 0x7f0f00ec

.field public static final date_time_suggestion_label:I = 0x7f0f00ee

.field public static final date_time_suggestion_value:I = 0x7f0f00ed

.field public static final decor_content_parent:I = 0x7f0f0069

.field public static final default_activity_button:I = 0x7f0f005e

.field public static final default_checkbox:I = 0x7f0f0157

.field public static final delete:I = 0x7f0f0111

.field public static final delete_button:I = 0x7f0f016b

.field public static final demote_common_words:I = 0x7f0f001f

.field public static final demote_rfc822_hostnames:I = 0x7f0f0020

.field public static final description:I = 0x7f0f0147

.field public static final detail_series:I = 0x7f0f00e9

.field public static final device_icon:I = 0x7f0f01b6

.field public static final device_label:I = 0x7f0f01b7

.field public static final dialog:I = 0x7f0f0051

.field public static final dialog_title:I = 0x7f0f010e

.field public static final direct_share_menu_id:I = 0x7f0f022b

.field public static final disableHome:I = 0x7f0f0046

.field public static final display_image:I = 0x7f0f0085

.field public static final distillation_quality_answer_no:I = 0x7f0f0106

.field public static final distillation_quality_answer_yes:I = 0x7f0f0108

.field public static final distillation_quality_question:I = 0x7f0f0107

.field public static final do_not_track:I = 0x7f0f0207

.field public static final document_box_background:I = 0x7f0f0100

.field public static final document_box_overlay:I = 0x7f0f0101

.field public static final document_mode:I = 0x7f0f0200

.field public static final document_url:I = 0x7f0f0103

.field public static final drop_shadow:I = 0x7f0f00f7

.field public static final dropdown:I = 0x7f0f0052

.field public static final dropdown_label:I = 0x7f0f010a

.field public static final dropdown_menu_text:I = 0x7f0f0109

.field public static final dropdown_popup_window:I = 0x7f0f0009

.field public static final dropdown_sublabel:I = 0x7f0f010b

.field public static final eb_action_bar:I = 0x7f0f0124

.field public static final eb_detail_action_bar:I = 0x7f0f011e

.field public static final eb_detail_action_bar_layout:I = 0x7f0f011d

.field public static final eb_detail_actionbar_close_button:I = 0x7f0f011f

.field public static final eb_detail_actionbar_delete_button:I = 0x7f0f0121

.field public static final eb_detail_actionbar_save_button:I = 0x7f0f0120

.field public static final eb_detail_autotag_label:I = 0x7f0f011a

.field public static final eb_detail_content:I = 0x7f0f0114

.field public static final eb_detail_description:I = 0x7f0f011c

.field public static final eb_detail_flow_layout:I = 0x7f0f011b

.field public static final eb_detail_folder_textview:I = 0x7f0f0119

.field public static final eb_detail_image_mask:I = 0x7f0f0116

.field public static final eb_detail_image_view:I = 0x7f0f0115

.field public static final eb_detail_root_layout:I = 0x7f0f0112

.field public static final eb_detail_scroll_view:I = 0x7f0f0113

.field public static final eb_detail_shadow:I = 0x7f0f0122

.field public static final eb_detail_title:I = 0x7f0f0117

.field public static final eb_detail_url:I = 0x7f0f0118

.field public static final eb_drawer_layout:I = 0x7f0f0123

.field public static final eb_drawer_list:I = 0x7f0f0127

.field public static final eb_folder_list:I = 0x7f0f0129

.field public static final eb_items_container:I = 0x7f0f0125

.field public static final edit_menu_id:I = 0x7f0f021c

.field public static final edit_query:I = 0x7f0f006d

.field public static final email:I = 0x7f0f0025

.field public static final empty_container:I = 0x7f0f012f

.field public static final empty_container_stub:I = 0x7f0f0175

.field public static final empty_folders:I = 0x7f0f012a

.field public static final empty_incognito_toggle_button:I = 0x7f0f0132

.field public static final empty_layout_button_container:I = 0x7f0f0130

.field public static final empty_menu_button:I = 0x7f0f0133

.field public static final empty_new_tab_button:I = 0x7f0f0131

.field public static final enable_button_front:I = 0x7f0f00de

.field public static final enable_sync_button:I = 0x7f0f01bc

.field public static final end:I = 0x7f0f0054

.field public static final expand_activities_button:I = 0x7f0f005c

.field public static final expand_collapse_icon:I = 0x7f0f01b8

.field public static final expanded_menu:I = 0x7f0f0062

.field public static final explanation:I = 0x7f0f0158

.field public static final fader_view:I = 0x7f0f01d4

.field public static final favicon:I = 0x7f0f00be

.field public static final features:I = 0x7f0f01f2

.field public static final feedback_reporting_view:I = 0x7f0f0105

.field public static final ffwd:I = 0x7f0f017b

.field public static final find_in_page_id:I = 0x7f0f022d

.field public static final find_next_button:I = 0x7f0f013c

.field public static final find_prev_button:I = 0x7f0f013b

.field public static final find_query:I = 0x7f0f0138

.field public static final find_separator:I = 0x7f0f013a

.field public static final find_status:I = 0x7f0f0139

.field public static final find_toolbar:I = 0x7f0f00fa

.field public static final find_toolbar_stub:I = 0x7f0f00f9

.field public static final find_toolbar_tablet_stub:I = 0x7f0f0174

.field public static final focus_url_bar:I = 0x7f0f0019

.field public static final folder_structure_scroll_view:I = 0x7f0f00c1

.field public static final folder_title:I = 0x7f0f010f

.field public static final font_family:I = 0x7f0f00f3

.field public static final font_size:I = 0x7f0f00f5

.field public static final font_size_percentage:I = 0x7f0f00f4

.field public static final foreground_drawable:I = 0x7f0f0013

.field public static final forward_button:I = 0x7f0f01e2

.field public static final forward_menu_id:I = 0x7f0f0221

.field public static final fragment_container:I = 0x7f0f0177

.field public static final fre_account_layout:I = 0x7f0f0142

.field public static final fre_account_linear_layout:I = 0x7f0f0143

.field public static final fre_content:I = 0x7f0f0144

.field public static final fre_pager:I = 0x7f0f0016

.field public static final fre_spinner_text:I = 0x7f0f0150

.field public static final front_layout:I = 0x7f0f00da

.field public static final go_incognito_button:I = 0x7f0f008f

.field public static final go_incognito_disabled_button:I = 0x7f0f0090

.field public static final google_accounts_spinner:I = 0x7f0f0146

.field public static final google_icon:I = 0x7f0f00dc

.field public static final google_logo:I = 0x7f0f0155

.field public static final got_it_button:I = 0x7f0f01a6

.field public static final gradient:I = 0x7f0f00c8

.field public static final gradient_border:I = 0x7f0f00c7

.field public static final grayscale:I = 0x7f0f0042

.field public static final grid:I = 0x7f0f00e7

.field public static final header_image:I = 0x7f0f01ac

.field public static final header_summary:I = 0x7f0f01ae

.field public static final header_switch:I = 0x7f0f01af

.field public static final header_title:I = 0x7f0f01ad

.field public static final help_id:I = 0x7f0f0233

.field public static final holo_dark:I = 0x7f0f0035

.field public static final holo_light:I = 0x7f0f0036

.field public static final home:I = 0x7f0f0000

.field public static final homeAsUp:I = 0x7f0f0047

.field public static final home_button:I = 0x7f0f01dd

.field public static final homepage:I = 0x7f0f0203

.field public static final hour:I = 0x7f0f018a

.field public static final html:I = 0x7f0f001b

.field public static final hybrid:I = 0x7f0f0030

.field public static final icon:I = 0x7f0f0060

.field public static final icon_row_menu_id:I = 0x7f0f0220

.field public static final icon_uri:I = 0x7f0f0027

.field public static final icon_view:I = 0x7f0f01ee

.field public static final ifRoom:I = 0x7f0f004e

.field public static final image:I = 0x7f0f005d

.field public static final image_slider:I = 0x7f0f0145

.field public static final image_view:I = 0x7f0f014e

.field public static final image_view_frame:I = 0x7f0f014c

.field public static final image_view_wrapper:I = 0x7f0f014d

.field public static final incognito_badge:I = 0x7f0f0161

.field public static final incognito_button_stub:I = 0x7f0f01da

.field public static final incognito_button_wrapper:I = 0x7f0f01d9

.field public static final incognito_tabs_button:I = 0x7f0f007d

.field public static final incognito_toggle_button:I = 0x7f0f01db

.field public static final infobar_close_button:I = 0x7f0f000c

.field public static final infobar_extra_check:I = 0x7f0f0010

.field public static final infobar_message:I = 0x7f0f000b

.field public static final instant_message:I = 0x7f0f0026

.field public static final intent_action:I = 0x7f0f0028

.field public static final intent_activity:I = 0x7f0f0029

.field public static final intent_data:I = 0x7f0f002a

.field public static final intent_data_id:I = 0x7f0f002b

.field public static final intent_extra_data:I = 0x7f0f002c

.field public static final item_divider:I = 0x7f0f0088

.field public static final js_modal_dialog_message:I = 0x7f0f015e

.field public static final js_modal_dialog_prompt:I = 0x7f0f015f

.field public static final js_modal_dialog_scroll_view:I = 0x7f0f015d

.field public static final label:I = 0x7f0f00bf

.field public static final large_icon_uri:I = 0x7f0f002d

.field public static final learn_more:I = 0x7f0f01a4

.field public static final light_mode:I = 0x7f0f00f0

.field public static final list:I = 0x7f0f01cc

.field public static final listMode:I = 0x7f0f0044

.field public static final list_item:I = 0x7f0f005f

.field public static final list_item_frame:I = 0x7f0f007e

.field public static final list_view:I = 0x7f0f007a

.field public static final load_button:I = 0x7f0f01b5

.field public static final location_bar:I = 0x7f0f01de

.field public static final location_bar_icon:I = 0x7f0f0162

.field public static final location_icon:I = 0x7f0f01f3

.field public static final main_text:I = 0x7f0f00d6

.field public static final manage_saved_passwords:I = 0x7f0f0202

.field public static final match_global_nicknames:I = 0x7f0f0021

.field public static final match_parent:I = 0x7f0f003c

.field public static final media_route_control_frame:I = 0x7f0f0188

.field public static final media_route_disconnect_button:I = 0x7f0f0189

.field public static final media_route_list:I = 0x7f0f0185

.field public static final media_route_volume_layout:I = 0x7f0f0186

.field public static final media_route_volume_slider:I = 0x7f0f0187

.field public static final mediacontroller_progress_bar:I = 0x7f0f017f

.field public static final mediacontroller_progress_container:I = 0x7f0f017d

.field public static final menu:I = 0x7f0f0104

.field public static final menu_anchor_stub:I = 0x7f0f0176

.field public static final menu_button:I = 0x7f0f016d

.field public static final menu_id_clear_browsing_data:I = 0x7f0f0243

.field public static final menu_id_clear_translate_data:I = 0x7f0f0240

.field public static final menu_id_contextual_search_learn:I = 0x7f0f024f

.field public static final menu_id_disconnect_sync_account:I = 0x7f0f0247

.field public static final menu_id_do_not_track_learn:I = 0x7f0f024d

.field public static final menu_id_help_default:I = 0x7f0f0239

.field public static final menu_id_help_privacy:I = 0x7f0f0244

.field public static final menu_id_help_sync_account:I = 0x7f0f0248

.field public static final menu_id_import_bookmarks:I = 0x7f0f023c

.field public static final menu_id_reset_device_credential:I = 0x7f0f023e

.field public static final menu_id_sign_in_to_chrome:I = 0x7f0f023b

.field public static final menu_id_sign_in_to_chrome_new:I = 0x7f0f0246

.field public static final menu_id_sync_manage_data:I = 0x7f0f024b

.field public static final menu_id_translate_help:I = 0x7f0f0241

.field public static final menu_item_enter_anim_id:I = 0x7f0f000a

.field public static final menu_item_icon:I = 0x7f0f0182

.field public static final menu_item_text:I = 0x7f0f0181

.field public static final message:I = 0x7f0f01cb

.field public static final mic:I = 0x7f0f0154

.field public static final mic_button:I = 0x7f0f016c

.field public static final middle:I = 0x7f0f0055

.field public static final midi_icon:I = 0x7f0f01f4

.field public static final milli:I = 0x7f0f018f

.field public static final minute:I = 0x7f0f018b

.field public static final monochrome:I = 0x7f0f0043

.field public static final more:I = 0x7f0f012e

.field public static final more_colors_button:I = 0x7f0f00cd

.field public static final more_colors_button_border:I = 0x7f0f00cc

.field public static final most_visited_layout:I = 0x7f0f019b

.field public static final most_visited_placeholder:I = 0x7f0f019d

.field public static final most_visited_placeholder_stub:I = 0x7f0f019c

.field public static final most_visited_thumbnail:I = 0x7f0f0184

.field public static final most_visited_title:I = 0x7f0f0183

.field public static final name:I = 0x7f0f012b

.field public static final navigation_button:I = 0x7f0f0163

.field public static final negative_button:I = 0x7f0f014a

.field public static final never:I = 0x7f0f004f

.field public static final new_folder_btn:I = 0x7f0f01c6

.field public static final new_incognito_tab_menu_id:I = 0x7f0f0225

.field public static final new_tab_button:I = 0x7f0f01d3

.field public static final new_tab_menu_id:I = 0x7f0f0224

.field public static final next:I = 0x7f0f017c

.field public static final no_search_logo_spacer:I = 0x7f0f019f

.field public static final no_thanks_button:I = 0x7f0f00df

.field public static final none:I = 0x7f0f0031

.field public static final normal:I = 0x7f0f0032

.field public static final ntp_bottom_spacer:I = 0x7f0f019e

.field public static final ntp_content:I = 0x7f0f0192

.field public static final ntp_middle_spacer:I = 0x7f0f0198

.field public static final ntp_scroll_spacer:I = 0x7f0f01a0

.field public static final ntp_scrollview:I = 0x7f0f0191

.field public static final ntp_toolbar:I = 0x7f0f01a1

.field public static final ntp_top_spacer:I = 0x7f0f0193

.field public static final odp_listview:I = 0x7f0f01ba

.field public static final ok:I = 0x7f0f00a1

.field public static final omnibox_results_container:I = 0x7f0f00fc

.field public static final omnibox_results_container_stub:I = 0x7f0f00fb

.field public static final omnibox_title_section:I = 0x7f0f0022

.field public static final omnibox_url_section:I = 0x7f0f0023

.field public static final open_history_menu_id:I = 0x7f0f0228

.field public static final opt_out_promo:I = 0x7f0f019a

.field public static final opt_out_promo_stub:I = 0x7f0f0199

.field public static final opt_out_text:I = 0x7f0f01a5

.field public static final parent_folder:I = 0x7f0f0110

.field public static final passphrase:I = 0x7f0f01c9

.field public static final password:I = 0x7f0f015c

.field public static final password_entry_editor_cancel:I = 0x7f0f01aa

.field public static final password_entry_editor_delete:I = 0x7f0f01a9

.field public static final password_entry_editor_name:I = 0x7f0f01a7

.field public static final password_entry_editor_url:I = 0x7f0f01a8

.field public static final password_label:I = 0x7f0f015b

.field public static final pause:I = 0x7f0f017a

.field public static final pickers:I = 0x7f0f01e5

.field public static final plain:I = 0x7f0f001c

.field public static final playpause:I = 0x7f0f01bf

.field public static final popups_icon:I = 0x7f0f01f6

.field public static final position_in_year:I = 0x7f0f01e6

.field public static final positive_button:I = 0x7f0f014b

.field public static final preferences_id:I = 0x7f0f0232

.field public static final preload_button:I = 0x7f0f01b4

.field public static final prev:I = 0x7f0f0178

.field public static final preview:I = 0x7f0f01ab

.field public static final print_id:I = 0x7f0f022c

.field public static final production:I = 0x7f0f0037

.field public static final progress:I = 0x7f0f00f8

.field public static final progress_circular:I = 0x7f0f0007

.field public static final progress_horizontal:I = 0x7f0f0008

.field public static final prompt_text:I = 0x7f0f01cd

.field public static final protected_media_identifier_icon:I = 0x7f0f01f7

.field public static final radio:I = 0x7f0f0065

.field public static final radio_button_group:I = 0x7f0f00ef

.field public static final reader_mode_button:I = 0x7f0f01e0

.field public static final reader_mode_id:I = 0x7f0f0230

.field public static final reader_mode_label:I = 0x7f0f0166

.field public static final reader_mode_prefs_id:I = 0x7f0f0231

.field public static final recent_tabs_button:I = 0x7f0f01a3

.field public static final recent_tabs_menu_id:I = 0x7f0f0227

.field public static final reduce_data_usage_settings:I = 0x7f0f01fd

.field public static final refine_view_id:I = 0x7f0f0011

.field public static final refresh_button:I = 0x7f0f01e3

.field public static final reload_menu_id:I = 0x7f0f0223

.field public static final remote_notification:I = 0x7f0f0017

.field public static final remove:I = 0x7f0f00a0

.field public static final request_desktop_site_id:I = 0x7f0f022f

.field public static final reset_text:I = 0x7f0f01cf

.field public static final rew:I = 0x7f0f0179

.field public static final rfc822:I = 0x7f0f001d

.field public static final sad_tab_image:I = 0x7f0f01c1

.field public static final sad_tab_message:I = 0x7f0f01c4

.field public static final sad_tab_reload_button:I = 0x7f0f01c3

.field public static final sad_tab_title:I = 0x7f0f01c2

.field public static final sandbox:I = 0x7f0f0038

.field public static final satellite:I = 0x7f0f0033

.field public static final save:I = 0x7f0f010d

.field public static final search_badge:I = 0x7f0f006f

.field public static final search_bar:I = 0x7f0f006e

.field public static final search_box:I = 0x7f0f0195

.field public static final search_box_text:I = 0x7f0f0196

.field public static final search_button:I = 0x7f0f0070

.field public static final search_close_btn:I = 0x7f0f0075

.field public static final search_edit_frame:I = 0x7f0f0071

.field public static final search_engine:I = 0x7f0f01ff

.field public static final search_go_btn:I = 0x7f0f0077

.field public static final search_mag_icon:I = 0x7f0f0072

.field public static final search_menu_id:I = 0x7f0f021d

.field public static final search_plate:I = 0x7f0f0073

.field public static final search_provider_icon:I = 0x7f0f00d3

.field public static final search_provider_logo:I = 0x7f0f0194

.field public static final search_src_text:I = 0x7f0f0074

.field public static final search_voice_btn:I = 0x7f0f0078

.field public static final second:I = 0x7f0f018d

.field public static final second_colon:I = 0x7f0f018c

.field public static final second_dot:I = 0x7f0f018e

.field public static final security_button:I = 0x7f0f0164

.field public static final seek_bar:I = 0x7f0f00c9

.field public static final seekbar:I = 0x7f0f01b2

.field public static final seekbar_amount:I = 0x7f0f01b1

.field public static final select_action_menu_copy:I = 0x7f0f0252

.field public static final select_action_menu_cut:I = 0x7f0f0251

.field public static final select_action_menu_paste:I = 0x7f0f0253

.field public static final select_action_menu_select_all:I = 0x7f0f0250

.field public static final select_action_menu_share:I = 0x7f0f0254

.field public static final select_action_menu_web_search:I = 0x7f0f0255

.field public static final selected_color_view:I = 0x7f0f00cf

.field public static final selected_color_view_border:I = 0x7f0f00ce

.field public static final selectionDetails:I = 0x7f0f003b

.field public static final send_report_checkbox:I = 0x7f0f0152

.field public static final sepia_mode:I = 0x7f0f00f2

.field public static final series:I = 0x7f0f00e8

.field public static final shadow:I = 0x7f0f0126

.field public static final share_menu_id:I = 0x7f0f022a

.field public static final share_row_menu_id:I = 0x7f0f0229

.field public static final shortcut:I = 0x7f0f0064

.field public static final showCustom:I = 0x7f0f0048

.field public static final showHome:I = 0x7f0f0049

.field public static final showTitle:I = 0x7f0f004a

.field public static final show_menu:I = 0x7f0f001a

.field public static final sign_in_child_message:I = 0x7f0f008c

.field public static final sign_in_out_switch:I = 0x7f0f008b

.field public static final signin_promo_view:I = 0x7f0f01bd

.field public static final snapshot_prefix_label:I = 0x7f0f0165

.field public static final split_action_bar:I = 0x7f0f0004

.field public static final standard_tabs_button:I = 0x7f0f007c

.field public static final status:I = 0x7f0f01be

.field public static final stop:I = 0x7f0f01c0

.field public static final store_logo:I = 0x7f0f00aa

.field public static final strict_sandbox:I = 0x7f0f0039

.field public static final sub_text:I = 0x7f0f01f1

.field public static final submit_area:I = 0x7f0f0076

.field public static final summary:I = 0x7f0f01c8

.field public static final suppress_js_modal_dialogs:I = 0x7f0f0160

.field public static final surrounding_text_end:I = 0x7f0f00d7

.field public static final surrounding_text_start:I = 0x7f0f00d5

.field public static final sync_account:I = 0x7f0f0204

.field public static final tabMode:I = 0x7f0f0045

.field public static final tab_close_btn:I = 0x7f0f01d2

.field public static final tab_contents:I = 0x7f0f007f

.field public static final tab_favicon:I = 0x7f0f0080

.field public static final tab_icon:I = 0x7f0f01d1

.field public static final tab_strip:I = 0x7f0f01d6

.field public static final tab_strip_incognito:I = 0x7f0f01d8

.field public static final tab_strip_incognito_stub:I = 0x7f0f01d7

.field public static final tab_switcher_button:I = 0x7f0f0169

.field public static final tab_title:I = 0x7f0f0081

.field public static final tabstrip:I = 0x7f0f0173

.field public static final tabstrip_stub:I = 0x7f0f0172

.field public static final tabswitcher_multiple_drawable:I = 0x7f0f0015

.field public static final tabswitcher_single_drawable:I = 0x7f0f0014

.field public static final terms_accept:I = 0x7f0f0153

.field public static final terrain:I = 0x7f0f0034

.field public static final text:I = 0x7f0f00c6

.field public static final text1:I = 0x7f0f002e

.field public static final text2:I = 0x7f0f002f

.field public static final text_view:I = 0x7f0f01bb

.field public static final text_wrapper:I = 0x7f0f01ef

.field public static final thumb:I = 0x7f0f00bd

.field public static final time:I = 0x7f0f0180

.field public static final time_current:I = 0x7f0f017e

.field public static final time_label:I = 0x7f0f01b9

.field public static final time_picker:I = 0x7f0f00eb

.field public static final title:I = 0x7f0f0061

.field public static final title_divider:I = 0x7f0f0097

.field public static final title_holder:I = 0x7f0f0128

.field public static final titlebar:I = 0x7f0f00ff

.field public static final toolbar:I = 0x7f0f00fd

.field public static final toolbar_bg:I = 0x7f0f020a

.field public static final toolbar_buttons:I = 0x7f0f01df

.field public static final toolbar_new_tab_bg:I = 0x7f0f0209

.field public static final toolbar_shadow:I = 0x7f0f00fe

.field public static final top:I = 0x7f0f0057

.field public static final top_view:I = 0x7f0f01ed

.field public static final tos_and_privacy:I = 0x7f0f0151

.field public static final trailing_text:I = 0x7f0f01ec

.field public static final translate_spinner:I = 0x7f0f01e4

.field public static final undo_button:I = 0x7f0f0084

.field public static final undo_contents:I = 0x7f0f0083

.field public static final undobar:I = 0x7f0f01e8

.field public static final undobar_button:I = 0x7f0f01ea

.field public static final undobar_message:I = 0x7f0f01e9

.field public static final up:I = 0x7f0f0001

.field public static final url:I = 0x7f0f001e

.field public static final url_action_container:I = 0x7f0f016a

.field public static final url_bar:I = 0x7f0f01eb

.field public static final url_container:I = 0x7f0f0168

.field public static final url_to_load:I = 0x7f0f01b3

.field public static final usage_icon:I = 0x7f0f01f5

.field public static final useLogo:I = 0x7f0f004b

.field public static final username:I = 0x7f0f015a

.field public static final username_label:I = 0x7f0f0159

.field public static final verifying:I = 0x7f0f01ce

.field public static final voice_and_video_capture_icon:I = 0x7f0f01f8

.field public static final voice_search_button:I = 0x7f0f0197

.field public static final website_settings_description:I = 0x7f0f01fc

.field public static final website_settings_headline:I = 0x7f0f01fb

.field public static final website_settings_icon:I = 0x7f0f01f9

.field public static final website_settings_text_layout:I = 0x7f0f01fa

.field public static final withText:I = 0x7f0f0050

.field public static final wrap_content:I = 0x7f0f003d

.field public static final wrapper:I = 0x7f0f0079

.field public static final year:I = 0x7f0f01e7


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3434
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
