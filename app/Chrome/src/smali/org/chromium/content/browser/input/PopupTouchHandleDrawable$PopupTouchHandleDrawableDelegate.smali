.class public interface abstract Lorg/chromium/content/browser/input/PopupTouchHandleDrawable$PopupTouchHandleDrawableDelegate;
.super Ljava/lang/Object;
.source "PopupTouchHandleDrawable.java"


# virtual methods
.method public abstract getParent()Landroid/view/View;
.end method

.method public abstract getParentPositionObserver()Lorg/chromium/content/browser/PositionObserver;
.end method

.method public abstract isScrollInProgress()Z
.end method

.method public abstract onTouchHandleEvent(Landroid/view/MotionEvent;)Z
.end method
