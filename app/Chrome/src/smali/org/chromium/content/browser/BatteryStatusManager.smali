.class Lorg/chromium/content/browser/BatteryStatusManager;
.super Ljava/lang/Object;
.source "BatteryStatusManager.java"


# annotations
.annotation runtime Lorg/chromium/base/JNINamespace;
.end annotation


# instance fields
.field private final mAppContext:Landroid/content/Context;

.field private mEnabled:Z

.field private final mFilter:Landroid/content/IntentFilter;

.field private mNativePtr:J

.field private final mNativePtrLock:Ljava/lang/Object;

.field private final mReceiver:Landroid/content/BroadcastReceiver;


# direct methods
.method protected constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    new-instance v0, Landroid/content/IntentFilter;

    const-string/jumbo v1, "android.intent.action.BATTERY_CHANGED"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lorg/chromium/content/browser/BatteryStatusManager;->mFilter:Landroid/content/IntentFilter;

    .line 30
    new-instance v0, Lorg/chromium/content/browser/BatteryStatusManager$1;

    invoke-direct {v0, p0}, Lorg/chromium/content/browser/BatteryStatusManager$1;-><init>(Lorg/chromium/content/browser/BatteryStatusManager;)V

    iput-object v0, p0, Lorg/chromium/content/browser/BatteryStatusManager;->mReceiver:Landroid/content/BroadcastReceiver;

    .line 41
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lorg/chromium/content/browser/BatteryStatusManager;->mNativePtrLock:Ljava/lang/Object;

    .line 43
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/chromium/content/browser/BatteryStatusManager;->mEnabled:Z

    .line 46
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lorg/chromium/content/browser/BatteryStatusManager;->mAppContext:Landroid/content/Context;

    .line 47
    return-void
.end method

.method static getInstance(Landroid/content/Context;)Lorg/chromium/content/browser/BatteryStatusManager;
    .locals 1

    .prologue
    .line 51
    new-instance v0, Lorg/chromium/content/browser/BatteryStatusManager;

    invoke-direct {v0, p0}, Lorg/chromium/content/browser/BatteryStatusManager;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method private native nativeGotBatteryStatus(JZDDD)V
.end method


# virtual methods
.method protected gotBatteryStatus(ZDDD)V
    .locals 12

    .prologue
    .line 135
    iget-object v10, p0, Lorg/chromium/content/browser/BatteryStatusManager;->mNativePtrLock:Ljava/lang/Object;

    monitor-enter v10

    .line 136
    :try_start_0
    iget-wide v0, p0, Lorg/chromium/content/browser/BatteryStatusManager;->mNativePtr:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 137
    iget-wide v1, p0, Lorg/chromium/content/browser/BatteryStatusManager;->mNativePtr:J

    move-object v0, p0

    move v3, p1

    move-wide v4, p2

    move-wide/from16 v6, p4

    move-wide/from16 v8, p6

    invoke-direct/range {v0 .. v9}, Lorg/chromium/content/browser/BatteryStatusManager;->nativeGotBatteryStatus(JZDDD)V

    .line 139
    :cond_0
    monitor-exit v10
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v10

    throw v0
.end method

.method protected ignoreBatteryPresentState()Z
    .locals 2

    .prologue
    .line 130
    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string/jumbo v1, "Galaxy Nexus"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method onReceive(Landroid/content/Intent;)V
    .locals 14

    .prologue
    const-wide/high16 v4, 0x7ff0000000000000L    # Double.POSITIVE_INFINITY

    const-wide/high16 v6, 0x3ff0000000000000L    # 1.0

    const-wide/16 v2, 0x0

    const/4 v1, 0x1

    const/4 v10, -0x1

    .line 86
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v8, "android.intent.action.BATTERY_CHANGED"

    invoke-virtual {v0, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 87
    const-string/jumbo v0, "BatteryStatusManager"

    const-string/jumbo v1, "Unexpected intent."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 121
    :goto_0
    return-void

    .line 91
    :cond_0
    invoke-virtual {p0}, Lorg/chromium/content/browser/BatteryStatusManager;->ignoreBatteryPresentState()Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    .line 93
    :goto_1
    const-string/jumbo v8, "plugged"

    invoke-virtual {p1, v8, v10}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v8

    .line 95
    if-eqz v0, :cond_1

    if-ne v8, v10, :cond_3

    :cond_1
    move-object v0, p0

    .line 97
    invoke-virtual/range {v0 .. v7}, Lorg/chromium/content/browser/BatteryStatusManager;->gotBatteryStatus(ZDDD)V

    goto :goto_0

    .line 91
    :cond_2
    const-string/jumbo v0, "present"

    const/4 v8, 0x0

    invoke-virtual {p1, v0, v8}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    goto :goto_1

    .line 101
    :cond_3
    if-eqz v8, :cond_5

    move v8, v1

    .line 102
    :goto_2
    const-string/jumbo v0, "status"

    invoke-virtual {p1, v0, v10}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 103
    const/4 v9, 0x5

    if-ne v0, v9, :cond_6

    .line 105
    :goto_3
    const-string/jumbo v0, "level"

    invoke-virtual {p1, v0, v10}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 106
    const-string/jumbo v9, "scale"

    invoke-virtual {p1, v9, v10}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v9

    .line 107
    int-to-double v10, v0

    int-to-double v12, v9

    div-double/2addr v10, v12

    .line 108
    cmpg-double v0, v10, v2

    if-ltz v0, :cond_4

    cmpl-double v0, v10, v6

    if-lez v0, :cond_8

    .line 117
    :cond_4
    :goto_4
    and-int v0, v8, v1

    if-eqz v0, :cond_7

    :goto_5
    move-object v0, p0

    move v1, v8

    .line 118
    invoke-virtual/range {v0 .. v7}, Lorg/chromium/content/browser/BatteryStatusManager;->gotBatteryStatus(ZDDD)V

    goto :goto_0

    .line 101
    :cond_5
    const/4 v0, 0x0

    move v8, v0

    goto :goto_2

    .line 103
    :cond_6
    const/4 v1, 0x0

    goto :goto_3

    :cond_7
    move-wide v2, v4

    .line 117
    goto :goto_5

    :cond_8
    move-wide v6, v10

    goto :goto_4
.end method

.method start(J)Z
    .locals 5

    .prologue
    .line 60
    iget-object v1, p0, Lorg/chromium/content/browser/BatteryStatusManager;->mNativePtrLock:Ljava/lang/Object;

    monitor-enter v1

    .line 61
    :try_start_0
    iget-boolean v0, p0, Lorg/chromium/content/browser/BatteryStatusManager;->mEnabled:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lorg/chromium/content/browser/BatteryStatusManager;->mAppContext:Landroid/content/Context;

    iget-object v2, p0, Lorg/chromium/content/browser/BatteryStatusManager;->mReceiver:Landroid/content/BroadcastReceiver;

    iget-object v3, p0, Lorg/chromium/content/browser/BatteryStatusManager;->mFilter:Landroid/content/IntentFilter;

    invoke-virtual {v0, v2, v3}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 63
    iput-wide p1, p0, Lorg/chromium/content/browser/BatteryStatusManager;->mNativePtr:J

    .line 64
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/chromium/content/browser/BatteryStatusManager;->mEnabled:Z

    .line 66
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 67
    iget-boolean v0, p0, Lorg/chromium/content/browser/BatteryStatusManager;->mEnabled:Z

    return v0

    .line 66
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method stop()V
    .locals 4

    .prologue
    .line 75
    iget-object v1, p0, Lorg/chromium/content/browser/BatteryStatusManager;->mNativePtrLock:Ljava/lang/Object;

    monitor-enter v1

    .line 76
    :try_start_0
    iget-boolean v0, p0, Lorg/chromium/content/browser/BatteryStatusManager;->mEnabled:Z

    if-eqz v0, :cond_0

    .line 77
    iget-object v0, p0, Lorg/chromium/content/browser/BatteryStatusManager;->mAppContext:Landroid/content/Context;

    iget-object v2, p0, Lorg/chromium/content/browser/BatteryStatusManager;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v2}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 78
    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lorg/chromium/content/browser/BatteryStatusManager;->mNativePtr:J

    .line 79
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/chromium/content/browser/BatteryStatusManager;->mEnabled:Z

    .line 81
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
