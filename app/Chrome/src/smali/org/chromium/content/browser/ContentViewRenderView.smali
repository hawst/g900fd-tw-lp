.class public Lorg/chromium/content/browser/ContentViewRenderView;
.super Landroid/widget/FrameLayout;
.source "ContentViewRenderView.java"


# annotations
.annotation runtime Lorg/chromium/base/JNINamespace;
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private mContentReadbackHandler:Lorg/chromium/content/browser/ContentReadbackHandler;

.field protected mContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

.field private mNativeContentViewRenderView:J

.field private mSurfaceCallback:Landroid/view/SurfaceHolder$Callback;

.field private final mSurfaceView:Landroid/view/SurfaceView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 25
    const-class v0, Lorg/chromium/content/browser/ContentViewRenderView;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/chromium/content/browser/ContentViewRenderView;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    const/4 v2, -0x1

    .line 44
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 46
    invoke-virtual {p0}, Lorg/chromium/content/browser/ContentViewRenderView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p0, v0}, Lorg/chromium/content/browser/ContentViewRenderView;->createSurfaceView(Landroid/content/Context;)Landroid/view/SurfaceView;

    move-result-object v0

    iput-object v0, p0, Lorg/chromium/content/browser/ContentViewRenderView;->mSurfaceView:Landroid/view/SurfaceView;

    .line 47
    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewRenderView;->mSurfaceView:Landroid/view/SurfaceView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/SurfaceView;->setZOrderMediaOverlay(Z)V

    .line 49
    invoke-virtual {p0, v2}, Lorg/chromium/content/browser/ContentViewRenderView;->setSurfaceViewBackgroundColor(I)V

    .line 50
    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewRenderView;->mSurfaceView:Landroid/view/SurfaceView;

    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v1, v2, v2}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {p0, v0, v1}, Lorg/chromium/content/browser/ContentViewRenderView;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 54
    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewRenderView;->mSurfaceView:Landroid/view/SurfaceView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/SurfaceView;->setVisibility(I)V

    .line 55
    return-void
.end method

.method static synthetic access$000(Lorg/chromium/content/browser/ContentViewRenderView;)J
    .locals 2

    .prologue
    .line 26
    iget-wide v0, p0, Lorg/chromium/content/browser/ContentViewRenderView;->mNativeContentViewRenderView:J

    return-wide v0
.end method

.method static synthetic access$100(Lorg/chromium/content/browser/ContentViewRenderView;JIIILandroid/view/Surface;)V
    .locals 1

    .prologue
    .line 26
    invoke-direct/range {p0 .. p6}, Lorg/chromium/content/browser/ContentViewRenderView;->nativeSurfaceChanged(JIIILandroid/view/Surface;)V

    return-void
.end method

.method static synthetic access$200(Lorg/chromium/content/browser/ContentViewRenderView;J)V
    .locals 1

    .prologue
    .line 26
    invoke-direct {p0, p1, p2}, Lorg/chromium/content/browser/ContentViewRenderView;->nativeSurfaceCreated(J)V

    return-void
.end method

.method static synthetic access$300(Lorg/chromium/content/browser/ContentViewRenderView;J)V
    .locals 1

    .prologue
    .line 26
    invoke-direct {p0, p1, p2}, Lorg/chromium/content/browser/ContentViewRenderView;->nativeSurfaceDestroyed(J)V

    return-void
.end method

.method static synthetic access$400(Lorg/chromium/content/browser/ContentViewRenderView;)Landroid/view/SurfaceView;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewRenderView;->mSurfaceView:Landroid/view/SurfaceView;

    return-object v0
.end method

.method private native nativeDestroy(J)V
.end method

.method private native nativeGetUIResourceProvider(J)J
.end method

.method private native nativeInit(J)J
.end method

.method private native nativeSetCurrentContentViewCore(JJ)V
.end method

.method private native nativeSetLayerTreeBuildHelper(JJ)V
.end method

.method private native nativeSetNeedsComposite(J)V
.end method

.method private native nativeSetOverlayVideoMode(JZ)V
.end method

.method private native nativeSurfaceChanged(JIIILandroid/view/Surface;)V
.end method

.method private native nativeSurfaceCreated(J)V
.end method

.method private native nativeSurfaceDestroyed(J)V
.end method

.method private onSwapBuffersCompleted()V
    .locals 1

    .prologue
    .line 207
    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewRenderView;->mSurfaceView:Landroid/view/SurfaceView;

    invoke-virtual {v0}, Landroid/view/SurfaceView;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 208
    new-instance v0, Lorg/chromium/content/browser/ContentViewRenderView$3;

    invoke-direct {v0, p0}, Lorg/chromium/content/browser/ContentViewRenderView$3;-><init>(Lorg/chromium/content/browser/ContentViewRenderView;)V

    invoke-virtual {p0, v0}, Lorg/chromium/content/browser/ContentViewRenderView;->post(Ljava/lang/Runnable;)Z

    .line 214
    :cond_0
    return-void
.end method


# virtual methods
.method protected createSurfaceView(Landroid/content/Context;)Landroid/view/SurfaceView;
    .locals 1

    .prologue
    .line 173
    new-instance v0, Landroid/view/SurfaceView;

    invoke-direct {v0, p1}, Landroid/view/SurfaceView;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method protected onCompositorLayout()V
    .locals 0

    .prologue
    .line 203
    return-void
.end method

.method public onNativeLibraryLoaded(Lorg/chromium/ui/base/WindowAndroid;)V
    .locals 4

    .prologue
    .line 64
    sget-boolean v0, Lorg/chromium/content/browser/ContentViewRenderView;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewRenderView;->mSurfaceView:Landroid/view/SurfaceView;

    invoke-virtual {v0}, Landroid/view/SurfaceView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v0

    invoke-interface {v0}, Landroid/view/SurfaceHolder;->getSurface()Landroid/view/Surface;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Surface;->isValid()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    const-string/jumbo v1, "Surface created before native library loaded."

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 65
    :cond_0
    sget-boolean v0, Lorg/chromium/content/browser/ContentViewRenderView;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    if-nez p1, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 66
    :cond_1
    invoke-virtual {p1}, Lorg/chromium/ui/base/WindowAndroid;->getNativePointer()J

    move-result-wide v0

    invoke-direct {p0, v0, v1}, Lorg/chromium/content/browser/ContentViewRenderView;->nativeInit(J)J

    move-result-wide v0

    iput-wide v0, p0, Lorg/chromium/content/browser/ContentViewRenderView;->mNativeContentViewRenderView:J

    .line 67
    sget-boolean v0, Lorg/chromium/content/browser/ContentViewRenderView;->$assertionsDisabled:Z

    if-nez v0, :cond_2

    iget-wide v0, p0, Lorg/chromium/content/browser/ContentViewRenderView;->mNativeContentViewRenderView:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 68
    :cond_2
    new-instance v0, Lorg/chromium/content/browser/ContentViewRenderView$1;

    invoke-direct {v0, p0}, Lorg/chromium/content/browser/ContentViewRenderView$1;-><init>(Lorg/chromium/content/browser/ContentViewRenderView;)V

    iput-object v0, p0, Lorg/chromium/content/browser/ContentViewRenderView;->mSurfaceCallback:Landroid/view/SurfaceHolder$Callback;

    .line 94
    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewRenderView;->mSurfaceView:Landroid/view/SurfaceView;

    invoke-virtual {v0}, Landroid/view/SurfaceView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v0

    iget-object v1, p0, Lorg/chromium/content/browser/ContentViewRenderView;->mSurfaceCallback:Landroid/view/SurfaceHolder$Callback;

    invoke-interface {v0, v1}, Landroid/view/SurfaceHolder;->addCallback(Landroid/view/SurfaceHolder$Callback;)V

    .line 95
    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewRenderView;->mSurfaceView:Landroid/view/SurfaceView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/SurfaceView;->setVisibility(I)V

    .line 97
    new-instance v0, Lorg/chromium/content/browser/ContentViewRenderView$2;

    invoke-direct {v0, p0}, Lorg/chromium/content/browser/ContentViewRenderView$2;-><init>(Lorg/chromium/content/browser/ContentViewRenderView;)V

    iput-object v0, p0, Lorg/chromium/content/browser/ContentViewRenderView;->mContentReadbackHandler:Lorg/chromium/content/browser/ContentReadbackHandler;

    .line 103
    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewRenderView;->mContentReadbackHandler:Lorg/chromium/content/browser/ContentReadbackHandler;

    invoke-virtual {v0}, Lorg/chromium/content/browser/ContentReadbackHandler;->initNativeContentReadbackHandler()V

    .line 104
    return-void
.end method

.method protected onReadyToRender()V
    .locals 0

    .prologue
    .line 164
    return-void
.end method

.method public setCurrentContentViewCore(Lorg/chromium/content/browser/ContentViewCore;)V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 138
    sget-boolean v0, Lorg/chromium/content/browser/ContentViewRenderView;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-wide v0, p0, Lorg/chromium/content/browser/ContentViewRenderView;->mNativeContentViewRenderView:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 139
    :cond_0
    iput-object p1, p0, Lorg/chromium/content/browser/ContentViewRenderView;->mContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

    .line 141
    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewRenderView;->mContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

    if-eqz v0, :cond_1

    .line 142
    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewRenderView;->mContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

    invoke-virtual {p0}, Lorg/chromium/content/browser/ContentViewRenderView;->getWidth()I

    move-result v1

    invoke-virtual {p0}, Lorg/chromium/content/browser/ContentViewRenderView;->getHeight()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lorg/chromium/content/browser/ContentViewCore;->onPhysicalBackingSizeChanged(II)V

    .line 143
    iget-wide v0, p0, Lorg/chromium/content/browser/ContentViewRenderView;->mNativeContentViewRenderView:J

    iget-object v2, p0, Lorg/chromium/content/browser/ContentViewRenderView;->mContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

    invoke-virtual {v2}, Lorg/chromium/content/browser/ContentViewCore;->getNativeContentViewCore()J

    move-result-wide v2

    invoke-direct {p0, v0, v1, v2, v3}, Lorg/chromium/content/browser/ContentViewRenderView;->nativeSetCurrentContentViewCore(JJ)V

    .line 148
    :goto_0
    return-void

    .line 146
    :cond_1
    iget-wide v0, p0, Lorg/chromium/content/browser/ContentViewRenderView;->mNativeContentViewRenderView:J

    invoke-direct {p0, v0, v1, v2, v3}, Lorg/chromium/content/browser/ContentViewRenderView;->nativeSetCurrentContentViewCore(JJ)V

    goto :goto_0
.end method

.method public setLayerTreeBuildHelper(J)V
    .locals 3

    .prologue
    .line 198
    iget-wide v0, p0, Lorg/chromium/content/browser/ContentViewRenderView;->mNativeContentViewRenderView:J

    invoke-direct {p0, v0, v1, p1, p2}, Lorg/chromium/content/browser/ContentViewRenderView;->nativeSetLayerTreeBuildHelper(JJ)V

    .line 199
    return-void
.end method

.method public setNeedsComposite()V
    .locals 4

    .prologue
    .line 155
    iget-wide v0, p0, Lorg/chromium/content/browser/ContentViewRenderView;->mNativeContentViewRenderView:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 157
    :goto_0
    return-void

    .line 156
    :cond_0
    iget-wide v0, p0, Lorg/chromium/content/browser/ContentViewRenderView;->mNativeContentViewRenderView:J

    invoke-direct {p0, v0, v1}, Lorg/chromium/content/browser/ContentViewRenderView;->nativeSetNeedsComposite(J)V

    goto :goto_0
.end method

.method public setOverlayVideoMode(Z)V
    .locals 2

    .prologue
    .line 188
    if-eqz p1, :cond_0

    const/4 v0, -0x3

    .line 189
    :goto_0
    iget-object v1, p0, Lorg/chromium/content/browser/ContentViewRenderView;->mSurfaceView:Landroid/view/SurfaceView;

    invoke-virtual {v1}, Landroid/view/SurfaceView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v1

    invoke-interface {v1, v0}, Landroid/view/SurfaceHolder;->setFormat(I)V

    .line 190
    iget-wide v0, p0, Lorg/chromium/content/browser/ContentViewRenderView;->mNativeContentViewRenderView:J

    invoke-direct {p0, v0, v1, p1}, Lorg/chromium/content/browser/ContentViewRenderView;->nativeSetOverlayVideoMode(JZ)V

    .line 191
    return-void

    .line 188
    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public setSurfaceViewBackgroundColor(I)V
    .locals 1

    .prologue
    .line 120
    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewRenderView;->mSurfaceView:Landroid/view/SurfaceView;

    if-eqz v0, :cond_0

    .line 121
    iget-object v0, p0, Lorg/chromium/content/browser/ContentViewRenderView;->mSurfaceView:Landroid/view/SurfaceView;

    invoke-virtual {v0, p1}, Landroid/view/SurfaceView;->setBackgroundColor(I)V

    .line 123
    :cond_0
    return-void
.end method
