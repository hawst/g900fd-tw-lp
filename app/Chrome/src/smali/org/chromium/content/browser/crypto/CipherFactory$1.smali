.class Lorg/chromium/content/browser/crypto/CipherFactory$1;
.super Ljava/lang/Object;
.source "CipherFactory.java"

# interfaces
.implements Ljava/util/concurrent/Callable;


# instance fields
.field final synthetic this$0:Lorg/chromium/content/browser/crypto/CipherFactory;


# direct methods
.method constructor <init>(Lorg/chromium/content/browser/crypto/CipherFactory;)V
    .locals 0

    .prologue
    .line 149
    iput-object p1, p0, Lorg/chromium/content/browser/crypto/CipherFactory$1;->this$0:Lorg/chromium/content/browser/crypto/CipherFactory;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic call()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 149
    invoke-virtual {p0}, Lorg/chromium/content/browser/crypto/CipherFactory$1;->call()Lorg/chromium/content/browser/crypto/CipherFactory$CipherData;

    move-result-object v0

    return-object v0
.end method

.method public call()Lorg/chromium/content/browser/crypto/CipherFactory$CipherData;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 155
    :try_start_0
    iget-object v0, p0, Lorg/chromium/content/browser/crypto/CipherFactory$1;->this$0:Lorg/chromium/content/browser/crypto/CipherFactory;

    # getter for: Lorg/chromium/content/browser/crypto/CipherFactory;->mRandomNumberProvider:Lorg/chromium/content/browser/crypto/ByteArrayGenerator;
    invoke-static {v0}, Lorg/chromium/content/browser/crypto/CipherFactory;->access$200(Lorg/chromium/content/browser/crypto/CipherFactory;)Lorg/chromium/content/browser/crypto/ByteArrayGenerator;

    move-result-object v0

    const/16 v2, 0x10

    invoke-virtual {v0, v2}, Lorg/chromium/content/browser/crypto/ByteArrayGenerator;->getBytes(I)[B

    move-result-object v0

    .line 156
    iget-object v2, p0, Lorg/chromium/content/browser/crypto/CipherFactory$1;->this$0:Lorg/chromium/content/browser/crypto/CipherFactory;

    # getter for: Lorg/chromium/content/browser/crypto/CipherFactory;->mRandomNumberProvider:Lorg/chromium/content/browser/crypto/ByteArrayGenerator;
    invoke-static {v2}, Lorg/chromium/content/browser/crypto/CipherFactory;->access$200(Lorg/chromium/content/browser/crypto/CipherFactory;)Lorg/chromium/content/browser/crypto/ByteArrayGenerator;

    move-result-object v2

    const/16 v3, 0x10

    invoke-virtual {v2, v3}, Lorg/chromium/content/browser/crypto/ByteArrayGenerator;->getBytes(I)[B
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/security/GeneralSecurityException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v2

    .line 174
    :try_start_1
    const-string/jumbo v3, "SHA1PRNG"

    invoke-static {v3}, Ljava/security/SecureRandom;->getInstance(Ljava/lang/String;)Ljava/security/SecureRandom;

    move-result-object v3

    .line 175
    invoke-virtual {v3, v0}, Ljava/security/SecureRandom;->setSeed([B)V

    .line 177
    const-string/jumbo v0, "AES"

    invoke-static {v0}, Ljavax/crypto/KeyGenerator;->getInstance(Ljava/lang/String;)Ljavax/crypto/KeyGenerator;

    move-result-object v4

    .line 178
    const/16 v0, 0x80

    invoke-virtual {v4, v0, v3}, Ljavax/crypto/KeyGenerator;->init(ILjava/security/SecureRandom;)V

    .line 179
    new-instance v0, Lorg/chromium/content/browser/crypto/CipherFactory$CipherData;

    invoke-virtual {v4}, Ljavax/crypto/KeyGenerator;->generateKey()Ljavax/crypto/SecretKey;

    move-result-object v3

    invoke-direct {v0, v3, v2}, Lorg/chromium/content/browser/crypto/CipherFactory$CipherData;-><init>(Ljava/security/Key;[B)V
    :try_end_1
    .catch Ljava/security/GeneralSecurityException; {:try_start_1 .. :try_end_1} :catch_2

    .line 182
    :goto_0
    return-object v0

    .line 158
    :catch_0
    move-exception v0

    const-string/jumbo v0, "CipherFactory"

    const-string/jumbo v2, "Couldn\'t get generator data."

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, v1

    .line 159
    goto :goto_0

    .line 161
    :catch_1
    move-exception v0

    const-string/jumbo v0, "CipherFactory"

    const-string/jumbo v2, "Couldn\'t get generator data."

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, v1

    .line 162
    goto :goto_0

    .line 181
    :catch_2
    move-exception v0

    const-string/jumbo v0, "CipherFactory"

    const-string/jumbo v2, "Couldn\'t get generator instances."

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, v1

    .line 182
    goto :goto_0
.end method
