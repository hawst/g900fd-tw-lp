.class final Lorg/chromium/content/browser/ChildProcessLauncher$3;
.super Lorg/chromium/content/common/IChildProcessCallback$Stub;
.source "ChildProcessLauncher.java"


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field final synthetic val$callbackType:I

.field final synthetic val$childProcessId:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 452
    const-class v0, Lorg/chromium/content/browser/ChildProcessLauncher;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/chromium/content/browser/ChildProcessLauncher$3;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>(II)V
    .locals 0

    .prologue
    .line 452
    iput p1, p0, Lorg/chromium/content/browser/ChildProcessLauncher$3;->val$callbackType:I

    iput p2, p0, Lorg/chromium/content/browser/ChildProcessLauncher$3;->val$childProcessId:I

    invoke-direct {p0}, Lorg/chromium/content/common/IChildProcessCallback$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public final establishSurfacePeer(ILandroid/view/Surface;II)V
    .locals 2

    .prologue
    .line 464
    iget v0, p0, Lorg/chromium/content/browser/ChildProcessLauncher$3;->val$callbackType:I

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    .line 465
    const-string/jumbo v0, "ChildProcessLauncher"

    const-string/jumbo v1, "Illegal callback for non-GPU process."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 470
    :goto_0
    return-void

    .line 469
    :cond_0
    # invokes: Lorg/chromium/content/browser/ChildProcessLauncher;->nativeEstablishSurfacePeer(ILandroid/view/Surface;II)V
    invoke-static {p1, p2, p3, p4}, Lorg/chromium/content/browser/ChildProcessLauncher;->access$400(ILandroid/view/Surface;II)V

    goto :goto_0
.end method

.method public final getSurfaceTextureSurface(II)Lorg/chromium/content/common/SurfaceWrapper;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 491
    iget v0, p0, Lorg/chromium/content/browser/ChildProcessLauncher$3;->val$callbackType:I

    const/4 v2, 0x2

    if-eq v0, v2, :cond_0

    .line 492
    const-string/jumbo v0, "ChildProcessLauncher"

    const-string/jumbo v2, "Illegal callback for non-renderer process."

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, v1

    .line 509
    :goto_0
    return-object v0

    .line 496
    :cond_0
    iget v0, p0, Lorg/chromium/content/browser/ChildProcessLauncher$3;->val$childProcessId:I

    if-eq p2, v0, :cond_1

    .line 497
    const-string/jumbo v0, "ChildProcessLauncher"

    const-string/jumbo v2, "Illegal secondaryId for renderer process."

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, v1

    .line 498
    goto :goto_0

    .line 501
    :cond_1
    new-instance v0, Landroid/util/Pair;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-direct {v0, v2, v3}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 503
    # getter for: Lorg/chromium/content/browser/ChildProcessLauncher;->sSurfaceTextureSurfaceMap:Ljava/util/Map;
    invoke-static {}, Lorg/chromium/content/browser/ChildProcessLauncher;->access$600()Ljava/util/Map;

    move-result-object v2

    invoke-interface {v2, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/Surface;

    .line 504
    if-nez v0, :cond_2

    .line 505
    const-string/jumbo v0, "ChildProcessLauncher"

    const-string/jumbo v2, "Invalid Id for surface texture."

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, v1

    .line 506
    goto :goto_0

    .line 508
    :cond_2
    sget-boolean v1, Lorg/chromium/content/browser/ChildProcessLauncher$3;->$assertionsDisabled:Z

    if-nez v1, :cond_3

    invoke-virtual {v0}, Landroid/view/Surface;->isValid()Z

    move-result v1

    if-nez v1, :cond_3

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 509
    :cond_3
    new-instance v1, Lorg/chromium/content/common/SurfaceWrapper;

    invoke-direct {v1, v0}, Lorg/chromium/content/common/SurfaceWrapper;-><init>(Landroid/view/Surface;)V

    move-object v0, v1

    goto :goto_0
.end method

.method public final getViewSurface(I)Lorg/chromium/content/common/SurfaceWrapper;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 475
    iget v0, p0, Lorg/chromium/content/browser/ChildProcessLauncher$3;->val$callbackType:I

    const/4 v2, 0x1

    if-eq v0, v2, :cond_0

    .line 476
    const-string/jumbo v0, "ChildProcessLauncher"

    const-string/jumbo v2, "Illegal callback for non-GPU process."

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, v1

    .line 486
    :goto_0
    return-object v0

    .line 480
    :cond_0
    # getter for: Lorg/chromium/content/browser/ChildProcessLauncher;->sViewSurfaceMap:Ljava/util/Map;
    invoke-static {}, Lorg/chromium/content/browser/ChildProcessLauncher;->access$500()Ljava/util/Map;

    move-result-object v0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/Surface;

    .line 481
    if-nez v0, :cond_1

    .line 482
    const-string/jumbo v0, "ChildProcessLauncher"

    const-string/jumbo v2, "Invalid surfaceId."

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, v1

    .line 483
    goto :goto_0

    .line 485
    :cond_1
    sget-boolean v1, Lorg/chromium/content/browser/ChildProcessLauncher$3;->$assertionsDisabled:Z

    if-nez v1, :cond_2

    invoke-virtual {v0}, Landroid/view/Surface;->isValid()Z

    move-result v1

    if-nez v1, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 486
    :cond_2
    new-instance v1, Lorg/chromium/content/common/SurfaceWrapper;

    invoke-direct {v1, v0}, Lorg/chromium/content/common/SurfaceWrapper;-><init>(Landroid/view/Surface;)V

    move-object v0, v1

    goto :goto_0
.end method
