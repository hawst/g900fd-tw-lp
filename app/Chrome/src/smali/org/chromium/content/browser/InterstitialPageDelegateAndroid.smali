.class public Lorg/chromium/content/browser/InterstitialPageDelegateAndroid;
.super Ljava/lang/Object;
.source "InterstitialPageDelegateAndroid.java"


# annotations
.annotation runtime Lorg/chromium/base/JNINamespace;
.end annotation


# instance fields
.field private mNativePtr:J


# direct methods
.method private native nativeDontProceed(J)V
.end method

.method private native nativeInit(Ljava/lang/String;)J
.end method

.method private native nativeProceed(J)V
.end method

.method private onNativeDestroyed()V
    .locals 2

    .prologue
    .line 59
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lorg/chromium/content/browser/InterstitialPageDelegateAndroid;->mNativePtr:J

    .line 60
    return-void
.end method


# virtual methods
.method protected commandReceived(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 55
    return-void
.end method

.method public getNative()J
    .locals 2

    .prologue
    .line 31
    iget-wide v0, p0, Lorg/chromium/content/browser/InterstitialPageDelegateAndroid;->mNativePtr:J

    return-wide v0
.end method

.method protected onDontProceed()V
    .locals 0

    .prologue
    .line 46
    return-void
.end method

.method protected onProceed()V
    .locals 0

    .prologue
    .line 39
    return-void
.end method
