.class Lorg/chromium/content/browser/accessibility/AccessibilityInjector$TextToSpeechWrapper;
.super Ljava/lang/Object;
.source "AccessibilityInjector.java"


# instance fields
.field private final mSelfBrailleClient:Lcom/googlecode/eyesfree/braille/selfbraille/d;

.field private final mTextToSpeech:Landroid/speech/tts/TextToSpeech;

.field private final mView:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/view/View;Landroid/content/Context;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 401
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 402
    iput-object p1, p0, Lorg/chromium/content/browser/accessibility/AccessibilityInjector$TextToSpeechWrapper;->mView:Landroid/view/View;

    .line 403
    new-instance v0, Landroid/speech/tts/TextToSpeech;

    invoke-direct {v0, p2, v1, v1}, Landroid/speech/tts/TextToSpeech;-><init>(Landroid/content/Context;Landroid/speech/tts/TextToSpeech$OnInitListener;Ljava/lang/String;)V

    iput-object v0, p0, Lorg/chromium/content/browser/accessibility/AccessibilityInjector$TextToSpeechWrapper;->mTextToSpeech:Landroid/speech/tts/TextToSpeech;

    .line 404
    new-instance v0, Lcom/googlecode/eyesfree/braille/selfbraille/d;

    invoke-static {}, Lorg/chromium/base/CommandLine;->getInstance()Lorg/chromium/base/CommandLine;

    move-result-object v1

    const-string/jumbo v2, "debug-braille-service"

    invoke-virtual {v1, v2}, Lorg/chromium/base/CommandLine;->hasSwitch(Ljava/lang/String;)Z

    move-result v1

    invoke-direct {v0, p2, v1}, Lcom/googlecode/eyesfree/braille/selfbraille/d;-><init>(Landroid/content/Context;Z)V

    iput-object v0, p0, Lorg/chromium/content/browser/accessibility/AccessibilityInjector$TextToSpeechWrapper;->mSelfBrailleClient:Lcom/googlecode/eyesfree/braille/selfbraille/d;

    .line 406
    return-void
.end method


# virtual methods
.method public braille(Ljava/lang/String;)V
    .locals 3
    .annotation runtime Lorg/chromium/content/browser/JavascriptInterface;
    .end annotation

    .prologue
    .line 453
    :try_start_0
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 455
    iget-object v1, p0, Lorg/chromium/content/browser/accessibility/AccessibilityInjector$TextToSpeechWrapper;->mView:Landroid/view/View;

    invoke-static {v1}, Lcom/googlecode/eyesfree/braille/selfbraille/WriteData;->a(Landroid/view/View;)Lcom/googlecode/eyesfree/braille/selfbraille/WriteData;

    move-result-object v1

    .line 456
    const-string/jumbo v2, "text"

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/googlecode/eyesfree/braille/selfbraille/WriteData;->a(Ljava/lang/CharSequence;)Lcom/googlecode/eyesfree/braille/selfbraille/WriteData;

    .line 457
    const-string/jumbo v2, "startIndex"

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/googlecode/eyesfree/braille/selfbraille/WriteData;->a(I)Lcom/googlecode/eyesfree/braille/selfbraille/WriteData;

    .line 458
    const-string/jumbo v2, "endIndex"

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {v1, v0}, Lcom/googlecode/eyesfree/braille/selfbraille/WriteData;->b(I)Lcom/googlecode/eyesfree/braille/selfbraille/WriteData;

    .line 459
    iget-object v0, p0, Lorg/chromium/content/browser/accessibility/AccessibilityInjector$TextToSpeechWrapper;->mSelfBrailleClient:Lcom/googlecode/eyesfree/braille/selfbraille/d;

    invoke-virtual {v0, v1}, Lcom/googlecode/eyesfree/braille/selfbraille/d;->a(Lcom/googlecode/eyesfree/braille/selfbraille/WriteData;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 463
    :goto_0
    return-void

    .line 460
    :catch_0
    move-exception v0

    .line 461
    const-string/jumbo v1, "AccessibilityInjector"

    const-string/jumbo v2, "Error parsing JS JSON object"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public isSpeaking()Z
    .locals 1
    .annotation runtime Lorg/chromium/content/browser/JavascriptInterface;
    .end annotation

    .prologue
    .line 411
    iget-object v0, p0, Lorg/chromium/content/browser/accessibility/AccessibilityInjector$TextToSpeechWrapper;->mTextToSpeech:Landroid/speech/tts/TextToSpeech;

    invoke-virtual {v0}, Landroid/speech/tts/TextToSpeech;->isSpeaking()Z

    move-result v0

    return v0
.end method

.method protected shutdownInternal()V
    .locals 1

    .prologue
    .line 467
    iget-object v0, p0, Lorg/chromium/content/browser/accessibility/AccessibilityInjector$TextToSpeechWrapper;->mTextToSpeech:Landroid/speech/tts/TextToSpeech;

    invoke-virtual {v0}, Landroid/speech/tts/TextToSpeech;->shutdown()V

    .line 468
    iget-object v0, p0, Lorg/chromium/content/browser/accessibility/AccessibilityInjector$TextToSpeechWrapper;->mSelfBrailleClient:Lcom/googlecode/eyesfree/braille/selfbraille/d;

    invoke-virtual {v0}, Lcom/googlecode/eyesfree/braille/selfbraille/d;->a()V

    .line 469
    return-void
.end method

.method public speak(Ljava/lang/String;ILjava/lang/String;)I
    .locals 6
    .annotation runtime Lorg/chromium/content/browser/JavascriptInterface;
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 418
    .line 420
    if-eqz p3, :cond_2

    .line 421
    :try_start_0
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 422
    new-instance v3, Lorg/json/JSONObject;

    invoke-direct {v3, p3}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 426
    invoke-virtual {v3}, Lorg/json/JSONObject;->keys()Ljava/util/Iterator;

    move-result-object v4

    .line 428
    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 429
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 431
    invoke-virtual {v3, v0}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v5

    if-nez v5, :cond_0

    invoke-virtual {v3, v0}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v5

    if-nez v5, :cond_0

    .line 432
    invoke-virtual {v3, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v0, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 437
    :catch_0
    move-exception v0

    .line 440
    :goto_1
    iget-object v0, p0, Lorg/chromium/content/browser/accessibility/AccessibilityInjector$TextToSpeechWrapper;->mTextToSpeech:Landroid/speech/tts/TextToSpeech;

    invoke-virtual {v0, p1, p2, v2}, Landroid/speech/tts/TextToSpeech;->speak(Ljava/lang/String;ILjava/util/HashMap;)I

    move-result v0

    return v0

    :cond_1
    move-object v0, v1

    :goto_2
    move-object v2, v0

    .line 438
    goto :goto_1

    :cond_2
    move-object v0, v2

    goto :goto_2
.end method

.method public stop()I
    .locals 1
    .annotation runtime Lorg/chromium/content/browser/JavascriptInterface;
    .end annotation

    .prologue
    .line 446
    iget-object v0, p0, Lorg/chromium/content/browser/accessibility/AccessibilityInjector$TextToSpeechWrapper;->mTextToSpeech:Landroid/speech/tts/TextToSpeech;

    invoke-virtual {v0}, Landroid/speech/tts/TextToSpeech;->stop()I

    move-result v0

    return v0
.end method
