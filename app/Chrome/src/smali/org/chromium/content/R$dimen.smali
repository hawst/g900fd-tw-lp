.class public final Lorg/chromium/content/R$dimen;
.super Ljava/lang/Object;
.source "R.java"


# static fields
.field public static final abc_action_bar_default_height_material:I = 0x7f0e0014

.field public static final abc_action_bar_default_padding_material:I = 0x7f0e0015

.field public static final abc_action_bar_icon_vertical_padding_material:I = 0x7f0e0016

.field public static final abc_action_bar_progress_bar_size:I = 0x7f0e0005

.field public static final abc_action_bar_stacked_max_height:I = 0x7f0e0004

.field public static final abc_action_bar_stacked_tab_max_width:I = 0x7f0e0003

.field public static final abc_action_bar_subtitle_bottom_margin_material:I = 0x7f0e0018

.field public static final abc_action_bar_subtitle_top_margin_material:I = 0x7f0e0017

.field public static final abc_action_button_min_height_material:I = 0x7f0e001b

.field public static final abc_action_button_min_width_material:I = 0x7f0e001a

.field public static final abc_action_button_min_width_overflow_material:I = 0x7f0e0019

.field public static final abc_config_prefDialogWidth:I = 0x7f0e0002

.field public static final abc_control_inset_material:I = 0x7f0e0010

.field public static final abc_control_padding_material:I = 0x7f0e0011

.field public static final abc_dropdownitem_icon_width:I = 0x7f0e000b

.field public static final abc_dropdownitem_text_padding_left:I = 0x7f0e0009

.field public static final abc_dropdownitem_text_padding_right:I = 0x7f0e000a

.field public static final abc_panel_menu_list_width:I = 0x7f0e0006

.field public static final abc_search_view_preferred_width:I = 0x7f0e0008

.field public static final abc_search_view_text_min_width:I = 0x7f0e0007

.field public static final abc_text_size_body_1_material:I = 0x7f0e0025

.field public static final abc_text_size_body_2_material:I = 0x7f0e0024

.field public static final abc_text_size_button_material:I = 0x7f0e0027

.field public static final abc_text_size_caption_material:I = 0x7f0e0026

.field public static final abc_text_size_display_1_material:I = 0x7f0e001f

.field public static final abc_text_size_display_2_material:I = 0x7f0e001e

.field public static final abc_text_size_display_3_material:I = 0x7f0e001d

.field public static final abc_text_size_display_4_material:I = 0x7f0e001c

.field public static final abc_text_size_headline_material:I = 0x7f0e0020

.field public static final abc_text_size_large_material:I = 0x7f0e0028

.field public static final abc_text_size_medium_material:I = 0x7f0e0029

.field public static final abc_text_size_menu_material:I = 0x7f0e0023

.field public static final abc_text_size_small_material:I = 0x7f0e002a

.field public static final abc_text_size_subhead_material:I = 0x7f0e0022

.field public static final abc_text_size_subtitle_material_toolbar:I = 0x7f0e0013

.field public static final abc_text_size_title_material:I = 0x7f0e0021

.field public static final abc_text_size_title_material_toolbar:I = 0x7f0e0012

.field public static final accessibility_tab_height:I = 0x7f0e0034

.field public static final action_bar_switch_padding:I = 0x7f0e00b8

.field public static final android_signin_promo_illustration_padding_bottom:I = 0x7f0e00b7

.field public static final app_banner_button_height:I = 0x7f0e0047

.field public static final app_banner_button_margin_top:I = 0x7f0e0048

.field public static final app_banner_button_padding_above_below:I = 0x7f0e004a

.field public static final app_banner_button_padding_sides:I = 0x7f0e0049

.field public static final app_banner_button_text_size:I = 0x7f0e0046

.field public static final app_banner_close_button_padding:I = 0x7f0e004b

.field public static final app_banner_icon_margin_end:I = 0x7f0e004d

.field public static final app_banner_icon_size:I = 0x7f0e004c

.field public static final app_banner_logo_height:I = 0x7f0e0051

.field public static final app_banner_logo_margin_bottom:I = 0x7f0e0053

.field public static final app_banner_logo_margin_end:I = 0x7f0e0054

.field public static final app_banner_logo_margin_top:I = 0x7f0e0052

.field public static final app_banner_margin_bottom:I = 0x7f0e0043

.field public static final app_banner_margin_sides:I = 0x7f0e0042

.field public static final app_banner_max_width:I = 0x7f0e0041

.field public static final app_banner_padding:I = 0x7f0e0044

.field public static final app_banner_padding_controls:I = 0x7f0e0045

.field public static final app_banner_star_height:I = 0x7f0e0055

.field public static final app_banner_title_margin_bottom:I = 0x7f0e0050

.field public static final app_banner_title_margin_top:I = 0x7f0e004f

.field public static final app_banner_title_text_size:I = 0x7f0e004e

.field public static final auto_scroll_full_velocity:I = 0x7f0e0039

.field public static final bookmark_dialog_height:I = 0x7f0e00be

.field public static final bookmark_dialog_width:I = 0x7f0e00bf

.field public static final bookmark_folder_min_height:I = 0x7f0e00a2

.field public static final bookmark_folder_text_size:I = 0x7f0e00a3

.field public static final border_texture_title_fade:I = 0x7f0e006c

.field public static final bubble_tip_margin:I = 0x7f0e0094

.field public static final certificate_viewer_padding_thin:I = 0x7f0e0033

.field public static final certificate_viewer_padding_wide:I = 0x7f0e0032

.field public static final close_button_slop:I = 0x7f0e005a

.field public static final color_button_height:I = 0x7f0e002c

.field public static final color_picker_gradient_margin:I = 0x7f0e002b

.field public static final config_min_scaling_span:I = 0x7f0e002f

.field public static final config_min_scaling_touch_major:I = 0x7f0e0030

.field public static final contextual_search_bar_height:I = 0x7f0e00c6

.field public static final contextual_search_text_size:I = 0x7f0e00c7

.field public static final control_container_height:I = 0x7f0e0058

.field public static final data_reduction_promo_screen_height:I = 0x7f0e00c3

.field public static final data_reduction_promo_screen_width:I = 0x7f0e00c2

.field public static final data_usage_chart_height:I = 0x7f0e00c1

.field public static final data_usage_chart_optimalWidth:I = 0x7f0e00c0

.field public static final dialog_fixed_height_major:I = 0x7f0e000e

.field public static final dialog_fixed_height_minor:I = 0x7f0e000f

.field public static final dialog_fixed_width_major:I = 0x7f0e000c

.field public static final dialog_fixed_width_minor:I = 0x7f0e000d

.field public static final disabled_alpha_material_dark:I = 0x7f0e0001

.field public static final disabled_alpha_material_light:I = 0x7f0e0000

.field public static final document_button_size:I = 0x7f0e00d0

.field public static final document_titlebar_box_margin:I = 0x7f0e00ce

.field public static final document_titlebar_height:I = 0x7f0e00cd

.field public static final document_titlebar_touch_highlight_diameter:I = 0x7f0e00cf

.field public static final document_titlebar_url_drawable_padding:I = 0x7f0e00d3

.field public static final document_titlebar_url_margin_end:I = 0x7f0e00d2

.field public static final document_titlebar_url_margin_start:I = 0x7f0e00d1

.field public static final dropdown_item_divider_height:I = 0x7f0e002e

.field public static final dropdown_item_height:I = 0x7f0e002d

.field public static final enhanced_bookmark_folder_item_left:I = 0x7f0e00ca

.field public static final enhanced_bookmark_full_shadow_scroll_distance:I = 0x7f0e00cb

.field public static final enhanced_bookmark_item_corner_radius:I = 0x7f0e00c8

.field public static final enhanced_bookmark_item_popup_width:I = 0x7f0e00c9

.field public static final even_out_scrolling:I = 0x7f0e0060

.field public static final find_in_page_popup_height:I = 0x7f0e0080

.field public static final find_in_page_popup_margin_end:I = 0x7f0e0081

.field public static final find_in_page_popup_width:I = 0x7f0e007f

.field public static final find_in_page_separator_width:I = 0x7f0e007e

.field public static final find_result_bar_active_min_height:I = 0x7f0e007a

.field public static final find_result_bar_draw_width:I = 0x7f0e0078

.field public static final find_result_bar_min_gap_between_stacks:I = 0x7f0e007c

.field public static final find_result_bar_result_min_height:I = 0x7f0e0079

.field public static final find_result_bar_stacked_result_height:I = 0x7f0e007d

.field public static final find_result_bar_touch_width:I = 0x7f0e0077

.field public static final find_result_bar_vertical_padding:I = 0x7f0e007b

.field public static final fre_bottom_panel_height:I = 0x7f0e00b0

.field public static final fre_bottom_panel_padding:I = 0x7f0e00b1

.field public static final fre_button_padding:I = 0x7f0e00a9

.field public static final fre_button_text_size:I = 0x7f0e00aa

.field public static final fre_checkmark_size:I = 0x7f0e00af

.field public static final fre_image_carousel_height:I = 0x7f0e00ae

.field public static final fre_image_carousel_width:I = 0x7f0e00ad

.field public static final fre_margin:I = 0x7f0e00a8

.field public static final fre_normal_text_size:I = 0x7f0e00ac

.field public static final fre_recents_loupe_margin_left:I = 0x7f0e00b2

.field public static final fre_recents_loupe_margin_top:I = 0x7f0e00b3

.field public static final fre_title_text_size:I = 0x7f0e00ab

.field public static final gutter_distance:I = 0x7f0e0064

.field public static final infobar_button_horizontal_padding:I = 0x7f0e003e

.field public static final infobar_button_text_size:I = 0x7f0e003b

.field public static final infobar_icon_size:I = 0x7f0e0040

.field public static final infobar_margin:I = 0x7f0e003d

.field public static final infobar_min_width:I = 0x7f0e003c

.field public static final infobar_text_size:I = 0x7f0e003a

.field public static final infobar_touch_target_height:I = 0x7f0e003f

.field public static final link_preview_overlay_radius:I = 0x7f0e0031

.field public static final location_bar_icon_width:I = 0x7f0e008e

.field public static final location_bar_incognito_badge_padding:I = 0x7f0e0089

.field public static final location_bar_margin_bottom:I = 0x7f0e0086

.field public static final location_bar_margin_top:I = 0x7f0e0085

.field public static final location_bar_url_text_size:I = 0x7f0e0088

.field public static final match_parent:I = 0x7f0e0056

.field public static final menu_negative_software_vertical_offset:I = 0x7f0e0037

.field public static final menu_vertical_fade_distance:I = 0x7f0e0038

.field public static final menu_width:I = 0x7f0e0036

.field public static final min_spacing:I = 0x7f0e005e

.field public static final most_visited_bg_corner_radius:I = 0x7f0e009a

.field public static final most_visited_favicon_size:I = 0x7f0e0099

.field public static final most_visited_spacing:I = 0x7f0e0095

.field public static final most_visited_thumbnail_height:I = 0x7f0e0098

.field public static final most_visited_thumbnail_width:I = 0x7f0e0097

.field public static final most_visited_tile_width:I = 0x7f0e0096

.field public static final ntp_list_item_favicon_container_size:I = 0x7f0e00a0

.field public static final ntp_list_item_favicon_size:I = 0x7f0e009f

.field public static final ntp_list_item_min_height:I = 0x7f0e009d

.field public static final ntp_list_item_padding:I = 0x7f0e009c

.field public static final ntp_list_item_text_size:I = 0x7f0e009e

.field public static final ntp_logo_height:I = 0x7f0e009b

.field public static final ntp_shadow_height:I = 0x7f0e00a1

.field public static final omnibox_suggestion_answer_height:I = 0x7f0e008b

.field public static final omnibox_suggestion_height:I = 0x7f0e008a

.field public static final omnibox_suggestion_list_padding_bottom:I = 0x7f0e008d

.field public static final omnibox_suggestion_list_padding_top:I = 0x7f0e008c

.field public static final over_scroll:I = 0x7f0e005f

.field public static final over_scroll_slide:I = 0x7f0e0067

.field public static final preference_fragment_padding_side:I = 0x7f0e00cc

.field public static final recent_tabs_visible_separator_padding:I = 0x7f0e00a4

.field public static final remote_notification_logo_max_height:I = 0x7f0e00c5

.field public static final remote_notification_logo_max_width:I = 0x7f0e00c4

.field public static final select_bookmark_folder_item_inc_left:I = 0x7f0e0076

.field public static final select_bookmark_folder_item_left:I = 0x7f0e0075

.field public static final side_swipe_accumulate_threshold:I = 0x7f0e0065

.field public static final side_swipe_constant:I = 0x7f0e0066

.field public static final sign_in_promo_button_padding_horizontal:I = 0x7f0e00b4

.field public static final sign_in_promo_button_padding_vertical:I = 0x7f0e00b5

.field public static final sign_in_promo_padding_bottom:I = 0x7f0e00b6

.field public static final stack_buffer_height:I = 0x7f0e005c

.field public static final stack_buffer_width:I = 0x7f0e005d

.field public static final stacked_tab_visible_size:I = 0x7f0e005b

.field public static final swipe_commit_distance:I = 0x7f0e0035

.field public static final swipe_region:I = 0x7f0e0063

.field public static final tab_strip_and_toolbar_height:I = 0x7f0e0059

.field public static final tab_strip_height:I = 0x7f0e0057

.field public static final tab_title_bar_shadow_x_offset:I = 0x7f0e006d

.field public static final tab_title_bar_shadow_x_offset_incognito:I = 0x7f0e006f

.field public static final tab_title_bar_shadow_y_offset:I = 0x7f0e006e

.field public static final tab_title_bar_shadow_y_offset_incognito:I = 0x7f0e0070

.field public static final tab_title_favicon_end_padding:I = 0x7f0e0073

.field public static final tab_title_favicon_size:I = 0x7f0e0074

.field public static final tab_title_favicon_start_padding:I = 0x7f0e0072

.field public static final tab_title_text_size:I = 0x7f0e0071

.field public static final tablet_toolbar_end_padding:I = 0x7f0e008f

.field public static final tabswitcher_border_frame_padding_left:I = 0x7f0e0068

.field public static final tabswitcher_border_frame_padding_top:I = 0x7f0e0069

.field public static final tabswitcher_border_frame_transparent_side:I = 0x7f0e006b

.field public static final tabswitcher_border_frame_transparent_top:I = 0x7f0e006a

.field public static final toolbar_edge_padding:I = 0x7f0e0087

.field public static final toolbar_height_no_shadow:I = 0x7f0e0084

.field public static final toolbar_swipe_commit_distance:I = 0x7f0e0062

.field public static final toolbar_swipe_space_between_tabs:I = 0x7f0e0061

.field public static final toolbar_tab_count_text_size_1_digit:I = 0x7f0e0082

.field public static final toolbar_tab_count_text_size_2_digit:I = 0x7f0e0083

.field public static final tooltip_border_width:I = 0x7f0e0092

.field public static final tooltip_content_padding:I = 0x7f0e0093

.field public static final tooltip_min_edge_margin:I = 0x7f0e0090

.field public static final tooltip_top_margin:I = 0x7f0e0091

.field public static final undo_bar_height:I = 0x7f0e00a5

.field public static final undo_bar_tablet_margin:I = 0x7f0e00a7

.field public static final undo_bar_tablet_width:I = 0x7f0e00a6

.field public static final widget_column_width:I = 0x7f0e00bc

.field public static final widget_favicon_size:I = 0x7f0e00bd

.field public static final widget_horizontal_spacing:I = 0x7f0e00ba

.field public static final widget_thumbnail_height:I = 0x7f0e00b9

.field public static final widget_vertical_spacing:I = 0x7f0e00bb


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2475
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
