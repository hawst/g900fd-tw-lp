.class public final Lorg/chromium/content/R$layout;
.super Ljava/lang/Object;
.source "R.java"


# static fields
.field public static final abc_action_bar_title_item:I = 0x7f040000

.field public static final abc_action_bar_up_container:I = 0x7f040001

.field public static final abc_action_bar_view_list_nav_layout:I = 0x7f040002

.field public static final abc_action_menu_item_layout:I = 0x7f040003

.field public static final abc_action_menu_layout:I = 0x7f040004

.field public static final abc_action_mode_bar:I = 0x7f040005

.field public static final abc_action_mode_close_item_material:I = 0x7f040006

.field public static final abc_activity_chooser_view:I = 0x7f040007

.field public static final abc_activity_chooser_view_include:I = 0x7f040008

.field public static final abc_activity_chooser_view_list_item:I = 0x7f040009

.field public static final abc_expanded_menu_layout:I = 0x7f04000a

.field public static final abc_list_menu_item_checkbox:I = 0x7f04000b

.field public static final abc_list_menu_item_icon:I = 0x7f04000c

.field public static final abc_list_menu_item_layout:I = 0x7f04000d

.field public static final abc_list_menu_item_radio:I = 0x7f04000e

.field public static final abc_popup_menu_item_layout:I = 0x7f04000f

.field public static final abc_screen_content_include:I = 0x7f040010

.field public static final abc_screen_simple:I = 0x7f040011

.field public static final abc_screen_simple_overlay_action_mode:I = 0x7f040012

.field public static final abc_screen_toolbar:I = 0x7f040013

.field public static final abc_search_dropdown_item_icons_2line:I = 0x7f040014

.field public static final abc_search_view:I = 0x7f040015

.field public static final abc_simple_dropdown_hint:I = 0x7f040016

.field public static final accessibility_tab_switcher:I = 0x7f040017

.field public static final accessibility_tab_switcher_list_item:I = 0x7f040018

.field public static final account_management_list_item:I = 0x7f040019

.field public static final account_management_list_separator:I = 0x7f04001a

.field public static final account_management_screen:I = 0x7f04001b

.field public static final account_management_upgrade_screen:I = 0x7f04001c

.field public static final add_bookmark:I = 0x7f04001d

.field public static final address_edittext:I = 0x7f04001e

.field public static final address_layout:I = 0x7f04001f

.field public static final address_spinner:I = 0x7f040020

.field public static final address_textview:I = 0x7f040021

.field public static final app_banner_view:I = 0x7f040022

.field public static final autofill_credit_card_editor:I = 0x7f040023

.field public static final autofill_profile_editor:I = 0x7f040024

.field public static final autofill_profile_label:I = 0x7f040025

.field public static final bookmark_thumbnail_widget:I = 0x7f040026

.field public static final bookmark_thumbnail_widget_item:I = 0x7f040027

.field public static final bookmark_thumbnail_widget_item_folder:I = 0x7f040028

.field public static final bookmarks_page:I = 0x7f040029

.field public static final cast_controller_media_route_button:I = 0x7f04002a

.field public static final color_picker_advanced_component:I = 0x7f04002b

.field public static final color_picker_dialog_content:I = 0x7f04002c

.field public static final color_picker_dialog_title:I = 0x7f04002d

.field public static final confirm_sync_account_change_account:I = 0x7f04002e

.field public static final contextual_search_view:I = 0x7f04002f

.field public static final country_item:I = 0x7f040030

.field public static final country_text:I = 0x7f040031

.field public static final custom_preference:I = 0x7f040032

.field public static final data_reduction_promo_screen:I = 0x7f040033

.field public static final data_reduction_statistics_layout:I = 0x7f040034

.field public static final data_usage_chart:I = 0x7f040035

.field public static final date_time_picker_dialog:I = 0x7f040036

.field public static final date_time_suggestion:I = 0x7f040037

.field public static final dialog_with_titlebar:I = 0x7f040038

.field public static final distilled_page_font_family_spinner:I = 0x7f040039

.field public static final distilled_page_prefs_view:I = 0x7f04003a

.field public static final document_control_container:I = 0x7f04003b

.field public static final document_omnibox:I = 0x7f04003c

.field public static final document_titlebar:I = 0x7f04003d

.field public static final dom_distiller_feedback_reporting_view:I = 0x7f04003e

.field public static final dropdown_item:I = 0x7f04003f

.field public static final eb_add_edit_folder_dialog:I = 0x7f040040

.field public static final eb_detail_dialog:I = 0x7f040041

.field public static final eb_dialog:I = 0x7f040042

.field public static final eb_drawer_item:I = 0x7f040043

.field public static final eb_drawer_item_divider:I = 0x7f040044

.field public static final eb_folder:I = 0x7f040045

.field public static final eb_folder_select_dialog:I = 0x7f040046

.field public static final eb_folder_select_item:I = 0x7f040047

.field public static final eb_item:I = 0x7f040048

.field public static final eb_popup_item:I = 0x7f040049

.field public static final empty_background_view:I = 0x7f04004a

.field public static final expanded_cast_controller:I = 0x7f04004b

.field public static final find_in_page:I = 0x7f04004c

.field public static final find_toolbar:I = 0x7f04004d

.field public static final four_button_menu_item:I = 0x7f04004e

.field public static final fre_choose_account:I = 0x7f04004f

.field public static final fre_page:I = 0x7f040050

.field public static final fre_spinner_dropdown:I = 0x7f040051

.field public static final fre_spinner_text:I = 0x7f040052

.field public static final fre_tosanduma:I = 0x7f040053

.field public static final home_control_container:I = 0x7f040054

.field public static final home_titlebar:I = 0x7f040055

.field public static final homepage_preferences:I = 0x7f040056

.field public static final http_auth_dialog:I = 0x7f040057

.field public static final incognito_button_instance:I = 0x7f040058

.field public static final infobar_button:I = 0x7f040059

.field public static final infobar_spinner_item:I = 0x7f04005a

.field public static final infobar_text:I = 0x7f04005b

.field public static final js_modal_dialog:I = 0x7f04005c

.field public static final location_bar:I = 0x7f04005d

.field public static final main:I = 0x7f04005e

.field public static final manage_bookmark:I = 0x7f04005f

.field public static final media_controller:I = 0x7f040060

.field public static final menu_item:I = 0x7f040061

.field public static final most_visited_item:I = 0x7f040062

.field public static final most_visited_placeholder:I = 0x7f040063

.field public static final mr_media_route_chooser_dialog:I = 0x7f040064

.field public static final mr_media_route_controller_dialog:I = 0x7f040065

.field public static final mr_media_route_list_item:I = 0x7f040066

.field public static final multi_field_time_picker_dialog:I = 0x7f040067

.field public static final new_tab_page:I = 0x7f040068

.field public static final new_tab_page_incognito:I = 0x7f040069

.field public static final omnibox_results_container:I = 0x7f04006a

.field public static final opt_out_promo:I = 0x7f04006b

.field public static final password_entry_editor:I = 0x7f04006c

.field public static final preference_font_size_preview:I = 0x7f04006d

.field public static final preference_header_bitmap_item_layout:I = 0x7f04006e

.field public static final preference_header_item_layout:I = 0x7f04006f

.field public static final preference_header_switch_item_layout:I = 0x7f040070

.field public static final preference_header_uca_bitmap_item_layout:I = 0x7f040071

.field public static final preference_seekbar:I = 0x7f040072

.field public static final prerender_test_main:I = 0x7f040073

.field public static final recent_tabs_group_item:I = 0x7f040074

.field public static final recent_tabs_group_separator_invisible:I = 0x7f040075

.field public static final recent_tabs_group_separator_visible:I = 0x7f040076

.field public static final recent_tabs_list_item:I = 0x7f040077

.field public static final recent_tabs_page:I = 0x7f040078

.field public static final recent_tabs_promo_view_controller:I = 0x7f040079

.field public static final remote_notification_bar:I = 0x7f04007a

.field public static final sad_tab:I = 0x7f04007b

.field public static final select_bookmark_folder:I = 0x7f04007c

.field public static final select_bookmark_folder_item:I = 0x7f04007d

.field public static final share_dialog_item:I = 0x7f04007e

.field public static final single_line_bottom_text_dialog:I = 0x7f04007f

.field public static final single_line_edit_dialog:I = 0x7f040080

.field public static final support_simple_spinner_dropdown_item:I = 0x7f040081

.field public static final sync_custom_passphrase:I = 0x7f040082

.field public static final sync_encryption_type:I = 0x7f040083

.field public static final sync_enter_passphrase:I = 0x7f040084

.field public static final sync_passphrase_activity:I = 0x7f040085

.field public static final tab:I = 0x7f040086

.field public static final tab_strip_instance:I = 0x7f040087

.field public static final tab_strip_manager:I = 0x7f040088

.field public static final tab_strip_manager_instance:I = 0x7f040089

.field public static final three_button_menu_item:I = 0x7f04008a

.field public static final title_button_menu_item:I = 0x7f04008b

.field public static final toolbar:I = 0x7f04008c

.field public static final translate_spinner:I = 0x7f04008d

.field public static final two_button_menu_item:I = 0x7f04008e

.field public static final two_field_date_picker:I = 0x7f04008f

.field public static final undo_bar:I = 0x7f040090

.field public static final url_container:I = 0x7f040091

.field public static final validation_message_bubble:I = 0x7f040092

.field public static final website_features:I = 0x7f040093

.field public static final website_settings:I = 0x7f040094


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 4098
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
