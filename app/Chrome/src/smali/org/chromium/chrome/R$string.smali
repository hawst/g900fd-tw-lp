.class public final Lorg/chromium/chrome/R$string;
.super Ljava/lang/Object;
.source "R.java"


# static fields
.field public static final abc_action_bar_home_description:I = 0x7f0a004f

.field public static final abc_action_bar_home_description_format:I = 0x7f0a0052

.field public static final abc_action_bar_home_subtitle_description_format:I = 0x7f0a0053

.field public static final abc_action_bar_up_description:I = 0x7f0a0050

.field public static final abc_action_menu_overflow_description:I = 0x7f0a0051

.field public static final abc_action_mode_done:I = 0x7f0a004e

.field public static final abc_activity_chooser_view_see_all:I = 0x7f0a005a

.field public static final abc_activitychooserview_choose_application:I = 0x7f0a0059

.field public static final abc_searchview_description_clear:I = 0x7f0a0056

.field public static final abc_searchview_description_query:I = 0x7f0a0055

.field public static final abc_searchview_description_search:I = 0x7f0a0054

.field public static final abc_searchview_description_submit:I = 0x7f0a0057

.field public static final abc_searchview_description_voice:I = 0x7f0a0058

.field public static final abc_shareactionprovider_share_with:I = 0x7f0a005c

.field public static final abc_shareactionprovider_share_with_application:I = 0x7f0a005b

.field public static final accept:I = 0x7f0a0002

.field public static final accept_cookies_summary:I = 0x7f0a01e3

.field public static final accept_cookies_title:I = 0x7f0a01e2

.field public static final accessibility_autofill_cc_month:I = 0x7f0a0297

.field public static final accessibility_autofill_cc_name_textbox:I = 0x7f0a0295

.field public static final accessibility_autofill_cc_number_textbox:I = 0x7f0a0296

.field public static final accessibility_autofill_cc_year:I = 0x7f0a0298

.field public static final accessibility_autofill_profile_city:I = 0x7f0a029c

.field public static final accessibility_autofill_profile_company:I = 0x7f0a029a

.field public static final accessibility_autofill_profile_dependent_locality:I = 0x7f0a029d

.field public static final accessibility_autofill_profile_email:I = 0x7f0a02a2

.field public static final accessibility_autofill_profile_name:I = 0x7f0a0299

.field public static final accessibility_autofill_profile_phone:I = 0x7f0a02a1

.field public static final accessibility_autofill_profile_sorting_code:I = 0x7f0a02a0

.field public static final accessibility_autofill_profile_state:I = 0x7f0a029e

.field public static final accessibility_autofill_profile_street_address:I = 0x7f0a029b

.field public static final accessibility_autofill_profile_zip:I = 0x7f0a029f

.field public static final accessibility_bookmark_folder:I = 0x7f0a0138

.field public static final accessibility_bookmark_title_textbox:I = 0x7f0a0293

.field public static final accessibility_bookmark_url_textbox:I = 0x7f0a0294

.field public static final accessibility_bookmark_widget_thumb_launch_chrome:I = 0x7f0a02a5

.field public static final accessibility_btn_refresh:I = 0x7f0a0289

.field public static final accessibility_btn_stop_loading:I = 0x7f0a028a

.field public static final accessibility_content_view:I = 0x7f0a005f

.field public static final accessibility_data_reduction_close_button:I = 0x7f0a0100

.field public static final accessibility_date_picker_month:I = 0x7f0a007b

.field public static final accessibility_date_picker_week:I = 0x7f0a007f

.field public static final accessibility_date_picker_year:I = 0x7f0a007c

.field public static final accessibility_datetime_picker_date:I = 0x7f0a008d

.field public static final accessibility_datetime_picker_time:I = 0x7f0a008e

.field public static final accessibility_enhanced_bookmark_detail_close_btn:I = 0x7f0a02a6

.field public static final accessibility_enhanced_bookmark_detail_delete_btn:I = 0x7f0a02a7

.field public static final accessibility_enhanced_bookmark_detail_save_btn:I = 0x7f0a02a8

.field public static final accessibility_find_toolbar_btn_close:I = 0x7f0a028d

.field public static final accessibility_find_toolbar_btn_next:I = 0x7f0a028b

.field public static final accessibility_find_toolbar_btn_prev:I = 0x7f0a028c

.field public static final accessibility_fre_account_spinner:I = 0x7f0a02a3

.field public static final accessibility_http_auth_password_input:I = 0x7f0a00d7

.field public static final accessibility_http_auth_username_input:I = 0x7f0a00d6

.field public static final accessibility_js_modal_dialog_prompt:I = 0x7f0a0099

.field public static final accessibility_menu_back:I = 0x7f0a00c7

.field public static final accessibility_menu_bookmark:I = 0x7f0a00c5

.field public static final accessibility_menu_forward:I = 0x7f0a00c6

.field public static final accessibility_menu_share_via:I = 0x7f0a00c3

.field public static final accessibility_ntp_toolbar_btn_bookmarks:I = 0x7f0a0291

.field public static final accessibility_ntp_toolbar_btn_recent_tabs:I = 0x7f0a0292

.field public static final accessibility_omnibox_btn_refine:I = 0x7f0a02a4

.field public static final accessibility_pause:I = 0x7f0a02aa

.field public static final accessibility_play:I = 0x7f0a02a9

.field public static final accessibility_stop:I = 0x7f0a02ab

.field public static final accessibility_tab_switcher_incognito_stack:I = 0x7f0a00a7

.field public static final accessibility_tab_switcher_standard_stack:I = 0x7f0a00a6

.field public static final accessibility_tab_switcher_undo_tab_closed:I = 0x7f0a00a8

.field public static final accessibility_tabstrip_btn_close_tab:I = 0x7f0a00aa

.field public static final accessibility_tabstrip_btn_empty_new_tab:I = 0x7f0a028e

.field public static final accessibility_tabstrip_btn_incognito_toggle_incognito:I = 0x7f0a0290

.field public static final accessibility_tabstrip_btn_incognito_toggle_standard:I = 0x7f0a028f

.field public static final accessibility_tabstrip_btn_new_tab:I = 0x7f0a00ab

.field public static final accessibility_tabstrip_tab:I = 0x7f0a00a9

.field public static final accessibility_time_picker_ampm:I = 0x7f0a0088

.field public static final accessibility_time_picker_hour:I = 0x7f0a0084

.field public static final accessibility_time_picker_milli:I = 0x7f0a0087

.field public static final accessibility_time_picker_minute:I = 0x7f0a0085

.field public static final accessibility_time_picker_second:I = 0x7f0a0086

.field public static final accessibility_toolbar_btn_back:I = 0x7f0a027f

.field public static final accessibility_toolbar_btn_delete_url:I = 0x7f0a0284

.field public static final accessibility_toolbar_btn_forward:I = 0x7f0a027e

.field public static final accessibility_toolbar_btn_home:I = 0x7f0a0288

.field public static final accessibility_toolbar_btn_menu:I = 0x7f0a0280

.field public static final accessibility_toolbar_btn_mic:I = 0x7f0a0283

.field public static final accessibility_toolbar_btn_new_incognito_tab:I = 0x7f0a0286

.field public static final accessibility_toolbar_btn_new_tab:I = 0x7f0a0285

.field public static final accessibility_toolbar_btn_reader_mode:I = 0x7f0a0282

.field public static final accessibility_toolbar_btn_security_lock:I = 0x7f0a0281

.field public static final accessibility_toolbar_btn_tabswitcher_toggle:I = 0x7f0a0287

.field public static final accessibility_website_location_allowed:I = 0x7f0a02ac

.field public static final accessibility_website_location_denied:I = 0x7f0a02ad

.field public static final accessibility_website_midi_allowed:I = 0x7f0a02ae

.field public static final accessibility_website_midi_denied:I = 0x7f0a02af

.field public static final accessibility_website_popups_allowed:I = 0x7f0a02b0

.field public static final accessibility_website_popups_denied:I = 0x7f0a02b1

.field public static final account_management_add_account_title:I = 0x7f0a02b7

.field public static final account_management_custodian_settings:I = 0x7f0a02f8

.field public static final account_management_go_incognito_disabled_text:I = 0x7f0a02ba

.field public static final account_management_go_incognito_text:I = 0x7f0a02b9

.field public static final account_management_no_custodian_data:I = 0x7f0a02f9

.field public static final account_management_not_you_text:I = 0x7f0a02bb

.field public static final account_management_one_custodian_name:I = 0x7f0a02fa

.field public static final account_management_title:I = 0x7f0a02b8

.field public static final account_management_two_custodian_names:I = 0x7f0a02fb

.field public static final account_management_uca_content_all:I = 0x7f0a02fe

.field public static final account_management_uca_content_approved:I = 0x7f0a02fd

.field public static final account_management_uca_content_title:I = 0x7f0a02fc

.field public static final account_management_uca_safe_search_title:I = 0x7f0a02ff

.field public static final account_management_upgrade_screen_ok_title:I = 0x7f0a02bf

.field public static final account_management_upgrade_screen_settings_title:I = 0x7f0a02be

.field public static final account_management_upgrade_screen_text:I = 0x7f0a02bd

.field public static final account_management_upgrade_screen_title:I = 0x7f0a02bc

.field public static final actionbar_share:I = 0x7f0a005d

.field public static final actionbar_textselection_title:I = 0x7f0a01c1

.field public static final actionbar_web_search:I = 0x7f0a005e

.field public static final add:I = 0x7f0a02c6

.field public static final add_account_continue:I = 0x7f0a019c

.field public static final add_account_message:I = 0x7f0a019b

.field public static final add_account_title:I = 0x7f0a019a

.field public static final add_bookmark:I = 0x7f0a015b

.field public static final add_folder:I = 0x7f0a015d

.field public static final add_to_homescreen_title:I = 0x7f0a02c5

.field public static final address_data_loading:I = 0x7f0a0023

.field public static final allow_exception_button:I = 0x7f0a022b

.field public static final always_prefetch_bandwidth_entry:I = 0x7f0a0244

.field public static final app_banner_install_accessibility:I = 0x7f0a00dd

.field public static final app_banner_installing:I = 0x7f0a00da

.field public static final app_banner_open:I = 0x7f0a00db

.field public static final app_banner_view_accessibility:I = 0x7f0a00dc

.field public static final app_name:I = 0x7f0a030a

.field public static final application_version_title:I = 0x7f0a0265

.field public static final athome_casting_video:I = 0x7f0a02dd

.field public static final athome_notification_finished:I = 0x7f0a02d5

.field public static final athome_notification_finished_for_video:I = 0x7f0a02dc

.field public static final athome_notification_loading:I = 0x7f0a02d7

.field public static final athome_notification_loading_for_video:I = 0x7f0a02d9

.field public static final athome_notification_paused:I = 0x7f0a02d6

.field public static final athome_notification_paused_for_video:I = 0x7f0a02db

.field public static final athome_notification_playing:I = 0x7f0a02d8

.field public static final athome_notification_playing_for_video:I = 0x7f0a02da

.field public static final athome_notification_stopped:I = 0x7f0a02d4

.field public static final audio_call_notification_text_2:I = 0x7f0a02c8

.field public static final autofill_create_or_edit_credit_card:I = 0x7f0a01cc

.field public static final autofill_create_or_edit_profile:I = 0x7f0a01cb

.field public static final autofill_credit_card_editor_expiration_date:I = 0x7f0a020f

.field public static final autofill_credit_card_editor_name:I = 0x7f0a020d

.field public static final autofill_credit_card_editor_number:I = 0x7f0a020e

.field public static final autofill_credit_card_generated_text:I = 0x7f0a02c0

.field public static final autofill_credit_cards_title:I = 0x7f0a01d1

.field public static final autofill_delete:I = 0x7f0a01d2

.field public static final autofill_profile_editor_city:I = 0x7f0a0206

.field public static final autofill_profile_editor_company_name:I = 0x7f0a0203

.field public static final autofill_profile_editor_country:I = 0x7f0a020b

.field public static final autofill_profile_editor_dependent_locality:I = 0x7f0a0207

.field public static final autofill_profile_editor_email_address:I = 0x7f0a0202

.field public static final autofill_profile_editor_name:I = 0x7f0a0201

.field public static final autofill_profile_editor_phone_number:I = 0x7f0a020c

.field public static final autofill_profile_editor_sorting_code:I = 0x7f0a020a

.field public static final autofill_profile_editor_state:I = 0x7f0a0208

.field public static final autofill_profile_editor_street_address:I = 0x7f0a0204

.field public static final autofill_profile_editor_street_address_hint:I = 0x7f0a0205

.field public static final autofill_profile_editor_zip_code:I = 0x7f0a0209

.field public static final autofill_profiles_title:I = 0x7f0a01d0

.field public static final blink_version_title:I = 0x7f0a0267

.field public static final block_popups_summary:I = 0x7f0a022a

.field public static final block_popups_title:I = 0x7f0a0229

.field public static final bookmark_folder:I = 0x7f0a0161

.field public static final bookmark_folder_tree_error:I = 0x7f0a016a

.field public static final bookmark_missing_name:I = 0x7f0a0162

.field public static final bookmark_missing_url:I = 0x7f0a0163

.field public static final bookmark_name:I = 0x7f0a015f

.field public static final bookmark_shortcut_choose_bookmark:I = 0x7f0a016e

.field public static final bookmark_shortcut_name:I = 0x7f0a016d

.field public static final bookmark_url:I = 0x7f0a0160

.field public static final bookmarks:I = 0x7f0a0132

.field public static final bookmarks_folder_empty:I = 0x7f0a0133

.field public static final button_home:I = 0x7f0a0116

.field public static final button_new_tab:I = 0x7f0a0118

.field public static final cancel:I = 0x7f0a00ed

.field public static final cancel_talkback_alert:I = 0x7f0a00ea

.field public static final cannot_add_downloaded_item_to_manager:I = 0x7f0a01b9

.field public static final cannot_create_download_directory_title:I = 0x7f0a01b5

.field public static final cannot_download_generic:I = 0x7f0a01b7

.field public static final cannot_download_http_or_https:I = 0x7f0a01b6

.field public static final cast_error_playing_video:I = 0x7f0a02de

.field public static final cast_permission_error_playing_video:I = 0x7f0a02df

.field public static final category_autofill_create_credit_card:I = 0x7f0a01ca

.field public static final category_autofill_create_profile:I = 0x7f0a01c9

.field public static final category_legal_information_summary:I = 0x7f0a0264

.field public static final category_legal_information_title:I = 0x7f0a0263

.field public static final category_protected_content:I = 0x7f0a01c7

.field public static final category_translate:I = 0x7f0a01c8

.field public static final category_website_settings:I = 0x7f0a01ce

.field public static final category_website_settings_subtitle:I = 0x7f0a01cf

.field public static final certtitle:I = 0x7f0a009a

.field public static final channel_name:I = 0x7f0a0309

.field public static final choose_account_sign_in:I = 0x7f0a019d

.field public static final choose_folder:I = 0x7f0a0164

.field public static final chrome_privacy_policy_url:I = 0x7f0a0271

.field public static final chrome_terms_of_service_url:I = 0x7f0a0272

.field public static final clear_bookmarks_title:I = 0x7f0a02c4

.field public static final clear_browsing_data_progress_message:I = 0x7f0a01e1

.field public static final clear_browsing_data_progress_title:I = 0x7f0a01e0

.field public static final clear_browsing_data_title:I = 0x7f0a01d8

.field public static final clear_cache_title:I = 0x7f0a01da

.field public static final clear_cookies_and_site_data_title:I = 0x7f0a01dc

.field public static final clear_cookies_no_sign_out_summary:I = 0x7f0a01dd

.field public static final clear_data_delete:I = 0x7f0a0262

.field public static final clear_formdata_title:I = 0x7f0a01df

.field public static final clear_history_title:I = 0x7f0a01db

.field public static final clear_passwords_title:I = 0x7f0a01de

.field public static final close_all_incognito:I = 0x7f0a0308

.field public static final color_picker_button_black:I = 0x7f0a0078

.field public static final color_picker_button_blue:I = 0x7f0a0074

.field public static final color_picker_button_cancel:I = 0x7f0a0070

.field public static final color_picker_button_cyan:I = 0x7f0a0073

.field public static final color_picker_button_green:I = 0x7f0a0075

.field public static final color_picker_button_magenta:I = 0x7f0a0076

.field public static final color_picker_button_more:I = 0x7f0a006b

.field public static final color_picker_button_red:I = 0x7f0a0072

.field public static final color_picker_button_set:I = 0x7f0a006f

.field public static final color_picker_button_white:I = 0x7f0a0079

.field public static final color_picker_button_yellow:I = 0x7f0a0077

.field public static final color_picker_dialog_title:I = 0x7f0a0071

.field public static final color_picker_hue:I = 0x7f0a006c

.field public static final color_picker_saturation:I = 0x7f0a006d

.field public static final color_picker_value:I = 0x7f0a006e

.field public static final common_android_wear_notification_needs_update_text:I = 0x7f0a0009

.field public static final common_android_wear_update_text:I = 0x7f0a0016

.field public static final common_android_wear_update_title:I = 0x7f0a0014

.field public static final common_google_play_services_enable_button:I = 0x7f0a0012

.field public static final common_google_play_services_enable_text:I = 0x7f0a0011

.field public static final common_google_play_services_enable_title:I = 0x7f0a0010

.field public static final common_google_play_services_error_notification_requested_by_msg:I = 0x7f0a000b

.field public static final common_google_play_services_install_button:I = 0x7f0a000f

.field public static final common_google_play_services_install_text_phone:I = 0x7f0a000d

.field public static final common_google_play_services_install_text_tablet:I = 0x7f0a000e

.field public static final common_google_play_services_install_title:I = 0x7f0a000c

.field public static final common_google_play_services_invalid_account_text:I = 0x7f0a001a

.field public static final common_google_play_services_invalid_account_title:I = 0x7f0a0019

.field public static final common_google_play_services_needs_enabling_title:I = 0x7f0a000a

.field public static final common_google_play_services_network_error_text:I = 0x7f0a0018

.field public static final common_google_play_services_network_error_title:I = 0x7f0a0017

.field public static final common_google_play_services_notification_needs_installation_title:I = 0x7f0a0007

.field public static final common_google_play_services_notification_needs_update_title:I = 0x7f0a0008

.field public static final common_google_play_services_notification_ticker:I = 0x7f0a0006

.field public static final common_google_play_services_unknown_issue:I = 0x7f0a001b

.field public static final common_google_play_services_unsupported_text:I = 0x7f0a001d

.field public static final common_google_play_services_unsupported_title:I = 0x7f0a001c

.field public static final common_google_play_services_update_button:I = 0x7f0a001e

.field public static final common_google_play_services_update_text:I = 0x7f0a0015

.field public static final common_google_play_services_update_title:I = 0x7f0a0013

.field public static final common_open_on_phone:I = 0x7f0a0021

.field public static final common_signin_button_text:I = 0x7f0a001f

.field public static final common_signin_button_text_long:I = 0x7f0a0020

.field public static final confirm_account_change_dialog_message:I = 0x7f0a02c2

.field public static final confirm_account_change_dialog_signin:I = 0x7f0a02c3

.field public static final confirm_account_change_dialog_title:I = 0x7f0a02c1

.field public static final content_provider_search_description:I = 0x7f0a027a

.field public static final contextmenu_copy_email_address:I = 0x7f0a00ca

.field public static final contextmenu_copy_image:I = 0x7f0a00d3

.field public static final contextmenu_copy_image_url:I = 0x7f0a00d4

.field public static final contextmenu_copy_link_address:I = 0x7f0a00cb

.field public static final contextmenu_copy_link_text:I = 0x7f0a00cc

.field public static final contextmenu_copy_url:I = 0x7f0a01bd

.field public static final contextmenu_delete_bookmark:I = 0x7f0a0135

.field public static final contextmenu_delete_folder:I = 0x7f0a0137

.field public static final contextmenu_edit_bookmark:I = 0x7f0a0134

.field public static final contextmenu_edit_folder:I = 0x7f0a0136

.field public static final contextmenu_open_image:I = 0x7f0a00cf

.field public static final contextmenu_open_image_in_new_tab:I = 0x7f0a00d0

.field public static final contextmenu_open_in_incognito_tab:I = 0x7f0a00c9

.field public static final contextmenu_open_in_new_tab:I = 0x7f0a00c8

.field public static final contextmenu_open_original_image_in_new_tab:I = 0x7f0a00d1

.field public static final contextmenu_save_image:I = 0x7f0a00ce

.field public static final contextmenu_save_link:I = 0x7f0a00cd

.field public static final contextmenu_save_video:I = 0x7f0a00d5

.field public static final contextmenu_search_web_for_image:I = 0x7f0a00d2

.field public static final contextmenu_webapp_open_in_chrome:I = 0x7f0a01be

.field public static final contextual_search_action_bar:I = 0x7f0a01f6

.field public static final contextual_search_description:I = 0x7f0a01f5

.field public static final contextual_search_error:I = 0x7f0a02eb

.field public static final contextual_search_learn_more_url:I = 0x7f0a01f7

.field public static final contextual_search_network_unavailable:I = 0x7f0a02ea

.field public static final contextual_search_title:I = 0x7f0a01f4

.field public static final copy_to_clipboard_failure_message:I = 0x7f0a007a

.field public static final crash_dump_always_upload:I = 0x7f0a01ec

.field public static final crash_dump_always_upload_value:I = 0x7f0a01ef

.field public static final crash_dump_never_upload:I = 0x7f0a01eb

.field public static final crash_dump_never_upload_value:I = 0x7f0a01ee

.field public static final crash_dump_only_with_wifi:I = 0x7f0a01ed

.field public static final crash_dump_only_with_wifi_value:I = 0x7f0a01f0

.field public static final crash_dump_upload_title:I = 0x7f0a01ea

.field public static final create_calendar_message:I = 0x7f0a0005

.field public static final create_calendar_title:I = 0x7f0a0004

.field public static final current_search_engine:I = 0x7f0a0258

.field public static final dark_mode:I = 0x7f0a00e1

.field public static final data_reduction_caveats_description:I = 0x7f0a0156

.field public static final data_reduction_enable_button:I = 0x7f0a00ff

.field public static final data_reduction_experiment_description:I = 0x7f0a0157

.field public static final data_reduction_experiment_link_url:I = 0x7f0a0158

.field public static final data_reduction_infobar_link_text:I = 0x7f0a00ec

.field public static final data_reduction_infobar_text:I = 0x7f0a00eb

.field public static final data_reduction_menu_item_summary:I = 0x7f0a014f

.field public static final data_reduction_proxy_unreachable_warn:I = 0x7f0a0153

.field public static final data_reduction_statistics_compressed_size_suffix:I = 0x7f0a0152

.field public static final data_reduction_statistics_original_size_suffix:I = 0x7f0a0151

.field public static final data_reduction_statistics_title:I = 0x7f0a0150

.field public static final data_reduction_title_1:I = 0x7f0a00fc

.field public static final data_reduction_title_2:I = 0x7f0a00fd

.field public static final data_reduction_title_3:I = 0x7f0a00fe

.field public static final date_picker_dialog_clear:I = 0x7f0a0091

.field public static final date_picker_dialog_other_button_label:I = 0x7f0a008f

.field public static final date_picker_dialog_set:I = 0x7f0a007d

.field public static final date_picker_dialog_title:I = 0x7f0a0090

.field public static final date_time_picker_dialog_title:I = 0x7f0a008c

.field public static final decline:I = 0x7f0a0003

.field public static final default_folder_error:I = 0x7f0a0166

.field public static final delete:I = 0x7f0a00f8

.field public static final distillation_quality_answer_no:I = 0x7f0a00e0

.field public static final distillation_quality_answer_yes:I = 0x7f0a00df

.field public static final distillation_quality_question:I = 0x7f0a00de

.field public static final do_not_track_description:I = 0x7f0a01f2

.field public static final do_not_track_learn_more_url:I = 0x7f0a01f3

.field public static final do_not_track_title:I = 0x7f0a01f1

.field public static final dont_reload_this_page:I = 0x7f0a0095

.field public static final download_no_sdcard_dlg_title:I = 0x7f0a01b3

.field public static final download_notification_failed:I = 0x7f0a02ed

.field public static final download_pending:I = 0x7f0a01b8

.field public static final download_sdcard_busy_dlg_title:I = 0x7f0a01b4

.field public static final edit_bookmark:I = 0x7f0a015c

.field public static final edit_folder:I = 0x7f0a015e

.field public static final enable_javascript_summary:I = 0x7f0a01e9

.field public static final enable_javascript_title:I = 0x7f0a01e8

.field public static final enable_location_summary:I = 0x7f0a01e7

.field public static final enable_location_title:I = 0x7f0a01e4

.field public static final enhanced_bookmark_action_bar_close:I = 0x7f0a0149

.field public static final enhanced_bookmark_action_bar_edit:I = 0x7f0a0147

.field public static final enhanced_bookmark_action_bar_search:I = 0x7f0a0148

.field public static final enhanced_bookmark_add_folder:I = 0x7f0a0143

.field public static final enhanced_bookmark_auto_tags:I = 0x7f0a013e

.field public static final enhanced_bookmark_choose_folder:I = 0x7f0a0145

.field public static final enhanced_bookmark_dismiss_on_delete:I = 0x7f0a0146

.field public static final enhanced_bookmark_drawer_all_items:I = 0x7f0a0139

.field public static final enhanced_bookmark_drawer_uncategorized:I = 0x7f0a013a

.field public static final enhanced_bookmark_item_delete:I = 0x7f0a0142

.field public static final enhanced_bookmark_item_edit:I = 0x7f0a0140

.field public static final enhanced_bookmark_item_move:I = 0x7f0a0141

.field public static final enhanced_bookmark_none_folder:I = 0x7f0a0144

.field public static final enhanced_bookmark_parent_folder:I = 0x7f0a013f

.field public static final enhanced_bookmark_title_bar_all_items:I = 0x7f0a013c

.field public static final enhanced_bookmark_title_bar_loading:I = 0x7f0a013b

.field public static final enhanced_bookmark_title_bar_uncategorized:I = 0x7f0a013d

.field public static final error_printing_failed:I = 0x7f0a00c4

.field public static final error_unexpected_system_problem:I = 0x7f0a017b

.field public static final executable_path_title:I = 0x7f0a0269

.field public static final external_app_leave_incognito_warning:I = 0x7f0a01bf

.field public static final external_app_leave_incognito_warning_title:I = 0x7f0a01c0

.field public static final failed_to_start_authenticator:I = 0x7f0a02ee

.field public static final find_in_page_count:I = 0x7f0a014b

.field public static final firstrun_signed_in_description:I = 0x7f0a00f1

.field public static final firstrun_signed_in_title:I = 0x7f0a00f0

.field public static final font_size:I = 0x7f0a0277

.field public static final font_size_preview:I = 0x7f0a0278

.field public static final font_size_preview_text:I = 0x7f0a0276

.field public static final force_enable_zoom_summary:I = 0x7f0a02b3

.field public static final force_enable_zoom_title:I = 0x7f0a02b2

.field public static final fre_accept_continue:I = 0x7f0a0248

.field public static final fre_account_choice_description:I = 0x7f0a0253

.field public static final fre_add_account:I = 0x7f0a024a

.field public static final fre_continue:I = 0x7f0a0247

.field public static final fre_done:I = 0x7f0a024b

.field public static final fre_hi_name:I = 0x7f0a0256

.field public static final fre_label:I = 0x7f0a0245

.field public static final fre_new_recents_menu_text:I = 0x7f0a027d

.field public static final fre_new_recents_menu_title:I = 0x7f0a027c

.field public static final fre_next:I = 0x7f0a024d

.field public static final fre_no_account_choice_description:I = 0x7f0a0254

.field public static final fre_no_accounts:I = 0x7f0a0255

.field public static final fre_privacy_notice_title:I = 0x7f0a024e

.field public static final fre_send_report_check:I = 0x7f0a0246

.field public static final fre_set_up_chrome:I = 0x7f0a0252

.field public static final fre_settings:I = 0x7f0a024c

.field public static final fre_signed_in_description:I = 0x7f0a0257

.field public static final fre_skip_text:I = 0x7f0a0249

.field public static final fre_tos_and_privacy:I = 0x7f0a0251

.field public static final fre_welcome:I = 0x7f0a024f

.field public static final fre_welcome_summary:I = 0x7f0a0250

.field public static final fullscreen_api_notification:I = 0x7f0a02b6

.field public static final geolocation_settings_page_summary_allowed:I = 0x7f0a022d

.field public static final geolocation_settings_page_summary_not_allowed:I = 0x7f0a022e

.field public static final geolocation_settings_page_title:I = 0x7f0a022c

.field public static final google_location_settings_title:I = 0x7f0a01e6

.field public static final help:I = 0x7f0a0131

.field public static final hint_find_in_page:I = 0x7f0a014a

.field public static final homepage_default_title:I = 0x7f0a01d9

.field public static final http_post_warning:I = 0x7f0a00ee

.field public static final http_post_warning_resend:I = 0x7f0a00ef

.field public static final i18n_address_line1_label:I = 0x7f0a0029

.field public static final i18n_area:I = 0x7f0a002c

.field public static final i18n_country_label:I = 0x7f0a0024

.field public static final i18n_county_label:I = 0x7f0a002d

.field public static final i18n_department:I = 0x7f0a002e

.field public static final i18n_dependent_locality_label:I = 0x7f0a0026

.field public static final i18n_do_si:I = 0x7f0a002f

.field public static final i18n_emirate:I = 0x7f0a0030

.field public static final i18n_island:I = 0x7f0a0031

.field public static final i18n_locality_label:I = 0x7f0a0025

.field public static final i18n_missing_required_field:I = 0x7f0a0037

.field public static final i18n_oblast:I = 0x7f0a0032

.field public static final i18n_organization_label:I = 0x7f0a0027

.field public static final i18n_parish:I = 0x7f0a0033

.field public static final i18n_postal_code_label:I = 0x7f0a002a

.field public static final i18n_prefecture:I = 0x7f0a0034

.field public static final i18n_province:I = 0x7f0a0035

.field public static final i18n_recipient_label:I = 0x7f0a0028

.field public static final i18n_state_label:I = 0x7f0a0036

.field public static final i18n_zip_code_label:I = 0x7f0a002b

.field public static final import_bookmarks:I = 0x7f0a016f

.field public static final import_bookmarks_failed_header:I = 0x7f0a0174

.field public static final import_bookmarks_failed_message:I = 0x7f0a0175

.field public static final import_bookmarks_ok:I = 0x7f0a0171

.field public static final import_bookmarks_progress_header:I = 0x7f0a0172

.field public static final import_bookmarks_progress_message:I = 0x7f0a0173

.field public static final import_bookmarks_prompt:I = 0x7f0a0170

.field public static final import_bookmarks_retry:I = 0x7f0a0176

.field public static final incompatible_libraries:I = 0x7f0a0179

.field public static final infobar_close:I = 0x7f0a00ad

.field public static final invalid_area:I = 0x7f0a003d

.field public static final invalid_bookmark:I = 0x7f0a016c

.field public static final invalid_county_label:I = 0x7f0a003e

.field public static final invalid_department:I = 0x7f0a003f

.field public static final invalid_dependent_locality_label:I = 0x7f0a003a

.field public static final invalid_do_si:I = 0x7f0a0040

.field public static final invalid_emirate:I = 0x7f0a0041

.field public static final invalid_entry:I = 0x7f0a0038

.field public static final invalid_island:I = 0x7f0a0042

.field public static final invalid_locality_label:I = 0x7f0a0039

.field public static final invalid_oblast:I = 0x7f0a0043

.field public static final invalid_parish:I = 0x7f0a0044

.field public static final invalid_postal_code_label:I = 0x7f0a003b

.field public static final invalid_prefecture:I = 0x7f0a0045

.field public static final invalid_province:I = 0x7f0a0046

.field public static final invalid_state_label:I = 0x7f0a0047

.field public static final invalid_zip_code_label:I = 0x7f0a003c

.field public static final javascript_version_title:I = 0x7f0a0268

.field public static final js_modal_dialog_cancel:I = 0x7f0a0097

.field public static final js_modal_dialog_confirm:I = 0x7f0a0096

.field public static final learn_more:I = 0x7f0a00fa

.field public static final leave_this_page:I = 0x7f0a0092

.field public static final light_mode:I = 0x7f0a00e2

.field public static final loading_bookmark:I = 0x7f0a0167

.field public static final location_settings_title:I = 0x7f0a01e5

.field public static final low_memory_error:I = 0x7f0a0069

.field public static final make_chrome_yours:I = 0x7f0a01c2

.field public static final manage_passwords_text:I = 0x7f0a01d3

.field public static final managed_by_your_administrator:I = 0x7f0a02f6

.field public static final media_player_error_button:I = 0x7f0a0063

.field public static final media_player_error_text_invalid_progressive_playback:I = 0x7f0a0061

.field public static final media_player_error_text_unknown:I = 0x7f0a0062

.field public static final media_player_error_title:I = 0x7f0a0060

.field public static final media_player_loading_video:I = 0x7f0a0064

.field public static final menu_add_to_homescreen:I = 0x7f0a011b

.field public static final menu_bookmarks:I = 0x7f0a011c

.field public static final menu_close_all_incognito_tabs:I = 0x7f0a0126

.field public static final menu_close_all_tabs:I = 0x7f0a0125

.field public static final menu_dismiss_btn:I = 0x7f0a00bf

.field public static final menu_find_in_page:I = 0x7f0a011f

.field public static final menu_help:I = 0x7f0a0124

.field public static final menu_history:I = 0x7f0a011e

.field public static final menu_new_incognito_tab:I = 0x7f0a011a

.field public static final menu_new_tab:I = 0x7f0a0119

.field public static final menu_preferences:I = 0x7f0a0123

.field public static final menu_print:I = 0x7f0a00c0

.field public static final menu_reader_mode:I = 0x7f0a0121

.field public static final menu_reader_mode_prefs:I = 0x7f0a0122

.field public static final menu_recent_tabs:I = 0x7f0a011d

.field public static final menu_request_desktop_site:I = 0x7f0a0120

.field public static final menu_share_page:I = 0x7f0a00c1

.field public static final monospace:I = 0x7f0a00e6

.field public static final month_picker_dialog_title:I = 0x7f0a007e

.field public static final most_visited_placeholder_summary:I = 0x7f0a0104

.field public static final most_visited_placeholder_title:I = 0x7f0a0103

.field public static final mr_media_route_button_content_description:I = 0x7f0a004a

.field public static final mr_media_route_chooser_searching:I = 0x7f0a004c

.field public static final mr_media_route_chooser_title:I = 0x7f0a004b

.field public static final mr_media_route_controller_disconnect:I = 0x7f0a004d

.field public static final mr_system_route_name:I = 0x7f0a0048

.field public static final mr_user_route_category_name:I = 0x7f0a0049

.field public static final native_startup_failed:I = 0x7f0a017a

.field public static final navigation_error_summary:I = 0x7f0a025a

.field public static final navigation_error_title:I = 0x7f0a0259

.field public static final network_prediction_always_value:I = 0x7f0a025f

.field public static final network_prediction_never_value:I = 0x7f0a0261

.field public static final network_prediction_wifi_only_value:I = 0x7f0a0260

.field public static final network_predictions_summary:I = 0x7f0a025e

.field public static final network_predictions_title:I = 0x7f0a025d

.field public static final never_prefetch_bandwidth_entry:I = 0x7f0a0242

.field public static final new_folder:I = 0x7f0a0165

.field public static final new_tab_incognito_header:I = 0x7f0a02e0

.field public static final new_tab_incognito_message:I = 0x7f0a02e1

.field public static final nfc_beam_error_bad_url:I = 0x7f0a0275

.field public static final nfc_beam_error_overlay_active:I = 0x7f0a0274

.field public static final no_bookmark_folders:I = 0x7f0a016b

.field public static final no_saved_website_settings:I = 0x7f0a0273

.field public static final no_thanks:I = 0x7f0a00fb

.field public static final ntp_bookmarks:I = 0x7f0a0101

.field public static final ntp_recent_tabs_accessibility_collapsed_group:I = 0x7f0a010e

.field public static final ntp_recent_tabs_accessibility_expanded_group:I = 0x7f0a010f

.field public static final ntp_recent_tabs_chrome_to_mobile_group:I = 0x7f0a010d

.field public static final ntp_recent_tabs_last_synced_days:I = 0x7f0a0109

.field public static final ntp_recent_tabs_last_synced_hours:I = 0x7f0a010a

.field public static final ntp_recent_tabs_last_synced_just_now:I = 0x7f0a010c

.field public static final ntp_recent_tabs_last_synced_minutes:I = 0x7f0a010b

.field public static final ntp_recent_tabs_remove_menu_option:I = 0x7f0a0110

.field public static final ntp_recent_tabs_sync_enable_sync:I = 0x7f0a0107

.field public static final ntp_recent_tabs_sync_enable_sync_button:I = 0x7f0a0108

.field public static final ntp_recent_tabs_sync_promo_instructions:I = 0x7f0a0106

.field public static final ntp_recent_tabs_sync_promo_title:I = 0x7f0a0105

.field public static final number_of_signed_in_accounts:I = 0x7f0a01c6

.field public static final ok:I = 0x7f0a00f7

.field public static final old_talkback_title:I = 0x7f0a00e8

.field public static final omnibox_confirm_delete:I = 0x7f0a02ef

.field public static final open_in_new_tab_toast:I = 0x7f0a01bc

.field public static final open_source_license_title:I = 0x7f0a026b

.field public static final open_source_license_url:I = 0x7f0a026c

.field public static final open_tabs:I = 0x7f0a0117

.field public static final opening_file_error:I = 0x7f0a006a

.field public static final options_homepage_title:I = 0x7f0a00f5

.field public static final options_startup_pages_placeholder:I = 0x7f0a00f6

.field public static final origin_settings_storage_bytes:I = 0x7f0a0239

.field public static final origin_settings_storage_gbytes:I = 0x7f0a023c

.field public static final origin_settings_storage_kbytes:I = 0x7f0a023a

.field public static final origin_settings_storage_mbytes:I = 0x7f0a023b

.field public static final origin_settings_storage_tbytes:I = 0x7f0a023d

.field public static final origin_settings_storage_usage:I = 0x7f0a0238

.field public static final os_version_missing_features:I = 0x7f0a0178

.field public static final os_version_title:I = 0x7f0a0266

.field public static final password_entry_editor_never_saved:I = 0x7f0a01d6

.field public static final password_entry_editor_title:I = 0x7f0a01cd

.field public static final policy_dialog_cancel:I = 0x7f0a00a3

.field public static final policy_dialog_message:I = 0x7f0a00a1

.field public static final policy_dialog_proceed:I = 0x7f0a00a2

.field public static final policy_dialog_title:I = 0x7f0a00a0

.field public static final popup_settings_page_summary_allowed:I = 0x7f0a023e

.field public static final popup_settings_page_summary_not_allowed:I = 0x7f0a023f

.field public static final preferences:I = 0x7f0a0127

.field public static final prefs_about_chrome:I = 0x7f0a0130

.field public static final prefs_accessibility:I = 0x7f0a012e

.field public static final prefs_autofill:I = 0x7f0a012b

.field public static final prefs_content_settings:I = 0x7f0a012f

.field public static final prefs_privacy:I = 0x7f0a012d

.field public static final prefs_saved_passwords:I = 0x7f0a012c

.field public static final prefs_search_engine:I = 0x7f0a012a

.field public static final prefs_section_advanced:I = 0x7f0a0129

.field public static final prefs_section_basics:I = 0x7f0a0128

.field public static final privacy_policy_title:I = 0x7f0a026f

.field public static final privacy_policy_url:I = 0x7f0a0270

.field public static final profile_path_title:I = 0x7f0a026a

.field public static final profiler_error_toast:I = 0x7f0a0068

.field public static final profiler_no_storage_toast:I = 0x7f0a0067

.field public static final profiler_started_toast:I = 0x7f0a0065

.field public static final profiler_stopped_toast:I = 0x7f0a0066

.field public static final protect_with_fingerprint:I = 0x7f0a02f3

.field public static final protected_content_learn_more_url:I = 0x7f0a01fd

.field public static final protected_content_prefs_description:I = 0x7f0a01f8

.field public static final protected_content_prefs_reset_fail_toast_description:I = 0x7f0a01f9

.field public static final protected_content_prefs_reset_message:I = 0x7f0a01fc

.field public static final protected_content_prefs_reset_title:I = 0x7f0a01fb

.field public static final protected_media_identifier_settings_page_summary_allowed:I = 0x7f0a0240

.field public static final protected_media_identifier_settings_page_summary_not_allowed:I = 0x7f0a0241

.field public static final reader_mode_omnibox:I = 0x7f0a0210

.field public static final recent_tabs:I = 0x7f0a0102

.field public static final recent_tabs_show_more:I = 0x7f0a0112

.field public static final recent_tabs_this_device:I = 0x7f0a0111

.field public static final recently_closed:I = 0x7f0a0113

.field public static final reduce_data_usage_description:I = 0x7f0a014e

.field public static final reduce_data_usage_learn_url:I = 0x7f0a0154

.field public static final reduce_data_usage_title:I = 0x7f0a014d

.field public static final reload_this_page:I = 0x7f0a0094

.field public static final remove:I = 0x7f0a0159

.field public static final remove_all:I = 0x7f0a0115

.field public static final removing_bookmark:I = 0x7f0a0169

.field public static final reset_device_credential:I = 0x7f0a01fa

.field public static final reset_translate_defaults:I = 0x7f0a0200

.field public static final rlz_access_point:I = 0x7f0a027b

.field public static final sad_tab_message:I = 0x7f0a00f3

.field public static final sad_tab_reload_label:I = 0x7f0a00f4

.field public static final sad_tab_suggestions:I = 0x7f0a02f5

.field public static final sad_tab_title:I = 0x7f0a00f2

.field public static final safe_browsing_description:I = 0x7f0a0155

.field public static final sans_serif:I = 0x7f0a00e4

.field public static final save:I = 0x7f0a00f9

.field public static final save_bookmark:I = 0x7f0a015a

.field public static final saved_passwords_none_text:I = 0x7f0a01d7

.field public static final saving_bookmark:I = 0x7f0a0168

.field public static final search_corpus_name:I = 0x7f0a02e5

.field public static final search_engine_name_and_domain:I = 0x7f0a0279

.field public static final search_or_type_url:I = 0x7f0a014c

.field public static final search_suggestion_intent_action:I = 0x7f0a02e8

.field public static final search_suggestion_intent_data:I = 0x7f0a02e9

.field public static final search_suggestion_text1:I = 0x7f0a02e6

.field public static final search_suggestion_text2:I = 0x7f0a02e7

.field public static final search_suggestions_summary:I = 0x7f0a025c

.field public static final search_suggestions_title:I = 0x7f0a025b

.field public static final section_saved_passwords:I = 0x7f0a01d4

.field public static final section_saved_passwords_exceptions:I = 0x7f0a01d5

.field public static final send_feedback:I = 0x7f0a01ba

.field public static final sepia_mode:I = 0x7f0a00e3

.field public static final serif:I = 0x7f0a00e5

.field public static final share_link_chooser_title:I = 0x7f0a00c2

.field public static final show_full_history:I = 0x7f0a0114

.field public static final sign_in_accounts:I = 0x7f0a0191

.field public static final sign_in_accounts_message:I = 0x7f0a0192

.field public static final sign_in_accounts_message_child:I = 0x7f0a0193

.field public static final sign_in_auto_login:I = 0x7f0a0198

.field public static final sign_in_auto_login_description:I = 0x7f0a0199

.field public static final sign_in_google_account:I = 0x7f0a0190

.field public static final sign_in_send_to_device:I = 0x7f0a0196

.field public static final sign_in_send_to_device_description:I = 0x7f0a0197

.field public static final sign_in_services:I = 0x7f0a0194

.field public static final sign_in_sync:I = 0x7f0a0195

.field public static final sign_in_to_chrome:I = 0x7f0a01c3

.field public static final sign_in_to_chrome_summary:I = 0x7f0a01c4

.field public static final sign_in_to_chrome_summary_variant:I = 0x7f0a01c5

.field public static final signout_managed_account_message:I = 0x7f0a01a3

.field public static final signout_managed_account_message_new:I = 0x7f0a01a4

.field public static final signout_message:I = 0x7f0a01a1

.field public static final signout_message_new:I = 0x7f0a01a2

.field public static final signout_signout:I = 0x7f0a01a5

.field public static final signout_title:I = 0x7f0a019f

.field public static final signout_title_new:I = 0x7f0a01a0

.field public static final smartcard_certificate_option:I = 0x7f0a00d9

.field public static final smartcard_dialog_title:I = 0x7f0a00d8

.field public static final snapshot_cannot_be_downloaded:I = 0x7f0a021d

.field public static final snapshot_cannot_be_opened:I = 0x7f0a021e

.field public static final snapshot_download_default_title:I = 0x7f0a0217

.field public static final snapshot_download_description_default:I = 0x7f0a0218

.field public static final snapshot_download_description_print:I = 0x7f0a0219

.field public static final snapshot_download_failed_infobar:I = 0x7f0a0214

.field public static final snapshot_downloaded_infobar:I = 0x7f0a0212

.field public static final snapshot_downloaded_infobar_button_open:I = 0x7f0a0213

.field public static final snapshot_downloading_infobar_text:I = 0x7f0a0211

.field public static final snapshot_error_opening_mime_type:I = 0x7f0a021b

.field public static final snapshot_external_public_directory_sub_path:I = 0x7f0a021a

.field public static final snapshot_external_storage_bad_removal:I = 0x7f0a0221

.field public static final snapshot_external_storage_checking:I = 0x7f0a0222

.field public static final snapshot_external_storage_generic_error:I = 0x7f0a0228

.field public static final snapshot_external_storage_nofs:I = 0x7f0a0223

.field public static final snapshot_external_storage_read_only:I = 0x7f0a0220

.field public static final snapshot_external_storage_removed:I = 0x7f0a0224

.field public static final snapshot_external_storage_shared:I = 0x7f0a0225

.field public static final snapshot_external_storage_unable_to_create_folder:I = 0x7f0a021f

.field public static final snapshot_external_storage_unmountable:I = 0x7f0a0226

.field public static final snapshot_external_storage_unmounted:I = 0x7f0a0227

.field public static final snapshot_visible_url_2:I = 0x7f0a021c

.field public static final stay_on_this_page:I = 0x7f0a0093

.field public static final store_picture_message:I = 0x7f0a0001

.field public static final store_picture_title:I = 0x7f0a0000

.field public static final suppress_js_modal_dialogs:I = 0x7f0a0098

.field public static final sync_android_master_sync_disabled:I = 0x7f0a0185

.field public static final sync_autofill:I = 0x7f0a017e

.field public static final sync_bookmarks:I = 0x7f0a017f

.field public static final sync_custom_passphrase:I = 0x7f0a01a7

.field public static final sync_dashboard_url:I = 0x7f0a01a8

.field public static final sync_data_types:I = 0x7f0a017c

.field public static final sync_encryption:I = 0x7f0a0183

.field public static final sync_enter_custom_passphrase_hint:I = 0x7f0a01a9

.field public static final sync_enter_custom_passphrase_hint_confirm:I = 0x7f0a01aa

.field public static final sync_enter_google_passphrase:I = 0x7f0a01ab

.field public static final sync_enter_google_passphrase_hint:I = 0x7f0a01ac

.field public static final sync_error_connection:I = 0x7f0a009d

.field public static final sync_error_domain:I = 0x7f0a009e

.field public static final sync_error_ga:I = 0x7f0a009c

.field public static final sync_error_generic:I = 0x7f0a009b

.field public static final sync_error_service_unavailable:I = 0x7f0a009f

.field public static final sync_everything_pref:I = 0x7f0a017d

.field public static final sync_history:I = 0x7f0a0180

.field public static final sync_is_disabled:I = 0x7f0a0187

.field public static final sync_is_enabled:I = 0x7f0a0186

.field public static final sync_loading:I = 0x7f0a01b2

.field public static final sync_manage_data:I = 0x7f0a0184

.field public static final sync_need_passphrase:I = 0x7f0a0189

.field public static final sync_need_password:I = 0x7f0a018a

.field public static final sync_passphrase_cannot_be_blank:I = 0x7f0a01ad

.field public static final sync_passphrase_incorrect:I = 0x7f0a01b0

.field public static final sync_passphrase_reset_instructions:I = 0x7f0a01ae

.field public static final sync_passphrase_type_custom:I = 0x7f0a018f

.field public static final sync_passphrase_type_frozen:I = 0x7f0a018e

.field public static final sync_passphrase_type_keystore:I = 0x7f0a018d

.field public static final sync_passphrase_type_none:I = 0x7f0a018c

.field public static final sync_passphrase_type_title:I = 0x7f0a018b

.field public static final sync_passphrases_do_not_match:I = 0x7f0a01af

.field public static final sync_passwords:I = 0x7f0a0181

.field public static final sync_pref_is_on:I = 0x7f0a0188

.field public static final sync_recent_tabs:I = 0x7f0a0182

.field public static final sync_setup_progress:I = 0x7f0a01a6

.field public static final sync_verifying:I = 0x7f0a01b1

.field public static final tab_loading_default_title:I = 0x7f0a00ac

.field public static final tabs_and_apps_got_it_button:I = 0x7f0a0302

.field public static final tabs_and_apps_opt_in_confirmation:I = 0x7f0a0307

.field public static final tabs_and_apps_opt_out:I = 0x7f0a0301

.field public static final tabs_and_apps_opt_out_confirmation:I = 0x7f0a0306

.field public static final tabs_and_apps_settings_description:I = 0x7f0a0303

.field public static final tabs_and_apps_title:I = 0x7f0a0300

.field public static final tabs_and_apps_turn_off_title:I = 0x7f0a0304

.field public static final tabs_and_apps_turn_on_title:I = 0x7f0a0305

.field public static final terms_of_service_title:I = 0x7f0a026d

.field public static final terms_of_service_url:I = 0x7f0a026e

.field public static final test_load_url_text:I = 0x7f0a02e3

.field public static final test_preload_url_text:I = 0x7f0a02e2

.field public static final test_url_to_load:I = 0x7f0a02e4

.field public static final text_off:I = 0x7f0a02b5

.field public static final text_on:I = 0x7f0a02b4

.field public static final text_size_signifier:I = 0x7f0a00e7

.field public static final time_picker_dialog_am:I = 0x7f0a0081

.field public static final time_picker_dialog_hour_minute_separator:I = 0x7f0a0089

.field public static final time_picker_dialog_minute_second_separator:I = 0x7f0a008a

.field public static final time_picker_dialog_pm:I = 0x7f0a0082

.field public static final time_picker_dialog_second_subsecond_separator:I = 0x7f0a008b

.field public static final time_picker_dialog_title:I = 0x7f0a0083

.field public static final translate_always_text:I = 0x7f0a00b4

.field public static final translate_button:I = 0x7f0a00b6

.field public static final translate_button_done:I = 0x7f0a00bc

.field public static final translate_infobar_change_languages:I = 0x7f0a00af

.field public static final translate_infobar_error:I = 0x7f0a00b3

.field public static final translate_infobar_text:I = 0x7f0a00ae

.field public static final translate_infobar_translating:I = 0x7f0a00b2

.field public static final translate_infobar_translation_done:I = 0x7f0a00b0

.field public static final translate_infobar_translation_more_options:I = 0x7f0a00b1

.field public static final translate_never_translate_language:I = 0x7f0a00be

.field public static final translate_never_translate_message_text:I = 0x7f0a00b5

.field public static final translate_never_translate_site:I = 0x7f0a00bd

.field public static final translate_nope:I = 0x7f0a00b7

.field public static final translate_options_source_hint:I = 0x7f0a00ba

.field public static final translate_options_target_hint:I = 0x7f0a00bb

.field public static final translate_prefs_description:I = 0x7f0a01fe

.field public static final translate_prefs_toast_description:I = 0x7f0a01ff

.field public static final translate_retry:I = 0x7f0a00b9

.field public static final translate_show_original:I = 0x7f0a00b8

.field public static final uca_account:I = 0x7f0a02f7

.field public static final undo_bar_button_text:I = 0x7f0a02f2

.field public static final undo_bar_close_message:I = 0x7f0a02f0

.field public static final undo_bar_delete_message:I = 0x7f0a02f1

.field public static final unsupported_number_of_windows:I = 0x7f0a02f4

.field public static final update_available_infobar:I = 0x7f0a0215

.field public static final update_available_infobar_button:I = 0x7f0a0216

.field public static final update_from_market:I = 0x7f0a00e9

.field public static final use_chrome_without_google_account:I = 0x7f0a019e

.field public static final video_audio_call_notification_text_2:I = 0x7f0a02c9

.field public static final video_call_notification_text_2:I = 0x7f0a02c7

.field public static final view_in_google_play:I = 0x7f0a01bb

.field public static final voice_search_error:I = 0x7f0a0177

.field public static final wallet_buy_button_place_holder:I = 0x7f0a0022

.field public static final webapp_activity_title:I = 0x7f0a02ec

.field public static final webrtc_call_notification_link_text:I = 0x7f0a02ca

.field public static final website_settings_embedded_in:I = 0x7f0a0232

.field public static final website_settings_midi_sysex_exceptions:I = 0x7f0a0233

.field public static final website_settings_midi_sysex_page_summary_allowed:I = 0x7f0a0234

.field public static final website_settings_midi_sysex_page_summary_not_allowed:I = 0x7f0a0235

.field public static final website_settings_popup_exceptions:I = 0x7f0a0236

.field public static final website_settings_protected_media_identifier_exceptions:I = 0x7f0a0237

.field public static final website_settings_summary_text_for_allow_camera_access:I = 0x7f0a02ce

.field public static final website_settings_summary_text_for_allow_mic_access:I = 0x7f0a02d0

.field public static final website_settings_summary_text_for_allow_mic_and_camera_access:I = 0x7f0a02d2

.field public static final website_settings_summary_text_for_deny_camera_access:I = 0x7f0a02cf

.field public static final website_settings_summary_text_for_deny_mic_access:I = 0x7f0a02d1

.field public static final website_settings_summary_text_for_deny_mic_and_camera_access:I = 0x7f0a02d3

.field public static final website_settings_title_video:I = 0x7f0a02cb

.field public static final website_settings_title_voice:I = 0x7f0a02cc

.field public static final website_settings_title_voice_and_video:I = 0x7f0a02cd

.field public static final webstorage_clear_data_dialog_message:I = 0x7f0a0230

.field public static final webstorage_clear_data_dialog_ok_button:I = 0x7f0a0231

.field public static final webstorage_clear_data_dialog_title:I = 0x7f0a022f

.field public static final week_picker_dialog_title:I = 0x7f0a0080

.field public static final wifi_prefetch_bandwidth_entry:I = 0x7f0a0243

.field public static final wiping_profile_data_message:I = 0x7f0a00a5

.field public static final wiping_profile_data_title:I = 0x7f0a00a4


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 4265
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
