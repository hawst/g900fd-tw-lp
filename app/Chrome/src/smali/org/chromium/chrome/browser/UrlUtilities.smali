.class public Lorg/chromium/chrome/browser/UrlUtilities;
.super Ljava/lang/Object;
.source "UrlUtilities.java"


# static fields
.field private static final ACCEPTED_SCHEMES:Ljava/util/HashSet;

.field private static final DOWNLOADABLE_SCHEMES:Ljava/util/HashSet;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 24
    const/4 v0, 0x7

    new-array v0, v0, [Ljava/lang/String;

    const-string/jumbo v1, "about"

    aput-object v1, v0, v3

    const-string/jumbo v1, "data"

    aput-object v1, v0, v4

    const-string/jumbo v1, "file"

    aput-object v1, v0, v5

    const-string/jumbo v1, "http"

    aput-object v1, v0, v6

    const-string/jumbo v1, "https"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string/jumbo v2, "inline"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string/jumbo v2, "javascript"

    aput-object v2, v0, v1

    invoke-static {v0}, Lorg/chromium/base/CollectionUtil;->newHashSet([Ljava/lang/Object;)Ljava/util/HashSet;

    move-result-object v0

    sput-object v0, Lorg/chromium/chrome/browser/UrlUtilities;->ACCEPTED_SCHEMES:Ljava/util/HashSet;

    .line 30
    new-array v0, v7, [Ljava/lang/String;

    const-string/jumbo v1, "data"

    aput-object v1, v0, v3

    const-string/jumbo v1, "filesystem"

    aput-object v1, v0, v4

    const-string/jumbo v1, "http"

    aput-object v1, v0, v5

    const-string/jumbo v1, "https"

    aput-object v1, v0, v6

    invoke-static {v0}, Lorg/chromium/base/CollectionUtil;->newHashSet([Ljava/lang/Object;)Ljava/util/HashSet;

    move-result-object v0

    sput-object v0, Lorg/chromium/chrome/browser/UrlUtilities;->DOWNLOADABLE_SCHEMES:Ljava/util/HashSet;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static fixUrl(Ljava/lang/String;)Ljava/lang/String;
    .locals 8

    .prologue
    const/4 v0, 0x0

    .line 83
    if-nez p0, :cond_0

    move-object p0, v0

    .line 108
    :goto_0
    return-object p0

    .line 86
    :cond_0
    :try_start_0
    invoke-virtual {p0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 87
    const-string/jumbo v1, "://"

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_1

    .line 88
    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "http"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    .line 90
    :cond_1
    const-string/jumbo v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    const/4 v2, -0x1

    if-ne v1, v2, :cond_2

    .line 91
    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "http://"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    .line 94
    :cond_2
    new-instance v7, Ljava/net/URI;

    invoke-direct {v7, v0}, Ljava/net/URI;-><init>(Ljava/lang/String;)V

    .line 95
    invoke-virtual {v7}, Ljava/net/URI;->getScheme()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_3

    .line 96
    new-instance v0, Ljava/net/URI;

    const-string/jumbo v1, "http"

    const/4 v2, 0x0

    invoke-virtual {v7}, Ljava/net/URI;->getHost()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v7}, Ljava/net/URI;->getPort()I

    move-result v4

    invoke-virtual {v7}, Ljava/net/URI;->getRawPath()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v7}, Ljava/net/URI;->getRawQuery()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v7}, Ljava/net/URI;->getRawFragment()Ljava/lang/String;

    move-result-object v7

    invoke-direct/range {v0 .. v7}, Ljava/net/URI;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 105
    :goto_1
    invoke-virtual {v0}, Ljava/net/URI;->toString()Ljava/lang/String;
    :try_end_0
    .catch Ljava/net/URISyntaxException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object p0

    goto :goto_0

    .line 108
    :catch_0
    move-exception v0

    goto :goto_0

    :cond_3
    move-object v0, v7

    goto :goto_1
.end method

.method public static fixupUrl(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 122
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lorg/chromium/chrome/browser/UrlUtilities;->nativeFixupUrl(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getDomainAndRegistry(Ljava/lang/String;Z)Ljava/lang/String;
    .locals 1

    .prologue
    .line 197
    invoke-static {p0, p1}, Lorg/chromium/chrome/browser/UrlUtilities;->nativeGetDomainAndRegistry(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getOriginForDisplay(Ljava/net/URI;Z)Ljava/lang/String;
    .locals 4

    .prologue
    .line 134
    invoke-virtual {p0}, Ljava/net/URI;->getScheme()Ljava/lang/String;

    move-result-object v0

    .line 135
    invoke-virtual {p0}, Ljava/net/URI;->getHost()Ljava/lang/String;

    move-result-object v1

    .line 136
    invoke-virtual {p0}, Ljava/net/URI;->getPort()I

    move-result v2

    .line 139
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 140
    :cond_0
    invoke-virtual {p0}, Ljava/net/URI;->toString()Ljava/lang/String;

    move-result-object v0

    .line 156
    :goto_0
    return-object v0

    .line 142
    :cond_1
    if-eqz p1, :cond_4

    .line 143
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v3, "://"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 148
    :goto_1
    const/4 v3, -0x1

    if-eq v2, v3, :cond_3

    const/16 v3, 0x50

    if-ne v2, v3, :cond_2

    const-string/jumbo v3, "http"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_3

    :cond_2
    const/16 v3, 0x1bb

    if-ne v2, v3, :cond_5

    const-string/jumbo v3, "https"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 150
    :cond_3
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 145
    :cond_4
    const-string/jumbo v0, ""

    goto :goto_1

    .line 152
    :cond_5
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static isAcceptedScheme(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 49
    :try_start_0
    new-instance v0, Ljava/net/URI;

    invoke-direct {v0, p0}, Ljava/net/URI;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lorg/chromium/chrome/browser/UrlUtilities;->isAcceptedScheme(Ljava/net/URI;)Z
    :try_end_0
    .catch Ljava/net/URISyntaxException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 51
    :goto_0
    return v0

    :catch_0
    move-exception v0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isAcceptedScheme(Ljava/net/URI;)Z
    .locals 2

    .prologue
    .line 39
    sget-object v0, Lorg/chromium/chrome/browser/UrlUtilities;->ACCEPTED_SCHEMES:Ljava/util/HashSet;

    invoke-virtual {p0}, Ljava/net/URI;->getScheme()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public static isDownloadableScheme(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 71
    :try_start_0
    new-instance v0, Ljava/net/URI;

    invoke-direct {v0, p0}, Ljava/net/URI;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lorg/chromium/chrome/browser/UrlUtilities;->isDownloadableScheme(Ljava/net/URI;)Z
    :try_end_0
    .catch Ljava/net/URISyntaxException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 73
    :goto_0
    return v0

    :catch_0
    move-exception v0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isDownloadableScheme(Ljava/net/URI;)Z
    .locals 2

    .prologue
    .line 61
    sget-object v0, Lorg/chromium/chrome/browser/UrlUtilities;->DOWNLOADABLE_SCHEMES:Ljava/util/HashSet;

    invoke-virtual {p0}, Ljava/net/URI;->getScheme()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public static isGooglePropertyUrl(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 205
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    .line 206
    :goto_0
    return v0

    :cond_0
    invoke-static {p0}, Lorg/chromium/chrome/browser/UrlUtilities;->nativeIsGooglePropertyUrl(Ljava/lang/String;)Z

    move-result v0

    goto :goto_0
.end method

.method public static native nativeFixupUrl(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
.end method

.method private static native nativeGetDomainAndRegistry(Ljava/lang/String;Z)Ljava/lang/String;
.end method

.method public static native nativeIsGoogleHomePageUrl(Ljava/lang/String;)Z
.end method

.method private static native nativeIsGooglePropertyUrl(Ljava/lang/String;)Z
.end method

.method public static native nativeIsGoogleSearchUrl(Ljava/lang/String;)Z
.end method

.method private static native nativeSameDomainOrHost(Ljava/lang/String;Ljava/lang/String;Z)Z
.end method

.method public static sameDomainOrHost(Ljava/lang/String;Ljava/lang/String;Z)Z
    .locals 1

    .prologue
    .line 181
    invoke-static {p0, p1, p2}, Lorg/chromium/chrome/browser/UrlUtilities;->nativeSameDomainOrHost(Ljava/lang/String;Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method
