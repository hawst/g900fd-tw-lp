.class Lorg/chromium/chrome/browser/KeyStoreSelectionDialog;
.super Landroid/app/DialogFragment;
.source "KeyStoreSelectionDialog.java"


# static fields
.field private static final SYSTEM_STORE:Ljava/lang/CharSequence;


# instance fields
.field private final mCancelCallback:Ljava/lang/Runnable;

.field private mSelectedChoice:Ljava/lang/Runnable;

.field private final mSmartCardCallback:Ljava/lang/Runnable;

.field private final mSystemCallback:Ljava/lang/Runnable;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 21
    const-string/jumbo v0, "Android"

    sput-object v0, Lorg/chromium/chrome/browser/KeyStoreSelectionDialog;->SYSTEM_STORE:Ljava/lang/CharSequence;

    return-void
.end method

.method public constructor <init>(Ljava/lang/Runnable;Ljava/lang/Runnable;Ljava/lang/Runnable;)V
    .locals 1

    .prologue
    .line 39
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    .line 40
    iput-object p1, p0, Lorg/chromium/chrome/browser/KeyStoreSelectionDialog;->mSystemCallback:Ljava/lang/Runnable;

    .line 41
    iput-object p2, p0, Lorg/chromium/chrome/browser/KeyStoreSelectionDialog;->mSmartCardCallback:Ljava/lang/Runnable;

    .line 42
    iput-object p3, p0, Lorg/chromium/chrome/browser/KeyStoreSelectionDialog;->mCancelCallback:Ljava/lang/Runnable;

    .line 45
    iget-object v0, p0, Lorg/chromium/chrome/browser/KeyStoreSelectionDialog;->mSmartCardCallback:Ljava/lang/Runnable;

    iput-object v0, p0, Lorg/chromium/chrome/browser/KeyStoreSelectionDialog;->mSelectedChoice:Ljava/lang/Runnable;

    .line 46
    return-void
.end method

.method static synthetic access$000(Lorg/chromium/chrome/browser/KeyStoreSelectionDialog;)Ljava/lang/Runnable;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lorg/chromium/chrome/browser/KeyStoreSelectionDialog;->mCancelCallback:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$100(Lorg/chromium/chrome/browser/KeyStoreSelectionDialog;)Ljava/lang/Runnable;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lorg/chromium/chrome/browser/KeyStoreSelectionDialog;->mSelectedChoice:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$102(Lorg/chromium/chrome/browser/KeyStoreSelectionDialog;Ljava/lang/Runnable;)Ljava/lang/Runnable;
    .locals 0

    .prologue
    .line 20
    iput-object p1, p0, Lorg/chromium/chrome/browser/KeyStoreSelectionDialog;->mSelectedChoice:Ljava/lang/Runnable;

    return-object p1
.end method

.method static synthetic access$200()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 20
    sget-object v0, Lorg/chromium/chrome/browser/KeyStoreSelectionDialog;->SYSTEM_STORE:Ljava/lang/CharSequence;

    return-object v0
.end method

.method static synthetic access$300(Lorg/chromium/chrome/browser/KeyStoreSelectionDialog;)Ljava/lang/Runnable;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lorg/chromium/chrome/browser/KeyStoreSelectionDialog;->mSystemCallback:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$400(Lorg/chromium/chrome/browser/KeyStoreSelectionDialog;)Ljava/lang/Runnable;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lorg/chromium/chrome/browser/KeyStoreSelectionDialog;->mSmartCardCallback:Ljava/lang/Runnable;

    return-object v0
.end method


# virtual methods
.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 50
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/CharSequence;

    sget v1, Lorg/chromium/chrome/R$string;->smartcard_certificate_option:I

    invoke-virtual {p0, v1}, Lorg/chromium/chrome/browser/KeyStoreSelectionDialog;->getString(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v3

    const/4 v1, 0x1

    sget-object v2, Lorg/chromium/chrome/browser/KeyStoreSelectionDialog;->SYSTEM_STORE:Ljava/lang/CharSequence;

    aput-object v2, v0, v1

    .line 54
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lorg/chromium/chrome/browser/KeyStoreSelectionDialog;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    sget v2, Lorg/chromium/chrome/R$string;->smartcard_dialog_title:I

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    new-instance v2, Lorg/chromium/chrome/browser/KeyStoreSelectionDialog$3;

    invoke-direct {v2, p0, v0}, Lorg/chromium/chrome/browser/KeyStoreSelectionDialog$3;-><init>(Lorg/chromium/chrome/browser/KeyStoreSelectionDialog;[Ljava/lang/CharSequence;)V

    invoke-virtual {v1, v0, v3, v2}, Landroid/app/AlertDialog$Builder;->setSingleChoiceItems([Ljava/lang/CharSequence;ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x104000a

    new-instance v2, Lorg/chromium/chrome/browser/KeyStoreSelectionDialog$2;

    invoke-direct {v2, p0}, Lorg/chromium/chrome/browser/KeyStoreSelectionDialog$2;-><init>(Lorg/chromium/chrome/browser/KeyStoreSelectionDialog;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    sget v1, Lorg/chromium/chrome/R$string;->cancel:I

    new-instance v2, Lorg/chromium/chrome/browser/KeyStoreSelectionDialog$1;

    invoke-direct {v2, p0}, Lorg/chromium/chrome/browser/KeyStoreSelectionDialog$1;-><init>(Lorg/chromium/chrome/browser/KeyStoreSelectionDialog;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 79
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0
.end method

.method public onDismiss(Landroid/content/DialogInterface;)V
    .locals 0

    .prologue
    .line 84
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onDismiss(Landroid/content/DialogInterface;)V

    .line 85
    return-void
.end method
