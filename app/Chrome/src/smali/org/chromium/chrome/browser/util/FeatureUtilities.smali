.class public Lorg/chromium/chrome/browser/util/FeatureUtilities;
.super Ljava/lang/Object;
.source "FeatureUtilities.java"


# static fields
.field private static sHasGoogleAccountAuthenticator:Ljava/lang/Boolean;

.field private static sHasRecognitionIntentHandler:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static canAllowSync(Landroid/content/Context;)Z
    .locals 1

    .prologue
    .line 60
    invoke-static {p0}, Lorg/chromium/chrome/browser/util/FeatureUtilities;->hasGoogleAccountAuthenticator(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p0}, Lorg/chromium/chrome/browser/util/FeatureUtilities;->hasSyncPermissions(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    invoke-static {p0}, Lorg/chromium/chrome/browser/util/FeatureUtilities;->hasGoogleAccounts(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static hasGoogleAccountAuthenticator(Landroid/content/Context;)Z
    .locals 1

    .prologue
    .line 66
    sget-object v0, Lorg/chromium/chrome/browser/util/FeatureUtilities;->sHasGoogleAccountAuthenticator:Ljava/lang/Boolean;

    if-nez v0, :cond_0

    .line 67
    invoke-static {p0}, Lorg/chromium/sync/signin/AccountManagerHelper;->get(Landroid/content/Context;)Lorg/chromium/sync/signin/AccountManagerHelper;

    move-result-object v0

    .line 68
    invoke-virtual {v0}, Lorg/chromium/sync/signin/AccountManagerHelper;->hasGoogleAccountAuthenticator()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lorg/chromium/chrome/browser/util/FeatureUtilities;->sHasGoogleAccountAuthenticator:Ljava/lang/Boolean;

    .line 70
    :cond_0
    sget-object v0, Lorg/chromium/chrome/browser/util/FeatureUtilities;->sHasGoogleAccountAuthenticator:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method static hasGoogleAccounts(Landroid/content/Context;)Z
    .locals 1

    .prologue
    .line 75
    invoke-static {p0}, Lorg/chromium/sync/signin/AccountManagerHelper;->get(Landroid/content/Context;)Lorg/chromium/sync/signin/AccountManagerHelper;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/sync/signin/AccountManagerHelper;->hasGoogleAccounts()Z

    move-result v0

    return v0
.end method

.method private static hasSyncPermissions(Landroid/content/Context;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 81
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x12

    if-ge v0, v3, :cond_0

    move v0, v1

    .line 85
    :goto_0
    return v0

    .line 83
    :cond_0
    const-string/jumbo v0, "user"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/UserManager;

    .line 84
    invoke-virtual {v0}, Landroid/os/UserManager;->getUserRestrictions()Landroid/os/Bundle;

    move-result-object v0

    .line 85
    const-string/jumbo v3, "no_modify_accounts"

    invoke-virtual {v0, v3, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_0
.end method

.method public static isRecognitionIntentPresent(Landroid/content/Context;Z)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 43
    invoke-static {}, Lorg/chromium/base/ThreadUtils;->assertOnUiThread()V

    .line 44
    sget-object v1, Lorg/chromium/chrome/browser/util/FeatureUtilities;->sHasRecognitionIntentHandler:Ljava/lang/Boolean;

    if-eqz v1, :cond_0

    if-nez p1, :cond_2

    .line 45
    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 46
    new-instance v2, Landroid/content/Intent;

    const-string/jumbo v3, "android.speech.action.RECOGNIZE_SPEECH"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2, v0}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v1

    .line 48
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_1

    const/4 v0, 0x1

    :cond_1
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lorg/chromium/chrome/browser/util/FeatureUtilities;->sHasRecognitionIntentHandler:Ljava/lang/Boolean;

    .line 51
    :cond_2
    sget-object v0, Lorg/chromium/chrome/browser/util/FeatureUtilities;->sHasRecognitionIntentHandler:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method private static native nativeSetDocumentModeEnabled(Z)V
.end method

.method public static setDocumentModeEnabled(Z)V
    .locals 0

    .prologue
    .line 93
    invoke-static {p0}, Lorg/chromium/chrome/browser/util/FeatureUtilities;->nativeSetDocumentModeEnabled(Z)V

    .line 94
    return-void
.end method
