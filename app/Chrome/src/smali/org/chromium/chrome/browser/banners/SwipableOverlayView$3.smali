.class Lorg/chromium/chrome/browser/banners/SwipableOverlayView$3;
.super Ljava/lang/Object;
.source "SwipableOverlayView.java"

# interfaces
.implements Landroid/view/View$OnLayoutChangeListener;


# instance fields
.field final synthetic this$0:Lorg/chromium/chrome/browser/banners/SwipableOverlayView;


# direct methods
.method constructor <init>(Lorg/chromium/chrome/browser/banners/SwipableOverlayView;)V
    .locals 0

    .prologue
    .line 390
    iput-object p1, p0, Lorg/chromium/chrome/browser/banners/SwipableOverlayView$3;->this$0:Lorg/chromium/chrome/browser/banners/SwipableOverlayView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onLayoutChange(Landroid/view/View;IIIIIIII)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 394
    iget-object v0, p0, Lorg/chromium/chrome/browser/banners/SwipableOverlayView$3;->this$0:Lorg/chromium/chrome/browser/banners/SwipableOverlayView;

    invoke-virtual {v0, p0}, Lorg/chromium/chrome/browser/banners/SwipableOverlayView;->removeOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    .line 397
    iget-object v0, p0, Lorg/chromium/chrome/browser/banners/SwipableOverlayView$3;->this$0:Lorg/chromium/chrome/browser/banners/SwipableOverlayView;

    iget-object v1, p0, Lorg/chromium/chrome/browser/banners/SwipableOverlayView$3;->this$0:Lorg/chromium/chrome/browser/banners/SwipableOverlayView;

    # getter for: Lorg/chromium/chrome/browser/banners/SwipableOverlayView;->mTotalHeight:I
    invoke-static {v1}, Lorg/chromium/chrome/browser/banners/SwipableOverlayView;->access$1000(Lorg/chromium/chrome/browser/banners/SwipableOverlayView;)I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Lorg/chromium/chrome/browser/banners/SwipableOverlayView;->setTranslationY(F)V

    .line 398
    iget-object v0, p0, Lorg/chromium/chrome/browser/banners/SwipableOverlayView$3;->this$0:Lorg/chromium/chrome/browser/banners/SwipableOverlayView;

    # setter for: Lorg/chromium/chrome/browser/banners/SwipableOverlayView;->mIsBeingDisplayedForFirstTime:Z
    invoke-static {v0, v2}, Lorg/chromium/chrome/browser/banners/SwipableOverlayView;->access$1102(Lorg/chromium/chrome/browser/banners/SwipableOverlayView;Z)Z

    .line 399
    iget-object v0, p0, Lorg/chromium/chrome/browser/banners/SwipableOverlayView$3;->this$0:Lorg/chromium/chrome/browser/banners/SwipableOverlayView;

    invoke-virtual {v0, v2}, Lorg/chromium/chrome/browser/banners/SwipableOverlayView;->createVerticalSnapAnimation(Z)V

    .line 400
    iget-object v0, p0, Lorg/chromium/chrome/browser/banners/SwipableOverlayView$3;->this$0:Lorg/chromium/chrome/browser/banners/SwipableOverlayView;

    # getter for: Lorg/chromium/chrome/browser/banners/SwipableOverlayView;->mCurrentAnimation:Landroid/animation/AnimatorSet;
    invoke-static {v0}, Lorg/chromium/chrome/browser/banners/SwipableOverlayView;->access$1200(Lorg/chromium/chrome/browser/banners/SwipableOverlayView;)Landroid/animation/AnimatorSet;

    move-result-object v0

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->start()V

    .line 401
    return-void
.end method
