.class public Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem;
.super Landroid/widget/FrameLayout;
.source "AccessibilityTabModelListItem.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private mActiveAnimation:Landroid/animation/Animator;

.field private final mActuallyCloseAnimatorListener:Landroid/animation/AnimatorListenerAdapter;

.field private mCanScrollListener:Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListView;

.field private mCanUndo:Z

.field private mCloseAnimationDurationMs:I

.field private final mCloseAnimatorListener:Landroid/animation/AnimatorListenerAdapter;

.field private mCloseButton:Landroid/widget/ImageButton;

.field private final mCloseRunnable:Ljava/lang/Runnable;

.field private mCloseTimeoutMs:I

.field private mDefaultAnimationDurationMs:I

.field private final mDefaultHeight:I

.field private mFaviconView:Landroid/widget/ImageView;

.field private final mFlingCommitDistance:F

.field private final mHandler:Landroid/os/Handler;

.field private mListener:Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem$AccessibilityTabModelListItemListener;

.field private final mSwipeCommitDistance:F

.field private final mSwipeGestureDetector:Landroid/view/GestureDetector;

.field private mSwipedAway:F

.field private mTab:Lorg/chromium/chrome/browser/Tab;

.field private mTabContents:Landroid/widget/LinearLayout;

.field private final mTabObserver:Lorg/chromium/chrome/browser/TabObserver;

.field private mTitleView:Landroid/widget/TextView;

.field private mUndoButton:Landroid/widget/Button;

.field private mUndoContents:Landroid/widget/LinearLayout;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3

    .prologue
    .line 194
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 118
    new-instance v0, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem$1;

    invoke-direct {v0, p0}, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem$1;-><init>(Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem;)V

    iput-object v0, p0, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem;->mCloseRunnable:Ljava/lang/Runnable;

    .line 125
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem;->mHandler:Landroid/os/Handler;

    .line 130
    new-instance v0, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem$2;

    invoke-direct {v0, p0}, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem$2;-><init>(Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem;)V

    iput-object v0, p0, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem;->mCloseAnimatorListener:Landroid/animation/AnimatorListenerAdapter;

    .line 162
    new-instance v0, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem$3;

    invoke-direct {v0, p0}, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem$3;-><init>(Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem;)V

    iput-object v0, p0, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem;->mActuallyCloseAnimatorListener:Landroid/animation/AnimatorListenerAdapter;

    .line 331
    new-instance v0, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem$4;

    invoke-direct {v0, p0}, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem$4;-><init>(Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem;)V

    iput-object v0, p0, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem;->mTabObserver:Lorg/chromium/chrome/browser/TabObserver;

    .line 195
    new-instance v0, Landroid/view/GestureDetector;

    new-instance v1, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem$SwipeGestureListener;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem$SwipeGestureListener;-><init>(Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem;Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem$1;)V

    invoke-direct {v0, p1, v1}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v0, p0, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem;->mSwipeGestureDetector:Landroid/view/GestureDetector;

    .line 196
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lorg/chromium/chrome/R$dimen;->swipe_commit_distance:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    iput v0, p0, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem;->mSwipeCommitDistance:F

    .line 198
    iget v0, p0, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem;->mSwipeCommitDistance:F

    const/high16 v1, 0x40400000    # 3.0f

    div-float/2addr v0, v1

    iput v0, p0, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem;->mFlingCommitDistance:F

    .line 200
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lorg/chromium/chrome/R$dimen;->accessibility_tab_height:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    iput v0, p0, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem;->mDefaultHeight:I

    .line 203
    const/16 v0, 0x64

    iput v0, p0, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem;->mCloseAnimationDurationMs:I

    .line 204
    const/16 v0, 0x12c

    iput v0, p0, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem;->mDefaultAnimationDurationMs:I

    .line 205
    const/16 v0, 0x7d0

    iput v0, p0, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem;->mCloseTimeoutMs:I

    .line 206
    return-void
.end method

.method static synthetic access$000(Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem;)V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0}, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem;->runCloseAnimation()V

    return-void
.end method

.method static synthetic access$100(Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem;)Lorg/chromium/chrome/browser/Tab;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem;->mTab:Lorg/chromium/chrome/browser/Tab;

    return-object v0
.end method

.method static synthetic access$1000(Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem;)V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0}, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem;->cancelRunningAnimation()V

    return-void
.end method

.method static synthetic access$1200(Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem;)V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0}, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem;->updateFavicon()V

    return-void
.end method

.method static synthetic access$1300(Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem;Lorg/chromium/chrome/browser/Tab;)V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0, p1}, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem;->notifyTabUpdated(Lorg/chromium/chrome/browser/Tab;)V

    return-void
.end method

.method static synthetic access$1400(Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem;)V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0}, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem;->updateTabTitle()V

    return-void
.end method

.method static synthetic access$1500(Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem;)Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListView;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem;->mCanScrollListener:Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListView;

    return-object v0
.end method

.method static synthetic access$1600(Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem;)F
    .locals 1

    .prologue
    .line 39
    iget v0, p0, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem;->mFlingCommitDistance:F

    return v0
.end method

.method static synthetic access$1700(Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem;)I
    .locals 1

    .prologue
    .line 39
    iget v0, p0, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem;->mDefaultAnimationDurationMs:I

    return v0
.end method

.method static synthetic access$1800(Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem;J)V
    .locals 1

    .prologue
    .line 39
    invoke-direct {p0, p1, p2}, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem;->runSwipeAnimation(J)V

    return-void
.end method

.method static synthetic access$200(Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem;)Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem$AccessibilityTabModelListItemListener;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem;->mListener:Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem$AccessibilityTabModelListItemListener;

    return-object v0
.end method

.method static synthetic access$300(Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem;Z)V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0, p1}, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem;->showUndoView(Z)V

    return-void
.end method

.method static synthetic access$400(Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem;Z)V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0, p1}, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem;->runResetAnimation(Z)V

    return-void
.end method

.method static synthetic access$500(Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem;)Ljava/lang/Runnable;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem;->mCloseRunnable:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$600(Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem;)I
    .locals 1

    .prologue
    .line 39
    iget v0, p0, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem;->mCloseTimeoutMs:I

    return v0
.end method

.method static synthetic access$700(Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$800(Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem;)Landroid/widget/LinearLayout;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem;->mTabContents:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$900(Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem;)Landroid/widget/LinearLayout;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem;->mUndoContents:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method private cancelRunningAnimation()V
    .locals 1

    .prologue
    .line 533
    iget-object v0, p0, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem;->mActiveAnimation:Landroid/animation/Animator;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem;->mActiveAnimation:Landroid/animation/Animator;

    invoke-virtual {v0}, Landroid/animation/Animator;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem;->mActiveAnimation:Landroid/animation/Animator;

    invoke-virtual {v0}, Landroid/animation/Animator;->cancel()V

    .line 535
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem;->mActiveAnimation:Landroid/animation/Animator;

    .line 536
    return-void
.end method

.method private notifyTabUpdated(Lorg/chromium/chrome/browser/Tab;)V
    .locals 2

    .prologue
    .line 539
    iget-object v0, p0, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem;->mListener:Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem$AccessibilityTabModelListItemListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem;->mListener:Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem$AccessibilityTabModelListItemListener;

    invoke-virtual {p1}, Lorg/chromium/chrome/browser/Tab;->getId()I

    move-result v1

    invoke-interface {v0, v1}, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem$AccessibilityTabModelListItemListener;->tabChanged(I)V

    .line 540
    :cond_0
    return-void
.end method

.method private runBlinkOutAnimation()V
    .locals 7

    .prologue
    const/4 v4, 0x0

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 501
    invoke-direct {p0}, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem;->cancelRunningAnimation()V

    .line 502
    iput v4, p0, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem;->mSwipedAway:F

    .line 504
    sget-object v0, Landroid/view/View;->SCALE_X:Landroid/util/Property;

    new-array v1, v6, [F

    const v2, 0x3f99999a    # 1.2f

    aput v2, v1, v5

    invoke-static {p0, v0, v1}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 505
    sget-object v1, Landroid/view/View;->SCALE_Y:Landroid/util/Property;

    new-array v2, v6, [F

    aput v4, v2, v5

    invoke-static {p0, v1, v2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    .line 506
    sget-object v2, Landroid/view/View;->ALPHA:Landroid/util/Property;

    new-array v3, v6, [F

    aput v4, v3, v5

    invoke-static {p0, v2, v3}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v2

    .line 508
    new-instance v3, Landroid/animation/AnimatorSet;

    invoke-direct {v3}, Landroid/animation/AnimatorSet;-><init>()V

    .line 509
    const/4 v4, 0x3

    new-array v4, v4, [Landroid/animation/Animator;

    aput-object v2, v4, v5

    aput-object v1, v4, v6

    const/4 v1, 0x2

    aput-object v0, v4, v1

    invoke-virtual {v3, v4}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    .line 510
    iget-object v0, p0, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem;->mCloseAnimatorListener:Landroid/animation/AnimatorListenerAdapter;

    invoke-virtual {v3, v0}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 511
    iget v0, p0, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem;->mCloseAnimationDurationMs:I

    int-to-long v0, v0

    invoke-virtual {v3, v0, v1}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    .line 512
    invoke-virtual {v3}, Landroid/animation/AnimatorSet;->start()V

    .line 514
    iput-object v3, p0, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem;->mActiveAnimation:Landroid/animation/Animator;

    .line 515
    return-void
.end method

.method private runCloseAnimation()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 518
    invoke-direct {p0}, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem;->cancelRunningAnimation()V

    .line 520
    const-string/jumbo v0, "height"

    new-array v1, v5, [I

    aput v4, v1, v4

    invoke-static {p0, v0, v1}, Landroid/animation/ObjectAnimator;->ofInt(Ljava/lang/Object;Ljava/lang/String;[I)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 521
    sget-object v1, Landroid/view/View;->SCALE_Y:Landroid/util/Property;

    new-array v2, v5, [F

    const/4 v3, 0x0

    aput v3, v2, v4

    invoke-static {p0, v1, v2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    .line 523
    new-instance v2, Landroid/animation/AnimatorSet;

    invoke-direct {v2}, Landroid/animation/AnimatorSet;-><init>()V

    .line 524
    const/4 v3, 0x2

    new-array v3, v3, [Landroid/animation/Animator;

    aput-object v0, v3, v4

    aput-object v1, v3, v5

    invoke-virtual {v2, v3}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    .line 525
    iget-object v0, p0, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem;->mActuallyCloseAnimatorListener:Landroid/animation/AnimatorListenerAdapter;

    invoke-virtual {v2, v0}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 526
    iget v0, p0, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem;->mDefaultAnimationDurationMs:I

    int-to-long v0, v0

    invoke-virtual {v2, v0, v1}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    .line 527
    invoke-virtual {v2}, Landroid/animation/AnimatorSet;->start()V

    .line 529
    iput-object v2, p0, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem;->mActiveAnimation:Landroid/animation/Animator;

    .line 530
    return-void
.end method

.method private runResetAnimation(Z)V
    .locals 9

    .prologue
    const/high16 v5, 0x3f800000    # 1.0f

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 483
    invoke-direct {p0}, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem;->cancelRunningAnimation()V

    .line 485
    sget-object v0, Landroid/view/View;->TRANSLATION_X:Landroid/util/Property;

    new-array v1, v8, [F

    const/4 v2, 0x0

    aput v2, v1, v7

    invoke-static {p0, v0, v1}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 486
    sget-object v1, Landroid/view/View;->ALPHA:Landroid/util/Property;

    new-array v2, v8, [F

    aput v5, v2, v7

    invoke-static {p0, v1, v2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    .line 487
    sget-object v2, Landroid/view/View;->SCALE_X:Landroid/util/Property;

    new-array v3, v8, [F

    aput v5, v3, v7

    invoke-static {p0, v2, v3}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v2

    .line 488
    sget-object v3, Landroid/view/View;->SCALE_Y:Landroid/util/Property;

    new-array v4, v8, [F

    aput v5, v4, v7

    invoke-static {p0, v3, v4}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v3

    .line 489
    const-string/jumbo v4, "height"

    new-array v5, v8, [I

    iget v6, p0, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem;->mDefaultHeight:I

    aput v6, v5, v7

    invoke-static {p0, v4, v5}, Landroid/animation/ObjectAnimator;->ofInt(Ljava/lang/Object;Ljava/lang/String;[I)Landroid/animation/ObjectAnimator;

    move-result-object v4

    .line 491
    new-instance v5, Landroid/animation/AnimatorSet;

    invoke-direct {v5}, Landroid/animation/AnimatorSet;-><init>()V

    .line 492
    const/4 v6, 0x5

    new-array v6, v6, [Landroid/animation/Animator;

    aput-object v0, v6, v7

    aput-object v1, v6, v8

    const/4 v0, 0x2

    aput-object v2, v6, v0

    const/4 v0, 0x3

    aput-object v3, v6, v0

    const/4 v0, 0x4

    aput-object v4, v6, v0

    invoke-virtual {v5, v6}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    .line 493
    if-eqz p1, :cond_0

    iget v0, p0, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem;->mCloseAnimationDurationMs:I

    int-to-long v0, v0

    :goto_0
    invoke-virtual {v5, v0, v1}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    .line 495
    invoke-virtual {v5}, Landroid/animation/AnimatorSet;->start()V

    .line 497
    iput-object v5, p0, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem;->mActiveAnimation:Landroid/animation/Animator;

    .line 498
    return-void

    .line 493
    :cond_0
    iget v0, p0, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem;->mDefaultAnimationDurationMs:I

    int-to-long v0, v0

    goto :goto_0
.end method

.method private runSwipeAnimation(J)V
    .locals 7

    .prologue
    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 466
    invoke-direct {p0}, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem;->cancelRunningAnimation()V

    .line 467
    invoke-virtual {p0}, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem;->getTranslationX()F

    move-result v0

    iput v0, p0, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem;->mSwipedAway:F

    .line 469
    sget-object v1, Landroid/view/View;->TRANSLATION_X:Landroid/util/Property;

    new-array v2, v5, [F

    invoke-virtual {p0}, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem;->getTranslationX()F

    move-result v0

    cmpl-float v0, v0, v3

    if-lez v0, :cond_0

    invoke-virtual {p0}, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem;->getWidth()I

    move-result v0

    int-to-float v0, v0

    :goto_0
    aput v0, v2, v4

    invoke-static {p0, v1, v2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 471
    sget-object v1, Landroid/view/View;->ALPHA:Landroid/util/Property;

    new-array v2, v5, [F

    aput v3, v2, v4

    invoke-static {p0, v1, v2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    .line 473
    new-instance v2, Landroid/animation/AnimatorSet;

    invoke-direct {v2}, Landroid/animation/AnimatorSet;-><init>()V

    .line 474
    const/4 v3, 0x2

    new-array v3, v3, [Landroid/animation/Animator;

    aput-object v1, v3, v4

    aput-object v0, v3, v5

    invoke-virtual {v2, v3}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    .line 475
    iget-object v0, p0, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem;->mCloseAnimatorListener:Landroid/animation/AnimatorListenerAdapter;

    invoke-virtual {v2, v0}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 476
    iget v0, p0, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem;->mDefaultAnimationDurationMs:I

    int-to-long v0, v0

    invoke-static {p1, p2, v0, v1}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    invoke-virtual {v2, v0, v1}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    .line 477
    invoke-virtual {v2}, Landroid/animation/AnimatorSet;->start()V

    .line 479
    iput-object v2, p0, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem;->mActiveAnimation:Landroid/animation/Animator;

    .line 480
    return-void

    .line 469
    :cond_0
    invoke-virtual {p0}, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem;->getWidth()I

    move-result v0

    neg-int v0, v0

    int-to-float v0, v0

    goto :goto_0
.end method

.method private showUndoView(Z)V
    .locals 3

    .prologue
    const/4 v2, 0x4

    const/4 v1, 0x0

    .line 242
    if-eqz p1, :cond_0

    iget-boolean v0, p0, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem;->mCanUndo:Z

    if-eqz v0, :cond_0

    .line 243
    iget-object v0, p0, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem;->mUndoContents:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 244
    iget-object v0, p0, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem;->mTabContents:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 251
    :goto_0
    return-void

    .line 246
    :cond_0
    iget-object v0, p0, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem;->mTabContents:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 247
    iget-object v0, p0, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem;->mUndoContents:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 248
    invoke-direct {p0}, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem;->updateTabTitle()V

    .line 249
    invoke-direct {p0}, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem;->updateFavicon()V

    goto :goto_0
.end method

.method private updateFavicon()V
    .locals 2

    .prologue
    .line 280
    iget-object v0, p0, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem;->mTab:Lorg/chromium/chrome/browser/Tab;

    if-eqz v0, :cond_0

    .line 281
    iget-object v0, p0, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem;->mTab:Lorg/chromium/chrome/browser/Tab;

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/Tab;->getFavicon()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 282
    if-eqz v0, :cond_1

    .line 283
    iget-object v1, p0, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem;->mFaviconView:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 288
    :cond_0
    :goto_0
    return-void

    .line 285
    :cond_1
    iget-object v0, p0, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem;->mFaviconView:Landroid/widget/ImageView;

    sget v1, Lorg/chromium/chrome/R$drawable;->globe_incognito_favicon:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0
.end method

.method private updateTabTitle()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 264
    iget-object v0, p0, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem;->mTab:Lorg/chromium/chrome/browser/Tab;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem;->mTab:Lorg/chromium/chrome/browser/Tab;

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/Tab;->getTitle()Ljava/lang/String;

    move-result-object v0

    .line 265
    :goto_0
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 266
    :cond_0
    invoke-virtual {p0}, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lorg/chromium/chrome/R$string;->tab_loading_default_title:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 269
    :cond_1
    iget-object v1, p0, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem;->mTitleView:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem;->mTitleView:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 271
    :cond_2
    invoke-virtual {p0}, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem;->getContext()Landroid/content/Context;

    move-result-object v1

    sget v2, Lorg/chromium/chrome/R$string;->accessibility_tabstrip_tab:I

    new-array v3, v5, [Ljava/lang/Object;

    aput-object v0, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 273
    invoke-virtual {p0}, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem;->getContentDescription()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 274
    invoke-virtual {p0}, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem;->getContext()Landroid/content/Context;

    move-result-object v1

    sget v2, Lorg/chromium/chrome/R$string;->accessibility_tabstrip_tab:I

    new-array v3, v5, [Ljava/lang/Object;

    aput-object v0, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 277
    :cond_3
    return-void

    .line 264
    :cond_4
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public disableAnimations()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 454
    iput v0, p0, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem;->mCloseAnimationDurationMs:I

    .line 455
    iput v0, p0, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem;->mDefaultAnimationDurationMs:I

    .line 456
    iput v0, p0, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem;->mCloseTimeoutMs:I

    .line 457
    return-void
.end method

.method public hasPendingClosure()Z
    .locals 2

    .prologue
    .line 461
    iget-object v0, p0, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem;->mListener:Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem$AccessibilityTabModelListItemListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem;->mListener:Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem$AccessibilityTabModelListItemListener;

    iget-object v1, p0, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem;->mTab:Lorg/chromium/chrome/browser/Tab;

    invoke-virtual {v1}, Lorg/chromium/chrome/browser/Tab;->getId()I

    move-result v1

    invoke-interface {v0, v1}, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem$AccessibilityTabModelListItemListener;->hasPendingClosure(I)Z

    move-result v0

    .line 462
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 292
    iget-object v0, p0, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem;->mListener:Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem$AccessibilityTabModelListItemListener;

    if-nez v0, :cond_1

    .line 322
    :cond_0
    :goto_0
    return-void

    .line 294
    :cond_1
    iget-object v0, p0, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem;->mTab:Lorg/chromium/chrome/browser/Tab;

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/Tab;->getId()I

    move-result v0

    .line 295
    if-ne p1, p0, :cond_2

    iget-object v1, p0, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem;->mListener:Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem$AccessibilityTabModelListItemListener;

    invoke-interface {v1, v0}, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem$AccessibilityTabModelListItemListener;->hasPendingClosure(I)Z

    move-result v1

    if-nez v1, :cond_2

    .line 296
    iget-object v1, p0, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem;->mListener:Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem$AccessibilityTabModelListItemListener;

    invoke-interface {v1, v0}, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem$AccessibilityTabModelListItemListener;->tabSelected(I)V

    goto :goto_0

    .line 297
    :cond_2
    iget-object v1, p0, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem;->mCloseButton:Landroid/widget/ImageButton;

    if-ne p1, v1, :cond_4

    .line 298
    iget-boolean v0, p0, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem;->mCanUndo:Z

    if-eqz v0, :cond_3

    .line 299
    invoke-direct {p0}, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem;->runBlinkOutAnimation()V

    goto :goto_0

    .line 301
    :cond_3
    invoke-direct {p0}, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem;->runCloseAnimation()V

    goto :goto_0

    .line 303
    :cond_4
    iget-object v1, p0, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem;->mUndoButton:Landroid/widget/Button;

    if-ne p1, v1, :cond_0

    .line 305
    iget-object v1, p0, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem;->mHandler:Landroid/os/Handler;

    iget-object v2, p0, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem;->mCloseRunnable:Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 307
    iget-object v1, p0, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem;->mListener:Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem$AccessibilityTabModelListItemListener;

    invoke-interface {v1, v0}, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem$AccessibilityTabModelListItemListener;->cancelPendingClosure(I)V

    .line 308
    invoke-direct {p0, v4}, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem;->showUndoView(Z)V

    .line 309
    invoke-virtual {p0, v3}, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem;->setAlpha(F)V

    .line 310
    iget v0, p0, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem;->mSwipedAway:F

    cmpl-float v0, v0, v3

    if-lez v0, :cond_5

    .line 311
    invoke-virtual {p0}, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem;->getWidth()I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {p0, v0}, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem;->setTranslationX(F)V

    .line 312
    invoke-direct {p0, v4}, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem;->runResetAnimation(Z)V

    goto :goto_0

    .line 313
    :cond_5
    iget v0, p0, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem;->mSwipedAway:F

    cmpg-float v0, v0, v3

    if-gez v0, :cond_6

    .line 314
    invoke-virtual {p0}, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem;->getWidth()I

    move-result v0

    neg-int v0, v0

    int-to-float v0, v0

    invoke-virtual {p0, v0}, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem;->setTranslationX(F)V

    .line 315
    invoke-direct {p0, v4}, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem;->runResetAnimation(Z)V

    goto :goto_0

    .line 317
    :cond_6
    const v0, 0x3f99999a    # 1.2f

    invoke-virtual {p0, v0}, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem;->setScaleX(F)V

    .line 318
    invoke-virtual {p0, v3}, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem;->setScaleY(F)V

    .line 319
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem;->runResetAnimation(Z)V

    goto :goto_0
.end method

.method protected onDetachedFromWindow()V
    .locals 2

    .prologue
    .line 326
    invoke-super {p0}, Landroid/widget/FrameLayout;->onDetachedFromWindow()V

    .line 327
    iget-object v0, p0, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem;->mTab:Lorg/chromium/chrome/browser/Tab;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem;->mTab:Lorg/chromium/chrome/browser/Tab;

    iget-object v1, p0, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem;->mTabObserver:Lorg/chromium/chrome/browser/TabObserver;

    invoke-virtual {v0, v1}, Lorg/chromium/chrome/browser/Tab;->removeObserver(Lorg/chromium/chrome/browser/TabObserver;)V

    .line 328
    :cond_0
    invoke-direct {p0}, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem;->cancelRunningAnimation()V

    .line 329
    return-void
.end method

.method public onFinishInflate()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 210
    invoke-super {p0}, Landroid/widget/FrameLayout;->onFinishInflate()V

    .line 211
    sget v0, Lorg/chromium/chrome/R$id;->tab_contents:I

    invoke-virtual {p0, v0}, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem;->mTabContents:Landroid/widget/LinearLayout;

    .line 212
    sget v0, Lorg/chromium/chrome/R$id;->tab_title:I

    invoke-virtual {p0, v0}, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem;->mTitleView:Landroid/widget/TextView;

    .line 213
    sget v0, Lorg/chromium/chrome/R$id;->tab_favicon:I

    invoke-virtual {p0, v0}, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem;->mFaviconView:Landroid/widget/ImageView;

    .line 214
    sget v0, Lorg/chromium/chrome/R$id;->close_btn:I

    invoke-virtual {p0, v0}, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem;->mCloseButton:Landroid/widget/ImageButton;

    .line 216
    sget v0, Lorg/chromium/chrome/R$id;->undo_contents:I

    invoke-virtual {p0, v0}, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem;->mUndoContents:Landroid/widget/LinearLayout;

    .line 217
    sget v0, Lorg/chromium/chrome/R$id;->undo_button:I

    invoke-virtual {p0, v0}, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem;->mUndoButton:Landroid/widget/Button;

    .line 219
    invoke-virtual {p0, v1}, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem;->setClickable(Z)V

    .line 220
    invoke-virtual {p0, v1}, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem;->setFocusable(Z)V

    .line 222
    iget-object v0, p0, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem;->mCloseButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 223
    iget-object v0, p0, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem;->mUndoButton:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 224
    invoke-virtual {p0, p0}, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 225
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 354
    iget-object v1, p0, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem;->mHandler:Landroid/os/Handler;

    iget-object v2, p0, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem;->mCloseRunnable:Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 356
    iget-object v1, p0, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem;->mSwipeGestureDetector:Landroid/view/GestureDetector;

    invoke-virtual {v1, p1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v1

    .line 357
    if-eqz v1, :cond_0

    .line 367
    :goto_0
    return v0

    .line 358
    :cond_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v1

    if-ne v1, v0, :cond_2

    .line 359
    invoke-virtual {p0}, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem;->getTranslationX()F

    move-result v1

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    iget v2, p0, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem;->mSwipeCommitDistance:F

    cmpl-float v1, v1, v2

    if-lez v1, :cond_1

    .line 360
    const-wide/16 v2, 0x12c

    invoke-direct {p0, v2, v3}, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem;->runSwipeAnimation(J)V

    .line 364
    :goto_1
    iget-object v1, p0, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem;->mCanScrollListener:Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListView;

    invoke-virtual {v1, v0}, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListView;->setCanScroll(Z)V

    goto :goto_0

    .line 362
    :cond_1
    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem;->runResetAnimation(Z)V

    goto :goto_1

    .line 367
    :cond_2
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public resetState()V
    .locals 4

    .prologue
    const/high16 v1, 0x3f800000    # 1.0f

    .line 390
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem;->setTranslationX(F)V

    .line 391
    invoke-virtual {p0, v1}, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem;->setAlpha(F)V

    .line 392
    invoke-virtual {p0, v1}, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem;->setScaleX(F)V

    .line 393
    invoke-virtual {p0, v1}, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem;->setScaleY(F)V

    .line 394
    iget v0, p0, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem;->mDefaultHeight:I

    invoke-virtual {p0, v0}, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem;->setHeight(I)V

    .line 395
    invoke-direct {p0}, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem;->cancelRunningAnimation()V

    .line 397
    iget-object v0, p0, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem;->mCloseRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 399
    iget-object v0, p0, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem;->mListener:Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem$AccessibilityTabModelListItemListener;

    if-eqz v0, :cond_1

    .line 400
    iget-object v0, p0, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem;->mListener:Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem$AccessibilityTabModelListItemListener;

    iget-object v1, p0, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem;->mTab:Lorg/chromium/chrome/browser/Tab;

    invoke-virtual {v1}, Lorg/chromium/chrome/browser/Tab;->getId()I

    move-result v1

    invoke-interface {v0, v1}, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem$AccessibilityTabModelListItemListener;->hasPendingClosure(I)Z

    move-result v0

    .line 401
    invoke-direct {p0, v0}, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem;->showUndoView(Z)V

    .line 402
    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem;->mCloseRunnable:Ljava/lang/Runnable;

    iget v2, p0, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem;->mCloseTimeoutMs:I

    int-to-long v2, v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 406
    :cond_0
    :goto_0
    return-void

    .line 404
    :cond_1
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem;->showUndoView(Z)V

    goto :goto_0
.end method

.method public setHeight(I)V
    .locals 2

    .prologue
    .line 376
    invoke-virtual {p0}, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/AbsListView$LayoutParams;

    .line 377
    if-nez v0, :cond_1

    .line 378
    new-instance v0, Landroid/widget/AbsListView$LayoutParams;

    const/4 v1, -0x1

    invoke-direct {v0, v1, p1}, Landroid/widget/AbsListView$LayoutParams;-><init>(II)V

    .line 383
    :goto_0
    invoke-virtual {p0, v0}, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 384
    :cond_0
    return-void

    .line 380
    :cond_1
    iget v1, v0, Landroid/widget/AbsListView$LayoutParams;->height:I

    if-eq v1, p1, :cond_0

    .line 381
    iput p1, v0, Landroid/widget/AbsListView$LayoutParams;->height:I

    goto :goto_0
.end method

.method public setListeners(Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem$AccessibilityTabModelListItemListener;Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListView;)V
    .locals 0

    .prologue
    .line 259
    iput-object p1, p0, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem;->mListener:Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem$AccessibilityTabModelListItemListener;

    .line 260
    iput-object p2, p0, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem;->mCanScrollListener:Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListView;

    .line 261
    return-void
.end method

.method public setTab(Lorg/chromium/chrome/browser/Tab;Z)V
    .locals 2

    .prologue
    .line 233
    iget-object v0, p0, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem;->mTab:Lorg/chromium/chrome/browser/Tab;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem;->mTab:Lorg/chromium/chrome/browser/Tab;

    iget-object v1, p0, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem;->mTabObserver:Lorg/chromium/chrome/browser/TabObserver;

    invoke-virtual {v0, v1}, Lorg/chromium/chrome/browser/Tab;->removeObserver(Lorg/chromium/chrome/browser/TabObserver;)V

    .line 234
    :cond_0
    iput-object p1, p0, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem;->mTab:Lorg/chromium/chrome/browser/Tab;

    .line 235
    iget-object v0, p0, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem;->mTabObserver:Lorg/chromium/chrome/browser/TabObserver;

    invoke-virtual {p1, v0}, Lorg/chromium/chrome/browser/Tab;->addObserver(Lorg/chromium/chrome/browser/TabObserver;)V

    .line 236
    iput-boolean p2, p0, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem;->mCanUndo:Z

    .line 237
    invoke-direct {p0}, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem;->updateTabTitle()V

    .line 238
    invoke-direct {p0}, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem;->updateFavicon()V

    .line 239
    return-void
.end method
