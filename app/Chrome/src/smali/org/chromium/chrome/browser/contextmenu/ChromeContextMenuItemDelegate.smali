.class public interface abstract Lorg/chromium/chrome/browser/contextmenu/ChromeContextMenuItemDelegate;
.super Ljava/lang/Object;
.source "ChromeContextMenuItemDelegate.java"


# virtual methods
.method public abstract canLoadOriginalImage()Z
.end method

.method public abstract getPageUrl()Ljava/lang/String;
.end method

.method public abstract isIncognito()Z
.end method

.method public abstract isIncognitoSupported()Z
.end method

.method public abstract onOpenImageInNewTab(Ljava/lang/String;Lorg/chromium/content_public/common/Referrer;)V
.end method

.method public abstract onOpenImageUrl(Ljava/lang/String;Lorg/chromium/content_public/common/Referrer;)V
.end method

.method public abstract onOpenInNewIncognitoTab(Ljava/lang/String;)V
.end method

.method public abstract onOpenInNewTab(Ljava/lang/String;Lorg/chromium/content_public/common/Referrer;)V
.end method

.method public abstract onSaveImageToClipboard(Ljava/lang/String;)V
.end method

.method public abstract onSaveToClipboard(Ljava/lang/String;Z)V
.end method

.method public abstract onSearchByImageInNewTab()V
.end method

.method public abstract startDownload(Ljava/lang/String;Z)Z
.end method
