.class public final Lorg/chromium/chrome/browser/signin/OAuth2TokenService;
.super Ljava/lang/Object;
.source "OAuth2TokenService.java"


# static fields
.field static final synthetic $assertionsDisabled:Z

.field public static final STORED_ACCOUNTS_KEY:Ljava/lang/String; = "google.services.stored_accounts"


# instance fields
.field private final mNativeProfileOAuth2TokenService:J

.field private final mObservers:Lorg/chromium/base/ObserverList;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 37
    const-class v0, Lorg/chromium/chrome/browser/signin/OAuth2TokenService;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/chromium/chrome/browser/signin/OAuth2TokenService;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private constructor <init>(J)V
    .locals 1

    .prologue
    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 60
    iput-wide p1, p0, Lorg/chromium/chrome/browser/signin/OAuth2TokenService;->mNativeProfileOAuth2TokenService:J

    .line 61
    new-instance v0, Lorg/chromium/base/ObserverList;

    invoke-direct {v0}, Lorg/chromium/base/ObserverList;-><init>()V

    iput-object v0, p0, Lorg/chromium/chrome/browser/signin/OAuth2TokenService;->mObservers:Lorg/chromium/base/ObserverList;

    .line 62
    return-void
.end method

.method static synthetic access$000(Ljava/lang/String;ZJ)V
    .locals 0

    .prologue
    .line 37
    invoke-static {p0, p1, p2, p3}, Lorg/chromium/chrome/browser/signin/OAuth2TokenService;->nativeOAuth2TokenFetched(Ljava/lang/String;ZJ)V

    return-void
.end method

.method private static create(J)Lorg/chromium/chrome/browser/signin/OAuth2TokenService;
    .locals 2

    .prologue
    .line 71
    invoke-static {}, Lorg/chromium/base/ThreadUtils;->assertOnUiThread()V

    .line 72
    new-instance v0, Lorg/chromium/chrome/browser/signin/OAuth2TokenService;

    invoke-direct {v0, p0, p1}, Lorg/chromium/chrome/browser/signin/OAuth2TokenService;-><init>(J)V

    return-object v0
.end method

.method private static getAccountOrNullFromUsername(Landroid/content/Context;Ljava/lang/String;)Landroid/accounts/Account;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 86
    if-nez p1, :cond_0

    .line 87
    const-string/jumbo v1, "OAuth2TokenService"

    const-string/jumbo v2, "Username is null"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 97
    :goto_0
    return-object v0

    .line 91
    :cond_0
    invoke-static {p0}, Lorg/chromium/sync/signin/AccountManagerHelper;->get(Landroid/content/Context;)Lorg/chromium/sync/signin/AccountManagerHelper;

    move-result-object v1

    .line 92
    invoke-virtual {v1, p1}, Lorg/chromium/sync/signin/AccountManagerHelper;->getAccountFromName(Ljava/lang/String;)Landroid/accounts/Account;

    move-result-object v1

    .line 93
    if-nez v1, :cond_1

    .line 94
    const-string/jumbo v1, "OAuth2TokenService"

    const-string/jumbo v2, "Account not found for provided username."

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_1
    move-object v0, v1

    .line 97
    goto :goto_0
.end method

.method public static getAccounts(Landroid/content/Context;)[Ljava/lang/String;
    .locals 1

    .prologue
    .line 119
    invoke-static {p0}, Lorg/chromium/chrome/browser/signin/OAuth2TokenService;->getStoredAccounts(Landroid/content/Context;)[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getForProfile(Lorg/chromium/chrome/browser/profiles/Profile;)Lorg/chromium/chrome/browser/signin/OAuth2TokenService;
    .locals 1

    .prologue
    .line 65
    invoke-static {}, Lorg/chromium/base/ThreadUtils;->assertOnUiThread()V

    .line 66
    invoke-static {p0}, Lorg/chromium/chrome/browser/signin/OAuth2TokenService;->nativeGetForProfile(Lorg/chromium/chrome/browser/profiles/Profile;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/chromium/chrome/browser/signin/OAuth2TokenService;

    return-object v0
.end method

.method public static getOAuth2AccessToken(Landroid/content/Context;Landroid/app/Activity;Landroid/accounts/Account;Ljava/lang/String;Lorg/chromium/sync/signin/AccountManagerHelper$GetAuthTokenCallback;)V
    .locals 2

    .prologue
    .line 161
    new-instance v0, Ljava/lang/StringBuilder;

    const-string/jumbo v1, "oauth2:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 162
    invoke-static {p0}, Lorg/chromium/sync/signin/AccountManagerHelper;->get(Landroid/content/Context;)Lorg/chromium/sync/signin/AccountManagerHelper;

    move-result-object v1

    invoke-virtual {v1, p1, p2, v0, p4}, Lorg/chromium/sync/signin/AccountManagerHelper;->getAuthTokenFromForeground(Landroid/app/Activity;Landroid/accounts/Account;Ljava/lang/String;Lorg/chromium/sync/signin/AccountManagerHelper$GetAuthTokenCallback;)V

    .line 164
    return-void
.end method

.method public static getOAuth2AccessTokenWithTimeout(Landroid/content/Context;Landroid/app/Activity;Landroid/accounts/Account;Ljava/lang/String;JLjava/util/concurrent/TimeUnit;)Ljava/lang/String;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 181
    sget-boolean v0, Lorg/chromium/chrome/browser/signin/OAuth2TokenService;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    invoke-static {}, Lorg/chromium/base/ThreadUtils;->runningOnUiThread()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 182
    :cond_0
    new-instance v0, Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    .line 183
    new-instance v2, Ljava/util/concurrent/Semaphore;

    const/4 v3, 0x0

    invoke-direct {v2, v3}, Ljava/util/concurrent/Semaphore;-><init>(I)V

    .line 184
    new-instance v3, Lorg/chromium/chrome/browser/signin/OAuth2TokenService$2;

    invoke-direct {v3, v0, v2}, Lorg/chromium/chrome/browser/signin/OAuth2TokenService$2;-><init>(Ljava/util/concurrent/atomic/AtomicReference;Ljava/util/concurrent/Semaphore;)V

    invoke-static {p0, p1, p2, p3, v3}, Lorg/chromium/chrome/browser/signin/OAuth2TokenService;->getOAuth2AccessToken(Landroid/content/Context;Landroid/app/Activity;Landroid/accounts/Account;Ljava/lang/String;Lorg/chromium/sync/signin/AccountManagerHelper$GetAuthTokenCallback;)V

    .line 194
    :try_start_0
    invoke-virtual {v2, p4, p5, p6}, Ljava/util/concurrent/Semaphore;->tryAcquire(JLjava/util/concurrent/TimeUnit;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 195
    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 203
    :goto_0
    return-object v0

    .line 197
    :cond_1
    const-string/jumbo v0, "OAuth2TokenService"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "Failed to retrieve auth token within timeout ("

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p4, p5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " + "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p6}, Ljava/util/concurrent/TimeUnit;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    move-object v0, v1

    .line 199
    goto :goto_0

    .line 202
    :catch_0
    move-exception v0

    const-string/jumbo v0, "OAuth2TokenService"

    const-string/jumbo v2, "Got interrupted while waiting for auth token"

    invoke-static {v0, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, v1

    .line 203
    goto :goto_0
.end method

.method public static getOAuth2AuthToken(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;J)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 132
    invoke-static {p0, p1}, Lorg/chromium/chrome/browser/signin/OAuth2TokenService;->getAccountOrNullFromUsername(Landroid/content/Context;Ljava/lang/String;)Landroid/accounts/Account;

    move-result-object v0

    .line 133
    if-nez v0, :cond_0

    .line 134
    const/4 v0, 0x0

    invoke-static {v4, v0, p3, p4}, Lorg/chromium/chrome/browser/signin/OAuth2TokenService;->nativeOAuth2TokenFetched(Ljava/lang/String;ZJ)V

    .line 148
    :goto_0
    return-void

    .line 137
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "oauth2:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 139
    invoke-static {p0}, Lorg/chromium/sync/signin/AccountManagerHelper;->get(Landroid/content/Context;)Lorg/chromium/sync/signin/AccountManagerHelper;

    move-result-object v2

    .line 140
    new-instance v3, Lorg/chromium/chrome/browser/signin/OAuth2TokenService$1;

    invoke-direct {v3, p3, p4}, Lorg/chromium/chrome/browser/signin/OAuth2TokenService$1;-><init>(J)V

    invoke-virtual {v2, v4, v0, v1, v3}, Lorg/chromium/sync/signin/AccountManagerHelper;->getAuthTokenFromForeground(Landroid/app/Activity;Landroid/accounts/Account;Ljava/lang/String;Lorg/chromium/sync/signin/AccountManagerHelper$GetAuthTokenCallback;)V

    goto :goto_0
.end method

.method private static getStoredAccounts(Landroid/content/Context;)[Ljava/lang/String;
    .locals 3

    .prologue
    .line 290
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string/jumbo v1, "google.services.stored_accounts"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getStringSet(Ljava/lang/String;Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    .line 293
    if-nez v0, :cond_0

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v1

    new-array v1, v1, [Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    goto :goto_0
.end method

.method public static getSystemAccounts(Landroid/content/Context;)[Ljava/lang/String;
    .locals 2

    .prologue
    .line 106
    invoke-static {p0}, Lorg/chromium/sync/signin/AccountManagerHelper;->get(Landroid/content/Context;)Lorg/chromium/sync/signin/AccountManagerHelper;

    move-result-object v0

    .line 107
    invoke-virtual {v0}, Lorg/chromium/sync/signin/AccountManagerHelper;->getGoogleAccountNames()Ljava/util/List;

    move-result-object v0

    .line 108
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    new-array v1, v1, [Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    return-object v0
.end method

.method public static hasOAuth2RefreshToken(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 212
    invoke-static {p0}, Lorg/chromium/sync/signin/AccountManagerHelper;->get(Landroid/content/Context;)Lorg/chromium/sync/signin/AccountManagerHelper;

    move-result-object v0

    invoke-virtual {v0, p1}, Lorg/chromium/sync/signin/AccountManagerHelper;->hasAccountForName(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static invalidateOAuth2AuthToken(Landroid/content/Context;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 220
    if-eqz p1, :cond_0

    .line 221
    invoke-static {p0}, Lorg/chromium/sync/signin/AccountManagerHelper;->get(Landroid/content/Context;)Lorg/chromium/sync/signin/AccountManagerHelper;

    move-result-object v0

    invoke-virtual {v0, p1}, Lorg/chromium/sync/signin/AccountManagerHelper;->invalidateAuthToken(Ljava/lang/String;)V

    .line 223
    :cond_0
    return-void
.end method

.method private native nativeFireRefreshTokenAvailableFromJava(JLjava/lang/String;)V
.end method

.method private native nativeFireRefreshTokenRevokedFromJava(JLjava/lang/String;)V
.end method

.method private native nativeFireRefreshTokensLoadedFromJava(J)V
.end method

.method private static native nativeGetForProfile(Lorg/chromium/chrome/browser/profiles/Profile;)Ljava/lang/Object;
.end method

.method private static native nativeOAuth2TokenFetched(Ljava/lang/String;ZJ)V
.end method

.method private native nativeValidateAccounts(JLjava/lang/String;Z)V
.end method

.method private static saveStoredAccounts(Landroid/content/Context;[Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 298
    new-instance v0, Ljava/util/HashSet;

    invoke-static {p1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    .line 299
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string/jumbo v2, "google.services.stored_accounts"

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences$Editor;->putStringSet(Ljava/lang/String;Ljava/util/Set;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 301
    return-void
.end method


# virtual methods
.method public final notifyRefreshTokenAvailable(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 247
    sget-boolean v0, Lorg/chromium/chrome/browser/signin/OAuth2TokenService;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 248
    :cond_0
    invoke-static {p1}, Lorg/chromium/sync/signin/AccountManagerHelper;->createAccountFromName(Ljava/lang/String;)Landroid/accounts/Account;

    move-result-object v1

    .line 249
    iget-object v0, p0, Lorg/chromium/chrome/browser/signin/OAuth2TokenService;->mObservers:Lorg/chromium/base/ObserverList;

    invoke-virtual {v0}, Lorg/chromium/base/ObserverList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/chromium/chrome/browser/signin/OAuth2TokenService$OAuth2TokenServiceObserver;

    .line 250
    invoke-interface {v0, v1}, Lorg/chromium/chrome/browser/signin/OAuth2TokenService$OAuth2TokenServiceObserver;->onRefreshTokenAvailable(Landroid/accounts/Account;)V

    goto :goto_0

    .line 252
    :cond_1
    return-void
.end method

.method public final notifyRefreshTokenRevoked(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 266
    sget-boolean v0, Lorg/chromium/chrome/browser/signin/OAuth2TokenService;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 267
    :cond_0
    invoke-static {p1}, Lorg/chromium/sync/signin/AccountManagerHelper;->createAccountFromName(Ljava/lang/String;)Landroid/accounts/Account;

    move-result-object v1

    .line 268
    iget-object v0, p0, Lorg/chromium/chrome/browser/signin/OAuth2TokenService;->mObservers:Lorg/chromium/base/ObserverList;

    invoke-virtual {v0}, Lorg/chromium/base/ObserverList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/chromium/chrome/browser/signin/OAuth2TokenService$OAuth2TokenServiceObserver;

    .line 269
    invoke-interface {v0, v1}, Lorg/chromium/chrome/browser/signin/OAuth2TokenService$OAuth2TokenServiceObserver;->onRefreshTokenRevoked(Landroid/accounts/Account;)V

    goto :goto_0

    .line 271
    :cond_1
    return-void
.end method

.method public final notifyRefreshTokensLoaded()V
    .locals 2

    .prologue
    .line 284
    iget-object v0, p0, Lorg/chromium/chrome/browser/signin/OAuth2TokenService;->mObservers:Lorg/chromium/base/ObserverList;

    invoke-virtual {v0}, Lorg/chromium/base/ObserverList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/chromium/chrome/browser/signin/OAuth2TokenService$OAuth2TokenServiceObserver;

    .line 285
    invoke-interface {v0}, Lorg/chromium/chrome/browser/signin/OAuth2TokenService$OAuth2TokenServiceObserver;->onRefreshTokensLoaded()V

    goto :goto_0

    .line 287
    :cond_0
    return-void
.end method

.method public final validateAccounts(Landroid/content/Context;Z)V
    .locals 4

    .prologue
    .line 227
    invoke-static {}, Lorg/chromium/base/ThreadUtils;->assertOnUiThread()V

    .line 228
    invoke-static {p1}, Lorg/chromium/sync/signin/ChromeSigninController;->get(Landroid/content/Context;)Lorg/chromium/sync/signin/ChromeSigninController;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/sync/signin/ChromeSigninController;->getSignedInAccountName()Ljava/lang/String;

    move-result-object v0

    .line 230
    iget-wide v2, p0, Lorg/chromium/chrome/browser/signin/OAuth2TokenService;->mNativeProfileOAuth2TokenService:J

    invoke-direct {p0, v2, v3, v0, p2}, Lorg/chromium/chrome/browser/signin/OAuth2TokenService;->nativeValidateAccounts(JLjava/lang/String;Z)V

    .line 232
    return-void
.end method
