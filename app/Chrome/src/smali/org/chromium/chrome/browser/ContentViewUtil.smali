.class public abstract Lorg/chromium/chrome/browser/ContentViewUtil;
.super Ljava/lang/Object;
.source "ContentViewUtil.java"


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    return-void
.end method

.method public static createNativeWebContents(Z)J
    .locals 2

    .prologue
    .line 23
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lorg/chromium/chrome/browser/ContentViewUtil;->nativeCreateNativeWebContents(ZZ)J

    move-result-wide v0

    return-wide v0
.end method

.method public static createNativeWebContents(ZZ)J
    .locals 2

    .prologue
    .line 31
    invoke-static {p0, p1}, Lorg/chromium/chrome/browser/ContentViewUtil;->nativeCreateNativeWebContents(ZZ)J

    move-result-wide v0

    return-wide v0
.end method

.method public static createNativeWebContentsWithSharedSiteInstance(Lorg/chromium/content/browser/ContentViewCore;)J
    .locals 2

    .prologue
    .line 40
    invoke-static {p0}, Lorg/chromium/chrome/browser/ContentViewUtil;->nativeCreateNativeWebContentsWithSharedSiteInstance(Lorg/chromium/content/browser/ContentViewCore;)J

    move-result-wide v0

    return-wide v0
.end method

.method public static destroyNativeWebContents(J)V
    .locals 0

    .prologue
    .line 47
    invoke-static {p0, p1}, Lorg/chromium/chrome/browser/ContentViewUtil;->nativeDestroyNativeWebContents(J)V

    .line 48
    return-void
.end method

.method private static native nativeCreateNativeWebContents(ZZ)J
.end method

.method private static native nativeCreateNativeWebContentsWithSharedSiteInstance(Lorg/chromium/content/browser/ContentViewCore;)J
.end method

.method private static native nativeDestroyNativeWebContents(J)V
.end method
