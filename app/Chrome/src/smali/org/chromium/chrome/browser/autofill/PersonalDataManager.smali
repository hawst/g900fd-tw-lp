.class public Lorg/chromium/chrome/browser/autofill/PersonalDataManager;
.super Ljava/lang/Object;
.source "PersonalDataManager.java"


# annotations
.annotation runtime Lorg/chromium/base/JNINamespace;
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z

.field private static sManager:Lorg/chromium/chrome/browser/autofill/PersonalDataManager;


# instance fields
.field private final mDataObservers:Ljava/util/List;

.field private final mPersonalDataManagerAndroid:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 23
    const-class v0, Lorg/chromium/chrome/browser/autofill/PersonalDataManager;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/chromium/chrome/browser/autofill/PersonalDataManager;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    .line 333
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 330
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lorg/chromium/chrome/browser/autofill/PersonalDataManager;->mDataObservers:Ljava/util/List;

    .line 336
    invoke-direct {p0}, Lorg/chromium/chrome/browser/autofill/PersonalDataManager;->nativeInit()J

    move-result-wide v0

    iput-wide v0, p0, Lorg/chromium/chrome/browser/autofill/PersonalDataManager;->mPersonalDataManagerAndroid:J

    .line 337
    return-void
.end method

.method public static getInstance()Lorg/chromium/chrome/browser/autofill/PersonalDataManager;
    .locals 1

    .prologue
    .line 322
    invoke-static {}, Lorg/chromium/base/ThreadUtils;->assertOnUiThread()V

    .line 323
    sget-object v0, Lorg/chromium/chrome/browser/autofill/PersonalDataManager;->sManager:Lorg/chromium/chrome/browser/autofill/PersonalDataManager;

    if-nez v0, :cond_0

    .line 324
    new-instance v0, Lorg/chromium/chrome/browser/autofill/PersonalDataManager;

    invoke-direct {v0}, Lorg/chromium/chrome/browser/autofill/PersonalDataManager;-><init>()V

    sput-object v0, Lorg/chromium/chrome/browser/autofill/PersonalDataManager;->sManager:Lorg/chromium/chrome/browser/autofill/PersonalDataManager;

    .line 326
    :cond_0
    sget-object v0, Lorg/chromium/chrome/browser/autofill/PersonalDataManager;->sManager:Lorg/chromium/chrome/browser/autofill/PersonalDataManager;

    return-object v0
.end method

.method public static isAutofillEnabled()Z
    .locals 1

    .prologue
    .line 429
    invoke-static {}, Lorg/chromium/chrome/browser/autofill/PersonalDataManager;->nativeIsAutofillEnabled()Z

    move-result v0

    return v0
.end method

.method public static isAutofillManaged()Z
    .locals 1

    .prologue
    .line 444
    invoke-static {}, Lorg/chromium/chrome/browser/autofill/PersonalDataManager;->nativeIsAutofillManaged()Z

    move-result v0

    return v0
.end method

.method private native nativeGetCreditCardByGUID(JLjava/lang/String;)Lorg/chromium/chrome/browser/autofill/PersonalDataManager$CreditCard;
.end method

.method private native nativeGetCreditCardByIndex(JI)Lorg/chromium/chrome/browser/autofill/PersonalDataManager$CreditCard;
.end method

.method private native nativeGetCreditCardCount(J)I
.end method

.method private native nativeGetProfileByGUID(JLjava/lang/String;)Lorg/chromium/chrome/browser/autofill/PersonalDataManager$AutofillProfile;
.end method

.method private native nativeGetProfileByIndex(JI)Lorg/chromium/chrome/browser/autofill/PersonalDataManager$AutofillProfile;
.end method

.method private native nativeGetProfileCount(J)I
.end method

.method private native nativeGetProfileLabels(J)[Ljava/lang/String;
.end method

.method private native nativeInit()J
.end method

.method private static native nativeIsAutofillEnabled()Z
.end method

.method private static native nativeIsAutofillManaged()Z
.end method

.method private native nativeRemoveByGUID(JLjava/lang/String;)V
.end method

.method private static native nativeSetAutofillEnabled(Z)V
.end method

.method private native nativeSetCreditCard(JLorg/chromium/chrome/browser/autofill/PersonalDataManager$CreditCard;)Ljava/lang/String;
.end method

.method private native nativeSetProfile(JLorg/chromium/chrome/browser/autofill/PersonalDataManager$AutofillProfile;)Ljava/lang/String;
.end method

.method private static native nativeToCountryCode(Ljava/lang/String;)Ljava/lang/String;
.end method

.method private personalDataChanged()V
    .locals 2

    .prologue
    .line 344
    invoke-static {}, Lorg/chromium/base/ThreadUtils;->assertOnUiThread()V

    .line 345
    iget-object v0, p0, Lorg/chromium/chrome/browser/autofill/PersonalDataManager;->mDataObservers:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/chromium/chrome/browser/autofill/PersonalDataManager$PersonalDataManagerObserver;

    .line 346
    invoke-interface {v0}, Lorg/chromium/chrome/browser/autofill/PersonalDataManager$PersonalDataManagerObserver;->onPersonalDataChanged()V

    goto :goto_0

    .line 348
    :cond_0
    return-void
.end method

.method public static setAutofillEnabled(Z)V
    .locals 0

    .prologue
    .line 437
    invoke-static {p0}, Lorg/chromium/chrome/browser/autofill/PersonalDataManager;->nativeSetAutofillEnabled(Z)V

    .line 438
    return-void
.end method


# virtual methods
.method public deleteCreditCard(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 421
    invoke-static {}, Lorg/chromium/base/ThreadUtils;->assertOnUiThread()V

    .line 422
    iget-wide v0, p0, Lorg/chromium/chrome/browser/autofill/PersonalDataManager;->mPersonalDataManagerAndroid:J

    invoke-direct {p0, v0, v1, p1}, Lorg/chromium/chrome/browser/autofill/PersonalDataManager;->nativeRemoveByGUID(JLjava/lang/String;)V

    .line 423
    return-void
.end method

.method public deleteProfile(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 391
    invoke-static {}, Lorg/chromium/base/ThreadUtils;->assertOnUiThread()V

    .line 392
    iget-wide v0, p0, Lorg/chromium/chrome/browser/autofill/PersonalDataManager;->mPersonalDataManagerAndroid:J

    invoke-direct {p0, v0, v1, p1}, Lorg/chromium/chrome/browser/autofill/PersonalDataManager;->nativeRemoveByGUID(JLjava/lang/String;)V

    .line 393
    return-void
.end method

.method public getCreditCard(Ljava/lang/String;)Lorg/chromium/chrome/browser/autofill/PersonalDataManager$CreditCard;
    .locals 2

    .prologue
    .line 411
    invoke-static {}, Lorg/chromium/base/ThreadUtils;->assertOnUiThread()V

    .line 412
    iget-wide v0, p0, Lorg/chromium/chrome/browser/autofill/PersonalDataManager;->mPersonalDataManagerAndroid:J

    invoke-direct {p0, v0, v1, p1}, Lorg/chromium/chrome/browser/autofill/PersonalDataManager;->nativeGetCreditCardByGUID(JLjava/lang/String;)Lorg/chromium/chrome/browser/autofill/PersonalDataManager$CreditCard;

    move-result-object v0

    return-object v0
.end method

.method public getCreditCards()Ljava/util/List;
    .locals 6

    .prologue
    .line 401
    invoke-static {}, Lorg/chromium/base/ThreadUtils;->assertOnUiThread()V

    .line 402
    iget-wide v0, p0, Lorg/chromium/chrome/browser/autofill/PersonalDataManager;->mPersonalDataManagerAndroid:J

    invoke-direct {p0, v0, v1}, Lorg/chromium/chrome/browser/autofill/PersonalDataManager;->nativeGetCreditCardCount(J)I

    move-result v1

    .line 403
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 404
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    .line 405
    iget-wide v4, p0, Lorg/chromium/chrome/browser/autofill/PersonalDataManager;->mPersonalDataManagerAndroid:J

    invoke-direct {p0, v4, v5, v0}, Lorg/chromium/chrome/browser/autofill/PersonalDataManager;->nativeGetCreditCardByIndex(JI)Lorg/chromium/chrome/browser/autofill/PersonalDataManager$CreditCard;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 404
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 407
    :cond_0
    return-object v2
.end method

.method public getProfile(Ljava/lang/String;)Lorg/chromium/chrome/browser/autofill/PersonalDataManager$AutofillProfile;
    .locals 2

    .prologue
    .line 386
    invoke-static {}, Lorg/chromium/base/ThreadUtils;->assertOnUiThread()V

    .line 387
    iget-wide v0, p0, Lorg/chromium/chrome/browser/autofill/PersonalDataManager;->mPersonalDataManagerAndroid:J

    invoke-direct {p0, v0, v1, p1}, Lorg/chromium/chrome/browser/autofill/PersonalDataManager;->nativeGetProfileByGUID(JLjava/lang/String;)Lorg/chromium/chrome/browser/autofill/PersonalDataManager$AutofillProfile;

    move-result-object v0

    return-object v0
.end method

.method public getProfiles()Ljava/util/List;
    .locals 6

    .prologue
    .line 370
    invoke-static {}, Lorg/chromium/base/ThreadUtils;->assertOnUiThread()V

    .line 372
    iget-wide v0, p0, Lorg/chromium/chrome/browser/autofill/PersonalDataManager;->mPersonalDataManagerAndroid:J

    invoke-direct {p0, v0, v1}, Lorg/chromium/chrome/browser/autofill/PersonalDataManager;->nativeGetProfileLabels(J)[Ljava/lang/String;

    move-result-object v1

    .line 374
    iget-wide v2, p0, Lorg/chromium/chrome/browser/autofill/PersonalDataManager;->mPersonalDataManagerAndroid:J

    invoke-direct {p0, v2, v3}, Lorg/chromium/chrome/browser/autofill/PersonalDataManager;->nativeGetProfileCount(J)I

    move-result v2

    .line 375
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 376
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    .line 377
    iget-wide v4, p0, Lorg/chromium/chrome/browser/autofill/PersonalDataManager;->mPersonalDataManagerAndroid:J

    invoke-direct {p0, v4, v5, v0}, Lorg/chromium/chrome/browser/autofill/PersonalDataManager;->nativeGetProfileByIndex(JI)Lorg/chromium/chrome/browser/autofill/PersonalDataManager$AutofillProfile;

    move-result-object v4

    .line 378
    aget-object v5, v1, v0

    invoke-virtual {v4, v5}, Lorg/chromium/chrome/browser/autofill/PersonalDataManager$AutofillProfile;->setLabel(Ljava/lang/String;)V

    .line 379
    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 376
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 382
    :cond_0
    return-object v3
.end method

.method public registerDataObserver(Lorg/chromium/chrome/browser/autofill/PersonalDataManager$PersonalDataManagerObserver;)V
    .locals 1

    .prologue
    .line 354
    invoke-static {}, Lorg/chromium/base/ThreadUtils;->assertOnUiThread()V

    .line 355
    sget-boolean v0, Lorg/chromium/chrome/browser/autofill/PersonalDataManager;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lorg/chromium/chrome/browser/autofill/PersonalDataManager;->mDataObservers:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 356
    :cond_0
    iget-object v0, p0, Lorg/chromium/chrome/browser/autofill/PersonalDataManager;->mDataObservers:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 357
    return-void
.end method

.method public setCreditCard(Lorg/chromium/chrome/browser/autofill/PersonalDataManager$CreditCard;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 416
    invoke-static {}, Lorg/chromium/base/ThreadUtils;->assertOnUiThread()V

    .line 417
    iget-wide v0, p0, Lorg/chromium/chrome/browser/autofill/PersonalDataManager;->mPersonalDataManagerAndroid:J

    invoke-direct {p0, v0, v1, p1}, Lorg/chromium/chrome/browser/autofill/PersonalDataManager;->nativeSetCreditCard(JLorg/chromium/chrome/browser/autofill/PersonalDataManager$CreditCard;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public setProfile(Lorg/chromium/chrome/browser/autofill/PersonalDataManager$AutofillProfile;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 396
    invoke-static {}, Lorg/chromium/base/ThreadUtils;->assertOnUiThread()V

    .line 397
    iget-wide v0, p0, Lorg/chromium/chrome/browser/autofill/PersonalDataManager;->mPersonalDataManagerAndroid:J

    invoke-direct {p0, v0, v1, p1}, Lorg/chromium/chrome/browser/autofill/PersonalDataManager;->nativeSetProfile(JLorg/chromium/chrome/browser/autofill/PersonalDataManager$AutofillProfile;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public unregisterDataObserver(Lorg/chromium/chrome/browser/autofill/PersonalDataManager$PersonalDataManagerObserver;)V
    .locals 1

    .prologue
    .line 363
    invoke-static {}, Lorg/chromium/base/ThreadUtils;->assertOnUiThread()V

    .line 364
    sget-boolean v0, Lorg/chromium/chrome/browser/autofill/PersonalDataManager;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lorg/chromium/chrome/browser/autofill/PersonalDataManager;->mDataObservers:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-gtz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 365
    :cond_0
    sget-boolean v0, Lorg/chromium/chrome/browser/autofill/PersonalDataManager;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lorg/chromium/chrome/browser/autofill/PersonalDataManager;->mDataObservers:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 366
    :cond_1
    iget-object v0, p0, Lorg/chromium/chrome/browser/autofill/PersonalDataManager;->mDataObservers:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 367
    return-void
.end method
