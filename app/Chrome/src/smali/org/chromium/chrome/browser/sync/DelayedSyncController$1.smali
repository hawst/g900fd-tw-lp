.class Lorg/chromium/chrome/browser/sync/DelayedSyncController$1;
.super Landroid/os/AsyncTask;
.source "DelayedSyncController.java"


# instance fields
.field final synthetic this$0:Lorg/chromium/chrome/browser/sync/DelayedSyncController;

.field final synthetic val$account:Landroid/accounts/Account;

.field final synthetic val$context:Landroid/content/Context;


# direct methods
.method constructor <init>(Lorg/chromium/chrome/browser/sync/DelayedSyncController;Landroid/content/Context;Landroid/accounts/Account;)V
    .locals 0

    .prologue
    .line 63
    iput-object p1, p0, Lorg/chromium/chrome/browser/sync/DelayedSyncController$1;->this$0:Lorg/chromium/chrome/browser/sync/DelayedSyncController;

    iput-object p2, p0, Lorg/chromium/chrome/browser/sync/DelayedSyncController$1;->val$context:Landroid/content/Context;

    iput-object p3, p0, Lorg/chromium/chrome/browser/sync/DelayedSyncController$1;->val$account:Landroid/accounts/Account;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 63
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lorg/chromium/chrome/browser/sync/DelayedSyncController$1;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 3

    .prologue
    .line 66
    iget-object v0, p0, Lorg/chromium/chrome/browser/sync/DelayedSyncController$1;->val$context:Landroid/content/Context;

    invoke-static {v0}, Lorg/chromium/sync/notifier/SyncStatusHelper;->get(Landroid/content/Context;)Lorg/chromium/sync/notifier/SyncStatusHelper;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/sync/notifier/SyncStatusHelper;->getContractAuthority()Ljava/lang/String;

    move-result-object v0

    .line 68
    iget-object v1, p0, Lorg/chromium/chrome/browser/sync/DelayedSyncController$1;->val$account:Landroid/accounts/Account;

    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    invoke-static {v1, v0, v2}, Landroid/content/ContentResolver;->requestSync(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 69
    const/4 v0, 0x0

    return-object v0
.end method
