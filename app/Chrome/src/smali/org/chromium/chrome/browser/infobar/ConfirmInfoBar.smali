.class public Lorg/chromium/chrome/browser/infobar/ConfirmInfoBar;
.super Lorg/chromium/chrome/browser/infobar/InfoBar;
.source "ConfirmInfoBar.java"


# instance fields
.field private final mConfirmListener:Lorg/chromium/chrome/browser/infobar/InfoBarListeners$Confirm;

.field private final mPrimaryButtonText:Ljava/lang/String;

.field private final mSecondaryButtonText:Ljava/lang/String;

.field private final mTertiaryButtonText:Ljava/lang/String;


# direct methods
.method public constructor <init>(JLorg/chromium/chrome/browser/infobar/InfoBarListeners$Confirm;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 28
    invoke-direct {p0, p3, p4, p5}, Lorg/chromium/chrome/browser/infobar/InfoBar;-><init>(Lorg/chromium/chrome/browser/infobar/InfoBarListeners$Dismiss;ILjava/lang/CharSequence;)V

    .line 29
    iput-object p7, p0, Lorg/chromium/chrome/browser/infobar/ConfirmInfoBar;->mPrimaryButtonText:Ljava/lang/String;

    .line 30
    iput-object p8, p0, Lorg/chromium/chrome/browser/infobar/ConfirmInfoBar;->mSecondaryButtonText:Ljava/lang/String;

    .line 31
    iput-object p6, p0, Lorg/chromium/chrome/browser/infobar/ConfirmInfoBar;->mTertiaryButtonText:Ljava/lang/String;

    .line 32
    iput-object p3, p0, Lorg/chromium/chrome/browser/infobar/ConfirmInfoBar;->mConfirmListener:Lorg/chromium/chrome/browser/infobar/InfoBarListeners$Confirm;

    .line 33
    invoke-virtual {p0, p1, p2}, Lorg/chromium/chrome/browser/infobar/ConfirmInfoBar;->setNativeInfoBar(J)V

    .line 34
    return-void
.end method


# virtual methods
.method public createContent(Lorg/chromium/chrome/browser/infobar/InfoBarLayout;)V
    .locals 3

    .prologue
    .line 38
    iget-object v0, p0, Lorg/chromium/chrome/browser/infobar/ConfirmInfoBar;->mPrimaryButtonText:Ljava/lang/String;

    iget-object v1, p0, Lorg/chromium/chrome/browser/infobar/ConfirmInfoBar;->mSecondaryButtonText:Ljava/lang/String;

    iget-object v2, p0, Lorg/chromium/chrome/browser/infobar/ConfirmInfoBar;->mTertiaryButtonText:Ljava/lang/String;

    invoke-virtual {p1, v0, v1, v2}, Lorg/chromium/chrome/browser/infobar/InfoBarLayout;->setButtons(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 39
    return-void
.end method

.method public onButtonClicked(Z)V
    .locals 4

    .prologue
    .line 43
    iget-object v0, p0, Lorg/chromium/chrome/browser/infobar/ConfirmInfoBar;->mConfirmListener:Lorg/chromium/chrome/browser/infobar/InfoBarListeners$Confirm;

    if-eqz v0, :cond_0

    .line 44
    iget-object v0, p0, Lorg/chromium/chrome/browser/infobar/ConfirmInfoBar;->mConfirmListener:Lorg/chromium/chrome/browser/infobar/InfoBarListeners$Confirm;

    invoke-interface {v0, p0, p1}, Lorg/chromium/chrome/browser/infobar/InfoBarListeners$Confirm;->onConfirmInfoBarButtonClicked(Lorg/chromium/chrome/browser/infobar/ConfirmInfoBar;Z)V

    .line 47
    :cond_0
    iget-wide v0, p0, Lorg/chromium/chrome/browser/infobar/ConfirmInfoBar;->mNativeInfoBarPtr:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    .line 48
    if-eqz p1, :cond_2

    const/4 v0, 0x1

    .line 49
    :goto_0
    iget-wide v2, p0, Lorg/chromium/chrome/browser/infobar/ConfirmInfoBar;->mNativeInfoBarPtr:J

    const-string/jumbo v1, ""

    invoke-virtual {p0, v2, v3, v0, v1}, Lorg/chromium/chrome/browser/infobar/ConfirmInfoBar;->nativeOnButtonClicked(JILjava/lang/String;)V

    .line 51
    :cond_1
    return-void

    .line 48
    :cond_2
    const/4 v0, 0x2

    goto :goto_0
.end method

.method public onCloseButtonClicked()V
    .locals 4

    .prologue
    .line 55
    iget-wide v0, p0, Lorg/chromium/chrome/browser/infobar/ConfirmInfoBar;->mNativeInfoBarPtr:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 56
    iget-wide v0, p0, Lorg/chromium/chrome/browser/infobar/ConfirmInfoBar;->mNativeInfoBarPtr:J

    invoke-virtual {p0, v0, v1}, Lorg/chromium/chrome/browser/infobar/ConfirmInfoBar;->nativeOnCloseButtonClicked(J)V

    .line 60
    :goto_0
    return-void

    .line 58
    :cond_0
    invoke-super {p0}, Lorg/chromium/chrome/browser/infobar/InfoBar;->dismissJavaOnlyInfoBar()V

    goto :goto_0
.end method
