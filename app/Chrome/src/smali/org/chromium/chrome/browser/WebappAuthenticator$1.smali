.class final Lorg/chromium/chrome/browser/WebappAuthenticator$1;
.super Ljava/lang/Object;
.source "WebappAuthenticator.java"

# interfaces
.implements Ljava/util/concurrent/Callable;


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 189
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final bridge synthetic call()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 189
    invoke-virtual {p0}, Lorg/chromium/chrome/browser/WebappAuthenticator$1;->call()Ljavax/crypto/SecretKey;

    move-result-object v0

    return-object v0
.end method

.method public final call()Ljavax/crypto/SecretKey;
    .locals 3

    .prologue
    .line 192
    const-string/jumbo v0, "HmacSHA256"

    invoke-static {v0}, Ljavax/crypto/KeyGenerator;->getInstance(Ljava/lang/String;)Ljavax/crypto/KeyGenerator;

    move-result-object v0

    .line 193
    const-string/jumbo v1, "SHA1PRNG"

    invoke-static {v1}, Ljava/security/SecureRandom;->getInstance(Ljava/lang/String;)Ljava/security/SecureRandom;

    move-result-object v1

    .line 203
    const/16 v2, 0x20

    # invokes: Lorg/chromium/chrome/browser/WebappAuthenticator;->getRandomBytes(I)[B
    invoke-static {v2}, Lorg/chromium/chrome/browser/WebappAuthenticator;->access$000(I)[B

    move-result-object v2

    .line 204
    if-nez v2, :cond_0

    .line 205
    const/4 v0, 0x0

    .line 209
    :goto_0
    return-object v0

    .line 207
    :cond_0
    invoke-virtual {v1, v2}, Ljava/security/SecureRandom;->setSeed([B)V

    .line 208
    const/16 v2, 0x100

    invoke-virtual {v0, v2, v1}, Ljavax/crypto/KeyGenerator;->init(ILjava/security/SecureRandom;)V

    .line 209
    invoke-virtual {v0}, Ljavax/crypto/KeyGenerator;->generateKey()Ljavax/crypto/SecretKey;

    move-result-object v0

    goto :goto_0
.end method
