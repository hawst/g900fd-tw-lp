.class Lorg/chromium/chrome/browser/appmenu/AppMenuDragHelper;
.super Ljava/lang/Object;
.source "AppMenuDragHelper.java"


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private final mActivity:Landroid/app/Activity;

.field private final mAppMenu:Lorg/chromium/chrome/browser/appmenu/AppMenu;

.field private final mAutoScrollFullVelocity:F

.field private mDragScrollOffset:F

.field private mDragScrollOffsetRounded:I

.field private final mDragScrolling:Landroid/animation/TimeAnimator;

.field private volatile mDragScrollingVelocity:F

.field mGestureSingleTapDetector:Landroid/view/GestureDetector;

.field private mIsSingleTapUpHappened:Z

.field private final mItemRowHeight:I

.field private volatile mLastTouchX:F

.field private volatile mLastTouchY:F

.field private final mScreenVisiblePoint:[I

.field private final mScreenVisibleRect:Landroid/graphics/Rect;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 32
    const-class v0, Lorg/chromium/chrome/browser/appmenu/AppMenuDragHelper;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/chromium/chrome/browser/appmenu/AppMenuDragHelper;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>(Landroid/app/Activity;Lorg/chromium/chrome/browser/appmenu/AppMenu;I)V
    .locals 2

    .prologue
    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    new-instance v0, Landroid/animation/TimeAnimator;

    invoke-direct {v0}, Landroid/animation/TimeAnimator;-><init>()V

    iput-object v0, p0, Lorg/chromium/chrome/browser/appmenu/AppMenuDragHelper;->mDragScrolling:Landroid/animation/TimeAnimator;

    .line 58
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lorg/chromium/chrome/browser/appmenu/AppMenuDragHelper;->mScreenVisibleRect:Landroid/graphics/Rect;

    .line 59
    const/4 v0, 0x2

    new-array v0, v0, [I

    iput-object v0, p0, Lorg/chromium/chrome/browser/appmenu/AppMenuDragHelper;->mScreenVisiblePoint:[I

    .line 62
    iput-object p1, p0, Lorg/chromium/chrome/browser/appmenu/AppMenuDragHelper;->mActivity:Landroid/app/Activity;

    .line 63
    iput-object p2, p0, Lorg/chromium/chrome/browser/appmenu/AppMenuDragHelper;->mAppMenu:Lorg/chromium/chrome/browser/appmenu/AppMenu;

    .line 64
    iput p3, p0, Lorg/chromium/chrome/browser/appmenu/AppMenuDragHelper;->mItemRowHeight:I

    .line 65
    iget-object v0, p0, Lorg/chromium/chrome/browser/appmenu/AppMenuDragHelper;->mActivity:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 66
    sget v1, Lorg/chromium/chrome/R$dimen;->auto_scroll_full_velocity:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    int-to-float v0, v0

    iput v0, p0, Lorg/chromium/chrome/browser/appmenu/AppMenuDragHelper;->mAutoScrollFullVelocity:F

    .line 70
    iget-object v0, p0, Lorg/chromium/chrome/browser/appmenu/AppMenuDragHelper;->mDragScrolling:Landroid/animation/TimeAnimator;

    new-instance v1, Lorg/chromium/chrome/browser/appmenu/AppMenuDragHelper$1;

    invoke-direct {v1, p0}, Lorg/chromium/chrome/browser/appmenu/AppMenuDragHelper$1;-><init>(Lorg/chromium/chrome/browser/appmenu/AppMenuDragHelper;)V

    invoke-virtual {v0, v1}, Landroid/animation/TimeAnimator;->setTimeListener(Landroid/animation/TimeAnimator$TimeListener;)V

    .line 91
    new-instance v0, Landroid/view/GestureDetector;

    new-instance v1, Lorg/chromium/chrome/browser/appmenu/AppMenuDragHelper$2;

    invoke-direct {v1, p0}, Lorg/chromium/chrome/browser/appmenu/AppMenuDragHelper$2;-><init>(Lorg/chromium/chrome/browser/appmenu/AppMenuDragHelper;)V

    invoke-direct {v0, p1, v1}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v0, p0, Lorg/chromium/chrome/browser/appmenu/AppMenuDragHelper;->mGestureSingleTapDetector:Landroid/view/GestureDetector;

    .line 98
    return-void
.end method

.method static synthetic access$000(Lorg/chromium/chrome/browser/appmenu/AppMenuDragHelper;)Lorg/chromium/chrome/browser/appmenu/AppMenu;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lorg/chromium/chrome/browser/appmenu/AppMenuDragHelper;->mAppMenu:Lorg/chromium/chrome/browser/appmenu/AppMenu;

    return-object v0
.end method

.method static synthetic access$100(Lorg/chromium/chrome/browser/appmenu/AppMenuDragHelper;)F
    .locals 1

    .prologue
    .line 33
    iget v0, p0, Lorg/chromium/chrome/browser/appmenu/AppMenuDragHelper;->mDragScrollOffset:F

    return v0
.end method

.method static synthetic access$116(Lorg/chromium/chrome/browser/appmenu/AppMenuDragHelper;F)F
    .locals 1

    .prologue
    .line 33
    iget v0, p0, Lorg/chromium/chrome/browser/appmenu/AppMenuDragHelper;->mDragScrollOffset:F

    add-float/2addr v0, p1

    iput v0, p0, Lorg/chromium/chrome/browser/appmenu/AppMenuDragHelper;->mDragScrollOffset:F

    return v0
.end method

.method static synthetic access$200(Lorg/chromium/chrome/browser/appmenu/AppMenuDragHelper;)F
    .locals 1

    .prologue
    .line 33
    iget v0, p0, Lorg/chromium/chrome/browser/appmenu/AppMenuDragHelper;->mDragScrollingVelocity:F

    return v0
.end method

.method static synthetic access$300(Lorg/chromium/chrome/browser/appmenu/AppMenuDragHelper;)I
    .locals 1

    .prologue
    .line 33
    iget v0, p0, Lorg/chromium/chrome/browser/appmenu/AppMenuDragHelper;->mDragScrollOffsetRounded:I

    return v0
.end method

.method static synthetic access$312(Lorg/chromium/chrome/browser/appmenu/AppMenuDragHelper;I)I
    .locals 1

    .prologue
    .line 33
    iget v0, p0, Lorg/chromium/chrome/browser/appmenu/AppMenuDragHelper;->mDragScrollOffsetRounded:I

    add-int/2addr v0, p1

    iput v0, p0, Lorg/chromium/chrome/browser/appmenu/AppMenuDragHelper;->mDragScrollOffsetRounded:I

    return v0
.end method

.method static synthetic access$400(Lorg/chromium/chrome/browser/appmenu/AppMenuDragHelper;)F
    .locals 1

    .prologue
    .line 33
    iget v0, p0, Lorg/chromium/chrome/browser/appmenu/AppMenuDragHelper;->mLastTouchX:F

    return v0
.end method

.method static synthetic access$500(Lorg/chromium/chrome/browser/appmenu/AppMenuDragHelper;)F
    .locals 1

    .prologue
    .line 33
    iget v0, p0, Lorg/chromium/chrome/browser/appmenu/AppMenuDragHelper;->mLastTouchY:F

    return v0
.end method

.method static synthetic access$600(Lorg/chromium/chrome/browser/appmenu/AppMenuDragHelper;III)Z
    .locals 1

    .prologue
    .line 33
    invoke-direct {p0, p1, p2, p3}, Lorg/chromium/chrome/browser/appmenu/AppMenuDragHelper;->menuItemAction(III)Z

    move-result v0

    return v0
.end method

.method static synthetic access$702(Lorg/chromium/chrome/browser/appmenu/AppMenuDragHelper;Z)Z
    .locals 0

    .prologue
    .line 33
    iput-boolean p1, p0, Lorg/chromium/chrome/browser/appmenu/AppMenuDragHelper;->mIsSingleTapUpHappened:Z

    return p1
.end method

.method private getScreenVisibleRect(Landroid/view/View;)Landroid/graphics/Rect;
    .locals 4

    .prologue
    .line 268
    iget-object v0, p0, Lorg/chromium/chrome/browser/appmenu/AppMenuDragHelper;->mScreenVisibleRect:Landroid/graphics/Rect;

    invoke-virtual {p1, v0}, Landroid/view/View;->getLocalVisibleRect(Landroid/graphics/Rect;)Z

    .line 269
    iget-object v0, p0, Lorg/chromium/chrome/browser/appmenu/AppMenuDragHelper;->mScreenVisiblePoint:[I

    invoke-virtual {p1, v0}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 270
    iget-object v0, p0, Lorg/chromium/chrome/browser/appmenu/AppMenuDragHelper;->mScreenVisibleRect:Landroid/graphics/Rect;

    iget-object v1, p0, Lorg/chromium/chrome/browser/appmenu/AppMenuDragHelper;->mScreenVisiblePoint:[I

    const/4 v2, 0x0

    aget v1, v1, v2

    iget-object v2, p0, Lorg/chromium/chrome/browser/appmenu/AppMenuDragHelper;->mScreenVisiblePoint:[I

    const/4 v3, 0x1

    aget v2, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Rect;->offset(II)V

    .line 271
    iget-object v0, p0, Lorg/chromium/chrome/browser/appmenu/AppMenuDragHelper;->mScreenVisibleRect:Landroid/graphics/Rect;

    return-object v0
.end method

.method private menuItemAction(III)Z
    .locals 9

    .prologue
    const/4 v5, 0x1

    const/4 v2, 0x0

    .line 220
    iget-object v0, p0, Lorg/chromium/chrome/browser/appmenu/AppMenuDragHelper;->mAppMenu:Lorg/chromium/chrome/browser/appmenu/AppMenu;

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/appmenu/AppMenu;->getPopup()Landroid/widget/ListPopupWindow;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ListPopupWindow;->getListView()Landroid/widget/ListView;

    move-result-object v6

    .line 222
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    move v1, v2

    .line 223
    :goto_0
    invoke-virtual {v6}, Landroid/widget/ListView;->getChildCount()I

    move-result v0

    if-ge v1, v0, :cond_4

    .line 225
    invoke-virtual {v6, v1}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    instance-of v0, v0, Landroid/widget/LinearLayout;

    if-eqz v0, :cond_1

    .line 226
    invoke-virtual {v6, v1}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    move v3, v2

    move v4, v2

    .line 227
    :goto_1
    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v8

    if-ge v3, v8, :cond_2

    .line 228
    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 229
    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v8

    instance-of v8, v8, Landroid/widget/ImageButton;

    if-eqz v8, :cond_0

    move v4, v5

    .line 227
    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_1
    move v4, v2

    .line 232
    :cond_2
    if-nez v4, :cond_3

    invoke-virtual {v6, v1}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 223
    :cond_3
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_4
    move v1, v2

    move v3, v2

    .line 236
    :goto_2
    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_7

    .line 237
    invoke-virtual {v7, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 239
    invoke-virtual {v0}, Landroid/view/View;->isEnabled()Z

    move-result v4

    if-eqz v4, :cond_5

    invoke-virtual {v0}, Landroid/view/View;->isShown()Z

    move-result v4

    if-eqz v4, :cond_5

    invoke-direct {p0, v0}, Lorg/chromium/chrome/browser/appmenu/AppMenuDragHelper;->getScreenVisibleRect(Landroid/view/View;)Landroid/graphics/Rect;

    move-result-object v4

    invoke-virtual {v4, p1, p2}, Landroid/graphics/Rect;->contains(II)Z

    move-result v4

    if-eqz v4, :cond_5

    move v4, v5

    .line 242
    :goto_3
    packed-switch p3, :pswitch_data_0

    .line 257
    sget-boolean v0, Lorg/chromium/chrome/browser/appmenu/AppMenuDragHelper;->$assertionsDisabled:Z

    if-nez v0, :cond_6

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_5
    move v4, v2

    .line 239
    goto :goto_3

    .line 244
    :pswitch_0
    invoke-virtual {v0, v4}, Landroid/view/View;->setPressed(Z)V

    .line 236
    :cond_6
    :goto_4
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 247
    :pswitch_1
    if-eqz v4, :cond_6

    .line 248
    invoke-static {v2, v5}, Lorg/chromium/chrome/browser/UmaBridge;->usingMenu(ZZ)V

    .line 249
    invoke-virtual {v0}, Landroid/view/View;->performClick()Z

    move v3, v5

    .line 250
    goto :goto_4

    .line 254
    :pswitch_2
    invoke-virtual {v0, v2}, Landroid/view/View;->setPressed(Z)V

    goto :goto_4

    .line 261
    :cond_7
    return v3

    .line 242
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method finishDragging()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 123
    const/4 v0, 0x2

    invoke-direct {p0, v1, v1, v0}, Lorg/chromium/chrome/browser/appmenu/AppMenuDragHelper;->menuItemAction(III)Z

    .line 124
    iget-object v0, p0, Lorg/chromium/chrome/browser/appmenu/AppMenuDragHelper;->mDragScrolling:Landroid/animation/TimeAnimator;

    invoke-virtual {v0}, Landroid/animation/TimeAnimator;->cancel()V

    .line 125
    return-void
.end method

.method handleDragging(Landroid/view/MotionEvent;)Z
    .locals 10

    .prologue
    const/4 v3, 0x2

    const/high16 v9, 0x3f800000    # 1.0f

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 137
    iget-object v0, p0, Lorg/chromium/chrome/browser/appmenu/AppMenuDragHelper;->mAppMenu:Lorg/chromium/chrome/browser/appmenu/AppMenu;

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/appmenu/AppMenu;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/chromium/chrome/browser/appmenu/AppMenuDragHelper;->mDragScrolling:Landroid/animation/TimeAnimator;

    invoke-virtual {v0}, Landroid/animation/TimeAnimator;->isRunning()Z

    move-result v0

    if-nez v0, :cond_2

    :cond_0
    move v2, v1

    .line 209
    :cond_1
    :goto_0
    return v2

    .line 143
    :cond_2
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawX()F

    move-result v0

    .line 144
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result v4

    .line 145
    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v5

    .line 146
    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    move-result v6

    .line 147
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v7

    .line 148
    iget-object v8, p0, Lorg/chromium/chrome/browser/appmenu/AppMenuDragHelper;->mAppMenu:Lorg/chromium/chrome/browser/appmenu/AppMenu;

    invoke-virtual {v8}, Lorg/chromium/chrome/browser/appmenu/AppMenu;->getPopup()Landroid/widget/ListPopupWindow;

    move-result-object v8

    invoke-virtual {v8}, Landroid/widget/ListPopupWindow;->getListView()Landroid/widget/ListView;

    move-result-object v8

    .line 150
    iput v0, p0, Lorg/chromium/chrome/browser/appmenu/AppMenuDragHelper;->mLastTouchX:F

    .line 151
    iput v4, p0, Lorg/chromium/chrome/browser/appmenu/AppMenuDragHelper;->mLastTouchY:F

    .line 153
    const/4 v0, 0x3

    if-ne v7, v0, :cond_3

    .line 154
    iget-object v0, p0, Lorg/chromium/chrome/browser/appmenu/AppMenuDragHelper;->mAppMenu:Lorg/chromium/chrome/browser/appmenu/AppMenu;

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/appmenu/AppMenu;->dismiss()V

    goto :goto_0

    .line 158
    :cond_3
    iget-boolean v0, p0, Lorg/chromium/chrome/browser/appmenu/AppMenuDragHelper;->mIsSingleTapUpHappened:Z

    if-nez v0, :cond_4

    .line 159
    iget-object v0, p0, Lorg/chromium/chrome/browser/appmenu/AppMenuDragHelper;->mGestureSingleTapDetector:Landroid/view/GestureDetector;

    invoke-virtual {v0, p1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 160
    iget-boolean v0, p0, Lorg/chromium/chrome/browser/appmenu/AppMenuDragHelper;->mIsSingleTapUpHappened:Z

    if-eqz v0, :cond_4

    .line 161
    invoke-static {v1, v1}, Lorg/chromium/chrome/browser/UmaBridge;->usingMenu(ZZ)V

    .line 162
    invoke-virtual {p0}, Lorg/chromium/chrome/browser/appmenu/AppMenuDragHelper;->finishDragging()V

    .line 167
    :cond_4
    iget-object v0, p0, Lorg/chromium/chrome/browser/appmenu/AppMenuDragHelper;->mDragScrolling:Landroid/animation/TimeAnimator;

    invoke-virtual {v0}, Landroid/animation/TimeAnimator;->isRunning()Z

    move-result v0

    if-nez v0, :cond_5

    move v2, v1

    goto :goto_0

    .line 171
    :cond_5
    packed-switch v7, :pswitch_data_0

    move v0, v3

    .line 178
    :goto_1
    invoke-direct {p0, v5, v6, v0}, Lorg/chromium/chrome/browser/appmenu/AppMenuDragHelper;->menuItemAction(III)Z

    move-result v0

    .line 184
    if-ne v7, v2, :cond_6

    if-nez v0, :cond_6

    .line 185
    invoke-static {v1, v2}, Lorg/chromium/chrome/browser/UmaBridge;->usingMenu(ZZ)V

    .line 186
    iget-object v0, p0, Lorg/chromium/chrome/browser/appmenu/AppMenuDragHelper;->mAppMenu:Lorg/chromium/chrome/browser/appmenu/AppMenu;

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/appmenu/AppMenu;->dismiss()V

    goto :goto_0

    :pswitch_0
    move v0, v1

    .line 175
    goto :goto_1

    :pswitch_1
    move v0, v2

    .line 177
    goto :goto_1

    .line 187
    :cond_6
    if-ne v7, v3, :cond_1

    .line 189
    invoke-virtual {v8}, Landroid/widget/ListView;->getHeight()I

    move-result v0

    if-lez v0, :cond_1

    .line 190
    const/high16 v0, 0x3e800000    # 0.25f

    iget v1, p0, Lorg/chromium/chrome/browser/appmenu/AppMenuDragHelper;->mItemRowHeight:I

    int-to-float v1, v1

    const v3, 0x3f99999a    # 1.2f

    mul-float/2addr v1, v3

    invoke-virtual {v8}, Landroid/widget/ListView;->getHeight()I

    move-result v3

    int-to-float v3, v3

    div-float/2addr v1, v3

    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v0

    .line 192
    invoke-direct {p0, v8}, Lorg/chromium/chrome/browser/appmenu/AppMenuDragHelper;->getScreenVisibleRect(Landroid/view/View;)Landroid/graphics/Rect;

    move-result-object v1

    iget v1, v1, Landroid/graphics/Rect;->top:I

    int-to-float v1, v1

    sub-float v1, v4, v1

    invoke-virtual {v8}, Landroid/widget/ListView;->getHeight()I

    move-result v3

    int-to-float v3, v3

    div-float/2addr v1, v3

    .line 194
    cmpg-float v3, v1, v0

    if-gez v3, :cond_7

    .line 196
    div-float v0, v1, v0

    sub-float/2addr v0, v9

    iget v1, p0, Lorg/chromium/chrome/browser/appmenu/AppMenuDragHelper;->mAutoScrollFullVelocity:F

    mul-float/2addr v0, v1

    iput v0, p0, Lorg/chromium/chrome/browser/appmenu/AppMenuDragHelper;->mDragScrollingVelocity:F

    goto/16 :goto_0

    .line 198
    :cond_7
    sub-float v3, v9, v0

    cmpl-float v3, v1, v3

    if-lez v3, :cond_8

    .line 200
    sub-float/2addr v1, v9

    div-float v0, v1, v0

    add-float/2addr v0, v9

    iget v1, p0, Lorg/chromium/chrome/browser/appmenu/AppMenuDragHelper;->mAutoScrollFullVelocity:F

    mul-float/2addr v0, v1

    iput v0, p0, Lorg/chromium/chrome/browser/appmenu/AppMenuDragHelper;->mDragScrollingVelocity:F

    goto/16 :goto_0

    .line 204
    :cond_8
    const/4 v0, 0x0

    iput v0, p0, Lorg/chromium/chrome/browser/appmenu/AppMenuDragHelper;->mDragScrollingVelocity:F

    goto/16 :goto_0

    .line 171
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method onShow(Z)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/high16 v1, 0x7fc00000    # NaNf

    const/4 v0, 0x0

    .line 108
    iput v1, p0, Lorg/chromium/chrome/browser/appmenu/AppMenuDragHelper;->mLastTouchX:F

    .line 109
    iput v1, p0, Lorg/chromium/chrome/browser/appmenu/AppMenuDragHelper;->mLastTouchY:F

    .line 110
    iput v0, p0, Lorg/chromium/chrome/browser/appmenu/AppMenuDragHelper;->mDragScrollOffset:F

    .line 111
    iput v2, p0, Lorg/chromium/chrome/browser/appmenu/AppMenuDragHelper;->mDragScrollOffsetRounded:I

    .line 112
    iput v0, p0, Lorg/chromium/chrome/browser/appmenu/AppMenuDragHelper;->mDragScrollingVelocity:F

    .line 113
    iput-boolean v2, p0, Lorg/chromium/chrome/browser/appmenu/AppMenuDragHelper;->mIsSingleTapUpHappened:Z

    .line 115
    if-eqz p1, :cond_0

    iget-object v0, p0, Lorg/chromium/chrome/browser/appmenu/AppMenuDragHelper;->mDragScrolling:Landroid/animation/TimeAnimator;

    invoke-virtual {v0}, Landroid/animation/TimeAnimator;->start()V

    .line 116
    :cond_0
    return-void
.end method
