.class public Lorg/chromium/chrome/browser/Tab$TabChromeContextMenuItemDelegate;
.super Lorg/chromium/chrome/browser/contextmenu/EmptyChromeContextMenuItemDelegate;
.source "Tab.java"


# instance fields
.field private final mClipboard:Lorg/chromium/ui/base/Clipboard;

.field final synthetic this$0:Lorg/chromium/chrome/browser/Tab;


# direct methods
.method public constructor <init>(Lorg/chromium/chrome/browser/Tab;)V
    .locals 2

    .prologue
    .line 158
    iput-object p1, p0, Lorg/chromium/chrome/browser/Tab$TabChromeContextMenuItemDelegate;->this$0:Lorg/chromium/chrome/browser/Tab;

    invoke-direct {p0}, Lorg/chromium/chrome/browser/contextmenu/EmptyChromeContextMenuItemDelegate;-><init>()V

    .line 159
    new-instance v0, Lorg/chromium/ui/base/Clipboard;

    invoke-virtual {p1}, Lorg/chromium/chrome/browser/Tab;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/chromium/ui/base/Clipboard;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lorg/chromium/chrome/browser/Tab$TabChromeContextMenuItemDelegate;->mClipboard:Lorg/chromium/ui/base/Clipboard;

    .line 160
    return-void
.end method


# virtual methods
.method public getPageUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 179
    iget-object v0, p0, Lorg/chromium/chrome/browser/Tab$TabChromeContextMenuItemDelegate;->this$0:Lorg/chromium/chrome/browser/Tab;

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/Tab;->getUrl()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public isIncognito()Z
    .locals 1

    .prologue
    .line 164
    iget-object v0, p0, Lorg/chromium/chrome/browser/Tab$TabChromeContextMenuItemDelegate;->this$0:Lorg/chromium/chrome/browser/Tab;

    # getter for: Lorg/chromium/chrome/browser/Tab;->mIncognito:Z
    invoke-static {v0}, Lorg/chromium/chrome/browser/Tab;->access$000(Lorg/chromium/chrome/browser/Tab;)Z

    move-result v0

    return v0
.end method

.method public onSaveImageToClipboard(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 174
    iget-object v0, p0, Lorg/chromium/chrome/browser/Tab$TabChromeContextMenuItemDelegate;->mClipboard:Lorg/chromium/ui/base/Clipboard;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "<img src=\""

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "\">"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p1, p1}, Lorg/chromium/ui/base/Clipboard;->setHTMLText(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 175
    return-void
.end method

.method public onSaveToClipboard(Ljava/lang/String;Z)V
    .locals 1

    .prologue
    .line 169
    iget-object v0, p0, Lorg/chromium/chrome/browser/Tab$TabChromeContextMenuItemDelegate;->mClipboard:Lorg/chromium/ui/base/Clipboard;

    invoke-virtual {v0, p1, p1}, Lorg/chromium/ui/base/Clipboard;->setText(Ljava/lang/String;Ljava/lang/String;)V

    .line 170
    return-void
.end method
