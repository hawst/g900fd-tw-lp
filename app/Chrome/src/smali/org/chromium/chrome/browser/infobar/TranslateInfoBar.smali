.class public Lorg/chromium/chrome/browser/infobar/TranslateInfoBar;
.super Lorg/chromium/chrome/browser/infobar/InfoBar;
.source "TranslateInfoBar.java"

# interfaces
.implements Lorg/chromium/chrome/browser/infobar/SubPanelListener;


# static fields
.field static final synthetic $assertionsDisabled:Z

.field public static final AFTER_TRANSLATE_INFOBAR:I = 0x2

.field public static final ALWAYS_PANEL:I = 0x3

.field public static final BEFORE_TRANSLATE_INFOBAR:I = 0x0

.field public static final LANGUAGE_PANEL:I = 0x1

.field public static final MAX_INFOBAR_INDEX:I = 0x4

.field public static final MAX_PANEL_INDEX:I = 0x4

.field public static final NEVER_PANEL:I = 0x2

.field public static final NO_PANEL:I = 0x0

.field public static final TRANSLATE_ERROR_INFOBAR:I = 0x3

.field public static final TRANSLATING_INFOBAR:I = 0x1


# instance fields
.field private mInfoBarType:I

.field private final mOptions:Lorg/chromium/chrome/browser/infobar/TranslateOptions;

.field private mOptionsPanelViewType:I

.field private final mShouldShowNeverBar:Z

.field private mSubPanel:Lorg/chromium/chrome/browser/infobar/TranslateSubPanel;

.field private final mTranslateDelegate:Lorg/chromium/chrome/browser/infobar/TranslateInfoBarDelegate;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22
    const-class v0, Lorg/chromium/chrome/browser/infobar/TranslateInfoBar;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/chromium/chrome/browser/infobar/TranslateInfoBar;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(JLorg/chromium/chrome/browser/infobar/TranslateInfoBarDelegate;IIIZZZ[Ljava/lang/String;)V
    .locals 7

    .prologue
    .line 48
    const/4 v0, 0x0

    sget v1, Lorg/chromium/chrome/R$drawable;->infobar_translate:I

    const/4 v2, 0x0

    invoke-direct {p0, v0, v1, v2}, Lorg/chromium/chrome/browser/infobar/InfoBar;-><init>(Lorg/chromium/chrome/browser/infobar/InfoBarListeners$Dismiss;ILjava/lang/CharSequence;)V

    .line 49
    iput-object p3, p0, Lorg/chromium/chrome/browser/infobar/TranslateInfoBar;->mTranslateDelegate:Lorg/chromium/chrome/browser/infobar/TranslateInfoBarDelegate;

    .line 50
    new-instance v0, Lorg/chromium/chrome/browser/infobar/TranslateOptions;

    move v1, p5

    move v2, p6

    move-object/from16 v3, p10

    move v4, p7

    move/from16 v5, p9

    invoke-direct/range {v0 .. v5}, Lorg/chromium/chrome/browser/infobar/TranslateOptions;-><init>(II[Ljava/lang/String;ZZ)V

    iput-object v0, p0, Lorg/chromium/chrome/browser/infobar/TranslateInfoBar;->mOptions:Lorg/chromium/chrome/browser/infobar/TranslateOptions;

    .line 52
    iput p4, p0, Lorg/chromium/chrome/browser/infobar/TranslateInfoBar;->mInfoBarType:I

    .line 53
    iput-boolean p8, p0, Lorg/chromium/chrome/browser/infobar/TranslateInfoBar;->mShouldShowNeverBar:Z

    .line 54
    const/4 v0, 0x0

    iput v0, p0, Lorg/chromium/chrome/browser/infobar/TranslateInfoBar;->mOptionsPanelViewType:I

    .line 55
    invoke-virtual {p0, p1, p2}, Lorg/chromium/chrome/browser/infobar/TranslateInfoBar;->setNativeInfoBar(J)V

    .line 56
    return-void
.end method

.method static synthetic access$000(Lorg/chromium/chrome/browser/infobar/TranslateInfoBar;I)V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0, p1}, Lorg/chromium/chrome/browser/infobar/TranslateInfoBar;->swapPanel(I)V

    return-void
.end method

.method private actionFor(Z)I
    .locals 3

    .prologue
    const/4 v0, 0x3

    .line 91
    const/4 v1, 0x0

    .line 92
    invoke-virtual {p0}, Lorg/chromium/chrome/browser/infobar/TranslateInfoBar;->getInfoBarType()I

    move-result v2

    .line 93
    packed-switch v2, :pswitch_data_0

    :cond_0
    :pswitch_0
    move v0, v1

    .line 106
    :cond_1
    :goto_0
    :pswitch_1
    return v0

    .line 95
    :pswitch_2
    if-nez p1, :cond_1

    const/4 v0, 0x2

    goto :goto_0

    .line 99
    :pswitch_3
    if-nez p1, :cond_0

    .line 100
    const/4 v0, 0x4

    goto :goto_0

    .line 93
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_1
    .end packed-switch
.end method

.method private formatAfterTranslateInfoBarMessage(Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/CharSequence;
    .locals 6

    .prologue
    .line 318
    new-instance v0, Landroid/text/SpannableStringBuilder;

    invoke-direct {v0}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 319
    invoke-virtual {v0, p1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    move-result-object v1

    const-string/jumbo v2, " "

    invoke-virtual {v1, v2}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 320
    new-instance v1, Landroid/text/SpannableString;

    invoke-direct {v1, p2}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 321
    new-instance v2, Lorg/chromium/chrome/browser/infobar/TranslateInfoBar$3;

    invoke-direct {v2, p0, p3}, Lorg/chromium/chrome/browser/infobar/TranslateInfoBar$3;-><init>(Lorg/chromium/chrome/browser/infobar/TranslateInfoBar;I)V

    const/4 v3, 0x0

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v4

    const/16 v5, 0x21

    invoke-virtual {v1, v2, v3, v4, v5}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 327
    invoke-virtual {v0, v1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 328
    return-object v0
.end method

.method private formatBeforeInfoBarMessage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/CharSequence;
    .locals 6

    .prologue
    const/16 v5, 0x21

    const/4 v4, 0x0

    .line 294
    new-instance v0, Landroid/text/SpannableString;

    invoke-direct {v0, p2}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 295
    new-instance v1, Lorg/chromium/chrome/browser/infobar/TranslateInfoBar$1;

    invoke-direct {v1, p0, p4}, Lorg/chromium/chrome/browser/infobar/TranslateInfoBar$1;-><init>(Lorg/chromium/chrome/browser/infobar/TranslateInfoBar;I)V

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v2

    invoke-virtual {v0, v1, v4, v2, v5}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 302
    new-instance v1, Landroid/text/SpannableString;

    invoke-direct {v1, p3}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 303
    new-instance v2, Lorg/chromium/chrome/browser/infobar/TranslateInfoBar$2;

    invoke-direct {v2, p0, p4}, Lorg/chromium/chrome/browser/infobar/TranslateInfoBar$2;-><init>(Lorg/chromium/chrome/browser/infobar/TranslateInfoBar;I)V

    invoke-virtual {p3}, Ljava/lang/String;->length()I

    move-result v3

    invoke-virtual {v1, v2, v4, v3, v5}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 310
    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/CharSequence;

    aput-object v0, v2, v4

    const/4 v0, 0x1

    aput-object v1, v2, v0

    invoke-static {p1, v2}, Landroid/text/TextUtils;->expandTemplate(Ljava/lang/CharSequence;[Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method private getMessageText(Landroid/content/Context;)Ljava/lang/CharSequence;
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 114
    invoke-virtual {p0}, Lorg/chromium/chrome/browser/infobar/TranslateInfoBar;->getInfoBarType()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 134
    sget v0, Lorg/chromium/chrome/R$string;->translate_infobar_error:I

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    :cond_0
    :goto_0
    return-object v0

    .line 116
    :pswitch_0
    sget v0, Lorg/chromium/chrome/R$string;->translate_infobar_text:I

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 117
    iget-object v1, p0, Lorg/chromium/chrome/browser/infobar/TranslateInfoBar;->mOptions:Lorg/chromium/chrome/browser/infobar/TranslateOptions;

    invoke-virtual {v1}, Lorg/chromium/chrome/browser/infobar/TranslateOptions;->sourceLanguage()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lorg/chromium/chrome/browser/infobar/TranslateInfoBar;->mOptions:Lorg/chromium/chrome/browser/infobar/TranslateOptions;

    invoke-virtual {v2}, Lorg/chromium/chrome/browser/infobar/TranslateOptions;->targetLanguage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v0, v1, v2, v3}, Lorg/chromium/chrome/browser/infobar/TranslateInfoBar;->formatBeforeInfoBarMessage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_0

    .line 120
    :pswitch_1
    sget v0, Lorg/chromium/chrome/R$string;->translate_infobar_translation_done:I

    new-array v1, v3, [Ljava/lang/Object;

    iget-object v2, p0, Lorg/chromium/chrome/browser/infobar/TranslateInfoBar;->mOptions:Lorg/chromium/chrome/browser/infobar/TranslateOptions;

    invoke-virtual {v2}, Lorg/chromium/chrome/browser/infobar/TranslateOptions;->targetLanguage()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 122
    invoke-direct {p0}, Lorg/chromium/chrome/browser/infobar/TranslateInfoBar;->needsAlwaysPanel()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 123
    sget v1, Lorg/chromium/chrome/R$string;->translate_infobar_translation_more_options:I

    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 125
    const/4 v2, 0x3

    invoke-direct {p0, v0, v1, v2}, Lorg/chromium/chrome/browser/infobar/TranslateInfoBar;->formatAfterTranslateInfoBarMessage(Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_0

    .line 131
    :pswitch_2
    sget v0, Lorg/chromium/chrome/R$string;->translate_infobar_translating:I

    new-array v1, v3, [Ljava/lang/Object;

    iget-object v2, p0, Lorg/chromium/chrome/browser/infobar/TranslateInfoBar;->mOptions:Lorg/chromium/chrome/browser/infobar/TranslateOptions;

    invoke-virtual {v2}, Lorg/chromium/chrome/browser/infobar/TranslateOptions;->targetLanguage()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 114
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method private getPrimaryButtonText(Landroid/content/Context;)Ljava/lang/String;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 139
    invoke-virtual {p0}, Lorg/chromium/chrome/browser/infobar/TranslateInfoBar;->getInfoBarType()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 150
    :cond_0
    :goto_0
    :pswitch_0
    return-object v0

    .line 141
    :pswitch_1
    sget v0, Lorg/chromium/chrome/R$string;->translate_button:I

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 143
    :pswitch_2
    invoke-direct {p0}, Lorg/chromium/chrome/browser/infobar/TranslateInfoBar;->needsAlwaysPanel()Z

    move-result v1

    if-nez v1, :cond_0

    .line 144
    sget v0, Lorg/chromium/chrome/R$string;->translate_button_done:I

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 148
    :pswitch_3
    sget v0, Lorg/chromium/chrome/R$string;->translate_retry:I

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 139
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private getSecondaryButtonText(Landroid/content/Context;)Ljava/lang/String;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 155
    invoke-virtual {p0}, Lorg/chromium/chrome/browser/infobar/TranslateInfoBar;->getInfoBarType()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 164
    :cond_0
    :goto_0
    :pswitch_0
    return-object v0

    .line 157
    :pswitch_1
    sget v0, Lorg/chromium/chrome/R$string;->translate_nope:I

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 159
    :pswitch_2
    invoke-direct {p0}, Lorg/chromium/chrome/browser/infobar/TranslateInfoBar;->needsAlwaysPanel()Z

    move-result v1

    if-nez v1, :cond_0

    .line 160
    sget v0, Lorg/chromium/chrome/R$string;->translate_show_original:I

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 155
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method private needsAlwaysPanel()Z
    .locals 2

    .prologue
    .line 248
    invoke-virtual {p0}, Lorg/chromium/chrome/browser/infobar/TranslateInfoBar;->getInfoBarType()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lorg/chromium/chrome/browser/infobar/TranslateInfoBar;->mOptions:Lorg/chromium/chrome/browser/infobar/TranslateOptions;

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/infobar/TranslateOptions;->alwaysTranslateLanguageState()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lorg/chromium/chrome/browser/infobar/TranslateInfoBar;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lorg/chromium/ui/base/DeviceFormFactor;->isTablet(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private needsNeverPanel()Z
    .locals 1

    .prologue
    .line 243
    invoke-virtual {p0}, Lorg/chromium/chrome/browser/infobar/TranslateInfoBar;->getInfoBarType()I

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lorg/chromium/chrome/browser/infobar/TranslateInfoBar;->mShouldShowNeverBar:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private onTranslateInfoBarButtonClicked(I)V
    .locals 4

    .prologue
    .line 208
    invoke-virtual {p0}, Lorg/chromium/chrome/browser/infobar/TranslateInfoBar;->onOptionsChanged()V

    .line 212
    iget-wide v0, p0, Lorg/chromium/chrome/browser/infobar/TranslateInfoBar;->mNativeInfoBarPtr:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 214
    :goto_0
    return-void

    .line 213
    :cond_0
    iget-wide v0, p0, Lorg/chromium/chrome/browser/infobar/TranslateInfoBar;->mNativeInfoBarPtr:J

    const-string/jumbo v2, ""

    invoke-virtual {p0, v0, v1, p1, v2}, Lorg/chromium/chrome/browser/infobar/TranslateInfoBar;->nativeOnButtonClicked(JILjava/lang/String;)V

    goto :goto_0
.end method

.method private panelFor(I)Lorg/chromium/chrome/browser/infobar/TranslateSubPanel;
    .locals 2

    .prologue
    .line 267
    sget-boolean v0, Lorg/chromium/chrome/browser/infobar/TranslateInfoBar;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    if-ltz p1, :cond_0

    const/4 v0, 0x4

    if-lt p1, v0, :cond_1

    :cond_0
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 268
    :cond_1
    packed-switch p1, :pswitch_data_0

    .line 276
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 270
    :pswitch_0
    new-instance v0, Lorg/chromium/chrome/browser/infobar/TranslateLanguagePanel;

    iget-object v1, p0, Lorg/chromium/chrome/browser/infobar/TranslateInfoBar;->mOptions:Lorg/chromium/chrome/browser/infobar/TranslateOptions;

    invoke-direct {v0, p0, v1}, Lorg/chromium/chrome/browser/infobar/TranslateLanguagePanel;-><init>(Lorg/chromium/chrome/browser/infobar/SubPanelListener;Lorg/chromium/chrome/browser/infobar/TranslateOptions;)V

    goto :goto_0

    .line 272
    :pswitch_1
    new-instance v0, Lorg/chromium/chrome/browser/infobar/TranslateNeverPanel;

    iget-object v1, p0, Lorg/chromium/chrome/browser/infobar/TranslateInfoBar;->mOptions:Lorg/chromium/chrome/browser/infobar/TranslateOptions;

    invoke-direct {v0, p0, v1}, Lorg/chromium/chrome/browser/infobar/TranslateNeverPanel;-><init>(Lorg/chromium/chrome/browser/infobar/SubPanelListener;Lorg/chromium/chrome/browser/infobar/TranslateOptions;)V

    goto :goto_0

    .line 274
    :pswitch_2
    new-instance v0, Lorg/chromium/chrome/browser/infobar/TranslateAlwaysPanel;

    iget-object v1, p0, Lorg/chromium/chrome/browser/infobar/TranslateInfoBar;->mOptions:Lorg/chromium/chrome/browser/infobar/TranslateOptions;

    invoke-direct {v0, p0, v1}, Lorg/chromium/chrome/browser/infobar/TranslateAlwaysPanel;-><init>(Lorg/chromium/chrome/browser/infobar/SubPanelListener;Lorg/chromium/chrome/browser/infobar/TranslateOptions;)V

    goto :goto_0

    .line 268
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private swapPanel(I)V
    .locals 1

    .prologue
    .line 258
    sget-boolean v0, Lorg/chromium/chrome/browser/infobar/TranslateInfoBar;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    if-ltz p1, :cond_0

    const/4 v0, 0x4

    if-lt p1, v0, :cond_1

    :cond_0
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 259
    :cond_1
    iput p1, p0, Lorg/chromium/chrome/browser/infobar/TranslateInfoBar;->mOptionsPanelViewType:I

    .line 260
    invoke-virtual {p0}, Lorg/chromium/chrome/browser/infobar/TranslateInfoBar;->createView()Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v0}, Lorg/chromium/chrome/browser/infobar/TranslateInfoBar;->updateViewForCurrentState(Landroid/view/View;)V

    .line 261
    return-void
.end method

.method private updateViewForCurrentState(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 284
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lorg/chromium/chrome/browser/infobar/TranslateInfoBar;->setControlsEnabled(Z)V

    .line 285
    invoke-virtual {p0}, Lorg/chromium/chrome/browser/infobar/TranslateInfoBar;->getInfoBarContainer()Lorg/chromium/chrome/browser/infobar/InfoBarContainer;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lorg/chromium/chrome/browser/infobar/InfoBarContainer;->swapInfoBarViews(Lorg/chromium/chrome/browser/infobar/InfoBar;Landroid/view/View;)V

    .line 286
    return-void
.end method


# virtual methods
.method changeInfoBarTypeAndNativePointer(IJ)V
    .locals 2

    .prologue
    .line 336
    if-ltz p1, :cond_1

    const/4 v0, 0x4

    if-ge p1, v0, :cond_1

    .line 337
    iput p1, p0, Lorg/chromium/chrome/browser/infobar/TranslateInfoBar;->mInfoBarType:I

    .line 338
    invoke-virtual {p0, p2, p3}, Lorg/chromium/chrome/browser/infobar/TranslateInfoBar;->replaceNativePointer(J)V

    .line 339
    invoke-virtual {p0}, Lorg/chromium/chrome/browser/infobar/TranslateInfoBar;->createView()Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v0}, Lorg/chromium/chrome/browser/infobar/TranslateInfoBar;->updateViewForCurrentState(Landroid/view/View;)V

    .line 343
    :cond_0
    return-void

    .line 341
    :cond_1
    sget-boolean v0, Lorg/chromium/chrome/browser/infobar/TranslateInfoBar;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    const-string/jumbo v1, "Trying to change the InfoBar to a type that is invalid."

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0
.end method

.method public createContent(Lorg/chromium/chrome/browser/infobar/InfoBarLayout;)V
    .locals 3

    .prologue
    .line 170
    iget v0, p0, Lorg/chromium/chrome/browser/infobar/TranslateInfoBar;->mOptionsPanelViewType:I

    if-nez v0, :cond_1

    .line 171
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/chromium/chrome/browser/infobar/TranslateInfoBar;->mSubPanel:Lorg/chromium/chrome/browser/infobar/TranslateSubPanel;

    .line 180
    invoke-virtual {p1}, Lorg/chromium/chrome/browser/infobar/InfoBarLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 181
    invoke-direct {p0, v0}, Lorg/chromium/chrome/browser/infobar/TranslateInfoBar;->getMessageText(Landroid/content/Context;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {p1, v1}, Lorg/chromium/chrome/browser/infobar/InfoBarLayout;->setMessage(Ljava/lang/CharSequence;)V

    .line 182
    invoke-direct {p0, v0}, Lorg/chromium/chrome/browser/infobar/TranslateInfoBar;->getPrimaryButtonText(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0}, Lorg/chromium/chrome/browser/infobar/TranslateInfoBar;->getSecondaryButtonText(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v1, v2}, Lorg/chromium/chrome/browser/infobar/InfoBarLayout;->setButtons(Ljava/lang/String;Ljava/lang/String;)V

    .line 184
    invoke-virtual {p0}, Lorg/chromium/chrome/browser/infobar/TranslateInfoBar;->getInfoBarType()I

    move-result v1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_0

    invoke-direct {p0}, Lorg/chromium/chrome/browser/infobar/TranslateInfoBar;->needsAlwaysPanel()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lorg/chromium/chrome/browser/infobar/TranslateInfoBar;->mOptions:Lorg/chromium/chrome/browser/infobar/TranslateOptions;

    invoke-virtual {v1}, Lorg/chromium/chrome/browser/infobar/TranslateOptions;->triggeredFromMenu()Z

    move-result v1

    if-nez v1, :cond_0

    .line 188
    new-instance v1, Lorg/chromium/chrome/browser/infobar/TranslateCheckBox;

    iget-object v2, p0, Lorg/chromium/chrome/browser/infobar/TranslateInfoBar;->mOptions:Lorg/chromium/chrome/browser/infobar/TranslateOptions;

    invoke-direct {v1, v0, v2, p0}, Lorg/chromium/chrome/browser/infobar/TranslateCheckBox;-><init>(Landroid/content/Context;Lorg/chromium/chrome/browser/infobar/TranslateOptions;Lorg/chromium/chrome/browser/infobar/SubPanelListener;)V

    .line 189
    invoke-virtual {p1, v1}, Lorg/chromium/chrome/browser/infobar/InfoBarLayout;->setCustomContent(Landroid/view/View;)V

    .line 191
    :cond_0
    :goto_0
    return-void

    .line 173
    :cond_1
    iget v0, p0, Lorg/chromium/chrome/browser/infobar/TranslateInfoBar;->mOptionsPanelViewType:I

    invoke-direct {p0, v0}, Lorg/chromium/chrome/browser/infobar/TranslateInfoBar;->panelFor(I)Lorg/chromium/chrome/browser/infobar/TranslateSubPanel;

    move-result-object v0

    iput-object v0, p0, Lorg/chromium/chrome/browser/infobar/TranslateInfoBar;->mSubPanel:Lorg/chromium/chrome/browser/infobar/TranslateSubPanel;

    .line 174
    iget-object v0, p0, Lorg/chromium/chrome/browser/infobar/TranslateInfoBar;->mSubPanel:Lorg/chromium/chrome/browser/infobar/TranslateSubPanel;

    if-eqz v0, :cond_0

    .line 175
    iget-object v0, p0, Lorg/chromium/chrome/browser/infobar/TranslateInfoBar;->mSubPanel:Lorg/chromium/chrome/browser/infobar/TranslateSubPanel;

    invoke-virtual {p0}, Lorg/chromium/chrome/browser/infobar/TranslateInfoBar;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Lorg/chromium/chrome/browser/infobar/TranslateSubPanel;->createContent(Landroid/content/Context;Lorg/chromium/chrome/browser/infobar/InfoBarLayout;)V

    goto :goto_0
.end method

.method getInfoBarType()I
    .locals 1

    .prologue
    .line 332
    iget v0, p0, Lorg/chromium/chrome/browser/infobar/TranslateInfoBar;->mInfoBarType:I

    return v0
.end method

.method public onButtonClicked(Z)V
    .locals 3

    .prologue
    const/4 v2, 0x2

    .line 70
    iget-object v0, p0, Lorg/chromium/chrome/browser/infobar/TranslateInfoBar;->mSubPanel:Lorg/chromium/chrome/browser/infobar/TranslateSubPanel;

    if-eqz v0, :cond_0

    .line 71
    iget-object v0, p0, Lorg/chromium/chrome/browser/infobar/TranslateInfoBar;->mSubPanel:Lorg/chromium/chrome/browser/infobar/TranslateSubPanel;

    invoke-interface {v0, p1}, Lorg/chromium/chrome/browser/infobar/TranslateSubPanel;->onButtonClicked(Z)V

    .line 85
    :goto_0
    return-void

    .line 75
    :cond_0
    invoke-direct {p0, p1}, Lorg/chromium/chrome/browser/infobar/TranslateInfoBar;->actionFor(Z)I

    move-result v0

    .line 77
    invoke-virtual {p0}, Lorg/chromium/chrome/browser/infobar/TranslateInfoBar;->getInfoBarType()I

    move-result v1

    if-nez v1, :cond_1

    iget v1, p0, Lorg/chromium/chrome/browser/infobar/TranslateInfoBar;->mOptionsPanelViewType:I

    if-nez v1, :cond_1

    if-ne v0, v2, :cond_1

    invoke-direct {p0}, Lorg/chromium/chrome/browser/infobar/TranslateInfoBar;->needsNeverPanel()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 81
    invoke-direct {p0, v2}, Lorg/chromium/chrome/browser/infobar/TranslateInfoBar;->swapPanel(I)V

    goto :goto_0

    .line 83
    :cond_1
    invoke-direct {p0, v0}, Lorg/chromium/chrome/browser/infobar/TranslateInfoBar;->onTranslateInfoBarButtonClicked(I)V

    goto :goto_0
.end method

.method public onCloseButtonClicked()V
    .locals 2

    .prologue
    .line 60
    invoke-virtual {p0}, Lorg/chromium/chrome/browser/infobar/TranslateInfoBar;->getInfoBarType()I

    move-result v0

    if-nez v0, :cond_0

    iget v0, p0, Lorg/chromium/chrome/browser/infobar/TranslateInfoBar;->mOptionsPanelViewType:I

    if-nez v0, :cond_0

    .line 62
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lorg/chromium/chrome/browser/infobar/TranslateInfoBar;->onButtonClicked(Z)V

    .line 66
    :goto_0
    return-void

    .line 64
    :cond_0
    iget-wide v0, p0, Lorg/chromium/chrome/browser/infobar/TranslateInfoBar;->mNativeInfoBarPtr:J

    invoke-virtual {p0, v0, v1}, Lorg/chromium/chrome/browser/infobar/TranslateInfoBar;->nativeOnCloseButtonClicked(J)V

    goto :goto_0
.end method

.method public onOptionsChanged()V
    .locals 9

    .prologue
    .line 230
    iget-wide v0, p0, Lorg/chromium/chrome/browser/infobar/TranslateInfoBar;->mNativeInfoBarPtr:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    .line 240
    :cond_0
    :goto_0
    return-void

    .line 232
    :cond_1
    iget-object v0, p0, Lorg/chromium/chrome/browser/infobar/TranslateInfoBar;->mOptions:Lorg/chromium/chrome/browser/infobar/TranslateOptions;

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/infobar/TranslateOptions;->optionsChanged()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 233
    iget-object v1, p0, Lorg/chromium/chrome/browser/infobar/TranslateInfoBar;->mTranslateDelegate:Lorg/chromium/chrome/browser/infobar/TranslateInfoBarDelegate;

    iget-wide v2, p0, Lorg/chromium/chrome/browser/infobar/TranslateInfoBar;->mNativeInfoBarPtr:J

    iget-object v0, p0, Lorg/chromium/chrome/browser/infobar/TranslateInfoBar;->mOptions:Lorg/chromium/chrome/browser/infobar/TranslateOptions;

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/infobar/TranslateOptions;->sourceLanguageIndex()I

    move-result v4

    iget-object v0, p0, Lorg/chromium/chrome/browser/infobar/TranslateInfoBar;->mOptions:Lorg/chromium/chrome/browser/infobar/TranslateOptions;

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/infobar/TranslateOptions;->targetLanguageIndex()I

    move-result v5

    iget-object v0, p0, Lorg/chromium/chrome/browser/infobar/TranslateInfoBar;->mOptions:Lorg/chromium/chrome/browser/infobar/TranslateOptions;

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/infobar/TranslateOptions;->alwaysTranslateLanguageState()Z

    move-result v6

    iget-object v0, p0, Lorg/chromium/chrome/browser/infobar/TranslateInfoBar;->mOptions:Lorg/chromium/chrome/browser/infobar/TranslateOptions;

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/infobar/TranslateOptions;->neverTranslateLanguageState()Z

    move-result v7

    iget-object v0, p0, Lorg/chromium/chrome/browser/infobar/TranslateInfoBar;->mOptions:Lorg/chromium/chrome/browser/infobar/TranslateOptions;

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/infobar/TranslateOptions;->neverTranslateDomainState()Z

    move-result v8

    invoke-virtual/range {v1 .. v8}, Lorg/chromium/chrome/browser/infobar/TranslateInfoBarDelegate;->applyTranslateOptions(JIIZZZ)V

    goto :goto_0
.end method

.method public onPanelClosed(I)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 196
    invoke-virtual {p0, v2}, Lorg/chromium/chrome/browser/infobar/TranslateInfoBar;->setControlsEnabled(Z)V

    .line 197
    iget v0, p0, Lorg/chromium/chrome/browser/infobar/TranslateInfoBar;->mOptionsPanelViewType:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 199
    iput v2, p0, Lorg/chromium/chrome/browser/infobar/TranslateInfoBar;->mOptionsPanelViewType:I

    .line 200
    invoke-virtual {p0}, Lorg/chromium/chrome/browser/infobar/TranslateInfoBar;->createView()Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v0}, Lorg/chromium/chrome/browser/infobar/TranslateInfoBar;->updateViewForCurrentState(Landroid/view/View;)V

    .line 205
    :goto_0
    return-void

    .line 203
    :cond_0
    invoke-direct {p0, p1}, Lorg/chromium/chrome/browser/infobar/TranslateInfoBar;->onTranslateInfoBarButtonClicked(I)V

    goto :goto_0
.end method

.method public setControlsEnabled(Z)V
    .locals 2

    .prologue
    .line 218
    invoke-super {p0, p1}, Lorg/chromium/chrome/browser/infobar/InfoBar;->setControlsEnabled(Z)V

    .line 221
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lorg/chromium/chrome/browser/infobar/TranslateInfoBar;->getContentWrapper(Z)Lorg/chromium/chrome/browser/infobar/ContentWrapperView;

    move-result-object v0

    .line 222
    if-eqz v0, :cond_0

    .line 223
    sget v1, Lorg/chromium/chrome/R$id;->infobar_extra_check:I

    invoke-virtual {v0, v1}, Lorg/chromium/chrome/browser/infobar/ContentWrapperView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    .line 224
    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Landroid/widget/CheckBox;->setEnabled(Z)V

    .line 226
    :cond_0
    return-void
.end method
