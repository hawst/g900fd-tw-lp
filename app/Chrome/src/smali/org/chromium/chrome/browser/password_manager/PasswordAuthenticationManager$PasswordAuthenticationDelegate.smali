.class public interface abstract Lorg/chromium/chrome/browser/password_manager/PasswordAuthenticationManager$PasswordAuthenticationDelegate;
.super Ljava/lang/Object;
.source "PasswordAuthenticationManager.java"


# virtual methods
.method public abstract getPasswordProtectionString()Ljava/lang/String;
.end method

.method public abstract isPasswordAuthenticationEnabled()Z
.end method

.method public abstract requestAuthentication(Lorg/chromium/chrome/browser/Tab;Lorg/chromium/chrome/browser/password_manager/PasswordAuthenticationManager$PasswordAuthenticationCallback;)V
.end method
