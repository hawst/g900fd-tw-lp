.class public Lorg/chromium/chrome/browser/ShortcutHelper;
.super Ljava/lang/Object;
.source "ShortcutHelper.java"


# static fields
.field static final synthetic $assertionsDisabled:Z

.field public static final EXTRA_ICON:Ljava/lang/String; = "org.chromium.chrome.browser.webapp_icon"

.field public static final EXTRA_ID:Ljava/lang/String; = "org.chromium.chrome.browser.webapp_id"

.field public static final EXTRA_MAC:Ljava/lang/String; = "org.chromium.chrome.browser.webapp_mac"

.field public static final EXTRA_ORIENTATION:Ljava/lang/String; = "org.chromium.content_public.common.orientation"

.field public static final EXTRA_TITLE:Ljava/lang/String; = "org.chromium.chrome.browser.webapp_title"

.field public static final EXTRA_URL:Ljava/lang/String; = "org.chromium.chrome.browser.webapp_url"

.field private static sFullScreenAction:Ljava/lang/String;


# instance fields
.field private final mAppContext:Landroid/content/Context;

.field private mCallback:Lorg/chromium/chrome/browser/ShortcutHelper$OnInitialized;

.field private mIsInitialized:Z

.field private mNativeShortcutHelper:J

.field private final mTab:Lorg/chromium/chrome/browser/Tab;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 24
    const-class v0, Lorg/chromium/chrome/browser/ShortcutHelper;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/chromium/chrome/browser/ShortcutHelper;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;Lorg/chromium/chrome/browser/Tab;)V
    .locals 0

    .prologue
    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57
    iput-object p1, p0, Lorg/chromium/chrome/browser/ShortcutHelper;->mAppContext:Landroid/content/Context;

    .line 58
    iput-object p2, p0, Lorg/chromium/chrome/browser/ShortcutHelper;->mTab:Lorg/chromium/chrome/browser/Tab;

    .line 59
    return-void
.end method

.method private static addShortcut(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Landroid/graphics/Bitmap;IIIZI)V
    .locals 7

    .prologue
    const/4 v3, 0x0

    .line 127
    sget-boolean v0, Lorg/chromium/chrome/browser/ShortcutHelper;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    sget-object v0, Lorg/chromium/chrome/browser/ShortcutHelper;->sFullScreenAction:Ljava/lang/String;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 130
    :cond_0
    if-eqz p7, :cond_2

    .line 132
    const-string/jumbo v0, ""

    .line 133
    if-eqz p3, :cond_1

    .line 134
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 135
    sget-object v1, Landroid/graphics/Bitmap$CompressFormat;->PNG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v2, 0x64

    invoke-virtual {p3, v1, v2, v0}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 136
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    .line 137
    invoke-static {v0, v3}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v0

    .line 141
    :cond_1
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 142
    sget-object v2, Lorg/chromium/chrome/browser/ShortcutHelper;->sFullScreenAction:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 143
    const-string/jumbo v2, "org.chromium.chrome.browser.webapp_icon"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 144
    const-string/jumbo v0, "org.chromium.chrome.browser.webapp_id"

    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 145
    const-string/jumbo v0, "org.chromium.chrome.browser.webapp_title"

    invoke-virtual {v1, v0, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 146
    const-string/jumbo v0, "org.chromium.chrome.browser.webapp_url"

    invoke-virtual {v1, v0, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 147
    const-string/jumbo v0, "org.chromium.content_public.common.orientation"

    invoke-virtual {v1, v0, p8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 152
    invoke-static {p0, p1}, Lorg/chromium/chrome/browser/WebappAuthenticator;->getMacForUrl(Landroid/content/Context;Ljava/lang/String;)[B

    move-result-object v0

    .line 153
    invoke-static {v0, v3}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v0

    .line 154
    const-string/jumbo v2, "org.chromium.chrome.browser.webapp_mac"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 160
    :goto_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-object v0, p0

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move v5, p5

    move v6, p6

    .line 161
    invoke-static/range {v0 .. v6}, Lorg/chromium/chrome/browser/BookmarkUtils;->createAddToHomeIntent(Landroid/content/Context;Landroid/content/Intent;Ljava/lang/String;Landroid/graphics/Bitmap;III)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 165
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "android.intent.action.MAIN"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 166
    const-string/jumbo v1, "android.intent.category.HOME"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 167
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 168
    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 169
    return-void

    .line 157
    :cond_2
    invoke-static {p0, p1}, Lorg/chromium/chrome/browser/BookmarkUtils;->createShortcutIntent(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    goto :goto_0
.end method

.method private native nativeAddShortcut(JLjava/lang/String;I)V
.end method

.method private native nativeInitialize(J)J
.end method

.method private native nativeTearDown(J)V
.end method

.method private onInitialized(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 92
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/chromium/chrome/browser/ShortcutHelper;->mIsInitialized:Z

    .line 94
    iget-object v0, p0, Lorg/chromium/chrome/browser/ShortcutHelper;->mCallback:Lorg/chromium/chrome/browser/ShortcutHelper$OnInitialized;

    if-eqz v0, :cond_0

    .line 95
    iget-object v0, p0, Lorg/chromium/chrome/browser/ShortcutHelper;->mCallback:Lorg/chromium/chrome/browser/ShortcutHelper$OnInitialized;

    invoke-interface {v0, p1}, Lorg/chromium/chrome/browser/ShortcutHelper$OnInitialized;->onInitialized(Ljava/lang/String;)V

    .line 97
    :cond_0
    return-void
.end method

.method public static setFullScreenAction(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 39
    sput-object p0, Lorg/chromium/chrome/browser/ShortcutHelper;->sFullScreenAction:Ljava/lang/String;

    .line 40
    return-void
.end method


# virtual methods
.method public addShortcut(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 104
    sget-object v0, Lorg/chromium/chrome/browser/ShortcutHelper;->sFullScreenAction:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 105
    const-string/jumbo v0, "ShortcutHelper"

    const-string/jumbo v1, "ShortcutHelper is uninitialized.  Aborting."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 115
    :goto_0
    return-void

    .line 108
    :cond_0
    iget-object v0, p0, Lorg/chromium/chrome/browser/ShortcutHelper;->mAppContext:Landroid/content/Context;

    const-string/jumbo v1, "activity"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    .line 110
    iget-wide v2, p0, Lorg/chromium/chrome/browser/ShortcutHelper;->mNativeShortcutHelper:J

    invoke-virtual {v0}, Landroid/app/ActivityManager;->getLauncherLargeIconSize()I

    move-result v0

    invoke-direct {p0, v2, v3, p1, v0}, Lorg/chromium/chrome/browser/ShortcutHelper;->nativeAddShortcut(JLjava/lang/String;I)V

    .line 113
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/chromium/chrome/browser/ShortcutHelper;->mCallback:Lorg/chromium/chrome/browser/ShortcutHelper$OnInitialized;

    .line 114
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lorg/chromium/chrome/browser/ShortcutHelper;->mNativeShortcutHelper:J

    goto :goto_0
.end method

.method public initialize(Lorg/chromium/chrome/browser/ShortcutHelper$OnInitialized;)V
    .locals 2

    .prologue
    .line 67
    iput-object p1, p0, Lorg/chromium/chrome/browser/ShortcutHelper;->mCallback:Lorg/chromium/chrome/browser/ShortcutHelper$OnInitialized;

    .line 68
    iget-object v0, p0, Lorg/chromium/chrome/browser/ShortcutHelper;->mTab:Lorg/chromium/chrome/browser/Tab;

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/Tab;->getNativePtr()J

    move-result-wide v0

    invoke-direct {p0, v0, v1}, Lorg/chromium/chrome/browser/ShortcutHelper;->nativeInitialize(J)J

    move-result-wide v0

    iput-wide v0, p0, Lorg/chromium/chrome/browser/ShortcutHelper;->mNativeShortcutHelper:J

    .line 69
    return-void
.end method

.method public isInitialized()Z
    .locals 1

    .prologue
    .line 75
    iget-boolean v0, p0, Lorg/chromium/chrome/browser/ShortcutHelper;->mIsInitialized:Z

    return v0
.end method

.method public tearDown()V
    .locals 2

    .prologue
    .line 82
    iget-wide v0, p0, Lorg/chromium/chrome/browser/ShortcutHelper;->mNativeShortcutHelper:J

    invoke-direct {p0, v0, v1}, Lorg/chromium/chrome/browser/ShortcutHelper;->nativeTearDown(J)V

    .line 86
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/chromium/chrome/browser/ShortcutHelper;->mCallback:Lorg/chromium/chrome/browser/ShortcutHelper$OnInitialized;

    .line 87
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lorg/chromium/chrome/browser/ShortcutHelper;->mNativeShortcutHelper:J

    .line 88
    return-void
.end method
