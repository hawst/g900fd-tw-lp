.class public Lorg/chromium/chrome/browser/ChromeBrowserProvider;
.super Landroid/content/ContentProvider;
.source "ChromeBrowserProvider.java"


# static fields
.field private static final BOOKMARK_DEFAULT_PROJECTION:[Ljava/lang/String;

.field public static final BOOKMARK_IS_FOLDER_PARAM:Ljava/lang/String; = "isFolder"

.field public static final BOOKMARK_PARENT_ID_PARAM:Ljava/lang/String; = "parentId"

.field public static final BROWSER_CONTRACTS_BOOKMAKRS_API_URI:Landroid/net/Uri;

.field public static final BROWSER_CONTRACTS_COMBINED_API_URI:Landroid/net/Uri;

.field public static final BROWSER_CONTRACTS_HISTORY_API_URI:Landroid/net/Uri;

.field public static final BROWSER_CONTRACTS_SEARCHES_API_URI:Landroid/net/Uri;

.field static final CLIENT_API_BOOKMARK_NODE_EXISTS:Ljava/lang/String; = "BOOKMARK_NODE_EXISTS"

.field static final CLIENT_API_CREATE_BOOKMARKS_FOLDER_ONCE:Ljava/lang/String; = "CREATE_BOOKMARKS_FOLDER_ONCE"

.field static final CLIENT_API_DELETE_ALL_USER_BOOKMARKS:Ljava/lang/String; = "DELETE_ALL_USER_BOOKMARKS"

.field static final CLIENT_API_GET_BOOKMARK_NODE:Ljava/lang/String; = "GET_BOOKMARK_NODE"

.field static final CLIENT_API_GET_DEFAULT_BOOKMARK_FOLDER:Ljava/lang/String; = "GET_DEFAULT_BOOKMARK_FOLDER"

.field static final CLIENT_API_GET_EDITABLE_BOOKMARK_FOLDER_HIERARCHY:Ljava/lang/String; = "GET_EDITABLE_BOOKMARK_FOLDER_HIERARCHY"

.field static final CLIENT_API_GET_MOBILE_BOOKMARKS_FOLDER_ID:Ljava/lang/String; = "GET_MOBILE_BOOKMARKS_FOLDER_ID"

.field static final CLIENT_API_IS_BOOKMARK_IN_MOBILE_BOOKMARKS_BRANCH:Ljava/lang/String; = "IS_BOOKMARK_IN_MOBILE_BOOKMARKS_BRANCH"

.field static final CLIENT_API_RESULT_KEY:Ljava/lang/String; = "result"

.field static final INVALID_BOOKMARK_ID:J = -0x1L

.field public static final INVALID_CONTENT_PROVIDER_ID:J

.field private static final SUGGEST_PROJECTION:[Ljava/lang/String;


# instance fields
.field protected mContentProviderApiCalled:Z

.field private final mInitializeUriMatcherLock:Ljava/lang/Object;

.field private mLastModifiedBookmarkFolderId:J

.field private final mLoadNativeLock:Ljava/lang/Object;

.field private mMobileBookmarksFolder:Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;

.field private mNativeChromeBrowserProvider:J

.field private mUriMatcher:Landroid/content/UriMatcher;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 98
    const-string/jumbo v0, "com.google.android.apps.chrome.browser-contract"

    const-string/jumbo v1, "bookmarks"

    invoke-static {v0, v1}, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->buildContentUri(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->BROWSER_CONTRACTS_BOOKMAKRS_API_URI:Landroid/net/Uri;

    .line 101
    const-string/jumbo v0, "com.google.android.apps.chrome.browser-contract"

    const-string/jumbo v1, "searches"

    invoke-static {v0, v1}, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->buildContentUri(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->BROWSER_CONTRACTS_SEARCHES_API_URI:Landroid/net/Uri;

    .line 104
    const-string/jumbo v0, "com.google.android.apps.chrome.browser-contract"

    const-string/jumbo v1, "history"

    invoke-static {v0, v1}, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->buildContentUri(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->BROWSER_CONTRACTS_HISTORY_API_URI:Landroid/net/Uri;

    .line 107
    const-string/jumbo v0, "com.google.android.apps.chrome.browser-contract"

    const-string/jumbo v1, "combined"

    invoke-static {v0, v1}, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->buildContentUri(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->BROWSER_CONTRACTS_COMBINED_API_URI:Landroid/net/Uri;

    .line 144
    const/16 v0, 0x8

    new-array v0, v0, [Ljava/lang/String;

    const-string/jumbo v1, "_id"

    aput-object v1, v0, v3

    const-string/jumbo v1, "url"

    aput-object v1, v0, v4

    const-string/jumbo v1, "visits"

    aput-object v1, v0, v5

    const-string/jumbo v1, "date"

    aput-object v1, v0, v6

    const-string/jumbo v1, "bookmark"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string/jumbo v2, "title"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string/jumbo v2, "favicon"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string/jumbo v2, "created"

    aput-object v2, v0, v1

    sput-object v0, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->BOOKMARK_DEFAULT_PROJECTION:[Ljava/lang/String;

    .line 150
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const-string/jumbo v1, "_id"

    aput-object v1, v0, v3

    const-string/jumbo v1, "title"

    aput-object v1, v0, v4

    const-string/jumbo v1, "url"

    aput-object v1, v0, v5

    const-string/jumbo v1, "date"

    aput-object v1, v0, v6

    const-string/jumbo v1, "bookmark"

    aput-object v1, v0, v7

    sput-object v0, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->SUGGEST_PROJECTION:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 50
    invoke-direct {p0}, Landroid/content/ContentProvider;-><init>()V

    .line 158
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->mInitializeUriMatcherLock:Ljava/lang/Object;

    .line 159
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->mLoadNativeLock:Ljava/lang/Object;

    .line 161
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->mLastModifiedBookmarkFolderId:J

    .line 1198
    return-void
.end method

.method static synthetic access$000(Lorg/chromium/chrome/browser/ChromeBrowserProvider;)V
    .locals 0

    .prologue
    .line 50
    invoke-direct {p0}, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->ensureNativeChromeDestroyedOnUIThread()V

    return-void
.end method

.method private addBookmark(Landroid/content/ContentValues;)J
    .locals 10

    .prologue
    const-wide/16 v8, -0x1

    .line 549
    const-string/jumbo v0, "url"

    invoke-virtual {p1, v0}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 550
    const-string/jumbo v0, "title"

    invoke-virtual {p1, v0}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 551
    const/4 v5, 0x0

    .line 552
    const-string/jumbo v0, "isFolder"

    invoke-virtual {p1, v0}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 553
    const-string/jumbo v0, "isFolder"

    invoke-virtual {p1, v0}, Landroid/content/ContentValues;->getAsBoolean(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    .line 556
    :cond_0
    const-string/jumbo v0, "parentId"

    invoke-virtual {p1, v0}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 557
    const-string/jumbo v0, "parentId"

    invoke-virtual {p1, v0}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    .line 559
    :goto_0
    iget-wide v1, p0, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->mNativeChromeBrowserProvider:J

    move-object v0, p0

    invoke-direct/range {v0 .. v7}, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->nativeAddBookmark(JLjava/lang/String;Ljava/lang/String;ZJ)J

    move-result-wide v0

    .line 560
    cmp-long v2, v0, v8

    if-nez v2, :cond_1

    .line 567
    :goto_1
    return-wide v0

    .line 562
    :cond_1
    if-eqz v5, :cond_2

    .line 563
    invoke-direct {p0, v0, v1}, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->updateLastModifiedBookmarkFolder(J)V

    goto :goto_1

    .line 565
    :cond_2
    invoke-direct {p0, v6, v7}, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->updateLastModifiedBookmarkFolder(J)V

    goto :goto_1

    :cond_3
    move-wide v6, v8

    goto :goto_0
.end method

.method private addBookmarkFromAPI(Landroid/content/ContentValues;)J
    .locals 12

    .prologue
    .line 1004
    invoke-static {p1}, Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkRow;->fromContentValues(Landroid/content/ContentValues;)Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkRow;

    move-result-object v0

    .line 1005
    iget-object v1, v0, Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkRow;->mUrl:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 1006
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "Must have a bookmark URL"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1008
    :cond_0
    iget-wide v1, p0, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->mNativeChromeBrowserProvider:J

    iget-object v3, v0, Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkRow;->mUrl:Ljava/lang/String;

    iget-object v4, v0, Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkRow;->mCreated:Ljava/lang/Long;

    iget-object v5, v0, Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkRow;->mIsBookmark:Ljava/lang/Boolean;

    iget-object v6, v0, Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkRow;->mDate:Ljava/lang/Long;

    iget-object v7, v0, Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkRow;->mFavicon:[B

    iget-object v8, v0, Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkRow;->mTitle:Ljava/lang/String;

    iget-object v9, v0, Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkRow;->mVisits:Ljava/lang/Integer;

    iget-wide v10, v0, Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkRow;->mParentId:J

    move-object v0, p0

    invoke-direct/range {v0 .. v11}, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->nativeAddBookmarkFromAPI(JLjava/lang/String;Ljava/lang/Long;Ljava/lang/Boolean;Ljava/lang/Long;[BLjava/lang/String;Ljava/lang/Integer;J)J

    move-result-wide v0

    return-wide v0
.end method

.method private addSearchTermFromAPI(Landroid/content/ContentValues;)J
    .locals 4

    .prologue
    .line 1058
    invoke-static {p1}, Lorg/chromium/chrome/browser/ChromeBrowserProvider$SearchRow;->fromContentValues(Landroid/content/ContentValues;)Lorg/chromium/chrome/browser/ChromeBrowserProvider$SearchRow;

    move-result-object v0

    .line 1059
    iget-object v1, v0, Lorg/chromium/chrome/browser/ChromeBrowserProvider$SearchRow;->mTerm:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 1060
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "Must have a search term"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1062
    :cond_0
    iget-wide v2, p0, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->mNativeChromeBrowserProvider:J

    iget-object v1, v0, Lorg/chromium/chrome/browser/ChromeBrowserProvider$SearchRow;->mTerm:Ljava/lang/String;

    iget-object v0, v0, Lorg/chromium/chrome/browser/ChromeBrowserProvider$SearchRow;->mDate:Ljava/lang/Long;

    invoke-direct {p0, v2, v3, v1, v0}, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->nativeAddSearchTermFromAPI(JLjava/lang/String;Ljava/lang/Long;)J

    move-result-wide v0

    return-wide v0
.end method

.method static argKey(I)Ljava/lang/String;
    .locals 2

    .prologue
    .line 684
    new-instance v0, Ljava/lang/StringBuilder;

    const-string/jumbo v1, "arg"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private bookmarkNodeExists(J)Z
    .locals 3

    .prologue
    .line 606
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-gez v0, :cond_0

    const/4 v0, 0x0

    .line 607
    :goto_0
    return v0

    :cond_0
    iget-wide v0, p0, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->mNativeChromeBrowserProvider:J

    invoke-direct {p0, v0, v1, p1, p2}, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->nativeBookmarkNodeExists(JJ)Z

    move-result v0

    goto :goto_0
.end method

.method private static buildAPIContentUri(Landroid/content/Context;Ljava/lang/String;)Landroid/net/Uri;
    .locals 2

    .prologue
    .line 1105
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ".browser"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p1}, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->buildContentUri(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method private static buildBookmarkWhereClause(JLjava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 1145
    const/4 v0, 0x1

    invoke-static {p2, v0}, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->buildBookmarkWhereClause(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, p1, v0}, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->buildWhereClause(JLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static buildBookmarkWhereClause(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1149
    const/4 v0, 0x1

    invoke-static {p0, v0}, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->buildBookmarkWhereClause(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static buildBookmarkWhereClause(Ljava/lang/String;Z)Ljava/lang/String;
    .locals 2

    .prologue
    .line 1133
    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    .line 1134
    const-string/jumbo v0, "bookmark"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1135
    if-eqz p1, :cond_1

    const-string/jumbo v0, " = 1 "

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1136
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1137
    const-string/jumbo v0, " AND ("

    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1138
    invoke-virtual {v1, p0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1139
    const-string/jumbo v0, ")"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1141
    :cond_0
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 1135
    :cond_1
    const-string/jumbo v0, " = 0"

    goto :goto_0
.end method

.method private static buildContentUri(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;
    .locals 2

    .prologue
    .line 1101
    new-instance v0, Ljava/lang/StringBuilder;

    const-string/jumbo v1, "content://"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method private static buildHistoryWhereClause(JLjava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 1122
    const/4 v0, 0x0

    invoke-static {p2, v0}, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->buildBookmarkWhereClause(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, p1, v0}, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->buildWhereClause(JLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static buildHistoryWhereClause(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1126
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->buildBookmarkWhereClause(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private buildSuggestWhere(Ljava/lang/String;I)Ljava/lang/String;
    .locals 3

    .prologue
    .line 259
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1, p1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 260
    const/4 v0, 0x0

    :goto_0
    add-int/lit8 v2, p2, -0x1

    if-ge v0, v2, :cond_0

    .line 261
    const-string/jumbo v2, " OR "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 262
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 260
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 264
    :cond_0
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static buildWhereClause(JLjava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 1109
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 1110
    const-string/jumbo v1, "_id"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1111
    const-string/jumbo v1, " = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1112
    invoke-virtual {v0, p0, p1}, Ljava/lang/StringBuffer;->append(J)Ljava/lang/StringBuffer;

    .line 1113
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1114
    const-string/jumbo v1, " AND ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1115
    invoke-virtual {v0, p2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1116
    const-string/jumbo v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1118
    :cond_0
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private canHandleContentProviderApiCall()Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 735
    iput-boolean v1, p0, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->mContentProviderApiCalled:Z

    .line 737
    invoke-static {}, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->isInUiThread()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 739
    :cond_0
    :goto_0
    return v0

    .line 738
    :cond_1
    invoke-direct {p0}, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->ensureNativeChromeLoaded()Z

    move-result v2

    if-eqz v2, :cond_0

    move v0, v1

    .line 739
    goto :goto_0
.end method

.method private createBookmarksFolderOnce(Ljava/lang/String;J)J
    .locals 6

    .prologue
    .line 611
    iget-wide v1, p0, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->mNativeChromeBrowserProvider:J

    move-object v0, p0

    move-object v3, p1

    move-wide v4, p2

    invoke-direct/range {v0 .. v5}, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->nativeCreateBookmarksFolderOnce(JLjava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method private ensureNativeChromeDestroyedOnUIThread()V
    .locals 2

    .prologue
    .line 1271
    invoke-virtual {p0}, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->isNativeSideInitialized()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1272
    iget-wide v0, p0, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->mNativeChromeBrowserProvider:J

    invoke-direct {p0, v0, v1}, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->nativeDestroy(J)V

    .line 1273
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->mNativeChromeBrowserProvider:J

    .line 1275
    :cond_0
    return-void
.end method

.method private ensureNativeChromeLoaded()Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    .line 1227
    invoke-direct {p0}, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->ensureUriMatcherInitialized()V

    .line 1229
    iget-object v1, p0, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->mLoadNativeLock:Ljava/lang/Object;

    monitor-enter v1

    .line 1230
    :try_start_0
    iget-wide v2, p0, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->mNativeChromeBrowserProvider:J

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    monitor-exit v1

    .line 1239
    :goto_0
    return v0

    .line 1232
    :cond_0
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v2, 0x1

    invoke-direct {v0, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    .line 1233
    new-instance v2, Lorg/chromium/chrome/browser/ChromeBrowserProvider$1;

    invoke-direct {v2, p0, v0}, Lorg/chromium/chrome/browser/ChromeBrowserProvider$1;-><init>(Lorg/chromium/chrome/browser/ChromeBrowserProvider;Ljava/util/concurrent/atomic/AtomicBoolean;)V

    invoke-static {v2}, Lorg/chromium/base/ThreadUtils;->runOnUiThreadBlocking(Ljava/lang/Runnable;)V

    .line 1239
    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 1240
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private ensureUriMatcherInitialized()V
    .locals 6

    .prologue
    .line 171
    iget-object v1, p0, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->mInitializeUriMatcherLock:Ljava/lang/Object;

    monitor-enter v1

    .line 172
    :try_start_0
    iget-object v0, p0, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->mUriMatcher:Landroid/content/UriMatcher;

    if-eqz v0, :cond_0

    monitor-exit v1

    .line 235
    :goto_0
    return-void

    .line 174
    :cond_0
    new-instance v0, Landroid/content/UriMatcher;

    const/4 v2, -0x1

    invoke-direct {v0, v2}, Landroid/content/UriMatcher;-><init>(I)V

    iput-object v0, p0, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->mUriMatcher:Landroid/content/UriMatcher;

    .line 176
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v2, ".ChromeBrowserProvider"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 177
    iget-object v2, p0, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->mUriMatcher:Landroid/content/UriMatcher;

    const-string/jumbo v3, "bookmarks"

    const/4 v4, 0x0

    invoke-virtual {v2, v0, v3, v4}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 178
    iget-object v2, p0, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->mUriMatcher:Landroid/content/UriMatcher;

    const-string/jumbo v3, "bookmarks/#"

    const/4 v4, 0x1

    invoke-virtual {v2, v0, v3, v4}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 180
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v2, ".browser"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 181
    iget-object v2, p0, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->mUriMatcher:Landroid/content/UriMatcher;

    const-string/jumbo v3, "bookmarks"

    const/4 v4, 0x2

    invoke-virtual {v2, v0, v3, v4}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 182
    iget-object v2, p0, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->mUriMatcher:Landroid/content/UriMatcher;

    const-string/jumbo v3, "bookmarks/#"

    const/4 v4, 0x3

    invoke-virtual {v2, v0, v3, v4}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 183
    iget-object v2, p0, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->mUriMatcher:Landroid/content/UriMatcher;

    const-string/jumbo v3, "searches"

    const/4 v4, 0x4

    invoke-virtual {v2, v0, v3, v4}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 184
    iget-object v2, p0, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->mUriMatcher:Landroid/content/UriMatcher;

    const-string/jumbo v3, "searches/#"

    const/4 v4, 0x5

    invoke-virtual {v2, v0, v3, v4}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 185
    iget-object v2, p0, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->mUriMatcher:Landroid/content/UriMatcher;

    const-string/jumbo v3, "history"

    const/4 v4, 0x6

    invoke-virtual {v2, v0, v3, v4}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 186
    iget-object v2, p0, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->mUriMatcher:Landroid/content/UriMatcher;

    const-string/jumbo v3, "history/#"

    const/4 v4, 0x7

    invoke-virtual {v2, v0, v3, v4}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 187
    iget-object v2, p0, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->mUriMatcher:Landroid/content/UriMatcher;

    const-string/jumbo v3, "combined"

    const/4 v4, 0x2

    invoke-virtual {v2, v0, v3, v4}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 188
    iget-object v2, p0, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->mUriMatcher:Landroid/content/UriMatcher;

    const-string/jumbo v3, "combined/#"

    const/4 v4, 0x3

    invoke-virtual {v2, v0, v3, v4}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 190
    iget-object v2, p0, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->mUriMatcher:Landroid/content/UriMatcher;

    const-string/jumbo v3, "com.google.android.apps.chrome.browser-contract"

    const-string/jumbo v4, "history"

    const/4 v5, 0x6

    invoke-virtual {v2, v3, v4, v5}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 192
    iget-object v2, p0, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->mUriMatcher:Landroid/content/UriMatcher;

    const-string/jumbo v3, "com.google.android.apps.chrome.browser-contract"

    const-string/jumbo v4, "history/#"

    const/4 v5, 0x7

    invoke-virtual {v2, v3, v4, v5}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 194
    iget-object v2, p0, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->mUriMatcher:Landroid/content/UriMatcher;

    const-string/jumbo v3, "com.google.android.apps.chrome.browser-contract"

    const-string/jumbo v4, "combined"

    const/4 v5, 0x2

    invoke-virtual {v2, v3, v4, v5}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 196
    iget-object v2, p0, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->mUriMatcher:Landroid/content/UriMatcher;

    const-string/jumbo v3, "com.google.android.apps.chrome.browser-contract"

    const-string/jumbo v4, "combined/#"

    const/4 v5, 0x3

    invoke-virtual {v2, v3, v4, v5}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 198
    iget-object v2, p0, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->mUriMatcher:Landroid/content/UriMatcher;

    const-string/jumbo v3, "com.google.android.apps.chrome.browser-contract"

    const-string/jumbo v4, "searches"

    const/4 v5, 0x4

    invoke-virtual {v2, v3, v4, v5}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 200
    iget-object v2, p0, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->mUriMatcher:Landroid/content/UriMatcher;

    const-string/jumbo v3, "com.google.android.apps.chrome.browser-contract"

    const-string/jumbo v4, "searches/#"

    const/4 v5, 0x5

    invoke-virtual {v2, v3, v4, v5}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 202
    iget-object v2, p0, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->mUriMatcher:Landroid/content/UriMatcher;

    const-string/jumbo v3, "com.google.android.apps.chrome.browser-contract"

    const-string/jumbo v4, "bookmarks"

    const/16 v5, 0x8

    invoke-virtual {v2, v3, v4, v5}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 204
    iget-object v2, p0, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->mUriMatcher:Landroid/content/UriMatcher;

    const-string/jumbo v3, "com.google.android.apps.chrome.browser-contract"

    const-string/jumbo v4, "bookmarks/#"

    const/16 v5, 0x9

    invoke-virtual {v2, v3, v4, v5}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 209
    iget-object v2, p0, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->mUriMatcher:Landroid/content/UriMatcher;

    const-string/jumbo v3, "com.android.browser"

    const-string/jumbo v4, "history"

    const/4 v5, 0x6

    invoke-virtual {v2, v3, v4, v5}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 211
    iget-object v2, p0, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->mUriMatcher:Landroid/content/UriMatcher;

    const-string/jumbo v3, "com.android.browser"

    const-string/jumbo v4, "history/#"

    const/4 v5, 0x7

    invoke-virtual {v2, v3, v4, v5}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 213
    iget-object v2, p0, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->mUriMatcher:Landroid/content/UriMatcher;

    const-string/jumbo v3, "com.android.browser"

    const-string/jumbo v4, "combined"

    const/4 v5, 0x2

    invoke-virtual {v2, v3, v4, v5}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 214
    iget-object v2, p0, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->mUriMatcher:Landroid/content/UriMatcher;

    const-string/jumbo v3, "com.android.browser"

    const-string/jumbo v4, "combined/#"

    const/4 v5, 0x3

    invoke-virtual {v2, v3, v4, v5}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 215
    iget-object v2, p0, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->mUriMatcher:Landroid/content/UriMatcher;

    const-string/jumbo v3, "com.android.browser"

    const-string/jumbo v4, "searches"

    const/4 v5, 0x4

    invoke-virtual {v2, v3, v4, v5}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 216
    iget-object v2, p0, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->mUriMatcher:Landroid/content/UriMatcher;

    const-string/jumbo v3, "com.android.browser"

    const-string/jumbo v4, "searches/#"

    const/4 v5, 0x5

    invoke-virtual {v2, v3, v4, v5}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 218
    iget-object v2, p0, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->mUriMatcher:Landroid/content/UriMatcher;

    const-string/jumbo v3, "com.android.browser"

    const-string/jumbo v4, "bookmarks"

    const/16 v5, 0x8

    invoke-virtual {v2, v3, v4, v5}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 220
    iget-object v2, p0, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->mUriMatcher:Landroid/content/UriMatcher;

    const-string/jumbo v3, "com.android.browser"

    const-string/jumbo v4, "bookmarks/#"

    const/16 v5, 0x9

    invoke-virtual {v2, v3, v4, v5}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 224
    iget-object v2, p0, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->mUriMatcher:Landroid/content/UriMatcher;

    const-string/jumbo v3, "browser"

    const-string/jumbo v4, "bookmarks"

    const/4 v5, 0x2

    invoke-virtual {v2, v3, v4, v5}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 225
    iget-object v2, p0, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->mUriMatcher:Landroid/content/UriMatcher;

    const-string/jumbo v3, "browser"

    const-string/jumbo v4, "bookmarks/#"

    const/4 v5, 0x3

    invoke-virtual {v2, v3, v4, v5}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 226
    iget-object v2, p0, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->mUriMatcher:Landroid/content/UriMatcher;

    const-string/jumbo v3, "browser"

    const-string/jumbo v4, "searches"

    const/4 v5, 0x4

    invoke-virtual {v2, v3, v4, v5}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 227
    iget-object v2, p0, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->mUriMatcher:Landroid/content/UriMatcher;

    const-string/jumbo v3, "browser"

    const-string/jumbo v4, "searches/#"

    const/4 v5, 0x5

    invoke-virtual {v2, v3, v4, v5}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 229
    iget-object v2, p0, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->mUriMatcher:Landroid/content/UriMatcher;

    const-string/jumbo v3, "bookmarks/search_suggest_query"

    const/16 v4, 0xa

    invoke-virtual {v2, v0, v3, v4}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 232
    iget-object v2, p0, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->mUriMatcher:Landroid/content/UriMatcher;

    const-string/jumbo v3, "search_suggest_query"

    const/16 v4, 0xb

    invoke-virtual {v2, v0, v3, v4}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 235
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto/16 :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static getApiAuthority(Landroid/content/Context;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 582
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ".browser"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getBookmarkHistorySuggestions(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Z)Landroid/database/Cursor;
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 274
    .line 275
    new-instance v1, Ljava/util/Vector;

    invoke-direct {v1}, Ljava/util/Vector;-><init>()V

    .line 276
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    aget-object v3, p2, v0

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "%"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 277
    aget-object v3, p2, v0

    const-string/jumbo v4, "http"

    invoke-virtual {v3, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    aget-object v3, p2, v0

    const-string/jumbo v4, "file"

    invoke-virtual {v3, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 278
    :cond_0
    invoke-virtual {v1, v2}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 289
    :goto_0
    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "("

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 290
    invoke-virtual {v1}, Ljava/util/Vector;->size()I

    move-result v4

    invoke-direct {p0, p1, v4}, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->buildSuggestWhere(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 291
    if-eqz v0, :cond_1

    .line 292
    invoke-virtual {v1, v2}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 293
    const-string/jumbo v0, " OR title LIKE ?"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 295
    :cond_1
    const-string/jumbo v0, ")"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 297
    if-eqz p4, :cond_2

    .line 298
    const-string/jumbo v0, " AND bookmark=?"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 299
    const-string/jumbo v0, "1"

    invoke-virtual {v1, v0}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 302
    :cond_2
    invoke-virtual {v1, p2}, Ljava/util/Vector;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    .line 303
    sget-object v1, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->SUGGEST_PROJECTION:[Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v1, v2, v0, p3}, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->queryBookmarkFromAPI([Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 305
    new-instance v1, Lorg/chromium/chrome/browser/ChromeBrowserProviderSuggestionsCursor;

    invoke-direct {v1, v0}, Lorg/chromium/chrome/browser/ChromeBrowserProviderSuggestionsCursor;-><init>(Landroid/database/Cursor;)V

    return-object v1

    .line 281
    :cond_3
    new-instance v0, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "http://"

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 282
    new-instance v0, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "https://"

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 283
    new-instance v0, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "http://www."

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 284
    new-instance v0, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "https://www."

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 285
    new-instance v0, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "file://"

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 286
    const/4 v0, 0x1

    goto/16 :goto_0
.end method

.method public static getBookmarksApiUri(Landroid/content/Context;)Landroid/net/Uri;
    .locals 2

    .prologue
    .line 598
    invoke-static {p0}, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->getApiAuthority(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "bookmarks"

    invoke-static {v0, v1}, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->buildContentUri(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public static getBookmarksUri(Landroid/content/Context;)Landroid/net/Uri;
    .locals 2

    .prologue
    .line 590
    invoke-static {p0}, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->getInternalAuthority(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "bookmarks"

    invoke-static {v0, v1}, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->buildContentUri(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method private static getContentUriId(Landroid/net/Uri;)J
    .locals 3

    .prologue
    const-wide/16 v0, -0x1

    .line 314
    :try_start_0
    invoke-static {p0}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J
    :try_end_0
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-wide v0

    .line 318
    :goto_0
    return-wide v0

    .line 316
    :catch_0
    move-exception v2

    goto :goto_0

    .line 318
    :catch_1
    move-exception v2

    goto :goto_0
.end method

.method private getDefaultBookmarkFolder()Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;
    .locals 8

    .prologue
    const/4 v4, 0x0

    .line 644
    invoke-direct {p0}, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->getLastModifiedBookmarkFolderId()J

    move-result-wide v2

    move-object v1, p0

    move v5, v4

    move v6, v4

    move v7, v4

    invoke-virtual/range {v1 .. v7}, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->getBookmarkNode(JZZZZ)Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;

    move-result-object v0

    .line 646
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;->isUrl()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 647
    :cond_0
    invoke-direct {p0}, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->getMobileBookmarksFolder()Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;

    move-result-object v2

    .line 648
    if-eqz v2, :cond_2

    invoke-virtual {v2}, Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;->id()J

    move-result-wide v0

    :goto_0
    iput-wide v0, p0, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->mLastModifiedBookmarkFolderId:J

    move-object v0, v2

    .line 651
    :cond_1
    return-object v0

    .line 648
    :cond_2
    const-wide/16 v0, -0x1

    goto :goto_0
.end method

.method private getEditableBookmarkFolderHierarchy()Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;
    .locals 2

    .prologue
    .line 615
    iget-wide v0, p0, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->mNativeChromeBrowserProvider:J

    invoke-direct {p0, v0, v1}, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->nativeGetEditableBookmarkFolders(J)Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;

    move-result-object v0

    return-object v0
.end method

.method public static getInternalAuthority(Landroid/content/Context;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 586
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ".ChromeBrowserProvider"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getLastModifiedBookmarkFolderId()J
    .locals 4

    .prologue
    const-wide/16 v2, -0x1

    .line 249
    iget-wide v0, p0, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->mLastModifiedBookmarkFolderId:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 250
    invoke-virtual {p0}, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 252
    const-string/jumbo v1, "last_bookmark_folder_id"

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    iput-wide v0, p0, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->mLastModifiedBookmarkFolderId:J

    .line 255
    :cond_0
    iget-wide v0, p0, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->mLastModifiedBookmarkFolderId:J

    return-wide v0
.end method

.method private getMobileBookmarksFolder()Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;
    .locals 2

    .prologue
    .line 667
    iget-object v0, p0, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->mMobileBookmarksFolder:Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;

    if-nez v0, :cond_0

    .line 668
    iget-wide v0, p0, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->mNativeChromeBrowserProvider:J

    invoke-direct {p0, v0, v1}, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->nativeGetMobileBookmarksFolder(J)Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;

    move-result-object v0

    iput-object v0, p0, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->mMobileBookmarksFolder:Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;

    .line 670
    :cond_0
    iget-object v0, p0, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->mMobileBookmarksFolder:Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;

    return-object v0
.end method

.method private getReadWritePermissionNameForBookmarkFolders()Ljava/lang/String;
    .locals 2

    .prologue
    .line 268
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ".permission.READ_WRITE_BOOKMARK_FOLDERS"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private isBookmarkInMobileBookmarksBranch(J)Z
    .locals 3

    .prologue
    .line 679
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-gtz v0, :cond_0

    const/4 v0, 0x0

    .line 680
    :goto_0
    return v0

    :cond_0
    iget-wide v0, p0, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->mNativeChromeBrowserProvider:J

    invoke-direct {p0, v0, v1, p1, p2}, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->nativeIsBookmarkInMobileBookmarksBranch(JJ)Z

    move-result v0

    goto :goto_0
.end method

.method private static isInUiThread()Z
    .locals 2

    .prologue
    .line 1090
    invoke-static {}, Lorg/chromium/base/ThreadUtils;->runningOnUiThread()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 1097
    :goto_0
    return v0

    .line 1092
    :cond_0
    const-string/jumbo v0, "REL"

    sget-object v1, Landroid/os/Build$VERSION;->CODENAME:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1093
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "Shouldn\'t run in the UI thread"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1096
    :cond_1
    const-string/jumbo v0, "ChromeBrowserProvider"

    const-string/jumbo v1, "ChromeBrowserProvider methods cannot be called from the UI thread."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1097
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private native nativeAddBookmark(JLjava/lang/String;Ljava/lang/String;ZJ)J
.end method

.method private native nativeAddBookmarkFromAPI(JLjava/lang/String;Ljava/lang/Long;Ljava/lang/Boolean;Ljava/lang/Long;[BLjava/lang/String;Ljava/lang/Integer;J)J
.end method

.method private native nativeAddSearchTermFromAPI(JLjava/lang/String;Ljava/lang/Long;)J
.end method

.method private native nativeBookmarkNodeExists(JJ)Z
.end method

.method private native nativeCreateBookmarksFolderOnce(JLjava/lang/String;J)J
.end method

.method private native nativeDestroy(J)V
.end method

.method private native nativeGetBookmarkNode(JJZZ)Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;
.end method

.method private native nativeGetEditableBookmarkFolders(J)Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;
.end method

.method private native nativeGetFaviconOrTouchIcon(JLjava/lang/String;)[B
.end method

.method private native nativeGetMobileBookmarksFolder(J)Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;
.end method

.method private native nativeGetThumbnail(JLjava/lang/String;)[B
.end method

.method private native nativeInit()J
.end method

.method private native nativeIsBookmarkInMobileBookmarksBranch(JJ)Z
.end method

.method private native nativeQueryBookmarkFromAPI(J[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Lorg/chromium/chrome/browser/database/SQLiteCursor;
.end method

.method private native nativeQuerySearchTermFromAPI(J[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Lorg/chromium/chrome/browser/database/SQLiteCursor;
.end method

.method private native nativeRemoveAllUserBookmarks(J)V
.end method

.method private native nativeRemoveBookmark(JJ)I
.end method

.method private native nativeRemoveBookmarkFromAPI(JLjava/lang/String;[Ljava/lang/String;)I
.end method

.method private native nativeRemoveHistoryFromAPI(JLjava/lang/String;[Ljava/lang/String;)I
.end method

.method private native nativeRemoveSearchTermFromAPI(JLjava/lang/String;[Ljava/lang/String;)I
.end method

.method private native nativeUpdateBookmark(JJLjava/lang/String;Ljava/lang/String;J)I
.end method

.method private native nativeUpdateBookmarkFromAPI(JLjava/lang/String;Ljava/lang/Long;Ljava/lang/Boolean;Ljava/lang/Long;[BLjava/lang/String;Ljava/lang/Integer;JLjava/lang/String;[Ljava/lang/String;)I
.end method

.method private native nativeUpdateSearchTermFromAPI(JLjava/lang/String;Ljava/lang/Long;Ljava/lang/String;[Ljava/lang/String;)I
.end method

.method private notifyChange(Landroid/net/Uri;)V
    .locals 2

    .prologue
    .line 1285
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x11

    if-lt v0, v1, :cond_0

    .line 1286
    invoke-static {}, Landroid/os/Binder;->getCallingUserHandle()Landroid/os/UserHandle;

    move-result-object v0

    .line 1287
    if-eqz v0, :cond_0

    invoke-static {}, Landroid/os/Process;->myUserHandle()Landroid/os/UserHandle;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/UserHandle;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1289
    new-instance v0, Lorg/chromium/chrome/browser/ChromeBrowserProvider$3;

    invoke-direct {v0, p0, p1}, Lorg/chromium/chrome/browser/ChromeBrowserProvider$3;-><init>(Lorg/chromium/chrome/browser/ChromeBrowserProvider;Landroid/net/Uri;)V

    invoke-static {v0}, Lorg/chromium/base/ThreadUtils;->postOnUiThread(Ljava/lang/Runnable;)V

    .line 1299
    :goto_0
    return-void

    .line 1298
    :cond_0
    invoke-virtual {p0}, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    goto :goto_0
.end method

.method private onBookmarkChanged()V
    .locals 2

    .prologue
    .line 1044
    invoke-virtual {p0}, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string/jumbo v1, "bookmarks"

    invoke-static {v0, v1}, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->buildAPIContentUri(Landroid/content/Context;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-direct {p0, v0}, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->notifyChange(Landroid/net/Uri;)V

    .line 1045
    return-void
.end method

.method private onHistoryChanged()V
    .locals 2

    .prologue
    .line 1049
    invoke-virtual {p0}, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string/jumbo v1, "history"

    invoke-static {v0, v1}, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->buildAPIContentUri(Landroid/content/Context;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-direct {p0, v0}, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->notifyChange(Landroid/net/Uri;)V

    .line 1050
    return-void
.end method

.method private onSearchTermChanged()V
    .locals 2

    .prologue
    .line 1054
    invoke-virtual {p0}, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string/jumbo v1, "searches"

    invoke-static {v0, v1}, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->buildAPIContentUri(Landroid/content/Context;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-direct {p0, v0}, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->notifyChange(Landroid/net/Uri;)V

    .line 1055
    return-void
.end method

.method private populateNodeImages(Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;ZZ)V
    .locals 3

    .prologue
    .line 655
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;->type()Lorg/chromium/chrome/browser/ChromeBrowserProvider$Type;

    move-result-object v0

    sget-object v1, Lorg/chromium/chrome/browser/ChromeBrowserProvider$Type;->URL:Lorg/chromium/chrome/browser/ChromeBrowserProvider$Type;

    if-eq v0, v1, :cond_1

    .line 664
    :cond_0
    :goto_0
    return-void

    .line 657
    :cond_1
    if-eqz p2, :cond_2

    .line 658
    iget-wide v0, p0, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->mNativeChromeBrowserProvider:J

    invoke-virtual {p1}, Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;->url()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v0, v1, v2}, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->nativeGetFaviconOrTouchIcon(JLjava/lang/String;)[B

    move-result-object v0

    invoke-virtual {p1, v0}, Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;->setFavicon([B)V

    .line 661
    :cond_2
    if-eqz p3, :cond_0

    .line 662
    iget-wide v0, p0, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->mNativeChromeBrowserProvider:J

    invoke-virtual {p1}, Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;->url()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v0, v1, v2}, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->nativeGetThumbnail(JLjava/lang/String;)[B

    move-result-object v0

    invoke-virtual {p1, v0}, Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;->setThumbnail([B)V

    goto :goto_0
.end method

.method private queryBookmarkFromAPI([Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 8

    .prologue
    .line 1015
    if-eqz p1, :cond_0

    array-length v0, p1

    if-nez v0, :cond_1

    .line 1017
    :cond_0
    sget-object v4, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->BOOKMARK_DEFAULT_PROJECTION:[Ljava/lang/String;

    .line 1022
    :goto_0
    iget-wide v2, p0, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->mNativeChromeBrowserProvider:J

    move-object v1, p0

    move-object v5, p2

    move-object v6, p3

    move-object v7, p4

    invoke-direct/range {v1 .. v7}, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->nativeQueryBookmarkFromAPI(J[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Lorg/chromium/chrome/browser/database/SQLiteCursor;

    move-result-object v0

    return-object v0

    :cond_1
    move-object v4, p1

    .line 1019
    goto :goto_0
.end method

.method private querySearchTermFromAPI([Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 8

    .prologue
    .line 1074
    if-eqz p1, :cond_0

    array-length v0, p1

    if-nez v0, :cond_1

    .line 1076
    :cond_0
    sget-object v4, Landroid/provider/Browser;->SEARCHES_PROJECTION:[Ljava/lang/String;

    .line 1080
    :goto_0
    iget-wide v2, p0, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->mNativeChromeBrowserProvider:J

    move-object v1, p0

    move-object v5, p2

    move-object v6, p3

    move-object v7, p4

    invoke-direct/range {v1 .. v7}, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->nativeQuerySearchTermFromAPI(J[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Lorg/chromium/chrome/browser/database/SQLiteCursor;

    move-result-object v0

    return-object v0

    :cond_1
    move-object v4, p1

    .line 1078
    goto :goto_0
.end method

.method private removeBookmarkFromAPI(Ljava/lang/String;[Ljava/lang/String;)I
    .locals 2

    .prologue
    .line 1035
    iget-wide v0, p0, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->mNativeChromeBrowserProvider:J

    invoke-direct {p0, v0, v1, p1, p2}, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->nativeRemoveBookmarkFromAPI(JLjava/lang/String;[Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method private removeHistoryFromAPI(Ljava/lang/String;[Ljava/lang/String;)I
    .locals 2

    .prologue
    .line 1039
    iget-wide v0, p0, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->mNativeChromeBrowserProvider:J

    invoke-direct {p0, v0, v1, p1, p2}, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->nativeRemoveHistoryFromAPI(JLjava/lang/String;[Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method private removeSearchFromAPI(Ljava/lang/String;[Ljava/lang/String;)I
    .locals 2

    .prologue
    .line 1085
    iget-wide v0, p0, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->mNativeChromeBrowserProvider:J

    invoke-direct {p0, v0, v1, p1, p2}, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->nativeRemoveSearchTermFromAPI(JLjava/lang/String;[Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method private updateBookmarkFromAPI(Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 14

    .prologue
    .line 1028
    invoke-static {p1}, Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkRow;->fromContentValues(Landroid/content/ContentValues;)Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkRow;

    move-result-object v0

    .line 1029
    iget-wide v1, p0, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->mNativeChromeBrowserProvider:J

    iget-object v3, v0, Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkRow;->mUrl:Ljava/lang/String;

    iget-object v4, v0, Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkRow;->mCreated:Ljava/lang/Long;

    iget-object v5, v0, Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkRow;->mIsBookmark:Ljava/lang/Boolean;

    iget-object v6, v0, Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkRow;->mDate:Ljava/lang/Long;

    iget-object v7, v0, Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkRow;->mFavicon:[B

    iget-object v8, v0, Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkRow;->mTitle:Ljava/lang/String;

    iget-object v9, v0, Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkRow;->mVisits:Ljava/lang/Integer;

    iget-wide v10, v0, Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkRow;->mParentId:J

    move-object v0, p0

    move-object/from16 v12, p2

    move-object/from16 v13, p3

    invoke-direct/range {v0 .. v13}, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->nativeUpdateBookmarkFromAPI(JLjava/lang/String;Ljava/lang/Long;Ljava/lang/Boolean;Ljava/lang/Long;[BLjava/lang/String;Ljava/lang/Integer;JLjava/lang/String;[Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method private updateLastModifiedBookmarkFolder(J)V
    .locals 5

    .prologue
    .line 571
    invoke-direct {p0}, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->getLastModifiedBookmarkFolderId()J

    move-result-wide v0

    cmp-long v0, v0, p1

    if-nez v0, :cond_0

    .line 579
    :goto_0
    return-void

    .line 573
    :cond_0
    iput-wide p1, p0, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->mLastModifiedBookmarkFolderId:J

    .line 574
    invoke-virtual {p0}, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 576
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string/jumbo v1, "last_bookmark_folder_id"

    iget-wide v2, p0, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->mLastModifiedBookmarkFolderId:J

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    goto :goto_0
.end method

.method private updateSearchTermFromAPI(Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 8

    .prologue
    .line 1067
    invoke-static {p1}, Lorg/chromium/chrome/browser/ChromeBrowserProvider$SearchRow;->fromContentValues(Landroid/content/ContentValues;)Lorg/chromium/chrome/browser/ChromeBrowserProvider$SearchRow;

    move-result-object v0

    .line 1068
    iget-wide v2, p0, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->mNativeChromeBrowserProvider:J

    iget-object v4, v0, Lorg/chromium/chrome/browser/ChromeBrowserProvider$SearchRow;->mTerm:Ljava/lang/String;

    iget-object v5, v0, Lorg/chromium/chrome/browser/ChromeBrowserProvider$SearchRow;->mDate:Ljava/lang/Long;

    move-object v1, p0

    move-object v6, p2

    move-object v7, p3

    invoke-direct/range {v1 .. v7}, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->nativeUpdateSearchTermFromAPI(JLjava/lang/String;Ljava/lang/Long;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    return v0
.end method


# virtual methods
.method public call(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;
    .locals 9

    .prologue
    const/4 v7, 0x1

    const/4 v0, 0x0

    const/4 v6, 0x0

    .line 691
    invoke-virtual {p0}, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {p0}, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->getReadWritePermissionNameForBookmarkFolders()Ljava/lang/String;

    move-result-object v2

    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v3

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v4

    const-string/jumbo v5, "ChromeBrowserProvider"

    invoke-virtual {v1, v2, v3, v4, v5}, Landroid/content/Context;->enforcePermission(Ljava/lang/String;IILjava/lang/String;)V

    .line 693
    invoke-direct {p0}, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->canHandleContentProviderApiCall()Z

    move-result v1

    if-nez v1, :cond_1

    .line 727
    :cond_0
    :goto_0
    return-object v0

    .line 694
    :cond_1
    if-eqz p1, :cond_0

    if-eqz p3, :cond_0

    .line 696
    new-instance v8, Landroid/os/Bundle;

    invoke-direct {v8}, Landroid/os/Bundle;-><init>()V

    .line 697
    const-string/jumbo v1, "BOOKMARK_NODE_EXISTS"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 698
    const-string/jumbo v0, "result"

    invoke-static {v6}, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->argKey(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p3, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-direct {p0, v2, v3}, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->bookmarkNodeExists(J)Z

    move-result v1

    invoke-virtual {v8, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    :goto_1
    move-object v0, v8

    .line 727
    goto :goto_0

    .line 700
    :cond_2
    const-string/jumbo v1, "CREATE_BOOKMARKS_FOLDER_ONCE"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 701
    const-string/jumbo v0, "result"

    invoke-static {v6}, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->argKey(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p3, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v7}, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->argKey(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p3, v2}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-direct {p0, v1, v2, v3}, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->createBookmarksFolderOnce(Ljava/lang/String;J)J

    move-result-wide v2

    invoke-virtual {v8, v0, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    goto :goto_1

    .line 704
    :cond_3
    const-string/jumbo v1, "GET_EDITABLE_BOOKMARK_FOLDER_HIERARCHY"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 705
    const-string/jumbo v0, "result"

    invoke-direct {p0}, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->getEditableBookmarkFolderHierarchy()Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;

    move-result-object v1

    invoke-virtual {v8, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    goto :goto_1

    .line 706
    :cond_4
    const-string/jumbo v1, "GET_BOOKMARK_NODE"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 707
    const-string/jumbo v0, "result"

    invoke-static {v6}, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->argKey(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p3, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-static {v7}, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->argKey(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p3, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v4

    const/4 v1, 0x2

    invoke-static {v1}, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->argKey(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p3, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v5

    const/4 v1, 0x3

    invoke-static {v1}, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->argKey(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p3, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v6

    const/4 v1, 0x4

    invoke-static {v1}, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->argKey(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p3, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v7

    move-object v1, p0

    invoke-virtual/range {v1 .. v7}, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->getBookmarkNode(JZZZZ)Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;

    move-result-object v1

    invoke-virtual {v8, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    goto :goto_1

    .line 713
    :cond_5
    const-string/jumbo v1, "GET_DEFAULT_BOOKMARK_FOLDER"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 714
    const-string/jumbo v0, "result"

    invoke-direct {p0}, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->getDefaultBookmarkFolder()Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;

    move-result-object v1

    invoke-virtual {v8, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    goto/16 :goto_1

    .line 715
    :cond_6
    const-string/jumbo v1, "GET_MOBILE_BOOKMARKS_FOLDER_ID"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 716
    const-string/jumbo v0, "result"

    invoke-virtual {p0}, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->getMobileBookmarksFolderId()J

    move-result-wide v2

    invoke-virtual {v8, v0, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    goto/16 :goto_1

    .line 717
    :cond_7
    const-string/jumbo v1, "IS_BOOKMARK_IN_MOBILE_BOOKMARKS_BRANCH"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 718
    const-string/jumbo v0, "result"

    invoke-static {v6}, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->argKey(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p3, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-direct {p0, v2, v3}, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->isBookmarkInMobileBookmarksBranch(J)Z

    move-result v1

    invoke-virtual {v8, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    goto/16 :goto_1

    .line 720
    :cond_8
    const-string/jumbo v1, "DELETE_ALL_USER_BOOKMARKS"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 721
    iget-wide v0, p0, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->mNativeChromeBrowserProvider:J

    invoke-direct {p0, v0, v1}, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->nativeRemoveAllUserBookmarks(J)V

    goto/16 :goto_1

    .line 723
    :cond_9
    const-string/jumbo v1, "ChromeBrowserProvider"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "Received invalid method "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method

.method public delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 415
    invoke-direct {p0}, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->canHandleContentProviderApiCall()Z

    move-result v1

    if-nez v1, :cond_1

    .line 459
    :cond_0
    :goto_0
    return v0

    .line 418
    :cond_1
    invoke-static {p1}, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->getContentUriId(Landroid/net/Uri;)J

    move-result-wide v2

    .line 419
    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-eqz v1, :cond_0

    .line 421
    iget-object v0, p0, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->mUriMatcher:Landroid/content/UriMatcher;

    invoke-virtual {v0, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v0

    .line 423
    packed-switch v0, :pswitch_data_0

    .line 456
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "ChromeBrowserProvider: delete - unknown URL "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 425
    :pswitch_0
    iget-wide v0, p0, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->mNativeChromeBrowserProvider:J

    invoke-direct {p0, v0, v1, v2, v3}, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->nativeRemoveBookmark(JJ)I

    move-result v0

    .line 458
    :goto_1
    if-eqz v0, :cond_0

    invoke-direct {p0, p1}, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->notifyChange(Landroid/net/Uri;)V

    goto :goto_0

    .line 428
    :pswitch_1
    invoke-static {v2, v3, p2}, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->buildWhereClause(JLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, p3}, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->removeBookmarkFromAPI(Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    goto :goto_1

    .line 432
    :pswitch_2
    invoke-direct {p0, p2, p3}, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->removeBookmarkFromAPI(Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    goto :goto_1

    .line 435
    :pswitch_3
    invoke-static {v2, v3, p2}, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->buildWhereClause(JLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, p3}, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->removeSearchFromAPI(Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    goto :goto_1

    .line 439
    :pswitch_4
    invoke-direct {p0, p2, p3}, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->removeSearchFromAPI(Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    goto :goto_1

    .line 442
    :pswitch_5
    invoke-direct {p0, p2, p3}, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->removeHistoryFromAPI(Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    goto :goto_1

    .line 445
    :pswitch_6
    invoke-static {v2, v3, p2}, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->buildWhereClause(JLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, p3}, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->removeHistoryFromAPI(Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    goto :goto_1

    .line 449
    :pswitch_7
    invoke-static {p2}, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->buildBookmarkWhereClause(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, p3}, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->removeBookmarkFromAPI(Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    goto :goto_1

    .line 452
    :pswitch_8
    invoke-static {v2, v3, p2}, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->buildBookmarkWhereClause(JLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, p3}, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->removeBookmarkFromAPI(Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    goto :goto_1

    .line 423
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_4
        :pswitch_3
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method

.method protected ensureNativeChromeLoadedOnUIThread()Z
    .locals 2

    .prologue
    .line 1247
    invoke-virtual {p0}, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->isNativeSideInitialized()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 1249
    :goto_0
    return v0

    .line 1248
    :cond_0
    invoke-direct {p0}, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->nativeInit()J

    move-result-wide v0

    iput-wide v0, p0, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->mNativeChromeBrowserProvider:J

    .line 1249
    invoke-virtual {p0}, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->isNativeSideInitialized()Z

    move-result v0

    goto :goto_0
.end method

.method protected finalize()V
    .locals 1

    .prologue
    .line 1256
    :try_start_0
    new-instance v0, Lorg/chromium/chrome/browser/ChromeBrowserProvider$2;

    invoke-direct {v0, p0}, Lorg/chromium/chrome/browser/ChromeBrowserProvider$2;-><init>(Lorg/chromium/chrome/browser/ChromeBrowserProvider;)V

    invoke-static {v0}, Lorg/chromium/base/ThreadUtils;->runOnUiThreadBlocking(Ljava/lang/Runnable;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1263
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    .line 1264
    return-void

    .line 1263
    :catchall_0
    move-exception v0

    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    throw v0
.end method

.method protected getBookmarkNode(JZZZZ)Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;
    .locals 9

    .prologue
    .line 622
    if-eqz p3, :cond_3

    invoke-virtual {p0}, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->getMobileBookmarksFolderId()J

    move-result-wide v0

    cmp-long v0, p1, v0

    if-nez v0, :cond_3

    invoke-virtual {p0}, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lorg/chromium/sync/notifier/SyncStatusHelper;->get(Landroid/content/Context;)Lorg/chromium/sync/notifier/SyncStatusHelper;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/sync/notifier/SyncStatusHelper;->isSyncEnabled()Z

    move-result v0

    if-nez v0, :cond_3

    .line 624
    const/4 p3, 0x0

    move v6, p3

    .line 627
    :goto_0
    iget-wide v2, p0, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->mNativeChromeBrowserProvider:J

    move-object v1, p0

    move-wide v4, p1

    move v7, p4

    invoke-direct/range {v1 .. v7}, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->nativeGetBookmarkNode(JJZZ)Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;

    move-result-object v1

    .line 629
    if-nez p5, :cond_0

    if-nez p6, :cond_0

    move-object v0, v1

    .line 638
    :goto_1
    return-object v0

    .line 633
    :cond_0
    invoke-virtual {v1}, Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;->parent()Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v1}, Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;->parent()Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;

    move-result-object v0

    invoke-direct {p0, v0, p5, p6}, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->populateNodeImages(Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;ZZ)V

    .line 634
    :cond_1
    invoke-virtual {v1}, Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;->children()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;

    .line 635
    invoke-direct {p0, v0, p5, p6}, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->populateNodeImages(Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;ZZ)V

    goto :goto_2

    :cond_2
    move-object v0, v1

    .line 638
    goto :goto_1

    :cond_3
    move v6, p3

    goto :goto_0
.end method

.method protected getMobileBookmarksFolderId()J
    .locals 2

    .prologue
    .line 674
    invoke-direct {p0}, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->getMobileBookmarksFolder()Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;

    move-result-object v0

    .line 675
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;->id()J

    move-result-wide v0

    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, -0x1

    goto :goto_0
.end method

.method public getType(Landroid/net/Uri;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 526
    invoke-direct {p0}, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->ensureUriMatcherInitialized()V

    .line 527
    iget-object v0, p0, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->mUriMatcher:Landroid/content/UriMatcher;

    invoke-virtual {v0, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v0

    .line 528
    packed-switch v0, :pswitch_data_0

    .line 544
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "ChromeBrowserProvider: getType - unknown URL "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 531
    :pswitch_0
    const-string/jumbo v0, "vnd.android.cursor.dir/bookmark"

    .line 542
    :goto_0
    return-object v0

    .line 534
    :pswitch_1
    const-string/jumbo v0, "vnd.android.cursor.item/bookmark"

    goto :goto_0

    .line 536
    :pswitch_2
    const-string/jumbo v0, "vnd.android.cursor.dir/searches"

    goto :goto_0

    .line 538
    :pswitch_3
    const-string/jumbo v0, "vnd.android.cursor.item/searches"

    goto :goto_0

    .line 540
    :pswitch_4
    const-string/jumbo v0, "vnd.android.cursor.dir/browser-history"

    goto :goto_0

    .line 542
    :pswitch_5
    const-string/jumbo v0, "vnd.android.cursor.item/browser-history"

    goto :goto_0

    .line 528
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const/4 v2, 0x0

    .line 382
    invoke-direct {p0}, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->canHandleContentProviderApiCall()Z

    move-result v0

    if-nez v0, :cond_0

    move-object v0, v2

    .line 410
    :goto_0
    return-object v0

    .line 384
    :cond_0
    iget-object v0, p0, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->mUriMatcher:Landroid/content/UriMatcher;

    invoke-virtual {v0, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v0

    .line 385
    packed-switch v0, :pswitch_data_0

    .line 405
    :pswitch_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "ChromeBrowserProvider: insert - unknown URL "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 389
    :pswitch_1
    invoke-direct {p0, p2}, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->addBookmark(Landroid/content/ContentValues;)J

    move-result-wide v0

    .line 390
    const-wide/16 v4, -0x1

    cmp-long v3, v0, v4

    if-nez v3, :cond_1

    move-object v0, v2

    goto :goto_0

    .line 393
    :pswitch_2
    const-string/jumbo v0, "bookmark"

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p2, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 397
    :pswitch_3
    invoke-direct {p0, p2}, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->addBookmarkFromAPI(Landroid/content/ContentValues;)J

    move-result-wide v0

    .line 398
    cmp-long v3, v0, v4

    if-nez v3, :cond_1

    move-object v0, v2

    goto :goto_0

    .line 401
    :pswitch_4
    invoke-direct {p0, p2}, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->addSearchTermFromAPI(Landroid/content/ContentValues;)J

    move-result-wide v0

    .line 402
    cmp-long v3, v0, v4

    if-nez v3, :cond_1

    move-object v0, v2

    goto :goto_0

    .line 408
    :cond_1
    invoke-static {p1, v0, v1}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    .line 409
    invoke-direct {p0, v0}, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->notifyChange(Landroid/net/Uri;)V

    goto :goto_0

    .line 385
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_3
        :pswitch_0
        :pswitch_4
        :pswitch_0
        :pswitch_3
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method protected isNativeSideInitialized()Z
    .locals 4

    .prologue
    .line 1218
    iget-wide v0, p0, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->mNativeChromeBrowserProvider:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onCreate()Z
    .locals 1

    .prologue
    .line 241
    invoke-virtual {p0}, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    .line 242
    const/4 v0, 0x1

    return v0
.end method

.method public query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 7

    .prologue
    const/4 v0, 0x0

    const/4 v6, 0x0

    .line 325
    invoke-direct {p0}, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->canHandleContentProviderApiCall()Z

    move-result v1

    if-nez v1, :cond_1

    .line 377
    :cond_0
    :goto_0
    return-object v0

    .line 328
    :cond_1
    invoke-static {p1}, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->getContentUriId(Landroid/net/Uri;)J

    move-result-wide v2

    .line 329
    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-eqz v1, :cond_0

    .line 331
    iget-object v0, p0, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->mUriMatcher:Landroid/content/UriMatcher;

    invoke-virtual {v0, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v0

    .line 332
    packed-switch v0, :pswitch_data_0

    .line 371
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "ChromeBrowserProvider: query - unknown URL uri = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 335
    :pswitch_0
    const/4 v0, 0x1

    invoke-direct {p0, p3, p4, p5, v0}, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->getBookmarkHistorySuggestions(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Z)Landroid/database/Cursor;

    move-result-object v0

    .line 373
    :goto_1
    if-nez v0, :cond_2

    .line 374
    new-instance v0, Landroid/database/MatrixCursor;

    new-array v1, v6, [Ljava/lang/String;

    invoke-direct {v0, v1}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 376
    :cond_2
    invoke-virtual {p0}, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Landroid/database/Cursor;->setNotificationUri(Landroid/content/ContentResolver;Landroid/net/Uri;)V

    goto :goto_0

    .line 338
    :pswitch_1
    invoke-direct {p0, p3, p4, p5, v6}, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->getBookmarkHistorySuggestions(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Z)Landroid/database/Cursor;

    move-result-object v0

    goto :goto_1

    .line 341
    :pswitch_2
    invoke-direct {p0, p2, p3, p4, p5}, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->queryBookmarkFromAPI([Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    goto :goto_1

    .line 344
    :pswitch_3
    invoke-static {v2, v3, p3}, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->buildWhereClause(JLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p2, v0, p4, p5}, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->queryBookmarkFromAPI([Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    goto :goto_1

    .line 348
    :pswitch_4
    invoke-direct {p0, p2, p3, p4, p5}, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->querySearchTermFromAPI([Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    goto :goto_1

    .line 351
    :pswitch_5
    invoke-static {v2, v3, p3}, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->buildWhereClause(JLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p2, v0, p4, p5}, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->querySearchTermFromAPI([Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    goto :goto_1

    .line 355
    :pswitch_6
    invoke-static {p3}, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->buildHistoryWhereClause(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p2, v0, p4, p5}, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->queryBookmarkFromAPI([Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    goto :goto_1

    .line 359
    :pswitch_7
    invoke-static {v2, v3, p3}, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->buildHistoryWhereClause(JLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p2, v0, p4, p5}, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->queryBookmarkFromAPI([Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    goto :goto_1

    .line 363
    :pswitch_8
    invoke-static {p3}, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->buildBookmarkWhereClause(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p2, v0, p4, p5}, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->queryBookmarkFromAPI([Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    goto :goto_1

    .line 367
    :pswitch_9
    invoke-static {v2, v3, p3}, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->buildBookmarkWhereClause(JLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p2, v0, p4, p5}, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->queryBookmarkFromAPI([Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    goto :goto_1

    .line 332
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 10

    .prologue
    const/4 v0, 0x0

    .line 464
    invoke-direct {p0}, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->canHandleContentProviderApiCall()Z

    move-result v1

    if-nez v1, :cond_1

    .line 521
    :cond_0
    :goto_0
    return v0

    .line 467
    :cond_1
    invoke-static {p1}, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->getContentUriId(Landroid/net/Uri;)J

    move-result-wide v4

    .line 468
    const-wide/16 v2, 0x0

    cmp-long v1, v4, v2

    if-eqz v1, :cond_0

    .line 470
    iget-object v0, p0, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->mUriMatcher:Landroid/content/UriMatcher;

    invoke-virtual {v0, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v0

    .line 472
    packed-switch v0, :pswitch_data_0

    .line 518
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "ChromeBrowserProvider: update - unknown URL "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 474
    :pswitch_0
    const/4 v6, 0x0

    .line 475
    const-string/jumbo v0, "url"

    invoke-virtual {p2, v0}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 476
    const-string/jumbo v0, "url"

    invoke-virtual {p2, v0}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 478
    :cond_2
    const-string/jumbo v0, "title"

    invoke-virtual {p2, v0}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 479
    const-wide/16 v8, -0x1

    .line 480
    const-string/jumbo v0, "parentId"

    invoke-virtual {p2, v0}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 481
    const-string/jumbo v0, "parentId"

    invoke-virtual {p2, v0}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    .line 483
    :cond_3
    iget-wide v2, p0, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->mNativeChromeBrowserProvider:J

    move-object v1, p0

    invoke-direct/range {v1 .. v9}, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->nativeUpdateBookmark(JJLjava/lang/String;Ljava/lang/String;J)I

    move-result v0

    .line 485
    invoke-direct {p0, v8, v9}, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->updateLastModifiedBookmarkFolder(J)V

    .line 520
    :goto_1
    if-eqz v0, :cond_0

    invoke-direct {p0, p1}, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->notifyChange(Landroid/net/Uri;)V

    goto :goto_0

    .line 488
    :pswitch_1
    invoke-static {v4, v5, p3}, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->buildWhereClause(JLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p2, v0, p4}, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->updateBookmarkFromAPI(Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    goto :goto_1

    .line 492
    :pswitch_2
    invoke-direct {p0, p2, p3, p4}, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->updateBookmarkFromAPI(Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    goto :goto_1

    .line 495
    :pswitch_3
    invoke-static {v4, v5, p3}, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->buildWhereClause(JLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p2, v0, p4}, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->updateSearchTermFromAPI(Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    goto :goto_1

    .line 499
    :pswitch_4
    invoke-direct {p0, p2, p3, p4}, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->updateSearchTermFromAPI(Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    goto :goto_1

    .line 502
    :pswitch_5
    invoke-static {p3}, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->buildHistoryWhereClause(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p2, v0, p4}, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->updateBookmarkFromAPI(Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    goto :goto_1

    .line 506
    :pswitch_6
    invoke-static {v4, v5, p3}, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->buildHistoryWhereClause(JLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p2, v0, p4}, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->updateBookmarkFromAPI(Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    goto :goto_1

    .line 510
    :pswitch_7
    invoke-static {p3}, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->buildBookmarkWhereClause(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p2, v0, p4}, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->updateBookmarkFromAPI(Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    goto :goto_1

    .line 514
    :pswitch_8
    invoke-static {v4, v5, p3}, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->buildBookmarkWhereClause(JLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p2, v0, p4}, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->updateBookmarkFromAPI(Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    goto :goto_1

    .line 472
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_4
        :pswitch_3
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method
