.class public Lorg/chromium/chrome/browser/autofill/AutofillPopupBridge;
.super Ljava/lang/Object;
.source "AutofillPopupBridge.java"

# interfaces
.implements Lorg/chromium/ui/autofill/AutofillPopup$AutofillPopupDelegate;


# annotations
.annotation runtime Lorg/chromium/base/JNINamespace;
.end annotation


# instance fields
.field private final mAutofillPopup:Lorg/chromium/ui/autofill/AutofillPopup;

.field private final mNativeAutofillPopup:J


# direct methods
.method public constructor <init>(JLorg/chromium/ui/base/WindowAndroid;Lorg/chromium/ui/base/ViewAndroidDelegate;)V
    .locals 3

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    iput-wide p1, p0, Lorg/chromium/chrome/browser/autofill/AutofillPopupBridge;->mNativeAutofillPopup:J

    .line 30
    invoke-virtual {p3}, Lorg/chromium/ui/base/WindowAndroid;->getActivity()Ljava/lang/ref/WeakReference;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    .line 31
    if-nez v0, :cond_0

    .line 32
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/chromium/chrome/browser/autofill/AutofillPopupBridge;->mAutofillPopup:Lorg/chromium/ui/autofill/AutofillPopup;

    .line 35
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v1, Lorg/chromium/chrome/browser/autofill/AutofillPopupBridge$1;

    invoke-direct {v1, p0}, Lorg/chromium/chrome/browser/autofill/AutofillPopupBridge$1;-><init>(Lorg/chromium/chrome/browser/autofill/AutofillPopupBridge;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 44
    :goto_0
    return-void

    .line 42
    :cond_0
    new-instance v1, Lorg/chromium/ui/autofill/AutofillPopup;

    invoke-direct {v1, v0, p4, p0}, Lorg/chromium/ui/autofill/AutofillPopup;-><init>(Landroid/content/Context;Lorg/chromium/ui/base/ViewAndroidDelegate;Lorg/chromium/ui/autofill/AutofillPopup$AutofillPopupDelegate;)V

    iput-object v1, p0, Lorg/chromium/chrome/browser/autofill/AutofillPopupBridge;->mAutofillPopup:Lorg/chromium/ui/autofill/AutofillPopup;

    goto :goto_0
.end method

.method private static addToAutofillSuggestionArray([Lorg/chromium/ui/autofill/AutofillSuggestion;ILjava/lang/String;Ljava/lang/String;I)V
    .locals 1

    .prologue
    .line 109
    new-instance v0, Lorg/chromium/ui/autofill/AutofillSuggestion;

    invoke-direct {v0, p2, p3, p4}, Lorg/chromium/ui/autofill/AutofillSuggestion;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    aput-object v0, p0, p1

    .line 110
    return-void
.end method

.method private static create(JLorg/chromium/ui/base/WindowAndroid;Lorg/chromium/ui/base/ViewAndroid;)Lorg/chromium/chrome/browser/autofill/AutofillPopupBridge;
    .locals 2

    .prologue
    .line 49
    new-instance v0, Lorg/chromium/chrome/browser/autofill/AutofillPopupBridge;

    invoke-virtual {p3}, Lorg/chromium/ui/base/ViewAndroid;->getViewAndroidDelegate()Lorg/chromium/ui/base/ViewAndroidDelegate;

    move-result-object v1

    invoke-direct {v0, p0, p1, p2, v1}, Lorg/chromium/chrome/browser/autofill/AutofillPopupBridge;-><init>(JLorg/chromium/ui/base/WindowAndroid;Lorg/chromium/ui/base/ViewAndroidDelegate;)V

    return-object v0
.end method

.method private static createAutofillSuggestionArray(I)[Lorg/chromium/ui/autofill/AutofillSuggestion;
    .locals 1

    .prologue
    .line 96
    new-array v0, p0, [Lorg/chromium/ui/autofill/AutofillSuggestion;

    return-object v0
.end method

.method private hide()V
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lorg/chromium/chrome/browser/autofill/AutofillPopupBridge;->mAutofillPopup:Lorg/chromium/ui/autofill/AutofillPopup;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/chromium/chrome/browser/autofill/AutofillPopupBridge;->mAutofillPopup:Lorg/chromium/ui/autofill/AutofillPopup;

    invoke-virtual {v0}, Lorg/chromium/ui/autofill/AutofillPopup;->hide()V

    .line 69
    :cond_0
    return-void
.end method

.method private native nativePopupDismissed(J)V
.end method

.method private native nativeSuggestionSelected(JI)V
.end method

.method private setAnchorRect(FFFF)V
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Lorg/chromium/chrome/browser/autofill/AutofillPopupBridge;->mAutofillPopup:Lorg/chromium/ui/autofill/AutofillPopup;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/chromium/chrome/browser/autofill/AutofillPopupBridge;->mAutofillPopup:Lorg/chromium/ui/autofill/AutofillPopup;

    invoke-virtual {v0, p1, p2, p3, p4}, Lorg/chromium/ui/autofill/AutofillPopup;->setAnchorRect(FFFF)V

    .line 90
    :cond_0
    return-void
.end method

.method private show([Lorg/chromium/ui/autofill/AutofillSuggestion;Z)V
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lorg/chromium/chrome/browser/autofill/AutofillPopupBridge;->mAutofillPopup:Lorg/chromium/ui/autofill/AutofillPopup;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/chromium/chrome/browser/autofill/AutofillPopupBridge;->mAutofillPopup:Lorg/chromium/ui/autofill/AutofillPopup;

    invoke-virtual {v0, p1, p2}, Lorg/chromium/ui/autofill/AutofillPopup;->filterAndShow([Lorg/chromium/ui/autofill/AutofillSuggestion;Z)V

    .line 78
    :cond_0
    return-void
.end method


# virtual methods
.method public dismissed()V
    .locals 2

    .prologue
    .line 55
    iget-wide v0, p0, Lorg/chromium/chrome/browser/autofill/AutofillPopupBridge;->mNativeAutofillPopup:J

    invoke-direct {p0, v0, v1}, Lorg/chromium/chrome/browser/autofill/AutofillPopupBridge;->nativePopupDismissed(J)V

    .line 56
    return-void
.end method

.method public suggestionSelected(I)V
    .locals 2

    .prologue
    .line 60
    iget-wide v0, p0, Lorg/chromium/chrome/browser/autofill/AutofillPopupBridge;->mNativeAutofillPopup:J

    invoke-direct {p0, v0, v1, p1}, Lorg/chromium/chrome/browser/autofill/AutofillPopupBridge;->nativeSuggestionSelected(JI)V

    .line 61
    return-void
.end method
