.class public Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkItem;
.super Ljava/lang/Object;
.source "BookmarksBridge.java"


# instance fields
.field private final mId:Lorg/chromium/components/bookmarks/BookmarkId;

.field private final mIsEditable:Z

.field private final mIsFolder:Z

.field private final mIsManaged:Z

.field private final mParentId:Lorg/chromium/components/bookmarks/BookmarkId;

.field private final mTitle:Ljava/lang/String;

.field private final mUrl:Ljava/lang/String;


# direct methods
.method private constructor <init>(Lorg/chromium/components/bookmarks/BookmarkId;Ljava/lang/String;Ljava/lang/String;ZLorg/chromium/components/bookmarks/BookmarkId;ZZ)V
    .locals 0

    .prologue
    .line 670
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 671
    iput-object p1, p0, Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkItem;->mId:Lorg/chromium/components/bookmarks/BookmarkId;

    .line 672
    iput-object p2, p0, Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkItem;->mTitle:Ljava/lang/String;

    .line 673
    iput-object p3, p0, Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkItem;->mUrl:Ljava/lang/String;

    .line 674
    iput-boolean p4, p0, Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkItem;->mIsFolder:Z

    .line 675
    iput-object p5, p0, Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkItem;->mParentId:Lorg/chromium/components/bookmarks/BookmarkId;

    .line 676
    iput-boolean p6, p0, Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkItem;->mIsEditable:Z

    .line 677
    iput-boolean p7, p0, Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkItem;->mIsManaged:Z

    .line 678
    return-void
.end method

.method synthetic constructor <init>(Lorg/chromium/components/bookmarks/BookmarkId;Ljava/lang/String;Ljava/lang/String;ZLorg/chromium/components/bookmarks/BookmarkId;ZZLorg/chromium/chrome/browser/BookmarksBridge$1;)V
    .locals 0

    .prologue
    .line 659
    invoke-direct/range {p0 .. p7}, Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkItem;-><init>(Lorg/chromium/components/bookmarks/BookmarkId;Ljava/lang/String;Ljava/lang/String;ZLorg/chromium/components/bookmarks/BookmarkId;ZZ)V

    return-void
.end method


# virtual methods
.method public getId()Lorg/chromium/components/bookmarks/BookmarkId;
    .locals 1

    .prologue
    .line 692
    iget-object v0, p0, Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkItem;->mId:Lorg/chromium/components/bookmarks/BookmarkId;

    return-object v0
.end method

.method public getParentId()Lorg/chromium/components/bookmarks/BookmarkId;
    .locals 1

    .prologue
    .line 702
    iget-object v0, p0, Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkItem;->mParentId:Lorg/chromium/components/bookmarks/BookmarkId;

    return-object v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 682
    iget-object v0, p0, Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkItem;->mTitle:Ljava/lang/String;

    return-object v0
.end method

.method public getUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 687
    iget-object v0, p0, Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkItem;->mUrl:Ljava/lang/String;

    return-object v0
.end method

.method public isEditable()Z
    .locals 1

    .prologue
    .line 707
    iget-boolean v0, p0, Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkItem;->mIsEditable:Z

    return v0
.end method

.method public isFolder()Z
    .locals 1

    .prologue
    .line 697
    iget-boolean v0, p0, Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkItem;->mIsFolder:Z

    return v0
.end method

.method public isManaged()Z
    .locals 1

    .prologue
    .line 712
    iget-boolean v0, p0, Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkItem;->mIsManaged:Z

    return v0
.end method
