.class public Lorg/chromium/chrome/browser/infobar/AnimationHelper;
.super Ljava/lang/Object;
.source "AnimationHelper.java"

# interfaces
.implements Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;


# static fields
.field static final synthetic $assertionsDisabled:Z

.field public static final ANIMATION_TYPE_BOUNDARY:I = 0x3

.field public static final ANIMATION_TYPE_HIDE:I = 0x2

.field public static final ANIMATION_TYPE_SHOW:I = 0x0

.field public static final ANIMATION_TYPE_SWAP:I = 0x1


# instance fields
.field private mAnimationStarted:Z

.field private final mAnimationType:I

.field private final mAnimatorSet:Landroid/animation/AnimatorSet;

.field private final mContainer:Lorg/chromium/chrome/browser/infobar/InfoBarContainer;

.field private final mInfoBar:Lorg/chromium/chrome/browser/infobar/InfoBar;

.field private final mLinearLayout:Landroid/widget/LinearLayout;

.field private final mTargetWrapperView:Lorg/chromium/chrome/browser/infobar/ContentWrapperView;

.field private final mToShow:Landroid/view/View;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 40
    const-class v0, Lorg/chromium/chrome/browser/infobar/AnimationHelper;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/chromium/chrome/browser/infobar/AnimationHelper;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lorg/chromium/chrome/browser/infobar/InfoBarContainer;Lorg/chromium/chrome/browser/infobar/ContentWrapperView;Lorg/chromium/chrome/browser/infobar/InfoBar;Landroid/view/View;I)V
    .locals 2

    .prologue
    .line 69
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 70
    iput-object p1, p0, Lorg/chromium/chrome/browser/infobar/AnimationHelper;->mContainer:Lorg/chromium/chrome/browser/infobar/InfoBarContainer;

    .line 71
    invoke-virtual {p1}, Lorg/chromium/chrome/browser/infobar/InfoBarContainer;->getLinearLayout()Landroid/widget/LinearLayout;

    move-result-object v0

    iput-object v0, p0, Lorg/chromium/chrome/browser/infobar/AnimationHelper;->mLinearLayout:Landroid/widget/LinearLayout;

    .line 72
    iput-object p3, p0, Lorg/chromium/chrome/browser/infobar/AnimationHelper;->mInfoBar:Lorg/chromium/chrome/browser/infobar/InfoBar;

    .line 73
    iput-object p2, p0, Lorg/chromium/chrome/browser/infobar/AnimationHelper;->mTargetWrapperView:Lorg/chromium/chrome/browser/infobar/ContentWrapperView;

    .line 74
    new-instance v0, Landroid/animation/AnimatorSet;

    invoke-direct {v0}, Landroid/animation/AnimatorSet;-><init>()V

    iput-object v0, p0, Lorg/chromium/chrome/browser/infobar/AnimationHelper;->mAnimatorSet:Landroid/animation/AnimatorSet;

    .line 75
    iput p5, p0, Lorg/chromium/chrome/browser/infobar/AnimationHelper;->mAnimationType:I

    .line 76
    iput-object p4, p0, Lorg/chromium/chrome/browser/infobar/AnimationHelper;->mToShow:Landroid/view/View;

    .line 77
    sget-boolean v0, Lorg/chromium/chrome/browser/infobar/AnimationHelper;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lorg/chromium/chrome/browser/infobar/AnimationHelper;->mLinearLayout:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lorg/chromium/chrome/browser/infobar/AnimationHelper;->mTargetWrapperView:Lorg/chromium/chrome/browser/infobar/ContentWrapperView;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->indexOfChild(Landroid/view/View;)I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 78
    :cond_0
    return-void
.end method

.method static synthetic access$000(Lorg/chromium/chrome/browser/infobar/AnimationHelper;)Lorg/chromium/chrome/browser/infobar/ContentWrapperView;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lorg/chromium/chrome/browser/infobar/AnimationHelper;->mTargetWrapperView:Lorg/chromium/chrome/browser/infobar/ContentWrapperView;

    return-object v0
.end method

.method static synthetic access$100(Lorg/chromium/chrome/browser/infobar/AnimationHelper;)Lorg/chromium/chrome/browser/infobar/InfoBarContainer;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lorg/chromium/chrome/browser/infobar/AnimationHelper;->mContainer:Lorg/chromium/chrome/browser/infobar/InfoBarContainer;

    return-object v0
.end method

.method static synthetic access$200(Lorg/chromium/chrome/browser/infobar/AnimationHelper;)Landroid/view/View;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lorg/chromium/chrome/browser/infobar/AnimationHelper;->mToShow:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$300(Lorg/chromium/chrome/browser/infobar/AnimationHelper;)I
    .locals 1

    .prologue
    .line 40
    iget v0, p0, Lorg/chromium/chrome/browser/infobar/AnimationHelper;->mAnimationType:I

    return v0
.end method

.method static synthetic access$400(Lorg/chromium/chrome/browser/infobar/AnimationHelper;)Lorg/chromium/chrome/browser/infobar/InfoBar;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lorg/chromium/chrome/browser/infobar/AnimationHelper;->mInfoBar:Lorg/chromium/chrome/browser/infobar/InfoBar;

    return-object v0
.end method

.method private continueAnimation()V
    .locals 18

    .prologue
    .line 127
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lorg/chromium/chrome/browser/infobar/AnimationHelper;->mAnimationStarted:Z

    if-eqz v2, :cond_0

    .line 235
    :goto_0
    return-void

    .line 128
    :cond_0
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lorg/chromium/chrome/browser/infobar/AnimationHelper;->mAnimationStarted:Z

    .line 130
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/chromium/chrome/browser/infobar/AnimationHelper;->mLinearLayout:Landroid/widget/LinearLayout;

    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/chromium/chrome/browser/infobar/AnimationHelper;->mTargetWrapperView:Lorg/chromium/chrome/browser/infobar/ContentWrapperView;

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->indexOfChild(Landroid/view/View;)I

    move-result v8

    .line 131
    sget-boolean v2, Lorg/chromium/chrome/browser/infobar/AnimationHelper;->$assertionsDisabled:Z

    if-nez v2, :cond_1

    const/4 v2, -0x1

    if-ne v8, v2, :cond_1

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    .line 133
    :cond_1
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 134
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/chromium/chrome/browser/infobar/AnimationHelper;->mTargetWrapperView:Lorg/chromium/chrome/browser/infobar/ContentWrapperView;

    invoke-virtual {v2, v9}, Lorg/chromium/chrome/browser/infobar/ContentWrapperView;->getAnimationsForTransition(Ljava/util/ArrayList;)V

    .line 137
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/chromium/chrome/browser/infobar/AnimationHelper;->mTargetWrapperView:Lorg/chromium/chrome/browser/infobar/ContentWrapperView;

    invoke-virtual {v2}, Lorg/chromium/chrome/browser/infobar/ContentWrapperView;->getTransitionHeightDifference()I

    move-result v4

    .line 138
    const/4 v3, 0x0

    .line 139
    const/4 v2, 0x0

    .line 140
    const/4 v6, 0x0

    .line 141
    if-ltz v4, :cond_2

    move v3, v4

    .line 151
    :goto_1
    const/4 v5, 0x0

    move/from16 v16, v5

    move v5, v2

    move/from16 v2, v16

    move/from16 v17, v6

    move v6, v3

    move/from16 v3, v17

    :goto_2
    move-object/from16 v0, p0

    iget-object v7, v0, Lorg/chromium/chrome/browser/infobar/AnimationHelper;->mLinearLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v7}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v7

    if-ge v2, v7, :cond_8

    .line 152
    move-object/from16 v0, p0

    iget-object v7, v0, Lorg/chromium/chrome/browser/infobar/AnimationHelper;->mLinearLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v7, v2}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v10

    .line 156
    invoke-virtual {v10}, Landroid/view/View;->getHeight()I

    move-result v11

    .line 157
    if-ne v2, v8, :cond_3

    move v7, v4

    :goto_3
    add-int v12, v11, v7

    .line 160
    add-int v7, v6, v11

    .line 161
    add-int v13, v5, v12

    .line 163
    if-ne v6, v5, :cond_4

    if-ne v7, v13, :cond_4

    .line 165
    invoke-virtual {v10, v5}, Landroid/view/View;->setTop(I)V

    .line 166
    invoke-virtual {v10, v13}, Landroid/view/View;->setBottom(I)V

    .line 167
    int-to-float v7, v5

    invoke-virtual {v10, v7}, Landroid/view/View;->setY(F)V

    .line 168
    const/4 v7, 0x0

    invoke-virtual {v10, v7}, Landroid/view/View;->setTranslationY(F)V

    .line 199
    :goto_4
    add-int/2addr v6, v11

    .line 200
    add-int/2addr v5, v12

    .line 201
    add-int/2addr v3, v12

    .line 151
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 148
    :cond_2
    neg-int v2, v4

    goto :goto_1

    .line 157
    :cond_3
    const/4 v7, 0x0

    goto :goto_3

    .line 171
    :cond_4
    if-lt v6, v5, :cond_6

    .line 175
    if-le v6, v5, :cond_5

    .line 177
    const/4 v7, 0x1

    .line 183
    :goto_5
    if-eqz v7, :cond_7

    .line 184
    invoke-virtual {v10, v5}, Landroid/view/View;->setTop(I)V

    .line 185
    invoke-virtual {v10, v13}, Landroid/view/View;->setBottom(I)V

    .line 186
    int-to-float v7, v4

    invoke-virtual {v10, v7}, Landroid/view/View;->setTranslationY(F)V

    .line 187
    add-int v7, v5, v4

    int-to-float v7, v7

    invoke-virtual {v10, v7}, Landroid/view/View;->setY(F)V

    .line 188
    const-string/jumbo v7, "translationY"

    const/4 v13, 0x2

    new-array v13, v13, [F

    const/4 v14, 0x0

    int-to-float v15, v4

    aput v15, v13, v14

    const/4 v14, 0x1

    const/4 v15, 0x0

    aput v15, v13, v14

    invoke-static {v7, v13}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v7

    .line 195
    :goto_6
    const/4 v13, 0x1

    new-array v13, v13, [Landroid/animation/PropertyValuesHolder;

    const/4 v14, 0x0

    aput-object v7, v13, v14

    invoke-static {v10, v13}, Landroid/animation/ObjectAnimator;->ofPropertyValuesHolder(Ljava/lang/Object;[Landroid/animation/PropertyValuesHolder;)Landroid/animation/ObjectAnimator;

    move-result-object v7

    invoke-virtual {v9, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 179
    :cond_5
    if-le v13, v7, :cond_6

    const/4 v7, 0x1

    goto :goto_5

    :cond_6
    const/4 v7, 0x0

    goto :goto_5

    .line 191
    :cond_7
    const-string/jumbo v7, "translationY"

    const/4 v13, 0x2

    new-array v13, v13, [F

    const/4 v14, 0x0

    const/4 v15, 0x0

    aput v15, v13, v14

    const/4 v14, 0x1

    neg-int v15, v4

    int-to-float v15, v15

    aput v15, v13, v14

    invoke-static {v7, v13}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v7

    goto :goto_6

    .line 206
    :cond_8
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/chromium/chrome/browser/infobar/AnimationHelper;->mLinearLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->getTop()I

    move-result v2

    .line 207
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/chromium/chrome/browser/infobar/AnimationHelper;->mLinearLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v4}, Landroid/widget/LinearLayout;->getBottom()I

    move-result v4

    sub-int v3, v4, v3

    .line 208
    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 209
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/chromium/chrome/browser/infobar/AnimationHelper;->mLinearLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v2}, Landroid/widget/LinearLayout;->setTop(I)V

    .line 212
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/chromium/chrome/browser/infobar/AnimationHelper;->mAnimatorSet:Landroid/animation/AnimatorSet;

    new-instance v3, Lorg/chromium/chrome/browser/infobar/AnimationHelper$1;

    move-object/from16 v0, p0

    invoke-direct {v3, v0}, Lorg/chromium/chrome/browser/infobar/AnimationHelper$1;-><init>(Lorg/chromium/chrome/browser/infobar/AnimationHelper;)V

    invoke-virtual {v2, v3}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 231
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/chromium/chrome/browser/infobar/AnimationHelper;->mAnimatorSet:Landroid/animation/AnimatorSet;

    invoke-virtual {v2, v9}, Landroid/animation/AnimatorSet;->playTogether(Ljava/util/Collection;)V

    .line 232
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/chromium/chrome/browser/infobar/AnimationHelper;->mAnimatorSet:Landroid/animation/AnimatorSet;

    const-wide/16 v4, 0xfa

    invoke-virtual {v2, v4, v5}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    .line 233
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/chromium/chrome/browser/infobar/AnimationHelper;->mAnimatorSet:Landroid/animation/AnimatorSet;

    new-instance v3, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v3}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    invoke-virtual {v2, v3}, Landroid/animation/AnimatorSet;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 234
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/chromium/chrome/browser/infobar/AnimationHelper;->mAnimatorSet:Landroid/animation/AnimatorSet;

    invoke-virtual {v2}, Landroid/animation/AnimatorSet;->start()V

    goto/16 :goto_0
.end method


# virtual methods
.method public getAnimationType()I
    .locals 1

    .prologue
    .line 114
    iget v0, p0, Lorg/chromium/chrome/browser/infobar/AnimationHelper;->mAnimationType:I

    return v0
.end method

.method public getTarget()Lorg/chromium/chrome/browser/infobar/ContentWrapperView;
    .locals 1

    .prologue
    .line 107
    iget-object v0, p0, Lorg/chromium/chrome/browser/infobar/AnimationHelper;->mTargetWrapperView:Lorg/chromium/chrome/browser/infobar/ContentWrapperView;

    return-object v0
.end method

.method public onGlobalLayout()V
    .locals 1

    .prologue
    .line 122
    iget-object v0, p0, Lorg/chromium/chrome/browser/infobar/AnimationHelper;->mTargetWrapperView:Lorg/chromium/chrome/browser/infobar/ContentWrapperView;

    invoke-static {v0, p0}, Lorg/chromium/base/ApiCompatibilityUtils;->removeOnGlobalLayoutListener(Landroid/view/View;Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 123
    invoke-direct {p0}, Lorg/chromium/chrome/browser/infobar/AnimationHelper;->continueAnimation()V

    .line 124
    return-void
.end method

.method public start()V
    .locals 2

    .prologue
    .line 84
    iget-object v0, p0, Lorg/chromium/chrome/browser/infobar/AnimationHelper;->mTargetWrapperView:Lorg/chromium/chrome/browser/infobar/ContentWrapperView;

    iget-object v1, p0, Lorg/chromium/chrome/browser/infobar/AnimationHelper;->mToShow:Landroid/view/View;

    invoke-virtual {v0, v1}, Lorg/chromium/chrome/browser/infobar/ContentWrapperView;->prepareTransition(Landroid/view/View;)V

    .line 85
    iget-object v0, p0, Lorg/chromium/chrome/browser/infobar/AnimationHelper;->mContainer:Lorg/chromium/chrome/browser/infobar/InfoBarContainer;

    iget-object v1, p0, Lorg/chromium/chrome/browser/infobar/AnimationHelper;->mToShow:Landroid/view/View;

    invoke-virtual {v0, v1}, Lorg/chromium/chrome/browser/infobar/InfoBarContainer;->prepareTransition(Landroid/view/View;)V

    .line 87
    iget-object v0, p0, Lorg/chromium/chrome/browser/infobar/AnimationHelper;->mToShow:Landroid/view/View;

    if-nez v0, :cond_0

    .line 89
    invoke-direct {p0}, Lorg/chromium/chrome/browser/infobar/AnimationHelper;->continueAnimation()V

    .line 94
    :goto_0
    return-void

    .line 92
    :cond_0
    iget-object v0, p0, Lorg/chromium/chrome/browser/infobar/AnimationHelper;->mTargetWrapperView:Lorg/chromium/chrome/browser/infobar/ContentWrapperView;

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/infobar/ContentWrapperView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    goto :goto_0
.end method
