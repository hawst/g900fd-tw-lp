.class public interface abstract Lorg/chromium/chrome/browser/PKCS11AuthenticationManager;
.super Ljava/lang/Object;
.source "PKCS11AuthenticationManager.java"


# virtual methods
.method public abstract getCertificateChain(Ljava/lang/String;)[Ljava/security/cert/X509Certificate;
.end method

.method public abstract getClientCertificateAlias(Ljava/lang/String;I)Ljava/lang/String;
.end method

.method public abstract getPrivateKey(Ljava/lang/String;)Lorg/chromium/net/AndroidPrivateKey;
.end method

.method public abstract initialize(Landroid/content/Context;)V
.end method

.method public abstract isPKCS11AuthEnabled()Z
.end method
