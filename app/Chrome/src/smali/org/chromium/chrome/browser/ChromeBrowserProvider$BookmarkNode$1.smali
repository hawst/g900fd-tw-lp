.class final Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode$1;
.super Ljava/lang/Object;
.source "ChromeBrowserProvider.java"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# instance fields
.field private mNodeMap:Ljava/util/HashMap;


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 934
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private getNode(J)Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 953
    const-wide/16 v2, -0x1

    cmp-long v1, p1, v2

    if-nez v1, :cond_0

    .line 959
    :goto_0
    return-object v0

    .line 954
    :cond_0
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    .line 955
    iget-object v2, p0, Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode$1;->mNodeMap:Ljava/util/HashMap;

    invoke-virtual {v2, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 956
    const-string/jumbo v1, "ChromeBrowserProvider"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "Invalid BookmarkNode hierarchy. Unknown id "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 959
    :cond_1
    iget-object v0, p0, Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode$1;->mNodeMap:Ljava/util/HashMap;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;

    goto :goto_0
.end method

.method private readNodeContents(Landroid/os/Parcel;)Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;
    .locals 12

    .prologue
    .line 963
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v2

    .line 964
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v5

    .line 965
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v6

    .line 966
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 967
    invoke-virtual {p1}, Landroid/os/Parcel;->createByteArray()[B

    move-result-object v8

    .line 968
    invoke-virtual {p1}, Landroid/os/Parcel;->createByteArray()[B

    move-result-object v9

    .line 969
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v10

    .line 970
    if-ltz v0, :cond_0

    invoke-static {}, Lorg/chromium/chrome/browser/ChromeBrowserProvider$Type;->values()[Lorg/chromium/chrome/browser/ChromeBrowserProvider$Type;

    move-result-object v1

    array-length v1, v1

    if-lt v0, v1, :cond_1

    .line 971
    :cond_0
    const-string/jumbo v0, "ChromeBrowserProvider"

    const-string/jumbo v1, "Invalid node type ordinal value."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 972
    const/4 v1, 0x0

    .line 979
    :goto_0
    return-object v1

    .line 975
    :cond_1
    new-instance v1, Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;

    invoke-static {}, Lorg/chromium/chrome/browser/ChromeBrowserProvider$Type;->values()[Lorg/chromium/chrome/browser/ChromeBrowserProvider$Type;

    move-result-object v4

    aget-object v4, v4, v0

    invoke-direct {p0, v10, v11}, Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode$1;->getNode(J)Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;

    move-result-object v7

    invoke-direct/range {v1 .. v7}, Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;-><init>(JLorg/chromium/chrome/browser/ChromeBrowserProvider$Type;Ljava/lang/String;Ljava/lang/String;Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;)V

    .line 977
    invoke-virtual {v1, v8}, Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;->setFavicon([B)V

    .line 978
    invoke-virtual {v1, v9}, Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;->setThumbnail([B)V

    goto :goto_0
.end method

.method private readNodeContentsRecursive(Landroid/os/Parcel;)Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 983
    invoke-direct {p0, p1}, Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode$1;->readNodeContents(Landroid/os/Parcel;)Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;

    move-result-object v1

    .line 984
    if-nez v1, :cond_0

    .line 998
    :goto_0
    return-object v0

    .line 986
    :cond_0
    invoke-virtual {v1}, Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;->id()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    .line 987
    iget-object v3, p0, Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode$1;->mNodeMap:Ljava/util/HashMap;

    invoke-virtual {v3, v2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 988
    const-string/jumbo v2, "ChromeBrowserProvider"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "Invalid BookmarkNode hierarchy. Duplicate id "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;->id()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 991
    :cond_1
    iget-object v0, p0, Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode$1;->mNodeMap:Ljava/util/HashMap;

    invoke-virtual {v0, v2, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 993
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 994
    const/4 v0, 0x0

    :goto_1
    if-ge v0, v2, :cond_2

    .line 995
    invoke-direct {p0, p1}, Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode$1;->readNodeContentsRecursive(Landroid/os/Parcel;)Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;

    move-result-object v3

    invoke-virtual {v1, v3}, Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;->addChild(Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;)V

    .line 994
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    move-object v0, v1

    .line 998
    goto :goto_0
.end method


# virtual methods
.method public final bridge synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 934
    invoke-virtual {p0, p1}, Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode$1;->createFromParcel(Landroid/os/Parcel;)Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;

    move-result-object v0

    return-object v0
.end method

.method public final createFromParcel(Landroid/os/Parcel;)Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;
    .locals 2

    .prologue
    .line 939
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode$1;->mNodeMap:Ljava/util/HashMap;

    .line 940
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    .line 941
    invoke-direct {p0, p1}, Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode$1;->readNodeContentsRecursive(Landroid/os/Parcel;)Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;

    .line 942
    invoke-direct {p0, v0, v1}, Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode$1;->getNode(J)Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;

    move-result-object v0

    .line 943
    iget-object v1, p0, Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode$1;->mNodeMap:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->clear()V

    .line 944
    return-object v0
.end method

.method public final bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 934
    invoke-virtual {p0, p1}, Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode$1;->newArray(I)[Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;

    move-result-object v0

    return-object v0
.end method

.method public final newArray(I)[Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;
    .locals 1

    .prologue
    .line 949
    new-array v0, p1, [Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;

    return-object v0
.end method
