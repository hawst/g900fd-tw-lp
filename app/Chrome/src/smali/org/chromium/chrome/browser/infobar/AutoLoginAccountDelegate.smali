.class public Lorg/chromium/chrome/browser/infobar/AutoLoginAccountDelegate;
.super Ljava/lang/Object;
.source "AutoLoginAccountDelegate.java"

# interfaces
.implements Landroid/accounts/AccountManagerCallback;


# instance fields
.field private final mAccount:Landroid/accounts/Account;

.field private final mAccountManager:Landroid/accounts/AccountManager;

.field private final mActivity:Landroid/app/Activity;

.field private final mAuthTokenType:Ljava/lang/String;

.field private final mAutoLoginProcessor:Lorg/chromium/chrome/browser/infobar/AutoLoginProcessor;

.field private mLogInRequested:Z


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lorg/chromium/chrome/browser/infobar/AutoLoginProcessor;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    iput-object p1, p0, Lorg/chromium/chrome/browser/infobar/AutoLoginAccountDelegate;->mActivity:Landroid/app/Activity;

    .line 33
    iput-object p2, p0, Lorg/chromium/chrome/browser/infobar/AutoLoginAccountDelegate;->mAutoLoginProcessor:Lorg/chromium/chrome/browser/infobar/AutoLoginProcessor;

    .line 34
    invoke-static {p1}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    iput-object v0, p0, Lorg/chromium/chrome/browser/infobar/AutoLoginAccountDelegate;->mAccountManager:Landroid/accounts/AccountManager;

    .line 35
    invoke-static {p1}, Lorg/chromium/sync/signin/ChromeSigninController;->get(Landroid/content/Context;)Lorg/chromium/sync/signin/ChromeSigninController;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/sync/signin/ChromeSigninController;->getSignedInUser()Landroid/accounts/Account;

    move-result-object v0

    iput-object v0, p0, Lorg/chromium/chrome/browser/infobar/AutoLoginAccountDelegate;->mAccount:Landroid/accounts/Account;

    .line 36
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/chromium/chrome/browser/infobar/AutoLoginAccountDelegate;->mLogInRequested:Z

    .line 37
    new-instance v0, Ljava/lang/StringBuilder;

    const-string/jumbo v1, "weblogin:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/chromium/chrome/browser/infobar/AutoLoginAccountDelegate;->mAuthTokenType:Ljava/lang/String;

    .line 38
    return-void
.end method


# virtual methods
.method getAccountName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lorg/chromium/chrome/browser/infobar/AutoLoginAccountDelegate;->mAccount:Landroid/accounts/Account;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/chromium/chrome/browser/infobar/AutoLoginAccountDelegate;->mAccount:Landroid/accounts/Account;

    iget-object v0, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    const-string/jumbo v0, ""

    goto :goto_0
.end method

.method getAuthToken()Ljava/lang/String;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lorg/chromium/chrome/browser/infobar/AutoLoginAccountDelegate;->mAuthTokenType:Ljava/lang/String;

    return-object v0
.end method

.method hasAccount()Z
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lorg/chromium/chrome/browser/infobar/AutoLoginAccountDelegate;->mAccount:Landroid/accounts/Account;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public logIn()Z
    .locals 8

    .prologue
    const/4 v3, 0x0

    const/4 v7, 0x1

    .line 41
    const-string/jumbo v1, "AutoLoginAccountDelegate"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v0, "auto-login requested for "

    invoke-direct {v2, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v0, p0, Lorg/chromium/chrome/browser/infobar/AutoLoginAccountDelegate;->mAccount:Landroid/accounts/Account;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lorg/chromium/chrome/browser/infobar/AutoLoginAccountDelegate;->mAccount:Landroid/accounts/Account;

    invoke-virtual {v0}, Landroid/accounts/Account;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 44
    iget-object v0, p0, Lorg/chromium/chrome/browser/infobar/AutoLoginAccountDelegate;->mActivity:Landroid/app/Activity;

    invoke-static {v0}, Lorg/chromium/sync/signin/ChromeSigninController;->get(Landroid/content/Context;)Lorg/chromium/sync/signin/ChromeSigninController;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/sync/signin/ChromeSigninController;->getSignedInUser()Landroid/accounts/Account;

    move-result-object v0

    .line 45
    iget-object v1, p0, Lorg/chromium/chrome/browser/infobar/AutoLoginAccountDelegate;->mAccount:Landroid/accounts/Account;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lorg/chromium/chrome/browser/infobar/AutoLoginAccountDelegate;->mAccount:Landroid/accounts/Account;

    invoke-virtual {v1, v0}, Landroid/accounts/Account;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 46
    :cond_0
    const-string/jumbo v0, "InfoBar"

    const-string/jumbo v1, "auto-login failed because account is no longer valid"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 47
    const/4 v0, 0x0

    .line 53
    :goto_1
    return v0

    .line 41
    :cond_1
    const-string/jumbo v0, "?"

    goto :goto_0

    .line 51
    :cond_2
    iget-object v0, p0, Lorg/chromium/chrome/browser/infobar/AutoLoginAccountDelegate;->mAccountManager:Landroid/accounts/AccountManager;

    iget-object v1, p0, Lorg/chromium/chrome/browser/infobar/AutoLoginAccountDelegate;->mAccount:Landroid/accounts/Account;

    iget-object v2, p0, Lorg/chromium/chrome/browser/infobar/AutoLoginAccountDelegate;->mAuthTokenType:Ljava/lang/String;

    iget-object v4, p0, Lorg/chromium/chrome/browser/infobar/AutoLoginAccountDelegate;->mActivity:Landroid/app/Activity;

    move-object v5, p0

    move-object v6, v3

    invoke-virtual/range {v0 .. v6}, Landroid/accounts/AccountManager;->getAuthToken(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;Landroid/app/Activity;Landroid/accounts/AccountManagerCallback;Landroid/os/Handler;)Landroid/accounts/AccountManagerFuture;

    .line 52
    iput-boolean v7, p0, Lorg/chromium/chrome/browser/infobar/AutoLoginAccountDelegate;->mLogInRequested:Z

    move v0, v7

    .line 53
    goto :goto_1
.end method

.method loginRequested()Z
    .locals 1

    .prologue
    .line 65
    iget-boolean v0, p0, Lorg/chromium/chrome/browser/infobar/AutoLoginAccountDelegate;->mLogInRequested:Z

    return v0
.end method

.method public run(Landroid/accounts/AccountManagerFuture;)V
    .locals 5

    .prologue
    .line 74
    :try_start_0
    invoke-interface {p1}, Landroid/accounts/AccountManagerFuture;->getResult()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    const-string/jumbo v1, "authtoken"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    move-object v1, v0

    .line 81
    :goto_0
    if-eqz v1, :cond_1

    const/4 v0, 0x1

    .line 85
    :goto_1
    iget-object v2, p0, Lorg/chromium/chrome/browser/infobar/AutoLoginAccountDelegate;->mAutoLoginProcessor:Lorg/chromium/chrome/browser/infobar/AutoLoginProcessor;

    if-eqz v2, :cond_0

    .line 86
    iget-object v2, p0, Lorg/chromium/chrome/browser/infobar/AutoLoginAccountDelegate;->mAutoLoginProcessor:Lorg/chromium/chrome/browser/infobar/AutoLoginProcessor;

    invoke-virtual {p0}, Lorg/chromium/chrome/browser/infobar/AutoLoginAccountDelegate;->getAccountName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0}, Lorg/chromium/chrome/browser/infobar/AutoLoginAccountDelegate;->getAuthToken()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v3, v4, v0, v1}, Lorg/chromium/chrome/browser/infobar/AutoLoginProcessor;->processAutoLoginResult(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)V

    .line 89
    :cond_0
    return-void

    .line 78
    :catch_0
    move-exception v0

    const/4 v0, 0x0

    move-object v1, v0

    goto :goto_0

    .line 81
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method
