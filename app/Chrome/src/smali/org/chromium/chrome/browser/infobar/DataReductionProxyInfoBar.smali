.class public Lorg/chromium/chrome/browser/infobar/DataReductionProxyInfoBar;
.super Lorg/chromium/chrome/browser/infobar/ConfirmInfoBar;
.source "DataReductionProxyInfoBar.java"


# static fields
.field private static sDataReductionProxySettingsClassName:Ljava/lang/String;

.field private static sLinkText:Ljava/lang/String;

.field private static sSettingsClassName:Ljava/lang/String;

.field private static sTitle:Ljava/lang/String;


# direct methods
.method constructor <init>(JI)V
    .locals 11

    .prologue
    const/4 v4, 0x0

    .line 56
    sget-object v6, Lorg/chromium/chrome/browser/infobar/DataReductionProxyInfoBar;->sTitle:Ljava/lang/String;

    sget-object v7, Lorg/chromium/chrome/browser/infobar/DataReductionProxyInfoBar;->sLinkText:Ljava/lang/String;

    move-object v1, p0

    move-wide v2, p1

    move v5, p3

    move-object v8, v4

    move-object v9, v4

    invoke-direct/range {v1 .. v9}, Lorg/chromium/chrome/browser/infobar/ConfirmInfoBar;-><init>(JLorg/chromium/chrome/browser/infobar/InfoBarListeners$Confirm;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 58
    return-void
.end method

.method public static launch(Lorg/chromium/content_public/browser/WebContents;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 35
    sput-object p1, Lorg/chromium/chrome/browser/infobar/DataReductionProxyInfoBar;->sSettingsClassName:Ljava/lang/String;

    .line 36
    sput-object p2, Lorg/chromium/chrome/browser/infobar/DataReductionProxyInfoBar;->sDataReductionProxySettingsClassName:Ljava/lang/String;

    .line 37
    sput-object p3, Lorg/chromium/chrome/browser/infobar/DataReductionProxyInfoBar;->sTitle:Ljava/lang/String;

    .line 38
    sput-object p4, Lorg/chromium/chrome/browser/infobar/DataReductionProxyInfoBar;->sLinkText:Ljava/lang/String;

    .line 39
    invoke-static {p0, p5}, Lorg/chromium/chrome/browser/infobar/DataReductionProxyInfoBarDelegate;->launch(Lorg/chromium/content_public/browser/WebContents;Ljava/lang/String;)V

    .line 40
    return-void
.end method


# virtual methods
.method public createContent(Lorg/chromium/chrome/browser/infobar/InfoBarLayout;)V
    .locals 2

    .prologue
    .line 62
    sget-object v0, Lorg/chromium/chrome/browser/infobar/DataReductionProxyInfoBar;->sLinkText:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Lorg/chromium/chrome/browser/infobar/InfoBarLayout;->setButtons(Ljava/lang/String;Ljava/lang/String;)V

    .line 63
    return-void
.end method

.method public onButtonClicked(Z)V
    .locals 0

    .prologue
    .line 67
    if-nez p1, :cond_0

    .line 69
    :goto_0
    return-void

    .line 68
    :cond_0
    invoke-virtual {p0}, Lorg/chromium/chrome/browser/infobar/DataReductionProxyInfoBar;->onLinkClicked()V

    goto :goto_0
.end method
