.class Lorg/chromium/chrome/browser/signin/SigninManager$2;
.super Ljava/lang/Object;
.source "SigninManager.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic this$0:Lorg/chromium/chrome/browser/signin/SigninManager;


# direct methods
.method constructor <init>(Lorg/chromium/chrome/browser/signin/SigninManager;)V
    .locals 0

    .prologue
    .line 280
    iput-object p1, p0, Lorg/chromium/chrome/browser/signin/SigninManager$2;->this$0:Lorg/chromium/chrome/browser/signin/SigninManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4

    .prologue
    .line 283
    const-string/jumbo v0, "SigninManager"

    const-string/jumbo v1, "Accepted policy management, proceeding with sign-in"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 285
    iget-object v0, p0, Lorg/chromium/chrome/browser/signin/SigninManager$2;->this$0:Lorg/chromium/chrome/browser/signin/SigninManager;

    iget-object v1, p0, Lorg/chromium/chrome/browser/signin/SigninManager$2;->this$0:Lorg/chromium/chrome/browser/signin/SigninManager;

    # getter for: Lorg/chromium/chrome/browser/signin/SigninManager;->mNativeSigninManagerAndroid:J
    invoke-static {v1}, Lorg/chromium/chrome/browser/signin/SigninManager;->access$100(Lorg/chromium/chrome/browser/signin/SigninManager;)J

    move-result-wide v2

    # invokes: Lorg/chromium/chrome/browser/signin/SigninManager;->nativeFetchPolicyBeforeSignIn(J)V
    invoke-static {v0, v2, v3}, Lorg/chromium/chrome/browser/signin/SigninManager;->access$200(Lorg/chromium/chrome/browser/signin/SigninManager;J)V

    .line 286
    iget-object v0, p0, Lorg/chromium/chrome/browser/signin/SigninManager$2;->this$0:Lorg/chromium/chrome/browser/signin/SigninManager;

    const/4 v1, 0x0

    # setter for: Lorg/chromium/chrome/browser/signin/SigninManager;->mPolicyConfirmationDialog:Landroid/app/AlertDialog;
    invoke-static {v0, v1}, Lorg/chromium/chrome/browser/signin/SigninManager;->access$302(Lorg/chromium/chrome/browser/signin/SigninManager;Landroid/app/AlertDialog;)Landroid/app/AlertDialog;

    .line 287
    return-void
.end method
