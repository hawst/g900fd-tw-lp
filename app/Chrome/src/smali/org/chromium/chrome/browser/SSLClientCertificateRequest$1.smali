.class final Lorg/chromium/chrome/browser/SSLClientCertificateRequest$1;
.super Ljava/lang/Object;
.source "SSLClientCertificateRequest.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic val$activity:Landroid/app/Activity;

.field final synthetic val$hostName:Ljava/lang/String;

.field final synthetic val$keyTypes:[Ljava/lang/String;

.field final synthetic val$nativePtr:J

.field final synthetic val$port:I

.field final synthetic val$principalsForCallback:[Ljava/security/Principal;


# direct methods
.method constructor <init>(Landroid/app/Activity;J[Ljava/lang/String;[Ljava/security/Principal;Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 255
    iput-object p1, p0, Lorg/chromium/chrome/browser/SSLClientCertificateRequest$1;->val$activity:Landroid/app/Activity;

    iput-wide p2, p0, Lorg/chromium/chrome/browser/SSLClientCertificateRequest$1;->val$nativePtr:J

    iput-object p4, p0, Lorg/chromium/chrome/browser/SSLClientCertificateRequest$1;->val$keyTypes:[Ljava/lang/String;

    iput-object p5, p0, Lorg/chromium/chrome/browser/SSLClientCertificateRequest$1;->val$principalsForCallback:[Ljava/security/Principal;

    iput-object p6, p0, Lorg/chromium/chrome/browser/SSLClientCertificateRequest$1;->val$hostName:Ljava/lang/String;

    iput p7, p0, Lorg/chromium/chrome/browser/SSLClientCertificateRequest$1;->val$port:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 7

    .prologue
    .line 258
    new-instance v1, Lorg/chromium/chrome/browser/SSLClientCertificateRequest$KeyChainCertSelectionCallback;

    iget-object v0, p0, Lorg/chromium/chrome/browser/SSLClientCertificateRequest$1;->val$activity:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iget-wide v2, p0, Lorg/chromium/chrome/browser/SSLClientCertificateRequest$1;->val$nativePtr:J

    invoke-direct {v1, v0, v2, v3}, Lorg/chromium/chrome/browser/SSLClientCertificateRequest$KeyChainCertSelectionCallback;-><init>(Landroid/content/Context;J)V

    .line 261
    iget-object v0, p0, Lorg/chromium/chrome/browser/SSLClientCertificateRequest$1;->val$activity:Landroid/app/Activity;

    iget-object v2, p0, Lorg/chromium/chrome/browser/SSLClientCertificateRequest$1;->val$keyTypes:[Ljava/lang/String;

    iget-object v3, p0, Lorg/chromium/chrome/browser/SSLClientCertificateRequest$1;->val$principalsForCallback:[Ljava/security/Principal;

    iget-object v4, p0, Lorg/chromium/chrome/browser/SSLClientCertificateRequest$1;->val$hostName:Ljava/lang/String;

    iget v5, p0, Lorg/chromium/chrome/browser/SSLClientCertificateRequest$1;->val$port:I

    const/4 v6, 0x0

    invoke-static/range {v0 .. v6}, Landroid/security/KeyChain;->choosePrivateKeyAlias(Landroid/app/Activity;Landroid/security/KeyChainAliasCallback;[Ljava/lang/String;[Ljava/security/Principal;Ljava/lang/String;ILjava/lang/String;)V

    .line 263
    return-void
.end method
