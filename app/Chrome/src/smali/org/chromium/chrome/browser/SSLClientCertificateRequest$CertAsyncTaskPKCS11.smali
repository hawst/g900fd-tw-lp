.class Lorg/chromium/chrome/browser/SSLClientCertificateRequest$CertAsyncTaskPKCS11;
.super Lorg/chromium/chrome/browser/SSLClientCertificateRequest$CertAsyncTask;
.source "SSLClientCertificateRequest.java"


# instance fields
.field private final mHostName:Ljava/lang/String;

.field private final mPKCS11AuthManager:Lorg/chromium/chrome/browser/PKCS11AuthenticationManager;

.field private final mPort:I


# direct methods
.method constructor <init>(JLjava/lang/String;ILorg/chromium/chrome/browser/PKCS11AuthenticationManager;)V
    .locals 1

    .prologue
    .line 156
    invoke-direct {p0, p1, p2}, Lorg/chromium/chrome/browser/SSLClientCertificateRequest$CertAsyncTask;-><init>(J)V

    .line 157
    iput-object p3, p0, Lorg/chromium/chrome/browser/SSLClientCertificateRequest$CertAsyncTaskPKCS11;->mHostName:Ljava/lang/String;

    .line 158
    iput p4, p0, Lorg/chromium/chrome/browser/SSLClientCertificateRequest$CertAsyncTaskPKCS11;->mPort:I

    .line 159
    iput-object p5, p0, Lorg/chromium/chrome/browser/SSLClientCertificateRequest$CertAsyncTaskPKCS11;->mPKCS11AuthManager:Lorg/chromium/chrome/browser/PKCS11AuthenticationManager;

    .line 160
    return-void
.end method


# virtual methods
.method getAlias()Ljava/lang/String;
    .locals 3

    .prologue
    .line 164
    iget-object v0, p0, Lorg/chromium/chrome/browser/SSLClientCertificateRequest$CertAsyncTaskPKCS11;->mPKCS11AuthManager:Lorg/chromium/chrome/browser/PKCS11AuthenticationManager;

    iget-object v1, p0, Lorg/chromium/chrome/browser/SSLClientCertificateRequest$CertAsyncTaskPKCS11;->mHostName:Ljava/lang/String;

    iget v2, p0, Lorg/chromium/chrome/browser/SSLClientCertificateRequest$CertAsyncTaskPKCS11;->mPort:I

    invoke-interface {v0, v1, v2}, Lorg/chromium/chrome/browser/PKCS11AuthenticationManager;->getClientCertificateAlias(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method getCertificateChain(Ljava/lang/String;)[Ljava/security/cert/X509Certificate;
    .locals 1

    .prologue
    .line 174
    iget-object v0, p0, Lorg/chromium/chrome/browser/SSLClientCertificateRequest$CertAsyncTaskPKCS11;->mPKCS11AuthManager:Lorg/chromium/chrome/browser/PKCS11AuthenticationManager;

    invoke-interface {v0, p1}, Lorg/chromium/chrome/browser/PKCS11AuthenticationManager;->getCertificateChain(Ljava/lang/String;)[Ljava/security/cert/X509Certificate;

    move-result-object v0

    return-object v0
.end method

.method getPrivateKey(Ljava/lang/String;)Lorg/chromium/net/AndroidPrivateKey;
    .locals 1

    .prologue
    .line 169
    iget-object v0, p0, Lorg/chromium/chrome/browser/SSLClientCertificateRequest$CertAsyncTaskPKCS11;->mPKCS11AuthManager:Lorg/chromium/chrome/browser/PKCS11AuthenticationManager;

    invoke-interface {v0, p1}, Lorg/chromium/chrome/browser/PKCS11AuthenticationManager;->getPrivateKey(Ljava/lang/String;)Lorg/chromium/net/AndroidPrivateKey;

    move-result-object v0

    return-object v0
.end method
