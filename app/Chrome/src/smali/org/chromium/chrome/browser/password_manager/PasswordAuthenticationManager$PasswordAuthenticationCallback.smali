.class public Lorg/chromium/chrome/browser/password_manager/PasswordAuthenticationManager$PasswordAuthenticationCallback;
.super Ljava/lang/Object;
.source "PasswordAuthenticationManager.java"


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private mNativePtr:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 41
    const-class v0, Lorg/chromium/chrome/browser/password_manager/PasswordAuthenticationManager;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/chromium/chrome/browser/password_manager/PasswordAuthenticationManager$PasswordAuthenticationCallback;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private constructor <init>(J)V
    .locals 1

    .prologue
    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    iput-wide p1, p0, Lorg/chromium/chrome/browser/password_manager/PasswordAuthenticationManager$PasswordAuthenticationCallback;->mNativePtr:J

    .line 51
    return-void
.end method

.method private static create(J)Lorg/chromium/chrome/browser/password_manager/PasswordAuthenticationManager$PasswordAuthenticationCallback;
    .locals 2

    .prologue
    .line 46
    new-instance v0, Lorg/chromium/chrome/browser/password_manager/PasswordAuthenticationManager$PasswordAuthenticationCallback;

    invoke-direct {v0, p0, p1}, Lorg/chromium/chrome/browser/password_manager/PasswordAuthenticationManager$PasswordAuthenticationCallback;-><init>(J)V

    return-object v0
.end method


# virtual methods
.method public final onResult(Z)V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 58
    iget-wide v0, p0, Lorg/chromium/chrome/browser/password_manager/PasswordAuthenticationManager$PasswordAuthenticationCallback;->mNativePtr:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 59
    sget-boolean v0, Lorg/chromium/chrome/browser/password_manager/PasswordAuthenticationManager$PasswordAuthenticationCallback;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    const-string/jumbo v1, "Can not call onResult more than once per callback."

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 62
    :cond_0
    iget-wide v0, p0, Lorg/chromium/chrome/browser/password_manager/PasswordAuthenticationManager$PasswordAuthenticationCallback;->mNativePtr:J

    # invokes: Lorg/chromium/chrome/browser/password_manager/PasswordAuthenticationManager;->nativeOnResult(JZ)V
    invoke-static {v0, v1, p1}, Lorg/chromium/chrome/browser/password_manager/PasswordAuthenticationManager;->access$000(JZ)V

    .line 63
    iput-wide v2, p0, Lorg/chromium/chrome/browser/password_manager/PasswordAuthenticationManager$PasswordAuthenticationCallback;->mNativePtr:J

    .line 64
    :cond_1
    return-void
.end method
