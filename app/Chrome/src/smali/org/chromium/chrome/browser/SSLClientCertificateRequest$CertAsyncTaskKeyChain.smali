.class Lorg/chromium/chrome/browser/SSLClientCertificateRequest$CertAsyncTaskKeyChain;
.super Lorg/chromium/chrome/browser/SSLClientCertificateRequest$CertAsyncTask;
.source "SSLClientCertificateRequest.java"


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field final mAlias:Ljava/lang/String;

.field final mContext:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 105
    const-class v0, Lorg/chromium/chrome/browser/SSLClientCertificateRequest;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/chromium/chrome/browser/SSLClientCertificateRequest$CertAsyncTaskKeyChain;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>(Landroid/content/Context;JLjava/lang/String;)V
    .locals 2

    .prologue
    .line 110
    invoke-direct {p0, p2, p3}, Lorg/chromium/chrome/browser/SSLClientCertificateRequest$CertAsyncTask;-><init>(J)V

    .line 111
    iput-object p1, p0, Lorg/chromium/chrome/browser/SSLClientCertificateRequest$CertAsyncTaskKeyChain;->mContext:Landroid/content/Context;

    .line 112
    sget-boolean v0, Lorg/chromium/chrome/browser/SSLClientCertificateRequest$CertAsyncTaskKeyChain;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    if-nez p4, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 113
    :cond_0
    iput-object p4, p0, Lorg/chromium/chrome/browser/SSLClientCertificateRequest$CertAsyncTaskKeyChain;->mAlias:Ljava/lang/String;

    .line 114
    return-void
.end method


# virtual methods
.method getAlias()Ljava/lang/String;
    .locals 1

    .prologue
    .line 118
    iget-object v0, p0, Lorg/chromium/chrome/browser/SSLClientCertificateRequest$CertAsyncTaskKeyChain;->mAlias:Ljava/lang/String;

    return-object v0
.end method

.method getCertificateChain(Ljava/lang/String;)[Ljava/security/cert/X509Certificate;
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 137
    :try_start_0
    iget-object v1, p0, Lorg/chromium/chrome/browser/SSLClientCertificateRequest$CertAsyncTaskKeyChain;->mContext:Landroid/content/Context;

    invoke-static {v1, p1}, Landroid/security/KeyChain;->getCertificateChain(Landroid/content/Context;Ljava/lang/String;)[Ljava/security/cert/X509Certificate;
    :try_end_0
    .catch Landroid/security/KeyChainException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    .line 143
    :goto_0
    return-object v0

    .line 139
    :catch_0
    move-exception v1

    const-string/jumbo v1, "SSLClientCertificateRequest"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "KeyChainException when looking for \'"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "\' certificate"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 142
    :catch_1
    move-exception v1

    const-string/jumbo v1, "SSLClientCertificateRequest"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "InterruptedException when looking for \'"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "\'certificate"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method getPrivateKey(Ljava/lang/String;)Lorg/chromium/net/AndroidPrivateKey;
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 124
    :try_start_0
    # getter for: Lorg/chromium/chrome/browser/SSLClientCertificateRequest;->sLocalKeyStore:Lorg/chromium/net/DefaultAndroidKeyStore;
    invoke-static {}, Lorg/chromium/chrome/browser/SSLClientCertificateRequest;->access$100()Lorg/chromium/net/DefaultAndroidKeyStore;

    move-result-object v1

    iget-object v2, p0, Lorg/chromium/chrome/browser/SSLClientCertificateRequest$CertAsyncTaskKeyChain;->mContext:Landroid/content/Context;

    invoke-static {v2, p1}, Landroid/security/KeyChain;->getPrivateKey(Landroid/content/Context;Ljava/lang/String;)Ljava/security/PrivateKey;

    move-result-object v2

    invoke-virtual {v1, v2}, Lorg/chromium/net/DefaultAndroidKeyStore;->createKey(Ljava/security/PrivateKey;)Lorg/chromium/net/AndroidPrivateKey;
    :try_end_0
    .catch Landroid/security/KeyChainException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    .line 130
    :goto_0
    return-object v0

    .line 126
    :catch_0
    move-exception v1

    const-string/jumbo v1, "SSLClientCertificateRequest"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "KeyChainException when looking for \'"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "\' certificate"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 129
    :catch_1
    move-exception v1

    const-string/jumbo v1, "SSLClientCertificateRequest"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "InterruptedException when looking for \'"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "\'certificate"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
