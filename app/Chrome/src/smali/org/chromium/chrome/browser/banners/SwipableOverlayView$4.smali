.class Lorg/chromium/chrome/browser/banners/SwipableOverlayView$4;
.super Landroid/animation/AnimatorListenerAdapter;
.source "SwipableOverlayView.java"


# instance fields
.field final synthetic this$0:Lorg/chromium/chrome/browser/banners/SwipableOverlayView;


# direct methods
.method constructor <init>(Lorg/chromium/chrome/browser/banners/SwipableOverlayView;)V
    .locals 0

    .prologue
    .line 560
    iput-object p1, p0, Lorg/chromium/chrome/browser/banners/SwipableOverlayView$4;->this$0:Lorg/chromium/chrome/browser/banners/SwipableOverlayView;

    invoke-direct {p0}, Landroid/animation/AnimatorListenerAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 563
    iget-object v0, p0, Lorg/chromium/chrome/browser/banners/SwipableOverlayView$4;->this$0:Lorg/chromium/chrome/browser/banners/SwipableOverlayView;

    # getter for: Lorg/chromium/chrome/browser/banners/SwipableOverlayView;->mIsDismissed:Z
    invoke-static {v0}, Lorg/chromium/chrome/browser/banners/SwipableOverlayView;->access$1300(Lorg/chromium/chrome/browser/banners/SwipableOverlayView;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/chromium/chrome/browser/banners/SwipableOverlayView$4;->this$0:Lorg/chromium/chrome/browser/banners/SwipableOverlayView;

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/banners/SwipableOverlayView;->removeFromParent()Z

    .line 565
    :cond_0
    iget-object v0, p0, Lorg/chromium/chrome/browser/banners/SwipableOverlayView$4;->this$0:Lorg/chromium/chrome/browser/banners/SwipableOverlayView;

    # setter for: Lorg/chromium/chrome/browser/banners/SwipableOverlayView;->mGestureState:I
    invoke-static {v0, v2}, Lorg/chromium/chrome/browser/banners/SwipableOverlayView;->access$002(Lorg/chromium/chrome/browser/banners/SwipableOverlayView;I)I

    .line 566
    iget-object v0, p0, Lorg/chromium/chrome/browser/banners/SwipableOverlayView$4;->this$0:Lorg/chromium/chrome/browser/banners/SwipableOverlayView;

    const/4 v1, 0x0

    # setter for: Lorg/chromium/chrome/browser/banners/SwipableOverlayView;->mCurrentAnimation:Landroid/animation/AnimatorSet;
    invoke-static {v0, v1}, Lorg/chromium/chrome/browser/banners/SwipableOverlayView;->access$1202(Lorg/chromium/chrome/browser/banners/SwipableOverlayView;Landroid/animation/AnimatorSet;)Landroid/animation/AnimatorSet;

    .line 567
    iget-object v0, p0, Lorg/chromium/chrome/browser/banners/SwipableOverlayView$4;->this$0:Lorg/chromium/chrome/browser/banners/SwipableOverlayView;

    # setter for: Lorg/chromium/chrome/browser/banners/SwipableOverlayView;->mIsBeingDisplayedForFirstTime:Z
    invoke-static {v0, v2}, Lorg/chromium/chrome/browser/banners/SwipableOverlayView;->access$1102(Lorg/chromium/chrome/browser/banners/SwipableOverlayView;Z)Z

    .line 568
    return-void
.end method
