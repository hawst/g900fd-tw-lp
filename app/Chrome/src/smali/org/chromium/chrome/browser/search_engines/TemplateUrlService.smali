.class public Lorg/chromium/chrome/browser/search_engines/TemplateUrlService;
.super Ljava/lang/Object;
.source "TemplateUrlService.java"


# static fields
.field static final synthetic $assertionsDisabled:Z

.field private static sService:Lorg/chromium/chrome/browser/search_engines/TemplateUrlService;


# instance fields
.field private final mLoadListeners:Lorg/chromium/base/ObserverList;

.field private final mNativeTemplateUrlServiceAndroid:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 23
    const-class v0, Lorg/chromium/chrome/browser/search_engines/TemplateUrlService;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/chromium/chrome/browser/search_engines/TemplateUrlService;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    .line 77
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 75
    new-instance v0, Lorg/chromium/base/ObserverList;

    invoke-direct {v0}, Lorg/chromium/base/ObserverList;-><init>()V

    iput-object v0, p0, Lorg/chromium/chrome/browser/search_engines/TemplateUrlService;->mLoadListeners:Lorg/chromium/base/ObserverList;

    .line 80
    invoke-direct {p0}, Lorg/chromium/chrome/browser/search_engines/TemplateUrlService;->nativeInit()J

    move-result-wide v0

    iput-wide v0, p0, Lorg/chromium/chrome/browser/search_engines/TemplateUrlService;->mNativeTemplateUrlServiceAndroid:J

    .line 81
    return-void
.end method

.method public static getInstance()Lorg/chromium/chrome/browser/search_engines/TemplateUrlService;
    .locals 1

    .prologue
    .line 67
    invoke-static {}, Lorg/chromium/base/ThreadUtils;->assertOnUiThread()V

    .line 68
    sget-object v0, Lorg/chromium/chrome/browser/search_engines/TemplateUrlService;->sService:Lorg/chromium/chrome/browser/search_engines/TemplateUrlService;

    if-nez v0, :cond_0

    .line 69
    new-instance v0, Lorg/chromium/chrome/browser/search_engines/TemplateUrlService;

    invoke-direct {v0}, Lorg/chromium/chrome/browser/search_engines/TemplateUrlService;-><init>()V

    sput-object v0, Lorg/chromium/chrome/browser/search_engines/TemplateUrlService;->sService:Lorg/chromium/chrome/browser/search_engines/TemplateUrlService;

    .line 71
    :cond_0
    sget-object v0, Lorg/chromium/chrome/browser/search_engines/TemplateUrlService;->sService:Lorg/chromium/chrome/browser/search_engines/TemplateUrlService;

    return-object v0
.end method

.method private native nativeGetDefaultSearchProvider(J)I
.end method

.method private native nativeGetPrepopulatedTemplateUrlAt(JI)Lorg/chromium/chrome/browser/search_engines/TemplateUrlService$TemplateUrl;
.end method

.method private native nativeGetTemplateUrlCount(J)I
.end method

.method private native nativeGetUrlForContextualSearchQuery(JLjava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;
.end method

.method private native nativeGetUrlForSearchQuery(JLjava/lang/String;)Ljava/lang/String;
.end method

.method private native nativeGetUrlForVoiceSearchQuery(JLjava/lang/String;)Ljava/lang/String;
.end method

.method private native nativeInit()J
.end method

.method private native nativeIsDefaultSearchEngineGoogle(J)Z
.end method

.method private native nativeIsLoaded(J)Z
.end method

.method private native nativeIsSearchByImageAvailable(J)Z
.end method

.method private native nativeIsSearchProviderManaged(J)Z
.end method

.method private native nativeLoad(J)V
.end method

.method private native nativeReplaceSearchTermsInUrl(JLjava/lang/String;Ljava/lang/String;)Ljava/lang/String;
.end method

.method private native nativeSetUserSelectedDefaultSearchProvider(JI)V
.end method

.method private templateUrlServiceLoaded()V
    .locals 2

    .prologue
    .line 115
    invoke-static {}, Lorg/chromium/base/ThreadUtils;->assertOnUiThread()V

    .line 116
    iget-object v0, p0, Lorg/chromium/chrome/browser/search_engines/TemplateUrlService;->mLoadListeners:Lorg/chromium/base/ObserverList;

    invoke-virtual {v0}, Lorg/chromium/base/ObserverList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/chromium/chrome/browser/search_engines/TemplateUrlService$LoadListener;

    .line 117
    invoke-interface {v0}, Lorg/chromium/chrome/browser/search_engines/TemplateUrlService$LoadListener;->onTemplateUrlServiceLoaded()V

    goto :goto_0

    .line 119
    :cond_0
    return-void
.end method


# virtual methods
.method public getDefaultSearchEngineIndex()I
    .locals 2

    .prologue
    .line 125
    invoke-static {}, Lorg/chromium/base/ThreadUtils;->assertOnUiThread()V

    .line 126
    iget-wide v0, p0, Lorg/chromium/chrome/browser/search_engines/TemplateUrlService;->mNativeTemplateUrlServiceAndroid:J

    invoke-direct {p0, v0, v1}, Lorg/chromium/chrome/browser/search_engines/TemplateUrlService;->nativeGetDefaultSearchProvider(J)I

    move-result v0

    return v0
.end method

.method public getDefaultSearchEngineTemplateUrl()Lorg/chromium/chrome/browser/search_engines/TemplateUrlService$TemplateUrl;
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 133
    invoke-virtual {p0}, Lorg/chromium/chrome/browser/search_engines/TemplateUrlService;->isLoaded()Z

    move-result v1

    if-nez v1, :cond_1

    .line 142
    :cond_0
    :goto_0
    return-object v0

    .line 135
    :cond_1
    invoke-virtual {p0}, Lorg/chromium/chrome/browser/search_engines/TemplateUrlService;->getDefaultSearchEngineIndex()I

    move-result v1

    .line 136
    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    .line 138
    sget-boolean v0, Lorg/chromium/chrome/browser/search_engines/TemplateUrlService;->$assertionsDisabled:Z

    if-nez v0, :cond_2

    if-gez v1, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 139
    :cond_2
    sget-boolean v0, Lorg/chromium/chrome/browser/search_engines/TemplateUrlService;->$assertionsDisabled:Z

    if-nez v0, :cond_3

    iget-wide v2, p0, Lorg/chromium/chrome/browser/search_engines/TemplateUrlService;->mNativeTemplateUrlServiceAndroid:J

    invoke-direct {p0, v2, v3}, Lorg/chromium/chrome/browser/search_engines/TemplateUrlService;->nativeGetTemplateUrlCount(J)I

    move-result v0

    if-lt v1, v0, :cond_3

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 142
    :cond_3
    iget-wide v2, p0, Lorg/chromium/chrome/browser/search_engines/TemplateUrlService;->mNativeTemplateUrlServiceAndroid:J

    invoke-direct {p0, v2, v3, v1}, Lorg/chromium/chrome/browser/search_engines/TemplateUrlService;->nativeGetPrepopulatedTemplateUrlAt(JI)Lorg/chromium/chrome/browser/search_engines/TemplateUrlService$TemplateUrl;

    move-result-object v0

    goto :goto_0
.end method

.method public getLocalizedSearchEngines()Ljava/util/List;
    .locals 6

    .prologue
    .line 97
    invoke-static {}, Lorg/chromium/base/ThreadUtils;->assertOnUiThread()V

    .line 98
    iget-wide v0, p0, Lorg/chromium/chrome/browser/search_engines/TemplateUrlService;->mNativeTemplateUrlServiceAndroid:J

    invoke-direct {p0, v0, v1}, Lorg/chromium/chrome/browser/search_engines/TemplateUrlService;->nativeGetTemplateUrlCount(J)I

    move-result v1

    .line 99
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 100
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_1

    .line 101
    iget-wide v4, p0, Lorg/chromium/chrome/browser/search_engines/TemplateUrlService;->mNativeTemplateUrlServiceAndroid:J

    invoke-direct {p0, v4, v5, v0}, Lorg/chromium/chrome/browser/search_engines/TemplateUrlService;->nativeGetPrepopulatedTemplateUrlAt(JI)Lorg/chromium/chrome/browser/search_engines/TemplateUrlService$TemplateUrl;

    move-result-object v3

    .line 103
    if-eqz v3, :cond_0

    .line 104
    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 100
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 107
    :cond_1
    return-object v2
.end method

.method public getUrlForContextualSearchQuery(Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;
    .locals 7

    .prologue
    .line 253
    iget-wide v2, p0, Lorg/chromium/chrome/browser/search_engines/TemplateUrlService;->mNativeTemplateUrlServiceAndroid:J

    move-object v1, p0

    move-object v4, p1

    move-object v5, p2

    move v6, p3

    invoke-direct/range {v1 .. v6}, Lorg/chromium/chrome/browser/search_engines/TemplateUrlService;->nativeGetUrlForContextualSearchQuery(JLjava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getUrlForSearchQuery(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 199
    iget-wide v0, p0, Lorg/chromium/chrome/browser/search_engines/TemplateUrlService;->mNativeTemplateUrlServiceAndroid:J

    invoke-direct {p0, v0, v1, p1}, Lorg/chromium/chrome/browser/search_engines/TemplateUrlService;->nativeGetUrlForSearchQuery(JLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getUrlForVoiceSearchQuery(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 211
    iget-wide v0, p0, Lorg/chromium/chrome/browser/search_engines/TemplateUrlService;->mNativeTemplateUrlServiceAndroid:J

    invoke-direct {p0, v0, v1, p1}, Lorg/chromium/chrome/browser/search_engines/TemplateUrlService;->nativeGetUrlForVoiceSearchQuery(JLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public isDefaultSearchEngineGoogle()Z
    .locals 2

    .prologue
    .line 167
    iget-wide v0, p0, Lorg/chromium/chrome/browser/search_engines/TemplateUrlService;->mNativeTemplateUrlServiceAndroid:J

    invoke-direct {p0, v0, v1}, Lorg/chromium/chrome/browser/search_engines/TemplateUrlService;->nativeIsDefaultSearchEngineGoogle(J)Z

    move-result v0

    return v0
.end method

.method public isLoaded()Z
    .locals 2

    .prologue
    .line 84
    invoke-static {}, Lorg/chromium/base/ThreadUtils;->assertOnUiThread()V

    .line 85
    iget-wide v0, p0, Lorg/chromium/chrome/browser/search_engines/TemplateUrlService;->mNativeTemplateUrlServiceAndroid:J

    invoke-direct {p0, v0, v1}, Lorg/chromium/chrome/browser/search_engines/TemplateUrlService;->nativeIsLoaded(J)Z

    move-result v0

    return v0
.end method

.method public isSearchByImageAvailable()Z
    .locals 2

    .prologue
    .line 159
    invoke-static {}, Lorg/chromium/base/ThreadUtils;->assertOnUiThread()V

    .line 160
    iget-wide v0, p0, Lorg/chromium/chrome/browser/search_engines/TemplateUrlService;->mNativeTemplateUrlServiceAndroid:J

    invoke-direct {p0, v0, v1}, Lorg/chromium/chrome/browser/search_engines/TemplateUrlService;->nativeIsSearchByImageAvailable(J)Z

    move-result v0

    return v0
.end method

.method public isSearchProviderManaged()Z
    .locals 2

    .prologue
    .line 152
    iget-wide v0, p0, Lorg/chromium/chrome/browser/search_engines/TemplateUrlService;->mNativeTemplateUrlServiceAndroid:J

    invoke-direct {p0, v0, v1}, Lorg/chromium/chrome/browser/search_engines/TemplateUrlService;->nativeIsSearchProviderManaged(J)Z

    move-result v0

    return v0
.end method

.method public load()V
    .locals 2

    .prologue
    .line 89
    invoke-static {}, Lorg/chromium/base/ThreadUtils;->assertOnUiThread()V

    .line 90
    iget-wide v0, p0, Lorg/chromium/chrome/browser/search_engines/TemplateUrlService;->mNativeTemplateUrlServiceAndroid:J

    invoke-direct {p0, v0, v1}, Lorg/chromium/chrome/browser/search_engines/TemplateUrlService;->nativeLoad(J)V

    .line 91
    return-void
.end method

.method public registerLoadListener(Lorg/chromium/chrome/browser/search_engines/TemplateUrlService$LoadListener;)V
    .locals 2

    .prologue
    .line 175
    invoke-static {}, Lorg/chromium/base/ThreadUtils;->assertOnUiThread()V

    .line 176
    iget-object v0, p0, Lorg/chromium/chrome/browser/search_engines/TemplateUrlService;->mLoadListeners:Lorg/chromium/base/ObserverList;

    invoke-virtual {v0, p1}, Lorg/chromium/base/ObserverList;->addObserver(Ljava/lang/Object;)Z

    move-result v0

    .line 177
    sget-boolean v1, Lorg/chromium/chrome/browser/search_engines/TemplateUrlService;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 178
    :cond_0
    return-void
.end method

.method public replaceSearchTermsInUrl(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 223
    iget-wide v0, p0, Lorg/chromium/chrome/browser/search_engines/TemplateUrlService;->mNativeTemplateUrlServiceAndroid:J

    invoke-direct {p0, v0, v1, p1, p2}, Lorg/chromium/chrome/browser/search_engines/TemplateUrlService;->nativeReplaceSearchTermsInUrl(JLjava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public setSearchEngine(I)V
    .locals 2

    .prologue
    .line 147
    invoke-static {}, Lorg/chromium/base/ThreadUtils;->assertOnUiThread()V

    .line 148
    iget-wide v0, p0, Lorg/chromium/chrome/browser/search_engines/TemplateUrlService;->mNativeTemplateUrlServiceAndroid:J

    invoke-direct {p0, v0, v1, p1}, Lorg/chromium/chrome/browser/search_engines/TemplateUrlService;->nativeSetUserSelectedDefaultSearchProvider(JI)V

    .line 149
    return-void
.end method

.method public unregisterLoadListener(Lorg/chromium/chrome/browser/search_engines/TemplateUrlService$LoadListener;)V
    .locals 2

    .prologue
    .line 185
    invoke-static {}, Lorg/chromium/base/ThreadUtils;->assertOnUiThread()V

    .line 186
    iget-object v0, p0, Lorg/chromium/chrome/browser/search_engines/TemplateUrlService;->mLoadListeners:Lorg/chromium/base/ObserverList;

    invoke-virtual {v0, p1}, Lorg/chromium/base/ObserverList;->removeObserver(Ljava/lang/Object;)Z

    move-result v0

    .line 187
    sget-boolean v1, Lorg/chromium/chrome/browser/search_engines/TemplateUrlService;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 188
    :cond_0
    return-void
.end method
