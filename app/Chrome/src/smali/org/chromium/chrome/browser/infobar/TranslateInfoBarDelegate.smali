.class public Lorg/chromium/chrome/browser/infobar/TranslateInfoBarDelegate;
.super Ljava/lang/Object;
.source "TranslateInfoBarDelegate.java"


# instance fields
.field private mInfoBar:Lorg/chromium/chrome/browser/infobar/TranslateInfoBar;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static create()Lorg/chromium/chrome/browser/infobar/TranslateInfoBarDelegate;
    .locals 1

    .prologue
    .line 19
    new-instance v0, Lorg/chromium/chrome/browser/infobar/TranslateInfoBarDelegate;

    invoke-direct {v0}, Lorg/chromium/chrome/browser/infobar/TranslateInfoBarDelegate;-><init>()V

    return-object v0
.end method

.method private native nativeApplyTranslateOptions(JIIZZZ)V
.end method


# virtual methods
.method public applyTranslateOptions(JIIZZZ)V
    .locals 1

    .prologue
    .line 45
    invoke-direct/range {p0 .. p7}, Lorg/chromium/chrome/browser/infobar/TranslateInfoBarDelegate;->nativeApplyTranslateOptions(JIIZZZ)V

    .line 47
    return-void
.end method

.method changeTranslateInfoBarTypeAndPointer(JI)Z
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lorg/chromium/chrome/browser/infobar/TranslateInfoBarDelegate;->mInfoBar:Lorg/chromium/chrome/browser/infobar/TranslateInfoBar;

    invoke-virtual {v0, p3, p1, p2}, Lorg/chromium/chrome/browser/infobar/TranslateInfoBar;->changeInfoBarTypeAndNativePointer(IJ)V

    .line 27
    const/4 v0, 0x1

    return v0
.end method

.method showTranslateInfoBar(JIIIZZZ[Ljava/lang/String;)Lorg/chromium/chrome/browser/infobar/InfoBar;
    .locals 13

    .prologue
    .line 36
    new-instance v1, Lorg/chromium/chrome/browser/infobar/TranslateInfoBar;

    move-wide v2, p1

    move-object v4, p0

    move/from16 v5, p3

    move/from16 v6, p4

    move/from16 v7, p5

    move/from16 v8, p6

    move/from16 v9, p7

    move/from16 v10, p8

    move-object/from16 v11, p9

    invoke-direct/range {v1 .. v11}, Lorg/chromium/chrome/browser/infobar/TranslateInfoBar;-><init>(JLorg/chromium/chrome/browser/infobar/TranslateInfoBarDelegate;IIIZZZ[Ljava/lang/String;)V

    iput-object v1, p0, Lorg/chromium/chrome/browser/infobar/TranslateInfoBarDelegate;->mInfoBar:Lorg/chromium/chrome/browser/infobar/TranslateInfoBar;

    .line 39
    iget-object v0, p0, Lorg/chromium/chrome/browser/infobar/TranslateInfoBarDelegate;->mInfoBar:Lorg/chromium/chrome/browser/infobar/TranslateInfoBar;

    return-object v0
.end method
