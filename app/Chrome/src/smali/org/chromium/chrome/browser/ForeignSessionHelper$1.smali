.class Lorg/chromium/chrome/browser/ForeignSessionHelper$1;
.super Ljava/lang/Object;
.source "ForeignSessionHelper.java"

# interfaces
.implements Ljava/util/Comparator;


# instance fields
.field final synthetic this$0:Lorg/chromium/chrome/browser/ForeignSessionHelper;


# direct methods
.method constructor <init>(Lorg/chromium/chrome/browser/ForeignSessionHelper;)V
    .locals 0

    .prologue
    .line 167
    iput-object p1, p0, Lorg/chromium/chrome/browser/ForeignSessionHelper$1;->this$0:Lorg/chromium/chrome/browser/ForeignSessionHelper;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 167
    check-cast p1, Lorg/chromium/chrome/browser/ForeignSessionHelper$ForeignSession;

    check-cast p2, Lorg/chromium/chrome/browser/ForeignSessionHelper$ForeignSession;

    invoke-virtual {p0, p1, p2}, Lorg/chromium/chrome/browser/ForeignSessionHelper$1;->compare(Lorg/chromium/chrome/browser/ForeignSessionHelper$ForeignSession;Lorg/chromium/chrome/browser/ForeignSessionHelper$ForeignSession;)I

    move-result v0

    return v0
.end method

.method public compare(Lorg/chromium/chrome/browser/ForeignSessionHelper$ForeignSession;Lorg/chromium/chrome/browser/ForeignSessionHelper$ForeignSession;)I
    .locals 4

    .prologue
    .line 170
    iget-wide v0, p1, Lorg/chromium/chrome/browser/ForeignSessionHelper$ForeignSession;->modifiedTime:J

    iget-wide v2, p2, Lorg/chromium/chrome/browser/ForeignSessionHelper$ForeignSession;->modifiedTime:J

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    iget-wide v0, p1, Lorg/chromium/chrome/browser/ForeignSessionHelper$ForeignSession;->modifiedTime:J

    iget-wide v2, p2, Lorg/chromium/chrome/browser/ForeignSessionHelper$ForeignSession;->modifiedTime:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    const/4 v0, -0x1

    goto :goto_0
.end method
