.class public Lorg/chromium/chrome/browser/LogoBridge;
.super Ljava/lang/Object;
.source "LogoBridge.java"


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private mNativeLogoBridge:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 15
    const-class v0, Lorg/chromium/chrome/browser/LogoBridge;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/chromium/chrome/browser/LogoBridge;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lorg/chromium/chrome/browser/profiles/Profile;)V
    .locals 2

    .prologue
    .line 65
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 66
    invoke-direct {p0, p1}, Lorg/chromium/chrome/browser/LogoBridge;->nativeInit(Lorg/chromium/chrome/browser/profiles/Profile;)J

    move-result-wide v0

    iput-wide v0, p0, Lorg/chromium/chrome/browser/LogoBridge;->mNativeLogoBridge:J

    .line 67
    return-void
.end method

.method private static createLogo(Landroid/graphics/Bitmap;Ljava/lang/String;Ljava/lang/String;)Lorg/chromium/chrome/browser/LogoBridge$Logo;
    .locals 1

    .prologue
    .line 98
    new-instance v0, Lorg/chromium/chrome/browser/LogoBridge$Logo;

    invoke-direct {v0, p0, p1, p2}, Lorg/chromium/chrome/browser/LogoBridge$Logo;-><init>(Landroid/graphics/Bitmap;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method private native nativeDestroy(J)V
.end method

.method private native nativeGetCurrentLogo(JLorg/chromium/chrome/browser/LogoBridge$LogoObserver;)V
.end method

.method private native nativeInit(Lorg/chromium/chrome/browser/profiles/Profile;)J
.end method


# virtual methods
.method public destroy()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 74
    sget-boolean v0, Lorg/chromium/chrome/browser/LogoBridge;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-wide v0, p0, Lorg/chromium/chrome/browser/LogoBridge;->mNativeLogoBridge:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 75
    :cond_0
    iget-wide v0, p0, Lorg/chromium/chrome/browser/LogoBridge;->mNativeLogoBridge:J

    invoke-direct {p0, v0, v1}, Lorg/chromium/chrome/browser/LogoBridge;->nativeDestroy(J)V

    .line 76
    iput-wide v2, p0, Lorg/chromium/chrome/browser/LogoBridge;->mNativeLogoBridge:J

    .line 77
    return-void
.end method

.method protected finalize()V
    .locals 4

    .prologue
    .line 93
    sget-boolean v0, Lorg/chromium/chrome/browser/LogoBridge;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-wide v0, p0, Lorg/chromium/chrome/browser/LogoBridge;->mNativeLogoBridge:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 94
    :cond_0
    return-void
.end method

.method public getCurrentLogo(Lorg/chromium/chrome/browser/LogoBridge$LogoObserver;)V
    .locals 2

    .prologue
    .line 87
    iget-wide v0, p0, Lorg/chromium/chrome/browser/LogoBridge;->mNativeLogoBridge:J

    invoke-direct {p0, v0, v1, p1}, Lorg/chromium/chrome/browser/LogoBridge;->nativeGetCurrentLogo(JLorg/chromium/chrome/browser/LogoBridge$LogoObserver;)V

    .line 88
    return-void
.end method
