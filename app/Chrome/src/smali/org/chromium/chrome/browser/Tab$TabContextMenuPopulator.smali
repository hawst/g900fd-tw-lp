.class Lorg/chromium/chrome/browser/Tab$TabContextMenuPopulator;
.super Lorg/chromium/chrome/browser/contextmenu/ContextMenuPopulatorWrapper;
.source "Tab.java"


# instance fields
.field final synthetic this$0:Lorg/chromium/chrome/browser/Tab;


# direct methods
.method public constructor <init>(Lorg/chromium/chrome/browser/Tab;Lorg/chromium/chrome/browser/contextmenu/ContextMenuPopulator;)V
    .locals 0

    .prologue
    .line 253
    iput-object p1, p0, Lorg/chromium/chrome/browser/Tab$TabContextMenuPopulator;->this$0:Lorg/chromium/chrome/browser/Tab;

    .line 254
    invoke-direct {p0, p2}, Lorg/chromium/chrome/browser/contextmenu/ContextMenuPopulatorWrapper;-><init>(Lorg/chromium/chrome/browser/contextmenu/ContextMenuPopulator;)V

    .line 255
    return-void
.end method


# virtual methods
.method public buildContextMenu(Landroid/view/ContextMenu;Landroid/content/Context;Lorg/chromium/chrome/browser/contextmenu/ContextMenuParams;)V
    .locals 3

    .prologue
    .line 259
    invoke-super {p0, p1, p2, p3}, Lorg/chromium/chrome/browser/contextmenu/ContextMenuPopulatorWrapper;->buildContextMenu(Landroid/view/ContextMenu;Landroid/content/Context;Lorg/chromium/chrome/browser/contextmenu/ContextMenuParams;)V

    .line 260
    iget-object v0, p0, Lorg/chromium/chrome/browser/Tab$TabContextMenuPopulator;->this$0:Lorg/chromium/chrome/browser/Tab;

    # getter for: Lorg/chromium/chrome/browser/Tab;->mObservers:Lorg/chromium/base/ObserverList;
    invoke-static {v0}, Lorg/chromium/chrome/browser/Tab;->access$100(Lorg/chromium/chrome/browser/Tab;)Lorg/chromium/base/ObserverList;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/base/ObserverList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/chromium/chrome/browser/TabObserver;

    iget-object v2, p0, Lorg/chromium/chrome/browser/Tab$TabContextMenuPopulator;->this$0:Lorg/chromium/chrome/browser/Tab;

    invoke-interface {v0, v2, p1}, Lorg/chromium/chrome/browser/TabObserver;->onContextMenuShown(Lorg/chromium/chrome/browser/Tab;Landroid/view/ContextMenu;)V

    goto :goto_0

    .line 261
    :cond_0
    return-void
.end method
