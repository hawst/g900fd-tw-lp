.class Lorg/chromium/chrome/browser/WebsiteSettingsPopup$2;
.super Ljava/lang/Object;
.source "WebsiteSettingsPopup.java"

# interfaces
.implements Landroid/content/DialogInterface$OnDismissListener;


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field final synthetic this$0:Lorg/chromium/chrome/browser/WebsiteSettingsPopup;

.field final synthetic val$webContentsObserver:Lorg/chromium/content/browser/WebContentsObserverAndroid;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 79
    const-class v0, Lorg/chromium/chrome/browser/WebsiteSettingsPopup;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/chromium/chrome/browser/WebsiteSettingsPopup$2;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>(Lorg/chromium/chrome/browser/WebsiteSettingsPopup;Lorg/chromium/content/browser/WebContentsObserverAndroid;)V
    .locals 0

    .prologue
    .line 79
    iput-object p1, p0, Lorg/chromium/chrome/browser/WebsiteSettingsPopup$2;->this$0:Lorg/chromium/chrome/browser/WebsiteSettingsPopup;

    iput-object p2, p0, Lorg/chromium/chrome/browser/WebsiteSettingsPopup$2;->val$webContentsObserver:Lorg/chromium/content/browser/WebContentsObserverAndroid;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDismiss(Landroid/content/DialogInterface;)V
    .locals 4

    .prologue
    .line 82
    sget-boolean v0, Lorg/chromium/chrome/browser/WebsiteSettingsPopup$2;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lorg/chromium/chrome/browser/WebsiteSettingsPopup$2;->this$0:Lorg/chromium/chrome/browser/WebsiteSettingsPopup;

    # getter for: Lorg/chromium/chrome/browser/WebsiteSettingsPopup;->mNativeWebsiteSettingsPopup:J
    invoke-static {v0}, Lorg/chromium/chrome/browser/WebsiteSettingsPopup;->access$100(Lorg/chromium/chrome/browser/WebsiteSettingsPopup;)J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 83
    :cond_0
    iget-object v0, p0, Lorg/chromium/chrome/browser/WebsiteSettingsPopup$2;->val$webContentsObserver:Lorg/chromium/content/browser/WebContentsObserverAndroid;

    invoke-virtual {v0}, Lorg/chromium/content/browser/WebContentsObserverAndroid;->detachFromWebContents()V

    .line 84
    iget-object v0, p0, Lorg/chromium/chrome/browser/WebsiteSettingsPopup$2;->this$0:Lorg/chromium/chrome/browser/WebsiteSettingsPopup;

    iget-object v1, p0, Lorg/chromium/chrome/browser/WebsiteSettingsPopup$2;->this$0:Lorg/chromium/chrome/browser/WebsiteSettingsPopup;

    # getter for: Lorg/chromium/chrome/browser/WebsiteSettingsPopup;->mNativeWebsiteSettingsPopup:J
    invoke-static {v1}, Lorg/chromium/chrome/browser/WebsiteSettingsPopup;->access$100(Lorg/chromium/chrome/browser/WebsiteSettingsPopup;)J

    move-result-wide v2

    # invokes: Lorg/chromium/chrome/browser/WebsiteSettingsPopup;->nativeDestroy(J)V
    invoke-static {v0, v2, v3}, Lorg/chromium/chrome/browser/WebsiteSettingsPopup;->access$200(Lorg/chromium/chrome/browser/WebsiteSettingsPopup;J)V

    .line 85
    return-void
.end method
