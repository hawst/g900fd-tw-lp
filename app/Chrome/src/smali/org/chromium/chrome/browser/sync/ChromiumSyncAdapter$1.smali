.class Lorg/chromium/chrome/browser/sync/ChromiumSyncAdapter$1;
.super Ljava/lang/Object;
.source "ChromiumSyncAdapter.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic this$0:Lorg/chromium/chrome/browser/sync/ChromiumSyncAdapter;

.field final synthetic val$callback:Lorg/chromium/content/browser/BrowserStartupController$StartupCallback;


# direct methods
.method constructor <init>(Lorg/chromium/chrome/browser/sync/ChromiumSyncAdapter;Lorg/chromium/content/browser/BrowserStartupController$StartupCallback;)V
    .locals 0

    .prologue
    .line 85
    iput-object p1, p0, Lorg/chromium/chrome/browser/sync/ChromiumSyncAdapter$1;->this$0:Lorg/chromium/chrome/browser/sync/ChromiumSyncAdapter;

    iput-object p2, p0, Lorg/chromium/chrome/browser/sync/ChromiumSyncAdapter$1;->val$callback:Lorg/chromium/content/browser/BrowserStartupController$StartupCallback;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 88
    iget-object v0, p0, Lorg/chromium/chrome/browser/sync/ChromiumSyncAdapter$1;->this$0:Lorg/chromium/chrome/browser/sync/ChromiumSyncAdapter;

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/sync/ChromiumSyncAdapter;->initCommandLine()V

    .line 89
    iget-object v0, p0, Lorg/chromium/chrome/browser/sync/ChromiumSyncAdapter$1;->this$0:Lorg/chromium/chrome/browser/sync/ChromiumSyncAdapter;

    # getter for: Lorg/chromium/chrome/browser/sync/ChromiumSyncAdapter;->mAsyncStartup:Z
    invoke-static {v0}, Lorg/chromium/chrome/browser/sync/ChromiumSyncAdapter;->access$000(Lorg/chromium/chrome/browser/sync/ChromiumSyncAdapter;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 91
    :try_start_0
    iget-object v0, p0, Lorg/chromium/chrome/browser/sync/ChromiumSyncAdapter$1;->this$0:Lorg/chromium/chrome/browser/sync/ChromiumSyncAdapter;

    # getter for: Lorg/chromium/chrome/browser/sync/ChromiumSyncAdapter;->mApplication:Landroid/app/Application;
    invoke-static {v0}, Lorg/chromium/chrome/browser/sync/ChromiumSyncAdapter;->access$100(Lorg/chromium/chrome/browser/sync/ChromiumSyncAdapter;)Landroid/app/Application;

    move-result-object v0

    invoke-static {v0}, Lorg/chromium/content/browser/BrowserStartupController;->get(Landroid/content/Context;)Lorg/chromium/content/browser/BrowserStartupController;

    move-result-object v0

    iget-object v1, p0, Lorg/chromium/chrome/browser/sync/ChromiumSyncAdapter$1;->val$callback:Lorg/chromium/content/browser/BrowserStartupController$StartupCallback;

    invoke-virtual {v0, v1}, Lorg/chromium/content/browser/BrowserStartupController;->startBrowserProcessesAsync(Lorg/chromium/content/browser/BrowserStartupController$StartupCallback;)V
    :try_end_0
    .catch Lorg/chromium/base/library_loader/ProcessInitException; {:try_start_0 .. :try_end_0} :catch_0

    .line 100
    :goto_0
    return-void

    .line 93
    :catch_0
    move-exception v0

    .line 94
    const-string/jumbo v1, "ChromiumSyncAdapter"

    const-string/jumbo v2, "Unable to load native library."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 95
    const/4 v0, -0x1

    invoke-static {v0}, Ljava/lang/System;->exit(I)V

    goto :goto_0

    .line 98
    :cond_0
    iget-object v0, p0, Lorg/chromium/chrome/browser/sync/ChromiumSyncAdapter$1;->this$0:Lorg/chromium/chrome/browser/sync/ChromiumSyncAdapter;

    iget-object v1, p0, Lorg/chromium/chrome/browser/sync/ChromiumSyncAdapter$1;->val$callback:Lorg/chromium/content/browser/BrowserStartupController$StartupCallback;

    # invokes: Lorg/chromium/chrome/browser/sync/ChromiumSyncAdapter;->startBrowserProcessesSync(Lorg/chromium/content/browser/BrowserStartupController$StartupCallback;)V
    invoke-static {v0, v1}, Lorg/chromium/chrome/browser/sync/ChromiumSyncAdapter;->access$200(Lorg/chromium/chrome/browser/sync/ChromiumSyncAdapter;Lorg/chromium/content/browser/BrowserStartupController$StartupCallback;)V

    goto :goto_0
.end method
