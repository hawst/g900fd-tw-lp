.class public abstract Lorg/chromium/chrome/browser/sync/ChromiumSyncAdapter;
.super Landroid/content/AbstractThreadedSyncAdapter;
.source "ChromiumSyncAdapter.java"


# static fields
.field public static final INVALIDATION_OBJECT_ID_KEY:Ljava/lang/String; = "objectId"

.field public static final INVALIDATION_OBJECT_SOURCE_KEY:Ljava/lang/String; = "objectSource"

.field public static final INVALIDATION_PAYLOAD_KEY:Ljava/lang/String; = "payload"

.field public static final INVALIDATION_VERSION_KEY:Ljava/lang/String; = "version"


# instance fields
.field private final mApplication:Landroid/app/Application;

.field private final mAsyncStartup:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/app/Application;)V
    .locals 1

    .prologue
    .line 47
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Landroid/content/AbstractThreadedSyncAdapter;-><init>(Landroid/content/Context;Z)V

    .line 48
    iput-object p2, p0, Lorg/chromium/chrome/browser/sync/ChromiumSyncAdapter;->mApplication:Landroid/app/Application;

    .line 49
    invoke-virtual {p0}, Lorg/chromium/chrome/browser/sync/ChromiumSyncAdapter;->useAsyncStartup()Z

    move-result v0

    iput-boolean v0, p0, Lorg/chromium/chrome/browser/sync/ChromiumSyncAdapter;->mAsyncStartup:Z

    .line 50
    return-void
.end method

.method static synthetic access$000(Lorg/chromium/chrome/browser/sync/ChromiumSyncAdapter;)Z
    .locals 1

    .prologue
    .line 29
    iget-boolean v0, p0, Lorg/chromium/chrome/browser/sync/ChromiumSyncAdapter;->mAsyncStartup:Z

    return v0
.end method

.method static synthetic access$100(Lorg/chromium/chrome/browser/sync/ChromiumSyncAdapter;)Landroid/app/Application;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lorg/chromium/chrome/browser/sync/ChromiumSyncAdapter;->mApplication:Landroid/app/Application;

    return-object v0
.end method

.method static synthetic access$200(Lorg/chromium/chrome/browser/sync/ChromiumSyncAdapter;Lorg/chromium/content/browser/BrowserStartupController$StartupCallback;)V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0, p1}, Lorg/chromium/chrome/browser/sync/ChromiumSyncAdapter;->startBrowserProcessesSync(Lorg/chromium/content/browser/BrowserStartupController$StartupCallback;)V

    return-void
.end method

.method private getStartupCallback(Landroid/content/Context;Landroid/accounts/Account;Landroid/os/Bundle;Landroid/content/SyncResult;Ljava/util/concurrent/Semaphore;)Lorg/chromium/content/browser/BrowserStartupController$StartupCallback;
    .locals 15

    .prologue
    .line 130
    const-string/jumbo v2, "objectId"

    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_0

    const/4 v5, 0x1

    .line 131
    :goto_0
    if-eqz v5, :cond_1

    const/4 v6, 0x0

    .line 132
    :goto_1
    if-eqz v5, :cond_2

    const-string/jumbo v7, ""

    .line 133
    :goto_2
    if-eqz v5, :cond_3

    const-wide/16 v8, 0x0

    .line 134
    :goto_3
    if-eqz v5, :cond_4

    const-string/jumbo v10, ""

    .line 136
    :goto_4
    new-instance v3, Lorg/chromium/chrome/browser/sync/ChromiumSyncAdapter$3;

    move-object v4, p0

    move-object/from16 v11, p5

    move-object/from16 v12, p1

    move-object/from16 v13, p2

    move-object/from16 v14, p4

    invoke-direct/range {v3 .. v14}, Lorg/chromium/chrome/browser/sync/ChromiumSyncAdapter$3;-><init>(Lorg/chromium/chrome/browser/sync/ChromiumSyncAdapter;ZILjava/lang/String;JLjava/lang/String;Ljava/util/concurrent/Semaphore;Landroid/content/Context;Landroid/accounts/Account;Landroid/content/SyncResult;)V

    return-object v3

    .line 130
    :cond_0
    const/4 v5, 0x0

    goto :goto_0

    .line 131
    :cond_1
    const-string/jumbo v2, "objectSource"

    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v6

    goto :goto_1

    .line 132
    :cond_2
    const-string/jumbo v2, "objectId"

    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    goto :goto_2

    .line 133
    :cond_3
    const-string/jumbo v2, "version"

    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v8

    goto :goto_3

    .line 134
    :cond_4
    const-string/jumbo v2, "payload"

    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    goto :goto_4
.end method

.method private startBrowserProcess(Lorg/chromium/content/browser/BrowserStartupController$StartupCallback;Landroid/content/SyncResult;Ljava/util/concurrent/Semaphore;)V
    .locals 6

    .prologue
    .line 85
    :try_start_0
    new-instance v0, Lorg/chromium/chrome/browser/sync/ChromiumSyncAdapter$1;

    invoke-direct {v0, p0, p1}, Lorg/chromium/chrome/browser/sync/ChromiumSyncAdapter$1;-><init>(Lorg/chromium/chrome/browser/sync/ChromiumSyncAdapter;Lorg/chromium/content/browser/BrowserStartupController$StartupCallback;)V

    invoke-static {v0}, Lorg/chromium/base/ThreadUtils;->runOnUiThreadBlocking(Ljava/lang/Runnable;)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 109
    :goto_0
    return-void

    .line 102
    :catch_0
    move-exception v0

    .line 104
    const-string/jumbo v1, "ChromiumSyncAdapter"

    const-string/jumbo v2, "Got exception when trying to request a sync. Informing Android system."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 106
    iget-object v0, p2, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v2, v0, Landroid/content/SyncStats;->numIoExceptions:J

    const-wide/16 v4, 0x1

    add-long/2addr v2, v4

    iput-wide v2, v0, Landroid/content/SyncStats;->numIoExceptions:J

    .line 107
    invoke-virtual {p3}, Ljava/util/concurrent/Semaphore;->release()V

    goto :goto_0
.end method

.method private startBrowserProcessesSync(Lorg/chromium/content/browser/BrowserStartupController$StartupCallback;)V
    .locals 3

    .prologue
    .line 114
    :try_start_0
    iget-object v0, p0, Lorg/chromium/chrome/browser/sync/ChromiumSyncAdapter;->mApplication:Landroid/app/Application;

    invoke-static {v0}, Lorg/chromium/content/browser/BrowserStartupController;->get(Landroid/content/Context;)Lorg/chromium/content/browser/BrowserStartupController;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lorg/chromium/content/browser/BrowserStartupController;->startBrowserProcessesSync(Z)V
    :try_end_0
    .catch Lorg/chromium/base/library_loader/ProcessInitException; {:try_start_0 .. :try_end_0} :catch_0

    .line 119
    :goto_0
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v1, Lorg/chromium/chrome/browser/sync/ChromiumSyncAdapter$2;

    invoke-direct {v1, p0, p1}, Lorg/chromium/chrome/browser/sync/ChromiumSyncAdapter$2;-><init>(Lorg/chromium/chrome/browser/sync/ChromiumSyncAdapter;Lorg/chromium/content/browser/BrowserStartupController$StartupCallback;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 125
    return-void

    .line 115
    :catch_0
    move-exception v0

    .line 116
    const-string/jumbo v1, "ChromiumSyncAdapter"

    const-string/jumbo v2, "Unable to load native library."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 117
    const/4 v0, -0x1

    invoke-static {v0}, Ljava/lang/System;->exit(I)V

    goto :goto_0
.end method


# virtual methods
.method protected abstract initCommandLine()V
.end method

.method public onPerformSync(Landroid/accounts/Account;Landroid/os/Bundle;Ljava/lang/String;Landroid/content/ContentProviderClient;Landroid/content/SyncResult;)V
    .locals 6

    .prologue
    .line 59
    invoke-static {}, Lorg/chromium/chrome/browser/sync/DelayedSyncController;->getInstance()Lorg/chromium/chrome/browser/sync/DelayedSyncController;

    move-result-object v0

    invoke-virtual {p0}, Lorg/chromium/chrome/browser/sync/ChromiumSyncAdapter;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1, p2, p1}, Lorg/chromium/chrome/browser/sync/DelayedSyncController;->shouldPerformSync(Landroid/content/Context;Landroid/os/Bundle;Landroid/accounts/Account;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 79
    :goto_0
    return-void

    .line 64
    :cond_0
    new-instance v5, Ljava/util/concurrent/Semaphore;

    const/4 v0, 0x0

    invoke-direct {v5, v0}, Ljava/util/concurrent/Semaphore;-><init>(I)V

    .line 67
    iget-object v1, p0, Lorg/chromium/chrome/browser/sync/ChromiumSyncAdapter;->mApplication:Landroid/app/Application;

    move-object v0, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p5

    invoke-direct/range {v0 .. v5}, Lorg/chromium/chrome/browser/sync/ChromiumSyncAdapter;->getStartupCallback(Landroid/content/Context;Landroid/accounts/Account;Landroid/os/Bundle;Landroid/content/SyncResult;Ljava/util/concurrent/Semaphore;)Lorg/chromium/content/browser/BrowserStartupController$StartupCallback;

    move-result-object v0

    .line 69
    invoke-direct {p0, v0, p5, v5}, Lorg/chromium/chrome/browser/sync/ChromiumSyncAdapter;->startBrowserProcess(Lorg/chromium/content/browser/BrowserStartupController$StartupCallback;Landroid/content/SyncResult;Ljava/util/concurrent/Semaphore;)V

    .line 73
    :try_start_0
    invoke-virtual {v5}, Ljava/util/concurrent/Semaphore;->acquire()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 74
    :catch_0
    move-exception v0

    .line 75
    const-string/jumbo v1, "ChromiumSyncAdapter"

    const-string/jumbo v2, "Got InterruptedException when trying to request a sync."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 77
    iget-object v0, p5, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v2, v0, Landroid/content/SyncStats;->numIoExceptions:J

    const-wide/16 v4, 0x1

    add-long/2addr v2, v4

    iput-wide v2, v0, Landroid/content/SyncStats;->numIoExceptions:J

    goto :goto_0
.end method

.method public requestSync(ILjava/lang/String;JLjava/lang/String;)V
    .locals 7

    .prologue
    .line 170
    iget-object v0, p0, Lorg/chromium/chrome/browser/sync/ChromiumSyncAdapter;->mApplication:Landroid/app/Application;

    invoke-static {v0}, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->get(Landroid/content/Context;)Lorg/chromium/chrome/browser/sync/ProfileSyncService;

    move-result-object v1

    move v2, p1

    move-object v3, p2

    move-wide v4, p3

    move-object v6, p5

    invoke-virtual/range {v1 .. v6}, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->requestSyncFromNativeChrome(ILjava/lang/String;JLjava/lang/String;)V

    .line 172
    return-void
.end method

.method public requestSyncForAllTypes()V
    .locals 1

    .prologue
    .line 176
    iget-object v0, p0, Lorg/chromium/chrome/browser/sync/ChromiumSyncAdapter;->mApplication:Landroid/app/Application;

    invoke-static {v0}, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->get(Landroid/content/Context;)Lorg/chromium/chrome/browser/sync/ProfileSyncService;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->requestSyncFromNativeChromeForAllTypes()V

    .line 177
    return-void
.end method

.method protected abstract useAsyncStartup()Z
.end method
