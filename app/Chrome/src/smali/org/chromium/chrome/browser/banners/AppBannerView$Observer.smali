.class public interface abstract Lorg/chromium/chrome/browser/banners/AppBannerView$Observer;
.super Ljava/lang/Object;
.source "AppBannerView.java"


# virtual methods
.method public abstract onBannerBlocked(Lorg/chromium/chrome/browser/banners/AppBannerView;Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract onBannerDismissEvent(Lorg/chromium/chrome/browser/banners/AppBannerView;I)V
.end method

.method public abstract onBannerInstallEvent(Lorg/chromium/chrome/browser/banners/AppBannerView;I)V
.end method

.method public abstract onBannerRemoved(Lorg/chromium/chrome/browser/banners/AppBannerView;)V
.end method

.method public abstract onFireIntent(Lorg/chromium/chrome/browser/banners/AppBannerView;Landroid/app/PendingIntent;)Z
.end method
