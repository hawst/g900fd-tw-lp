.class public Lorg/chromium/chrome/browser/Tab;
.super Ljava/lang/Object;
.source "Tab.java"

# interfaces
.implements Lorg/chromium/content/browser/NavigationClient;


# static fields
.field static final synthetic $assertionsDisabled:Z

.field public static final INVALID_TAB_ID:I = -0x1

.field private static final sIdCounter:Ljava/util/concurrent/atomic/AtomicInteger;


# instance fields
.field private mAppBannerManager:Lorg/chromium/chrome/browser/banners/AppBannerManager;

.field private final mApplicationContext:Landroid/content/Context;

.field private mContentViewClient:Lorg/chromium/content/browser/ContentViewClient;

.field private mContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

.field private final mContext:Landroid/content/Context;

.field private mDomDistillerFeedbackReporter:Lorg/chromium/chrome/browser/dom_distiller/DomDistillerFeedbackReporter;

.field private mFavicon:Landroid/graphics/Bitmap;

.field private mFaviconUrl:Ljava/lang/String;

.field private mGroupedWithParent:Z

.field private final mId:I

.field private final mIncognito:Z

.field private mInfoBarContainer:Lorg/chromium/chrome/browser/infobar/InfoBarContainer;

.field private mIsClosing:Z

.field private mNativePage:Lorg/chromium/chrome/browser/NativePage;

.field private mNativeTabAndroid:J

.field private final mObservers:Lorg/chromium/base/ObserverList;

.field private mParentId:I

.field private mSyncId:I

.field private mVoiceSearchTabHelper:Lorg/chromium/chrome/browser/VoiceSearchTabHelper;

.field private mWebContentsDelegate:Lorg/chromium/chrome/browser/Tab$TabChromeWebContentsDelegateAndroid;

.field private mWebContentsObserver:Lorg/chromium/content/browser/WebContentsObserverAndroid;

.field private final mWindowAndroid:Lorg/chromium/ui/base/WindowAndroid;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 74
    const-class v0, Lorg/chromium/chrome/browser/Tab;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/chromium/chrome/browser/Tab;->$assertionsDisabled:Z

    .line 78
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>()V

    sput-object v0, Lorg/chromium/chrome/browser/Tab;->sIdCounter:Ljava/util/concurrent/atomic/AtomicInteger;

    return-void

    .line 74
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(IIZLandroid/content/Context;Lorg/chromium/ui/base/WindowAndroid;)V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 344
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 120
    new-instance v1, Lorg/chromium/base/ObserverList;

    invoke-direct {v1}, Lorg/chromium/base/ObserverList;-><init>()V

    iput-object v1, p0, Lorg/chromium/chrome/browser/Tab;->mObservers:Lorg/chromium/base/ObserverList;

    .line 134
    const/4 v1, -0x1

    iput v1, p0, Lorg/chromium/chrome/browser/Tab;->mParentId:I

    .line 139
    const/4 v1, 0x1

    iput-boolean v1, p0, Lorg/chromium/chrome/browser/Tab;->mGroupedWithParent:Z

    .line 141
    const/4 v1, 0x0

    iput-boolean v1, p0, Lorg/chromium/chrome/browser/Tab;->mIsClosing:Z

    .line 143
    iput-object v0, p0, Lorg/chromium/chrome/browser/Tab;->mFavicon:Landroid/graphics/Bitmap;

    .line 145
    iput-object v0, p0, Lorg/chromium/chrome/browser/Tab;->mFaviconUrl:Ljava/lang/String;

    .line 346
    sget-boolean v1, Lorg/chromium/chrome/browser/Tab;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    if-eqz p4, :cond_0

    instance-of v1, p4, Landroid/app/Activity;

    if-nez v1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 348
    :cond_0
    invoke-static {p1}, Lorg/chromium/chrome/browser/Tab;->generateValidId(I)I

    move-result v1

    iput v1, p0, Lorg/chromium/chrome/browser/Tab;->mId:I

    .line 349
    iput p2, p0, Lorg/chromium/chrome/browser/Tab;->mParentId:I

    .line 350
    iput-boolean p3, p0, Lorg/chromium/chrome/browser/Tab;->mIncognito:Z

    .line 352
    iput-object p4, p0, Lorg/chromium/chrome/browser/Tab;->mContext:Landroid/content/Context;

    .line 353
    if-eqz p4, :cond_1

    invoke-virtual {p4}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    :cond_1
    iput-object v0, p0, Lorg/chromium/chrome/browser/Tab;->mApplicationContext:Landroid/content/Context;

    .line 354
    iput-object p5, p0, Lorg/chromium/chrome/browser/Tab;->mWindowAndroid:Lorg/chromium/ui/base/WindowAndroid;

    .line 355
    return-void
.end method

.method public constructor <init>(IZLandroid/content/Context;Lorg/chromium/ui/base/WindowAndroid;)V
    .locals 6

    .prologue
    .line 333
    const/4 v1, -0x1

    move-object v0, p0

    move v2, p1

    move v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lorg/chromium/chrome/browser/Tab;-><init>(IIZLandroid/content/Context;Lorg/chromium/ui/base/WindowAndroid;)V

    .line 334
    return-void
.end method

.method public constructor <init>(ZLandroid/content/Context;Lorg/chromium/ui/base/WindowAndroid;)V
    .locals 1

    .prologue
    .line 322
    const/4 v0, -0x1

    invoke-direct {p0, v0, p1, p2, p3}, Lorg/chromium/chrome/browser/Tab;-><init>(IZLandroid/content/Context;Lorg/chromium/ui/base/WindowAndroid;)V

    .line 323
    return-void
.end method

.method static synthetic access$000(Lorg/chromium/chrome/browser/Tab;)Z
    .locals 1

    .prologue
    .line 74
    iget-boolean v0, p0, Lorg/chromium/chrome/browser/Tab;->mIncognito:Z

    return v0
.end method

.method static synthetic access$100(Lorg/chromium/chrome/browser/Tab;)Lorg/chromium/base/ObserverList;
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lorg/chromium/chrome/browser/Tab;->mObservers:Lorg/chromium/base/ObserverList;

    return-object v0
.end method

.method static synthetic access$200(Lorg/chromium/chrome/browser/Tab;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lorg/chromium/chrome/browser/Tab;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$300(Lorg/chromium/chrome/browser/Tab;)V
    .locals 0

    .prologue
    .line 74
    invoke-direct {p0}, Lorg/chromium/chrome/browser/Tab;->pushNativePageStateToNavigationEntry()V

    return-void
.end method

.method private clearNativePtr()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 1150
    sget-boolean v0, Lorg/chromium/chrome/browser/Tab;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-wide v0, p0, Lorg/chromium/chrome/browser/Tab;->mNativeTabAndroid:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 1151
    :cond_0
    iput-wide v2, p0, Lorg/chromium/chrome/browser/Tab;->mNativeTabAndroid:J

    .line 1152
    return-void
.end method

.method private destroyNativePageInternal(Lorg/chromium/chrome/browser/NativePage;)V
    .locals 2

    .prologue
    .line 994
    if-nez p1, :cond_0

    .line 998
    :goto_0
    return-void

    .line 995
    :cond_0
    sget-boolean v0, Lorg/chromium/chrome/browser/Tab;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lorg/chromium/chrome/browser/Tab;->mNativePage:Lorg/chromium/chrome/browser/NativePage;

    if-ne p1, v0, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    const-string/jumbo v1, "Attempting to destroy active page."

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 997
    :cond_1
    invoke-interface {p1}, Lorg/chromium/chrome/browser/NativePage;->destroy()V

    goto :goto_0
.end method

.method private static generateNextId()I
    .locals 1

    .prologue
    .line 1182
    sget-object v0, Lorg/chromium/chrome/browser/Tab;->sIdCounter:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->getAndIncrement()I

    move-result v0

    return v0
.end method

.method public static generateValidId(I)I
    .locals 1

    .prologue
    .line 1172
    const/4 v0, -0x1

    if-ne p0, v0, :cond_0

    invoke-static {}, Lorg/chromium/chrome/browser/Tab;->generateNextId()I

    move-result p0

    .line 1173
    :cond_0
    add-int/lit8 v0, p0, 0x1

    invoke-static {v0}, Lorg/chromium/chrome/browser/Tab;->incrementIdCounterTo(I)V

    .line 1175
    return p0
.end method

.method private getNativeInfoBarContainer()J
    .locals 2

    .prologue
    .line 1162
    invoke-virtual {p0}, Lorg/chromium/chrome/browser/Tab;->getInfoBarContainer()Lorg/chromium/chrome/browser/infobar/InfoBarContainer;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/infobar/InfoBarContainer;->getNative()J

    move-result-wide v0

    return-wide v0
.end method

.method public static incrementIdCounterTo(I)V
    .locals 2

    .prologue
    .line 1200
    sget-object v0, Lorg/chromium/chrome/browser/Tab;->sIdCounter:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v0

    sub-int v0, p0, v0

    .line 1201
    if-gtz v0, :cond_0

    .line 1205
    :goto_0
    return-void

    .line 1204
    :cond_0
    sget-object v1, Lorg/chromium/chrome/browser/Tab;->sIdCounter:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v1, v0}, Ljava/util/concurrent/atomic/AtomicInteger;->addAndGet(I)I

    goto :goto_0
.end method

.method private native nativeDestroy(J)V
.end method

.method private native nativeDestroyWebContents(JZ)V
.end method

.method private native nativeGetFavicon(J)Landroid/graphics/Bitmap;
.end method

.method private native nativeGetProfileAndroid(J)Lorg/chromium/chrome/browser/profiles/Profile;
.end method

.method private native nativeGetSecurityLevel(J)I
.end method

.method private native nativeInit()V
.end method

.method private native nativeInitWebContents(JZLorg/chromium/content/browser/ContentViewCore;Lorg/chromium/chrome/browser/ChromeWebContentsDelegateAndroid;Lorg/chromium/chrome/browser/contextmenu/ContextMenuPopulator;)V
.end method

.method private native nativeIsFaviconValid(J)Z
.end method

.method private native nativeLoadUrl(JLjava/lang/String;Ljava/lang/String;[BILjava/lang/String;IZ)I
.end method

.method private native nativePrint(J)Z
.end method

.method private native nativeSetActiveNavigationEntryTitleForUrl(JLjava/lang/String;Ljava/lang/String;)V
.end method

.method private onWebContentsInstantSupportDisabled()V
    .locals 2

    .prologue
    .line 1048
    iget-object v0, p0, Lorg/chromium/chrome/browser/Tab;->mObservers:Lorg/chromium/base/ObserverList;

    invoke-virtual {v0}, Lorg/chromium/base/ObserverList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/chromium/chrome/browser/TabObserver;

    invoke-interface {v0}, Lorg/chromium/chrome/browser/TabObserver;->onWebContentsInstantSupportDisabled()V

    goto :goto_0

    .line 1049
    :cond_0
    return-void
.end method

.method private pushNativePageStateToNavigationEntry()V
    .locals 4

    .prologue
    .line 1186
    sget-boolean v0, Lorg/chromium/chrome/browser/Tab;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    iget-wide v0, p0, Lorg/chromium/chrome/browser/Tab;->mNativeTabAndroid:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lorg/chromium/chrome/browser/Tab;->getNativePage()Lorg/chromium/chrome/browser/NativePage;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 1187
    :cond_1
    iget-wide v0, p0, Lorg/chromium/chrome/browser/Tab;->mNativeTabAndroid:J

    invoke-virtual {p0}, Lorg/chromium/chrome/browser/Tab;->getNativePage()Lorg/chromium/chrome/browser/NativePage;

    move-result-object v2

    invoke-interface {v2}, Lorg/chromium/chrome/browser/NativePage;->getUrl()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lorg/chromium/chrome/browser/Tab;->getNativePage()Lorg/chromium/chrome/browser/NativePage;

    move-result-object v3

    invoke-interface {v3}, Lorg/chromium/chrome/browser/NativePage;->getTitle()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v0, v1, v2, v3}, Lorg/chromium/chrome/browser/Tab;->nativeSetActiveNavigationEntryTitleForUrl(JLjava/lang/String;Ljava/lang/String;)V

    .line 1189
    return-void
.end method

.method private setNativePtr(J)V
    .locals 5

    .prologue
    .line 1156
    sget-boolean v0, Lorg/chromium/chrome/browser/Tab;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-wide v0, p0, Lorg/chromium/chrome/browser/Tab;->mNativeTabAndroid:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 1157
    :cond_0
    iput-wide p1, p0, Lorg/chromium/chrome/browser/Tab;->mNativeTabAndroid:J

    .line 1158
    return-void
.end method

.method private swapWebContents(JZZ)V
    .locals 7

    .prologue
    .line 1103
    new-instance v1, Lorg/chromium/content/browser/ContentViewCore;

    iget-object v0, p0, Lorg/chromium/chrome/browser/Tab;->mContext:Landroid/content/Context;

    invoke-direct {v1, v0}, Lorg/chromium/content/browser/ContentViewCore;-><init>(Landroid/content/Context;)V

    .line 1104
    iget-object v0, p0, Lorg/chromium/chrome/browser/Tab;->mContext:Landroid/content/Context;

    invoke-static {v0, v1}, Lorg/chromium/content/browser/ContentView;->newInstance(Landroid/content/Context;Lorg/chromium/content/browser/ContentViewCore;)Lorg/chromium/content/browser/ContentView;

    move-result-object v2

    .line 1105
    invoke-virtual {p0}, Lorg/chromium/chrome/browser/Tab;->getWindowAndroid()Lorg/chromium/ui/base/WindowAndroid;

    move-result-object v6

    move-object v3, v2

    move-wide v4, p1

    invoke-virtual/range {v1 .. v6}, Lorg/chromium/content/browser/ContentViewCore;->initialize(Landroid/view/ViewGroup;Lorg/chromium/content/browser/ContentViewCore$InternalAccessDelegate;JLorg/chromium/ui/base/WindowAndroid;)V

    .line 1106
    const/4 v0, 0x0

    invoke-virtual {p0, v1, v0, p3, p4}, Lorg/chromium/chrome/browser/Tab;->swapContentViewCore(Lorg/chromium/content/browser/ContentViewCore;ZZZ)V

    .line 1107
    return-void
.end method


# virtual methods
.method public addObserver(Lorg/chromium/chrome/browser/TabObserver;)V
    .locals 1

    .prologue
    .line 362
    iget-object v0, p0, Lorg/chromium/chrome/browser/Tab;->mObservers:Lorg/chromium/base/ObserverList;

    invoke-virtual {v0, p1}, Lorg/chromium/base/ObserverList;->addObserver(Ljava/lang/Object;)Z

    .line 363
    return-void
.end method

.method public canGoBack()Z
    .locals 1

    .prologue
    .line 377
    invoke-virtual {p0}, Lorg/chromium/chrome/browser/Tab;->getWebContents()Lorg/chromium/content_public/browser/WebContents;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lorg/chromium/chrome/browser/Tab;->getWebContents()Lorg/chromium/content_public/browser/WebContents;

    move-result-object v0

    invoke-interface {v0}, Lorg/chromium/content_public/browser/WebContents;->getNavigationController()Lorg/chromium/content_public/browser/NavigationController;

    move-result-object v0

    invoke-interface {v0}, Lorg/chromium/content_public/browser/NavigationController;->canGoBack()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public canGoForward()Z
    .locals 1

    .prologue
    .line 384
    invoke-virtual {p0}, Lorg/chromium/chrome/browser/Tab;->getWebContents()Lorg/chromium/content_public/browser/WebContents;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lorg/chromium/chrome/browser/Tab;->getWebContents()Lorg/chromium/content_public/browser/WebContents;

    move-result-object v0

    invoke-interface {v0}, Lorg/chromium/content_public/browser/WebContents;->getNavigationController()Lorg/chromium/content_public/browser/NavigationController;

    move-result-object v0

    invoke-interface {v0}, Lorg/chromium/content_public/browser/NavigationController;->canGoForward()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected createAutoLoginProcessor()Lorg/chromium/chrome/browser/infobar/AutoLoginProcessor;
    .locals 1

    .prologue
    .line 529
    new-instance v0, Lorg/chromium/chrome/browser/Tab$1;

    invoke-direct {v0, p0}, Lorg/chromium/chrome/browser/Tab$1;-><init>(Lorg/chromium/chrome/browser/Tab;)V

    return-object v0
.end method

.method protected createContextMenuPopulator()Lorg/chromium/chrome/browser/contextmenu/ContextMenuPopulator;
    .locals 2

    .prologue
    .line 1056
    new-instance v0, Lorg/chromium/chrome/browser/contextmenu/ChromeContextMenuPopulator;

    new-instance v1, Lorg/chromium/chrome/browser/Tab$TabChromeContextMenuItemDelegate;

    invoke-direct {v1, p0}, Lorg/chromium/chrome/browser/Tab$TabChromeContextMenuItemDelegate;-><init>(Lorg/chromium/chrome/browser/Tab;)V

    invoke-direct {v0, v1}, Lorg/chromium/chrome/browser/contextmenu/ChromeContextMenuPopulator;-><init>(Lorg/chromium/chrome/browser/contextmenu/ChromeContextMenuItemDelegate;)V

    return-object v0
.end method

.method protected createWebContentsDelegate()Lorg/chromium/chrome/browser/Tab$TabChromeWebContentsDelegateAndroid;
    .locals 1

    .prologue
    .line 1039
    new-instance v0, Lorg/chromium/chrome/browser/Tab$TabChromeWebContentsDelegateAndroid;

    invoke-direct {v0, p0}, Lorg/chromium/chrome/browser/Tab$TabChromeWebContentsDelegateAndroid;-><init>(Lorg/chromium/chrome/browser/Tab;)V

    return-object v0
.end method

.method public destroy()V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const/4 v2, 0x0

    .line 868
    iget-object v0, p0, Lorg/chromium/chrome/browser/Tab;->mObservers:Lorg/chromium/base/ObserverList;

    invoke-virtual {v0}, Lorg/chromium/base/ObserverList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/chromium/chrome/browser/TabObserver;

    invoke-interface {v0, p0}, Lorg/chromium/chrome/browser/TabObserver;->onDestroyed(Lorg/chromium/chrome/browser/Tab;)V

    goto :goto_0

    .line 869
    :cond_0
    iget-object v0, p0, Lorg/chromium/chrome/browser/Tab;->mObservers:Lorg/chromium/base/ObserverList;

    invoke-virtual {v0}, Lorg/chromium/base/ObserverList;->clear()V

    .line 871
    iget-object v0, p0, Lorg/chromium/chrome/browser/Tab;->mNativePage:Lorg/chromium/chrome/browser/NativePage;

    .line 872
    iput-object v2, p0, Lorg/chromium/chrome/browser/Tab;->mNativePage:Lorg/chromium/chrome/browser/NativePage;

    .line 873
    invoke-direct {p0, v0}, Lorg/chromium/chrome/browser/Tab;->destroyNativePageInternal(Lorg/chromium/chrome/browser/NativePage;)V

    .line 874
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lorg/chromium/chrome/browser/Tab;->destroyContentViewCore(Z)V

    .line 880
    sget-boolean v0, Lorg/chromium/chrome/browser/Tab;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    iget-wide v0, p0, Lorg/chromium/chrome/browser/Tab;->mNativeTabAndroid:J

    cmp-long v0, v0, v4

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 881
    :cond_1
    iget-wide v0, p0, Lorg/chromium/chrome/browser/Tab;->mNativeTabAndroid:J

    invoke-direct {p0, v0, v1}, Lorg/chromium/chrome/browser/Tab;->nativeDestroy(J)V

    .line 882
    sget-boolean v0, Lorg/chromium/chrome/browser/Tab;->$assertionsDisabled:Z

    if-nez v0, :cond_2

    iget-wide v0, p0, Lorg/chromium/chrome/browser/Tab;->mNativeTabAndroid:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 884
    :cond_2
    iget-object v0, p0, Lorg/chromium/chrome/browser/Tab;->mInfoBarContainer:Lorg/chromium/chrome/browser/infobar/InfoBarContainer;

    if-eqz v0, :cond_3

    .line 885
    iget-object v0, p0, Lorg/chromium/chrome/browser/Tab;->mInfoBarContainer:Lorg/chromium/chrome/browser/infobar/InfoBarContainer;

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/infobar/InfoBarContainer;->destroy()V

    .line 886
    iput-object v2, p0, Lorg/chromium/chrome/browser/Tab;->mInfoBarContainer:Lorg/chromium/chrome/browser/infobar/InfoBarContainer;

    .line 888
    :cond_3
    return-void
.end method

.method protected final destroyContentViewCore(Z)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1005
    iget-object v0, p0, Lorg/chromium/chrome/browser/Tab;->mContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

    if-nez v0, :cond_0

    .line 1021
    :goto_0
    return-void

    .line 1007
    :cond_0
    iget-object v0, p0, Lorg/chromium/chrome/browser/Tab;->mContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

    invoke-virtual {p0, v0}, Lorg/chromium/chrome/browser/Tab;->destroyContentViewCoreInternal(Lorg/chromium/content/browser/ContentViewCore;)V

    .line 1009
    iget-object v0, p0, Lorg/chromium/chrome/browser/Tab;->mInfoBarContainer:Lorg/chromium/chrome/browser/infobar/InfoBarContainer;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lorg/chromium/chrome/browser/Tab;->mInfoBarContainer:Lorg/chromium/chrome/browser/infobar/InfoBarContainer;

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/infobar/InfoBarContainer;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1010
    iget-object v0, p0, Lorg/chromium/chrome/browser/Tab;->mInfoBarContainer:Lorg/chromium/chrome/browser/infobar/InfoBarContainer;

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/infobar/InfoBarContainer;->removeFromParentView()V

    .line 1012
    :cond_1
    iget-object v0, p0, Lorg/chromium/chrome/browser/Tab;->mContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

    invoke-virtual {v0}, Lorg/chromium/content/browser/ContentViewCore;->destroy()V

    .line 1014
    iput-object v1, p0, Lorg/chromium/chrome/browser/Tab;->mContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

    .line 1015
    iput-object v1, p0, Lorg/chromium/chrome/browser/Tab;->mWebContentsDelegate:Lorg/chromium/chrome/browser/Tab$TabChromeWebContentsDelegateAndroid;

    .line 1016
    iput-object v1, p0, Lorg/chromium/chrome/browser/Tab;->mWebContentsObserver:Lorg/chromium/content/browser/WebContentsObserverAndroid;

    .line 1017
    iput-object v1, p0, Lorg/chromium/chrome/browser/Tab;->mVoiceSearchTabHelper:Lorg/chromium/chrome/browser/VoiceSearchTabHelper;

    .line 1019
    sget-boolean v0, Lorg/chromium/chrome/browser/Tab;->$assertionsDisabled:Z

    if-nez v0, :cond_2

    iget-wide v0, p0, Lorg/chromium/chrome/browser/Tab;->mNativeTabAndroid:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 1020
    :cond_2
    iget-wide v0, p0, Lorg/chromium/chrome/browser/Tab;->mNativeTabAndroid:J

    invoke-direct {p0, v0, v1, p1}, Lorg/chromium/chrome/browser/Tab;->nativeDestroyWebContents(JZ)V

    goto :goto_0
.end method

.method protected destroyContentViewCoreInternal(Lorg/chromium/content/browser/ContentViewCore;)V
    .locals 0

    .prologue
    .line 1032
    return-void
.end method

.method public freezeNativePage()V
    .locals 2

    .prologue
    .line 751
    iget-object v0, p0, Lorg/chromium/chrome/browser/Tab;->mNativePage:Lorg/chromium/chrome/browser/NativePage;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/chromium/chrome/browser/Tab;->mNativePage:Lorg/chromium/chrome/browser/NativePage;

    instance-of v0, v0, Lorg/chromium/chrome/browser/FrozenNativePage;

    if-eqz v0, :cond_1

    .line 754
    :cond_0
    :goto_0
    return-void

    .line 752
    :cond_1
    sget-boolean v0, Lorg/chromium/chrome/browser/Tab;->$assertionsDisabled:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lorg/chromium/chrome/browser/Tab;->mNativePage:Lorg/chromium/chrome/browser/NativePage;

    invoke-interface {v0}, Lorg/chromium/chrome/browser/NativePage;->getView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-eqz v0, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    const-string/jumbo v1, "Cannot freeze visible native page"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 753
    :cond_2
    iget-object v0, p0, Lorg/chromium/chrome/browser/Tab;->mNativePage:Lorg/chromium/chrome/browser/NativePage;

    invoke-static {v0}, Lorg/chromium/chrome/browser/FrozenNativePage;->freeze(Lorg/chromium/chrome/browser/NativePage;)Lorg/chromium/chrome/browser/FrozenNativePage;

    move-result-object v0

    iput-object v0, p0, Lorg/chromium/chrome/browser/Tab;->mNativePage:Lorg/chromium/chrome/browser/NativePage;

    goto :goto_0
.end method

.method protected getApplicationContext()Landroid/content/Context;
    .locals 1

    .prologue
    .line 514
    iget-object v0, p0, Lorg/chromium/chrome/browser/Tab;->mApplicationContext:Landroid/content/Context;

    return-object v0
.end method

.method public getBackgroundColor()I
    .locals 1

    .prologue
    .line 574
    iget-object v0, p0, Lorg/chromium/chrome/browser/Tab;->mNativePage:Lorg/chromium/chrome/browser/NativePage;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/chromium/chrome/browser/Tab;->mNativePage:Lorg/chromium/chrome/browser/NativePage;

    invoke-interface {v0}, Lorg/chromium/chrome/browser/NativePage;->getBackgroundColor()I

    move-result v0

    .line 576
    :goto_0
    return v0

    .line 575
    :cond_0
    invoke-virtual {p0}, Lorg/chromium/chrome/browser/Tab;->getWebContents()Lorg/chromium/content_public/browser/WebContents;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lorg/chromium/chrome/browser/Tab;->getWebContents()Lorg/chromium/content_public/browser/WebContents;

    move-result-object v0

    invoke-interface {v0}, Lorg/chromium/content_public/browser/WebContents;->getBackgroundColor()I

    move-result v0

    goto :goto_0

    .line 576
    :cond_1
    const/4 v0, -0x1

    goto :goto_0
.end method

.method protected getChromeWebContentsDelegateAndroid()Lorg/chromium/chrome/browser/Tab$TabChromeWebContentsDelegateAndroid;
    .locals 1

    .prologue
    .line 1070
    iget-object v0, p0, Lorg/chromium/chrome/browser/Tab;->mWebContentsDelegate:Lorg/chromium/chrome/browser/Tab$TabChromeWebContentsDelegateAndroid;

    return-object v0
.end method

.method protected getContentViewClient()Lorg/chromium/content/browser/ContentViewClient;
    .locals 1

    .prologue
    .line 696
    iget-object v0, p0, Lorg/chromium/chrome/browser/Tab;->mContentViewClient:Lorg/chromium/content/browser/ContentViewClient;

    return-object v0
.end method

.method public getContentViewCore()Lorg/chromium/content/browser/ContentViewCore;
    .locals 1

    .prologue
    .line 616
    iget-object v0, p0, Lorg/chromium/chrome/browser/Tab;->mNativePage:Lorg/chromium/chrome/browser/NativePage;

    if-nez v0, :cond_0

    iget-object v0, p0, Lorg/chromium/chrome/browser/Tab;->mContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getDirectedNavigationHistory(ZI)Lorg/chromium/content_public/browser/NavigationHistory;
    .locals 1

    .prologue
    .line 404
    invoke-virtual {p0}, Lorg/chromium/chrome/browser/Tab;->getWebContents()Lorg/chromium/content_public/browser/WebContents;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 405
    invoke-virtual {p0}, Lorg/chromium/chrome/browser/Tab;->getWebContents()Lorg/chromium/content_public/browser/WebContents;

    move-result-object v0

    invoke-interface {v0}, Lorg/chromium/content_public/browser/WebContents;->getNavigationController()Lorg/chromium/content_public/browser/NavigationController;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Lorg/chromium/content_public/browser/NavigationController;->getDirectedNavigationHistory(ZI)Lorg/chromium/content_public/browser/NavigationHistory;

    move-result-object v0

    .line 408
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lorg/chromium/content_public/browser/NavigationHistory;

    invoke-direct {v0}, Lorg/chromium/content_public/browser/NavigationHistory;-><init>()V

    goto :goto_0
.end method

.method public getFavicon()Landroid/graphics/Bitmap;
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 920
    invoke-virtual {p0}, Lorg/chromium/chrome/browser/Tab;->getUrl()Ljava/lang/String;

    move-result-object v1

    .line 922
    if-eqz v1, :cond_0

    iget-object v2, p0, Lorg/chromium/chrome/browser/Tab;->mFaviconUrl:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 923
    :cond_0
    iput-object v0, p0, Lorg/chromium/chrome/browser/Tab;->mFavicon:Landroid/graphics/Bitmap;

    .line 924
    iput-object v0, p0, Lorg/chromium/chrome/browser/Tab;->mFaviconUrl:Ljava/lang/String;

    .line 927
    :cond_1
    iget-object v2, p0, Lorg/chromium/chrome/browser/Tab;->mFavicon:Landroid/graphics/Bitmap;

    if-nez v2, :cond_4

    .line 929
    invoke-virtual {p0}, Lorg/chromium/chrome/browser/Tab;->getNativePage()Lorg/chromium/chrome/browser/NativePage;

    move-result-object v2

    if-nez v2, :cond_3

    invoke-virtual {p0}, Lorg/chromium/chrome/browser/Tab;->getContentViewCore()Lorg/chromium/content/browser/ContentViewCore;

    move-result-object v2

    if-nez v2, :cond_3

    .line 943
    :cond_2
    :goto_0
    return-object v0

    .line 931
    :cond_3
    iget-wide v2, p0, Lorg/chromium/chrome/browser/Tab;->mNativeTabAndroid:J

    invoke-direct {p0, v2, v3}, Lorg/chromium/chrome/browser/Tab;->nativeGetFavicon(J)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 935
    if-eqz v0, :cond_2

    iget-wide v2, p0, Lorg/chromium/chrome/browser/Tab;->mNativeTabAndroid:J

    invoke-direct {p0, v2, v3}, Lorg/chromium/chrome/browser/Tab;->nativeIsFaviconValid(J)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 936
    iput-object v0, p0, Lorg/chromium/chrome/browser/Tab;->mFavicon:Landroid/graphics/Bitmap;

    .line 937
    iput-object v1, p0, Lorg/chromium/chrome/browser/Tab;->mFaviconUrl:Ljava/lang/String;

    goto :goto_0

    .line 943
    :cond_4
    iget-object v0, p0, Lorg/chromium/chrome/browser/Tab;->mFavicon:Landroid/graphics/Bitmap;

    goto :goto_0
.end method

.method public getHeight()I
    .locals 1

    .prologue
    .line 506
    invoke-virtual {p0}, Lorg/chromium/chrome/browser/Tab;->getView()Landroid/view/View;

    move-result-object v0

    .line 507
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getId()I
    .locals 1

    .prologue
    .line 601
    iget v0, p0, Lorg/chromium/chrome/browser/Tab;->mId:I

    return v0
.end method

.method public final getInfoBarContainer()Lorg/chromium/chrome/browser/infobar/InfoBarContainer;
    .locals 1

    .prologue
    .line 521
    iget-object v0, p0, Lorg/chromium/chrome/browser/Tab;->mInfoBarContainer:Lorg/chromium/chrome/browser/infobar/InfoBarContainer;

    return-object v0
.end method

.method public getNativePage()Lorg/chromium/chrome/browser/NativePage;
    .locals 1

    .prologue
    .line 625
    iget-object v0, p0, Lorg/chromium/chrome/browser/Tab;->mNativePage:Lorg/chromium/chrome/browser/NativePage;

    return-object v0
.end method

.method protected getNativePtr()J
    .locals 2

    .prologue
    .line 1096
    iget-wide v0, p0, Lorg/chromium/chrome/browser/Tab;->mNativeTabAndroid:J

    return-wide v0
.end method

.method public getParentId()I
    .locals 1

    .prologue
    .line 973
    iget v0, p0, Lorg/chromium/chrome/browser/Tab;->mParentId:I

    return v0
.end method

.method public getProfile()Lorg/chromium/chrome/browser/profiles/Profile;
    .locals 4

    .prologue
    .line 590
    iget-wide v0, p0, Lorg/chromium/chrome/browser/Tab;->mNativeTabAndroid:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 591
    :goto_0
    return-object v0

    :cond_0
    iget-wide v0, p0, Lorg/chromium/chrome/browser/Tab;->mNativeTabAndroid:J

    invoke-direct {p0, v0, v1}, Lorg/chromium/chrome/browser/Tab;->nativeGetProfileAndroid(J)Lorg/chromium/chrome/browser/profiles/Profile;

    move-result-object v0

    goto :goto_0
.end method

.method public getSecurityLevel()I
    .locals 4

    .prologue
    .line 660
    iget-wide v0, p0, Lorg/chromium/chrome/browser/Tab;->mNativeTabAndroid:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 661
    :goto_0
    return v0

    :cond_0
    iget-wide v0, p0, Lorg/chromium/chrome/browser/Tab;->mNativeTabAndroid:J

    invoke-direct {p0, v0, v1}, Lorg/chromium/chrome/browser/Tab;->nativeGetSecurityLevel(J)I

    move-result v0

    goto :goto_0
.end method

.method protected getSyncId()I
    .locals 1

    .prologue
    .line 669
    iget v0, p0, Lorg/chromium/chrome/browser/Tab;->mSyncId:I

    return v0
.end method

.method protected getTabObservers()Lorg/chromium/base/ObserverList$RewindableIterator;
    .locals 1

    .prologue
    .line 687
    iget-object v0, p0, Lorg/chromium/chrome/browser/Tab;->mObservers:Lorg/chromium/base/ObserverList;

    invoke-virtual {v0}, Lorg/chromium/base/ObserverList;->rewindableIterator()Lorg/chromium/base/ObserverList$RewindableIterator;

    move-result-object v0

    return-object v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 910
    iget-object v0, p0, Lorg/chromium/chrome/browser/Tab;->mNativePage:Lorg/chromium/chrome/browser/NativePage;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/chromium/chrome/browser/Tab;->mNativePage:Lorg/chromium/chrome/browser/NativePage;

    invoke-interface {v0}, Lorg/chromium/chrome/browser/NativePage;->getTitle()Ljava/lang/String;

    move-result-object v0

    .line 912
    :goto_0
    return-object v0

    .line 911
    :cond_0
    invoke-virtual {p0}, Lorg/chromium/chrome/browser/Tab;->getWebContents()Lorg/chromium/content_public/browser/WebContents;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lorg/chromium/chrome/browser/Tab;->getWebContents()Lorg/chromium/content_public/browser/WebContents;

    move-result-object v0

    invoke-interface {v0}, Lorg/chromium/content_public/browser/WebContents;->getTitle()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 912
    :cond_1
    const-string/jumbo v0, ""

    goto :goto_0
.end method

.method public getUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 902
    invoke-virtual {p0}, Lorg/chromium/chrome/browser/Tab;->getWebContents()Lorg/chromium/content_public/browser/WebContents;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lorg/chromium/chrome/browser/Tab;->getWebContents()Lorg/chromium/content_public/browser/WebContents;

    move-result-object v0

    invoke-interface {v0}, Lorg/chromium/content_public/browser/WebContents;->getUrl()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const-string/jumbo v0, ""

    goto :goto_0
.end method

.method public getUseDesktopUserAgent()Z
    .locals 1

    .prologue
    .line 652
    invoke-virtual {p0}, Lorg/chromium/chrome/browser/Tab;->getWebContents()Lorg/chromium/content_public/browser/WebContents;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lorg/chromium/chrome/browser/Tab;->getWebContents()Lorg/chromium/content_public/browser/WebContents;

    move-result-object v0

    invoke-interface {v0}, Lorg/chromium/content_public/browser/WebContents;->getNavigationController()Lorg/chromium/content_public/browser/NavigationController;

    move-result-object v0

    invoke-interface {v0}, Lorg/chromium/content_public/browser/NavigationController;->getUseDesktopUserAgent()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getView()Landroid/view/View;
    .locals 1

    .prologue
    .line 490
    iget-object v0, p0, Lorg/chromium/chrome/browser/Tab;->mNativePage:Lorg/chromium/chrome/browser/NativePage;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/chromium/chrome/browser/Tab;->mNativePage:Lorg/chromium/chrome/browser/NativePage;

    invoke-interface {v0}, Lorg/chromium/chrome/browser/NativePage;->getView()Landroid/view/View;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lorg/chromium/chrome/browser/Tab;->mContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lorg/chromium/chrome/browser/Tab;->mContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

    invoke-virtual {v0}, Lorg/chromium/content/browser/ContentViewCore;->getContainerView()Landroid/view/ViewGroup;

    move-result-object v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getWebContents()Lorg/chromium/content_public/browser/WebContents;
    .locals 1

    .prologue
    .line 583
    iget-object v0, p0, Lorg/chromium/chrome/browser/Tab;->mContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/chromium/chrome/browser/Tab;->mContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

    invoke-virtual {v0}, Lorg/chromium/content/browser/ContentViewCore;->getWebContents()Lorg/chromium/content_public/browser/WebContents;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getWidth()I
    .locals 1

    .prologue
    .line 498
    invoke-virtual {p0}, Lorg/chromium/chrome/browser/Tab;->getView()Landroid/view/View;

    move-result-object v0

    .line 499
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getWindowAndroid()Lorg/chromium/ui/base/WindowAndroid;
    .locals 1

    .prologue
    .line 1063
    iget-object v0, p0, Lorg/chromium/chrome/browser/Tab;->mWindowAndroid:Lorg/chromium/ui/base/WindowAndroid;

    return-object v0
.end method

.method public goBack()V
    .locals 1

    .prologue
    .line 392
    invoke-virtual {p0}, Lorg/chromium/chrome/browser/Tab;->getWebContents()Lorg/chromium/content_public/browser/WebContents;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lorg/chromium/chrome/browser/Tab;->getWebContents()Lorg/chromium/content_public/browser/WebContents;

    move-result-object v0

    invoke-interface {v0}, Lorg/chromium/content_public/browser/WebContents;->getNavigationController()Lorg/chromium/content_public/browser/NavigationController;

    move-result-object v0

    invoke-interface {v0}, Lorg/chromium/content_public/browser/NavigationController;->goBack()V

    .line 393
    :cond_0
    return-void
.end method

.method public goForward()V
    .locals 1

    .prologue
    .line 399
    invoke-virtual {p0}, Lorg/chromium/chrome/browser/Tab;->getWebContents()Lorg/chromium/content_public/browser/WebContents;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lorg/chromium/chrome/browser/Tab;->getWebContents()Lorg/chromium/content_public/browser/WebContents;

    move-result-object v0

    invoke-interface {v0}, Lorg/chromium/content_public/browser/WebContents;->getNavigationController()Lorg/chromium/content_public/browser/NavigationController;

    move-result-object v0

    invoke-interface {v0}, Lorg/chromium/content_public/browser/NavigationController;->goForward()V

    .line 400
    :cond_0
    return-void
.end method

.method public goToNavigationIndex(I)V
    .locals 1

    .prologue
    .line 414
    invoke-virtual {p0}, Lorg/chromium/chrome/browser/Tab;->getWebContents()Lorg/chromium/content_public/browser/WebContents;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 415
    invoke-virtual {p0}, Lorg/chromium/chrome/browser/Tab;->getWebContents()Lorg/chromium/content_public/browser/WebContents;

    move-result-object v0

    invoke-interface {v0}, Lorg/chromium/content_public/browser/WebContents;->getNavigationController()Lorg/chromium/content_public/browser/NavigationController;

    move-result-object v0

    invoke-interface {v0, p1}, Lorg/chromium/content_public/browser/NavigationController;->goToNavigationIndex(I)V

    .line 417
    :cond_0
    return-void
.end method

.method protected hide()V
    .locals 1

    .prologue
    .line 730
    iget-object v0, p0, Lorg/chromium/chrome/browser/Tab;->mContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/chromium/chrome/browser/Tab;->mContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

    invoke-virtual {v0}, Lorg/chromium/content/browser/ContentViewCore;->onHide()V

    .line 731
    :cond_0
    return-void
.end method

.method protected initContentViewCore(J)V
    .locals 7

    .prologue
    .line 798
    new-instance v1, Lorg/chromium/content/browser/ContentViewCore;

    iget-object v0, p0, Lorg/chromium/chrome/browser/Tab;->mContext:Landroid/content/Context;

    invoke-direct {v1, v0}, Lorg/chromium/content/browser/ContentViewCore;-><init>(Landroid/content/Context;)V

    .line 799
    iget-object v0, p0, Lorg/chromium/chrome/browser/Tab;->mContext:Landroid/content/Context;

    invoke-static {v0, v1}, Lorg/chromium/content/browser/ContentView;->newInstance(Landroid/content/Context;Lorg/chromium/content/browser/ContentViewCore;)Lorg/chromium/content/browser/ContentView;

    move-result-object v2

    .line 800
    invoke-virtual {p0}, Lorg/chromium/chrome/browser/Tab;->getWindowAndroid()Lorg/chromium/ui/base/WindowAndroid;

    move-result-object v6

    move-object v3, v2

    move-wide v4, p1

    invoke-virtual/range {v1 .. v6}, Lorg/chromium/content/browser/ContentViewCore;->initialize(Landroid/view/ViewGroup;Lorg/chromium/content/browser/ContentViewCore$InternalAccessDelegate;JLorg/chromium/ui/base/WindowAndroid;)V

    .line 801
    invoke-virtual {p0, v1}, Lorg/chromium/chrome/browser/Tab;->setContentViewCore(Lorg/chromium/content/browser/ContentViewCore;)V

    .line 802
    return-void
.end method

.method public initialize()V
    .locals 0

    .prologue
    .line 771
    invoke-virtual {p0}, Lorg/chromium/chrome/browser/Tab;->initializeNative()V

    .line 772
    return-void
.end method

.method protected initializeNative()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 780
    iget-wide v0, p0, Lorg/chromium/chrome/browser/Tab;->mNativeTabAndroid:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    invoke-direct {p0}, Lorg/chromium/chrome/browser/Tab;->nativeInit()V

    .line 781
    :cond_0
    sget-boolean v0, Lorg/chromium/chrome/browser/Tab;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    iget-wide v0, p0, Lorg/chromium/chrome/browser/Tab;->mNativeTabAndroid:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 782
    :cond_1
    return-void
.end method

.method public isClosing()Z
    .locals 1

    .prologue
    .line 959
    iget-boolean v0, p0, Lorg/chromium/chrome/browser/Tab;->mIsClosing:Z

    return v0
.end method

.method public isGroupedWithParent()Z
    .locals 1

    .prologue
    .line 980
    iget-boolean v0, p0, Lorg/chromium/chrome/browser/Tab;->mGroupedWithParent:Z

    return v0
.end method

.method public isIncognito()Z
    .locals 1

    .prologue
    .line 608
    iget-boolean v0, p0, Lorg/chromium/chrome/browser/Tab;->mIncognito:Z

    return v0
.end method

.method public isInitialized()Z
    .locals 4

    .prologue
    .line 894
    iget-wide v0, p0, Lorg/chromium/chrome/browser/Tab;->mNativeTabAndroid:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isNativePage()Z
    .locals 1

    .prologue
    .line 632
    iget-object v0, p0, Lorg/chromium/chrome/browser/Tab;->mNativePage:Lorg/chromium/chrome/browser/NativePage;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isReady()Z
    .locals 1

    .prologue
    .line 481
    iget-object v0, p0, Lorg/chromium/chrome/browser/Tab;->mNativePage:Lorg/chromium/chrome/browser/NativePage;

    if-nez v0, :cond_0

    iget-object v0, p0, Lorg/chromium/chrome/browser/Tab;->mContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lorg/chromium/chrome/browser/Tab;->mContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

    invoke-virtual {v0}, Lorg/chromium/content/browser/ContentViewCore;->isReady()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isShowingInterstitialPage()Z
    .locals 1

    .prologue
    .line 474
    invoke-virtual {p0}, Lorg/chromium/chrome/browser/Tab;->getWebContents()Lorg/chromium/content_public/browser/WebContents;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lorg/chromium/chrome/browser/Tab;->getWebContents()Lorg/chromium/content_public/browser/WebContents;

    move-result-object v0

    invoke-interface {v0}, Lorg/chromium/content_public/browser/WebContents;->isShowingInterstitialPage()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public loadIfNecessary()V
    .locals 1

    .prologue
    .line 423
    invoke-virtual {p0}, Lorg/chromium/chrome/browser/Tab;->getWebContents()Lorg/chromium/content_public/browser/WebContents;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lorg/chromium/chrome/browser/Tab;->getWebContents()Lorg/chromium/content_public/browser/WebContents;

    move-result-object v0

    invoke-interface {v0}, Lorg/chromium/content_public/browser/WebContents;->getNavigationController()Lorg/chromium/content_public/browser/NavigationController;

    move-result-object v0

    invoke-interface {v0}, Lorg/chromium/content_public/browser/NavigationController;->loadIfNecessary()V

    .line 424
    :cond_0
    return-void
.end method

.method public loadIfNeeded()Z
    .locals 1

    .prologue
    .line 952
    const/4 v0, 0x0

    return v0
.end method

.method public loadUrl(Lorg/chromium/content_public/browser/LoadUrlParams;)I
    .locals 11

    .prologue
    .line 444
    invoke-static {}, Lorg/chromium/base/TraceEvent;->begin()V

    .line 448
    iget-wide v2, p0, Lorg/chromium/chrome/browser/Tab;->mNativeTabAndroid:J

    invoke-virtual {p1}, Lorg/chromium/content_public/browser/LoadUrlParams;->getUrl()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1}, Lorg/chromium/content_public/browser/LoadUrlParams;->getVerbatimHeaders()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1}, Lorg/chromium/content_public/browser/LoadUrlParams;->getPostData()[B

    move-result-object v6

    invoke-virtual {p1}, Lorg/chromium/content_public/browser/LoadUrlParams;->getTransitionType()I

    move-result v7

    invoke-virtual {p1}, Lorg/chromium/content_public/browser/LoadUrlParams;->getReferrer()Lorg/chromium/content_public/common/Referrer;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lorg/chromium/content_public/browser/LoadUrlParams;->getReferrer()Lorg/chromium/content_public/common/Referrer;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/content_public/common/Referrer;->getUrl()Ljava/lang/String;

    move-result-object v8

    :goto_0
    invoke-virtual {p1}, Lorg/chromium/content_public/browser/LoadUrlParams;->getReferrer()Lorg/chromium/content_public/common/Referrer;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lorg/chromium/content_public/browser/LoadUrlParams;->getReferrer()Lorg/chromium/content_public/common/Referrer;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/content_public/common/Referrer;->getPolicy()I

    move-result v9

    :goto_1
    invoke-virtual {p1}, Lorg/chromium/content_public/browser/LoadUrlParams;->getIsRendererInitiated()Z

    move-result v10

    move-object v1, p0

    invoke-direct/range {v1 .. v10}, Lorg/chromium/chrome/browser/Tab;->nativeLoadUrl(JLjava/lang/String;Ljava/lang/String;[BILjava/lang/String;IZ)I

    move-result v1

    .line 461
    invoke-static {}, Lorg/chromium/base/TraceEvent;->end()V

    .line 463
    iget-object v0, p0, Lorg/chromium/chrome/browser/Tab;->mObservers:Lorg/chromium/base/ObserverList;

    invoke-virtual {v0}, Lorg/chromium/base/ObserverList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/chromium/chrome/browser/TabObserver;

    .line 464
    invoke-virtual {p1}, Lorg/chromium/content_public/browser/LoadUrlParams;->getUrl()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, p0, v3, v1}, Lorg/chromium/chrome/browser/TabObserver;->onLoadUrl(Lorg/chromium/chrome/browser/Tab;Ljava/lang/String;I)V

    goto :goto_2

    .line 448
    :cond_0
    const/4 v8, 0x0

    goto :goto_0

    :cond_1
    const/4 v9, 0x0

    goto :goto_1

    .line 466
    :cond_2
    return v1
.end method

.method protected onFaviconUpdated()V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1078
    iput-object v0, p0, Lorg/chromium/chrome/browser/Tab;->mFavicon:Landroid/graphics/Bitmap;

    .line 1079
    iput-object v0, p0, Lorg/chromium/chrome/browser/Tab;->mFaviconUrl:Ljava/lang/String;

    .line 1080
    iget-object v0, p0, Lorg/chromium/chrome/browser/Tab;->mObservers:Lorg/chromium/base/ObserverList;

    invoke-virtual {v0}, Lorg/chromium/base/ObserverList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/chromium/chrome/browser/TabObserver;

    invoke-interface {v0, p0}, Lorg/chromium/chrome/browser/TabObserver;->onFaviconUpdated(Lorg/chromium/chrome/browser/Tab;)V

    goto :goto_0

    .line 1081
    :cond_0
    return-void
.end method

.method protected onNavEntryChanged()V
    .locals 0

    .prologue
    .line 1089
    return-void
.end method

.method public print()Z
    .locals 4

    .prologue
    .line 543
    sget-boolean v0, Lorg/chromium/chrome/browser/Tab;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-wide v0, p0, Lorg/chromium/chrome/browser/Tab;->mNativeTabAndroid:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 544
    :cond_0
    iget-wide v0, p0, Lorg/chromium/chrome/browser/Tab;->mNativeTabAndroid:J

    invoke-direct {p0, v0, v1}, Lorg/chromium/chrome/browser/Tab;->nativePrint(J)Z

    move-result v0

    return v0
.end method

.method public reload()V
    .locals 2

    .prologue
    .line 552
    invoke-virtual {p0}, Lorg/chromium/chrome/browser/Tab;->getWebContents()Lorg/chromium/content_public/browser/WebContents;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lorg/chromium/chrome/browser/Tab;->getWebContents()Lorg/chromium/content_public/browser/WebContents;

    move-result-object v0

    invoke-interface {v0}, Lorg/chromium/content_public/browser/WebContents;->getNavigationController()Lorg/chromium/content_public/browser/NavigationController;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lorg/chromium/content_public/browser/NavigationController;->reload(Z)V

    .line 553
    :cond_0
    return-void
.end method

.method public reloadIgnoringCache()V
    .locals 2

    .prologue
    .line 560
    invoke-virtual {p0}, Lorg/chromium/chrome/browser/Tab;->getWebContents()Lorg/chromium/content_public/browser/WebContents;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 561
    invoke-virtual {p0}, Lorg/chromium/chrome/browser/Tab;->getWebContents()Lorg/chromium/content_public/browser/WebContents;

    move-result-object v0

    invoke-interface {v0}, Lorg/chromium/content_public/browser/WebContents;->getNavigationController()Lorg/chromium/content_public/browser/NavigationController;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lorg/chromium/content_public/browser/NavigationController;->reloadIgnoringCache(Z)V

    .line 563
    :cond_0
    return-void
.end method

.method public removeObserver(Lorg/chromium/chrome/browser/TabObserver;)V
    .locals 1

    .prologue
    .line 370
    iget-object v0, p0, Lorg/chromium/chrome/browser/Tab;->mObservers:Lorg/chromium/base/ObserverList;

    invoke-virtual {v0, p1}, Lorg/chromium/base/ObserverList;->removeObserver(Ljava/lang/Object;)Z

    .line 371
    return-void
.end method

.method protected requestRestoreLoad()V
    .locals 1

    .prologue
    .line 430
    invoke-virtual {p0}, Lorg/chromium/chrome/browser/Tab;->getWebContents()Lorg/chromium/content_public/browser/WebContents;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 431
    invoke-virtual {p0}, Lorg/chromium/chrome/browser/Tab;->getWebContents()Lorg/chromium/content_public/browser/WebContents;

    move-result-object v0

    invoke-interface {v0}, Lorg/chromium/content_public/browser/WebContents;->getNavigationController()Lorg/chromium/content_public/browser/NavigationController;

    move-result-object v0

    invoke-interface {v0}, Lorg/chromium/content_public/browser/NavigationController;->requestRestoreLoad()V

    .line 433
    :cond_0
    return-void
.end method

.method public setClosing(Z)V
    .locals 0

    .prologue
    .line 966
    iput-boolean p1, p0, Lorg/chromium/chrome/browser/Tab;->mIsClosing:Z

    .line 967
    return-void
.end method

.method protected setContentViewClient(Lorg/chromium/content/browser/ContentViewClient;)V
    .locals 2

    .prologue
    .line 704
    iget-object v0, p0, Lorg/chromium/chrome/browser/Tab;->mContentViewClient:Lorg/chromium/content/browser/ContentViewClient;

    if-ne v0, p1, :cond_1

    .line 717
    :cond_0
    :goto_0
    return-void

    .line 706
    :cond_1
    iget-object v0, p0, Lorg/chromium/chrome/browser/Tab;->mContentViewClient:Lorg/chromium/content/browser/ContentViewClient;

    .line 707
    iput-object p1, p0, Lorg/chromium/chrome/browser/Tab;->mContentViewClient:Lorg/chromium/content/browser/ContentViewClient;

    .line 709
    iget-object v1, p0, Lorg/chromium/chrome/browser/Tab;->mContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

    if-eqz v1, :cond_0

    .line 711
    iget-object v1, p0, Lorg/chromium/chrome/browser/Tab;->mContentViewClient:Lorg/chromium/content/browser/ContentViewClient;

    if-eqz v1, :cond_2

    .line 712
    iget-object v0, p0, Lorg/chromium/chrome/browser/Tab;->mContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

    iget-object v1, p0, Lorg/chromium/chrome/browser/Tab;->mContentViewClient:Lorg/chromium/content/browser/ContentViewClient;

    invoke-virtual {v0, v1}, Lorg/chromium/content/browser/ContentViewCore;->setContentViewClient(Lorg/chromium/content/browser/ContentViewClient;)V

    goto :goto_0

    .line 713
    :cond_2
    if-eqz v0, :cond_0

    .line 715
    iget-object v0, p0, Lorg/chromium/chrome/browser/Tab;->mContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

    new-instance v1, Lorg/chromium/content/browser/ContentViewClient;

    invoke-direct {v1}, Lorg/chromium/content/browser/ContentViewClient;-><init>()V

    invoke-virtual {v0, v1}, Lorg/chromium/content/browser/ContentViewCore;->setContentViewClient(Lorg/chromium/content/browser/ContentViewClient;)V

    goto :goto_0
.end method

.method protected setContentViewCore(Lorg/chromium/content/browser/ContentViewCore;)V
    .locals 8

    .prologue
    .line 815
    iget-object v0, p0, Lorg/chromium/chrome/browser/Tab;->mNativePage:Lorg/chromium/chrome/browser/NativePage;

    .line 816
    const/4 v1, 0x0

    iput-object v1, p0, Lorg/chromium/chrome/browser/Tab;->mNativePage:Lorg/chromium/chrome/browser/NativePage;

    .line 817
    invoke-direct {p0, v0}, Lorg/chromium/chrome/browser/Tab;->destroyNativePageInternal(Lorg/chromium/chrome/browser/NativePage;)V

    .line 819
    iput-object p1, p0, Lorg/chromium/chrome/browser/Tab;->mContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

    .line 821
    invoke-virtual {p0}, Lorg/chromium/chrome/browser/Tab;->createWebContentsDelegate()Lorg/chromium/chrome/browser/Tab$TabChromeWebContentsDelegateAndroid;

    move-result-object v0

    iput-object v0, p0, Lorg/chromium/chrome/browser/Tab;->mWebContentsDelegate:Lorg/chromium/chrome/browser/Tab$TabChromeWebContentsDelegateAndroid;

    .line 822
    new-instance v0, Lorg/chromium/chrome/browser/Tab$TabWebContentsObserverAndroid;

    iget-object v1, p0, Lorg/chromium/chrome/browser/Tab;->mContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

    invoke-virtual {v1}, Lorg/chromium/content/browser/ContentViewCore;->getWebContents()Lorg/chromium/content_public/browser/WebContents;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lorg/chromium/chrome/browser/Tab$TabWebContentsObserverAndroid;-><init>(Lorg/chromium/chrome/browser/Tab;Lorg/chromium/content_public/browser/WebContents;)V

    iput-object v0, p0, Lorg/chromium/chrome/browser/Tab;->mWebContentsObserver:Lorg/chromium/content/browser/WebContentsObserverAndroid;

    .line 823
    new-instance v0, Lorg/chromium/chrome/browser/VoiceSearchTabHelper;

    iget-object v1, p0, Lorg/chromium/chrome/browser/Tab;->mContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

    invoke-virtual {v1}, Lorg/chromium/content/browser/ContentViewCore;->getWebContents()Lorg/chromium/content_public/browser/WebContents;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/chromium/chrome/browser/VoiceSearchTabHelper;-><init>(Lorg/chromium/content_public/browser/WebContents;)V

    iput-object v0, p0, Lorg/chromium/chrome/browser/Tab;->mVoiceSearchTabHelper:Lorg/chromium/chrome/browser/VoiceSearchTabHelper;

    .line 825
    iget-object v0, p0, Lorg/chromium/chrome/browser/Tab;->mContentViewClient:Lorg/chromium/content/browser/ContentViewClient;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/chromium/chrome/browser/Tab;->mContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

    iget-object v1, p0, Lorg/chromium/chrome/browser/Tab;->mContentViewClient:Lorg/chromium/content/browser/ContentViewClient;

    invoke-virtual {v0, v1}, Lorg/chromium/content/browser/ContentViewCore;->setContentViewClient(Lorg/chromium/content/browser/ContentViewClient;)V

    .line 827
    :cond_0
    sget-boolean v0, Lorg/chromium/chrome/browser/Tab;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    iget-wide v0, p0, Lorg/chromium/chrome/browser/Tab;->mNativeTabAndroid:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 828
    :cond_1
    iget-wide v2, p0, Lorg/chromium/chrome/browser/Tab;->mNativeTabAndroid:J

    iget-boolean v4, p0, Lorg/chromium/chrome/browser/Tab;->mIncognito:Z

    iget-object v5, p0, Lorg/chromium/chrome/browser/Tab;->mContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

    iget-object v6, p0, Lorg/chromium/chrome/browser/Tab;->mWebContentsDelegate:Lorg/chromium/chrome/browser/Tab$TabChromeWebContentsDelegateAndroid;

    new-instance v7, Lorg/chromium/chrome/browser/Tab$TabContextMenuPopulator;

    invoke-virtual {p0}, Lorg/chromium/chrome/browser/Tab;->createContextMenuPopulator()Lorg/chromium/chrome/browser/contextmenu/ContextMenuPopulator;

    move-result-object v0

    invoke-direct {v7, p0, v0}, Lorg/chromium/chrome/browser/Tab$TabContextMenuPopulator;-><init>(Lorg/chromium/chrome/browser/Tab;Lorg/chromium/chrome/browser/contextmenu/ContextMenuPopulator;)V

    move-object v1, p0

    invoke-direct/range {v1 .. v7}, Lorg/chromium/chrome/browser/Tab;->nativeInitWebContents(JZLorg/chromium/content/browser/ContentViewCore;Lorg/chromium/chrome/browser/ChromeWebContentsDelegateAndroid;Lorg/chromium/chrome/browser/contextmenu/ContextMenuPopulator;)V

    .line 834
    iget-object v0, p0, Lorg/chromium/chrome/browser/Tab;->mInfoBarContainer:Lorg/chromium/chrome/browser/infobar/InfoBarContainer;

    if-nez v0, :cond_4

    .line 837
    iget-object v0, p0, Lorg/chromium/chrome/browser/Tab;->mContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

    invoke-virtual {v0}, Lorg/chromium/content/browser/ContentViewCore;->getWebContents()Lorg/chromium/content_public/browser/WebContents;

    move-result-object v5

    .line 838
    new-instance v0, Lorg/chromium/chrome/browser/infobar/InfoBarContainer;

    iget-object v1, p0, Lorg/chromium/chrome/browser/Tab;->mContext:Landroid/content/Context;

    check-cast v1, Landroid/app/Activity;

    invoke-virtual {p0}, Lorg/chromium/chrome/browser/Tab;->createAutoLoginProcessor()Lorg/chromium/chrome/browser/infobar/AutoLoginProcessor;

    move-result-object v2

    invoke-virtual {p0}, Lorg/chromium/chrome/browser/Tab;->getId()I

    move-result v3

    iget-object v4, p0, Lorg/chromium/chrome/browser/Tab;->mContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

    invoke-virtual {v4}, Lorg/chromium/content/browser/ContentViewCore;->getContainerView()Landroid/view/ViewGroup;

    move-result-object v4

    invoke-direct/range {v0 .. v5}, Lorg/chromium/chrome/browser/infobar/InfoBarContainer;-><init>(Landroid/app/Activity;Lorg/chromium/chrome/browser/infobar/AutoLoginProcessor;ILandroid/view/ViewGroup;Lorg/chromium/content_public/browser/WebContents;)V

    iput-object v0, p0, Lorg/chromium/chrome/browser/Tab;->mInfoBarContainer:Lorg/chromium/chrome/browser/infobar/InfoBarContainer;

    .line 845
    :goto_0
    invoke-static {}, Lorg/chromium/chrome/browser/banners/AppBannerManager;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lorg/chromium/chrome/browser/Tab;->mAppBannerManager:Lorg/chromium/chrome/browser/banners/AppBannerManager;

    if-nez v0, :cond_2

    .line 846
    new-instance v0, Lorg/chromium/chrome/browser/banners/AppBannerManager;

    invoke-direct {v0, p0}, Lorg/chromium/chrome/browser/banners/AppBannerManager;-><init>(Lorg/chromium/chrome/browser/Tab;)V

    iput-object v0, p0, Lorg/chromium/chrome/browser/Tab;->mAppBannerManager:Lorg/chromium/chrome/browser/banners/AppBannerManager;

    .line 849
    :cond_2
    invoke-static {}, Lorg/chromium/chrome/browser/dom_distiller/DomDistillerFeedbackReporter;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lorg/chromium/chrome/browser/Tab;->mDomDistillerFeedbackReporter:Lorg/chromium/chrome/browser/dom_distiller/DomDistillerFeedbackReporter;

    if-nez v0, :cond_3

    .line 850
    new-instance v0, Lorg/chromium/chrome/browser/dom_distiller/DomDistillerFeedbackReporter;

    invoke-direct {v0, p0}, Lorg/chromium/chrome/browser/dom_distiller/DomDistillerFeedbackReporter;-><init>(Lorg/chromium/chrome/browser/Tab;)V

    iput-object v0, p0, Lorg/chromium/chrome/browser/Tab;->mDomDistillerFeedbackReporter:Lorg/chromium/chrome/browser/dom_distiller/DomDistillerFeedbackReporter;

    .line 853
    :cond_3
    iget-object v0, p0, Lorg/chromium/chrome/browser/Tab;->mObservers:Lorg/chromium/base/ObserverList;

    invoke-virtual {v0}, Lorg/chromium/base/ObserverList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/chromium/chrome/browser/TabObserver;

    invoke-interface {v0, p0}, Lorg/chromium/chrome/browser/TabObserver;->onContentChanged(Lorg/chromium/chrome/browser/Tab;)V

    goto :goto_1

    .line 842
    :cond_4
    iget-object v0, p0, Lorg/chromium/chrome/browser/Tab;->mInfoBarContainer:Lorg/chromium/chrome/browser/infobar/InfoBarContainer;

    invoke-virtual {p0}, Lorg/chromium/chrome/browser/Tab;->getId()I

    move-result v1

    iget-object v2, p0, Lorg/chromium/chrome/browser/Tab;->mContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

    invoke-virtual {v2}, Lorg/chromium/content/browser/ContentViewCore;->getContainerView()Landroid/view/ViewGroup;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/chromium/chrome/browser/infobar/InfoBarContainer;->onParentViewChanged(ILandroid/view/ViewGroup;)V

    goto :goto_0

    .line 858
    :cond_5
    iget-object v0, p0, Lorg/chromium/chrome/browser/Tab;->mContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lorg/chromium/content/browser/ContentViewCore;->setShouldSetAccessibilityFocusOnPageLoad(Z)V

    .line 859
    return-void
.end method

.method public setGroupedWithParent(Z)V
    .locals 0

    .prologue
    .line 990
    iput-boolean p1, p0, Lorg/chromium/chrome/browser/Tab;->mGroupedWithParent:Z

    .line 991
    return-void
.end method

.method protected setSyncId(I)V
    .locals 0

    .prologue
    .line 677
    iput p1, p0, Lorg/chromium/chrome/browser/Tab;->mSyncId:I

    .line 678
    return-void
.end method

.method public setUseDesktopUserAgent(ZZ)V
    .locals 1

    .prologue
    .line 642
    invoke-virtual {p0}, Lorg/chromium/chrome/browser/Tab;->getWebContents()Lorg/chromium/content_public/browser/WebContents;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 643
    invoke-virtual {p0}, Lorg/chromium/chrome/browser/Tab;->getWebContents()Lorg/chromium/content_public/browser/WebContents;

    move-result-object v0

    invoke-interface {v0}, Lorg/chromium/content_public/browser/WebContents;->getNavigationController()Lorg/chromium/content_public/browser/NavigationController;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Lorg/chromium/content_public/browser/NavigationController;->setUseDesktopUserAgent(ZZ)V

    .line 646
    :cond_0
    return-void
.end method

.method protected show()V
    .locals 1

    .prologue
    .line 723
    iget-object v0, p0, Lorg/chromium/chrome/browser/Tab;->mContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/chromium/chrome/browser/Tab;->mContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

    invoke-virtual {v0}, Lorg/chromium/content/browser/ContentViewCore;->onShow()V

    .line 724
    :cond_0
    return-void
.end method

.method protected showNativePage(Lorg/chromium/chrome/browser/NativePage;)V
    .locals 3

    .prologue
    .line 738
    iget-object v0, p0, Lorg/chromium/chrome/browser/Tab;->mNativePage:Lorg/chromium/chrome/browser/NativePage;

    if-ne v0, p1, :cond_0

    .line 744
    :goto_0
    return-void

    .line 739
    :cond_0
    iget-object v1, p0, Lorg/chromium/chrome/browser/Tab;->mNativePage:Lorg/chromium/chrome/browser/NativePage;

    .line 740
    iput-object p1, p0, Lorg/chromium/chrome/browser/Tab;->mNativePage:Lorg/chromium/chrome/browser/NativePage;

    .line 741
    invoke-direct {p0}, Lorg/chromium/chrome/browser/Tab;->pushNativePageStateToNavigationEntry()V

    .line 742
    iget-object v0, p0, Lorg/chromium/chrome/browser/Tab;->mObservers:Lorg/chromium/base/ObserverList;

    invoke-virtual {v0}, Lorg/chromium/base/ObserverList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/chromium/chrome/browser/TabObserver;

    invoke-interface {v0, p0}, Lorg/chromium/chrome/browser/TabObserver;->onContentChanged(Lorg/chromium/chrome/browser/Tab;)V

    goto :goto_1

    .line 743
    :cond_1
    invoke-direct {p0, v1}, Lorg/chromium/chrome/browser/Tab;->destroyNativePageInternal(Lorg/chromium/chrome/browser/NativePage;)V

    goto :goto_0
.end method

.method protected showRenderedPage()V
    .locals 3

    .prologue
    .line 760
    iget-object v0, p0, Lorg/chromium/chrome/browser/Tab;->mNativePage:Lorg/chromium/chrome/browser/NativePage;

    if-nez v0, :cond_0

    .line 765
    :goto_0
    return-void

    .line 761
    :cond_0
    iget-object v1, p0, Lorg/chromium/chrome/browser/Tab;->mNativePage:Lorg/chromium/chrome/browser/NativePage;

    .line 762
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/chromium/chrome/browser/Tab;->mNativePage:Lorg/chromium/chrome/browser/NativePage;

    .line 763
    iget-object v0, p0, Lorg/chromium/chrome/browser/Tab;->mObservers:Lorg/chromium/base/ObserverList;

    invoke-virtual {v0}, Lorg/chromium/base/ObserverList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/chromium/chrome/browser/TabObserver;

    invoke-interface {v0, p0}, Lorg/chromium/chrome/browser/TabObserver;->onContentChanged(Lorg/chromium/chrome/browser/Tab;)V

    goto :goto_1

    .line 764
    :cond_1
    invoke-direct {p0, v1}, Lorg/chromium/chrome/browser/Tab;->destroyNativePageInternal(Lorg/chromium/chrome/browser/NativePage;)V

    goto :goto_0
.end method

.method public stopLoading()V
    .locals 1

    .prologue
    .line 567
    invoke-virtual {p0}, Lorg/chromium/chrome/browser/Tab;->getWebContents()Lorg/chromium/content_public/browser/WebContents;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lorg/chromium/chrome/browser/Tab;->getWebContents()Lorg/chromium/content_public/browser/WebContents;

    move-result-object v0

    invoke-interface {v0}, Lorg/chromium/content_public/browser/WebContents;->stop()V

    .line 568
    :cond_0
    return-void
.end method

.method protected swapContentViewCore(Lorg/chromium/content/browser/ContentViewCore;ZZZ)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 1123
    .line 1125
    iget-object v0, p0, Lorg/chromium/chrome/browser/Tab;->mContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

    if-eqz v0, :cond_1

    .line 1126
    iget-object v0, p0, Lorg/chromium/chrome/browser/Tab;->mContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

    invoke-virtual {v0}, Lorg/chromium/content/browser/ContentViewCore;->getViewportWidthPix()I

    move-result v2

    .line 1127
    iget-object v0, p0, Lorg/chromium/chrome/browser/Tab;->mContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

    invoke-virtual {v0}, Lorg/chromium/content/browser/ContentViewCore;->getViewportHeightPix()I

    move-result v0

    .line 1128
    iget-object v3, p0, Lorg/chromium/chrome/browser/Tab;->mContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

    invoke-virtual {v3}, Lorg/chromium/content/browser/ContentViewCore;->onHide()V

    .line 1130
    :goto_0
    invoke-virtual {p0, p2}, Lorg/chromium/chrome/browser/Tab;->destroyContentViewCore(Z)V

    .line 1131
    iget-object v3, p0, Lorg/chromium/chrome/browser/Tab;->mNativePage:Lorg/chromium/chrome/browser/NativePage;

    .line 1132
    const/4 v4, 0x0

    iput-object v4, p0, Lorg/chromium/chrome/browser/Tab;->mNativePage:Lorg/chromium/chrome/browser/NativePage;

    .line 1133
    invoke-virtual {p0, p1}, Lorg/chromium/chrome/browser/Tab;->setContentViewCore(Lorg/chromium/content/browser/ContentViewCore;)V

    .line 1139
    iget-object v4, p0, Lorg/chromium/chrome/browser/Tab;->mContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

    invoke-virtual {v4, v2, v0, v1, v1}, Lorg/chromium/content/browser/ContentViewCore;->onSizeChanged(IIII)V

    .line 1140
    iget-object v0, p0, Lorg/chromium/chrome/browser/Tab;->mContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

    invoke-virtual {v0}, Lorg/chromium/content/browser/ContentViewCore;->onShow()V

    .line 1141
    iget-object v0, p0, Lorg/chromium/chrome/browser/Tab;->mContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

    invoke-virtual {v0}, Lorg/chromium/content/browser/ContentViewCore;->attachImeAdapter()V

    .line 1142
    invoke-direct {p0, v3}, Lorg/chromium/chrome/browser/Tab;->destroyNativePageInternal(Lorg/chromium/chrome/browser/NativePage;)V

    .line 1143
    iget-object v0, p0, Lorg/chromium/chrome/browser/Tab;->mObservers:Lorg/chromium/base/ObserverList;

    invoke-virtual {v0}, Lorg/chromium/base/ObserverList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/chromium/chrome/browser/TabObserver;

    .line 1144
    invoke-interface {v0, p0, p3, p4}, Lorg/chromium/chrome/browser/TabObserver;->onWebContentsSwapped(Lorg/chromium/chrome/browser/Tab;ZZ)V

    goto :goto_1

    .line 1146
    :cond_0
    return-void

    :cond_1
    move v0, v1

    move v2, v1

    goto :goto_0
.end method
