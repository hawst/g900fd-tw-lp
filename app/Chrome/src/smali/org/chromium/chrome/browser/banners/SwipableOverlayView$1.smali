.class Lorg/chromium/chrome/browser/banners/SwipableOverlayView$1;
.super Landroid/view/GestureDetector$SimpleOnGestureListener;
.source "SwipableOverlayView.java"


# instance fields
.field final synthetic this$0:Lorg/chromium/chrome/browser/banners/SwipableOverlayView;


# direct methods
.method constructor <init>(Lorg/chromium/chrome/browser/banners/SwipableOverlayView;)V
    .locals 0

    .prologue
    .line 237
    iput-object p1, p0, Lorg/chromium/chrome/browser/banners/SwipableOverlayView$1;->this$0:Lorg/chromium/chrome/browser/banners/SwipableOverlayView;

    invoke-direct {p0}, Landroid/view/GestureDetector$SimpleOnGestureListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onDown(Landroid/view/MotionEvent;)Z
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 240
    iget-object v0, p0, Lorg/chromium/chrome/browser/banners/SwipableOverlayView$1;->this$0:Lorg/chromium/chrome/browser/banners/SwipableOverlayView;

    # setter for: Lorg/chromium/chrome/browser/banners/SwipableOverlayView;->mGestureState:I
    invoke-static {v0, v4}, Lorg/chromium/chrome/browser/banners/SwipableOverlayView;->access$002(Lorg/chromium/chrome/browser/banners/SwipableOverlayView;I)I

    .line 241
    iget-object v0, p0, Lorg/chromium/chrome/browser/banners/SwipableOverlayView$1;->this$0:Lorg/chromium/chrome/browser/banners/SwipableOverlayView;

    const/4 v1, 0x0

    # setter for: Lorg/chromium/chrome/browser/banners/SwipableOverlayView;->mDragDirection:I
    invoke-static {v0, v1}, Lorg/chromium/chrome/browser/banners/SwipableOverlayView;->access$102(Lorg/chromium/chrome/browser/banners/SwipableOverlayView;I)I

    .line 242
    iget-object v0, p0, Lorg/chromium/chrome/browser/banners/SwipableOverlayView$1;->this$0:Lorg/chromium/chrome/browser/banners/SwipableOverlayView;

    const/4 v1, 0x0

    # setter for: Lorg/chromium/chrome/browser/banners/SwipableOverlayView;->mDragXPerMs:F
    invoke-static {v0, v1}, Lorg/chromium/chrome/browser/banners/SwipableOverlayView;->access$202(Lorg/chromium/chrome/browser/banners/SwipableOverlayView;F)F

    .line 243
    iget-object v0, p0, Lorg/chromium/chrome/browser/banners/SwipableOverlayView$1;->this$0:Lorg/chromium/chrome/browser/banners/SwipableOverlayView;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v2

    # setter for: Lorg/chromium/chrome/browser/banners/SwipableOverlayView;->mDragStartMs:J
    invoke-static {v0, v2, v3}, Lorg/chromium/chrome/browser/banners/SwipableOverlayView;->access$302(Lorg/chromium/chrome/browser/banners/SwipableOverlayView;J)J

    .line 244
    return v4
.end method

.method public onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 6

    .prologue
    .line 261
    iget-object v0, p0, Lorg/chromium/chrome/browser/banners/SwipableOverlayView$1;->this$0:Lorg/chromium/chrome/browser/banners/SwipableOverlayView;

    const/4 v1, 0x2

    # setter for: Lorg/chromium/chrome/browser/banners/SwipableOverlayView;->mGestureState:I
    invoke-static {v0, v1}, Lorg/chromium/chrome/browser/banners/SwipableOverlayView;->access$002(Lorg/chromium/chrome/browser/banners/SwipableOverlayView;I)I

    .line 265
    invoke-static {p3}, Ljava/lang/Math;->abs(F)F

    move-result v0

    const/high16 v1, 0x447a0000    # 1000.0f

    div-float/2addr v0, v1

    .line 268
    iget-object v1, p0, Lorg/chromium/chrome/browser/banners/SwipableOverlayView$1;->this$0:Lorg/chromium/chrome/browser/banners/SwipableOverlayView;

    invoke-virtual {v1}, Lorg/chromium/chrome/browser/banners/SwipableOverlayView;->getTranslationX()F

    move-result v1

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v2

    iget-object v4, p0, Lorg/chromium/chrome/browser/banners/SwipableOverlayView$1;->this$0:Lorg/chromium/chrome/browser/banners/SwipableOverlayView;

    # getter for: Lorg/chromium/chrome/browser/banners/SwipableOverlayView;->mDragStartMs:J
    invoke-static {v4}, Lorg/chromium/chrome/browser/banners/SwipableOverlayView;->access$300(Lorg/chromium/chrome/browser/banners/SwipableOverlayView;)J

    move-result-wide v4

    sub-long/2addr v2, v4

    long-to-float v2, v2

    div-float/2addr v1, v2

    .line 272
    iget-object v2, p0, Lorg/chromium/chrome/browser/banners/SwipableOverlayView$1;->this$0:Lorg/chromium/chrome/browser/banners/SwipableOverlayView;

    iget-object v3, p0, Lorg/chromium/chrome/browser/banners/SwipableOverlayView$1;->this$0:Lorg/chromium/chrome/browser/banners/SwipableOverlayView;

    # getter for: Lorg/chromium/chrome/browser/banners/SwipableOverlayView;->mDragDirection:I
    invoke-static {v3}, Lorg/chromium/chrome/browser/banners/SwipableOverlayView;->access$100(Lorg/chromium/chrome/browser/banners/SwipableOverlayView;)I

    move-result v3

    int-to-float v3, v3

    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    move-result v0

    mul-float/2addr v0, v3

    # setter for: Lorg/chromium/chrome/browser/banners/SwipableOverlayView;->mDragXPerMs:F
    invoke-static {v2, v0}, Lorg/chromium/chrome/browser/banners/SwipableOverlayView;->access$202(Lorg/chromium/chrome/browser/banners/SwipableOverlayView;F)F

    .line 273
    iget-object v0, p0, Lorg/chromium/chrome/browser/banners/SwipableOverlayView$1;->this$0:Lorg/chromium/chrome/browser/banners/SwipableOverlayView;

    # invokes: Lorg/chromium/chrome/browser/banners/SwipableOverlayView;->onFinishHorizontalGesture()V
    invoke-static {v0}, Lorg/chromium/chrome/browser/banners/SwipableOverlayView;->access$500(Lorg/chromium/chrome/browser/banners/SwipableOverlayView;)V

    .line 274
    const/4 v0, 0x1

    return v0
.end method

.method public onScroll(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 249
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    sub-float/2addr v0, v2

    .line 250
    iget-object v2, p0, Lorg/chromium/chrome/browser/banners/SwipableOverlayView$1;->this$0:Lorg/chromium/chrome/browser/banners/SwipableOverlayView;

    iget-object v3, p0, Lorg/chromium/chrome/browser/banners/SwipableOverlayView$1;->this$0:Lorg/chromium/chrome/browser/banners/SwipableOverlayView;

    invoke-virtual {v3}, Lorg/chromium/chrome/browser/banners/SwipableOverlayView;->getTranslationX()F

    move-result v3

    add-float/2addr v3, v0

    invoke-virtual {v2, v3}, Lorg/chromium/chrome/browser/banners/SwipableOverlayView;->setTranslationX(F)V

    .line 251
    iget-object v2, p0, Lorg/chromium/chrome/browser/banners/SwipableOverlayView$1;->this$0:Lorg/chromium/chrome/browser/banners/SwipableOverlayView;

    iget-object v3, p0, Lorg/chromium/chrome/browser/banners/SwipableOverlayView$1;->this$0:Lorg/chromium/chrome/browser/banners/SwipableOverlayView;

    # invokes: Lorg/chromium/chrome/browser/banners/SwipableOverlayView;->calculateAnimationAlpha()F
    invoke-static {v3}, Lorg/chromium/chrome/browser/banners/SwipableOverlayView;->access$400(Lorg/chromium/chrome/browser/banners/SwipableOverlayView;)F

    move-result v3

    invoke-virtual {v2, v3}, Lorg/chromium/chrome/browser/banners/SwipableOverlayView;->setAlpha(F)V

    .line 255
    iget-object v2, p0, Lorg/chromium/chrome/browser/banners/SwipableOverlayView$1;->this$0:Lorg/chromium/chrome/browser/banners/SwipableOverlayView;

    const/4 v3, 0x0

    cmpg-float v0, v0, v3

    if-gez v0, :cond_0

    const/4 v0, -0x1

    :goto_0
    # setter for: Lorg/chromium/chrome/browser/banners/SwipableOverlayView;->mDragDirection:I
    invoke-static {v2, v0}, Lorg/chromium/chrome/browser/banners/SwipableOverlayView;->access$102(Lorg/chromium/chrome/browser/banners/SwipableOverlayView;I)I

    .line 256
    return v1

    :cond_0
    move v0, v1

    .line 255
    goto :goto_0
.end method

.method public onShowPress(Landroid/view/MotionEvent;)V
    .locals 1

    .prologue
    .line 285
    iget-object v0, p0, Lorg/chromium/chrome/browser/banners/SwipableOverlayView$1;->this$0:Lorg/chromium/chrome/browser/banners/SwipableOverlayView;

    invoke-virtual {v0, p1}, Lorg/chromium/chrome/browser/banners/SwipableOverlayView;->onViewPressed(Landroid/view/MotionEvent;)V

    .line 286
    return-void
.end method

.method public onSingleTapConfirmed(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 279
    iget-object v0, p0, Lorg/chromium/chrome/browser/banners/SwipableOverlayView$1;->this$0:Lorg/chromium/chrome/browser/banners/SwipableOverlayView;

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/banners/SwipableOverlayView;->onViewClicked()V

    .line 280
    const/4 v0, 0x1

    return v0
.end method
