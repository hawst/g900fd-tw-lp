.class public Lorg/chromium/chrome/browser/NavigationPopup;
.super Landroid/widget/ListPopupWindow;
.source "NavigationPopup.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field private final mAdapter:Lorg/chromium/chrome/browser/NavigationPopup$NavigationAdapter;

.field private final mContext:Landroid/content/Context;

.field private final mFaviconSize:I

.field private final mHistory:Lorg/chromium/content_public/browser/NavigationHistory;

.field private final mListItemFactory:Lorg/chromium/chrome/browser/NavigationPopup$ListItemFactory;

.field private mNativeNavigationPopup:J

.field private final mNavigationClient:Lorg/chromium/content/browser/NavigationClient;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lorg/chromium/content/browser/NavigationClient;Z)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 62
    const v0, 0x1010300

    invoke-direct {p0, p1, v2, v0}, Landroid/widget/ListPopupWindow;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 63
    iput-object p1, p0, Lorg/chromium/chrome/browser/NavigationPopup;->mContext:Landroid/content/Context;

    .line 64
    iput-object p2, p0, Lorg/chromium/chrome/browser/NavigationPopup;->mNavigationClient:Lorg/chromium/content/browser/NavigationClient;

    .line 65
    iget-object v0, p0, Lorg/chromium/chrome/browser/NavigationPopup;->mNavigationClient:Lorg/chromium/content/browser/NavigationClient;

    const/16 v1, 0x8

    invoke-interface {v0, p3, v1}, Lorg/chromium/content/browser/NavigationClient;->getDirectedNavigationHistory(ZI)Lorg/chromium/content_public/browser/NavigationHistory;

    move-result-object v0

    iput-object v0, p0, Lorg/chromium/chrome/browser/NavigationPopup;->mHistory:Lorg/chromium/content_public/browser/NavigationHistory;

    .line 67
    new-instance v0, Lorg/chromium/chrome/browser/NavigationPopup$NavigationAdapter;

    invoke-direct {v0, p0, v2}, Lorg/chromium/chrome/browser/NavigationPopup$NavigationAdapter;-><init>(Lorg/chromium/chrome/browser/NavigationPopup;Lorg/chromium/chrome/browser/NavigationPopup$1;)V

    iput-object v0, p0, Lorg/chromium/chrome/browser/NavigationPopup;->mAdapter:Lorg/chromium/chrome/browser/NavigationPopup$NavigationAdapter;

    .line 69
    iget-object v0, p0, Lorg/chromium/chrome/browser/NavigationPopup;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    .line 70
    const/high16 v1, 0x41800000    # 16.0f

    mul-float/2addr v0, v1

    float-to-int v0, v0

    iput v0, p0, Lorg/chromium/chrome/browser/NavigationPopup;->mFaviconSize:I

    .line 72
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lorg/chromium/chrome/browser/NavigationPopup;->setModal(Z)V

    .line 73
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lorg/chromium/chrome/browser/NavigationPopup;->setInputMethodMode(I)V

    .line 74
    const/4 v0, -0x2

    invoke-virtual {p0, v0}, Lorg/chromium/chrome/browser/NavigationPopup;->setHeight(I)V

    .line 75
    invoke-virtual {p0, p0}, Lorg/chromium/chrome/browser/NavigationPopup;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 77
    new-instance v0, Landroid/widget/HeaderViewListAdapter;

    iget-object v1, p0, Lorg/chromium/chrome/browser/NavigationPopup;->mAdapter:Lorg/chromium/chrome/browser/NavigationPopup$NavigationAdapter;

    invoke-direct {v0, v2, v2, v1}, Landroid/widget/HeaderViewListAdapter;-><init>(Ljava/util/ArrayList;Ljava/util/ArrayList;Landroid/widget/ListAdapter;)V

    invoke-virtual {p0, v0}, Lorg/chromium/chrome/browser/NavigationPopup;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 79
    new-instance v0, Lorg/chromium/chrome/browser/NavigationPopup$ListItemFactory;

    invoke-direct {v0, p1}, Lorg/chromium/chrome/browser/NavigationPopup$ListItemFactory;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lorg/chromium/chrome/browser/NavigationPopup;->mListItemFactory:Lorg/chromium/chrome/browser/NavigationPopup$ListItemFactory;

    .line 80
    return-void
.end method

.method static synthetic access$100(Lorg/chromium/chrome/browser/NavigationPopup;)Lorg/chromium/content_public/browser/NavigationHistory;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lorg/chromium/chrome/browser/NavigationPopup;->mHistory:Lorg/chromium/content_public/browser/NavigationHistory;

    return-object v0
.end method

.method static synthetic access$200(Lorg/chromium/chrome/browser/NavigationPopup;)Lorg/chromium/chrome/browser/NavigationPopup$ListItemFactory;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lorg/chromium/chrome/browser/NavigationPopup;->mListItemFactory:Lorg/chromium/chrome/browser/NavigationPopup$ListItemFactory;

    return-object v0
.end method

.method static synthetic access$300(Lorg/chromium/chrome/browser/NavigationPopup;Landroid/widget/TextView;Landroid/graphics/Bitmap;)V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0, p1, p2}, Lorg/chromium/chrome/browser/NavigationPopup;->updateBitmapForTextView(Landroid/widget/TextView;Landroid/graphics/Bitmap;)V

    return-void
.end method

.method private initializeNative()V
    .locals 6

    .prologue
    .line 105
    invoke-static {}, Lorg/chromium/base/ThreadUtils;->assertOnUiThread()V

    .line 106
    invoke-direct {p0}, Lorg/chromium/chrome/browser/NavigationPopup;->nativeInit()J

    move-result-wide v0

    iput-wide v0, p0, Lorg/chromium/chrome/browser/NavigationPopup;->mNativeNavigationPopup:J

    .line 108
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 109
    const/4 v0, 0x0

    :goto_0
    iget-object v2, p0, Lorg/chromium/chrome/browser/NavigationPopup;->mHistory:Lorg/chromium/content_public/browser/NavigationHistory;

    invoke-virtual {v2}, Lorg/chromium/content_public/browser/NavigationHistory;->getEntryCount()I

    move-result v2

    if-ge v0, v2, :cond_1

    .line 110
    iget-object v2, p0, Lorg/chromium/chrome/browser/NavigationPopup;->mHistory:Lorg/chromium/content_public/browser/NavigationHistory;

    invoke-virtual {v2, v0}, Lorg/chromium/content_public/browser/NavigationHistory;->getEntryAtIndex(I)Lorg/chromium/content_public/browser/NavigationEntry;

    move-result-object v2

    .line 111
    invoke-virtual {v2}, Lorg/chromium/content_public/browser/NavigationEntry;->getFavicon()Landroid/graphics/Bitmap;

    move-result-object v3

    if-nez v3, :cond_0

    .line 112
    invoke-virtual {v2}, Lorg/chromium/content_public/browser/NavigationEntry;->getUrl()Ljava/lang/String;

    move-result-object v2

    .line 113
    invoke-interface {v1, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 114
    iget-wide v4, p0, Lorg/chromium/chrome/browser/NavigationPopup;->mNativeNavigationPopup:J

    invoke-direct {p0, v4, v5, v2}, Lorg/chromium/chrome/browser/NavigationPopup;->nativeFetchFaviconForUrl(JLjava/lang/String;)V

    .line 115
    invoke-interface {v1, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 109
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 118
    :cond_1
    iget-wide v0, p0, Lorg/chromium/chrome/browser/NavigationPopup;->mNativeNavigationPopup:J

    invoke-static {}, Lorg/chromium/chrome/browser/NavigationPopup;->nativeGetHistoryUrl()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v0, v1, v2}, Lorg/chromium/chrome/browser/NavigationPopup;->nativeFetchFaviconForUrl(JLjava/lang/String;)V

    .line 119
    return-void
.end method

.method private native nativeDestroy(J)V
.end method

.method private native nativeFetchFaviconForUrl(JLjava/lang/String;)V
.end method

.method private static native nativeGetHistoryUrl()Ljava/lang/String;
.end method

.method private native nativeInit()J
.end method

.method private onFaviconUpdated(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 123
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lorg/chromium/chrome/browser/NavigationPopup;->mHistory:Lorg/chromium/content_public/browser/NavigationHistory;

    invoke-virtual {v0}, Lorg/chromium/content_public/browser/NavigationHistory;->getEntryCount()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 124
    iget-object v0, p0, Lorg/chromium/chrome/browser/NavigationPopup;->mHistory:Lorg/chromium/content_public/browser/NavigationHistory;

    invoke-virtual {v0, v1}, Lorg/chromium/content_public/browser/NavigationHistory;->getEntryAtIndex(I)Lorg/chromium/content_public/browser/NavigationEntry;

    move-result-object v2

    .line 125
    invoke-virtual {v2}, Lorg/chromium/content_public/browser/NavigationEntry;->getUrl()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    move-object v0, p2

    check-cast v0, Landroid/graphics/Bitmap;

    invoke-virtual {v2, v0}, Lorg/chromium/content_public/browser/NavigationEntry;->updateFavicon(Landroid/graphics/Bitmap;)V

    .line 123
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 127
    :cond_1
    iget-object v0, p0, Lorg/chromium/chrome/browser/NavigationPopup;->mAdapter:Lorg/chromium/chrome/browser/NavigationPopup$NavigationAdapter;

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/NavigationPopup$NavigationAdapter;->notifyDataSetChanged()V

    .line 128
    return-void
.end method

.method private updateBitmapForTextView(Landroid/widget/TextView;Landroid/graphics/Bitmap;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 138
    if-eqz p2, :cond_0

    .line 140
    new-instance v1, Landroid/graphics/drawable/BitmapDrawable;

    iget-object v0, p0, Lorg/chromium/chrome/browser/NavigationPopup;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-direct {v1, v0, p2}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    move-object v0, v1

    .line 141
    check-cast v0, Landroid/graphics/drawable/BitmapDrawable;

    const/16 v2, 0x77

    invoke-virtual {v0, v2}, Landroid/graphics/drawable/BitmapDrawable;->setGravity(I)V

    .line 145
    :goto_0
    iget v0, p0, Lorg/chromium/chrome/browser/NavigationPopup;->mFaviconSize:I

    iget v2, p0, Lorg/chromium/chrome/browser/NavigationPopup;->mFaviconSize:I

    invoke-virtual {v1, v3, v3, v0, v2}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 146
    invoke-virtual {p1, v1, v4, v4, v4}, Landroid/widget/TextView;->setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 147
    return-void

    .line 143
    :cond_0
    new-instance v1, Landroid/graphics/drawable/ColorDrawable;

    invoke-direct {v1, v3}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    goto :goto_0
.end method


# virtual methods
.method public dismiss()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 97
    iget-wide v0, p0, Lorg/chromium/chrome/browser/NavigationPopup;->mNativeNavigationPopup:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 98
    iget-wide v0, p0, Lorg/chromium/chrome/browser/NavigationPopup;->mNativeNavigationPopup:J

    invoke-direct {p0, v0, v1}, Lorg/chromium/chrome/browser/NavigationPopup;->nativeDestroy(J)V

    .line 99
    iput-wide v2, p0, Lorg/chromium/chrome/browser/NavigationPopup;->mNativeNavigationPopup:J

    .line 101
    :cond_0
    invoke-super {p0}, Landroid/widget/ListPopupWindow;->dismiss()V

    .line 102
    return-void
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 2

    .prologue
    .line 132
    invoke-virtual {p1, p3}, Landroid/widget/AdapterView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/chromium/content_public/browser/NavigationEntry;

    .line 133
    iget-object v1, p0, Lorg/chromium/chrome/browser/NavigationPopup;->mNavigationClient:Lorg/chromium/content/browser/NavigationClient;

    invoke-virtual {v0}, Lorg/chromium/content_public/browser/NavigationEntry;->getIndex()I

    move-result v0

    invoke-interface {v1, v0}, Lorg/chromium/content/browser/NavigationClient;->goToNavigationIndex(I)V

    .line 134
    invoke-virtual {p0}, Lorg/chromium/chrome/browser/NavigationPopup;->dismiss()V

    .line 135
    return-void
.end method

.method public shouldBeShown()Z
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lorg/chromium/chrome/browser/NavigationPopup;->mHistory:Lorg/chromium/content_public/browser/NavigationHistory;

    invoke-virtual {v0}, Lorg/chromium/content_public/browser/NavigationHistory;->getEntryCount()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public show()V
    .locals 4

    .prologue
    .line 91
    iget-wide v0, p0, Lorg/chromium/chrome/browser/NavigationPopup;->mNativeNavigationPopup:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    invoke-direct {p0}, Lorg/chromium/chrome/browser/NavigationPopup;->initializeNative()V

    .line 92
    :cond_0
    invoke-super {p0}, Landroid/widget/ListPopupWindow;->show()V

    .line 93
    return-void
.end method
