.class public abstract Lorg/chromium/chrome/browser/tabmodel/TabModelBase;
.super Ljava/lang/Object;
.source "TabModelBase.java"

# interfaces
.implements Lorg/chromium/chrome/browser/tabmodel/TabModel;


# static fields
.field static final synthetic $assertionsDisabled:Z

.field private static sPerceivedTabSwitchLatencyMetricLogged:Z

.field private static sTabSelectionType:Lorg/chromium/chrome/browser/tabmodel/TabModel$TabSelectionType;

.field private static sTabSwitchLatencyMetricRequired:Z

.field private static sTabSwitchStartTime:J


# instance fields
.field private mIndex:I

.field private final mIsIncognito:Z

.field protected final mModelDelegate:Lorg/chromium/chrome/browser/tabmodel/TabModelDelegate;

.field private mNativeTabModelImpl:J

.field private final mObservers:Lorg/chromium/base/ObserverList;

.field private final mOrderController:Lorg/chromium/chrome/browser/tabmodel/TabModelOrderController;

.field private final mRewoundList:Lorg/chromium/chrome/browser/tabmodel/TabModelBase$RewoundList;

.field private final mTabs:Ljava/util/List;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 23
    const-class v0, Lorg/chromium/chrome/browser/tabmodel/TabModelBase;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/chromium/chrome/browser/tabmodel/TabModelBase;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(ZLorg/chromium/chrome/browser/tabmodel/TabModelOrderController;Lorg/chromium/chrome/browser/tabmodel/TabModelDelegate;)V
    .locals 2

    .prologue
    .line 68
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lorg/chromium/chrome/browser/tabmodel/TabModelBase;->mTabs:Ljava/util/List;

    .line 57
    new-instance v0, Lorg/chromium/chrome/browser/tabmodel/TabModelBase$RewoundList;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lorg/chromium/chrome/browser/tabmodel/TabModelBase$RewoundList;-><init>(Lorg/chromium/chrome/browser/tabmodel/TabModelBase;Lorg/chromium/chrome/browser/tabmodel/TabModelBase$1;)V

    iput-object v0, p0, Lorg/chromium/chrome/browser/tabmodel/TabModelBase;->mRewoundList:Lorg/chromium/chrome/browser/tabmodel/TabModelBase$RewoundList;

    .line 62
    const/4 v0, -0x1

    iput v0, p0, Lorg/chromium/chrome/browser/tabmodel/TabModelBase;->mIndex:I

    .line 65
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lorg/chromium/chrome/browser/tabmodel/TabModelBase;->mNativeTabModelImpl:J

    .line 69
    iput-boolean p1, p0, Lorg/chromium/chrome/browser/tabmodel/TabModelBase;->mIsIncognito:Z

    .line 70
    invoke-direct {p0, p1}, Lorg/chromium/chrome/browser/tabmodel/TabModelBase;->nativeInit(Z)J

    move-result-wide v0

    iput-wide v0, p0, Lorg/chromium/chrome/browser/tabmodel/TabModelBase;->mNativeTabModelImpl:J

    .line 71
    iput-object p2, p0, Lorg/chromium/chrome/browser/tabmodel/TabModelBase;->mOrderController:Lorg/chromium/chrome/browser/tabmodel/TabModelOrderController;

    .line 72
    iput-object p3, p0, Lorg/chromium/chrome/browser/tabmodel/TabModelBase;->mModelDelegate:Lorg/chromium/chrome/browser/tabmodel/TabModelDelegate;

    .line 73
    new-instance v0, Lorg/chromium/base/ObserverList;

    invoke-direct {v0}, Lorg/chromium/base/ObserverList;-><init>()V

    iput-object v0, p0, Lorg/chromium/chrome/browser/tabmodel/TabModelBase;->mObservers:Lorg/chromium/base/ObserverList;

    .line 74
    return-void
.end method

.method private finalizeTabClosure(Lorg/chromium/chrome/browser/Tab;)V
    .locals 2

    .prologue
    .line 481
    iget-object v0, p0, Lorg/chromium/chrome/browser/tabmodel/TabModelBase;->mObservers:Lorg/chromium/base/ObserverList;

    invoke-virtual {v0}, Lorg/chromium/base/ObserverList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/chromium/chrome/browser/tabmodel/TabModelObserver;

    invoke-interface {v0, p1}, Lorg/chromium/chrome/browser/tabmodel/TabModelObserver;->didCloseTab(Lorg/chromium/chrome/browser/Tab;)V

    goto :goto_0

    .line 482
    :cond_0
    invoke-virtual {p1}, Lorg/chromium/chrome/browser/Tab;->destroy()V

    .line 483
    return-void
.end method

.method private findTabInAllTabModels(I)Lorg/chromium/chrome/browser/Tab;
    .locals 2

    .prologue
    .line 206
    iget-object v0, p0, Lorg/chromium/chrome/browser/tabmodel/TabModelBase;->mModelDelegate:Lorg/chromium/chrome/browser/tabmodel/TabModelDelegate;

    iget-boolean v1, p0, Lorg/chromium/chrome/browser/tabmodel/TabModelBase;->mIsIncognito:Z

    invoke-interface {v0, v1}, Lorg/chromium/chrome/browser/tabmodel/TabModelDelegate;->getModel(Z)Lorg/chromium/chrome/browser/tabmodel/TabModel;

    move-result-object v0

    invoke-static {v0, p1}, Lorg/chromium/chrome/browser/tabmodel/TabModelUtils;->getTabById(Lorg/chromium/chrome/browser/tabmodel/TabList;I)Lorg/chromium/chrome/browser/Tab;

    move-result-object v0

    .line 207
    if-eqz v0, :cond_0

    .line 208
    :goto_0
    return-object v0

    :cond_0
    iget-object v1, p0, Lorg/chromium/chrome/browser/tabmodel/TabModelBase;->mModelDelegate:Lorg/chromium/chrome/browser/tabmodel/TabModelDelegate;

    iget-boolean v0, p0, Lorg/chromium/chrome/browser/tabmodel/TabModelBase;->mIsIncognito:Z

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    invoke-interface {v1, v0}, Lorg/chromium/chrome/browser/tabmodel/TabModelDelegate;->getModel(Z)Lorg/chromium/chrome/browser/tabmodel/TabModel;

    move-result-object v0

    invoke-static {v0, p1}, Lorg/chromium/chrome/browser/tabmodel/TabModelUtils;->getTabById(Lorg/chromium/chrome/browser/tabmodel/TabList;I)Lorg/chromium/chrome/browser/Tab;

    move-result-object v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static flushActualTabSwitchLatencyMetric()V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const/4 v2, 0x0

    .line 684
    sget-wide v0, Lorg/chromium/chrome/browser/tabmodel/TabModelBase;->sTabSwitchStartTime:J

    cmp-long v0, v0, v4

    if-lez v0, :cond_0

    sget-boolean v0, Lorg/chromium/chrome/browser/tabmodel/TabModelBase;->sTabSwitchLatencyMetricRequired:Z

    if-nez v0, :cond_1

    .line 690
    :cond_0
    :goto_0
    return-void

    .line 685
    :cond_1
    invoke-static {}, Lorg/chromium/chrome/browser/tabmodel/TabModelBase;->logPerceivedTabSwitchLatencyMetric()V

    .line 686
    invoke-static {v2}, Lorg/chromium/chrome/browser/tabmodel/TabModelBase;->flushTabSwitchLatencyMetric(Z)V

    .line 688
    sput-wide v4, Lorg/chromium/chrome/browser/tabmodel/TabModelBase;->sTabSwitchStartTime:J

    .line 689
    sput-boolean v2, Lorg/chromium/chrome/browser/tabmodel/TabModelBase;->sTabSwitchLatencyMetricRequired:Z

    goto :goto_0
.end method

.method private static flushTabSwitchLatencyMetric(Z)V
    .locals 4

    .prologue
    .line 693
    sget-wide v0, Lorg/chromium/chrome/browser/tabmodel/TabModelBase;->sTabSwitchStartTime:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-gtz v0, :cond_0

    .line 709
    :goto_0
    return-void

    .line 694
    :cond_0
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    sget-wide v2, Lorg/chromium/chrome/browser/tabmodel/TabModelBase;->sTabSwitchStartTime:J

    sub-long/2addr v0, v2

    .line 695
    sget-object v2, Lorg/chromium/chrome/browser/tabmodel/TabModelBase$1;->$SwitchMap$org$chromium$chrome$browser$tabmodel$TabModel$TabSelectionType:[I

    sget-object v3, Lorg/chromium/chrome/browser/tabmodel/TabModelBase;->sTabSelectionType:Lorg/chromium/chrome/browser/tabmodel/TabModel$TabSelectionType;

    invoke-virtual {v3}, Lorg/chromium/chrome/browser/tabmodel/TabModel$TabSelectionType;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    goto :goto_0

    .line 697
    :pswitch_0
    invoke-static {v0, v1, p0}, Lorg/chromium/chrome/browser/tabmodel/TabModelBase;->nativeLogFromCloseMetric(JZ)V

    goto :goto_0

    .line 700
    :pswitch_1
    invoke-static {v0, v1, p0}, Lorg/chromium/chrome/browser/tabmodel/TabModelBase;->nativeLogFromExitMetric(JZ)V

    goto :goto_0

    .line 703
    :pswitch_2
    invoke-static {v0, v1, p0}, Lorg/chromium/chrome/browser/tabmodel/TabModelBase;->nativeLogFromNewMetric(JZ)V

    goto :goto_0

    .line 706
    :pswitch_3
    invoke-static {v0, v1, p0}, Lorg/chromium/chrome/browser/tabmodel/TabModelBase;->nativeLogFromUserMetric(JZ)V

    goto :goto_0

    .line 695
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private getLastId(Lorg/chromium/chrome/browser/tabmodel/TabModel$TabSelectionType;)I
    .locals 2

    .prologue
    const/4 v0, -0x1

    .line 374
    sget-object v1, Lorg/chromium/chrome/browser/tabmodel/TabModel$TabSelectionType;->FROM_CLOSE:Lorg/chromium/chrome/browser/tabmodel/TabModel$TabSelectionType;

    if-ne p1, v1, :cond_1

    .line 378
    :cond_0
    :goto_0
    return v0

    .line 377
    :cond_1
    iget-object v1, p0, Lorg/chromium/chrome/browser/tabmodel/TabModelBase;->mModelDelegate:Lorg/chromium/chrome/browser/tabmodel/TabModelDelegate;

    invoke-interface {v1}, Lorg/chromium/chrome/browser/tabmodel/TabModelDelegate;->getCurrentModel()Lorg/chromium/chrome/browser/tabmodel/TabModel;

    move-result-object v1

    invoke-static {v1}, Lorg/chromium/chrome/browser/tabmodel/TabModelUtils;->getCurrentTab(Lorg/chromium/chrome/browser/tabmodel/TabList;)Lorg/chromium/chrome/browser/Tab;

    move-result-object v1

    .line 378
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lorg/chromium/chrome/browser/Tab;->getId()I

    move-result v0

    goto :goto_0
.end method

.method private isCurrentModel()Z
    .locals 1

    .prologue
    .line 369
    iget-object v0, p0, Lorg/chromium/chrome/browser/tabmodel/TabModelBase;->mModelDelegate:Lorg/chromium/chrome/browser/tabmodel/TabModelDelegate;

    invoke-interface {v0}, Lorg/chromium/chrome/browser/tabmodel/TabModelDelegate;->getCurrentModel()Lorg/chromium/chrome/browser/tabmodel/TabModel;

    move-result-object v0

    if-ne v0, p0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isSessionRestoreInProgress()Z
    .locals 1

    .prologue
    .line 644
    iget-object v0, p0, Lorg/chromium/chrome/browser/tabmodel/TabModelBase;->mModelDelegate:Lorg/chromium/chrome/browser/tabmodel/TabModelDelegate;

    invoke-interface {v0}, Lorg/chromium/chrome/browser/tabmodel/TabModelDelegate;->isSessionRestoreInProgress()Z

    move-result v0

    return v0
.end method

.method public static logPerceivedTabSwitchLatencyMetric()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 674
    sget-wide v0, Lorg/chromium/chrome/browser/tabmodel/TabModelBase;->sTabSwitchStartTime:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    sget-boolean v0, Lorg/chromium/chrome/browser/tabmodel/TabModelBase;->sPerceivedTabSwitchLatencyMetricLogged:Z

    if-eqz v0, :cond_1

    .line 678
    :cond_0
    :goto_0
    return-void

    .line 676
    :cond_1
    invoke-static {v4}, Lorg/chromium/chrome/browser/tabmodel/TabModelBase;->flushTabSwitchLatencyMetric(Z)V

    .line 677
    sput-boolean v4, Lorg/chromium/chrome/browser/tabmodel/TabModelBase;->sPerceivedTabSwitchLatencyMetricLogged:Z

    goto :goto_0
.end method

.method private native nativeBroadcastSessionRestoreComplete(J)V
.end method

.method private native nativeDestroy(J)V
.end method

.method private native nativeGetProfileAndroid(J)Lorg/chromium/chrome/browser/profiles/Profile;
.end method

.method private native nativeInit(Z)J
.end method

.method private static native nativeLogFromCloseMetric(JZ)V
.end method

.method private static native nativeLogFromExitMetric(JZ)V
.end method

.method private static native nativeLogFromNewMetric(JZ)V
.end method

.method private static native nativeLogFromUserMetric(JZ)V
.end method

.method private native nativeTabAddedToModel(JLorg/chromium/chrome/browser/Tab;)V
.end method

.method public static setActualTabSwitchLatencyMetricRequired()V
    .locals 4

    .prologue
    .line 665
    sget-wide v0, Lorg/chromium/chrome/browser/tabmodel/TabModelBase;->sTabSwitchStartTime:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-gtz v0, :cond_0

    .line 667
    :goto_0
    return-void

    .line 666
    :cond_0
    const/4 v0, 0x1

    sput-boolean v0, Lorg/chromium/chrome/browser/tabmodel/TabModelBase;->sTabSwitchLatencyMetricRequired:Z

    goto :goto_0
.end method

.method private setIndex(I)V
    .locals 0

    .prologue
    .line 630
    invoke-static {p0, p1}, Lorg/chromium/chrome/browser/tabmodel/TabModelUtils;->setIndex(Lorg/chromium/chrome/browser/tabmodel/TabModel;I)V

    .line 631
    return-void
.end method

.method private startTabClosure(Lorg/chromium/chrome/browser/Tab;ZZZ)V
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v2, -0x1

    .line 436
    invoke-virtual {p1}, Lorg/chromium/chrome/browser/Tab;->getId()I

    move-result v3

    .line 437
    invoke-virtual {p0, p1}, Lorg/chromium/chrome/browser/tabmodel/TabModelBase;->indexOf(Lorg/chromium/chrome/browser/Tab;)I

    move-result v4

    .line 439
    invoke-virtual {p1, v1}, Lorg/chromium/chrome/browser/Tab;->setClosing(Z)V

    .line 441
    iget-object v0, p0, Lorg/chromium/chrome/browser/tabmodel/TabModelBase;->mObservers:Lorg/chromium/base/ObserverList;

    invoke-virtual {v0}, Lorg/chromium/base/ObserverList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/chromium/chrome/browser/tabmodel/TabModelObserver;

    invoke-interface {v0, p1, p2}, Lorg/chromium/chrome/browser/tabmodel/TabModelObserver;->willCloseTab(Lorg/chromium/chrome/browser/Tab;Z)V

    goto :goto_0

    .line 443
    :cond_0
    invoke-static {p0}, Lorg/chromium/chrome/browser/tabmodel/TabModelUtils;->getCurrentTab(Lorg/chromium/chrome/browser/tabmodel/TabList;)Lorg/chromium/chrome/browser/Tab;

    move-result-object v5

    .line 444
    if-nez v4, :cond_5

    move v0, v1

    :goto_1
    invoke-virtual {p0, v0}, Lorg/chromium/chrome/browser/tabmodel/TabModelBase;->getTabAt(I)Lorg/chromium/chrome/browser/Tab;

    move-result-object v4

    .line 445
    invoke-virtual {p0, v3}, Lorg/chromium/chrome/browser/tabmodel/TabModelBase;->getNextTabIfClosed(I)Lorg/chromium/chrome/browser/Tab;

    move-result-object v6

    .line 448
    if-nez p4, :cond_1

    invoke-virtual {p0}, Lorg/chromium/chrome/browser/tabmodel/TabModelBase;->commitAllTabClosures()V

    .line 451
    :cond_1
    if-eqz p4, :cond_2

    .line 452
    invoke-virtual {p1}, Lorg/chromium/chrome/browser/Tab;->getWebContents()Lorg/chromium/content_public/browser/WebContents;

    move-result-object v0

    .line 453
    if-eqz v0, :cond_2

    invoke-interface {v0}, Lorg/chromium/content_public/browser/WebContents;->releaseMediaPlayers()V

    .line 456
    :cond_2
    iget-object v0, p0, Lorg/chromium/chrome/browser/tabmodel/TabModelBase;->mTabs:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 458
    if-nez v6, :cond_6

    const/4 v0, 0x0

    move v3, v0

    .line 459
    :goto_2
    if-nez v6, :cond_7

    move v0, v2

    .line 460
    :goto_3
    if-nez v6, :cond_8

    move v1, v2

    .line 463
    :goto_4
    if-eq v6, v5, :cond_a

    .line 464
    invoke-virtual {p0}, Lorg/chromium/chrome/browser/tabmodel/TabModelBase;->isIncognito()Z

    move-result v0

    if-eq v3, v0, :cond_3

    invoke-virtual {p0, v4}, Lorg/chromium/chrome/browser/tabmodel/TabModelBase;->indexOf(Lorg/chromium/chrome/browser/Tab;)I

    move-result v0

    iput v0, p0, Lorg/chromium/chrome/browser/tabmodel/TabModelBase;->mIndex:I

    .line 466
    :cond_3
    iget-object v0, p0, Lorg/chromium/chrome/browser/tabmodel/TabModelBase;->mModelDelegate:Lorg/chromium/chrome/browser/tabmodel/TabModelDelegate;

    invoke-interface {v0, v3}, Lorg/chromium/chrome/browser/tabmodel/TabModelDelegate;->getModel(Z)Lorg/chromium/chrome/browser/tabmodel/TabModel;

    move-result-object v2

    .line 467
    if-eqz p3, :cond_9

    sget-object v0, Lorg/chromium/chrome/browser/tabmodel/TabModel$TabSelectionType;->FROM_EXIT:Lorg/chromium/chrome/browser/tabmodel/TabModel$TabSelectionType;

    :goto_5
    invoke-interface {v2, v1, v0}, Lorg/chromium/chrome/browser/tabmodel/TabModel;->setIndex(ILorg/chromium/chrome/browser/tabmodel/TabModel$TabSelectionType;)V

    .line 473
    :goto_6
    if-nez p4, :cond_4

    iget-object v0, p0, Lorg/chromium/chrome/browser/tabmodel/TabModelBase;->mRewoundList:Lorg/chromium/chrome/browser/tabmodel/TabModelBase$RewoundList;

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/tabmodel/TabModelBase$RewoundList;->resetRewoundState()V

    .line 474
    :cond_4
    return-void

    .line 444
    :cond_5
    add-int/lit8 v0, v4, -0x1

    goto :goto_1

    .line 458
    :cond_6
    invoke-virtual {v6}, Lorg/chromium/chrome/browser/Tab;->isIncognito()Z

    move-result v0

    move v3, v0

    goto :goto_2

    .line 459
    :cond_7
    invoke-virtual {v6}, Lorg/chromium/chrome/browser/Tab;->getId()I

    move-result v0

    goto :goto_3

    .line 460
    :cond_8
    iget-object v1, p0, Lorg/chromium/chrome/browser/tabmodel/TabModelBase;->mModelDelegate:Lorg/chromium/chrome/browser/tabmodel/TabModelDelegate;

    invoke-interface {v1, v3}, Lorg/chromium/chrome/browser/tabmodel/TabModelDelegate;->getModel(Z)Lorg/chromium/chrome/browser/tabmodel/TabModel;

    move-result-object v1

    invoke-static {v1, v0}, Lorg/chromium/chrome/browser/tabmodel/TabModelUtils;->getTabIndexById(Lorg/chromium/chrome/browser/tabmodel/TabList;I)I

    move-result v0

    move v1, v0

    goto :goto_4

    .line 467
    :cond_9
    sget-object v0, Lorg/chromium/chrome/browser/tabmodel/TabModel$TabSelectionType;->FROM_CLOSE:Lorg/chromium/chrome/browser/tabmodel/TabModel$TabSelectionType;

    goto :goto_5

    .line 470
    :cond_a
    iput v1, p0, Lorg/chromium/chrome/browser/tabmodel/TabModelBase;->mIndex:I

    goto :goto_6
.end method

.method public static startTabSwitchLatencyTiming(Lorg/chromium/chrome/browser/tabmodel/TabModel$TabSelectionType;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 653
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    sput-wide v0, Lorg/chromium/chrome/browser/tabmodel/TabModelBase;->sTabSwitchStartTime:J

    .line 654
    sput-object p0, Lorg/chromium/chrome/browser/tabmodel/TabModelBase;->sTabSelectionType:Lorg/chromium/chrome/browser/tabmodel/TabModel$TabSelectionType;

    .line 655
    sput-boolean v2, Lorg/chromium/chrome/browser/tabmodel/TabModelBase;->sTabSwitchLatencyMetricRequired:Z

    .line 656
    sput-boolean v2, Lorg/chromium/chrome/browser/tabmodel/TabModelBase;->sPerceivedTabSwitchLatencyMetricLogged:Z

    .line 657
    return-void
.end method


# virtual methods
.method public addObserver(Lorg/chromium/chrome/browser/tabmodel/TabModelObserver;)V
    .locals 1

    .prologue
    .line 105
    iget-object v0, p0, Lorg/chromium/chrome/browser/tabmodel/TabModelBase;->mObservers:Lorg/chromium/base/ObserverList;

    invoke-virtual {v0, p1}, Lorg/chromium/base/ObserverList;->addObserver(Ljava/lang/Object;)Z

    .line 106
    return-void
.end method

.method public addTab(Lorg/chromium/chrome/browser/Tab;ILorg/chromium/chrome/browser/tabmodel/TabModel$TabLaunchType;)V
    .locals 8

    .prologue
    .line 119
    invoke-static {}, Lorg/chromium/base/TraceEvent;->begin()V

    .line 121
    iget-object v0, p0, Lorg/chromium/chrome/browser/tabmodel/TabModelBase;->mObservers:Lorg/chromium/base/ObserverList;

    invoke-virtual {v0}, Lorg/chromium/base/ObserverList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/chromium/chrome/browser/tabmodel/TabModelObserver;

    invoke-interface {v0, p1, p3}, Lorg/chromium/chrome/browser/tabmodel/TabModelObserver;->willAddTab(Lorg/chromium/chrome/browser/Tab;Lorg/chromium/chrome/browser/tabmodel/TabModel$TabLaunchType;)V

    goto :goto_0

    .line 123
    :cond_0
    iget-object v0, p0, Lorg/chromium/chrome/browser/tabmodel/TabModelBase;->mOrderController:Lorg/chromium/chrome/browser/tabmodel/TabModelOrderController;

    iget-boolean v1, p0, Lorg/chromium/chrome/browser/tabmodel/TabModelBase;->mIsIncognito:Z

    invoke-virtual {v0, p3, v1}, Lorg/chromium/chrome/browser/tabmodel/TabModelOrderController;->willOpenInForeground(Lorg/chromium/chrome/browser/tabmodel/TabModel$TabLaunchType;Z)Z

    move-result v1

    .line 125
    iget-object v0, p0, Lorg/chromium/chrome/browser/tabmodel/TabModelBase;->mOrderController:Lorg/chromium/chrome/browser/tabmodel/TabModelOrderController;

    invoke-virtual {v0, p3, p2, p1}, Lorg/chromium/chrome/browser/tabmodel/TabModelOrderController;->determineInsertionIndex(Lorg/chromium/chrome/browser/tabmodel/TabModel$TabLaunchType;ILorg/chromium/chrome/browser/Tab;)I

    move-result v0

    .line 126
    sget-boolean v2, Lorg/chromium/chrome/browser/tabmodel/TabModelBase;->$assertionsDisabled:Z

    if-nez v2, :cond_1

    iget-object v2, p0, Lorg/chromium/chrome/browser/tabmodel/TabModelBase;->mTabs:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-le v0, v2, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 128
    :cond_1
    sget-boolean v2, Lorg/chromium/chrome/browser/tabmodel/TabModelBase;->$assertionsDisabled:Z

    if-nez v2, :cond_2

    invoke-virtual {p1}, Lorg/chromium/chrome/browser/Tab;->isIncognito()Z

    move-result v2

    iget-boolean v3, p0, Lorg/chromium/chrome/browser/tabmodel/TabModelBase;->mIsIncognito:Z

    if-eq v2, v3, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 131
    :cond_2
    invoke-virtual {p0}, Lorg/chromium/chrome/browser/tabmodel/TabModelBase;->commitAllTabClosures()V

    .line 133
    if-ltz v0, :cond_3

    iget-object v2, p0, Lorg/chromium/chrome/browser/tabmodel/TabModelBase;->mTabs:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-le v0, v2, :cond_7

    .line 134
    :cond_3
    iget-object v0, p0, Lorg/chromium/chrome/browser/tabmodel/TabModelBase;->mTabs:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 142
    :cond_4
    :goto_1
    invoke-direct {p0}, Lorg/chromium/chrome/browser/tabmodel/TabModelBase;->isCurrentModel()Z

    move-result v0

    if-nez v0, :cond_5

    .line 146
    iget v0, p0, Lorg/chromium/chrome/browser/tabmodel/TabModelBase;->mIndex:I

    const/4 v2, 0x0

    invoke-static {v0, v2}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Lorg/chromium/chrome/browser/tabmodel/TabModelBase;->mIndex:I

    .line 149
    :cond_5
    iget-object v0, p0, Lorg/chromium/chrome/browser/tabmodel/TabModelBase;->mRewoundList:Lorg/chromium/chrome/browser/tabmodel/TabModelBase$RewoundList;

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/tabmodel/TabModelBase$RewoundList;->resetRewoundState()V

    .line 151
    invoke-virtual {p0, p1}, Lorg/chromium/chrome/browser/tabmodel/TabModelBase;->indexOf(Lorg/chromium/chrome/browser/Tab;)I

    move-result v2

    .line 152
    iget-object v0, p0, Lorg/chromium/chrome/browser/tabmodel/TabModelBase;->mModelDelegate:Lorg/chromium/chrome/browser/tabmodel/TabModelDelegate;

    invoke-interface {v0}, Lorg/chromium/chrome/browser/tabmodel/TabModelDelegate;->didChange()V

    .line 153
    iget-object v0, p0, Lorg/chromium/chrome/browser/tabmodel/TabModelBase;->mModelDelegate:Lorg/chromium/chrome/browser/tabmodel/TabModelDelegate;

    invoke-interface {v0, p1}, Lorg/chromium/chrome/browser/tabmodel/TabModelDelegate;->didCreateNewTab(Lorg/chromium/chrome/browser/Tab;)V

    .line 155
    iget-wide v4, p0, Lorg/chromium/chrome/browser/tabmodel/TabModelBase;->mNativeTabModelImpl:J

    const-wide/16 v6, 0x0

    cmp-long v0, v4, v6

    if-eqz v0, :cond_6

    iget-wide v4, p0, Lorg/chromium/chrome/browser/tabmodel/TabModelBase;->mNativeTabModelImpl:J

    invoke-direct {p0, v4, v5, p1}, Lorg/chromium/chrome/browser/tabmodel/TabModelBase;->nativeTabAddedToModel(JLorg/chromium/chrome/browser/Tab;)V

    .line 157
    :cond_6
    iget-object v0, p0, Lorg/chromium/chrome/browser/tabmodel/TabModelBase;->mObservers:Lorg/chromium/base/ObserverList;

    invoke-virtual {v0}, Lorg/chromium/base/ObserverList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/chromium/chrome/browser/tabmodel/TabModelObserver;

    invoke-interface {v0, p1, p3}, Lorg/chromium/chrome/browser/tabmodel/TabModelObserver;->didAddTab(Lorg/chromium/chrome/browser/Tab;Lorg/chromium/chrome/browser/tabmodel/TabModel$TabLaunchType;)V

    goto :goto_2

    .line 136
    :cond_7
    iget-object v2, p0, Lorg/chromium/chrome/browser/tabmodel/TabModelBase;->mTabs:Ljava/util/List;

    invoke-interface {v2, v0, p1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 137
    iget v2, p0, Lorg/chromium/chrome/browser/tabmodel/TabModelBase;->mIndex:I

    if-gt v0, v2, :cond_4

    .line 138
    iget v0, p0, Lorg/chromium/chrome/browser/tabmodel/TabModelBase;->mIndex:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/chromium/chrome/browser/tabmodel/TabModelBase;->mIndex:I

    goto :goto_1

    .line 159
    :cond_8
    if-eqz v1, :cond_9

    .line 160
    iget-object v0, p0, Lorg/chromium/chrome/browser/tabmodel/TabModelBase;->mModelDelegate:Lorg/chromium/chrome/browser/tabmodel/TabModelDelegate;

    iget-boolean v1, p0, Lorg/chromium/chrome/browser/tabmodel/TabModelBase;->mIsIncognito:Z

    invoke-interface {v0, v1}, Lorg/chromium/chrome/browser/tabmodel/TabModelDelegate;->selectModel(Z)V

    .line 161
    sget-object v0, Lorg/chromium/chrome/browser/tabmodel/TabModel$TabSelectionType;->FROM_NEW:Lorg/chromium/chrome/browser/tabmodel/TabModel$TabSelectionType;

    invoke-virtual {p0, v2, v0}, Lorg/chromium/chrome/browser/tabmodel/TabModelBase;->setIndex(ILorg/chromium/chrome/browser/tabmodel/TabModel$TabSelectionType;)V

    .line 164
    :cond_9
    invoke-static {}, Lorg/chromium/base/TraceEvent;->end()V

    .line 165
    return-void
.end method

.method public broadcastSessionRestoreComplete()V
    .locals 2

    .prologue
    .line 610
    iget-wide v0, p0, Lorg/chromium/chrome/browser/tabmodel/TabModelBase;->mNativeTabModelImpl:J

    invoke-direct {p0, v0, v1}, Lorg/chromium/chrome/browser/tabmodel/TabModelBase;->nativeBroadcastSessionRestoreComplete(J)V

    .line 611
    return-void
.end method

.method public cancelTabClosure(I)V
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 259
    iget-object v0, p0, Lorg/chromium/chrome/browser/tabmodel/TabModelBase;->mRewoundList:Lorg/chromium/chrome/browser/tabmodel/TabModelBase$RewoundList;

    invoke-virtual {v0, p1}, Lorg/chromium/chrome/browser/tabmodel/TabModelBase$RewoundList;->getPendingRewindTab(I)Lorg/chromium/chrome/browser/Tab;

    move-result-object v3

    .line 260
    if-nez v3, :cond_1

    .line 296
    :cond_0
    return-void

    .line 262
    :cond_1
    invoke-virtual {v3, v1}, Lorg/chromium/chrome/browser/Tab;->setClosing(Z)V

    .line 271
    const/4 v0, -0x1

    .line 272
    iget-object v2, p0, Lorg/chromium/chrome/browser/tabmodel/TabModelBase;->mRewoundList:Lorg/chromium/chrome/browser/tabmodel/TabModelBase$RewoundList;

    invoke-virtual {v2, v3}, Lorg/chromium/chrome/browser/tabmodel/TabModelBase$RewoundList;->indexOf(Lorg/chromium/chrome/browser/Tab;)I

    move-result v4

    move v2, v1

    .line 273
    :goto_0
    if-ge v2, v4, :cond_3

    .line 274
    iget-object v5, p0, Lorg/chromium/chrome/browser/tabmodel/TabModelBase;->mRewoundList:Lorg/chromium/chrome/browser/tabmodel/TabModelBase$RewoundList;

    invoke-virtual {v5, v2}, Lorg/chromium/chrome/browser/tabmodel/TabModelBase$RewoundList;->getTabAt(I)Lorg/chromium/chrome/browser/Tab;

    move-result-object v5

    .line 275
    iget-object v6, p0, Lorg/chromium/chrome/browser/tabmodel/TabModelBase;->mTabs:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v6

    add-int/lit8 v6, v6, -0x1

    if-eq v0, v6, :cond_3

    .line 276
    iget-object v6, p0, Lorg/chromium/chrome/browser/tabmodel/TabModelBase;->mTabs:Ljava/util/List;

    add-int/lit8 v7, v0, 0x1

    invoke-interface {v6, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    if-ne v5, v6, :cond_2

    add-int/lit8 v0, v0, 0x1

    .line 273
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 281
    :cond_3
    add-int/lit8 v2, v0, 0x1

    .line 282
    iget v0, p0, Lorg/chromium/chrome/browser/tabmodel/TabModelBase;->mIndex:I

    if-lt v0, v2, :cond_4

    iget v0, p0, Lorg/chromium/chrome/browser/tabmodel/TabModelBase;->mIndex:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/chromium/chrome/browser/tabmodel/TabModelBase;->mIndex:I

    .line 283
    :cond_4
    iget-object v0, p0, Lorg/chromium/chrome/browser/tabmodel/TabModelBase;->mTabs:Ljava/util/List;

    invoke-interface {v0, v2, v3}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 285
    iget-object v0, p0, Lorg/chromium/chrome/browser/tabmodel/TabModelBase;->mModelDelegate:Lorg/chromium/chrome/browser/tabmodel/TabModelDelegate;

    invoke-interface {v0}, Lorg/chromium/chrome/browser/tabmodel/TabModelDelegate;->getCurrentModel()Lorg/chromium/chrome/browser/tabmodel/TabModel;

    move-result-object v0

    if-ne v0, p0, :cond_5

    const/4 v0, 0x1

    .line 289
    :goto_1
    if-eqz v0, :cond_6

    .line 290
    invoke-static {p0, v2}, Lorg/chromium/chrome/browser/tabmodel/TabModelUtils;->setIndex(Lorg/chromium/chrome/browser/tabmodel/TabModel;I)V

    .line 295
    :goto_2
    iget-object v0, p0, Lorg/chromium/chrome/browser/tabmodel/TabModelBase;->mObservers:Lorg/chromium/base/ObserverList;

    invoke-virtual {v0}, Lorg/chromium/base/ObserverList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/chromium/chrome/browser/tabmodel/TabModelObserver;

    invoke-interface {v0, v3}, Lorg/chromium/chrome/browser/tabmodel/TabModelObserver;->tabClosureUndone(Lorg/chromium/chrome/browser/Tab;)V

    goto :goto_3

    :cond_5
    move v0, v1

    .line 285
    goto :goto_1

    .line 292
    :cond_6
    iput v2, p0, Lorg/chromium/chrome/browser/tabmodel/TabModelBase;->mIndex:I

    goto :goto_2
.end method

.method public closeAllTabs()V
    .locals 1

    .prologue
    .line 344
    invoke-virtual {p0}, Lorg/chromium/chrome/browser/tabmodel/TabModelBase;->commitAllTabClosures()V

    .line 346
    :goto_0
    invoke-virtual {p0}, Lorg/chromium/chrome/browser/tabmodel/TabModelBase;->getCount()I

    move-result v0

    if-lez v0, :cond_0

    .line 347
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lorg/chromium/chrome/browser/tabmodel/TabModelUtils;->closeTabByIndex(Lorg/chromium/chrome/browser/tabmodel/TabModel;I)Z

    goto :goto_0

    .line 349
    :cond_0
    return-void
.end method

.method public closeTab(Lorg/chromium/chrome/browser/Tab;)Z
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 202
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0, v1, v1}, Lorg/chromium/chrome/browser/tabmodel/TabModelBase;->closeTab(Lorg/chromium/chrome/browser/Tab;ZZZ)Z

    move-result v0

    return v0
.end method

.method public closeTab(Lorg/chromium/chrome/browser/Tab;ZZZ)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 321
    if-nez p1, :cond_0

    .line 322
    sget-boolean v1, Lorg/chromium/chrome/browser/tabmodel/TabModelBase;->$assertionsDisabled:Z

    if-nez v1, :cond_4

    new-instance v0, Ljava/lang/AssertionError;

    const-string/jumbo v1, "Tab is null!"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 326
    :cond_0
    iget-object v1, p0, Lorg/chromium/chrome/browser/tabmodel/TabModelBase;->mTabs:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 327
    sget-boolean v1, Lorg/chromium/chrome/browser/tabmodel/TabModelBase;->$assertionsDisabled:Z

    if-nez v1, :cond_4

    new-instance v0, Ljava/lang/AssertionError;

    const-string/jumbo v1, "Tried to close a tab from another model!"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 331
    :cond_1
    invoke-virtual {p0}, Lorg/chromium/chrome/browser/tabmodel/TabModelBase;->supportsPendingClosures()Z

    move-result v0

    and-int v1, p4, v0

    .line 333
    if-eqz v1, :cond_2

    .line 334
    iget-object v0, p0, Lorg/chromium/chrome/browser/tabmodel/TabModelBase;->mObservers:Lorg/chromium/base/ObserverList;

    invoke-virtual {v0}, Lorg/chromium/base/ObserverList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/chromium/chrome/browser/tabmodel/TabModelObserver;

    invoke-interface {v0, p1}, Lorg/chromium/chrome/browser/tabmodel/TabModelObserver;->tabPendingClosure(Lorg/chromium/chrome/browser/Tab;)V

    goto :goto_0

    .line 336
    :cond_2
    invoke-direct {p0, p1, p2, p3, v1}, Lorg/chromium/chrome/browser/tabmodel/TabModelBase;->startTabClosure(Lorg/chromium/chrome/browser/Tab;ZZZ)V

    .line 337
    if-nez v1, :cond_3

    invoke-direct {p0, p1}, Lorg/chromium/chrome/browser/tabmodel/TabModelBase;->finalizeTabClosure(Lorg/chromium/chrome/browser/Tab;)V

    .line 339
    :cond_3
    const/4 v0, 0x1

    :cond_4
    return v0
.end method

.method public commitAllTabClosures()V
    .locals 2

    .prologue
    .line 312
    :goto_0
    iget-object v0, p0, Lorg/chromium/chrome/browser/tabmodel/TabModelBase;->mRewoundList:Lorg/chromium/chrome/browser/tabmodel/TabModelBase$RewoundList;

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/tabmodel/TabModelBase$RewoundList;->getCount()I

    move-result v0

    iget-object v1, p0, Lorg/chromium/chrome/browser/tabmodel/TabModelBase;->mTabs:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-le v0, v1, :cond_0

    .line 313
    iget-object v0, p0, Lorg/chromium/chrome/browser/tabmodel/TabModelBase;->mRewoundList:Lorg/chromium/chrome/browser/tabmodel/TabModelBase$RewoundList;

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/tabmodel/TabModelBase$RewoundList;->getNextRewindableTab()Lorg/chromium/chrome/browser/Tab;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/Tab;->getId()I

    move-result v0

    invoke-virtual {p0, v0}, Lorg/chromium/chrome/browser/tabmodel/TabModelBase;->commitTabClosure(I)V

    goto :goto_0

    .line 316
    :cond_0
    sget-boolean v0, Lorg/chromium/chrome/browser/tabmodel/TabModelBase;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lorg/chromium/chrome/browser/tabmodel/TabModelBase;->mRewoundList:Lorg/chromium/chrome/browser/tabmodel/TabModelBase$RewoundList;

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/tabmodel/TabModelBase$RewoundList;->hasPendingClosures()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 317
    :cond_1
    return-void
.end method

.method public commitTabClosure(I)V
    .locals 3

    .prologue
    .line 300
    iget-object v0, p0, Lorg/chromium/chrome/browser/tabmodel/TabModelBase;->mRewoundList:Lorg/chromium/chrome/browser/tabmodel/TabModelBase$RewoundList;

    invoke-virtual {v0, p1}, Lorg/chromium/chrome/browser/tabmodel/TabModelBase$RewoundList;->getPendingRewindTab(I)Lorg/chromium/chrome/browser/Tab;

    move-result-object v1

    .line 301
    if-nez v1, :cond_1

    .line 308
    :cond_0
    return-void

    .line 305
    :cond_1
    iget-object v0, p0, Lorg/chromium/chrome/browser/tabmodel/TabModelBase;->mRewoundList:Lorg/chromium/chrome/browser/tabmodel/TabModelBase$RewoundList;

    invoke-virtual {v0, v1}, Lorg/chromium/chrome/browser/tabmodel/TabModelBase$RewoundList;->removeTab(Lorg/chromium/chrome/browser/Tab;)V

    .line 306
    invoke-direct {p0, v1}, Lorg/chromium/chrome/browser/tabmodel/TabModelBase;->finalizeTabClosure(Lorg/chromium/chrome/browser/Tab;)V

    .line 307
    iget-object v0, p0, Lorg/chromium/chrome/browser/tabmodel/TabModelBase;->mObservers:Lorg/chromium/base/ObserverList;

    invoke-virtual {v0}, Lorg/chromium/base/ObserverList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/chromium/chrome/browser/tabmodel/TabModelObserver;

    invoke-interface {v0, v1}, Lorg/chromium/chrome/browser/tabmodel/TabModelObserver;->tabClosureCommitted(Lorg/chromium/chrome/browser/Tab;)V

    goto :goto_0
.end method

.method protected abstract createNewTabForDevTools(Ljava/lang/String;)Lorg/chromium/chrome/browser/Tab;
.end method

.method protected abstract createTabWithNativeContents(ZJI)Lorg/chromium/chrome/browser/Tab;
.end method

.method public destroy()V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 88
    iget-object v0, p0, Lorg/chromium/chrome/browser/tabmodel/TabModelBase;->mTabs:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/chromium/chrome/browser/Tab;

    .line 89
    invoke-virtual {v0}, Lorg/chromium/chrome/browser/Tab;->isInitialized()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/Tab;->destroy()V

    goto :goto_0

    .line 92
    :cond_1
    iget-object v0, p0, Lorg/chromium/chrome/browser/tabmodel/TabModelBase;->mRewoundList:Lorg/chromium/chrome/browser/tabmodel/TabModelBase$RewoundList;

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/tabmodel/TabModelBase$RewoundList;->destroy()V

    .line 94
    iget-wide v0, p0, Lorg/chromium/chrome/browser/tabmodel/TabModelBase;->mNativeTabModelImpl:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_2

    .line 95
    iget-wide v0, p0, Lorg/chromium/chrome/browser/tabmodel/TabModelBase;->mNativeTabModelImpl:J

    invoke-direct {p0, v0, v1}, Lorg/chromium/chrome/browser/tabmodel/TabModelBase;->nativeDestroy(J)V

    .line 96
    iput-wide v4, p0, Lorg/chromium/chrome/browser/tabmodel/TabModelBase;->mNativeTabModelImpl:J

    .line 99
    :cond_2
    iget-object v0, p0, Lorg/chromium/chrome/browser/tabmodel/TabModelBase;->mTabs:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 100
    iget-object v0, p0, Lorg/chromium/chrome/browser/tabmodel/TabModelBase;->mObservers:Lorg/chromium/base/ObserverList;

    invoke-virtual {v0}, Lorg/chromium/base/ObserverList;->clear()V

    .line 101
    return-void
.end method

.method public getComprehensiveModel()Lorg/chromium/chrome/browser/tabmodel/TabList;
    .locals 1

    .prologue
    .line 253
    invoke-virtual {p0}, Lorg/chromium/chrome/browser/tabmodel/TabModelBase;->supportsPendingClosures()Z

    move-result v0

    if-nez v0, :cond_0

    .line 254
    :goto_0
    return-object p0

    :cond_0
    iget-object p0, p0, Lorg/chromium/chrome/browser/tabmodel/TabModelBase;->mRewoundList:Lorg/chromium/chrome/browser/tabmodel/TabModelBase$RewoundList;

    goto :goto_0
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 618
    iget-object v0, p0, Lorg/chromium/chrome/browser/tabmodel/TabModelBase;->mTabs:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getNextTabIfClosed(I)Lorg/chromium/chrome/browser/Tab;
    .locals 5

    .prologue
    .line 213
    invoke-static {p0, p1}, Lorg/chromium/chrome/browser/tabmodel/TabModelUtils;->getTabById(Lorg/chromium/chrome/browser/tabmodel/TabList;I)Lorg/chromium/chrome/browser/Tab;

    move-result-object v4

    .line 214
    invoke-static {p0}, Lorg/chromium/chrome/browser/tabmodel/TabModelUtils;->getCurrentTab(Lorg/chromium/chrome/browser/tabmodel/TabList;)Lorg/chromium/chrome/browser/Tab;

    move-result-object v1

    .line 215
    if-nez v4, :cond_0

    .line 238
    :goto_0
    return-object v1

    .line 217
    :cond_0
    invoke-virtual {p0, v4}, Lorg/chromium/chrome/browser/tabmodel/TabModelBase;->indexOf(Lorg/chromium/chrome/browser/Tab;)I

    move-result v0

    .line 218
    if-nez v0, :cond_2

    const/4 v0, 0x1

    :goto_1
    invoke-virtual {p0, v0}, Lorg/chromium/chrome/browser/tabmodel/TabModelBase;->getTabAt(I)Lorg/chromium/chrome/browser/Tab;

    move-result-object v2

    .line 219
    invoke-virtual {v4}, Lorg/chromium/chrome/browser/Tab;->getParentId()I

    move-result v0

    invoke-direct {p0, v0}, Lorg/chromium/chrome/browser/tabmodel/TabModelBase;->findTabInAllTabModels(I)Lorg/chromium/chrome/browser/Tab;

    move-result-object v0

    .line 227
    const/4 v3, 0x0

    .line 228
    if-eq v4, v1, :cond_3

    if-eqz v1, :cond_3

    move-object v0, v1

    :cond_1
    :goto_2
    move-object v1, v0

    .line 238
    goto :goto_0

    .line 218
    :cond_2
    add-int/lit8 v0, v0, -0x1

    goto :goto_1

    .line 230
    :cond_3
    if-eqz v0, :cond_4

    iget-object v1, p0, Lorg/chromium/chrome/browser/tabmodel/TabModelBase;->mModelDelegate:Lorg/chromium/chrome/browser/tabmodel/TabModelDelegate;

    invoke-interface {v1}, Lorg/chromium/chrome/browser/tabmodel/TabModelDelegate;->isInOverviewMode()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 232
    :cond_4
    if-eqz v2, :cond_5

    move-object v0, v2

    .line 233
    goto :goto_2

    .line 234
    :cond_5
    iget-boolean v0, p0, Lorg/chromium/chrome/browser/tabmodel/TabModelBase;->mIsIncognito:Z

    if-eqz v0, :cond_6

    .line 235
    iget-object v0, p0, Lorg/chromium/chrome/browser/tabmodel/TabModelBase;->mModelDelegate:Lorg/chromium/chrome/browser/tabmodel/TabModelDelegate;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lorg/chromium/chrome/browser/tabmodel/TabModelDelegate;->getModel(Z)Lorg/chromium/chrome/browser/tabmodel/TabModel;

    move-result-object v0

    invoke-static {v0}, Lorg/chromium/chrome/browser/tabmodel/TabModelUtils;->getCurrentTab(Lorg/chromium/chrome/browser/tabmodel/TabList;)Lorg/chromium/chrome/browser/Tab;

    move-result-object v0

    goto :goto_2

    :cond_6
    move-object v0, v3

    goto :goto_2
.end method

.method public getProfile()Lorg/chromium/chrome/browser/profiles/Profile;
    .locals 2

    .prologue
    .line 78
    iget-wide v0, p0, Lorg/chromium/chrome/browser/tabmodel/TabModelBase;->mNativeTabModelImpl:J

    invoke-direct {p0, v0, v1}, Lorg/chromium/chrome/browser/tabmodel/TabModelBase;->nativeGetProfileAndroid(J)Lorg/chromium/chrome/browser/profiles/Profile;

    move-result-object v0

    return-object v0
.end method

.method public getTabAt(I)Lorg/chromium/chrome/browser/Tab;
    .locals 1

    .prologue
    .line 355
    if-ltz p1, :cond_0

    iget-object v0, p0, Lorg/chromium/chrome/browser/tabmodel/TabModelBase;->mTabs:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lt p1, v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    .line 356
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lorg/chromium/chrome/browser/tabmodel/TabModelBase;->mTabs:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/chromium/chrome/browser/Tab;

    goto :goto_0
.end method

.method public index()I
    .locals 1

    .prologue
    .line 624
    iget v0, p0, Lorg/chromium/chrome/browser/tabmodel/TabModelBase;->mIndex:I

    return v0
.end method

.method public indexOf(Lorg/chromium/chrome/browser/Tab;)I
    .locals 1

    .prologue
    .line 362
    iget-object v0, p0, Lorg/chromium/chrome/browser/tabmodel/TabModelBase;->mTabs:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public isClosurePending(I)Z
    .locals 1

    .prologue
    .line 243
    iget-object v0, p0, Lorg/chromium/chrome/browser/tabmodel/TabModelBase;->mRewoundList:Lorg/chromium/chrome/browser/tabmodel/TabModelBase$RewoundList;

    invoke-virtual {v0, p1}, Lorg/chromium/chrome/browser/tabmodel/TabModelBase$RewoundList;->getPendingRewindTab(I)Lorg/chromium/chrome/browser/Tab;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isIncognito()Z
    .locals 1

    .prologue
    .line 83
    iget-boolean v0, p0, Lorg/chromium/chrome/browser/tabmodel/TabModelBase;->mIsIncognito:Z

    return v0
.end method

.method public moveTab(II)V
    .locals 5

    .prologue
    .line 169
    const/4 v0, 0x0

    iget-object v1, p0, Lorg/chromium/chrome/browser/tabmodel/TabModelBase;->mTabs:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    invoke-static {p2, v0, v1}, Lorg/chromium/chrome/browser/util/MathUtils;->clamp(III)I

    move-result v1

    .line 171
    invoke-static {p0, p1}, Lorg/chromium/chrome/browser/tabmodel/TabModelUtils;->getTabIndexById(Lorg/chromium/chrome/browser/tabmodel/TabList;I)I

    move-result v3

    .line 173
    const/4 v0, -0x1

    if-eq v3, v0, :cond_0

    if-eq v3, v1, :cond_0

    add-int/lit8 v0, v3, 0x1

    if-ne v0, v1, :cond_1

    .line 197
    :cond_0
    return-void

    .line 178
    :cond_1
    invoke-virtual {p0}, Lorg/chromium/chrome/browser/tabmodel/TabModelBase;->commitAllTabClosures()V

    .line 180
    iget-object v0, p0, Lorg/chromium/chrome/browser/tabmodel/TabModelBase;->mTabs:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/chromium/chrome/browser/Tab;

    .line 181
    if-ge v3, v1, :cond_5

    add-int/lit8 v1, v1, -0x1

    move v2, v1

    .line 183
    :goto_0
    iget-object v1, p0, Lorg/chromium/chrome/browser/tabmodel/TabModelBase;->mTabs:Ljava/util/List;

    invoke-interface {v1, v2, v0}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 185
    iget v1, p0, Lorg/chromium/chrome/browser/tabmodel/TabModelBase;->mIndex:I

    if-ne v3, v1, :cond_3

    .line 186
    iput v2, p0, Lorg/chromium/chrome/browser/tabmodel/TabModelBase;->mIndex:I

    .line 193
    :cond_2
    :goto_1
    iget-object v1, p0, Lorg/chromium/chrome/browser/tabmodel/TabModelBase;->mRewoundList:Lorg/chromium/chrome/browser/tabmodel/TabModelBase$RewoundList;

    invoke-virtual {v1}, Lorg/chromium/chrome/browser/tabmodel/TabModelBase$RewoundList;->resetRewoundState()V

    .line 195
    iget-object v1, p0, Lorg/chromium/chrome/browser/tabmodel/TabModelBase;->mModelDelegate:Lorg/chromium/chrome/browser/tabmodel/TabModelDelegate;

    invoke-interface {v1}, Lorg/chromium/chrome/browser/tabmodel/TabModelDelegate;->didChange()V

    .line 196
    iget-object v1, p0, Lorg/chromium/chrome/browser/tabmodel/TabModelBase;->mObservers:Lorg/chromium/base/ObserverList;

    invoke-virtual {v1}, Lorg/chromium/base/ObserverList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/chromium/chrome/browser/tabmodel/TabModelObserver;

    invoke-interface {v1, v0, v2, v3}, Lorg/chromium/chrome/browser/tabmodel/TabModelObserver;->didMoveTab(Lorg/chromium/chrome/browser/Tab;II)V

    goto :goto_2

    .line 187
    :cond_3
    iget v1, p0, Lorg/chromium/chrome/browser/tabmodel/TabModelBase;->mIndex:I

    if-ge v3, v1, :cond_4

    iget v1, p0, Lorg/chromium/chrome/browser/tabmodel/TabModelBase;->mIndex:I

    if-lt v2, v1, :cond_4

    .line 188
    iget v1, p0, Lorg/chromium/chrome/browser/tabmodel/TabModelBase;->mIndex:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lorg/chromium/chrome/browser/tabmodel/TabModelBase;->mIndex:I

    goto :goto_1

    .line 189
    :cond_4
    iget v1, p0, Lorg/chromium/chrome/browser/tabmodel/TabModelBase;->mIndex:I

    if-le v3, v1, :cond_2

    iget v1, p0, Lorg/chromium/chrome/browser/tabmodel/TabModelBase;->mIndex:I

    if-gt v2, v1, :cond_2

    .line 190
    iget v1, p0, Lorg/chromium/chrome/browser/tabmodel/TabModelBase;->mIndex:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lorg/chromium/chrome/browser/tabmodel/TabModelBase;->mIndex:I

    goto :goto_1

    :cond_5
    move v2, v1

    goto :goto_0
.end method

.method public setIndex(ILorg/chromium/chrome/browser/tabmodel/TabModel$TabSelectionType;)V
    .locals 4

    .prologue
    .line 384
    invoke-static {}, Lorg/chromium/base/TraceEvent;->begin()V

    .line 385
    invoke-direct {p0, p2}, Lorg/chromium/chrome/browser/tabmodel/TabModelBase;->getLastId(Lorg/chromium/chrome/browser/tabmodel/TabModel$TabSelectionType;)I

    move-result v1

    .line 387
    invoke-direct {p0}, Lorg/chromium/chrome/browser/tabmodel/TabModelBase;->isCurrentModel()Z

    move-result v0

    if-nez v0, :cond_0

    .line 388
    iget-object v0, p0, Lorg/chromium/chrome/browser/tabmodel/TabModelBase;->mModelDelegate:Lorg/chromium/chrome/browser/tabmodel/TabModelDelegate;

    invoke-virtual {p0}, Lorg/chromium/chrome/browser/tabmodel/TabModelBase;->isIncognito()Z

    move-result v2

    invoke-interface {v0, v2}, Lorg/chromium/chrome/browser/tabmodel/TabModelDelegate;->selectModel(Z)V

    .line 391
    :cond_0
    iget-object v0, p0, Lorg/chromium/chrome/browser/tabmodel/TabModelBase;->mTabs:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-gtz v0, :cond_1

    .line 392
    const/4 v0, -0x1

    iput v0, p0, Lorg/chromium/chrome/browser/tabmodel/TabModelBase;->mIndex:I

    .line 397
    :goto_0
    invoke-static {p0}, Lorg/chromium/chrome/browser/tabmodel/TabModelUtils;->getCurrentTab(Lorg/chromium/chrome/browser/tabmodel/TabList;)Lorg/chromium/chrome/browser/Tab;

    move-result-object v2

    .line 399
    iget-object v0, p0, Lorg/chromium/chrome/browser/tabmodel/TabModelBase;->mModelDelegate:Lorg/chromium/chrome/browser/tabmodel/TabModelDelegate;

    invoke-interface {v0, v2, p2}, Lorg/chromium/chrome/browser/tabmodel/TabModelDelegate;->requestToShowTab(Lorg/chromium/chrome/browser/Tab;Lorg/chromium/chrome/browser/tabmodel/TabModel$TabSelectionType;)V

    .line 401
    if-eqz v2, :cond_2

    .line 402
    iget-object v0, p0, Lorg/chromium/chrome/browser/tabmodel/TabModelBase;->mObservers:Lorg/chromium/base/ObserverList;

    invoke-virtual {v0}, Lorg/chromium/base/ObserverList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/chromium/chrome/browser/tabmodel/TabModelObserver;

    invoke-interface {v0, v2, p2, v1}, Lorg/chromium/chrome/browser/tabmodel/TabModelObserver;->didSelectTab(Lorg/chromium/chrome/browser/Tab;Lorg/chromium/chrome/browser/tabmodel/TabModel$TabSelectionType;I)V

    goto :goto_1

    .line 394
    :cond_1
    const/4 v0, 0x0

    iget-object v2, p0, Lorg/chromium/chrome/browser/tabmodel/TabModelBase;->mTabs:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-static {p1, v0, v2}, Lorg/chromium/chrome/browser/util/MathUtils;->clamp(III)I

    move-result v0

    iput v0, p0, Lorg/chromium/chrome/browser/tabmodel/TabModelBase;->mIndex:I

    goto :goto_0

    .line 408
    :cond_2
    iget-object v0, p0, Lorg/chromium/chrome/browser/tabmodel/TabModelBase;->mModelDelegate:Lorg/chromium/chrome/browser/tabmodel/TabModelDelegate;

    invoke-interface {v0}, Lorg/chromium/chrome/browser/tabmodel/TabModelDelegate;->didChange()V

    .line 409
    invoke-static {}, Lorg/chromium/base/TraceEvent;->end()V

    .line 410
    return-void
.end method

.method public supportsPendingClosures()Z
    .locals 1

    .prologue
    .line 248
    iget-boolean v0, p0, Lorg/chromium/chrome/browser/tabmodel/TabModelBase;->mIsIncognito:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
