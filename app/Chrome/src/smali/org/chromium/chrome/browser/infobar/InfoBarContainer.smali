.class public Lorg/chromium/chrome/browser/infobar/InfoBarContainer;
.super Landroid/widget/ScrollView;
.source "InfoBarContainer.java"


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private final mActivity:Landroid/app/Activity;

.field private mAnimation:Lorg/chromium/chrome/browser/infobar/AnimationHelper;

.field private mAnimationListener:Lorg/chromium/chrome/browser/infobar/InfoBarContainer$InfoBarAnimationListener;

.field private final mAnimationSizer:Landroid/widget/FrameLayout;

.field private final mAutoLoginDelegate:Lorg/chromium/chrome/browser/infobar/AutoLoginDelegate;

.field private mDestroyed:Z

.field private mDistanceFromBottom:I

.field private mDoStayInvisible:Z

.field private mHeight:I

.field private final mInfoBarTransitions:Ljava/util/ArrayDeque;

.field private final mInfoBars:Ljava/util/ArrayList;

.field private mInnerHeight:I

.field private mLinearLayout:Landroid/widget/LinearLayout;

.field private mNativeInfoBarContainer:J

.field private mParentView:Landroid/view/ViewGroup;

.field private mTabId:I

.field private mTabObserver:Lorg/chromium/chrome/browser/TabObserver;

.field private mTopBorderPaint:Landroid/graphics/Paint;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 41
    const-class v0, Lorg/chromium/chrome/browser/infobar/InfoBarContainer;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/chromium/chrome/browser/infobar/InfoBarContainer;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Landroid/app/Activity;Lorg/chromium/chrome/browser/infobar/AutoLoginProcessor;ILandroid/view/ViewGroup;Lorg/chromium/content_public/browser/WebContents;)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/4 v4, -0x1

    const/4 v3, -0x2

    .line 122
    invoke-direct {p0, p1}, Landroid/widget/ScrollView;-><init>(Landroid/content/Context;)V

    .line 87
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lorg/chromium/chrome/browser/infobar/InfoBarContainer;->mInfoBars:Ljava/util/ArrayList;

    .line 97
    iput-boolean v1, p0, Lorg/chromium/chrome/browser/infobar/InfoBarContainer;->mDestroyed:Z

    .line 125
    invoke-virtual {p0, v1}, Lorg/chromium/chrome/browser/infobar/InfoBarContainer;->setVerticalScrollBarEnabled(Z)V

    .line 127
    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    const/16 v0, 0x50

    invoke-direct {v1, v4, v3, v0}, Landroid/widget/FrameLayout$LayoutParams;-><init>(III)V

    .line 129
    invoke-static {p1}, Lorg/chromium/ui/base/DeviceFormFactor;->isTablet(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x60

    .line 132
    :goto_0
    int-to-float v0, v0

    invoke-virtual {p0}, Lorg/chromium/chrome/browser/infobar/InfoBarContainer;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v2, v2, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v0, v2

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    iput v0, v1, Landroid/widget/FrameLayout$LayoutParams;->topMargin:I

    .line 133
    invoke-virtual {p0, v1}, Lorg/chromium/chrome/browser/infobar/InfoBarContainer;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 135
    new-instance v0, Landroid/widget/LinearLayout;

    invoke-direct {v0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lorg/chromium/chrome/browser/infobar/InfoBarContainer;->mLinearLayout:Landroid/widget/LinearLayout;

    .line 136
    iget-object v0, p0, Lorg/chromium/chrome/browser/infobar/InfoBarContainer;->mLinearLayout:Landroid/widget/LinearLayout;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 137
    iget-object v0, p0, Lorg/chromium/chrome/browser/infobar/InfoBarContainer;->mLinearLayout:Landroid/widget/LinearLayout;

    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v1, v4, v3}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {p0, v0, v1}, Lorg/chromium/chrome/browser/infobar/InfoBarContainer;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 140
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/chromium/chrome/browser/infobar/InfoBarContainer;->mAnimationListener:Lorg/chromium/chrome/browser/infobar/InfoBarContainer$InfoBarAnimationListener;

    .line 141
    new-instance v0, Ljava/util/ArrayDeque;

    invoke-direct {v0}, Ljava/util/ArrayDeque;-><init>()V

    iput-object v0, p0, Lorg/chromium/chrome/browser/infobar/InfoBarContainer;->mInfoBarTransitions:Ljava/util/ArrayDeque;

    .line 143
    new-instance v0, Lorg/chromium/chrome/browser/infobar/AutoLoginDelegate;

    invoke-direct {v0, p2, p1}, Lorg/chromium/chrome/browser/infobar/AutoLoginDelegate;-><init>(Lorg/chromium/chrome/browser/infobar/AutoLoginProcessor;Landroid/app/Activity;)V

    iput-object v0, p0, Lorg/chromium/chrome/browser/infobar/InfoBarContainer;->mAutoLoginDelegate:Lorg/chromium/chrome/browser/infobar/AutoLoginDelegate;

    .line 144
    iput-object p1, p0, Lorg/chromium/chrome/browser/infobar/InfoBarContainer;->mActivity:Landroid/app/Activity;

    .line 145
    iput p3, p0, Lorg/chromium/chrome/browser/infobar/InfoBarContainer;->mTabId:I

    .line 146
    iput-object p4, p0, Lorg/chromium/chrome/browser/infobar/InfoBarContainer;->mParentView:Landroid/view/ViewGroup;

    .line 148
    new-instance v0, Landroid/widget/FrameLayout;

    invoke-direct {v0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lorg/chromium/chrome/browser/infobar/InfoBarContainer;->mAnimationSizer:Landroid/widget/FrameLayout;

    .line 149
    iget-object v0, p0, Lorg/chromium/chrome/browser/infobar/InfoBarContainer;->mAnimationSizer:Landroid/widget/FrameLayout;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 153
    iget-object v0, p0, Lorg/chromium/chrome/browser/infobar/InfoBarContainer;->mAutoLoginDelegate:Lorg/chromium/chrome/browser/infobar/AutoLoginDelegate;

    invoke-direct {p0, p5, v0}, Lorg/chromium/chrome/browser/infobar/InfoBarContainer;->nativeInit(Lorg/chromium/content_public/browser/WebContents;Lorg/chromium/chrome/browser/infobar/AutoLoginDelegate;)J

    move-result-wide v0

    iput-wide v0, p0, Lorg/chromium/chrome/browser/infobar/InfoBarContainer;->mNativeInfoBarContainer:J

    .line 154
    return-void

    .line 129
    :cond_0
    const/16 v0, 0x38

    goto :goto_0
.end method

.method private addToParentView()V
    .locals 2

    .prologue
    .line 193
    iget-object v0, p0, Lorg/chromium/chrome/browser/infobar/InfoBarContainer;->mParentView:Landroid/view/ViewGroup;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/chromium/chrome/browser/infobar/InfoBarContainer;->mParentView:Landroid/view/ViewGroup;

    invoke-virtual {v0, p0}, Landroid/view/ViewGroup;->indexOfChild(Landroid/view/View;)I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 194
    iget-object v0, p0, Lorg/chromium/chrome/browser/infobar/InfoBarContainer;->mParentView:Landroid/view/ViewGroup;

    invoke-virtual {v0, p0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 196
    :cond_0
    return-void
.end method

.method private createTabObserver()Lorg/chromium/chrome/browser/TabObserver;
    .locals 1

    .prologue
    .line 239
    new-instance v0, Lorg/chromium/chrome/browser/infobar/InfoBarContainer$1;

    invoke-direct {v0, p0}, Lorg/chromium/chrome/browser/infobar/InfoBarContainer$1;-><init>(Lorg/chromium/chrome/browser/infobar/InfoBarContainer;)V

    return-object v0
.end method

.method private enqueueInfoBarAnimation(Lorg/chromium/chrome/browser/infobar/InfoBar;Landroid/view/View;I)V
    .locals 2

    .prologue
    .line 366
    new-instance v0, Lorg/chromium/chrome/browser/infobar/InfoBarContainer$InfoBarTransitionInfo;

    invoke-direct {v0, p1, p2, p3}, Lorg/chromium/chrome/browser/infobar/InfoBarContainer$InfoBarTransitionInfo;-><init>(Lorg/chromium/chrome/browser/infobar/InfoBar;Landroid/view/View;I)V

    .line 367
    iget-object v1, p0, Lorg/chromium/chrome/browser/infobar/InfoBarContainer;->mInfoBarTransitions:Ljava/util/ArrayDeque;

    invoke-virtual {v1, v0}, Ljava/util/ArrayDeque;->add(Ljava/lang/Object;)Z

    .line 368
    invoke-direct {p0}, Lorg/chromium/chrome/browser/infobar/InfoBarContainer;->processPendingInfoBars()V

    .line 369
    return-void
.end method

.method private native nativeDestroy(J)V
.end method

.method private native nativeInit(Lorg/chromium/content_public/browser/WebContents;Lorg/chromium/chrome/browser/infobar/AutoLoginDelegate;)J
.end method

.method private processPendingInfoBars()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v5, -0x2

    .line 432
    iget-object v0, p0, Lorg/chromium/chrome/browser/infobar/InfoBarContainer;->mAnimation:Lorg/chromium/chrome/browser/infobar/AnimationHelper;

    if-nez v0, :cond_0

    iget-object v0, p0, Lorg/chromium/chrome/browser/infobar/InfoBarContainer;->mInfoBarTransitions:Ljava/util/ArrayDeque;

    invoke-virtual {v0}, Ljava/util/ArrayDeque;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 454
    :cond_0
    :goto_0
    return-void

    .line 435
    :cond_1
    iget-object v0, p0, Lorg/chromium/chrome/browser/infobar/InfoBarContainer;->mInfoBarTransitions:Ljava/util/ArrayDeque;

    invoke-virtual {v0}, Ljava/util/ArrayDeque;->remove()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lorg/chromium/chrome/browser/infobar/InfoBarContainer$InfoBarTransitionInfo;

    .line 436
    iget-object v4, v1, Lorg/chromium/chrome/browser/infobar/InfoBarContainer$InfoBarTransitionInfo;->toShow:Landroid/view/View;

    .line 439
    invoke-direct {p0}, Lorg/chromium/chrome/browser/infobar/InfoBarContainer;->addToParentView()V

    .line 441
    iget v0, v1, Lorg/chromium/chrome/browser/infobar/InfoBarContainer$InfoBarTransitionInfo;->animationType:I

    if-nez v0, :cond_3

    .line 442
    iget-object v0, v1, Lorg/chromium/chrome/browser/infobar/InfoBarContainer$InfoBarTransitionInfo;->target:Lorg/chromium/chrome/browser/infobar/InfoBar;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lorg/chromium/chrome/browser/infobar/InfoBar;->getContentWrapper(Z)Lorg/chromium/chrome/browser/infobar/ContentWrapperView;

    move-result-object v2

    .line 443
    sget-boolean v0, Lorg/chromium/chrome/browser/infobar/InfoBarContainer;->$assertionsDisabled:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lorg/chromium/chrome/browser/infobar/InfoBarContainer;->mInfoBars:Ljava/util/ArrayList;

    iget-object v3, v1, Lorg/chromium/chrome/browser/infobar/InfoBarContainer$InfoBarTransitionInfo;->target:Lorg/chromium/chrome/browser/infobar/InfoBar;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 444
    :cond_2
    invoke-virtual {v2}, Lorg/chromium/chrome/browser/infobar/ContentWrapperView;->detachCurrentView()Landroid/view/View;

    move-result-object v4

    .line 445
    iget-object v0, p0, Lorg/chromium/chrome/browser/infobar/InfoBarContainer;->mLinearLayout:Landroid/widget/LinearLayout;

    new-instance v3, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v3, v5, v5}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v2, v6, v3}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    .line 452
    :goto_1
    new-instance v0, Lorg/chromium/chrome/browser/infobar/AnimationHelper;

    iget-object v3, v1, Lorg/chromium/chrome/browser/infobar/InfoBarContainer$InfoBarTransitionInfo;->target:Lorg/chromium/chrome/browser/infobar/InfoBar;

    iget v5, v1, Lorg/chromium/chrome/browser/infobar/InfoBarContainer$InfoBarTransitionInfo;->animationType:I

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lorg/chromium/chrome/browser/infobar/AnimationHelper;-><init>(Lorg/chromium/chrome/browser/infobar/InfoBarContainer;Lorg/chromium/chrome/browser/infobar/ContentWrapperView;Lorg/chromium/chrome/browser/infobar/InfoBar;Landroid/view/View;I)V

    iput-object v0, p0, Lorg/chromium/chrome/browser/infobar/InfoBarContainer;->mAnimation:Lorg/chromium/chrome/browser/infobar/AnimationHelper;

    .line 453
    iget-object v0, p0, Lorg/chromium/chrome/browser/infobar/InfoBarContainer;->mAnimation:Lorg/chromium/chrome/browser/infobar/AnimationHelper;

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/infobar/AnimationHelper;->start()V

    goto :goto_0

    .line 448
    :cond_3
    iget-object v0, v1, Lorg/chromium/chrome/browser/infobar/InfoBarContainer$InfoBarTransitionInfo;->target:Lorg/chromium/chrome/browser/infobar/InfoBar;

    invoke-virtual {v0, v6}, Lorg/chromium/chrome/browser/infobar/InfoBar;->getContentWrapper(Z)Lorg/chromium/chrome/browser/infobar/ContentWrapperView;

    move-result-object v2

    goto :goto_1
.end method


# virtual methods
.method public addInfoBar(Lorg/chromium/chrome/browser/infobar/InfoBar;)V
    .locals 2

    .prologue
    .line 271
    sget-boolean v0, Lorg/chromium/chrome/browser/infobar/InfoBarContainer;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lorg/chromium/chrome/browser/infobar/InfoBarContainer;->mDestroyed:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 272
    :cond_0
    if-nez p1, :cond_2

    .line 288
    :cond_1
    :goto_0
    return-void

    .line 275
    :cond_2
    iget-object v0, p0, Lorg/chromium/chrome/browser/infobar/InfoBarContainer;->mInfoBars:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 276
    sget-boolean v0, Lorg/chromium/chrome/browser/infobar/InfoBarContainer;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    const-string/jumbo v1, "Trying to add an info bar that has already been added."

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 283
    :cond_3
    iget-object v0, p0, Lorg/chromium/chrome/browser/infobar/InfoBarContainer;->mInfoBars:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 284
    iget-object v0, p0, Lorg/chromium/chrome/browser/infobar/InfoBarContainer;->mActivity:Landroid/app/Activity;

    invoke-virtual {p1, v0}, Lorg/chromium/chrome/browser/infobar/InfoBar;->setContext(Landroid/content/Context;)V

    .line 285
    invoke-virtual {p1, p0}, Lorg/chromium/chrome/browser/infobar/InfoBar;->setInfoBarContainer(Lorg/chromium/chrome/browser/infobar/InfoBarContainer;)V

    .line 287
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lorg/chromium/chrome/browser/infobar/InfoBarContainer;->enqueueInfoBarAnimation(Lorg/chromium/chrome/browser/infobar/InfoBar;Landroid/view/View;I)V

    goto :goto_0
.end method

.method public destroy()V
    .locals 4

    .prologue
    .line 479
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/chromium/chrome/browser/infobar/InfoBarContainer;->mDestroyed:Z

    .line 480
    iget-object v0, p0, Lorg/chromium/chrome/browser/infobar/InfoBarContainer;->mLinearLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 481
    iget-wide v0, p0, Lorg/chromium/chrome/browser/infobar/InfoBarContainer;->mNativeInfoBarContainer:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 482
    iget-wide v0, p0, Lorg/chromium/chrome/browser/infobar/InfoBarContainer;->mNativeInfoBarContainer:J

    invoke-direct {p0, v0, v1}, Lorg/chromium/chrome/browser/infobar/InfoBarContainer;->nativeDestroy(J)V

    .line 484
    :cond_0
    iget-object v0, p0, Lorg/chromium/chrome/browser/infobar/InfoBarContainer;->mInfoBarTransitions:Ljava/util/ArrayDeque;

    invoke-virtual {v0}, Ljava/util/ArrayDeque;->clear()V

    .line 485
    return-void
.end method

.method public dismissAutoLoginInfoBars()V
    .locals 5

    .prologue
    .line 514
    iget-object v0, p0, Lorg/chromium/chrome/browser/infobar/InfoBarContainer;->mAutoLoginDelegate:Lorg/chromium/chrome/browser/infobar/AutoLoginDelegate;

    const-string/jumbo v1, ""

    const-string/jumbo v2, ""

    const/4 v3, 0x0

    const-string/jumbo v4, ""

    invoke-virtual {v0, v1, v2, v3, v4}, Lorg/chromium/chrome/browser/infobar/AutoLoginDelegate;->dismissAutoLogins(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)V

    .line 515
    return-void
.end method

.method protected dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 6

    .prologue
    .line 407
    invoke-super {p0, p1}, Landroid/widget/ScrollView;->dispatchDraw(Landroid/graphics/Canvas;)V

    .line 412
    invoke-virtual {p0}, Lorg/chromium/chrome/browser/infobar/InfoBarContainer;->getScrollY()I

    move-result v0

    if-eqz v0, :cond_1

    .line 413
    iget-object v0, p0, Lorg/chromium/chrome/browser/infobar/InfoBarContainer;->mTopBorderPaint:Landroid/graphics/Paint;

    if-nez v0, :cond_0

    .line 414
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lorg/chromium/chrome/browser/infobar/InfoBarContainer;->mTopBorderPaint:Landroid/graphics/Paint;

    .line 415
    iget-object v0, p0, Lorg/chromium/chrome/browser/infobar/InfoBarContainer;->mTopBorderPaint:Landroid/graphics/Paint;

    invoke-virtual {p0}, Lorg/chromium/chrome/browser/infobar/InfoBarContainer;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lorg/chromium/chrome/R$color;->infobar_background_separator:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 418
    :cond_0
    invoke-virtual {p0}, Lorg/chromium/chrome/browser/infobar/InfoBarContainer;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lorg/chromium/chrome/browser/infobar/ContentWrapperView;->getBoundaryHeight(Landroid/content/Context;)I

    move-result v0

    .line 419
    const/4 v1, 0x0

    invoke-virtual {p0}, Lorg/chromium/chrome/browser/infobar/InfoBarContainer;->getScrollY()I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {p0}, Lorg/chromium/chrome/browser/infobar/InfoBarContainer;->getWidth()I

    move-result v3

    int-to-float v3, v3

    invoke-virtual {p0}, Lorg/chromium/chrome/browser/infobar/InfoBarContainer;->getScrollY()I

    move-result v4

    add-int/2addr v0, v4

    int-to-float v4, v0

    iget-object v5, p0, Lorg/chromium/chrome/browser/infobar/InfoBarContainer;->mTopBorderPaint:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 421
    :cond_1
    return-void
.end method

.method public findLastTransitionForInfoBar(Lorg/chromium/chrome/browser/infobar/InfoBar;)Lorg/chromium/chrome/browser/infobar/InfoBarContainer$InfoBarTransitionInfo;
    .locals 3

    .prologue
    .line 295
    iget-object v0, p0, Lorg/chromium/chrome/browser/infobar/InfoBarContainer;->mInfoBarTransitions:Ljava/util/ArrayDeque;

    invoke-virtual {v0}, Ljava/util/ArrayDeque;->descendingIterator()Ljava/util/Iterator;

    move-result-object v1

    .line 296
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 297
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/chromium/chrome/browser/infobar/InfoBarContainer$InfoBarTransitionInfo;

    .line 298
    iget-object v2, v0, Lorg/chromium/chrome/browser/infobar/InfoBarContainer$InfoBarTransitionInfo;->target:Lorg/chromium/chrome/browser/infobar/InfoBar;

    if-ne v2, p1, :cond_0

    .line 300
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public finishTransition()V
    .locals 3

    .prologue
    .line 538
    sget-boolean v0, Lorg/chromium/chrome/browser/infobar/InfoBarContainer;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lorg/chromium/chrome/browser/infobar/InfoBarContainer;->mAnimation:Lorg/chromium/chrome/browser/infobar/AnimationHelper;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 541
    :cond_0
    iget-object v0, p0, Lorg/chromium/chrome/browser/infobar/InfoBarContainer;->mAnimation:Lorg/chromium/chrome/browser/infobar/AnimationHelper;

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/infobar/AnimationHelper;->getAnimationType()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    .line 542
    iget-object v0, p0, Lorg/chromium/chrome/browser/infobar/InfoBarContainer;->mLinearLayout:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lorg/chromium/chrome/browser/infobar/InfoBarContainer;->mAnimation:Lorg/chromium/chrome/browser/infobar/AnimationHelper;

    invoke-virtual {v1}, Lorg/chromium/chrome/browser/infobar/AnimationHelper;->getTarget()Lorg/chromium/chrome/browser/infobar/ContentWrapperView;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->removeView(Landroid/view/View;)V

    .line 546
    :cond_1
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lorg/chromium/chrome/browser/infobar/InfoBarContainer;->mLinearLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v1

    if-ge v0, v1, :cond_2

    .line 547
    iget-object v1, p0, Lorg/chromium/chrome/browser/infobar/InfoBarContainer;->mLinearLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 548
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/View;->setTranslationY(F)V

    .line 546
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 550
    :cond_2
    invoke-virtual {p0}, Lorg/chromium/chrome/browser/infobar/InfoBarContainer;->requestLayout()V

    .line 554
    iget-object v0, p0, Lorg/chromium/chrome/browser/infobar/InfoBarContainer;->mLinearLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v0

    if-nez v0, :cond_3

    .line 555
    invoke-virtual {p0}, Lorg/chromium/chrome/browser/infobar/InfoBarContainer;->removeFromParentView()V

    .line 558
    :cond_3
    iget-object v0, p0, Lorg/chromium/chrome/browser/infobar/InfoBarContainer;->mAnimationSizer:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 559
    iget-object v0, p0, Lorg/chromium/chrome/browser/infobar/InfoBarContainer;->mAnimationSizer:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iget-object v1, p0, Lorg/chromium/chrome/browser/infobar/InfoBarContainer;->mAnimationSizer:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 563
    :cond_4
    iget-object v0, p0, Lorg/chromium/chrome/browser/infobar/InfoBarContainer;->mAnimationListener:Lorg/chromium/chrome/browser/infobar/InfoBarContainer$InfoBarAnimationListener;

    if-eqz v0, :cond_5

    .line 564
    iget-object v0, p0, Lorg/chromium/chrome/browser/infobar/InfoBarContainer;->mAnimationListener:Lorg/chromium/chrome/browser/infobar/InfoBarContainer$InfoBarAnimationListener;

    iget-object v1, p0, Lorg/chromium/chrome/browser/infobar/InfoBarContainer;->mAnimation:Lorg/chromium/chrome/browser/infobar/AnimationHelper;

    invoke-virtual {v1}, Lorg/chromium/chrome/browser/infobar/AnimationHelper;->getAnimationType()I

    move-result v1

    invoke-interface {v0, v1}, Lorg/chromium/chrome/browser/infobar/InfoBarContainer$InfoBarAnimationListener;->notifyAnimationFinished(I)V

    .line 566
    :cond_5
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/chromium/chrome/browser/infobar/InfoBarContainer;->mAnimation:Lorg/chromium/chrome/browser/infobar/AnimationHelper;

    .line 567
    invoke-direct {p0}, Lorg/chromium/chrome/browser/infobar/InfoBarContainer;->processPendingInfoBars()V

    .line 568
    return-void
.end method

.method public getAnimationListener()Lorg/chromium/chrome/browser/infobar/InfoBarContainer$InfoBarAnimationListener;
    .locals 1

    .prologue
    .line 183
    iget-object v0, p0, Lorg/chromium/chrome/browser/infobar/InfoBarContainer;->mAnimationListener:Lorg/chromium/chrome/browser/infobar/InfoBarContainer$InfoBarAnimationListener;

    return-object v0
.end method

.method public getInfoBars()Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 492
    iget-object v0, p0, Lorg/chromium/chrome/browser/infobar/InfoBarContainer;->mInfoBars:Ljava/util/ArrayList;

    return-object v0
.end method

.method getLinearLayout()Landroid/widget/LinearLayout;
    .locals 1

    .prologue
    .line 173
    iget-object v0, p0, Lorg/chromium/chrome/browser/infobar/InfoBarContainer;->mLinearLayout:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method public getNative()J
    .locals 2

    .prologue
    .line 587
    iget-wide v0, p0, Lorg/chromium/chrome/browser/infobar/InfoBarContainer;->mNativeInfoBarContainer:J

    return-wide v0
.end method

.method public hasBeenDestroyed()Z
    .locals 1

    .prologue
    .line 428
    iget-boolean v0, p0, Lorg/chromium/chrome/browser/infobar/InfoBarContainer;->mDestroyed:Z

    return v0
.end method

.method protected onAttachedToWindow()V
    .locals 4

    .prologue
    .line 251
    invoke-super {p0}, Landroid/widget/ScrollView;->onAttachedToWindow()V

    .line 252
    iget-boolean v0, p0, Lorg/chromium/chrome/browser/infobar/InfoBarContainer;->mDoStayInvisible:Z

    if-nez v0, :cond_0

    .line 253
    const-string/jumbo v0, "alpha"

    const/4 v1, 0x2

    new-array v1, v1, [F

    fill-array-data v1, :array_0

    invoke-static {p0, v0, v1}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    const-wide/16 v2, 0xfa

    invoke-virtual {v0, v2, v3}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    .line 255
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lorg/chromium/chrome/browser/infobar/InfoBarContainer;->setVisibility(I)V

    .line 257
    :cond_0
    return-void

    .line 253
    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .prologue
    .line 261
    invoke-super {p0}, Landroid/widget/ScrollView;->onDetachedFromWindow()V

    .line 262
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lorg/chromium/chrome/browser/infobar/InfoBarContainer;->setVisibility(I)V

    .line 263
    return-void
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 189
    invoke-super {p0, p1}, Landroid/widget/ScrollView;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lorg/chromium/chrome/browser/infobar/InfoBarContainer;->mAnimation:Lorg/chromium/chrome/browser/infobar/AnimationHelper;

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onLayout(ZIIII)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 374
    invoke-virtual {p0}, Lorg/chromium/chrome/browser/infobar/InfoBarContainer;->getVisibility()I

    move-result v0

    if-nez v0, :cond_2

    const/4 v0, 0x1

    .line 375
    :goto_0
    iget-object v2, p0, Lorg/chromium/chrome/browser/infobar/InfoBarContainer;->mActivity:Landroid/app/Activity;

    invoke-static {v2, p0}, Lorg/chromium/ui/UiUtils;->isKeyboardShowing(Landroid/content/Context;Landroid/view/View;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 376
    if-eqz v0, :cond_0

    .line 377
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lorg/chromium/chrome/browser/infobar/InfoBarContainer;->setVisibility(I)V

    .line 384
    :cond_0
    :goto_1
    invoke-super/range {p0 .. p5}, Landroid/widget/ScrollView;->onLayout(ZIIII)V

    .line 388
    invoke-virtual {p0}, Lorg/chromium/chrome/browser/infobar/InfoBarContainer;->getHeight()I

    move-result v0

    .line 389
    iget-object v2, p0, Lorg/chromium/chrome/browser/infobar/InfoBarContainer;->mLinearLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->getHeight()I

    move-result v2

    .line 390
    iget v3, p0, Lorg/chromium/chrome/browser/infobar/InfoBarContainer;->mInnerHeight:I

    if-eq v3, v2, :cond_1

    .line 391
    sub-int v3, v2, v0

    iget v4, p0, Lorg/chromium/chrome/browser/infobar/InfoBarContainer;->mDistanceFromBottom:I

    sub-int/2addr v3, v4

    .line 392
    invoke-virtual {p0, v1, v3}, Lorg/chromium/chrome/browser/infobar/InfoBarContainer;->scrollTo(II)V

    .line 394
    :cond_1
    iput v0, p0, Lorg/chromium/chrome/browser/infobar/InfoBarContainer;->mHeight:I

    .line 395
    iput v2, p0, Lorg/chromium/chrome/browser/infobar/InfoBarContainer;->mInnerHeight:I

    .line 396
    iget v0, p0, Lorg/chromium/chrome/browser/infobar/InfoBarContainer;->mInnerHeight:I

    iget v1, p0, Lorg/chromium/chrome/browser/infobar/InfoBarContainer;->mHeight:I

    sub-int/2addr v0, v1

    invoke-virtual {p0}, Lorg/chromium/chrome/browser/infobar/InfoBarContainer;->getScrollY()I

    move-result v1

    sub-int/2addr v0, v1

    iput v0, p0, Lorg/chromium/chrome/browser/infobar/InfoBarContainer;->mDistanceFromBottom:I

    .line 397
    return-void

    :cond_2
    move v0, v1

    .line 374
    goto :goto_0

    .line 380
    :cond_3
    if-nez v0, :cond_0

    iget-boolean v0, p0, Lorg/chromium/chrome/browser/infobar/InfoBarContainer;->mDoStayInvisible:Z

    if-nez v0, :cond_0

    .line 381
    invoke-virtual {p0, v1}, Lorg/chromium/chrome/browser/infobar/InfoBarContainer;->setVisibility(I)V

    goto :goto_1
.end method

.method protected onMeasure(II)V
    .locals 2

    .prologue
    .line 158
    invoke-super {p0, p1, p2}, Landroid/widget/ScrollView;->onMeasure(II)V

    .line 163
    iget-object v0, p0, Lorg/chromium/chrome/browser/infobar/InfoBarContainer;->mLinearLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getMeasuredHeight()I

    move-result v0

    invoke-virtual {p0}, Lorg/chromium/chrome/browser/infobar/InfoBarContainer;->getMeasuredHeight()I

    move-result v1

    if-le v0, v1, :cond_1

    const/4 v0, 0x1

    .line 164
    :goto_0
    invoke-virtual {p0}, Lorg/chromium/chrome/browser/infobar/InfoBarContainer;->isVerticalScrollBarEnabled()Z

    move-result v1

    if-eq v0, v1, :cond_0

    .line 165
    invoke-virtual {p0, v0}, Lorg/chromium/chrome/browser/infobar/InfoBarContainer;->setVerticalScrollBarEnabled(Z)V

    .line 167
    :cond_0
    return-void

    .line 163
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onPageStarted()V
    .locals 4

    .prologue
    .line 458
    new-instance v1, Ljava/util/LinkedList;

    invoke-direct {v1}, Ljava/util/LinkedList;-><init>()V

    .line 460
    iget-object v0, p0, Lorg/chromium/chrome/browser/infobar/InfoBarContainer;->mInfoBars:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/chromium/chrome/browser/infobar/InfoBar;

    .line 461
    invoke-virtual {v0}, Lorg/chromium/chrome/browser/infobar/InfoBar;->shouldExpire()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 462
    invoke-virtual {v1, v0}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 466
    :cond_1
    invoke-virtual {v1}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/chromium/chrome/browser/infobar/InfoBar;

    .line 467
    invoke-virtual {v0}, Lorg/chromium/chrome/browser/infobar/InfoBar;->dismissJavaOnlyInfoBar()V

    goto :goto_1

    .line 469
    :cond_2
    return-void
.end method

.method public onParentViewChanged(ILandroid/view/ViewGroup;)V
    .locals 0

    .prologue
    .line 209
    iput p1, p0, Lorg/chromium/chrome/browser/infobar/InfoBarContainer;->mTabId:I

    .line 210
    iput-object p2, p0, Lorg/chromium/chrome/browser/infobar/InfoBarContainer;->mParentView:Landroid/view/ViewGroup;

    .line 212
    invoke-virtual {p0}, Lorg/chromium/chrome/browser/infobar/InfoBarContainer;->removeFromParentView()V

    .line 213
    invoke-direct {p0}, Lorg/chromium/chrome/browser/infobar/InfoBarContainer;->addToParentView()V

    .line 214
    return-void
.end method

.method protected onScrollChanged(IIII)V
    .locals 2

    .prologue
    .line 401
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/ScrollView;->onScrollChanged(IIII)V

    .line 402
    iget v0, p0, Lorg/chromium/chrome/browser/infobar/InfoBarContainer;->mInnerHeight:I

    iget v1, p0, Lorg/chromium/chrome/browser/infobar/InfoBarContainer;->mHeight:I

    sub-int/2addr v0, v1

    invoke-virtual {p0}, Lorg/chromium/chrome/browser/infobar/InfoBarContainer;->getScrollY()I

    move-result v1

    sub-int/2addr v0, v1

    iput v0, p0, Lorg/chromium/chrome/browser/infobar/InfoBarContainer;->mDistanceFromBottom:I

    .line 403
    return-void
.end method

.method public prepareTransition(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v4, -0x1

    const/4 v3, -0x2

    .line 518
    if-eqz p1, :cond_2

    .line 522
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 523
    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 525
    :cond_0
    sget-boolean v0, Lorg/chromium/chrome/browser/infobar/InfoBarContainer;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lorg/chromium/chrome/browser/infobar/InfoBarContainer;->mAnimationSizer:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 526
    :cond_1
    iget-object v0, p0, Lorg/chromium/chrome/browser/infobar/InfoBarContainer;->mParentView:Landroid/view/ViewGroup;

    iget-object v1, p0, Lorg/chromium/chrome/browser/infobar/InfoBarContainer;->mAnimationSizer:Landroid/widget/FrameLayout;

    new-instance v2, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v2, v4, v3}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 528
    iget-object v0, p0, Lorg/chromium/chrome/browser/infobar/InfoBarContainer;->mAnimationSizer:Landroid/widget/FrameLayout;

    const/4 v1, 0x0

    new-instance v2, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v2, v4, v3}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, p1, v1, v2}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    .line 530
    iget-object v0, p0, Lorg/chromium/chrome/browser/infobar/InfoBarContainer;->mAnimationSizer:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->requestLayout()V

    .line 532
    :cond_2
    return-void
.end method

.method public processAutoLogin(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)V
    .locals 1

    .prologue
    .line 507
    iget-object v0, p0, Lorg/chromium/chrome/browser/infobar/InfoBarContainer;->mAutoLoginDelegate:Lorg/chromium/chrome/browser/infobar/AutoLoginDelegate;

    invoke-virtual {v0, p1, p2, p3, p4}, Lorg/chromium/chrome/browser/infobar/AutoLoginDelegate;->dismissAutoLogins(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)V

    .line 508
    return-void
.end method

.method public removeFromParentView()V
    .locals 1

    .prologue
    .line 199
    invoke-virtual {p0}, Lorg/chromium/chrome/browser/infobar/InfoBarContainer;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 200
    invoke-virtual {p0}, Lorg/chromium/chrome/browser/infobar/InfoBarContainer;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v0, p0}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 202
    :cond_0
    return-void
.end method

.method public removeInfoBar(Lorg/chromium/chrome/browser/infobar/InfoBar;)V
    .locals 4

    .prologue
    .line 331
    sget-boolean v0, Lorg/chromium/chrome/browser/infobar/InfoBarContainer;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lorg/chromium/chrome/browser/infobar/InfoBarContainer;->mDestroyed:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 333
    :cond_0
    iget-object v0, p0, Lorg/chromium/chrome/browser/infobar/InfoBarContainer;->mInfoBars:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 334
    sget-boolean v0, Lorg/chromium/chrome/browser/infobar/InfoBarContainer;->$assertionsDisabled:Z

    if-nez v0, :cond_6

    new-instance v0, Ljava/lang/AssertionError;

    const-string/jumbo v1, "Trying to remove an InfoBar that is not in this container."

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 340
    :cond_1
    const/4 v0, 0x0

    .line 341
    new-instance v1, Ljava/util/ArrayDeque;

    iget-object v2, p0, Lorg/chromium/chrome/browser/infobar/InfoBarContainer;->mInfoBarTransitions:Ljava/util/ArrayDeque;

    invoke-direct {v1, v2}, Ljava/util/ArrayDeque;-><init>(Ljava/util/Collection;)V

    .line 343
    invoke-virtual {v1}, Ljava/util/ArrayDeque;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/chromium/chrome/browser/infobar/InfoBarContainer$InfoBarTransitionInfo;

    .line 344
    iget-object v3, v0, Lorg/chromium/chrome/browser/infobar/InfoBarContainer$InfoBarTransitionInfo;->target:Lorg/chromium/chrome/browser/infobar/InfoBar;

    if-ne v3, p1, :cond_4

    .line 345
    iget v3, v0, Lorg/chromium/chrome/browser/infobar/InfoBarContainer$InfoBarTransitionInfo;->animationType:I

    if-nez v3, :cond_3

    .line 348
    sget-boolean v3, Lorg/chromium/chrome/browser/infobar/InfoBarContainer;->$assertionsDisabled:Z

    if-nez v3, :cond_2

    if-eqz v1, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 349
    :cond_2
    const/4 v1, 0x1

    .line 351
    :cond_3
    if-eqz v1, :cond_4

    .line 352
    iget-object v3, p0, Lorg/chromium/chrome/browser/infobar/InfoBarContainer;->mInfoBarTransitions:Ljava/util/ArrayDeque;

    invoke-virtual {v3, v0}, Ljava/util/ArrayDeque;->remove(Ljava/lang/Object;)Z

    :cond_4
    move v0, v1

    move v1, v0

    .line 355
    goto :goto_0

    .line 357
    :cond_5
    if-nez v1, :cond_6

    .line 358
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-direct {p0, p1, v0, v1}, Lorg/chromium/chrome/browser/infobar/InfoBarContainer;->enqueueInfoBarAnimation(Lorg/chromium/chrome/browser/infobar/InfoBar;Landroid/view/View;I)V

    .line 360
    :cond_6
    return-void
.end method

.method public setAnimationListener(Lorg/chromium/chrome/browser/infobar/InfoBarContainer$InfoBarAnimationListener;)V
    .locals 0

    .prologue
    .line 178
    iput-object p1, p0, Lorg/chromium/chrome/browser/infobar/InfoBarContainer;->mAnimationListener:Lorg/chromium/chrome/browser/infobar/InfoBarContainer$InfoBarAnimationListener;

    .line 179
    return-void
.end method

.method public setDoStayInvisible(ZLorg/chromium/chrome/browser/Tab;)V
    .locals 1

    .prologue
    .line 224
    iput-boolean p1, p0, Lorg/chromium/chrome/browser/infobar/InfoBarContainer;->mDoStayInvisible:Z

    .line 225
    iget-object v0, p0, Lorg/chromium/chrome/browser/infobar/InfoBarContainer;->mTabObserver:Lorg/chromium/chrome/browser/TabObserver;

    if-nez v0, :cond_0

    invoke-direct {p0}, Lorg/chromium/chrome/browser/infobar/InfoBarContainer;->createTabObserver()Lorg/chromium/chrome/browser/TabObserver;

    move-result-object v0

    iput-object v0, p0, Lorg/chromium/chrome/browser/infobar/InfoBarContainer;->mTabObserver:Lorg/chromium/chrome/browser/TabObserver;

    .line 226
    :cond_0
    if-eqz p1, :cond_1

    .line 227
    iget-object v0, p0, Lorg/chromium/chrome/browser/infobar/InfoBarContainer;->mTabObserver:Lorg/chromium/chrome/browser/TabObserver;

    invoke-virtual {p2, v0}, Lorg/chromium/chrome/browser/Tab;->addObserver(Lorg/chromium/chrome/browser/TabObserver;)V

    .line 231
    :goto_0
    return-void

    .line 229
    :cond_1
    iget-object v0, p0, Lorg/chromium/chrome/browser/infobar/InfoBarContainer;->mTabObserver:Lorg/chromium/chrome/browser/TabObserver;

    invoke-virtual {p2, v0}, Lorg/chromium/chrome/browser/Tab;->removeObserver(Lorg/chromium/chrome/browser/TabObserver;)V

    goto :goto_0
.end method

.method public swapInfoBarViews(Lorg/chromium/chrome/browser/infobar/InfoBar;Landroid/view/View;)V
    .locals 2

    .prologue
    .line 310
    sget-boolean v0, Lorg/chromium/chrome/browser/infobar/InfoBarContainer;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lorg/chromium/chrome/browser/infobar/InfoBarContainer;->mDestroyed:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 312
    :cond_0
    iget-object v0, p0, Lorg/chromium/chrome/browser/infobar/InfoBarContainer;->mInfoBars:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 313
    sget-boolean v0, Lorg/chromium/chrome/browser/infobar/InfoBarContainer;->$assertionsDisabled:Z

    if-nez v0, :cond_3

    new-instance v0, Ljava/lang/AssertionError;

    const-string/jumbo v1, "Trying to swap an InfoBar that is not in this container."

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 317
    :cond_1
    invoke-virtual {p0, p1}, Lorg/chromium/chrome/browser/infobar/InfoBarContainer;->findLastTransitionForInfoBar(Lorg/chromium/chrome/browser/infobar/InfoBar;)Lorg/chromium/chrome/browser/infobar/InfoBarContainer$InfoBarTransitionInfo;

    move-result-object v0

    .line 318
    if-eqz v0, :cond_2

    iget-object v0, v0, Lorg/chromium/chrome/browser/infobar/InfoBarContainer$InfoBarTransitionInfo;->toShow:Landroid/view/View;

    if-ne v0, p2, :cond_2

    .line 319
    sget-boolean v0, Lorg/chromium/chrome/browser/infobar/InfoBarContainer;->$assertionsDisabled:Z

    if-nez v0, :cond_3

    new-instance v0, Ljava/lang/AssertionError;

    const-string/jumbo v1, "Tried to enqueue the same swap twice in a row."

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 323
    :cond_2
    const/4 v0, 0x1

    invoke-direct {p0, p1, p2, v0}, Lorg/chromium/chrome/browser/infobar/InfoBarContainer;->enqueueInfoBarAnimation(Lorg/chromium/chrome/browser/infobar/InfoBar;Landroid/view/View;I)V

    .line 324
    :cond_3
    return-void
.end method
