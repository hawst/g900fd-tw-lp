.class Lorg/chromium/chrome/browser/dom_distiller/DomDistillerFeedbackReporter$1;
.super Lorg/chromium/chrome/browser/EmptyTabObserver;
.source "DomDistillerFeedbackReporter.java"


# instance fields
.field final synthetic this$0:Lorg/chromium/chrome/browser/dom_distiller/DomDistillerFeedbackReporter;


# direct methods
.method constructor <init>(Lorg/chromium/chrome/browser/dom_distiller/DomDistillerFeedbackReporter;)V
    .locals 0

    .prologue
    .line 127
    iput-object p1, p0, Lorg/chromium/chrome/browser/dom_distiller/DomDistillerFeedbackReporter$1;->this$0:Lorg/chromium/chrome/browser/dom_distiller/DomDistillerFeedbackReporter;

    invoke-direct {p0}, Lorg/chromium/chrome/browser/EmptyTabObserver;-><init>()V

    return-void
.end method


# virtual methods
.method public onContentChanged(Lorg/chromium/chrome/browser/Tab;)V
    .locals 1

    .prologue
    .line 136
    iget-object v0, p0, Lorg/chromium/chrome/browser/dom_distiller/DomDistillerFeedbackReporter$1;->this$0:Lorg/chromium/chrome/browser/dom_distiller/DomDistillerFeedbackReporter;

    # invokes: Lorg/chromium/chrome/browser/dom_distiller/DomDistillerFeedbackReporter;->updatePointers()V
    invoke-static {v0}, Lorg/chromium/chrome/browser/dom_distiller/DomDistillerFeedbackReporter;->access$100(Lorg/chromium/chrome/browser/dom_distiller/DomDistillerFeedbackReporter;)V

    .line 137
    return-void
.end method

.method public onDestroyed(Lorg/chromium/chrome/browser/Tab;)V
    .locals 4

    .prologue
    .line 141
    iget-object v0, p0, Lorg/chromium/chrome/browser/dom_distiller/DomDistillerFeedbackReporter$1;->this$0:Lorg/chromium/chrome/browser/dom_distiller/DomDistillerFeedbackReporter;

    iget-object v1, p0, Lorg/chromium/chrome/browser/dom_distiller/DomDistillerFeedbackReporter$1;->this$0:Lorg/chromium/chrome/browser/dom_distiller/DomDistillerFeedbackReporter;

    # getter for: Lorg/chromium/chrome/browser/dom_distiller/DomDistillerFeedbackReporter;->mNativePointer:J
    invoke-static {v1}, Lorg/chromium/chrome/browser/dom_distiller/DomDistillerFeedbackReporter;->access$200(Lorg/chromium/chrome/browser/dom_distiller/DomDistillerFeedbackReporter;)J

    move-result-wide v2

    # invokes: Lorg/chromium/chrome/browser/dom_distiller/DomDistillerFeedbackReporter;->nativeDestroy(J)V
    invoke-static {v0, v2, v3}, Lorg/chromium/chrome/browser/dom_distiller/DomDistillerFeedbackReporter;->access$300(Lorg/chromium/chrome/browser/dom_distiller/DomDistillerFeedbackReporter;J)V

    .line 142
    iget-object v0, p0, Lorg/chromium/chrome/browser/dom_distiller/DomDistillerFeedbackReporter$1;->this$0:Lorg/chromium/chrome/browser/dom_distiller/DomDistillerFeedbackReporter;

    const/4 v1, 0x0

    # setter for: Lorg/chromium/chrome/browser/dom_distiller/DomDistillerFeedbackReporter;->mContentViewCore:Lorg/chromium/content/browser/ContentViewCore;
    invoke-static {v0, v1}, Lorg/chromium/chrome/browser/dom_distiller/DomDistillerFeedbackReporter;->access$402(Lorg/chromium/chrome/browser/dom_distiller/DomDistillerFeedbackReporter;Lorg/chromium/content/browser/ContentViewCore;)Lorg/chromium/content/browser/ContentViewCore;

    .line 143
    return-void
.end method

.method public onWebContentsSwapped(Lorg/chromium/chrome/browser/Tab;ZZ)V
    .locals 1

    .prologue
    .line 131
    iget-object v0, p0, Lorg/chromium/chrome/browser/dom_distiller/DomDistillerFeedbackReporter$1;->this$0:Lorg/chromium/chrome/browser/dom_distiller/DomDistillerFeedbackReporter;

    # invokes: Lorg/chromium/chrome/browser/dom_distiller/DomDistillerFeedbackReporter;->updatePointers()V
    invoke-static {v0}, Lorg/chromium/chrome/browser/dom_distiller/DomDistillerFeedbackReporter;->access$100(Lorg/chromium/chrome/browser/dom_distiller/DomDistillerFeedbackReporter;)V

    .line 132
    return-void
.end method
