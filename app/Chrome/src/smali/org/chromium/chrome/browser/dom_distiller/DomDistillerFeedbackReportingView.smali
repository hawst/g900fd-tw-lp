.class public Lorg/chromium/chrome/browser/dom_distiller/DomDistillerFeedbackReportingView;
.super Lorg/chromium/chrome/browser/banners/SwipableOverlayView;
.source "DomDistillerFeedbackReportingView.java"


# static fields
.field private static final VIEW_LAYOUT:I


# instance fields
.field private mFeedbackObserver:Lorg/chromium/chrome/browser/dom_distiller/DomDistillerFeedbackReportingView$FeedbackObserver;

.field private mNoButton:Landroid/widget/ImageButton;

.field private mSelectionMade:Z

.field private mYesButton:Landroid/widget/ImageButton;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 28
    sget v0, Lorg/chromium/chrome/R$layout;->dom_distiller_feedback_reporting_view:I

    sput v0, Lorg/chromium/chrome/browser/dom_distiller/DomDistillerFeedbackReportingView;->VIEW_LAYOUT:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 77
    invoke-direct {p0, p1, p2}, Lorg/chromium/chrome/browser/banners/SwipableOverlayView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 78
    return-void
.end method

.method static synthetic access$000(Lorg/chromium/chrome/browser/dom_distiller/DomDistillerFeedbackReportingView;)Z
    .locals 1

    .prologue
    .line 26
    iget-boolean v0, p0, Lorg/chromium/chrome/browser/dom_distiller/DomDistillerFeedbackReportingView;->mSelectionMade:Z

    return v0
.end method

.method static synthetic access$002(Lorg/chromium/chrome/browser/dom_distiller/DomDistillerFeedbackReportingView;Z)Z
    .locals 0

    .prologue
    .line 26
    iput-boolean p1, p0, Lorg/chromium/chrome/browser/dom_distiller/DomDistillerFeedbackReportingView;->mSelectionMade:Z

    return p1
.end method

.method static synthetic access$100(Lorg/chromium/chrome/browser/dom_distiller/DomDistillerFeedbackReportingView;)Landroid/widget/ImageButton;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lorg/chromium/chrome/browser/dom_distiller/DomDistillerFeedbackReportingView;->mNoButton:Landroid/widget/ImageButton;

    return-object v0
.end method

.method static synthetic access$200(Lorg/chromium/chrome/browser/dom_distiller/DomDistillerFeedbackReportingView;)V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Lorg/chromium/chrome/browser/dom_distiller/DomDistillerFeedbackReportingView;->disableUI()V

    return-void
.end method

.method static synthetic access$300(Lorg/chromium/chrome/browser/dom_distiller/DomDistillerFeedbackReportingView;)Lorg/chromium/chrome/browser/dom_distiller/DomDistillerFeedbackReportingView$FeedbackObserver;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lorg/chromium/chrome/browser/dom_distiller/DomDistillerFeedbackReportingView;->mFeedbackObserver:Lorg/chromium/chrome/browser/dom_distiller/DomDistillerFeedbackReportingView$FeedbackObserver;

    return-object v0
.end method

.method static synthetic access$400(Lorg/chromium/chrome/browser/dom_distiller/DomDistillerFeedbackReportingView;)Landroid/widget/ImageButton;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lorg/chromium/chrome/browser/dom_distiller/DomDistillerFeedbackReportingView;->mYesButton:Landroid/widget/ImageButton;

    return-object v0
.end method

.method public static create(Lorg/chromium/content/browser/ContentViewCore;Lorg/chromium/chrome/browser/dom_distiller/DomDistillerFeedbackReportingView$FeedbackObserver;)Lorg/chromium/chrome/browser/dom_distiller/DomDistillerFeedbackReportingView;
    .locals 3

    .prologue
    .line 61
    invoke-virtual {p0}, Lorg/chromium/content/browser/ContentViewCore;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 62
    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Lorg/chromium/chrome/browser/dom_distiller/DomDistillerFeedbackReportingView;->VIEW_LAYOUT:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lorg/chromium/chrome/browser/dom_distiller/DomDistillerFeedbackReportingView;

    .line 65
    invoke-direct {v0, p1}, Lorg/chromium/chrome/browser/dom_distiller/DomDistillerFeedbackReportingView;->initialize(Lorg/chromium/chrome/browser/dom_distiller/DomDistillerFeedbackReportingView$FeedbackObserver;)V

    .line 66
    invoke-virtual {v0, p0}, Lorg/chromium/chrome/browser/dom_distiller/DomDistillerFeedbackReportingView;->addToView(Lorg/chromium/content/browser/ContentViewCore;)V

    .line 67
    return-object v0
.end method

.method private disableUI()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 114
    iget-object v0, p0, Lorg/chromium/chrome/browser/dom_distiller/DomDistillerFeedbackReportingView;->mNoButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 115
    iget-object v0, p0, Lorg/chromium/chrome/browser/dom_distiller/DomDistillerFeedbackReportingView;->mYesButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 119
    iget-object v0, p0, Lorg/chromium/chrome/browser/dom_distiller/DomDistillerFeedbackReportingView;->mNoButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 120
    iget-object v0, p0, Lorg/chromium/chrome/browser/dom_distiller/DomDistillerFeedbackReportingView;->mYesButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 121
    return-void
.end method

.method private initialize(Lorg/chromium/chrome/browser/dom_distiller/DomDistillerFeedbackReportingView$FeedbackObserver;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 81
    iput-object p1, p0, Lorg/chromium/chrome/browser/dom_distiller/DomDistillerFeedbackReportingView;->mFeedbackObserver:Lorg/chromium/chrome/browser/dom_distiller/DomDistillerFeedbackReportingView$FeedbackObserver;

    .line 82
    sget v0, Lorg/chromium/chrome/R$id;->distillation_quality_answer_no:I

    invoke-virtual {p0, v0}, Lorg/chromium/chrome/browser/dom_distiller/DomDistillerFeedbackReportingView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lorg/chromium/chrome/browser/dom_distiller/DomDistillerFeedbackReportingView;->mNoButton:Landroid/widget/ImageButton;

    .line 83
    sget v0, Lorg/chromium/chrome/R$id;->distillation_quality_answer_yes:I

    invoke-virtual {p0, v0}, Lorg/chromium/chrome/browser/dom_distiller/DomDistillerFeedbackReportingView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lorg/chromium/chrome/browser/dom_distiller/DomDistillerFeedbackReportingView;->mYesButton:Landroid/widget/ImageButton;

    .line 84
    iget-object v0, p0, Lorg/chromium/chrome/browser/dom_distiller/DomDistillerFeedbackReportingView;->mNoButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setClickable(Z)V

    .line 85
    iget-object v0, p0, Lorg/chromium/chrome/browser/dom_distiller/DomDistillerFeedbackReportingView;->mYesButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setClickable(Z)V

    .line 86
    iget-object v0, p0, Lorg/chromium/chrome/browser/dom_distiller/DomDistillerFeedbackReportingView;->mNoButton:Landroid/widget/ImageButton;

    new-instance v1, Lorg/chromium/chrome/browser/dom_distiller/DomDistillerFeedbackReportingView$1;

    invoke-direct {v1, p0}, Lorg/chromium/chrome/browser/dom_distiller/DomDistillerFeedbackReportingView$1;-><init>(Lorg/chromium/chrome/browser/dom_distiller/DomDistillerFeedbackReportingView;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 98
    iget-object v0, p0, Lorg/chromium/chrome/browser/dom_distiller/DomDistillerFeedbackReportingView;->mYesButton:Landroid/widget/ImageButton;

    new-instance v1, Lorg/chromium/chrome/browser/dom_distiller/DomDistillerFeedbackReportingView$2;

    invoke-direct {v1, p0}, Lorg/chromium/chrome/browser/dom_distiller/DomDistillerFeedbackReportingView$2;-><init>(Lorg/chromium/chrome/browser/dom_distiller/DomDistillerFeedbackReportingView;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 110
    return-void
.end method


# virtual methods
.method protected dismiss(Z)Z
    .locals 1

    .prologue
    .line 131
    invoke-super {p0, p1}, Lorg/chromium/chrome/browser/banners/SwipableOverlayView;->dismiss(Z)Z

    move-result v0

    return v0
.end method

.method protected onViewClicked()V
    .locals 0

    .prologue
    .line 136
    return-void
.end method

.method protected onViewPressed(Landroid/view/MotionEvent;)V
    .locals 0

    .prologue
    .line 140
    return-void
.end method

.method protected onViewSwipedAway()V
    .locals 0

    .prologue
    .line 144
    return-void
.end method
