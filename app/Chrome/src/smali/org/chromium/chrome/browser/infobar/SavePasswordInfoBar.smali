.class public Lorg/chromium/chrome/browser/infobar/SavePasswordInfoBar;
.super Lorg/chromium/chrome/browser/infobar/ConfirmInfoBar;
.source "SavePasswordInfoBar.java"


# instance fields
.field private final mDelegate:Lorg/chromium/chrome/browser/infobar/SavePasswordInfoBarDelegate;

.field private final mNativeInfoBar:J

.field private mUseAdditionalAuthenticationCheckbox:Landroid/widget/CheckBox;


# direct methods
.method public constructor <init>(JLorg/chromium/chrome/browser/infobar/SavePasswordInfoBarDelegate;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 11

    .prologue
    .line 23
    const/4 v4, 0x0

    const/4 v7, 0x0

    move-object v1, p0

    move-wide v2, p1

    move v5, p4

    move-object/from16 v6, p5

    move-object/from16 v8, p6

    move-object/from16 v9, p7

    invoke-direct/range {v1 .. v9}, Lorg/chromium/chrome/browser/infobar/ConfirmInfoBar;-><init>(JLorg/chromium/chrome/browser/infobar/InfoBarListeners$Confirm;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 25
    iput-wide p1, p0, Lorg/chromium/chrome/browser/infobar/SavePasswordInfoBar;->mNativeInfoBar:J

    .line 26
    iput-object p3, p0, Lorg/chromium/chrome/browser/infobar/SavePasswordInfoBar;->mDelegate:Lorg/chromium/chrome/browser/infobar/SavePasswordInfoBarDelegate;

    .line 27
    return-void
.end method


# virtual methods
.method public createContent(Lorg/chromium/chrome/browser/infobar/InfoBarLayout;)V
    .locals 2

    .prologue
    .line 31
    invoke-static {}, Lorg/chromium/chrome/browser/password_manager/PasswordAuthenticationManager;->isPasswordAuthenticationEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 32
    new-instance v0, Landroid/widget/CheckBox;

    invoke-virtual {p0}, Lorg/chromium/chrome/browser/infobar/SavePasswordInfoBar;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/CheckBox;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lorg/chromium/chrome/browser/infobar/SavePasswordInfoBar;->mUseAdditionalAuthenticationCheckbox:Landroid/widget/CheckBox;

    .line 33
    iget-object v0, p0, Lorg/chromium/chrome/browser/infobar/SavePasswordInfoBar;->mUseAdditionalAuthenticationCheckbox:Landroid/widget/CheckBox;

    invoke-static {}, Lorg/chromium/chrome/browser/password_manager/PasswordAuthenticationManager;->getPasswordProtectionString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setText(Ljava/lang/CharSequence;)V

    .line 35
    iget-object v0, p0, Lorg/chromium/chrome/browser/infobar/SavePasswordInfoBar;->mUseAdditionalAuthenticationCheckbox:Landroid/widget/CheckBox;

    invoke-virtual {p1, v0}, Lorg/chromium/chrome/browser/infobar/InfoBarLayout;->setCustomContent(Landroid/view/View;)V

    .line 38
    :cond_0
    invoke-super {p0, p1}, Lorg/chromium/chrome/browser/infobar/ConfirmInfoBar;->createContent(Lorg/chromium/chrome/browser/infobar/InfoBarLayout;)V

    .line 39
    return-void
.end method

.method public onButtonClicked(Z)V
    .locals 4

    .prologue
    .line 43
    if-eqz p1, :cond_0

    iget-object v0, p0, Lorg/chromium/chrome/browser/infobar/SavePasswordInfoBar;->mUseAdditionalAuthenticationCheckbox:Landroid/widget/CheckBox;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/chromium/chrome/browser/infobar/SavePasswordInfoBar;->mUseAdditionalAuthenticationCheckbox:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 45
    iget-object v0, p0, Lorg/chromium/chrome/browser/infobar/SavePasswordInfoBar;->mDelegate:Lorg/chromium/chrome/browser/infobar/SavePasswordInfoBarDelegate;

    iget-wide v2, p0, Lorg/chromium/chrome/browser/infobar/SavePasswordInfoBar;->mNativeInfoBar:J

    const/4 v1, 0x1

    invoke-virtual {v0, v2, v3, v1}, Lorg/chromium/chrome/browser/infobar/SavePasswordInfoBarDelegate;->setUseAdditionalAuthentication(JZ)V

    .line 47
    :cond_0
    invoke-super {p0, p1}, Lorg/chromium/chrome/browser/infobar/ConfirmInfoBar;->onButtonClicked(Z)V

    .line 48
    return-void
.end method
