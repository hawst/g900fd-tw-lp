.class public Lorg/chromium/chrome/browser/contextmenu/ContextMenuPopulatorWrapper;
.super Ljava/lang/Object;
.source "ContextMenuPopulatorWrapper.java"

# interfaces
.implements Lorg/chromium/chrome/browser/contextmenu/ContextMenuPopulator;


# instance fields
.field private final mPopulator:Lorg/chromium/chrome/browser/contextmenu/ContextMenuPopulator;


# direct methods
.method public constructor <init>(Lorg/chromium/chrome/browser/contextmenu/ContextMenuPopulator;)V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-object p1, p0, Lorg/chromium/chrome/browser/contextmenu/ContextMenuPopulatorWrapper;->mPopulator:Lorg/chromium/chrome/browser/contextmenu/ContextMenuPopulator;

    .line 24
    return-void
.end method


# virtual methods
.method public buildContextMenu(Landroid/view/ContextMenu;Landroid/content/Context;Lorg/chromium/chrome/browser/contextmenu/ContextMenuParams;)V
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lorg/chromium/chrome/browser/contextmenu/ContextMenuPopulatorWrapper;->mPopulator:Lorg/chromium/chrome/browser/contextmenu/ContextMenuPopulator;

    invoke-interface {v0, p1, p2, p3}, Lorg/chromium/chrome/browser/contextmenu/ContextMenuPopulator;->buildContextMenu(Landroid/view/ContextMenu;Landroid/content/Context;Lorg/chromium/chrome/browser/contextmenu/ContextMenuParams;)V

    .line 34
    return-void
.end method

.method public onItemSelected(Lorg/chromium/chrome/browser/contextmenu/ContextMenuHelper;Lorg/chromium/chrome/browser/contextmenu/ContextMenuParams;I)Z
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lorg/chromium/chrome/browser/contextmenu/ContextMenuPopulatorWrapper;->mPopulator:Lorg/chromium/chrome/browser/contextmenu/ContextMenuPopulator;

    invoke-interface {v0, p1, p2, p3}, Lorg/chromium/chrome/browser/contextmenu/ContextMenuPopulator;->onItemSelected(Lorg/chromium/chrome/browser/contextmenu/ContextMenuHelper;Lorg/chromium/chrome/browser/contextmenu/ContextMenuParams;I)Z

    move-result v0

    return v0
.end method

.method public shouldShowContextMenu(Lorg/chromium/chrome/browser/contextmenu/ContextMenuParams;)Z
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lorg/chromium/chrome/browser/contextmenu/ContextMenuPopulatorWrapper;->mPopulator:Lorg/chromium/chrome/browser/contextmenu/ContextMenuPopulator;

    invoke-interface {v0, p1}, Lorg/chromium/chrome/browser/contextmenu/ContextMenuPopulator;->shouldShowContextMenu(Lorg/chromium/chrome/browser/contextmenu/ContextMenuParams;)Z

    move-result v0

    return v0
.end method
