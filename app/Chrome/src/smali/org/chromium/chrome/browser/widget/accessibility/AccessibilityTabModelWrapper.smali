.class public Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelWrapper;
.super Landroid/widget/LinearLayout;
.source "AccessibilityTabModelWrapper.java"


# instance fields
.field private mAccessibilityView:Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListView;

.field private mIncognitoButton:Landroid/widget/ImageButton;

.field private mIsAttachedToWindow:Z

.field private mStackButtonWrapper:Landroid/widget/LinearLayout;

.field private mStandardButton:Landroid/widget/ImageButton;

.field private mTabModelChangeListener:Lorg/chromium/chrome/browser/tabmodel/TabModelSelector$ChangeListener;

.field private mTabModelSelector:Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 69
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 32
    new-instance v0, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelWrapper$1;

    invoke-direct {v0, p0}, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelWrapper$1;-><init>(Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelWrapper;)V

    iput-object v0, p0, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelWrapper;->mTabModelChangeListener:Lorg/chromium/chrome/browser/tabmodel/TabModelSelector$ChangeListener;

    .line 70
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 73
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 32
    new-instance v0, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelWrapper$1;

    invoke-direct {v0, p0}, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelWrapper$1;-><init>(Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelWrapper;)V

    iput-object v0, p0, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelWrapper;->mTabModelChangeListener:Lorg/chromium/chrome/browser/tabmodel/TabModelSelector$ChangeListener;

    .line 74
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 78
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 32
    new-instance v0, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelWrapper$1;

    invoke-direct {v0, p0}, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelWrapper$1;-><init>(Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelWrapper;)V

    iput-object v0, p0, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelWrapper;->mTabModelChangeListener:Lorg/chromium/chrome/browser/tabmodel/TabModelSelector$ChangeListener;

    .line 79
    return-void
.end method

.method static synthetic access$000(Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelWrapper;)Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelAdapter;
    .locals 1

    .prologue
    .line 25
    invoke-direct {p0}, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelWrapper;->getAdapter()Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelAdapter;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$100(Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelWrapper;)Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelWrapper;->mTabModelSelector:Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

    return-object v0
.end method

.method private getAdapter()Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelAdapter;
    .locals 1

    .prologue
    .line 150
    iget-object v0, p0, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelWrapper;->mAccessibilityView:Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListView;

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelAdapter;

    return-object v0
.end method


# virtual methods
.method protected onAttachedToWindow()V
    .locals 2

    .prologue
    .line 155
    iget-object v0, p0, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelWrapper;->mTabModelSelector:Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

    iget-object v1, p0, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelWrapper;->mTabModelChangeListener:Lorg/chromium/chrome/browser/tabmodel/TabModelSelector$ChangeListener;

    invoke-interface {v0, v1}, Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;->registerChangeListener(Lorg/chromium/chrome/browser/tabmodel/TabModelSelector$ChangeListener;)V

    .line 156
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelWrapper;->mIsAttachedToWindow:Z

    .line 157
    invoke-super {p0}, Landroid/widget/LinearLayout;->onAttachedToWindow()V

    .line 158
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 2

    .prologue
    .line 162
    iget-object v0, p0, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelWrapper;->mTabModelSelector:Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

    iget-object v1, p0, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelWrapper;->mTabModelChangeListener:Lorg/chromium/chrome/browser/tabmodel/TabModelSelector$ChangeListener;

    invoke-interface {v0, v1}, Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;->unregisterChangeListener(Lorg/chromium/chrome/browser/tabmodel/TabModelSelector$ChangeListener;)V

    .line 163
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelWrapper;->mIsAttachedToWindow:Z

    .line 164
    invoke-super {p0}, Landroid/widget/LinearLayout;->onDetachedFromWindow()V

    .line 165
    return-void
.end method

.method public setStateBasedOnModel()V
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 123
    iget-object v2, p0, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelWrapper;->mTabModelSelector:Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

    if-nez v2, :cond_0

    .line 147
    :goto_0
    return-void

    .line 125
    :cond_0
    iget-object v2, p0, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelWrapper;->mTabModelSelector:Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

    invoke-interface {v2, v0}, Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;->getModel(Z)Lorg/chromium/chrome/browser/tabmodel/TabModel;

    move-result-object v2

    invoke-interface {v2}, Lorg/chromium/chrome/browser/tabmodel/TabModel;->getComprehensiveModel()Lorg/chromium/chrome/browser/tabmodel/TabList;

    move-result-object v2

    invoke-interface {v2}, Lorg/chromium/chrome/browser/tabmodel/TabList;->getCount()I

    move-result v2

    if-lez v2, :cond_1

    .line 128
    :goto_1
    iget-object v2, p0, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelWrapper;->mTabModelSelector:Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

    invoke-interface {v2}, Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;->isIncognitoSelected()Z

    move-result v2

    .line 130
    if-eqz v0, :cond_2

    .line 131
    iget-object v0, p0, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelWrapper;->mStackButtonWrapper:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 136
    :goto_2
    if-eqz v2, :cond_3

    .line 137
    iget-object v0, p0, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelWrapper;->mIncognitoButton:Landroid/widget/ImageButton;

    sget v1, Lorg/chromium/chrome/R$drawable;->ntp_toolbar_button_background_selected:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setBackgroundResource(I)V

    .line 139
    iget-object v0, p0, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelWrapper;->mStandardButton:Landroid/widget/ImageButton;

    sget v1, Lorg/chromium/chrome/R$drawable;->ntp_toolbar_button_background:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setBackgroundResource(I)V

    .line 146
    :goto_3
    invoke-direct {p0}, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelWrapper;->getAdapter()Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelAdapter;

    move-result-object v0

    iget-object v1, p0, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelWrapper;->mTabModelSelector:Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

    invoke-interface {v1, v2}, Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;->getModel(Z)Lorg/chromium/chrome/browser/tabmodel/TabModel;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelAdapter;->setTabModel(Lorg/chromium/chrome/browser/tabmodel/TabModel;)V

    goto :goto_0

    :cond_1
    move v0, v1

    .line 125
    goto :goto_1

    .line 133
    :cond_2
    iget-object v0, p0, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelWrapper;->mStackButtonWrapper:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_2

    .line 141
    :cond_3
    iget-object v0, p0, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelWrapper;->mIncognitoButton:Landroid/widget/ImageButton;

    sget v1, Lorg/chromium/chrome/R$drawable;->ntp_toolbar_button_background:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setBackgroundResource(I)V

    .line 142
    iget-object v0, p0, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelWrapper;->mStandardButton:Landroid/widget/ImageButton;

    sget v1, Lorg/chromium/chrome/R$drawable;->ntp_toolbar_button_background_selected:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setBackgroundResource(I)V

    goto :goto_3
.end method

.method public setTabModelSelector(Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;)V
    .locals 2

    .prologue
    .line 108
    iget-boolean v0, p0, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelWrapper;->mIsAttachedToWindow:Z

    if-eqz v0, :cond_0

    .line 109
    iget-object v0, p0, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelWrapper;->mTabModelSelector:Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

    iget-object v1, p0, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelWrapper;->mTabModelChangeListener:Lorg/chromium/chrome/browser/tabmodel/TabModelSelector$ChangeListener;

    invoke-interface {v0, v1}, Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;->unregisterChangeListener(Lorg/chromium/chrome/browser/tabmodel/TabModelSelector$ChangeListener;)V

    .line 111
    :cond_0
    iput-object p1, p0, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelWrapper;->mTabModelSelector:Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

    .line 112
    iget-boolean v0, p0, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelWrapper;->mIsAttachedToWindow:Z

    if-eqz v0, :cond_1

    .line 113
    iget-object v0, p0, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelWrapper;->mTabModelChangeListener:Lorg/chromium/chrome/browser/tabmodel/TabModelSelector$ChangeListener;

    invoke-interface {p1, v0}, Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;->registerChangeListener(Lorg/chromium/chrome/browser/tabmodel/TabModelSelector$ChangeListener;)V

    .line 115
    :cond_1
    invoke-virtual {p0}, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelWrapper;->setStateBasedOnModel()V

    .line 116
    return-void
.end method

.method public setup(Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelAdapter$AccessibilityTabModelAdapterListener;)V
    .locals 3

    .prologue
    .line 88
    sget v0, Lorg/chromium/chrome/R$id;->button_wrapper:I

    invoke-virtual {p0, v0}, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelWrapper;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelWrapper;->mStackButtonWrapper:Landroid/widget/LinearLayout;

    .line 90
    sget v0, Lorg/chromium/chrome/R$id;->standard_tabs_button:I

    invoke-virtual {p0, v0}, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelWrapper;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelWrapper;->mStandardButton:Landroid/widget/ImageButton;

    .line 91
    iget-object v0, p0, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelWrapper;->mStandardButton:Landroid/widget/ImageButton;

    new-instance v1, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelWrapper$ButtonOnClickListener;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelWrapper$ButtonOnClickListener;-><init>(Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelWrapper;Z)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 93
    sget v0, Lorg/chromium/chrome/R$id;->incognito_tabs_button:I

    invoke-virtual {p0, v0}, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelWrapper;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelWrapper;->mIncognitoButton:Landroid/widget/ImageButton;

    .line 94
    iget-object v0, p0, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelWrapper;->mIncognitoButton:Landroid/widget/ImageButton;

    new-instance v1, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelWrapper$ButtonOnClickListener;

    const/4 v2, 0x1

    invoke-direct {v1, p0, v2}, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelWrapper$ButtonOnClickListener;-><init>(Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelWrapper;Z)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 96
    sget v0, Lorg/chromium/chrome/R$id;->list_view:I

    invoke-virtual {p0, v0}, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelWrapper;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListView;

    iput-object v0, p0, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelWrapper;->mAccessibilityView:Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListView;

    .line 98
    invoke-direct {p0}, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelWrapper;->getAdapter()Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelAdapter;

    move-result-object v0

    .line 100
    invoke-virtual {v0, p1}, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelAdapter;->setListener(Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelAdapter$AccessibilityTabModelAdapterListener;)V

    .line 101
    return-void
.end method
