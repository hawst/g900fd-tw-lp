.class public Lorg/chromium/chrome/browser/tabmodel/EmptyTabModelObserver;
.super Ljava/lang/Object;
.source "EmptyTabModelObserver.java"

# interfaces
.implements Lorg/chromium/chrome/browser/tabmodel/TabModelObserver;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public didAddTab(Lorg/chromium/chrome/browser/Tab;Lorg/chromium/chrome/browser/tabmodel/TabModel$TabLaunchType;)V
    .locals 0

    .prologue
    .line 34
    return-void
.end method

.method public didCloseTab(Lorg/chromium/chrome/browser/Tab;)V
    .locals 0

    .prologue
    .line 26
    return-void
.end method

.method public didMoveTab(Lorg/chromium/chrome/browser/Tab;II)V
    .locals 0

    .prologue
    .line 38
    return-void
.end method

.method public didSelectTab(Lorg/chromium/chrome/browser/Tab;Lorg/chromium/chrome/browser/tabmodel/TabModel$TabSelectionType;I)V
    .locals 0

    .prologue
    .line 18
    return-void
.end method

.method public tabClosureCommitted(Lorg/chromium/chrome/browser/Tab;)V
    .locals 0

    .prologue
    .line 50
    return-void
.end method

.method public tabClosureUndone(Lorg/chromium/chrome/browser/Tab;)V
    .locals 0

    .prologue
    .line 46
    return-void
.end method

.method public tabPendingClosure(Lorg/chromium/chrome/browser/Tab;)V
    .locals 0

    .prologue
    .line 42
    return-void
.end method

.method public willAddTab(Lorg/chromium/chrome/browser/Tab;Lorg/chromium/chrome/browser/tabmodel/TabModel$TabLaunchType;)V
    .locals 0

    .prologue
    .line 30
    return-void
.end method

.method public willCloseTab(Lorg/chromium/chrome/browser/Tab;Z)V
    .locals 0

    .prologue
    .line 22
    return-void
.end method
