.class public Lorg/chromium/chrome/browser/sync/DelayedSyncController;
.super Ljava/lang/Object;
.source "DelayedSyncController.java"


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getInstance()Lorg/chromium/chrome/browser/sync/DelayedSyncController;
    .locals 1

    .prologue
    .line 34
    # getter for: Lorg/chromium/chrome/browser/sync/DelayedSyncController$LazyHolder;->INSTANCE:Lorg/chromium/chrome/browser/sync/DelayedSyncController;
    invoke-static {}, Lorg/chromium/chrome/browser/sync/DelayedSyncController$LazyHolder;->access$000()Lorg/chromium/chrome/browser/sync/DelayedSyncController;

    move-result-object v0

    return-object v0
.end method

.method private static isManualSync(Landroid/os/Bundle;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 106
    .line 107
    const-string/jumbo v1, "force"

    invoke-virtual {p0, v1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 108
    const/4 v0, 0x1

    .line 109
    const-string/jumbo v1, "DelayedSyncController"

    const-string/jumbo v2, "Manual sync requested."

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 111
    :cond_0
    return v0
.end method


# virtual methods
.method clearDelayedSyncs(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 88
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lorg/chromium/chrome/browser/sync/DelayedSyncController;->setDelayedSync(Landroid/content/Context;Ljava/lang/String;)V

    .line 89
    return-void
.end method

.method requestSyncOnBackgroundThread(Landroid/content/Context;Landroid/accounts/Account;)V
    .locals 2

    .prologue
    .line 63
    new-instance v0, Lorg/chromium/chrome/browser/sync/DelayedSyncController$1;

    invoke-direct {v0, p0, p1, p2}, Lorg/chromium/chrome/browser/sync/DelayedSyncController$1;-><init>(Lorg/chromium/chrome/browser/sync/DelayedSyncController;Landroid/content/Context;Landroid/accounts/Account;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lorg/chromium/chrome/browser/sync/DelayedSyncController$1;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 72
    return-void
.end method

.method public resumeDelayedSyncs(Landroid/content/Context;)Z
    .locals 3

    .prologue
    .line 44
    invoke-static {p1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 45
    const-string/jumbo v1, "delayed_account"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 46
    if-nez v0, :cond_0

    .line 47
    const-string/jumbo v0, "DelayedSyncController"

    const-string/jumbo v1, "No delayed sync."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 48
    const/4 v0, 0x0

    .line 53
    :goto_0
    return v0

    .line 50
    :cond_0
    const-string/jumbo v1, "DelayedSyncController"

    const-string/jumbo v2, "Handling delayed sync."

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 51
    invoke-static {v0}, Lorg/chromium/sync/signin/AccountManagerHelper;->createAccountFromName(Ljava/lang/String;)Landroid/accounts/Account;

    move-result-object v0

    .line 52
    invoke-virtual {p0, p1, v0}, Lorg/chromium/chrome/browser/sync/DelayedSyncController;->requestSyncOnBackgroundThread(Landroid/content/Context;Landroid/accounts/Account;)V

    .line 53
    const/4 v0, 0x1

    goto :goto_0
.end method

.method setDelayedSync(Landroid/content/Context;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 78
    invoke-static {p1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 79
    const-string/jumbo v1, "delayed_account"

    invoke-interface {v0, v1, p2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 80
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 81
    return-void
.end method

.method shouldPerformSync(Landroid/content/Context;Landroid/os/Bundle;Landroid/accounts/Account;)Z
    .locals 2

    .prologue
    .line 93
    invoke-static {p2}, Lorg/chromium/chrome/browser/sync/DelayedSyncController;->isManualSync(Landroid/os/Bundle;)Z

    move-result v0

    .line 95
    if-nez v0, :cond_0

    invoke-static {}, Lorg/chromium/base/ApplicationStatus;->hasVisibleActivities()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 96
    :cond_0
    invoke-virtual {p0, p1}, Lorg/chromium/chrome/browser/sync/DelayedSyncController;->clearDelayedSyncs(Landroid/content/Context;)V

    .line 97
    const/4 v0, 0x1

    .line 101
    :goto_0
    return v0

    .line 99
    :cond_1
    const-string/jumbo v0, "DelayedSyncController"

    const-string/jumbo v1, "Delaying sync."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 100
    iget-object v0, p3, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {p0, p1, v0}, Lorg/chromium/chrome/browser/sync/DelayedSyncController;->setDelayedSync(Landroid/content/Context;Ljava/lang/String;)V

    .line 101
    const/4 v0, 0x0

    goto :goto_0
.end method
