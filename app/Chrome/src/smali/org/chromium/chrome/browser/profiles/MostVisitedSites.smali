.class public Lorg/chromium/chrome/browser/profiles/MostVisitedSites;
.super Ljava/lang/Object;
.source "MostVisitedSites.java"


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private mNativeMostVisitedSites:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 14
    const-class v0, Lorg/chromium/chrome/browser/profiles/MostVisitedSites;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/chromium/chrome/browser/profiles/MostVisitedSites;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lorg/chromium/chrome/browser/profiles/Profile;)V
    .locals 2

    .prologue
    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    invoke-direct {p0, p1}, Lorg/chromium/chrome/browser/profiles/MostVisitedSites;->nativeInit(Lorg/chromium/chrome/browser/profiles/Profile;)J

    move-result-wide v0

    iput-wide v0, p0, Lorg/chromium/chrome/browser/profiles/MostVisitedSites;->mNativeMostVisitedSites:J

    .line 54
    return-void
.end method

.method static synthetic access$000(Lorg/chromium/chrome/browser/profiles/MostVisitedSites;)J
    .locals 2

    .prologue
    .line 14
    iget-wide v0, p0, Lorg/chromium/chrome/browser/profiles/MostVisitedSites;->mNativeMostVisitedSites:J

    return-wide v0
.end method

.method private native nativeBlacklistUrl(JLjava/lang/String;)V
.end method

.method private native nativeDestroy(J)V
.end method

.method private native nativeGetURLThumbnail(JLjava/lang/String;Lorg/chromium/chrome/browser/profiles/MostVisitedSites$ThumbnailCallback;)V
.end method

.method private native nativeInit(Lorg/chromium/chrome/browser/profiles/Profile;)J
.end method

.method private native nativeOnLoadingComplete(J)V
.end method

.method private native nativeRecordOpenedMostVisitedItem(JI)V
.end method

.method private native nativeSetMostVisitedURLsObserver(JLorg/chromium/chrome/browser/profiles/MostVisitedSites$MostVisitedURLsObserver;I)V
.end method


# virtual methods
.method public blacklistUrl(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 116
    iget-wide v0, p0, Lorg/chromium/chrome/browser/profiles/MostVisitedSites;->mNativeMostVisitedSites:J

    invoke-direct {p0, v0, v1, p1}, Lorg/chromium/chrome/browser/profiles/MostVisitedSites;->nativeBlacklistUrl(JLjava/lang/String;)V

    .line 117
    return-void
.end method

.method public destroy()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 60
    sget-boolean v0, Lorg/chromium/chrome/browser/profiles/MostVisitedSites;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-wide v0, p0, Lorg/chromium/chrome/browser/profiles/MostVisitedSites;->mNativeMostVisitedSites:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 61
    :cond_0
    iget-wide v0, p0, Lorg/chromium/chrome/browser/profiles/MostVisitedSites;->mNativeMostVisitedSites:J

    invoke-direct {p0, v0, v1}, Lorg/chromium/chrome/browser/profiles/MostVisitedSites;->nativeDestroy(J)V

    .line 62
    iput-wide v2, p0, Lorg/chromium/chrome/browser/profiles/MostVisitedSites;->mNativeMostVisitedSites:J

    .line 63
    return-void
.end method

.method protected finalize()V
    .locals 4

    .prologue
    .line 68
    sget-boolean v0, Lorg/chromium/chrome/browser/profiles/MostVisitedSites;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-wide v0, p0, Lorg/chromium/chrome/browser/profiles/MostVisitedSites;->mNativeMostVisitedSites:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 69
    :cond_0
    return-void
.end method

.method public getURLThumbnail(Ljava/lang/String;Lorg/chromium/chrome/browser/profiles/MostVisitedSites$ThumbnailCallback;)V
    .locals 4

    .prologue
    .line 99
    new-instance v0, Lorg/chromium/chrome/browser/profiles/MostVisitedSites$2;

    invoke-direct {v0, p0, p2}, Lorg/chromium/chrome/browser/profiles/MostVisitedSites$2;-><init>(Lorg/chromium/chrome/browser/profiles/MostVisitedSites;Lorg/chromium/chrome/browser/profiles/MostVisitedSites$ThumbnailCallback;)V

    .line 108
    iget-wide v2, p0, Lorg/chromium/chrome/browser/profiles/MostVisitedSites;->mNativeMostVisitedSites:J

    invoke-direct {p0, v2, v3, p1, v0}, Lorg/chromium/chrome/browser/profiles/MostVisitedSites;->nativeGetURLThumbnail(JLjava/lang/String;Lorg/chromium/chrome/browser/profiles/MostVisitedSites$ThumbnailCallback;)V

    .line 109
    return-void
.end method

.method public onLoadingComplete()V
    .locals 2

    .prologue
    .line 123
    iget-wide v0, p0, Lorg/chromium/chrome/browser/profiles/MostVisitedSites;->mNativeMostVisitedSites:J

    invoke-direct {p0, v0, v1}, Lorg/chromium/chrome/browser/profiles/MostVisitedSites;->nativeOnLoadingComplete(J)V

    .line 124
    return-void
.end method

.method public recordOpenedMostVisitedItem(I)V
    .locals 2

    .prologue
    .line 131
    iget-wide v0, p0, Lorg/chromium/chrome/browser/profiles/MostVisitedSites;->mNativeMostVisitedSites:J

    invoke-direct {p0, v0, v1, p1}, Lorg/chromium/chrome/browser/profiles/MostVisitedSites;->nativeRecordOpenedMostVisitedItem(JI)V

    .line 132
    return-void
.end method

.method public setMostVisitedURLsObserver(Lorg/chromium/chrome/browser/profiles/MostVisitedSites$MostVisitedURLsObserver;I)V
    .locals 4

    .prologue
    .line 80
    new-instance v0, Lorg/chromium/chrome/browser/profiles/MostVisitedSites$1;

    invoke-direct {v0, p0, p1}, Lorg/chromium/chrome/browser/profiles/MostVisitedSites$1;-><init>(Lorg/chromium/chrome/browser/profiles/MostVisitedSites;Lorg/chromium/chrome/browser/profiles/MostVisitedSites$MostVisitedURLsObserver;)V

    .line 89
    iget-wide v2, p0, Lorg/chromium/chrome/browser/profiles/MostVisitedSites;->mNativeMostVisitedSites:J

    invoke-direct {p0, v2, v3, v0, p2}, Lorg/chromium/chrome/browser/profiles/MostVisitedSites;->nativeSetMostVisitedURLsObserver(JLorg/chromium/chrome/browser/profiles/MostVisitedSites$MostVisitedURLsObserver;I)V

    .line 90
    return-void
.end method
