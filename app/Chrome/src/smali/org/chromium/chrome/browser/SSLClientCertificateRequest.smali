.class public Lorg/chromium/chrome/browser/SSLClientCertificateRequest;
.super Ljava/lang/Object;
.source "SSLClientCertificateRequest.java"


# annotations
.annotation runtime Lorg/chromium/base/JNINamespace;
.end annotation


# static fields
.field static final TAG:Ljava/lang/String; = "SSLClientCertificateRequest"

.field private static final sLocalKeyStore:Lorg/chromium/net/DefaultAndroidKeyStore;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 41
    new-instance v0, Lorg/chromium/net/DefaultAndroidKeyStore;

    invoke-direct {v0}, Lorg/chromium/net/DefaultAndroidKeyStore;-><init>()V

    sput-object v0, Lorg/chromium/chrome/browser/SSLClientCertificateRequest;->sLocalKeyStore:Lorg/chromium/net/DefaultAndroidKeyStore;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 182
    return-void
.end method

.method static synthetic access$000(J[[BLorg/chromium/net/AndroidPrivateKey;)V
    .locals 0

    .prologue
    .line 38
    invoke-static {p0, p1, p2, p3}, Lorg/chromium/chrome/browser/SSLClientCertificateRequest;->nativeOnSystemRequestCompletion(J[[BLorg/chromium/net/AndroidPrivateKey;)V

    return-void
.end method

.method static synthetic access$100()Lorg/chromium/net/DefaultAndroidKeyStore;
    .locals 1

    .prologue
    .line 38
    sget-object v0, Lorg/chromium/chrome/browser/SSLClientCertificateRequest;->sLocalKeyStore:Lorg/chromium/net/DefaultAndroidKeyStore;

    return-object v0
.end method

.method private static native nativeNotifyClientCertificatesChangedOnIOThread()V
.end method

.method private static native nativeOnSystemRequestCompletion(J[[BLorg/chromium/net/AndroidPrivateKey;)V
.end method

.method public static notifyClientCertificatesChangedOnIOThread()V
    .locals 2

    .prologue
    .line 300
    const-string/jumbo v0, "SSLClientCertificateRequest"

    const-string/jumbo v1, "ClientCertificatesChanged!"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 301
    invoke-static {}, Lorg/chromium/chrome/browser/SSLClientCertificateRequest;->nativeNotifyClientCertificatesChangedOnIOThread()V

    .line 302
    return-void
.end method

.method private static selectClientCertificate(JLorg/chromium/ui/base/WindowAndroid;[Ljava/lang/String;[[BLjava/lang/String;I)Z
    .locals 10

    .prologue
    .line 230
    invoke-static {}, Lorg/chromium/base/ThreadUtils;->assertOnUiThread()V

    .line 232
    invoke-virtual {p2}, Lorg/chromium/ui/base/WindowAndroid;->getActivity()Ljava/lang/ref/WeakReference;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/Activity;

    .line 233
    if-nez v1, :cond_0

    .line 234
    const-string/jumbo v0, "SSLClientCertificateRequest"

    const-string/jumbo v1, "Certificate request on GC\'d activity."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 235
    const/4 v0, 0x0

    .line 296
    :goto_0
    return v0

    .line 239
    :cond_0
    const/4 v5, 0x0

    .line 240
    array-length v0, p4

    if-lez v0, :cond_1

    .line 241
    array-length v0, p4

    new-array v5, v0, [Ljavax/security/auth/x500/X500Principal;

    .line 243
    const/4 v0, 0x0

    :goto_1
    :try_start_0
    array-length v2, p4

    if-ge v0, v2, :cond_1

    .line 244
    new-instance v2, Ljavax/security/auth/x500/X500Principal;

    aget-object v3, p4, v0

    invoke-direct {v2, v3}, Ljavax/security/auth/x500/X500Principal;-><init>([B)V

    aput-object v2, v5, v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 243
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 246
    :catch_0
    move-exception v0

    .line 247
    const-string/jumbo v1, "SSLClientCertificateRequest"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "Exception while decoding issuers list: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 248
    const/4 v0, 0x0

    goto :goto_0

    .line 255
    :cond_1
    new-instance v0, Lorg/chromium/chrome/browser/SSLClientCertificateRequest$1;

    move-wide v2, p0

    move-object v4, p3

    move-object v6, p5

    move/from16 v7, p6

    invoke-direct/range {v0 .. v7}, Lorg/chromium/chrome/browser/SSLClientCertificateRequest$1;-><init>(Landroid/app/Activity;J[Ljava/lang/String;[Ljava/security/Principal;Ljava/lang/String;I)V

    .line 266
    invoke-virtual {v1}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    .line 267
    check-cast v2, Lorg/chromium/chrome/browser/ChromiumApplication;

    invoke-virtual {v2}, Lorg/chromium/chrome/browser/ChromiumApplication;->getPKCS11AuthenticationManager()Lorg/chromium/chrome/browser/PKCS11AuthenticationManager;

    move-result-object v8

    .line 269
    invoke-interface {v8}, Lorg/chromium/chrome/browser/PKCS11AuthenticationManager;->isPKCS11AuthEnabled()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 272
    new-instance v3, Lorg/chromium/chrome/browser/SSLClientCertificateRequest$2;

    move-wide v4, p0

    move-object v6, p5

    move/from16 v7, p6

    invoke-direct/range {v3 .. v8}, Lorg/chromium/chrome/browser/SSLClientCertificateRequest$2;-><init>(JLjava/lang/String;ILorg/chromium/chrome/browser/PKCS11AuthenticationManager;)V

    .line 279
    new-instance v2, Lorg/chromium/chrome/browser/SSLClientCertificateRequest$3;

    invoke-direct {v2, p0, p1}, Lorg/chromium/chrome/browser/SSLClientCertificateRequest$3;-><init>(J)V

    .line 287
    new-instance v4, Lorg/chromium/chrome/browser/KeyStoreSelectionDialog;

    invoke-direct {v4, v0, v3, v2}, Lorg/chromium/chrome/browser/KeyStoreSelectionDialog;-><init>(Ljava/lang/Runnable;Ljava/lang/Runnable;Ljava/lang/Runnable;)V

    .line 289
    invoke-virtual {v1}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v4, v0, v1}, Lorg/chromium/chrome/browser/KeyStoreSelectionDialog;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 296
    :goto_2
    const/4 v0, 0x1

    goto :goto_0

    .line 292
    :cond_2
    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    goto :goto_2
.end method
