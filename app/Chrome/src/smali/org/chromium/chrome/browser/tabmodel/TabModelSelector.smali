.class public interface abstract Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;
.super Ljava/lang/Object;
.source "TabModelSelector.java"


# virtual methods
.method public abstract closeAllTabs()V
.end method

.method public abstract closeTab(Lorg/chromium/chrome/browser/Tab;)Z
.end method

.method public abstract commitAllTabClosures()V
.end method

.method public abstract getCurrentModel()Lorg/chromium/chrome/browser/tabmodel/TabModel;
.end method

.method public abstract getCurrentTab()Lorg/chromium/chrome/browser/Tab;
.end method

.method public abstract getCurrentTabId()I
.end method

.method public abstract getModel(Z)Lorg/chromium/chrome/browser/tabmodel/TabModel;
.end method

.method public abstract getModelAt(I)Lorg/chromium/chrome/browser/tabmodel/TabModel;
.end method

.method public abstract getModelForTabId(I)Lorg/chromium/chrome/browser/tabmodel/TabModel;
.end method

.method public abstract getModels()Ljava/util/List;
.end method

.method public abstract getTabById(I)Lorg/chromium/chrome/browser/Tab;
.end method

.method public abstract getTotalTabCount()I
.end method

.method public abstract isIncognitoSelected()Z
.end method

.method public abstract openNewTab(Lorg/chromium/content_public/browser/LoadUrlParams;Lorg/chromium/chrome/browser/tabmodel/TabModel$TabLaunchType;Lorg/chromium/chrome/browser/Tab;Z)Lorg/chromium/chrome/browser/Tab;
.end method

.method public abstract registerChangeListener(Lorg/chromium/chrome/browser/tabmodel/TabModelSelector$ChangeListener;)V
.end method

.method public abstract selectModel(Z)V
.end method

.method public abstract unregisterChangeListener(Lorg/chromium/chrome/browser/tabmodel/TabModelSelector$ChangeListener;)V
.end method
