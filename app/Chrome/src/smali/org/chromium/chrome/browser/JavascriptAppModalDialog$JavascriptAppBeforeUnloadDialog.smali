.class Lorg/chromium/chrome/browser/JavascriptAppModalDialog$JavascriptAppBeforeUnloadDialog;
.super Lorg/chromium/chrome/browser/JavascriptAppModalDialog$JavascriptAppConfirmDialog;
.source "JavascriptAppModalDialog.java"


# instance fields
.field private final mIsReload:Z


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;ZZ)V
    .locals 0

    .prologue
    .line 240
    invoke-direct {p0, p1, p2, p4}, Lorg/chromium/chrome/browser/JavascriptAppModalDialog$JavascriptAppConfirmDialog;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 241
    iput-boolean p3, p0, Lorg/chromium/chrome/browser/JavascriptAppModalDialog$JavascriptAppBeforeUnloadDialog;->mIsReload:Z

    .line 242
    return-void
.end method


# virtual methods
.method public getNegativeButtonText()I
    .locals 1

    .prologue
    .line 261
    iget-boolean v0, p0, Lorg/chromium/chrome/browser/JavascriptAppModalDialog$JavascriptAppBeforeUnloadDialog;->mIsReload:Z

    if-eqz v0, :cond_0

    sget v0, Lorg/chromium/chrome/R$string;->dont_reload_this_page:I

    :goto_0
    return v0

    :cond_0
    sget v0, Lorg/chromium/chrome/R$string;->stay_on_this_page:I

    goto :goto_0
.end method

.method public getPositiveButtonText()I
    .locals 1

    .prologue
    .line 251
    iget-boolean v0, p0, Lorg/chromium/chrome/browser/JavascriptAppModalDialog$JavascriptAppBeforeUnloadDialog;->mIsReload:Z

    if-eqz v0, :cond_0

    sget v0, Lorg/chromium/chrome/R$string;->reload_this_page:I

    :goto_0
    return v0

    :cond_0
    sget v0, Lorg/chromium/chrome/R$string;->leave_this_page:I

    goto :goto_0
.end method

.method public hasNegativeButton()Z
    .locals 1

    .prologue
    .line 256
    const/4 v0, 0x1

    return v0
.end method

.method public hasPositiveButton()Z
    .locals 1

    .prologue
    .line 246
    const/4 v0, 0x1

    return v0
.end method
