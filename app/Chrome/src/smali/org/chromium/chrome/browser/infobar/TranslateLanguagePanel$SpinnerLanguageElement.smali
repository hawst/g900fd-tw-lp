.class Lorg/chromium/chrome/browser/infobar/TranslateLanguagePanel$SpinnerLanguageElement;
.super Ljava/lang/Object;
.source "TranslateLanguagePanel.java"


# instance fields
.field private final mLanguageId:I

.field private final mLanguageName:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 265
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 266
    iput-object p1, p0, Lorg/chromium/chrome/browser/infobar/TranslateLanguagePanel$SpinnerLanguageElement;->mLanguageName:Ljava/lang/String;

    .line 267
    iput p2, p0, Lorg/chromium/chrome/browser/infobar/TranslateLanguagePanel$SpinnerLanguageElement;->mLanguageId:I

    .line 268
    return-void
.end method


# virtual methods
.method public getLanguageId()I
    .locals 1

    .prologue
    .line 271
    iget v0, p0, Lorg/chromium/chrome/browser/infobar/TranslateLanguagePanel$SpinnerLanguageElement;->mLanguageId:I

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 280
    iget-object v0, p0, Lorg/chromium/chrome/browser/infobar/TranslateLanguagePanel$SpinnerLanguageElement;->mLanguageName:Ljava/lang/String;

    return-object v0
.end method
