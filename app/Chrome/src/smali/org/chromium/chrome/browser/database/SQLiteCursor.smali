.class public Lorg/chromium/chrome/browser/database/SQLiteCursor;
.super Landroid/database/AbstractCursor;
.source "SQLiteCursor.java"


# instance fields
.field private final mColumnTypeLock:Ljava/lang/Object;

.field private mColumnTypes:[I

.field private mCount:I

.field private final mDestoryNativeLock:Ljava/lang/Object;

.field private final mGetBlobLock:Ljava/lang/Object;

.field private final mMoveLock:Ljava/lang/Object;

.field private mNativeSQLiteCursor:J


# direct methods
.method private constructor <init>(J)V
    .locals 1

    .prologue
    .line 37
    invoke-direct {p0}, Landroid/database/AbstractCursor;-><init>()V

    .line 25
    const/4 v0, -0x1

    iput v0, p0, Lorg/chromium/chrome/browser/database/SQLiteCursor;->mCount:I

    .line 29
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lorg/chromium/chrome/browser/database/SQLiteCursor;->mColumnTypeLock:Ljava/lang/Object;

    .line 30
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lorg/chromium/chrome/browser/database/SQLiteCursor;->mDestoryNativeLock:Ljava/lang/Object;

    .line 34
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lorg/chromium/chrome/browser/database/SQLiteCursor;->mMoveLock:Ljava/lang/Object;

    .line 35
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lorg/chromium/chrome/browser/database/SQLiteCursor;->mGetBlobLock:Ljava/lang/Object;

    .line 38
    iput-wide p1, p0, Lorg/chromium/chrome/browser/database/SQLiteCursor;->mNativeSQLiteCursor:J

    .line 39
    return-void
.end method

.method private static create(J)Lorg/chromium/chrome/browser/database/SQLiteCursor;
    .locals 2

    .prologue
    .line 43
    new-instance v0, Lorg/chromium/chrome/browser/database/SQLiteCursor;

    invoke-direct {v0, p0, p1}, Lorg/chromium/chrome/browser/database/SQLiteCursor;-><init>(J)V

    return-object v0
.end method

.method private fillRow(Landroid/database/CursorWindow;Ljava/lang/Object;II)Z
    .locals 1

    .prologue
    .line 190
    invoke-direct {p0, p1, p2, p3, p4}, Lorg/chromium/chrome/browser/database/SQLiteCursor;->putValue(Landroid/database/CursorWindow;Ljava/lang/Object;II)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 191
    const/4 v0, 0x1

    .line 194
    :goto_0
    return v0

    .line 193
    :cond_0
    invoke-virtual {p1}, Landroid/database/CursorWindow;->freeLastRow()V

    .line 194
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private getColumnType(I)I
    .locals 6

    .prologue
    .line 225
    iget-object v1, p0, Lorg/chromium/chrome/browser/database/SQLiteCursor;->mColumnTypeLock:Ljava/lang/Object;

    monitor-enter v1

    .line 226
    :try_start_0
    iget-object v0, p0, Lorg/chromium/chrome/browser/database/SQLiteCursor;->mColumnTypes:[I

    if-nez v0, :cond_0

    .line 227
    invoke-virtual {p0}, Lorg/chromium/chrome/browser/database/SQLiteCursor;->getColumnCount()I

    move-result v2

    .line 228
    new-array v0, v2, [I

    iput-object v0, p0, Lorg/chromium/chrome/browser/database/SQLiteCursor;->mColumnTypes:[I

    .line 229
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    .line 230
    iget-object v3, p0, Lorg/chromium/chrome/browser/database/SQLiteCursor;->mColumnTypes:[I

    iget-wide v4, p0, Lorg/chromium/chrome/browser/database/SQLiteCursor;->mNativeSQLiteCursor:J

    invoke-direct {p0, v4, v5, v0}, Lorg/chromium/chrome/browser/database/SQLiteCursor;->nativeGetColumnType(JI)I

    move-result v4

    aput v4, v3, v0

    .line 229
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 233
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 234
    iget-object v0, p0, Lorg/chromium/chrome/browser/database/SQLiteCursor;->mColumnTypes:[I

    aget v0, v0, p1

    return v0

    .line 233
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private native nativeDestroy(J)V
.end method

.method private native nativeGetBlob(JI)[B
.end method

.method private native nativeGetColumnNames(J)[Ljava/lang/String;
.end method

.method private native nativeGetColumnType(JI)I
.end method

.method private native nativeGetCount(J)I
.end method

.method private native nativeGetDouble(JI)D
.end method

.method private native nativeGetInt(JI)I
.end method

.method private native nativeGetLong(JI)J
.end method

.method private native nativeGetString(JI)Ljava/lang/String;
.end method

.method private native nativeIsNull(JI)Z
.end method

.method private native nativeMoveTo(JI)I
.end method

.method private putValue(Landroid/database/CursorWindow;Ljava/lang/Object;II)Z
    .locals 2

    .prologue
    .line 205
    if-nez p2, :cond_0

    .line 206
    invoke-virtual {p1, p3, p4}, Landroid/database/CursorWindow;->putNull(II)Z

    move-result v0

    .line 216
    :goto_0
    return v0

    .line 207
    :cond_0
    instance-of v0, p2, Ljava/lang/Long;

    if-eqz v0, :cond_1

    .line 208
    check-cast p2, Ljava/lang/Long;

    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1, p3, p4}, Landroid/database/CursorWindow;->putLong(JII)Z

    move-result v0

    goto :goto_0

    .line 209
    :cond_1
    instance-of v0, p2, Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 210
    check-cast p2, Ljava/lang/String;

    invoke-virtual {p1, p2, p3, p4}, Landroid/database/CursorWindow;->putString(Ljava/lang/String;II)Z

    move-result v0

    goto :goto_0

    .line 211
    :cond_2
    instance-of v0, p2, [B

    if-eqz v0, :cond_3

    move-object v0, p2

    check-cast v0, [B

    array-length v0, v0

    if-lez v0, :cond_3

    .line 212
    check-cast p2, [B

    invoke-virtual {p1, p2, p3, p4}, Landroid/database/CursorWindow;->putBlob([BII)Z

    move-result v0

    goto :goto_0

    .line 213
    :cond_3
    instance-of v0, p2, Ljava/lang/Double;

    if-eqz v0, :cond_4

    .line 214
    check-cast p2, Ljava/lang/Double;

    invoke-virtual {p2}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v0

    invoke-virtual {p1, v0, v1, p3, p4}, Landroid/database/CursorWindow;->putDouble(DII)Z

    move-result v0

    goto :goto_0

    .line 216
    :cond_4
    invoke-virtual {p1, p3, p4}, Landroid/database/CursorWindow;->putNull(II)Z

    move-result v0

    goto :goto_0
.end method


# virtual methods
.method public close()V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 97
    invoke-super {p0}, Landroid/database/AbstractCursor;->close()V

    .line 98
    iget-object v1, p0, Lorg/chromium/chrome/browser/database/SQLiteCursor;->mDestoryNativeLock:Ljava/lang/Object;

    monitor-enter v1

    .line 99
    :try_start_0
    iget-wide v2, p0, Lorg/chromium/chrome/browser/database/SQLiteCursor;->mNativeSQLiteCursor:J

    cmp-long v0, v2, v4

    if-eqz v0, :cond_0

    .line 100
    iget-wide v2, p0, Lorg/chromium/chrome/browser/database/SQLiteCursor;->mNativeSQLiteCursor:J

    invoke-direct {p0, v2, v3}, Lorg/chromium/chrome/browser/database/SQLiteCursor;->nativeDestroy(J)V

    .line 101
    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lorg/chromium/chrome/browser/database/SQLiteCursor;->mNativeSQLiteCursor:J

    .line 103
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public fillWindow(ILandroid/database/CursorWindow;)V
    .locals 6

    .prologue
    .line 137
    if-ltz p1, :cond_0

    invoke-virtual {p0}, Lorg/chromium/chrome/browser/database/SQLiteCursor;->getCount()I

    move-result v0

    if-le p1, v0, :cond_1

    .line 180
    :cond_0
    :goto_0
    return-void

    .line 140
    :cond_1
    invoke-virtual {p2}, Landroid/database/CursorWindow;->acquireReference()V

    .line 142
    :try_start_0
    iget v2, p0, Lorg/chromium/chrome/browser/database/SQLiteCursor;->mPos:I

    .line 143
    add-int/lit8 v0, p1, -0x1

    iput v0, p0, Lorg/chromium/chrome/browser/database/SQLiteCursor;->mPos:I

    .line 144
    invoke-virtual {p2}, Landroid/database/CursorWindow;->clear()V

    .line 145
    invoke-virtual {p2, p1}, Landroid/database/CursorWindow;->setStartPosition(I)V

    .line 146
    invoke-virtual {p0}, Lorg/chromium/chrome/browser/database/SQLiteCursor;->getColumnCount()I

    move-result v3

    .line 147
    invoke-virtual {p2, v3}, Landroid/database/CursorWindow;->setNumColumns(I)Z

    .line 148
    :cond_2
    invoke-virtual {p0}, Lorg/chromium/chrome/browser/database/SQLiteCursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p2}, Landroid/database/CursorWindow;->allocRow()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 149
    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_2

    .line 150
    const/4 v0, 0x1

    .line 151
    invoke-direct {p0, v1}, Lorg/chromium/chrome/browser/database/SQLiteCursor;->getColumnType(I)I

    move-result v4

    sparse-switch v4, :sswitch_data_0

    .line 166
    :goto_2
    if-eqz v0, :cond_2

    .line 171
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 153
    :sswitch_0
    invoke-virtual {p0, v1}, Lorg/chromium/chrome/browser/database/SQLiteCursor;->getDouble(I)D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    iget v4, p0, Lorg/chromium/chrome/browser/database/SQLiteCursor;->mPos:I

    invoke-direct {p0, p2, v0, v4, v1}, Lorg/chromium/chrome/browser/database/SQLiteCursor;->fillRow(Landroid/database/CursorWindow;Ljava/lang/Object;II)Z

    move-result v0

    goto :goto_2

    .line 156
    :sswitch_1
    invoke-virtual {p0, v1}, Lorg/chromium/chrome/browser/database/SQLiteCursor;->getLong(I)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iget v4, p0, Lorg/chromium/chrome/browser/database/SQLiteCursor;->mPos:I

    invoke-direct {p0, p2, v0, v4, v1}, Lorg/chromium/chrome/browser/database/SQLiteCursor;->fillRow(Landroid/database/CursorWindow;Ljava/lang/Object;II)Z

    move-result v0

    goto :goto_2

    .line 159
    :sswitch_2
    invoke-virtual {p0, v1}, Lorg/chromium/chrome/browser/database/SQLiteCursor;->getBlob(I)[B

    move-result-object v0

    iget v4, p0, Lorg/chromium/chrome/browser/database/SQLiteCursor;->mPos:I

    invoke-direct {p0, p2, v0, v4, v1}, Lorg/chromium/chrome/browser/database/SQLiteCursor;->fillRow(Landroid/database/CursorWindow;Ljava/lang/Object;II)Z

    move-result v0

    goto :goto_2

    .line 162
    :sswitch_3
    invoke-virtual {p0, v1}, Lorg/chromium/chrome/browser/database/SQLiteCursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget v4, p0, Lorg/chromium/chrome/browser/database/SQLiteCursor;->mPos:I

    invoke-direct {p0, p2, v0, v4, v1}, Lorg/chromium/chrome/browser/database/SQLiteCursor;->fillRow(Landroid/database/CursorWindow;Ljava/lang/Object;II)Z

    move-result v0

    goto :goto_2

    .line 165
    :sswitch_4
    const/4 v0, 0x0

    iget v4, p0, Lorg/chromium/chrome/browser/database/SQLiteCursor;->mPos:I

    invoke-direct {p0, p2, v0, v4, v1}, Lorg/chromium/chrome/browser/database/SQLiteCursor;->fillRow(Landroid/database/CursorWindow;Ljava/lang/Object;II)Z

    move-result v0

    goto :goto_2

    .line 175
    :cond_3
    iput v2, p0, Lorg/chromium/chrome/browser/database/SQLiteCursor;->mPos:I
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 179
    invoke-virtual {p2}, Landroid/database/CursorWindow;->releaseReference()V

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {p2}, Landroid/database/CursorWindow;->releaseReference()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-virtual {p2}, Landroid/database/CursorWindow;->releaseReference()V

    throw v0

    .line 151
    nop

    :sswitch_data_0
    .sparse-switch
        -0x1 -> :sswitch_3
        0x0 -> :sswitch_4
        0x2 -> :sswitch_1
        0x8 -> :sswitch_0
        0x7d4 -> :sswitch_2
    .end sparse-switch
.end method

.method protected finalize()V
    .locals 2

    .prologue
    .line 128
    invoke-super {p0}, Landroid/database/AbstractCursor;->finalize()V

    .line 129
    invoke-virtual {p0}, Lorg/chromium/chrome/browser/database/SQLiteCursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 130
    const-string/jumbo v0, "SQLiteCursor"

    const-string/jumbo v1, "Cursor hasn\'t been closed"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 131
    invoke-virtual {p0}, Lorg/chromium/chrome/browser/database/SQLiteCursor;->close()V

    .line 133
    :cond_0
    return-void
.end method

.method public getBlob(I)[B
    .locals 4

    .prologue
    .line 116
    iget-object v1, p0, Lorg/chromium/chrome/browser/database/SQLiteCursor;->mGetBlobLock:Ljava/lang/Object;

    monitor-enter v1

    .line 117
    :try_start_0
    iget-wide v2, p0, Lorg/chromium/chrome/browser/database/SQLiteCursor;->mNativeSQLiteCursor:J

    invoke-direct {p0, v2, v3, p1}, Lorg/chromium/chrome/browser/database/SQLiteCursor;->nativeGetBlob(JI)[B

    move-result-object v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    .line 118
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public getColumnNames()[Ljava/lang/String;
    .locals 2

    .prologue
    .line 57
    iget-wide v0, p0, Lorg/chromium/chrome/browser/database/SQLiteCursor;->mNativeSQLiteCursor:J

    invoke-direct {p0, v0, v1}, Lorg/chromium/chrome/browser/database/SQLiteCursor;->nativeGetColumnNames(J)[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getCount()I
    .locals 4

    .prologue
    .line 48
    iget-object v1, p0, Lorg/chromium/chrome/browser/database/SQLiteCursor;->mMoveLock:Ljava/lang/Object;

    monitor-enter v1

    .line 49
    :try_start_0
    iget v0, p0, Lorg/chromium/chrome/browser/database/SQLiteCursor;->mCount:I

    const/4 v2, -0x1

    if-ne v0, v2, :cond_0

    .line 50
    iget-wide v2, p0, Lorg/chromium/chrome/browser/database/SQLiteCursor;->mNativeSQLiteCursor:J

    invoke-direct {p0, v2, v3}, Lorg/chromium/chrome/browser/database/SQLiteCursor;->nativeGetCount(J)I

    move-result v0

    iput v0, p0, Lorg/chromium/chrome/browser/database/SQLiteCursor;->mCount:I

    .line 51
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 52
    iget v0, p0, Lorg/chromium/chrome/browser/database/SQLiteCursor;->mCount:I

    return v0

    .line 51
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public getDouble(I)D
    .locals 2

    .prologue
    .line 87
    iget-wide v0, p0, Lorg/chromium/chrome/browser/database/SQLiteCursor;->mNativeSQLiteCursor:J

    invoke-direct {p0, v0, v1, p1}, Lorg/chromium/chrome/browser/database/SQLiteCursor;->nativeGetDouble(JI)D

    move-result-wide v0

    return-wide v0
.end method

.method public getFloat(I)F
    .locals 2

    .prologue
    .line 82
    iget-wide v0, p0, Lorg/chromium/chrome/browser/database/SQLiteCursor;->mNativeSQLiteCursor:J

    invoke-direct {p0, v0, v1, p1}, Lorg/chromium/chrome/browser/database/SQLiteCursor;->nativeGetDouble(JI)D

    move-result-wide v0

    double-to-float v0, v0

    return v0
.end method

.method public getInt(I)I
    .locals 2

    .prologue
    .line 72
    iget-wide v0, p0, Lorg/chromium/chrome/browser/database/SQLiteCursor;->mNativeSQLiteCursor:J

    invoke-direct {p0, v0, v1, p1}, Lorg/chromium/chrome/browser/database/SQLiteCursor;->nativeGetInt(JI)I

    move-result v0

    return v0
.end method

.method public getLong(I)J
    .locals 2

    .prologue
    .line 77
    iget-wide v0, p0, Lorg/chromium/chrome/browser/database/SQLiteCursor;->mNativeSQLiteCursor:J

    invoke-direct {p0, v0, v1, p1}, Lorg/chromium/chrome/browser/database/SQLiteCursor;->nativeGetLong(JI)J

    move-result-wide v0

    return-wide v0
.end method

.method public getShort(I)S
    .locals 2

    .prologue
    .line 67
    iget-wide v0, p0, Lorg/chromium/chrome/browser/database/SQLiteCursor;->mNativeSQLiteCursor:J

    invoke-direct {p0, v0, v1, p1}, Lorg/chromium/chrome/browser/database/SQLiteCursor;->nativeGetInt(JI)I

    move-result v0

    int-to-short v0, v0

    return v0
.end method

.method public getString(I)Ljava/lang/String;
    .locals 2

    .prologue
    .line 62
    iget-wide v0, p0, Lorg/chromium/chrome/browser/database/SQLiteCursor;->mNativeSQLiteCursor:J

    invoke-direct {p0, v0, v1, p1}, Lorg/chromium/chrome/browser/database/SQLiteCursor;->nativeGetString(JI)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public isNull(I)Z
    .locals 2

    .prologue
    .line 92
    iget-wide v0, p0, Lorg/chromium/chrome/browser/database/SQLiteCursor;->mNativeSQLiteCursor:J

    invoke-direct {p0, v0, v1, p1}, Lorg/chromium/chrome/browser/database/SQLiteCursor;->nativeIsNull(JI)Z

    move-result v0

    return v0
.end method

.method public onMove(II)Z
    .locals 4

    .prologue
    .line 108
    iget-object v1, p0, Lorg/chromium/chrome/browser/database/SQLiteCursor;->mMoveLock:Ljava/lang/Object;

    monitor-enter v1

    .line 109
    :try_start_0
    iget-wide v2, p0, Lorg/chromium/chrome/browser/database/SQLiteCursor;->mNativeSQLiteCursor:J

    invoke-direct {p0, v2, v3, p2}, Lorg/chromium/chrome/browser/database/SQLiteCursor;->nativeMoveTo(JI)I

    .line 110
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 111
    invoke-super {p0, p1, p2}, Landroid/database/AbstractCursor;->onMove(II)Z

    move-result v0

    return v0

    .line 110
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
