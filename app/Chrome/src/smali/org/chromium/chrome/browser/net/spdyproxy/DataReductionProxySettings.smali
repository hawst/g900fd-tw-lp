.class public Lorg/chromium/chrome/browser/net/spdyproxy/DataReductionProxySettings;
.super Ljava/lang/Object;
.source "DataReductionProxySettings.java"


# static fields
.field private static sSettings:Lorg/chromium/chrome/browser/net/spdyproxy/DataReductionProxySettings;


# instance fields
.field private final mNativeDataReductionProxySettings:J


# direct methods
.method private constructor <init>()V
    .locals 2

    .prologue
    .line 60
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 64
    invoke-direct {p0}, Lorg/chromium/chrome/browser/net/spdyproxy/DataReductionProxySettings;->nativeInit()J

    move-result-wide v0

    iput-wide v0, p0, Lorg/chromium/chrome/browser/net/spdyproxy/DataReductionProxySettings;->mNativeDataReductionProxySettings:J

    .line 65
    return-void
.end method

.method public static getInstance()Lorg/chromium/chrome/browser/net/spdyproxy/DataReductionProxySettings;
    .locals 1

    .prologue
    .line 51
    invoke-static {}, Lorg/chromium/base/ThreadUtils;->assertOnUiThread()V

    .line 52
    sget-object v0, Lorg/chromium/chrome/browser/net/spdyproxy/DataReductionProxySettings;->sSettings:Lorg/chromium/chrome/browser/net/spdyproxy/DataReductionProxySettings;

    if-nez v0, :cond_0

    .line 53
    new-instance v0, Lorg/chromium/chrome/browser/net/spdyproxy/DataReductionProxySettings;

    invoke-direct {v0}, Lorg/chromium/chrome/browser/net/spdyproxy/DataReductionProxySettings;-><init>()V

    sput-object v0, Lorg/chromium/chrome/browser/net/spdyproxy/DataReductionProxySettings;->sSettings:Lorg/chromium/chrome/browser/net/spdyproxy/DataReductionProxySettings;

    .line 55
    :cond_0
    sget-object v0, Lorg/chromium/chrome/browser/net/spdyproxy/DataReductionProxySettings;->sSettings:Lorg/chromium/chrome/browser/net/spdyproxy/DataReductionProxySettings;

    return-object v0
.end method

.method private native nativeGetContentLengths(J)Lorg/chromium/chrome/browser/net/spdyproxy/DataReductionProxySettings$ContentLengths;
.end method

.method private native nativeGetDailyOriginalContentLengths(J)[J
.end method

.method private native nativeGetDailyReceivedContentLengths(J)[J
.end method

.method private native nativeGetDataReductionLastUpdateTime(J)J
.end method

.method private native nativeGetDataReductionProxyOrigin(J)Ljava/lang/String;
.end method

.method private native nativeInit()J
.end method

.method private native nativeIsDataReductionProxyAllowed(J)Z
.end method

.method private native nativeIsDataReductionProxyEnabled(J)Z
.end method

.method private native nativeIsDataReductionProxyManaged(J)Z
.end method

.method private native nativeIsDataReductionProxyPromoAllowed(J)Z
.end method

.method private native nativeIsDataReductionProxyUnreachable(J)Z
.end method

.method private native nativeIsIncludedInAltFieldTrial(J)Z
.end method

.method private native nativeSetDataReductionProxyEnabled(JZ)V
.end method


# virtual methods
.method public getContentLengthPercentSavings()Ljava/lang/String;
    .locals 8

    .prologue
    .line 153
    invoke-virtual {p0}, Lorg/chromium/chrome/browser/net/spdyproxy/DataReductionProxySettings;->getContentLengths()Lorg/chromium/chrome/browser/net/spdyproxy/DataReductionProxySettings$ContentLengths;

    move-result-object v2

    .line 155
    const-wide/16 v0, 0x0

    .line 156
    invoke-virtual {v2}, Lorg/chromium/chrome/browser/net/spdyproxy/DataReductionProxySettings$ContentLengths;->getOriginal()J

    move-result-wide v4

    const-wide/16 v6, 0x0

    cmp-long v3, v4, v6

    if-lez v3, :cond_0

    invoke-virtual {v2}, Lorg/chromium/chrome/browser/net/spdyproxy/DataReductionProxySettings$ContentLengths;->getOriginal()J

    move-result-wide v4

    invoke-virtual {v2}, Lorg/chromium/chrome/browser/net/spdyproxy/DataReductionProxySettings$ContentLengths;->getReceived()J

    move-result-wide v6

    cmp-long v3, v4, v6

    if-lez v3, :cond_0

    .line 157
    invoke-virtual {v2}, Lorg/chromium/chrome/browser/net/spdyproxy/DataReductionProxySettings$ContentLengths;->getOriginal()J

    move-result-wide v0

    invoke-virtual {v2}, Lorg/chromium/chrome/browser/net/spdyproxy/DataReductionProxySettings$ContentLengths;->getReceived()J

    move-result-wide v4

    sub-long/2addr v0, v4

    long-to-double v0, v0

    invoke-virtual {v2}, Lorg/chromium/chrome/browser/net/spdyproxy/DataReductionProxySettings$ContentLengths;->getOriginal()J

    move-result-wide v2

    long-to-double v2, v2

    div-double/2addr v0, v2

    .line 159
    :cond_0
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-static {v2}, Ljava/text/NumberFormat;->getPercentInstance(Ljava/util/Locale;)Ljava/text/NumberFormat;

    move-result-object v2

    .line 160
    invoke-virtual {v2, v0, v1}, Ljava/text/NumberFormat;->format(D)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getContentLengths()Lorg/chromium/chrome/browser/net/spdyproxy/DataReductionProxySettings$ContentLengths;
    .locals 2

    .prologue
    .line 120
    iget-wide v0, p0, Lorg/chromium/chrome/browser/net/spdyproxy/DataReductionProxySettings;->mNativeDataReductionProxySettings:J

    invoke-direct {p0, v0, v1}, Lorg/chromium/chrome/browser/net/spdyproxy/DataReductionProxySettings;->nativeGetContentLengths(J)Lorg/chromium/chrome/browser/net/spdyproxy/DataReductionProxySettings$ContentLengths;

    move-result-object v0

    return-object v0
.end method

.method public getDataReductionLastUpdateTime()J
    .locals 2

    .prologue
    .line 112
    iget-wide v0, p0, Lorg/chromium/chrome/browser/net/spdyproxy/DataReductionProxySettings;->mNativeDataReductionProxySettings:J

    invoke-direct {p0, v0, v1}, Lorg/chromium/chrome/browser/net/spdyproxy/DataReductionProxySettings;->nativeGetDataReductionLastUpdateTime(J)J

    move-result-wide v0

    return-wide v0
.end method

.method public getOriginalNetworkStatsHistory()[J
    .locals 2

    .prologue
    .line 129
    iget-wide v0, p0, Lorg/chromium/chrome/browser/net/spdyproxy/DataReductionProxySettings;->mNativeDataReductionProxySettings:J

    invoke-direct {p0, v0, v1}, Lorg/chromium/chrome/browser/net/spdyproxy/DataReductionProxySettings;->nativeGetDailyOriginalContentLengths(J)[J

    move-result-object v0

    return-object v0
.end method

.method public getReceivedNetworkStatsHistory()[J
    .locals 2

    .prologue
    .line 138
    iget-wide v0, p0, Lorg/chromium/chrome/browser/net/spdyproxy/DataReductionProxySettings;->mNativeDataReductionProxySettings:J

    invoke-direct {p0, v0, v1}, Lorg/chromium/chrome/browser/net/spdyproxy/DataReductionProxySettings;->nativeGetDailyReceivedContentLengths(J)[J

    move-result-object v0

    return-object v0
.end method

.method public isDataReductionProxyAllowed()Z
    .locals 2

    .prologue
    .line 69
    iget-wide v0, p0, Lorg/chromium/chrome/browser/net/spdyproxy/DataReductionProxySettings;->mNativeDataReductionProxySettings:J

    invoke-direct {p0, v0, v1}, Lorg/chromium/chrome/browser/net/spdyproxy/DataReductionProxySettings;->nativeIsDataReductionProxyAllowed(J)Z

    move-result v0

    return v0
.end method

.method public isDataReductionProxyEnabled()Z
    .locals 2

    .prologue
    .line 99
    iget-wide v0, p0, Lorg/chromium/chrome/browser/net/spdyproxy/DataReductionProxySettings;->mNativeDataReductionProxySettings:J

    invoke-direct {p0, v0, v1}, Lorg/chromium/chrome/browser/net/spdyproxy/DataReductionProxySettings;->nativeIsDataReductionProxyEnabled(J)Z

    move-result v0

    return v0
.end method

.method public isDataReductionProxyManaged()Z
    .locals 2

    .prologue
    .line 104
    iget-wide v0, p0, Lorg/chromium/chrome/browser/net/spdyproxy/DataReductionProxySettings;->mNativeDataReductionProxySettings:J

    invoke-direct {p0, v0, v1}, Lorg/chromium/chrome/browser/net/spdyproxy/DataReductionProxySettings;->nativeIsDataReductionProxyManaged(J)Z

    move-result v0

    return v0
.end method

.method public isDataReductionProxyPromoAllowed()Z
    .locals 2

    .prologue
    .line 74
    iget-wide v0, p0, Lorg/chromium/chrome/browser/net/spdyproxy/DataReductionProxySettings;->mNativeDataReductionProxySettings:J

    invoke-direct {p0, v0, v1}, Lorg/chromium/chrome/browser/net/spdyproxy/DataReductionProxySettings;->nativeIsDataReductionProxyPromoAllowed(J)Z

    move-result v0

    return v0
.end method

.method public isDataReductionProxyUnreachable()Z
    .locals 2

    .prologue
    .line 146
    iget-wide v0, p0, Lorg/chromium/chrome/browser/net/spdyproxy/DataReductionProxySettings;->mNativeDataReductionProxySettings:J

    invoke-direct {p0, v0, v1}, Lorg/chromium/chrome/browser/net/spdyproxy/DataReductionProxySettings;->nativeIsDataReductionProxyUnreachable(J)Z

    move-result v0

    return v0
.end method

.method public isIncludedInAltFieldTrial()Z
    .locals 2

    .prologue
    .line 79
    iget-wide v0, p0, Lorg/chromium/chrome/browser/net/spdyproxy/DataReductionProxySettings;->mNativeDataReductionProxySettings:J

    invoke-direct {p0, v0, v1}, Lorg/chromium/chrome/browser/net/spdyproxy/DataReductionProxySettings;->nativeIsIncludedInAltFieldTrial(J)Z

    move-result v0

    return v0
.end method

.method public setDataReductionProxyEnabled(Z)V
    .locals 2

    .prologue
    .line 94
    iget-wide v0, p0, Lorg/chromium/chrome/browser/net/spdyproxy/DataReductionProxySettings;->mNativeDataReductionProxySettings:J

    invoke-direct {p0, v0, v1, p1}, Lorg/chromium/chrome/browser/net/spdyproxy/DataReductionProxySettings;->nativeSetDataReductionProxyEnabled(JZ)V

    .line 95
    return-void
.end method
