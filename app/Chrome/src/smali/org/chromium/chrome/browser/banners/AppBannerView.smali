.class public Lorg/chromium/chrome/browser/banners/AppBannerView;
.super Lorg/chromium/chrome/browser/banners/SwipableOverlayView;
.source "AppBannerView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lorg/chromium/chrome/browser/banners/InstallerDelegate$Observer;
.implements Lorg/chromium/ui/base/WindowAndroid$IntentCallback;


# static fields
.field static final synthetic $assertionsDisabled:Z

.field private static final BANNER_LAYOUT:I


# instance fields
.field private mAppData:Lorg/chromium/chrome/browser/banners/AppData;

.field private final mBackgroundDrawablePadding:Landroid/graphics/Rect;

.field private mBannerHighlightView:Landroid/view/View;

.field private mCloseButtonView:Landroid/widget/ImageButton;

.field private mDefinedMaxWidth:I

.field private mIconView:Landroid/widget/ImageView;

.field private mInitialXForHighlight:F

.field private mInstallButtonView:Landroid/widget/Button;

.field private mInstallState:I

.field private mInstallTask:Lorg/chromium/chrome/browser/banners/InstallerDelegate;

.field private mIsBannerPressed:Z

.field private final mIsLayoutLTR:Z

.field private mLogoView:Landroid/view/View;

.field private mMarginBottom:I

.field private mMarginLeft:I

.field private mMarginRight:I

.field private mObserver:Lorg/chromium/chrome/browser/banners/AppBannerView$Observer;

.field private mPaddingCard:I

.field private mPaddingControls:I

.field private mRatingView:Lorg/chromium/chrome/browser/banners/RatingView;

.field private mTitleView:Landroid/widget/TextView;

.field private mTouchSlop:I

.field private mWasInstallDialogShown:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 65
    const-class v0, Lorg/chromium/chrome/browser/banners/AppBannerView;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/chromium/chrome/browser/banners/AppBannerView;->$assertionsDisabled:Z

    .line 113
    sget v0, Lorg/chromium/chrome/R$layout;->app_banner_view:I

    sput v0, Lorg/chromium/chrome/browser/banners/AppBannerView;->BANNER_LAYOUT:I

    return-void

    .line 65
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 175
    invoke-direct {p0, p1, p2}, Lorg/chromium/chrome/browser/banners/SwipableOverlayView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 176
    invoke-static {}, Lorg/chromium/ui/base/LocalizationUtils;->isLayoutRtl()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lorg/chromium/chrome/browser/banners/AppBannerView;->mIsLayoutLTR:Z

    .line 181
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lorg/chromium/chrome/browser/banners/AppBannerView;->mBackgroundDrawablePadding:Landroid/graphics/Rect;

    .line 182
    iget-object v0, p0, Lorg/chromium/chrome/browser/banners/AppBannerView;->mBackgroundDrawablePadding:Landroid/graphics/Rect;

    invoke-static {p0}, Lorg/chromium/base/ApiCompatibilityUtils;->getPaddingStart(Landroid/view/View;)I

    move-result v2

    iput v2, v0, Landroid/graphics/Rect;->left:I

    .line 183
    iget-object v0, p0, Lorg/chromium/chrome/browser/banners/AppBannerView;->mBackgroundDrawablePadding:Landroid/graphics/Rect;

    invoke-static {p0}, Lorg/chromium/base/ApiCompatibilityUtils;->getPaddingEnd(Landroid/view/View;)I

    move-result v2

    iput v2, v0, Landroid/graphics/Rect;->right:I

    .line 184
    iget-object v0, p0, Lorg/chromium/chrome/browser/banners/AppBannerView;->mBackgroundDrawablePadding:Landroid/graphics/Rect;

    invoke-virtual {p0}, Lorg/chromium/chrome/browser/banners/AppBannerView;->getPaddingTop()I

    move-result v2

    iput v2, v0, Landroid/graphics/Rect;->top:I

    .line 185
    iget-object v0, p0, Lorg/chromium/chrome/browser/banners/AppBannerView;->mBackgroundDrawablePadding:Landroid/graphics/Rect;

    invoke-virtual {p0}, Lorg/chromium/chrome/browser/banners/AppBannerView;->getPaddingBottom()I

    move-result v2

    iput v2, v0, Landroid/graphics/Rect;->bottom:I

    .line 187
    iput v1, p0, Lorg/chromium/chrome/browser/banners/AppBannerView;->mInstallState:I

    .line 188
    return-void

    :cond_0
    move v0, v1

    .line 176
    goto :goto_0
.end method

.method public static create(Lorg/chromium/content/browser/ContentViewCore;Lorg/chromium/chrome/browser/banners/AppBannerView$Observer;Lorg/chromium/chrome/browser/banners/AppData;)Lorg/chromium/chrome/browser/banners/AppBannerView;
    .locals 3

    .prologue
    .line 163
    invoke-virtual {p0}, Lorg/chromium/content/browser/ContentViewCore;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 164
    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Lorg/chromium/chrome/browser/banners/AppBannerView;->BANNER_LAYOUT:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lorg/chromium/chrome/browser/banners/AppBannerView;

    .line 166
    invoke-direct {v0, p1, p2}, Lorg/chromium/chrome/browser/banners/AppBannerView;->initialize(Lorg/chromium/chrome/browser/banners/AppBannerView$Observer;Lorg/chromium/chrome/browser/banners/AppData;)V

    .line 167
    invoke-virtual {v0, p0}, Lorg/chromium/chrome/browser/banners/AppBannerView;->addToView(Lorg/chromium/content/browser/ContentViewCore;)V

    .line 168
    return-object v0
.end method

.method private getAppLaunchIntent()Landroid/content/Intent;
    .locals 2

    .prologue
    .line 567
    iget-object v0, p0, Lorg/chromium/chrome/browser/banners/AppBannerView;->mAppData:Lorg/chromium/chrome/browser/banners/AppData;

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/banners/AppData;->packageName()Ljava/lang/String;

    move-result-object v0

    .line 568
    invoke-virtual {p0}, Lorg/chromium/chrome/browser/banners/AppBannerView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 569
    invoke-virtual {v1, v0}, Landroid/content/pm/PackageManager;->getLaunchIntentForPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method private static getHeightWithMargins(Landroid/view/View;)I
    .locals 2

    .prologue
    .line 855
    invoke-virtual {p0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    invoke-static {p0}, Lorg/chromium/chrome/browser/banners/AppBannerView;->getMarginHeight(Landroid/view/View;)I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method static getIconSize(Landroid/content/Context;)I
    .locals 2

    .prologue
    .line 478
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lorg/chromium/chrome/R$dimen;->app_banner_icon_size:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    return v0
.end method

.method private static getMarginHeight(Landroid/view/View;)I
    .locals 2

    .prologue
    .line 845
    invoke-virtual {p0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 846
    iget v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    add-int/2addr v0, v1

    return v0
.end method

.method private static getMarginWidth(Landroid/view/View;)I
    .locals 2

    .prologue
    .line 826
    invoke-virtual {p0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 827
    iget v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    add-int/2addr v0, v1

    return v0
.end method

.method private static getWidthWithMargins(Landroid/view/View;)I
    .locals 2

    .prologue
    .line 836
    invoke-virtual {p0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v0

    invoke-static {p0}, Lorg/chromium/chrome/browser/banners/AppBannerView;->getMarginWidth(Landroid/view/View;)I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method private initialize(Lorg/chromium/chrome/browser/banners/AppBannerView$Observer;Lorg/chromium/chrome/browser/banners/AppData;)V
    .locals 0

    .prologue
    .line 196
    iput-object p1, p0, Lorg/chromium/chrome/browser/banners/AppBannerView;->mObserver:Lorg/chromium/chrome/browser/banners/AppBannerView$Observer;

    .line 197
    iput-object p2, p0, Lorg/chromium/chrome/browser/banners/AppBannerView;->mAppData:Lorg/chromium/chrome/browser/banners/AppData;

    .line 198
    invoke-direct {p0}, Lorg/chromium/chrome/browser/banners/AppBannerView;->initializeControls()V

    .line 199
    return-void
.end method

.method private initializeControls()V
    .locals 3

    .prologue
    .line 204
    invoke-virtual {p0}, Lorg/chromium/chrome/browser/banners/AppBannerView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 205
    sget v1, Lorg/chromium/chrome/R$dimen;->app_banner_max_width:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lorg/chromium/chrome/browser/banners/AppBannerView;->mDefinedMaxWidth:I

    .line 206
    sget v1, Lorg/chromium/chrome/R$dimen;->app_banner_padding:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lorg/chromium/chrome/browser/banners/AppBannerView;->mPaddingCard:I

    .line 207
    sget v1, Lorg/chromium/chrome/R$dimen;->app_banner_padding_controls:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lorg/chromium/chrome/browser/banners/AppBannerView;->mPaddingControls:I

    .line 208
    sget v1, Lorg/chromium/chrome/R$dimen;->app_banner_margin_sides:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iget-object v2, p0, Lorg/chromium/chrome/browser/banners/AppBannerView;->mBackgroundDrawablePadding:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->left:I

    sub-int/2addr v1, v2

    iput v1, p0, Lorg/chromium/chrome/browser/banners/AppBannerView;->mMarginLeft:I

    .line 210
    sget v1, Lorg/chromium/chrome/R$dimen;->app_banner_margin_sides:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iget-object v2, p0, Lorg/chromium/chrome/browser/banners/AppBannerView;->mBackgroundDrawablePadding:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->right:I

    sub-int/2addr v1, v2

    iput v1, p0, Lorg/chromium/chrome/browser/banners/AppBannerView;->mMarginRight:I

    .line 212
    sget v1, Lorg/chromium/chrome/R$dimen;->app_banner_margin_bottom:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iget-object v1, p0, Lorg/chromium/chrome/browser/banners/AppBannerView;->mBackgroundDrawablePadding:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->bottom:I

    sub-int/2addr v0, v1

    iput v0, p0, Lorg/chromium/chrome/browser/banners/AppBannerView;->mMarginBottom:I

    .line 214
    invoke-virtual {p0}, Lorg/chromium/chrome/browser/banners/AppBannerView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 215
    invoke-virtual {p0}, Lorg/chromium/chrome/browser/banners/AppBannerView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 216
    iget v1, p0, Lorg/chromium/chrome/browser/banners/AppBannerView;->mMarginLeft:I

    iput v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    .line 217
    iget v1, p0, Lorg/chromium/chrome/browser/banners/AppBannerView;->mMarginRight:I

    iput v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    .line 218
    iget v1, p0, Lorg/chromium/chrome/browser/banners/AppBannerView;->mMarginBottom:I

    iput v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    .line 222
    :cond_0
    sget v0, Lorg/chromium/chrome/R$id;->app_icon:I

    invoke-virtual {p0, v0}, Lorg/chromium/chrome/browser/banners/AppBannerView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lorg/chromium/chrome/browser/banners/AppBannerView;->mIconView:Landroid/widget/ImageView;

    .line 223
    sget v0, Lorg/chromium/chrome/R$id;->app_title:I

    invoke-virtual {p0, v0}, Lorg/chromium/chrome/browser/banners/AppBannerView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lorg/chromium/chrome/browser/banners/AppBannerView;->mTitleView:Landroid/widget/TextView;

    .line 224
    sget v0, Lorg/chromium/chrome/R$id;->app_install_button:I

    invoke-virtual {p0, v0}, Lorg/chromium/chrome/browser/banners/AppBannerView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lorg/chromium/chrome/browser/banners/AppBannerView;->mInstallButtonView:Landroid/widget/Button;

    .line 225
    sget v0, Lorg/chromium/chrome/R$id;->app_rating:I

    invoke-virtual {p0, v0}, Lorg/chromium/chrome/browser/banners/AppBannerView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lorg/chromium/chrome/browser/banners/RatingView;

    iput-object v0, p0, Lorg/chromium/chrome/browser/banners/AppBannerView;->mRatingView:Lorg/chromium/chrome/browser/banners/RatingView;

    .line 226
    sget v0, Lorg/chromium/chrome/R$id;->store_logo:I

    invoke-virtual {p0, v0}, Lorg/chromium/chrome/browser/banners/AppBannerView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lorg/chromium/chrome/browser/banners/AppBannerView;->mLogoView:Landroid/view/View;

    .line 227
    sget v0, Lorg/chromium/chrome/R$id;->banner_highlight:I

    invoke-virtual {p0, v0}, Lorg/chromium/chrome/browser/banners/AppBannerView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lorg/chromium/chrome/browser/banners/AppBannerView;->mBannerHighlightView:Landroid/view/View;

    .line 228
    sget v0, Lorg/chromium/chrome/R$id;->close_button:I

    invoke-virtual {p0, v0}, Lorg/chromium/chrome/browser/banners/AppBannerView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lorg/chromium/chrome/browser/banners/AppBannerView;->mCloseButtonView:Landroid/widget/ImageButton;

    .line 230
    sget-boolean v0, Lorg/chromium/chrome/browser/banners/AppBannerView;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lorg/chromium/chrome/browser/banners/AppBannerView;->mIconView:Landroid/widget/ImageView;

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 231
    :cond_1
    sget-boolean v0, Lorg/chromium/chrome/browser/banners/AppBannerView;->$assertionsDisabled:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lorg/chromium/chrome/browser/banners/AppBannerView;->mTitleView:Landroid/widget/TextView;

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 232
    :cond_2
    sget-boolean v0, Lorg/chromium/chrome/browser/banners/AppBannerView;->$assertionsDisabled:Z

    if-nez v0, :cond_3

    iget-object v0, p0, Lorg/chromium/chrome/browser/banners/AppBannerView;->mInstallButtonView:Landroid/widget/Button;

    if-nez v0, :cond_3

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 233
    :cond_3
    sget-boolean v0, Lorg/chromium/chrome/browser/banners/AppBannerView;->$assertionsDisabled:Z

    if-nez v0, :cond_4

    iget-object v0, p0, Lorg/chromium/chrome/browser/banners/AppBannerView;->mLogoView:Landroid/view/View;

    if-nez v0, :cond_4

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 234
    :cond_4
    sget-boolean v0, Lorg/chromium/chrome/browser/banners/AppBannerView;->$assertionsDisabled:Z

    if-nez v0, :cond_5

    iget-object v0, p0, Lorg/chromium/chrome/browser/banners/AppBannerView;->mRatingView:Lorg/chromium/chrome/browser/banners/RatingView;

    if-nez v0, :cond_5

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 235
    :cond_5
    sget-boolean v0, Lorg/chromium/chrome/browser/banners/AppBannerView;->$assertionsDisabled:Z

    if-nez v0, :cond_6

    iget-object v0, p0, Lorg/chromium/chrome/browser/banners/AppBannerView;->mBannerHighlightView:Landroid/view/View;

    if-nez v0, :cond_6

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 236
    :cond_6
    sget-boolean v0, Lorg/chromium/chrome/browser/banners/AppBannerView;->$assertionsDisabled:Z

    if-nez v0, :cond_7

    iget-object v0, p0, Lorg/chromium/chrome/browser/banners/AppBannerView;->mCloseButtonView:Landroid/widget/ImageButton;

    if-nez v0, :cond_7

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 239
    :cond_7
    iget-object v0, p0, Lorg/chromium/chrome/browser/banners/AppBannerView;->mInstallButtonView:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 240
    iget-object v0, p0, Lorg/chromium/chrome/browser/banners/AppBannerView;->mCloseButtonView:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 243
    iget-object v0, p0, Lorg/chromium/chrome/browser/banners/AppBannerView;->mTitleView:Landroid/widget/TextView;

    iget-object v1, p0, Lorg/chromium/chrome/browser/banners/AppBannerView;->mAppData:Lorg/chromium/chrome/browser/banners/AppData;

    invoke-virtual {v1}, Lorg/chromium/chrome/browser/banners/AppData;->title()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 244
    iget-object v0, p0, Lorg/chromium/chrome/browser/banners/AppBannerView;->mIconView:Landroid/widget/ImageView;

    iget-object v1, p0, Lorg/chromium/chrome/browser/banners/AppBannerView;->mAppData:Lorg/chromium/chrome/browser/banners/AppData;

    invoke-virtual {v1}, Lorg/chromium/chrome/browser/banners/AppData;->icon()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 245
    iget-object v0, p0, Lorg/chromium/chrome/browser/banners/AppBannerView;->mRatingView:Lorg/chromium/chrome/browser/banners/RatingView;

    iget-object v1, p0, Lorg/chromium/chrome/browser/banners/AppBannerView;->mAppData:Lorg/chromium/chrome/browser/banners/AppData;

    invoke-virtual {v1}, Lorg/chromium/chrome/browser/banners/AppData;->rating()F

    move-result v1

    invoke-virtual {v0, v1}, Lorg/chromium/chrome/browser/banners/RatingView;->initialize(F)V

    .line 246
    invoke-direct {p0}, Lorg/chromium/chrome/browser/banners/AppBannerView;->setAccessibilityInformation()V

    .line 249
    invoke-virtual {p0}, Lorg/chromium/chrome/browser/banners/AppBannerView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v0

    iput v0, p0, Lorg/chromium/chrome/browser/banners/AppBannerView;->mTouchSlop:I

    .line 252
    invoke-virtual {p0}, Lorg/chromium/chrome/browser/banners/AppBannerView;->updateButtonStatus()V

    .line 253
    return-void
.end method

.method private measureChildForSpace(Landroid/view/View;II)V
    .locals 5

    .prologue
    const/high16 v4, -0x80000000

    .line 794
    invoke-static {p1}, Lorg/chromium/chrome/browser/banners/AppBannerView;->getMarginWidth(Landroid/view/View;)I

    move-result v0

    sub-int v1, p2, v0

    .line 795
    invoke-static {p1}, Lorg/chromium/chrome/browser/banners/AppBannerView;->getMarginHeight(Landroid/view/View;)I

    move-result v0

    sub-int v0, p3, v0

    .line 798
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    iget v2, v2, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 799
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    iget v3, v3, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 800
    if-ltz v2, :cond_0

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 801
    :cond_0
    if-ltz v3, :cond_1

    invoke-static {v0, v3}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 803
    :cond_1
    invoke-static {v1, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 804
    invoke-static {v0, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    .line 805
    invoke-virtual {p1, v1, v0}, Landroid/view/View;->measure(II)V

    .line 806
    return-void
.end method

.method private measureChildForSpaceExactly(Landroid/view/View;II)V
    .locals 2

    .prologue
    const/high16 v1, 0x40000000    # 2.0f

    .line 815
    invoke-static {p2, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    .line 816
    invoke-static {p3, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 817
    invoke-virtual {p1, v0, v1}, Landroid/view/View;->measure(II)V

    .line 818
    return-void
.end method

.method private setAccessibilityInformation()V
    .locals 5

    .prologue
    .line 259
    invoke-virtual {p0}, Lorg/chromium/chrome/browser/banners/AppBannerView;->getContext()Landroid/content/Context;

    move-result-object v0

    sget v1, Lorg/chromium/chrome/R$string;->app_banner_view_accessibility:I

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lorg/chromium/chrome/browser/banners/AppBannerView;->mAppData:Lorg/chromium/chrome/browser/banners/AppData;

    invoke-virtual {v4}, Lorg/chromium/chrome/browser/banners/AppData;->title()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget-object v4, p0, Lorg/chromium/chrome/browser/banners/AppBannerView;->mAppData:Lorg/chromium/chrome/browser/banners/AppData;

    invoke-virtual {v4}, Lorg/chromium/chrome/browser/banners/AppData;->rating()F

    move-result v4

    invoke-static {v4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 261
    invoke-virtual {p0, v0}, Lorg/chromium/chrome/browser/banners/AppBannerView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 262
    return-void
.end method


# virtual methods
.method protected createLayoutParams()Landroid/view/ViewGroup$MarginLayoutParams;
    .locals 5

    .prologue
    .line 385
    invoke-super {p0}, Lorg/chromium/chrome/browser/banners/SwipableOverlayView;->createLayoutParams()Landroid/view/ViewGroup$MarginLayoutParams;

    move-result-object v0

    .line 386
    iget v1, p0, Lorg/chromium/chrome/browser/banners/AppBannerView;->mMarginLeft:I

    const/4 v2, 0x0

    iget v3, p0, Lorg/chromium/chrome/browser/banners/AppBannerView;->mMarginRight:I

    iget v4, p0, Lorg/chromium/chrome/browser/banners/AppBannerView;->mMarginBottom:I

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    .line 387
    return-object v0
.end method

.method public destroy()V
    .locals 1

    .prologue
    .line 420
    invoke-virtual {p0}, Lorg/chromium/chrome/browser/banners/AppBannerView;->isDismissed()Z

    move-result v0

    if-nez v0, :cond_0

    const/16 v0, 0x29

    invoke-virtual {p0, v0}, Lorg/chromium/chrome/browser/banners/AppBannerView;->dismiss(I)V

    .line 422
    :cond_0
    iget-object v0, p0, Lorg/chromium/chrome/browser/banners/AppBannerView;->mInstallTask:Lorg/chromium/chrome/browser/banners/InstallerDelegate;

    if-eqz v0, :cond_1

    .line 423
    iget-object v0, p0, Lorg/chromium/chrome/browser/banners/AppBannerView;->mInstallTask:Lorg/chromium/chrome/browser/banners/InstallerDelegate;

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/banners/InstallerDelegate;->cancel()V

    .line 424
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/chromium/chrome/browser/banners/AppBannerView;->mInstallTask:Lorg/chromium/chrome/browser/banners/InstallerDelegate;

    .line 426
    :cond_1
    return-void
.end method

.method public dismiss(I)V
    .locals 1

    .prologue
    .line 410
    invoke-virtual {p0}, Lorg/chromium/chrome/browser/banners/AppBannerView;->isDismissed()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lorg/chromium/chrome/browser/banners/AppBannerView;->mObserver:Lorg/chromium/chrome/browser/banners/AppBannerView$Observer;

    if-nez v0, :cond_1

    .line 414
    :cond_0
    :goto_0
    return-void

    .line 412
    :cond_1
    const/16 v0, 0x2d

    if-ne p1, v0, :cond_2

    const/4 v0, 0x1

    :goto_1
    invoke-virtual {p0, v0}, Lorg/chromium/chrome/browser/banners/AppBannerView;->dismiss(Z)Z

    .line 413
    iget-object v0, p0, Lorg/chromium/chrome/browser/banners/AppBannerView;->mObserver:Lorg/chromium/chrome/browser/banners/AppBannerView$Observer;

    invoke-interface {v0, p0, p1}, Lorg/chromium/chrome/browser/banners/AppBannerView$Observer;->onBannerDismissEvent(Lorg/chromium/chrome/browser/banners/AppBannerView;I)V

    goto :goto_0

    .line 412
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method protected onAttachedToWindow()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 506
    invoke-super {p0}, Lorg/chromium/chrome/browser/banners/SwipableOverlayView;->onAttachedToWindow()V

    .line 507
    const-string/jumbo v0, "alpha"

    const/4 v1, 0x2

    new-array v1, v1, [F

    invoke-virtual {p0}, Lorg/chromium/chrome/browser/banners/AppBannerView;->getAlpha()F

    move-result v2

    aput v2, v1, v4

    const/4 v2, 0x1

    const/high16 v3, 0x3f800000    # 1.0f

    aput v3, v1, v2

    invoke-static {p0, v0, v1}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    const-wide/16 v2, 0xfa

    invoke-virtual {v0, v2, v3}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    .line 509
    invoke-virtual {p0, v4}, Lorg/chromium/chrome/browser/banners/AppBannerView;->setVisibility(I)V

    .line 510
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    const v1, 0x3a83126f    # 0.001f

    .line 266
    iget-object v0, p0, Lorg/chromium/chrome/browser/banners/AppBannerView;->mObserver:Lorg/chromium/chrome/browser/banners/AppBannerView$Observer;

    if-nez v0, :cond_1

    .line 317
    :cond_0
    :goto_0
    return-void

    .line 269
    :cond_1
    invoke-virtual {p0}, Lorg/chromium/chrome/browser/banners/AppBannerView;->getTranslationX()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    cmpl-float v0, v0, v1

    if-gtz v0, :cond_0

    invoke-virtual {p0}, Lorg/chromium/chrome/browser/banners/AppBannerView;->getTranslationY()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    cmpl-float v0, v0, v1

    if-gtz v0, :cond_0

    .line 274
    iget-object v0, p0, Lorg/chromium/chrome/browser/banners/AppBannerView;->mInstallButtonView:Landroid/widget/Button;

    if-ne p1, v0, :cond_6

    .line 276
    iget v0, p0, Lorg/chromium/chrome/browser/banners/AppBannerView;->mInstallState:I

    .line 277
    invoke-virtual {p0}, Lorg/chromium/chrome/browser/banners/AppBannerView;->updateButtonStatus()V

    .line 278
    iget v1, p0, Lorg/chromium/chrome/browser/banners/AppBannerView;->mInstallState:I

    if-ne v1, v0, :cond_0

    .line 281
    iget v0, p0, Lorg/chromium/chrome/browser/banners/AppBannerView;->mInstallState:I

    if-eq v0, v3, :cond_0

    .line 283
    iget-object v0, p0, Lorg/chromium/chrome/browser/banners/AppBannerView;->mInstallButtonView:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setEnabled(Z)V

    .line 285
    iget v0, p0, Lorg/chromium/chrome/browser/banners/AppBannerView;->mInstallState:I

    if-nez v0, :cond_4

    .line 287
    iget-boolean v0, p0, Lorg/chromium/chrome/browser/banners/AppBannerView;->mWasInstallDialogShown:Z

    if-nez v0, :cond_2

    .line 288
    iget-object v0, p0, Lorg/chromium/chrome/browser/banners/AppBannerView;->mObserver:Lorg/chromium/chrome/browser/banners/AppBannerView$Observer;

    const/16 v1, 0x15

    invoke-interface {v0, p0, v1}, Lorg/chromium/chrome/browser/banners/AppBannerView$Observer;->onBannerInstallEvent(Lorg/chromium/chrome/browser/banners/AppBannerView;I)V

    .line 289
    iput-boolean v3, p0, Lorg/chromium/chrome/browser/banners/AppBannerView;->mWasInstallDialogShown:Z

    .line 292
    :cond_2
    iget-object v0, p0, Lorg/chromium/chrome/browser/banners/AppBannerView;->mObserver:Lorg/chromium/chrome/browser/banners/AppBannerView$Observer;

    iget-object v1, p0, Lorg/chromium/chrome/browser/banners/AppBannerView;->mAppData:Lorg/chromium/chrome/browser/banners/AppData;

    invoke-virtual {v1}, Lorg/chromium/chrome/browser/banners/AppData;->installIntent()Landroid/app/PendingIntent;

    move-result-object v1

    invoke-interface {v0, p0, v1}, Lorg/chromium/chrome/browser/banners/AppBannerView$Observer;->onFireIntent(Lorg/chromium/chrome/browser/banners/AppBannerView;Landroid/app/PendingIntent;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 294
    invoke-virtual {p0, v2}, Lorg/chromium/chrome/browser/banners/AppBannerView;->createVerticalSnapAnimation(Z)V

    goto :goto_0

    .line 296
    :cond_3
    const-string/jumbo v0, "AppBannerView"

    const-string/jumbo v1, "Failed to fire install intent."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 297
    const/16 v0, 0x29

    invoke-virtual {p0, v0}, Lorg/chromium/chrome/browser/banners/AppBannerView;->dismiss(I)V

    goto :goto_0

    .line 299
    :cond_4
    iget v0, p0, Lorg/chromium/chrome/browser/banners/AppBannerView;->mInstallState:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 302
    :try_start_0
    invoke-direct {p0}, Lorg/chromium/chrome/browser/banners/AppBannerView;->getAppLaunchIntent()Landroid/content/Intent;

    move-result-object v0

    .line 303
    if-eqz v0, :cond_5

    invoke-virtual {p0}, Lorg/chromium/chrome/browser/banners/AppBannerView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 308
    :cond_5
    :goto_1
    const/16 v0, 0x2a

    invoke-virtual {p0, v0}, Lorg/chromium/chrome/browser/banners/AppBannerView;->dismiss(I)V

    goto :goto_0

    .line 304
    :catch_0
    move-exception v0

    .line 305
    const-string/jumbo v0, "AppBannerView"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "Failed to find app package: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lorg/chromium/chrome/browser/banners/AppBannerView;->mAppData:Lorg/chromium/chrome/browser/banners/AppData;

    invoke-virtual {v2}, Lorg/chromium/chrome/browser/banners/AppData;->packageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 310
    :cond_6
    iget-object v0, p0, Lorg/chromium/chrome/browser/banners/AppBannerView;->mCloseButtonView:Landroid/widget/ImageButton;

    if-ne p1, v0, :cond_0

    .line 311
    iget-object v0, p0, Lorg/chromium/chrome/browser/banners/AppBannerView;->mObserver:Lorg/chromium/chrome/browser/banners/AppBannerView$Observer;

    if-eqz v0, :cond_7

    .line 312
    iget-object v0, p0, Lorg/chromium/chrome/browser/banners/AppBannerView;->mObserver:Lorg/chromium/chrome/browser/banners/AppBannerView$Observer;

    iget-object v1, p0, Lorg/chromium/chrome/browser/banners/AppBannerView;->mAppData:Lorg/chromium/chrome/browser/banners/AppData;

    invoke-virtual {v1}, Lorg/chromium/chrome/browser/banners/AppData;->siteUrl()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lorg/chromium/chrome/browser/banners/AppBannerView;->mAppData:Lorg/chromium/chrome/browser/banners/AppData;

    invoke-virtual {v2}, Lorg/chromium/chrome/browser/banners/AppData;->packageName()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, p0, v1, v2}, Lorg/chromium/chrome/browser/banners/AppBannerView$Observer;->onBannerBlocked(Lorg/chromium/chrome/browser/banners/AppBannerView;Ljava/lang/String;Ljava/lang/String;)V

    .line 315
    :cond_7
    const/16 v0, 0x2d

    invoke-virtual {p0, v0}, Lorg/chromium/chrome/browser/banners/AppBannerView;->dismiss(I)V

    goto/16 :goto_0
.end method

.method protected onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 529
    invoke-super {p0, p1}, Lorg/chromium/chrome/browser/banners/SwipableOverlayView;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 531
    invoke-virtual {p0}, Lorg/chromium/chrome/browser/banners/AppBannerView;->isDismissed()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 556
    :cond_0
    :goto_0
    return-void

    .line 534
    :cond_1
    invoke-virtual {p0}, Lorg/chromium/chrome/browser/banners/AppBannerView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lorg/chromium/chrome/R$dimen;->app_banner_max_width:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 535
    iget v1, p0, Lorg/chromium/chrome/browser/banners/AppBannerView;->mDefinedMaxWidth:I

    if-eq v1, v0, :cond_0

    .line 539
    :goto_1
    invoke-virtual {p0}, Lorg/chromium/chrome/browser/banners/AppBannerView;->getChildCount()I

    move-result v0

    if-lez v0, :cond_2

    invoke-virtual {p0, v3}, Lorg/chromium/chrome/browser/banners/AppBannerView;->removeViewAt(I)V

    goto :goto_1

    .line 540
    :cond_2
    iput-object v2, p0, Lorg/chromium/chrome/browser/banners/AppBannerView;->mIconView:Landroid/widget/ImageView;

    .line 541
    iput-object v2, p0, Lorg/chromium/chrome/browser/banners/AppBannerView;->mTitleView:Landroid/widget/TextView;

    .line 542
    iput-object v2, p0, Lorg/chromium/chrome/browser/banners/AppBannerView;->mInstallButtonView:Landroid/widget/Button;

    .line 543
    iput-object v2, p0, Lorg/chromium/chrome/browser/banners/AppBannerView;->mRatingView:Lorg/chromium/chrome/browser/banners/RatingView;

    .line 544
    iput-object v2, p0, Lorg/chromium/chrome/browser/banners/AppBannerView;->mLogoView:Landroid/view/View;

    .line 545
    iput-object v2, p0, Lorg/chromium/chrome/browser/banners/AppBannerView;->mBannerHighlightView:Landroid/view/View;

    .line 547
    invoke-virtual {p0}, Lorg/chromium/chrome/browser/banners/AppBannerView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Lorg/chromium/chrome/browser/banners/AppBannerView;->BANNER_LAYOUT:I

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lorg/chromium/chrome/browser/banners/AppBannerView;

    .line 549
    :goto_2
    invoke-virtual {v0}, Lorg/chromium/chrome/browser/banners/AppBannerView;->getChildCount()I

    move-result v1

    if-lez v1, :cond_3

    .line 550
    invoke-virtual {v0, v3}, Lorg/chromium/chrome/browser/banners/AppBannerView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 551
    invoke-virtual {v0, v3}, Lorg/chromium/chrome/browser/banners/AppBannerView;->removeViewAt(I)V

    .line 552
    invoke-virtual {p0, v1}, Lorg/chromium/chrome/browser/banners/AppBannerView;->addView(Landroid/view/View;)V

    goto :goto_2

    .line 554
    :cond_3
    invoke-direct {p0}, Lorg/chromium/chrome/browser/banners/AppBannerView;->initializeControls()V

    .line 555
    invoke-virtual {p0}, Lorg/chromium/chrome/browser/banners/AppBannerView;->requestLayout()V

    goto :goto_0
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .prologue
    .line 517
    invoke-super {p0}, Lorg/chromium/chrome/browser/banners/SwipableOverlayView;->onDetachedFromWindow()V

    .line 518
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lorg/chromium/chrome/browser/banners/AppBannerView;->setAlpha(F)V

    .line 519
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lorg/chromium/chrome/browser/banners/AppBannerView;->setVisibility(I)V

    .line 520
    return-void
.end method

.method public onInstallFinished(Lorg/chromium/chrome/browser/banners/InstallerDelegate;Z)V
    .locals 2

    .prologue
    .line 370
    invoke-virtual {p0}, Lorg/chromium/chrome/browser/banners/AppBannerView;->isDismissed()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lorg/chromium/chrome/browser/banners/AppBannerView;->mInstallTask:Lorg/chromium/chrome/browser/banners/InstallerDelegate;

    if-eq v0, p1, :cond_1

    .line 380
    :cond_0
    :goto_0
    return-void

    .line 372
    :cond_1
    if-eqz p2, :cond_2

    .line 374
    iget-object v0, p0, Lorg/chromium/chrome/browser/banners/AppBannerView;->mObserver:Lorg/chromium/chrome/browser/banners/AppBannerView$Observer;

    const/16 v1, 0x17

    invoke-interface {v0, p0, v1}, Lorg/chromium/chrome/browser/banners/AppBannerView$Observer;->onBannerInstallEvent(Lorg/chromium/chrome/browser/banners/AppBannerView;I)V

    .line 375
    const/4 v0, 0x2

    iput v0, p0, Lorg/chromium/chrome/browser/banners/AppBannerView;->mInstallState:I

    .line 376
    invoke-virtual {p0}, Lorg/chromium/chrome/browser/banners/AppBannerView;->updateButtonStatus()V

    goto :goto_0

    .line 378
    :cond_2
    const/16 v0, 0x2e

    invoke-virtual {p0, v0}, Lorg/chromium/chrome/browser/banners/AppBannerView;->dismiss(I)V

    goto :goto_0
.end method

.method public onIntentCompleted(Lorg/chromium/ui/base/WindowAndroid;ILandroid/content/ContentResolver;Landroid/content/Intent;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 350
    invoke-virtual {p0}, Lorg/chromium/chrome/browser/banners/AppBannerView;->isDismissed()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 365
    :goto_0
    return-void

    .line 352
    :cond_0
    invoke-virtual {p0, v4}, Lorg/chromium/chrome/browser/banners/AppBannerView;->createVerticalSnapAnimation(Z)V

    .line 353
    const/4 v0, -0x1

    if-ne p2, v0, :cond_1

    .line 356
    iget-object v0, p0, Lorg/chromium/chrome/browser/banners/AppBannerView;->mObserver:Lorg/chromium/chrome/browser/banners/AppBannerView$Observer;

    const/16 v1, 0x16

    invoke-interface {v0, p0, v1}, Lorg/chromium/chrome/browser/banners/AppBannerView$Observer;->onBannerInstallEvent(Lorg/chromium/chrome/browser/banners/AppBannerView;I)V

    .line 358
    invoke-virtual {p0}, Lorg/chromium/chrome/browser/banners/AppBannerView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 359
    new-instance v1, Lorg/chromium/chrome/browser/banners/InstallerDelegate;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    iget-object v3, p0, Lorg/chromium/chrome/browser/banners/AppBannerView;->mAppData:Lorg/chromium/chrome/browser/banners/AppData;

    invoke-virtual {v3}, Lorg/chromium/chrome/browser/banners/AppData;->packageName()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v0, p0, v3}, Lorg/chromium/chrome/browser/banners/InstallerDelegate;-><init>(Landroid/os/Looper;Landroid/content/pm/PackageManager;Lorg/chromium/chrome/browser/banners/InstallerDelegate$Observer;Ljava/lang/String;)V

    iput-object v1, p0, Lorg/chromium/chrome/browser/banners/AppBannerView;->mInstallTask:Lorg/chromium/chrome/browser/banners/InstallerDelegate;

    .line 361
    iget-object v0, p0, Lorg/chromium/chrome/browser/banners/AppBannerView;->mInstallTask:Lorg/chromium/chrome/browser/banners/InstallerDelegate;

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/banners/InstallerDelegate;->start()V

    .line 362
    iput v4, p0, Lorg/chromium/chrome/browser/banners/AppBannerView;->mInstallState:I

    .line 364
    :cond_1
    invoke-virtual {p0}, Lorg/chromium/chrome/browser/banners/AppBannerView;->updateButtonStatus()V

    goto :goto_0
.end method

.method protected onLayout(ZIIII)V
    .locals 9

    .prologue
    .line 715
    invoke-super/range {p0 .. p5}, Lorg/chromium/chrome/browser/banners/SwipableOverlayView;->onLayout(ZIIII)V

    .line 716
    iget-object v0, p0, Lorg/chromium/chrome/browser/banners/AppBannerView;->mBackgroundDrawablePadding:Landroid/graphics/Rect;

    iget v2, v0, Landroid/graphics/Rect;->top:I

    .line 717
    invoke-virtual {p0}, Lorg/chromium/chrome/browser/banners/AppBannerView;->getMeasuredHeight()I

    move-result v0

    iget-object v1, p0, Lorg/chromium/chrome/browser/banners/AppBannerView;->mBackgroundDrawablePadding:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->bottom:I

    sub-int v3, v0, v1

    .line 718
    iget-object v0, p0, Lorg/chromium/chrome/browser/banners/AppBannerView;->mBackgroundDrawablePadding:Landroid/graphics/Rect;

    iget v4, v0, Landroid/graphics/Rect;->left:I

    .line 719
    invoke-virtual {p0}, Lorg/chromium/chrome/browser/banners/AppBannerView;->getMeasuredWidth()I

    move-result v0

    iget-object v1, p0, Lorg/chromium/chrome/browser/banners/AppBannerView;->mBackgroundDrawablePadding:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->right:I

    sub-int v1, v0, v1

    .line 722
    iget-object v0, p0, Lorg/chromium/chrome/browser/banners/AppBannerView;->mBannerHighlightView:Landroid/view/View;

    invoke-virtual {v0, v4, v2, v1, v3}, Landroid/view/View;->layout(IIII)V

    .line 726
    iget-object v0, p0, Lorg/chromium/chrome/browser/banners/AppBannerView;->mCloseButtonView:Landroid/widget/ImageButton;

    invoke-virtual {v0}, Landroid/widget/ImageButton;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 727
    iget-object v0, p0, Lorg/chromium/chrome/browser/banners/AppBannerView;->mCloseButtonView:Landroid/widget/ImageButton;

    invoke-virtual {v0}, Landroid/widget/ImageButton;->getMeasuredWidth()I

    move-result v5

    .line 728
    iget-object v0, p0, Lorg/chromium/chrome/browser/banners/AppBannerView;->mCloseButtonView:Landroid/widget/ImageButton;

    invoke-virtual {v0}, Landroid/widget/ImageButton;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    add-int v6, v2, v0

    .line 730
    iget-object v0, p0, Lorg/chromium/chrome/browser/banners/AppBannerView;->mCloseButtonView:Landroid/widget/ImageButton;

    invoke-virtual {v0}, Landroid/widget/ImageButton;->getMeasuredHeight()I

    move-result v0

    add-int v7, v6, v0

    .line 731
    iget-boolean v0, p0, Lorg/chromium/chrome/browser/banners/AppBannerView;->mIsLayoutLTR:Z

    if-eqz v0, :cond_1

    move v0, v1

    .line 732
    :goto_0
    sub-int v5, v0, v5

    .line 733
    iget-object v8, p0, Lorg/chromium/chrome/browser/banners/AppBannerView;->mCloseButtonView:Landroid/widget/ImageButton;

    invoke-virtual {v8, v5, v6, v0, v7}, Landroid/widget/ImageButton;->layout(IIII)V

    .line 737
    :cond_0
    iget v0, p0, Lorg/chromium/chrome/browser/banners/AppBannerView;->mPaddingCard:I

    add-int v5, v2, v0

    .line 738
    iget v0, p0, Lorg/chromium/chrome/browser/banners/AppBannerView;->mPaddingCard:I

    sub-int/2addr v3, v0

    .line 739
    iget v0, p0, Lorg/chromium/chrome/browser/banners/AppBannerView;->mPaddingCard:I

    add-int v2, v4, v0

    .line 740
    iget v0, p0, Lorg/chromium/chrome/browser/banners/AppBannerView;->mPaddingCard:I

    sub-int v4, v1, v0

    .line 743
    iget-object v0, p0, Lorg/chromium/chrome/browser/banners/AppBannerView;->mIconView:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getMeasuredWidth()I

    move-result v1

    .line 744
    iget-boolean v0, p0, Lorg/chromium/chrome/browser/banners/AppBannerView;->mIsLayoutLTR:Z

    if-eqz v0, :cond_2

    move v0, v2

    .line 745
    :goto_1
    iget-object v6, p0, Lorg/chromium/chrome/browser/banners/AppBannerView;->mIconView:Landroid/widget/ImageView;

    add-int/2addr v1, v0

    iget-object v7, p0, Lorg/chromium/chrome/browser/banners/AppBannerView;->mIconView:Landroid/widget/ImageView;

    invoke-virtual {v7}, Landroid/widget/ImageView;->getMeasuredHeight()I

    move-result v7

    add-int/2addr v7, v5

    invoke-virtual {v6, v0, v5, v1, v7}, Landroid/widget/ImageView;->layout(IIII)V

    .line 746
    iget-object v0, p0, Lorg/chromium/chrome/browser/banners/AppBannerView;->mIconView:Landroid/widget/ImageView;

    invoke-static {v0}, Lorg/chromium/chrome/browser/banners/AppBannerView;->getWidthWithMargins(Landroid/view/View;)I

    move-result v0

    add-int v1, v2, v0

    .line 749
    iget v0, p0, Lorg/chromium/chrome/browser/banners/AppBannerView;->mPaddingControls:I

    sub-int v2, v4, v0

    .line 750
    iget v0, p0, Lorg/chromium/chrome/browser/banners/AppBannerView;->mPaddingControls:I

    sub-int/2addr v3, v0

    .line 753
    iget-object v0, p0, Lorg/chromium/chrome/browser/banners/AppBannerView;->mTitleView:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v4

    .line 754
    iget-object v0, p0, Lorg/chromium/chrome/browser/banners/AppBannerView;->mTitleView:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    add-int/2addr v5, v0

    .line 755
    iget-object v0, p0, Lorg/chromium/chrome/browser/banners/AppBannerView;->mTitleView:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v0

    add-int v6, v5, v0

    .line 756
    iget-boolean v0, p0, Lorg/chromium/chrome/browser/banners/AppBannerView;->mIsLayoutLTR:Z

    if-eqz v0, :cond_3

    move v0, v1

    .line 757
    :goto_2
    iget-object v7, p0, Lorg/chromium/chrome/browser/banners/AppBannerView;->mTitleView:Landroid/widget/TextView;

    add-int/2addr v4, v0

    invoke-virtual {v7, v0, v5, v4, v6}, Landroid/widget/TextView;->layout(IIII)V

    .line 760
    iget-object v0, p0, Lorg/chromium/chrome/browser/banners/AppBannerView;->mTitleView:Landroid/widget/TextView;

    iget-object v4, p0, Lorg/chromium/chrome/browser/banners/AppBannerView;->mTitleView:Landroid/widget/TextView;

    invoke-virtual {v4}, Landroid/widget/TextView;->getLineCount()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    const/4 v6, 0x0

    invoke-virtual {v0, v4, v6}, Landroid/widget/TextView;->getLineBounds(ILandroid/graphics/Rect;)I

    move-result v0

    .line 761
    add-int v4, v5, v0

    iget-object v0, p0, Lorg/chromium/chrome/browser/banners/AppBannerView;->mTitleView:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    add-int/2addr v4, v0

    .line 765
    iget-object v0, p0, Lorg/chromium/chrome/browser/banners/AppBannerView;->mRatingView:Lorg/chromium/chrome/browser/banners/RatingView;

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/banners/RatingView;->getMeasuredWidth()I

    move-result v5

    .line 766
    iget-object v0, p0, Lorg/chromium/chrome/browser/banners/AppBannerView;->mRatingView:Lorg/chromium/chrome/browser/banners/RatingView;

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/banners/RatingView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    add-int/2addr v4, v0

    .line 767
    iget-object v0, p0, Lorg/chromium/chrome/browser/banners/AppBannerView;->mRatingView:Lorg/chromium/chrome/browser/banners/RatingView;

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/banners/RatingView;->getMeasuredHeight()I

    move-result v0

    add-int v6, v4, v0

    .line 768
    iget-boolean v0, p0, Lorg/chromium/chrome/browser/banners/AppBannerView;->mIsLayoutLTR:Z

    if-eqz v0, :cond_4

    move v0, v1

    .line 769
    :goto_3
    iget-object v7, p0, Lorg/chromium/chrome/browser/banners/AppBannerView;->mRatingView:Lorg/chromium/chrome/browser/banners/RatingView;

    add-int/2addr v5, v0

    invoke-virtual {v7, v0, v4, v5, v6}, Lorg/chromium/chrome/browser/banners/RatingView;->layout(IIII)V

    .line 772
    iget-object v0, p0, Lorg/chromium/chrome/browser/banners/AppBannerView;->mLogoView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v4

    .line 773
    iget-object v0, p0, Lorg/chromium/chrome/browser/banners/AppBannerView;->mLogoView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    sub-int v0, v3, v0

    .line 774
    iget-object v5, p0, Lorg/chromium/chrome/browser/banners/AppBannerView;->mLogoView:Landroid/view/View;

    invoke-virtual {v5}, Landroid/view/View;->getMeasuredHeight()I

    move-result v5

    sub-int v5, v0, v5

    .line 775
    iget-boolean v6, p0, Lorg/chromium/chrome/browser/banners/AppBannerView;->mIsLayoutLTR:Z

    if-eqz v6, :cond_5

    .line 776
    :goto_4
    iget-object v6, p0, Lorg/chromium/chrome/browser/banners/AppBannerView;->mLogoView:Landroid/view/View;

    add-int/2addr v4, v1

    invoke-virtual {v6, v1, v5, v4, v0}, Landroid/view/View;->layout(IIII)V

    .line 779
    iget-object v0, p0, Lorg/chromium/chrome/browser/banners/AppBannerView;->mInstallButtonView:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->getMeasuredHeight()I

    move-result v1

    .line 780
    iget-object v0, p0, Lorg/chromium/chrome/browser/banners/AppBannerView;->mInstallButtonView:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->getMeasuredWidth()I

    move-result v4

    .line 781
    iget-boolean v0, p0, Lorg/chromium/chrome/browser/banners/AppBannerView;->mIsLayoutLTR:Z

    if-eqz v0, :cond_6

    move v0, v2

    .line 782
    :goto_5
    sub-int v2, v0, v4

    .line 783
    iget-object v4, p0, Lorg/chromium/chrome/browser/banners/AppBannerView;->mInstallButtonView:Landroid/widget/Button;

    sub-int v1, v3, v1

    invoke-virtual {v4, v2, v1, v0, v3}, Landroid/widget/Button;->layout(IIII)V

    .line 784
    return-void

    .line 731
    :cond_1
    invoke-virtual {p0}, Lorg/chromium/chrome/browser/banners/AppBannerView;->getMeasuredWidth()I

    move-result v0

    sub-int/2addr v0, v1

    add-int/2addr v0, v5

    goto/16 :goto_0

    .line 744
    :cond_2
    invoke-virtual {p0}, Lorg/chromium/chrome/browser/banners/AppBannerView;->getMeasuredWidth()I

    move-result v0

    sub-int/2addr v0, v2

    sub-int/2addr v0, v1

    goto/16 :goto_1

    .line 756
    :cond_3
    invoke-virtual {p0}, Lorg/chromium/chrome/browser/banners/AppBannerView;->getMeasuredWidth()I

    move-result v0

    sub-int/2addr v0, v1

    sub-int/2addr v0, v4

    goto/16 :goto_2

    .line 768
    :cond_4
    invoke-virtual {p0}, Lorg/chromium/chrome/browser/banners/AppBannerView;->getMeasuredWidth()I

    move-result v0

    sub-int/2addr v0, v1

    sub-int/2addr v0, v5

    goto :goto_3

    .line 775
    :cond_5
    invoke-virtual {p0}, Lorg/chromium/chrome/browser/banners/AppBannerView;->getMeasuredWidth()I

    move-result v6

    sub-int v1, v6, v1

    sub-int/2addr v1, v4

    goto :goto_4

    .line 781
    :cond_6
    invoke-virtual {p0}, Lorg/chromium/chrome/browser/banners/AppBannerView;->getMeasuredWidth()I

    move-result v0

    sub-int/2addr v0, v2

    add-int/2addr v0, v4

    goto :goto_5
.end method

.method protected onMeasure(II)V
    .locals 9

    .prologue
    .line 610
    invoke-virtual {p0}, Lorg/chromium/chrome/browser/banners/AppBannerView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 611
    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->density:F

    .line 612
    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->smallestScreenWidthDp:I

    int-to-float v0, v0

    mul-float/2addr v0, v1

    float-to-int v0, v0

    .line 613
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    .line 614
    iget v2, p0, Lorg/chromium/chrome/browser/banners/AppBannerView;->mDefinedMaxWidth:I

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 620
    iget-object v0, p0, Lorg/chromium/chrome/browser/banners/AppBannerView;->mBackgroundDrawablePadding:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    iget-object v2, p0, Lorg/chromium/chrome/browser/banners/AppBannerView;->mBackgroundDrawablePadding:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->right:I

    add-int/2addr v2, v0

    .line 621
    iget-object v0, p0, Lorg/chromium/chrome/browser/banners/AppBannerView;->mBackgroundDrawablePadding:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->top:I

    iget-object v3, p0, Lorg/chromium/chrome/browser/banners/AppBannerView;->mBackgroundDrawablePadding:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v3, v0

    .line 622
    sub-int v0, v1, v2

    iget v4, p0, Lorg/chromium/chrome/browser/banners/AppBannerView;->mPaddingCard:I

    shl-int/lit8 v4, v4, 0x1

    sub-int v4, v0, v4

    .line 628
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 629
    div-int/lit8 v5, v4, 0x4

    .line 630
    iget v6, p0, Lorg/chromium/chrome/browser/banners/AppBannerView;->mPaddingCard:I

    shl-int/lit8 v6, v6, 0x1

    add-int/2addr v6, v3

    .line 631
    invoke-static {v0, v5}, Ljava/lang/Math;->min(II)I

    move-result v0

    sub-int/2addr v0, v6

    .line 632
    div-int/lit8 v5, v4, 0x3

    .line 636
    iget-object v6, p0, Lorg/chromium/chrome/browser/banners/AppBannerView;->mIconView:Landroid/widget/ImageView;

    invoke-direct {p0, v6, v4, v0}, Lorg/chromium/chrome/browser/banners/AppBannerView;->measureChildForSpace(Landroid/view/View;II)V

    .line 637
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0}, Lorg/chromium/chrome/browser/banners/AppBannerView;->getChildCount()I

    move-result v6

    if-ge v0, v6, :cond_1

    .line 638
    invoke-virtual {p0, v0}, Lorg/chromium/chrome/browser/banners/AppBannerView;->getChildAt(I)Landroid/view/View;

    move-result-object v6

    iget-object v7, p0, Lorg/chromium/chrome/browser/banners/AppBannerView;->mIconView:Landroid/widget/ImageView;

    if-eq v6, v7, :cond_0

    .line 639
    invoke-virtual {p0, v0}, Lorg/chromium/chrome/browser/banners/AppBannerView;->getChildAt(I)Landroid/view/View;

    move-result-object v6

    invoke-direct {p0, v6, v4, v5}, Lorg/chromium/chrome/browser/banners/AppBannerView;->measureChildForSpace(Landroid/view/View;II)V

    .line 637
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 650
    :cond_1
    iget-object v0, p0, Lorg/chromium/chrome/browser/banners/AppBannerView;->mIconView:Landroid/widget/ImageView;

    invoke-static {v0}, Lorg/chromium/chrome/browser/banners/AppBannerView;->getHeightWithMargins(Landroid/view/View;)I

    move-result v0

    .line 651
    iget-object v5, p0, Lorg/chromium/chrome/browser/banners/AppBannerView;->mTitleView:Landroid/widget/TextView;

    invoke-static {v5}, Lorg/chromium/chrome/browser/banners/AppBannerView;->getHeightWithMargins(Landroid/view/View;)I

    move-result v5

    iget v6, p0, Lorg/chromium/chrome/browser/banners/AppBannerView;->mPaddingControls:I

    add-int/2addr v5, v6

    iget-object v6, p0, Lorg/chromium/chrome/browser/banners/AppBannerView;->mRatingView:Lorg/chromium/chrome/browser/banners/RatingView;

    invoke-static {v6}, Lorg/chromium/chrome/browser/banners/AppBannerView;->getHeightWithMargins(Landroid/view/View;)I

    move-result v6

    add-int/2addr v5, v6

    iget-object v6, p0, Lorg/chromium/chrome/browser/banners/AppBannerView;->mLogoView:Landroid/view/View;

    invoke-static {v6}, Lorg/chromium/chrome/browser/banners/AppBannerView;->getHeightWithMargins(Landroid/view/View;)I

    move-result v6

    add-int/2addr v5, v6

    .line 653
    iget-object v6, p0, Lorg/chromium/chrome/browser/banners/AppBannerView;->mTitleView:Landroid/widget/TextView;

    invoke-static {v6}, Lorg/chromium/chrome/browser/banners/AppBannerView;->getHeightWithMargins(Landroid/view/View;)I

    move-result v6

    iget v7, p0, Lorg/chromium/chrome/browser/banners/AppBannerView;->mPaddingControls:I

    add-int/2addr v6, v7

    iget-object v7, p0, Lorg/chromium/chrome/browser/banners/AppBannerView;->mInstallButtonView:Landroid/widget/Button;

    invoke-static {v7}, Lorg/chromium/chrome/browser/banners/AppBannerView;->getHeightWithMargins(Landroid/view/View;)I

    move-result v7

    add-int/2addr v6, v7

    .line 655
    invoke-static {v5, v6}, Ljava/lang/Math;->max(II)I

    move-result v5

    invoke-static {v0, v5}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 660
    iget-object v5, p0, Lorg/chromium/chrome/browser/banners/AppBannerView;->mIconView:Landroid/widget/ImageView;

    invoke-direct {p0, v5, v0, v0}, Lorg/chromium/chrome/browser/banners/AppBannerView;->measureChildForSpaceExactly(Landroid/view/View;II)V

    .line 664
    iget-object v5, p0, Lorg/chromium/chrome/browser/banners/AppBannerView;->mIconView:Landroid/widget/ImageView;

    invoke-static {v5}, Lorg/chromium/chrome/browser/banners/AppBannerView;->getWidthWithMargins(Landroid/view/View;)I

    move-result v5

    sub-int v5, v4, v5

    iget v6, p0, Lorg/chromium/chrome/browser/banners/AppBannerView;->mPaddingControls:I

    sub-int/2addr v5, v6

    .line 666
    iget v6, p0, Lorg/chromium/chrome/browser/banners/AppBannerView;->mPaddingControls:I

    sub-int v6, v0, v6

    .line 667
    iget-object v7, p0, Lorg/chromium/chrome/browser/banners/AppBannerView;->mLogoView:Landroid/view/View;

    invoke-direct {p0, v7, v5, v6}, Lorg/chromium/chrome/browser/banners/AppBannerView;->measureChildForSpace(Landroid/view/View;II)V

    .line 670
    iget-object v7, p0, Lorg/chromium/chrome/browser/banners/AppBannerView;->mLogoView:Landroid/view/View;

    invoke-static {v7}, Lorg/chromium/chrome/browser/banners/AppBannerView;->getWidthWithMargins(Landroid/view/View;)I

    move-result v7

    sub-int/2addr v4, v7

    iget-object v7, p0, Lorg/chromium/chrome/browser/banners/AppBannerView;->mIconView:Landroid/widget/ImageView;

    invoke-static {v7}, Lorg/chromium/chrome/browser/banners/AppBannerView;->getWidthWithMargins(Landroid/view/View;)I

    move-result v7

    sub-int/2addr v4, v7

    .line 672
    iget-object v7, p0, Lorg/chromium/chrome/browser/banners/AppBannerView;->mInstallButtonView:Landroid/widget/Button;

    invoke-virtual {v7, v4}, Landroid/widget/Button;->setMaxWidth(I)V

    .line 673
    iget-object v4, p0, Lorg/chromium/chrome/browser/banners/AppBannerView;->mInstallButtonView:Landroid/widget/Button;

    invoke-direct {p0, v4, v5, v6}, Lorg/chromium/chrome/browser/banners/AppBannerView;->measureChildForSpace(Landroid/view/View;II)V

    .line 677
    iget-object v4, p0, Lorg/chromium/chrome/browser/banners/AppBannerView;->mLogoView:Landroid/view/View;

    invoke-static {v4}, Lorg/chromium/chrome/browser/banners/AppBannerView;->getHeightWithMargins(Landroid/view/View;)I

    move-result v4

    sub-int v4, v6, v4

    .line 678
    iget-object v7, p0, Lorg/chromium/chrome/browser/banners/AppBannerView;->mRatingView:Lorg/chromium/chrome/browser/banners/RatingView;

    invoke-direct {p0, v7, v5, v4}, Lorg/chromium/chrome/browser/banners/AppBannerView;->measureChildForSpace(Landroid/view/View;II)V

    .line 682
    iget-object v4, p0, Lorg/chromium/chrome/browser/banners/AppBannerView;->mInstallButtonView:Landroid/widget/Button;

    invoke-static {v4}, Lorg/chromium/chrome/browser/banners/AppBannerView;->getHeightWithMargins(Landroid/view/View;)I

    move-result v4

    sub-int v4, v6, v4

    .line 683
    iget-object v7, p0, Lorg/chromium/chrome/browser/banners/AppBannerView;->mCloseButtonView:Landroid/widget/ImageButton;

    invoke-direct {p0, v7, v5, v4}, Lorg/chromium/chrome/browser/banners/AppBannerView;->measureChildForSpace(Landroid/view/View;II)V

    .line 689
    iget-object v4, p0, Lorg/chromium/chrome/browser/banners/AppBannerView;->mInstallButtonView:Landroid/widget/Button;

    invoke-static {v4}, Lorg/chromium/chrome/browser/banners/AppBannerView;->getHeightWithMargins(Landroid/view/View;)I

    move-result v4

    iget-object v7, p0, Lorg/chromium/chrome/browser/banners/AppBannerView;->mLogoView:Landroid/view/View;

    invoke-static {v7}, Lorg/chromium/chrome/browser/banners/AppBannerView;->getHeightWithMargins(Landroid/view/View;)I

    move-result v7

    iget-object v8, p0, Lorg/chromium/chrome/browser/banners/AppBannerView;->mRatingView:Lorg/chromium/chrome/browser/banners/RatingView;

    invoke-static {v8}, Lorg/chromium/chrome/browser/banners/AppBannerView;->getHeightWithMargins(Landroid/view/View;)I

    move-result v8

    add-int/2addr v7, v8

    invoke-static {v4, v7}, Ljava/lang/Math;->max(II)I

    move-result v4

    .line 691
    iget-object v7, p0, Lorg/chromium/chrome/browser/banners/AppBannerView;->mCloseButtonView:Landroid/widget/ImageButton;

    invoke-static {v7}, Lorg/chromium/chrome/browser/banners/AppBannerView;->getWidthWithMargins(Landroid/view/View;)I

    move-result v7

    sub-int/2addr v5, v7

    iget v7, p0, Lorg/chromium/chrome/browser/banners/AppBannerView;->mPaddingCard:I

    add-int/2addr v5, v7

    .line 692
    sub-int v4, v6, v4

    .line 693
    iget-object v6, p0, Lorg/chromium/chrome/browser/banners/AppBannerView;->mTitleView:Landroid/widget/TextView;

    invoke-direct {p0, v6, v5, v4}, Lorg/chromium/chrome/browser/banners/AppBannerView;->measureChildForSpace(Landroid/view/View;II)V

    .line 698
    iget-object v4, p0, Lorg/chromium/chrome/browser/banners/AppBannerView;->mBackgroundDrawablePadding:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->top:I

    iget-object v5, p0, Lorg/chromium/chrome/browser/banners/AppBannerView;->mBackgroundDrawablePadding:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v4, v5

    iget v5, p0, Lorg/chromium/chrome/browser/banners/AppBannerView;->mPaddingCard:I

    shl-int/lit8 v5, v5, 0x1

    add-int/2addr v4, v5

    .line 700
    add-int/2addr v0, v4

    .line 701
    invoke-virtual {p0, v1, v0}, Lorg/chromium/chrome/browser/banners/AppBannerView;->setMeasuredDimension(II)V

    .line 704
    sub-int/2addr v1, v2

    .line 705
    sub-int/2addr v0, v3

    .line 706
    iget-object v2, p0, Lorg/chromium/chrome/browser/banners/AppBannerView;->mBannerHighlightView:Landroid/view/View;

    invoke-direct {p0, v2, v1, v0}, Lorg/chromium/chrome/browser/banners/AppBannerView;->measureChildForSpaceExactly(Landroid/view/View;II)V

    .line 707
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 3

    .prologue
    .line 486
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v0

    .line 487
    iget-boolean v1, p0, Lorg/chromium/chrome/browser/banners/AppBannerView;->mIsBannerPressed:Z

    if-eqz v1, :cond_1

    .line 490
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawX()F

    move-result v1

    iget v2, p0, Lorg/chromium/chrome/browser/banners/AppBannerView;->mInitialXForHighlight:F

    sub-float/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    .line 491
    const/4 v2, 0x1

    if-eq v0, v2, :cond_0

    const/4 v2, 0x3

    if-eq v0, v2, :cond_0

    const/4 v2, 0x2

    if-ne v0, v2, :cond_1

    iget v0, p0, Lorg/chromium/chrome/browser/banners/AppBannerView;->mTouchSlop:I

    int-to-float v0, v0

    cmpl-float v0, v1, v0

    if-lez v0, :cond_1

    .line 493
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/chromium/chrome/browser/banners/AppBannerView;->mIsBannerPressed:Z

    .line 494
    iget-object v0, p0, Lorg/chromium/chrome/browser/banners/AppBannerView;->mBannerHighlightView:Landroid/view/View;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 498
    :cond_1
    invoke-super {p0, p1}, Lorg/chromium/chrome/browser/banners/SwipableOverlayView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method protected onViewClicked()V
    .locals 6

    .prologue
    .line 330
    :try_start_0
    iget-object v0, p0, Lorg/chromium/chrome/browser/banners/AppBannerView;->mAppData:Lorg/chromium/chrome/browser/banners/AppData;

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/banners/AppData;->detailsIntent()Landroid/app/PendingIntent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/PendingIntent;->getIntentSender()Landroid/content/IntentSender;

    move-result-object v1

    .line 331
    invoke-virtual {p0}, Lorg/chromium/chrome/browser/banners/AppBannerView;->getContext()Landroid/content/Context;

    move-result-object v0

    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/Context;->startIntentSender(Landroid/content/IntentSender;Landroid/content/Intent;III)V
    :try_end_0
    .catch Landroid/content/IntentSender$SendIntentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 336
    :goto_0
    const/16 v0, 0x2b

    invoke-virtual {p0, v0}, Lorg/chromium/chrome/browser/banners/AppBannerView;->dismiss(I)V

    .line 337
    return-void

    .line 332
    :catch_0
    move-exception v0

    .line 333
    const-string/jumbo v0, "AppBannerView"

    const-string/jumbo v1, "Failed to launch details intent."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method protected onViewPressed(Landroid/view/MotionEvent;)V
    .locals 2

    .prologue
    .line 342
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawX()F

    move-result v0

    iput v0, p0, Lorg/chromium/chrome/browser/banners/AppBannerView;->mInitialXForHighlight:F

    .line 343
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/chromium/chrome/browser/banners/AppBannerView;->mIsBannerPressed:Z

    .line 344
    iget-object v0, p0, Lorg/chromium/chrome/browser/banners/AppBannerView;->mBannerHighlightView:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 345
    return-void
.end method

.method protected onViewSwipedAway()V
    .locals 3

    .prologue
    .line 321
    iget-object v0, p0, Lorg/chromium/chrome/browser/banners/AppBannerView;->mObserver:Lorg/chromium/chrome/browser/banners/AppBannerView$Observer;

    if-nez v0, :cond_0

    .line 324
    :goto_0
    return-void

    .line 322
    :cond_0
    iget-object v0, p0, Lorg/chromium/chrome/browser/banners/AppBannerView;->mObserver:Lorg/chromium/chrome/browser/banners/AppBannerView$Observer;

    const/16 v1, 0x2c

    invoke-interface {v0, p0, v1}, Lorg/chromium/chrome/browser/banners/AppBannerView$Observer;->onBannerDismissEvent(Lorg/chromium/chrome/browser/banners/AppBannerView;I)V

    .line 323
    iget-object v0, p0, Lorg/chromium/chrome/browser/banners/AppBannerView;->mObserver:Lorg/chromium/chrome/browser/banners/AppBannerView$Observer;

    iget-object v1, p0, Lorg/chromium/chrome/browser/banners/AppBannerView;->mAppData:Lorg/chromium/chrome/browser/banners/AppData;

    invoke-virtual {v1}, Lorg/chromium/chrome/browser/banners/AppData;->siteUrl()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lorg/chromium/chrome/browser/banners/AppBannerView;->mAppData:Lorg/chromium/chrome/browser/banners/AppData;

    invoke-virtual {v2}, Lorg/chromium/chrome/browser/banners/AppData;->packageName()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, p0, v1, v2}, Lorg/chromium/chrome/browser/banners/AppBannerView$Observer;->onBannerBlocked(Lorg/chromium/chrome/browser/banners/AppBannerView;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onWindowFocusChanged(Z)V
    .locals 0

    .prologue
    .line 560
    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lorg/chromium/chrome/browser/banners/AppBannerView;->updateButtonStatus()V

    .line 561
    :cond_0
    return-void
.end method

.method removeFromParent()Z
    .locals 1

    .prologue
    .line 396
    invoke-super {p0}, Lorg/chromium/chrome/browser/banners/SwipableOverlayView;->removeFromParent()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 397
    iget-object v0, p0, Lorg/chromium/chrome/browser/banners/AppBannerView;->mObserver:Lorg/chromium/chrome/browser/banners/AppBannerView$Observer;

    invoke-interface {v0, p0}, Lorg/chromium/chrome/browser/banners/AppBannerView$Observer;->onBannerRemoved(Lorg/chromium/chrome/browser/banners/AppBannerView;)V

    .line 398
    invoke-virtual {p0}, Lorg/chromium/chrome/browser/banners/AppBannerView;->destroy()V

    .line 399
    const/4 v0, 0x1

    .line 402
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method updateButtonStatus()V
    .locals 8

    .prologue
    const/4 v4, 0x2

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 432
    iget-object v0, p0, Lorg/chromium/chrome/browser/banners/AppBannerView;->mInstallButtonView:Landroid/widget/Button;

    if-nez v0, :cond_0

    .line 470
    :goto_0
    return-void

    .line 437
    :cond_0
    invoke-direct {p0}, Lorg/chromium/chrome/browser/banners/AppBannerView;->getAppLaunchIntent()Landroid/content/Intent;

    move-result-object v0

    if-nez v0, :cond_2

    .line 438
    iget v0, p0, Lorg/chromium/chrome/browser/banners/AppBannerView;->mInstallState:I

    if-ne v0, v4, :cond_1

    .line 439
    iput v3, p0, Lorg/chromium/chrome/browser/banners/AppBannerView;->mInstallState:I

    .line 446
    :cond_1
    :goto_1
    invoke-virtual {p0}, Lorg/chromium/chrome/browser/banners/AppBannerView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 449
    iget v0, p0, Lorg/chromium/chrome/browser/banners/AppBannerView;->mInstallState:I

    if-ne v0, v4, :cond_3

    .line 450
    iget-object v0, p0, Lorg/chromium/chrome/browser/banners/AppBannerView;->mInstallButtonView:Landroid/widget/Button;

    sget v4, Lorg/chromium/chrome/R$drawable;->app_banner_button_open:I

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-static {v0, v4}, Lorg/chromium/base/ApiCompatibilityUtils;->setBackgroundForView(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 452
    sget v0, Lorg/chromium/chrome/R$color;->app_banner_open_button_fg:I

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    .line 453
    sget v4, Lorg/chromium/chrome/R$string;->app_banner_open:I

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 467
    :goto_2
    iget-object v4, p0, Lorg/chromium/chrome/browser/banners/AppBannerView;->mInstallButtonView:Landroid/widget/Button;

    invoke-virtual {v4, v0}, Landroid/widget/Button;->setTextColor(I)V

    .line 468
    iget-object v0, p0, Lorg/chromium/chrome/browser/banners/AppBannerView;->mInstallButtonView:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 469
    iget-object v1, p0, Lorg/chromium/chrome/browser/banners/AppBannerView;->mInstallButtonView:Landroid/widget/Button;

    iget v0, p0, Lorg/chromium/chrome/browser/banners/AppBannerView;->mInstallState:I

    if-eq v0, v2, :cond_5

    move v0, v2

    :goto_3
    invoke-virtual {v1, v0}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_0

    .line 442
    :cond_2
    iput v4, p0, Lorg/chromium/chrome/browser/banners/AppBannerView;->mInstallState:I

    goto :goto_1

    .line 455
    :cond_3
    iget-object v0, p0, Lorg/chromium/chrome/browser/banners/AppBannerView;->mInstallButtonView:Landroid/widget/Button;

    sget v4, Lorg/chromium/chrome/R$drawable;->app_banner_button_install:I

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-static {v0, v4}, Lorg/chromium/base/ApiCompatibilityUtils;->setBackgroundForView(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 457
    sget v0, Lorg/chromium/chrome/R$color;->app_banner_install_button_fg:I

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    .line 458
    iget v4, p0, Lorg/chromium/chrome/browser/banners/AppBannerView;->mInstallState:I

    if-nez v4, :cond_4

    .line 459
    iget-object v1, p0, Lorg/chromium/chrome/browser/banners/AppBannerView;->mAppData:Lorg/chromium/chrome/browser/banners/AppData;

    invoke-virtual {v1}, Lorg/chromium/chrome/browser/banners/AppData;->installButtonText()Ljava/lang/String;

    move-result-object v1

    .line 460
    iget-object v4, p0, Lorg/chromium/chrome/browser/banners/AppBannerView;->mInstallButtonView:Landroid/widget/Button;

    invoke-virtual {p0}, Lorg/chromium/chrome/browser/banners/AppBannerView;->getContext()Landroid/content/Context;

    move-result-object v5

    sget v6, Lorg/chromium/chrome/R$string;->app_banner_install_accessibility:I

    new-array v7, v2, [Ljava/lang/Object;

    aput-object v1, v7, v3

    invoke-virtual {v5, v6, v7}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/Button;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_2

    .line 463
    :cond_4
    sget v4, Lorg/chromium/chrome/R$string;->app_banner_installing:I

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_2

    :cond_5
    move v0, v3

    .line 469
    goto :goto_3
.end method
