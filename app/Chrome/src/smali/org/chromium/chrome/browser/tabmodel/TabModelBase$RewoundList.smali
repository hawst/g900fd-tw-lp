.class Lorg/chromium/chrome/browser/tabmodel/TabModelBase$RewoundList;
.super Ljava/lang/Object;
.source "TabModelBase.java"

# interfaces
.implements Lorg/chromium/chrome/browser/tabmodel/TabList;


# instance fields
.field private mRewoundTabs:Ljava/util/List;

.field final synthetic this$0:Lorg/chromium/chrome/browser/tabmodel/TabModelBase;


# direct methods
.method private constructor <init>(Lorg/chromium/chrome/browser/tabmodel/TabModelBase;)V
    .locals 1

    .prologue
    .line 485
    iput-object p1, p0, Lorg/chromium/chrome/browser/tabmodel/TabModelBase$RewoundList;->this$0:Lorg/chromium/chrome/browser/tabmodel/TabModelBase;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 491
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lorg/chromium/chrome/browser/tabmodel/TabModelBase$RewoundList;->mRewoundTabs:Ljava/util/List;

    return-void
.end method

.method synthetic constructor <init>(Lorg/chromium/chrome/browser/tabmodel/TabModelBase;Lorg/chromium/chrome/browser/tabmodel/TabModelBase$1;)V
    .locals 0

    .prologue
    .line 485
    invoke-direct {p0, p1}, Lorg/chromium/chrome/browser/tabmodel/TabModelBase$RewoundList;-><init>(Lorg/chromium/chrome/browser/tabmodel/TabModelBase;)V

    return-void
.end method


# virtual methods
.method public destroy()V
    .locals 3

    .prologue
    .line 595
    iget-object v0, p0, Lorg/chromium/chrome/browser/tabmodel/TabModelBase$RewoundList;->mRewoundTabs:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/chromium/chrome/browser/Tab;

    .line 596
    invoke-virtual {v0}, Lorg/chromium/chrome/browser/Tab;->isInitialized()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/Tab;->destroy()V

    goto :goto_0

    .line 598
    :cond_1
    return-void
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 516
    iget-object v0, p0, Lorg/chromium/chrome/browser/tabmodel/TabModelBase$RewoundList;->mRewoundTabs:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getNextRewindableTab()Lorg/chromium/chrome/browser/Tab;
    .locals 5

    .prologue
    const/4 v3, 0x0

    .line 570
    invoke-virtual {p0}, Lorg/chromium/chrome/browser/tabmodel/TabModelBase$RewoundList;->hasPendingClosures()Z

    move-result v0

    if-nez v0, :cond_1

    .line 579
    :cond_0
    :goto_0
    return-object v3

    .line 572
    :cond_1
    const/4 v0, 0x0

    move v1, v0

    :goto_1
    iget-object v0, p0, Lorg/chromium/chrome/browser/tabmodel/TabModelBase$RewoundList;->mRewoundTabs:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 573
    iget-object v0, p0, Lorg/chromium/chrome/browser/tabmodel/TabModelBase$RewoundList;->this$0:Lorg/chromium/chrome/browser/tabmodel/TabModelBase;

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/tabmodel/TabModelBase;->getCount()I

    move-result v0

    if-ge v1, v0, :cond_3

    iget-object v0, p0, Lorg/chromium/chrome/browser/tabmodel/TabModelBase$RewoundList;->this$0:Lorg/chromium/chrome/browser/tabmodel/TabModelBase;

    invoke-virtual {v0, v1}, Lorg/chromium/chrome/browser/tabmodel/TabModelBase;->getTabAt(I)Lorg/chromium/chrome/browser/Tab;

    move-result-object v0

    move-object v2, v0

    .line 574
    :goto_2
    iget-object v0, p0, Lorg/chromium/chrome/browser/tabmodel/TabModelBase$RewoundList;->mRewoundTabs:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/chromium/chrome/browser/Tab;

    .line 576
    if-eqz v2, :cond_2

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/Tab;->getId()I

    move-result v4

    invoke-virtual {v2}, Lorg/chromium/chrome/browser/Tab;->getId()I

    move-result v2

    if-eq v4, v2, :cond_4

    :cond_2
    move-object v3, v0

    goto :goto_0

    :cond_3
    move-object v2, v3

    .line 573
    goto :goto_2

    .line 572
    :cond_4
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1
.end method

.method public getPendingRewindTab(I)Lorg/chromium/chrome/browser/Tab;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 560
    iget-object v1, p0, Lorg/chromium/chrome/browser/tabmodel/TabModelBase$RewoundList;->this$0:Lorg/chromium/chrome/browser/tabmodel/TabModelBase;

    invoke-virtual {v1}, Lorg/chromium/chrome/browser/tabmodel/TabModelBase;->supportsPendingClosures()Z

    move-result v1

    if-nez v1, :cond_1

    .line 562
    :cond_0
    :goto_0
    return-object v0

    .line 561
    :cond_1
    iget-object v1, p0, Lorg/chromium/chrome/browser/tabmodel/TabModelBase$RewoundList;->this$0:Lorg/chromium/chrome/browser/tabmodel/TabModelBase;

    invoke-static {v1, p1}, Lorg/chromium/chrome/browser/tabmodel/TabModelUtils;->getTabById(Lorg/chromium/chrome/browser/tabmodel/TabList;I)Lorg/chromium/chrome/browser/Tab;

    move-result-object v1

    if-nez v1, :cond_0

    .line 562
    invoke-static {p0, p1}, Lorg/chromium/chrome/browser/tabmodel/TabModelUtils;->getTabById(Lorg/chromium/chrome/browser/tabmodel/TabList;I)Lorg/chromium/chrome/browser/Tab;

    move-result-object v0

    goto :goto_0
.end method

.method public getTabAt(I)Lorg/chromium/chrome/browser/Tab;
    .locals 1

    .prologue
    .line 521
    if-ltz p1, :cond_0

    iget-object v0, p0, Lorg/chromium/chrome/browser/tabmodel/TabModelBase$RewoundList;->mRewoundTabs:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lt p1, v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    .line 522
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lorg/chromium/chrome/browser/tabmodel/TabModelBase$RewoundList;->mRewoundTabs:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/chromium/chrome/browser/Tab;

    goto :goto_0
.end method

.method public hasPendingClosures()Z
    .locals 2

    .prologue
    .line 601
    iget-object v0, p0, Lorg/chromium/chrome/browser/tabmodel/TabModelBase$RewoundList;->this$0:Lorg/chromium/chrome/browser/tabmodel/TabModelBase;

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/tabmodel/TabModelBase;->supportsPendingClosures()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/chromium/chrome/browser/tabmodel/TabModelBase$RewoundList;->mRewoundTabs:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    iget-object v1, p0, Lorg/chromium/chrome/browser/tabmodel/TabModelBase$RewoundList;->this$0:Lorg/chromium/chrome/browser/tabmodel/TabModelBase;

    invoke-virtual {v1}, Lorg/chromium/chrome/browser/tabmodel/TabModelBase;->getCount()I

    move-result v1

    if-le v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public index()I
    .locals 2

    .prologue
    const/4 v0, -0x1

    .line 507
    iget-object v1, p0, Lorg/chromium/chrome/browser/tabmodel/TabModelBase$RewoundList;->this$0:Lorg/chromium/chrome/browser/tabmodel/TabModelBase;

    invoke-virtual {v1}, Lorg/chromium/chrome/browser/tabmodel/TabModelBase;->index()I

    move-result v1

    if-eq v1, v0, :cond_1

    .line 508
    iget-object v0, p0, Lorg/chromium/chrome/browser/tabmodel/TabModelBase$RewoundList;->mRewoundTabs:Ljava/util/List;

    iget-object v1, p0, Lorg/chromium/chrome/browser/tabmodel/TabModelBase$RewoundList;->this$0:Lorg/chromium/chrome/browser/tabmodel/TabModelBase;

    invoke-static {v1}, Lorg/chromium/chrome/browser/tabmodel/TabModelUtils;->getCurrentTab(Lorg/chromium/chrome/browser/tabmodel/TabList;)Lorg/chromium/chrome/browser/Tab;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v0

    .line 511
    :cond_0
    :goto_0
    return v0

    .line 510
    :cond_1
    iget-object v1, p0, Lorg/chromium/chrome/browser/tabmodel/TabModelBase$RewoundList;->mRewoundTabs:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public indexOf(Lorg/chromium/chrome/browser/Tab;)I
    .locals 1

    .prologue
    .line 527
    iget-object v0, p0, Lorg/chromium/chrome/browser/tabmodel/TabModelBase$RewoundList;->mRewoundTabs:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public isClosurePending(I)Z
    .locals 1

    .prologue
    .line 532
    iget-object v0, p0, Lorg/chromium/chrome/browser/tabmodel/TabModelBase$RewoundList;->this$0:Lorg/chromium/chrome/browser/tabmodel/TabModelBase;

    invoke-virtual {v0, p1}, Lorg/chromium/chrome/browser/tabmodel/TabModelBase;->isClosurePending(I)Z

    move-result v0

    return v0
.end method

.method public isIncognito()Z
    .locals 1

    .prologue
    .line 495
    iget-object v0, p0, Lorg/chromium/chrome/browser/tabmodel/TabModelBase$RewoundList;->this$0:Lorg/chromium/chrome/browser/tabmodel/TabModelBase;

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/tabmodel/TabModelBase;->isIncognito()Z

    move-result v0

    return v0
.end method

.method public removeTab(Lorg/chromium/chrome/browser/Tab;)V
    .locals 1

    .prologue
    .line 587
    iget-object v0, p0, Lorg/chromium/chrome/browser/tabmodel/TabModelBase$RewoundList;->mRewoundTabs:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 588
    return-void
.end method

.method public resetRewoundState()V
    .locals 3

    .prologue
    .line 541
    iget-object v0, p0, Lorg/chromium/chrome/browser/tabmodel/TabModelBase$RewoundList;->mRewoundTabs:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 543
    iget-object v0, p0, Lorg/chromium/chrome/browser/tabmodel/TabModelBase$RewoundList;->this$0:Lorg/chromium/chrome/browser/tabmodel/TabModelBase;

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/tabmodel/TabModelBase;->supportsPendingClosures()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 544
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lorg/chromium/chrome/browser/tabmodel/TabModelBase$RewoundList;->this$0:Lorg/chromium/chrome/browser/tabmodel/TabModelBase;

    invoke-virtual {v1}, Lorg/chromium/chrome/browser/tabmodel/TabModelBase;->getCount()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 545
    iget-object v1, p0, Lorg/chromium/chrome/browser/tabmodel/TabModelBase$RewoundList;->mRewoundTabs:Ljava/util/List;

    iget-object v2, p0, Lorg/chromium/chrome/browser/tabmodel/TabModelBase$RewoundList;->this$0:Lorg/chromium/chrome/browser/tabmodel/TabModelBase;

    invoke-virtual {v2, v0}, Lorg/chromium/chrome/browser/tabmodel/TabModelBase;->getTabAt(I)Lorg/chromium/chrome/browser/Tab;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 544
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 548
    :cond_0
    return-void
.end method
