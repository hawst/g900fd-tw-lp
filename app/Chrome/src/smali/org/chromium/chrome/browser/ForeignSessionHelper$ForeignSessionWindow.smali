.class public Lorg/chromium/chrome/browser/ForeignSessionHelper$ForeignSessionWindow;
.super Ljava/lang/Object;
.source "ForeignSessionHelper.java"


# instance fields
.field public final sessionId:I

.field public final tabs:Ljava/util/List;

.field public final timestamp:J


# direct methods
.method private constructor <init>(JI)V
    .locals 1

    .prologue
    .line 73
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 71
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lorg/chromium/chrome/browser/ForeignSessionHelper$ForeignSessionWindow;->tabs:Ljava/util/List;

    .line 74
    iput-wide p1, p0, Lorg/chromium/chrome/browser/ForeignSessionHelper$ForeignSessionWindow;->timestamp:J

    .line 75
    iput p3, p0, Lorg/chromium/chrome/browser/ForeignSessionHelper$ForeignSessionWindow;->sessionId:I

    .line 76
    return-void
.end method

.method synthetic constructor <init>(JILorg/chromium/chrome/browser/ForeignSessionHelper$1;)V
    .locals 1

    .prologue
    .line 68
    invoke-direct {p0, p1, p2, p3}, Lorg/chromium/chrome/browser/ForeignSessionHelper$ForeignSessionWindow;-><init>(JI)V

    return-void
.end method
