.class public Lorg/chromium/chrome/browser/autofill/PersonalDataManager$CreditCard;
.super Ljava/lang/Object;
.source "PersonalDataManager.java"


# instance fields
.field private mGUID:Ljava/lang/String;

.field private mMonth:Ljava/lang/String;

.field private mName:Ljava/lang/String;

.field private mNumber:Ljava/lang/String;

.field private mObfuscatedNumber:Ljava/lang/String;

.field private mOrigin:Ljava/lang/String;

.field private mYear:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 246
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 247
    iput-object p1, p0, Lorg/chromium/chrome/browser/autofill/PersonalDataManager$CreditCard;->mGUID:Ljava/lang/String;

    .line 248
    iput-object p2, p0, Lorg/chromium/chrome/browser/autofill/PersonalDataManager$CreditCard;->mOrigin:Ljava/lang/String;

    .line 249
    iput-object p3, p0, Lorg/chromium/chrome/browser/autofill/PersonalDataManager$CreditCard;->mName:Ljava/lang/String;

    .line 250
    iput-object p4, p0, Lorg/chromium/chrome/browser/autofill/PersonalDataManager$CreditCard;->mNumber:Ljava/lang/String;

    .line 251
    iput-object p5, p0, Lorg/chromium/chrome/browser/autofill/PersonalDataManager$CreditCard;->mObfuscatedNumber:Ljava/lang/String;

    .line 252
    iput-object p6, p0, Lorg/chromium/chrome/browser/autofill/PersonalDataManager$CreditCard;->mMonth:Ljava/lang/String;

    .line 253
    iput-object p7, p0, Lorg/chromium/chrome/browser/autofill/PersonalDataManager$CreditCard;->mYear:Ljava/lang/String;

    .line 254
    return-void
.end method

.method public static create(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/chromium/chrome/browser/autofill/PersonalDataManager$CreditCard;
    .locals 8

    .prologue
    .line 242
    new-instance v0, Lorg/chromium/chrome/browser/autofill/PersonalDataManager$CreditCard;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    invoke-direct/range {v0 .. v7}, Lorg/chromium/chrome/browser/autofill/PersonalDataManager$CreditCard;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public getGUID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 258
    iget-object v0, p0, Lorg/chromium/chrome/browser/autofill/PersonalDataManager$CreditCard;->mGUID:Ljava/lang/String;

    return-object v0
.end method

.method public getMonth()Ljava/lang/String;
    .locals 1

    .prologue
    .line 282
    iget-object v0, p0, Lorg/chromium/chrome/browser/autofill/PersonalDataManager$CreditCard;->mMonth:Ljava/lang/String;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 268
    iget-object v0, p0, Lorg/chromium/chrome/browser/autofill/PersonalDataManager$CreditCard;->mName:Ljava/lang/String;

    return-object v0
.end method

.method public getNumber()Ljava/lang/String;
    .locals 1

    .prologue
    .line 273
    iget-object v0, p0, Lorg/chromium/chrome/browser/autofill/PersonalDataManager$CreditCard;->mNumber:Ljava/lang/String;

    return-object v0
.end method

.method public getObfuscatedNumber()Ljava/lang/String;
    .locals 1

    .prologue
    .line 277
    iget-object v0, p0, Lorg/chromium/chrome/browser/autofill/PersonalDataManager$CreditCard;->mObfuscatedNumber:Ljava/lang/String;

    return-object v0
.end method

.method public getOrigin()Ljava/lang/String;
    .locals 1

    .prologue
    .line 263
    iget-object v0, p0, Lorg/chromium/chrome/browser/autofill/PersonalDataManager$CreditCard;->mOrigin:Ljava/lang/String;

    return-object v0
.end method

.method public getYear()Ljava/lang/String;
    .locals 1

    .prologue
    .line 287
    iget-object v0, p0, Lorg/chromium/chrome/browser/autofill/PersonalDataManager$CreditCard;->mYear:Ljava/lang/String;

    return-object v0
.end method
