.class public Lorg/chromium/chrome/browser/signin/SigninManager;
.super Ljava/lang/Object;
.source "SigninManager.java"


# static fields
.field static final synthetic $assertionsDisabled:Z

.field private static sSigninManager:Lorg/chromium/chrome/browser/signin/SigninManager;


# instance fields
.field private final mContext:Landroid/content/Context;

.field private mFirstRunCheckIsPending:Z

.field private final mNativeSigninManagerAndroid:J

.field private mPassive:Z

.field private mPolicyConfirmationDialog:Landroid/app/AlertDialog;

.field private mSignInAccount:Landroid/accounts/Account;

.field private mSignInActivity:Landroid/app/Activity;

.field private final mSignInAllowedObservers:Lorg/chromium/base/ObserverList;

.field private mSignInFlowObserver:Lorg/chromium/chrome/browser/signin/SigninManager$SignInFlowObserver;

.field private final mSignInStateObservers:Lorg/chromium/base/ObserverList;

.field private mSignOutCallback:Ljava/lang/Runnable;

.field private mSignOutProgressDialog:Landroid/app/ProgressDialog;

.field private mSigninAllowedByPolicy:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 41
    const-class v0, Lorg/chromium/chrome/browser/signin/SigninManager;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/chromium/chrome/browser/signin/SigninManager;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 139
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/chromium/chrome/browser/signin/SigninManager;->mFirstRunCheckIsPending:Z

    .line 56
    new-instance v0, Lorg/chromium/base/ObserverList;

    invoke-direct {v0}, Lorg/chromium/base/ObserverList;-><init>()V

    iput-object v0, p0, Lorg/chromium/chrome/browser/signin/SigninManager;->mSignInStateObservers:Lorg/chromium/base/ObserverList;

    .line 58
    new-instance v0, Lorg/chromium/base/ObserverList;

    invoke-direct {v0}, Lorg/chromium/base/ObserverList;-><init>()V

    iput-object v0, p0, Lorg/chromium/chrome/browser/signin/SigninManager;->mSignInAllowedObservers:Lorg/chromium/base/ObserverList;

    .line 64
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/chromium/chrome/browser/signin/SigninManager;->mPassive:Z

    .line 140
    invoke-static {}, Lorg/chromium/base/ThreadUtils;->assertOnUiThread()V

    .line 141
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lorg/chromium/chrome/browser/signin/SigninManager;->mContext:Landroid/content/Context;

    .line 142
    invoke-direct {p0}, Lorg/chromium/chrome/browser/signin/SigninManager;->nativeInit()J

    move-result-wide v0

    iput-wide v0, p0, Lorg/chromium/chrome/browser/signin/SigninManager;->mNativeSigninManagerAndroid:J

    .line 143
    iget-wide v0, p0, Lorg/chromium/chrome/browser/signin/SigninManager;->mNativeSigninManagerAndroid:J

    invoke-direct {p0, v0, v1}, Lorg/chromium/chrome/browser/signin/SigninManager;->nativeIsSigninAllowedByPolicy(J)Z

    move-result v0

    iput-boolean v0, p0, Lorg/chromium/chrome/browser/signin/SigninManager;->mSigninAllowedByPolicy:Z

    .line 144
    return-void
.end method

.method static synthetic access$000(Lorg/chromium/chrome/browser/signin/SigninManager;)Lorg/chromium/base/ObserverList;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lorg/chromium/chrome/browser/signin/SigninManager;->mSignInAllowedObservers:Lorg/chromium/base/ObserverList;

    return-object v0
.end method

.method static synthetic access$100(Lorg/chromium/chrome/browser/signin/SigninManager;)J
    .locals 2

    .prologue
    .line 41
    iget-wide v0, p0, Lorg/chromium/chrome/browser/signin/SigninManager;->mNativeSigninManagerAndroid:J

    return-wide v0
.end method

.method static synthetic access$200(Lorg/chromium/chrome/browser/signin/SigninManager;J)V
    .locals 1

    .prologue
    .line 41
    invoke-direct {p0, p1, p2}, Lorg/chromium/chrome/browser/signin/SigninManager;->nativeFetchPolicyBeforeSignIn(J)V

    return-void
.end method

.method static synthetic access$300(Lorg/chromium/chrome/browser/signin/SigninManager;)Landroid/app/AlertDialog;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lorg/chromium/chrome/browser/signin/SigninManager;->mPolicyConfirmationDialog:Landroid/app/AlertDialog;

    return-object v0
.end method

.method static synthetic access$302(Lorg/chromium/chrome/browser/signin/SigninManager;Landroid/app/AlertDialog;)Landroid/app/AlertDialog;
    .locals 0

    .prologue
    .line 41
    iput-object p1, p0, Lorg/chromium/chrome/browser/signin/SigninManager;->mPolicyConfirmationDialog:Landroid/app/AlertDialog;

    return-object p1
.end method

.method static synthetic access$400(Lorg/chromium/chrome/browser/signin/SigninManager;)V
    .locals 0

    .prologue
    .line 41
    invoke-direct {p0}, Lorg/chromium/chrome/browser/signin/SigninManager;->cancelSignIn()V

    return-void
.end method

.method private cancelSignIn()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 406
    iget-object v0, p0, Lorg/chromium/chrome/browser/signin/SigninManager;->mSignInFlowObserver:Lorg/chromium/chrome/browser/signin/SigninManager$SignInFlowObserver;

    if-eqz v0, :cond_0

    .line 407
    iget-object v0, p0, Lorg/chromium/chrome/browser/signin/SigninManager;->mSignInFlowObserver:Lorg/chromium/chrome/browser/signin/SigninManager$SignInFlowObserver;

    invoke-interface {v0}, Lorg/chromium/chrome/browser/signin/SigninManager$SignInFlowObserver;->onSigninCancelled()V

    .line 408
    :cond_0
    iput-object v1, p0, Lorg/chromium/chrome/browser/signin/SigninManager;->mSignInActivity:Landroid/app/Activity;

    .line 409
    iput-object v1, p0, Lorg/chromium/chrome/browser/signin/SigninManager;->mSignInFlowObserver:Lorg/chromium/chrome/browser/signin/SigninManager$SignInFlowObserver;

    .line 410
    iput-object v1, p0, Lorg/chromium/chrome/browser/signin/SigninManager;->mSignInAccount:Landroid/accounts/Account;

    .line 411
    invoke-direct {p0}, Lorg/chromium/chrome/browser/signin/SigninManager;->notifySignInAllowedChanged()V

    .line 412
    return-void
.end method

.method private doSignIn()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 322
    const-string/jumbo v0, "SigninManager"

    const-string/jumbo v1, "Committing the sign-in process now"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 323
    sget-boolean v0, Lorg/chromium/chrome/browser/signin/SigninManager;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lorg/chromium/chrome/browser/signin/SigninManager;->mSignInAccount:Landroid/accounts/Account;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 326
    :cond_0
    iget-object v0, p0, Lorg/chromium/chrome/browser/signin/SigninManager;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lorg/chromium/sync/signin/ChromeSigninController;->get(Landroid/content/Context;)Lorg/chromium/sync/signin/ChromeSigninController;

    move-result-object v0

    iget-object v1, p0, Lorg/chromium/chrome/browser/signin/SigninManager;->mSignInAccount:Landroid/accounts/Account;

    iget-object v1, v1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lorg/chromium/sync/signin/ChromeSigninController;->setSignedInAccountName(Ljava/lang/String;)V

    .line 329
    iget-wide v0, p0, Lorg/chromium/chrome/browser/signin/SigninManager;->mNativeSigninManagerAndroid:J

    iget-object v2, p0, Lorg/chromium/chrome/browser/signin/SigninManager;->mSignInAccount:Landroid/accounts/Account;

    iget-object v2, v2, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-direct {p0, v0, v1, v2}, Lorg/chromium/chrome/browser/signin/SigninManager;->nativeOnSignInCompleted(JLjava/lang/String;)V

    .line 332
    iget-object v0, p0, Lorg/chromium/chrome/browser/signin/SigninManager;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lorg/chromium/chrome/browser/invalidation/InvalidationController;->get(Landroid/content/Context;)Lorg/chromium/chrome/browser/invalidation/InvalidationController;

    move-result-object v0

    .line 333
    iget-object v1, p0, Lorg/chromium/chrome/browser/signin/SigninManager;->mSignInAccount:Landroid/accounts/Account;

    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    invoke-virtual {v0, v1, v4, v2}, Lorg/chromium/chrome/browser/invalidation/InvalidationController;->setRegisteredTypes(Landroid/accounts/Account;ZLjava/util/Set;)V

    .line 336
    iget-object v0, p0, Lorg/chromium/chrome/browser/signin/SigninManager;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->get(Landroid/content/Context;)Lorg/chromium/chrome/browser/sync/ProfileSyncService;

    move-result-object v0

    .line 337
    iget-object v1, p0, Lorg/chromium/chrome/browser/signin/SigninManager;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lorg/chromium/sync/notifier/SyncStatusHelper;->get(Landroid/content/Context;)Lorg/chromium/sync/notifier/SyncStatusHelper;

    move-result-object v1

    iget-object v2, p0, Lorg/chromium/chrome/browser/signin/SigninManager;->mSignInAccount:Landroid/accounts/Account;

    invoke-virtual {v1, v2}, Lorg/chromium/sync/notifier/SyncStatusHelper;->isSyncEnabled(Landroid/accounts/Account;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->hasSyncSetupCompleted()Z

    move-result v1

    if-nez v1, :cond_1

    .line 339
    invoke-virtual {v0, v4}, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->setSetupInProgress(Z)V

    .line 340
    invoke-virtual {v0}, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->syncSignIn()V

    .line 343
    :cond_1
    iget-object v0, p0, Lorg/chromium/chrome/browser/signin/SigninManager;->mSignInFlowObserver:Lorg/chromium/chrome/browser/signin/SigninManager$SignInFlowObserver;

    if-eqz v0, :cond_2

    .line 344
    iget-object v0, p0, Lorg/chromium/chrome/browser/signin/SigninManager;->mSignInFlowObserver:Lorg/chromium/chrome/browser/signin/SigninManager$SignInFlowObserver;

    invoke-interface {v0}, Lorg/chromium/chrome/browser/signin/SigninManager$SignInFlowObserver;->onSigninComplete()V

    .line 347
    :cond_2
    const-string/jumbo v0, "SigninManager"

    const-string/jumbo v1, "Signin done"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 348
    iput-object v3, p0, Lorg/chromium/chrome/browser/signin/SigninManager;->mSignInActivity:Landroid/app/Activity;

    .line 349
    iput-object v3, p0, Lorg/chromium/chrome/browser/signin/SigninManager;->mSignInAccount:Landroid/accounts/Account;

    .line 350
    iput-object v3, p0, Lorg/chromium/chrome/browser/signin/SigninManager;->mSignInFlowObserver:Lorg/chromium/chrome/browser/signin/SigninManager$SignInFlowObserver;

    .line 352
    invoke-direct {p0}, Lorg/chromium/chrome/browser/signin/SigninManager;->notifySignInAllowedChanged()V

    .line 353
    iget-object v0, p0, Lorg/chromium/chrome/browser/signin/SigninManager;->mSignInStateObservers:Lorg/chromium/base/ObserverList;

    invoke-virtual {v0}, Lorg/chromium/base/ObserverList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/chromium/chrome/browser/signin/SigninManager$SignInStateObserver;

    .line 354
    invoke-interface {v0}, Lorg/chromium/chrome/browser/signin/SigninManager$SignInStateObserver;->onSignedIn()V

    goto :goto_0

    .line 356
    :cond_3
    return-void
.end method

.method public static get(Landroid/content/Context;)Lorg/chromium/chrome/browser/signin/SigninManager;
    .locals 1

    .prologue
    .line 132
    invoke-static {}, Lorg/chromium/base/ThreadUtils;->assertOnUiThread()V

    .line 133
    sget-object v0, Lorg/chromium/chrome/browser/signin/SigninManager;->sSigninManager:Lorg/chromium/chrome/browser/signin/SigninManager;

    if-nez v0, :cond_0

    .line 134
    new-instance v0, Lorg/chromium/chrome/browser/signin/SigninManager;

    invoke-direct {v0, p0}, Lorg/chromium/chrome/browser/signin/SigninManager;-><init>(Landroid/content/Context;)V

    sput-object v0, Lorg/chromium/chrome/browser/signin/SigninManager;->sSigninManager:Lorg/chromium/chrome/browser/signin/SigninManager;

    .line 136
    :cond_0
    sget-object v0, Lorg/chromium/chrome/browser/signin/SigninManager;->sSigninManager:Lorg/chromium/chrome/browser/signin/SigninManager;

    return-object v0
.end method

.method public static getAndroidSigninPromoExperimentGroup()I
    .locals 1

    .prologue
    .line 458
    const-string/jumbo v0, "AndroidSigninPromo"

    invoke-static {v0}, Lorg/chromium/base/FieldTrialList;->findFullName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 461
    :try_start_0
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 463
    :goto_0
    return v0

    :catch_0
    move-exception v0

    const/4 v0, -0x1

    goto :goto_0
.end method

.method public static isNewProfileManagementEnabled()Z
    .locals 1

    .prologue
    .line 448
    invoke-static {}, Lorg/chromium/chrome/browser/signin/SigninManager;->nativeIsNewProfileManagementEnabled()Z

    move-result v0

    return v0
.end method

.method private native nativeCheckPolicyBeforeSignIn(JLjava/lang/String;)V
.end method

.method private native nativeClearLastSignedInUser(J)V
.end method

.method private native nativeFetchPolicyBeforeSignIn(J)V
.end method

.method private native nativeGetManagementDomain(J)Ljava/lang/String;
.end method

.method private native nativeInit()J
.end method

.method private static native nativeIsNewProfileManagementEnabled()Z
.end method

.method private native nativeIsSigninAllowedByPolicy(J)Z
.end method

.method private native nativeLogInSignedInUser(J)V
.end method

.method private native nativeOnSignInCompleted(JLjava/lang/String;)V
.end method

.method private native nativeShouldLoadPolicyForUser(Ljava/lang/String;)Z
.end method

.method private native nativeSignOut(J)V
.end method

.method private native nativeWipeProfileData(J)V
.end method

.method private notifySignInAllowedChanged()V
    .locals 2

    .prologue
    .line 199
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v1, Lorg/chromium/chrome/browser/signin/SigninManager$1;

    invoke-direct {v1, p0}, Lorg/chromium/chrome/browser/signin/SigninManager$1;-><init>(Lorg/chromium/chrome/browser/signin/SigninManager;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 207
    return-void
.end method

.method private onPolicyCheckedBeforeSignIn(Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 253
    if-nez p1, :cond_0

    .line 254
    const-string/jumbo v0, "SigninManager"

    const-string/jumbo v1, "Account doesn\'t have policy"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 255
    invoke-direct {p0}, Lorg/chromium/chrome/browser/signin/SigninManager;->doSignIn()V

    .line 312
    :goto_0
    return-void

    .line 259
    :cond_0
    iget-object v0, p0, Lorg/chromium/chrome/browser/signin/SigninManager;->mSignInActivity:Landroid/app/Activity;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lorg/chromium/chrome/browser/signin/SigninManager;->mSignInActivity:Landroid/app/Activity;

    invoke-static {v0}, Lorg/chromium/base/ApplicationStatus;->getStateForActivity(Landroid/app/Activity;)I

    move-result v0

    const/4 v1, 0x6

    if-ne v0, v1, :cond_1

    .line 262
    invoke-direct {p0}, Lorg/chromium/chrome/browser/signin/SigninManager;->cancelSignIn()V

    goto :goto_0

    .line 266
    :cond_1
    iget-boolean v0, p0, Lorg/chromium/chrome/browser/signin/SigninManager;->mPassive:Z

    if-eqz v0, :cond_2

    .line 269
    iget-wide v0, p0, Lorg/chromium/chrome/browser/signin/SigninManager;->mNativeSigninManagerAndroid:J

    invoke-direct {p0, v0, v1}, Lorg/chromium/chrome/browser/signin/SigninManager;->nativeFetchPolicyBeforeSignIn(J)V

    goto :goto_0

    .line 273
    :cond_2
    const-string/jumbo v0, "SigninManager"

    const-string/jumbo v1, "Account has policy management"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 274
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lorg/chromium/chrome/browser/signin/SigninManager;->mSignInActivity:Landroid/app/Activity;

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 275
    sget v1, Lorg/chromium/chrome/R$string;->policy_dialog_title:I

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 276
    iget-object v1, p0, Lorg/chromium/chrome/browser/signin/SigninManager;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lorg/chromium/chrome/R$string;->policy_dialog_message:I

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 278
    sget v1, Lorg/chromium/chrome/R$string;->policy_dialog_proceed:I

    new-instance v2, Lorg/chromium/chrome/browser/signin/SigninManager$2;

    invoke-direct {v2, p0}, Lorg/chromium/chrome/browser/signin/SigninManager$2;-><init>(Lorg/chromium/chrome/browser/signin/SigninManager;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 289
    sget v1, Lorg/chromium/chrome/R$string;->policy_dialog_cancel:I

    new-instance v2, Lorg/chromium/chrome/browser/signin/SigninManager$3;

    invoke-direct {v2, p0}, Lorg/chromium/chrome/browser/signin/SigninManager$3;-><init>(Lorg/chromium/chrome/browser/signin/SigninManager;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 299
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lorg/chromium/chrome/browser/signin/SigninManager;->mPolicyConfirmationDialog:Landroid/app/AlertDialog;

    .line 300
    iget-object v0, p0, Lorg/chromium/chrome/browser/signin/SigninManager;->mPolicyConfirmationDialog:Landroid/app/AlertDialog;

    new-instance v1, Lorg/chromium/chrome/browser/signin/SigninManager$4;

    invoke-direct {v1, p0}, Lorg/chromium/chrome/browser/signin/SigninManager$4;-><init>(Lorg/chromium/chrome/browser/signin/SigninManager;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 311
    iget-object v0, p0, Lorg/chromium/chrome/browser/signin/SigninManager;->mPolicyConfirmationDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    goto :goto_0
.end method

.method private onPolicyFetchedBeforeSignIn()V
    .locals 0

    .prologue
    .line 318
    invoke-direct {p0}, Lorg/chromium/chrome/browser/signin/SigninManager;->doSignIn()V

    .line 319
    return-void
.end method

.method private onProfileDataWiped()V
    .locals 1

    .prologue
    .line 432
    iget-object v0, p0, Lorg/chromium/chrome/browser/signin/SigninManager;->mSignOutProgressDialog:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/chromium/chrome/browser/signin/SigninManager;->mSignOutProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 433
    iget-object v0, p0, Lorg/chromium/chrome/browser/signin/SigninManager;->mSignOutProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 434
    :cond_0
    invoke-direct {p0}, Lorg/chromium/chrome/browser/signin/SigninManager;->onSignOutDone()V

    .line 435
    return-void
.end method

.method private onSignOutDone()V
    .locals 2

    .prologue
    .line 438
    iget-object v0, p0, Lorg/chromium/chrome/browser/signin/SigninManager;->mSignOutCallback:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    .line 439
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iget-object v1, p0, Lorg/chromium/chrome/browser/signin/SigninManager;->mSignOutCallback:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 440
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/chromium/chrome/browser/signin/SigninManager;->mSignOutCallback:Ljava/lang/Runnable;

    .line 442
    :cond_0
    return-void
.end method

.method private onSigninAllowedByPolicyChanged(Z)V
    .locals 0

    .prologue
    .line 469
    iput-boolean p1, p0, Lorg/chromium/chrome/browser/signin/SigninManager;->mSigninAllowedByPolicy:Z

    .line 470
    invoke-direct {p0}, Lorg/chromium/chrome/browser/signin/SigninManager;->notifySignInAllowedChanged()V

    .line 471
    return-void
.end method

.method private wipeProfileData(Landroid/app/Activity;)V
    .locals 4

    .prologue
    .line 415
    if-eqz p1, :cond_0

    .line 417
    sget v0, Lorg/chromium/chrome/R$string;->wiping_profile_data_title:I

    invoke-virtual {p1, v0}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v0

    sget v1, Lorg/chromium/chrome/R$string;->wiping_profile_data_message:I

    invoke-virtual {p1, v1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-static {p1, v0, v1, v2, v3}, Landroid/app/ProgressDialog;->show(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ZZ)Landroid/app/ProgressDialog;

    move-result-object v0

    iput-object v0, p0, Lorg/chromium/chrome/browser/signin/SigninManager;->mSignOutProgressDialog:Landroid/app/ProgressDialog;

    .line 427
    :cond_0
    iget-wide v0, p0, Lorg/chromium/chrome/browser/signin/SigninManager;->mNativeSigninManagerAndroid:J

    invoke-direct {p0, v0, v1}, Lorg/chromium/chrome/browser/signin/SigninManager;->nativeWipeProfileData(J)V

    .line 428
    return-void
.end method


# virtual methods
.method public addSignInAllowedObserver(Lorg/chromium/chrome/browser/signin/SigninManager$SignInAllowedObserver;)V
    .locals 1

    .prologue
    .line 191
    iget-object v0, p0, Lorg/chromium/chrome/browser/signin/SigninManager;->mSignInAllowedObservers:Lorg/chromium/base/ObserverList;

    invoke-virtual {v0, p1}, Lorg/chromium/base/ObserverList;->addObserver(Ljava/lang/Object;)Z

    .line 192
    return-void
.end method

.method public addSignInStateObserver(Lorg/chromium/chrome/browser/signin/SigninManager$SignInStateObserver;)V
    .locals 1

    .prologue
    .line 180
    iget-object v0, p0, Lorg/chromium/chrome/browser/signin/SigninManager;->mSignInStateObservers:Lorg/chromium/base/ObserverList;

    invoke-virtual {v0, p1}, Lorg/chromium/base/ObserverList;->addObserver(Ljava/lang/Object;)Z

    .line 181
    return-void
.end method

.method public clearLastSignedInUser()V
    .locals 2

    .prologue
    .line 402
    iget-wide v0, p0, Lorg/chromium/chrome/browser/signin/SigninManager;->mNativeSigninManagerAndroid:J

    invoke-direct {p0, v0, v1}, Lorg/chromium/chrome/browser/signin/SigninManager;->nativeClearLastSignedInUser(J)V

    .line 403
    return-void
.end method

.method public getManagementDomain()Ljava/lang/String;
    .locals 2

    .prologue
    .line 394
    iget-wide v0, p0, Lorg/chromium/chrome/browser/signin/SigninManager;->mNativeSigninManagerAndroid:J

    invoke-direct {p0, v0, v1}, Lorg/chromium/chrome/browser/signin/SigninManager;->nativeGetManagementDomain(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public isSignInAllowed()Z
    .locals 1

    .prologue
    .line 163
    iget-boolean v0, p0, Lorg/chromium/chrome/browser/signin/SigninManager;->mSigninAllowedByPolicy:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lorg/chromium/chrome/browser/signin/SigninManager;->mFirstRunCheckIsPending:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lorg/chromium/chrome/browser/signin/SigninManager;->mSignInAccount:Landroid/accounts/Account;

    if-nez v0, :cond_0

    iget-object v0, p0, Lorg/chromium/chrome/browser/signin/SigninManager;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lorg/chromium/sync/signin/ChromeSigninController;->get(Landroid/content/Context;)Lorg/chromium/sync/signin/ChromeSigninController;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/sync/signin/ChromeSigninController;->getSignedInUser()Landroid/accounts/Account;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isSigninDisabledByPolicy()Z
    .locals 1

    .prologue
    .line 173
    iget-boolean v0, p0, Lorg/chromium/chrome/browser/signin/SigninManager;->mSigninAllowedByPolicy:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public logInSignedInUser()V
    .locals 2

    .prologue
    .line 398
    iget-wide v0, p0, Lorg/chromium/chrome/browser/signin/SigninManager;->mNativeSigninManagerAndroid:J

    invoke-direct {p0, v0, v1}, Lorg/chromium/chrome/browser/signin/SigninManager;->nativeLogInSignedInUser(J)V

    .line 399
    return-void
.end method

.method public onFirstRunCheckDone()V
    .locals 1

    .prologue
    .line 152
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/chromium/chrome/browser/signin/SigninManager;->mFirstRunCheckIsPending:Z

    .line 154
    invoke-virtual {p0}, Lorg/chromium/chrome/browser/signin/SigninManager;->isSignInAllowed()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 155
    invoke-direct {p0}, Lorg/chromium/chrome/browser/signin/SigninManager;->notifySignInAllowedChanged()V

    .line 157
    :cond_0
    return-void
.end method

.method public removeSignInAllowedObserver(Lorg/chromium/chrome/browser/signin/SigninManager$SignInAllowedObserver;)V
    .locals 1

    .prologue
    .line 195
    iget-object v0, p0, Lorg/chromium/chrome/browser/signin/SigninManager;->mSignInAllowedObservers:Lorg/chromium/base/ObserverList;

    invoke-virtual {v0, p1}, Lorg/chromium/base/ObserverList;->removeObserver(Ljava/lang/Object;)Z

    .line 196
    return-void
.end method

.method public removeSignInStateObserver(Lorg/chromium/chrome/browser/signin/SigninManager$SignInStateObserver;)V
    .locals 1

    .prologue
    .line 187
    iget-object v0, p0, Lorg/chromium/chrome/browser/signin/SigninManager;->mSignInStateObservers:Lorg/chromium/base/ObserverList;

    invoke-virtual {v0, p1}, Lorg/chromium/base/ObserverList;->removeObserver(Ljava/lang/Object;)Z

    .line 188
    return-void
.end method

.method public signOut(Landroid/app/Activity;Ljava/lang/Runnable;)V
    .locals 4

    .prologue
    .line 370
    iput-object p2, p0, Lorg/chromium/chrome/browser/signin/SigninManager;->mSignOutCallback:Ljava/lang/Runnable;

    .line 372
    invoke-virtual {p0}, Lorg/chromium/chrome/browser/signin/SigninManager;->getManagementDomain()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 373
    :goto_0
    const-string/jumbo v1, "SigninManager"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "Signing out, wipe data? "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 375
    iget-object v1, p0, Lorg/chromium/chrome/browser/signin/SigninManager;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lorg/chromium/sync/signin/ChromeSigninController;->get(Landroid/content/Context;)Lorg/chromium/sync/signin/ChromeSigninController;

    move-result-object v1

    invoke-virtual {v1}, Lorg/chromium/sync/signin/ChromeSigninController;->clearSignedInUser()V

    .line 376
    iget-object v1, p0, Lorg/chromium/chrome/browser/signin/SigninManager;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->get(Landroid/content/Context;)Lorg/chromium/chrome/browser/sync/ProfileSyncService;

    move-result-object v1

    invoke-virtual {v1}, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->signOut()V

    .line 377
    iget-wide v2, p0, Lorg/chromium/chrome/browser/signin/SigninManager;->mNativeSigninManagerAndroid:J

    invoke-direct {p0, v2, v3}, Lorg/chromium/chrome/browser/signin/SigninManager;->nativeSignOut(J)V

    .line 379
    if-eqz v0, :cond_1

    .line 380
    invoke-direct {p0, p1}, Lorg/chromium/chrome/browser/signin/SigninManager;->wipeProfileData(Landroid/app/Activity;)V

    .line 385
    :goto_1
    iget-object v0, p0, Lorg/chromium/chrome/browser/signin/SigninManager;->mSignInStateObservers:Lorg/chromium/base/ObserverList;

    invoke-virtual {v0}, Lorg/chromium/base/ObserverList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/chromium/chrome/browser/signin/SigninManager$SignInStateObserver;

    .line 386
    invoke-interface {v0}, Lorg/chromium/chrome/browser/signin/SigninManager$SignInStateObserver;->onSignedOut()V

    goto :goto_2

    .line 372
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 382
    :cond_1
    invoke-direct {p0}, Lorg/chromium/chrome/browser/signin/SigninManager;->onSignOutDone()V

    goto :goto_1

    .line 388
    :cond_2
    return-void
.end method

.method public startSignIn(Landroid/app/Activity;Landroid/accounts/Account;ZLorg/chromium/chrome/browser/signin/SigninManager$SignInFlowObserver;)V
    .locals 3

    .prologue
    .line 223
    sget-boolean v0, Lorg/chromium/chrome/browser/signin/SigninManager;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lorg/chromium/chrome/browser/signin/SigninManager;->mSignInActivity:Landroid/app/Activity;

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 224
    :cond_0
    sget-boolean v0, Lorg/chromium/chrome/browser/signin/SigninManager;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lorg/chromium/chrome/browser/signin/SigninManager;->mSignInAccount:Landroid/accounts/Account;

    if-eqz v0, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 225
    :cond_1
    sget-boolean v0, Lorg/chromium/chrome/browser/signin/SigninManager;->$assertionsDisabled:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lorg/chromium/chrome/browser/signin/SigninManager;->mSignInFlowObserver:Lorg/chromium/chrome/browser/signin/SigninManager$SignInFlowObserver;

    if-eqz v0, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 227
    :cond_2
    iget-boolean v0, p0, Lorg/chromium/chrome/browser/signin/SigninManager;->mFirstRunCheckIsPending:Z

    if-eqz v0, :cond_3

    .line 228
    const-string/jumbo v0, "SigninManager"

    const-string/jumbo v1, "Ignoring sign-in request until the First Run check completes."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 249
    :goto_0
    return-void

    .line 232
    :cond_3
    iput-object p1, p0, Lorg/chromium/chrome/browser/signin/SigninManager;->mSignInActivity:Landroid/app/Activity;

    .line 233
    iput-object p2, p0, Lorg/chromium/chrome/browser/signin/SigninManager;->mSignInAccount:Landroid/accounts/Account;

    .line 234
    iput-object p4, p0, Lorg/chromium/chrome/browser/signin/SigninManager;->mSignInFlowObserver:Lorg/chromium/chrome/browser/signin/SigninManager$SignInFlowObserver;

    .line 235
    iput-boolean p3, p0, Lorg/chromium/chrome/browser/signin/SigninManager;->mPassive:Z

    .line 237
    invoke-direct {p0}, Lorg/chromium/chrome/browser/signin/SigninManager;->notifySignInAllowedChanged()V

    .line 239
    iget-object v0, p2, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-direct {p0, v0}, Lorg/chromium/chrome/browser/signin/SigninManager;->nativeShouldLoadPolicyForUser(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 242
    invoke-direct {p0}, Lorg/chromium/chrome/browser/signin/SigninManager;->doSignIn()V

    goto :goto_0

    .line 246
    :cond_4
    const-string/jumbo v0, "SigninManager"

    const-string/jumbo v1, "Checking if account has policy management enabled"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 248
    iget-wide v0, p0, Lorg/chromium/chrome/browser/signin/SigninManager;->mNativeSigninManagerAndroid:J

    iget-object v2, p2, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-direct {p0, v0, v1, v2}, Lorg/chromium/chrome/browser/signin/SigninManager;->nativeCheckPolicyBeforeSignIn(JLjava/lang/String;)V

    goto :goto_0
.end method
