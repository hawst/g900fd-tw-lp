.class Lorg/chromium/chrome/browser/infobar/AnimationHelper$1;
.super Landroid/animation/AnimatorListenerAdapter;
.source "AnimationHelper.java"


# instance fields
.field final synthetic this$0:Lorg/chromium/chrome/browser/infobar/AnimationHelper;


# direct methods
.method constructor <init>(Lorg/chromium/chrome/browser/infobar/AnimationHelper;)V
    .locals 0

    .prologue
    .line 212
    iput-object p1, p0, Lorg/chromium/chrome/browser/infobar/AnimationHelper$1;->this$0:Lorg/chromium/chrome/browser/infobar/AnimationHelper;

    invoke-direct {p0}, Landroid/animation/AnimatorListenerAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 2

    .prologue
    .line 220
    iget-object v0, p0, Lorg/chromium/chrome/browser/infobar/AnimationHelper$1;->this$0:Lorg/chromium/chrome/browser/infobar/AnimationHelper;

    # getter for: Lorg/chromium/chrome/browser/infobar/AnimationHelper;->mTargetWrapperView:Lorg/chromium/chrome/browser/infobar/ContentWrapperView;
    invoke-static {v0}, Lorg/chromium/chrome/browser/infobar/AnimationHelper;->access$000(Lorg/chromium/chrome/browser/infobar/AnimationHelper;)Lorg/chromium/chrome/browser/infobar/ContentWrapperView;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/infobar/ContentWrapperView;->finishTransition()V

    .line 221
    iget-object v0, p0, Lorg/chromium/chrome/browser/infobar/AnimationHelper$1;->this$0:Lorg/chromium/chrome/browser/infobar/AnimationHelper;

    # getter for: Lorg/chromium/chrome/browser/infobar/AnimationHelper;->mContainer:Lorg/chromium/chrome/browser/infobar/InfoBarContainer;
    invoke-static {v0}, Lorg/chromium/chrome/browser/infobar/AnimationHelper;->access$100(Lorg/chromium/chrome/browser/infobar/AnimationHelper;)Lorg/chromium/chrome/browser/infobar/InfoBarContainer;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/infobar/InfoBarContainer;->finishTransition()V

    .line 223
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_1

    iget-object v0, p0, Lorg/chromium/chrome/browser/infobar/AnimationHelper$1;->this$0:Lorg/chromium/chrome/browser/infobar/AnimationHelper;

    # getter for: Lorg/chromium/chrome/browser/infobar/AnimationHelper;->mToShow:Landroid/view/View;
    invoke-static {v0}, Lorg/chromium/chrome/browser/infobar/AnimationHelper;->access$200(Lorg/chromium/chrome/browser/infobar/AnimationHelper;)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lorg/chromium/chrome/browser/infobar/AnimationHelper$1;->this$0:Lorg/chromium/chrome/browser/infobar/AnimationHelper;

    # getter for: Lorg/chromium/chrome/browser/infobar/AnimationHelper;->mAnimationType:I
    invoke-static {v0}, Lorg/chromium/chrome/browser/infobar/AnimationHelper;->access$300(Lorg/chromium/chrome/browser/infobar/AnimationHelper;)I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/chromium/chrome/browser/infobar/AnimationHelper$1;->this$0:Lorg/chromium/chrome/browser/infobar/AnimationHelper;

    # getter for: Lorg/chromium/chrome/browser/infobar/AnimationHelper;->mAnimationType:I
    invoke-static {v0}, Lorg/chromium/chrome/browser/infobar/AnimationHelper;->access$300(Lorg/chromium/chrome/browser/infobar/AnimationHelper;)I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 226
    :cond_0
    iget-object v0, p0, Lorg/chromium/chrome/browser/infobar/AnimationHelper$1;->this$0:Lorg/chromium/chrome/browser/infobar/AnimationHelper;

    # getter for: Lorg/chromium/chrome/browser/infobar/AnimationHelper;->mToShow:Landroid/view/View;
    invoke-static {v0}, Lorg/chromium/chrome/browser/infobar/AnimationHelper;->access$200(Lorg/chromium/chrome/browser/infobar/AnimationHelper;)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lorg/chromium/chrome/browser/infobar/AnimationHelper$1;->this$0:Lorg/chromium/chrome/browser/infobar/AnimationHelper;

    # getter for: Lorg/chromium/chrome/browser/infobar/AnimationHelper;->mInfoBar:Lorg/chromium/chrome/browser/infobar/InfoBar;
    invoke-static {v1}, Lorg/chromium/chrome/browser/infobar/AnimationHelper;->access$400(Lorg/chromium/chrome/browser/infobar/AnimationHelper;)Lorg/chromium/chrome/browser/infobar/InfoBar;

    move-result-object v1

    invoke-virtual {v1}, Lorg/chromium/chrome/browser/infobar/InfoBar;->getMessage()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->announceForAccessibility(Ljava/lang/CharSequence;)V

    .line 228
    :cond_1
    return-void
.end method

.method public onAnimationStart(Landroid/animation/Animator;)V
    .locals 1

    .prologue
    .line 215
    iget-object v0, p0, Lorg/chromium/chrome/browser/infobar/AnimationHelper$1;->this$0:Lorg/chromium/chrome/browser/infobar/AnimationHelper;

    # getter for: Lorg/chromium/chrome/browser/infobar/AnimationHelper;->mTargetWrapperView:Lorg/chromium/chrome/browser/infobar/ContentWrapperView;
    invoke-static {v0}, Lorg/chromium/chrome/browser/infobar/AnimationHelper;->access$000(Lorg/chromium/chrome/browser/infobar/AnimationHelper;)Lorg/chromium/chrome/browser/infobar/ContentWrapperView;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/infobar/ContentWrapperView;->startTransition()V

    .line 216
    return-void
.end method
