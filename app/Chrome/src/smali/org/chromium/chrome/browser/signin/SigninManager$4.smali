.class Lorg/chromium/chrome/browser/signin/SigninManager$4;
.super Ljava/lang/Object;
.source "SigninManager.java"

# interfaces
.implements Landroid/content/DialogInterface$OnDismissListener;


# instance fields
.field final synthetic this$0:Lorg/chromium/chrome/browser/signin/SigninManager;


# direct methods
.method constructor <init>(Lorg/chromium/chrome/browser/signin/SigninManager;)V
    .locals 0

    .prologue
    .line 301
    iput-object p1, p0, Lorg/chromium/chrome/browser/signin/SigninManager$4;->this$0:Lorg/chromium/chrome/browser/signin/SigninManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDismiss(Landroid/content/DialogInterface;)V
    .locals 2

    .prologue
    .line 304
    iget-object v0, p0, Lorg/chromium/chrome/browser/signin/SigninManager$4;->this$0:Lorg/chromium/chrome/browser/signin/SigninManager;

    # getter for: Lorg/chromium/chrome/browser/signin/SigninManager;->mPolicyConfirmationDialog:Landroid/app/AlertDialog;
    invoke-static {v0}, Lorg/chromium/chrome/browser/signin/SigninManager;->access$300(Lorg/chromium/chrome/browser/signin/SigninManager;)Landroid/app/AlertDialog;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 305
    const-string/jumbo v0, "SigninManager"

    const-string/jumbo v1, "Policy dialog dismissed, cancelling sign-in."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 306
    iget-object v0, p0, Lorg/chromium/chrome/browser/signin/SigninManager$4;->this$0:Lorg/chromium/chrome/browser/signin/SigninManager;

    # invokes: Lorg/chromium/chrome/browser/signin/SigninManager;->cancelSignIn()V
    invoke-static {v0}, Lorg/chromium/chrome/browser/signin/SigninManager;->access$400(Lorg/chromium/chrome/browser/signin/SigninManager;)V

    .line 307
    iget-object v0, p0, Lorg/chromium/chrome/browser/signin/SigninManager$4;->this$0:Lorg/chromium/chrome/browser/signin/SigninManager;

    const/4 v1, 0x0

    # setter for: Lorg/chromium/chrome/browser/signin/SigninManager;->mPolicyConfirmationDialog:Landroid/app/AlertDialog;
    invoke-static {v0, v1}, Lorg/chromium/chrome/browser/signin/SigninManager;->access$302(Lorg/chromium/chrome/browser/signin/SigninManager;Landroid/app/AlertDialog;)Landroid/app/AlertDialog;

    .line 309
    :cond_0
    return-void
.end method
