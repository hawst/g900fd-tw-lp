.class public Lorg/chromium/chrome/browser/infobar/ContentWrapperView;
.super Landroid/widget/FrameLayout;
.source "ContentWrapperView.java"


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private final mGravity:I

.field private final mInfoBar:Lorg/chromium/chrome/browser/infobar/InfoBar;

.field private mViewToHide:Landroid/view/View;

.field private mViewToShow:Landroid/view/View;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 33
    const-class v0, Lorg/chromium/chrome/browser/infobar/ContentWrapperView;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/chromium/chrome/browser/infobar/ContentWrapperView;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;Lorg/chromium/chrome/browser/infobar/InfoBar;Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v4, -0x1

    .line 49
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 50
    iput-object p2, p0, Lorg/chromium/chrome/browser/infobar/ContentWrapperView;->mInfoBar:Lorg/chromium/chrome/browser/infobar/InfoBar;

    .line 51
    const/16 v0, 0x30

    iput v0, p0, Lorg/chromium/chrome/browser/infobar/ContentWrapperView;->mGravity:I

    .line 54
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 55
    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    const/4 v2, -0x2

    invoke-direct {v1, v4, v2}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 57
    invoke-virtual {p0, v1}, Lorg/chromium/chrome/browser/infobar/ContentWrapperView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 58
    sget v1, Lorg/chromium/chrome/R$color;->infobar_background:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {p0, v1}, Lorg/chromium/chrome/browser/infobar/ContentWrapperView;->setBackgroundColor(I)V

    .line 61
    new-instance v1, Landroid/view/View;

    invoke-direct {v1, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 62
    sget v2, Lorg/chromium/chrome/R$color;->infobar_background_separator:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/view/View;->setBackgroundColor(I)V

    .line 63
    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    invoke-static {p1}, Lorg/chromium/chrome/browser/infobar/ContentWrapperView;->getBoundaryHeight(Landroid/content/Context;)I

    move-result v2

    iget v3, p0, Lorg/chromium/chrome/browser/infobar/ContentWrapperView;->mGravity:I

    invoke-direct {v0, v4, v2, v3}, Landroid/widget/FrameLayout$LayoutParams;-><init>(III)V

    invoke-virtual {p0, v1, v0}, Lorg/chromium/chrome/browser/infobar/ContentWrapperView;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 67
    invoke-direct {p0, p3}, Lorg/chromium/chrome/browser/infobar/ContentWrapperView;->addChildView(Landroid/view/View;)V

    .line 68
    return-void
.end method

.method private addChildView(Landroid/view/View;)V
    .locals 5

    .prologue
    .line 116
    const/4 v0, 0x0

    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    const/4 v2, -0x1

    const/4 v3, -0x2

    iget v4, p0, Lorg/chromium/chrome/browser/infobar/ContentWrapperView;->mGravity:I

    invoke-direct {v1, v2, v3, v4}, Landroid/widget/FrameLayout$LayoutParams;-><init>(III)V

    invoke-virtual {p0, p1, v0, v1}, Lorg/chromium/chrome/browser/infobar/ContentWrapperView;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    .line 118
    return-void
.end method

.method static getBoundaryHeight(Landroid/content/Context;)I
    .locals 2

    .prologue
    .line 87
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    .line 88
    const/high16 v1, 0x40000000    # 2.0f

    cmpg-float v0, v0, v1

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x2

    goto :goto_0
.end method

.method private getViewToHideHeight()I
    .locals 1

    .prologue
    .line 187
    iget-object v0, p0, Lorg/chromium/chrome/browser/infobar/ContentWrapperView;->mViewToHide:Landroid/view/View;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lorg/chromium/chrome/browser/infobar/ContentWrapperView;->mViewToHide:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v0

    goto :goto_0
.end method

.method private getViewToShowHeight()I
    .locals 1

    .prologue
    .line 178
    iget-object v0, p0, Lorg/chromium/chrome/browser/infobar/ContentWrapperView;->mViewToShow:Landroid/view/View;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lorg/chromium/chrome/browser/infobar/ContentWrapperView;->mViewToShow:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v0

    goto :goto_0
.end method


# virtual methods
.method public detachCurrentView()Landroid/view/View;
    .locals 2

    .prologue
    .line 105
    sget-boolean v0, Lorg/chromium/chrome/browser/infobar/ContentWrapperView;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lorg/chromium/chrome/browser/infobar/ContentWrapperView;->getChildCount()I

    move-result v0

    const/4 v1, 0x1

    if-gt v0, v1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 106
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lorg/chromium/chrome/browser/infobar/ContentWrapperView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 107
    invoke-virtual {p0, v0}, Lorg/chromium/chrome/browser/infobar/ContentWrapperView;->removeView(Landroid/view/View;)V

    .line 108
    return-object v0
.end method

.method public finishTransition()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 161
    iget-object v0, p0, Lorg/chromium/chrome/browser/infobar/ContentWrapperView;->mViewToHide:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 162
    iget-object v0, p0, Lorg/chromium/chrome/browser/infobar/ContentWrapperView;->mViewToHide:Landroid/view/View;

    invoke-virtual {p0, v0}, Lorg/chromium/chrome/browser/infobar/ContentWrapperView;->removeView(Landroid/view/View;)V

    .line 164
    :cond_0
    invoke-virtual {p0}, Lorg/chromium/chrome/browser/infobar/ContentWrapperView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    const/4 v1, -0x2

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 165
    invoke-virtual {p0}, Lorg/chromium/chrome/browser/infobar/ContentWrapperView;->requestLayout()V

    .line 167
    iput-object v2, p0, Lorg/chromium/chrome/browser/infobar/ContentWrapperView;->mViewToHide:Landroid/view/View;

    .line 168
    iput-object v2, p0, Lorg/chromium/chrome/browser/infobar/ContentWrapperView;->mViewToShow:Landroid/view/View;

    .line 169
    iget-object v0, p0, Lorg/chromium/chrome/browser/infobar/ContentWrapperView;->mInfoBar:Lorg/chromium/chrome/browser/infobar/InfoBar;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lorg/chromium/chrome/browser/infobar/InfoBar;->setControlsEnabled(Z)V

    .line 170
    return-void
.end method

.method public getAnimationsForTransition(Ljava/util/ArrayList;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    .line 202
    iget-object v0, p0, Lorg/chromium/chrome/browser/infobar/ContentWrapperView;->mViewToHide:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/chromium/chrome/browser/infobar/ContentWrapperView;->mViewToShow:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 204
    iget-object v0, p0, Lorg/chromium/chrome/browser/infobar/ContentWrapperView;->mViewToHide:Landroid/view/View;

    const-string/jumbo v1, "alpha"

    new-array v2, v3, [F

    fill-array-data v2, :array_0

    invoke-static {v0, v1, v2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 205
    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 208
    iget-object v0, p0, Lorg/chromium/chrome/browser/infobar/ContentWrapperView;->mViewToShow:Landroid/view/View;

    const-string/jumbo v1, "alpha"

    new-array v2, v3, [F

    fill-array-data v2, :array_1

    invoke-static {v0, v1, v2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 209
    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 211
    :cond_0
    return-void

    .line 204
    :array_0
    .array-data 4
        0x3f800000    # 1.0f
        0x0
    .end array-data

    .line 208
    :array_1
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method

.method public getTransitionHeightDifference()I
    .locals 2

    .prologue
    .line 194
    invoke-direct {p0}, Lorg/chromium/chrome/browser/infobar/ContentWrapperView;->getViewToShowHeight()I

    move-result v0

    invoke-direct {p0}, Lorg/chromium/chrome/browser/infobar/ContentWrapperView;->getViewToHideHeight()I

    move-result v1

    sub-int/2addr v0, v1

    return v0
.end method

.method public hasChildView()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 97
    invoke-virtual {p0}, Lorg/chromium/chrome/browser/infobar/ContentWrapperView;->getChildCount()I

    move-result v1

    if-le v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lorg/chromium/chrome/browser/infobar/ContentWrapperView;->mInfoBar:Lorg/chromium/chrome/browser/infobar/InfoBar;

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/infobar/InfoBar;->areControlsEnabled()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 78
    const/4 v0, 0x1

    return v0
.end method

.method public prepareTransition(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 125
    sget-boolean v0, Lorg/chromium/chrome/browser/infobar/ContentWrapperView;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lorg/chromium/chrome/browser/infobar/ContentWrapperView;->mViewToHide:Landroid/view/View;

    if-nez v0, :cond_0

    iget-object v0, p0, Lorg/chromium/chrome/browser/infobar/ContentWrapperView;->mViewToShow:Landroid/view/View;

    if-eqz v0, :cond_1

    :cond_0
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 129
    :cond_1
    sget-boolean v0, Lorg/chromium/chrome/browser/infobar/ContentWrapperView;->$assertionsDisabled:Z

    if-nez v0, :cond_2

    invoke-virtual {p0}, Lorg/chromium/chrome/browser/infobar/ContentWrapperView;->getChildCount()I

    move-result v0

    const/4 v1, 0x2

    if-le v0, v1, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 130
    :cond_2
    invoke-virtual {p0}, Lorg/chromium/chrome/browser/infobar/ContentWrapperView;->hasChildView()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 131
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lorg/chromium/chrome/browser/infobar/ContentWrapperView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lorg/chromium/chrome/browser/infobar/ContentWrapperView;->mViewToHide:Landroid/view/View;

    .line 134
    :cond_3
    iput-object p1, p0, Lorg/chromium/chrome/browser/infobar/ContentWrapperView;->mViewToShow:Landroid/view/View;

    .line 135
    sget-boolean v0, Lorg/chromium/chrome/browser/infobar/ContentWrapperView;->$assertionsDisabled:Z

    if-nez v0, :cond_4

    iget-object v0, p0, Lorg/chromium/chrome/browser/infobar/ContentWrapperView;->mViewToHide:Landroid/view/View;

    if-nez v0, :cond_4

    iget-object v0, p0, Lorg/chromium/chrome/browser/infobar/ContentWrapperView;->mViewToShow:Landroid/view/View;

    if-nez v0, :cond_4

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 136
    :cond_4
    sget-boolean v0, Lorg/chromium/chrome/browser/infobar/ContentWrapperView;->$assertionsDisabled:Z

    if-nez v0, :cond_5

    iget-object v0, p0, Lorg/chromium/chrome/browser/infobar/ContentWrapperView;->mViewToHide:Landroid/view/View;

    iget-object v1, p0, Lorg/chromium/chrome/browser/infobar/ContentWrapperView;->mViewToShow:Landroid/view/View;

    if-ne v0, v1, :cond_5

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 137
    :cond_5
    return-void
.end method

.method public startTransition()V
    .locals 2

    .prologue
    .line 143
    iget-object v0, p0, Lorg/chromium/chrome/browser/infobar/ContentWrapperView;->mViewToShow:Landroid/view/View;

    if-eqz v0, :cond_2

    .line 145
    iget-object v0, p0, Lorg/chromium/chrome/browser/infobar/ContentWrapperView;->mViewToShow:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    .line 146
    sget-boolean v1, Lorg/chromium/chrome/browser/infobar/ContentWrapperView;->$assertionsDisabled:Z

    if-nez v1, :cond_1

    if-eqz v0, :cond_0

    instance-of v1, v0, Landroid/view/ViewGroup;

    if-nez v1, :cond_1

    :cond_0
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 147
    :cond_1
    check-cast v0, Landroid/view/ViewGroup;

    iget-object v1, p0, Lorg/chromium/chrome/browser/infobar/ContentWrapperView;->mViewToShow:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 148
    iget-object v0, p0, Lorg/chromium/chrome/browser/infobar/ContentWrapperView;->mViewToShow:Landroid/view/View;

    invoke-direct {p0, v0}, Lorg/chromium/chrome/browser/infobar/ContentWrapperView;->addChildView(Landroid/view/View;)V

    .line 151
    iget-object v0, p0, Lorg/chromium/chrome/browser/infobar/ContentWrapperView;->mViewToHide:Landroid/view/View;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lorg/chromium/chrome/browser/infobar/ContentWrapperView;->mViewToShow:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setAlpha(F)V

    .line 153
    :cond_2
    return-void
.end method
