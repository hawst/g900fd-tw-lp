.class public Lorg/chromium/chrome/browser/infobar/ConfirmInfoBarDelegate;
.super Ljava/lang/Object;
.source "ConfirmInfoBarDelegate.java"


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    return-void
.end method

.method public static create()Lorg/chromium/chrome/browser/infobar/ConfirmInfoBarDelegate;
    .locals 1

    .prologue
    .line 20
    new-instance v0, Lorg/chromium/chrome/browser/infobar/ConfirmInfoBarDelegate;

    invoke-direct {v0}, Lorg/chromium/chrome/browser/infobar/ConfirmInfoBarDelegate;-><init>()V

    return-object v0
.end method


# virtual methods
.method showConfirmInfoBar(JILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/chromium/chrome/browser/infobar/InfoBar;
    .locals 11

    .prologue
    .line 37
    invoke-static {p3}, Lorg/chromium/chrome/browser/ResourceId;->mapToDrawableId(I)I

    move-result v5

    .line 39
    new-instance v1, Lorg/chromium/chrome/browser/infobar/ConfirmInfoBar;

    const/4 v4, 0x0

    move-wide v2, p1

    move-object v6, p4

    move-object/from16 v7, p5

    move-object/from16 v8, p6

    move-object/from16 v9, p7

    invoke-direct/range {v1 .. v9}, Lorg/chromium/chrome/browser/infobar/ConfirmInfoBar;-><init>(JLorg/chromium/chrome/browser/infobar/InfoBarListeners$Confirm;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 41
    return-object v1
.end method
