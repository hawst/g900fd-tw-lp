.class final Lorg/chromium/chrome/browser/SSLClientCertificateRequest$2;
.super Ljava/lang/Object;
.source "SSLClientCertificateRequest.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic val$hostName:Ljava/lang/String;

.field final synthetic val$nativePtr:J

.field final synthetic val$port:I

.field final synthetic val$smartCardAuthManager:Lorg/chromium/chrome/browser/PKCS11AuthenticationManager;


# direct methods
.method constructor <init>(JLjava/lang/String;ILorg/chromium/chrome/browser/PKCS11AuthenticationManager;)V
    .locals 1

    .prologue
    .line 272
    iput-wide p1, p0, Lorg/chromium/chrome/browser/SSLClientCertificateRequest$2;->val$nativePtr:J

    iput-object p3, p0, Lorg/chromium/chrome/browser/SSLClientCertificateRequest$2;->val$hostName:Ljava/lang/String;

    iput p4, p0, Lorg/chromium/chrome/browser/SSLClientCertificateRequest$2;->val$port:I

    iput-object p5, p0, Lorg/chromium/chrome/browser/SSLClientCertificateRequest$2;->val$smartCardAuthManager:Lorg/chromium/chrome/browser/PKCS11AuthenticationManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 7

    .prologue
    .line 275
    new-instance v1, Lorg/chromium/chrome/browser/SSLClientCertificateRequest$CertAsyncTaskPKCS11;

    iget-wide v2, p0, Lorg/chromium/chrome/browser/SSLClientCertificateRequest$2;->val$nativePtr:J

    iget-object v4, p0, Lorg/chromium/chrome/browser/SSLClientCertificateRequest$2;->val$hostName:Ljava/lang/String;

    iget v5, p0, Lorg/chromium/chrome/browser/SSLClientCertificateRequest$2;->val$port:I

    iget-object v6, p0, Lorg/chromium/chrome/browser/SSLClientCertificateRequest$2;->val$smartCardAuthManager:Lorg/chromium/chrome/browser/PKCS11AuthenticationManager;

    invoke-direct/range {v1 .. v6}, Lorg/chromium/chrome/browser/SSLClientCertificateRequest$CertAsyncTaskPKCS11;-><init>(JLjava/lang/String;ILorg/chromium/chrome/browser/PKCS11AuthenticationManager;)V

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Void;

    invoke-virtual {v1, v0}, Lorg/chromium/chrome/browser/SSLClientCertificateRequest$CertAsyncTaskPKCS11;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 277
    return-void
.end method
