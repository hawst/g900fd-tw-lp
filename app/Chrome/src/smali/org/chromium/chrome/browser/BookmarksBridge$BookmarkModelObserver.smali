.class public abstract Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkModelObserver;
.super Ljava/lang/Object;
.source "BookmarksBridge.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 60
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract bookmarkModelChanged()V
.end method

.method public bookmarkModelLoaded()V
    .locals 0

    .prologue
    .line 129
    invoke-virtual {p0}, Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkModelObserver;->bookmarkModelChanged()V

    .line 130
    return-void
.end method

.method public bookmarkNodeAdded(Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkItem;I)V
    .locals 0

    .prologue
    .line 79
    invoke-virtual {p0}, Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkModelObserver;->bookmarkModelChanged()V

    .line 80
    return-void
.end method

.method public bookmarkNodeChanged(Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkItem;)V
    .locals 0

    .prologue
    .line 113
    invoke-virtual {p0}, Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkModelObserver;->bookmarkModelChanged()V

    .line 114
    return-void
.end method

.method public bookmarkNodeChildrenReordered(Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkItem;)V
    .locals 0

    .prologue
    .line 122
    invoke-virtual {p0}, Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkModelObserver;->bookmarkModelChanged()V

    .line 123
    return-void
.end method

.method public bookmarkNodeMoved(Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkItem;ILorg/chromium/chrome/browser/BookmarksBridge$BookmarkItem;I)V
    .locals 0

    .prologue
    .line 70
    invoke-virtual {p0}, Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkModelObserver;->bookmarkModelChanged()V

    .line 71
    return-void
.end method

.method public bookmarkNodeRemoved(Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkItem;ILorg/chromium/chrome/browser/BookmarksBridge$BookmarkItem;)V
    .locals 0

    .prologue
    .line 105
    invoke-virtual {p0}, Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkModelObserver;->bookmarkModelChanged()V

    .line 106
    return-void
.end method

.method public bookmarkNodeRemoved(Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkItem;ILorg/chromium/chrome/browser/BookmarksBridge$BookmarkItem;Z)V
    .locals 0

    .prologue
    .line 92
    if-eqz p4, :cond_0

    .line 95
    :goto_0
    return-void

    .line 94
    :cond_0
    invoke-virtual {p0, p1, p2, p3}, Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkModelObserver;->bookmarkNodeRemoved(Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkItem;ILorg/chromium/chrome/browser/BookmarksBridge$BookmarkItem;)V

    goto :goto_0
.end method
