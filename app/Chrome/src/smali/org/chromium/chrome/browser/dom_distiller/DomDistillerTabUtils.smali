.class public Lorg/chromium/chrome/browser/dom_distiller/DomDistillerTabUtils;
.super Ljava/lang/Object;
.source "DomDistillerTabUtils.java"


# annotations
.annotation runtime Lorg/chromium/base/JNINamespace;
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    return-void
.end method

.method public static distillCurrentPageAndView(Lorg/chromium/content_public/browser/WebContents;)V
    .locals 0

    .prologue
    .line 27
    invoke-static {p0}, Lorg/chromium/chrome/browser/dom_distiller/DomDistillerTabUtils;->nativeDistillCurrentPageAndView(Lorg/chromium/content_public/browser/WebContents;)V

    .line 28
    return-void
.end method

.method public static getFormattedUrlFromOriginalDistillerUrl(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 37
    invoke-static {p0}, Lorg/chromium/chrome/browser/dom_distiller/DomDistillerTabUtils;->nativeGetFormattedUrlFromOriginalDistillerUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static native nativeDistillCurrentPageAndView(Lorg/chromium/content_public/browser/WebContents;)V
.end method

.method private static native nativeGetFormattedUrlFromOriginalDistillerUrl(Ljava/lang/String;)Ljava/lang/String;
.end method
