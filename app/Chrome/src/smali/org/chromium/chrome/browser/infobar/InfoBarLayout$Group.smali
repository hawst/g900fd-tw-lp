.class Lorg/chromium/chrome/browser/infobar/InfoBarLayout$Group;
.super Ljava/lang/Object;
.source "InfoBarLayout.java"


# instance fields
.field public gravity:I

.field public isStacked:Z

.field public views:[Landroid/view/View;


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 66
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 73
    const v0, 0x800003

    iput v0, p0, Lorg/chromium/chrome/browser/infobar/InfoBarLayout$Group;->gravity:I

    return-void
.end method

.method synthetic constructor <init>(Lorg/chromium/chrome/browser/infobar/InfoBarLayout$1;)V
    .locals 0

    .prologue
    .line 66
    invoke-direct {p0}, Lorg/chromium/chrome/browser/infobar/InfoBarLayout$Group;-><init>()V

    return-void
.end method


# virtual methods
.method setHorizontalMode(III)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 79
    iput-boolean v2, p0, Lorg/chromium/chrome/browser/infobar/InfoBarLayout$Group;->isStacked:Z

    move v1, v2

    .line 80
    :goto_0
    iget-object v0, p0, Lorg/chromium/chrome/browser/infobar/InfoBarLayout$Group;->views:[Landroid/view/View;

    array-length v0, v0

    if-ge v1, v0, :cond_2

    .line 81
    iget-object v0, p0, Lorg/chromium/chrome/browser/infobar/InfoBarLayout$Group;->views:[Landroid/view/View;

    aget-object v0, v0, v1

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lorg/chromium/chrome/browser/infobar/InfoBarLayout$LayoutParams;

    .line 82
    if-nez v1, :cond_0

    move v3, p2

    :goto_1
    iput v3, v0, Lorg/chromium/chrome/browser/infobar/InfoBarLayout$LayoutParams;->startMargin:I

    .line 83
    iput v2, v0, Lorg/chromium/chrome/browser/infobar/InfoBarLayout$LayoutParams;->topMargin:I

    .line 84
    iget-object v3, p0, Lorg/chromium/chrome/browser/infobar/InfoBarLayout$Group;->views:[Landroid/view/View;

    array-length v3, v3

    add-int/lit8 v3, v3, -0x1

    if-ne v1, v3, :cond_1

    move v3, p3

    :goto_2
    iput v3, v0, Lorg/chromium/chrome/browser/infobar/InfoBarLayout$LayoutParams;->endMargin:I

    .line 85
    iput v2, v0, Lorg/chromium/chrome/browser/infobar/InfoBarLayout$LayoutParams;->bottomMargin:I

    .line 80
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    move v3, p1

    .line 82
    goto :goto_1

    :cond_1
    move v3, v2

    .line 84
    goto :goto_2

    .line 88
    :cond_2
    return-void
.end method

.method setVerticalMode(II)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 91
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/chromium/chrome/browser/infobar/InfoBarLayout$Group;->isStacked:Z

    move v1, v2

    .line 92
    :goto_0
    iget-object v0, p0, Lorg/chromium/chrome/browser/infobar/InfoBarLayout$Group;->views:[Landroid/view/View;

    array-length v0, v0

    if-ge v1, v0, :cond_2

    .line 93
    iget-object v0, p0, Lorg/chromium/chrome/browser/infobar/InfoBarLayout$Group;->views:[Landroid/view/View;

    aget-object v0, v0, v1

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lorg/chromium/chrome/browser/infobar/InfoBarLayout$LayoutParams;

    .line 94
    iput v2, v0, Lorg/chromium/chrome/browser/infobar/InfoBarLayout$LayoutParams;->startMargin:I

    .line 95
    if-nez v1, :cond_0

    move v3, v2

    :goto_1
    iput v3, v0, Lorg/chromium/chrome/browser/infobar/InfoBarLayout$LayoutParams;->topMargin:I

    .line 96
    iput v2, v0, Lorg/chromium/chrome/browser/infobar/InfoBarLayout$LayoutParams;->endMargin:I

    .line 97
    iget-object v3, p0, Lorg/chromium/chrome/browser/infobar/InfoBarLayout$Group;->views:[Landroid/view/View;

    array-length v3, v3

    add-int/lit8 v3, v3, -0x1

    if-ne v1, v3, :cond_1

    move v3, p2

    :goto_2
    iput v3, v0, Lorg/chromium/chrome/browser/infobar/InfoBarLayout$LayoutParams;->bottomMargin:I

    .line 92
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    move v3, p1

    .line 95
    goto :goto_1

    :cond_1
    move v3, v2

    .line 97
    goto :goto_2

    .line 99
    :cond_2
    return-void
.end method
