.class Lorg/chromium/chrome/browser/sync/ChromiumSyncAdapter$3;
.super Ljava/lang/Object;
.source "ChromiumSyncAdapter.java"

# interfaces
.implements Lorg/chromium/content/browser/BrowserStartupController$StartupCallback;


# instance fields
.field final synthetic this$0:Lorg/chromium/chrome/browser/sync/ChromiumSyncAdapter;

.field final synthetic val$acct:Landroid/accounts/Account;

.field final synthetic val$context:Landroid/content/Context;

.field final synthetic val$objectId:Ljava/lang/String;

.field final synthetic val$objectSource:I

.field final synthetic val$payload:Ljava/lang/String;

.field final synthetic val$semaphore:Ljava/util/concurrent/Semaphore;

.field final synthetic val$syncAllTypes:Z

.field final synthetic val$syncResult:Landroid/content/SyncResult;

.field final synthetic val$version:J


# direct methods
.method constructor <init>(Lorg/chromium/chrome/browser/sync/ChromiumSyncAdapter;ZILjava/lang/String;JLjava/lang/String;Ljava/util/concurrent/Semaphore;Landroid/content/Context;Landroid/accounts/Account;Landroid/content/SyncResult;)V
    .locals 1

    .prologue
    .line 136
    iput-object p1, p0, Lorg/chromium/chrome/browser/sync/ChromiumSyncAdapter$3;->this$0:Lorg/chromium/chrome/browser/sync/ChromiumSyncAdapter;

    iput-boolean p2, p0, Lorg/chromium/chrome/browser/sync/ChromiumSyncAdapter$3;->val$syncAllTypes:Z

    iput p3, p0, Lorg/chromium/chrome/browser/sync/ChromiumSyncAdapter$3;->val$objectSource:I

    iput-object p4, p0, Lorg/chromium/chrome/browser/sync/ChromiumSyncAdapter$3;->val$objectId:Ljava/lang/String;

    iput-wide p5, p0, Lorg/chromium/chrome/browser/sync/ChromiumSyncAdapter$3;->val$version:J

    iput-object p7, p0, Lorg/chromium/chrome/browser/sync/ChromiumSyncAdapter$3;->val$payload:Ljava/lang/String;

    iput-object p8, p0, Lorg/chromium/chrome/browser/sync/ChromiumSyncAdapter$3;->val$semaphore:Ljava/util/concurrent/Semaphore;

    iput-object p9, p0, Lorg/chromium/chrome/browser/sync/ChromiumSyncAdapter$3;->val$context:Landroid/content/Context;

    iput-object p10, p0, Lorg/chromium/chrome/browser/sync/ChromiumSyncAdapter$3;->val$acct:Landroid/accounts/Account;

    iput-object p11, p0, Lorg/chromium/chrome/browser/sync/ChromiumSyncAdapter$3;->val$syncResult:Landroid/content/SyncResult;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFailure()V
    .locals 6

    .prologue
    .line 160
    invoke-static {}, Lorg/chromium/chrome/browser/sync/DelayedSyncController;->getInstance()Lorg/chromium/chrome/browser/sync/DelayedSyncController;

    move-result-object v0

    iget-object v1, p0, Lorg/chromium/chrome/browser/sync/ChromiumSyncAdapter$3;->val$context:Landroid/content/Context;

    iget-object v2, p0, Lorg/chromium/chrome/browser/sync/ChromiumSyncAdapter$3;->val$acct:Landroid/accounts/Account;

    iget-object v2, v2, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/chromium/chrome/browser/sync/DelayedSyncController;->setDelayedSync(Landroid/content/Context;Ljava/lang/String;)V

    .line 162
    iget-object v0, p0, Lorg/chromium/chrome/browser/sync/ChromiumSyncAdapter$3;->val$syncResult:Landroid/content/SyncResult;

    iget-object v0, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v2, v0, Landroid/content/SyncStats;->numIoExceptions:J

    const-wide/16 v4, 0x1

    add-long/2addr v2, v4

    iput-wide v2, v0, Landroid/content/SyncStats;->numIoExceptions:J

    .line 163
    iget-object v0, p0, Lorg/chromium/chrome/browser/sync/ChromiumSyncAdapter$3;->val$semaphore:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v0}, Ljava/util/concurrent/Semaphore;->release()V

    .line 164
    return-void
.end method

.method public onSuccess(Z)V
    .locals 7

    .prologue
    .line 140
    iget-boolean v0, p0, Lorg/chromium/chrome/browser/sync/ChromiumSyncAdapter$3;->val$syncAllTypes:Z

    if-eqz v0, :cond_0

    .line 141
    const-string/jumbo v0, "ChromiumSyncAdapter"

    const-string/jumbo v1, "Received sync tickle for all types."

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 142
    iget-object v0, p0, Lorg/chromium/chrome/browser/sync/ChromiumSyncAdapter$3;->this$0:Lorg/chromium/chrome/browser/sync/ChromiumSyncAdapter;

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/sync/ChromiumSyncAdapter;->requestSyncForAllTypes()V

    .line 154
    :goto_0
    iget-object v0, p0, Lorg/chromium/chrome/browser/sync/ChromiumSyncAdapter$3;->val$semaphore:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v0}, Ljava/util/concurrent/Semaphore;->release()V

    .line 155
    return-void

    .line 147
    :cond_0
    iget v2, p0, Lorg/chromium/chrome/browser/sync/ChromiumSyncAdapter$3;->val$objectSource:I

    .line 148
    if-nez v2, :cond_1

    .line 149
    const/16 v2, 0x3ec

    .line 151
    :cond_1
    const-string/jumbo v0, "ChromiumSyncAdapter"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "Received sync tickle for "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v3, " "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v3, p0, Lorg/chromium/chrome/browser/sync/ChromiumSyncAdapter$3;->val$objectId:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v3, "."

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 152
    iget-object v1, p0, Lorg/chromium/chrome/browser/sync/ChromiumSyncAdapter$3;->this$0:Lorg/chromium/chrome/browser/sync/ChromiumSyncAdapter;

    iget-object v3, p0, Lorg/chromium/chrome/browser/sync/ChromiumSyncAdapter$3;->val$objectId:Ljava/lang/String;

    iget-wide v4, p0, Lorg/chromium/chrome/browser/sync/ChromiumSyncAdapter$3;->val$version:J

    iget-object v6, p0, Lorg/chromium/chrome/browser/sync/ChromiumSyncAdapter$3;->val$payload:Ljava/lang/String;

    invoke-virtual/range {v1 .. v6}, Lorg/chromium/chrome/browser/sync/ChromiumSyncAdapter;->requestSync(ILjava/lang/String;JLjava/lang/String;)V

    goto :goto_0
.end method
