.class public Lorg/chromium/chrome/browser/VoiceSearchTabHelper;
.super Lorg/chromium/content/browser/WebContentsObserverAndroid;
.source "VoiceSearchTabHelper.java"


# instance fields
.field private final mWebContents:Lorg/chromium/content_public/browser/WebContents;


# direct methods
.method public constructor <init>(Lorg/chromium/content_public/browser/WebContents;)V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0, p1}, Lorg/chromium/content/browser/WebContentsObserverAndroid;-><init>(Lorg/chromium/content_public/browser/WebContents;)V

    .line 23
    iput-object p1, p0, Lorg/chromium/chrome/browser/VoiceSearchTabHelper;->mWebContents:Lorg/chromium/content_public/browser/WebContents;

    .line 24
    return-void
.end method

.method private native nativeUpdateAutoplayStatus(Lorg/chromium/content_public/browser/WebContents;)V
.end method


# virtual methods
.method public navigationEntryCommitted()V
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lorg/chromium/chrome/browser/VoiceSearchTabHelper;->mWebContents:Lorg/chromium/content_public/browser/WebContents;

    invoke-direct {p0, v0}, Lorg/chromium/chrome/browser/VoiceSearchTabHelper;->nativeUpdateAutoplayStatus(Lorg/chromium/content_public/browser/WebContents;)V

    .line 29
    return-void
.end method
