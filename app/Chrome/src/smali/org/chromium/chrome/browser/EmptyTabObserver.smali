.class public Lorg/chromium/chrome/browser/EmptyTabObserver;
.super Ljava/lang/Object;
.source "EmptyTabObserver.java"

# interfaces
.implements Lorg/chromium/chrome/browser/TabObserver;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onContentChanged(Lorg/chromium/chrome/browser/Tab;)V
    .locals 0

    .prologue
    .line 18
    return-void
.end method

.method public onContextMenuShown(Lorg/chromium/chrome/browser/Tab;Landroid/view/ContextMenu;)V
    .locals 0

    .prologue
    .line 39
    return-void
.end method

.method public onDestroyed(Lorg/chromium/chrome/browser/Tab;)V
    .locals 0

    .prologue
    .line 15
    return-void
.end method

.method public onDidChangeThemeColor(I)V
    .locals 0

    .prologue
    .line 73
    return-void
.end method

.method public onDidFailLoad(Lorg/chromium/chrome/browser/Tab;ZZILjava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 61
    return-void
.end method

.method public onDidNavigateMainFrame(Lorg/chromium/chrome/browser/Tab;Ljava/lang/String;Ljava/lang/String;ZZI)V
    .locals 0

    .prologue
    .line 70
    return-void
.end method

.method public onDidStartProvisionalLoadForFrame(Lorg/chromium/chrome/browser/Tab;JJZLjava/lang/String;ZZ)V
    .locals 0

    .prologue
    .line 66
    return-void
.end method

.method public onFaviconUpdated(Lorg/chromium/chrome/browser/Tab;)V
    .locals 0

    .prologue
    .line 24
    return-void
.end method

.method public onLoadProgressChanged(Lorg/chromium/chrome/browser/Tab;I)V
    .locals 0

    .prologue
    .line 51
    return-void
.end method

.method public onLoadStarted(Lorg/chromium/chrome/browser/Tab;)V
    .locals 0

    .prologue
    .line 45
    return-void
.end method

.method public onLoadStopped(Lorg/chromium/chrome/browser/Tab;)V
    .locals 0

    .prologue
    .line 48
    return-void
.end method

.method public onLoadUrl(Lorg/chromium/chrome/browser/Tab;Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 21
    return-void
.end method

.method public onSSLStateUpdated(Lorg/chromium/chrome/browser/Tab;)V
    .locals 0

    .prologue
    .line 33
    return-void
.end method

.method public onTitleUpdated(Lorg/chromium/chrome/browser/Tab;)V
    .locals 0

    .prologue
    .line 27
    return-void
.end method

.method public onToggleFullscreenMode(Lorg/chromium/chrome/browser/Tab;Z)V
    .locals 0

    .prologue
    .line 57
    return-void
.end method

.method public onUpdateUrl(Lorg/chromium/chrome/browser/Tab;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 54
    return-void
.end method

.method public onUrlUpdated(Lorg/chromium/chrome/browser/Tab;)V
    .locals 0

    .prologue
    .line 30
    return-void
.end method

.method public onWebContentsInstantSupportDisabled()V
    .locals 0

    .prologue
    .line 42
    return-void
.end method

.method public onWebContentsSwapped(Lorg/chromium/chrome/browser/Tab;ZZ)V
    .locals 0

    .prologue
    .line 36
    return-void
.end method
