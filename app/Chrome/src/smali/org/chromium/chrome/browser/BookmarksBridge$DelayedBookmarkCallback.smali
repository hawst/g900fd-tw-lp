.class Lorg/chromium/chrome/browser/BookmarksBridge$DelayedBookmarkCallback;
.super Ljava/lang/Object;
.source "BookmarksBridge.java"


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private final mCallback:Lorg/chromium/chrome/browser/BookmarksBridge$BookmarksCallback;

.field private final mCallbackMethod:I

.field private final mFolderId:Lorg/chromium/components/bookmarks/BookmarkId;

.field private final mHandler:Lorg/chromium/chrome/browser/BookmarksBridge;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 719
    const-class v0, Lorg/chromium/chrome/browser/BookmarksBridge;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/chromium/chrome/browser/BookmarksBridge$DelayedBookmarkCallback;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private constructor <init>(Lorg/chromium/components/bookmarks/BookmarkId;Lorg/chromium/chrome/browser/BookmarksBridge$BookmarksCallback;ILorg/chromium/chrome/browser/BookmarksBridge;)V
    .locals 0

    .prologue
    .line 730
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 731
    iput-object p1, p0, Lorg/chromium/chrome/browser/BookmarksBridge$DelayedBookmarkCallback;->mFolderId:Lorg/chromium/components/bookmarks/BookmarkId;

    .line 732
    iput-object p2, p0, Lorg/chromium/chrome/browser/BookmarksBridge$DelayedBookmarkCallback;->mCallback:Lorg/chromium/chrome/browser/BookmarksBridge$BookmarksCallback;

    .line 733
    iput p3, p0, Lorg/chromium/chrome/browser/BookmarksBridge$DelayedBookmarkCallback;->mCallbackMethod:I

    .line 734
    iput-object p4, p0, Lorg/chromium/chrome/browser/BookmarksBridge$DelayedBookmarkCallback;->mHandler:Lorg/chromium/chrome/browser/BookmarksBridge;

    .line 735
    return-void
.end method

.method synthetic constructor <init>(Lorg/chromium/components/bookmarks/BookmarkId;Lorg/chromium/chrome/browser/BookmarksBridge$BookmarksCallback;ILorg/chromium/chrome/browser/BookmarksBridge;Lorg/chromium/chrome/browser/BookmarksBridge$1;)V
    .locals 0

    .prologue
    .line 719
    invoke-direct {p0, p1, p2, p3, p4}, Lorg/chromium/chrome/browser/BookmarksBridge$DelayedBookmarkCallback;-><init>(Lorg/chromium/components/bookmarks/BookmarkId;Lorg/chromium/chrome/browser/BookmarksBridge$BookmarksCallback;ILorg/chromium/chrome/browser/BookmarksBridge;)V

    return-void
.end method

.method static synthetic access$100(Lorg/chromium/chrome/browser/BookmarksBridge$DelayedBookmarkCallback;)V
    .locals 0

    .prologue
    .line 719
    invoke-direct {p0}, Lorg/chromium/chrome/browser/BookmarksBridge$DelayedBookmarkCallback;->callCallbackMethod()V

    return-void
.end method

.method private callCallbackMethod()V
    .locals 3

    .prologue
    .line 741
    iget v0, p0, Lorg/chromium/chrome/browser/BookmarksBridge$DelayedBookmarkCallback;->mCallbackMethod:I

    packed-switch v0, :pswitch_data_0

    .line 749
    sget-boolean v0, Lorg/chromium/chrome/browser/BookmarksBridge$DelayedBookmarkCallback;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 743
    :pswitch_0
    iget-object v0, p0, Lorg/chromium/chrome/browser/BookmarksBridge$DelayedBookmarkCallback;->mHandler:Lorg/chromium/chrome/browser/BookmarksBridge;

    iget-object v1, p0, Lorg/chromium/chrome/browser/BookmarksBridge$DelayedBookmarkCallback;->mFolderId:Lorg/chromium/components/bookmarks/BookmarkId;

    iget-object v2, p0, Lorg/chromium/chrome/browser/BookmarksBridge$DelayedBookmarkCallback;->mCallback:Lorg/chromium/chrome/browser/BookmarksBridge$BookmarksCallback;

    invoke-virtual {v0, v1, v2}, Lorg/chromium/chrome/browser/BookmarksBridge;->getBookmarksForFolder(Lorg/chromium/components/bookmarks/BookmarkId;Lorg/chromium/chrome/browser/BookmarksBridge$BookmarksCallback;)V

    .line 752
    :cond_0
    :goto_0
    return-void

    .line 746
    :pswitch_1
    iget-object v0, p0, Lorg/chromium/chrome/browser/BookmarksBridge$DelayedBookmarkCallback;->mHandler:Lorg/chromium/chrome/browser/BookmarksBridge;

    iget-object v1, p0, Lorg/chromium/chrome/browser/BookmarksBridge$DelayedBookmarkCallback;->mFolderId:Lorg/chromium/components/bookmarks/BookmarkId;

    iget-object v2, p0, Lorg/chromium/chrome/browser/BookmarksBridge$DelayedBookmarkCallback;->mCallback:Lorg/chromium/chrome/browser/BookmarksBridge$BookmarksCallback;

    invoke-virtual {v0, v1, v2}, Lorg/chromium/chrome/browser/BookmarksBridge;->getCurrentFolderHierarchy(Lorg/chromium/components/bookmarks/BookmarkId;Lorg/chromium/chrome/browser/BookmarksBridge$BookmarksCallback;)V

    goto :goto_0

    .line 741
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
