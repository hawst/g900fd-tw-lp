.class public Lorg/chromium/chrome/browser/Tab$TabChromeWebContentsDelegateAndroid;
.super Lorg/chromium/chrome/browser/ChromeWebContentsDelegateAndroid;
.source "Tab.java"


# instance fields
.field final synthetic this$0:Lorg/chromium/chrome/browser/Tab;


# direct methods
.method public constructor <init>(Lorg/chromium/chrome/browser/Tab;)V
    .locals 0

    .prologue
    .line 187
    iput-object p1, p0, Lorg/chromium/chrome/browser/Tab$TabChromeWebContentsDelegateAndroid;->this$0:Lorg/chromium/chrome/browser/Tab;

    invoke-direct {p0}, Lorg/chromium/chrome/browser/ChromeWebContentsDelegateAndroid;-><init>()V

    return-void
.end method


# virtual methods
.method public navigationStateChanged(I)V
    .locals 3

    .prologue
    .line 238
    and-int/lit8 v0, p1, 0x8

    if-eqz v0, :cond_0

    .line 239
    iget-object v0, p0, Lorg/chromium/chrome/browser/Tab$TabChromeWebContentsDelegateAndroid;->this$0:Lorg/chromium/chrome/browser/Tab;

    # getter for: Lorg/chromium/chrome/browser/Tab;->mObservers:Lorg/chromium/base/ObserverList;
    invoke-static {v0}, Lorg/chromium/chrome/browser/Tab;->access$100(Lorg/chromium/chrome/browser/Tab;)Lorg/chromium/base/ObserverList;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/base/ObserverList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/chromium/chrome/browser/TabObserver;

    iget-object v2, p0, Lorg/chromium/chrome/browser/Tab$TabChromeWebContentsDelegateAndroid;->this$0:Lorg/chromium/chrome/browser/Tab;

    invoke-interface {v0, v2}, Lorg/chromium/chrome/browser/TabObserver;->onTitleUpdated(Lorg/chromium/chrome/browser/Tab;)V

    goto :goto_0

    .line 241
    :cond_0
    and-int/lit8 v0, p1, 0x1

    if-eqz v0, :cond_1

    .line 242
    iget-object v0, p0, Lorg/chromium/chrome/browser/Tab$TabChromeWebContentsDelegateAndroid;->this$0:Lorg/chromium/chrome/browser/Tab;

    # getter for: Lorg/chromium/chrome/browser/Tab;->mObservers:Lorg/chromium/base/ObserverList;
    invoke-static {v0}, Lorg/chromium/chrome/browser/Tab;->access$100(Lorg/chromium/chrome/browser/Tab;)Lorg/chromium/base/ObserverList;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/base/ObserverList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/chromium/chrome/browser/TabObserver;

    iget-object v2, p0, Lorg/chromium/chrome/browser/Tab$TabChromeWebContentsDelegateAndroid;->this$0:Lorg/chromium/chrome/browser/Tab;

    invoke-interface {v0, v2}, Lorg/chromium/chrome/browser/TabObserver;->onUrlUpdated(Lorg/chromium/chrome/browser/Tab;)V

    goto :goto_1

    .line 244
    :cond_1
    return-void
.end method

.method public onLoadProgressChanged(I)V
    .locals 3

    .prologue
    .line 191
    iget-object v0, p0, Lorg/chromium/chrome/browser/Tab$TabChromeWebContentsDelegateAndroid;->this$0:Lorg/chromium/chrome/browser/Tab;

    # getter for: Lorg/chromium/chrome/browser/Tab;->mObservers:Lorg/chromium/base/ObserverList;
    invoke-static {v0}, Lorg/chromium/chrome/browser/Tab;->access$100(Lorg/chromium/chrome/browser/Tab;)Lorg/chromium/base/ObserverList;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/base/ObserverList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/chromium/chrome/browser/TabObserver;

    .line 192
    iget-object v2, p0, Lorg/chromium/chrome/browser/Tab$TabChromeWebContentsDelegateAndroid;->this$0:Lorg/chromium/chrome/browser/Tab;

    invoke-interface {v0, v2, p1}, Lorg/chromium/chrome/browser/TabObserver;->onLoadProgressChanged(Lorg/chromium/chrome/browser/Tab;I)V

    goto :goto_0

    .line 194
    :cond_0
    return-void
.end method

.method public onLoadStarted()V
    .locals 3

    .prologue
    .line 198
    iget-object v0, p0, Lorg/chromium/chrome/browser/Tab$TabChromeWebContentsDelegateAndroid;->this$0:Lorg/chromium/chrome/browser/Tab;

    # getter for: Lorg/chromium/chrome/browser/Tab;->mObservers:Lorg/chromium/base/ObserverList;
    invoke-static {v0}, Lorg/chromium/chrome/browser/Tab;->access$100(Lorg/chromium/chrome/browser/Tab;)Lorg/chromium/base/ObserverList;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/base/ObserverList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/chromium/chrome/browser/TabObserver;

    iget-object v2, p0, Lorg/chromium/chrome/browser/Tab$TabChromeWebContentsDelegateAndroid;->this$0:Lorg/chromium/chrome/browser/Tab;

    invoke-interface {v0, v2}, Lorg/chromium/chrome/browser/TabObserver;->onLoadStarted(Lorg/chromium/chrome/browser/Tab;)V

    goto :goto_0

    .line 199
    :cond_0
    return-void
.end method

.method public onLoadStopped()V
    .locals 3

    .prologue
    .line 203
    iget-object v0, p0, Lorg/chromium/chrome/browser/Tab$TabChromeWebContentsDelegateAndroid;->this$0:Lorg/chromium/chrome/browser/Tab;

    # getter for: Lorg/chromium/chrome/browser/Tab;->mObservers:Lorg/chromium/base/ObserverList;
    invoke-static {v0}, Lorg/chromium/chrome/browser/Tab;->access$100(Lorg/chromium/chrome/browser/Tab;)Lorg/chromium/base/ObserverList;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/base/ObserverList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/chromium/chrome/browser/TabObserver;

    iget-object v2, p0, Lorg/chromium/chrome/browser/Tab$TabChromeWebContentsDelegateAndroid;->this$0:Lorg/chromium/chrome/browser/Tab;

    invoke-interface {v0, v2}, Lorg/chromium/chrome/browser/TabObserver;->onLoadStopped(Lorg/chromium/chrome/browser/Tab;)V

    goto :goto_0

    .line 204
    :cond_0
    return-void
.end method

.method public onUpdateUrl(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 208
    iget-object v0, p0, Lorg/chromium/chrome/browser/Tab$TabChromeWebContentsDelegateAndroid;->this$0:Lorg/chromium/chrome/browser/Tab;

    # getter for: Lorg/chromium/chrome/browser/Tab;->mObservers:Lorg/chromium/base/ObserverList;
    invoke-static {v0}, Lorg/chromium/chrome/browser/Tab;->access$100(Lorg/chromium/chrome/browser/Tab;)Lorg/chromium/base/ObserverList;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/base/ObserverList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/chromium/chrome/browser/TabObserver;

    iget-object v2, p0, Lorg/chromium/chrome/browser/Tab$TabChromeWebContentsDelegateAndroid;->this$0:Lorg/chromium/chrome/browser/Tab;

    invoke-interface {v0, v2, p1}, Lorg/chromium/chrome/browser/TabObserver;->onUpdateUrl(Lorg/chromium/chrome/browser/Tab;Ljava/lang/String;)V

    goto :goto_0

    .line 209
    :cond_0
    return-void
.end method

.method public showRepostFormWarningDialog(Lorg/chromium/content/browser/ContentViewCore;)V
    .locals 3

    .prologue
    .line 213
    new-instance v1, Lorg/chromium/chrome/browser/RepostFormWarningDialog;

    new-instance v0, Lorg/chromium/chrome/browser/Tab$TabChromeWebContentsDelegateAndroid$1;

    invoke-direct {v0, p0}, Lorg/chromium/chrome/browser/Tab$TabChromeWebContentsDelegateAndroid$1;-><init>(Lorg/chromium/chrome/browser/Tab$TabChromeWebContentsDelegateAndroid;)V

    new-instance v2, Lorg/chromium/chrome/browser/Tab$TabChromeWebContentsDelegateAndroid$2;

    invoke-direct {v2, p0}, Lorg/chromium/chrome/browser/Tab$TabChromeWebContentsDelegateAndroid$2;-><init>(Lorg/chromium/chrome/browser/Tab$TabChromeWebContentsDelegateAndroid;)V

    invoke-direct {v1, v0, v2}, Lorg/chromium/chrome/browser/RepostFormWarningDialog;-><init>(Ljava/lang/Runnable;Ljava/lang/Runnable;)V

    .line 225
    iget-object v0, p0, Lorg/chromium/chrome/browser/Tab$TabChromeWebContentsDelegateAndroid;->this$0:Lorg/chromium/chrome/browser/Tab;

    # getter for: Lorg/chromium/chrome/browser/Tab;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lorg/chromium/chrome/browser/Tab;->access$200(Lorg/chromium/chrome/browser/Tab;)Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    .line 226
    invoke-virtual {v0}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Lorg/chromium/chrome/browser/RepostFormWarningDialog;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 227
    return-void
.end method

.method public toggleFullscreenModeForTab(Z)V
    .locals 3

    .prologue
    .line 231
    iget-object v0, p0, Lorg/chromium/chrome/browser/Tab$TabChromeWebContentsDelegateAndroid;->this$0:Lorg/chromium/chrome/browser/Tab;

    # getter for: Lorg/chromium/chrome/browser/Tab;->mObservers:Lorg/chromium/base/ObserverList;
    invoke-static {v0}, Lorg/chromium/chrome/browser/Tab;->access$100(Lorg/chromium/chrome/browser/Tab;)Lorg/chromium/base/ObserverList;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/base/ObserverList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/chromium/chrome/browser/TabObserver;

    .line 232
    iget-object v2, p0, Lorg/chromium/chrome/browser/Tab$TabChromeWebContentsDelegateAndroid;->this$0:Lorg/chromium/chrome/browser/Tab;

    invoke-interface {v0, v2, p1}, Lorg/chromium/chrome/browser/TabObserver;->onToggleFullscreenMode(Lorg/chromium/chrome/browser/Tab;Z)V

    goto :goto_0

    .line 234
    :cond_0
    return-void
.end method

.method public visibleSSLStateChanged()V
    .locals 3

    .prologue
    .line 248
    iget-object v0, p0, Lorg/chromium/chrome/browser/Tab$TabChromeWebContentsDelegateAndroid;->this$0:Lorg/chromium/chrome/browser/Tab;

    # getter for: Lorg/chromium/chrome/browser/Tab;->mObservers:Lorg/chromium/base/ObserverList;
    invoke-static {v0}, Lorg/chromium/chrome/browser/Tab;->access$100(Lorg/chromium/chrome/browser/Tab;)Lorg/chromium/base/ObserverList;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/base/ObserverList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/chromium/chrome/browser/TabObserver;

    iget-object v2, p0, Lorg/chromium/chrome/browser/Tab$TabChromeWebContentsDelegateAndroid;->this$0:Lorg/chromium/chrome/browser/Tab;

    invoke-interface {v0, v2}, Lorg/chromium/chrome/browser/TabObserver;->onSSLStateUpdated(Lorg/chromium/chrome/browser/Tab;)V

    goto :goto_0

    .line 249
    :cond_0
    return-void
.end method
