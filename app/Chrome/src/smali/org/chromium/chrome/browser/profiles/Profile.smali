.class public Lorg/chromium/chrome/browser/profiles/Profile;
.super Ljava/lang/Object;
.source "Profile.java"


# instance fields
.field private mNativeProfileAndroid:J


# direct methods
.method private constructor <init>(J)V
    .locals 1

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    iput-wide p1, p0, Lorg/chromium/chrome/browser/profiles/Profile;->mNativeProfileAndroid:J

    .line 18
    return-void
.end method

.method private static create(J)Lorg/chromium/chrome/browser/profiles/Profile;
    .locals 2

    .prologue
    .line 42
    new-instance v0, Lorg/chromium/chrome/browser/profiles/Profile;

    invoke-direct {v0, p0, p1}, Lorg/chromium/chrome/browser/profiles/Profile;-><init>(J)V

    return-object v0
.end method

.method private destroy()V
    .locals 2

    .prologue
    .line 47
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lorg/chromium/chrome/browser/profiles/Profile;->mNativeProfileAndroid:J

    .line 48
    return-void
.end method

.method public static getLastUsedProfile()Lorg/chromium/chrome/browser/profiles/Profile;
    .locals 1

    .prologue
    .line 21
    invoke-static {}, Lorg/chromium/chrome/browser/profiles/Profile;->nativeGetLastUsedProfile()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/chromium/chrome/browser/profiles/Profile;

    return-object v0
.end method

.method private getNativePointer()J
    .locals 2

    .prologue
    .line 52
    iget-wide v0, p0, Lorg/chromium/chrome/browser/profiles/Profile;->mNativeProfileAndroid:J

    return-wide v0
.end method

.method private static native nativeGetLastUsedProfile()Ljava/lang/Object;
.end method

.method private native nativeGetOffTheRecordProfile(J)Ljava/lang/Object;
.end method

.method private native nativeGetOriginalProfile(J)Ljava/lang/Object;
.end method

.method private native nativeHasOffTheRecordProfile(J)Z
.end method

.method private native nativeIsOffTheRecord(J)Z
.end method


# virtual methods
.method public getOffTheRecordProfile()Lorg/chromium/chrome/browser/profiles/Profile;
    .locals 2

    .prologue
    .line 29
    iget-wide v0, p0, Lorg/chromium/chrome/browser/profiles/Profile;->mNativeProfileAndroid:J

    invoke-direct {p0, v0, v1}, Lorg/chromium/chrome/browser/profiles/Profile;->nativeGetOffTheRecordProfile(J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/chromium/chrome/browser/profiles/Profile;

    return-object v0
.end method

.method public getOriginalProfile()Lorg/chromium/chrome/browser/profiles/Profile;
    .locals 2

    .prologue
    .line 25
    iget-wide v0, p0, Lorg/chromium/chrome/browser/profiles/Profile;->mNativeProfileAndroid:J

    invoke-direct {p0, v0, v1}, Lorg/chromium/chrome/browser/profiles/Profile;->nativeGetOriginalProfile(J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/chromium/chrome/browser/profiles/Profile;

    return-object v0
.end method

.method public hasOffTheRecordProfile()Z
    .locals 2

    .prologue
    .line 33
    iget-wide v0, p0, Lorg/chromium/chrome/browser/profiles/Profile;->mNativeProfileAndroid:J

    invoke-direct {p0, v0, v1}, Lorg/chromium/chrome/browser/profiles/Profile;->nativeHasOffTheRecordProfile(J)Z

    move-result v0

    return v0
.end method
