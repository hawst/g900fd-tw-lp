.class public Lorg/chromium/chrome/browser/appmenu/AppMenu;
.super Ljava/lang/Object;
.source "AppMenu.java"

# interfaces
.implements Landroid/view/View$OnKeyListener;
.implements Landroid/widget/AdapterView$OnItemClickListener;


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private mAdapter:Lorg/chromium/chrome/browser/appmenu/AppMenuAdapter;

.field private mCurrentScreenRotation:I

.field private mHandler:Lorg/chromium/chrome/browser/appmenu/AppMenuHandler;

.field private mIsByHardwareButton:Z

.field private final mItemDividerHeight:I

.field private final mItemRowHeight:I

.field private final mMenu:Landroid/view/Menu;

.field private final mNegativeSoftwareVerticalOffset:I

.field private mPopup:Landroid/widget/ListPopupWindow;

.field private final mVerticalFadeDistance:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 40
    const-class v0, Lorg/chromium/chrome/browser/appmenu/AppMenu;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/chromium/chrome/browser/appmenu/AppMenu;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>(Landroid/view/Menu;IILorg/chromium/chrome/browser/appmenu/AppMenuHandler;Landroid/content/res/Resources;)V
    .locals 1

    .prologue
    .line 66
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    const/4 v0, -0x1

    iput v0, p0, Lorg/chromium/chrome/browser/appmenu/AppMenu;->mCurrentScreenRotation:I

    .line 67
    iput-object p1, p0, Lorg/chromium/chrome/browser/appmenu/AppMenu;->mMenu:Landroid/view/Menu;

    .line 69
    iput p2, p0, Lorg/chromium/chrome/browser/appmenu/AppMenu;->mItemRowHeight:I

    .line 70
    sget-boolean v0, Lorg/chromium/chrome/browser/appmenu/AppMenu;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget v0, p0, Lorg/chromium/chrome/browser/appmenu/AppMenu;->mItemRowHeight:I

    if-gtz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 72
    :cond_0
    iput-object p4, p0, Lorg/chromium/chrome/browser/appmenu/AppMenu;->mHandler:Lorg/chromium/chrome/browser/appmenu/AppMenuHandler;

    .line 74
    iput p3, p0, Lorg/chromium/chrome/browser/appmenu/AppMenu;->mItemDividerHeight:I

    .line 75
    sget-boolean v0, Lorg/chromium/chrome/browser/appmenu/AppMenu;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    iget v0, p0, Lorg/chromium/chrome/browser/appmenu/AppMenu;->mItemDividerHeight:I

    if-gez v0, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 77
    :cond_1
    sget v0, Lorg/chromium/chrome/R$dimen;->menu_negative_software_vertical_offset:I

    invoke-virtual {p5, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lorg/chromium/chrome/browser/appmenu/AppMenu;->mNegativeSoftwareVerticalOffset:I

    .line 79
    sget v0, Lorg/chromium/chrome/R$dimen;->menu_vertical_fade_distance:I

    invoke-virtual {p5, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lorg/chromium/chrome/browser/appmenu/AppMenu;->mVerticalFadeDistance:I

    .line 80
    return-void
.end method

.method static synthetic access$000(Lorg/chromium/chrome/browser/appmenu/AppMenu;)Landroid/widget/ListPopupWindow;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lorg/chromium/chrome/browser/appmenu/AppMenu;->mPopup:Landroid/widget/ListPopupWindow;

    return-object v0
.end method

.method static synthetic access$100(Lorg/chromium/chrome/browser/appmenu/AppMenu;)Lorg/chromium/chrome/browser/appmenu/AppMenuHandler;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lorg/chromium/chrome/browser/appmenu/AppMenu;->mHandler:Lorg/chromium/chrome/browser/appmenu/AppMenuHandler;

    return-object v0
.end method

.method static synthetic access$200(Lorg/chromium/chrome/browser/appmenu/AppMenu;)V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0}, Lorg/chromium/chrome/browser/appmenu/AppMenu;->runMenuItemEnterAnimations()V

    return-void
.end method

.method private runMenuItemEnterAnimations()V
    .locals 6

    .prologue
    .line 329
    new-instance v3, Landroid/animation/AnimatorSet;

    invoke-direct {v3}, Landroid/animation/AnimatorSet;-><init>()V

    .line 330
    const/4 v1, 0x0

    .line 332
    iget-object v0, p0, Lorg/chromium/chrome/browser/appmenu/AppMenu;->mPopup:Landroid/widget/ListPopupWindow;

    invoke-virtual {v0}, Landroid/widget/ListPopupWindow;->getListView()Landroid/widget/ListView;

    move-result-object v4

    .line 333
    const/4 v0, 0x0

    move v2, v0

    :goto_0
    invoke-virtual {v4}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    if-ge v2, v0, :cond_2

    .line 334
    invoke-virtual {v4, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 335
    sget v5, Lorg/chromium/chrome/R$id;->menu_item_enter_anim_id:I

    invoke-virtual {v0, v5}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    .line 336
    if-eqz v0, :cond_1

    .line 337
    if-nez v1, :cond_0

    .line 338
    check-cast v0, Landroid/animation/Animator;

    invoke-virtual {v3, v0}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v0

    .line 333
    :goto_1
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move-object v1, v0

    goto :goto_0

    .line 340
    :cond_0
    check-cast v0, Landroid/animation/Animator;

    invoke-virtual {v1, v0}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    :cond_1
    move-object v0, v1

    goto :goto_1

    .line 345
    :cond_2
    invoke-virtual {v3}, Landroid/animation/AnimatorSet;->start()V

    .line 346
    return-void
.end method

.method private setMenuHeight(ILandroid/graphics/Rect;ILandroid/graphics/Rect;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 292
    sget-boolean v0, Lorg/chromium/chrome/browser/appmenu/AppMenu;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lorg/chromium/chrome/browser/appmenu/AppMenu;->mPopup:Landroid/widget/ListPopupWindow;

    invoke-virtual {v0}, Landroid/widget/ListPopupWindow;->getAnchorView()Landroid/view/View;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 293
    :cond_0
    iget-object v0, p0, Lorg/chromium/chrome/browser/appmenu/AppMenu;->mPopup:Landroid/widget/ListPopupWindow;

    invoke-virtual {v0}, Landroid/widget/ListPopupWindow;->getAnchorView()Landroid/view/View;

    move-result-object v0

    .line 294
    const/4 v1, 0x2

    new-array v1, v1, [I

    .line 295
    invoke-virtual {v0, v1}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 296
    aget v2, v1, v4

    iget v3, p2, Landroid/graphics/Rect;->top:I

    sub-int/2addr v2, v3

    aput v2, v1, v4

    .line 297
    iget-boolean v2, p0, Lorg/chromium/chrome/browser/appmenu/AppMenu;->mIsByHardwareButton:Z

    if-eqz v2, :cond_3

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v0

    .line 300
    :goto_0
    aget v2, v1, v4

    if-le v2, p3, :cond_1

    .line 301
    invoke-virtual {p2}, Landroid/graphics/Rect;->height()I

    move-result v2

    aput v2, v1, v4

    .line 303
    :cond_1
    aget v2, v1, v4

    invoke-virtual {p2}, Landroid/graphics/Rect;->height()I

    move-result v3

    aget v1, v1, v4

    sub-int v1, v3, v1

    sub-int v0, v1, v0

    invoke-static {v2, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 306
    iget v1, p4, Landroid/graphics/Rect;->bottom:I

    sub-int/2addr v0, v1

    .line 307
    iget-boolean v1, p0, Lorg/chromium/chrome/browser/appmenu/AppMenu;->mIsByHardwareButton:Z

    if-eqz v1, :cond_2

    iget v1, p4, Landroid/graphics/Rect;->top:I

    sub-int/2addr v0, v1

    .line 309
    :cond_2
    iget v1, p0, Lorg/chromium/chrome/browser/appmenu/AppMenu;->mItemRowHeight:I

    iget v2, p0, Lorg/chromium/chrome/browser/appmenu/AppMenu;->mItemDividerHeight:I

    add-int/2addr v1, v2

    div-int v1, v0, v1

    .line 312
    if-ge v1, p1, :cond_5

    .line 313
    iget v2, p0, Lorg/chromium/chrome/browser/appmenu/AppMenu;->mItemRowHeight:I

    iget v3, p0, Lorg/chromium/chrome/browser/appmenu/AppMenu;->mItemDividerHeight:I

    add-int/2addr v2, v3

    mul-int/2addr v1, v2

    .line 314
    const/high16 v2, 0x3f000000    # 0.5f

    iget v3, p0, Lorg/chromium/chrome/browser/appmenu/AppMenu;->mItemRowHeight:I

    int-to-float v3, v3

    mul-float/2addr v2, v3

    float-to-int v2, v2

    .line 316
    add-int v3, v1, v2

    if-ge v3, v0, :cond_4

    .line 317
    iget-object v0, p0, Lorg/chromium/chrome/browser/appmenu/AppMenu;->mPopup:Landroid/widget/ListPopupWindow;

    add-int/2addr v1, v2

    iget v2, p4, Landroid/graphics/Rect;->top:I

    add-int/2addr v1, v2

    iget v2, p4, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v1, v2

    invoke-virtual {v0, v1}, Landroid/widget/ListPopupWindow;->setHeight(I)V

    .line 326
    :goto_1
    return-void

    .line 297
    :cond_3
    const/4 v0, 0x0

    goto :goto_0

    .line 320
    :cond_4
    iget-object v0, p0, Lorg/chromium/chrome/browser/appmenu/AppMenu;->mPopup:Landroid/widget/ListPopupWindow;

    iget v3, p0, Lorg/chromium/chrome/browser/appmenu/AppMenu;->mItemRowHeight:I

    sub-int/2addr v1, v3

    add-int/2addr v1, v2

    iget v2, p4, Landroid/graphics/Rect;->top:I

    add-int/2addr v1, v2

    iget v2, p4, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v1, v2

    invoke-virtual {v0, v1}, Landroid/widget/ListPopupWindow;->setHeight(I)V

    goto :goto_1

    .line 324
    :cond_5
    iget-object v0, p0, Lorg/chromium/chrome/browser/appmenu/AppMenu;->mPopup:Landroid/widget/ListPopupWindow;

    const/4 v1, -0x2

    invoke-virtual {v0, v1}, Landroid/widget/ListPopupWindow;->setHeight(I)V

    goto :goto_1
.end method

.method private setPopupOffset(Landroid/widget/ListPopupWindow;ILandroid/graphics/Rect;Landroid/graphics/Rect;)V
    .locals 3

    .prologue
    .line 194
    const/4 v0, 0x2

    new-array v0, v0, [I

    .line 195
    invoke-virtual {p1}, Landroid/widget/ListPopupWindow;->getAnchorView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/View;->getLocationInWindow([I)V

    .line 196
    invoke-virtual {p1}, Landroid/widget/ListPopupWindow;->getAnchorView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    move-result v1

    .line 200
    iget-boolean v2, p0, Lorg/chromium/chrome/browser/appmenu/AppMenu;->mIsByHardwareButton:Z

    if-eqz v2, :cond_1

    .line 201
    const/4 v1, 0x0

    aget v0, v0, v1

    neg-int v0, v0

    .line 202
    packed-switch p2, :pswitch_data_0

    .line 213
    sget-boolean v1, Lorg/chromium/chrome/browser/appmenu/AppMenu;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 205
    :pswitch_0
    invoke-virtual {p3}, Landroid/graphics/Rect;->width()I

    move-result v1

    iget-object v2, p0, Lorg/chromium/chrome/browser/appmenu/AppMenu;->mPopup:Landroid/widget/ListPopupWindow;

    invoke-virtual {v2}, Landroid/widget/ListPopupWindow;->getWidth()I

    move-result v2

    sub-int/2addr v1, v2

    div-int/lit8 v1, v1, 0x2

    add-int/2addr v0, v1

    .line 216
    :cond_0
    :goto_0
    :pswitch_1
    invoke-virtual {p1, v0}, Landroid/widget/ListPopupWindow;->setHorizontalOffset(I)V

    .line 219
    iget v0, p4, Landroid/graphics/Rect;->bottom:I

    neg-int v0, v0

    invoke-virtual {p1, v0}, Landroid/widget/ListPopupWindow;->setVerticalOffset(I)V

    .line 225
    :goto_1
    return-void

    .line 208
    :pswitch_2
    invoke-virtual {p3}, Landroid/graphics/Rect;->width()I

    move-result v1

    iget-object v2, p0, Lorg/chromium/chrome/browser/appmenu/AppMenu;->mPopup:Landroid/widget/ListPopupWindow;

    invoke-virtual {v2}, Landroid/widget/ListPopupWindow;->getWidth()I

    move-result v2

    sub-int/2addr v1, v2

    add-int/2addr v0, v1

    .line 209
    goto :goto_0

    .line 223
    :cond_1
    iget v0, p0, Lorg/chromium/chrome/browser/appmenu/AppMenu;->mNegativeSoftwareVerticalOffset:I

    neg-int v0, v0

    sub-int/2addr v0, v1

    invoke-virtual {p1, v0}, Landroid/widget/ListPopupWindow;->setVerticalOffset(I)V

    goto :goto_1

    .line 202
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method dismiss()V
    .locals 1

    .prologue
    .line 267
    iget-object v0, p0, Lorg/chromium/chrome/browser/appmenu/AppMenu;->mHandler:Lorg/chromium/chrome/browser/appmenu/AppMenuHandler;

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/appmenu/AppMenuHandler;->appMenuDismissed()V

    .line 268
    invoke-virtual {p0}, Lorg/chromium/chrome/browser/appmenu/AppMenu;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 269
    iget-object v0, p0, Lorg/chromium/chrome/browser/appmenu/AppMenu;->mPopup:Landroid/widget/ListPopupWindow;

    invoke-virtual {v0}, Landroid/widget/ListPopupWindow;->dismiss()V

    .line 271
    :cond_0
    return-void
.end method

.method getPopup()Landroid/widget/ListPopupWindow;
    .locals 1

    .prologue
    .line 287
    iget-object v0, p0, Lorg/chromium/chrome/browser/appmenu/AppMenu;->mPopup:Landroid/widget/ListPopupWindow;

    return-object v0
.end method

.method isShowing()Z
    .locals 1

    .prologue
    .line 277
    iget-object v0, p0, Lorg/chromium/chrome/browser/appmenu/AppMenu;->mPopup:Landroid/widget/ListPopupWindow;

    if-nez v0, :cond_0

    .line 278
    const/4 v0, 0x0

    .line 280
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lorg/chromium/chrome/browser/appmenu/AppMenu;->mPopup:Landroid/widget/ListPopupWindow;

    invoke-virtual {v0}, Landroid/widget/ListPopupWindow;->isShowing()Z

    move-result v0

    goto :goto_0
.end method

.method onItemClick(Landroid/view/MenuItem;)V
    .locals 1

    .prologue
    .line 232
    invoke-interface {p1}, Landroid/view/MenuItem;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 233
    invoke-virtual {p0}, Lorg/chromium/chrome/browser/appmenu/AppMenu;->dismiss()V

    .line 234
    iget-object v0, p0, Lorg/chromium/chrome/browser/appmenu/AppMenu;->mHandler:Lorg/chromium/chrome/browser/appmenu/AppMenuHandler;

    invoke-virtual {v0, p1}, Lorg/chromium/chrome/browser/appmenu/AppMenuHandler;->onOptionsItemSelected(Landroid/view/MenuItem;)V

    .line 236
    :cond_0
    return-void
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 1

    .prologue
    .line 240
    iget-object v0, p0, Lorg/chromium/chrome/browser/appmenu/AppMenu;->mAdapter:Lorg/chromium/chrome/browser/appmenu/AppMenuAdapter;

    invoke-virtual {v0, p3}, Lorg/chromium/chrome/browser/appmenu/AppMenuAdapter;->getItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-virtual {p0, v0}, Lorg/chromium/chrome/browser/appmenu/AppMenu;->onItemClick(Landroid/view/MenuItem;)V

    .line 241
    return-void
.end method

.method public onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 245
    iget-object v2, p0, Lorg/chromium/chrome/browser/appmenu/AppMenu;->mPopup:Landroid/widget/ListPopupWindow;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lorg/chromium/chrome/browser/appmenu/AppMenu;->mPopup:Landroid/widget/ListPopupWindow;

    invoke-virtual {v2}, Landroid/widget/ListPopupWindow;->getListView()Landroid/widget/ListView;

    move-result-object v2

    if-nez v2, :cond_1

    :cond_0
    move v0, v1

    .line 260
    :goto_0
    return v0

    .line 247
    :cond_1
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v2

    const/16 v3, 0x52

    if-ne v2, v3, :cond_3

    .line 248
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v2

    if-nez v2, :cond_2

    invoke-virtual {p3}, Landroid/view/KeyEvent;->getRepeatCount()I

    move-result v2

    if-nez v2, :cond_2

    .line 249
    invoke-virtual {p3}, Landroid/view/KeyEvent;->startTracking()V

    .line 250
    invoke-virtual {p1}, Landroid/view/View;->getKeyDispatcherState()Landroid/view/KeyEvent$DispatcherState;

    move-result-object v1

    invoke-virtual {v1, p3, p0}, Landroid/view/KeyEvent$DispatcherState;->startTracking(Landroid/view/KeyEvent;Ljava/lang/Object;)V

    goto :goto_0

    .line 252
    :cond_2
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v2

    if-ne v2, v0, :cond_3

    .line 253
    invoke-virtual {p1}, Landroid/view/View;->getKeyDispatcherState()Landroid/view/KeyEvent$DispatcherState;

    move-result-object v2

    invoke-virtual {v2, p3}, Landroid/view/KeyEvent$DispatcherState;->handleUpEvent(Landroid/view/KeyEvent;)V

    .line 254
    invoke-virtual {p3}, Landroid/view/KeyEvent;->isTracking()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-virtual {p3}, Landroid/view/KeyEvent;->isCanceled()Z

    move-result v2

    if-nez v2, :cond_3

    .line 255
    invoke-virtual {p0}, Lorg/chromium/chrome/browser/appmenu/AppMenu;->dismiss()V

    goto :goto_0

    :cond_3
    move v0, v1

    .line 260
    goto :goto_0
.end method

.method show(Landroid/content/Context;Landroid/view/View;ZILandroid/graphics/Rect;I)V
    .locals 9

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 96
    new-instance v2, Landroid/widget/ListPopupWindow;

    const/4 v3, 0x0

    const v4, 0x1010300

    invoke-direct {v2, p1, v3, v4}, Landroid/widget/ListPopupWindow;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-object v2, p0, Lorg/chromium/chrome/browser/appmenu/AppMenu;->mPopup:Landroid/widget/ListPopupWindow;

    .line 97
    iget-object v2, p0, Lorg/chromium/chrome/browser/appmenu/AppMenu;->mPopup:Landroid/widget/ListPopupWindow;

    invoke-virtual {v2, v1}, Landroid/widget/ListPopupWindow;->setModal(Z)V

    .line 98
    iget-object v2, p0, Lorg/chromium/chrome/browser/appmenu/AppMenu;->mPopup:Landroid/widget/ListPopupWindow;

    invoke-virtual {v2, p2}, Landroid/widget/ListPopupWindow;->setAnchorView(Landroid/view/View;)V

    .line 99
    iget-object v2, p0, Lorg/chromium/chrome/browser/appmenu/AppMenu;->mPopup:Landroid/widget/ListPopupWindow;

    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Landroid/widget/ListPopupWindow;->setInputMethodMode(I)V

    .line 100
    iget-object v2, p0, Lorg/chromium/chrome/browser/appmenu/AppMenu;->mPopup:Landroid/widget/ListPopupWindow;

    new-instance v3, Lorg/chromium/chrome/browser/appmenu/AppMenu$1;

    invoke-direct {v3, p0}, Lorg/chromium/chrome/browser/appmenu/AppMenu$1;-><init>(Lorg/chromium/chrome/browser/appmenu/AppMenu;)V

    invoke-virtual {v2, v3}, Landroid/widget/ListPopupWindow;->setOnDismissListener(Landroid/widget/PopupWindow$OnDismissListener;)V

    .line 113
    iget-object v2, p0, Lorg/chromium/chrome/browser/appmenu/AppMenu;->mPopup:Landroid/widget/ListPopupWindow;

    invoke-virtual {v2}, Landroid/widget/ListPopupWindow;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v3

    .line 117
    if-eqz p3, :cond_2

    .line 118
    iget-object v2, p0, Lorg/chromium/chrome/browser/appmenu/AppMenu;->mPopup:Landroid/widget/ListPopupWindow;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    sget v5, Lorg/chromium/chrome/R$drawable;->menu_bg:I

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/widget/ListPopupWindow;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 126
    :goto_0
    invoke-static {}, Lorg/chromium/base/SysUtils;->isLowEndDevice()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lorg/chromium/chrome/browser/appmenu/AppMenu;->mPopup:Landroid/widget/ListPopupWindow;

    invoke-virtual {v2, v0}, Landroid/widget/ListPopupWindow;->setAnimationStyle(I)V

    .line 128
    :cond_0
    new-instance v4, Landroid/graphics/Rect;

    invoke-direct {v4}, Landroid/graphics/Rect;-><init>()V

    .line 129
    iget-object v2, p0, Lorg/chromium/chrome/browser/appmenu/AppMenu;->mPopup:Landroid/widget/ListPopupWindow;

    invoke-virtual {v2}, Landroid/widget/ListPopupWindow;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/graphics/drawable/Drawable;->getPadding(Landroid/graphics/Rect;)Z

    .line 131
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v5, Lorg/chromium/chrome/R$dimen;->menu_width:I

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iget v5, v4, Landroid/graphics/Rect;->left:I

    add-int/2addr v2, v5

    iget v5, v4, Landroid/graphics/Rect;->right:I

    add-int/2addr v2, v5

    .line 134
    iget-object v5, p0, Lorg/chromium/chrome/browser/appmenu/AppMenu;->mPopup:Landroid/widget/ListPopupWindow;

    invoke-virtual {v5, v2}, Landroid/widget/ListPopupWindow;->setWidth(I)V

    .line 136
    iput p4, p0, Lorg/chromium/chrome/browser/appmenu/AppMenu;->mCurrentScreenRotation:I

    .line 137
    iput-boolean p3, p0, Lorg/chromium/chrome/browser/appmenu/AppMenu;->mIsByHardwareButton:Z

    .line 140
    iget-object v2, p0, Lorg/chromium/chrome/browser/appmenu/AppMenu;->mMenu:Landroid/view/Menu;

    invoke-interface {v2}, Landroid/view/Menu;->size()I

    move-result v5

    .line 141
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    move v2, v0

    .line 142
    :goto_1
    if-ge v2, v5, :cond_3

    .line 143
    iget-object v7, p0, Lorg/chromium/chrome/browser/appmenu/AppMenu;->mMenu:Landroid/view/Menu;

    invoke-interface {v7, v2}, Landroid/view/Menu;->getItem(I)Landroid/view/MenuItem;

    move-result-object v7

    .line 144
    invoke-interface {v7}, Landroid/view/MenuItem;->isVisible()Z

    move-result v8

    if-eqz v8, :cond_1

    .line 145
    invoke-interface {v6, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 142
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 120
    :cond_2
    iget-object v2, p0, Lorg/chromium/chrome/browser/appmenu/AppMenu;->mPopup:Landroid/widget/ListPopupWindow;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    sget v5, Lorg/chromium/chrome/R$drawable;->edge_menu_bg:I

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/widget/ListPopupWindow;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 122
    iget-object v2, p0, Lorg/chromium/chrome/browser/appmenu/AppMenu;->mPopup:Landroid/widget/ListPopupWindow;

    sget v4, Lorg/chromium/chrome/R$style;->OverflowMenuAnim:I

    invoke-virtual {v2, v4}, Landroid/widget/ListPopupWindow;->setAnimationStyle(I)V

    goto :goto_0

    .line 149
    :cond_3
    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2, v4}, Landroid/graphics/Rect;-><init>(Landroid/graphics/Rect;)V

    .line 150
    if-eqz p3, :cond_4

    if-eqz v3, :cond_4

    .line 151
    new-instance v4, Landroid/graphics/Rect;

    invoke-direct {v4}, Landroid/graphics/Rect;-><init>()V

    .line 152
    invoke-virtual {v3, v4}, Landroid/graphics/drawable/Drawable;->getPadding(Landroid/graphics/Rect;)Z

    .line 153
    iget v3, v4, Landroid/graphics/Rect;->top:I

    iput v3, v2, Landroid/graphics/Rect;->top:I

    .line 154
    iget v3, v4, Landroid/graphics/Rect;->bottom:I

    iput v3, v2, Landroid/graphics/Rect;->bottom:I

    .line 157
    :cond_4
    iget-boolean v3, p0, Lorg/chromium/chrome/browser/appmenu/AppMenu;->mIsByHardwareButton:Z

    if-nez v3, :cond_5

    move v0, v1

    .line 161
    :cond_5
    new-instance v3, Lorg/chromium/chrome/browser/appmenu/AppMenuAdapter;

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v4

    invoke-direct {v3, p0, v6, v4, v0}, Lorg/chromium/chrome/browser/appmenu/AppMenuAdapter;-><init>(Lorg/chromium/chrome/browser/appmenu/AppMenu;Ljava/util/List;Landroid/view/LayoutInflater;Z)V

    iput-object v3, p0, Lorg/chromium/chrome/browser/appmenu/AppMenu;->mAdapter:Lorg/chromium/chrome/browser/appmenu/AppMenuAdapter;

    .line 163
    iget-object v0, p0, Lorg/chromium/chrome/browser/appmenu/AppMenu;->mPopup:Landroid/widget/ListPopupWindow;

    iget-object v3, p0, Lorg/chromium/chrome/browser/appmenu/AppMenu;->mAdapter:Lorg/chromium/chrome/browser/appmenu/AppMenuAdapter;

    invoke-virtual {v0, v3}, Landroid/widget/ListPopupWindow;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 165
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {p0, v0, p5, p6, v2}, Lorg/chromium/chrome/browser/appmenu/AppMenu;->setMenuHeight(ILandroid/graphics/Rect;ILandroid/graphics/Rect;)V

    .line 166
    iget-object v0, p0, Lorg/chromium/chrome/browser/appmenu/AppMenu;->mPopup:Landroid/widget/ListPopupWindow;

    iget v3, p0, Lorg/chromium/chrome/browser/appmenu/AppMenu;->mCurrentScreenRotation:I

    invoke-direct {p0, v0, v3, p5, v2}, Lorg/chromium/chrome/browser/appmenu/AppMenu;->setPopupOffset(Landroid/widget/ListPopupWindow;ILandroid/graphics/Rect;Landroid/graphics/Rect;)V

    .line 167
    iget-object v0, p0, Lorg/chromium/chrome/browser/appmenu/AppMenu;->mPopup:Landroid/widget/ListPopupWindow;

    invoke-virtual {v0, p0}, Landroid/widget/ListPopupWindow;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 168
    iget-object v0, p0, Lorg/chromium/chrome/browser/appmenu/AppMenu;->mPopup:Landroid/widget/ListPopupWindow;

    invoke-virtual {v0}, Landroid/widget/ListPopupWindow;->show()V

    .line 169
    iget-object v0, p0, Lorg/chromium/chrome/browser/appmenu/AppMenu;->mPopup:Landroid/widget/ListPopupWindow;

    invoke-virtual {v0}, Landroid/widget/ListPopupWindow;->getListView()Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setItemsCanFocus(Z)V

    .line 170
    iget-object v0, p0, Lorg/chromium/chrome/browser/appmenu/AppMenu;->mPopup:Landroid/widget/ListPopupWindow;

    invoke-virtual {v0}, Landroid/widget/ListPopupWindow;->getListView()Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/widget/ListView;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 172
    iget-object v0, p0, Lorg/chromium/chrome/browser/appmenu/AppMenu;->mHandler:Lorg/chromium/chrome/browser/appmenu/AppMenuHandler;

    invoke-virtual {v0, v1}, Lorg/chromium/chrome/browser/appmenu/AppMenuHandler;->onMenuVisibilityChanged(Z)V

    .line 174
    iget v0, p0, Lorg/chromium/chrome/browser/appmenu/AppMenu;->mVerticalFadeDistance:I

    if-lez v0, :cond_6

    .line 175
    iget-object v0, p0, Lorg/chromium/chrome/browser/appmenu/AppMenu;->mPopup:Landroid/widget/ListPopupWindow;

    invoke-virtual {v0}, Landroid/widget/ListPopupWindow;->getListView()Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setVerticalFadingEdgeEnabled(Z)V

    .line 176
    iget-object v0, p0, Lorg/chromium/chrome/browser/appmenu/AppMenu;->mPopup:Landroid/widget/ListPopupWindow;

    invoke-virtual {v0}, Landroid/widget/ListPopupWindow;->getListView()Landroid/widget/ListView;

    move-result-object v0

    iget v1, p0, Lorg/chromium/chrome/browser/appmenu/AppMenu;->mVerticalFadeDistance:I

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setFadingEdgeLength(I)V

    .line 180
    :cond_6
    invoke-static {}, Lorg/chromium/base/SysUtils;->isLowEndDevice()Z

    move-result v0

    if-nez v0, :cond_7

    .line 181
    iget-object v0, p0, Lorg/chromium/chrome/browser/appmenu/AppMenu;->mPopup:Landroid/widget/ListPopupWindow;

    invoke-virtual {v0}, Landroid/widget/ListPopupWindow;->getListView()Landroid/widget/ListView;

    move-result-object v0

    new-instance v1, Lorg/chromium/chrome/browser/appmenu/AppMenu$2;

    invoke-direct {v1, p0}, Lorg/chromium/chrome/browser/appmenu/AppMenu$2;-><init>(Lorg/chromium/chrome/browser/appmenu/AppMenu;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->addOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    .line 190
    :cond_7
    return-void
.end method
