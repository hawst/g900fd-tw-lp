.class public final Lorg/chromium/chrome/browser/dom_distiller/DomDistillerFeedbackReporter;
.super Ljava/lang/Object;
.source "DomDistillerFeedbackReporter.java"

# interfaces
.implements Lorg/chromium/chrome/browser/dom_distiller/DomDistillerFeedbackReportingView$FeedbackObserver;


# annotations
.annotation runtime Lorg/chromium/base/JNINamespace;
.end annotation


# static fields
.field private static sExternalFeedbackReporter:Lorg/chromium/chrome/browser/dom_distiller/ExternalFeedbackReporter;


# instance fields
.field private mContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

.field private final mNativePointer:J

.field private mReportingView:Lorg/chromium/chrome/browser/dom_distiller/DomDistillerFeedbackReportingView;

.field private final mTab:Lorg/chromium/chrome/browser/Tab;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 25
    new-instance v0, Lorg/chromium/chrome/browser/dom_distiller/DomDistillerFeedbackReporter$NoOpExternalFeedbackReporter;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lorg/chromium/chrome/browser/dom_distiller/DomDistillerFeedbackReporter$NoOpExternalFeedbackReporter;-><init>(Lorg/chromium/chrome/browser/dom_distiller/DomDistillerFeedbackReporter$1;)V

    sput-object v0, Lorg/chromium/chrome/browser/dom_distiller/DomDistillerFeedbackReporter;->sExternalFeedbackReporter:Lorg/chromium/chrome/browser/dom_distiller/ExternalFeedbackReporter;

    return-void
.end method

.method public constructor <init>(Lorg/chromium/chrome/browser/Tab;)V
    .locals 2

    .prologue
    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 58
    invoke-direct {p0}, Lorg/chromium/chrome/browser/dom_distiller/DomDistillerFeedbackReporter;->nativeInit()J

    move-result-wide v0

    iput-wide v0, p0, Lorg/chromium/chrome/browser/dom_distiller/DomDistillerFeedbackReporter;->mNativePointer:J

    .line 59
    iput-object p1, p0, Lorg/chromium/chrome/browser/dom_distiller/DomDistillerFeedbackReporter;->mTab:Lorg/chromium/chrome/browser/Tab;

    .line 60
    iget-object v0, p0, Lorg/chromium/chrome/browser/dom_distiller/DomDistillerFeedbackReporter;->mTab:Lorg/chromium/chrome/browser/Tab;

    invoke-direct {p0}, Lorg/chromium/chrome/browser/dom_distiller/DomDistillerFeedbackReporter;->createTabObserver()Lorg/chromium/chrome/browser/TabObserver;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/chromium/chrome/browser/Tab;->addObserver(Lorg/chromium/chrome/browser/TabObserver;)V

    .line 61
    invoke-direct {p0}, Lorg/chromium/chrome/browser/dom_distiller/DomDistillerFeedbackReporter;->updatePointers()V

    .line 62
    return-void
.end method

.method static synthetic access$100(Lorg/chromium/chrome/browser/dom_distiller/DomDistillerFeedbackReporter;)V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Lorg/chromium/chrome/browser/dom_distiller/DomDistillerFeedbackReporter;->updatePointers()V

    return-void
.end method

.method static synthetic access$200(Lorg/chromium/chrome/browser/dom_distiller/DomDistillerFeedbackReporter;)J
    .locals 2

    .prologue
    .line 22
    iget-wide v0, p0, Lorg/chromium/chrome/browser/dom_distiller/DomDistillerFeedbackReporter;->mNativePointer:J

    return-wide v0
.end method

.method static synthetic access$300(Lorg/chromium/chrome/browser/dom_distiller/DomDistillerFeedbackReporter;J)V
    .locals 1

    .prologue
    .line 22
    invoke-direct {p0, p1, p2}, Lorg/chromium/chrome/browser/dom_distiller/DomDistillerFeedbackReporter;->nativeDestroy(J)V

    return-void
.end method

.method static synthetic access$402(Lorg/chromium/chrome/browser/dom_distiller/DomDistillerFeedbackReporter;Lorg/chromium/content/browser/ContentViewCore;)Lorg/chromium/content/browser/ContentViewCore;
    .locals 0

    .prologue
    .line 22
    iput-object p1, p0, Lorg/chromium/chrome/browser/dom_distiller/DomDistillerFeedbackReporter;->mContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

    return-object p1
.end method

.method private createTabObserver()Lorg/chromium/chrome/browser/TabObserver;
    .locals 1

    .prologue
    .line 127
    new-instance v0, Lorg/chromium/chrome/browser/dom_distiller/DomDistillerFeedbackReporter$1;

    invoke-direct {v0, p0}, Lorg/chromium/chrome/browser/dom_distiller/DomDistillerFeedbackReporter$1;-><init>(Lorg/chromium/chrome/browser/dom_distiller/DomDistillerFeedbackReporter;)V

    return-object v0
.end method

.method private dismissOverlay()V
    .locals 2

    .prologue
    .line 106
    iget-object v0, p0, Lorg/chromium/chrome/browser/dom_distiller/DomDistillerFeedbackReporter;->mReportingView:Lorg/chromium/chrome/browser/dom_distiller/DomDistillerFeedbackReportingView;

    if-eqz v0, :cond_0

    .line 107
    iget-object v0, p0, Lorg/chromium/chrome/browser/dom_distiller/DomDistillerFeedbackReporter;->mReportingView:Lorg/chromium/chrome/browser/dom_distiller/DomDistillerFeedbackReportingView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lorg/chromium/chrome/browser/dom_distiller/DomDistillerFeedbackReportingView;->dismiss(Z)Z

    .line 108
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/chromium/chrome/browser/dom_distiller/DomDistillerFeedbackReporter;->mReportingView:Lorg/chromium/chrome/browser/dom_distiller/DomDistillerFeedbackReportingView;

    .line 110
    :cond_0
    return-void
.end method

.method public static isEnabled()Z
    .locals 1

    .prologue
    .line 48
    const/4 v0, 0x0

    return v0
.end method

.method private native nativeDestroy(J)V
.end method

.method private native nativeInit()J
.end method

.method private static native nativeIsEnabled()Z
.end method

.method private native nativeReplaceWebContents(JLorg/chromium/content_public/browser/WebContents;)V
.end method

.method private static native nativeReportQuality(Z)V
.end method

.method private recordQuality(Z)V
    .locals 3

    .prologue
    .line 84
    invoke-static {p1}, Lorg/chromium/chrome/browser/dom_distiller/DomDistillerFeedbackReporter;->nativeReportQuality(Z)V

    .line 85
    if-nez p1, :cond_0

    .line 86
    iget-object v0, p0, Lorg/chromium/chrome/browser/dom_distiller/DomDistillerFeedbackReporter;->mTab:Lorg/chromium/chrome/browser/Tab;

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/Tab;->getWindowAndroid()Lorg/chromium/ui/base/WindowAndroid;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/ui/base/WindowAndroid;->getActivity()Ljava/lang/ref/WeakReference;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    .line 87
    iget-object v1, p0, Lorg/chromium/chrome/browser/dom_distiller/DomDistillerFeedbackReporter;->mContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

    invoke-virtual {v1}, Lorg/chromium/content/browser/ContentViewCore;->getWebContents()Lorg/chromium/content_public/browser/WebContents;

    move-result-object v1

    invoke-interface {v1}, Lorg/chromium/content_public/browser/WebContents;->getUrl()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lorg/chromium/components/dom_distiller/core/DomDistillerUrlUtils;->getOriginalUrlFromDistillerUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 89
    sget-object v2, Lorg/chromium/chrome/browser/dom_distiller/DomDistillerFeedbackReporter;->sExternalFeedbackReporter:Lorg/chromium/chrome/browser/dom_distiller/ExternalFeedbackReporter;

    invoke-interface {v2, v0, v1, p1}, Lorg/chromium/chrome/browser/dom_distiller/ExternalFeedbackReporter;->reportFeedback(Landroid/app/Activity;Ljava/lang/String;Z)V

    .line 91
    :cond_0
    return-void
.end method

.method public static setExternalFeedbackReporter(Lorg/chromium/chrome/browser/dom_distiller/ExternalFeedbackReporter;)V
    .locals 0

    .prologue
    .line 29
    sput-object p0, Lorg/chromium/chrome/browser/dom_distiller/DomDistillerFeedbackReporter;->sExternalFeedbackReporter:Lorg/chromium/chrome/browser/dom_distiller/ExternalFeedbackReporter;

    .line 30
    return-void
.end method

.method private showOverlay()V
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Lorg/chromium/chrome/browser/dom_distiller/DomDistillerFeedbackReporter;->mContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

    invoke-static {v0, p0}, Lorg/chromium/chrome/browser/dom_distiller/DomDistillerFeedbackReportingView;->create(Lorg/chromium/content/browser/ContentViewCore;Lorg/chromium/chrome/browser/dom_distiller/DomDistillerFeedbackReportingView$FeedbackObserver;)Lorg/chromium/chrome/browser/dom_distiller/DomDistillerFeedbackReportingView;

    move-result-object v0

    iput-object v0, p0, Lorg/chromium/chrome/browser/dom_distiller/DomDistillerFeedbackReporter;->mReportingView:Lorg/chromium/chrome/browser/dom_distiller/DomDistillerFeedbackReportingView;

    .line 99
    return-void
.end method

.method private updatePointers()V
    .locals 3

    .prologue
    .line 116
    iget-object v0, p0, Lorg/chromium/chrome/browser/dom_distiller/DomDistillerFeedbackReporter;->mTab:Lorg/chromium/chrome/browser/Tab;

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/Tab;->getContentViewCore()Lorg/chromium/content/browser/ContentViewCore;

    move-result-object v0

    iput-object v0, p0, Lorg/chromium/chrome/browser/dom_distiller/DomDistillerFeedbackReporter;->mContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

    .line 117
    iget-wide v0, p0, Lorg/chromium/chrome/browser/dom_distiller/DomDistillerFeedbackReporter;->mNativePointer:J

    iget-object v2, p0, Lorg/chromium/chrome/browser/dom_distiller/DomDistillerFeedbackReporter;->mTab:Lorg/chromium/chrome/browser/Tab;

    invoke-virtual {v2}, Lorg/chromium/chrome/browser/Tab;->getWebContents()Lorg/chromium/content_public/browser/WebContents;

    move-result-object v2

    invoke-direct {p0, v0, v1, v2}, Lorg/chromium/chrome/browser/dom_distiller/DomDistillerFeedbackReporter;->nativeReplaceWebContents(JLorg/chromium/content_public/browser/WebContents;)V

    .line 118
    return-void
.end method


# virtual methods
.method public final onNoPressed(Lorg/chromium/chrome/browser/dom_distiller/DomDistillerFeedbackReportingView;)V
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lorg/chromium/chrome/browser/dom_distiller/DomDistillerFeedbackReporter;->mReportingView:Lorg/chromium/chrome/browser/dom_distiller/DomDistillerFeedbackReportingView;

    if-eq p1, v0, :cond_0

    .line 76
    :goto_0
    return-void

    .line 74
    :cond_0
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lorg/chromium/chrome/browser/dom_distiller/DomDistillerFeedbackReporter;->recordQuality(Z)V

    .line 75
    invoke-direct {p0}, Lorg/chromium/chrome/browser/dom_distiller/DomDistillerFeedbackReporter;->dismissOverlay()V

    goto :goto_0
.end method

.method public final onYesPressed(Lorg/chromium/chrome/browser/dom_distiller/DomDistillerFeedbackReportingView;)V
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lorg/chromium/chrome/browser/dom_distiller/DomDistillerFeedbackReporter;->mReportingView:Lorg/chromium/chrome/browser/dom_distiller/DomDistillerFeedbackReportingView;

    if-eq p1, v0, :cond_0

    .line 69
    :goto_0
    return-void

    .line 67
    :cond_0
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lorg/chromium/chrome/browser/dom_distiller/DomDistillerFeedbackReporter;->recordQuality(Z)V

    .line 68
    invoke-direct {p0}, Lorg/chromium/chrome/browser/dom_distiller/DomDistillerFeedbackReporter;->dismissOverlay()V

    goto :goto_0
.end method
