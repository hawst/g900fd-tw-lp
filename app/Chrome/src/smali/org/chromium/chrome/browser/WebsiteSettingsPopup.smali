.class public Lorg/chromium/chrome/browser/WebsiteSettingsPopup;
.super Ljava/lang/Object;
.source "WebsiteSettingsPopup.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private mCertificateLayout:Landroid/view/ViewGroup;

.field private mCertificateViewer:Landroid/widget/TextView;

.field private final mContainer:Landroid/widget/LinearLayout;

.field private final mContext:Landroid/content/Context;

.field private mDescriptionLayout:Landroid/view/ViewGroup;

.field private final mDialog:Landroid/app/Dialog;

.field private mLinkUrl:Ljava/lang/String;

.field private mMoreInfoLink:Landroid/widget/TextView;

.field private final mNativeWebsiteSettingsPopup:J

.field private final mPaddingThin:I

.field private final mPaddingWide:I

.field private mResetCertDecisionsButton:Landroid/widget/Button;

.field private final mWebContents:Lorg/chromium/content_public/browser/WebContents;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 36
    const-class v0, Lorg/chromium/chrome/browser/WebsiteSettingsPopup;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/chromium/chrome/browser/WebsiteSettingsPopup;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private constructor <init>(Landroid/content/Context;Lorg/chromium/content_public/browser/WebContents;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    iput-object p1, p0, Lorg/chromium/chrome/browser/WebsiteSettingsPopup;->mContext:Landroid/content/Context;

    .line 53
    iput-object p2, p0, Lorg/chromium/chrome/browser/WebsiteSettingsPopup;->mWebContents:Lorg/chromium/content_public/browser/WebContents;

    .line 55
    new-instance v0, Landroid/widget/LinearLayout;

    iget-object v1, p0, Lorg/chromium/chrome/browser/WebsiteSettingsPopup;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lorg/chromium/chrome/browser/WebsiteSettingsPopup;->mContainer:Landroid/widget/LinearLayout;

    .line 56
    iget-object v0, p0, Lorg/chromium/chrome/browser/WebsiteSettingsPopup;->mContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v5}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 57
    iget-object v0, p0, Lorg/chromium/chrome/browser/WebsiteSettingsPopup;->mContainer:Landroid/widget/LinearLayout;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setBackgroundColor(I)V

    .line 58
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lorg/chromium/chrome/R$dimen;->certificate_viewer_padding_wide:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lorg/chromium/chrome/browser/WebsiteSettingsPopup;->mPaddingWide:I

    .line 60
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lorg/chromium/chrome/R$dimen;->certificate_viewer_padding_thin:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lorg/chromium/chrome/browser/WebsiteSettingsPopup;->mPaddingThin:I

    .line 62
    iget-object v0, p0, Lorg/chromium/chrome/browser/WebsiteSettingsPopup;->mContainer:Landroid/widget/LinearLayout;

    iget v1, p0, Lorg/chromium/chrome/browser/WebsiteSettingsPopup;->mPaddingWide:I

    iget v2, p0, Lorg/chromium/chrome/browser/WebsiteSettingsPopup;->mPaddingWide:I

    iget v3, p0, Lorg/chromium/chrome/browser/WebsiteSettingsPopup;->mPaddingThin:I

    add-int/2addr v2, v3

    iget v3, p0, Lorg/chromium/chrome/browser/WebsiteSettingsPopup;->mPaddingWide:I

    iget v4, p0, Lorg/chromium/chrome/browser/WebsiteSettingsPopup;->mPaddingWide:I

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/widget/LinearLayout;->setPadding(IIII)V

    .line 65
    new-instance v0, Landroid/app/Dialog;

    iget-object v1, p0, Lorg/chromium/chrome/browser/WebsiteSettingsPopup;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/app/Dialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lorg/chromium/chrome/browser/WebsiteSettingsPopup;->mDialog:Landroid/app/Dialog;

    .line 66
    iget-object v0, p0, Lorg/chromium/chrome/browser/WebsiteSettingsPopup;->mDialog:Landroid/app/Dialog;

    invoke-virtual {v0, v5}, Landroid/app/Dialog;->requestWindowFeature(I)Z

    .line 67
    iget-object v0, p0, Lorg/chromium/chrome/browser/WebsiteSettingsPopup;->mDialog:Landroid/app/Dialog;

    invoke-virtual {v0, v5}, Landroid/app/Dialog;->setCanceledOnTouchOutside(Z)V

    .line 69
    invoke-static {p0, p2}, Lorg/chromium/chrome/browser/WebsiteSettingsPopup;->nativeInit(Lorg/chromium/chrome/browser/WebsiteSettingsPopup;Lorg/chromium/content_public/browser/WebContents;)J

    move-result-wide v0

    iput-wide v0, p0, Lorg/chromium/chrome/browser/WebsiteSettingsPopup;->mNativeWebsiteSettingsPopup:J

    .line 70
    new-instance v0, Lorg/chromium/chrome/browser/WebsiteSettingsPopup$1;

    iget-object v1, p0, Lorg/chromium/chrome/browser/WebsiteSettingsPopup;->mWebContents:Lorg/chromium/content_public/browser/WebContents;

    invoke-direct {v0, p0, v1}, Lorg/chromium/chrome/browser/WebsiteSettingsPopup$1;-><init>(Lorg/chromium/chrome/browser/WebsiteSettingsPopup;Lorg/chromium/content_public/browser/WebContents;)V

    .line 79
    iget-object v1, p0, Lorg/chromium/chrome/browser/WebsiteSettingsPopup;->mDialog:Landroid/app/Dialog;

    new-instance v2, Lorg/chromium/chrome/browser/WebsiteSettingsPopup$2;

    invoke-direct {v2, p0, v0}, Lorg/chromium/chrome/browser/WebsiteSettingsPopup$2;-><init>(Lorg/chromium/chrome/browser/WebsiteSettingsPopup;Lorg/chromium/content/browser/WebContentsObserverAndroid;)V

    invoke-virtual {v1, v2}, Landroid/app/Dialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 87
    return-void
.end method

.method static synthetic access$000(Lorg/chromium/chrome/browser/WebsiteSettingsPopup;)Landroid/app/Dialog;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lorg/chromium/chrome/browser/WebsiteSettingsPopup;->mDialog:Landroid/app/Dialog;

    return-object v0
.end method

.method static synthetic access$100(Lorg/chromium/chrome/browser/WebsiteSettingsPopup;)J
    .locals 2

    .prologue
    .line 36
    iget-wide v0, p0, Lorg/chromium/chrome/browser/WebsiteSettingsPopup;->mNativeWebsiteSettingsPopup:J

    return-wide v0
.end method

.method static synthetic access$200(Lorg/chromium/chrome/browser/WebsiteSettingsPopup;J)V
    .locals 1

    .prologue
    .line 36
    invoke-direct {p0, p1, p2}, Lorg/chromium/chrome/browser/WebsiteSettingsPopup;->nativeDestroy(J)V

    return-void
.end method

.method private addCertificateSection(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 96
    invoke-direct {p0, p1, p2, p3}, Lorg/chromium/chrome/browser/WebsiteSettingsPopup;->addSection(ILjava/lang/String;Ljava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 97
    sget-boolean v1, Lorg/chromium/chrome/browser/WebsiteSettingsPopup;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lorg/chromium/chrome/browser/WebsiteSettingsPopup;->mCertificateLayout:Landroid/view/ViewGroup;

    if-eqz v1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 98
    :cond_0
    sget v1, Lorg/chromium/chrome/R$id;->website_settings_text_layout:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lorg/chromium/chrome/browser/WebsiteSettingsPopup;->mCertificateLayout:Landroid/view/ViewGroup;

    .line 99
    if-eqz p4, :cond_1

    invoke-virtual {p4}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 100
    invoke-direct {p0, p4}, Lorg/chromium/chrome/browser/WebsiteSettingsPopup;->setCertificateViewer(Ljava/lang/String;)V

    .line 102
    :cond_1
    return-void
.end method

.method private addDescriptionSection(ILjava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 110
    invoke-direct {p0, p1, p2, p3}, Lorg/chromium/chrome/browser/WebsiteSettingsPopup;->addSection(ILjava/lang/String;Ljava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 111
    sget-boolean v1, Lorg/chromium/chrome/browser/WebsiteSettingsPopup;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lorg/chromium/chrome/browser/WebsiteSettingsPopup;->mDescriptionLayout:Landroid/view/ViewGroup;

    if-eqz v1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 112
    :cond_0
    sget v1, Lorg/chromium/chrome/R$id;->website_settings_text_layout:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lorg/chromium/chrome/browser/WebsiteSettingsPopup;->mDescriptionLayout:Landroid/view/ViewGroup;

    .line 113
    return-void
.end method

.method private addMoreInfoLink(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 171
    const-string/jumbo v0, "http://www.google.com/support/chrome/bin/answer.py?answer=95617"

    invoke-direct {p0, p1, v0}, Lorg/chromium/chrome/browser/WebsiteSettingsPopup;->addUrl(Ljava/lang/String;Ljava/lang/String;)V

    .line 172
    return-void
.end method

.method private addResetCertDecisionsButton(Ljava/lang/String;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 148
    sget-boolean v0, Lorg/chromium/chrome/browser/WebsiteSettingsPopup;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-wide v0, p0, Lorg/chromium/chrome/browser/WebsiteSettingsPopup;->mNativeWebsiteSettingsPopup:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 149
    :cond_0
    sget-boolean v0, Lorg/chromium/chrome/browser/WebsiteSettingsPopup;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lorg/chromium/chrome/browser/WebsiteSettingsPopup;->mResetCertDecisionsButton:Landroid/widget/Button;

    if-eqz v0, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 151
    :cond_1
    new-instance v0, Landroid/widget/Button;

    iget-object v1, p0, Lorg/chromium/chrome/browser/WebsiteSettingsPopup;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/Button;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lorg/chromium/chrome/browser/WebsiteSettingsPopup;->mResetCertDecisionsButton:Landroid/widget/Button;

    .line 152
    iget-object v0, p0, Lorg/chromium/chrome/browser/WebsiteSettingsPopup;->mResetCertDecisionsButton:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 153
    iget-object v0, p0, Lorg/chromium/chrome/browser/WebsiteSettingsPopup;->mResetCertDecisionsButton:Landroid/widget/Button;

    iget-object v1, p0, Lorg/chromium/chrome/browser/WebsiteSettingsPopup;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lorg/chromium/chrome/R$drawable;->website_settings_reset_cert_decisions:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-static {v0, v1}, Lorg/chromium/base/ApiCompatibilityUtils;->setBackgroundForView(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 156
    iget-object v0, p0, Lorg/chromium/chrome/browser/WebsiteSettingsPopup;->mResetCertDecisionsButton:Landroid/widget/Button;

    iget-object v1, p0, Lorg/chromium/chrome/browser/WebsiteSettingsPopup;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lorg/chromium/chrome/R$color;->website_settings_popup_reset_cert_decisions_button:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setTextColor(I)V

    .line 159
    iget-object v0, p0, Lorg/chromium/chrome/browser/WebsiteSettingsPopup;->mResetCertDecisionsButton:Landroid/widget/Button;

    const/high16 v1, 0x41400000    # 12.0f

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setTextSize(F)V

    .line 160
    iget-object v0, p0, Lorg/chromium/chrome/browser/WebsiteSettingsPopup;->mResetCertDecisionsButton:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 162
    new-instance v0, Landroid/widget/LinearLayout;

    iget-object v1, p0, Lorg/chromium/chrome/browser/WebsiteSettingsPopup;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 163
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 164
    iget-object v1, p0, Lorg/chromium/chrome/browser/WebsiteSettingsPopup;->mResetCertDecisionsButton:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 165
    iget v1, p0, Lorg/chromium/chrome/browser/WebsiteSettingsPopup;->mPaddingWide:I

    invoke-virtual {v0, v4, v4, v4, v1}, Landroid/widget/LinearLayout;->setPadding(IIII)V

    .line 166
    iget-object v1, p0, Lorg/chromium/chrome/browser/WebsiteSettingsPopup;->mContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 167
    return-void
.end method

.method private addSection(ILjava/lang/String;Ljava/lang/String;)Landroid/view/View;
    .locals 4

    .prologue
    const/16 v3, 0x8

    .line 116
    iget-object v0, p0, Lorg/chromium/chrome/browser/WebsiteSettingsPopup;->mContext:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Lorg/chromium/chrome/R$layout;->website_settings:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 117
    sget v0, Lorg/chromium/chrome/R$id;->website_settings_icon:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 118
    invoke-static {p1}, Lorg/chromium/chrome/browser/ResourceId;->mapToDrawableId(I)I

    move-result v2

    .line 119
    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 121
    sget v0, Lorg/chromium/chrome/R$id;->website_settings_headline:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 122
    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 123
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 125
    :cond_0
    sget v0, Lorg/chromium/chrome/R$id;->website_settings_description:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 126
    invoke-virtual {v0, p3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 127
    const/high16 v2, 0x41400000    # 12.0f

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextSize(F)V

    .line 128
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 130
    :cond_1
    iget-object v0, p0, Lorg/chromium/chrome/browser/WebsiteSettingsPopup;->mContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 131
    return-object v1
.end method

.method private addUrl(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 176
    new-instance v0, Landroid/widget/TextView;

    iget-object v1, p0, Lorg/chromium/chrome/browser/WebsiteSettingsPopup;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lorg/chromium/chrome/browser/WebsiteSettingsPopup;->mMoreInfoLink:Landroid/widget/TextView;

    .line 177
    iput-object p2, p0, Lorg/chromium/chrome/browser/WebsiteSettingsPopup;->mLinkUrl:Ljava/lang/String;

    .line 178
    iget-object v0, p0, Lorg/chromium/chrome/browser/WebsiteSettingsPopup;->mMoreInfoLink:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 179
    iget-object v0, p0, Lorg/chromium/chrome/browser/WebsiteSettingsPopup;->mMoreInfoLink:Landroid/widget/TextView;

    iget-object v1, p0, Lorg/chromium/chrome/browser/WebsiteSettingsPopup;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lorg/chromium/chrome/R$color;->website_settings_popup_text_link:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 181
    iget-object v0, p0, Lorg/chromium/chrome/browser/WebsiteSettingsPopup;->mMoreInfoLink:Landroid/widget/TextView;

    const/high16 v1, 0x41400000    # 12.0f

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextSize(F)V

    .line 182
    iget-object v0, p0, Lorg/chromium/chrome/browser/WebsiteSettingsPopup;->mMoreInfoLink:Landroid/widget/TextView;

    iget v1, p0, Lorg/chromium/chrome/browser/WebsiteSettingsPopup;->mPaddingWide:I

    iget v2, p0, Lorg/chromium/chrome/browser/WebsiteSettingsPopup;->mPaddingThin:I

    add-int/2addr v1, v2

    iget v2, p0, Lorg/chromium/chrome/browser/WebsiteSettingsPopup;->mPaddingWide:I

    invoke-virtual {v0, v3, v1, v3, v2}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 183
    iget-object v0, p0, Lorg/chromium/chrome/browser/WebsiteSettingsPopup;->mMoreInfoLink:Landroid/widget/TextView;

    invoke-virtual {v0, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 184
    iget-object v0, p0, Lorg/chromium/chrome/browser/WebsiteSettingsPopup;->mDescriptionLayout:Landroid/view/ViewGroup;

    iget-object v1, p0, Lorg/chromium/chrome/browser/WebsiteSettingsPopup;->mMoreInfoLink:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 185
    return-void
.end method

.method private native nativeDestroy(J)V
.end method

.method private native nativeGetCertificateChain(Lorg/chromium/content_public/browser/WebContents;)[[B
.end method

.method private static native nativeInit(Lorg/chromium/chrome/browser/WebsiteSettingsPopup;Lorg/chromium/content_public/browser/WebContents;)J
.end method

.method private native nativeResetCertDecisions(JLorg/chromium/content_public/browser/WebContents;)V
.end method

.method private setCertificateViewer(Ljava/lang/String;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 135
    sget-boolean v0, Lorg/chromium/chrome/browser/WebsiteSettingsPopup;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lorg/chromium/chrome/browser/WebsiteSettingsPopup;->mCertificateViewer:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 136
    :cond_0
    new-instance v0, Landroid/widget/TextView;

    iget-object v1, p0, Lorg/chromium/chrome/browser/WebsiteSettingsPopup;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lorg/chromium/chrome/browser/WebsiteSettingsPopup;->mCertificateViewer:Landroid/widget/TextView;

    .line 137
    iget-object v0, p0, Lorg/chromium/chrome/browser/WebsiteSettingsPopup;->mCertificateViewer:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 138
    iget-object v0, p0, Lorg/chromium/chrome/browser/WebsiteSettingsPopup;->mCertificateViewer:Landroid/widget/TextView;

    iget-object v1, p0, Lorg/chromium/chrome/browser/WebsiteSettingsPopup;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lorg/chromium/chrome/R$color;->website_settings_popup_text_link:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 140
    iget-object v0, p0, Lorg/chromium/chrome/browser/WebsiteSettingsPopup;->mCertificateViewer:Landroid/widget/TextView;

    const/high16 v1, 0x41400000    # 12.0f

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextSize(F)V

    .line 141
    iget-object v0, p0, Lorg/chromium/chrome/browser/WebsiteSettingsPopup;->mCertificateViewer:Landroid/widget/TextView;

    invoke-virtual {v0, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 142
    iget-object v0, p0, Lorg/chromium/chrome/browser/WebsiteSettingsPopup;->mCertificateViewer:Landroid/widget/TextView;

    iget v1, p0, Lorg/chromium/chrome/browser/WebsiteSettingsPopup;->mPaddingWide:I

    iget v2, p0, Lorg/chromium/chrome/browser/WebsiteSettingsPopup;->mPaddingWide:I

    invoke-virtual {v0, v3, v1, v3, v2}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 143
    iget-object v0, p0, Lorg/chromium/chrome/browser/WebsiteSettingsPopup;->mCertificateLayout:Landroid/view/ViewGroup;

    iget-object v1, p0, Lorg/chromium/chrome/browser/WebsiteSettingsPopup;->mCertificateViewer:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 144
    return-void
.end method

.method public static show(Landroid/content/Context;Lorg/chromium/content_public/browser/WebContents;)V
    .locals 1

    .prologue
    .line 235
    new-instance v0, Lorg/chromium/chrome/browser/WebsiteSettingsPopup;

    invoke-direct {v0, p0, p1}, Lorg/chromium/chrome/browser/WebsiteSettingsPopup;-><init>(Landroid/content/Context;Lorg/chromium/content_public/browser/WebContents;)V

    .line 236
    return-void
.end method

.method private showDialog()V
    .locals 4

    .prologue
    const/4 v3, -0x1

    .line 190
    new-instance v0, Landroid/widget/ScrollView;

    iget-object v1, p0, Lorg/chromium/chrome/browser/WebsiteSettingsPopup;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/ScrollView;-><init>(Landroid/content/Context;)V

    .line 191
    iget-object v1, p0, Lorg/chromium/chrome/browser/WebsiteSettingsPopup;->mContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/ScrollView;->addView(Landroid/view/View;)V

    .line 192
    iget-object v1, p0, Lorg/chromium/chrome/browser/WebsiteSettingsPopup;->mDialog:Landroid/app/Dialog;

    new-instance v2, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v2, v3, v3}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v1, v0, v2}, Landroid/app/Dialog;->addContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 195
    iget-object v0, p0, Lorg/chromium/chrome/browser/WebsiteSettingsPopup;->mDialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    .line 196
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 200
    iget-object v0, p0, Lorg/chromium/chrome/browser/WebsiteSettingsPopup;->mDialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    .line 201
    iget-object v0, p0, Lorg/chromium/chrome/browser/WebsiteSettingsPopup;->mResetCertDecisionsButton:Landroid/widget/Button;

    if-ne v0, p1, :cond_1

    .line 202
    iget-wide v0, p0, Lorg/chromium/chrome/browser/WebsiteSettingsPopup;->mNativeWebsiteSettingsPopup:J

    iget-object v2, p0, Lorg/chromium/chrome/browser/WebsiteSettingsPopup;->mWebContents:Lorg/chromium/content_public/browser/WebContents;

    invoke-direct {p0, v0, v1, v2}, Lorg/chromium/chrome/browser/WebsiteSettingsPopup;->nativeResetCertDecisions(JLorg/chromium/content_public/browser/WebContents;)V

    .line 221
    :cond_0
    :goto_0
    return-void

    .line 203
    :cond_1
    iget-object v0, p0, Lorg/chromium/chrome/browser/WebsiteSettingsPopup;->mCertificateViewer:Landroid/widget/TextView;

    if-ne v0, p1, :cond_2

    .line 204
    iget-object v0, p0, Lorg/chromium/chrome/browser/WebsiteSettingsPopup;->mWebContents:Lorg/chromium/content_public/browser/WebContents;

    invoke-direct {p0, v0}, Lorg/chromium/chrome/browser/WebsiteSettingsPopup;->nativeGetCertificateChain(Lorg/chromium/content_public/browser/WebContents;)[[B

    move-result-object v0

    .line 205
    if-eqz v0, :cond_0

    .line 210
    iget-object v1, p0, Lorg/chromium/chrome/browser/WebsiteSettingsPopup;->mContext:Landroid/content/Context;

    invoke-static {v1, v0}, Lorg/chromium/chrome/browser/CertificateViewer;->showCertificateChain(Landroid/content/Context;[[B)V

    goto :goto_0

    .line 211
    :cond_2
    iget-object v0, p0, Lorg/chromium/chrome/browser/WebsiteSettingsPopup;->mMoreInfoLink:Landroid/widget/TextView;

    if-ne v0, p1, :cond_0

    .line 213
    :try_start_0
    iget-object v0, p0, Lorg/chromium/chrome/browser/WebsiteSettingsPopup;->mLinkUrl:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Landroid/content/Intent;->parseUri(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    .line 214
    const-string/jumbo v1, "create_new_tab"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 215
    const-string/jumbo v1, "com.android.browser.application_id"

    iget-object v2, p0, Lorg/chromium/chrome/browser/WebsiteSettingsPopup;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 216
    iget-object v1, p0, Lorg/chromium/chrome/browser/WebsiteSettingsPopup;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/net/URISyntaxException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    goto :goto_0
.end method
