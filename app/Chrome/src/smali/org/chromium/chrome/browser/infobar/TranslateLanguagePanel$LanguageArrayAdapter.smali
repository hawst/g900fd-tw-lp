.class Lorg/chromium/chrome/browser/infobar/TranslateLanguagePanel$LanguageArrayAdapter;
.super Landroid/widget/ArrayAdapter;
.source "TranslateLanguagePanel.java"


# instance fields
.field private mMinimumWidth:I

.field private final mTextTemplate:Landroid/text/SpannableString;


# direct methods
.method public constructor <init>(Landroid/content/Context;II)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 189
    invoke-direct {p0, p1, p2}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    .line 193
    if-nez p3, :cond_0

    sget v0, Lorg/chromium/chrome/R$string;->translate_options_source_hint:I

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 196
    :goto_0
    new-instance v1, Landroid/text/SpannableString;

    invoke-direct {v1, v0}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    iput-object v1, p0, Lorg/chromium/chrome/browser/infobar/TranslateLanguagePanel$LanguageArrayAdapter;->mTextTemplate:Landroid/text/SpannableString;

    .line 197
    iget-object v1, p0, Lorg/chromium/chrome/browser/infobar/TranslateLanguagePanel$LanguageArrayAdapter;->mTextTemplate:Landroid/text/SpannableString;

    new-instance v2, Landroid/text/style/ForegroundColorSpan;

    const v3, -0x777778

    invoke-direct {v2, v3}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    invoke-virtual {v1, v2, v4, v0, v4}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 199
    return-void

    .line 193
    :cond_0
    sget v0, Lorg/chromium/chrome/R$string;->translate_options_target_hint:I

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private getStringForLanguage(I)Ljava/lang/CharSequence;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 251
    invoke-virtual {p0, p1}, Lorg/chromium/chrome/browser/infobar/TranslateLanguagePanel$LanguageArrayAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/chromium/chrome/browser/infobar/TranslateLanguagePanel$SpinnerLanguageElement;

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/infobar/TranslateLanguagePanel$SpinnerLanguageElement;->toString()Ljava/lang/String;

    move-result-object v0

    .line 252
    new-instance v1, Landroid/text/SpannableString;

    invoke-direct {v1, v0}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 253
    new-instance v0, Landroid/text/style/ForegroundColorSpan;

    const/high16 v2, -0x1000000

    invoke-direct {v0, v2}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    invoke-virtual {v1}, Landroid/text/SpannableString;->length()I

    move-result v2

    invoke-virtual {v1, v0, v3, v2, v3}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 254
    iget-object v0, p0, Lorg/chromium/chrome/browser/infobar/TranslateLanguagePanel$LanguageArrayAdapter;->mTextTemplate:Landroid/text/SpannableString;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/CharSequence;

    aput-object v1, v2, v3

    invoke-static {v0, v2}, Landroid/text/TextUtils;->expandTemplate(Ljava/lang/CharSequence;[Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getDropDownView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    .line 221
    instance-of v0, p2, Landroid/widget/TextView;

    if-nez v0, :cond_0

    .line 222
    invoke-virtual {p0}, Lorg/chromium/chrome/browser/infobar/TranslateLanguagePanel$LanguageArrayAdapter;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Lorg/chromium/chrome/R$layout;->infobar_spinner_item:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    move-object p2, v0

    .line 228
    :goto_0
    invoke-virtual {p0, p1}, Lorg/chromium/chrome/browser/infobar/TranslateLanguagePanel$LanguageArrayAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/chromium/chrome/browser/infobar/TranslateLanguagePanel$SpinnerLanguageElement;

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/infobar/TranslateLanguagePanel$SpinnerLanguageElement;->toString()Ljava/lang/String;

    move-result-object v0

    .line 229
    invoke-virtual {p2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 230
    return-object p2

    .line 225
    :cond_0
    check-cast p2, Landroid/widget/TextView;

    goto :goto_0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    .line 236
    instance-of v0, p2, Landroid/widget/TextView;

    if-nez v0, :cond_0

    .line 237
    invoke-virtual {p0}, Lorg/chromium/chrome/browser/infobar/TranslateLanguagePanel$LanguageArrayAdapter;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Lorg/chromium/chrome/R$layout;->infobar_text:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 242
    :goto_0
    sget-object v1, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 243
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMaxLines(I)V

    .line 244
    invoke-direct {p0, p1}, Lorg/chromium/chrome/browser/infobar/TranslateLanguagePanel$LanguageArrayAdapter;->getStringForLanguage(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 245
    iget v1, p0, Lorg/chromium/chrome/browser/infobar/TranslateLanguagePanel$LanguageArrayAdapter;->mMinimumWidth:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMinWidth(I)V

    .line 246
    return-object v0

    .line 240
    :cond_0
    check-cast p2, Landroid/widget/TextView;

    move-object v0, p2

    goto :goto_0
.end method

.method public measureWidthRequiredForView()V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 203
    iput v1, p0, Lorg/chromium/chrome/browser/infobar/TranslateLanguagePanel$LanguageArrayAdapter;->mMinimumWidth:I

    .line 205
    invoke-static {v1, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    .line 207
    new-instance v3, Landroid/widget/FrameLayout;

    invoke-virtual {p0}, Lorg/chromium/chrome/browser/infobar/TranslateLanguagePanel$LanguageArrayAdapter;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {v3, v0}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 208
    invoke-virtual {p0}, Lorg/chromium/chrome/browser/infobar/TranslateLanguagePanel$LanguageArrayAdapter;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v4, Lorg/chromium/chrome/R$layout;->infobar_text:I

    const/4 v5, 0x0

    invoke-virtual {v0, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 210
    invoke-virtual {v3, v0}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 211
    :goto_0
    invoke-virtual {p0}, Lorg/chromium/chrome/browser/infobar/TranslateLanguagePanel$LanguageArrayAdapter;->getCount()I

    move-result v3

    if-ge v1, v3, :cond_0

    .line 212
    invoke-direct {p0, v1}, Lorg/chromium/chrome/browser/infobar/TranslateLanguagePanel$LanguageArrayAdapter;->getStringForLanguage(I)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 213
    invoke-virtual {v0, v2, v2}, Landroid/widget/TextView;->measure(II)V

    .line 214
    iget v3, p0, Lorg/chromium/chrome/browser/infobar/TranslateLanguagePanel$LanguageArrayAdapter;->mMinimumWidth:I

    invoke-virtual {v0}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v4

    invoke-static {v3, v4}, Ljava/lang/Math;->max(II)I

    move-result v3

    iput v3, p0, Lorg/chromium/chrome/browser/infobar/TranslateLanguagePanel$LanguageArrayAdapter;->mMinimumWidth:I

    .line 211
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 216
    :cond_0
    return-void
.end method
