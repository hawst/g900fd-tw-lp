.class public Lorg/chromium/chrome/browser/UmaBridge;
.super Ljava/lang/Object;
.source "UmaBridge.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static menuShow()V
    .locals 0

    .prologue
    .line 20
    invoke-static {}, Lorg/chromium/chrome/browser/UmaBridge;->nativeRecordMenuShow()V

    .line 21
    return-void
.end method

.method private static native nativeRecordMenuShow()V
.end method

.method private static native nativeRecordUsingMenu(ZZ)V
.end method

.method public static usingMenu(ZZ)V
    .locals 0

    .prologue
    .line 29
    invoke-static {p0, p1}, Lorg/chromium/chrome/browser/UmaBridge;->nativeRecordUsingMenu(ZZ)V

    .line 30
    return-void
.end method
