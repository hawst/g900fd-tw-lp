.class public Lorg/chromium/chrome/browser/password_manager/PasswordAuthenticationManager;
.super Ljava/lang/Object;
.source "PasswordAuthenticationManager.java"


# static fields
.field private static sDelegate:Lorg/chromium/chrome/browser/password_manager/PasswordAuthenticationManager$PasswordAuthenticationDelegate;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 87
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$000(JZ)V
    .locals 0

    .prologue
    .line 13
    invoke-static {p0, p1, p2}, Lorg/chromium/chrome/browser/password_manager/PasswordAuthenticationManager;->nativeOnResult(JZ)V

    return-void
.end method

.method private static getDelegate()Lorg/chromium/chrome/browser/password_manager/PasswordAuthenticationManager$PasswordAuthenticationDelegate;
    .locals 2

    .prologue
    .line 90
    sget-object v0, Lorg/chromium/chrome/browser/password_manager/PasswordAuthenticationManager;->sDelegate:Lorg/chromium/chrome/browser/password_manager/PasswordAuthenticationManager$PasswordAuthenticationDelegate;

    if-nez v0, :cond_0

    .line 91
    new-instance v0, Lorg/chromium/chrome/browser/password_manager/PasswordAuthenticationManager$DefaultPasswordAuthenticationDelegate;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lorg/chromium/chrome/browser/password_manager/PasswordAuthenticationManager$DefaultPasswordAuthenticationDelegate;-><init>(Lorg/chromium/chrome/browser/password_manager/PasswordAuthenticationManager$1;)V

    sput-object v0, Lorg/chromium/chrome/browser/password_manager/PasswordAuthenticationManager;->sDelegate:Lorg/chromium/chrome/browser/password_manager/PasswordAuthenticationManager$PasswordAuthenticationDelegate;

    .line 93
    :cond_0
    sget-object v0, Lorg/chromium/chrome/browser/password_manager/PasswordAuthenticationManager;->sDelegate:Lorg/chromium/chrome/browser/password_manager/PasswordAuthenticationManager$PasswordAuthenticationDelegate;

    return-object v0
.end method

.method public static getPasswordProtectionString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 126
    invoke-static {}, Lorg/chromium/chrome/browser/password_manager/PasswordAuthenticationManager;->getDelegate()Lorg/chromium/chrome/browser/password_manager/PasswordAuthenticationManager$PasswordAuthenticationDelegate;

    move-result-object v0

    invoke-interface {v0}, Lorg/chromium/chrome/browser/password_manager/PasswordAuthenticationManager$PasswordAuthenticationDelegate;->getPasswordProtectionString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static isPasswordAuthenticationEnabled()Z
    .locals 1

    .prologue
    .line 107
    invoke-static {}, Lorg/chromium/chrome/browser/password_manager/PasswordAuthenticationManager;->getDelegate()Lorg/chromium/chrome/browser/password_manager/PasswordAuthenticationManager$PasswordAuthenticationDelegate;

    move-result-object v0

    invoke-interface {v0}, Lorg/chromium/chrome/browser/password_manager/PasswordAuthenticationManager$PasswordAuthenticationDelegate;->isPasswordAuthenticationEnabled()Z

    move-result v0

    return v0
.end method

.method private static native nativeOnResult(JZ)V
.end method

.method public static requestAuthentication(Lorg/chromium/chrome/browser/Tab;Lorg/chromium/chrome/browser/password_manager/PasswordAuthenticationManager$PasswordAuthenticationCallback;)V
    .locals 1

    .prologue
    .line 118
    invoke-static {}, Lorg/chromium/chrome/browser/password_manager/PasswordAuthenticationManager;->getDelegate()Lorg/chromium/chrome/browser/password_manager/PasswordAuthenticationManager$PasswordAuthenticationDelegate;

    move-result-object v0

    invoke-interface {v0, p0, p1}, Lorg/chromium/chrome/browser/password_manager/PasswordAuthenticationManager$PasswordAuthenticationDelegate;->requestAuthentication(Lorg/chromium/chrome/browser/Tab;Lorg/chromium/chrome/browser/password_manager/PasswordAuthenticationManager$PasswordAuthenticationCallback;)V

    .line 119
    return-void
.end method

.method public static setDelegate(Lorg/chromium/chrome/browser/password_manager/PasswordAuthenticationManager$PasswordAuthenticationDelegate;)V
    .locals 0

    .prologue
    .line 100
    sput-object p0, Lorg/chromium/chrome/browser/password_manager/PasswordAuthenticationManager;->sDelegate:Lorg/chromium/chrome/browser/password_manager/PasswordAuthenticationManager$PasswordAuthenticationDelegate;

    .line 101
    return-void
.end method
