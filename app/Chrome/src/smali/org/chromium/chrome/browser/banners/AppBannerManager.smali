.class public Lorg/chromium/chrome/browser/banners/AppBannerManager;
.super Ljava/lang/Object;
.source "AppBannerManager.java"

# interfaces
.implements Lorg/chromium/chrome/browser/banners/AppBannerView$Observer;
.implements Lorg/chromium/chrome/browser/banners/AppDetailsDelegate$Observer;


# annotations
.annotation runtime Lorg/chromium/base/JNINamespace;
.end annotation


# static fields
.field private static sAppDetailsDelegate:Lorg/chromium/chrome/browser/banners/AppDetailsDelegate;


# instance fields
.field private mAppData:Lorg/chromium/chrome/browser/banners/AppData;

.field private mBannerView:Lorg/chromium/chrome/browser/banners/AppBannerView;

.field private mContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

.field private final mNativePointer:J

.field private final mTab:Lorg/chromium/chrome/browser/Tab;


# direct methods
.method public constructor <init>(Lorg/chromium/chrome/browser/Tab;)V
    .locals 2

    .prologue
    .line 75
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 76
    invoke-direct {p0}, Lorg/chromium/chrome/browser/banners/AppBannerManager;->nativeInit()J

    move-result-wide v0

    iput-wide v0, p0, Lorg/chromium/chrome/browser/banners/AppBannerManager;->mNativePointer:J

    .line 77
    iput-object p1, p0, Lorg/chromium/chrome/browser/banners/AppBannerManager;->mTab:Lorg/chromium/chrome/browser/Tab;

    .line 78
    iget-object v0, p0, Lorg/chromium/chrome/browser/banners/AppBannerManager;->mTab:Lorg/chromium/chrome/browser/Tab;

    invoke-direct {p0}, Lorg/chromium/chrome/browser/banners/AppBannerManager;->createTabObserver()Lorg/chromium/chrome/browser/TabObserver;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/chromium/chrome/browser/Tab;->addObserver(Lorg/chromium/chrome/browser/TabObserver;)V

    .line 79
    invoke-direct {p0}, Lorg/chromium/chrome/browser/banners/AppBannerManager;->updatePointers()V

    .line 80
    return-void
.end method

.method static synthetic access$000(Lorg/chromium/chrome/browser/banners/AppBannerManager;)V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0}, Lorg/chromium/chrome/browser/banners/AppBannerManager;->updatePointers()V

    return-void
.end method

.method static synthetic access$100(Lorg/chromium/chrome/browser/banners/AppBannerManager;)J
    .locals 2

    .prologue
    .line 33
    iget-wide v0, p0, Lorg/chromium/chrome/browser/banners/AppBannerManager;->mNativePointer:J

    return-wide v0
.end method

.method static synthetic access$200(Lorg/chromium/chrome/browser/banners/AppBannerManager;J)V
    .locals 1

    .prologue
    .line 33
    invoke-direct {p0, p1, p2}, Lorg/chromium/chrome/browser/banners/AppBannerManager;->nativeDestroy(J)V

    return-void
.end method

.method static synthetic access$302(Lorg/chromium/chrome/browser/banners/AppBannerManager;Lorg/chromium/content/browser/ContentViewCore;)Lorg/chromium/content/browser/ContentViewCore;
    .locals 0

    .prologue
    .line 33
    iput-object p1, p0, Lorg/chromium/chrome/browser/banners/AppBannerManager;->mContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

    return-object p1
.end method

.method static synthetic access$400(Lorg/chromium/chrome/browser/banners/AppBannerManager;)V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0}, Lorg/chromium/chrome/browser/banners/AppBannerManager;->resetState()V

    return-void
.end method

.method private createBanner(Ljava/lang/String;Landroid/graphics/Bitmap;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 157
    iget-object v1, p0, Lorg/chromium/chrome/browser/banners/AppBannerManager;->mAppData:Lorg/chromium/chrome/browser/banners/AppData;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lorg/chromium/chrome/browser/banners/AppBannerManager;->mAppData:Lorg/chromium/chrome/browser/banners/AppData;

    invoke-virtual {v1}, Lorg/chromium/chrome/browser/banners/AppData;->siteUrl()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lorg/chromium/chrome/browser/banners/AppBannerManager;->isBannerForCurrentPage(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 166
    :cond_0
    :goto_0
    return v0

    .line 159
    :cond_1
    iget-object v1, p0, Lorg/chromium/chrome/browser/banners/AppBannerManager;->mAppData:Lorg/chromium/chrome/browser/banners/AppData;

    invoke-virtual {v1}, Lorg/chromium/chrome/browser/banners/AppData;->imageUrl()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 160
    invoke-direct {p0}, Lorg/chromium/chrome/browser/banners/AppBannerManager;->resetState()V

    goto :goto_0

    .line 164
    :cond_2
    iget-object v0, p0, Lorg/chromium/chrome/browser/banners/AppBannerManager;->mAppData:Lorg/chromium/chrome/browser/banners/AppData;

    new-instance v1, Landroid/graphics/drawable/BitmapDrawable;

    iget-object v2, p0, Lorg/chromium/chrome/browser/banners/AppBannerManager;->mContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

    invoke-virtual {v2}, Lorg/chromium/content/browser/ContentViewCore;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-direct {v1, v2, p2}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    invoke-virtual {v0, v1}, Lorg/chromium/chrome/browser/banners/AppData;->setIcon(Landroid/graphics/drawable/Drawable;)V

    .line 165
    iget-object v0, p0, Lorg/chromium/chrome/browser/banners/AppBannerManager;->mContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

    iget-object v1, p0, Lorg/chromium/chrome/browser/banners/AppBannerManager;->mAppData:Lorg/chromium/chrome/browser/banners/AppData;

    invoke-static {v0, p0, v1}, Lorg/chromium/chrome/browser/banners/AppBannerView;->create(Lorg/chromium/content/browser/ContentViewCore;Lorg/chromium/chrome/browser/banners/AppBannerView$Observer;Lorg/chromium/chrome/browser/banners/AppData;)Lorg/chromium/chrome/browser/banners/AppBannerView;

    move-result-object v0

    iput-object v0, p0, Lorg/chromium/chrome/browser/banners/AppBannerManager;->mBannerView:Lorg/chromium/chrome/browser/banners/AppBannerView;

    .line 166
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private createTabObserver()Lorg/chromium/chrome/browser/TabObserver;
    .locals 1

    .prologue
    .line 88
    new-instance v0, Lorg/chromium/chrome/browser/banners/AppBannerManager$1;

    invoke-direct {v0, p0}, Lorg/chromium/chrome/browser/banners/AppBannerManager$1;-><init>(Lorg/chromium/chrome/browser/banners/AppBannerManager;)V

    return-object v0
.end method

.method private dismissCurrentBanner(I)V
    .locals 1

    .prologue
    .line 176
    iget-object v0, p0, Lorg/chromium/chrome/browser/banners/AppBannerManager;->mBannerView:Lorg/chromium/chrome/browser/banners/AppBannerView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/chromium/chrome/browser/banners/AppBannerManager;->mBannerView:Lorg/chromium/chrome/browser/banners/AppBannerView;

    invoke-virtual {v0, p1}, Lorg/chromium/chrome/browser/banners/AppBannerView;->dismiss(I)V

    .line 177
    :cond_0
    invoke-direct {p0}, Lorg/chromium/chrome/browser/banners/AppBannerManager;->resetState()V

    .line 178
    return-void
.end method

.method private isBannerForCurrentPage(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 228
    iget-object v0, p0, Lorg/chromium/chrome/browser/banners/AppBannerManager;->mContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/chromium/chrome/browser/banners/AppBannerManager;->mContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

    invoke-virtual {v0}, Lorg/chromium/content/browser/ContentViewCore;->getWebContents()Lorg/chromium/content_public/browser/WebContents;

    move-result-object v0

    invoke-interface {v0}, Lorg/chromium/content_public/browser/WebContents;->getUrl()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isEnabled()Z
    .locals 1

    .prologue
    .line 59
    invoke-static {}, Lorg/chromium/chrome/browser/banners/AppBannerManager;->nativeIsEnabled()Z

    move-result v0

    return v0
.end method

.method private native nativeBlockBanner(JLjava/lang/String;Ljava/lang/String;)V
.end method

.method private native nativeDestroy(J)V
.end method

.method private native nativeFetchIcon(JLjava/lang/String;)Z
.end method

.method private native nativeInit()J
.end method

.method private static native nativeIsEnabled()Z
.end method

.method private static native nativeRecordDismissEvent(I)V
.end method

.method private static native nativeRecordInstallEvent(I)V
.end method

.method private native nativeReplaceWebContents(JLorg/chromium/content_public/browser/WebContents;)V
.end method

.method private prepareBanner(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 126
    iget-object v0, p0, Lorg/chromium/chrome/browser/banners/AppBannerManager;->mBannerView:Lorg/chromium/chrome/browser/banners/AppBannerView;

    if-eqz v0, :cond_0

    const/16 v0, 0x29

    invoke-direct {p0, v0}, Lorg/chromium/chrome/browser/banners/AppBannerManager;->dismissCurrentBanner(I)V

    .line 128
    :cond_0
    sget-object v0, Lorg/chromium/chrome/browser/banners/AppBannerManager;->sAppDetailsDelegate:Lorg/chromium/chrome/browser/banners/AppDetailsDelegate;

    if-eqz v0, :cond_1

    invoke-direct {p0, p1}, Lorg/chromium/chrome/browser/banners/AppBannerManager;->isBannerForCurrentPage(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 132
    :cond_1
    :goto_0
    return-void

    .line 130
    :cond_2
    iget-object v0, p0, Lorg/chromium/chrome/browser/banners/AppBannerManager;->mContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

    invoke-virtual {v0}, Lorg/chromium/content/browser/ContentViewCore;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lorg/chromium/chrome/browser/banners/AppBannerView;->getIconSize(Landroid/content/Context;)I

    move-result v0

    .line 131
    sget-object v1, Lorg/chromium/chrome/browser/banners/AppBannerManager;->sAppDetailsDelegate:Lorg/chromium/chrome/browser/banners/AppDetailsDelegate;

    invoke-virtual {v1, p0, p1, p2, v0}, Lorg/chromium/chrome/browser/banners/AppDetailsDelegate;->getAppDetailsAsynchronously(Lorg/chromium/chrome/browser/banners/AppDetailsDelegate$Observer;Ljava/lang/String;Ljava/lang/String;I)V

    goto :goto_0
.end method

.method private resetState()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 214
    iget-object v0, p0, Lorg/chromium/chrome/browser/banners/AppBannerManager;->mBannerView:Lorg/chromium/chrome/browser/banners/AppBannerView;

    if-eqz v0, :cond_0

    .line 215
    iget-object v0, p0, Lorg/chromium/chrome/browser/banners/AppBannerManager;->mBannerView:Lorg/chromium/chrome/browser/banners/AppBannerView;

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/banners/AppBannerView;->destroy()V

    .line 216
    iput-object v1, p0, Lorg/chromium/chrome/browser/banners/AppBannerManager;->mBannerView:Lorg/chromium/chrome/browser/banners/AppBannerView;

    .line 219
    :cond_0
    iput-object v1, p0, Lorg/chromium/chrome/browser/banners/AppBannerManager;->mAppData:Lorg/chromium/chrome/browser/banners/AppData;

    .line 220
    return-void
.end method

.method public static setAppDetailsDelegate(Lorg/chromium/chrome/browser/banners/AppDetailsDelegate;)V
    .locals 1

    .prologue
    .line 67
    sget-object v0, Lorg/chromium/chrome/browser/banners/AppBannerManager;->sAppDetailsDelegate:Lorg/chromium/chrome/browser/banners/AppDetailsDelegate;

    if-eqz v0, :cond_0

    sget-object v0, Lorg/chromium/chrome/browser/banners/AppBannerManager;->sAppDetailsDelegate:Lorg/chromium/chrome/browser/banners/AppDetailsDelegate;

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/banners/AppDetailsDelegate;->destroy()V

    .line 68
    :cond_0
    sput-object p0, Lorg/chromium/chrome/browser/banners/AppBannerManager;->sAppDetailsDelegate:Lorg/chromium/chrome/browser/banners/AppDetailsDelegate;

    .line 69
    return-void
.end method

.method private updatePointers()V
    .locals 3

    .prologue
    .line 113
    iget-object v0, p0, Lorg/chromium/chrome/browser/banners/AppBannerManager;->mContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

    iget-object v1, p0, Lorg/chromium/chrome/browser/banners/AppBannerManager;->mTab:Lorg/chromium/chrome/browser/Tab;

    invoke-virtual {v1}, Lorg/chromium/chrome/browser/Tab;->getContentViewCore()Lorg/chromium/content/browser/ContentViewCore;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 114
    iget-object v0, p0, Lorg/chromium/chrome/browser/banners/AppBannerManager;->mTab:Lorg/chromium/chrome/browser/Tab;

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/Tab;->getContentViewCore()Lorg/chromium/content/browser/ContentViewCore;

    move-result-object v0

    iput-object v0, p0, Lorg/chromium/chrome/browser/banners/AppBannerManager;->mContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

    .line 115
    :cond_0
    iget-wide v0, p0, Lorg/chromium/chrome/browser/banners/AppBannerManager;->mNativePointer:J

    iget-object v2, p0, Lorg/chromium/chrome/browser/banners/AppBannerManager;->mTab:Lorg/chromium/chrome/browser/Tab;

    invoke-virtual {v2}, Lorg/chromium/chrome/browser/Tab;->getWebContents()Lorg/chromium/content_public/browser/WebContents;

    move-result-object v2

    invoke-direct {p0, v0, v1, v2}, Lorg/chromium/chrome/browser/banners/AppBannerManager;->nativeReplaceWebContents(JLorg/chromium/content_public/browser/WebContents;)V

    .line 116
    return-void
.end method


# virtual methods
.method public onAppDetailsRetrieved(Lorg/chromium/chrome/browser/banners/AppData;)V
    .locals 4

    .prologue
    .line 141
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lorg/chromium/chrome/browser/banners/AppData;->siteUrl()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lorg/chromium/chrome/browser/banners/AppBannerManager;->isBannerForCurrentPage(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 146
    :cond_0
    :goto_0
    return-void

    .line 143
    :cond_1
    iput-object p1, p0, Lorg/chromium/chrome/browser/banners/AppBannerManager;->mAppData:Lorg/chromium/chrome/browser/banners/AppData;

    .line 144
    invoke-virtual {p1}, Lorg/chromium/chrome/browser/banners/AppData;->imageUrl()Ljava/lang/String;

    move-result-object v0

    .line 145
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    iget-wide v2, p0, Lorg/chromium/chrome/browser/banners/AppBannerManager;->mNativePointer:J

    invoke-direct {p0, v2, v3, v0}, Lorg/chromium/chrome/browser/banners/AppBannerManager;->nativeFetchIcon(JLjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    :cond_2
    invoke-direct {p0}, Lorg/chromium/chrome/browser/banners/AppBannerManager;->resetState()V

    goto :goto_0
.end method

.method public onBannerBlocked(Lorg/chromium/chrome/browser/banners/AppBannerView;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 188
    iget-object v0, p0, Lorg/chromium/chrome/browser/banners/AppBannerManager;->mBannerView:Lorg/chromium/chrome/browser/banners/AppBannerView;

    if-eq v0, p1, :cond_0

    .line 190
    :goto_0
    return-void

    .line 189
    :cond_0
    iget-wide v0, p0, Lorg/chromium/chrome/browser/banners/AppBannerManager;->mNativePointer:J

    invoke-direct {p0, v0, v1, p2, p3}, Lorg/chromium/chrome/browser/banners/AppBannerManager;->nativeBlockBanner(JLjava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onBannerDismissEvent(Lorg/chromium/chrome/browser/banners/AppBannerView;I)V
    .locals 1

    .prologue
    .line 194
    iget-object v0, p0, Lorg/chromium/chrome/browser/banners/AppBannerManager;->mBannerView:Lorg/chromium/chrome/browser/banners/AppBannerView;

    if-eq v0, p1, :cond_0

    .line 196
    :goto_0
    return-void

    .line 195
    :cond_0
    invoke-static {p2}, Lorg/chromium/chrome/browser/banners/AppBannerManager;->nativeRecordDismissEvent(I)V

    goto :goto_0
.end method

.method public onBannerInstallEvent(Lorg/chromium/chrome/browser/banners/AppBannerView;I)V
    .locals 1

    .prologue
    .line 200
    iget-object v0, p0, Lorg/chromium/chrome/browser/banners/AppBannerManager;->mBannerView:Lorg/chromium/chrome/browser/banners/AppBannerView;

    if-eq v0, p1, :cond_0

    .line 202
    :goto_0
    return-void

    .line 201
    :cond_0
    invoke-static {p2}, Lorg/chromium/chrome/browser/banners/AppBannerManager;->nativeRecordInstallEvent(I)V

    goto :goto_0
.end method

.method public onBannerRemoved(Lorg/chromium/chrome/browser/banners/AppBannerView;)V
    .locals 1

    .prologue
    .line 182
    iget-object v0, p0, Lorg/chromium/chrome/browser/banners/AppBannerManager;->mBannerView:Lorg/chromium/chrome/browser/banners/AppBannerView;

    if-eq v0, p1, :cond_0

    .line 184
    :goto_0
    return-void

    .line 183
    :cond_0
    invoke-direct {p0}, Lorg/chromium/chrome/browser/banners/AppBannerManager;->resetState()V

    goto :goto_0
.end method

.method public onFireIntent(Lorg/chromium/chrome/browser/banners/AppBannerView;Landroid/app/PendingIntent;)Z
    .locals 2

    .prologue
    .line 206
    iget-object v0, p0, Lorg/chromium/chrome/browser/banners/AppBannerManager;->mBannerView:Lorg/chromium/chrome/browser/banners/AppBannerView;

    if-eq v0, p1, :cond_0

    const/4 v0, 0x0

    .line 207
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lorg/chromium/chrome/browser/banners/AppBannerManager;->mTab:Lorg/chromium/chrome/browser/Tab;

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/Tab;->getWindowAndroid()Lorg/chromium/ui/base/WindowAndroid;

    move-result-object v0

    sget v1, Lorg/chromium/ui/R$string;->low_memory_error:I

    invoke-virtual {v0, p2, p1, v1}, Lorg/chromium/ui/base/WindowAndroid;->showIntent(Landroid/app/PendingIntent;Lorg/chromium/ui/base/WindowAndroid$IntentCallback;I)Z

    move-result v0

    goto :goto_0
.end method
