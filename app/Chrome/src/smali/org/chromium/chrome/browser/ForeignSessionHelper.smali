.class public Lorg/chromium/chrome/browser/ForeignSessionHelper;
.super Ljava/lang/Object;
.source "ForeignSessionHelper.java"


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private mNativeForeignSessionHelper:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 19
    const-class v0, Lorg/chromium/chrome/browser/ForeignSessionHelper;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/chromium/chrome/browser/ForeignSessionHelper;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lorg/chromium/chrome/browser/profiles/Profile;)V
    .locals 2

    .prologue
    .line 124
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 125
    invoke-static {p1}, Lorg/chromium/chrome/browser/ForeignSessionHelper;->nativeInit(Lorg/chromium/chrome/browser/profiles/Profile;)J

    move-result-wide v0

    iput-wide v0, p0, Lorg/chromium/chrome/browser/ForeignSessionHelper;->mNativeForeignSessionHelper:J

    .line 126
    return-void
.end method

.method private static native nativeDeleteForeignSession(JLjava/lang/String;)V
.end method

.method private static native nativeDestroy(J)V
.end method

.method private static native nativeGetForeignSessions(JLjava/util/List;)Z
.end method

.method private static native nativeInit(Lorg/chromium/chrome/browser/profiles/Profile;)J
.end method

.method private static native nativeIsTabSyncEnabled(J)Z
.end method

.method private static native nativeOpenForeignSessionTab(JLorg/chromium/chrome/browser/Tab;Ljava/lang/String;II)Z
.end method

.method private static native nativeSetOnForeignSessionCallback(JLorg/chromium/chrome/browser/ForeignSessionHelper$ForeignSessionCallback;)V
.end method

.method private static pushSession(Ljava/util/List;Ljava/lang/String;Ljava/lang/String;IJ)Lorg/chromium/chrome/browser/ForeignSessionHelper$ForeignSession;
    .locals 8

    .prologue
    .line 100
    new-instance v0, Lorg/chromium/chrome/browser/ForeignSessionHelper$ForeignSession;

    const/4 v6, 0x0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move-wide v4, p4

    invoke-direct/range {v0 .. v6}, Lorg/chromium/chrome/browser/ForeignSessionHelper$ForeignSession;-><init>(Ljava/lang/String;Ljava/lang/String;IJLorg/chromium/chrome/browser/ForeignSessionHelper$1;)V

    .line 101
    invoke-interface {p0, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 102
    return-object v0
.end method

.method private static pushTab(Lorg/chromium/chrome/browser/ForeignSessionHelper$ForeignSessionWindow;Ljava/lang/String;Ljava/lang/String;JI)V
    .locals 9

    .prologue
    .line 116
    new-instance v1, Lorg/chromium/chrome/browser/ForeignSessionHelper$ForeignSessionTab;

    const/4 v7, 0x0

    move-object v2, p1

    move-object v3, p2

    move-wide v4, p3

    move v6, p5

    invoke-direct/range {v1 .. v7}, Lorg/chromium/chrome/browser/ForeignSessionHelper$ForeignSessionTab;-><init>(Ljava/lang/String;Ljava/lang/String;JILorg/chromium/chrome/browser/ForeignSessionHelper$1;)V

    .line 117
    iget-object v0, p0, Lorg/chromium/chrome/browser/ForeignSessionHelper$ForeignSessionWindow;->tabs:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 118
    return-void
.end method

.method private static pushWindow(Lorg/chromium/chrome/browser/ForeignSessionHelper$ForeignSession;JI)Lorg/chromium/chrome/browser/ForeignSessionHelper$ForeignSessionWindow;
    .locals 3

    .prologue
    .line 108
    new-instance v0, Lorg/chromium/chrome/browser/ForeignSessionHelper$ForeignSessionWindow;

    const/4 v1, 0x0

    invoke-direct {v0, p1, p2, p3, v1}, Lorg/chromium/chrome/browser/ForeignSessionHelper$ForeignSessionWindow;-><init>(JILorg/chromium/chrome/browser/ForeignSessionHelper$1;)V

    .line 109
    iget-object v1, p0, Lorg/chromium/chrome/browser/ForeignSessionHelper$ForeignSession;->windows:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 110
    return-object v0
.end method


# virtual methods
.method public deleteForeignSession(Lorg/chromium/chrome/browser/ForeignSessionHelper$ForeignSession;)V
    .locals 3

    .prologue
    .line 203
    iget-wide v0, p0, Lorg/chromium/chrome/browser/ForeignSessionHelper;->mNativeForeignSessionHelper:J

    iget-object v2, p1, Lorg/chromium/chrome/browser/ForeignSessionHelper$ForeignSession;->tag:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lorg/chromium/chrome/browser/ForeignSessionHelper;->nativeDeleteForeignSession(JLjava/lang/String;)V

    .line 204
    return-void
.end method

.method public destroy()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 132
    sget-boolean v0, Lorg/chromium/chrome/browser/ForeignSessionHelper;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-wide v0, p0, Lorg/chromium/chrome/browser/ForeignSessionHelper;->mNativeForeignSessionHelper:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 133
    :cond_0
    iget-wide v0, p0, Lorg/chromium/chrome/browser/ForeignSessionHelper;->mNativeForeignSessionHelper:J

    invoke-static {v0, v1}, Lorg/chromium/chrome/browser/ForeignSessionHelper;->nativeDestroy(J)V

    .line 134
    iput-wide v2, p0, Lorg/chromium/chrome/browser/ForeignSessionHelper;->mNativeForeignSessionHelper:J

    .line 135
    return-void
.end method

.method protected finalize()V
    .locals 4

    .prologue
    .line 140
    sget-boolean v0, Lorg/chromium/chrome/browser/ForeignSessionHelper;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-wide v0, p0, Lorg/chromium/chrome/browser/ForeignSessionHelper;->mNativeForeignSessionHelper:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 141
    :cond_0
    return-void
.end method

.method public getForeignSessions()Ljava/util/List;
    .locals 4

    .prologue
    .line 163
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 164
    iget-wide v2, p0, Lorg/chromium/chrome/browser/ForeignSessionHelper;->mNativeForeignSessionHelper:J

    invoke-static {v2, v3, v0}, Lorg/chromium/chrome/browser/ForeignSessionHelper;->nativeGetForeignSessions(JLjava/util/List;)Z

    move-result v1

    .line 165
    if-eqz v1, :cond_0

    .line 167
    new-instance v1, Lorg/chromium/chrome/browser/ForeignSessionHelper$1;

    invoke-direct {v1, p0}, Lorg/chromium/chrome/browser/ForeignSessionHelper$1;-><init>(Lorg/chromium/chrome/browser/ForeignSessionHelper;)V

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 178
    :goto_0
    return-object v0

    .line 175
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public openForeignSessionTab(Lorg/chromium/chrome/browser/Tab;Lorg/chromium/chrome/browser/ForeignSessionHelper$ForeignSession;Lorg/chromium/chrome/browser/ForeignSessionHelper$ForeignSessionTab;I)Z
    .locals 6

    .prologue
    .line 191
    iget-wide v0, p0, Lorg/chromium/chrome/browser/ForeignSessionHelper;->mNativeForeignSessionHelper:J

    iget-object v3, p2, Lorg/chromium/chrome/browser/ForeignSessionHelper$ForeignSession;->tag:Ljava/lang/String;

    iget v4, p3, Lorg/chromium/chrome/browser/ForeignSessionHelper$ForeignSessionTab;->id:I

    move-object v2, p1

    move v5, p4

    invoke-static/range {v0 .. v5}, Lorg/chromium/chrome/browser/ForeignSessionHelper;->nativeOpenForeignSessionTab(JLorg/chromium/chrome/browser/Tab;Ljava/lang/String;II)Z

    move-result v0

    return v0
.end method

.method public setOnForeignSessionCallback(Lorg/chromium/chrome/browser/ForeignSessionHelper$ForeignSessionCallback;)V
    .locals 2

    .prologue
    .line 155
    iget-wide v0, p0, Lorg/chromium/chrome/browser/ForeignSessionHelper;->mNativeForeignSessionHelper:J

    invoke-static {v0, v1, p1}, Lorg/chromium/chrome/browser/ForeignSessionHelper;->nativeSetOnForeignSessionCallback(JLorg/chromium/chrome/browser/ForeignSessionHelper$ForeignSessionCallback;)V

    .line 156
    return-void
.end method
