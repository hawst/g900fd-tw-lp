.class public Lorg/chromium/chrome/browser/profiles/ProfileDownloader;
.super Ljava/lang/Object;
.source "ProfileDownloader.java"


# static fields
.field private static final sObservers:Lorg/chromium/base/ObserverList;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 19
    new-instance v0, Lorg/chromium/base/ObserverList;

    invoke-direct {v0}, Lorg/chromium/base/ObserverList;-><init>()V

    sput-object v0, Lorg/chromium/chrome/browser/profiles/ProfileDownloader;->sObservers:Lorg/chromium/base/ObserverList;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    return-void
.end method

.method public static addObserver(Lorg/chromium/chrome/browser/profiles/ProfileDownloader$Observer;)V
    .locals 1

    .prologue
    .line 39
    sget-object v0, Lorg/chromium/chrome/browser/profiles/ProfileDownloader;->sObservers:Lorg/chromium/base/ObserverList;

    invoke-virtual {v0, p0}, Lorg/chromium/base/ObserverList;->addObserver(Ljava/lang/Object;)Z

    .line 40
    return-void
.end method

.method public static getCachedAvatar(Lorg/chromium/chrome/browser/profiles/Profile;)Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 83
    invoke-static {p0}, Lorg/chromium/chrome/browser/profiles/ProfileDownloader;->nativeGetCachedAvatarForPrimaryAccount(Lorg/chromium/chrome/browser/profiles/Profile;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public static getCachedName(Lorg/chromium/chrome/browser/profiles/Profile;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 75
    invoke-static {p0}, Lorg/chromium/chrome/browser/profiles/ProfileDownloader;->nativeGetCachedNameForPrimaryAccount(Lorg/chromium/chrome/browser/profiles/Profile;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static native nativeGetCachedAvatarForPrimaryAccount(Lorg/chromium/chrome/browser/profiles/Profile;)Landroid/graphics/Bitmap;
.end method

.method private static native nativeGetCachedNameForPrimaryAccount(Lorg/chromium/chrome/browser/profiles/Profile;)Ljava/lang/String;
.end method

.method private static native nativeStartFetchingAccountInfoFor(Lorg/chromium/chrome/browser/profiles/Profile;Ljava/lang/String;I)V
.end method

.method private static onProfileDownloadSuccess(Ljava/lang/String;Ljava/lang/String;Landroid/graphics/Bitmap;)V
    .locals 2

    .prologue
    .line 64
    invoke-static {}, Lorg/chromium/base/ThreadUtils;->assertOnUiThread()V

    .line 65
    sget-object v0, Lorg/chromium/chrome/browser/profiles/ProfileDownloader;->sObservers:Lorg/chromium/base/ObserverList;

    invoke-virtual {v0}, Lorg/chromium/base/ObserverList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/chromium/chrome/browser/profiles/ProfileDownloader$Observer;

    .line 66
    invoke-interface {v0, p0, p1, p2}, Lorg/chromium/chrome/browser/profiles/ProfileDownloader$Observer;->onProfileDownloaded(Ljava/lang/String;Ljava/lang/String;Landroid/graphics/Bitmap;)V

    goto :goto_0

    .line 68
    :cond_0
    return-void
.end method

.method public static removeObserver(Lorg/chromium/chrome/browser/profiles/ProfileDownloader$Observer;)V
    .locals 1

    .prologue
    .line 47
    sget-object v0, Lorg/chromium/chrome/browser/profiles/ProfileDownloader;->sObservers:Lorg/chromium/base/ObserverList;

    invoke-virtual {v0, p0}, Lorg/chromium/base/ObserverList;->removeObserver(Ljava/lang/Object;)Z

    .line 48
    return-void
.end method

.method public static startFetchingAccountInfoFor(Lorg/chromium/chrome/browser/profiles/Profile;Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 58
    invoke-static {}, Lorg/chromium/base/ThreadUtils;->assertOnUiThread()V

    .line 59
    invoke-static {p0, p1, p2}, Lorg/chromium/chrome/browser/profiles/ProfileDownloader;->nativeStartFetchingAccountInfoFor(Lorg/chromium/chrome/browser/profiles/Profile;Ljava/lang/String;I)V

    .line 60
    return-void
.end method
