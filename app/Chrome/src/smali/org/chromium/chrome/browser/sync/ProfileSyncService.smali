.class public Lorg/chromium/chrome/browser/sync/ProfileSyncService;
.super Ljava/lang/Object;
.source "ProfileSyncService.java"


# static fields
.field static final synthetic $assertionsDisabled:Z

.field public static final SESSION_TAG_PREFIX:Ljava/lang/String; = "session_sync"

.field private static sSyncSetupManager:Lorg/chromium/chrome/browser/sync/ProfileSyncService;


# instance fields
.field protected final mContext:Landroid/content/Context;

.field private final mListeners:Ljava/util/List;

.field private final mNativeProfileSyncServiceAndroid:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 36
    const-class v0, Lorg/chromium/chrome/browser/sync/ProfileSyncService;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 79
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->mListeners:Ljava/util/List;

    .line 80
    invoke-static {}, Lorg/chromium/base/ThreadUtils;->assertOnUiThread()V

    .line 82
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->mContext:Landroid/content/Context;

    .line 87
    invoke-direct {p0}, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->nativeInit()J

    move-result-wide v0

    iput-wide v0, p0, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->mNativeProfileSyncServiceAndroid:J

    .line 88
    return-void
.end method

.method public static get(Landroid/content/Context;)Lorg/chromium/chrome/browser/sync/ProfileSyncService;
    .locals 1

    .prologue
    .line 69
    invoke-static {}, Lorg/chromium/base/ThreadUtils;->assertOnUiThread()V

    .line 70
    sget-object v0, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->sSyncSetupManager:Lorg/chromium/chrome/browser/sync/ProfileSyncService;

    if-nez v0, :cond_0

    .line 71
    new-instance v0, Lorg/chromium/chrome/browser/sync/ProfileSyncService;

    invoke-direct {v0, p0}, Lorg/chromium/chrome/browser/sync/ProfileSyncService;-><init>(Landroid/content/Context;)V

    sput-object v0, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->sSyncSetupManager:Lorg/chromium/chrome/browser/sync/ProfileSyncService;

    .line 73
    :cond_0
    sget-object v0, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->sSyncSetupManager:Lorg/chromium/chrome/browser/sync/ProfileSyncService;

    return-object v0
.end method

.method private static getProfileSyncServiceAndroid(Landroid/content/Context;)J
    .locals 2

    .prologue
    .line 92
    invoke-static {p0}, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->get(Landroid/content/Context;)Lorg/chromium/chrome/browser/sync/ProfileSyncService;

    move-result-object v0

    iget-wide v0, v0, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->mNativeProfileSyncServiceAndroid:J

    return-wide v0
.end method

.method public static modelTypeSelectionToSet(J)Ljava/util/Set;
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 374
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 375
    const-wide/16 v2, 0x1

    and-long/2addr v2, p0

    cmp-long v1, v2, v4

    if-eqz v1, :cond_0

    .line 376
    sget-object v1, Lorg/chromium/sync/internal_api/pub/base/ModelType;->AUTOFILL:Lorg/chromium/sync/internal_api/pub/base/ModelType;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 378
    :cond_0
    const-wide/16 v2, 0x20

    and-long/2addr v2, p0

    cmp-long v1, v2, v4

    if-eqz v1, :cond_1

    .line 379
    sget-object v1, Lorg/chromium/sync/internal_api/pub/base/ModelType;->AUTOFILL_PROFILE:Lorg/chromium/sync/internal_api/pub/base/ModelType;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 381
    :cond_1
    const-wide/16 v2, 0x2

    and-long/2addr v2, p0

    cmp-long v1, v2, v4

    if-eqz v1, :cond_2

    .line 382
    sget-object v1, Lorg/chromium/sync/internal_api/pub/base/ModelType;->BOOKMARK:Lorg/chromium/sync/internal_api/pub/base/ModelType;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 384
    :cond_2
    const-wide/16 v2, 0x1000

    and-long/2addr v2, p0

    cmp-long v1, v2, v4

    if-eqz v1, :cond_3

    .line 385
    sget-object v1, Lorg/chromium/sync/internal_api/pub/base/ModelType;->EXPERIMENTS:Lorg/chromium/sync/internal_api/pub/base/ModelType;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 387
    :cond_3
    const-wide/16 v2, 0x400

    and-long/2addr v2, p0

    cmp-long v1, v2, v4

    if-eqz v1, :cond_4

    .line 388
    sget-object v1, Lorg/chromium/sync/internal_api/pub/base/ModelType;->NIGORI:Lorg/chromium/sync/internal_api/pub/base/ModelType;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 390
    :cond_4
    const-wide/16 v2, 0x4

    and-long/2addr v2, p0

    cmp-long v1, v2, v4

    if-eqz v1, :cond_5

    .line 391
    sget-object v1, Lorg/chromium/sync/internal_api/pub/base/ModelType;->PASSWORD:Lorg/chromium/sync/internal_api/pub/base/ModelType;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 393
    :cond_5
    const-wide/16 v2, 0x8

    and-long/2addr v2, p0

    cmp-long v1, v2, v4

    if-eqz v1, :cond_6

    .line 394
    sget-object v1, Lorg/chromium/sync/internal_api/pub/base/ModelType;->SESSION:Lorg/chromium/sync/internal_api/pub/base/ModelType;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 396
    :cond_6
    const-wide/16 v2, 0x10

    and-long/2addr v2, p0

    cmp-long v1, v2, v4

    if-eqz v1, :cond_7

    .line 397
    sget-object v1, Lorg/chromium/sync/internal_api/pub/base/ModelType;->TYPED_URL:Lorg/chromium/sync/internal_api/pub/base/ModelType;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 399
    :cond_7
    const-wide/16 v2, 0x40

    and-long/2addr v2, p0

    cmp-long v1, v2, v4

    if-eqz v1, :cond_8

    .line 400
    sget-object v1, Lorg/chromium/sync/internal_api/pub/base/ModelType;->HISTORY_DELETE_DIRECTIVE:Lorg/chromium/sync/internal_api/pub/base/ModelType;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 402
    :cond_8
    const-wide/16 v2, 0x800

    and-long/2addr v2, p0

    cmp-long v1, v2, v4

    if-eqz v1, :cond_9

    .line 403
    sget-object v1, Lorg/chromium/sync/internal_api/pub/base/ModelType;->DEVICE_INFO:Lorg/chromium/sync/internal_api/pub/base/ModelType;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 405
    :cond_9
    const-wide/16 v2, 0x80

    and-long/2addr v2, p0

    cmp-long v1, v2, v4

    if-eqz v1, :cond_a

    .line 406
    sget-object v1, Lorg/chromium/sync/internal_api/pub/base/ModelType;->PROXY_TABS:Lorg/chromium/sync/internal_api/pub/base/ModelType;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 408
    :cond_a
    const-wide/16 v2, 0x100

    and-long/2addr v2, p0

    cmp-long v1, v2, v4

    if-eqz v1, :cond_b

    .line 409
    sget-object v1, Lorg/chromium/sync/internal_api/pub/base/ModelType;->FAVICON_IMAGE:Lorg/chromium/sync/internal_api/pub/base/ModelType;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 411
    :cond_b
    const-wide/16 v2, 0x200

    and-long/2addr v2, p0

    cmp-long v1, v2, v4

    if-eqz v1, :cond_c

    .line 412
    sget-object v1, Lorg/chromium/sync/internal_api/pub/base/ModelType;->FAVICON_TRACKING:Lorg/chromium/sync/internal_api/pub/base/ModelType;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 414
    :cond_c
    const-wide/16 v2, 0x2000

    and-long/2addr v2, p0

    cmp-long v1, v2, v4

    if-eqz v1, :cond_d

    .line 415
    sget-object v1, Lorg/chromium/sync/internal_api/pub/base/ModelType;->MANAGED_USER_SETTING:Lorg/chromium/sync/internal_api/pub/base/ModelType;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 417
    :cond_d
    return-object v0
.end method

.method private static modelTypeSelectionToStringForTest(J)Ljava/lang/String;
    .locals 4

    .prologue
    .line 548
    new-instance v1, Ljava/util/TreeSet;

    invoke-direct {v1}, Ljava/util/TreeSet;-><init>()V

    .line 549
    invoke-static {p0, p1}, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->modelTypeSelectionToSet(J)Ljava/util/Set;

    move-result-object v0

    invoke-static {v0}, Lorg/chromium/sync/internal_api/pub/base/ModelType;->filterOutNonInvalidationTypes(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    .line 551
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/chromium/sync/internal_api/pub/base/ModelType;

    .line 552
    invoke-virtual {v0}, Lorg/chromium/sync/internal_api/pub/base/ModelType;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/SortedSet;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 554
    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 555
    invoke-interface {v1}, Ljava/util/SortedSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 556
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 557
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 558
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 559
    const-string/jumbo v0, ", "

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 560
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 563
    :cond_1
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private native nativeDisableSync(J)V
.end method

.method private native nativeEnableEncryptEverything(J)V
.end method

.method private native nativeEnableSync(J)V
.end method

.method private native nativeGetAboutInfoForTest(J)Ljava/lang/String;
.end method

.method private native nativeGetAuthError(J)I
.end method

.method private native nativeGetCurrentSignedInAccountText(J)Ljava/lang/String;
.end method

.method private native nativeGetEnabledDataTypes(J)J
.end method

.method private native nativeGetExplicitPassphraseTime(J)J
.end method

.method private native nativeGetLastSyncedTimeForTest(J)J
.end method

.method private native nativeGetPassphraseType(J)I
.end method

.method private native nativeGetSyncEnterCustomPassphraseBodyText(J)Ljava/lang/String;
.end method

.method private native nativeGetSyncEnterCustomPassphraseBodyWithDateText(J)Ljava/lang/String;
.end method

.method private native nativeGetSyncEnterGooglePassphraseBodyWithDateText(J)Ljava/lang/String;
.end method

.method private native nativeHasExplicitPassphraseTime(J)Z
.end method

.method private native nativeHasKeepEverythingSynced(J)Z
.end method

.method private native nativeHasSyncSetupCompleted(J)Z
.end method

.method private native nativeHasUnrecoverableError(J)Z
.end method

.method private native nativeInit()J
.end method

.method private native nativeIsCryptographerReady(J)Z
.end method

.method private native nativeIsEncryptEverythingAllowed(J)Z
.end method

.method private native nativeIsEncryptEverythingEnabled(J)Z
.end method

.method private native nativeIsFirstSetupInProgress(J)Z
.end method

.method private native nativeIsPassphraseRequiredForDecryption(J)Z
.end method

.method private native nativeIsPassphraseRequiredForExternalType(J)Z
.end method

.method private native nativeIsStartSuppressed(J)Z
.end method

.method private native nativeIsSyncInitialized(J)Z
.end method

.method private native nativeIsSyncKeystoreMigrationDone(J)Z
.end method

.method private native nativeIsUsingSecondaryPassphrase(J)Z
.end method

.method private native nativeNudgeSyncer(JILjava/lang/String;JLjava/lang/String;)V
.end method

.method private native nativeNudgeSyncerForAllTypes(J)V
.end method

.method private native nativeOverrideNetworkResourcesForTest(JJ)V
.end method

.method private native nativeQuerySyncStatusSummary(J)Ljava/lang/String;
.end method

.method private native nativeSetDecryptionPassphrase(JLjava/lang/String;)Z
.end method

.method private native nativeSetEncryptionPassphrase(JLjava/lang/String;Z)V
.end method

.method private native nativeSetPreferredDataTypes(JZJ)V
.end method

.method private native nativeSetSetupInProgress(JZ)V
.end method

.method private native nativeSetSyncSessionsId(JLjava/lang/String;)Z
.end method

.method private native nativeSetSyncSetupCompleted(J)V
.end method

.method private native nativeSignInSync(J)V
.end method

.method private native nativeSignOutSync(J)V
.end method


# virtual methods
.method public addSyncStateChangedListener(Lorg/chromium/chrome/browser/sync/ProfileSyncService$SyncStateChangedListener;)V
    .locals 1

    .prologue
    .line 477
    invoke-static {}, Lorg/chromium/base/ThreadUtils;->assertOnUiThread()V

    .line 478
    iget-object v0, p0, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->mListeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 479
    return-void
.end method

.method public disableSync()V
    .locals 2

    .prologue
    .line 520
    iget-wide v0, p0, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->mNativeProfileSyncServiceAndroid:J

    invoke-direct {p0, v0, v1}, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->nativeDisableSync(J)V

    .line 521
    return-void
.end method

.method public enableEncryptEverything()V
    .locals 2

    .prologue
    .line 337
    sget-boolean v0, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->isSyncInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 338
    :cond_0
    iget-wide v0, p0, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->mNativeProfileSyncServiceAndroid:J

    invoke-direct {p0, v0, v1}, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->nativeEnableEncryptEverything(J)V

    .line 339
    return-void
.end method

.method public enableSync()V
    .locals 2

    .prologue
    .line 513
    iget-wide v0, p0, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->mNativeProfileSyncServiceAndroid:J

    invoke-direct {p0, v0, v1}, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->nativeEnableSync(J)V

    .line 514
    return-void
.end method

.method public finishSyncFirstSetupIfNeeded()V
    .locals 1

    .prologue
    .line 101
    invoke-virtual {p0}, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->isFirstSetupInProgress()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 102
    invoke-virtual {p0}, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->setSyncSetupCompleted()V

    .line 103
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->setSetupInProgress(Z)V

    .line 105
    :cond_0
    return-void
.end method

.method public getAuthError()Lorg/chromium/chrome/browser/sync/GoogleServiceAuthError$State;
    .locals 2

    .prologue
    .line 357
    iget-wide v0, p0, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->mNativeProfileSyncServiceAndroid:J

    invoke-direct {p0, v0, v1}, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->nativeGetAuthError(J)I

    move-result v0

    .line 358
    invoke-static {v0}, Lorg/chromium/chrome/browser/sync/GoogleServiceAuthError$State;->fromCode(I)Lorg/chromium/chrome/browser/sync/GoogleServiceAuthError$State;

    move-result-object v0

    return-object v0
.end method

.method public getCurrentSignedInAccountText()Ljava/lang/String;
    .locals 2

    .prologue
    .line 251
    sget-boolean v0, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->isSyncInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 252
    :cond_0
    iget-wide v0, p0, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->mNativeProfileSyncServiceAndroid:J

    invoke-direct {p0, v0, v1}, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->nativeGetCurrentSignedInAccountText(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getExplicitPassphraseTime()J
    .locals 2

    .prologue
    .line 236
    sget-boolean v0, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->isSyncInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 237
    :cond_0
    iget-wide v0, p0, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->mNativeProfileSyncServiceAndroid:J

    invoke-direct {p0, v0, v1}, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->nativeGetExplicitPassphraseTime(J)J

    move-result-wide v0

    return-wide v0
.end method

.method public getLastSyncedTimeForTest()J
    .locals 2

    .prologue
    .line 531
    iget-wide v0, p0, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->mNativeProfileSyncServiceAndroid:J

    invoke-direct {p0, v0, v1}, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->nativeGetLastSyncedTimeForTest(J)J

    move-result-wide v0

    return-wide v0
.end method

.method public getPreferredDataTypes()Ljava/util/Set;
    .locals 2

    .prologue
    .line 367
    iget-wide v0, p0, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->mNativeProfileSyncServiceAndroid:J

    invoke-direct {p0, v0, v1}, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->nativeGetEnabledDataTypes(J)J

    move-result-wide v0

    .line 369
    invoke-static {v0, v1}, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->modelTypeSelectionToSet(J)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public getSyncDecryptionPassphraseType()Lorg/chromium/sync/internal_api/pub/SyncDecryptionPassphraseType;
    .locals 2

    .prologue
    .line 214
    sget-boolean v0, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->isSyncInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 215
    :cond_0
    iget-wide v0, p0, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->mNativeProfileSyncServiceAndroid:J

    invoke-direct {p0, v0, v1}, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->nativeGetPassphraseType(J)I

    move-result v0

    .line 216
    invoke-static {v0}, Lorg/chromium/sync/internal_api/pub/SyncDecryptionPassphraseType;->fromInternalValue(I)Lorg/chromium/sync/internal_api/pub/SyncDecryptionPassphraseType;

    move-result-object v0

    return-object v0
.end method

.method public getSyncDecryptionPassphraseTypeIfRequired()Lorg/chromium/sync/internal_api/pub/SyncDecryptionPassphraseType;
    .locals 1

    .prologue
    .line 199
    invoke-virtual {p0}, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->isSyncInitialized()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->isPassphraseRequiredForDecryption()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 200
    invoke-virtual {p0}, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->getSyncDecryptionPassphraseType()Lorg/chromium/sync/internal_api/pub/SyncDecryptionPassphraseType;

    move-result-object v0

    .line 202
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lorg/chromium/sync/internal_api/pub/SyncDecryptionPassphraseType;->NONE:Lorg/chromium/sync/internal_api/pub/SyncDecryptionPassphraseType;

    goto :goto_0
.end method

.method public getSyncEnterCustomPassphraseBodyText()Ljava/lang/String;
    .locals 2

    .prologue
    .line 256
    iget-wide v0, p0, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->mNativeProfileSyncServiceAndroid:J

    invoke-direct {p0, v0, v1}, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->nativeGetSyncEnterCustomPassphraseBodyText(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getSyncEnterCustomPassphraseBodyWithDateText()Ljava/lang/String;
    .locals 2

    .prologue
    .line 246
    sget-boolean v0, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->isSyncInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 247
    :cond_0
    iget-wide v0, p0, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->mNativeProfileSyncServiceAndroid:J

    invoke-direct {p0, v0, v1}, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->nativeGetSyncEnterCustomPassphraseBodyWithDateText(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getSyncEnterGooglePassphraseBodyWithDateText()Ljava/lang/String;
    .locals 2

    .prologue
    .line 241
    sget-boolean v0, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->isSyncInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 242
    :cond_0
    iget-wide v0, p0, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->mNativeProfileSyncServiceAndroid:J

    invoke-direct {p0, v0, v1}, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->nativeGetSyncEnterGooglePassphraseBodyWithDateText(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getSyncInternalsInfoForTest()Ljava/lang/String;
    .locals 2

    .prologue
    .line 505
    invoke-static {}, Lorg/chromium/base/ThreadUtils;->assertOnUiThread()V

    .line 506
    iget-wide v0, p0, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->mNativeProfileSyncServiceAndroid:J

    invoke-direct {p0, v0, v1}, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->nativeGetAboutInfoForTest(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public hasExplicitPassphraseTime()Z
    .locals 2

    .prologue
    .line 228
    sget-boolean v0, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->isSyncInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 229
    :cond_0
    iget-wide v0, p0, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->mNativeProfileSyncServiceAndroid:J

    invoke-direct {p0, v0, v1}, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->nativeHasExplicitPassphraseTime(J)Z

    move-result v0

    return v0
.end method

.method public hasKeepEverythingSynced()Z
    .locals 2

    .prologue
    .line 421
    iget-wide v0, p0, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->mNativeProfileSyncServiceAndroid:J

    invoke-direct {p0, v0, v1}, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->nativeHasKeepEverythingSynced(J)Z

    move-result v0

    return v0
.end method

.method public hasSyncSetupCompleted()Z
    .locals 2

    .prologue
    .line 458
    iget-wide v0, p0, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->mNativeProfileSyncServiceAndroid:J

    invoke-direct {p0, v0, v1}, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->nativeHasSyncSetupCompleted(J)Z

    move-result v0

    return v0
.end method

.method public hasUnrecoverableError()Z
    .locals 2

    .prologue
    .line 487
    iget-wide v0, p0, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->mNativeProfileSyncServiceAndroid:J

    invoke-direct {p0, v0, v1}, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->nativeHasUnrecoverableError(J)Z

    move-result v0

    return v0
.end method

.method public isCryptographerReady()Z
    .locals 2

    .prologue
    .line 347
    sget-boolean v0, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->isSyncInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 348
    :cond_0
    iget-wide v0, p0, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->mNativeProfileSyncServiceAndroid:J

    invoke-direct {p0, v0, v1}, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->nativeIsCryptographerReady(J)Z

    move-result v0

    return v0
.end method

.method public isEncryptEverythingAllowed()Z
    .locals 2

    .prologue
    .line 318
    sget-boolean v0, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->isSyncInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 319
    :cond_0
    iget-wide v0, p0, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->mNativeProfileSyncServiceAndroid:J

    invoke-direct {p0, v0, v1}, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->nativeIsEncryptEverythingAllowed(J)Z

    move-result v0

    return v0
.end method

.method public isEncryptEverythingEnabled()Z
    .locals 2

    .prologue
    .line 328
    sget-boolean v0, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->isSyncInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 329
    :cond_0
    iget-wide v0, p0, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->mNativeProfileSyncServiceAndroid:J

    invoke-direct {p0, v0, v1}, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->nativeIsEncryptEverythingEnabled(J)Z

    move-result v0

    return v0
.end method

.method public isFirstSetupInProgress()Z
    .locals 2

    .prologue
    .line 308
    iget-wide v0, p0, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->mNativeProfileSyncServiceAndroid:J

    invoke-direct {p0, v0, v1}, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->nativeIsFirstSetupInProgress(J)Z

    move-result v0

    return v0
.end method

.method public isPassphraseRequiredForDecryption()Z
    .locals 2

    .prologue
    .line 277
    sget-boolean v0, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->isSyncInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 278
    :cond_0
    iget-wide v0, p0, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->mNativeProfileSyncServiceAndroid:J

    invoke-direct {p0, v0, v1}, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->nativeIsPassphraseRequiredForDecryption(J)Z

    move-result v0

    return v0
.end method

.method public isPassphraseRequiredForExternalType()Z
    .locals 2

    .prologue
    .line 289
    sget-boolean v0, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->isSyncInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 290
    :cond_0
    iget-wide v0, p0, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->mNativeProfileSyncServiceAndroid:J

    invoke-direct {p0, v0, v1}, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->nativeIsPassphraseRequiredForExternalType(J)Z

    move-result v0

    return v0
.end method

.method public isStartSuppressed()Z
    .locals 2

    .prologue
    .line 462
    iget-wide v0, p0, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->mNativeProfileSyncServiceAndroid:J

    invoke-direct {p0, v0, v1}, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->nativeIsStartSuppressed(J)Z

    move-result v0

    return v0
.end method

.method public isSyncInitialized()Z
    .locals 2

    .prologue
    .line 299
    iget-wide v0, p0, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->mNativeProfileSyncServiceAndroid:J

    invoke-direct {p0, v0, v1}, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->nativeIsSyncInitialized(J)Z

    move-result v0

    return v0
.end method

.method public isUsingSecondaryPassphrase()Z
    .locals 2

    .prologue
    .line 266
    sget-boolean v0, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->isSyncInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 267
    :cond_0
    iget-wide v0, p0, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->mNativeProfileSyncServiceAndroid:J

    invoke-direct {p0, v0, v1}, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->nativeIsUsingSecondaryPassphrase(J)Z

    move-result v0

    return v0
.end method

.method public removeSyncStateChangedListener(Lorg/chromium/chrome/browser/sync/ProfileSyncService$SyncStateChangedListener;)V
    .locals 1

    .prologue
    .line 482
    invoke-static {}, Lorg/chromium/base/ThreadUtils;->assertOnUiThread()V

    .line 483
    iget-object v0, p0, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->mListeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 484
    return-void
.end method

.method public requestSyncCycleForTest()V
    .locals 0

    .prologue
    .line 158
    invoke-static {}, Lorg/chromium/base/ThreadUtils;->assertOnUiThread()V

    .line 159
    invoke-virtual {p0}, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->requestSyncFromNativeChromeForAllTypes()V

    .line 160
    return-void
.end method

.method public requestSyncFromNativeChrome(ILjava/lang/String;JLjava/lang/String;)V
    .locals 9

    .prologue
    .line 143
    invoke-static {}, Lorg/chromium/base/ThreadUtils;->assertOnUiThread()V

    .line 144
    iget-wide v2, p0, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->mNativeProfileSyncServiceAndroid:J

    move-object v1, p0

    move v4, p1

    move-object v5, p2

    move-wide v6, p3

    move-object v8, p5

    invoke-direct/range {v1 .. v8}, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->nativeNudgeSyncer(JILjava/lang/String;JLjava/lang/String;)V

    .line 146
    return-void
.end method

.method public requestSyncFromNativeChromeForAllTypes()V
    .locals 2

    .prologue
    .line 149
    invoke-static {}, Lorg/chromium/base/ThreadUtils;->assertOnUiThread()V

    .line 150
    iget-wide v0, p0, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->mNativeProfileSyncServiceAndroid:J

    invoke-direct {p0, v0, v1}, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->nativeNudgeSyncerForAllTypes(J)V

    .line 151
    return-void
.end method

.method public setDecryptionPassphrase(Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 352
    sget-boolean v0, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->isSyncInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 353
    :cond_0
    iget-wide v0, p0, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->mNativeProfileSyncServiceAndroid:J

    invoke-direct {p0, v0, v1, p1}, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->nativeSetDecryptionPassphrase(JLjava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public setEncryptionPassphrase(Ljava/lang/String;Z)V
    .locals 2

    .prologue
    .line 342
    sget-boolean v0, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->isSyncInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 343
    :cond_0
    iget-wide v0, p0, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->mNativeProfileSyncServiceAndroid:J

    invoke-direct {p0, v0, v1, p1, p2}, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->nativeSetEncryptionPassphrase(JLjava/lang/String;Z)V

    .line 344
    return-void
.end method

.method public setPreferredDataTypes(ZLjava/util/Set;)V
    .locals 6

    .prologue
    .line 433
    const-wide/16 v4, 0x0

    .line 434
    if-nez p1, :cond_0

    sget-object v0, Lorg/chromium/sync/internal_api/pub/base/ModelType;->AUTOFILL:Lorg/chromium/sync/internal_api/pub/base/ModelType;

    invoke-interface {p2, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 435
    :cond_0
    const-wide/16 v4, 0x1

    .line 437
    :cond_1
    if-nez p1, :cond_2

    sget-object v0, Lorg/chromium/sync/internal_api/pub/base/ModelType;->BOOKMARK:Lorg/chromium/sync/internal_api/pub/base/ModelType;

    invoke-interface {p2, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 438
    :cond_2
    const-wide/16 v0, 0x2

    or-long/2addr v4, v0

    .line 440
    :cond_3
    if-nez p1, :cond_4

    sget-object v0, Lorg/chromium/sync/internal_api/pub/base/ModelType;->PASSWORD:Lorg/chromium/sync/internal_api/pub/base/ModelType;

    invoke-interface {p2, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 441
    :cond_4
    const-wide/16 v0, 0x4

    or-long/2addr v4, v0

    .line 443
    :cond_5
    if-nez p1, :cond_6

    sget-object v0, Lorg/chromium/sync/internal_api/pub/base/ModelType;->PROXY_TABS:Lorg/chromium/sync/internal_api/pub/base/ModelType;

    invoke-interface {p2, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 444
    :cond_6
    const-wide/16 v0, 0x80

    or-long/2addr v4, v0

    .line 446
    :cond_7
    if-nez p1, :cond_8

    sget-object v0, Lorg/chromium/sync/internal_api/pub/base/ModelType;->TYPED_URL:Lorg/chromium/sync/internal_api/pub/base/ModelType;

    invoke-interface {p2, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 447
    :cond_8
    const-wide/16 v0, 0x10

    or-long/2addr v4, v0

    .line 449
    :cond_9
    iget-wide v1, p0, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->mNativeProfileSyncServiceAndroid:J

    move-object v0, p0

    move v3, p1

    invoke-direct/range {v0 .. v5}, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->nativeSetPreferredDataTypes(JZJ)V

    .line 451
    return-void
.end method

.method public setSessionsId(Lorg/chromium/chrome/browser/identity/UniqueIdentificationGenerator;)V
    .locals 4

    .prologue
    .line 171
    invoke-static {}, Lorg/chromium/base/ThreadUtils;->assertOnUiThread()V

    .line 172
    const/4 v0, 0x0

    invoke-interface {p1, v0}, Lorg/chromium/chrome/browser/identity/UniqueIdentificationGenerator;->getUniqueId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 173
    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 174
    const-string/jumbo v0, "ProfileSyncService"

    const-string/jumbo v1, "Unable to get unique tag for sync. This may lead to unexpected tab sync behavior."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 183
    :cond_0
    :goto_0
    return-void

    .line 178
    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "session_sync"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 179
    iget-wide v2, p0, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->mNativeProfileSyncServiceAndroid:J

    invoke-direct {p0, v2, v3, v0}, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->nativeSetSyncSessionsId(JLjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 180
    const-string/jumbo v0, "ProfileSyncService"

    const-string/jumbo v1, "Unable to write session sync tag. This may lead to unexpected tab sync behavior."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public setSetupInProgress(Z)V
    .locals 2

    .prologue
    .line 473
    iget-wide v0, p0, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->mNativeProfileSyncServiceAndroid:J

    invoke-direct {p0, v0, v1, p1}, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->nativeSetSetupInProgress(JZ)V

    .line 474
    return-void
.end method

.method public setSyncSetupCompleted()V
    .locals 2

    .prologue
    .line 454
    iget-wide v0, p0, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->mNativeProfileSyncServiceAndroid:J

    invoke-direct {p0, v0, v1}, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->nativeSetSyncSetupCompleted(J)V

    .line 455
    return-void
.end method

.method public signOut()V
    .locals 2

    .prologue
    .line 108
    iget-wide v0, p0, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->mNativeProfileSyncServiceAndroid:J

    invoke-direct {p0, v0, v1}, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->nativeSignOutSync(J)V

    .line 109
    return-void
.end method

.method public syncSignIn()V
    .locals 2

    .prologue
    .line 115
    iget-wide v0, p0, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->mNativeProfileSyncServiceAndroid:J

    invoke-direct {p0, v0, v1}, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->nativeSignInSync(J)V

    .line 118
    invoke-virtual {p0}, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->syncStateChanged()V

    .line 119
    return-void
.end method

.method public syncSignIn(Ljava/lang/String;)V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 126
    invoke-virtual {p0}, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->syncSignIn()V

    .line 127
    return-void
.end method

.method public syncSignInWithAuthToken(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 138
    invoke-virtual {p0, p1}, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->syncSignIn(Ljava/lang/String;)V

    .line 139
    return-void
.end method

.method public syncStateChanged()V
    .locals 2

    .prologue
    .line 496
    iget-object v0, p0, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->mListeners:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 497
    iget-object v0, p0, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->mListeners:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/chromium/chrome/browser/sync/ProfileSyncService$SyncStateChangedListener;

    .line 498
    invoke-interface {v0}, Lorg/chromium/chrome/browser/sync/ProfileSyncService$SyncStateChangedListener;->syncStateChanged()V

    goto :goto_0

    .line 501
    :cond_0
    return-void
.end method
