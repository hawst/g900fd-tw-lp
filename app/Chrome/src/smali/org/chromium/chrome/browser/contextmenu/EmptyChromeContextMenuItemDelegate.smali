.class public Lorg/chromium/chrome/browser/contextmenu/EmptyChromeContextMenuItemDelegate;
.super Ljava/lang/Object;
.source "EmptyChromeContextMenuItemDelegate.java"

# interfaces
.implements Lorg/chromium/chrome/browser/contextmenu/ChromeContextMenuItemDelegate;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public canLoadOriginalImage()Z
    .locals 1

    .prologue
    .line 26
    const/4 v0, 0x0

    return v0
.end method

.method public getPageUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 64
    const-string/jumbo v0, ""

    return-object v0
.end method

.method public isIncognito()Z
    .locals 1

    .prologue
    .line 16
    const/4 v0, 0x0

    return v0
.end method

.method public isIncognitoSupported()Z
    .locals 1

    .prologue
    .line 21
    const/4 v0, 0x0

    return v0
.end method

.method public onOpenImageInNewTab(Ljava/lang/String;Lorg/chromium/content_public/common/Referrer;)V
    .locals 0

    .prologue
    .line 48
    return-void
.end method

.method public onOpenImageUrl(Ljava/lang/String;Lorg/chromium/content_public/common/Referrer;)V
    .locals 0

    .prologue
    .line 44
    return-void
.end method

.method public onOpenInNewIncognitoTab(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 40
    return-void
.end method

.method public onOpenInNewTab(Ljava/lang/String;Lorg/chromium/content_public/common/Referrer;)V
    .locals 0

    .prologue
    .line 36
    return-void
.end method

.method public onSaveImageToClipboard(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 56
    return-void
.end method

.method public onSaveToClipboard(Ljava/lang/String;Z)V
    .locals 0

    .prologue
    .line 52
    return-void
.end method

.method public onSearchByImageInNewTab()V
    .locals 0

    .prologue
    .line 60
    return-void
.end method

.method public startDownload(Ljava/lang/String;Z)Z
    .locals 1

    .prologue
    .line 31
    const/4 v0, 0x0

    return v0
.end method
