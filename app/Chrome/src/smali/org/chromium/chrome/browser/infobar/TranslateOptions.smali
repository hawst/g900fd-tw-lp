.class public Lorg/chromium/chrome/browser/infobar/TranslateOptions;
.super Ljava/lang/Object;
.source "TranslateOptions.java"


# instance fields
.field private final mAllLanguages:[Ljava/lang/String;

.field private final mOptions:[Z

.field private final mOriginalOptions:[Z

.field private final mOriginalSourceLanguageIndex:I

.field private final mOriginalTargetLanguageIndex:I

.field private mSourceLanguageIndex:I

.field private mTargetLanguageIndex:I

.field private final mTriggeredFromMenu:Z


# direct methods
.method public constructor <init>(II[Ljava/lang/String;ZZ)V
    .locals 9

    .prologue
    const/4 v4, 0x0

    .line 62
    const/4 v8, 0x0

    move-object v0, p0

    move v1, p1

    move v2, p2

    move-object v3, p3

    move v5, v4

    move v6, p4

    move v7, p5

    invoke-direct/range {v0 .. v8}, Lorg/chromium/chrome/browser/infobar/TranslateOptions;-><init>(II[Ljava/lang/String;ZZZZ[Z)V

    .line 64
    return-void
.end method

.method private constructor <init>(II[Ljava/lang/String;ZZZZ[Z)V
    .locals 2

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    iput-object p3, p0, Lorg/chromium/chrome/browser/infobar/TranslateOptions;->mAllLanguages:[Ljava/lang/String;

    .line 40
    iput p1, p0, Lorg/chromium/chrome/browser/infobar/TranslateOptions;->mSourceLanguageIndex:I

    .line 41
    iput p2, p0, Lorg/chromium/chrome/browser/infobar/TranslateOptions;->mTargetLanguageIndex:I

    .line 42
    iput-boolean p7, p0, Lorg/chromium/chrome/browser/infobar/TranslateOptions;->mTriggeredFromMenu:Z

    .line 44
    const/4 v0, 0x3

    new-array v0, v0, [Z

    iput-object v0, p0, Lorg/chromium/chrome/browser/infobar/TranslateOptions;->mOptions:[Z

    .line 45
    iget-object v0, p0, Lorg/chromium/chrome/browser/infobar/TranslateOptions;->mOptions:[Z

    const/4 v1, 0x0

    aput-boolean p4, v0, v1

    .line 46
    iget-object v0, p0, Lorg/chromium/chrome/browser/infobar/TranslateOptions;->mOptions:[Z

    const/4 v1, 0x1

    aput-boolean p5, v0, v1

    .line 47
    iget-object v0, p0, Lorg/chromium/chrome/browser/infobar/TranslateOptions;->mOptions:[Z

    const/4 v1, 0x2

    aput-boolean p6, v0, v1

    .line 50
    if-nez p8, :cond_0

    .line 51
    iget-object v0, p0, Lorg/chromium/chrome/browser/infobar/TranslateOptions;->mOptions:[Z

    invoke-virtual {v0}, [Z->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Z

    iput-object v0, p0, Lorg/chromium/chrome/browser/infobar/TranslateOptions;->mOriginalOptions:[Z

    .line 56
    :goto_0
    iget v0, p0, Lorg/chromium/chrome/browser/infobar/TranslateOptions;->mSourceLanguageIndex:I

    iput v0, p0, Lorg/chromium/chrome/browser/infobar/TranslateOptions;->mOriginalSourceLanguageIndex:I

    .line 57
    iget v0, p0, Lorg/chromium/chrome/browser/infobar/TranslateOptions;->mTargetLanguageIndex:I

    iput v0, p0, Lorg/chromium/chrome/browser/infobar/TranslateOptions;->mOriginalTargetLanguageIndex:I

    .line 58
    return-void

    .line 53
    :cond_0
    invoke-virtual {p8}, [Z->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Z

    iput-object v0, p0, Lorg/chromium/chrome/browser/infobar/TranslateOptions;->mOriginalOptions:[Z

    goto :goto_0
.end method

.method public constructor <init>(Lorg/chromium/chrome/browser/infobar/TranslateOptions;)V
    .locals 9

    .prologue
    .line 70
    iget v1, p1, Lorg/chromium/chrome/browser/infobar/TranslateOptions;->mSourceLanguageIndex:I

    iget v2, p1, Lorg/chromium/chrome/browser/infobar/TranslateOptions;->mTargetLanguageIndex:I

    iget-object v3, p1, Lorg/chromium/chrome/browser/infobar/TranslateOptions;->mAllLanguages:[Ljava/lang/String;

    iget-object v0, p1, Lorg/chromium/chrome/browser/infobar/TranslateOptions;->mOptions:[Z

    const/4 v4, 0x0

    aget-boolean v4, v0, v4

    iget-object v0, p1, Lorg/chromium/chrome/browser/infobar/TranslateOptions;->mOptions:[Z

    const/4 v5, 0x1

    aget-boolean v5, v0, v5

    iget-object v0, p1, Lorg/chromium/chrome/browser/infobar/TranslateOptions;->mOptions:[Z

    const/4 v6, 0x2

    aget-boolean v6, v0, v6

    iget-boolean v7, p1, Lorg/chromium/chrome/browser/infobar/TranslateOptions;->mTriggeredFromMenu:Z

    iget-object v8, p1, Lorg/chromium/chrome/browser/infobar/TranslateOptions;->mOriginalOptions:[Z

    move-object v0, p0

    invoke-direct/range {v0 .. v8}, Lorg/chromium/chrome/browser/infobar/TranslateOptions;-><init>(II[Ljava/lang/String;ZZZZ[Z)V

    .line 74
    return-void
.end method

.method private canSetLanguage(II)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 191
    if-ne p1, p2, :cond_1

    .line 193
    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-direct {p0, p1}, Lorg/chromium/chrome/browser/infobar/TranslateOptions;->checkLanguageBoundaries(I)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-direct {p0, p2}, Lorg/chromium/chrome/browser/infobar/TranslateOptions;->checkLanguageBoundaries(I)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method private static checkElementBoundaries(I)Z
    .locals 1

    .prologue
    .line 198
    if-ltz p0, :cond_0

    const/4 v0, 0x2

    if-gt p0, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private checkLanguageBoundaries(I)Z
    .locals 1

    .prologue
    .line 187
    if-ltz p1, :cond_0

    iget-object v0, p0, Lorg/chromium/chrome/browser/infobar/TranslateOptions;->mAllLanguages:[Ljava/lang/String;

    array-length v0, v0

    if-ge p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private toggleState(IZ)Z
    .locals 1

    .prologue
    .line 178
    invoke-static {p1}, Lorg/chromium/chrome/browser/infobar/TranslateOptions;->checkElementBoundaries(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 179
    const/4 v0, 0x0

    .line 182
    :goto_0
    return v0

    .line 181
    :cond_0
    iget-object v0, p0, Lorg/chromium/chrome/browser/infobar/TranslateOptions;->mOptions:[Z

    aput-boolean p2, v0, p1

    .line 182
    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public allLanguages()Ljava/util/List;
    .locals 1

    .prologue
    .line 110
    iget-object v0, p0, Lorg/chromium/chrome/browser/infobar/TranslateOptions;->mAllLanguages:[Ljava/lang/String;

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public alwaysTranslateLanguageState()Z
    .locals 2

    .prologue
    .line 118
    iget-object v0, p0, Lorg/chromium/chrome/browser/infobar/TranslateOptions;->mOptions:[Z

    const/4 v1, 0x2

    aget-boolean v0, v0, v1

    return v0
.end method

.method public neverTranslateDomainState()Z
    .locals 2

    .prologue
    .line 122
    iget-object v0, p0, Lorg/chromium/chrome/browser/infobar/TranslateOptions;->mOptions:[Z

    const/4 v1, 0x1

    aget-boolean v0, v0, v1

    return v0
.end method

.method public neverTranslateLanguageState()Z
    .locals 2

    .prologue
    .line 114
    iget-object v0, p0, Lorg/chromium/chrome/browser/infobar/TranslateOptions;->mOptions:[Z

    const/4 v1, 0x0

    aget-boolean v0, v0, v1

    return v0
.end method

.method public optionsChanged()Z
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 101
    iget v2, p0, Lorg/chromium/chrome/browser/infobar/TranslateOptions;->mSourceLanguageIndex:I

    iget v3, p0, Lorg/chromium/chrome/browser/infobar/TranslateOptions;->mOriginalSourceLanguageIndex:I

    if-ne v2, v3, :cond_0

    iget v2, p0, Lorg/chromium/chrome/browser/infobar/TranslateOptions;->mTargetLanguageIndex:I

    iget v3, p0, Lorg/chromium/chrome/browser/infobar/TranslateOptions;->mOriginalTargetLanguageIndex:I

    if-ne v2, v3, :cond_0

    iget-object v2, p0, Lorg/chromium/chrome/browser/infobar/TranslateOptions;->mOptions:[Z

    aget-boolean v2, v2, v0

    iget-object v3, p0, Lorg/chromium/chrome/browser/infobar/TranslateOptions;->mOriginalOptions:[Z

    aget-boolean v3, v3, v0

    if-ne v2, v3, :cond_0

    iget-object v2, p0, Lorg/chromium/chrome/browser/infobar/TranslateOptions;->mOptions:[Z

    aget-boolean v2, v2, v1

    iget-object v3, p0, Lorg/chromium/chrome/browser/infobar/TranslateOptions;->mOriginalOptions:[Z

    aget-boolean v3, v3, v1

    if-ne v2, v3, :cond_0

    iget-object v2, p0, Lorg/chromium/chrome/browser/infobar/TranslateOptions;->mOptions:[Z

    aget-boolean v2, v2, v4

    iget-object v3, p0, Lorg/chromium/chrome/browser/infobar/TranslateOptions;->mOriginalOptions:[Z

    aget-boolean v3, v3, v4

    if-eq v2, v3, :cond_1

    :cond_0
    move v0, v1

    :cond_1
    return v0
.end method

.method public setSourceLanguage(I)Z
    .locals 1

    .prologue
    .line 126
    iget v0, p0, Lorg/chromium/chrome/browser/infobar/TranslateOptions;->mTargetLanguageIndex:I

    invoke-direct {p0, p1, v0}, Lorg/chromium/chrome/browser/infobar/TranslateOptions;->canSetLanguage(II)Z

    move-result v0

    .line 127
    if-eqz v0, :cond_0

    .line 128
    iput p1, p0, Lorg/chromium/chrome/browser/infobar/TranslateOptions;->mSourceLanguageIndex:I

    .line 130
    :cond_0
    return v0
.end method

.method public setTargetLanguage(I)Z
    .locals 1

    .prologue
    .line 134
    iget v0, p0, Lorg/chromium/chrome/browser/infobar/TranslateOptions;->mSourceLanguageIndex:I

    invoke-direct {p0, v0, p1}, Lorg/chromium/chrome/browser/infobar/TranslateOptions;->canSetLanguage(II)Z

    move-result v0

    .line 135
    if-eqz v0, :cond_0

    .line 136
    iput p1, p0, Lorg/chromium/chrome/browser/infobar/TranslateOptions;->mTargetLanguageIndex:I

    .line 138
    :cond_0
    return v0
.end method

.method public sourceLanguage()Ljava/lang/String;
    .locals 2

    .prologue
    .line 77
    iget v0, p0, Lorg/chromium/chrome/browser/infobar/TranslateOptions;->mSourceLanguageIndex:I

    invoke-direct {p0, v0}, Lorg/chromium/chrome/browser/infobar/TranslateOptions;->checkLanguageBoundaries(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 78
    iget-object v0, p0, Lorg/chromium/chrome/browser/infobar/TranslateOptions;->mAllLanguages:[Ljava/lang/String;

    iget v1, p0, Lorg/chromium/chrome/browser/infobar/TranslateOptions;->mSourceLanguageIndex:I

    aget-object v0, v0, v1

    .line 79
    :goto_0
    return-object v0

    :cond_0
    const-string/jumbo v0, ""

    goto :goto_0
.end method

.method public sourceLanguageIndex()I
    .locals 1

    .prologue
    .line 89
    iget v0, p0, Lorg/chromium/chrome/browser/infobar/TranslateOptions;->mSourceLanguageIndex:I

    invoke-direct {p0, v0}, Lorg/chromium/chrome/browser/infobar/TranslateOptions;->checkLanguageBoundaries(I)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lorg/chromium/chrome/browser/infobar/TranslateOptions;->mSourceLanguageIndex:I

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public targetLanguage()Ljava/lang/String;
    .locals 2

    .prologue
    .line 83
    iget v0, p0, Lorg/chromium/chrome/browser/infobar/TranslateOptions;->mTargetLanguageIndex:I

    invoke-direct {p0, v0}, Lorg/chromium/chrome/browser/infobar/TranslateOptions;->checkLanguageBoundaries(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 84
    iget-object v0, p0, Lorg/chromium/chrome/browser/infobar/TranslateOptions;->mAllLanguages:[Ljava/lang/String;

    iget v1, p0, Lorg/chromium/chrome/browser/infobar/TranslateOptions;->mTargetLanguageIndex:I

    aget-object v0, v0, v1

    .line 85
    :goto_0
    return-object v0

    :cond_0
    const-string/jumbo v0, ""

    goto :goto_0
.end method

.method public targetLanguageIndex()I
    .locals 1

    .prologue
    .line 93
    iget v0, p0, Lorg/chromium/chrome/browser/infobar/TranslateOptions;->mTargetLanguageIndex:I

    invoke-direct {p0, v0}, Lorg/chromium/chrome/browser/infobar/TranslateOptions;->checkLanguageBoundaries(I)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lorg/chromium/chrome/browser/infobar/TranslateOptions;->mTargetLanguageIndex:I

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 203
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lorg/chromium/chrome/browser/infobar/TranslateOptions;->sourceLanguage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " -> "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lorg/chromium/chrome/browser/infobar/TranslateOptions;->targetLanguage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " - Never Language:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lorg/chromium/chrome/browser/infobar/TranslateOptions;->mOptions:[Z

    const/4 v2, 0x0

    aget-boolean v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " Always Language:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lorg/chromium/chrome/browser/infobar/TranslateOptions;->mOptions:[Z

    const/4 v2, 0x2

    aget-boolean v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " Never Domain:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lorg/chromium/chrome/browser/infobar/TranslateOptions;->mOptions:[Z

    const/4 v2, 0x1

    aget-boolean v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public toggleAlwaysTranslateLanguageState(Z)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 171
    iget-object v1, p0, Lorg/chromium/chrome/browser/infobar/TranslateOptions;->mOptions:[Z

    aget-boolean v1, v1, v0

    if-eqz v1, :cond_0

    if-eqz p1, :cond_0

    .line 174
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x2

    invoke-direct {p0, v0, p1}, Lorg/chromium/chrome/browser/infobar/TranslateOptions;->toggleState(IZ)Z

    move-result v0

    goto :goto_0
.end method

.method public toggleNeverTranslateDomainState(Z)Z
    .locals 1

    .prologue
    .line 147
    const/4 v0, 0x1

    invoke-direct {p0, v0, p1}, Lorg/chromium/chrome/browser/infobar/TranslateOptions;->toggleState(IZ)Z

    move-result v0

    return v0
.end method

.method public toggleNeverTranslateLanguageState(Z)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 158
    iget-object v1, p0, Lorg/chromium/chrome/browser/infobar/TranslateOptions;->mOptions:[Z

    const/4 v2, 0x2

    aget-boolean v1, v1, v2

    if-eqz v1, :cond_0

    if-eqz p1, :cond_0

    .line 161
    :goto_0
    return v0

    :cond_0
    invoke-direct {p0, v0, p1}, Lorg/chromium/chrome/browser/infobar/TranslateOptions;->toggleState(IZ)Z

    move-result v0

    goto :goto_0
.end method

.method public triggeredFromMenu()Z
    .locals 1

    .prologue
    .line 97
    iget-boolean v0, p0, Lorg/chromium/chrome/browser/infobar/TranslateOptions;->mTriggeredFromMenu:Z

    return v0
.end method
