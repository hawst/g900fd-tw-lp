.class abstract Lorg/chromium/chrome/browser/SSLClientCertificateRequest$CertAsyncTask;
.super Landroid/os/AsyncTask;
.source "SSLClientCertificateRequest.java"


# instance fields
.field private mAndroidPrivateKey:Lorg/chromium/net/AndroidPrivateKey;

.field private mEncodedChain:[[B

.field private final mNativePtr:J


# direct methods
.method constructor <init>(J)V
    .locals 1

    .prologue
    .line 59
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 60
    iput-wide p1, p0, Lorg/chromium/chrome/browser/SSLClientCertificateRequest$CertAsyncTask;->mNativePtr:J

    .line 61
    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 50
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lorg/chromium/chrome/browser/SSLClientCertificateRequest$CertAsyncTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 70
    invoke-virtual {p0}, Lorg/chromium/chrome/browser/SSLClientCertificateRequest$CertAsyncTask;->getAlias()Ljava/lang/String;

    move-result-object v0

    .line 71
    if-nez v0, :cond_0

    .line 94
    :goto_0
    return-object v5

    .line 73
    :cond_0
    invoke-virtual {p0, v0}, Lorg/chromium/chrome/browser/SSLClientCertificateRequest$CertAsyncTask;->getPrivateKey(Ljava/lang/String;)Lorg/chromium/net/AndroidPrivateKey;

    move-result-object v1

    .line 74
    invoke-virtual {p0, v0}, Lorg/chromium/chrome/browser/SSLClientCertificateRequest$CertAsyncTask;->getCertificateChain(Ljava/lang/String;)[Ljava/security/cert/X509Certificate;

    move-result-object v2

    .line 76
    if-eqz v1, :cond_1

    if-eqz v2, :cond_1

    array-length v0, v2

    if-nez v0, :cond_2

    .line 77
    :cond_1
    const-string/jumbo v0, "SSLClientCertificateRequest"

    const-string/jumbo v1, "Empty client certificate chain?"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 82
    :cond_2
    array-length v0, v2

    new-array v3, v0, [[B

    .line 84
    const/4 v0, 0x0

    :goto_1
    :try_start_0
    array-length v4, v2

    if-ge v0, v4, :cond_3

    .line 85
    aget-object v4, v2, v0

    invoke-virtual {v4}, Ljava/security/cert/X509Certificate;->getEncoded()[B

    move-result-object v4

    aput-object v4, v3, v0
    :try_end_0
    .catch Ljava/security/cert/CertificateEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    .line 84
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 87
    :catch_0
    move-exception v0

    .line 88
    const-string/jumbo v1, "SSLClientCertificateRequest"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "Could not retrieve encoded certificate chain: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 92
    :cond_3
    iput-object v3, p0, Lorg/chromium/chrome/browser/SSLClientCertificateRequest$CertAsyncTask;->mEncodedChain:[[B

    .line 93
    iput-object v1, p0, Lorg/chromium/chrome/browser/SSLClientCertificateRequest$CertAsyncTask;->mAndroidPrivateKey:Lorg/chromium/net/AndroidPrivateKey;

    goto :goto_0
.end method

.method abstract getAlias()Ljava/lang/String;
.end method

.method abstract getCertificateChain(Ljava/lang/String;)[Ljava/security/cert/X509Certificate;
.end method

.method abstract getPrivateKey(Ljava/lang/String;)Lorg/chromium/net/AndroidPrivateKey;
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 50
    check-cast p1, Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lorg/chromium/chrome/browser/SSLClientCertificateRequest$CertAsyncTask;->onPostExecute(Ljava/lang/Void;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/Void;)V
    .locals 4

    .prologue
    .line 99
    invoke-static {}, Lorg/chromium/base/ThreadUtils;->assertOnUiThread()V

    .line 100
    iget-wide v0, p0, Lorg/chromium/chrome/browser/SSLClientCertificateRequest$CertAsyncTask;->mNativePtr:J

    iget-object v2, p0, Lorg/chromium/chrome/browser/SSLClientCertificateRequest$CertAsyncTask;->mEncodedChain:[[B

    iget-object v3, p0, Lorg/chromium/chrome/browser/SSLClientCertificateRequest$CertAsyncTask;->mAndroidPrivateKey:Lorg/chromium/net/AndroidPrivateKey;

    # invokes: Lorg/chromium/chrome/browser/SSLClientCertificateRequest;->nativeOnSystemRequestCompletion(J[[BLorg/chromium/net/AndroidPrivateKey;)V
    invoke-static {v0, v1, v2, v3}, Lorg/chromium/chrome/browser/SSLClientCertificateRequest;->access$000(J[[BLorg/chromium/net/AndroidPrivateKey;)V

    .line 101
    return-void
.end method
