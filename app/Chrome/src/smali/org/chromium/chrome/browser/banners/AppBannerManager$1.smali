.class Lorg/chromium/chrome/browser/banners/AppBannerManager$1;
.super Lorg/chromium/chrome/browser/EmptyTabObserver;
.source "AppBannerManager.java"


# instance fields
.field final synthetic this$0:Lorg/chromium/chrome/browser/banners/AppBannerManager;


# direct methods
.method constructor <init>(Lorg/chromium/chrome/browser/banners/AppBannerManager;)V
    .locals 0

    .prologue
    .line 88
    iput-object p1, p0, Lorg/chromium/chrome/browser/banners/AppBannerManager$1;->this$0:Lorg/chromium/chrome/browser/banners/AppBannerManager;

    invoke-direct {p0}, Lorg/chromium/chrome/browser/EmptyTabObserver;-><init>()V

    return-void
.end method


# virtual methods
.method public onContentChanged(Lorg/chromium/chrome/browser/Tab;)V
    .locals 1

    .prologue
    .line 97
    iget-object v0, p0, Lorg/chromium/chrome/browser/banners/AppBannerManager$1;->this$0:Lorg/chromium/chrome/browser/banners/AppBannerManager;

    # invokes: Lorg/chromium/chrome/browser/banners/AppBannerManager;->updatePointers()V
    invoke-static {v0}, Lorg/chromium/chrome/browser/banners/AppBannerManager;->access$000(Lorg/chromium/chrome/browser/banners/AppBannerManager;)V

    .line 98
    return-void
.end method

.method public onDestroyed(Lorg/chromium/chrome/browser/Tab;)V
    .locals 4

    .prologue
    .line 102
    iget-object v0, p0, Lorg/chromium/chrome/browser/banners/AppBannerManager$1;->this$0:Lorg/chromium/chrome/browser/banners/AppBannerManager;

    iget-object v1, p0, Lorg/chromium/chrome/browser/banners/AppBannerManager$1;->this$0:Lorg/chromium/chrome/browser/banners/AppBannerManager;

    # getter for: Lorg/chromium/chrome/browser/banners/AppBannerManager;->mNativePointer:J
    invoke-static {v1}, Lorg/chromium/chrome/browser/banners/AppBannerManager;->access$100(Lorg/chromium/chrome/browser/banners/AppBannerManager;)J

    move-result-wide v2

    # invokes: Lorg/chromium/chrome/browser/banners/AppBannerManager;->nativeDestroy(J)V
    invoke-static {v0, v2, v3}, Lorg/chromium/chrome/browser/banners/AppBannerManager;->access$200(Lorg/chromium/chrome/browser/banners/AppBannerManager;J)V

    .line 103
    iget-object v0, p0, Lorg/chromium/chrome/browser/banners/AppBannerManager$1;->this$0:Lorg/chromium/chrome/browser/banners/AppBannerManager;

    const/4 v1, 0x0

    # setter for: Lorg/chromium/chrome/browser/banners/AppBannerManager;->mContentViewCore:Lorg/chromium/content/browser/ContentViewCore;
    invoke-static {v0, v1}, Lorg/chromium/chrome/browser/banners/AppBannerManager;->access$302(Lorg/chromium/chrome/browser/banners/AppBannerManager;Lorg/chromium/content/browser/ContentViewCore;)Lorg/chromium/content/browser/ContentViewCore;

    .line 104
    iget-object v0, p0, Lorg/chromium/chrome/browser/banners/AppBannerManager$1;->this$0:Lorg/chromium/chrome/browser/banners/AppBannerManager;

    # invokes: Lorg/chromium/chrome/browser/banners/AppBannerManager;->resetState()V
    invoke-static {v0}, Lorg/chromium/chrome/browser/banners/AppBannerManager;->access$400(Lorg/chromium/chrome/browser/banners/AppBannerManager;)V

    .line 105
    return-void
.end method

.method public onWebContentsSwapped(Lorg/chromium/chrome/browser/Tab;ZZ)V
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lorg/chromium/chrome/browser/banners/AppBannerManager$1;->this$0:Lorg/chromium/chrome/browser/banners/AppBannerManager;

    # invokes: Lorg/chromium/chrome/browser/banners/AppBannerManager;->updatePointers()V
    invoke-static {v0}, Lorg/chromium/chrome/browser/banners/AppBannerManager;->access$000(Lorg/chromium/chrome/browser/banners/AppBannerManager;)V

    .line 93
    return-void
.end method
