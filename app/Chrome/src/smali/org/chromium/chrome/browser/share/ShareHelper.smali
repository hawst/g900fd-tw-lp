.class public Lorg/chromium/chrome/browser/share/ShareHelper;
.super Ljava/lang/Object;
.source "ShareHelper.java"


# static fields
.field static final synthetic $assertionsDisabled:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 34
    const-class v0, Lorg/chromium/chrome/browser/share/ShareHelper;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/chromium/chrome/browser/share/ShareHelper;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$000(Landroid/content/Context;Landroid/content/ComponentName;)V
    .locals 0

    .prologue
    .line 34
    invoke-static {p0, p1}, Lorg/chromium/chrome/browser/share/ShareHelper;->setLastShareComponentName(Landroid/content/Context;Landroid/content/ComponentName;)V

    return-void
.end method

.method static synthetic access$100(Ljava/lang/String;Ljava/lang/String;Landroid/graphics/Bitmap;Landroid/content/ComponentName;I)Landroid/content/Intent;
    .locals 1

    .prologue
    .line 34
    invoke-static {p0, p1, p2, p3, p4}, Lorg/chromium/chrome/browser/share/ShareHelper;->getDirectShareIntentForComponent(Ljava/lang/String;Ljava/lang/String;Landroid/graphics/Bitmap;Landroid/content/ComponentName;I)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public static configureDirectShareMenuItem(Landroid/app/Activity;Landroid/view/MenuItem;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    const/4 v5, 0x0

    .line 135
    .line 138
    invoke-static {p0}, Lorg/chromium/chrome/browser/share/ShareHelper;->getLastShareComponentName(Landroid/content/Context;)Landroid/content/ComponentName;

    move-result-object v2

    .line 139
    if-eqz v2, :cond_1

    .line 141
    :try_start_0
    invoke-virtual {p0}, Landroid/app/Activity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/content/pm/PackageManager;->getActivityIcon(Landroid/content/ComponentName;)Landroid/graphics/drawable/Drawable;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 142
    :try_start_1
    invoke-virtual {p0}, Landroid/app/Activity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    invoke-virtual {v2}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v2

    const/4 v4, 0x0

    invoke-virtual {v3, v2, v4}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v2

    .line 144
    invoke-virtual {p0}, Landroid/app/Activity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/content/pm/PackageManager;->getApplicationLabel(Landroid/content/pm/ApplicationInfo;)Ljava/lang/CharSequence;
    :try_end_1
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v0

    .line 150
    :goto_0
    invoke-interface {p1, v1}, Landroid/view/MenuItem;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;

    .line 151
    if-eqz v0, :cond_0

    .line 152
    sget v1, Lorg/chromium/chrome/R$string;->accessibility_menu_share_via:I

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    aput-object v0, v2, v5

    invoke-virtual {p0, v1, v2}, Landroid/app/Activity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Landroid/view/MenuItem;->setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 155
    :cond_0
    return-void

    :catch_0
    move-exception v1

    move-object v1, v0

    goto :goto_0

    :catch_1
    move-exception v2

    goto :goto_0

    :cond_1
    move-object v1, v0

    goto :goto_0
.end method

.method private static getDirectShareIntentForComponent(Ljava/lang/String;Ljava/lang/String;Landroid/graphics/Bitmap;Landroid/content/ComponentName;I)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 170
    invoke-static {p0, p1, p2, p4}, Lorg/chromium/chrome/browser/share/ShareHelper;->getShareIntent(Ljava/lang/String;Ljava/lang/String;Landroid/graphics/Bitmap;I)Landroid/content/Intent;

    move-result-object v0

    .line 171
    const/high16 v1, 0x3000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 173
    invoke-virtual {v0, p3}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 174
    return-object v0
.end method

.method private static getLastShareComponentName(Landroid/content/Context;)Landroid/content/ComponentName;
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 178
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 179
    const-string/jumbo v2, "last_shared_package_name"

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 180
    const-string/jumbo v3, "last_shared_class_name"

    invoke-interface {v1, v3, v0}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 181
    if-eqz v2, :cond_0

    if-nez v1, :cond_1

    .line 182
    :cond_0
    :goto_0
    return-object v0

    :cond_1
    new-instance v0, Landroid/content/ComponentName;

    invoke-direct {v0, v2, v1}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private static getShareIntent(Ljava/lang/String;Ljava/lang/String;Landroid/graphics/Bitmap;I)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 159
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "android.intent.action.SEND"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 160
    invoke-virtual {v0, p3}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 161
    const-string/jumbo v1, "text/plain"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 162
    const-string/jumbo v1, "android.intent.extra.SUBJECT"

    invoke-virtual {v0, v1, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 163
    const-string/jumbo v1, "android.intent.extra.TEXT"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 164
    if-eqz p2, :cond_0

    const-string/jumbo v1, "share_screenshot"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 165
    :cond_0
    return-object v0
.end method

.method private static setLastShareComponentName(Landroid/content/Context;Landroid/content/ComponentName;)V
    .locals 3

    .prologue
    .line 186
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 187
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 188
    const-string/jumbo v1, "last_shared_package_name"

    invoke-virtual {p1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 189
    const-string/jumbo v1, "last_shared_class_name"

    invoke-virtual {p1}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 190
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 191
    return-void
.end method

.method public static share(ZLandroid/app/Activity;Ljava/lang/String;Ljava/lang/String;Landroid/graphics/Bitmap;I)V
    .locals 0

    .prologue
    .line 61
    if-eqz p0, :cond_0

    .line 62
    invoke-static {p1, p2, p3, p4, p5}, Lorg/chromium/chrome/browser/share/ShareHelper;->shareWithLastUsed(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;Landroid/graphics/Bitmap;I)V

    .line 66
    :goto_0
    return-void

    .line 64
    :cond_0
    invoke-static {p1, p2, p3, p4, p5}, Lorg/chromium/chrome/browser/share/ShareHelper;->showShareDialog(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;Landroid/graphics/Bitmap;I)V

    goto :goto_0
.end method

.method private static shareWithLastUsed(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;Landroid/graphics/Bitmap;I)V
    .locals 1

    .prologue
    .line 121
    invoke-static {p0}, Lorg/chromium/chrome/browser/share/ShareHelper;->getLastShareComponentName(Landroid/content/Context;)Landroid/content/ComponentName;

    move-result-object v0

    .line 122
    if-nez v0, :cond_0

    .line 126
    :goto_0
    return-void

    .line 123
    :cond_0
    invoke-static {p1, p2, p3, v0, p4}, Lorg/chromium/chrome/browser/share/ShareHelper;->getDirectShareIntentForComponent(Ljava/lang/String;Ljava/lang/String;Landroid/graphics/Bitmap;Landroid/content/ComponentName;I)Landroid/content/Intent;

    move-result-object v0

    .line 125
    invoke-virtual {p0, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method private static showShareDialog(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;Landroid/graphics/Bitmap;I)V
    .locals 9

    .prologue
    .line 79
    invoke-static {p1, p2, p3, p4}, Lorg/chromium/chrome/browser/share/ShareHelper;->getShareIntent(Ljava/lang/String;Ljava/lang/String;Landroid/graphics/Bitmap;I)Landroid/content/Intent;

    move-result-object v0

    .line 80
    invoke-virtual {p0}, Landroid/app/Activity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    .line 81
    const/4 v1, 0x0

    invoke-virtual {v2, v0, v1}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v0

    .line 82
    sget-boolean v1, Lorg/chromium/chrome/browser/share/ShareHelper;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-gtz v1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 83
    :cond_0
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-nez v1, :cond_1

    .line 108
    :goto_0
    return-void

    .line 84
    :cond_1
    new-instance v1, Landroid/content/pm/ResolveInfo$DisplayNameComparator;

    invoke-direct {v1, v2}, Landroid/content/pm/ResolveInfo$DisplayNameComparator;-><init>(Landroid/content/pm/PackageManager;)V

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 86
    new-instance v1, Lorg/chromium/chrome/browser/share/ShareDialogAdapter;

    invoke-direct {v1, p0, v2, v0}, Lorg/chromium/chrome/browser/share/ShareDialogAdapter;-><init>(Landroid/content/Context;Landroid/content/pm/PackageManager;Ljava/util/List;)V

    .line 88
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 89
    sget v2, Lorg/chromium/chrome/R$string;->share_link_chooser_title:I

    invoke-virtual {p0, v2}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 90
    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setAdapter(Landroid/widget/ListAdapter;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 92
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v7

    .line 93
    invoke-virtual {v7}, Landroid/app/AlertDialog;->show()V

    .line 94
    invoke-virtual {v7}, Landroid/app/AlertDialog;->getListView()Landroid/widget/ListView;

    move-result-object v8

    new-instance v0, Lorg/chromium/chrome/browser/share/ShareHelper$1;

    move-object v2, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    move v6, p4

    invoke-direct/range {v0 .. v7}, Lorg/chromium/chrome/browser/share/ShareHelper$1;-><init>(Lorg/chromium/chrome/browser/share/ShareDialogAdapter;Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;Landroid/graphics/Bitmap;ILandroid/app/AlertDialog;)V

    invoke-virtual {v8, v0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    goto :goto_0
.end method
