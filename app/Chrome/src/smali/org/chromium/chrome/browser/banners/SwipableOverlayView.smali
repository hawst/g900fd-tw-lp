.class public abstract Lorg/chromium/chrome/browser/banners/SwipableOverlayView;
.super Landroid/widget/FrameLayout;
.source "SwipableOverlayView.java"


# static fields
.field static final synthetic $assertionsDisabled:Z

.field protected static final MS_ANIMATION_DURATION:J = 0xfaL

.field protected static final ZERO_THRESHOLD:F = 0.001f


# instance fields
.field private final mAnimatorListenerAdapter:Landroid/animation/AnimatorListenerAdapter;

.field private mContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

.field private mCurrentAnimation:Landroid/animation/AnimatorSet;

.field private mDragDirection:I

.field private mDragStartMs:J

.field private mDragXPerMs:F

.field private final mGestureDetector:Landroid/view/GestureDetector;

.field private mGestureState:I

.field private final mGestureStateListener:Lorg/chromium/content_public/browser/GestureStateListener;

.field private mInitialOffsetY:I

.field private mInitialTranslationY:F

.field private final mInterpolator:Landroid/view/animation/Interpolator;

.field private mIsBeingDisplayedForFirstTime:Z

.field private mIsDismissed:Z

.field private mParentHeight:I

.field private mTotalHeight:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 70
    const-class v0, Lorg/chromium/chrome/browser/banners/SwipableOverlayView;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/chromium/chrome/browser/banners/SwipableOverlayView;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .prologue
    .line 144
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 145
    invoke-direct {p0}, Lorg/chromium/chrome/browser/banners/SwipableOverlayView;->createGestureListener()Landroid/view/GestureDetector$SimpleOnGestureListener;

    move-result-object v0

    .line 146
    new-instance v1, Landroid/view/GestureDetector;

    invoke-direct {v1, p1, v0}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v1, p0, Lorg/chromium/chrome/browser/banners/SwipableOverlayView;->mGestureDetector:Landroid/view/GestureDetector;

    .line 147
    invoke-direct {p0}, Lorg/chromium/chrome/browser/banners/SwipableOverlayView;->createGestureStateListener()Lorg/chromium/content_public/browser/GestureStateListener;

    move-result-object v0

    iput-object v0, p0, Lorg/chromium/chrome/browser/banners/SwipableOverlayView;->mGestureStateListener:Lorg/chromium/content_public/browser/GestureStateListener;

    .line 148
    const/4 v0, 0x0

    iput v0, p0, Lorg/chromium/chrome/browser/banners/SwipableOverlayView;->mGestureState:I

    .line 149
    invoke-direct {p0}, Lorg/chromium/chrome/browser/banners/SwipableOverlayView;->createAnimatorListenerAdapter()Landroid/animation/AnimatorListenerAdapter;

    move-result-object v0

    iput-object v0, p0, Lorg/chromium/chrome/browser/banners/SwipableOverlayView;->mAnimatorListenerAdapter:Landroid/animation/AnimatorListenerAdapter;

    .line 150
    new-instance v0, Landroid/view/animation/DecelerateInterpolator;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-direct {v0, v1}, Landroid/view/animation/DecelerateInterpolator;-><init>(F)V

    iput-object v0, p0, Lorg/chromium/chrome/browser/banners/SwipableOverlayView;->mInterpolator:Landroid/view/animation/Interpolator;

    .line 151
    return-void
.end method

.method static synthetic access$000(Lorg/chromium/chrome/browser/banners/SwipableOverlayView;)I
    .locals 1

    .prologue
    .line 70
    iget v0, p0, Lorg/chromium/chrome/browser/banners/SwipableOverlayView;->mGestureState:I

    return v0
.end method

.method static synthetic access$002(Lorg/chromium/chrome/browser/banners/SwipableOverlayView;I)I
    .locals 0

    .prologue
    .line 70
    iput p1, p0, Lorg/chromium/chrome/browser/banners/SwipableOverlayView;->mGestureState:I

    return p1
.end method

.method static synthetic access$100(Lorg/chromium/chrome/browser/banners/SwipableOverlayView;)I
    .locals 1

    .prologue
    .line 70
    iget v0, p0, Lorg/chromium/chrome/browser/banners/SwipableOverlayView;->mDragDirection:I

    return v0
.end method

.method static synthetic access$1000(Lorg/chromium/chrome/browser/banners/SwipableOverlayView;)I
    .locals 1

    .prologue
    .line 70
    iget v0, p0, Lorg/chromium/chrome/browser/banners/SwipableOverlayView;->mTotalHeight:I

    return v0
.end method

.method static synthetic access$102(Lorg/chromium/chrome/browser/banners/SwipableOverlayView;I)I
    .locals 0

    .prologue
    .line 70
    iput p1, p0, Lorg/chromium/chrome/browser/banners/SwipableOverlayView;->mDragDirection:I

    return p1
.end method

.method static synthetic access$1102(Lorg/chromium/chrome/browser/banners/SwipableOverlayView;Z)Z
    .locals 0

    .prologue
    .line 70
    iput-boolean p1, p0, Lorg/chromium/chrome/browser/banners/SwipableOverlayView;->mIsBeingDisplayedForFirstTime:Z

    return p1
.end method

.method static synthetic access$1200(Lorg/chromium/chrome/browser/banners/SwipableOverlayView;)Landroid/animation/AnimatorSet;
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lorg/chromium/chrome/browser/banners/SwipableOverlayView;->mCurrentAnimation:Landroid/animation/AnimatorSet;

    return-object v0
.end method

.method static synthetic access$1202(Lorg/chromium/chrome/browser/banners/SwipableOverlayView;Landroid/animation/AnimatorSet;)Landroid/animation/AnimatorSet;
    .locals 0

    .prologue
    .line 70
    iput-object p1, p0, Lorg/chromium/chrome/browser/banners/SwipableOverlayView;->mCurrentAnimation:Landroid/animation/AnimatorSet;

    return-object p1
.end method

.method static synthetic access$1300(Lorg/chromium/chrome/browser/banners/SwipableOverlayView;)Z
    .locals 1

    .prologue
    .line 70
    iget-boolean v0, p0, Lorg/chromium/chrome/browser/banners/SwipableOverlayView;->mIsDismissed:Z

    return v0
.end method

.method static synthetic access$202(Lorg/chromium/chrome/browser/banners/SwipableOverlayView;F)F
    .locals 0

    .prologue
    .line 70
    iput p1, p0, Lorg/chromium/chrome/browser/banners/SwipableOverlayView;->mDragXPerMs:F

    return p1
.end method

.method static synthetic access$300(Lorg/chromium/chrome/browser/banners/SwipableOverlayView;)J
    .locals 2

    .prologue
    .line 70
    iget-wide v0, p0, Lorg/chromium/chrome/browser/banners/SwipableOverlayView;->mDragStartMs:J

    return-wide v0
.end method

.method static synthetic access$302(Lorg/chromium/chrome/browser/banners/SwipableOverlayView;J)J
    .locals 1

    .prologue
    .line 70
    iput-wide p1, p0, Lorg/chromium/chrome/browser/banners/SwipableOverlayView;->mDragStartMs:J

    return-wide p1
.end method

.method static synthetic access$400(Lorg/chromium/chrome/browser/banners/SwipableOverlayView;)F
    .locals 1

    .prologue
    .line 70
    invoke-direct {p0}, Lorg/chromium/chrome/browser/banners/SwipableOverlayView;->calculateAnimationAlpha()F

    move-result v0

    return v0
.end method

.method static synthetic access$500(Lorg/chromium/chrome/browser/banners/SwipableOverlayView;)V
    .locals 0

    .prologue
    .line 70
    invoke-direct {p0}, Lorg/chromium/chrome/browser/banners/SwipableOverlayView;->onFinishHorizontalGesture()V

    return-void
.end method

.method static synthetic access$600(Lorg/chromium/chrome/browser/banners/SwipableOverlayView;)Z
    .locals 1

    .prologue
    .line 70
    invoke-direct {p0}, Lorg/chromium/chrome/browser/banners/SwipableOverlayView;->cancelCurrentAnimation()Z

    move-result v0

    return v0
.end method

.method static synthetic access$700(Lorg/chromium/chrome/browser/banners/SwipableOverlayView;II)V
    .locals 0

    .prologue
    .line 70
    invoke-direct {p0, p1, p2}, Lorg/chromium/chrome/browser/banners/SwipableOverlayView;->beginGesture(II)V

    return-void
.end method

.method static synthetic access$800(Lorg/chromium/chrome/browser/banners/SwipableOverlayView;II)I
    .locals 1

    .prologue
    .line 70
    invoke-direct {p0, p1, p2}, Lorg/chromium/chrome/browser/banners/SwipableOverlayView;->computeScrollDifference(II)I

    move-result v0

    return v0
.end method

.method static synthetic access$900(Lorg/chromium/chrome/browser/banners/SwipableOverlayView;)F
    .locals 1

    .prologue
    .line 70
    iget v0, p0, Lorg/chromium/chrome/browser/banners/SwipableOverlayView;->mInitialTranslationY:F

    return v0
.end method

.method private beginGesture(II)V
    .locals 2

    .prologue
    .line 576
    invoke-virtual {p0}, Lorg/chromium/chrome/browser/banners/SwipableOverlayView;->getTranslationY()F

    move-result v0

    iput v0, p0, Lorg/chromium/chrome/browser/banners/SwipableOverlayView;->mInitialTranslationY:F

    .line 577
    iget v0, p0, Lorg/chromium/chrome/browser/banners/SwipableOverlayView;->mInitialTranslationY:F

    iget v1, p0, Lorg/chromium/chrome/browser/banners/SwipableOverlayView;->mTotalHeight:I

    int-to-float v1, v1

    cmpg-float v0, v0, v1

    if-gez v0, :cond_0

    const/4 v0, 0x1

    .line 578
    :goto_0
    if-eqz v0, :cond_1

    .line 579
    :goto_1
    add-int v0, p1, p2

    iput v0, p0, Lorg/chromium/chrome/browser/banners/SwipableOverlayView;->mInitialOffsetY:I

    .line 580
    return-void

    .line 577
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 578
    :cond_1
    iget v0, p0, Lorg/chromium/chrome/browser/banners/SwipableOverlayView;->mTotalHeight:I

    invoke-static {p1, v0}, Ljava/lang/Math;->min(II)I

    move-result p1

    goto :goto_1
.end method

.method private calculateAnimationAlpha()F
    .locals 3

    .prologue
    .line 485
    invoke-virtual {p0}, Lorg/chromium/chrome/browser/banners/SwipableOverlayView;->getTranslationX()F

    move-result v0

    invoke-virtual {p0}, Lorg/chromium/chrome/browser/banners/SwipableOverlayView;->getWidth()I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    .line 486
    const/4 v1, 0x0

    const/high16 v2, 0x3e800000    # 0.25f

    sub-float/2addr v0, v2

    invoke-static {v1, v0}, Ljava/lang/Math;->max(FF)F

    move-result v0

    .line 487
    const/high16 v1, 0x3f800000    # 1.0f

    const/high16 v2, 0x3f400000    # 0.75f

    div-float/2addr v0, v2

    sub-float v0, v1, v0

    return v0
.end method

.method private calculateMsRequiredToFlingOffScreen()F
    .locals 2

    .prologue
    .line 525
    iget v0, p0, Lorg/chromium/chrome/browser/banners/SwipableOverlayView;->mDragDirection:I

    invoke-virtual {p0}, Lorg/chromium/chrome/browser/banners/SwipableOverlayView;->getWidth()I

    move-result v1

    mul-int/2addr v0, v1

    int-to-float v0, v0

    invoke-virtual {p0}, Lorg/chromium/chrome/browser/banners/SwipableOverlayView;->getTranslationX()F

    move-result v1

    sub-float/2addr v0, v1

    .line 526
    iget v1, p0, Lorg/chromium/chrome/browser/banners/SwipableOverlayView;->mDragXPerMs:F

    div-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    return v0
.end method

.method private cancelCurrentAnimation()Z
    .locals 1

    .prologue
    .line 587
    invoke-direct {p0}, Lorg/chromium/chrome/browser/banners/SwipableOverlayView;->mayCancelCurrentAnimation()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 589
    :goto_0
    return v0

    .line 588
    :cond_0
    iget-object v0, p0, Lorg/chromium/chrome/browser/banners/SwipableOverlayView;->mCurrentAnimation:Landroid/animation/AnimatorSet;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lorg/chromium/chrome/browser/banners/SwipableOverlayView;->mCurrentAnimation:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->cancel()V

    .line 589
    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private computeScrollDifference(II)I
    .locals 2

    .prologue
    .line 492
    add-int v0, p1, p2

    iget v1, p0, Lorg/chromium/chrome/browser/banners/SwipableOverlayView;->mInitialOffsetY:I

    sub-int/2addr v0, v1

    return v0
.end method

.method private createAnimation(FFFJ)V
    .locals 10

    .prologue
    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 537
    new-array v0, v7, [Landroid/animation/PropertyValuesHolder;

    const-string/jumbo v1, "alpha"

    new-array v2, v8, [F

    invoke-virtual {p0}, Lorg/chromium/chrome/browser/banners/SwipableOverlayView;->getAlpha()F

    move-result v3

    aput v3, v2, v6

    aput p1, v2, v7

    invoke-static {v1, v2}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v1

    aput-object v1, v0, v6

    invoke-static {p0, v0}, Landroid/animation/ObjectAnimator;->ofPropertyValuesHolder(Ljava/lang/Object;[Landroid/animation/PropertyValuesHolder;)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 540
    new-array v1, v7, [Landroid/animation/PropertyValuesHolder;

    const-string/jumbo v2, "translationX"

    new-array v3, v8, [F

    invoke-virtual {p0}, Lorg/chromium/chrome/browser/banners/SwipableOverlayView;->getTranslationX()F

    move-result v4

    aput v4, v3, v6

    aput p2, v3, v7

    invoke-static {v2, v3}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v2

    aput-object v2, v1, v6

    invoke-static {p0, v1}, Landroid/animation/ObjectAnimator;->ofPropertyValuesHolder(Ljava/lang/Object;[Landroid/animation/PropertyValuesHolder;)Landroid/animation/ObjectAnimator;

    move-result-object v1

    .line 543
    new-array v2, v7, [Landroid/animation/PropertyValuesHolder;

    const-string/jumbo v3, "translationY"

    new-array v4, v8, [F

    invoke-virtual {p0}, Lorg/chromium/chrome/browser/banners/SwipableOverlayView;->getTranslationY()F

    move-result v5

    aput v5, v4, v6

    aput p3, v4, v7

    invoke-static {v3, v4}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v3

    aput-object v3, v2, v6

    invoke-static {p0, v2}, Landroid/animation/ObjectAnimator;->ofPropertyValuesHolder(Ljava/lang/Object;[Landroid/animation/PropertyValuesHolder;)Landroid/animation/ObjectAnimator;

    move-result-object v2

    .line 547
    new-instance v3, Landroid/animation/AnimatorSet;

    invoke-direct {v3}, Landroid/animation/AnimatorSet;-><init>()V

    iput-object v3, p0, Lorg/chromium/chrome/browser/banners/SwipableOverlayView;->mCurrentAnimation:Landroid/animation/AnimatorSet;

    .line 548
    iget-object v3, p0, Lorg/chromium/chrome/browser/banners/SwipableOverlayView;->mCurrentAnimation:Landroid/animation/AnimatorSet;

    invoke-virtual {v3, p4, p5}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    .line 549
    iget-object v3, p0, Lorg/chromium/chrome/browser/banners/SwipableOverlayView;->mCurrentAnimation:Landroid/animation/AnimatorSet;

    const/4 v4, 0x3

    new-array v4, v4, [Landroid/animation/Animator;

    aput-object v0, v4, v6

    aput-object v1, v4, v7

    aput-object v2, v4, v8

    invoke-virtual {v3, v4}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    .line 550
    iget-object v0, p0, Lorg/chromium/chrome/browser/banners/SwipableOverlayView;->mCurrentAnimation:Landroid/animation/AnimatorSet;

    iget-object v1, p0, Lorg/chromium/chrome/browser/banners/SwipableOverlayView;->mAnimatorListenerAdapter:Landroid/animation/AnimatorListenerAdapter;

    invoke-virtual {v0, v1}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 551
    iget-object v0, p0, Lorg/chromium/chrome/browser/banners/SwipableOverlayView;->mCurrentAnimation:Landroid/animation/AnimatorSet;

    iget-object v1, p0, Lorg/chromium/chrome/browser/banners/SwipableOverlayView;->mInterpolator:Landroid/view/animation/Interpolator;

    invoke-virtual {v0, v1}, Landroid/animation/AnimatorSet;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 552
    iget-object v0, p0, Lorg/chromium/chrome/browser/banners/SwipableOverlayView;->mCurrentAnimation:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->start()V

    .line 553
    return-void
.end method

.method private createAnimatorListenerAdapter()Landroid/animation/AnimatorListenerAdapter;
    .locals 1

    .prologue
    .line 560
    new-instance v0, Lorg/chromium/chrome/browser/banners/SwipableOverlayView$4;

    invoke-direct {v0, p0}, Lorg/chromium/chrome/browser/banners/SwipableOverlayView$4;-><init>(Lorg/chromium/chrome/browser/banners/SwipableOverlayView;)V

    return-object v0
.end method

.method private createGestureListener()Landroid/view/GestureDetector$SimpleOnGestureListener;
    .locals 1

    .prologue
    .line 237
    new-instance v0, Lorg/chromium/chrome/browser/banners/SwipableOverlayView$1;

    invoke-direct {v0, p0}, Lorg/chromium/chrome/browser/banners/SwipableOverlayView$1;-><init>(Lorg/chromium/chrome/browser/banners/SwipableOverlayView;)V

    return-object v0
.end method

.method private createGestureStateListener()Lorg/chromium/content_public/browser/GestureStateListener;
    .locals 1

    .prologue
    .line 312
    new-instance v0, Lorg/chromium/chrome/browser/banners/SwipableOverlayView$2;

    invoke-direct {v0, p0}, Lorg/chromium/chrome/browser/banners/SwipableOverlayView$2;-><init>(Lorg/chromium/chrome/browser/banners/SwipableOverlayView;)V

    return-object v0
.end method

.method private createHorizontalSnapAnimation(Z)V
    .locals 8

    .prologue
    const-wide/16 v5, 0xfa

    const/4 v3, 0x0

    .line 424
    if-eqz p1, :cond_0

    .line 426
    const/high16 v2, 0x3f800000    # 1.0f

    invoke-virtual {p0}, Lorg/chromium/chrome/browser/banners/SwipableOverlayView;->getTranslationY()F

    move-result v4

    move-object v1, p0

    invoke-direct/range {v1 .. v6}, Lorg/chromium/chrome/browser/banners/SwipableOverlayView;->createAnimation(FFFJ)V

    .line 451
    :goto_0
    return-void

    .line 428
    :cond_0
    iget v1, p0, Lorg/chromium/chrome/browser/banners/SwipableOverlayView;->mDragDirection:I

    if-nez v1, :cond_1

    .line 430
    const/4 v1, -0x1

    iput v1, p0, Lorg/chromium/chrome/browser/banners/SwipableOverlayView;->mDragDirection:I

    .line 433
    :cond_1
    iget v1, p0, Lorg/chromium/chrome/browser/banners/SwipableOverlayView;->mDragDirection:I

    invoke-virtual {p0}, Lorg/chromium/chrome/browser/banners/SwipableOverlayView;->getWidth()I

    move-result v2

    mul-int/2addr v1, v2

    int-to-float v4, v1

    .line 437
    iget v1, p0, Lorg/chromium/chrome/browser/banners/SwipableOverlayView;->mGestureState:I

    packed-switch v1, :pswitch_data_0

    :pswitch_0
    move-wide v6, v5

    .line 444
    :goto_1
    invoke-virtual {p0}, Lorg/chromium/chrome/browser/banners/SwipableOverlayView;->getTranslationY()F

    move-result v5

    move-object v2, p0

    invoke-direct/range {v2 .. v7}, Lorg/chromium/chrome/browser/banners/SwipableOverlayView;->createAnimation(FFFJ)V

    goto :goto_0

    .line 439
    :pswitch_1
    invoke-direct {p0}, Lorg/chromium/chrome/browser/banners/SwipableOverlayView;->calculateMsRequiredToFlingOffScreen()F

    move-result v1

    float-to-long v6, v1

    .line 440
    goto :goto_1

    .line 443
    :pswitch_2
    const-wide/16 v6, 0x2ee

    goto :goto_1

    .line 437
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private createLayoutChangeListener()Landroid/view/View$OnLayoutChangeListener;
    .locals 1

    .prologue
    .line 390
    new-instance v0, Lorg/chromium/chrome/browser/banners/SwipableOverlayView$3;

    invoke-direct {v0, p0}, Lorg/chromium/chrome/browser/banners/SwipableOverlayView$3;-><init>(Lorg/chromium/chrome/browser/banners/SwipableOverlayView;)V

    return-object v0
.end method

.method private determineFinalHorizontalLocation()I
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 503
    iget v1, p0, Lorg/chromium/chrome/browser/banners/SwipableOverlayView;->mGestureState:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_1

    .line 508
    invoke-direct {p0}, Lorg/chromium/chrome/browser/banners/SwipableOverlayView;->calculateMsRequiredToFlingOffScreen()F

    move-result v1

    .line 509
    const/high16 v2, 0x43fa0000    # 500.0f

    cmpl-float v1, v1, v2

    if-lez v1, :cond_2

    .line 517
    :cond_0
    :goto_0
    return v0

    .line 510
    :cond_1
    iget v1, p0, Lorg/chromium/chrome/browser/banners/SwipableOverlayView;->mGestureState:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_2

    .line 512
    invoke-virtual {p0}, Lorg/chromium/chrome/browser/banners/SwipableOverlayView;->getWidth()I

    move-result v1

    int-to-float v1, v1

    const/high16 v2, 0x3f400000    # 0.75f

    mul-float/2addr v1, v2

    .line 514
    invoke-virtual {p0}, Lorg/chromium/chrome/browser/banners/SwipableOverlayView;->getTranslationX()F

    move-result v2

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    cmpg-float v1, v2, v1

    if-ltz v1, :cond_0

    .line 517
    :cond_2
    iget v0, p0, Lorg/chromium/chrome/browser/banners/SwipableOverlayView;->mDragDirection:I

    goto :goto_0
.end method

.method private mayCancelCurrentAnimation()Z
    .locals 1

    .prologue
    .line 598
    iget-boolean v0, p0, Lorg/chromium/chrome/browser/banners/SwipableOverlayView;->mIsBeingDisplayedForFirstTime:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lorg/chromium/chrome/browser/banners/SwipableOverlayView;->mIsDismissed:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private onFinishHorizontalGesture()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 295
    invoke-direct {p0}, Lorg/chromium/chrome/browser/banners/SwipableOverlayView;->determineFinalHorizontalLocation()I

    move-result v0

    iput v0, p0, Lorg/chromium/chrome/browser/banners/SwipableOverlayView;->mDragDirection:I

    .line 296
    iget v0, p0, Lorg/chromium/chrome/browser/banners/SwipableOverlayView;->mDragDirection:I

    if-nez v0, :cond_0

    .line 298
    invoke-direct {p0, v1}, Lorg/chromium/chrome/browser/banners/SwipableOverlayView;->createHorizontalSnapAnimation(Z)V

    .line 304
    :goto_0
    return-void

    .line 301
    :cond_0
    invoke-virtual {p0}, Lorg/chromium/chrome/browser/banners/SwipableOverlayView;->onViewSwipedAway()V

    .line 302
    invoke-virtual {p0, v1}, Lorg/chromium/chrome/browser/banners/SwipableOverlayView;->dismiss(Z)Z

    goto :goto_0
.end method


# virtual methods
.method protected addToView(Lorg/chromium/content/browser/ContentViewCore;)V
    .locals 3

    .prologue
    .line 158
    sget-boolean v0, Lorg/chromium/chrome/browser/banners/SwipableOverlayView;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lorg/chromium/chrome/browser/banners/SwipableOverlayView;->mContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 159
    :cond_0
    iput-object p1, p0, Lorg/chromium/chrome/browser/banners/SwipableOverlayView;->mContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

    .line 160
    invoke-virtual {p1}, Lorg/chromium/content/browser/ContentViewCore;->getContainerView()Landroid/view/ViewGroup;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p0}, Lorg/chromium/chrome/browser/banners/SwipableOverlayView;->createLayoutParams()Landroid/view/ViewGroup$MarginLayoutParams;

    move-result-object v2

    invoke-virtual {v0, p0, v1, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    .line 161
    iget-object v0, p0, Lorg/chromium/chrome/browser/banners/SwipableOverlayView;->mGestureStateListener:Lorg/chromium/content_public/browser/GestureStateListener;

    invoke-virtual {p1, v0}, Lorg/chromium/content/browser/ContentViewCore;->addGestureStateListener(Lorg/chromium/content_public/browser/GestureStateListener;)V

    .line 164
    invoke-direct {p0}, Lorg/chromium/chrome/browser/banners/SwipableOverlayView;->createLayoutChangeListener()Landroid/view/View$OnLayoutChangeListener;

    move-result-object v0

    invoke-virtual {p0, v0}, Lorg/chromium/chrome/browser/banners/SwipableOverlayView;->addOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    .line 165
    return-void
.end method

.method protected createLayoutParams()Landroid/view/ViewGroup$MarginLayoutParams;
    .locals 4

    .prologue
    .line 173
    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    const/4 v1, -0x1

    const/4 v2, -0x2

    const/16 v3, 0x51

    invoke-direct {v0, v1, v2, v3}, Landroid/widget/FrameLayout$LayoutParams;-><init>(III)V

    return-object v0
.end method

.method createVerticalSnapAnimation(Z)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 412
    if-eqz p1, :cond_0

    move v3, v2

    .line 413
    :goto_0
    invoke-virtual {p0}, Lorg/chromium/chrome/browser/banners/SwipableOverlayView;->getTranslationY()F

    move-result v0

    sub-float v0, v3, v0

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    iget v1, p0, Lorg/chromium/chrome/browser/banners/SwipableOverlayView;->mTotalHeight:I

    int-to-float v1, v1

    div-float/2addr v0, v1

    .line 414
    const/high16 v1, 0x437a0000    # 250.0f

    mul-float/2addr v0, v1

    float-to-long v4, v0

    .line 415
    const/high16 v1, 0x3f800000    # 1.0f

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lorg/chromium/chrome/browser/banners/SwipableOverlayView;->createAnimation(FFFJ)V

    .line 416
    return-void

    .line 412
    :cond_0
    iget v0, p0, Lorg/chromium/chrome/browser/banners/SwipableOverlayView;->mTotalHeight:I

    int-to-float v3, v0

    goto :goto_0
.end method

.method protected dismiss(Z)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 458
    invoke-virtual {p0}, Lorg/chromium/chrome/browser/banners/SwipableOverlayView;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-boolean v2, p0, Lorg/chromium/chrome/browser/banners/SwipableOverlayView;->mIsDismissed:Z

    if-eqz v2, :cond_1

    :cond_0
    move v0, v1

    .line 466
    :goto_0
    return v0

    .line 460
    :cond_1
    iput-boolean v0, p0, Lorg/chromium/chrome/browser/banners/SwipableOverlayView;->mIsDismissed:Z

    .line 461
    if-eqz p1, :cond_2

    .line 462
    invoke-direct {p0, v1}, Lorg/chromium/chrome/browser/banners/SwipableOverlayView;->createHorizontalSnapAnimation(Z)V

    goto :goto_0

    .line 464
    :cond_2
    invoke-virtual {p0, v1}, Lorg/chromium/chrome/browser/banners/SwipableOverlayView;->createVerticalSnapAnimation(Z)V

    goto :goto_0
.end method

.method protected isDismissed()Z
    .locals 1

    .prologue
    .line 473
    iget-boolean v0, p0, Lorg/chromium/chrome/browser/banners/SwipableOverlayView;->mIsDismissed:Z

    return v0
.end method

.method protected onLayout(ZIIII)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 195
    invoke-virtual {p0}, Lorg/chromium/chrome/browser/banners/SwipableOverlayView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p0}, Lorg/chromium/ui/UiUtils;->isKeyboardShowing(Landroid/content/Context;Landroid/view/View;)Z

    move-result v0

    .line 196
    if-eqz v0, :cond_2

    const/4 v0, 0x4

    :goto_0
    invoke-virtual {p0, v0}, Lorg/chromium/chrome/browser/banners/SwipableOverlayView;->setVisibility(I)V

    .line 199
    invoke-virtual {p0}, Lorg/chromium/chrome/browser/banners/SwipableOverlayView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-nez v0, :cond_3

    move v0, v1

    .line 200
    :goto_1
    iget v2, p0, Lorg/chromium/chrome/browser/banners/SwipableOverlayView;->mParentHeight:I

    if-eq v2, v0, :cond_0

    .line 201
    iput v0, p0, Lorg/chromium/chrome/browser/banners/SwipableOverlayView;->mParentHeight:I

    .line 202
    iput v1, p0, Lorg/chromium/chrome/browser/banners/SwipableOverlayView;->mGestureState:I

    .line 203
    iget-object v0, p0, Lorg/chromium/chrome/browser/banners/SwipableOverlayView;->mCurrentAnimation:Landroid/animation/AnimatorSet;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/chromium/chrome/browser/banners/SwipableOverlayView;->mCurrentAnimation:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->end()V

    .line 207
    :cond_0
    invoke-virtual {p0}, Lorg/chromium/chrome/browser/banners/SwipableOverlayView;->getMeasuredHeight()I

    move-result v0

    iput v0, p0, Lorg/chromium/chrome/browser/banners/SwipableOverlayView;->mTotalHeight:I

    .line 208
    invoke-virtual {p0}, Lorg/chromium/chrome/browser/banners/SwipableOverlayView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    instance-of v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;

    if-eqz v0, :cond_1

    .line 209
    invoke-virtual {p0}, Lorg/chromium/chrome/browser/banners/SwipableOverlayView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 210
    iget v1, p0, Lorg/chromium/chrome/browser/banners/SwipableOverlayView;->mTotalHeight:I

    iget v2, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    add-int/2addr v0, v2

    add-int/2addr v0, v1

    iput v0, p0, Lorg/chromium/chrome/browser/banners/SwipableOverlayView;->mTotalHeight:I

    .line 213
    :cond_1
    invoke-super/range {p0 .. p5}, Landroid/widget/FrameLayout;->onLayout(ZIIII)V

    .line 214
    return-void

    :cond_2
    move v0, v1

    .line 196
    goto :goto_0

    .line 199
    :cond_3
    invoke-virtual {p0}, Lorg/chromium/chrome/browser/banners/SwipableOverlayView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v0

    goto :goto_1
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 221
    iget-object v1, p0, Lorg/chromium/chrome/browser/banners/SwipableOverlayView;->mGestureDetector:Landroid/view/GestureDetector;

    invoke-virtual {v1, p1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 229
    :cond_0
    :goto_0
    return v0

    .line 222
    :cond_1
    iget-object v1, p0, Lorg/chromium/chrome/browser/banners/SwipableOverlayView;->mCurrentAnimation:Landroid/animation/AnimatorSet;

    if-nez v1, :cond_0

    .line 224
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v1

    .line 225
    if-eq v1, v0, :cond_2

    const/4 v2, 0x3

    if-ne v1, v2, :cond_3

    .line 226
    :cond_2
    invoke-direct {p0}, Lorg/chromium/chrome/browser/banners/SwipableOverlayView;->onFinishHorizontalGesture()V

    goto :goto_0

    .line 229
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected abstract onViewClicked()V
.end method

.method protected abstract onViewPressed(Landroid/view/MotionEvent;)V
.end method

.method protected abstract onViewSwipedAway()V
.end method

.method removeFromParent()Z
    .locals 1

    .prologue
    .line 181
    iget-object v0, p0, Lorg/chromium/chrome/browser/banners/SwipableOverlayView;->mContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

    if-eqz v0, :cond_0

    .line 182
    iget-object v0, p0, Lorg/chromium/chrome/browser/banners/SwipableOverlayView;->mContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

    invoke-virtual {v0}, Lorg/chromium/content/browser/ContentViewCore;->getContainerView()Landroid/view/ViewGroup;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 183
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/chromium/chrome/browser/banners/SwipableOverlayView;->mContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

    .line 184
    const/4 v0, 0x1

    .line 186
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
