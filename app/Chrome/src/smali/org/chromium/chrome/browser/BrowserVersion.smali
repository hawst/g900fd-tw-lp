.class public Lorg/chromium/chrome/browser/BrowserVersion;
.super Ljava/lang/Object;
.source "BrowserVersion.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getTermsOfServiceHtml()Ljava/lang/String;
    .locals 1

    .prologue
    .line 30
    invoke-static {}, Lorg/chromium/chrome/browser/BrowserVersion;->nativeGetTermsOfServiceHtml()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static isOfficialBuild()Z
    .locals 1

    .prologue
    .line 16
    invoke-static {}, Lorg/chromium/chrome/browser/BrowserVersion;->nativeIsOfficialBuild()Z

    move-result v0

    return v0
.end method

.method private static native nativeGetTermsOfServiceHtml()Ljava/lang/String;
.end method

.method private static native nativeIsOfficialBuild()Z
.end method
