.class public Lorg/chromium/chrome/browser/LogoBridge$Logo;
.super Ljava/lang/Object;
.source "LogoBridge.java"


# instance fields
.field public final altText:Ljava/lang/String;

.field public final image:Landroid/graphics/Bitmap;

.field public final onClickUrl:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/graphics/Bitmap;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    iput-object p1, p0, Lorg/chromium/chrome/browser/LogoBridge$Logo;->image:Landroid/graphics/Bitmap;

    .line 38
    iput-object p2, p0, Lorg/chromium/chrome/browser/LogoBridge$Logo;->onClickUrl:Ljava/lang/String;

    .line 39
    iput-object p3, p0, Lorg/chromium/chrome/browser/LogoBridge$Logo;->altText:Ljava/lang/String;

    .line 40
    return-void
.end method
