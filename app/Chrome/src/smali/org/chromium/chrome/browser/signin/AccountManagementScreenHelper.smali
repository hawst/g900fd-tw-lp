.class public Lorg/chromium/chrome/browser/signin/AccountManagementScreenHelper;
.super Ljava/lang/Object;
.source "AccountManagementScreenHelper.java"


# static fields
.field public static final ACCOUNT_MANAGEMENT_ADD_ACCOUNT:I = 0x8

.field public static final ACCOUNT_MANAGEMENT_MENU_ADD_ACCOUNT:I = 0x1

.field public static final ACCOUNT_MANAGEMENT_MENU_CLICK_PRIMARY_ACCOUNT:I = 0x3

.field public static final ACCOUNT_MANAGEMENT_MENU_CLICK_SECONDARY_ACCOUNT:I = 0x4

.field public static final ACCOUNT_MANAGEMENT_MENU_GO_INCOGNITO:I = 0x2

.field public static final ACCOUNT_MANAGEMENT_MENU_SIGNOUT_CANCEL:I = 0x7

.field public static final ACCOUNT_MANAGEMENT_MENU_SIGNOUT_SIGNOUT:I = 0x6

.field public static final ACCOUNT_MANAGEMENT_MENU_TOGGLE_SIGNOUT:I = 0x5

.field public static final ACCOUNT_MANAGEMENT_MENU_VIEW:I = 0x0

.field public static final GAIA_SERVICE_TYPE_SIGNUP:I = 0x5

.field private static sManager:Lorg/chromium/chrome/browser/signin/AccountManagementScreenHelper$AccountManagementScreenManager;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 59
    return-void
.end method

.method public static logEvent(II)V
    .locals 0

    .prologue
    .line 115
    invoke-static {p0, p1}, Lorg/chromium/chrome/browser/signin/AccountManagementScreenHelper;->nativeLogEvent(II)V

    .line 116
    return-void
.end method

.method private static native nativeLogEvent(II)V
.end method

.method private static openAccountManagementScreen(Landroid/content/Context;Lorg/chromium/chrome/browser/profiles/Profile;I)V
    .locals 1

    .prologue
    .line 82
    invoke-static {}, Lorg/chromium/base/ThreadUtils;->assertOnUiThread()V

    .line 83
    sget-object v0, Lorg/chromium/chrome/browser/signin/AccountManagementScreenHelper;->sManager:Lorg/chromium/chrome/browser/signin/AccountManagementScreenHelper$AccountManagementScreenManager;

    if-nez v0, :cond_0

    .line 89
    :goto_0
    return-void

    .line 85
    :cond_0
    const/4 v0, 0x5

    if-ne p2, v0, :cond_1

    .line 86
    invoke-static {p0}, Lorg/chromium/chrome/browser/signin/AccountManagementScreenHelper;->openAndroidAccountCreationScreen(Landroid/content/Context;)V

    goto :goto_0

    .line 88
    :cond_1
    sget-object v0, Lorg/chromium/chrome/browser/signin/AccountManagementScreenHelper;->sManager:Lorg/chromium/chrome/browser/signin/AccountManagementScreenHelper$AccountManagementScreenManager;

    invoke-interface {v0, p0, p1, p2}, Lorg/chromium/chrome/browser/signin/AccountManagementScreenHelper$AccountManagementScreenManager;->openAccountManagementScreen(Landroid/content/Context;Lorg/chromium/chrome/browser/profiles/Profile;I)V

    goto :goto_0
.end method

.method private static openAndroidAccountCreationScreen(Landroid/content/Context;)V
    .locals 5

    .prologue
    .line 97
    const/16 v0, 0x8

    const/4 v1, 0x5

    invoke-static {v0, v1}, Lorg/chromium/chrome/browser/signin/AccountManagementScreenHelper;->nativeLogEvent(II)V

    .line 99
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "android.settings.ADD_ACCOUNT_SETTINGS"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 100
    const-string/jumbo v1, "account_types"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string/jumbo v4, "com.google"

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 102
    const/high16 v1, 0x34020000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 106
    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 107
    return-void
.end method

.method public static setManager(Lorg/chromium/chrome/browser/signin/AccountManagementScreenHelper$AccountManagementScreenManager;)V
    .locals 0

    .prologue
    .line 75
    invoke-static {}, Lorg/chromium/base/ThreadUtils;->assertOnUiThread()V

    .line 76
    sput-object p0, Lorg/chromium/chrome/browser/signin/AccountManagementScreenHelper;->sManager:Lorg/chromium/chrome/browser/signin/AccountManagementScreenHelper$AccountManagementScreenManager;

    .line 77
    return-void
.end method
