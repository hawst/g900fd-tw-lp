.class public Lorg/chromium/chrome/browser/BookmarksBridge;
.super Ljava/lang/Object;
.source "BookmarksBridge.java"


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private final mDelayedBookmarkCallbacks:Ljava/util/List;

.field private mIsDoingExtensiveChanges:Z

.field private mIsNativeBookmarkModelLoaded:Z

.field private mNativeBookmarksBridge:J

.field private final mObservers:Lorg/chromium/base/ObserverList;

.field private final mProfile:Lorg/chromium/chrome/browser/profiles/Profile;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 21
    const-class v0, Lorg/chromium/chrome/browser/BookmarksBridge;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/chromium/chrome/browser/BookmarksBridge;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lorg/chromium/chrome/browser/profiles/Profile;)V
    .locals 2

    .prologue
    .line 147
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lorg/chromium/chrome/browser/BookmarksBridge;->mDelayedBookmarkCallbacks:Ljava/util/List;

    .line 28
    new-instance v0, Lorg/chromium/base/ObserverList;

    invoke-direct {v0}, Lorg/chromium/base/ObserverList;-><init>()V

    iput-object v0, p0, Lorg/chromium/chrome/browser/BookmarksBridge;->mObservers:Lorg/chromium/base/ObserverList;

    .line 148
    iput-object p1, p0, Lorg/chromium/chrome/browser/BookmarksBridge;->mProfile:Lorg/chromium/chrome/browser/profiles/Profile;

    .line 149
    invoke-direct {p0, p1}, Lorg/chromium/chrome/browser/BookmarksBridge;->nativeInit(Lorg/chromium/chrome/browser/profiles/Profile;)J

    move-result-wide v0

    iput-wide v0, p0, Lorg/chromium/chrome/browser/BookmarksBridge;->mNativeBookmarksBridge:J

    .line 150
    iget-wide v0, p0, Lorg/chromium/chrome/browser/BookmarksBridge;->mNativeBookmarksBridge:J

    invoke-direct {p0, v0, v1}, Lorg/chromium/chrome/browser/BookmarksBridge;->nativeIsDoingExtensiveChanges(J)Z

    move-result v0

    iput-boolean v0, p0, Lorg/chromium/chrome/browser/BookmarksBridge;->mIsDoingExtensiveChanges:Z

    .line 151
    return-void
.end method

.method private static addToBookmarkIdList(Ljava/util/List;JI)V
    .locals 1

    .prologue
    .line 592
    new-instance v0, Lorg/chromium/components/bookmarks/BookmarkId;

    invoke-direct {v0, p1, p2, p3}, Lorg/chromium/components/bookmarks/BookmarkId;-><init>(JI)V

    invoke-interface {p0, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 593
    return-void
.end method

.method private static addToBookmarkIdListWithDepth(Ljava/util/List;JILjava/util/List;I)V
    .locals 1

    .prologue
    .line 598
    new-instance v0, Lorg/chromium/components/bookmarks/BookmarkId;

    invoke-direct {v0, p1, p2, p3}, Lorg/chromium/components/bookmarks/BookmarkId;-><init>(JI)V

    invoke-interface {p0, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 599
    invoke-static {p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {p4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 600
    return-void
.end method

.method private static addToList(Ljava/util/List;Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkItem;)V
    .locals 0

    .prologue
    .line 587
    invoke-interface {p0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 588
    return-void
.end method

.method private bookmarkModelChanged()V
    .locals 2

    .prologue
    .line 570
    iget-boolean v0, p0, Lorg/chromium/chrome/browser/BookmarksBridge;->mIsDoingExtensiveChanges:Z

    if-eqz v0, :cond_1

    .line 575
    :cond_0
    return-void

    .line 572
    :cond_1
    iget-object v0, p0, Lorg/chromium/chrome/browser/BookmarksBridge;->mObservers:Lorg/chromium/base/ObserverList;

    invoke-virtual {v0}, Lorg/chromium/base/ObserverList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkModelObserver;

    .line 573
    invoke-virtual {v0}, Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkModelObserver;->bookmarkModelChanged()V

    goto :goto_0
.end method

.method private bookmarkModelDeleted()V
    .locals 0

    .prologue
    .line 509
    invoke-virtual {p0}, Lorg/chromium/chrome/browser/BookmarksBridge;->destroy()V

    .line 510
    return-void
.end method

.method private bookmarkModelLoaded()V
    .locals 2

    .prologue
    .line 493
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/chromium/chrome/browser/BookmarksBridge;->mIsNativeBookmarkModelLoaded:Z

    .line 495
    iget-object v0, p0, Lorg/chromium/chrome/browser/BookmarksBridge;->mObservers:Lorg/chromium/base/ObserverList;

    invoke-virtual {v0}, Lorg/chromium/base/ObserverList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkModelObserver;

    .line 496
    invoke-virtual {v0}, Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkModelObserver;->bookmarkModelLoaded()V

    goto :goto_0

    .line 499
    :cond_0
    iget-object v0, p0, Lorg/chromium/chrome/browser/BookmarksBridge;->mDelayedBookmarkCallbacks:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 500
    const/4 v0, 0x0

    move v1, v0

    :goto_1
    iget-object v0, p0, Lorg/chromium/chrome/browser/BookmarksBridge;->mDelayedBookmarkCallbacks:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 501
    iget-object v0, p0, Lorg/chromium/chrome/browser/BookmarksBridge;->mDelayedBookmarkCallbacks:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/chromium/chrome/browser/BookmarksBridge$DelayedBookmarkCallback;

    # invokes: Lorg/chromium/chrome/browser/BookmarksBridge$DelayedBookmarkCallback;->callCallbackMethod()V
    invoke-static {v0}, Lorg/chromium/chrome/browser/BookmarksBridge$DelayedBookmarkCallback;->access$100(Lorg/chromium/chrome/browser/BookmarksBridge$DelayedBookmarkCallback;)V

    .line 500
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 503
    :cond_1
    iget-object v0, p0, Lorg/chromium/chrome/browser/BookmarksBridge;->mDelayedBookmarkCallbacks:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 505
    :cond_2
    return-void
.end method

.method private bookmarkNodeAdded(Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkItem;I)V
    .locals 2

    .prologue
    .line 524
    iget-boolean v0, p0, Lorg/chromium/chrome/browser/BookmarksBridge;->mIsDoingExtensiveChanges:Z

    if-eqz v0, :cond_1

    .line 529
    :cond_0
    return-void

    .line 526
    :cond_1
    iget-object v0, p0, Lorg/chromium/chrome/browser/BookmarksBridge;->mObservers:Lorg/chromium/base/ObserverList;

    invoke-virtual {v0}, Lorg/chromium/base/ObserverList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkModelObserver;

    .line 527
    invoke-virtual {v0, p1, p2}, Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkModelObserver;->bookmarkNodeAdded(Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkItem;I)V

    goto :goto_0
.end method

.method private bookmarkNodeChanged(Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkItem;)V
    .locals 2

    .prologue
    .line 541
    iget-boolean v0, p0, Lorg/chromium/chrome/browser/BookmarksBridge;->mIsDoingExtensiveChanges:Z

    if-eqz v0, :cond_1

    .line 546
    :cond_0
    return-void

    .line 543
    :cond_1
    iget-object v0, p0, Lorg/chromium/chrome/browser/BookmarksBridge;->mObservers:Lorg/chromium/base/ObserverList;

    invoke-virtual {v0}, Lorg/chromium/base/ObserverList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkModelObserver;

    .line 544
    invoke-virtual {v0, p1}, Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkModelObserver;->bookmarkNodeChanged(Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkItem;)V

    goto :goto_0
.end method

.method private bookmarkNodeChildrenReordered(Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkItem;)V
    .locals 2

    .prologue
    .line 550
    iget-boolean v0, p0, Lorg/chromium/chrome/browser/BookmarksBridge;->mIsDoingExtensiveChanges:Z

    if-eqz v0, :cond_1

    .line 555
    :cond_0
    return-void

    .line 552
    :cond_1
    iget-object v0, p0, Lorg/chromium/chrome/browser/BookmarksBridge;->mObservers:Lorg/chromium/base/ObserverList;

    invoke-virtual {v0}, Lorg/chromium/base/ObserverList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkModelObserver;

    .line 553
    invoke-virtual {v0, p1}, Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkModelObserver;->bookmarkNodeChildrenReordered(Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkItem;)V

    goto :goto_0
.end method

.method private bookmarkNodeMoved(Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkItem;ILorg/chromium/chrome/browser/BookmarksBridge$BookmarkItem;I)V
    .locals 2

    .prologue
    .line 515
    iget-boolean v0, p0, Lorg/chromium/chrome/browser/BookmarksBridge;->mIsDoingExtensiveChanges:Z

    if-eqz v0, :cond_1

    .line 520
    :cond_0
    return-void

    .line 517
    :cond_1
    iget-object v0, p0, Lorg/chromium/chrome/browser/BookmarksBridge;->mObservers:Lorg/chromium/base/ObserverList;

    invoke-virtual {v0}, Lorg/chromium/base/ObserverList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkModelObserver;

    .line 518
    invoke-virtual {v0, p1, p2, p3, p4}, Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkModelObserver;->bookmarkNodeMoved(Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkItem;ILorg/chromium/chrome/browser/BookmarksBridge$BookmarkItem;I)V

    goto :goto_0
.end method

.method private bookmarkNodeRemoved(Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkItem;ILorg/chromium/chrome/browser/BookmarksBridge$BookmarkItem;)V
    .locals 3

    .prologue
    .line 533
    iget-object v0, p0, Lorg/chromium/chrome/browser/BookmarksBridge;->mObservers:Lorg/chromium/base/ObserverList;

    invoke-virtual {v0}, Lorg/chromium/base/ObserverList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkModelObserver;

    .line 534
    iget-boolean v2, p0, Lorg/chromium/chrome/browser/BookmarksBridge;->mIsDoingExtensiveChanges:Z

    invoke-virtual {v0, p1, p2, p3, v2}, Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkModelObserver;->bookmarkNodeRemoved(Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkItem;ILorg/chromium/chrome/browser/BookmarksBridge$BookmarkItem;Z)V

    goto :goto_0

    .line 537
    :cond_0
    return-void
.end method

.method private static createBookmarkId(JI)Lorg/chromium/components/bookmarks/BookmarkId;
    .locals 2

    .prologue
    .line 604
    new-instance v0, Lorg/chromium/components/bookmarks/BookmarkId;

    invoke-direct {v0, p0, p1, p2}, Lorg/chromium/components/bookmarks/BookmarkId;-><init>(JI)V

    return-object v0
.end method

.method private static createBookmarkItem(JILjava/lang/String;Ljava/lang/String;ZJIZZ)Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkItem;
    .locals 12

    .prologue
    .line 581
    new-instance v3, Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkItem;

    new-instance v4, Lorg/chromium/components/bookmarks/BookmarkId;

    invoke-direct {v4, p0, p1, p2}, Lorg/chromium/components/bookmarks/BookmarkId;-><init>(JI)V

    new-instance v8, Lorg/chromium/components/bookmarks/BookmarkId;

    move-wide/from16 v0, p6

    move/from16 v2, p8

    invoke-direct {v8, v0, v1, v2}, Lorg/chromium/components/bookmarks/BookmarkId;-><init>(JI)V

    const/4 v11, 0x0

    move-object v5, p3

    move-object/from16 v6, p4

    move/from16 v7, p5

    move/from16 v9, p9

    move/from16 v10, p10

    invoke-direct/range {v3 .. v11}, Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkItem;-><init>(Lorg/chromium/components/bookmarks/BookmarkId;Ljava/lang/String;Ljava/lang/String;ZLorg/chromium/components/bookmarks/BookmarkId;ZZLorg/chromium/chrome/browser/BookmarksBridge$1;)V

    return-object v3
.end method

.method private extensiveBookmarkChangesBeginning()V
    .locals 1

    .prologue
    .line 559
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/chromium/chrome/browser/BookmarksBridge;->mIsDoingExtensiveChanges:Z

    .line 560
    return-void
.end method

.method private extensiveBookmarkChangesEnded()V
    .locals 1

    .prologue
    .line 564
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/chromium/chrome/browser/BookmarksBridge;->mIsDoingExtensiveChanges:Z

    .line 565
    invoke-direct {p0}, Lorg/chromium/chrome/browser/BookmarksBridge;->bookmarkModelChanged()V

    .line 566
    return-void
.end method

.method public static getNativeBookmarkModel(Lorg/chromium/chrome/browser/profiles/Profile;)J
    .locals 2

    .prologue
    .line 480
    invoke-static {p0}, Lorg/chromium/chrome/browser/BookmarksBridge;->nativeGetNativeBookmarkModel(Lorg/chromium/chrome/browser/profiles/Profile;)J

    move-result-wide v0

    return-wide v0
.end method

.method public static isEditBookmarksEnabled()Z
    .locals 1

    .prologue
    .line 484
    invoke-static {}, Lorg/chromium/chrome/browser/BookmarksBridge;->nativeIsEditBookmarksEnabled()Z

    move-result v0

    return v0
.end method

.method public static isEnhancedBookmarksEnabled(Lorg/chromium/chrome/browser/profiles/Profile;)Z
    .locals 1

    .prologue
    .line 488
    invoke-static {p0}, Lorg/chromium/chrome/browser/BookmarksBridge;->nativeIsEnhancedBookmarksFeatureEnabled(Lorg/chromium/chrome/browser/profiles/Profile;)Z

    move-result v0

    return v0
.end method

.method private native nativeAddBookmark(JLorg/chromium/components/bookmarks/BookmarkId;ILjava/lang/String;Ljava/lang/String;)Lorg/chromium/components/bookmarks/BookmarkId;
.end method

.method private native nativeAddFolder(JLorg/chromium/components/bookmarks/BookmarkId;ILjava/lang/String;)Lorg/chromium/components/bookmarks/BookmarkId;
.end method

.method private native nativeDeleteBookmark(JLorg/chromium/components/bookmarks/BookmarkId;)V
.end method

.method private native nativeDestroy(J)V
.end method

.method private native nativeDoesBookmarkExist(JJI)Z
.end method

.method private native nativeEndGroupingUndos(J)V
.end method

.method private native nativeGetAllBookmarkIDsOrderedByCreationDate(JLjava/util/List;)V
.end method

.method private native nativeGetAllFoldersWithDepths(JLjava/util/List;Ljava/util/List;)V
.end method

.method private native nativeGetBookmarkByID(JJI)Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkItem;
.end method

.method private native nativeGetBookmarksForFolder(JLorg/chromium/components/bookmarks/BookmarkId;Lorg/chromium/chrome/browser/BookmarksBridge$BookmarksCallback;Ljava/util/List;)V
.end method

.method private native nativeGetChildIDs(JJIZZLjava/util/List;)V
.end method

.method private native nativeGetCurrentFolderHierarchy(JLorg/chromium/components/bookmarks/BookmarkId;Lorg/chromium/chrome/browser/BookmarksBridge$BookmarksCallback;Ljava/util/List;)V
.end method

.method private native nativeGetDesktopFolderId(J)Lorg/chromium/components/bookmarks/BookmarkId;
.end method

.method private native nativeGetMobileFolderId(J)Lorg/chromium/components/bookmarks/BookmarkId;
.end method

.method private static native nativeGetNativeBookmarkModel(Lorg/chromium/chrome/browser/profiles/Profile;)J
.end method

.method private native nativeGetOtherFolderId(J)Lorg/chromium/components/bookmarks/BookmarkId;
.end method

.method private native nativeGetPermanentNodeIDs(JLjava/util/List;)V
.end method

.method private native nativeGetTopLevelFolderIDs(JZZLjava/util/List;)V
.end method

.method private native nativeGetTopLevelFolderParentIDs(JLjava/util/List;)V
.end method

.method private native nativeGetUncategorizedBookmarkIDs(JLjava/util/List;)V
.end method

.method private native nativeInit(Lorg/chromium/chrome/browser/profiles/Profile;)J
.end method

.method private native nativeIsDoingExtensiveChanges(J)Z
.end method

.method private static native nativeIsEditBookmarksEnabled()Z
.end method

.method private static native nativeIsEnhancedBookmarksFeatureEnabled(Lorg/chromium/chrome/browser/profiles/Profile;)Z
.end method

.method private native nativeLoadEmptyPartnerBookmarkShimForTesting(J)V
.end method

.method private native nativeMoveBookmark(JLorg/chromium/components/bookmarks/BookmarkId;Lorg/chromium/components/bookmarks/BookmarkId;I)V
.end method

.method private native nativeSetBookmarkTitle(JJILjava/lang/String;)V
.end method

.method private native nativeSetBookmarkUrl(JJILjava/lang/String;)V
.end method

.method private native nativeStartGroupingUndos(J)V
.end method

.method private native nativeUndo(J)V
.end method


# virtual methods
.method public addFolder(Lorg/chromium/components/bookmarks/BookmarkId;ILjava/lang/String;)Lorg/chromium/components/bookmarks/BookmarkId;
    .locals 7

    .prologue
    .line 425
    sget-boolean v0, Lorg/chromium/chrome/browser/BookmarksBridge;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lorg/chromium/components/bookmarks/BookmarkId;->getType()I

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 426
    :cond_0
    sget-boolean v0, Lorg/chromium/chrome/browser/BookmarksBridge;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    if-gez p2, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 427
    :cond_1
    sget-boolean v0, Lorg/chromium/chrome/browser/BookmarksBridge;->$assertionsDisabled:Z

    if-nez v0, :cond_2

    if-nez p3, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 429
    :cond_2
    iget-wide v2, p0, Lorg/chromium/chrome/browser/BookmarksBridge;->mNativeBookmarksBridge:J

    move-object v1, p0

    move-object v4, p1

    move v5, p2

    move-object v6, p3

    invoke-direct/range {v1 .. v6}, Lorg/chromium/chrome/browser/BookmarksBridge;->nativeAddFolder(JLorg/chromium/components/bookmarks/BookmarkId;ILjava/lang/String;)Lorg/chromium/components/bookmarks/BookmarkId;

    move-result-object v0

    return-object v0
.end method

.method public addObserver(Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkModelObserver;)V
    .locals 1

    .prologue
    .line 180
    iget-object v0, p0, Lorg/chromium/chrome/browser/BookmarksBridge;->mObservers:Lorg/chromium/base/ObserverList;

    invoke-virtual {v0, p1}, Lorg/chromium/base/ObserverList;->addObserver(Ljava/lang/Object;)Z

    .line 181
    return-void
.end method

.method public deleteBookmark(Lorg/chromium/components/bookmarks/BookmarkId;)V
    .locals 2

    .prologue
    .line 400
    iget-wide v0, p0, Lorg/chromium/chrome/browser/BookmarksBridge;->mNativeBookmarksBridge:J

    invoke-direct {p0, v0, v1, p1}, Lorg/chromium/chrome/browser/BookmarksBridge;->nativeDeleteBookmark(JLorg/chromium/components/bookmarks/BookmarkId;)V

    .line 401
    return-void
.end method

.method public destroy()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 157
    iget-wide v0, p0, Lorg/chromium/chrome/browser/BookmarksBridge;->mNativeBookmarksBridge:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 158
    iget-wide v0, p0, Lorg/chromium/chrome/browser/BookmarksBridge;->mNativeBookmarksBridge:J

    invoke-direct {p0, v0, v1}, Lorg/chromium/chrome/browser/BookmarksBridge;->nativeDestroy(J)V

    .line 159
    iput-wide v2, p0, Lorg/chromium/chrome/browser/BookmarksBridge;->mNativeBookmarksBridge:J

    .line 160
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/chromium/chrome/browser/BookmarksBridge;->mIsNativeBookmarkModelLoaded:Z

    .line 161
    iget-object v0, p0, Lorg/chromium/chrome/browser/BookmarksBridge;->mDelayedBookmarkCallbacks:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 163
    :cond_0
    iget-object v0, p0, Lorg/chromium/chrome/browser/BookmarksBridge;->mObservers:Lorg/chromium/base/ObserverList;

    invoke-virtual {v0}, Lorg/chromium/base/ObserverList;->clear()V

    .line 164
    return-void
.end method

.method public doesBookmarkExist(Lorg/chromium/components/bookmarks/BookmarkId;)Z
    .locals 7

    .prologue
    .line 343
    sget-boolean v0, Lorg/chromium/chrome/browser/BookmarksBridge;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lorg/chromium/chrome/browser/BookmarksBridge;->mIsNativeBookmarkModelLoaded:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 344
    :cond_0
    iget-wide v2, p0, Lorg/chromium/chrome/browser/BookmarksBridge;->mNativeBookmarksBridge:J

    invoke-virtual {p1}, Lorg/chromium/components/bookmarks/BookmarkId;->getId()J

    move-result-wide v4

    invoke-virtual {p1}, Lorg/chromium/components/bookmarks/BookmarkId;->getType()I

    move-result v6

    move-object v1, p0

    invoke-direct/range {v1 .. v6}, Lorg/chromium/chrome/browser/BookmarksBridge;->nativeDoesBookmarkExist(JJI)Z

    move-result v0

    return v0
.end method

.method public getAllBookmarkIDsOrderedByCreationDate()Ljava/util/List;
    .locals 4

    .prologue
    .line 317
    sget-boolean v0, Lorg/chromium/chrome/browser/BookmarksBridge;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lorg/chromium/chrome/browser/BookmarksBridge;->mIsNativeBookmarkModelLoaded:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 318
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 319
    iget-wide v2, p0, Lorg/chromium/chrome/browser/BookmarksBridge;->mNativeBookmarksBridge:J

    invoke-direct {p0, v2, v3, v0}, Lorg/chromium/chrome/browser/BookmarksBridge;->nativeGetAllBookmarkIDsOrderedByCreationDate(JLjava/util/List;)V

    .line 320
    return-object v0
.end method

.method public getAllFoldersWithDepths(Ljava/util/List;Ljava/util/List;)V
    .locals 2

    .prologue
    .line 264
    sget-boolean v0, Lorg/chromium/chrome/browser/BookmarksBridge;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lorg/chromium/chrome/browser/BookmarksBridge;->mIsNativeBookmarkModelLoaded:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 265
    :cond_0
    iget-wide v0, p0, Lorg/chromium/chrome/browser/BookmarksBridge;->mNativeBookmarksBridge:J

    invoke-direct {p0, v0, v1, p1, p2}, Lorg/chromium/chrome/browser/BookmarksBridge;->nativeGetAllFoldersWithDepths(JLjava/util/List;Ljava/util/List;)V

    .line 266
    return-void
.end method

.method public getBookmarkById(Lorg/chromium/components/bookmarks/BookmarkId;)Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkItem;
    .locals 7

    .prologue
    .line 202
    sget-boolean v0, Lorg/chromium/chrome/browser/BookmarksBridge;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lorg/chromium/chrome/browser/BookmarksBridge;->mIsNativeBookmarkModelLoaded:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 203
    :cond_0
    iget-wide v2, p0, Lorg/chromium/chrome/browser/BookmarksBridge;->mNativeBookmarksBridge:J

    invoke-virtual {p1}, Lorg/chromium/components/bookmarks/BookmarkId;->getId()J

    move-result-wide v4

    invoke-virtual {p1}, Lorg/chromium/components/bookmarks/BookmarkId;->getType()I

    move-result v6

    move-object v1, p0

    invoke-direct/range {v1 .. v6}, Lorg/chromium/chrome/browser/BookmarksBridge;->nativeGetBookmarkByID(JJI)Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkItem;

    move-result-object v0

    return-object v0
.end method

.method public getBookmarksForFolder(Lorg/chromium/components/bookmarks/BookmarkId;Lorg/chromium/chrome/browser/BookmarksBridge$BookmarksCallback;)V
    .locals 7

    .prologue
    .line 369
    iget-boolean v0, p0, Lorg/chromium/chrome/browser/BookmarksBridge;->mIsNativeBookmarkModelLoaded:Z

    if-eqz v0, :cond_0

    .line 370
    iget-wide v2, p0, Lorg/chromium/chrome/browser/BookmarksBridge;->mNativeBookmarksBridge:J

    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    move-object v1, p0

    move-object v4, p1

    move-object v5, p2

    invoke-direct/range {v1 .. v6}, Lorg/chromium/chrome/browser/BookmarksBridge;->nativeGetBookmarksForFolder(JLorg/chromium/components/bookmarks/BookmarkId;Lorg/chromium/chrome/browser/BookmarksBridge$BookmarksCallback;Ljava/util/List;)V

    .line 376
    :goto_0
    return-void

    .line 373
    :cond_0
    iget-object v6, p0, Lorg/chromium/chrome/browser/BookmarksBridge;->mDelayedBookmarkCallbacks:Ljava/util/List;

    new-instance v0, Lorg/chromium/chrome/browser/BookmarksBridge$DelayedBookmarkCallback;

    const/4 v3, 0x0

    const/4 v5, 0x0

    move-object v1, p1

    move-object v2, p2

    move-object v4, p0

    invoke-direct/range {v0 .. v5}, Lorg/chromium/chrome/browser/BookmarksBridge$DelayedBookmarkCallback;-><init>(Lorg/chromium/components/bookmarks/BookmarkId;Lorg/chromium/chrome/browser/BookmarksBridge$BookmarksCallback;ILorg/chromium/chrome/browser/BookmarksBridge;Lorg/chromium/chrome/browser/BookmarksBridge$1;)V

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public getChildIDs(Lorg/chromium/components/bookmarks/BookmarkId;ZZ)Ljava/util/List;
    .locals 10

    .prologue
    .line 302
    sget-boolean v0, Lorg/chromium/chrome/browser/BookmarksBridge;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lorg/chromium/chrome/browser/BookmarksBridge;->mIsNativeBookmarkModelLoaded:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 303
    :cond_0
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 304
    iget-wide v2, p0, Lorg/chromium/chrome/browser/BookmarksBridge;->mNativeBookmarksBridge:J

    invoke-virtual {p1}, Lorg/chromium/components/bookmarks/BookmarkId;->getId()J

    move-result-wide v4

    invoke-virtual {p1}, Lorg/chromium/components/bookmarks/BookmarkId;->getType()I

    move-result v6

    move-object v1, p0

    move v7, p2

    move v8, p3

    invoke-direct/range {v1 .. v9}, Lorg/chromium/chrome/browser/BookmarksBridge;->nativeGetChildIDs(JJIZZLjava/util/List;)V

    .line 310
    return-object v9
.end method

.method public getCurrentFolderHierarchy(Lorg/chromium/components/bookmarks/BookmarkId;Lorg/chromium/chrome/browser/BookmarksBridge$BookmarksCallback;)V
    .locals 7

    .prologue
    .line 386
    iget-boolean v0, p0, Lorg/chromium/chrome/browser/BookmarksBridge;->mIsNativeBookmarkModelLoaded:Z

    if-eqz v0, :cond_0

    .line 387
    iget-wide v2, p0, Lorg/chromium/chrome/browser/BookmarksBridge;->mNativeBookmarksBridge:J

    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    move-object v1, p0

    move-object v4, p1

    move-object v5, p2

    invoke-direct/range {v1 .. v6}, Lorg/chromium/chrome/browser/BookmarksBridge;->nativeGetCurrentFolderHierarchy(JLorg/chromium/components/bookmarks/BookmarkId;Lorg/chromium/chrome/browser/BookmarksBridge$BookmarksCallback;Ljava/util/List;)V

    .line 393
    :goto_0
    return-void

    .line 390
    :cond_0
    iget-object v6, p0, Lorg/chromium/chrome/browser/BookmarksBridge;->mDelayedBookmarkCallbacks:Ljava/util/List;

    new-instance v0, Lorg/chromium/chrome/browser/BookmarksBridge$DelayedBookmarkCallback;

    const/4 v3, 0x1

    const/4 v5, 0x0

    move-object v1, p1

    move-object v2, p2

    move-object v4, p0

    invoke-direct/range {v0 .. v5}, Lorg/chromium/chrome/browser/BookmarksBridge$DelayedBookmarkCallback;-><init>(Lorg/chromium/components/bookmarks/BookmarkId;Lorg/chromium/chrome/browser/BookmarksBridge$BookmarksCallback;ILorg/chromium/chrome/browser/BookmarksBridge;Lorg/chromium/chrome/browser/BookmarksBridge$1;)V

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public getDesktopFolderId()Lorg/chromium/components/bookmarks/BookmarkId;
    .locals 2

    .prologue
    .line 290
    sget-boolean v0, Lorg/chromium/chrome/browser/BookmarksBridge;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lorg/chromium/chrome/browser/BookmarksBridge;->mIsNativeBookmarkModelLoaded:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 291
    :cond_0
    iget-wide v0, p0, Lorg/chromium/chrome/browser/BookmarksBridge;->mNativeBookmarksBridge:J

    invoke-direct {p0, v0, v1}, Lorg/chromium/chrome/browser/BookmarksBridge;->nativeGetDesktopFolderId(J)Lorg/chromium/components/bookmarks/BookmarkId;

    move-result-object v0

    return-object v0
.end method

.method public getMobileFolderId()Lorg/chromium/components/bookmarks/BookmarkId;
    .locals 2

    .prologue
    .line 272
    sget-boolean v0, Lorg/chromium/chrome/browser/BookmarksBridge;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lorg/chromium/chrome/browser/BookmarksBridge;->mIsNativeBookmarkModelLoaded:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 273
    :cond_0
    iget-wide v0, p0, Lorg/chromium/chrome/browser/BookmarksBridge;->mNativeBookmarksBridge:J

    invoke-direct {p0, v0, v1}, Lorg/chromium/chrome/browser/BookmarksBridge;->nativeGetMobileFolderId(J)Lorg/chromium/components/bookmarks/BookmarkId;

    move-result-object v0

    return-object v0
.end method

.method public getOtherFolderId()Lorg/chromium/components/bookmarks/BookmarkId;
    .locals 2

    .prologue
    .line 281
    sget-boolean v0, Lorg/chromium/chrome/browser/BookmarksBridge;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lorg/chromium/chrome/browser/BookmarksBridge;->mIsNativeBookmarkModelLoaded:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 282
    :cond_0
    iget-wide v0, p0, Lorg/chromium/chrome/browser/BookmarksBridge;->mNativeBookmarksBridge:J

    invoke-direct {p0, v0, v1}, Lorg/chromium/chrome/browser/BookmarksBridge;->nativeGetOtherFolderId(J)Lorg/chromium/components/bookmarks/BookmarkId;

    move-result-object v0

    return-object v0
.end method

.method public getTopLevelFolderIDs(ZZ)Ljava/util/List;
    .locals 7

    .prologue
    .line 235
    sget-boolean v0, Lorg/chromium/chrome/browser/BookmarksBridge;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lorg/chromium/chrome/browser/BookmarksBridge;->mIsNativeBookmarkModelLoaded:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 236
    :cond_0
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 237
    iget-wide v2, p0, Lorg/chromium/chrome/browser/BookmarksBridge;->mNativeBookmarksBridge:J

    move-object v1, p0

    move v4, p1

    move v5, p2

    invoke-direct/range {v1 .. v6}, Lorg/chromium/chrome/browser/BookmarksBridge;->nativeGetTopLevelFolderIDs(JZZLjava/util/List;)V

    .line 238
    return-object v6
.end method

.method public getTopLevelFolderParentIDs()Ljava/util/List;
    .locals 4

    .prologue
    .line 220
    sget-boolean v0, Lorg/chromium/chrome/browser/BookmarksBridge;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lorg/chromium/chrome/browser/BookmarksBridge;->mIsNativeBookmarkModelLoaded:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 221
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 222
    iget-wide v2, p0, Lorg/chromium/chrome/browser/BookmarksBridge;->mNativeBookmarksBridge:J

    invoke-direct {p0, v2, v3, v0}, Lorg/chromium/chrome/browser/BookmarksBridge;->nativeGetTopLevelFolderParentIDs(JLjava/util/List;)V

    .line 223
    return-object v0
.end method

.method public getUncategorizedBookmarkIDs()Ljava/util/List;
    .locals 4

    .prologue
    .line 246
    sget-boolean v0, Lorg/chromium/chrome/browser/BookmarksBridge;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lorg/chromium/chrome/browser/BookmarksBridge;->mIsNativeBookmarkModelLoaded:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 247
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 248
    iget-wide v2, p0, Lorg/chromium/chrome/browser/BookmarksBridge;->mNativeBookmarksBridge:J

    invoke-direct {p0, v2, v3, v0}, Lorg/chromium/chrome/browser/BookmarksBridge;->nativeGetUncategorizedBookmarkIDs(JLjava/util/List;)V

    .line 249
    return-object v0
.end method

.method public isBookmarkModelLoaded()Z
    .locals 1

    .prologue
    .line 195
    iget-boolean v0, p0, Lorg/chromium/chrome/browser/BookmarksBridge;->mIsNativeBookmarkModelLoaded:Z

    return v0
.end method

.method public loadEmptyPartnerBookmarkShimForTesting()V
    .locals 2

    .prologue
    .line 172
    iget-wide v0, p0, Lorg/chromium/chrome/browser/BookmarksBridge;->mNativeBookmarksBridge:J

    invoke-direct {p0, v0, v1}, Lorg/chromium/chrome/browser/BookmarksBridge;->nativeLoadEmptyPartnerBookmarkShimForTesting(J)V

    .line 173
    return-void
.end method

.method public moveBookmark(Lorg/chromium/components/bookmarks/BookmarkId;Lorg/chromium/components/bookmarks/BookmarkId;I)V
    .locals 7

    .prologue
    .line 410
    iget-wide v2, p0, Lorg/chromium/chrome/browser/BookmarksBridge;->mNativeBookmarksBridge:J

    move-object v1, p0

    move-object v4, p1

    move-object v5, p2

    move v6, p3

    invoke-direct/range {v1 .. v6}, Lorg/chromium/chrome/browser/BookmarksBridge;->nativeMoveBookmark(JLorg/chromium/components/bookmarks/BookmarkId;Lorg/chromium/components/bookmarks/BookmarkId;I)V

    .line 411
    return-void
.end method

.method public removeObserver(Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkModelObserver;)V
    .locals 1

    .prologue
    .line 188
    iget-object v0, p0, Lorg/chromium/chrome/browser/BookmarksBridge;->mObservers:Lorg/chromium/base/ObserverList;

    invoke-virtual {v0, p1}, Lorg/chromium/base/ObserverList;->removeObserver(Ljava/lang/Object;)Z

    .line 189
    return-void
.end method

.method public setBookmarkTitle(Lorg/chromium/components/bookmarks/BookmarkId;Ljava/lang/String;)V
    .locals 8

    .prologue
    .line 327
    sget-boolean v0, Lorg/chromium/chrome/browser/BookmarksBridge;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lorg/chromium/chrome/browser/BookmarksBridge;->mIsNativeBookmarkModelLoaded:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 328
    :cond_0
    iget-wide v2, p0, Lorg/chromium/chrome/browser/BookmarksBridge;->mNativeBookmarksBridge:J

    invoke-virtual {p1}, Lorg/chromium/components/bookmarks/BookmarkId;->getId()J

    move-result-wide v4

    invoke-virtual {p1}, Lorg/chromium/components/bookmarks/BookmarkId;->getType()I

    move-result v6

    move-object v1, p0

    move-object v7, p2

    invoke-direct/range {v1 .. v7}, Lorg/chromium/chrome/browser/BookmarksBridge;->nativeSetBookmarkTitle(JJILjava/lang/String;)V

    .line 329
    return-void
.end method

.method public setBookmarkUrl(Lorg/chromium/components/bookmarks/BookmarkId;Ljava/lang/String;)V
    .locals 8

    .prologue
    .line 335
    sget-boolean v0, Lorg/chromium/chrome/browser/BookmarksBridge;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lorg/chromium/chrome/browser/BookmarksBridge;->mIsNativeBookmarkModelLoaded:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 336
    :cond_0
    iget-wide v2, p0, Lorg/chromium/chrome/browser/BookmarksBridge;->mNativeBookmarksBridge:J

    invoke-virtual {p1}, Lorg/chromium/components/bookmarks/BookmarkId;->getId()J

    move-result-wide v4

    invoke-virtual {p1}, Lorg/chromium/components/bookmarks/BookmarkId;->getType()I

    move-result v6

    move-object v1, p0

    move-object v7, p2

    invoke-direct/range {v1 .. v7}, Lorg/chromium/chrome/browser/BookmarksBridge;->nativeSetBookmarkUrl(JJILjava/lang/String;)V

    .line 337
    return-void
.end method

.method public undo()V
    .locals 2

    .prologue
    .line 457
    iget-wide v0, p0, Lorg/chromium/chrome/browser/BookmarksBridge;->mNativeBookmarksBridge:J

    invoke-direct {p0, v0, v1}, Lorg/chromium/chrome/browser/BookmarksBridge;->nativeUndo(J)V

    .line 458
    return-void
.end method
