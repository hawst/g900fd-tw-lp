.class public Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;
.super Ljava/lang/Object;
.source "ChromeBrowserProvider.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private final mChildren:Ljava/util/List;

.field private mFavicon:[B

.field private final mId:J

.field private final mName:Ljava/lang/String;

.field private final mParent:Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;

.field private mThumbnail:[B

.field private final mType:Lorg/chromium/chrome/browser/ChromeBrowserProvider$Type;

.field private final mUrl:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 934
    new-instance v0, Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode$1;

    invoke-direct {v0}, Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode$1;-><init>()V

    sput-object v0, Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(JLorg/chromium/chrome/browser/ChromeBrowserProvider$Type;Ljava/lang/String;Ljava/lang/String;Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;)V
    .locals 1

    .prologue
    .line 770
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 762
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;->mChildren:Ljava/util/List;

    .line 771
    iput-wide p1, p0, Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;->mId:J

    .line 772
    iput-object p4, p0, Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;->mName:Ljava/lang/String;

    .line 773
    iput-object p5, p0, Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;->mUrl:Ljava/lang/String;

    .line 774
    iput-object p3, p0, Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;->mType:Lorg/chromium/chrome/browser/ChromeBrowserProvider$Type;

    .line 775
    iput-object p6, p0, Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;->mParent:Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;

    .line 776
    return-void
.end method

.method private static create(JILjava/lang/String;Ljava/lang/String;Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;)Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;
    .locals 8

    .prologue
    .line 880
    new-instance v1, Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;

    invoke-static {}, Lorg/chromium/chrome/browser/ChromeBrowserProvider$Type;->values()[Lorg/chromium/chrome/browser/ChromeBrowserProvider$Type;

    move-result-object v0

    aget-object v4, v0, p2

    move-wide v2, p0

    move-object v5, p3

    move-object v6, p4

    move-object v7, p5

    invoke-direct/range {v1 .. v7}, Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;-><init>(JLorg/chromium/chrome/browser/ChromeBrowserProvider$Type;Ljava/lang/String;Ljava/lang/String;Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;)V

    return-object v1
.end method

.method private writeNodeContents(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 925
    iget-wide v0, p0, Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;->mId:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 926
    iget-object v0, p0, Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;->mName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 927
    iget-object v0, p0, Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;->mUrl:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 928
    iget-object v0, p0, Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;->mType:Lorg/chromium/chrome/browser/ChromeBrowserProvider$Type;

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/ChromeBrowserProvider$Type;->ordinal()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 929
    iget-object v0, p0, Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;->mFavicon:[B

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByteArray([B)V

    .line 930
    iget-object v0, p0, Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;->mThumbnail:[B

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByteArray([B)V

    .line 931
    iget-object v0, p0, Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;->mParent:Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;->mParent:Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;

    iget-wide v0, v0, Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;->mId:J

    :goto_0
    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 932
    return-void

    .line 931
    :cond_0
    const-wide/16 v0, -0x1

    goto :goto_0
.end method

.method private writeNodeContentsRecursive(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 917
    invoke-direct {p0, p1}, Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;->writeNodeContents(Landroid/os/Parcel;)V

    .line 918
    iget-object v0, p0, Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;->mChildren:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 919
    iget-object v0, p0, Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;->mChildren:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;

    .line 920
    invoke-direct {v0, p1}, Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;->writeNodeContentsRecursive(Landroid/os/Parcel;)V

    goto :goto_0

    .line 922
    :cond_0
    return-void
.end method


# virtual methods
.method public addChild(Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;)V
    .locals 1

    .prologue
    .line 836
    iget-object v0, p0, Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;->mChildren:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 837
    return-void
.end method

.method public children()Ljava/util/List;
    .locals 1

    .prologue
    .line 843
    iget-object v0, p0, Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;->mChildren:Ljava/util/List;

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 895
    const/4 v0, 0x0

    return v0
.end method

.method public favicon()[B
    .locals 1

    .prologue
    .line 810
    iget-object v0, p0, Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;->mFavicon:[B

    return-object v0
.end method

.method public getHierarchyRoot()Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;
    .locals 1

    .prologue
    .line 909
    .line 910
    :goto_0
    invoke-virtual {p0}, Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;->parent()Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 911
    invoke-virtual {p0}, Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;->parent()Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;

    move-result-object p0

    goto :goto_0

    .line 913
    :cond_0
    return-object p0
.end method

.method public id()J
    .locals 2

    .prologue
    .line 782
    iget-wide v0, p0, Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;->mId:J

    return-wide v0
.end method

.method public isUrl()Z
    .locals 1

    .prologue
    .line 850
    iget-object v0, p0, Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;->mUrl:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public name()Ljava/lang/String;
    .locals 1

    .prologue
    .line 789
    iget-object v0, p0, Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;->mName:Ljava/lang/String;

    return-object v0
.end method

.method public parent()Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;
    .locals 1

    .prologue
    .line 824
    iget-object v0, p0, Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;->mParent:Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;

    return-object v0
.end method

.method public setFavicon([B)V
    .locals 0

    .prologue
    .line 885
    iput-object p1, p0, Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;->mFavicon:[B

    .line 886
    return-void
.end method

.method public setThumbnail([B)V
    .locals 0

    .prologue
    .line 890
    iput-object p1, p0, Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;->mThumbnail:[B

    .line 891
    return-void
.end method

.method public thumbnail()[B
    .locals 1

    .prologue
    .line 817
    iget-object v0, p0, Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;->mThumbnail:[B

    return-object v0
.end method

.method public type()Lorg/chromium/chrome/browser/ChromeBrowserProvider$Type;
    .locals 1

    .prologue
    .line 803
    iget-object v0, p0, Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;->mType:Lorg/chromium/chrome/browser/ChromeBrowserProvider$Type;

    return-object v0
.end method

.method public url()Ljava/lang/String;
    .locals 1

    .prologue
    .line 796
    iget-object v0, p0, Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;->mUrl:Ljava/lang/String;

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 901
    iget-wide v0, p0, Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;->mId:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 904
    invoke-virtual {p0}, Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;->getHierarchyRoot()Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;

    move-result-object v0

    invoke-direct {v0, p1}, Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;->writeNodeContentsRecursive(Landroid/os/Parcel;)V

    .line 905
    return-void
.end method
