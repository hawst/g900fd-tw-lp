.class Lorg/chromium/chrome/browser/JavascriptAppModalDialog$JavascriptAppPromptDialog;
.super Lorg/chromium/chrome/browser/JavascriptAppModalDialog$JavascriptAppConfirmDialog;
.source "JavascriptAppModalDialog.java"


# instance fields
.field private final mDefaultPromptText:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)V
    .locals 0

    .prologue
    .line 270
    invoke-direct {p0, p1, p2, p3}, Lorg/chromium/chrome/browser/JavascriptAppModalDialog$JavascriptAppConfirmDialog;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 271
    iput-object p4, p0, Lorg/chromium/chrome/browser/JavascriptAppModalDialog$JavascriptAppPromptDialog;->mDefaultPromptText:Ljava/lang/String;

    .line 272
    return-void
.end method


# virtual methods
.method public prepare(Landroid/view/ViewGroup;)V
    .locals 2

    .prologue
    .line 276
    invoke-super {p0, p1}, Lorg/chromium/chrome/browser/JavascriptAppModalDialog$JavascriptAppConfirmDialog;->prepare(Landroid/view/ViewGroup;)V

    .line 277
    sget v0, Lorg/chromium/chrome/R$id;->js_modal_dialog_prompt:I

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    .line 278
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setVisibility(I)V

    .line 280
    iget-object v1, p0, Lorg/chromium/chrome/browser/JavascriptAppModalDialog$JavascriptAppPromptDialog;->mDefaultPromptText:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_0

    .line 281
    iget-object v1, p0, Lorg/chromium/chrome/browser/JavascriptAppModalDialog$JavascriptAppPromptDialog;->mDefaultPromptText:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 282
    invoke-virtual {v0}, Landroid/widget/EditText;->selectAll()V

    .line 284
    :cond_0
    return-void
.end method
