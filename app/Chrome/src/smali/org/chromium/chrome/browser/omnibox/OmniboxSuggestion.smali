.class public Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion;
.super Ljava/lang/Object;
.source "OmniboxSuggestion.java"


# instance fields
.field private final mAnswer:Lorg/chromium/chrome/browser/omnibox/SuggestionAnswer;

.field private final mAnswerContents:Ljava/lang/String;

.field private final mAnswerType:Ljava/lang/String;

.field private final mDescription:Ljava/lang/String;

.field private final mDisplayText:Ljava/lang/String;

.field private final mFillIntoEdit:Ljava/lang/String;

.field private final mFormattedUrl:Ljava/lang/String;

.field private final mIsDeletable:Z

.field private final mIsStarred:Z

.field private final mRelevance:I

.field private final mTransition:I

.field private final mType:Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;

.field private final mUrl:Ljava/lang/String;


# direct methods
.method public constructor <init>(IIILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)V
    .locals 1

    .prologue
    .line 95
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 96
    invoke-static {p1}, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;->getTypeFromNativeType(I)Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;

    move-result-object v0

    iput-object v0, p0, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion;->mType:Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;

    .line 97
    iput p2, p0, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion;->mRelevance:I

    .line 98
    iput p3, p0, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion;->mTransition:I

    .line 99
    iput-object p4, p0, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion;->mDisplayText:Ljava/lang/String;

    .line 100
    iput-object p5, p0, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion;->mDescription:Ljava/lang/String;

    .line 101
    iput-object p6, p0, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion;->mAnswerContents:Ljava/lang/String;

    .line 102
    iput-object p7, p0, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion;->mAnswerType:Ljava/lang/String;

    .line 103
    invoke-static {p8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    iput-object p4, p0, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion;->mFillIntoEdit:Ljava/lang/String;

    .line 104
    iput-object p9, p0, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion;->mUrl:Ljava/lang/String;

    .line 105
    iput-object p10, p0, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion;->mFormattedUrl:Ljava/lang/String;

    .line 106
    iput-boolean p11, p0, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion;->mIsStarred:Z

    .line 107
    iput-boolean p12, p0, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion;->mIsDeletable:Z

    .line 109
    iget-object v0, p0, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion;->mAnswerContents:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 112
    iget-object v0, p0, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion;->mAnswerContents:Ljava/lang/String;

    invoke-static {v0}, Lorg/chromium/chrome/browser/omnibox/SuggestionAnswer;->parseAnswerContents(Ljava/lang/String;)Lorg/chromium/chrome/browser/omnibox/SuggestionAnswer;

    move-result-object v0

    iput-object v0, p0, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion;->mAnswer:Lorg/chromium/chrome/browser/omnibox/SuggestionAnswer;

    .line 116
    :goto_1
    return-void

    :cond_0
    move-object p4, p8

    .line 103
    goto :goto_0

    .line 114
    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion;->mAnswer:Lorg/chromium/chrome/browser/omnibox/SuggestionAnswer;

    goto :goto_1
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 201
    instance-of v0, p1, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion;

    if-nez v0, :cond_1

    .line 212
    :cond_0
    :goto_0
    return v1

    .line 205
    :cond_1
    check-cast p1, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion;

    .line 207
    iget-object v0, p0, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion;->mAnswerContents:Ljava/lang/String;

    if-nez v0, :cond_2

    iget-object v0, p1, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion;->mAnswerContents:Ljava/lang/String;

    if-eqz v0, :cond_3

    :cond_2
    iget-object v0, p0, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion;->mAnswerContents:Ljava/lang/String;

    if-eqz v0, :cond_4

    iget-object v0, p1, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion;->mAnswerContents:Ljava/lang/String;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion;->mAnswerContents:Ljava/lang/String;

    iget-object v3, p1, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion;->mAnswerContents:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    :cond_3
    move v0, v2

    .line 212
    :goto_1
    iget-object v3, p0, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion;->mType:Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;

    iget-object v4, p1, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion;->mType:Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;

    if-ne v3, v4, :cond_0

    iget-object v3, p0, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion;->mFillIntoEdit:Ljava/lang/String;

    iget-object v4, p1, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion;->mFillIntoEdit:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion;->mDisplayText:Ljava/lang/String;

    iget-object v4, p1, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion;->mDisplayText:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion;->mIsStarred:Z

    iget-boolean v3, p1, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion;->mIsStarred:Z

    if-ne v0, v3, :cond_0

    iget-boolean v0, p0, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion;->mIsDeletable:Z

    iget-boolean v3, p1, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion;->mIsDeletable:Z

    if-ne v0, v3, :cond_0

    move v1, v2

    goto :goto_0

    :cond_4
    move v0, v1

    .line 207
    goto :goto_1
.end method

.method public getAnswer()Lorg/chromium/chrome/browser/omnibox/SuggestionAnswer;
    .locals 1

    .prologue
    .line 143
    iget-object v0, p0, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion;->mAnswer:Lorg/chromium/chrome/browser/omnibox/SuggestionAnswer;

    return-object v0
.end method

.method public getAnswerContents()Ljava/lang/String;
    .locals 1

    .prologue
    .line 135
    iget-object v0, p0, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion;->mAnswerContents:Ljava/lang/String;

    return-object v0
.end method

.method public getAnswerType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 139
    iget-object v0, p0, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion;->mAnswerType:Ljava/lang/String;

    return-object v0
.end method

.method public getDescription()Ljava/lang/String;
    .locals 1

    .prologue
    .line 131
    iget-object v0, p0, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion;->mDescription:Ljava/lang/String;

    return-object v0
.end method

.method public getDisplayText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 127
    iget-object v0, p0, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion;->mDisplayText:Ljava/lang/String;

    return-object v0
.end method

.method public getFillIntoEdit()Ljava/lang/String;
    .locals 1

    .prologue
    .line 151
    iget-object v0, p0, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion;->mFillIntoEdit:Ljava/lang/String;

    return-object v0
.end method

.method public getFormattedUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 159
    iget-object v0, p0, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion;->mFormattedUrl:Ljava/lang/String;

    return-object v0
.end method

.method public getRelevance()I
    .locals 1

    .prologue
    .line 181
    iget v0, p0, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion;->mRelevance:I

    return v0
.end method

.method public getTransition()I
    .locals 1

    .prologue
    .line 123
    iget v0, p0, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion;->mTransition:I

    return v0
.end method

.method public getType()Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;
    .locals 1

    .prologue
    .line 119
    iget-object v0, p0, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion;->mType:Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;

    return-object v0
.end method

.method public getUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 155
    iget-object v0, p0, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion;->mUrl:Ljava/lang/String;

    return-object v0
.end method

.method public hasAnswer()Z
    .locals 1

    .prologue
    .line 147
    iget-object v0, p0, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion;->mAnswer:Lorg/chromium/chrome/browser/omnibox/SuggestionAnswer;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 191
    iget-object v0, p0, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion;->mType:Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;

    # getter for: Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;->mNativeType:I
    invoke-static {v0}, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;->access$000(Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;)I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    iget-object v3, p0, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion;->mDisplayText:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->hashCode()I

    move-result v3

    add-int/2addr v0, v3

    iget-object v3, p0, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion;->mFillIntoEdit:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->hashCode()I

    move-result v3

    add-int/2addr v3, v0

    iget-boolean v0, p0, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion;->mIsStarred:Z

    if-eqz v0, :cond_1

    move v0, v1

    :goto_0
    add-int/2addr v0, v3

    iget-boolean v3, p0, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion;->mIsDeletable:Z

    if-eqz v3, :cond_2

    :goto_1
    add-int/2addr v0, v1

    .line 193
    iget-object v1, p0, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion;->mAnswerContents:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 194
    iget-object v1, p0, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion;->mAnswerContents:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 196
    :cond_0
    return v0

    :cond_1
    move v0, v2

    .line 191
    goto :goto_0

    :cond_2
    move v1, v2

    goto :goto_1
.end method

.method public isDeletable()Z
    .locals 1

    .prologue
    .line 174
    iget-boolean v0, p0, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion;->mIsDeletable:Z

    return v0
.end method

.method public isStarred()Z
    .locals 1

    .prologue
    .line 170
    iget-boolean v0, p0, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion;->mIsStarred:Z

    return v0
.end method

.method public isUrlSuggestion()Z
    .locals 1

    .prologue
    .line 163
    iget-object v0, p0, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion;->mType:Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;->isUrl()Z

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 186
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion;->mType:Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " relevance="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion;->mRelevance:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " \""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion;->mDisplayText:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "\" -> "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion;->mUrl:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
