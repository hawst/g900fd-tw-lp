.class Lorg/chromium/chrome/browser/KeyStoreSelectionDialog$3;
.super Ljava/lang/Object;
.source "KeyStoreSelectionDialog.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic this$0:Lorg/chromium/chrome/browser/KeyStoreSelectionDialog;

.field final synthetic val$choices:[Ljava/lang/CharSequence;


# direct methods
.method constructor <init>(Lorg/chromium/chrome/browser/KeyStoreSelectionDialog;[Ljava/lang/CharSequence;)V
    .locals 0

    .prologue
    .line 56
    iput-object p1, p0, Lorg/chromium/chrome/browser/KeyStoreSelectionDialog$3;->this$0:Lorg/chromium/chrome/browser/KeyStoreSelectionDialog;

    iput-object p2, p0, Lorg/chromium/chrome/browser/KeyStoreSelectionDialog$3;->val$choices:[Ljava/lang/CharSequence;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 2

    .prologue
    .line 59
    iget-object v0, p0, Lorg/chromium/chrome/browser/KeyStoreSelectionDialog$3;->val$choices:[Ljava/lang/CharSequence;

    aget-object v0, v0, p2

    # getter for: Lorg/chromium/chrome/browser/KeyStoreSelectionDialog;->SYSTEM_STORE:Ljava/lang/CharSequence;
    invoke-static {}, Lorg/chromium/chrome/browser/KeyStoreSelectionDialog;->access$200()Ljava/lang/CharSequence;

    move-result-object v1

    if-ne v0, v1, :cond_0

    .line 60
    iget-object v0, p0, Lorg/chromium/chrome/browser/KeyStoreSelectionDialog$3;->this$0:Lorg/chromium/chrome/browser/KeyStoreSelectionDialog;

    iget-object v1, p0, Lorg/chromium/chrome/browser/KeyStoreSelectionDialog$3;->this$0:Lorg/chromium/chrome/browser/KeyStoreSelectionDialog;

    # getter for: Lorg/chromium/chrome/browser/KeyStoreSelectionDialog;->mSystemCallback:Ljava/lang/Runnable;
    invoke-static {v1}, Lorg/chromium/chrome/browser/KeyStoreSelectionDialog;->access$300(Lorg/chromium/chrome/browser/KeyStoreSelectionDialog;)Ljava/lang/Runnable;

    move-result-object v1

    # setter for: Lorg/chromium/chrome/browser/KeyStoreSelectionDialog;->mSelectedChoice:Ljava/lang/Runnable;
    invoke-static {v0, v1}, Lorg/chromium/chrome/browser/KeyStoreSelectionDialog;->access$102(Lorg/chromium/chrome/browser/KeyStoreSelectionDialog;Ljava/lang/Runnable;)Ljava/lang/Runnable;

    .line 64
    :goto_0
    return-void

    .line 62
    :cond_0
    iget-object v0, p0, Lorg/chromium/chrome/browser/KeyStoreSelectionDialog$3;->this$0:Lorg/chromium/chrome/browser/KeyStoreSelectionDialog;

    iget-object v1, p0, Lorg/chromium/chrome/browser/KeyStoreSelectionDialog$3;->this$0:Lorg/chromium/chrome/browser/KeyStoreSelectionDialog;

    # getter for: Lorg/chromium/chrome/browser/KeyStoreSelectionDialog;->mSmartCardCallback:Ljava/lang/Runnable;
    invoke-static {v1}, Lorg/chromium/chrome/browser/KeyStoreSelectionDialog;->access$400(Lorg/chromium/chrome/browser/KeyStoreSelectionDialog;)Ljava/lang/Runnable;

    move-result-object v1

    # setter for: Lorg/chromium/chrome/browser/KeyStoreSelectionDialog;->mSelectedChoice:Ljava/lang/Runnable;
    invoke-static {v0, v1}, Lorg/chromium/chrome/browser/KeyStoreSelectionDialog;->access$102(Lorg/chromium/chrome/browser/KeyStoreSelectionDialog;Ljava/lang/Runnable;)Ljava/lang/Runnable;

    goto :goto_0
.end method
