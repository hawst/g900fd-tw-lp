.class public Lorg/chromium/chrome/browser/contextmenu/ChromeContextMenuPopulator;
.super Ljava/lang/Object;
.source "ChromeContextMenuPopulator.java"

# interfaces
.implements Lorg/chromium/chrome/browser/contextmenu/ContextMenuPopulator;


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private final mDelegate:Lorg/chromium/chrome/browser/contextmenu/ChromeContextMenuItemDelegate;

.field private mMenuInflater:Landroid/view/MenuInflater;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 21
    const-class v0, Lorg/chromium/chrome/browser/contextmenu/ChromeContextMenuPopulator;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/chromium/chrome/browser/contextmenu/ChromeContextMenuPopulator;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lorg/chromium/chrome/browser/contextmenu/ChromeContextMenuItemDelegate;)V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    iput-object p1, p0, Lorg/chromium/chrome/browser/contextmenu/ChromeContextMenuPopulator;->mDelegate:Lorg/chromium/chrome/browser/contextmenu/ChromeContextMenuItemDelegate;

    .line 33
    return-void
.end method


# virtual methods
.method public buildContextMenu(Landroid/view/ContextMenu;Landroid/content/Context;Lorg/chromium/chrome/browser/contextmenu/ContextMenuParams;)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 43
    invoke-virtual {p3}, Lorg/chromium/chrome/browser/contextmenu/ContextMenuParams;->getLinkUrl()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p3}, Lorg/chromium/chrome/browser/contextmenu/ContextMenuParams;->getLinkUrl()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v3, "about:blank"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 44
    invoke-virtual {p3}, Lorg/chromium/chrome/browser/contextmenu/ContextMenuParams;->getLinkUrl()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Landroid/view/ContextMenu;->setHeaderTitle(Ljava/lang/CharSequence;)Landroid/view/ContextMenu;

    .line 46
    :cond_0
    iget-object v0, p0, Lorg/chromium/chrome/browser/contextmenu/ChromeContextMenuPopulator;->mMenuInflater:Landroid/view/MenuInflater;

    if-nez v0, :cond_1

    new-instance v0, Landroid/view/MenuInflater;

    invoke-direct {v0, p2}, Landroid/view/MenuInflater;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lorg/chromium/chrome/browser/contextmenu/ChromeContextMenuPopulator;->mMenuInflater:Landroid/view/MenuInflater;

    .line 48
    :cond_1
    iget-object v0, p0, Lorg/chromium/chrome/browser/contextmenu/ChromeContextMenuPopulator;->mMenuInflater:Landroid/view/MenuInflater;

    sget v3, Lorg/chromium/chrome/R$menu;->chrome_context_menu:I

    invoke-virtual {v0, v3, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 50
    sget v0, Lorg/chromium/chrome/R$id;->contextmenu_group_anchor:I

    invoke-virtual {p3}, Lorg/chromium/chrome/browser/contextmenu/ContextMenuParams;->isAnchor()Z

    move-result v3

    invoke-interface {p1, v0, v3}, Landroid/view/ContextMenu;->setGroupVisible(IZ)V

    .line 51
    sget v0, Lorg/chromium/chrome/R$id;->contextmenu_group_image:I

    invoke-virtual {p3}, Lorg/chromium/chrome/browser/contextmenu/ContextMenuParams;->isImage()Z

    move-result v3

    invoke-interface {p1, v0, v3}, Landroid/view/ContextMenu;->setGroupVisible(IZ)V

    .line 52
    sget v0, Lorg/chromium/chrome/R$id;->contextmenu_group_video:I

    invoke-virtual {p3}, Lorg/chromium/chrome/browser/contextmenu/ContextMenuParams;->isVideo()Z

    move-result v3

    invoke-interface {p1, v0, v3}, Landroid/view/ContextMenu;->setGroupVisible(IZ)V

    .line 54
    iget-object v0, p0, Lorg/chromium/chrome/browser/contextmenu/ChromeContextMenuPopulator;->mDelegate:Lorg/chromium/chrome/browser/contextmenu/ChromeContextMenuItemDelegate;

    invoke-interface {v0}, Lorg/chromium/chrome/browser/contextmenu/ChromeContextMenuItemDelegate;->isIncognito()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lorg/chromium/chrome/browser/contextmenu/ChromeContextMenuPopulator;->mDelegate:Lorg/chromium/chrome/browser/contextmenu/ChromeContextMenuItemDelegate;

    invoke-interface {v0}, Lorg/chromium/chrome/browser/contextmenu/ChromeContextMenuItemDelegate;->isIncognitoSupported()Z

    move-result v0

    if-nez v0, :cond_3

    .line 55
    :cond_2
    sget v0, Lorg/chromium/chrome/R$id;->contextmenu_open_in_incognito_tab:I

    invoke-interface {p1, v0}, Landroid/view/ContextMenu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 58
    :cond_3
    invoke-virtual {p3}, Lorg/chromium/chrome/browser/contextmenu/ContextMenuParams;->getLinkText()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 59
    sget v0, Lorg/chromium/chrome/R$id;->contextmenu_copy_link_text:I

    invoke-interface {p1, v0}, Landroid/view/ContextMenu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 62
    :cond_4
    invoke-virtual {p3}, Lorg/chromium/chrome/browser/contextmenu/ContextMenuParams;->getLinkUrl()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/MailTo;->isMailTo(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 63
    sget v0, Lorg/chromium/chrome/R$id;->contextmenu_copy_link_address_text:I

    invoke-interface {p1, v0}, Landroid/view/ContextMenu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 68
    :goto_0
    sget v0, Lorg/chromium/chrome/R$id;->contextmenu_save_link_as:I

    invoke-interface {p1, v0}, Landroid/view/ContextMenu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-virtual {p3}, Lorg/chromium/chrome/browser/contextmenu/ContextMenuParams;->getLinkUrl()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lorg/chromium/chrome/browser/UrlUtilities;->isDownloadableScheme(Ljava/lang/String;)Z

    move-result v3

    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 71
    invoke-virtual {p3}, Lorg/chromium/chrome/browser/contextmenu/ContextMenuParams;->isVideo()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 72
    sget v0, Lorg/chromium/chrome/R$id;->contextmenu_save_video:I

    invoke-interface {p1, v0}, Landroid/view/ContextMenu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-virtual {p3}, Lorg/chromium/chrome/browser/contextmenu/ContextMenuParams;->getSrcUrl()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lorg/chromium/chrome/browser/UrlUtilities;->isDownloadableScheme(Ljava/lang/String;)Z

    move-result v1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 106
    :cond_5
    :goto_1
    return-void

    .line 65
    :cond_6
    sget v0, Lorg/chromium/chrome/R$id;->contextmenu_copy_email_address:I

    invoke-interface {p1, v0}, Landroid/view/ContextMenu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_0

    .line 74
    :cond_7
    invoke-virtual {p3}, Lorg/chromium/chrome/browser/contextmenu/ContextMenuParams;->isImage()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 75
    sget v0, Lorg/chromium/chrome/R$id;->contextmenu_save_image:I

    invoke-interface {p1, v0}, Landroid/view/ContextMenu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-virtual {p3}, Lorg/chromium/chrome/browser/contextmenu/ContextMenuParams;->getSrcUrl()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lorg/chromium/chrome/browser/UrlUtilities;->isDownloadableScheme(Ljava/lang/String;)Z

    move-result v3

    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 78
    iget-object v0, p0, Lorg/chromium/chrome/browser/contextmenu/ChromeContextMenuPopulator;->mDelegate:Lorg/chromium/chrome/browser/contextmenu/ChromeContextMenuItemDelegate;

    invoke-interface {v0}, Lorg/chromium/chrome/browser/contextmenu/ChromeContextMenuItemDelegate;->canLoadOriginalImage()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 79
    sget v0, Lorg/chromium/chrome/R$id;->contextmenu_open_image_in_new_tab:I

    invoke-interface {p1, v0}, Landroid/view/ContextMenu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 85
    :goto_2
    iget-object v0, p0, Lorg/chromium/chrome/browser/contextmenu/ChromeContextMenuPopulator;->mDelegate:Lorg/chromium/chrome/browser/contextmenu/ChromeContextMenuItemDelegate;

    invoke-interface {v0}, Lorg/chromium/chrome/browser/contextmenu/ChromeContextMenuItemDelegate;->getPageUrl()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3}, Lorg/chromium/chrome/browser/contextmenu/ContextMenuParams;->getSrcUrl()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 86
    sget v0, Lorg/chromium/chrome/R$id;->contextmenu_open_image:I

    invoke-interface {p1, v0}, Landroid/view/ContextMenu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 88
    :cond_8
    invoke-static {}, Lorg/chromium/chrome/browser/search_engines/TemplateUrlService;->getInstance()Lorg/chromium/chrome/browser/search_engines/TemplateUrlService;

    move-result-object v0

    .line 89
    invoke-virtual {p3}, Lorg/chromium/chrome/browser/contextmenu/ContextMenuParams;->getSrcUrl()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lorg/chromium/chrome/browser/UrlUtilities;->isDownloadableScheme(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_b

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/search_engines/TemplateUrlService;->isLoaded()Z

    move-result v3

    if-eqz v3, :cond_b

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/search_engines/TemplateUrlService;->isSearchByImageAvailable()Z

    move-result v3

    if-eqz v3, :cond_b

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/search_engines/TemplateUrlService;->getDefaultSearchEngineTemplateUrl()Lorg/chromium/chrome/browser/search_engines/TemplateUrlService$TemplateUrl;

    move-result-object v0

    if-eqz v0, :cond_b

    move v0, v1

    .line 95
    :goto_3
    sget v3, Lorg/chromium/chrome/R$id;->contextmenu_search_by_image:I

    invoke-interface {p1, v3}, Landroid/view/ContextMenu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v3

    invoke-interface {v3, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 96
    if-eqz v0, :cond_9

    .line 97
    sget v0, Lorg/chromium/chrome/R$id;->contextmenu_search_by_image:I

    invoke-interface {p1, v0}, Landroid/view/ContextMenu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    sget v3, Lorg/chromium/chrome/R$string;->contextmenu_search_web_for_image:I

    new-array v4, v1, [Ljava/lang/Object;

    invoke-static {}, Lorg/chromium/chrome/browser/search_engines/TemplateUrlService;->getInstance()Lorg/chromium/chrome/browser/search_engines/TemplateUrlService;

    move-result-object v5

    invoke-virtual {v5}, Lorg/chromium/chrome/browser/search_engines/TemplateUrlService;->getDefaultSearchEngineTemplateUrl()Lorg/chromium/chrome/browser/search_engines/TemplateUrlService$TemplateUrl;

    move-result-object v5

    invoke-virtual {v5}, Lorg/chromium/chrome/browser/search_engines/TemplateUrlService$TemplateUrl;->getShortName()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v2

    invoke-virtual {p2, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 103
    :cond_9
    sget v0, Lorg/chromium/chrome/R$id;->contextmenu_copy_image:I

    invoke-interface {p1, v0}, Landroid/view/ContextMenu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0x10

    if-lt v3, v4, :cond_c

    :goto_4
    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto/16 :goto_1

    .line 81
    :cond_a
    sget v0, Lorg/chromium/chrome/R$id;->contextmenu_open_original_image_in_new_tab:I

    invoke-interface {p1, v0}, Landroid/view/ContextMenu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_2

    :cond_b
    move v0, v2

    .line 89
    goto :goto_3

    :cond_c
    move v1, v2

    .line 103
    goto :goto_4
.end method

.method public onItemSelected(Lorg/chromium/chrome/browser/contextmenu/ContextMenuHelper;Lorg/chromium/chrome/browser/contextmenu/ContextMenuParams;I)Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v3, 0x1

    .line 110
    sget v0, Lorg/chromium/chrome/R$id;->contextmenu_open_in_new_tab:I

    if-ne p3, v0, :cond_1

    .line 111
    iget-object v0, p0, Lorg/chromium/chrome/browser/contextmenu/ChromeContextMenuPopulator;->mDelegate:Lorg/chromium/chrome/browser/contextmenu/ChromeContextMenuItemDelegate;

    invoke-virtual {p2}, Lorg/chromium/chrome/browser/contextmenu/ContextMenuParams;->getLinkUrl()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2}, Lorg/chromium/chrome/browser/contextmenu/ContextMenuParams;->getReferrer()Lorg/chromium/content_public/common/Referrer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lorg/chromium/chrome/browser/contextmenu/ChromeContextMenuItemDelegate;->onOpenInNewTab(Ljava/lang/String;Lorg/chromium/content_public/common/Referrer;)V

    .line 144
    :cond_0
    :goto_0
    return v3

    .line 112
    :cond_1
    sget v0, Lorg/chromium/chrome/R$id;->contextmenu_open_in_incognito_tab:I

    if-ne p3, v0, :cond_2

    .line 113
    iget-object v0, p0, Lorg/chromium/chrome/browser/contextmenu/ChromeContextMenuPopulator;->mDelegate:Lorg/chromium/chrome/browser/contextmenu/ChromeContextMenuItemDelegate;

    invoke-virtual {p2}, Lorg/chromium/chrome/browser/contextmenu/ContextMenuParams;->getLinkUrl()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lorg/chromium/chrome/browser/contextmenu/ChromeContextMenuItemDelegate;->onOpenInNewIncognitoTab(Ljava/lang/String;)V

    goto :goto_0

    .line 114
    :cond_2
    sget v0, Lorg/chromium/chrome/R$id;->contextmenu_open_image:I

    if-ne p3, v0, :cond_3

    .line 115
    iget-object v0, p0, Lorg/chromium/chrome/browser/contextmenu/ChromeContextMenuPopulator;->mDelegate:Lorg/chromium/chrome/browser/contextmenu/ChromeContextMenuItemDelegate;

    invoke-virtual {p2}, Lorg/chromium/chrome/browser/contextmenu/ContextMenuParams;->getSrcUrl()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2}, Lorg/chromium/chrome/browser/contextmenu/ContextMenuParams;->getReferrer()Lorg/chromium/content_public/common/Referrer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lorg/chromium/chrome/browser/contextmenu/ChromeContextMenuItemDelegate;->onOpenImageUrl(Ljava/lang/String;Lorg/chromium/content_public/common/Referrer;)V

    goto :goto_0

    .line 116
    :cond_3
    sget v0, Lorg/chromium/chrome/R$id;->contextmenu_open_image_in_new_tab:I

    if-eq p3, v0, :cond_4

    sget v0, Lorg/chromium/chrome/R$id;->contextmenu_open_original_image_in_new_tab:I

    if-ne p3, v0, :cond_5

    .line 118
    :cond_4
    iget-object v0, p0, Lorg/chromium/chrome/browser/contextmenu/ChromeContextMenuPopulator;->mDelegate:Lorg/chromium/chrome/browser/contextmenu/ChromeContextMenuItemDelegate;

    invoke-virtual {p2}, Lorg/chromium/chrome/browser/contextmenu/ContextMenuParams;->getSrcUrl()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2}, Lorg/chromium/chrome/browser/contextmenu/ContextMenuParams;->getReferrer()Lorg/chromium/content_public/common/Referrer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lorg/chromium/chrome/browser/contextmenu/ChromeContextMenuItemDelegate;->onOpenImageInNewTab(Ljava/lang/String;Lorg/chromium/content_public/common/Referrer;)V

    goto :goto_0

    .line 119
    :cond_5
    sget v0, Lorg/chromium/chrome/R$id;->contextmenu_copy_link_address_text:I

    if-ne p3, v0, :cond_6

    .line 120
    iget-object v0, p0, Lorg/chromium/chrome/browser/contextmenu/ChromeContextMenuPopulator;->mDelegate:Lorg/chromium/chrome/browser/contextmenu/ChromeContextMenuItemDelegate;

    invoke-virtual {p2}, Lorg/chromium/chrome/browser/contextmenu/ContextMenuParams;->getUnfilteredLinkUrl()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, v3}, Lorg/chromium/chrome/browser/contextmenu/ChromeContextMenuItemDelegate;->onSaveToClipboard(Ljava/lang/String;Z)V

    goto :goto_0

    .line 121
    :cond_6
    sget v0, Lorg/chromium/chrome/R$id;->contextmenu_copy_email_address:I

    if-ne p3, v0, :cond_7

    .line 122
    iget-object v0, p0, Lorg/chromium/chrome/browser/contextmenu/ChromeContextMenuPopulator;->mDelegate:Lorg/chromium/chrome/browser/contextmenu/ChromeContextMenuItemDelegate;

    invoke-virtual {p2}, Lorg/chromium/chrome/browser/contextmenu/ContextMenuParams;->getLinkUrl()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/MailTo;->parse(Ljava/lang/String;)Landroid/net/MailTo;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/MailTo;->getTo()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, v2}, Lorg/chromium/chrome/browser/contextmenu/ChromeContextMenuItemDelegate;->onSaveToClipboard(Ljava/lang/String;Z)V

    goto :goto_0

    .line 123
    :cond_7
    sget v0, Lorg/chromium/chrome/R$id;->contextmenu_copy_link_text:I

    if-ne p3, v0, :cond_8

    .line 124
    iget-object v0, p0, Lorg/chromium/chrome/browser/contextmenu/ChromeContextMenuPopulator;->mDelegate:Lorg/chromium/chrome/browser/contextmenu/ChromeContextMenuItemDelegate;

    invoke-virtual {p2}, Lorg/chromium/chrome/browser/contextmenu/ContextMenuParams;->getLinkText()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, v2}, Lorg/chromium/chrome/browser/contextmenu/ChromeContextMenuItemDelegate;->onSaveToClipboard(Ljava/lang/String;Z)V

    goto :goto_0

    .line 125
    :cond_8
    sget v0, Lorg/chromium/chrome/R$id;->contextmenu_save_image:I

    if-eq p3, v0, :cond_9

    sget v0, Lorg/chromium/chrome/R$id;->contextmenu_save_video:I

    if-ne p3, v0, :cond_a

    .line 127
    :cond_9
    iget-object v0, p0, Lorg/chromium/chrome/browser/contextmenu/ChromeContextMenuPopulator;->mDelegate:Lorg/chromium/chrome/browser/contextmenu/ChromeContextMenuItemDelegate;

    invoke-virtual {p2}, Lorg/chromium/chrome/browser/contextmenu/ContextMenuParams;->getSrcUrl()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, v2}, Lorg/chromium/chrome/browser/contextmenu/ChromeContextMenuItemDelegate;->startDownload(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 128
    invoke-virtual {p1, v2}, Lorg/chromium/chrome/browser/contextmenu/ContextMenuHelper;->startContextMenuDownload(Z)V

    goto :goto_0

    .line 130
    :cond_a
    sget v0, Lorg/chromium/chrome/R$id;->contextmenu_save_link_as:I

    if-ne p3, v0, :cond_b

    .line 131
    iget-object v0, p0, Lorg/chromium/chrome/browser/contextmenu/ChromeContextMenuPopulator;->mDelegate:Lorg/chromium/chrome/browser/contextmenu/ChromeContextMenuItemDelegate;

    invoke-virtual {p2}, Lorg/chromium/chrome/browser/contextmenu/ContextMenuParams;->getUnfilteredLinkUrl()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, v3}, Lorg/chromium/chrome/browser/contextmenu/ChromeContextMenuItemDelegate;->startDownload(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 132
    invoke-virtual {p1, v3}, Lorg/chromium/chrome/browser/contextmenu/ContextMenuHelper;->startContextMenuDownload(Z)V

    goto/16 :goto_0

    .line 134
    :cond_b
    sget v0, Lorg/chromium/chrome/R$id;->contextmenu_search_by_image:I

    if-ne p3, v0, :cond_c

    .line 135
    iget-object v0, p0, Lorg/chromium/chrome/browser/contextmenu/ChromeContextMenuPopulator;->mDelegate:Lorg/chromium/chrome/browser/contextmenu/ChromeContextMenuItemDelegate;

    invoke-interface {v0}, Lorg/chromium/chrome/browser/contextmenu/ChromeContextMenuItemDelegate;->onSearchByImageInNewTab()V

    goto/16 :goto_0

    .line 136
    :cond_c
    sget v0, Lorg/chromium/chrome/R$id;->contextmenu_copy_image:I

    if-ne p3, v0, :cond_d

    .line 137
    iget-object v0, p0, Lorg/chromium/chrome/browser/contextmenu/ChromeContextMenuPopulator;->mDelegate:Lorg/chromium/chrome/browser/contextmenu/ChromeContextMenuItemDelegate;

    invoke-virtual {p2}, Lorg/chromium/chrome/browser/contextmenu/ContextMenuParams;->getSrcUrl()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lorg/chromium/chrome/browser/contextmenu/ChromeContextMenuItemDelegate;->onSaveImageToClipboard(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 138
    :cond_d
    sget v0, Lorg/chromium/chrome/R$id;->contextmenu_copy_image_url:I

    if-ne p3, v0, :cond_e

    .line 139
    iget-object v0, p0, Lorg/chromium/chrome/browser/contextmenu/ChromeContextMenuPopulator;->mDelegate:Lorg/chromium/chrome/browser/contextmenu/ChromeContextMenuItemDelegate;

    invoke-virtual {p2}, Lorg/chromium/chrome/browser/contextmenu/ContextMenuParams;->getSrcUrl()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, v3}, Lorg/chromium/chrome/browser/contextmenu/ChromeContextMenuItemDelegate;->onSaveToClipboard(Ljava/lang/String;Z)V

    goto/16 :goto_0

    .line 141
    :cond_e
    sget-boolean v0, Lorg/chromium/chrome/browser/contextmenu/ChromeContextMenuPopulator;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0
.end method

.method public shouldShowContextMenu(Lorg/chromium/chrome/browser/contextmenu/ContextMenuParams;)Z
    .locals 1

    .prologue
    .line 37
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lorg/chromium/chrome/browser/contextmenu/ContextMenuParams;->isAnchor()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lorg/chromium/chrome/browser/contextmenu/ContextMenuParams;->isEditable()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lorg/chromium/chrome/browser/contextmenu/ContextMenuParams;->isImage()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lorg/chromium/chrome/browser/contextmenu/ContextMenuParams;->isSelectedText()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lorg/chromium/chrome/browser/contextmenu/ContextMenuParams;->isVideo()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lorg/chromium/chrome/browser/contextmenu/ContextMenuParams;->isCustomMenu()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
