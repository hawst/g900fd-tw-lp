.class public final Lorg/chromium/chrome/browser/EnhancedBookmarksBridge;
.super Ljava/lang/Object;
.source "EnhancedBookmarksBridge.java"


# annotations
.annotation runtime Lorg/chromium/base/JNINamespace;
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private mNativeEnhancedBookmarksBridge:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 13
    const-class v0, Lorg/chromium/chrome/browser/EnhancedBookmarksBridge;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/chromium/chrome/browser/EnhancedBookmarksBridge;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(J)V
    .locals 3

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    invoke-direct {p0, p1, p2}, Lorg/chromium/chrome/browser/EnhancedBookmarksBridge;->nativeInit(J)J

    move-result-wide v0

    iput-wide v0, p0, Lorg/chromium/chrome/browser/EnhancedBookmarksBridge;->mNativeEnhancedBookmarksBridge:J

    .line 19
    return-void
.end method

.method private native nativeDestroy(J)V
.end method

.method private native nativeGetBookmarkDescription(JJI)Ljava/lang/String;
.end method

.method private native nativeInit(J)J
.end method

.method private native nativeSetBookmarkDescription(JJILjava/lang/String;)V
.end method


# virtual methods
.method public final destroy()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 22
    sget-boolean v0, Lorg/chromium/chrome/browser/EnhancedBookmarksBridge;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-wide v0, p0, Lorg/chromium/chrome/browser/EnhancedBookmarksBridge;->mNativeEnhancedBookmarksBridge:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 23
    :cond_0
    iget-wide v0, p0, Lorg/chromium/chrome/browser/EnhancedBookmarksBridge;->mNativeEnhancedBookmarksBridge:J

    invoke-direct {p0, v0, v1}, Lorg/chromium/chrome/browser/EnhancedBookmarksBridge;->nativeDestroy(J)V

    .line 24
    iput-wide v2, p0, Lorg/chromium/chrome/browser/EnhancedBookmarksBridge;->mNativeEnhancedBookmarksBridge:J

    .line 25
    return-void
.end method

.method public final getBookmarkDescription(Lorg/chromium/components/bookmarks/BookmarkId;)Ljava/lang/String;
    .locals 7

    .prologue
    .line 28
    iget-wide v2, p0, Lorg/chromium/chrome/browser/EnhancedBookmarksBridge;->mNativeEnhancedBookmarksBridge:J

    invoke-virtual {p1}, Lorg/chromium/components/bookmarks/BookmarkId;->getId()J

    move-result-wide v4

    invoke-virtual {p1}, Lorg/chromium/components/bookmarks/BookmarkId;->getType()I

    move-result v6

    move-object v1, p0

    invoke-direct/range {v1 .. v6}, Lorg/chromium/chrome/browser/EnhancedBookmarksBridge;->nativeGetBookmarkDescription(JJI)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final setBookmarkDescription(Lorg/chromium/components/bookmarks/BookmarkId;Ljava/lang/String;)V
    .locals 8

    .prologue
    .line 33
    iget-wide v2, p0, Lorg/chromium/chrome/browser/EnhancedBookmarksBridge;->mNativeEnhancedBookmarksBridge:J

    invoke-virtual {p1}, Lorg/chromium/components/bookmarks/BookmarkId;->getId()J

    move-result-wide v4

    invoke-virtual {p1}, Lorg/chromium/components/bookmarks/BookmarkId;->getType()I

    move-result v6

    move-object v1, p0

    move-object v7, p2

    invoke-direct/range {v1 .. v7}, Lorg/chromium/chrome/browser/EnhancedBookmarksBridge;->nativeSetBookmarkDescription(JJILjava/lang/String;)V

    .line 35
    return-void
.end method
