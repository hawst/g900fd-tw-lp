.class public Lorg/chromium/chrome/browser/infobar/MessageInfoBar;
.super Lorg/chromium/chrome/browser/infobar/InfoBar;
.source "MessageInfoBar.java"


# direct methods
.method public constructor <init>(Ljava/lang/CharSequence;)V
    .locals 2

    .prologue
    .line 22
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1, p1}, Lorg/chromium/chrome/browser/infobar/MessageInfoBar;-><init>(Lorg/chromium/chrome/browser/infobar/InfoBarListeners$Dismiss;ILjava/lang/CharSequence;)V

    .line 23
    return-void
.end method

.method public constructor <init>(Lorg/chromium/chrome/browser/infobar/InfoBarListeners$Dismiss;ILjava/lang/CharSequence;)V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0, p1, p2, p3}, Lorg/chromium/chrome/browser/infobar/InfoBar;-><init>(Lorg/chromium/chrome/browser/infobar/InfoBarListeners$Dismiss;ILjava/lang/CharSequence;)V

    .line 34
    return-void
.end method


# virtual methods
.method public onCloseButtonClicked()V
    .locals 0

    .prologue
    .line 38
    invoke-super {p0}, Lorg/chromium/chrome/browser/infobar/InfoBar;->dismissJavaOnlyInfoBar()V

    .line 39
    return-void
.end method
