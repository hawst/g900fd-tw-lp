.class public Lorg/chromium/chrome/browser/omnibox/AutocompleteController;
.super Ljava/lang/Object;
.source "AutocompleteController.java"


# instance fields
.field private mCurrentNativeAutocompleteResult:J

.field private final mListener:Lorg/chromium/chrome/browser/omnibox/AutocompleteController$OnSuggestionsReceivedListener;

.field private mNativeAutocompleteControllerAndroid:J


# direct methods
.method public constructor <init>(Lorg/chromium/chrome/browser/omnibox/AutocompleteController$OnSuggestionsReceivedListener;)V
    .locals 1

    .prologue
    .line 34
    const/4 v0, 0x0

    invoke-direct {p0, v0, p1}, Lorg/chromium/chrome/browser/omnibox/AutocompleteController;-><init>(Lorg/chromium/chrome/browser/profiles/Profile;Lorg/chromium/chrome/browser/omnibox/AutocompleteController$OnSuggestionsReceivedListener;)V

    .line 35
    return-void
.end method

.method public constructor <init>(Lorg/chromium/chrome/browser/profiles/Profile;Lorg/chromium/chrome/browser/omnibox/AutocompleteController$OnSuggestionsReceivedListener;)V
    .locals 2

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    if-eqz p1, :cond_0

    .line 39
    invoke-virtual {p0, p1}, Lorg/chromium/chrome/browser/omnibox/AutocompleteController;->nativeInit(Lorg/chromium/chrome/browser/profiles/Profile;)J

    move-result-wide v0

    iput-wide v0, p0, Lorg/chromium/chrome/browser/omnibox/AutocompleteController;->mNativeAutocompleteControllerAndroid:J

    .line 41
    :cond_0
    iput-object p2, p0, Lorg/chromium/chrome/browser/omnibox/AutocompleteController;->mListener:Lorg/chromium/chrome/browser/omnibox/AutocompleteController$OnSuggestionsReceivedListener;

    .line 42
    return-void
.end method

.method private static addOmniboxSuggestionToList(Ljava/util/List;Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion;)V
    .locals 0

    .prologue
    .line 211
    invoke-interface {p0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 212
    return-void
.end method

.method private static buildOmniboxSuggestion(IIILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion;
    .locals 13

    .prologue
    .line 219
    new-instance v0, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion;

    move v1, p0

    move v2, p1

    move v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move/from16 v11, p10

    move/from16 v12, p11

    invoke-direct/range {v0 .. v12}, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion;-><init>(IIILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)V

    return-object v0
.end method

.method private static createOmniboxSuggestionList(I)Ljava/util/List;
    .locals 1

    .prologue
    .line 205
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, p0}, Ljava/util/ArrayList;-><init>(I)V

    return-object v0
.end method

.method private native nativeClassify(JLjava/lang/String;)Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion;
.end method

.method private native nativeDeleteSuggestion(JI)V
.end method

.method private native nativeGetTopSynchronousMatch(JLjava/lang/String;)Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion;
.end method

.method private native nativeOnSuggestionSelected(JILjava/lang/String;ZZJLorg/chromium/content_public/browser/WebContents;)V
.end method

.method public static native nativePrefetchZeroSuggestResults()V
.end method

.method public static native nativeQualifyPartialURLQuery(Ljava/lang/String;)Ljava/lang/String;
.end method

.method private native nativeResetSession(J)V
.end method

.method private native nativeStart(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZZZ)V
.end method

.method private native nativeStartZeroSuggest(JLjava/lang/String;Ljava/lang/String;ZZ)V
.end method

.method private native nativeStop(JZ)V
.end method

.method private native nativeUpdateMatchDestinationURLWithQueryFormulationTime(JIJ)Ljava/lang/String;
.end method

.method private notifyNativeDestroyed()V
    .locals 2

    .prologue
    .line 179
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lorg/chromium/chrome/browser/omnibox/AutocompleteController;->mNativeAutocompleteControllerAndroid:J

    .line 180
    return-void
.end method


# virtual methods
.method public classify(Ljava/lang/String;)Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion;
    .locals 2

    .prologue
    .line 97
    iget-wide v0, p0, Lorg/chromium/chrome/browser/omnibox/AutocompleteController;->mNativeAutocompleteControllerAndroid:J

    invoke-direct {p0, v0, v1, p1}, Lorg/chromium/chrome/browser/omnibox/AutocompleteController;->nativeClassify(JLjava/lang/String;)Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion;

    move-result-object v0

    return-object v0
.end method

.method public deleteSuggestion(I)V
    .locals 4

    .prologue
    .line 153
    iget-wide v0, p0, Lorg/chromium/chrome/browser/omnibox/AutocompleteController;->mNativeAutocompleteControllerAndroid:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 154
    iget-wide v0, p0, Lorg/chromium/chrome/browser/omnibox/AutocompleteController;->mNativeAutocompleteControllerAndroid:J

    invoke-direct {p0, v0, v1, p1}, Lorg/chromium/chrome/browser/omnibox/AutocompleteController;->nativeDeleteSuggestion(JI)V

    .line 156
    :cond_0
    return-void
.end method

.method public getCurrentNativeAutocompleteResult()J
    .locals 2

    .prologue
    .line 163
    iget-wide v0, p0, Lorg/chromium/chrome/browser/omnibox/AutocompleteController;->mCurrentNativeAutocompleteResult:J

    return-wide v0
.end method

.method public getTopSynchronousMatch(Ljava/lang/String;)Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion;
    .locals 2

    .prologue
    .line 248
    iget-wide v0, p0, Lorg/chromium/chrome/browser/omnibox/AutocompleteController;->mNativeAutocompleteControllerAndroid:J

    invoke-direct {p0, v0, v1, p1}, Lorg/chromium/chrome/browser/omnibox/AutocompleteController;->nativeGetTopSynchronousMatch(JLjava/lang/String;)Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion;

    move-result-object v0

    return-object v0
.end method

.method protected native nativeInit(Lorg/chromium/chrome/browser/profiles/Profile;)J
.end method

.method protected onSuggestionSelected(ILorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;Ljava/lang/String;ZZJLorg/chromium/content_public/browser/WebContents;)V
    .locals 12

    .prologue
    .line 198
    iget-wide v2, p0, Lorg/chromium/chrome/browser/omnibox/AutocompleteController;->mNativeAutocompleteControllerAndroid:J

    move-object v1, p0

    move v4, p1

    move-object v5, p3

    move/from16 v6, p4

    move/from16 v7, p5

    move-wide/from16 v8, p6

    move-object/from16 v10, p8

    invoke-direct/range {v1 .. v10}, Lorg/chromium/chrome/browser/omnibox/AutocompleteController;->nativeOnSuggestionSelected(JILjava/lang/String;ZZJLorg/chromium/content_public/browser/WebContents;)V

    .line 201
    return-void
.end method

.method protected onSuggestionsReceived(Ljava/util/List;Ljava/lang/String;J)V
    .locals 1

    .prologue
    .line 171
    iput-wide p3, p0, Lorg/chromium/chrome/browser/omnibox/AutocompleteController;->mCurrentNativeAutocompleteResult:J

    .line 174
    iget-object v0, p0, Lorg/chromium/chrome/browser/omnibox/AutocompleteController;->mListener:Lorg/chromium/chrome/browser/omnibox/AutocompleteController$OnSuggestionsReceivedListener;

    invoke-interface {v0, p1, p2}, Lorg/chromium/chrome/browser/omnibox/AutocompleteController$OnSuggestionsReceivedListener;->onSuggestionsReceived(Ljava/util/List;Ljava/lang/String;)V

    .line 175
    return-void
.end method

.method public resetSession()V
    .locals 4

    .prologue
    .line 143
    iget-wide v0, p0, Lorg/chromium/chrome/browser/omnibox/AutocompleteController;->mNativeAutocompleteControllerAndroid:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 144
    iget-wide v0, p0, Lorg/chromium/chrome/browser/omnibox/AutocompleteController;->mNativeAutocompleteControllerAndroid:J

    invoke-direct {p0, v0, v1}, Lorg/chromium/chrome/browser/omnibox/AutocompleteController;->nativeResetSession(J)V

    .line 146
    :cond_0
    return-void
.end method

.method public setProfile(Lorg/chromium/chrome/browser/profiles/Profile;)V
    .locals 2

    .prologue
    .line 55
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lorg/chromium/chrome/browser/omnibox/AutocompleteController;->stop(Z)V

    .line 56
    if-nez p1, :cond_0

    .line 57
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lorg/chromium/chrome/browser/omnibox/AutocompleteController;->mNativeAutocompleteControllerAndroid:J

    .line 62
    :goto_0
    return-void

    .line 61
    :cond_0
    invoke-virtual {p0, p1}, Lorg/chromium/chrome/browser/omnibox/AutocompleteController;->nativeInit(Lorg/chromium/chrome/browser/profiles/Profile;)J

    move-result-wide v0

    iput-wide v0, p0, Lorg/chromium/chrome/browser/omnibox/AutocompleteController;->mNativeAutocompleteControllerAndroid:J

    goto :goto_0
.end method

.method public start(Lorg/chromium/chrome/browser/profiles/Profile;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 11

    .prologue
    const/4 v8, 0x0

    .line 73
    if-eqz p1, :cond_0

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 81
    :cond_0
    :goto_0
    return-void

    .line 75
    :cond_1
    invoke-virtual {p0, p1}, Lorg/chromium/chrome/browser/omnibox/AutocompleteController;->nativeInit(Lorg/chromium/chrome/browser/profiles/Profile;)J

    move-result-wide v0

    iput-wide v0, p0, Lorg/chromium/chrome/browser/omnibox/AutocompleteController;->mNativeAutocompleteControllerAndroid:J

    .line 77
    iget-wide v0, p0, Lorg/chromium/chrome/browser/omnibox/AutocompleteController;->mNativeAutocompleteControllerAndroid:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 78
    iget-wide v2, p0, Lorg/chromium/chrome/browser/omnibox/AutocompleteController;->mNativeAutocompleteControllerAndroid:J

    const/4 v5, 0x0

    const/4 v10, 0x1

    move-object v1, p0

    move-object v4, p3

    move-object v6, p2

    move v7, p4

    move v9, v8

    invoke-direct/range {v1 .. v10}, Lorg/chromium/chrome/browser/omnibox/AutocompleteController;->nativeStart(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZZZ)V

    goto :goto_0
.end method

.method public startZeroSuggest(Lorg/chromium/chrome/browser/profiles/Profile;Ljava/lang/String;Ljava/lang/String;ZZ)V
    .locals 8

    .prologue
    .line 112
    if-eqz p1, :cond_0

    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 118
    :cond_0
    :goto_0
    return-void

    .line 113
    :cond_1
    invoke-virtual {p0, p1}, Lorg/chromium/chrome/browser/omnibox/AutocompleteController;->nativeInit(Lorg/chromium/chrome/browser/profiles/Profile;)J

    move-result-wide v0

    iput-wide v0, p0, Lorg/chromium/chrome/browser/omnibox/AutocompleteController;->mNativeAutocompleteControllerAndroid:J

    .line 114
    iget-wide v0, p0, Lorg/chromium/chrome/browser/omnibox/AutocompleteController;->mNativeAutocompleteControllerAndroid:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 115
    iget-wide v2, p0, Lorg/chromium/chrome/browser/omnibox/AutocompleteController;->mNativeAutocompleteControllerAndroid:J

    move-object v1, p0

    move-object v4, p2

    move-object v5, p3

    move v6, p4

    move v7, p5

    invoke-direct/range {v1 .. v7}, Lorg/chromium/chrome/browser/omnibox/AutocompleteController;->nativeStartZeroSuggest(JLjava/lang/String;Ljava/lang/String;ZZ)V

    goto :goto_0
.end method

.method public stop(Z)V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 132
    iput-wide v2, p0, Lorg/chromium/chrome/browser/omnibox/AutocompleteController;->mCurrentNativeAutocompleteResult:J

    .line 133
    iget-wide v0, p0, Lorg/chromium/chrome/browser/omnibox/AutocompleteController;->mNativeAutocompleteControllerAndroid:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 134
    iget-wide v0, p0, Lorg/chromium/chrome/browser/omnibox/AutocompleteController;->mNativeAutocompleteControllerAndroid:J

    invoke-direct {p0, v0, v1, p1}, Lorg/chromium/chrome/browser/omnibox/AutocompleteController;->nativeStop(JZ)V

    .line 136
    :cond_0
    return-void
.end method

.method public updateMatchDestinationUrlWithQueryFormulationTime(IJ)Ljava/lang/String;
    .locals 6

    .prologue
    .line 239
    iget-wide v1, p0, Lorg/chromium/chrome/browser/omnibox/AutocompleteController;->mNativeAutocompleteControllerAndroid:J

    move-object v0, p0

    move v3, p1

    move-wide v4, p2

    invoke-direct/range {v0 .. v5}, Lorg/chromium/chrome/browser/omnibox/AutocompleteController;->nativeUpdateMatchDestinationURLWithQueryFormulationTime(JIJ)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
