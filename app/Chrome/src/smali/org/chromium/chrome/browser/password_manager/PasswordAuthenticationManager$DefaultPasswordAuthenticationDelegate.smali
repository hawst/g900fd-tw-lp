.class Lorg/chromium/chrome/browser/password_manager/PasswordAuthenticationManager$DefaultPasswordAuthenticationDelegate;
.super Ljava/lang/Object;
.source "PasswordAuthenticationManager.java"

# interfaces
.implements Lorg/chromium/chrome/browser/password_manager/PasswordAuthenticationManager$PasswordAuthenticationDelegate;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 67
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lorg/chromium/chrome/browser/password_manager/PasswordAuthenticationManager$1;)V
    .locals 0

    .prologue
    .line 67
    invoke-direct {p0}, Lorg/chromium/chrome/browser/password_manager/PasswordAuthenticationManager$DefaultPasswordAuthenticationDelegate;-><init>()V

    return-void
.end method


# virtual methods
.method public getPasswordProtectionString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 81
    const-string/jumbo v0, ""

    return-object v0
.end method

.method public isPasswordAuthenticationEnabled()Z
    .locals 1

    .prologue
    .line 71
    const/4 v0, 0x0

    return v0
.end method

.method public requestAuthentication(Lorg/chromium/chrome/browser/Tab;Lorg/chromium/chrome/browser/password_manager/PasswordAuthenticationManager$PasswordAuthenticationCallback;)V
    .locals 1

    .prologue
    .line 76
    const/4 v0, 0x1

    invoke-virtual {p2, v0}, Lorg/chromium/chrome/browser/password_manager/PasswordAuthenticationManager$PasswordAuthenticationCallback;->onResult(Z)V

    .line 77
    return-void
.end method
