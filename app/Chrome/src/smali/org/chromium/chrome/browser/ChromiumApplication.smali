.class public abstract Lorg/chromium/chrome/browser/ChromiumApplication;
.super Lorg/chromium/content/app/ContentApplication;
.source "ChromiumApplication.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Lorg/chromium/content/app/ContentApplication;-><init>()V

    return-void
.end method


# virtual methods
.method protected abstract areParentalControlsEnabled()Z
.end method

.method protected abstract getPKCS11AuthenticationManager()Lorg/chromium/chrome/browser/PKCS11AuthenticationManager;
.end method

.method protected abstract openClearBrowsingData(Lorg/chromium/chrome/browser/Tab;)V
.end method

.method protected abstract openProtectedContentSettings()V
.end method

.method protected abstract showAutofillSettings()V
.end method

.method protected abstract showSyncSettings()V
.end method

.method protected abstract showTermsOfServiceDialog()V
.end method
