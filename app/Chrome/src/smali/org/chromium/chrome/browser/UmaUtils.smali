.class public Lorg/chromium/chrome/browser/UmaUtils;
.super Ljava/lang/Object;
.source "UmaUtils.java"


# static fields
.field private static sApplicationStartWallClockMs:J


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static getMainEntryPointTime()J
    .locals 2

    .prologue
    .line 30
    sget-wide v0, Lorg/chromium/chrome/browser/UmaUtils;->sApplicationStartWallClockMs:J

    return-wide v0
.end method

.method public static recordMainEntryPointTime()V
    .locals 2

    .prologue
    .line 25
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    sput-wide v0, Lorg/chromium/chrome/browser/UmaUtils;->sApplicationStartWallClockMs:J

    .line 26
    return-void
.end method
