.class public Lorg/chromium/chrome/browser/JavascriptAppModalDialog;
.super Ljava/lang/Object;
.source "JavascriptAppModalDialog.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private mDialog:Landroid/app/AlertDialog;

.field private final mMessage:Ljava/lang/String;

.field private mNativeDialogPointer:J

.field private mPromptTextView:Landroid/widget/TextView;

.field private final mShouldShowSuppressCheckBox:Z

.field private mSuppressCheckBox:Landroid/widget/CheckBox;

.field private final mTitle:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 24
    const-class v0, Lorg/chromium/chrome/browser/JavascriptAppModalDialog;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/chromium/chrome/browser/JavascriptAppModalDialog;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private constructor <init>(Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    iput-object p1, p0, Lorg/chromium/chrome/browser/JavascriptAppModalDialog;->mTitle:Ljava/lang/String;

    .line 38
    iput-object p2, p0, Lorg/chromium/chrome/browser/JavascriptAppModalDialog;->mMessage:Ljava/lang/String;

    .line 39
    iput-boolean p3, p0, Lorg/chromium/chrome/browser/JavascriptAppModalDialog;->mShouldShowSuppressCheckBox:Z

    .line 40
    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;Ljava/lang/String;ZLorg/chromium/chrome/browser/JavascriptAppModalDialog$1;)V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0, p1, p2, p3}, Lorg/chromium/chrome/browser/JavascriptAppModalDialog;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    return-void
.end method

.method public static createAlertDialog(Ljava/lang/String;Ljava/lang/String;Z)Lorg/chromium/chrome/browser/JavascriptAppModalDialog;
    .locals 1

    .prologue
    .line 45
    new-instance v0, Lorg/chromium/chrome/browser/JavascriptAppModalDialog$JavascriptAppAlertDialog;

    invoke-direct {v0, p0, p1, p2}, Lorg/chromium/chrome/browser/JavascriptAppModalDialog$JavascriptAppAlertDialog;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    return-object v0
.end method

.method public static createBeforeUnloadDialog(Ljava/lang/String;Ljava/lang/String;ZZ)Lorg/chromium/chrome/browser/JavascriptAppModalDialog;
    .locals 1

    .prologue
    .line 57
    new-instance v0, Lorg/chromium/chrome/browser/JavascriptAppModalDialog$JavascriptAppBeforeUnloadDialog;

    invoke-direct {v0, p0, p1, p2, p3}, Lorg/chromium/chrome/browser/JavascriptAppModalDialog$JavascriptAppBeforeUnloadDialog;-><init>(Ljava/lang/String;Ljava/lang/String;ZZ)V

    return-object v0
.end method

.method public static createConfirmDialog(Ljava/lang/String;Ljava/lang/String;Z)Lorg/chromium/chrome/browser/JavascriptAppModalDialog;
    .locals 1

    .prologue
    .line 51
    new-instance v0, Lorg/chromium/chrome/browser/JavascriptAppModalDialog$JavascriptAppConfirmDialog;

    invoke-direct {v0, p0, p1, p2}, Lorg/chromium/chrome/browser/JavascriptAppModalDialog$JavascriptAppConfirmDialog;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    return-object v0
.end method

.method public static createPromptDialog(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)Lorg/chromium/chrome/browser/JavascriptAppModalDialog;
    .locals 1

    .prologue
    .line 64
    new-instance v0, Lorg/chromium/chrome/browser/JavascriptAppModalDialog$JavascriptAppPromptDialog;

    invoke-direct {v0, p0, p1, p2, p3}, Lorg/chromium/chrome/browser/JavascriptAppModalDialog$JavascriptAppPromptDialog;-><init>(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)V

    return-object v0
.end method

.method public static getCurrentDialogForTest()Lorg/chromium/chrome/browser/JavascriptAppModalDialog;
    .locals 1

    .prologue
    .line 136
    invoke-static {}, Lorg/chromium/chrome/browser/JavascriptAppModalDialog;->nativeGetCurrentModalDialog()Lorg/chromium/chrome/browser/JavascriptAppModalDialog;

    move-result-object v0

    return-object v0
.end method

.method private native nativeDidAcceptAppModalDialog(JLjava/lang/String;Z)V
.end method

.method private native nativeDidCancelAppModalDialog(JZ)V
.end method

.method private static native nativeGetCurrentModalDialog()Lorg/chromium/chrome/browser/JavascriptAppModalDialog;
.end method


# virtual methods
.method public cancel(Z)V
    .locals 4

    .prologue
    .line 196
    iget-wide v0, p0, Lorg/chromium/chrome/browser/JavascriptAppModalDialog;->mNativeDialogPointer:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 197
    iget-wide v0, p0, Lorg/chromium/chrome/browser/JavascriptAppModalDialog;->mNativeDialogPointer:J

    invoke-direct {p0, v0, v1, p1}, Lorg/chromium/chrome/browser/JavascriptAppModalDialog;->nativeDidCancelAppModalDialog(JZ)V

    .line 199
    :cond_0
    return-void
.end method

.method public confirm(Ljava/lang/String;Z)V
    .locals 4

    .prologue
    .line 190
    iget-wide v0, p0, Lorg/chromium/chrome/browser/JavascriptAppModalDialog;->mNativeDialogPointer:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 191
    iget-wide v0, p0, Lorg/chromium/chrome/browser/JavascriptAppModalDialog;->mNativeDialogPointer:J

    invoke-direct {p0, v0, v1, p1, p2}, Lorg/chromium/chrome/browser/JavascriptAppModalDialog;->nativeDidAcceptAppModalDialog(JLjava/lang/String;Z)V

    .line 193
    :cond_0
    return-void
.end method

.method dismiss()V
    .locals 2

    .prologue
    .line 127
    iget-object v0, p0, Lorg/chromium/chrome/browser/JavascriptAppModalDialog;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 128
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lorg/chromium/chrome/browser/JavascriptAppModalDialog;->mNativeDialogPointer:J

    .line 129
    return-void
.end method

.method public getDialogForTest()Landroid/app/AlertDialog;
    .locals 1

    .prologue
    .line 145
    iget-object v0, p0, Lorg/chromium/chrome/browser/JavascriptAppModalDialog;->mDialog:Landroid/app/AlertDialog;

    return-object v0
.end method

.method public getNegativeButtonText()I
    .locals 1

    .prologue
    .line 162
    const/4 v0, -0x1

    return v0
.end method

.method public getPositiveButtonText()I
    .locals 1

    .prologue
    .line 154
    const/4 v0, -0x1

    return v0
.end method

.method public hasNegativeButton()Z
    .locals 1

    .prologue
    .line 158
    const/4 v0, 0x0

    return v0
.end method

.method public hasPositiveButton()Z
    .locals 1

    .prologue
    .line 150
    const/4 v0, 0x0

    return v0
.end method

.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3

    .prologue
    .line 113
    packed-switch p2, :pswitch_data_0

    .line 121
    const-string/jumbo v0, "JavascriptAppModalDialog"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "Unexpected button pressed in dialog: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 123
    :goto_0
    return-void

    .line 115
    :pswitch_0
    invoke-virtual {p0}, Lorg/chromium/chrome/browser/JavascriptAppModalDialog;->onPositiveButtonClicked()V

    goto :goto_0

    .line 118
    :pswitch_1
    invoke-virtual {p0}, Lorg/chromium/chrome/browser/JavascriptAppModalDialog;->onNegativeButtonClicked()V

    goto :goto_0

    .line 113
    :pswitch_data_0
    .packed-switch -0x2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public onNegativeButtonClicked()V
    .locals 1

    .prologue
    .line 171
    iget-object v0, p0, Lorg/chromium/chrome/browser/JavascriptAppModalDialog;->mSuppressCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    invoke-virtual {p0, v0}, Lorg/chromium/chrome/browser/JavascriptAppModalDialog;->cancel(Z)V

    .line 172
    iget-object v0, p0, Lorg/chromium/chrome/browser/JavascriptAppModalDialog;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 173
    return-void
.end method

.method public onPositiveButtonClicked()V
    .locals 2

    .prologue
    .line 166
    iget-object v0, p0, Lorg/chromium/chrome/browser/JavascriptAppModalDialog;->mPromptTextView:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lorg/chromium/chrome/browser/JavascriptAppModalDialog;->mSuppressCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v1

    invoke-virtual {p0, v0, v1}, Lorg/chromium/chrome/browser/JavascriptAppModalDialog;->confirm(Ljava/lang/String;Z)V

    .line 167
    iget-object v0, p0, Lorg/chromium/chrome/browser/JavascriptAppModalDialog;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 168
    return-void
.end method

.method prepare(Landroid/view/ViewGroup;)V
    .locals 3

    .prologue
    const/16 v1, 0x8

    .line 177
    sget v0, Lorg/chromium/chrome/R$id;->suppress_js_modal_dialogs:I

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iget-boolean v0, p0, Lorg/chromium/chrome/browser/JavascriptAppModalDialog;->mShouldShowSuppressCheckBox:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v2, v0}, Landroid/view/View;->setVisibility(I)V

    .line 182
    iget-object v0, p0, Lorg/chromium/chrome/browser/JavascriptAppModalDialog;->mMessage:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 183
    sget v0, Lorg/chromium/chrome/R$id;->js_modal_dialog_scroll_view:I

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 187
    :goto_1
    return-void

    :cond_0
    move v0, v1

    .line 177
    goto :goto_0

    .line 185
    :cond_1
    sget v0, Lorg/chromium/chrome/R$id;->js_modal_dialog_message:I

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v1, p0, Lorg/chromium/chrome/browser/JavascriptAppModalDialog;->mMessage:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method

.method showJavascriptAppModalDialog(Lorg/chromium/ui/base/WindowAndroid;J)V
    .locals 6

    .prologue
    const/4 v4, 0x0

    .line 70
    sget-boolean v0, Lorg/chromium/chrome/browser/JavascriptAppModalDialog;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 71
    :cond_0
    invoke-virtual {p1}, Lorg/chromium/ui/base/WindowAndroid;->getActivity()Ljava/lang/ref/WeakReference;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    .line 73
    if-nez v0, :cond_1

    .line 74
    invoke-direct {p0, p2, p3, v4}, Lorg/chromium/chrome/browser/JavascriptAppModalDialog;->nativeDidCancelAppModalDialog(JZ)V

    .line 109
    :goto_0
    return-void

    .line 79
    :cond_1
    iput-wide p2, p0, Lorg/chromium/chrome/browser/JavascriptAppModalDialog;->mNativeDialogPointer:J

    .line 81
    const-string/jumbo v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/LayoutInflater;

    .line 84
    sget v2, Lorg/chromium/chrome/R$layout;->js_modal_dialog:I

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    .line 85
    sget v2, Lorg/chromium/chrome/R$id;->suppress_js_modal_dialogs:I

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/CheckBox;

    iput-object v2, p0, Lorg/chromium/chrome/browser/JavascriptAppModalDialog;->mSuppressCheckBox:Landroid/widget/CheckBox;

    .line 86
    sget v2, Lorg/chromium/chrome/R$id;->js_modal_dialog_prompt:I

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lorg/chromium/chrome/browser/JavascriptAppModalDialog;->mPromptTextView:Landroid/widget/TextView;

    .line 88
    invoke-virtual {p0, v1}, Lorg/chromium/chrome/browser/JavascriptAppModalDialog;->prepare(Landroid/view/ViewGroup;)V

    .line 90
    new-instance v2, Landroid/app/AlertDialog$Builder;

    invoke-direct {v2, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v2, v1}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    iget-object v1, p0, Lorg/chromium/chrome/browser/JavascriptAppModalDialog;->mTitle:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v1, Lorg/chromium/chrome/browser/JavascriptAppModalDialog$1;

    invoke-direct {v1, p0}, Lorg/chromium/chrome/browser/JavascriptAppModalDialog$1;-><init>(Lorg/chromium/chrome/browser/JavascriptAppModalDialog;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 99
    invoke-virtual {p0}, Lorg/chromium/chrome/browser/JavascriptAppModalDialog;->hasPositiveButton()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 100
    invoke-virtual {p0}, Lorg/chromium/chrome/browser/JavascriptAppModalDialog;->getPositiveButtonText()I

    move-result v1

    invoke-virtual {v0, v1, p0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 102
    :cond_2
    invoke-virtual {p0}, Lorg/chromium/chrome/browser/JavascriptAppModalDialog;->hasNegativeButton()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 103
    invoke-virtual {p0}, Lorg/chromium/chrome/browser/JavascriptAppModalDialog;->getNegativeButtonText()I

    move-result v1

    invoke-virtual {v0, v1, p0}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 106
    :cond_3
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lorg/chromium/chrome/browser/JavascriptAppModalDialog;->mDialog:Landroid/app/AlertDialog;

    .line 107
    iget-object v0, p0, Lorg/chromium/chrome/browser/JavascriptAppModalDialog;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0, v4}, Landroid/app/AlertDialog;->setCanceledOnTouchOutside(Z)V

    .line 108
    iget-object v0, p0, Lorg/chromium/chrome/browser/JavascriptAppModalDialog;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    goto :goto_0
.end method
