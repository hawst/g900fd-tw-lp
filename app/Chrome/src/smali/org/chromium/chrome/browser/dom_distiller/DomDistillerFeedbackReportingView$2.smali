.class Lorg/chromium/chrome/browser/dom_distiller/DomDistillerFeedbackReportingView$2;
.super Ljava/lang/Object;
.source "DomDistillerFeedbackReportingView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic this$0:Lorg/chromium/chrome/browser/dom_distiller/DomDistillerFeedbackReportingView;


# direct methods
.method constructor <init>(Lorg/chromium/chrome/browser/dom_distiller/DomDistillerFeedbackReportingView;)V
    .locals 0

    .prologue
    .line 98
    iput-object p1, p0, Lorg/chromium/chrome/browser/dom_distiller/DomDistillerFeedbackReportingView$2;->this$0:Lorg/chromium/chrome/browser/dom_distiller/DomDistillerFeedbackReportingView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 101
    iget-object v0, p0, Lorg/chromium/chrome/browser/dom_distiller/DomDistillerFeedbackReportingView$2;->this$0:Lorg/chromium/chrome/browser/dom_distiller/DomDistillerFeedbackReportingView;

    # getter for: Lorg/chromium/chrome/browser/dom_distiller/DomDistillerFeedbackReportingView;->mSelectionMade:Z
    invoke-static {v0}, Lorg/chromium/chrome/browser/dom_distiller/DomDistillerFeedbackReportingView;->access$000(Lorg/chromium/chrome/browser/dom_distiller/DomDistillerFeedbackReportingView;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 108
    :cond_0
    :goto_0
    return-void

    .line 102
    :cond_1
    iget-object v0, p0, Lorg/chromium/chrome/browser/dom_distiller/DomDistillerFeedbackReportingView$2;->this$0:Lorg/chromium/chrome/browser/dom_distiller/DomDistillerFeedbackReportingView;

    const/4 v1, 0x1

    # setter for: Lorg/chromium/chrome/browser/dom_distiller/DomDistillerFeedbackReportingView;->mSelectionMade:Z
    invoke-static {v0, v1}, Lorg/chromium/chrome/browser/dom_distiller/DomDistillerFeedbackReportingView;->access$002(Lorg/chromium/chrome/browser/dom_distiller/DomDistillerFeedbackReportingView;Z)Z

    .line 103
    iget-object v0, p0, Lorg/chromium/chrome/browser/dom_distiller/DomDistillerFeedbackReportingView$2;->this$0:Lorg/chromium/chrome/browser/dom_distiller/DomDistillerFeedbackReportingView;

    # getter for: Lorg/chromium/chrome/browser/dom_distiller/DomDistillerFeedbackReportingView;->mYesButton:Landroid/widget/ImageButton;
    invoke-static {v0}, Lorg/chromium/chrome/browser/dom_distiller/DomDistillerFeedbackReportingView;->access$400(Lorg/chromium/chrome/browser/dom_distiller/DomDistillerFeedbackReportingView;)Landroid/widget/ImageButton;

    move-result-object v0

    sget v1, Lorg/chromium/chrome/R$drawable;->distillation_quality_answer_yes_pressed:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 104
    iget-object v0, p0, Lorg/chromium/chrome/browser/dom_distiller/DomDistillerFeedbackReportingView$2;->this$0:Lorg/chromium/chrome/browser/dom_distiller/DomDistillerFeedbackReportingView;

    # invokes: Lorg/chromium/chrome/browser/dom_distiller/DomDistillerFeedbackReportingView;->disableUI()V
    invoke-static {v0}, Lorg/chromium/chrome/browser/dom_distiller/DomDistillerFeedbackReportingView;->access$200(Lorg/chromium/chrome/browser/dom_distiller/DomDistillerFeedbackReportingView;)V

    .line 105
    iget-object v0, p0, Lorg/chromium/chrome/browser/dom_distiller/DomDistillerFeedbackReportingView$2;->this$0:Lorg/chromium/chrome/browser/dom_distiller/DomDistillerFeedbackReportingView;

    # getter for: Lorg/chromium/chrome/browser/dom_distiller/DomDistillerFeedbackReportingView;->mFeedbackObserver:Lorg/chromium/chrome/browser/dom_distiller/DomDistillerFeedbackReportingView$FeedbackObserver;
    invoke-static {v0}, Lorg/chromium/chrome/browser/dom_distiller/DomDistillerFeedbackReportingView;->access$300(Lorg/chromium/chrome/browser/dom_distiller/DomDistillerFeedbackReportingView;)Lorg/chromium/chrome/browser/dom_distiller/DomDistillerFeedbackReportingView$FeedbackObserver;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 106
    iget-object v0, p0, Lorg/chromium/chrome/browser/dom_distiller/DomDistillerFeedbackReportingView$2;->this$0:Lorg/chromium/chrome/browser/dom_distiller/DomDistillerFeedbackReportingView;

    # getter for: Lorg/chromium/chrome/browser/dom_distiller/DomDistillerFeedbackReportingView;->mFeedbackObserver:Lorg/chromium/chrome/browser/dom_distiller/DomDistillerFeedbackReportingView$FeedbackObserver;
    invoke-static {v0}, Lorg/chromium/chrome/browser/dom_distiller/DomDistillerFeedbackReportingView;->access$300(Lorg/chromium/chrome/browser/dom_distiller/DomDistillerFeedbackReportingView;)Lorg/chromium/chrome/browser/dom_distiller/DomDistillerFeedbackReportingView$FeedbackObserver;

    move-result-object v0

    iget-object v1, p0, Lorg/chromium/chrome/browser/dom_distiller/DomDistillerFeedbackReportingView$2;->this$0:Lorg/chromium/chrome/browser/dom_distiller/DomDistillerFeedbackReportingView;

    invoke-interface {v0, v1}, Lorg/chromium/chrome/browser/dom_distiller/DomDistillerFeedbackReportingView$FeedbackObserver;->onYesPressed(Lorg/chromium/chrome/browser/dom_distiller/DomDistillerFeedbackReportingView;)V

    goto :goto_0
.end method
