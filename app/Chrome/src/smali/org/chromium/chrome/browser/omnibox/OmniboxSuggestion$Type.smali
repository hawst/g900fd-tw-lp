.class public final enum Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;
.super Ljava/lang/Enum;
.source "OmniboxSuggestion.java"


# static fields
.field private static final synthetic $VALUES:[Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;

.field public static final enum HISTORY_BODY:Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;

.field public static final enum HISTORY_KEYWORD:Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;

.field public static final enum HISTORY_TITLE:Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;

.field public static final enum HISTORY_URL:Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;

.field public static final enum NAVSUGGEST:Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;

.field public static final enum OPEN_HISTORY_PAGE:Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;

.field public static final enum SEARCH_HISTORY:Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;

.field public static final enum SEARCH_OTHER_ENGINE:Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;

.field public static final enum SEARCH_SUGGEST:Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;

.field public static final enum SEARCH_SUGGEST_ANSWER:Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;

.field public static final enum SEARCH_SUGGEST_ENTITY:Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;

.field public static final enum SEARCH_SUGGEST_INFINITE:Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;

.field public static final enum SEARCH_SUGGEST_PERSONALIZED:Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;

.field public static final enum SEARCH_SUGGEST_PROFILE:Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;

.field public static final enum SEARCH_WHAT_YOU_TYPED:Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;

.field public static final enum URL_WHAT_YOU_TYPED:Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;

.field public static final enum VOICE_SUGGEST:Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;


# instance fields
.field private final mNativeType:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 37
    new-instance v0, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;

    const-string/jumbo v1, "VOICE_SUGGEST"

    const/16 v2, -0x64

    invoke-direct {v0, v1, v4, v2}, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;->VOICE_SUGGEST:Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;

    .line 39
    new-instance v0, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;

    const-string/jumbo v1, "URL_WHAT_YOU_TYPED"

    invoke-direct {v0, v1, v5, v4}, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;->URL_WHAT_YOU_TYPED:Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;

    .line 40
    new-instance v0, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;

    const-string/jumbo v1, "HISTORY_URL"

    invoke-direct {v0, v1, v6, v5}, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;->HISTORY_URL:Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;

    .line 41
    new-instance v0, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;

    const-string/jumbo v1, "HISTORY_TITLE"

    invoke-direct {v0, v1, v7, v6}, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;->HISTORY_TITLE:Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;

    .line 42
    new-instance v0, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;

    const-string/jumbo v1, "HISTORY_BODY"

    invoke-direct {v0, v1, v8, v7}, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;->HISTORY_BODY:Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;

    .line 43
    new-instance v0, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;

    const-string/jumbo v1, "HISTORY_KEYWORD"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2, v8}, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;->HISTORY_KEYWORD:Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;

    .line 44
    new-instance v0, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;

    const-string/jumbo v1, "NAVSUGGEST"

    const/4 v2, 0x6

    const/4 v3, 0x5

    invoke-direct {v0, v1, v2, v3}, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;->NAVSUGGEST:Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;

    .line 45
    new-instance v0, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;

    const-string/jumbo v1, "SEARCH_WHAT_YOU_TYPED"

    const/4 v2, 0x7

    const/4 v3, 0x6

    invoke-direct {v0, v1, v2, v3}, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;->SEARCH_WHAT_YOU_TYPED:Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;

    .line 47
    new-instance v0, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;

    const-string/jumbo v1, "SEARCH_HISTORY"

    const/16 v2, 0x8

    const/4 v3, 0x7

    invoke-direct {v0, v1, v2, v3}, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;->SEARCH_HISTORY:Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;

    .line 49
    new-instance v0, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;

    const-string/jumbo v1, "SEARCH_SUGGEST"

    const/16 v2, 0x9

    const/16 v3, 0x8

    invoke-direct {v0, v1, v2, v3}, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;->SEARCH_SUGGEST:Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;

    .line 50
    new-instance v0, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;

    const-string/jumbo v1, "SEARCH_SUGGEST_ENTITY"

    const/16 v2, 0xa

    const/16 v3, 0x9

    invoke-direct {v0, v1, v2, v3}, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;->SEARCH_SUGGEST_ENTITY:Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;

    .line 51
    new-instance v0, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;

    const-string/jumbo v1, "SEARCH_SUGGEST_INFINITE"

    const/16 v2, 0xb

    const/16 v3, 0xa

    invoke-direct {v0, v1, v2, v3}, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;->SEARCH_SUGGEST_INFINITE:Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;

    .line 53
    new-instance v0, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;

    const-string/jumbo v1, "SEARCH_SUGGEST_PERSONALIZED"

    const/16 v2, 0xc

    const/16 v3, 0xb

    invoke-direct {v0, v1, v2, v3}, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;->SEARCH_SUGGEST_PERSONALIZED:Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;

    .line 54
    new-instance v0, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;

    const-string/jumbo v1, "SEARCH_SUGGEST_PROFILE"

    const/16 v2, 0xd

    const/16 v3, 0xc

    invoke-direct {v0, v1, v2, v3}, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;->SEARCH_SUGGEST_PROFILE:Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;

    .line 56
    new-instance v0, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;

    const-string/jumbo v1, "SEARCH_OTHER_ENGINE"

    const/16 v2, 0xe

    const/16 v3, 0xd

    invoke-direct {v0, v1, v2, v3}, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;->SEARCH_OTHER_ENGINE:Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;

    .line 57
    new-instance v0, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;

    const-string/jumbo v1, "OPEN_HISTORY_PAGE"

    const/16 v2, 0xf

    const/16 v3, 0x11

    invoke-direct {v0, v1, v2, v3}, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;->OPEN_HISTORY_PAGE:Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;

    .line 59
    new-instance v0, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;

    const-string/jumbo v1, "SEARCH_SUGGEST_ANSWER"

    const/16 v2, 0x10

    const/16 v3, 0x12

    invoke-direct {v0, v1, v2, v3}, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;->SEARCH_SUGGEST_ANSWER:Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;

    .line 36
    const/16 v0, 0x11

    new-array v0, v0, [Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;

    sget-object v1, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;->VOICE_SUGGEST:Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;

    aput-object v1, v0, v4

    sget-object v1, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;->URL_WHAT_YOU_TYPED:Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;

    aput-object v1, v0, v5

    sget-object v1, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;->HISTORY_URL:Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;

    aput-object v1, v0, v6

    sget-object v1, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;->HISTORY_TITLE:Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;

    aput-object v1, v0, v7

    sget-object v1, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;->HISTORY_BODY:Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;->HISTORY_KEYWORD:Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;->NAVSUGGEST:Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;->SEARCH_WHAT_YOU_TYPED:Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;->SEARCH_HISTORY:Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;->SEARCH_SUGGEST:Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;->SEARCH_SUGGEST_ENTITY:Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;->SEARCH_SUGGEST_INFINITE:Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;->SEARCH_SUGGEST_PERSONALIZED:Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;->SEARCH_SUGGEST_PROFILE:Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;->SEARCH_OTHER_ENGINE:Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;->OPEN_HISTORY_PAGE:Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;->SEARCH_SUGGEST_ANSWER:Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;

    aput-object v2, v0, v1

    sput-object v0, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;->$VALUES:[Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0

    .prologue
    .line 63
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 64
    iput p3, p0, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;->mNativeType:I

    .line 65
    return-void
.end method

.method static synthetic access$000(Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;)I
    .locals 1

    .prologue
    .line 36
    iget v0, p0, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;->mNativeType:I

    return v0
.end method

.method static getTypeFromNativeType(I)Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;
    .locals 5

    .prologue
    .line 68
    invoke-static {}, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;->values()[Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v0, v2, v1

    .line 69
    iget v4, v0, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;->mNativeType:I

    if-ne v4, p0, :cond_0

    .line 72
    :goto_1
    return-object v0

    .line 68
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 72
    :cond_1
    sget-object v0, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;->URL_WHAT_YOU_TYPED:Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;

    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;
    .locals 1

    .prologue
    .line 36
    const-class v0, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;

    return-object v0
.end method

.method public static values()[Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;
    .locals 1

    .prologue
    .line 36
    sget-object v0, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;->$VALUES:[Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;

    invoke-virtual {v0}, [Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;

    return-object v0
.end method


# virtual methods
.method public final isHistoryUrl()Z
    .locals 1

    .prologue
    .line 76
    sget-object v0, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;->HISTORY_URL:Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;

    if-eq p0, v0, :cond_0

    sget-object v0, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;->HISTORY_TITLE:Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;

    if-eq p0, v0, :cond_0

    sget-object v0, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;->HISTORY_BODY:Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;

    if-eq p0, v0, :cond_0

    sget-object v0, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;->HISTORY_KEYWORD:Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isUrl()Z
    .locals 1

    .prologue
    .line 81
    sget-object v0, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;->URL_WHAT_YOU_TYPED:Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;

    if-eq p0, v0, :cond_0

    invoke-virtual {p0}, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;->isHistoryUrl()Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;->NAVSUGGEST:Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final nativeType()I
    .locals 1

    .prologue
    .line 88
    iget v0, p0, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;->mNativeType:I

    return v0
.end method
