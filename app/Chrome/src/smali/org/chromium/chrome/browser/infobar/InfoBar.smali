.class public abstract Lorg/chromium/chrome/browser/infobar/InfoBar;
.super Ljava/lang/Object;
.source "InfoBar.java"

# interfaces
.implements Lorg/chromium/chrome/browser/infobar/InfoBarView;


# static fields
.field static final synthetic $assertionsDisabled:Z

.field public static final ACTION_TYPE_CANCEL:I = 0x2

.field public static final ACTION_TYPE_NONE:I = 0x0

.field public static final ACTION_TYPE_OK:I = 0x1

.field public static final ACTION_TYPE_TRANSLATE:I = 0x3

.field public static final ACTION_TYPE_TRANSLATE_SHOW_ORIGINAL:I = 0x4

.field private static sIdCounter:I


# instance fields
.field private mContainer:Lorg/chromium/chrome/browser/infobar/InfoBarContainer;

.field private mContentView:Lorg/chromium/chrome/browser/infobar/ContentWrapperView;

.field private mContext:Landroid/content/Context;

.field private mControlsEnabled:Z

.field private mExpireOnNavigation:Z

.field private final mIconDrawableId:I

.field private final mId:I

.field private mIsDismissed:Z

.field private mListener:Lorg/chromium/chrome/browser/infobar/InfoBarListeners$Dismiss;

.field private final mMessage:Ljava/lang/CharSequence;

.field protected mNativeInfoBarPtr:J


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 19
    const-class v0, Lorg/chromium/chrome/browser/infobar/InfoBar;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/chromium/chrome/browser/infobar/InfoBar;->$assertionsDisabled:Z

    .line 57
    sput v1, Lorg/chromium/chrome/browser/infobar/InfoBar;->sIdCounter:I

    return-void

    :cond_0
    move v0, v1

    .line 19
    goto :goto_0
.end method

.method public constructor <init>(Lorg/chromium/chrome/browser/infobar/InfoBarListeners$Dismiss;ILjava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 67
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 68
    iput-object p1, p0, Lorg/chromium/chrome/browser/infobar/InfoBar;->mListener:Lorg/chromium/chrome/browser/infobar/InfoBarListeners$Dismiss;

    .line 69
    invoke-static {}, Lorg/chromium/chrome/browser/infobar/InfoBar;->generateId()I

    move-result v0

    iput v0, p0, Lorg/chromium/chrome/browser/infobar/InfoBar;->mId:I

    .line 70
    iput p2, p0, Lorg/chromium/chrome/browser/infobar/InfoBar;->mIconDrawableId:I

    .line 71
    iput-object p3, p0, Lorg/chromium/chrome/browser/infobar/InfoBar;->mMessage:Ljava/lang/CharSequence;

    .line 72
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/chromium/chrome/browser/infobar/InfoBar;->mExpireOnNavigation:Z

    .line 73
    return-void
.end method

.method private static generateId()I
    .locals 2

    .prologue
    .line 59
    sget v0, Lorg/chromium/chrome/browser/infobar/InfoBar;->sIdCounter:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lorg/chromium/chrome/browser/infobar/InfoBar;->sIdCounter:I

    return v0
.end method


# virtual methods
.method public areControlsEnabled()Z
    .locals 1

    .prologue
    .line 211
    iget-boolean v0, p0, Lorg/chromium/chrome/browser/infobar/InfoBar;->mControlsEnabled:Z

    return v0
.end method

.method public closeInfoBar()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 175
    iget-boolean v1, p0, Lorg/chromium/chrome/browser/infobar/InfoBar;->mIsDismissed:Z

    if-nez v1, :cond_1

    .line 176
    iput-boolean v0, p0, Lorg/chromium/chrome/browser/infobar/InfoBar;->mIsDismissed:Z

    .line 177
    iget-object v1, p0, Lorg/chromium/chrome/browser/infobar/InfoBar;->mContainer:Lorg/chromium/chrome/browser/infobar/InfoBarContainer;

    invoke-virtual {v1}, Lorg/chromium/chrome/browser/infobar/InfoBarContainer;->hasBeenDestroyed()Z

    move-result v1

    if-nez v1, :cond_0

    .line 179
    iget-object v1, p0, Lorg/chromium/chrome/browser/infobar/InfoBar;->mContainer:Lorg/chromium/chrome/browser/infobar/InfoBarContainer;

    invoke-virtual {v1, p0}, Lorg/chromium/chrome/browser/infobar/InfoBarContainer;->removeInfoBar(Lorg/chromium/chrome/browser/infobar/InfoBar;)V

    .line 183
    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public createContent(Lorg/chromium/chrome/browser/infobar/InfoBarLayout;)V
    .locals 0

    .prologue
    .line 244
    return-void
.end method

.method protected final createView()Landroid/view/View;
    .locals 4

    .prologue
    .line 153
    sget-boolean v0, Lorg/chromium/chrome/browser/infobar/InfoBar;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lorg/chromium/chrome/browser/infobar/InfoBar;->mContext:Landroid/content/Context;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 155
    :cond_0
    new-instance v0, Lorg/chromium/chrome/browser/infobar/InfoBarLayout;

    iget-object v1, p0, Lorg/chromium/chrome/browser/infobar/InfoBar;->mContext:Landroid/content/Context;

    iget v2, p0, Lorg/chromium/chrome/browser/infobar/InfoBar;->mIconDrawableId:I

    iget-object v3, p0, Lorg/chromium/chrome/browser/infobar/InfoBar;->mMessage:Ljava/lang/CharSequence;

    invoke-direct {v0, v1, p0, v2, v3}, Lorg/chromium/chrome/browser/infobar/InfoBarLayout;-><init>(Landroid/content/Context;Lorg/chromium/chrome/browser/infobar/InfoBarView;ILjava/lang/CharSequence;)V

    .line 156
    invoke-virtual {p0, v0}, Lorg/chromium/chrome/browser/infobar/InfoBar;->createContent(Lorg/chromium/chrome/browser/infobar/InfoBarLayout;)V

    .line 157
    return-object v0
.end method

.method public dismissJavaOnlyInfoBar()V
    .locals 4

    .prologue
    .line 164
    sget-boolean v0, Lorg/chromium/chrome/browser/infobar/InfoBar;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-wide v0, p0, Lorg/chromium/chrome/browser/infobar/InfoBar;->mNativeInfoBarPtr:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 165
    :cond_0
    invoke-virtual {p0}, Lorg/chromium/chrome/browser/infobar/InfoBar;->closeInfoBar()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lorg/chromium/chrome/browser/infobar/InfoBar;->mListener:Lorg/chromium/chrome/browser/infobar/InfoBarListeners$Dismiss;

    if-eqz v0, :cond_1

    .line 166
    iget-object v0, p0, Lorg/chromium/chrome/browser/infobar/InfoBar;->mListener:Lorg/chromium/chrome/browser/infobar/InfoBarListeners$Dismiss;

    invoke-interface {v0, p0}, Lorg/chromium/chrome/browser/infobar/InfoBarListeners$Dismiss;->onInfoBarDismissed(Lorg/chromium/chrome/browser/infobar/InfoBar;)V

    .line 168
    :cond_1
    return-void
.end method

.method public getContentWrapper()Lorg/chromium/chrome/browser/infobar/ContentWrapperView;
    .locals 1

    .prologue
    .line 203
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lorg/chromium/chrome/browser/infobar/InfoBar;->getContentWrapper(Z)Lorg/chromium/chrome/browser/infobar/ContentWrapperView;

    move-result-object v0

    return-object v0
.end method

.method protected getContentWrapper(Z)Lorg/chromium/chrome/browser/infobar/ContentWrapperView;
    .locals 3

    .prologue
    .line 187
    iget-object v0, p0, Lorg/chromium/chrome/browser/infobar/InfoBar;->mContentView:Lorg/chromium/chrome/browser/infobar/ContentWrapperView;

    if-nez v0, :cond_0

    if-eqz p1, :cond_0

    .line 188
    new-instance v0, Lorg/chromium/chrome/browser/infobar/ContentWrapperView;

    invoke-virtual {p0}, Lorg/chromium/chrome/browser/infobar/InfoBar;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p0}, Lorg/chromium/chrome/browser/infobar/InfoBar;->createView()Landroid/view/View;

    move-result-object v2

    invoke-direct {v0, v1, p0, v2}, Lorg/chromium/chrome/browser/infobar/ContentWrapperView;-><init>(Landroid/content/Context;Lorg/chromium/chrome/browser/infobar/InfoBar;Landroid/view/View;)V

    iput-object v0, p0, Lorg/chromium/chrome/browser/infobar/InfoBar;->mContentView:Lorg/chromium/chrome/browser/infobar/ContentWrapperView;

    .line 189
    iget-object v0, p0, Lorg/chromium/chrome/browser/infobar/InfoBar;->mContentView:Lorg/chromium/chrome/browser/infobar/ContentWrapperView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lorg/chromium/chrome/browser/infobar/ContentWrapperView;->setFocusable(Z)V

    .line 191
    :cond_0
    iget-object v0, p0, Lorg/chromium/chrome/browser/infobar/InfoBar;->mContentView:Lorg/chromium/chrome/browser/infobar/ContentWrapperView;

    return-object v0
.end method

.method protected getContext()Landroid/content/Context;
    .locals 1

    .prologue
    .line 145
    iget-object v0, p0, Lorg/chromium/chrome/browser/infobar/InfoBar;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method public getId()I
    .locals 1

    .prologue
    .line 255
    iget v0, p0, Lorg/chromium/chrome/browser/infobar/InfoBar;->mId:I

    return v0
.end method

.method protected getInfoBarContainer()Lorg/chromium/chrome/browser/infobar/InfoBarContainer;
    .locals 1

    .prologue
    .line 195
    iget-object v0, p0, Lorg/chromium/chrome/browser/infobar/InfoBar;->mContainer:Lorg/chromium/chrome/browser/infobar/InfoBarContainer;

    return-object v0
.end method

.method public getMessage()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lorg/chromium/chrome/browser/infobar/InfoBar;->mMessage:Ljava/lang/CharSequence;

    return-object v0
.end method

.method protected isDismissed()Z
    .locals 1

    .prologue
    .line 130
    iget-boolean v0, p0, Lorg/chromium/chrome/browser/infobar/InfoBar;->mIsDismissed:Z

    return v0
.end method

.method protected native nativeOnButtonClicked(JILjava/lang/String;)V
.end method

.method protected native nativeOnCloseButtonClicked(J)V
.end method

.method protected native nativeOnLinkClicked(J)V
.end method

.method public onButtonClicked(Z)V
    .locals 0

    .prologue
    .line 233
    return-void
.end method

.method public onLinkClicked()V
    .locals 4

    .prologue
    .line 237
    iget-wide v0, p0, Lorg/chromium/chrome/browser/infobar/InfoBar;->mNativeInfoBarPtr:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 238
    iget-wide v0, p0, Lorg/chromium/chrome/browser/infobar/InfoBar;->mNativeInfoBarPtr:J

    invoke-virtual {p0, v0, v1}, Lorg/chromium/chrome/browser/infobar/InfoBar;->nativeOnLinkClicked(J)V

    .line 240
    :cond_0
    return-void
.end method

.method protected replaceNativePointer(J)V
    .locals 1

    .prologue
    .line 100
    iput-wide p1, p0, Lorg/chromium/chrome/browser/infobar/InfoBar;->mNativeInfoBarPtr:J

    .line 101
    return-void
.end method

.method protected setContext(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 137
    iput-object p1, p0, Lorg/chromium/chrome/browser/infobar/InfoBar;->mContext:Landroid/content/Context;

    .line 138
    return-void
.end method

.method public setControlsEnabled(Z)V
    .locals 5

    .prologue
    .line 216
    iput-boolean p1, p0, Lorg/chromium/chrome/browser/infobar/InfoBar;->mControlsEnabled:Z

    .line 219
    iget-object v0, p0, Lorg/chromium/chrome/browser/infobar/InfoBar;->mContentView:Lorg/chromium/chrome/browser/infobar/ContentWrapperView;

    if-eqz v0, :cond_3

    .line 220
    iget-object v0, p0, Lorg/chromium/chrome/browser/infobar/InfoBar;->mContentView:Lorg/chromium/chrome/browser/infobar/ContentWrapperView;

    sget v1, Lorg/chromium/chrome/R$id;->infobar_close_button:I

    invoke-virtual {v0, v1}, Lorg/chromium/chrome/browser/infobar/ContentWrapperView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 221
    iget-object v1, p0, Lorg/chromium/chrome/browser/infobar/InfoBar;->mContentView:Lorg/chromium/chrome/browser/infobar/ContentWrapperView;

    sget v2, Lorg/chromium/chrome/R$id;->button_primary:I

    invoke-virtual {v1, v2}, Lorg/chromium/chrome/browser/infobar/ContentWrapperView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 222
    iget-object v2, p0, Lorg/chromium/chrome/browser/infobar/InfoBar;->mContentView:Lorg/chromium/chrome/browser/infobar/ContentWrapperView;

    sget v3, Lorg/chromium/chrome/R$id;->button_secondary:I

    invoke-virtual {v2, v3}, Lorg/chromium/chrome/browser/infobar/ContentWrapperView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 223
    iget-object v3, p0, Lorg/chromium/chrome/browser/infobar/InfoBar;->mContentView:Lorg/chromium/chrome/browser/infobar/ContentWrapperView;

    sget v4, Lorg/chromium/chrome/R$id;->button_tertiary:I

    invoke-virtual {v3, v4}, Lorg/chromium/chrome/browser/infobar/ContentWrapperView;->findViewById(I)Landroid/view/View;

    move-result-object v3

    .line 224
    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Landroid/view/View;->setEnabled(Z)V

    .line 225
    :cond_0
    if-eqz v1, :cond_1

    invoke-virtual {v1, p1}, Landroid/view/View;->setEnabled(Z)V

    .line 226
    :cond_1
    if-eqz v2, :cond_2

    invoke-virtual {v2, p1}, Landroid/view/View;->setEnabled(Z)V

    .line 227
    :cond_2
    if-eqz v3, :cond_3

    invoke-virtual {v3, p1}, Landroid/view/View;->setEnabled(Z)V

    .line 229
    :cond_3
    return-void
.end method

.method public setDismissedListener(Lorg/chromium/chrome/browser/infobar/InfoBarListeners$Dismiss;)V
    .locals 0

    .prologue
    .line 260
    iput-object p1, p0, Lorg/chromium/chrome/browser/infobar/InfoBar;->mListener:Lorg/chromium/chrome/browser/infobar/InfoBarListeners$Dismiss;

    .line 261
    return-void
.end method

.method public setExpireOnNavigation(Z)V
    .locals 0

    .prologue
    .line 116
    iput-boolean p1, p0, Lorg/chromium/chrome/browser/infobar/InfoBar;->mExpireOnNavigation:Z

    .line 117
    return-void
.end method

.method setInfoBarContainer(Lorg/chromium/chrome/browser/infobar/InfoBarContainer;)V
    .locals 0

    .prologue
    .line 207
    iput-object p1, p0, Lorg/chromium/chrome/browser/infobar/InfoBar;->mContainer:Lorg/chromium/chrome/browser/infobar/InfoBarContainer;

    .line 208
    return-void
.end method

.method protected setNativeInfoBar(J)V
    .locals 3

    .prologue
    .line 87
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-eqz v0, :cond_0

    .line 89
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/chromium/chrome/browser/infobar/InfoBar;->mExpireOnNavigation:Z

    .line 90
    iput-wide p1, p0, Lorg/chromium/chrome/browser/infobar/InfoBar;->mNativeInfoBarPtr:J

    .line 92
    :cond_0
    return-void
.end method

.method public final shouldExpire()Z
    .locals 4

    .prologue
    .line 111
    iget-boolean v0, p0, Lorg/chromium/chrome/browser/infobar/InfoBar;->mExpireOnNavigation:Z

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lorg/chromium/chrome/browser/infobar/InfoBar;->mNativeInfoBarPtr:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
