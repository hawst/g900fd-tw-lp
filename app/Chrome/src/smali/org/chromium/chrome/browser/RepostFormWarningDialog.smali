.class Lorg/chromium/chrome/browser/RepostFormWarningDialog;
.super Landroid/app/DialogFragment;
.source "RepostFormWarningDialog.java"


# static fields
.field static final synthetic $assertionsDisabled:Z

.field private static sCurrentDialog:Landroid/app/Dialog;


# instance fields
.field private final mCancelCallback:Ljava/lang/Runnable;

.field private final mContinueCallback:Ljava/lang/Runnable;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 19
    const-class v0, Lorg/chromium/chrome/browser/RepostFormWarningDialog;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/chromium/chrome/browser/RepostFormWarningDialog;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Ljava/lang/Runnable;Ljava/lang/Runnable;)V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    .line 27
    iput-object p1, p0, Lorg/chromium/chrome/browser/RepostFormWarningDialog;->mCancelCallback:Ljava/lang/Runnable;

    .line 28
    iput-object p2, p0, Lorg/chromium/chrome/browser/RepostFormWarningDialog;->mContinueCallback:Ljava/lang/Runnable;

    .line 29
    return-void
.end method

.method static synthetic access$000(Lorg/chromium/chrome/browser/RepostFormWarningDialog;)Ljava/lang/Runnable;
    .locals 1

    .prologue
    .line 19
    iget-object v0, p0, Lorg/chromium/chrome/browser/RepostFormWarningDialog;->mContinueCallback:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$100(Lorg/chromium/chrome/browser/RepostFormWarningDialog;)Ljava/lang/Runnable;
    .locals 1

    .prologue
    .line 19
    iget-object v0, p0, Lorg/chromium/chrome/browser/RepostFormWarningDialog;->mCancelCallback:Ljava/lang/Runnable;

    return-object v0
.end method

.method public static getCurrentDialog()Landroid/app/Dialog;
    .locals 1

    .prologue
    .line 74
    sget-object v0, Lorg/chromium/chrome/browser/RepostFormWarningDialog;->sCurrentDialog:Landroid/app/Dialog;

    return-object v0
.end method

.method private static setCurrentDialog(Landroid/app/Dialog;)V
    .locals 0

    .prologue
    .line 67
    sput-object p0, Lorg/chromium/chrome/browser/RepostFormWarningDialog;->sCurrentDialog:Landroid/app/Dialog;

    .line 68
    return-void
.end method


# virtual methods
.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 3

    .prologue
    .line 33
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lorg/chromium/chrome/browser/RepostFormWarningDialog;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    sget v1, Lorg/chromium/chrome/R$string;->http_post_warning:I

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    sget v1, Lorg/chromium/chrome/R$string;->cancel:I

    new-instance v2, Lorg/chromium/chrome/browser/RepostFormWarningDialog$2;

    invoke-direct {v2, p0}, Lorg/chromium/chrome/browser/RepostFormWarningDialog$2;-><init>(Lorg/chromium/chrome/browser/RepostFormWarningDialog;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    sget v1, Lorg/chromium/chrome/R$string;->http_post_warning_resend:I

    new-instance v2, Lorg/chromium/chrome/browser/RepostFormWarningDialog$1;

    invoke-direct {v2, p0}, Lorg/chromium/chrome/browser/RepostFormWarningDialog$1;-><init>(Lorg/chromium/chrome/browser/RepostFormWarningDialog;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 49
    sget-boolean v1, Lorg/chromium/chrome/browser/RepostFormWarningDialog;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    invoke-static {}, Lorg/chromium/chrome/browser/RepostFormWarningDialog;->getCurrentDialog()Landroid/app/Dialog;

    move-result-object v1

    if-eqz v1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 50
    :cond_0
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 51
    invoke-static {v0}, Lorg/chromium/chrome/browser/RepostFormWarningDialog;->setCurrentDialog(Landroid/app/Dialog;)V

    .line 53
    return-object v0
.end method

.method public onDismiss(Landroid/content/DialogInterface;)V
    .locals 1

    .prologue
    .line 58
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onDismiss(Landroid/content/DialogInterface;)V

    .line 59
    const/4 v0, 0x0

    sput-object v0, Lorg/chromium/chrome/browser/RepostFormWarningDialog;->sCurrentDialog:Landroid/app/Dialog;

    .line 60
    return-void
.end method
