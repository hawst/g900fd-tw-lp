.class public Lorg/chromium/chrome/browser/invalidation/InvalidationController;
.super Ljava/lang/Object;
.source "InvalidationController.java"

# interfaces
.implements Lorg/chromium/base/ApplicationStatus$ApplicationStateListener;


# static fields
.field private static final LOCK:Ljava/lang/Object;

.field private static sInstance:Lorg/chromium/chrome/browser/invalidation/InvalidationController;


# instance fields
.field private final mContext:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 29
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lorg/chromium/chrome/browser/invalidation/InvalidationController;->LOCK:Ljava/lang/Object;

    return-void
.end method

.method constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 119
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 120
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 121
    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    const-string/jumbo v1, "Unable to get application context"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 122
    :cond_0
    iput-object v0, p0, Lorg/chromium/chrome/browser/invalidation/InvalidationController;->mContext:Landroid/content/Context;

    .line 123
    invoke-static {p0}, Lorg/chromium/base/ApplicationStatus;->registerApplicationStateListener(Lorg/chromium/base/ApplicationStatus$ApplicationStateListener;)V

    .line 124
    return-void
.end method

.method public static get(Landroid/content/Context;)Lorg/chromium/chrome/browser/invalidation/InvalidationController;
    .locals 2

    .prologue
    .line 107
    sget-object v1, Lorg/chromium/chrome/browser/invalidation/InvalidationController;->LOCK:Ljava/lang/Object;

    monitor-enter v1

    .line 108
    :try_start_0
    sget-object v0, Lorg/chromium/chrome/browser/invalidation/InvalidationController;->sInstance:Lorg/chromium/chrome/browser/invalidation/InvalidationController;

    if-nez v0, :cond_0

    .line 109
    new-instance v0, Lorg/chromium/chrome/browser/invalidation/InvalidationController;

    invoke-direct {v0, p0}, Lorg/chromium/chrome/browser/invalidation/InvalidationController;-><init>(Landroid/content/Context;)V

    sput-object v0, Lorg/chromium/chrome/browser/invalidation/InvalidationController;->sInstance:Lorg/chromium/chrome/browser/invalidation/InvalidationController;

    .line 111
    :cond_0
    sget-object v0, Lorg/chromium/chrome/browser/invalidation/InvalidationController;->sInstance:Lorg/chromium/chrome/browser/invalidation/InvalidationController;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    .line 112
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public getInvalidatorClientId()[B
    .locals 1

    .prologue
    .line 146
    invoke-static {}, Lorg/chromium/sync/notifier/InvalidationClientNameProvider;->get()Lorg/chromium/sync/notifier/InvalidationClientNameProvider;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/sync/notifier/InvalidationClientNameProvider;->getInvalidatorClientName()[B

    move-result-object v0

    return-object v0
.end method

.method public onApplicationStateChange(I)V
    .locals 1

    .prologue
    .line 128
    iget-object v0, p0, Lorg/chromium/chrome/browser/invalidation/InvalidationController;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lorg/chromium/sync/notifier/SyncStatusHelper;->get(Landroid/content/Context;)Lorg/chromium/sync/notifier/SyncStatusHelper;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/sync/notifier/SyncStatusHelper;->isSyncEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 129
    const/4 v0, 0x1

    if-ne p1, v0, :cond_1

    .line 130
    invoke-virtual {p0}, Lorg/chromium/chrome/browser/invalidation/InvalidationController;->start()V

    .line 135
    :cond_0
    :goto_0
    return-void

    .line 131
    :cond_1
    const/4 v0, 0x2

    if-ne p1, v0, :cond_0

    .line 132
    invoke-virtual {p0}, Lorg/chromium/chrome/browser/invalidation/InvalidationController;->stop()V

    goto :goto_0
.end method

.method public refreshRegisteredTypes(Ljava/util/Set;)V
    .locals 3

    .prologue
    .line 57
    new-instance v0, Lorg/chromium/sync/notifier/InvalidationPreferences;

    iget-object v1, p0, Lorg/chromium/chrome/browser/invalidation/InvalidationController;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lorg/chromium/sync/notifier/InvalidationPreferences;-><init>(Landroid/content/Context;)V

    .line 58
    invoke-virtual {v0}, Lorg/chromium/sync/notifier/InvalidationPreferences;->getSavedSyncedTypes()Ljava/util/Set;

    move-result-object v1

    .line 59
    invoke-virtual {v0}, Lorg/chromium/sync/notifier/InvalidationPreferences;->getSavedSyncedAccount()Landroid/accounts/Account;

    move-result-object v2

    .line 60
    if-eqz v1, :cond_0

    const-string/jumbo v0, "ALL_TYPES"

    invoke-interface {v1, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 62
    :goto_0
    invoke-virtual {p0, v2, v0, p1}, Lorg/chromium/chrome/browser/invalidation/InvalidationController;->setRegisteredTypes(Landroid/accounts/Account;ZLjava/util/Set;)V

    .line 63
    return-void

    .line 60
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setRegisteredObjectIds([I[Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 74
    new-instance v0, Lorg/chromium/sync/notifier/InvalidationPreferences;

    iget-object v1, p0, Lorg/chromium/chrome/browser/invalidation/InvalidationController;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lorg/chromium/sync/notifier/InvalidationPreferences;-><init>(Landroid/content/Context;)V

    .line 75
    invoke-virtual {v0}, Lorg/chromium/sync/notifier/InvalidationPreferences;->getSavedSyncedAccount()Landroid/accounts/Account;

    move-result-object v0

    .line 76
    invoke-static {v0, p1, p2}, Lorg/chromium/sync/notifier/InvalidationIntentProtocol;->createRegisterIntent(Landroid/accounts/Account;[I[Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 79
    iget-object v1, p0, Lorg/chromium/chrome/browser/invalidation/InvalidationController;->mContext:Landroid/content/Context;

    const-class v2, Lorg/chromium/sync/notifier/InvalidationService;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 80
    iget-object v1, p0, Lorg/chromium/chrome/browser/invalidation/InvalidationController;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 81
    return-void
.end method

.method public setRegisteredTypes(Landroid/accounts/Account;ZLjava/util/Set;)V
    .locals 3

    .prologue
    .line 43
    invoke-static {p1, p2, p3}, Lorg/chromium/sync/notifier/InvalidationIntentProtocol;->createRegisterIntent(Landroid/accounts/Account;ZLjava/util/Set;)Landroid/content/Intent;

    move-result-object v0

    .line 45
    iget-object v1, p0, Lorg/chromium/chrome/browser/invalidation/InvalidationController;->mContext:Landroid/content/Context;

    const-class v2, Lorg/chromium/sync/notifier/InvalidationService;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 46
    iget-object v1, p0, Lorg/chromium/chrome/browser/invalidation/InvalidationController;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 47
    return-void
.end method

.method public start()V
    .locals 3

    .prologue
    .line 87
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lorg/chromium/chrome/browser/invalidation/InvalidationController;->mContext:Landroid/content/Context;

    const-class v2, Lorg/chromium/sync/notifier/InvalidationService;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 88
    iget-object v1, p0, Lorg/chromium/chrome/browser/invalidation/InvalidationController;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 89
    return-void
.end method

.method public stop()V
    .locals 3

    .prologue
    .line 95
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lorg/chromium/chrome/browser/invalidation/InvalidationController;->mContext:Landroid/content/Context;

    const-class v2, Lorg/chromium/sync/notifier/InvalidationService;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 96
    const-string/jumbo v1, "stop"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 97
    iget-object v1, p0, Lorg/chromium/chrome/browser/invalidation/InvalidationController;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 98
    return-void
.end method
