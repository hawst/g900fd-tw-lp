.class public Lorg/chromium/chrome/browser/banners/RatingView;
.super Landroid/view/View;
.source "RatingView.java"


# instance fields
.field private final mDrawingRect:Landroid/graphics/Rect;

.field private mIncrements:I

.field private final mIsLayoutLTR:Z

.field private final mStarEmpty:Landroid/graphics/Bitmap;

.field private final mStarFull:Landroid/graphics/Bitmap;

.field private final mStarHalf:Landroid/graphics/Bitmap;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 41
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 42
    invoke-static {}, Lorg/chromium/ui/base/LocalizationUtils;->isLayoutRtl()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lorg/chromium/chrome/browser/banners/RatingView;->mIsLayoutLTR:Z

    .line 43
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lorg/chromium/chrome/browser/banners/RatingView;->mDrawingRect:Landroid/graphics/Rect;

    .line 46
    invoke-virtual {p0}, Lorg/chromium/chrome/browser/banners/RatingView;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    .line 47
    sget v0, Lorg/chromium/chrome/R$drawable;->btn_star_half:I

    invoke-static {v7, v0}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 48
    iget-boolean v2, p0, Lorg/chromium/chrome/browser/banners/RatingView;->mIsLayoutLTR:Z

    if-nez v2, :cond_0

    .line 50
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    .line 51
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    .line 52
    new-instance v5, Landroid/graphics/Matrix;

    invoke-direct {v5}, Landroid/graphics/Matrix;-><init>()V

    .line 53
    const/high16 v2, -0x40800000    # -1.0f

    const/high16 v6, 0x3f800000    # 1.0f

    invoke-virtual {v5, v2, v6}, Landroid/graphics/Matrix;->preScale(FF)Z

    move v2, v1

    move v6, v1

    .line 54
    invoke-static/range {v0 .. v6}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 56
    :cond_0
    iput-object v0, p0, Lorg/chromium/chrome/browser/banners/RatingView;->mStarHalf:Landroid/graphics/Bitmap;

    .line 57
    sget v0, Lorg/chromium/chrome/R$drawable;->btn_star_full:I

    invoke-static {v7, v0}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lorg/chromium/chrome/browser/banners/RatingView;->mStarFull:Landroid/graphics/Bitmap;

    .line 58
    sget v0, Lorg/chromium/chrome/R$drawable;->btn_star_empty:I

    invoke-static {v7, v0}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lorg/chromium/chrome/browser/banners/RatingView;->mStarEmpty:Landroid/graphics/Bitmap;

    .line 59
    return-void

    :cond_1
    move v0, v1

    .line 42
    goto :goto_0
.end method


# virtual methods
.method initialize(F)V
    .locals 1

    .prologue
    .line 67
    const/high16 v0, 0x40000000    # 2.0f

    mul-float/2addr v0, p1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    iput v0, p0, Lorg/chromium/chrome/browser/banners/RatingView;->mIncrements:I

    .line 68
    return-void
.end method

.method public onDraw(Landroid/graphics/Canvas;)V
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 72
    invoke-virtual {p1}, Landroid/graphics/Canvas;->getHeight()I

    move-result v4

    .line 75
    iget-object v0, p0, Lorg/chromium/chrome/browser/banners/RatingView;->mDrawingRect:Landroid/graphics/Rect;

    iput v1, v0, Landroid/graphics/Rect;->top:I

    .line 76
    iget-object v0, p0, Lorg/chromium/chrome/browser/banners/RatingView;->mDrawingRect:Landroid/graphics/Rect;

    iput v4, v0, Landroid/graphics/Rect;->bottom:I

    .line 77
    iget-object v3, p0, Lorg/chromium/chrome/browser/banners/RatingView;->mDrawingRect:Landroid/graphics/Rect;

    iget-boolean v0, p0, Lorg/chromium/chrome/browser/banners/RatingView;->mIsLayoutLTR:Z

    if-eqz v0, :cond_1

    move v0, v1

    :goto_0
    iput v0, v3, Landroid/graphics/Rect;->left:I

    .line 78
    iget-object v0, p0, Lorg/chromium/chrome/browser/banners/RatingView;->mDrawingRect:Landroid/graphics/Rect;

    iget-object v3, p0, Lorg/chromium/chrome/browser/banners/RatingView;->mDrawingRect:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->left:I

    add-int/2addr v3, v4

    iput v3, v0, Landroid/graphics/Rect;->right:I

    move v3, v1

    .line 81
    :goto_1
    const/16 v0, 0xa

    if-ge v3, v0, :cond_5

    .line 82
    iget-object v0, p0, Lorg/chromium/chrome/browser/banners/RatingView;->mStarEmpty:Landroid/graphics/Bitmap;

    .line 83
    iget v5, p0, Lorg/chromium/chrome/browser/banners/RatingView;->mIncrements:I

    if-ge v3, v5, :cond_0

    .line 84
    iget v0, p0, Lorg/chromium/chrome/browser/banners/RatingView;->mIncrements:I

    sub-int/2addr v0, v3

    const/4 v5, 0x2

    if-lt v0, v5, :cond_2

    move v0, v2

    .line 85
    :goto_2
    if-eqz v0, :cond_3

    iget-object v0, p0, Lorg/chromium/chrome/browser/banners/RatingView;->mStarFull:Landroid/graphics/Bitmap;

    .line 87
    :cond_0
    :goto_3
    iget-object v5, p0, Lorg/chromium/chrome/browser/banners/RatingView;->mDrawingRect:Landroid/graphics/Rect;

    invoke-virtual {p1, v0, v7, v5, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 90
    iget-object v5, p0, Lorg/chromium/chrome/browser/banners/RatingView;->mDrawingRect:Landroid/graphics/Rect;

    iget v6, v5, Landroid/graphics/Rect;->left:I

    iget-boolean v0, p0, Lorg/chromium/chrome/browser/banners/RatingView;->mIsLayoutLTR:Z

    if-eqz v0, :cond_4

    move v0, v2

    :goto_4
    mul-int/2addr v0, v4

    add-int/2addr v0, v6

    iput v0, v5, Landroid/graphics/Rect;->left:I

    .line 91
    iget-object v0, p0, Lorg/chromium/chrome/browser/banners/RatingView;->mDrawingRect:Landroid/graphics/Rect;

    iget-object v5, p0, Lorg/chromium/chrome/browser/banners/RatingView;->mDrawingRect:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->left:I

    add-int/2addr v5, v4

    iput v5, v0, Landroid/graphics/Rect;->right:I

    .line 81
    add-int/lit8 v0, v3, 0x2

    move v3, v0

    goto :goto_1

    .line 77
    :cond_1
    invoke-virtual {p1}, Landroid/graphics/Canvas;->getWidth()I

    move-result v0

    sub-int/2addr v0, v4

    goto :goto_0

    :cond_2
    move v0, v1

    .line 84
    goto :goto_2

    .line 85
    :cond_3
    iget-object v0, p0, Lorg/chromium/chrome/browser/banners/RatingView;->mStarHalf:Landroid/graphics/Bitmap;

    goto :goto_3

    .line 90
    :cond_4
    const/4 v0, -0x1

    goto :goto_4

    .line 93
    :cond_5
    return-void
.end method
