.class public abstract Lorg/chromium/chrome/browser/tabmodel/TabModelSelectorBase;
.super Ljava/lang/Object;
.source "TabModelSelectorBase.java"

# interfaces
.implements Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;


# static fields
.field static final synthetic $assertionsDisabled:Z

.field public static final INCOGNITO_TAB_MODEL_INDEX:I = 0x1

.field public static final NORMAL_TAB_MODEL_INDEX:I


# instance fields
.field private mActiveModelIndex:I

.field private final mChangeListeners:Ljava/util/ArrayList;

.field private mTabModels:Ljava/util/List;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 16
    const-class v0, Lorg/chromium/chrome/browser/tabmodel/TabModelSelectorBase;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/chromium/chrome/browser/tabmodel/TabModelSelectorBase;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lorg/chromium/chrome/browser/tabmodel/TabModelSelectorBase;->mTabModels:Ljava/util/List;

    .line 21
    const/4 v0, 0x0

    iput v0, p0, Lorg/chromium/chrome/browser/tabmodel/TabModelSelectorBase;->mActiveModelIndex:I

    .line 22
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lorg/chromium/chrome/browser/tabmodel/TabModelSelectorBase;->mChangeListeners:Ljava/util/ArrayList;

    return-void
.end method


# virtual methods
.method public closeAllTabs()V
    .locals 2

    .prologue
    .line 130
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0}, Lorg/chromium/chrome/browser/tabmodel/TabModelSelectorBase;->getModels()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    invoke-virtual {p0, v0}, Lorg/chromium/chrome/browser/tabmodel/TabModelSelectorBase;->getModelAt(I)Lorg/chromium/chrome/browser/tabmodel/TabModel;

    move-result-object v1

    invoke-interface {v1}, Lorg/chromium/chrome/browser/tabmodel/TabModel;->closeAllTabs()V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 131
    :cond_0
    return-void
.end method

.method public closeTab(Lorg/chromium/chrome/browser/Tab;)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 102
    move v0, v1

    :goto_0
    invoke-virtual {p0}, Lorg/chromium/chrome/browser/tabmodel/TabModelSelectorBase;->getModels()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v0, v2, :cond_2

    .line 103
    invoke-virtual {p0, v0}, Lorg/chromium/chrome/browser/tabmodel/TabModelSelectorBase;->getModelAt(I)Lorg/chromium/chrome/browser/tabmodel/TabModel;

    move-result-object v2

    .line 104
    invoke-interface {v2, p1}, Lorg/chromium/chrome/browser/tabmodel/TabModel;->indexOf(Lorg/chromium/chrome/browser/Tab;)I

    move-result v3

    if-ltz v3, :cond_1

    .line 105
    invoke-interface {v2, p1}, Lorg/chromium/chrome/browser/tabmodel/TabModel;->closeTab(Lorg/chromium/chrome/browser/Tab;)Z

    move-result v1

    .line 109
    :cond_0
    return v1

    .line 102
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 108
    :cond_2
    sget-boolean v0, Lorg/chromium/chrome/browser/tabmodel/TabModelSelectorBase;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    const-string/jumbo v1, "Tried to close a tab that is not in any model!"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0
.end method

.method public commitAllTabClosures()V
    .locals 2

    .prologue
    .line 114
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lorg/chromium/chrome/browser/tabmodel/TabModelSelectorBase;->mTabModels:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 115
    iget-object v0, p0, Lorg/chromium/chrome/browser/tabmodel/TabModelSelectorBase;->mTabModels:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/chromium/chrome/browser/tabmodel/TabModel;

    invoke-interface {v0}, Lorg/chromium/chrome/browser/tabmodel/TabModel;->commitAllTabClosures()V

    .line 114
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 117
    :cond_0
    return-void
.end method

.method public getCurrentModel()Lorg/chromium/chrome/browser/tabmodel/TabModel;
    .locals 1

    .prologue
    .line 76
    iget v0, p0, Lorg/chromium/chrome/browser/tabmodel/TabModelSelectorBase;->mActiveModelIndex:I

    invoke-virtual {p0, v0}, Lorg/chromium/chrome/browser/tabmodel/TabModelSelectorBase;->getModelAt(I)Lorg/chromium/chrome/browser/tabmodel/TabModel;

    move-result-object v0

    return-object v0
.end method

.method public getCurrentTab()Lorg/chromium/chrome/browser/Tab;
    .locals 1

    .prologue
    .line 54
    invoke-virtual {p0}, Lorg/chromium/chrome/browser/tabmodel/TabModelSelectorBase;->getCurrentModel()Lorg/chromium/chrome/browser/tabmodel/TabModel;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lorg/chromium/chrome/browser/tabmodel/TabModelSelectorBase;->getCurrentModel()Lorg/chromium/chrome/browser/tabmodel/TabModel;

    move-result-object v0

    invoke-static {v0}, Lorg/chromium/chrome/browser/tabmodel/TabModelUtils;->getCurrentTab(Lorg/chromium/chrome/browser/tabmodel/TabList;)Lorg/chromium/chrome/browser/Tab;

    move-result-object v0

    goto :goto_0
.end method

.method public getCurrentTabId()I
    .locals 1

    .prologue
    .line 59
    invoke-virtual {p0}, Lorg/chromium/chrome/browser/tabmodel/TabModelSelectorBase;->getCurrentTab()Lorg/chromium/chrome/browser/Tab;

    move-result-object v0

    .line 60
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/Tab;->getId()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public getModel(Z)Lorg/chromium/chrome/browser/tabmodel/TabModel;
    .locals 1

    .prologue
    .line 86
    if-eqz p1, :cond_0

    const/4 v0, 0x1

    .line 87
    :goto_0
    invoke-virtual {p0, v0}, Lorg/chromium/chrome/browser/tabmodel/TabModelSelectorBase;->getModelAt(I)Lorg/chromium/chrome/browser/tabmodel/TabModel;

    move-result-object v0

    return-object v0

    .line 86
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getModelAt(I)Lorg/chromium/chrome/browser/tabmodel/TabModel;
    .locals 3

    .prologue
    .line 48
    sget-boolean v0, Lorg/chromium/chrome/browser/tabmodel/TabModelSelectorBase;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lorg/chromium/chrome/browser/tabmodel/TabModelSelectorBase;->mTabModels:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge p1, v0, :cond_0

    if-gez p1, :cond_1

    :cond_0
    new-instance v0, Ljava/lang/AssertionError;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "requested index "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " size "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lorg/chromium/chrome/browser/tabmodel/TabModelSelectorBase;->mTabModels:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 49
    :cond_1
    iget-object v0, p0, Lorg/chromium/chrome/browser/tabmodel/TabModelSelectorBase;->mTabModels:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/chromium/chrome/browser/tabmodel/TabModel;

    return-object v0
.end method

.method public getModelForTabId(I)Lorg/chromium/chrome/browser/tabmodel/TabModel;
    .locals 3

    .prologue
    .line 65
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lorg/chromium/chrome/browser/tabmodel/TabModelSelectorBase;->mTabModels:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 66
    iget-object v0, p0, Lorg/chromium/chrome/browser/tabmodel/TabModelSelectorBase;->mTabModels:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/chromium/chrome/browser/tabmodel/TabModel;

    .line 67
    invoke-static {v0, p1}, Lorg/chromium/chrome/browser/tabmodel/TabModelUtils;->getTabById(Lorg/chromium/chrome/browser/tabmodel/TabList;I)Lorg/chromium/chrome/browser/Tab;

    move-result-object v2

    if-nez v2, :cond_0

    invoke-interface {v0, p1}, Lorg/chromium/chrome/browser/tabmodel/TabModel;->isClosurePending(I)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 71
    :cond_0
    :goto_1
    return-object v0

    .line 65
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 71
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public getModels()Ljava/util/List;
    .locals 1

    .prologue
    .line 97
    iget-object v0, p0, Lorg/chromium/chrome/browser/tabmodel/TabModelSelectorBase;->mTabModels:Ljava/util/List;

    return-object v0
.end method

.method public getTabById(I)Lorg/chromium/chrome/browser/Tab;
    .locals 2

    .prologue
    .line 121
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0}, Lorg/chromium/chrome/browser/tabmodel/TabModelSelectorBase;->getModels()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 122
    invoke-virtual {p0, v0}, Lorg/chromium/chrome/browser/tabmodel/TabModelSelectorBase;->getModelAt(I)Lorg/chromium/chrome/browser/tabmodel/TabModel;

    move-result-object v1

    invoke-static {v1, p1}, Lorg/chromium/chrome/browser/tabmodel/TabModelUtils;->getTabById(Lorg/chromium/chrome/browser/tabmodel/TabList;I)Lorg/chromium/chrome/browser/Tab;

    move-result-object v1

    .line 123
    if-eqz v1, :cond_0

    move-object v0, v1

    .line 125
    :goto_1
    return-object v0

    .line 121
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 125
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public getTotalTabCount()I
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 135
    move v1, v0

    .line 136
    :goto_0
    invoke-virtual {p0}, Lorg/chromium/chrome/browser/tabmodel/TabModelSelectorBase;->getModels()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 137
    invoke-virtual {p0, v0}, Lorg/chromium/chrome/browser/tabmodel/TabModelSelectorBase;->getModelAt(I)Lorg/chromium/chrome/browser/tabmodel/TabModel;

    move-result-object v2

    invoke-interface {v2}, Lorg/chromium/chrome/browser/tabmodel/TabModel;->getCount()I

    move-result v2

    add-int/2addr v1, v2

    .line 136
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 139
    :cond_0
    return v1
.end method

.method protected final varargs initialize(Z[Lorg/chromium/chrome/browser/tabmodel/TabModel;)V
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 26
    sget-boolean v0, Lorg/chromium/chrome/browser/tabmodel/TabModelSelectorBase;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lorg/chromium/chrome/browser/tabmodel/TabModelSelectorBase;->mTabModels:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 27
    :cond_0
    sget-boolean v0, Lorg/chromium/chrome/browser/tabmodel/TabModelSelectorBase;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    array-length v0, p2

    if-gtz v0, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 28
    :cond_1
    if-eqz p1, :cond_2

    .line 29
    sget-boolean v0, Lorg/chromium/chrome/browser/tabmodel/TabModelSelectorBase;->$assertionsDisabled:Z

    if-nez v0, :cond_2

    array-length v0, p2

    if-gt v0, v2, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 32
    :cond_2
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    move v0, v1

    .line 33
    :goto_0
    array-length v4, p2

    if-ge v0, v4, :cond_3

    .line 34
    aget-object v4, p2, v0

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 33
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 36
    :cond_3
    if-eqz p1, :cond_4

    move v1, v2

    :cond_4
    iput v1, p0, Lorg/chromium/chrome/browser/tabmodel/TabModelSelectorBase;->mActiveModelIndex:I

    .line 37
    invoke-static {v3}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lorg/chromium/chrome/browser/tabmodel/TabModelSelectorBase;->mTabModels:Ljava/util/List;

    .line 38
    return-void
.end method

.method public isIncognitoSelected()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 92
    iget v1, p0, Lorg/chromium/chrome/browser/tabmodel/TabModelSelectorBase;->mActiveModelIndex:I

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected notifyChanged()V
    .locals 2

    .prologue
    .line 157
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lorg/chromium/chrome/browser/tabmodel/TabModelSelectorBase;->mChangeListeners:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 158
    iget-object v0, p0, Lorg/chromium/chrome/browser/tabmodel/TabModelSelectorBase;->mChangeListeners:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/chromium/chrome/browser/tabmodel/TabModelSelector$ChangeListener;

    invoke-interface {v0}, Lorg/chromium/chrome/browser/tabmodel/TabModelSelector$ChangeListener;->onChange()V

    .line 157
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 160
    :cond_0
    return-void
.end method

.method protected notifyNewTabCreated(Lorg/chromium/chrome/browser/Tab;)V
    .locals 2

    .prologue
    .line 167
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lorg/chromium/chrome/browser/tabmodel/TabModelSelectorBase;->mChangeListeners:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 168
    iget-object v0, p0, Lorg/chromium/chrome/browser/tabmodel/TabModelSelectorBase;->mChangeListeners:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/chromium/chrome/browser/tabmodel/TabModelSelector$ChangeListener;

    invoke-interface {v0, p1}, Lorg/chromium/chrome/browser/tabmodel/TabModelSelector$ChangeListener;->onNewTabCreated(Lorg/chromium/chrome/browser/Tab;)V

    .line 167
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 170
    :cond_0
    return-void
.end method

.method public registerChangeListener(Lorg/chromium/chrome/browser/tabmodel/TabModelSelector$ChangeListener;)V
    .locals 1

    .prologue
    .line 144
    iget-object v0, p0, Lorg/chromium/chrome/browser/tabmodel/TabModelSelectorBase;->mChangeListeners:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lorg/chromium/chrome/browser/tabmodel/TabModelSelectorBase;->mChangeListeners:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 145
    :cond_0
    return-void
.end method

.method public selectModel(Z)V
    .locals 1

    .prologue
    .line 42
    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput v0, p0, Lorg/chromium/chrome/browser/tabmodel/TabModelSelectorBase;->mActiveModelIndex:I

    .line 43
    return-void

    .line 42
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public unregisterChangeListener(Lorg/chromium/chrome/browser/tabmodel/TabModelSelector$ChangeListener;)V
    .locals 1

    .prologue
    .line 149
    iget-object v0, p0, Lorg/chromium/chrome/browser/tabmodel/TabModelSelectorBase;->mChangeListeners:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 150
    return-void
.end method
