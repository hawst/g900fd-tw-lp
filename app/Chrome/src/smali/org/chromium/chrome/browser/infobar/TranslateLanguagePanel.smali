.class public Lorg/chromium/chrome/browser/infobar/TranslateLanguagePanel;
.super Ljava/lang/Object;
.source "TranslateLanguagePanel.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemSelectedListener;
.implements Lorg/chromium/chrome/browser/infobar/TranslateSubPanel;


# instance fields
.field private final mListener:Lorg/chromium/chrome/browser/infobar/SubPanelListener;

.field private final mOptions:Lorg/chromium/chrome/browser/infobar/TranslateOptions;

.field private final mSessionOptions:Lorg/chromium/chrome/browser/infobar/TranslateOptions;

.field private mSourceAdapter:Lorg/chromium/chrome/browser/infobar/TranslateLanguagePanel$LanguageArrayAdapter;

.field private mSourceSpinner:Landroid/widget/Spinner;

.field private mTargetAdapter:Lorg/chromium/chrome/browser/infobar/TranslateLanguagePanel$LanguageArrayAdapter;

.field private mTargetSpinner:Landroid/widget/Spinner;


# direct methods
.method public constructor <init>(Lorg/chromium/chrome/browser/infobar/SubPanelListener;Lorg/chromium/chrome/browser/infobar/TranslateOptions;)V
    .locals 2

    .prologue
    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 62
    iput-object p1, p0, Lorg/chromium/chrome/browser/infobar/TranslateLanguagePanel;->mListener:Lorg/chromium/chrome/browser/infobar/SubPanelListener;

    .line 63
    iput-object p2, p0, Lorg/chromium/chrome/browser/infobar/TranslateLanguagePanel;->mOptions:Lorg/chromium/chrome/browser/infobar/TranslateOptions;

    .line 64
    new-instance v0, Lorg/chromium/chrome/browser/infobar/TranslateOptions;

    iget-object v1, p0, Lorg/chromium/chrome/browser/infobar/TranslateLanguagePanel;->mOptions:Lorg/chromium/chrome/browser/infobar/TranslateOptions;

    invoke-direct {v0, v1}, Lorg/chromium/chrome/browser/infobar/TranslateOptions;-><init>(Lorg/chromium/chrome/browser/infobar/TranslateOptions;)V

    iput-object v0, p0, Lorg/chromium/chrome/browser/infobar/TranslateLanguagePanel;->mSessionOptions:Lorg/chromium/chrome/browser/infobar/TranslateOptions;

    .line 65
    return-void
.end method

.method private createSpinnerLanguages(I)Ljava/util/ArrayList;
    .locals 5

    .prologue
    .line 170
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 171
    iget-object v0, p0, Lorg/chromium/chrome/browser/infobar/TranslateLanguagePanel;->mSessionOptions:Lorg/chromium/chrome/browser/infobar/TranslateOptions;

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/infobar/TranslateOptions;->allLanguages()Ljava/util/List;

    move-result-object v3

    .line 172
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 173
    if-eq v1, p1, :cond_0

    .line 174
    new-instance v4, Lorg/chromium/chrome/browser/infobar/TranslateLanguagePanel$SpinnerLanguageElement;

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-direct {v4, v0, v1}, Lorg/chromium/chrome/browser/infobar/TranslateLanguagePanel$SpinnerLanguageElement;-><init>(Ljava/lang/String;I)V

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 172
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 177
    :cond_1
    return-object v2
.end method

.method private createSpinners(Landroid/content/Context;)V
    .locals 4

    .prologue
    const/4 v3, -0x1

    .line 94
    new-instance v0, Lorg/chromium/chrome/browser/infobar/TranslateLanguagePanel$LanguageArrayAdapter;

    sget v1, Lorg/chromium/chrome/R$layout;->translate_spinner:I

    const/4 v2, 0x0

    invoke-direct {v0, p1, v1, v2}, Lorg/chromium/chrome/browser/infobar/TranslateLanguagePanel$LanguageArrayAdapter;-><init>(Landroid/content/Context;II)V

    iput-object v0, p0, Lorg/chromium/chrome/browser/infobar/TranslateLanguagePanel;->mSourceAdapter:Lorg/chromium/chrome/browser/infobar/TranslateLanguagePanel$LanguageArrayAdapter;

    .line 96
    new-instance v0, Lorg/chromium/chrome/browser/infobar/TranslateLanguagePanel$LanguageArrayAdapter;

    sget v1, Lorg/chromium/chrome/R$layout;->translate_spinner:I

    const/4 v2, 0x1

    invoke-direct {v0, p1, v1, v2}, Lorg/chromium/chrome/browser/infobar/TranslateLanguagePanel$LanguageArrayAdapter;-><init>(Landroid/content/Context;II)V

    iput-object v0, p0, Lorg/chromium/chrome/browser/infobar/TranslateLanguagePanel;->mTargetAdapter:Lorg/chromium/chrome/browser/infobar/TranslateLanguagePanel$LanguageArrayAdapter;

    .line 100
    iget-object v0, p0, Lorg/chromium/chrome/browser/infobar/TranslateLanguagePanel;->mSourceAdapter:Lorg/chromium/chrome/browser/infobar/TranslateLanguagePanel$LanguageArrayAdapter;

    invoke-direct {p0, v3}, Lorg/chromium/chrome/browser/infobar/TranslateLanguagePanel;->createSpinnerLanguages(I)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/chromium/chrome/browser/infobar/TranslateLanguagePanel$LanguageArrayAdapter;->addAll(Ljava/util/Collection;)V

    .line 101
    iget-object v0, p0, Lorg/chromium/chrome/browser/infobar/TranslateLanguagePanel;->mTargetAdapter:Lorg/chromium/chrome/browser/infobar/TranslateLanguagePanel$LanguageArrayAdapter;

    invoke-direct {p0, v3}, Lorg/chromium/chrome/browser/infobar/TranslateLanguagePanel;->createSpinnerLanguages(I)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/chromium/chrome/browser/infobar/TranslateLanguagePanel$LanguageArrayAdapter;->addAll(Ljava/util/Collection;)V

    .line 102
    iget-object v0, p0, Lorg/chromium/chrome/browser/infobar/TranslateLanguagePanel;->mSourceAdapter:Lorg/chromium/chrome/browser/infobar/TranslateLanguagePanel$LanguageArrayAdapter;

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/infobar/TranslateLanguagePanel$LanguageArrayAdapter;->measureWidthRequiredForView()V

    .line 103
    iget-object v0, p0, Lorg/chromium/chrome/browser/infobar/TranslateLanguagePanel;->mTargetAdapter:Lorg/chromium/chrome/browser/infobar/TranslateLanguagePanel$LanguageArrayAdapter;

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/infobar/TranslateLanguagePanel$LanguageArrayAdapter;->measureWidthRequiredForView()V

    .line 106
    new-instance v0, Landroid/widget/Spinner;

    invoke-direct {v0, p1}, Landroid/widget/Spinner;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lorg/chromium/chrome/browser/infobar/TranslateLanguagePanel;->mSourceSpinner:Landroid/widget/Spinner;

    .line 107
    new-instance v0, Landroid/widget/Spinner;

    invoke-direct {v0, p1}, Landroid/widget/Spinner;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lorg/chromium/chrome/browser/infobar/TranslateLanguagePanel;->mTargetSpinner:Landroid/widget/Spinner;

    .line 108
    iget-object v0, p0, Lorg/chromium/chrome/browser/infobar/TranslateLanguagePanel;->mSourceSpinner:Landroid/widget/Spinner;

    invoke-virtual {v0, p0}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 109
    iget-object v0, p0, Lorg/chromium/chrome/browser/infobar/TranslateLanguagePanel;->mTargetSpinner:Landroid/widget/Spinner;

    invoke-virtual {v0, p0}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 110
    iget-object v0, p0, Lorg/chromium/chrome/browser/infobar/TranslateLanguagePanel;->mSourceSpinner:Landroid/widget/Spinner;

    iget-object v1, p0, Lorg/chromium/chrome/browser/infobar/TranslateLanguagePanel;->mSourceAdapter:Lorg/chromium/chrome/browser/infobar/TranslateLanguagePanel$LanguageArrayAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 111
    iget-object v0, p0, Lorg/chromium/chrome/browser/infobar/TranslateLanguagePanel;->mTargetSpinner:Landroid/widget/Spinner;

    iget-object v1, p0, Lorg/chromium/chrome/browser/infobar/TranslateLanguagePanel;->mTargetAdapter:Lorg/chromium/chrome/browser/infobar/TranslateLanguagePanel$LanguageArrayAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 112
    invoke-direct {p0}, Lorg/chromium/chrome/browser/infobar/TranslateLanguagePanel;->reloadSpinners()V

    .line 113
    return-void
.end method

.method private getSelectionPosition(I)I
    .locals 2

    .prologue
    .line 136
    if-nez p1, :cond_1

    iget-object v0, p0, Lorg/chromium/chrome/browser/infobar/TranslateLanguagePanel;->mSessionOptions:Lorg/chromium/chrome/browser/infobar/TranslateOptions;

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/infobar/TranslateOptions;->sourceLanguageIndex()I

    move-result v0

    .line 142
    :goto_0
    if-nez p1, :cond_2

    iget-object v1, p0, Lorg/chromium/chrome/browser/infobar/TranslateLanguagePanel;->mSessionOptions:Lorg/chromium/chrome/browser/infobar/TranslateOptions;

    invoke-virtual {v1}, Lorg/chromium/chrome/browser/infobar/TranslateOptions;->targetLanguageIndex()I

    move-result v1

    .line 144
    :goto_1
    if-ge v1, v0, :cond_0

    add-int/lit8 v0, v0, -0x1

    .line 146
    :cond_0
    return v0

    .line 136
    :cond_1
    iget-object v0, p0, Lorg/chromium/chrome/browser/infobar/TranslateLanguagePanel;->mSessionOptions:Lorg/chromium/chrome/browser/infobar/TranslateOptions;

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/infobar/TranslateOptions;->targetLanguageIndex()I

    move-result v0

    goto :goto_0

    .line 142
    :cond_2
    iget-object v1, p0, Lorg/chromium/chrome/browser/infobar/TranslateLanguagePanel;->mSessionOptions:Lorg/chromium/chrome/browser/infobar/TranslateOptions;

    invoke-virtual {v1}, Lorg/chromium/chrome/browser/infobar/TranslateOptions;->sourceLanguageIndex()I

    move-result v1

    goto :goto_1
.end method

.method private reloadSpinners()V
    .locals 3

    .prologue
    .line 116
    iget-object v0, p0, Lorg/chromium/chrome/browser/infobar/TranslateLanguagePanel;->mSourceAdapter:Lorg/chromium/chrome/browser/infobar/TranslateLanguagePanel$LanguageArrayAdapter;

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/infobar/TranslateLanguagePanel$LanguageArrayAdapter;->clear()V

    .line 117
    iget-object v0, p0, Lorg/chromium/chrome/browser/infobar/TranslateLanguagePanel;->mTargetAdapter:Lorg/chromium/chrome/browser/infobar/TranslateLanguagePanel$LanguageArrayAdapter;

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/infobar/TranslateLanguagePanel$LanguageArrayAdapter;->clear()V

    .line 119
    iget-object v0, p0, Lorg/chromium/chrome/browser/infobar/TranslateLanguagePanel;->mSessionOptions:Lorg/chromium/chrome/browser/infobar/TranslateOptions;

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/infobar/TranslateOptions;->targetLanguageIndex()I

    move-result v0

    .line 120
    iget-object v1, p0, Lorg/chromium/chrome/browser/infobar/TranslateLanguagePanel;->mSessionOptions:Lorg/chromium/chrome/browser/infobar/TranslateOptions;

    invoke-virtual {v1}, Lorg/chromium/chrome/browser/infobar/TranslateOptions;->sourceLanguageIndex()I

    move-result v1

    .line 121
    iget-object v2, p0, Lorg/chromium/chrome/browser/infobar/TranslateLanguagePanel;->mSourceAdapter:Lorg/chromium/chrome/browser/infobar/TranslateLanguagePanel$LanguageArrayAdapter;

    invoke-direct {p0, v0}, Lorg/chromium/chrome/browser/infobar/TranslateLanguagePanel;->createSpinnerLanguages(I)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v2, v0}, Lorg/chromium/chrome/browser/infobar/TranslateLanguagePanel$LanguageArrayAdapter;->addAll(Ljava/util/Collection;)V

    .line 122
    iget-object v0, p0, Lorg/chromium/chrome/browser/infobar/TranslateLanguagePanel;->mTargetAdapter:Lorg/chromium/chrome/browser/infobar/TranslateLanguagePanel$LanguageArrayAdapter;

    invoke-direct {p0, v1}, Lorg/chromium/chrome/browser/infobar/TranslateLanguagePanel;->createSpinnerLanguages(I)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/chromium/chrome/browser/infobar/TranslateLanguagePanel$LanguageArrayAdapter;->addAll(Ljava/util/Collection;)V

    .line 124
    iget-object v0, p0, Lorg/chromium/chrome/browser/infobar/TranslateLanguagePanel;->mSourceSpinner:Landroid/widget/Spinner;

    invoke-virtual {v0}, Landroid/widget/Spinner;->getSelectedItemPosition()I

    move-result v0

    .line 125
    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lorg/chromium/chrome/browser/infobar/TranslateLanguagePanel;->getSelectionPosition(I)I

    move-result v1

    .line 126
    if-eq v0, v1, :cond_0

    .line 127
    iget-object v0, p0, Lorg/chromium/chrome/browser/infobar/TranslateLanguagePanel;->mSourceSpinner:Landroid/widget/Spinner;

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setSelection(I)V

    .line 129
    :cond_0
    iget-object v0, p0, Lorg/chromium/chrome/browser/infobar/TranslateLanguagePanel;->mTargetSpinner:Landroid/widget/Spinner;

    invoke-virtual {v0}, Landroid/widget/Spinner;->getSelectedItemPosition()I

    move-result v0

    .line 130
    const/4 v1, 0x1

    invoke-direct {p0, v1}, Lorg/chromium/chrome/browser/infobar/TranslateLanguagePanel;->getSelectionPosition(I)I

    move-result v1

    .line 131
    if-eq v0, v1, :cond_1

    .line 132
    iget-object v0, p0, Lorg/chromium/chrome/browser/infobar/TranslateLanguagePanel;->mTargetSpinner:Landroid/widget/Spinner;

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setSelection(I)V

    .line 133
    :cond_1
    return-void
.end method


# virtual methods
.method public createContent(Landroid/content/Context;Lorg/chromium/chrome/browser/infobar/InfoBarLayout;)V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 69
    iput-object v0, p0, Lorg/chromium/chrome/browser/infobar/TranslateLanguagePanel;->mSourceSpinner:Landroid/widget/Spinner;

    .line 70
    iput-object v0, p0, Lorg/chromium/chrome/browser/infobar/TranslateLanguagePanel;->mTargetSpinner:Landroid/widget/Spinner;

    .line 72
    sget v0, Lorg/chromium/chrome/R$string;->translate_infobar_change_languages:I

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 73
    invoke-virtual {p2, v0}, Lorg/chromium/chrome/browser/infobar/InfoBarLayout;->setMessage(Ljava/lang/CharSequence;)V

    .line 76
    invoke-direct {p0, p1}, Lorg/chromium/chrome/browser/infobar/TranslateLanguagePanel;->createSpinners(Landroid/content/Context;)V

    .line 77
    iget-object v0, p0, Lorg/chromium/chrome/browser/infobar/TranslateLanguagePanel;->mSourceSpinner:Landroid/widget/Spinner;

    iget-object v1, p0, Lorg/chromium/chrome/browser/infobar/TranslateLanguagePanel;->mTargetSpinner:Landroid/widget/Spinner;

    invoke-virtual {p2, v0, v1}, Lorg/chromium/chrome/browser/infobar/InfoBarLayout;->setCustomContent(Landroid/view/View;Landroid/view/View;)V

    .line 80
    sget v0, Lorg/chromium/chrome/R$string;->translate_button_done:I

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    sget v1, Lorg/chromium/chrome/R$string;->cancel:I

    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v0, v1}, Lorg/chromium/chrome/browser/infobar/InfoBarLayout;->setButtons(Ljava/lang/String;Ljava/lang/String;)V

    .line 82
    return-void
.end method

.method public onButtonClicked(Z)V
    .locals 2

    .prologue
    .line 86
    if-eqz p1, :cond_0

    .line 87
    iget-object v0, p0, Lorg/chromium/chrome/browser/infobar/TranslateLanguagePanel;->mOptions:Lorg/chromium/chrome/browser/infobar/TranslateOptions;

    iget-object v1, p0, Lorg/chromium/chrome/browser/infobar/TranslateLanguagePanel;->mSessionOptions:Lorg/chromium/chrome/browser/infobar/TranslateOptions;

    invoke-virtual {v1}, Lorg/chromium/chrome/browser/infobar/TranslateOptions;->sourceLanguageIndex()I

    move-result v1

    invoke-virtual {v0, v1}, Lorg/chromium/chrome/browser/infobar/TranslateOptions;->setSourceLanguage(I)Z

    .line 88
    iget-object v0, p0, Lorg/chromium/chrome/browser/infobar/TranslateLanguagePanel;->mOptions:Lorg/chromium/chrome/browser/infobar/TranslateOptions;

    iget-object v1, p0, Lorg/chromium/chrome/browser/infobar/TranslateLanguagePanel;->mSessionOptions:Lorg/chromium/chrome/browser/infobar/TranslateOptions;

    invoke-virtual {v1}, Lorg/chromium/chrome/browser/infobar/TranslateOptions;->targetLanguageIndex()I

    move-result v1

    invoke-virtual {v0, v1}, Lorg/chromium/chrome/browser/infobar/TranslateOptions;->setTargetLanguage(I)Z

    .line 90
    :cond_0
    iget-object v0, p0, Lorg/chromium/chrome/browser/infobar/TranslateLanguagePanel;->mListener:Lorg/chromium/chrome/browser/infobar/SubPanelListener;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lorg/chromium/chrome/browser/infobar/SubPanelListener;->onPanelClosed(I)V

    .line 91
    return-void
.end method

.method public onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 2

    .prologue
    .line 151
    check-cast p1, Landroid/widget/Spinner;

    .line 152
    invoke-virtual {p1}, Landroid/widget/Spinner;->getSelectedItem()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/chromium/chrome/browser/infobar/TranslateLanguagePanel$SpinnerLanguageElement;

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/infobar/TranslateLanguagePanel$SpinnerLanguageElement;->getLanguageId()I

    move-result v0

    .line 153
    iget-object v1, p0, Lorg/chromium/chrome/browser/infobar/TranslateLanguagePanel;->mSourceSpinner:Landroid/widget/Spinner;

    if-ne p1, v1, :cond_0

    .line 154
    iget-object v1, p0, Lorg/chromium/chrome/browser/infobar/TranslateLanguagePanel;->mSessionOptions:Lorg/chromium/chrome/browser/infobar/TranslateOptions;

    invoke-virtual {v1, v0}, Lorg/chromium/chrome/browser/infobar/TranslateOptions;->setSourceLanguage(I)Z

    .line 158
    :goto_0
    invoke-direct {p0}, Lorg/chromium/chrome/browser/infobar/TranslateLanguagePanel;->reloadSpinners()V

    .line 159
    return-void

    .line 156
    :cond_0
    iget-object v1, p0, Lorg/chromium/chrome/browser/infobar/TranslateLanguagePanel;->mSessionOptions:Lorg/chromium/chrome/browser/infobar/TranslateOptions;

    invoke-virtual {v1, v0}, Lorg/chromium/chrome/browser/infobar/TranslateOptions;->setTargetLanguage(I)Z

    goto :goto_0
.end method

.method public onNothingSelected(Landroid/widget/AdapterView;)V
    .locals 0

    .prologue
    .line 163
    return-void
.end method
