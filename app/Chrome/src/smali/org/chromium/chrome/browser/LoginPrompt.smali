.class public Lorg/chromium/chrome/browser/LoginPrompt;
.super Ljava/lang/Object;
.source "LoginPrompt.java"

# interfaces
.implements Lorg/chromium/chrome/browser/ChromeHttpAuthHandler$AutofillObserver;


# instance fields
.field private final mAuthHandler:Lorg/chromium/chrome/browser/ChromeHttpAuthHandler;

.field private final mContext:Landroid/content/Context;

.field private mDialog:Landroid/app/AlertDialog;

.field private mPasswordView:Landroid/widget/EditText;

.field private mUsernameView:Landroid/widget/EditText;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lorg/chromium/chrome/browser/ChromeHttpAuthHandler;)V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    iput-object p1, p0, Lorg/chromium/chrome/browser/LoginPrompt;->mContext:Landroid/content/Context;

    .line 36
    iput-object p2, p0, Lorg/chromium/chrome/browser/LoginPrompt;->mAuthHandler:Lorg/chromium/chrome/browser/ChromeHttpAuthHandler;

    .line 37
    invoke-direct {p0}, Lorg/chromium/chrome/browser/LoginPrompt;->createDialog()V

    .line 38
    return-void
.end method

.method static synthetic access$000(Lorg/chromium/chrome/browser/LoginPrompt;)Landroid/app/AlertDialog;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lorg/chromium/chrome/browser/LoginPrompt;->mDialog:Landroid/app/AlertDialog;

    return-object v0
.end method

.method static synthetic access$100(Lorg/chromium/chrome/browser/LoginPrompt;)Lorg/chromium/chrome/browser/ChromeHttpAuthHandler;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lorg/chromium/chrome/browser/LoginPrompt;->mAuthHandler:Lorg/chromium/chrome/browser/ChromeHttpAuthHandler;

    return-object v0
.end method

.method static synthetic access$200(Lorg/chromium/chrome/browser/LoginPrompt;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 26
    invoke-direct {p0}, Lorg/chromium/chrome/browser/LoginPrompt;->getUsername()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$300(Lorg/chromium/chrome/browser/LoginPrompt;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 26
    invoke-direct {p0}, Lorg/chromium/chrome/browser/LoginPrompt;->getPassword()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private createDialog()V
    .locals 5

    .prologue
    .line 41
    iget-object v0, p0, Lorg/chromium/chrome/browser/LoginPrompt;->mContext:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Lorg/chromium/chrome/R$layout;->http_auth_dialog:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    .line 42
    sget v0, Lorg/chromium/chrome/R$id;->username:I

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lorg/chromium/chrome/browser/LoginPrompt;->mUsernameView:Landroid/widget/EditText;

    .line 43
    sget v0, Lorg/chromium/chrome/R$id;->password:I

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lorg/chromium/chrome/browser/LoginPrompt;->mPasswordView:Landroid/widget/EditText;

    .line 44
    iget-object v0, p0, Lorg/chromium/chrome/browser/LoginPrompt;->mPasswordView:Landroid/widget/EditText;

    new-instance v1, Lorg/chromium/chrome/browser/LoginPrompt$1;

    invoke-direct {v1, p0}, Lorg/chromium/chrome/browser/LoginPrompt$1;-><init>(Lorg/chromium/chrome/browser/LoginPrompt;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 55
    sget v0, Lorg/chromium/chrome/R$id;->explanation:I

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 56
    sget v1, Lorg/chromium/chrome/R$id;->username_label:I

    invoke-virtual {v3, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 57
    sget v2, Lorg/chromium/chrome/R$id;->password_label:I

    invoke-virtual {v3, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 58
    iget-object v4, p0, Lorg/chromium/chrome/browser/LoginPrompt;->mAuthHandler:Lorg/chromium/chrome/browser/ChromeHttpAuthHandler;

    invoke-virtual {v4}, Lorg/chromium/chrome/browser/ChromeHttpAuthHandler;->getMessageBody()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 59
    iget-object v0, p0, Lorg/chromium/chrome/browser/LoginPrompt;->mAuthHandler:Lorg/chromium/chrome/browser/ChromeHttpAuthHandler;

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/ChromeHttpAuthHandler;->getUsernameLabelText()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 60
    iget-object v0, p0, Lorg/chromium/chrome/browser/LoginPrompt;->mAuthHandler:Lorg/chromium/chrome/browser/ChromeHttpAuthHandler;

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/ChromeHttpAuthHandler;->getPasswordLabelText()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 62
    iget-object v0, p0, Lorg/chromium/chrome/browser/LoginPrompt;->mAuthHandler:Lorg/chromium/chrome/browser/ChromeHttpAuthHandler;

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/ChromeHttpAuthHandler;->getOkButtonText()Ljava/lang/String;

    move-result-object v0

    .line 63
    iget-object v1, p0, Lorg/chromium/chrome/browser/LoginPrompt;->mAuthHandler:Lorg/chromium/chrome/browser/ChromeHttpAuthHandler;

    invoke-virtual {v1}, Lorg/chromium/chrome/browser/ChromeHttpAuthHandler;->getCancelButtonText()Ljava/lang/String;

    move-result-object v1

    .line 65
    new-instance v2, Landroid/app/AlertDialog$Builder;

    iget-object v4, p0, Lorg/chromium/chrome/browser/LoginPrompt;->mContext:Landroid/content/Context;

    invoke-direct {v2, v4}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    iget-object v4, p0, Lorg/chromium/chrome/browser/LoginPrompt;->mAuthHandler:Lorg/chromium/chrome/browser/ChromeHttpAuthHandler;

    invoke-virtual {v4}, Lorg/chromium/chrome/browser/ChromeHttpAuthHandler;->getMessageTitle()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    new-instance v3, Lorg/chromium/chrome/browser/LoginPrompt$4;

    invoke-direct {v3, p0}, Lorg/chromium/chrome/browser/LoginPrompt$4;-><init>(Lorg/chromium/chrome/browser/LoginPrompt;)V

    invoke-virtual {v2, v0, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v2, Lorg/chromium/chrome/browser/LoginPrompt$3;

    invoke-direct {v2, p0}, Lorg/chromium/chrome/browser/LoginPrompt$3;-><init>(Lorg/chromium/chrome/browser/LoginPrompt;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v1, Lorg/chromium/chrome/browser/LoginPrompt$2;

    invoke-direct {v1, p0}, Lorg/chromium/chrome/browser/LoginPrompt$2;-><init>(Lorg/chromium/chrome/browser/LoginPrompt;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lorg/chromium/chrome/browser/LoginPrompt;->mDialog:Landroid/app/AlertDialog;

    .line 89
    iget-object v0, p0, Lorg/chromium/chrome/browser/LoginPrompt;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/Window;->setSoftInputMode(I)V

    .line 90
    return-void
.end method

.method private getPassword()Ljava/lang/String;
    .locals 1

    .prologue
    .line 105
    iget-object v0, p0, Lorg/chromium/chrome/browser/LoginPrompt;->mPasswordView:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getUsername()Ljava/lang/String;
    .locals 1

    .prologue
    .line 101
    iget-object v0, p0, Lorg/chromium/chrome/browser/LoginPrompt;->mUsernameView:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public onAutofillDataAvailable(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 110
    iget-object v0, p0, Lorg/chromium/chrome/browser/LoginPrompt;->mUsernameView:Landroid/widget/EditText;

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 111
    iget-object v0, p0, Lorg/chromium/chrome/browser/LoginPrompt;->mPasswordView:Landroid/widget/EditText;

    invoke-virtual {v0, p2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 112
    iget-object v0, p0, Lorg/chromium/chrome/browser/LoginPrompt;->mUsernameView:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->selectAll()V

    .line 113
    return-void
.end method

.method public show()V
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lorg/chromium/chrome/browser/LoginPrompt;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 97
    iget-object v0, p0, Lorg/chromium/chrome/browser/LoginPrompt;->mUsernameView:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    .line 98
    return-void
.end method
