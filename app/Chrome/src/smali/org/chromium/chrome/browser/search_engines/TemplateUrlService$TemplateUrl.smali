.class public Lorg/chromium/chrome/browser/search_engines/TemplateUrlService$TemplateUrl;
.super Ljava/lang/Object;
.source "TemplateUrlService.java"


# instance fields
.field private final mIndex:I

.field private final mKeyword:Ljava/lang/String;

.field private final mShortName:Ljava/lang/String;


# direct methods
.method public constructor <init>(ILjava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    iput p1, p0, Lorg/chromium/chrome/browser/search_engines/TemplateUrlService$TemplateUrl;->mIndex:I

    .line 47
    iput-object p2, p0, Lorg/chromium/chrome/browser/search_engines/TemplateUrlService$TemplateUrl;->mShortName:Ljava/lang/String;

    .line 48
    iput-object p3, p0, Lorg/chromium/chrome/browser/search_engines/TemplateUrlService$TemplateUrl;->mKeyword:Ljava/lang/String;

    .line 49
    return-void
.end method

.method public static create(ILjava/lang/String;Ljava/lang/String;)Lorg/chromium/chrome/browser/search_engines/TemplateUrlService$TemplateUrl;
    .locals 1

    .prologue
    .line 42
    new-instance v0, Lorg/chromium/chrome/browser/search_engines/TemplateUrlService$TemplateUrl;

    invoke-direct {v0, p0, p1, p2}, Lorg/chromium/chrome/browser/search_engines/TemplateUrlService$TemplateUrl;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public getIndex()I
    .locals 1

    .prologue
    .line 52
    iget v0, p0, Lorg/chromium/chrome/browser/search_engines/TemplateUrlService$TemplateUrl;->mIndex:I

    return v0
.end method

.method public getKeyword()Ljava/lang/String;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lorg/chromium/chrome/browser/search_engines/TemplateUrlService$TemplateUrl;->mKeyword:Ljava/lang/String;

    return-object v0
.end method

.method public getShortName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lorg/chromium/chrome/browser/search_engines/TemplateUrlService$TemplateUrl;->mShortName:Ljava/lang/String;

    return-object v0
.end method
