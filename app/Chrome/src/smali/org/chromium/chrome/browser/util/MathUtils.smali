.class public Lorg/chromium/chrome/browser/util/MathUtils;
.super Ljava/lang/Object;
.source "MathUtils.java"


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static clamp(FFF)F
    .locals 3

    .prologue
    .line 61
    cmpl-float v0, p1, p2

    if-lez v0, :cond_1

    move v0, p2

    .line 62
    :goto_0
    cmpl-float v1, p1, p2

    if-lez v1, :cond_2

    move v1, p1

    .line 63
    :goto_1
    cmpg-float v2, p0, v0

    if-gez v2, :cond_3

    move p0, v0

    .line 65
    :cond_0
    :goto_2
    return p0

    :cond_1
    move v0, p1

    .line 61
    goto :goto_0

    :cond_2
    move v1, p2

    .line 62
    goto :goto_1

    .line 64
    :cond_3
    cmpl-float v0, p0, v1

    if-lez v0, :cond_0

    move p0, v1

    goto :goto_2
.end method

.method public static clamp(III)I
    .locals 2

    .prologue
    .line 25
    if-le p1, p2, :cond_1

    move v0, p2

    .line 26
    :goto_0
    if-le p1, p2, :cond_2

    move v1, p1

    .line 27
    :goto_1
    if-ge p0, v0, :cond_3

    move p0, v0

    .line 29
    :cond_0
    :goto_2
    return p0

    :cond_1
    move v0, p1

    .line 25
    goto :goto_0

    :cond_2
    move v1, p2

    .line 26
    goto :goto_1

    .line 28
    :cond_3
    if-le p0, v1, :cond_0

    move p0, v1

    goto :goto_2
.end method

.method public static clamp(JJJ)J
    .locals 6

    .prologue
    .line 43
    cmp-long v0, p2, p4

    if-lez v0, :cond_1

    move-wide v0, p4

    .line 44
    :goto_0
    cmp-long v2, p2, p4

    if-lez v2, :cond_2

    move-wide v2, p2

    .line 45
    :goto_1
    cmp-long v4, p0, v0

    if-gez v4, :cond_3

    move-wide p0, v0

    .line 47
    :cond_0
    :goto_2
    return-wide p0

    :cond_1
    move-wide v0, p2

    .line 43
    goto :goto_0

    :cond_2
    move-wide v2, p4

    .line 44
    goto :goto_1

    .line 46
    :cond_3
    cmp-long v0, p0, v2

    if-lez v0, :cond_0

    move-wide p0, v2

    goto :goto_2
.end method

.method public static positiveModulo(II)I
    .locals 1

    .prologue
    .line 73
    rem-int v0, p0, p1

    .line 74
    if-ltz v0, :cond_0

    :goto_0
    return v0

    :cond_0
    add-int/2addr v0, p1

    goto :goto_0
.end method
