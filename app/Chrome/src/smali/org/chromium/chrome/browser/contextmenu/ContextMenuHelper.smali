.class public Lorg/chromium/chrome/browser/contextmenu/ContextMenuHelper;
.super Ljava/lang/Object;
.source "ContextMenuHelper.java"

# interfaces
.implements Landroid/view/MenuItem$OnMenuItemClickListener;
.implements Landroid/view/View$OnCreateContextMenuListener;


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private mCurrentContextMenuParams:Lorg/chromium/chrome/browser/contextmenu/ContextMenuParams;

.field private mNativeContextMenuHelper:J

.field private mPopulator:Lorg/chromium/chrome/browser/contextmenu/ContextMenuPopulator;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 23
    const-class v0, Lorg/chromium/chrome/browser/contextmenu/ContextMenuHelper;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/chromium/chrome/browser/contextmenu/ContextMenuHelper;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private constructor <init>(J)V
    .locals 1

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput-wide p1, p0, Lorg/chromium/chrome/browser/contextmenu/ContextMenuHelper;->mNativeContextMenuHelper:J

    .line 31
    return-void
.end method

.method private static create(J)Lorg/chromium/chrome/browser/contextmenu/ContextMenuHelper;
    .locals 2

    .prologue
    .line 35
    new-instance v0, Lorg/chromium/chrome/browser/contextmenu/ContextMenuHelper;

    invoke-direct {v0, p0, p1}, Lorg/chromium/chrome/browser/contextmenu/ContextMenuHelper;-><init>(J)V

    return-object v0
.end method

.method private destroy()V
    .locals 2

    .prologue
    .line 40
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lorg/chromium/chrome/browser/contextmenu/ContextMenuHelper;->mNativeContextMenuHelper:J

    .line 41
    return-void
.end method

.method private native nativeOnCustomItemSelected(JI)V
.end method

.method private native nativeOnStartDownload(JZ)V
.end method

.method private setPopulator(Lorg/chromium/chrome/browser/contextmenu/ContextMenuPopulator;)V
    .locals 0

    .prologue
    .line 49
    iput-object p1, p0, Lorg/chromium/chrome/browser/contextmenu/ContextMenuHelper;->mPopulator:Lorg/chromium/chrome/browser/contextmenu/ContextMenuPopulator;

    .line 50
    return-void
.end method

.method private shouldShowMenu(Lorg/chromium/chrome/browser/contextmenu/ContextMenuParams;)Z
    .locals 1

    .prologue
    .line 125
    invoke-virtual {p1}, Lorg/chromium/chrome/browser/contextmenu/ContextMenuParams;->isCustomMenu()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lorg/chromium/chrome/browser/contextmenu/ContextMenuHelper;->mPopulator:Lorg/chromium/chrome/browser/contextmenu/ContextMenuPopulator;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lorg/chromium/chrome/browser/contextmenu/ContextMenuHelper;->mPopulator:Lorg/chromium/chrome/browser/contextmenu/ContextMenuPopulator;

    invoke-interface {v0, p1}, Lorg/chromium/chrome/browser/contextmenu/ContextMenuPopulator;->shouldShowContextMenu(Lorg/chromium/chrome/browser/contextmenu/ContextMenuParams;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private showContextMenu(Lorg/chromium/content/browser/ContentViewCore;Lorg/chromium/chrome/browser/contextmenu/ContextMenuParams;)V
    .locals 2

    .prologue
    .line 59
    invoke-virtual {p1}, Lorg/chromium/content/browser/ContentViewCore;->getContainerView()Landroid/view/ViewGroup;

    move-result-object v0

    .line 61
    invoke-direct {p0, p2}, Lorg/chromium/chrome/browser/contextmenu/ContextMenuHelper;->shouldShowMenu(Lorg/chromium/chrome/browser/contextmenu/ContextMenuParams;)Z

    move-result v1

    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    if-nez v1, :cond_1

    .line 74
    :cond_0
    :goto_0
    return-void

    .line 68
    :cond_1
    iput-object p2, p0, Lorg/chromium/chrome/browser/contextmenu/ContextMenuHelper;->mCurrentContextMenuParams:Lorg/chromium/chrome/browser/contextmenu/ContextMenuParams;

    .line 70
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->performHapticFeedback(I)Z

    .line 71
    invoke-virtual {p1}, Lorg/chromium/content/browser/ContentViewCore;->setIgnoreRemainingTouchEvents()V

    .line 72
    invoke-virtual {v0, p0}, Landroid/view/View;->setOnCreateContextMenuListener(Landroid/view/View$OnCreateContextMenuListener;)V

    .line 73
    invoke-virtual {v0}, Landroid/view/View;->showContextMenu()Z

    goto :goto_0
.end method


# virtual methods
.method public getPopulator()Lorg/chromium/chrome/browser/contextmenu/ContextMenuPopulator;
    .locals 1

    .prologue
    .line 120
    iget-object v0, p0, Lorg/chromium/chrome/browser/contextmenu/ContextMenuHelper;->mPopulator:Lorg/chromium/chrome/browser/contextmenu/ContextMenuPopulator;

    return-object v0
.end method

.method public onCreateContextMenu(Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 86
    iget-object v0, p0, Lorg/chromium/chrome/browser/contextmenu/ContextMenuHelper;->mCurrentContextMenuParams:Lorg/chromium/chrome/browser/contextmenu/ContextMenuParams;

    invoke-direct {p0, v0}, Lorg/chromium/chrome/browser/contextmenu/ContextMenuHelper;->shouldShowMenu(Lorg/chromium/chrome/browser/contextmenu/ContextMenuParams;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 100
    :cond_0
    return-void

    .line 88
    :cond_1
    iget-object v0, p0, Lorg/chromium/chrome/browser/contextmenu/ContextMenuHelper;->mCurrentContextMenuParams:Lorg/chromium/chrome/browser/contextmenu/ContextMenuParams;

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/contextmenu/ContextMenuParams;->isCustomMenu()Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    .line 89
    :goto_0
    iget-object v2, p0, Lorg/chromium/chrome/browser/contextmenu/ContextMenuHelper;->mCurrentContextMenuParams:Lorg/chromium/chrome/browser/contextmenu/ContextMenuParams;

    invoke-virtual {v2}, Lorg/chromium/chrome/browser/contextmenu/ContextMenuParams;->getCustomMenuSize()I

    move-result v2

    if-ge v0, v2, :cond_4

    .line 90
    iget-object v2, p0, Lorg/chromium/chrome/browser/contextmenu/ContextMenuHelper;->mCurrentContextMenuParams:Lorg/chromium/chrome/browser/contextmenu/ContextMenuParams;

    invoke-virtual {v2, v0}, Lorg/chromium/chrome/browser/contextmenu/ContextMenuParams;->getCustomLabelAt(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1, v1, v0, v1, v2}, Landroid/view/ContextMenu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 89
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 93
    :cond_2
    sget-boolean v0, Lorg/chromium/chrome/browser/contextmenu/ContextMenuHelper;->$assertionsDisabled:Z

    if-nez v0, :cond_3

    iget-object v0, p0, Lorg/chromium/chrome/browser/contextmenu/ContextMenuHelper;->mPopulator:Lorg/chromium/chrome/browser/contextmenu/ContextMenuPopulator;

    if-nez v0, :cond_3

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 94
    :cond_3
    iget-object v0, p0, Lorg/chromium/chrome/browser/contextmenu/ContextMenuHelper;->mPopulator:Lorg/chromium/chrome/browser/contextmenu/ContextMenuPopulator;

    invoke-virtual {p2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lorg/chromium/chrome/browser/contextmenu/ContextMenuHelper;->mCurrentContextMenuParams:Lorg/chromium/chrome/browser/contextmenu/ContextMenuParams;

    invoke-interface {v0, p1, v2, v3}, Lorg/chromium/chrome/browser/contextmenu/ContextMenuPopulator;->buildContextMenu(Landroid/view/ContextMenu;Landroid/content/Context;Lorg/chromium/chrome/browser/contextmenu/ContextMenuParams;)V

    .line 97
    :cond_4
    :goto_1
    invoke-interface {p1}, Landroid/view/ContextMenu;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 98
    invoke-interface {p1, v1}, Landroid/view/ContextMenu;->getItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, p0}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 97
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method public onMenuItemClick(Landroid/view/MenuItem;)Z
    .locals 4

    .prologue
    .line 104
    iget-object v0, p0, Lorg/chromium/chrome/browser/contextmenu/ContextMenuHelper;->mCurrentContextMenuParams:Lorg/chromium/chrome/browser/contextmenu/ContextMenuParams;

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/contextmenu/ContextMenuParams;->isCustomMenu()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 105
    iget-wide v0, p0, Lorg/chromium/chrome/browser/contextmenu/ContextMenuHelper;->mNativeContextMenuHelper:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 106
    iget-object v0, p0, Lorg/chromium/chrome/browser/contextmenu/ContextMenuHelper;->mCurrentContextMenuParams:Lorg/chromium/chrome/browser/contextmenu/ContextMenuParams;

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    invoke-virtual {v0, v1}, Lorg/chromium/chrome/browser/contextmenu/ContextMenuParams;->getCustomActionAt(I)I

    move-result v0

    .line 107
    iget-wide v2, p0, Lorg/chromium/chrome/browser/contextmenu/ContextMenuHelper;->mNativeContextMenuHelper:J

    invoke-direct {p0, v2, v3, v0}, Lorg/chromium/chrome/browser/contextmenu/ContextMenuHelper;->nativeOnCustomItemSelected(JI)V

    .line 109
    :cond_0
    const/4 v0, 0x1

    .line 111
    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, Lorg/chromium/chrome/browser/contextmenu/ContextMenuHelper;->mPopulator:Lorg/chromium/chrome/browser/contextmenu/ContextMenuPopulator;

    iget-object v1, p0, Lorg/chromium/chrome/browser/contextmenu/ContextMenuHelper;->mCurrentContextMenuParams:Lorg/chromium/chrome/browser/contextmenu/ContextMenuParams;

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v2

    invoke-interface {v0, p0, v1, v2}, Lorg/chromium/chrome/browser/contextmenu/ContextMenuPopulator;->onItemSelected(Lorg/chromium/chrome/browser/contextmenu/ContextMenuHelper;Lorg/chromium/chrome/browser/contextmenu/ContextMenuParams;I)Z

    move-result v0

    goto :goto_0
.end method

.method public startContextMenuDownload(Z)V
    .locals 4

    .prologue
    .line 81
    iget-wide v0, p0, Lorg/chromium/chrome/browser/contextmenu/ContextMenuHelper;->mNativeContextMenuHelper:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lorg/chromium/chrome/browser/contextmenu/ContextMenuHelper;->mNativeContextMenuHelper:J

    invoke-direct {p0, v0, v1, p1}, Lorg/chromium/chrome/browser/contextmenu/ContextMenuHelper;->nativeOnStartDownload(JZ)V

    .line 82
    :cond_0
    return-void
.end method
