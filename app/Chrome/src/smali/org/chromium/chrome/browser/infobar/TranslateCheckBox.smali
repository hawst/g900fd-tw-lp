.class public Lorg/chromium/chrome/browser/infobar/TranslateCheckBox;
.super Landroid/widget/CheckBox;
.source "TranslateCheckBox.java"

# interfaces
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# instance fields
.field private final mListener:Lorg/chromium/chrome/browser/infobar/SubPanelListener;

.field private final mOptions:Lorg/chromium/chrome/browser/infobar/TranslateOptions;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lorg/chromium/chrome/browser/infobar/TranslateOptions;Lorg/chromium/chrome/browser/infobar/SubPanelListener;)V
    .locals 4

    .prologue
    .line 23
    invoke-direct {p0, p1}, Landroid/widget/CheckBox;-><init>(Landroid/content/Context;)V

    .line 24
    iput-object p2, p0, Lorg/chromium/chrome/browser/infobar/TranslateCheckBox;->mOptions:Lorg/chromium/chrome/browser/infobar/TranslateOptions;

    .line 25
    iput-object p3, p0, Lorg/chromium/chrome/browser/infobar/TranslateCheckBox;->mListener:Lorg/chromium/chrome/browser/infobar/SubPanelListener;

    .line 27
    sget v0, Lorg/chromium/chrome/R$id;->infobar_extra_check:I

    invoke-virtual {p0, v0}, Lorg/chromium/chrome/browser/infobar/TranslateCheckBox;->setId(I)V

    .line 28
    sget v0, Lorg/chromium/chrome/R$string;->translate_always_text:I

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lorg/chromium/chrome/browser/infobar/TranslateCheckBox;->mOptions:Lorg/chromium/chrome/browser/infobar/TranslateOptions;

    invoke-virtual {v3}, Lorg/chromium/chrome/browser/infobar/TranslateOptions;->sourceLanguage()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lorg/chromium/chrome/browser/infobar/TranslateCheckBox;->setText(Ljava/lang/CharSequence;)V

    .line 29
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lorg/chromium/chrome/R$color;->infobar_text:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p0, v0}, Lorg/chromium/chrome/browser/infobar/TranslateCheckBox;->setTextColor(I)V

    .line 30
    const/high16 v0, 0x41500000    # 13.0f

    invoke-virtual {p0, v0}, Lorg/chromium/chrome/browser/infobar/TranslateCheckBox;->setTextSize(F)V

    .line 31
    iget-object v0, p0, Lorg/chromium/chrome/browser/infobar/TranslateCheckBox;->mOptions:Lorg/chromium/chrome/browser/infobar/TranslateOptions;

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/infobar/TranslateOptions;->alwaysTranslateLanguageState()Z

    move-result v0

    invoke-virtual {p0, v0}, Lorg/chromium/chrome/browser/infobar/TranslateCheckBox;->setChecked(Z)V

    .line 32
    invoke-virtual {p0, p0}, Lorg/chromium/chrome/browser/infobar/TranslateCheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 33
    return-void
.end method


# virtual methods
.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 2

    .prologue
    .line 37
    iget-object v0, p0, Lorg/chromium/chrome/browser/infobar/TranslateCheckBox;->mOptions:Lorg/chromium/chrome/browser/infobar/TranslateOptions;

    invoke-virtual {v0, p2}, Lorg/chromium/chrome/browser/infobar/TranslateOptions;->toggleAlwaysTranslateLanguageState(Z)Z

    .line 38
    if-eqz p2, :cond_0

    .line 39
    iget-object v0, p0, Lorg/chromium/chrome/browser/infobar/TranslateCheckBox;->mListener:Lorg/chromium/chrome/browser/infobar/SubPanelListener;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lorg/chromium/chrome/browser/infobar/SubPanelListener;->onPanelClosed(I)V

    .line 43
    :goto_0
    return-void

    .line 41
    :cond_0
    iget-object v0, p0, Lorg/chromium/chrome/browser/infobar/TranslateCheckBox;->mListener:Lorg/chromium/chrome/browser/infobar/SubPanelListener;

    invoke-interface {v0}, Lorg/chromium/chrome/browser/infobar/SubPanelListener;->onOptionsChanged()V

    goto :goto_0
.end method
