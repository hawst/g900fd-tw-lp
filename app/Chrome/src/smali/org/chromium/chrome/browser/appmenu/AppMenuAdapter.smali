.class Lorg/chromium/chrome/browser/appmenu/AppMenuAdapter;
.super Landroid/widget/BaseAdapter;
.source "AppMenuAdapter.java"


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private final mAppMenu:Lorg/chromium/chrome/browser/appmenu/AppMenu;

.field private final mDpToPx:F

.field private final mInflater:Landroid/view/LayoutInflater;

.field private final mMenuItems:Ljava/util/List;

.field private final mNumMenuItems:I

.field private final mShowMenuButton:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 35
    const-class v0, Lorg/chromium/chrome/browser/appmenu/AppMenuAdapter;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/chromium/chrome/browser/appmenu/AppMenuAdapter;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lorg/chromium/chrome/browser/appmenu/AppMenu;Ljava/util/List;Landroid/view/LayoutInflater;Z)V
    .locals 1

    .prologue
    .line 87
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 88
    iput-object p1, p0, Lorg/chromium/chrome/browser/appmenu/AppMenuAdapter;->mAppMenu:Lorg/chromium/chrome/browser/appmenu/AppMenu;

    .line 89
    iput-object p2, p0, Lorg/chromium/chrome/browser/appmenu/AppMenuAdapter;->mMenuItems:Ljava/util/List;

    .line 90
    iput-object p3, p0, Lorg/chromium/chrome/browser/appmenu/AppMenuAdapter;->mInflater:Landroid/view/LayoutInflater;

    .line 91
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    iput v0, p0, Lorg/chromium/chrome/browser/appmenu/AppMenuAdapter;->mNumMenuItems:I

    .line 92
    iput-boolean p4, p0, Lorg/chromium/chrome/browser/appmenu/AppMenuAdapter;->mShowMenuButton:Z

    .line 93
    invoke-virtual {p3}, Landroid/view/LayoutInflater;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    iput v0, p0, Lorg/chromium/chrome/browser/appmenu/AppMenuAdapter;->mDpToPx:F

    .line 94
    return-void
.end method

.method static synthetic access$000(Lorg/chromium/chrome/browser/appmenu/AppMenuAdapter;)Lorg/chromium/chrome/browser/appmenu/AppMenu;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lorg/chromium/chrome/browser/appmenu/AppMenuAdapter;->mAppMenu:Lorg/chromium/chrome/browser/appmenu/AppMenu;

    return-object v0
.end method

.method private buildIconItemEnterAnimator([Landroid/widget/ImageView;Z)Landroid/animation/Animator;
    .locals 13

    .prologue
    .line 380
    invoke-static {}, Lorg/chromium/ui/base/LocalizationUtils;->isLayoutRtl()Z

    move-result v0

    .line 381
    const/high16 v1, 0x41200000    # 10.0f

    iget v2, p0, Lorg/chromium/chrome/browser/appmenu/AppMenuAdapter;->mDpToPx:F

    mul-float/2addr v1, v2

    if-eqz v0, :cond_0

    const/high16 v0, -0x40800000    # -1.0f

    :goto_0
    mul-float v2, v1, v0

    .line 382
    array-length v1, p1

    if-eqz p2, :cond_1

    const/4 v0, 0x1

    :goto_1
    sub-int v3, v1, v0

    .line 384
    new-instance v4, Landroid/animation/AnimatorSet;

    invoke-direct {v4}, Landroid/animation/AnimatorSet;-><init>()V

    .line 385
    const/4 v1, 0x0

    .line 386
    const/4 v0, 0x0

    move v12, v0

    move-object v0, v1

    move v1, v12

    :goto_2
    if-ge v1, v3, :cond_3

    .line 387
    mul-int/lit8 v5, v1, 0x1e

    .line 389
    aget-object v6, p1, v1

    sget-object v7, Landroid/view/View;->ALPHA:Landroid/util/Property;

    const/4 v8, 0x2

    new-array v8, v8, [F

    fill-array-data v8, :array_0

    invoke-static {v6, v7, v8}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v6

    .line 390
    aget-object v7, p1, v1

    sget-object v8, Landroid/view/View;->TRANSLATION_X:Landroid/util/Property;

    const/4 v9, 0x2

    new-array v9, v9, [F

    const/4 v10, 0x0

    aput v2, v9, v10

    const/4 v10, 0x1

    const/4 v11, 0x0

    aput v11, v9, v10

    invoke-static {v7, v8, v9}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v7

    .line 391
    int-to-long v8, v5

    invoke-virtual {v6, v8, v9}, Landroid/animation/Animator;->setStartDelay(J)V

    .line 392
    int-to-long v8, v5

    invoke-virtual {v7, v8, v9}, Landroid/animation/Animator;->setStartDelay(J)V

    .line 393
    const-wide/16 v8, 0x15e

    invoke-virtual {v6, v8, v9}, Landroid/animation/Animator;->setDuration(J)Landroid/animation/Animator;

    .line 394
    const-wide/16 v8, 0x15e

    invoke-virtual {v7, v8, v9}, Landroid/animation/Animator;->setDuration(J)Landroid/animation/Animator;

    .line 396
    if-nez v0, :cond_2

    .line 397
    invoke-virtual {v4, v6}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v0

    .line 401
    :goto_3
    invoke-virtual {v0, v7}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 386
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 381
    :cond_0
    const/high16 v0, 0x3f800000    # 1.0f

    goto :goto_0

    .line 382
    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 399
    :cond_2
    invoke-virtual {v0, v6}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    goto :goto_3

    .line 403
    :cond_3
    const-wide/16 v0, 0x50

    invoke-virtual {v4, v0, v1}, Landroid/animation/AnimatorSet;->setStartDelay(J)V

    .line 404
    sget-object v0, Lorg/chromium/ui/interpolators/BakedBezierInterpolator;->FADE_IN_CURVE:Lorg/chromium/ui/interpolators/BakedBezierInterpolator;

    invoke-virtual {v4, v0}, Landroid/animation/AnimatorSet;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 406
    new-instance v0, Lorg/chromium/chrome/browser/appmenu/AppMenuAdapter$6;

    invoke-direct {v0, p0, v3, p1}, Lorg/chromium/chrome/browser/appmenu/AppMenuAdapter$6;-><init>(Lorg/chromium/chrome/browser/appmenu/AppMenuAdapter;I[Landroid/widget/ImageView;)V

    invoke-virtual {v4, v0}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 414
    return-object v4

    .line 389
    nop

    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method

.method private buildStandardItemEnterAnimator(Landroid/view/View;I)Landroid/animation/Animator;
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    const/4 v6, 0x2

    .line 351
    const/high16 v0, -0x3ee00000    # -10.0f

    iget v1, p0, Lorg/chromium/chrome/browser/appmenu/AppMenuAdapter;->mDpToPx:F

    mul-float/2addr v0, v1

    .line 352
    mul-int/lit8 v1, p2, 0x1e

    add-int/lit8 v1, v1, 0x50

    .line 354
    new-instance v2, Landroid/animation/AnimatorSet;

    invoke-direct {v2}, Landroid/animation/AnimatorSet;-><init>()V

    .line 355
    new-array v3, v6, [Landroid/animation/Animator;

    sget-object v4, Landroid/view/View;->ALPHA:Landroid/util/Property;

    new-array v5, v6, [F

    fill-array-data v5, :array_0

    invoke-static {p1, v4, v5}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v4

    aput-object v4, v3, v7

    sget-object v4, Landroid/view/View;->TRANSLATION_Y:Landroid/util/Property;

    new-array v5, v6, [F

    aput v0, v5, v7

    const/4 v0, 0x0

    aput v0, v5, v8

    invoke-static {p1, v4, v5}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    aput-object v0, v3, v8

    invoke-virtual {v2, v3}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    .line 358
    const-wide/16 v4, 0x15e

    invoke-virtual {v2, v4, v5}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    .line 359
    int-to-long v0, v1

    invoke-virtual {v2, v0, v1}, Landroid/animation/AnimatorSet;->setStartDelay(J)V

    .line 360
    sget-object v0, Lorg/chromium/ui/interpolators/BakedBezierInterpolator;->FADE_IN_CURVE:Lorg/chromium/ui/interpolators/BakedBezierInterpolator;

    invoke-virtual {v2, v0}, Landroid/animation/AnimatorSet;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 362
    new-instance v0, Lorg/chromium/chrome/browser/appmenu/AppMenuAdapter$5;

    invoke-direct {v0, p0, p1}, Lorg/chromium/chrome/browser/appmenu/AppMenuAdapter$5;-><init>(Lorg/chromium/chrome/browser/appmenu/AppMenuAdapter;Landroid/view/View;)V

    invoke-virtual {v2, v0}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 368
    return-object v2

    .line 355
    nop

    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method

.method private setupImageButton(Landroid/widget/ImageButton;Landroid/view/MenuItem;)V
    .locals 2

    .prologue
    .line 308
    invoke-interface {p2}, Landroid/view/MenuItem;->getIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getLevel()I

    move-result v0

    .line 309
    invoke-interface {p2}, Landroid/view/MenuItem;->getIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/widget/ImageButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 310
    invoke-interface {p2}, Landroid/view/MenuItem;->getIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/graphics/drawable/Drawable;->setLevel(I)Z

    .line 311
    invoke-interface {p2}, Landroid/view/MenuItem;->getTitle()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 312
    invoke-interface {p2}, Landroid/view/MenuItem;->isEnabled()Z

    move-result v0

    invoke-virtual {p1, v0}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 313
    invoke-interface {p2}, Landroid/view/MenuItem;->isEnabled()Z

    move-result v0

    invoke-virtual {p1, v0}, Landroid/widget/ImageButton;->setFocusable(Z)V

    .line 314
    new-instance v0, Lorg/chromium/chrome/browser/appmenu/AppMenuAdapter$3;

    invoke-direct {v0, p0, p2}, Lorg/chromium/chrome/browser/appmenu/AppMenuAdapter$3;-><init>(Lorg/chromium/chrome/browser/appmenu/AppMenuAdapter;Landroid/view/MenuItem;)V

    invoke-virtual {p1, v0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 320
    return-void
.end method

.method private setupMenuButton(Landroid/widget/ImageButton;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 323
    sget v0, Lorg/chromium/chrome/R$drawable;->btn_menu_pressed:I

    invoke-virtual {p1, v0}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 324
    invoke-virtual {p1}, Landroid/widget/ImageButton;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lorg/chromium/chrome/R$string;->menu_dismiss_btn:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 325
    invoke-virtual {p1, v3}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 326
    invoke-virtual {p1, v3}, Landroid/widget/ImageButton;->setFocusable(Z)V

    .line 327
    new-instance v0, Lorg/chromium/chrome/browser/appmenu/AppMenuAdapter$4;

    invoke-direct {v0, p0}, Lorg/chromium/chrome/browser/appmenu/AppMenuAdapter$4;-><init>(Lorg/chromium/chrome/browser/appmenu/AppMenuAdapter;)V

    invoke-virtual {p1, v0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 335
    const/high16 v0, 0x41a80000    # 21.0f

    iget v1, p0, Lorg/chromium/chrome/browser/appmenu/AppMenuAdapter;->mDpToPx:F

    mul-float/2addr v0, v1

    float-to-int v0, v0

    invoke-static {p1, v0, v2, v2, v2}, Lorg/chromium/base/ApiCompatibilityUtils;->setPaddingRelative(Landroid/view/View;IIII)V

    .line 337
    invoke-virtual {p1}, Landroid/widget/ImageButton;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    const/high16 v1, 0x426c0000    # 59.0f

    iget v2, p0, Lorg/chromium/chrome/browser/appmenu/AppMenuAdapter;->mDpToPx:F

    mul-float/2addr v1, v2

    float-to-int v1, v1

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 338
    invoke-virtual {p1}, Landroid/widget/ImageButton;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v1, 0x0

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    .line 339
    sget-object v0, Landroid/widget/ImageView$ScaleType;->CENTER:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {p1, v0}, Landroid/widget/ImageButton;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 340
    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 98
    iget v0, p0, Lorg/chromium/chrome/browser/appmenu/AppMenuAdapter;->mNumMenuItems:I

    return v0
.end method

.method public getItem(I)Landroid/view/MenuItem;
    .locals 1

    .prologue
    .line 135
    const/4 v0, -0x1

    if-ne p1, v0, :cond_0

    const/4 v0, 0x0

    .line 138
    :goto_0
    return-object v0

    .line 136
    :cond_0
    sget-boolean v0, Lorg/chromium/chrome/browser/appmenu/AppMenuAdapter;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    if-gez p1, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 137
    :cond_1
    sget-boolean v0, Lorg/chromium/chrome/browser/appmenu/AppMenuAdapter;->$assertionsDisabled:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lorg/chromium/chrome/browser/appmenu/AppMenuAdapter;->mMenuItems:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lt p1, v0, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 138
    :cond_2
    iget-object v0, p0, Lorg/chromium/chrome/browser/appmenu/AppMenuAdapter;->mMenuItems:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/MenuItem;

    goto :goto_0
.end method

.method public bridge synthetic getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 35
    invoke-virtual {p0, p1}, Lorg/chromium/chrome/browser/appmenu/AppMenuAdapter;->getItem(I)Landroid/view/MenuItem;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2

    .prologue
    .line 130
    invoke-virtual {p0, p1}, Lorg/chromium/chrome/browser/appmenu/AppMenuAdapter;->getItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    int-to-long v0, v0

    return-wide v0
.end method

.method public getItemViewType(I)I
    .locals 8

    .prologue
    const/4 v3, 0x4

    const/4 v4, 0x3

    const/4 v5, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 108
    invoke-virtual {p0, p1}, Lorg/chromium/chrome/browser/appmenu/AppMenuAdapter;->getItem(I)Landroid/view/MenuItem;

    move-result-object v7

    .line 109
    iget-boolean v0, p0, Lorg/chromium/chrome/browser/appmenu/AppMenuAdapter;->mShowMenuButton:Z

    if-eqz v0, :cond_2

    if-nez p1, :cond_2

    move v0, v1

    .line 110
    :goto_0
    invoke-interface {v7}, Landroid/view/MenuItem;->hasSubMenu()Z

    move-result v6

    if-eqz v6, :cond_3

    invoke-interface {v7}, Landroid/view/MenuItem;->getSubMenu()Landroid/view/SubMenu;

    move-result-object v6

    invoke-interface {v6}, Landroid/view/SubMenu;->size()I

    move-result v6

    .line 111
    :goto_1
    if-eqz v0, :cond_0

    add-int/lit8 v6, v6, 0x1

    .line 113
    :cond_0
    if-ne v6, v3, :cond_4

    move v1, v3

    .line 125
    :cond_1
    :goto_2
    return v1

    :cond_2
    move v0, v2

    .line 109
    goto :goto_0

    :cond_3
    move v6, v1

    .line 110
    goto :goto_1

    .line 115
    :cond_4
    if-ne v6, v4, :cond_5

    move v1, v4

    .line 116
    goto :goto_2

    .line 117
    :cond_5
    if-ne v6, v5, :cond_9

    .line 118
    if-nez p1, :cond_8

    iget-boolean v3, p0, Lorg/chromium/chrome/browser/appmenu/AppMenuAdapter;->mShowMenuButton:Z

    if-nez v3, :cond_6

    invoke-interface {v7}, Landroid/view/MenuItem;->getSubMenu()Landroid/view/SubMenu;

    move-result-object v3

    invoke-interface {v3, v2}, Landroid/view/SubMenu;->getItem(I)Landroid/view/MenuItem;

    move-result-object v2

    invoke-interface {v2}, Landroid/view/MenuItem;->getIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    if-nez v2, :cond_7

    :cond_6
    if-eqz v0, :cond_8

    invoke-interface {v7}, Landroid/view/MenuItem;->getIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    if-eqz v2, :cond_8

    :cond_7
    move v1, v5

    .line 121
    goto :goto_2

    .line 123
    :cond_8
    if-eqz v0, :cond_1

    const/4 v1, 0x5

    goto :goto_2

    :cond_9
    move v1, v2

    .line 125
    goto :goto_2
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 10

    .prologue
    const/16 v4, 0x8

    const/4 v8, 0x3

    const/4 v6, 0x2

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 143
    iget-boolean v0, p0, Lorg/chromium/chrome/browser/appmenu/AppMenuAdapter;->mShowMenuButton:Z

    if-eqz v0, :cond_0

    if-nez p1, :cond_0

    move v1, v2

    .line 144
    :goto_0
    invoke-virtual {p0, p1}, Lorg/chromium/chrome/browser/appmenu/AppMenuAdapter;->getItem(I)Landroid/view/MenuItem;

    move-result-object v7

    .line 145
    invoke-virtual {p0, p1}, Lorg/chromium/chrome/browser/appmenu/AppMenuAdapter;->getItemViewType(I)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 300
    sget-boolean v0, Lorg/chromium/chrome/browser/appmenu/AppMenuAdapter;->$assertionsDisabled:Z

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    const-string/jumbo v1, "Unexpected MenuItem type"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    :cond_0
    move v1, v3

    .line 143
    goto :goto_0

    .line 147
    :pswitch_0
    if-nez p2, :cond_3

    .line 149
    new-instance v1, Lorg/chromium/chrome/browser/appmenu/AppMenuAdapter$StandardMenuItemViewHolder;

    invoke-direct {v1}, Lorg/chromium/chrome/browser/appmenu/AppMenuAdapter$StandardMenuItemViewHolder;-><init>()V

    .line 150
    iget-object v0, p0, Lorg/chromium/chrome/browser/appmenu/AppMenuAdapter;->mInflater:Landroid/view/LayoutInflater;

    sget v2, Lorg/chromium/chrome/R$layout;->menu_item:I

    invoke-virtual {v0, v2, p3, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 151
    sget v0, Lorg/chromium/chrome/R$id;->menu_item_text:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lorg/chromium/chrome/browser/appmenu/AppMenuAdapter$StandardMenuItemViewHolder;->text:Landroid/widget/TextView;

    .line 152
    sget v0, Lorg/chromium/chrome/R$id;->menu_item_icon:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lorg/chromium/chrome/browser/appmenu/AppMenuItemIcon;

    iput-object v0, v1, Lorg/chromium/chrome/browser/appmenu/AppMenuAdapter$StandardMenuItemViewHolder;->image:Lorg/chromium/chrome/browser/appmenu/AppMenuItemIcon;

    .line 153
    invoke-virtual {p2, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 154
    sget v0, Lorg/chromium/chrome/R$id;->menu_item_enter_anim_id:I

    invoke-direct {p0, p2, p1}, Lorg/chromium/chrome/browser/appmenu/AppMenuAdapter;->buildStandardItemEnterAnimator(Landroid/view/View;I)Landroid/animation/Animator;

    move-result-object v2

    invoke-virtual {p2, v0, v2}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    move-object v0, v1

    .line 160
    :goto_1
    new-instance v1, Lorg/chromium/chrome/browser/appmenu/AppMenuAdapter$1;

    invoke-direct {v1, p0, v7}, Lorg/chromium/chrome/browser/appmenu/AppMenuAdapter$1;-><init>(Lorg/chromium/chrome/browser/appmenu/AppMenuAdapter;Landroid/view/MenuItem;)V

    invoke-virtual {p2, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 167
    invoke-interface {v7}, Landroid/view/MenuItem;->getIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 168
    iget-object v2, v0, Lorg/chromium/chrome/browser/appmenu/AppMenuAdapter$StandardMenuItemViewHolder;->image:Lorg/chromium/chrome/browser/appmenu/AppMenuItemIcon;

    invoke-virtual {v2, v1}, Lorg/chromium/chrome/browser/appmenu/AppMenuItemIcon;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 169
    iget-object v2, v0, Lorg/chromium/chrome/browser/appmenu/AppMenuAdapter$StandardMenuItemViewHolder;->image:Lorg/chromium/chrome/browser/appmenu/AppMenuItemIcon;

    if-nez v1, :cond_1

    move v3, v4

    :cond_1
    invoke-virtual {v2, v3}, Lorg/chromium/chrome/browser/appmenu/AppMenuItemIcon;->setVisibility(I)V

    .line 170
    iget-object v1, v0, Lorg/chromium/chrome/browser/appmenu/AppMenuAdapter$StandardMenuItemViewHolder;->image:Lorg/chromium/chrome/browser/appmenu/AppMenuItemIcon;

    invoke-interface {v7}, Landroid/view/MenuItem;->isChecked()Z

    move-result v2

    invoke-virtual {v1, v2}, Lorg/chromium/chrome/browser/appmenu/AppMenuItemIcon;->setChecked(Z)V

    .line 172
    iget-object v1, v0, Lorg/chromium/chrome/browser/appmenu/AppMenuAdapter$StandardMenuItemViewHolder;->text:Landroid/widget/TextView;

    invoke-interface {v7}, Landroid/view/MenuItem;->getTitle()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 173
    invoke-interface {v7}, Landroid/view/MenuItem;->isEnabled()Z

    move-result v1

    .line 175
    iget-object v0, v0, Lorg/chromium/chrome/browser/appmenu/AppMenuAdapter$StandardMenuItemViewHolder;->text:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 177
    invoke-virtual {p2, v1}, Landroid/view/View;->setEnabled(Z)V

    .line 302
    :cond_2
    :goto_2
    return-object p2

    .line 157
    :cond_3
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/chromium/chrome/browser/appmenu/AppMenuAdapter$StandardMenuItemViewHolder;

    goto :goto_1

    .line 181
    :pswitch_1
    if-nez p2, :cond_4

    .line 183
    new-instance v4, Lorg/chromium/chrome/browser/appmenu/AppMenuAdapter$TwoButtonMenuItemViewHolder;

    invoke-direct {v4}, Lorg/chromium/chrome/browser/appmenu/AppMenuAdapter$TwoButtonMenuItemViewHolder;-><init>()V

    .line 184
    iget-object v0, p0, Lorg/chromium/chrome/browser/appmenu/AppMenuAdapter;->mInflater:Landroid/view/LayoutInflater;

    sget v5, Lorg/chromium/chrome/R$layout;->two_button_menu_item:I

    invoke-virtual {v0, v5, p3, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 185
    iget-object v5, v4, Lorg/chromium/chrome/browser/appmenu/AppMenuAdapter$TwoButtonMenuItemViewHolder;->buttons:[Landroid/widget/ImageButton;

    sget v0, Lorg/chromium/chrome/R$id;->button_one:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    aput-object v0, v5, v3

    .line 186
    iget-object v5, v4, Lorg/chromium/chrome/browser/appmenu/AppMenuAdapter$TwoButtonMenuItemViewHolder;->buttons:[Landroid/widget/ImageButton;

    sget v0, Lorg/chromium/chrome/R$id;->button_two:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    aput-object v0, v5, v2

    .line 187
    invoke-virtual {p2, v4}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 188
    sget v0, Lorg/chromium/chrome/R$id;->menu_item_enter_anim_id:I

    iget-object v5, v4, Lorg/chromium/chrome/browser/appmenu/AppMenuAdapter$TwoButtonMenuItemViewHolder;->buttons:[Landroid/widget/ImageButton;

    invoke-direct {p0, v5, v1}, Lorg/chromium/chrome/browser/appmenu/AppMenuAdapter;->buildIconItemEnterAnimator([Landroid/widget/ImageView;Z)Landroid/animation/Animator;

    move-result-object v5

    invoke-virtual {p2, v0, v5}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    move-object v0, v4

    .line 193
    :goto_3
    iget-object v4, v0, Lorg/chromium/chrome/browser/appmenu/AppMenuAdapter$TwoButtonMenuItemViewHolder;->buttons:[Landroid/widget/ImageButton;

    aget-object v4, v4, v3

    invoke-interface {v7}, Landroid/view/MenuItem;->getSubMenu()Landroid/view/SubMenu;

    move-result-object v5

    invoke-interface {v5, v3}, Landroid/view/SubMenu;->getItem(I)Landroid/view/MenuItem;

    move-result-object v5

    invoke-direct {p0, v4, v5}, Lorg/chromium/chrome/browser/appmenu/AppMenuAdapter;->setupImageButton(Landroid/widget/ImageButton;Landroid/view/MenuItem;)V

    .line 194
    if-eqz v1, :cond_5

    .line 195
    iget-object v0, v0, Lorg/chromium/chrome/browser/appmenu/AppMenuAdapter$TwoButtonMenuItemViewHolder;->buttons:[Landroid/widget/ImageButton;

    aget-object v0, v0, v2

    invoke-direct {p0, v0}, Lorg/chromium/chrome/browser/appmenu/AppMenuAdapter;->setupMenuButton(Landroid/widget/ImageButton;)V

    .line 200
    :goto_4
    invoke-virtual {p2, v3}, Landroid/view/View;->setFocusable(Z)V

    .line 201
    invoke-virtual {p2, v3}, Landroid/view/View;->setEnabled(Z)V

    goto :goto_2

    .line 191
    :cond_4
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/chromium/chrome/browser/appmenu/AppMenuAdapter$TwoButtonMenuItemViewHolder;

    goto :goto_3

    .line 197
    :cond_5
    iget-object v0, v0, Lorg/chromium/chrome/browser/appmenu/AppMenuAdapter$TwoButtonMenuItemViewHolder;->buttons:[Landroid/widget/ImageButton;

    aget-object v0, v0, v2

    invoke-interface {v7}, Landroid/view/MenuItem;->getSubMenu()Landroid/view/SubMenu;

    move-result-object v1

    invoke-interface {v1, v2}, Landroid/view/SubMenu;->getItem(I)Landroid/view/MenuItem;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lorg/chromium/chrome/browser/appmenu/AppMenuAdapter;->setupImageButton(Landroid/widget/ImageButton;Landroid/view/MenuItem;)V

    goto :goto_4

    .line 205
    :pswitch_2
    if-nez p2, :cond_6

    .line 207
    new-instance v4, Lorg/chromium/chrome/browser/appmenu/AppMenuAdapter$ThreeButtonMenuItemViewHolder;

    invoke-direct {v4}, Lorg/chromium/chrome/browser/appmenu/AppMenuAdapter$ThreeButtonMenuItemViewHolder;-><init>()V

    .line 208
    iget-object v0, p0, Lorg/chromium/chrome/browser/appmenu/AppMenuAdapter;->mInflater:Landroid/view/LayoutInflater;

    sget v5, Lorg/chromium/chrome/R$layout;->three_button_menu_item:I

    invoke-virtual {v0, v5, p3, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 209
    iget-object v5, v4, Lorg/chromium/chrome/browser/appmenu/AppMenuAdapter$ThreeButtonMenuItemViewHolder;->buttons:[Landroid/widget/ImageButton;

    sget v0, Lorg/chromium/chrome/R$id;->button_one:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    aput-object v0, v5, v3

    .line 210
    iget-object v5, v4, Lorg/chromium/chrome/browser/appmenu/AppMenuAdapter$ThreeButtonMenuItemViewHolder;->buttons:[Landroid/widget/ImageButton;

    sget v0, Lorg/chromium/chrome/R$id;->button_two:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    aput-object v0, v5, v2

    .line 211
    iget-object v5, v4, Lorg/chromium/chrome/browser/appmenu/AppMenuAdapter$ThreeButtonMenuItemViewHolder;->buttons:[Landroid/widget/ImageButton;

    sget v0, Lorg/chromium/chrome/R$id;->button_three:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    aput-object v0, v5, v6

    .line 212
    invoke-virtual {p2, v4}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 213
    sget v0, Lorg/chromium/chrome/R$id;->menu_item_enter_anim_id:I

    iget-object v5, v4, Lorg/chromium/chrome/browser/appmenu/AppMenuAdapter$ThreeButtonMenuItemViewHolder;->buttons:[Landroid/widget/ImageButton;

    invoke-direct {p0, v5, v1}, Lorg/chromium/chrome/browser/appmenu/AppMenuAdapter;->buildIconItemEnterAnimator([Landroid/widget/ImageView;Z)Landroid/animation/Animator;

    move-result-object v5

    invoke-virtual {p2, v0, v5}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    move-object v0, v4

    .line 218
    :goto_5
    iget-object v4, v0, Lorg/chromium/chrome/browser/appmenu/AppMenuAdapter$ThreeButtonMenuItemViewHolder;->buttons:[Landroid/widget/ImageButton;

    aget-object v4, v4, v3

    invoke-interface {v7}, Landroid/view/MenuItem;->getSubMenu()Landroid/view/SubMenu;

    move-result-object v5

    invoke-interface {v5, v3}, Landroid/view/SubMenu;->getItem(I)Landroid/view/MenuItem;

    move-result-object v5

    invoke-direct {p0, v4, v5}, Lorg/chromium/chrome/browser/appmenu/AppMenuAdapter;->setupImageButton(Landroid/widget/ImageButton;Landroid/view/MenuItem;)V

    .line 219
    iget-object v4, v0, Lorg/chromium/chrome/browser/appmenu/AppMenuAdapter$ThreeButtonMenuItemViewHolder;->buttons:[Landroid/widget/ImageButton;

    aget-object v4, v4, v2

    invoke-interface {v7}, Landroid/view/MenuItem;->getSubMenu()Landroid/view/SubMenu;

    move-result-object v5

    invoke-interface {v5, v2}, Landroid/view/SubMenu;->getItem(I)Landroid/view/MenuItem;

    move-result-object v2

    invoke-direct {p0, v4, v2}, Lorg/chromium/chrome/browser/appmenu/AppMenuAdapter;->setupImageButton(Landroid/widget/ImageButton;Landroid/view/MenuItem;)V

    .line 220
    if-eqz v1, :cond_7

    .line 221
    iget-object v0, v0, Lorg/chromium/chrome/browser/appmenu/AppMenuAdapter$ThreeButtonMenuItemViewHolder;->buttons:[Landroid/widget/ImageButton;

    aget-object v0, v0, v6

    invoke-direct {p0, v0}, Lorg/chromium/chrome/browser/appmenu/AppMenuAdapter;->setupMenuButton(Landroid/widget/ImageButton;)V

    .line 226
    :goto_6
    invoke-virtual {p2, v3}, Landroid/view/View;->setFocusable(Z)V

    .line 227
    invoke-virtual {p2, v3}, Landroid/view/View;->setEnabled(Z)V

    goto/16 :goto_2

    .line 216
    :cond_6
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/chromium/chrome/browser/appmenu/AppMenuAdapter$ThreeButtonMenuItemViewHolder;

    goto :goto_5

    .line 223
    :cond_7
    iget-object v0, v0, Lorg/chromium/chrome/browser/appmenu/AppMenuAdapter$ThreeButtonMenuItemViewHolder;->buttons:[Landroid/widget/ImageButton;

    aget-object v0, v0, v6

    invoke-interface {v7}, Landroid/view/MenuItem;->getSubMenu()Landroid/view/SubMenu;

    move-result-object v1

    invoke-interface {v1, v6}, Landroid/view/SubMenu;->getItem(I)Landroid/view/MenuItem;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lorg/chromium/chrome/browser/appmenu/AppMenuAdapter;->setupImageButton(Landroid/widget/ImageButton;Landroid/view/MenuItem;)V

    goto :goto_6

    .line 231
    :pswitch_3
    if-nez p2, :cond_8

    .line 233
    new-instance v4, Lorg/chromium/chrome/browser/appmenu/AppMenuAdapter$FourButtonMenuItemViewHolder;

    invoke-direct {v4}, Lorg/chromium/chrome/browser/appmenu/AppMenuAdapter$FourButtonMenuItemViewHolder;-><init>()V

    .line 234
    iget-object v0, p0, Lorg/chromium/chrome/browser/appmenu/AppMenuAdapter;->mInflater:Landroid/view/LayoutInflater;

    sget v5, Lorg/chromium/chrome/R$layout;->four_button_menu_item:I

    invoke-virtual {v0, v5, p3, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 235
    iget-object v5, v4, Lorg/chromium/chrome/browser/appmenu/AppMenuAdapter$FourButtonMenuItemViewHolder;->buttons:[Landroid/widget/ImageButton;

    sget v0, Lorg/chromium/chrome/R$id;->button_one:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    aput-object v0, v5, v3

    .line 236
    iget-object v5, v4, Lorg/chromium/chrome/browser/appmenu/AppMenuAdapter$FourButtonMenuItemViewHolder;->buttons:[Landroid/widget/ImageButton;

    sget v0, Lorg/chromium/chrome/R$id;->button_two:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    aput-object v0, v5, v2

    .line 237
    iget-object v5, v4, Lorg/chromium/chrome/browser/appmenu/AppMenuAdapter$FourButtonMenuItemViewHolder;->buttons:[Landroid/widget/ImageButton;

    sget v0, Lorg/chromium/chrome/R$id;->button_three:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    aput-object v0, v5, v6

    .line 238
    iget-object v5, v4, Lorg/chromium/chrome/browser/appmenu/AppMenuAdapter$FourButtonMenuItemViewHolder;->buttons:[Landroid/widget/ImageButton;

    sget v0, Lorg/chromium/chrome/R$id;->button_four:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    aput-object v0, v5, v8

    .line 239
    invoke-virtual {p2, v4}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 240
    sget v0, Lorg/chromium/chrome/R$id;->menu_item_enter_anim_id:I

    iget-object v5, v4, Lorg/chromium/chrome/browser/appmenu/AppMenuAdapter$FourButtonMenuItemViewHolder;->buttons:[Landroid/widget/ImageButton;

    invoke-direct {p0, v5, v1}, Lorg/chromium/chrome/browser/appmenu/AppMenuAdapter;->buildIconItemEnterAnimator([Landroid/widget/ImageView;Z)Landroid/animation/Animator;

    move-result-object v5

    invoke-virtual {p2, v0, v5}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    move-object v0, v4

    .line 245
    :goto_7
    iget-object v4, v0, Lorg/chromium/chrome/browser/appmenu/AppMenuAdapter$FourButtonMenuItemViewHolder;->buttons:[Landroid/widget/ImageButton;

    aget-object v4, v4, v3

    invoke-interface {v7}, Landroid/view/MenuItem;->getSubMenu()Landroid/view/SubMenu;

    move-result-object v5

    invoke-interface {v5, v3}, Landroid/view/SubMenu;->getItem(I)Landroid/view/MenuItem;

    move-result-object v5

    invoke-direct {p0, v4, v5}, Lorg/chromium/chrome/browser/appmenu/AppMenuAdapter;->setupImageButton(Landroid/widget/ImageButton;Landroid/view/MenuItem;)V

    .line 246
    iget-object v4, v0, Lorg/chromium/chrome/browser/appmenu/AppMenuAdapter$FourButtonMenuItemViewHolder;->buttons:[Landroid/widget/ImageButton;

    aget-object v4, v4, v2

    invoke-interface {v7}, Landroid/view/MenuItem;->getSubMenu()Landroid/view/SubMenu;

    move-result-object v5

    invoke-interface {v5, v2}, Landroid/view/SubMenu;->getItem(I)Landroid/view/MenuItem;

    move-result-object v2

    invoke-direct {p0, v4, v2}, Lorg/chromium/chrome/browser/appmenu/AppMenuAdapter;->setupImageButton(Landroid/widget/ImageButton;Landroid/view/MenuItem;)V

    .line 247
    iget-object v2, v0, Lorg/chromium/chrome/browser/appmenu/AppMenuAdapter$FourButtonMenuItemViewHolder;->buttons:[Landroid/widget/ImageButton;

    aget-object v2, v2, v6

    invoke-interface {v7}, Landroid/view/MenuItem;->getSubMenu()Landroid/view/SubMenu;

    move-result-object v4

    invoke-interface {v4, v6}, Landroid/view/SubMenu;->getItem(I)Landroid/view/MenuItem;

    move-result-object v4

    invoke-direct {p0, v2, v4}, Lorg/chromium/chrome/browser/appmenu/AppMenuAdapter;->setupImageButton(Landroid/widget/ImageButton;Landroid/view/MenuItem;)V

    .line 248
    if-eqz v1, :cond_9

    .line 249
    iget-object v0, v0, Lorg/chromium/chrome/browser/appmenu/AppMenuAdapter$FourButtonMenuItemViewHolder;->buttons:[Landroid/widget/ImageButton;

    aget-object v0, v0, v8

    invoke-direct {p0, v0}, Lorg/chromium/chrome/browser/appmenu/AppMenuAdapter;->setupMenuButton(Landroid/widget/ImageButton;)V

    .line 253
    :goto_8
    invoke-virtual {p2, v3}, Landroid/view/View;->setFocusable(Z)V

    .line 254
    invoke-virtual {p2, v3}, Landroid/view/View;->setEnabled(Z)V

    goto/16 :goto_2

    .line 243
    :cond_8
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/chromium/chrome/browser/appmenu/AppMenuAdapter$FourButtonMenuItemViewHolder;

    goto :goto_7

    .line 251
    :cond_9
    iget-object v0, v0, Lorg/chromium/chrome/browser/appmenu/AppMenuAdapter$FourButtonMenuItemViewHolder;->buttons:[Landroid/widget/ImageButton;

    aget-object v0, v0, v8

    invoke-interface {v7}, Landroid/view/MenuItem;->getSubMenu()Landroid/view/SubMenu;

    move-result-object v1

    invoke-interface {v1, v8}, Landroid/view/SubMenu;->getItem(I)Landroid/view/MenuItem;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lorg/chromium/chrome/browser/appmenu/AppMenuAdapter;->setupImageButton(Landroid/widget/ImageButton;Landroid/view/MenuItem;)V

    goto :goto_8

    .line 260
    :pswitch_4
    if-nez p2, :cond_b

    .line 262
    new-instance v6, Lorg/chromium/chrome/browser/appmenu/AppMenuAdapter$TitleButtonMenuItemViewHolder;

    invoke-direct {v6}, Lorg/chromium/chrome/browser/appmenu/AppMenuAdapter$TitleButtonMenuItemViewHolder;-><init>()V

    .line 263
    iget-object v0, p0, Lorg/chromium/chrome/browser/appmenu/AppMenuAdapter;->mInflater:Landroid/view/LayoutInflater;

    sget v5, Lorg/chromium/chrome/R$layout;->title_button_menu_item:I

    invoke-virtual {v0, v5, p3, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v5

    .line 264
    sget v0, Lorg/chromium/chrome/R$id;->title:I

    invoke-virtual {v5, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v6, Lorg/chromium/chrome/browser/appmenu/AppMenuAdapter$TitleButtonMenuItemViewHolder;->title:Landroid/widget/TextView;

    .line 265
    sget v0, Lorg/chromium/chrome/R$id;->button:I

    invoke-virtual {v5, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, v6, Lorg/chromium/chrome/browser/appmenu/AppMenuAdapter$TitleButtonMenuItemViewHolder;->button:Landroid/widget/ImageButton;

    .line 267
    if-eqz v1, :cond_a

    iget-object v0, v6, Lorg/chromium/chrome/browser/appmenu/AppMenuAdapter$TitleButtonMenuItemViewHolder;->title:Landroid/widget/TextView;

    .line 269
    :goto_9
    invoke-virtual {v5, v6}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 270
    sget v8, Lorg/chromium/chrome/R$id;->menu_item_enter_anim_id:I

    invoke-direct {p0, v0, p1}, Lorg/chromium/chrome/browser/appmenu/AppMenuAdapter;->buildStandardItemEnterAnimator(Landroid/view/View;I)Landroid/animation/Animator;

    move-result-object v0

    invoke-virtual {v5, v8, v0}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    move-object v0, v6

    .line 275
    :goto_a
    invoke-interface {v7}, Landroid/view/MenuItem;->hasSubMenu()Z

    move-result v6

    if-eqz v6, :cond_c

    invoke-interface {v7}, Landroid/view/MenuItem;->getSubMenu()Landroid/view/SubMenu;

    move-result-object v6

    invoke-interface {v6, v3}, Landroid/view/SubMenu;->getItem(I)Landroid/view/MenuItem;

    move-result-object v6

    .line 276
    :goto_b
    iget-object v8, v0, Lorg/chromium/chrome/browser/appmenu/AppMenuAdapter$TitleButtonMenuItemViewHolder;->title:Landroid/widget/TextView;

    invoke-interface {v6}, Landroid/view/MenuItem;->getTitle()Ljava/lang/CharSequence;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 277
    iget-object v8, v0, Lorg/chromium/chrome/browser/appmenu/AppMenuAdapter$TitleButtonMenuItemViewHolder;->title:Landroid/widget/TextView;

    invoke-interface {v6}, Landroid/view/MenuItem;->isEnabled()Z

    move-result v9

    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 278
    iget-object v8, v0, Lorg/chromium/chrome/browser/appmenu/AppMenuAdapter$TitleButtonMenuItemViewHolder;->title:Landroid/widget/TextView;

    invoke-interface {v6}, Landroid/view/MenuItem;->isEnabled()Z

    move-result v9

    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setFocusable(Z)V

    .line 279
    iget-object v8, v0, Lorg/chromium/chrome/browser/appmenu/AppMenuAdapter$TitleButtonMenuItemViewHolder;->title:Landroid/widget/TextView;

    new-instance v9, Lorg/chromium/chrome/browser/appmenu/AppMenuAdapter$2;

    invoke-direct {v9, p0, v6}, Lorg/chromium/chrome/browser/appmenu/AppMenuAdapter$2;-><init>(Lorg/chromium/chrome/browser/appmenu/AppMenuAdapter;Landroid/view/MenuItem;)V

    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 286
    if-eqz v1, :cond_d

    .line 287
    iget-object v1, v0, Lorg/chromium/chrome/browser/appmenu/AppMenuAdapter$TitleButtonMenuItemViewHolder;->button:Landroid/widget/ImageButton;

    invoke-virtual {v1, v3}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 288
    iget-object v0, v0, Lorg/chromium/chrome/browser/appmenu/AppMenuAdapter$TitleButtonMenuItemViewHolder;->button:Landroid/widget/ImageButton;

    invoke-direct {p0, v0}, Lorg/chromium/chrome/browser/appmenu/AppMenuAdapter;->setupMenuButton(Landroid/widget/ImageButton;)V

    .line 295
    :goto_c
    invoke-virtual {v5, v3}, Landroid/view/View;->setFocusable(Z)V

    .line 296
    invoke-virtual {v5, v3}, Landroid/view/View;->setEnabled(Z)V

    move-object p2, v5

    .line 297
    goto/16 :goto_2

    :cond_a
    move-object v0, v5

    .line 267
    goto :goto_9

    .line 273
    :cond_b
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/chromium/chrome/browser/appmenu/AppMenuAdapter$TitleButtonMenuItemViewHolder;

    move-object v5, p2

    goto :goto_a

    :cond_c
    move-object v6, v7

    .line 275
    goto :goto_b

    .line 289
    :cond_d
    invoke-interface {v7}, Landroid/view/MenuItem;->getSubMenu()Landroid/view/SubMenu;

    move-result-object v1

    invoke-interface {v1, v2}, Landroid/view/SubMenu;->getItem(I)Landroid/view/MenuItem;

    move-result-object v1

    invoke-interface {v1}, Landroid/view/MenuItem;->getIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    if-eqz v1, :cond_e

    .line 290
    iget-object v1, v0, Lorg/chromium/chrome/browser/appmenu/AppMenuAdapter$TitleButtonMenuItemViewHolder;->button:Landroid/widget/ImageButton;

    invoke-virtual {v1, v3}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 291
    iget-object v0, v0, Lorg/chromium/chrome/browser/appmenu/AppMenuAdapter$TitleButtonMenuItemViewHolder;->button:Landroid/widget/ImageButton;

    invoke-interface {v7}, Landroid/view/MenuItem;->getSubMenu()Landroid/view/SubMenu;

    move-result-object v1

    invoke-interface {v1, v2}, Landroid/view/SubMenu;->getItem(I)Landroid/view/MenuItem;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lorg/chromium/chrome/browser/appmenu/AppMenuAdapter;->setupImageButton(Landroid/widget/ImageButton;Landroid/view/MenuItem;)V

    goto :goto_c

    .line 293
    :cond_e
    iget-object v0, v0, Lorg/chromium/chrome/browser/appmenu/AppMenuAdapter$TitleButtonMenuItemViewHolder;->button:Landroid/widget/ImageButton;

    invoke-virtual {v0, v4}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto :goto_c

    .line 145
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_4
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public getViewTypeCount()I
    .locals 1

    .prologue
    .line 103
    const/4 v0, 0x6

    return v0
.end method
