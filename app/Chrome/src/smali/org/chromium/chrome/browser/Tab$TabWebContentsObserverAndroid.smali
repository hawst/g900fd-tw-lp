.class Lorg/chromium/chrome/browser/Tab$TabWebContentsObserverAndroid;
.super Lorg/chromium/content/browser/WebContentsObserverAndroid;
.source "Tab.java"


# instance fields
.field final synthetic this$0:Lorg/chromium/chrome/browser/Tab;


# direct methods
.method public constructor <init>(Lorg/chromium/chrome/browser/Tab;Lorg/chromium/content_public/browser/WebContents;)V
    .locals 0

    .prologue
    .line 265
    iput-object p1, p0, Lorg/chromium/chrome/browser/Tab$TabWebContentsObserverAndroid;->this$0:Lorg/chromium/chrome/browser/Tab;

    .line 266
    invoke-direct {p0, p2}, Lorg/chromium/content/browser/WebContentsObserverAndroid;-><init>(Lorg/chromium/content_public/browser/WebContents;)V

    .line 267
    return-void
.end method


# virtual methods
.method public didChangeThemeColor(I)V
    .locals 2

    .prologue
    .line 308
    iget-object v0, p0, Lorg/chromium/chrome/browser/Tab$TabWebContentsObserverAndroid;->this$0:Lorg/chromium/chrome/browser/Tab;

    # getter for: Lorg/chromium/chrome/browser/Tab;->mObservers:Lorg/chromium/base/ObserverList;
    invoke-static {v0}, Lorg/chromium/chrome/browser/Tab;->access$100(Lorg/chromium/chrome/browser/Tab;)Lorg/chromium/base/ObserverList;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/base/ObserverList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/chromium/chrome/browser/TabObserver;

    .line 309
    invoke-interface {v0, p1}, Lorg/chromium/chrome/browser/TabObserver;->onDidChangeThemeColor(I)V

    goto :goto_0

    .line 311
    :cond_0
    return-void
.end method

.method public didFailLoad(ZZILjava/lang/String;Ljava/lang/String;)V
    .locals 8

    .prologue
    .line 279
    iget-object v0, p0, Lorg/chromium/chrome/browser/Tab$TabWebContentsObserverAndroid;->this$0:Lorg/chromium/chrome/browser/Tab;

    # getter for: Lorg/chromium/chrome/browser/Tab;->mObservers:Lorg/chromium/base/ObserverList;
    invoke-static {v0}, Lorg/chromium/chrome/browser/Tab;->access$100(Lorg/chromium/chrome/browser/Tab;)Lorg/chromium/base/ObserverList;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/base/ObserverList;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/chromium/chrome/browser/TabObserver;

    .line 280
    iget-object v1, p0, Lorg/chromium/chrome/browser/Tab$TabWebContentsObserverAndroid;->this$0:Lorg/chromium/chrome/browser/Tab;

    move v2, p1

    move v3, p2

    move v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-interface/range {v0 .. v6}, Lorg/chromium/chrome/browser/TabObserver;->onDidFailLoad(Lorg/chromium/chrome/browser/Tab;ZZILjava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 283
    :cond_0
    return-void
.end method

.method public didNavigateMainFrame(Ljava/lang/String;Ljava/lang/String;ZZI)V
    .locals 8

    .prologue
    .line 298
    iget-object v0, p0, Lorg/chromium/chrome/browser/Tab$TabWebContentsObserverAndroid;->this$0:Lorg/chromium/chrome/browser/Tab;

    # getter for: Lorg/chromium/chrome/browser/Tab;->mObservers:Lorg/chromium/base/ObserverList;
    invoke-static {v0}, Lorg/chromium/chrome/browser/Tab;->access$100(Lorg/chromium/chrome/browser/Tab;)Lorg/chromium/base/ObserverList;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/base/ObserverList;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/chromium/chrome/browser/TabObserver;

    .line 299
    iget-object v1, p0, Lorg/chromium/chrome/browser/Tab$TabWebContentsObserverAndroid;->this$0:Lorg/chromium/chrome/browser/Tab;

    move-object v2, p1

    move-object v3, p2

    move v4, p3

    move v5, p4

    move v6, p5

    invoke-interface/range {v0 .. v6}, Lorg/chromium/chrome/browser/TabObserver;->onDidNavigateMainFrame(Lorg/chromium/chrome/browser/Tab;Ljava/lang/String;Ljava/lang/String;ZZI)V

    goto :goto_0

    .line 304
    :cond_0
    return-void
.end method

.method public didStartProvisionalLoadForFrame(JJZLjava/lang/String;ZZ)V
    .locals 11

    .prologue
    .line 289
    iget-object v0, p0, Lorg/chromium/chrome/browser/Tab$TabWebContentsObserverAndroid;->this$0:Lorg/chromium/chrome/browser/Tab;

    # getter for: Lorg/chromium/chrome/browser/Tab;->mObservers:Lorg/chromium/base/ObserverList;
    invoke-static {v0}, Lorg/chromium/chrome/browser/Tab;->access$100(Lorg/chromium/chrome/browser/Tab;)Lorg/chromium/base/ObserverList;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/base/ObserverList;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :goto_0
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/chromium/chrome/browser/TabObserver;

    .line 290
    iget-object v1, p0, Lorg/chromium/chrome/browser/Tab$TabWebContentsObserverAndroid;->this$0:Lorg/chromium/chrome/browser/Tab;

    move-wide v2, p1

    move-wide v4, p3

    move/from16 v6, p5

    move-object/from16 v7, p6

    move/from16 v8, p7

    move/from16 v9, p8

    invoke-interface/range {v0 .. v9}, Lorg/chromium/chrome/browser/TabObserver;->onDidStartProvisionalLoadForFrame(Lorg/chromium/chrome/browser/Tab;JJZLjava/lang/String;ZZ)V

    goto :goto_0

    .line 293
    :cond_0
    return-void
.end method

.method public navigationEntryCommitted()V
    .locals 1

    .prologue
    .line 271
    iget-object v0, p0, Lorg/chromium/chrome/browser/Tab$TabWebContentsObserverAndroid;->this$0:Lorg/chromium/chrome/browser/Tab;

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/Tab;->getNativePage()Lorg/chromium/chrome/browser/NativePage;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 272
    iget-object v0, p0, Lorg/chromium/chrome/browser/Tab$TabWebContentsObserverAndroid;->this$0:Lorg/chromium/chrome/browser/Tab;

    # invokes: Lorg/chromium/chrome/browser/Tab;->pushNativePageStateToNavigationEntry()V
    invoke-static {v0}, Lorg/chromium/chrome/browser/Tab;->access$300(Lorg/chromium/chrome/browser/Tab;)V

    .line 274
    :cond_0
    return-void
.end method
