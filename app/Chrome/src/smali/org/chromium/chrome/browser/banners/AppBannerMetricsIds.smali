.class public Lorg/chromium/chrome/browser/banners/AppBannerMetricsIds;
.super Ljava/lang/Object;
.source "AppBannerMetricsIds.java"


# static fields
.field public static final DISMISS_APP_OPEN:I = 0x2a

.field public static final DISMISS_BANNER_CLICK:I = 0x2b

.field public static final DISMISS_BANNER_SWIPE:I = 0x2c

.field public static final DISMISS_CLOSE_BUTTON:I = 0x2d

.field public static final DISMISS_ERROR:I = 0x29

.field public static final DISMISS_INSTALL_TIMEOUT:I = 0x2e

.field public static final DISMISS_MAX:I = 0x30

.field public static final DISMISS_MIN:I = 0x28

.field public static final DISMISS_NAVIGATE:I = 0x2f

.field public static final DISPLAY_BANNER_REQUESTED:I = 0x1

.field public static final DISPLAY_BLOCKED_PREVIOUSLY:I = 0x2

.field public static final DISPLAY_BLOCKED_TOO_MANY_OTHERS:I = 0x3

.field public static final DISPLAY_CREATED:I = 0x4

.field public static final DISPLAY_MAX:I = 0x5

.field public static final DISPLAY_MIN:I = 0x0

.field public static final INSTALL_COMPLETED:I = 0x17

.field public static final INSTALL_MAX:I = 0x18

.field public static final INSTALL_MIN:I = 0x14

.field public static final INSTALL_STARTED:I = 0x16

.field public static final INSTALL_TRIGGERED:I = 0x15


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
