.class public Lorg/chromium/chrome/browser/infobar/AutoLoginDelegate;
.super Ljava/lang/Object;
.source "AutoLoginDelegate.java"


# instance fields
.field private mAccountHelper:Landroid/util/Pair;

.field private final mActivity:Landroid/app/Activity;

.field private final mAutoLoginProcessor:Lorg/chromium/chrome/browser/infobar/AutoLoginProcessor;


# direct methods
.method public constructor <init>(Lorg/chromium/chrome/browser/infobar/AutoLoginProcessor;Landroid/app/Activity;)V
    .locals 1

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput-object p2, p0, Lorg/chromium/chrome/browser/infobar/AutoLoginDelegate;->mActivity:Landroid/app/Activity;

    .line 27
    iput-object p1, p0, Lorg/chromium/chrome/browser/infobar/AutoLoginDelegate;->mAutoLoginProcessor:Lorg/chromium/chrome/browser/infobar/AutoLoginProcessor;

    .line 28
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/chromium/chrome/browser/infobar/AutoLoginDelegate;->mAccountHelper:Landroid/util/Pair;

    .line 29
    return-void
.end method

.method private native nativeLoginDismiss(J)V
.end method

.method private native nativeLoginFailed(J)V
.end method

.method private native nativeLoginSuccess(JLjava/lang/String;)V
.end method


# virtual methods
.method cancelLogIn(J)Z
    .locals 1

    .prologue
    .line 68
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/chromium/chrome/browser/infobar/AutoLoginDelegate;->mAccountHelper:Landroid/util/Pair;

    .line 69
    const/4 v0, 0x1

    return v0
.end method

.method public dismissAutoLogins(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)V
    .locals 4

    .prologue
    .line 78
    iget-object v0, p0, Lorg/chromium/chrome/browser/infobar/AutoLoginDelegate;->mAccountHelper:Landroid/util/Pair;

    if-eqz v0, :cond_1

    .line 79
    iget-object v0, p0, Lorg/chromium/chrome/browser/infobar/AutoLoginDelegate;->mAccountHelper:Landroid/util/Pair;

    iget-object v0, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    .line 80
    iget-object v0, p0, Lorg/chromium/chrome/browser/infobar/AutoLoginDelegate;->mAccountHelper:Landroid/util/Pair;

    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Lorg/chromium/chrome/browser/infobar/AutoLoginAccountDelegate;

    .line 81
    invoke-virtual {v0}, Lorg/chromium/chrome/browser/infobar/AutoLoginAccountDelegate;->loginRequested()Z

    move-result v1

    if-nez v1, :cond_2

    .line 82
    invoke-direct {p0, v2, v3}, Lorg/chromium/chrome/browser/infobar/AutoLoginDelegate;->nativeLoginDismiss(J)V

    .line 94
    :cond_0
    :goto_0
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/chromium/chrome/browser/infobar/AutoLoginDelegate;->mAccountHelper:Landroid/util/Pair;

    .line 96
    :cond_1
    return-void

    .line 84
    :cond_2
    invoke-virtual {v0}, Lorg/chromium/chrome/browser/infobar/AutoLoginAccountDelegate;->getAuthToken()Ljava/lang/String;

    move-result-object v1

    .line 85
    if-eqz v1, :cond_0

    invoke-virtual {v1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/infobar/AutoLoginAccountDelegate;->loginRequested()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 87
    if-eqz p3, :cond_3

    .line 88
    invoke-direct {p0, v2, v3, p4}, Lorg/chromium/chrome/browser/infobar/AutoLoginDelegate;->nativeLoginSuccess(JLjava/lang/String;)V

    goto :goto_0

    .line 90
    :cond_3
    invoke-direct {p0, v2, v3}, Lorg/chromium/chrome/browser/infobar/AutoLoginDelegate;->nativeLoginFailed(J)V

    goto :goto_0
.end method

.method initializeAccount(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 7

    .prologue
    .line 36
    new-instance v0, Lorg/chromium/chrome/browser/infobar/AutoLoginAccountDelegate;

    iget-object v1, p0, Lorg/chromium/chrome/browser/infobar/AutoLoginDelegate;->mActivity:Landroid/app/Activity;

    iget-object v2, p0, Lorg/chromium/chrome/browser/infobar/AutoLoginDelegate;->mAutoLoginProcessor:Lorg/chromium/chrome/browser/infobar/AutoLoginProcessor;

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v5}, Lorg/chromium/chrome/browser/infobar/AutoLoginAccountDelegate;-><init>(Landroid/app/Activity;Lorg/chromium/chrome/browser/infobar/AutoLoginProcessor;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 39
    invoke-virtual {v0}, Lorg/chromium/chrome/browser/infobar/AutoLoginAccountDelegate;->hasAccount()Z

    move-result v1

    if-nez v1, :cond_0

    .line 40
    const-string/jumbo v0, ""

    .line 44
    :goto_0
    return-object v0

    .line 43
    :cond_0
    new-instance v1, Landroid/util/Pair;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    iput-object v1, p0, Lorg/chromium/chrome/browser/infobar/AutoLoginDelegate;->mAccountHelper:Landroid/util/Pair;

    .line 44
    invoke-virtual {v0}, Lorg/chromium/chrome/browser/infobar/AutoLoginAccountDelegate;->getAccountName()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method logIn(J)Z
    .locals 3

    .prologue
    .line 52
    iget-object v0, p0, Lorg/chromium/chrome/browser/infobar/AutoLoginDelegate;->mAccountHelper:Landroid/util/Pair;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lorg/chromium/chrome/browser/infobar/AutoLoginDelegate;->mAccountHelper:Landroid/util/Pair;

    iget-object v0, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    cmp-long v0, v0, p1

    if-nez v0, :cond_1

    iget-object v0, p0, Lorg/chromium/chrome/browser/infobar/AutoLoginDelegate;->mAccountHelper:Landroid/util/Pair;

    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Lorg/chromium/chrome/browser/infobar/AutoLoginAccountDelegate;

    .line 56
    :goto_0
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/infobar/AutoLoginAccountDelegate;->logIn()Z

    move-result v0

    if-nez v0, :cond_2

    .line 57
    :cond_0
    invoke-direct {p0, p1, p2}, Lorg/chromium/chrome/browser/infobar/AutoLoginDelegate;->nativeLoginFailed(J)V

    .line 58
    const/4 v0, 0x0

    .line 60
    :goto_1
    return v0

    .line 52
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 60
    :cond_2
    const/4 v0, 0x1

    goto :goto_1
.end method
