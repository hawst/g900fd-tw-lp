.class public Lorg/chromium/chrome/browser/contextmenu/ContextMenuParams;
.super Ljava/lang/Object;
.source "ContextMenuParams.java"


# annotations
.annotation runtime Lorg/chromium/base/JNINamespace;
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private final mCustomMenuItems:Ljava/util/ArrayList;

.field private final mIsAnchor:Z

.field private final mIsEditable:Z

.field private final mIsImage:Z

.field private final mIsSelectedText:Z

.field private final mIsVideo:Z

.field private final mLinkText:Ljava/lang/String;

.field private final mLinkUrl:Ljava/lang/String;

.field private final mReferrer:Lorg/chromium/content_public/common/Referrer;

.field private final mSrcUrl:Ljava/lang/String;

.field private final mUnfilteredLinkUrl:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 19
    const-class v0, Lorg/chromium/chrome/browser/contextmenu/ContextMenuParams;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/chromium/chrome/browser/contextmenu/ContextMenuParams;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private constructor <init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLorg/chromium/content_public/common/Referrer;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 162
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lorg/chromium/chrome/browser/contextmenu/ContextMenuParams;->mCustomMenuItems:Ljava/util/ArrayList;

    .line 163
    iput-object p2, p0, Lorg/chromium/chrome/browser/contextmenu/ContextMenuParams;->mLinkUrl:Ljava/lang/String;

    .line 164
    iput-object p3, p0, Lorg/chromium/chrome/browser/contextmenu/ContextMenuParams;->mLinkText:Ljava/lang/String;

    .line 165
    iput-object p4, p0, Lorg/chromium/chrome/browser/contextmenu/ContextMenuParams;->mUnfilteredLinkUrl:Ljava/lang/String;

    .line 166
    iput-object p5, p0, Lorg/chromium/chrome/browser/contextmenu/ContextMenuParams;->mSrcUrl:Ljava/lang/String;

    .line 167
    iput-boolean p7, p0, Lorg/chromium/chrome/browser/contextmenu/ContextMenuParams;->mIsEditable:Z

    .line 168
    iput-object p8, p0, Lorg/chromium/chrome/browser/contextmenu/ContextMenuParams;->mReferrer:Lorg/chromium/content_public/common/Referrer;

    .line 170
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lorg/chromium/chrome/browser/contextmenu/ContextMenuParams;->mIsAnchor:Z

    .line 171
    invoke-static {p6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lorg/chromium/chrome/browser/contextmenu/ContextMenuParams;->mIsSelectedText:Z

    .line 172
    if-ne p1, v1, :cond_2

    move v0, v1

    :goto_2
    iput-boolean v0, p0, Lorg/chromium/chrome/browser/contextmenu/ContextMenuParams;->mIsImage:Z

    .line 173
    const/4 v0, 0x2

    if-ne p1, v0, :cond_3

    :goto_3
    iput-boolean v1, p0, Lorg/chromium/chrome/browser/contextmenu/ContextMenuParams;->mIsVideo:Z

    .line 174
    return-void

    :cond_0
    move v0, v2

    .line 170
    goto :goto_0

    :cond_1
    move v0, v2

    .line 171
    goto :goto_1

    :cond_2
    move v0, v2

    .line 172
    goto :goto_2

    :cond_3
    move v1, v2

    .line 173
    goto :goto_3
.end method

.method private addCustomItem(Ljava/lang/String;I)V
    .locals 2

    .prologue
    .line 188
    iget-object v0, p0, Lorg/chromium/chrome/browser/contextmenu/ContextMenuParams;->mCustomMenuItems:Ljava/util/ArrayList;

    new-instance v1, Lorg/chromium/chrome/browser/contextmenu/ContextMenuParams$CustomMenuItem;

    invoke-direct {v1, p1, p2}, Lorg/chromium/chrome/browser/contextmenu/ContextMenuParams$CustomMenuItem;-><init>(Ljava/lang/String;I)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 189
    return-void
.end method

.method private static create(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;I)Lorg/chromium/chrome/browser/contextmenu/ContextMenuParams;
    .locals 11

    .prologue
    .line 180
    invoke-static/range {p7 .. p7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v10, 0x0

    .line 182
    :goto_0
    new-instance v2, Lorg/chromium/chrome/browser/contextmenu/ContextMenuParams;

    move v3, p0

    move-object v4, p1

    move-object v5, p2

    move-object v6, p3

    move-object v7, p4

    move-object/from16 v8, p5

    move/from16 v9, p6

    invoke-direct/range {v2 .. v10}, Lorg/chromium/chrome/browser/contextmenu/ContextMenuParams;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLorg/chromium/content_public/common/Referrer;)V

    return-object v2

    .line 180
    :cond_0
    new-instance v10, Lorg/chromium/content_public/common/Referrer;

    move-object/from16 v0, p7

    move/from16 v1, p8

    invoke-direct {v10, v0, v1}, Lorg/chromium/content_public/common/Referrer;-><init>(Ljava/lang/String;I)V

    goto :goto_0
.end method


# virtual methods
.method public getCustomActionAt(I)I
    .locals 1

    .prologue
    .line 86
    sget-boolean v0, Lorg/chromium/chrome/browser/contextmenu/ContextMenuParams;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    if-ltz p1, :cond_0

    iget-object v0, p0, Lorg/chromium/chrome/browser/contextmenu/ContextMenuParams;->mCustomMenuItems:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt p1, v0, :cond_1

    :cond_0
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 87
    :cond_1
    iget-object v0, p0, Lorg/chromium/chrome/browser/contextmenu/ContextMenuParams;->mCustomMenuItems:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/chromium/chrome/browser/contextmenu/ContextMenuParams$CustomMenuItem;

    iget v0, v0, Lorg/chromium/chrome/browser/contextmenu/ContextMenuParams$CustomMenuItem;->action:I

    return v0
.end method

.method public getCustomLabelAt(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 76
    sget-boolean v0, Lorg/chromium/chrome/browser/contextmenu/ContextMenuParams;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    if-ltz p1, :cond_0

    iget-object v0, p0, Lorg/chromium/chrome/browser/contextmenu/ContextMenuParams;->mCustomMenuItems:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt p1, v0, :cond_1

    :cond_0
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 77
    :cond_1
    iget-object v0, p0, Lorg/chromium/chrome/browser/contextmenu/ContextMenuParams;->mCustomMenuItems:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/chromium/chrome/browser/contextmenu/ContextMenuParams$CustomMenuItem;

    iget-object v0, v0, Lorg/chromium/chrome/browser/contextmenu/ContextMenuParams$CustomMenuItem;->label:Ljava/lang/String;

    return-object v0
.end method

.method public getCustomMenuSize()I
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lorg/chromium/chrome/browser/contextmenu/ContextMenuParams;->mCustomMenuItems:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public getLinkText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 101
    iget-object v0, p0, Lorg/chromium/chrome/browser/contextmenu/ContextMenuParams;->mLinkText:Ljava/lang/String;

    return-object v0
.end method

.method public getLinkUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lorg/chromium/chrome/browser/contextmenu/ContextMenuParams;->mLinkUrl:Ljava/lang/String;

    return-object v0
.end method

.method public getReferrer()Lorg/chromium/content_public/common/Referrer;
    .locals 1

    .prologue
    .line 129
    iget-object v0, p0, Lorg/chromium/chrome/browser/contextmenu/ContextMenuParams;->mReferrer:Lorg/chromium/content_public/common/Referrer;

    return-object v0
.end method

.method public getSrcUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lorg/chromium/chrome/browser/contextmenu/ContextMenuParams;->mSrcUrl:Ljava/lang/String;

    return-object v0
.end method

.method public getUnfilteredLinkUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 108
    iget-object v0, p0, Lorg/chromium/chrome/browser/contextmenu/ContextMenuParams;->mUnfilteredLinkUrl:Ljava/lang/String;

    return-object v0
.end method

.method public isAnchor()Z
    .locals 1

    .prologue
    .line 136
    iget-boolean v0, p0, Lorg/chromium/chrome/browser/contextmenu/ContextMenuParams;->mIsAnchor:Z

    return v0
.end method

.method public isCustomMenu()Z
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lorg/chromium/chrome/browser/contextmenu/ContextMenuParams;->mCustomMenuItems:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isEditable()Z
    .locals 1

    .prologue
    .line 122
    iget-boolean v0, p0, Lorg/chromium/chrome/browser/contextmenu/ContextMenuParams;->mIsEditable:Z

    return v0
.end method

.method public isImage()Z
    .locals 1

    .prologue
    .line 150
    iget-boolean v0, p0, Lorg/chromium/chrome/browser/contextmenu/ContextMenuParams;->mIsImage:Z

    return v0
.end method

.method public isSelectedText()Z
    .locals 1

    .prologue
    .line 143
    iget-boolean v0, p0, Lorg/chromium/chrome/browser/contextmenu/ContextMenuParams;->mIsSelectedText:Z

    return v0
.end method

.method public isVideo()Z
    .locals 1

    .prologue
    .line 157
    iget-boolean v0, p0, Lorg/chromium/chrome/browser/contextmenu/ContextMenuParams;->mIsVideo:Z

    return v0
.end method
