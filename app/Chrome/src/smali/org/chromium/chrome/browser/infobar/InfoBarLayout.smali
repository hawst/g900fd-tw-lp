.class public Lorg/chromium/chrome/browser/infobar/InfoBarLayout;
.super Landroid/view/ViewGroup;
.source "InfoBarLayout.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private final mAccentColor:I

.field private mBottom:I

.field private mButtonGroup:Lorg/chromium/chrome/browser/infobar/InfoBarLayout$Group;

.field private final mCloseButton:Landroid/widget/ImageButton;

.field private mCustomGroup:Lorg/chromium/chrome/browser/infobar/InfoBarLayout$Group;

.field private mEnd:I

.field private final mIconSize:I

.field private mIconView:Landroid/widget/ImageView;

.field private final mInfoBarView:Lorg/chromium/chrome/browser/infobar/InfoBarView;

.field private mMainGroup:Lorg/chromium/chrome/browser/infobar/InfoBarLayout$Group;

.field private final mMargin:I

.field private final mMessageView:Landroid/widget/TextView;

.field private final mMinWidth:I

.field private mStart:I

.field private mTop:I

.field private mWidth:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 40
    const-class v0, Lorg/chromium/chrome/browser/infobar/InfoBarLayout;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/chromium/chrome/browser/infobar/InfoBarLayout;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;Lorg/chromium/chrome/browser/infobar/InfoBarView;ILjava/lang/CharSequence;)V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 147
    invoke-direct {p0, p1}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;)V

    .line 148
    iput-object p2, p0, Lorg/chromium/chrome/browser/infobar/InfoBarLayout;->mInfoBarView:Lorg/chromium/chrome/browser/infobar/InfoBarView;

    .line 151
    invoke-virtual {p0}, Lorg/chromium/chrome/browser/infobar/InfoBarLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 152
    sget v1, Lorg/chromium/chrome/R$dimen;->infobar_margin:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    iput v1, p0, Lorg/chromium/chrome/browser/infobar/InfoBarLayout;->mMargin:I

    .line 153
    sget v1, Lorg/chromium/chrome/R$dimen;->infobar_icon_size:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lorg/chromium/chrome/browser/infobar/InfoBarLayout;->mIconSize:I

    .line 154
    sget v1, Lorg/chromium/chrome/R$dimen;->infobar_min_width:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lorg/chromium/chrome/browser/infobar/InfoBarLayout;->mMinWidth:I

    .line 155
    sget v1, Lorg/chromium/chrome/R$color;->infobar_accent_blue:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lorg/chromium/chrome/browser/infobar/InfoBarLayout;->mAccentColor:I

    .line 158
    new-instance v1, Landroid/widget/ImageButton;

    invoke-direct {v1, p1}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lorg/chromium/chrome/browser/infobar/InfoBarLayout;->mCloseButton:Landroid/widget/ImageButton;

    .line 159
    iget-object v1, p0, Lorg/chromium/chrome/browser/infobar/InfoBarLayout;->mCloseButton:Landroid/widget/ImageButton;

    sget v2, Lorg/chromium/chrome/R$id;->infobar_close_button:I

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setId(I)V

    .line 160
    iget-object v1, p0, Lorg/chromium/chrome/browser/infobar/InfoBarLayout;->mCloseButton:Landroid/widget/ImageButton;

    sget v2, Lorg/chromium/chrome/R$drawable;->infobar_close_button:I

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 161
    invoke-virtual {p0}, Lorg/chromium/chrome/browser/infobar/InfoBarLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    new-array v2, v7, [I

    const v3, 0x101030e

    aput v3, v2, v6

    invoke-virtual {v1, v2}, Landroid/content/Context;->obtainStyledAttributes([I)Landroid/content/res/TypedArray;

    move-result-object v1

    .line 163
    invoke-virtual {v1, v6}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 164
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    .line 165
    iget-object v1, p0, Lorg/chromium/chrome/browser/infobar/InfoBarLayout;->mCloseButton:Landroid/widget/ImageButton;

    invoke-static {v1, v2}, Lorg/chromium/base/ApiCompatibilityUtils;->setBackgroundForView(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 166
    iget-object v1, p0, Lorg/chromium/chrome/browser/infobar/InfoBarLayout;->mCloseButton:Landroid/widget/ImageButton;

    iget v2, p0, Lorg/chromium/chrome/browser/infobar/InfoBarLayout;->mMargin:I

    iget v3, p0, Lorg/chromium/chrome/browser/infobar/InfoBarLayout;->mMargin:I

    iget v4, p0, Lorg/chromium/chrome/browser/infobar/InfoBarLayout;->mMargin:I

    iget v5, p0, Lorg/chromium/chrome/browser/infobar/InfoBarLayout;->mMargin:I

    invoke-virtual {v1, v2, v3, v4, v5}, Landroid/widget/ImageButton;->setPadding(IIII)V

    .line 167
    iget-object v1, p0, Lorg/chromium/chrome/browser/infobar/InfoBarLayout;->mCloseButton:Landroid/widget/ImageButton;

    invoke-virtual {v1, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 168
    iget-object v1, p0, Lorg/chromium/chrome/browser/infobar/InfoBarLayout;->mCloseButton:Landroid/widget/ImageButton;

    sget v2, Lorg/chromium/chrome/R$string;->infobar_close:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 169
    iget-object v0, p0, Lorg/chromium/chrome/browser/infobar/InfoBarLayout;->mCloseButton:Landroid/widget/ImageButton;

    new-instance v1, Lorg/chromium/chrome/browser/infobar/InfoBarLayout$LayoutParams;

    iget v2, p0, Lorg/chromium/chrome/browser/infobar/InfoBarLayout;->mMargin:I

    neg-int v2, v2

    iget v3, p0, Lorg/chromium/chrome/browser/infobar/InfoBarLayout;->mMargin:I

    neg-int v3, v3

    iget v4, p0, Lorg/chromium/chrome/browser/infobar/InfoBarLayout;->mMargin:I

    neg-int v4, v4

    invoke-direct {v1, v6, v2, v3, v4}, Lorg/chromium/chrome/browser/infobar/InfoBarLayout$LayoutParams;-><init>(IIII)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 170
    iget-object v0, p0, Lorg/chromium/chrome/browser/infobar/InfoBarLayout;->mCloseButton:Landroid/widget/ImageButton;

    invoke-virtual {p0, v0}, Lorg/chromium/chrome/browser/infobar/InfoBarLayout;->addView(Landroid/view/View;)V

    .line 173
    if-eqz p3, :cond_0

    .line 174
    new-instance v0, Landroid/widget/ImageView;

    invoke-direct {v0, p1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lorg/chromium/chrome/browser/infobar/InfoBarLayout;->mIconView:Landroid/widget/ImageView;

    .line 175
    iget-object v0, p0, Lorg/chromium/chrome/browser/infobar/InfoBarLayout;->mIconView:Landroid/widget/ImageView;

    invoke-virtual {v0, p3}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 176
    iget-object v0, p0, Lorg/chromium/chrome/browser/infobar/InfoBarLayout;->mIconView:Landroid/widget/ImageView;

    invoke-virtual {v0, v6}, Landroid/widget/ImageView;->setFocusable(Z)V

    .line 177
    iget-object v0, p0, Lorg/chromium/chrome/browser/infobar/InfoBarLayout;->mIconView:Landroid/widget/ImageView;

    new-instance v1, Lorg/chromium/chrome/browser/infobar/InfoBarLayout$LayoutParams;

    iget v2, p0, Lorg/chromium/chrome/browser/infobar/InfoBarLayout;->mMargin:I

    div-int/lit8 v2, v2, 0x2

    invoke-direct {v1, v6, v6, v2, v6}, Lorg/chromium/chrome/browser/infobar/InfoBarLayout$LayoutParams;-><init>(IIII)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 178
    iget-object v0, p0, Lorg/chromium/chrome/browser/infobar/InfoBarLayout;->mIconView:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iget v1, p0, Lorg/chromium/chrome/browser/infobar/InfoBarLayout;->mIconSize:I

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 179
    iget-object v0, p0, Lorg/chromium/chrome/browser/infobar/InfoBarLayout;->mIconView:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iget v1, p0, Lorg/chromium/chrome/browser/infobar/InfoBarLayout;->mIconSize:I

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 183
    :cond_0
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Lorg/chromium/chrome/R$layout;->infobar_text:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lorg/chromium/chrome/browser/infobar/InfoBarLayout;->mMessageView:Landroid/widget/TextView;

    .line 184
    iget-object v0, p0, Lorg/chromium/chrome/browser/infobar/InfoBarLayout;->mMessageView:Landroid/widget/TextView;

    sget-object v1, Landroid/widget/TextView$BufferType;->SPANNABLE:Landroid/widget/TextView$BufferType;

    invoke-virtual {v0, p4, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;Landroid/widget/TextView$BufferType;)V

    .line 185
    iget-object v0, p0, Lorg/chromium/chrome/browser/infobar/InfoBarLayout;->mMessageView:Landroid/widget/TextView;

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 186
    iget-object v0, p0, Lorg/chromium/chrome/browser/infobar/InfoBarLayout;->mMessageView:Landroid/widget/TextView;

    iget v1, p0, Lorg/chromium/chrome/browser/infobar/InfoBarLayout;->mAccentColor:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setLinkTextColor(I)V

    .line 187
    iget-object v0, p0, Lorg/chromium/chrome/browser/infobar/InfoBarLayout;->mMessageView:Landroid/widget/TextView;

    new-instance v1, Lorg/chromium/chrome/browser/infobar/InfoBarLayout$LayoutParams;

    iget v2, p0, Lorg/chromium/chrome/browser/infobar/InfoBarLayout;->mMargin:I

    div-int/lit8 v2, v2, 0x4

    invoke-direct {v1, v6, v2, v6, v6}, Lorg/chromium/chrome/browser/infobar/InfoBarLayout$LayoutParams;-><init>(IIII)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 189
    iget-object v0, p0, Lorg/chromium/chrome/browser/infobar/InfoBarLayout;->mIconView:Landroid/widget/ImageView;

    if-eqz v0, :cond_1

    .line 190
    const/4 v0, 0x2

    new-array v0, v0, [Landroid/view/View;

    iget-object v1, p0, Lorg/chromium/chrome/browser/infobar/InfoBarLayout;->mIconView:Landroid/widget/ImageView;

    aput-object v1, v0, v6

    iget-object v1, p0, Lorg/chromium/chrome/browser/infobar/InfoBarLayout;->mMessageView:Landroid/widget/TextView;

    aput-object v1, v0, v7

    invoke-direct {p0, v0}, Lorg/chromium/chrome/browser/infobar/InfoBarLayout;->addGroup([Landroid/view/View;)Lorg/chromium/chrome/browser/infobar/InfoBarLayout$Group;

    move-result-object v0

    iput-object v0, p0, Lorg/chromium/chrome/browser/infobar/InfoBarLayout;->mMainGroup:Lorg/chromium/chrome/browser/infobar/InfoBarLayout$Group;

    .line 194
    :goto_0
    return-void

    .line 192
    :cond_1
    new-array v0, v7, [Landroid/view/View;

    iget-object v1, p0, Lorg/chromium/chrome/browser/infobar/InfoBarLayout;->mMessageView:Landroid/widget/TextView;

    aput-object v1, v0, v6

    invoke-direct {p0, v0}, Lorg/chromium/chrome/browser/infobar/InfoBarLayout;->addGroup([Landroid/view/View;)Lorg/chromium/chrome/browser/infobar/InfoBarLayout$Group;

    move-result-object v0

    iput-object v0, p0, Lorg/chromium/chrome/browser/infobar/InfoBarLayout;->mMainGroup:Lorg/chromium/chrome/browser/infobar/InfoBarLayout$Group;

    goto :goto_0
.end method

.method private varargs addGroup([Landroid/view/View;)Lorg/chromium/chrome/browser/infobar/InfoBarLayout$Group;
    .locals 4

    .prologue
    .line 284
    new-instance v1, Lorg/chromium/chrome/browser/infobar/InfoBarLayout$Group;

    const/4 v0, 0x0

    invoke-direct {v1, v0}, Lorg/chromium/chrome/browser/infobar/InfoBarLayout$Group;-><init>(Lorg/chromium/chrome/browser/infobar/InfoBarLayout$1;)V

    .line 285
    iput-object p1, v1, Lorg/chromium/chrome/browser/infobar/InfoBarLayout$Group;->views:[Landroid/view/View;

    .line 287
    array-length v2, p1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, p1, v0

    .line 288
    invoke-virtual {p0, v3}, Lorg/chromium/chrome/browser/infobar/InfoBarLayout;->addView(Landroid/view/View;)V

    .line 287
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 290
    :cond_0
    return-object v1
.end method

.method private availableWidth()I
    .locals 2

    .prologue
    .line 468
    iget v0, p0, Lorg/chromium/chrome/browser/infobar/InfoBarLayout;->mEnd:I

    iget v1, p0, Lorg/chromium/chrome/browser/infobar/InfoBarLayout;->mStart:I

    sub-int/2addr v0, v1

    return v0
.end method

.method private getWidthWithMargins(Landroid/view/View;)I
    .locals 3

    .prologue
    .line 485
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lorg/chromium/chrome/browser/infobar/InfoBarLayout$LayoutParams;

    .line 486
    invoke-virtual {p1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    iget v2, v0, Lorg/chromium/chrome/browser/infobar/InfoBarLayout$LayoutParams;->startMargin:I

    add-int/2addr v1, v2

    iget v0, v0, Lorg/chromium/chrome/browser/infobar/InfoBarLayout$LayoutParams;->endMargin:I

    add-int/2addr v0, v1

    return v0
.end method

.method private getWidthWithMargins(Lorg/chromium/chrome/browser/infobar/InfoBarLayout$Group;)I
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 475
    iget-boolean v1, p1, Lorg/chromium/chrome/browser/infobar/InfoBarLayout$Group;->isStacked:Z

    if-eqz v1, :cond_1

    iget-object v1, p1, Lorg/chromium/chrome/browser/infobar/InfoBarLayout$Group;->views:[Landroid/view/View;

    aget-object v0, v1, v0

    invoke-direct {p0, v0}, Lorg/chromium/chrome/browser/infobar/InfoBarLayout;->getWidthWithMargins(Landroid/view/View;)I

    move-result v0

    .line 481
    :cond_0
    return v0

    .line 478
    :cond_1
    iget-object v3, p1, Lorg/chromium/chrome/browser/infobar/InfoBarLayout$Group;->views:[Landroid/view/View;

    array-length v4, v3

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_0

    aget-object v2, v3, v1

    .line 479
    invoke-direct {p0, v2}, Lorg/chromium/chrome/browser/infobar/InfoBarLayout;->getWidthWithMargins(Landroid/view/View;)I

    move-result v2

    add-int/2addr v2, v0

    .line 478
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    move v0, v2

    goto :goto_0
.end method

.method private measureChildWithFixedWidth(Landroid/view/View;I)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 490
    const/high16 v0, 0x40000000    # 2.0f

    invoke-static {p2, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    .line 491
    invoke-static {v1, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 492
    invoke-virtual {p1, v0, v1}, Landroid/view/View;->measure(II)V

    .line 493
    return-void
.end method

.method private placeChild(Landroid/view/View;I)V
    .locals 5

    .prologue
    const/4 v4, 0x7

    .line 437
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lorg/chromium/chrome/browser/infobar/InfoBarLayout$LayoutParams;

    .line 439
    const/4 v1, 0x0

    iget v2, p0, Lorg/chromium/chrome/browser/infobar/InfoBarLayout;->mEnd:I

    iget v3, p0, Lorg/chromium/chrome/browser/infobar/InfoBarLayout;->mStart:I

    sub-int/2addr v2, v3

    iget v3, v0, Lorg/chromium/chrome/browser/infobar/InfoBarLayout$LayoutParams;->startMargin:I

    sub-int/2addr v2, v3

    iget v3, v0, Lorg/chromium/chrome/browser/infobar/InfoBarLayout$LayoutParams;->endMargin:I

    sub-int/2addr v2, v3

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 440
    invoke-virtual {p1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v2

    if-gt v2, v1, :cond_0

    if-ne p2, v4, :cond_1

    .line 441
    :cond_0
    invoke-direct {p0, p1, v1}, Lorg/chromium/chrome/browser/infobar/InfoBarLayout;->measureChildWithFixedWidth(Landroid/view/View;I)V

    .line 444
    :cond_1
    const v1, 0x800003

    if-eq p2, v1, :cond_2

    if-ne p2, v4, :cond_3

    .line 445
    :cond_2
    iget v1, p0, Lorg/chromium/chrome/browser/infobar/InfoBarLayout;->mStart:I

    iget v2, v0, Lorg/chromium/chrome/browser/infobar/InfoBarLayout$LayoutParams;->startMargin:I

    add-int/2addr v1, v2

    iput v1, v0, Lorg/chromium/chrome/browser/infobar/InfoBarLayout$LayoutParams;->start:I

    .line 446
    iget v1, v0, Lorg/chromium/chrome/browser/infobar/InfoBarLayout$LayoutParams;->start:I

    invoke-virtual {p1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v2

    add-int/2addr v1, v2

    iget v2, v0, Lorg/chromium/chrome/browser/infobar/InfoBarLayout$LayoutParams;->endMargin:I

    add-int/2addr v1, v2

    iput v1, p0, Lorg/chromium/chrome/browser/infobar/InfoBarLayout;->mStart:I

    .line 452
    :goto_0
    iget v1, p0, Lorg/chromium/chrome/browser/infobar/InfoBarLayout;->mTop:I

    iget v2, v0, Lorg/chromium/chrome/browser/infobar/InfoBarLayout$LayoutParams;->topMargin:I

    add-int/2addr v1, v2

    iput v1, v0, Lorg/chromium/chrome/browser/infobar/InfoBarLayout$LayoutParams;->top:I

    .line 453
    iget v1, p0, Lorg/chromium/chrome/browser/infobar/InfoBarLayout;->mBottom:I

    iget v2, v0, Lorg/chromium/chrome/browser/infobar/InfoBarLayout$LayoutParams;->top:I

    invoke-virtual {p1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v3

    add-int/2addr v2, v3

    iget v0, v0, Lorg/chromium/chrome/browser/infobar/InfoBarLayout$LayoutParams;->bottomMargin:I

    add-int/2addr v0, v2

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Lorg/chromium/chrome/browser/infobar/InfoBarLayout;->mBottom:I

    .line 454
    return-void

    .line 448
    :cond_3
    iget v1, p0, Lorg/chromium/chrome/browser/infobar/InfoBarLayout;->mEnd:I

    iget v2, v0, Lorg/chromium/chrome/browser/infobar/InfoBarLayout$LayoutParams;->endMargin:I

    sub-int/2addr v1, v2

    invoke-virtual {p1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v2

    sub-int/2addr v1, v2

    iput v1, v0, Lorg/chromium/chrome/browser/infobar/InfoBarLayout$LayoutParams;->start:I

    .line 449
    iget v1, v0, Lorg/chromium/chrome/browser/infobar/InfoBarLayout$LayoutParams;->start:I

    iget v2, v0, Lorg/chromium/chrome/browser/infobar/InfoBarLayout$LayoutParams;->startMargin:I

    sub-int/2addr v1, v2

    iput v1, p0, Lorg/chromium/chrome/browser/infobar/InfoBarLayout;->mEnd:I

    goto :goto_0
.end method

.method private placeGroup(Lorg/chromium/chrome/browser/infobar/InfoBarLayout$Group;)V
    .locals 3

    .prologue
    .line 419
    iget v0, p1, Lorg/chromium/chrome/browser/infobar/InfoBarLayout$Group;->gravity:I

    const v1, 0x800005

    if-ne v0, v1, :cond_1

    .line 420
    iget-object v0, p1, Lorg/chromium/chrome/browser/infobar/InfoBarLayout$Group;->views:[Landroid/view/View;

    array-length v0, v0

    add-int/lit8 v0, v0, -0x1

    :goto_0
    if-ltz v0, :cond_3

    .line 421
    iget-object v1, p1, Lorg/chromium/chrome/browser/infobar/InfoBarLayout$Group;->views:[Landroid/view/View;

    aget-object v1, v1, v0

    iget v2, p1, Lorg/chromium/chrome/browser/infobar/InfoBarLayout$Group;->gravity:I

    invoke-direct {p0, v1, v2}, Lorg/chromium/chrome/browser/infobar/InfoBarLayout;->placeChild(Landroid/view/View;I)V

    .line 422
    iget-boolean v1, p1, Lorg/chromium/chrome/browser/infobar/InfoBarLayout$Group;->isStacked:Z

    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lorg/chromium/chrome/browser/infobar/InfoBarLayout;->startRow()V

    .line 420
    :cond_0
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 425
    :cond_1
    const/4 v0, 0x0

    :goto_1
    iget-object v1, p1, Lorg/chromium/chrome/browser/infobar/InfoBarLayout$Group;->views:[Landroid/view/View;

    array-length v1, v1

    if-ge v0, v1, :cond_3

    .line 426
    iget-object v1, p1, Lorg/chromium/chrome/browser/infobar/InfoBarLayout$Group;->views:[Landroid/view/View;

    aget-object v1, v1, v0

    iget v2, p1, Lorg/chromium/chrome/browser/infobar/InfoBarLayout$Group;->gravity:I

    invoke-direct {p0, v1, v2}, Lorg/chromium/chrome/browser/infobar/InfoBarLayout;->placeChild(Landroid/view/View;I)V

    .line 427
    iget-boolean v1, p1, Lorg/chromium/chrome/browser/infobar/InfoBarLayout$Group;->isStacked:Z

    if-eqz v1, :cond_2

    iget-object v1, p1, Lorg/chromium/chrome/browser/infobar/InfoBarLayout$Group;->views:[Landroid/view/View;

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-eq v0, v1, :cond_2

    invoke-direct {p0}, Lorg/chromium/chrome/browser/infobar/InfoBarLayout;->startRow()V

    .line 425
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 430
    :cond_3
    return-void
.end method

.method private placeGroups()V
    .locals 7

    .prologue
    const v5, 0x800005

    const/4 v6, 0x2

    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 353
    invoke-direct {p0}, Lorg/chromium/chrome/browser/infobar/InfoBarLayout;->startRow()V

    .line 354
    iget-object v0, p0, Lorg/chromium/chrome/browser/infobar/InfoBarLayout;->mCloseButton:Landroid/widget/ImageButton;

    invoke-direct {p0, v0, v5}, Lorg/chromium/chrome/browser/infobar/InfoBarLayout;->placeChild(Landroid/view/View;I)V

    .line 355
    iget-object v0, p0, Lorg/chromium/chrome/browser/infobar/InfoBarLayout;->mMainGroup:Lorg/chromium/chrome/browser/infobar/InfoBarLayout$Group;

    invoke-direct {p0, v0}, Lorg/chromium/chrome/browser/infobar/InfoBarLayout;->placeGroup(Lorg/chromium/chrome/browser/infobar/InfoBarLayout$Group;)V

    .line 358
    iget-object v0, p0, Lorg/chromium/chrome/browser/infobar/InfoBarLayout;->mCustomGroup:Lorg/chromium/chrome/browser/infobar/InfoBarLayout$Group;

    if-eqz v0, :cond_9

    .line 359
    invoke-direct {p0, v4}, Lorg/chromium/chrome/browser/infobar/InfoBarLayout;->updateCustomGroupForRow(I)V

    .line 360
    iget-object v0, p0, Lorg/chromium/chrome/browser/infobar/InfoBarLayout;->mCustomGroup:Lorg/chromium/chrome/browser/infobar/InfoBarLayout$Group;

    invoke-direct {p0, v0}, Lorg/chromium/chrome/browser/infobar/InfoBarLayout;->getWidthWithMargins(Lorg/chromium/chrome/browser/infobar/InfoBarLayout$Group;)I

    move-result v0

    .line 364
    :goto_0
    iget-object v2, p0, Lorg/chromium/chrome/browser/infobar/InfoBarLayout;->mButtonGroup:Lorg/chromium/chrome/browser/infobar/InfoBarLayout$Group;

    if-eqz v2, :cond_8

    .line 365
    invoke-direct {p0, v4}, Lorg/chromium/chrome/browser/infobar/InfoBarLayout;->updateButtonGroupForRow(I)V

    .line 366
    iget-object v2, p0, Lorg/chromium/chrome/browser/infobar/InfoBarLayout;->mButtonGroup:Lorg/chromium/chrome/browser/infobar/InfoBarLayout$Group;

    invoke-direct {p0, v2}, Lorg/chromium/chrome/browser/infobar/InfoBarLayout;->getWidthWithMargins(Lorg/chromium/chrome/browser/infobar/InfoBarLayout$Group;)I

    move-result v2

    .line 369
    :goto_1
    invoke-direct {p0}, Lorg/chromium/chrome/browser/infobar/InfoBarLayout;->availableWidth()I

    move-result v3

    if-gt v0, v3, :cond_3

    move v3, v4

    .line 370
    :goto_2
    add-int/2addr v0, v2

    invoke-direct {p0}, Lorg/chromium/chrome/browser/infobar/InfoBarLayout;->availableWidth()I

    move-result v2

    if-gt v0, v2, :cond_4

    .line 372
    :goto_3
    iget-object v0, p0, Lorg/chromium/chrome/browser/infobar/InfoBarLayout;->mCustomGroup:Lorg/chromium/chrome/browser/infobar/InfoBarLayout$Group;

    if-eqz v0, :cond_0

    .line 373
    if-eqz v3, :cond_6

    .line 374
    iget-object v2, p0, Lorg/chromium/chrome/browser/infobar/InfoBarLayout;->mCustomGroup:Lorg/chromium/chrome/browser/infobar/InfoBarLayout$Group;

    iget-object v0, p0, Lorg/chromium/chrome/browser/infobar/InfoBarLayout;->mButtonGroup:Lorg/chromium/chrome/browser/infobar/InfoBarLayout$Group;

    if-eqz v0, :cond_5

    if-eqz v4, :cond_5

    const v0, 0x800003

    :goto_4
    iput v0, v2, Lorg/chromium/chrome/browser/infobar/InfoBarLayout$Group;->gravity:I

    .line 380
    :goto_5
    iget-object v0, p0, Lorg/chromium/chrome/browser/infobar/InfoBarLayout;->mCustomGroup:Lorg/chromium/chrome/browser/infobar/InfoBarLayout$Group;

    invoke-direct {p0, v0}, Lorg/chromium/chrome/browser/infobar/InfoBarLayout;->placeGroup(Lorg/chromium/chrome/browser/infobar/InfoBarLayout$Group;)V

    .line 383
    :cond_0
    iget-object v0, p0, Lorg/chromium/chrome/browser/infobar/InfoBarLayout;->mButtonGroup:Lorg/chromium/chrome/browser/infobar/InfoBarLayout$Group;

    if-eqz v0, :cond_2

    .line 384
    if-nez v4, :cond_1

    .line 385
    invoke-direct {p0}, Lorg/chromium/chrome/browser/infobar/InfoBarLayout;->startRow()V

    .line 386
    invoke-direct {p0, v6}, Lorg/chromium/chrome/browser/infobar/InfoBarLayout;->updateButtonGroupForRow(I)V

    .line 390
    iget-object v0, p0, Lorg/chromium/chrome/browser/infobar/InfoBarLayout;->mCustomGroup:Lorg/chromium/chrome/browser/infobar/InfoBarLayout$Group;

    if-nez v0, :cond_1

    .line 391
    iget-object v0, p0, Lorg/chromium/chrome/browser/infobar/InfoBarLayout;->mMessageView:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lorg/chromium/chrome/browser/infobar/InfoBarLayout$LayoutParams;

    .line 392
    iget v0, v0, Lorg/chromium/chrome/browser/infobar/InfoBarLayout$LayoutParams;->top:I

    iget-object v2, p0, Lorg/chromium/chrome/browser/infobar/InfoBarLayout;->mMessageView:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v2

    add-int/2addr v0, v2

    .line 393
    iget v2, p0, Lorg/chromium/chrome/browser/infobar/InfoBarLayout;->mTop:I

    iget v3, p0, Lorg/chromium/chrome/browser/infobar/InfoBarLayout;->mMargin:I

    mul-int/lit8 v3, v3, 0x2

    add-int/2addr v0, v3

    invoke-static {v2, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Lorg/chromium/chrome/browser/infobar/InfoBarLayout;->mTop:I

    .line 396
    :cond_1
    iget-object v0, p0, Lorg/chromium/chrome/browser/infobar/InfoBarLayout;->mButtonGroup:Lorg/chromium/chrome/browser/infobar/InfoBarLayout$Group;

    invoke-direct {p0, v0}, Lorg/chromium/chrome/browser/infobar/InfoBarLayout;->placeGroup(Lorg/chromium/chrome/browser/infobar/InfoBarLayout$Group;)V

    .line 399
    :cond_2
    invoke-direct {p0}, Lorg/chromium/chrome/browser/infobar/InfoBarLayout;->startRow()V

    .line 402
    if-eqz v4, :cond_7

    .line 403
    iget v2, p0, Lorg/chromium/chrome/browser/infobar/InfoBarLayout;->mBottom:I

    .line 404
    :goto_6
    invoke-virtual {p0}, Lorg/chromium/chrome/browser/infobar/InfoBarLayout;->getChildCount()I

    move-result v0

    if-ge v1, v0, :cond_7

    .line 405
    invoke-virtual {p0, v1}, Lorg/chromium/chrome/browser/infobar/InfoBarLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 406
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v3

    sub-int v3, v2, v3

    .line 407
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lorg/chromium/chrome/browser/infobar/InfoBarLayout$LayoutParams;

    .line 408
    div-int/lit8 v3, v3, 0x2

    iput v3, v0, Lorg/chromium/chrome/browser/infobar/InfoBarLayout$LayoutParams;->top:I

    .line 404
    add-int/lit8 v1, v1, 0x1

    goto :goto_6

    :cond_3
    move v3, v1

    .line 369
    goto :goto_2

    :cond_4
    move v4, v1

    .line 370
    goto :goto_3

    :cond_5
    move v0, v5

    .line 374
    goto :goto_4

    .line 377
    :cond_6
    invoke-direct {p0}, Lorg/chromium/chrome/browser/infobar/InfoBarLayout;->startRow()V

    .line 378
    invoke-direct {p0, v6}, Lorg/chromium/chrome/browser/infobar/InfoBarLayout;->updateCustomGroupForRow(I)V

    goto :goto_5

    .line 411
    :cond_7
    return-void

    :cond_8
    move v2, v1

    goto/16 :goto_1

    :cond_9
    move v0, v1

    goto/16 :goto_0
.end method

.method private startRow()V
    .locals 2

    .prologue
    .line 461
    iget v0, p0, Lorg/chromium/chrome/browser/infobar/InfoBarLayout;->mMargin:I

    iput v0, p0, Lorg/chromium/chrome/browser/infobar/InfoBarLayout;->mStart:I

    .line 462
    iget v0, p0, Lorg/chromium/chrome/browser/infobar/InfoBarLayout;->mWidth:I

    iget v1, p0, Lorg/chromium/chrome/browser/infobar/InfoBarLayout;->mMargin:I

    sub-int/2addr v0, v1

    iput v0, p0, Lorg/chromium/chrome/browser/infobar/InfoBarLayout;->mEnd:I

    .line 463
    iget v0, p0, Lorg/chromium/chrome/browser/infobar/InfoBarLayout;->mBottom:I

    iget v1, p0, Lorg/chromium/chrome/browser/infobar/InfoBarLayout;->mMargin:I

    add-int/2addr v0, v1

    iput v0, p0, Lorg/chromium/chrome/browser/infobar/InfoBarLayout;->mTop:I

    .line 464
    iget v0, p0, Lorg/chromium/chrome/browser/infobar/InfoBarLayout;->mTop:I

    iput v0, p0, Lorg/chromium/chrome/browser/infobar/InfoBarLayout;->mBottom:I

    .line 465
    return-void
.end method

.method private updateButtonGroupForRow(I)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v1, 0x0

    .line 503
    const/4 v0, 0x1

    if-ne p1, v0, :cond_1

    iget v0, p0, Lorg/chromium/chrome/browser/infobar/InfoBarLayout;->mMargin:I

    .line 504
    :goto_0
    iget-object v2, p0, Lorg/chromium/chrome/browser/infobar/InfoBarLayout;->mButtonGroup:Lorg/chromium/chrome/browser/infobar/InfoBarLayout$Group;

    iget v3, p0, Lorg/chromium/chrome/browser/infobar/InfoBarLayout;->mMargin:I

    div-int/lit8 v3, v3, 0x2

    invoke-virtual {v2, v3, v0, v0}, Lorg/chromium/chrome/browser/infobar/InfoBarLayout$Group;->setHorizontalMode(III)V

    .line 505
    iget-object v0, p0, Lorg/chromium/chrome/browser/infobar/InfoBarLayout;->mButtonGroup:Lorg/chromium/chrome/browser/infobar/InfoBarLayout$Group;

    const v2, 0x800005

    iput v2, v0, Lorg/chromium/chrome/browser/infobar/InfoBarLayout$Group;->gravity:I

    .line 507
    if-ne p1, v4, :cond_0

    iget-object v0, p0, Lorg/chromium/chrome/browser/infobar/InfoBarLayout;->mButtonGroup:Lorg/chromium/chrome/browser/infobar/InfoBarLayout$Group;

    iget-object v0, v0, Lorg/chromium/chrome/browser/infobar/InfoBarLayout$Group;->views:[Landroid/view/View;

    array-length v0, v0

    if-lt v0, v4, :cond_0

    .line 508
    invoke-direct {p0}, Lorg/chromium/chrome/browser/infobar/InfoBarLayout;->availableWidth()I

    move-result v0

    iget-object v2, p0, Lorg/chromium/chrome/browser/infobar/InfoBarLayout;->mButtonGroup:Lorg/chromium/chrome/browser/infobar/InfoBarLayout$Group;

    invoke-direct {p0, v2}, Lorg/chromium/chrome/browser/infobar/InfoBarLayout;->getWidthWithMargins(Lorg/chromium/chrome/browser/infobar/InfoBarLayout$Group;)I

    move-result v2

    sub-int v2, v0, v2

    .line 509
    if-gez v2, :cond_2

    .line 511
    iget-object v0, p0, Lorg/chromium/chrome/browser/infobar/InfoBarLayout;->mButtonGroup:Lorg/chromium/chrome/browser/infobar/InfoBarLayout$Group;

    iget v2, p0, Lorg/chromium/chrome/browser/infobar/InfoBarLayout;->mMargin:I

    div-int/lit8 v2, v2, 0x2

    invoke-virtual {v0, v2, v1}, Lorg/chromium/chrome/browser/infobar/InfoBarLayout$Group;->setVerticalMode(II)V

    .line 512
    iget-object v0, p0, Lorg/chromium/chrome/browser/infobar/InfoBarLayout;->mButtonGroup:Lorg/chromium/chrome/browser/infobar/InfoBarLayout$Group;

    const/4 v1, 0x7

    iput v1, v0, Lorg/chromium/chrome/browser/infobar/InfoBarLayout$Group;->gravity:I

    .line 518
    :cond_0
    :goto_1
    return-void

    :cond_1
    move v0, v1

    .line 503
    goto :goto_0

    .line 513
    :cond_2
    iget-object v0, p0, Lorg/chromium/chrome/browser/infobar/InfoBarLayout;->mButtonGroup:Lorg/chromium/chrome/browser/infobar/InfoBarLayout$Group;

    iget-object v0, v0, Lorg/chromium/chrome/browser/infobar/InfoBarLayout$Group;->views:[Landroid/view/View;

    array-length v0, v0

    const/4 v3, 0x3

    if-ne v0, v3, :cond_0

    .line 515
    iget-object v0, p0, Lorg/chromium/chrome/browser/infobar/InfoBarLayout;->mButtonGroup:Lorg/chromium/chrome/browser/infobar/InfoBarLayout$Group;

    iget-object v0, v0, Lorg/chromium/chrome/browser/infobar/InfoBarLayout$Group;->views:[Landroid/view/View;

    aget-object v0, v0, v1

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lorg/chromium/chrome/browser/infobar/InfoBarLayout$LayoutParams;

    iget v1, v0, Lorg/chromium/chrome/browser/infobar/InfoBarLayout$LayoutParams;->endMargin:I

    add-int/2addr v1, v2

    iput v1, v0, Lorg/chromium/chrome/browser/infobar/InfoBarLayout$LayoutParams;->endMargin:I

    goto :goto_1
.end method

.method private updateCustomGroupForRow(I)V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 524
    if-ne p1, v4, :cond_1

    iget v0, p0, Lorg/chromium/chrome/browser/infobar/InfoBarLayout;->mMargin:I

    .line 525
    :goto_0
    iget-object v2, p0, Lorg/chromium/chrome/browser/infobar/InfoBarLayout;->mCustomGroup:Lorg/chromium/chrome/browser/infobar/InfoBarLayout$Group;

    iget v3, p0, Lorg/chromium/chrome/browser/infobar/InfoBarLayout;->mMargin:I

    invoke-virtual {v2, v3, v0, v0}, Lorg/chromium/chrome/browser/infobar/InfoBarLayout$Group;->setHorizontalMode(III)V

    .line 526
    iget-object v0, p0, Lorg/chromium/chrome/browser/infobar/InfoBarLayout;->mCustomGroup:Lorg/chromium/chrome/browser/infobar/InfoBarLayout$Group;

    const v2, 0x800003

    iput v2, v0, Lorg/chromium/chrome/browser/infobar/InfoBarLayout$Group;->gravity:I

    .line 528
    if-ne p1, v5, :cond_0

    iget-object v0, p0, Lorg/chromium/chrome/browser/infobar/InfoBarLayout;->mCustomGroup:Lorg/chromium/chrome/browser/infobar/InfoBarLayout$Group;

    iget-object v0, v0, Lorg/chromium/chrome/browser/infobar/InfoBarLayout$Group;->views:[Landroid/view/View;

    array-length v0, v0

    if-ne v0, v5, :cond_0

    .line 529
    invoke-direct {p0}, Lorg/chromium/chrome/browser/infobar/InfoBarLayout;->availableWidth()I

    move-result v0

    iget-object v2, p0, Lorg/chromium/chrome/browser/infobar/InfoBarLayout;->mCustomGroup:Lorg/chromium/chrome/browser/infobar/InfoBarLayout$Group;

    invoke-direct {p0, v2}, Lorg/chromium/chrome/browser/infobar/InfoBarLayout;->getWidthWithMargins(Lorg/chromium/chrome/browser/infobar/InfoBarLayout$Group;)I

    move-result v2

    sub-int/2addr v0, v2

    .line 530
    if-gez v0, :cond_2

    .line 532
    iget-object v0, p0, Lorg/chromium/chrome/browser/infobar/InfoBarLayout;->mCustomGroup:Lorg/chromium/chrome/browser/infobar/InfoBarLayout$Group;

    iget v2, p0, Lorg/chromium/chrome/browser/infobar/InfoBarLayout;->mMargin:I

    invoke-virtual {v0, v1, v2}, Lorg/chromium/chrome/browser/infobar/InfoBarLayout$Group;->setVerticalMode(II)V

    .line 533
    iget-object v0, p0, Lorg/chromium/chrome/browser/infobar/InfoBarLayout;->mCustomGroup:Lorg/chromium/chrome/browser/infobar/InfoBarLayout$Group;

    const/4 v1, 0x7

    iput v1, v0, Lorg/chromium/chrome/browser/infobar/InfoBarLayout$Group;->gravity:I

    .line 544
    :cond_0
    :goto_1
    return-void

    :cond_1
    move v0, v1

    .line 524
    goto :goto_0

    .line 536
    :cond_2
    iget-object v2, p0, Lorg/chromium/chrome/browser/infobar/InfoBarLayout;->mCustomGroup:Lorg/chromium/chrome/browser/infobar/InfoBarLayout$Group;

    iget-object v2, v2, Lorg/chromium/chrome/browser/infobar/InfoBarLayout$Group;->views:[Landroid/view/View;

    aget-object v1, v2, v1

    .line 537
    iget-object v2, p0, Lorg/chromium/chrome/browser/infobar/InfoBarLayout;->mCustomGroup:Lorg/chromium/chrome/browser/infobar/InfoBarLayout$Group;

    iget-object v2, v2, Lorg/chromium/chrome/browser/infobar/InfoBarLayout$Group;->views:[Landroid/view/View;

    aget-object v2, v2, v4

    .line 538
    div-int/lit8 v3, v0, 0x2

    .line 539
    sub-int/2addr v0, v3

    .line 540
    invoke-virtual {v1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v4

    add-int/2addr v3, v4

    invoke-direct {p0, v1, v3}, Lorg/chromium/chrome/browser/infobar/InfoBarLayout;->measureChildWithFixedWidth(Landroid/view/View;I)V

    .line 541
    invoke-virtual {v2}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    add-int/2addr v0, v1

    invoke-direct {p0, v2, v0}, Lorg/chromium/chrome/browser/infobar/InfoBarLayout;->measureChildWithFixedWidth(Landroid/view/View;I)V

    goto :goto_1
.end method


# virtual methods
.method protected bridge synthetic generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;
    .locals 1

    .prologue
    .line 40
    invoke-virtual {p0}, Lorg/chromium/chrome/browser/infobar/InfoBarLayout;->generateDefaultLayoutParams()Lorg/chromium/chrome/browser/infobar/InfoBarLayout$LayoutParams;

    move-result-object v0

    return-object v0
.end method

.method protected generateDefaultLayoutParams()Lorg/chromium/chrome/browser/infobar/InfoBarLayout$LayoutParams;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 295
    new-instance v0, Lorg/chromium/chrome/browser/infobar/InfoBarLayout$LayoutParams;

    invoke-direct {v0, v1, v1, v1, v1}, Lorg/chromium/chrome/browser/infobar/InfoBarLayout$LayoutParams;-><init>(IIII)V

    return-object v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 555
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    sget v1, Lorg/chromium/chrome/R$id;->button_tertiary:I

    if-eq v0, v1, :cond_0

    .line 556
    iget-object v0, p0, Lorg/chromium/chrome/browser/infobar/InfoBarLayout;->mInfoBarView:Lorg/chromium/chrome/browser/infobar/InfoBarView;

    invoke-interface {v0, v2}, Lorg/chromium/chrome/browser/infobar/InfoBarView;->setControlsEnabled(Z)V

    .line 559
    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    sget v1, Lorg/chromium/chrome/R$id;->infobar_close_button:I

    if-ne v0, v1, :cond_2

    .line 560
    iget-object v0, p0, Lorg/chromium/chrome/browser/infobar/InfoBarLayout;->mInfoBarView:Lorg/chromium/chrome/browser/infobar/InfoBarView;

    invoke-interface {v0}, Lorg/chromium/chrome/browser/infobar/InfoBarView;->onCloseButtonClicked()V

    .line 568
    :cond_1
    :goto_0
    return-void

    .line 561
    :cond_2
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    sget v1, Lorg/chromium/chrome/R$id;->button_primary:I

    if-ne v0, v1, :cond_3

    .line 562
    iget-object v0, p0, Lorg/chromium/chrome/browser/infobar/InfoBarLayout;->mInfoBarView:Lorg/chromium/chrome/browser/infobar/InfoBarView;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lorg/chromium/chrome/browser/infobar/InfoBarView;->onButtonClicked(Z)V

    goto :goto_0

    .line 563
    :cond_3
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    sget v1, Lorg/chromium/chrome/R$id;->button_secondary:I

    if-ne v0, v1, :cond_4

    .line 564
    iget-object v0, p0, Lorg/chromium/chrome/browser/infobar/InfoBarLayout;->mInfoBarView:Lorg/chromium/chrome/browser/infobar/InfoBarView;

    invoke-interface {v0, v2}, Lorg/chromium/chrome/browser/infobar/InfoBarView;->onButtonClicked(Z)V

    goto :goto_0

    .line 565
    :cond_4
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    sget v1, Lorg/chromium/chrome/R$id;->button_tertiary:I

    if-ne v0, v1, :cond_1

    .line 566
    iget-object v0, p0, Lorg/chromium/chrome/browser/infobar/InfoBarLayout;->mInfoBarView:Lorg/chromium/chrome/browser/infobar/InfoBarView;

    invoke-interface {v0}, Lorg/chromium/chrome/browser/infobar/InfoBarView;->onLinkClicked()V

    goto :goto_0
.end method

.method protected onLayout(ZIIII)V
    .locals 9

    .prologue
    .line 301
    sub-int v5, p4, p2

    .line 302
    invoke-static {p0}, Lorg/chromium/base/ApiCompatibilityUtils;->isLayoutRtl(Landroid/view/View;)Z

    move-result v6

    .line 304
    const/4 v0, 0x0

    move v4, v0

    :goto_0
    invoke-virtual {p0}, Lorg/chromium/chrome/browser/infobar/InfoBarLayout;->getChildCount()I

    move-result v0

    if-ge v4, v0, :cond_0

    .line 305
    invoke-virtual {p0, v4}, Lorg/chromium/chrome/browser/infobar/InfoBarLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v7

    .line 306
    invoke-virtual {v7}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lorg/chromium/chrome/browser/infobar/InfoBarLayout$LayoutParams;

    .line 307
    iget v3, v0, Lorg/chromium/chrome/browser/infobar/InfoBarLayout$LayoutParams;->start:I

    .line 308
    iget v1, v0, Lorg/chromium/chrome/browser/infobar/InfoBarLayout$LayoutParams;->start:I

    invoke-virtual {v7}, Landroid/view/View;->getMeasuredWidth()I

    move-result v2

    add-int/2addr v1, v2

    .line 310
    if-eqz v6, :cond_1

    .line 311
    sub-int v2, v5, v1

    .line 312
    sub-int v1, v5, v3

    .line 316
    :goto_1
    iget v3, v0, Lorg/chromium/chrome/browser/infobar/InfoBarLayout$LayoutParams;->top:I

    iget v0, v0, Lorg/chromium/chrome/browser/infobar/InfoBarLayout$LayoutParams;->top:I

    invoke-virtual {v7}, Landroid/view/View;->getMeasuredHeight()I

    move-result v8

    add-int/2addr v0, v8

    invoke-virtual {v7, v2, v3, v1, v0}, Landroid/view/View;->layout(IIII)V

    .line 304
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_0

    .line 318
    :cond_0
    return-void

    :cond_1
    move v2, v3

    goto :goto_1
.end method

.method protected onMeasure(II)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 329
    sget-boolean v0, Lorg/chromium/chrome/browser/infobar/InfoBarLayout;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lorg/chromium/chrome/browser/infobar/InfoBarLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iget v0, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    const/4 v2, -0x2

    if-eq v0, v2, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    const-string/jumbo v1, "InfoBar heights cannot be constrained."

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 333
    :cond_0
    invoke-static {v1, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    move v0, v1

    .line 334
    :goto_0
    invoke-virtual {p0}, Lorg/chromium/chrome/browser/infobar/InfoBarLayout;->getChildCount()I

    move-result v3

    if-ge v0, v3, :cond_1

    .line 335
    invoke-virtual {p0, v0}, Lorg/chromium/chrome/browser/infobar/InfoBarLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {p0, v3, v2, v2}, Lorg/chromium/chrome/browser/infobar/InfoBarLayout;->measureChild(Landroid/view/View;II)V

    .line 334
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 340
    :cond_1
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    iget v2, p0, Lorg/chromium/chrome/browser/infobar/InfoBarLayout;->mMinWidth:I

    invoke-static {v0, v2}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Lorg/chromium/chrome/browser/infobar/InfoBarLayout;->mWidth:I

    .line 341
    iput v1, p0, Lorg/chromium/chrome/browser/infobar/InfoBarLayout;->mBottom:I

    iput v1, p0, Lorg/chromium/chrome/browser/infobar/InfoBarLayout;->mTop:I

    .line 342
    invoke-direct {p0}, Lorg/chromium/chrome/browser/infobar/InfoBarLayout;->placeGroups()V

    .line 344
    iget v0, p0, Lorg/chromium/chrome/browser/infobar/InfoBarLayout;->mWidth:I

    iget v1, p0, Lorg/chromium/chrome/browser/infobar/InfoBarLayout;->mBottom:I

    invoke-static {v1, p2}, Lorg/chromium/chrome/browser/infobar/InfoBarLayout;->resolveSize(II)I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lorg/chromium/chrome/browser/infobar/InfoBarLayout;->setMeasuredDimension(II)V

    .line 345
    return-void
.end method

.method public setButtons(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 231
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lorg/chromium/chrome/browser/infobar/InfoBarLayout;->setButtons(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 232
    return-void
.end method

.method public setButtons(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 10

    .prologue
    const/4 v9, 0x2

    const/4 v4, 0x0

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 242
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 278
    :goto_0
    return-void

    .line 244
    :cond_0
    invoke-virtual {p0}, Lorg/chromium/chrome/browser/infobar/InfoBarLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    .line 245
    sget v0, Lorg/chromium/chrome/R$layout;->infobar_button:I

    invoke-virtual {v2, v0, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 246
    sget v1, Lorg/chromium/chrome/R$id;->button_primary:I

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setId(I)V

    .line 247
    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 248
    invoke-virtual {v0, p1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 249
    sget v1, Lorg/chromium/chrome/R$drawable;->btn_infobar_blue:I

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setBackgroundResource(I)V

    .line 250
    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setTextColor(I)V

    .line 252
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 253
    new-array v1, v8, [Landroid/view/View;

    aput-object v0, v1, v7

    invoke-direct {p0, v1}, Lorg/chromium/chrome/browser/infobar/InfoBarLayout;->addGroup([Landroid/view/View;)Lorg/chromium/chrome/browser/infobar/InfoBarLayout$Group;

    move-result-object v0

    iput-object v0, p0, Lorg/chromium/chrome/browser/infobar/InfoBarLayout;->mButtonGroup:Lorg/chromium/chrome/browser/infobar/InfoBarLayout$Group;

    goto :goto_0

    .line 257
    :cond_1
    sget v1, Lorg/chromium/chrome/R$layout;->infobar_button:I

    invoke-virtual {v2, v1, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    .line 258
    sget v3, Lorg/chromium/chrome/R$id;->button_secondary:I

    invoke-virtual {v1, v3}, Landroid/widget/Button;->setId(I)V

    .line 259
    invoke-virtual {v1, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 260
    invoke-virtual {v1, p2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 261
    iget v3, p0, Lorg/chromium/chrome/browser/infobar/InfoBarLayout;->mAccentColor:I

    invoke-virtual {v1, v3}, Landroid/widget/Button;->setTextColor(I)V

    .line 263
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 264
    new-array v2, v9, [Landroid/view/View;

    aput-object v1, v2, v7

    aput-object v0, v2, v8

    invoke-direct {p0, v2}, Lorg/chromium/chrome/browser/infobar/InfoBarLayout;->addGroup([Landroid/view/View;)Lorg/chromium/chrome/browser/infobar/InfoBarLayout$Group;

    move-result-object v0

    iput-object v0, p0, Lorg/chromium/chrome/browser/infobar/InfoBarLayout;->mButtonGroup:Lorg/chromium/chrome/browser/infobar/InfoBarLayout$Group;

    goto :goto_0

    .line 268
    :cond_2
    sget v3, Lorg/chromium/chrome/R$layout;->infobar_button:I

    invoke-virtual {v2, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    .line 269
    sget v3, Lorg/chromium/chrome/R$id;->button_tertiary:I

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setId(I)V

    .line 270
    invoke-virtual {v2, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 271
    invoke-virtual {v2, p3}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 272
    iget v3, p0, Lorg/chromium/chrome/browser/infobar/InfoBarLayout;->mMargin:I

    div-int/lit8 v3, v3, 0x2

    invoke-virtual {v2}, Landroid/widget/Button;->getPaddingTop()I

    move-result v4

    iget v5, p0, Lorg/chromium/chrome/browser/infobar/InfoBarLayout;->mMargin:I

    div-int/lit8 v5, v5, 0x2

    invoke-virtual {v2}, Landroid/widget/Button;->getPaddingBottom()I

    move-result v6

    invoke-virtual {v2, v3, v4, v5, v6}, Landroid/widget/Button;->setPadding(IIII)V

    .line 274
    invoke-virtual {p0}, Lorg/chromium/chrome/browser/infobar/InfoBarLayout;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lorg/chromium/chrome/R$color;->infobar_tertiary_button_text:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setTextColor(I)V

    .line 277
    const/4 v3, 0x3

    new-array v3, v3, [Landroid/view/View;

    aput-object v2, v3, v7

    aput-object v1, v3, v8

    aput-object v0, v3, v9

    invoke-direct {p0, v3}, Lorg/chromium/chrome/browser/infobar/InfoBarLayout;->addGroup([Landroid/view/View;)Lorg/chromium/chrome/browser/infobar/InfoBarLayout$Group;

    move-result-object v0

    iput-object v0, p0, Lorg/chromium/chrome/browser/infobar/InfoBarLayout;->mButtonGroup:Lorg/chromium/chrome/browser/infobar/InfoBarLayout$Group;

    goto/16 :goto_0
.end method

.method public setCustomContent(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 224
    const/4 v0, 0x1

    new-array v0, v0, [Landroid/view/View;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    invoke-direct {p0, v0}, Lorg/chromium/chrome/browser/infobar/InfoBarLayout;->addGroup([Landroid/view/View;)Lorg/chromium/chrome/browser/infobar/InfoBarLayout$Group;

    move-result-object v0

    iput-object v0, p0, Lorg/chromium/chrome/browser/infobar/InfoBarLayout;->mCustomGroup:Lorg/chromium/chrome/browser/infobar/InfoBarLayout$Group;

    .line 225
    return-void
.end method

.method public setCustomContent(Landroid/view/View;Landroid/view/View;)V
    .locals 2

    .prologue
    .line 212
    const/4 v0, 0x2

    new-array v0, v0, [Landroid/view/View;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const/4 v1, 0x1

    aput-object p2, v0, v1

    invoke-direct {p0, v0}, Lorg/chromium/chrome/browser/infobar/InfoBarLayout;->addGroup([Landroid/view/View;)Lorg/chromium/chrome/browser/infobar/InfoBarLayout$Group;

    move-result-object v0

    iput-object v0, p0, Lorg/chromium/chrome/browser/infobar/InfoBarLayout;->mCustomGroup:Lorg/chromium/chrome/browser/infobar/InfoBarLayout$Group;

    .line 213
    return-void
.end method

.method public setMessage(Ljava/lang/CharSequence;)V
    .locals 2

    .prologue
    .line 200
    iget-object v0, p0, Lorg/chromium/chrome/browser/infobar/InfoBarLayout;->mMessageView:Landroid/widget/TextView;

    sget-object v1, Landroid/widget/TextView$BufferType;->SPANNABLE:Landroid/widget/TextView$BufferType;

    invoke-virtual {v0, p1, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;Landroid/widget/TextView$BufferType;)V

    .line 201
    return-void
.end method
