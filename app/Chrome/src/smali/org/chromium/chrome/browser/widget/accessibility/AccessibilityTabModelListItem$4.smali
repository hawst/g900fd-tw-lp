.class Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem$4;
.super Lorg/chromium/chrome/browser/EmptyTabObserver;
.source "AccessibilityTabModelListItem.java"


# instance fields
.field final synthetic this$0:Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem;


# direct methods
.method constructor <init>(Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem;)V
    .locals 0

    .prologue
    .line 331
    iput-object p1, p0, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem$4;->this$0:Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem;

    invoke-direct {p0}, Lorg/chromium/chrome/browser/EmptyTabObserver;-><init>()V

    return-void
.end method


# virtual methods
.method public onFaviconUpdated(Lorg/chromium/chrome/browser/Tab;)V
    .locals 1

    .prologue
    .line 334
    iget-object v0, p0, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem$4;->this$0:Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem;

    # invokes: Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem;->updateFavicon()V
    invoke-static {v0}, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem;->access$1200(Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem;)V

    .line 335
    iget-object v0, p0, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem$4;->this$0:Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem;

    # invokes: Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem;->notifyTabUpdated(Lorg/chromium/chrome/browser/Tab;)V
    invoke-static {v0, p1}, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem;->access$1300(Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem;Lorg/chromium/chrome/browser/Tab;)V

    .line 336
    return-void
.end method

.method public onTitleUpdated(Lorg/chromium/chrome/browser/Tab;)V
    .locals 1

    .prologue
    .line 340
    iget-object v0, p0, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem$4;->this$0:Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem;

    # invokes: Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem;->updateTabTitle()V
    invoke-static {v0}, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem;->access$1400(Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem;)V

    .line 341
    iget-object v0, p0, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem$4;->this$0:Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem;

    # invokes: Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem;->notifyTabUpdated(Lorg/chromium/chrome/browser/Tab;)V
    invoke-static {v0, p1}, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem;->access$1300(Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem;Lorg/chromium/chrome/browser/Tab;)V

    .line 342
    return-void
.end method

.method public onUrlUpdated(Lorg/chromium/chrome/browser/Tab;)V
    .locals 1

    .prologue
    .line 346
    iget-object v0, p0, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem$4;->this$0:Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem;

    # invokes: Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem;->updateTabTitle()V
    invoke-static {v0}, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem;->access$1400(Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem;)V

    .line 347
    iget-object v0, p0, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem$4;->this$0:Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem;

    # invokes: Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem;->notifyTabUpdated(Lorg/chromium/chrome/browser/Tab;)V
    invoke-static {v0, p1}, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem;->access$1300(Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelListItem;Lorg/chromium/chrome/browser/Tab;)V

    .line 348
    return-void
.end method
