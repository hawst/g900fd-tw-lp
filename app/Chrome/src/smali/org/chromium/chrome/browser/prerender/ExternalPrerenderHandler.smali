.class public Lorg/chromium/chrome/browser/prerender/ExternalPrerenderHandler;
.super Ljava/lang/Object;
.source "ExternalPrerenderHandler.java"


# annotations
.annotation runtime Lorg/chromium/base/JNINamespace;
.end annotation


# instance fields
.field private mNativeExternalPrerenderHandler:J


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    invoke-static {}, Lorg/chromium/chrome/browser/prerender/ExternalPrerenderHandler;->nativeInit()J

    move-result-wide v0

    iput-wide v0, p0, Lorg/chromium/chrome/browser/prerender/ExternalPrerenderHandler;->mNativeExternalPrerenderHandler:J

    .line 21
    return-void
.end method

.method public static hasPrerenderedUrl(Lorg/chromium/chrome/browser/profiles/Profile;Ljava/lang/String;J)Z
    .locals 2

    .prologue
    .line 38
    invoke-static {p0, p1, p2, p3}, Lorg/chromium/chrome/browser/prerender/ExternalPrerenderHandler;->nativeHasPrerenderedUrl(Lorg/chromium/chrome/browser/profiles/Profile;Ljava/lang/String;J)Z

    move-result v0

    return v0
.end method

.method private static native nativeAddPrerender(JLorg/chromium/chrome/browser/profiles/Profile;JLjava/lang/String;Ljava/lang/String;II)Z
.end method

.method private static native nativeCancelCurrentPrerender(J)V
.end method

.method private static native nativeHasCookieStoreLoaded(Lorg/chromium/chrome/browser/profiles/Profile;)Z
.end method

.method private static native nativeHasPrerenderedUrl(Lorg/chromium/chrome/browser/profiles/Profile;Ljava/lang/String;J)Z
.end method

.method private static native nativeInit()J
.end method


# virtual methods
.method public addPrerender(Lorg/chromium/chrome/browser/profiles/Profile;Ljava/lang/String;Ljava/lang/String;II)J
    .locals 10

    .prologue
    .line 24
    const/4 v1, 0x0

    invoke-static {v1}, Lorg/chromium/chrome/browser/ContentViewUtil;->createNativeWebContents(Z)J

    move-result-wide v4

    .line 25
    iget-wide v1, p0, Lorg/chromium/chrome/browser/prerender/ExternalPrerenderHandler;->mNativeExternalPrerenderHandler:J

    move-object v3, p1

    move-object v6, p2

    move-object v7, p3

    move v8, p4

    move v9, p5

    invoke-static/range {v1 .. v9}, Lorg/chromium/chrome/browser/prerender/ExternalPrerenderHandler;->nativeAddPrerender(JLorg/chromium/chrome/browser/profiles/Profile;JLjava/lang/String;Ljava/lang/String;II)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 30
    :goto_0
    return-wide v4

    .line 29
    :cond_0
    invoke-static {v4, v5}, Lorg/chromium/chrome/browser/ContentViewUtil;->destroyNativeWebContents(J)V

    .line 30
    const-wide/16 v4, 0x0

    goto :goto_0
.end method

.method public cancelCurrentPrerender()V
    .locals 2

    .prologue
    .line 34
    iget-wide v0, p0, Lorg/chromium/chrome/browser/prerender/ExternalPrerenderHandler;->mNativeExternalPrerenderHandler:J

    invoke-static {v0, v1}, Lorg/chromium/chrome/browser/prerender/ExternalPrerenderHandler;->nativeCancelCurrentPrerender(J)V

    .line 35
    return-void
.end method
