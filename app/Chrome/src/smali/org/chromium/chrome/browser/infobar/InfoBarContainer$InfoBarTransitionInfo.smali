.class Lorg/chromium/chrome/browser/infobar/InfoBarContainer$InfoBarTransitionInfo;
.super Ljava/lang/Object;
.source "InfoBarContainer.java"


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field public animationType:I

.field public target:Lorg/chromium/chrome/browser/infobar/InfoBar;

.field public toShow:Landroid/view/View;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 57
    const-class v0, Lorg/chromium/chrome/browser/infobar/InfoBarContainer;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/chromium/chrome/browser/infobar/InfoBarContainer$InfoBarTransitionInfo;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lorg/chromium/chrome/browser/infobar/InfoBar;Landroid/view/View;I)V
    .locals 1

    .prologue
    .line 67
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 68
    sget-boolean v0, Lorg/chromium/chrome/browser/infobar/InfoBarContainer$InfoBarTransitionInfo;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    if-gez p3, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 69
    :cond_0
    sget-boolean v0, Lorg/chromium/chrome/browser/infobar/InfoBarContainer$InfoBarTransitionInfo;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    const/4 v0, 0x3

    if-lt p3, v0, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 71
    :cond_1
    iput-object p1, p0, Lorg/chromium/chrome/browser/infobar/InfoBarContainer$InfoBarTransitionInfo;->target:Lorg/chromium/chrome/browser/infobar/InfoBar;

    .line 72
    iput-object p2, p0, Lorg/chromium/chrome/browser/infobar/InfoBarContainer$InfoBarTransitionInfo;->toShow:Landroid/view/View;

    .line 73
    iput p3, p0, Lorg/chromium/chrome/browser/infobar/InfoBarContainer$InfoBarTransitionInfo;->animationType:I

    .line 74
    return-void
.end method
