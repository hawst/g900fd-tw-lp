.class public Lorg/chromium/chrome/browser/infobar/SavePasswordInfoBarDelegate;
.super Ljava/lang/Object;
.source "SavePasswordInfoBarDelegate.java"


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    return-void
.end method

.method public static create()Lorg/chromium/chrome/browser/infobar/SavePasswordInfoBarDelegate;
    .locals 1

    .prologue
    .line 19
    new-instance v0, Lorg/chromium/chrome/browser/infobar/SavePasswordInfoBarDelegate;

    invoke-direct {v0}, Lorg/chromium/chrome/browser/infobar/SavePasswordInfoBarDelegate;-><init>()V

    return-object v0
.end method

.method private native nativeSetUseAdditionalAuthentication(JZ)V
.end method


# virtual methods
.method setUseAdditionalAuthentication(JZ)V
    .locals 1

    .prologue
    .line 49
    invoke-direct {p0, p1, p2, p3}, Lorg/chromium/chrome/browser/infobar/SavePasswordInfoBarDelegate;->nativeSetUseAdditionalAuthentication(JZ)V

    .line 50
    return-void
.end method

.method showSavePasswordInfoBar(JILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/chromium/chrome/browser/infobar/InfoBar;
    .locals 9

    .prologue
    .line 35
    invoke-static {p3}, Lorg/chromium/chrome/browser/ResourceId;->mapToDrawableId(I)I

    move-result v5

    .line 36
    new-instance v1, Lorg/chromium/chrome/browser/infobar/SavePasswordInfoBar;

    move-wide v2, p1

    move-object v4, p0

    move-object v6, p4

    move-object v7, p5

    move-object v8, p6

    invoke-direct/range {v1 .. v8}, Lorg/chromium/chrome/browser/infobar/SavePasswordInfoBar;-><init>(JLorg/chromium/chrome/browser/infobar/SavePasswordInfoBarDelegate;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 38
    return-object v1
.end method
