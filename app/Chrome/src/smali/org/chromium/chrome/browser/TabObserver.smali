.class public interface abstract Lorg/chromium/chrome/browser/TabObserver;
.super Ljava/lang/Object;
.source "TabObserver.java"


# virtual methods
.method public abstract onContentChanged(Lorg/chromium/chrome/browser/Tab;)V
.end method

.method public abstract onContextMenuShown(Lorg/chromium/chrome/browser/Tab;Landroid/view/ContextMenu;)V
.end method

.method public abstract onDestroyed(Lorg/chromium/chrome/browser/Tab;)V
.end method

.method public abstract onDidChangeThemeColor(I)V
.end method

.method public abstract onDidFailLoad(Lorg/chromium/chrome/browser/Tab;ZZILjava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract onDidNavigateMainFrame(Lorg/chromium/chrome/browser/Tab;Ljava/lang/String;Ljava/lang/String;ZZI)V
.end method

.method public abstract onDidStartProvisionalLoadForFrame(Lorg/chromium/chrome/browser/Tab;JJZLjava/lang/String;ZZ)V
.end method

.method public abstract onFaviconUpdated(Lorg/chromium/chrome/browser/Tab;)V
.end method

.method public abstract onLoadProgressChanged(Lorg/chromium/chrome/browser/Tab;I)V
.end method

.method public abstract onLoadStarted(Lorg/chromium/chrome/browser/Tab;)V
.end method

.method public abstract onLoadStopped(Lorg/chromium/chrome/browser/Tab;)V
.end method

.method public abstract onLoadUrl(Lorg/chromium/chrome/browser/Tab;Ljava/lang/String;I)V
.end method

.method public abstract onSSLStateUpdated(Lorg/chromium/chrome/browser/Tab;)V
.end method

.method public abstract onTitleUpdated(Lorg/chromium/chrome/browser/Tab;)V
.end method

.method public abstract onToggleFullscreenMode(Lorg/chromium/chrome/browser/Tab;Z)V
.end method

.method public abstract onUpdateUrl(Lorg/chromium/chrome/browser/Tab;Ljava/lang/String;)V
.end method

.method public abstract onUrlUpdated(Lorg/chromium/chrome/browser/Tab;)V
.end method

.method public abstract onWebContentsInstantSupportDisabled()V
.end method

.method public abstract onWebContentsSwapped(Lorg/chromium/chrome/browser/Tab;ZZ)V
.end method
