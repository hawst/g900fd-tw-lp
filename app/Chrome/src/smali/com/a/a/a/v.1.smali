.class final Lcom/a/a/a/v;
.super Ljava/lang/Object;
.source "CacheData.java"


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Lorg/json/JSONObject;

.field private final c:Lcom/a/a/a/y;

.field private synthetic d:Lcom/a/a/a/t;


# direct methods
.method private constructor <init>(Lcom/a/a/a/t;Ljava/lang/String;Lorg/json/JSONObject;Lcom/a/a/a/y;)V
    .locals 0

    .prologue
    .line 170
    iput-object p1, p0, Lcom/a/a/a/v;->d:Lcom/a/a/a/t;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 171
    invoke-static {p2}, Lcom/a/a/a/X;->a(Ljava/lang/Object;)V

    .line 172
    iput-object p2, p0, Lcom/a/a/a/v;->a:Ljava/lang/String;

    .line 173
    iput-object p3, p0, Lcom/a/a/a/v;->b:Lorg/json/JSONObject;

    .line 174
    iput-object p4, p0, Lcom/a/a/a/v;->c:Lcom/a/a/a/y;

    .line 175
    return-void
.end method

.method synthetic constructor <init>(Lcom/a/a/a/t;Ljava/lang/String;Lorg/json/JSONObject;Lcom/a/a/a/y;B)V
    .locals 0

    .prologue
    .line 150
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/a/a/a/v;-><init>(Lcom/a/a/a/t;Ljava/lang/String;Lorg/json/JSONObject;Lcom/a/a/a/y;)V

    return-void
.end method

.method static synthetic a(Lcom/a/a/a/v;Lcom/a/a/a/H;)V
    .locals 3

    .prologue
    .line 150
    if-nez p1, :cond_0

    const-string/jumbo v0, "CacheData"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "server returns null for key:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/a/a/a/v;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/a/a/a/v;->d:Lcom/a/a/a/t;

    invoke-static {v0}, Lcom/a/a/a/t;->a(Lcom/a/a/a/t;)Ljava/util/HashSet;

    move-result-object v0

    iget-object v1, p0, Lcom/a/a/a/v;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/a/a/a/v;->d:Lcom/a/a/a/t;

    iget-object v1, p0, Lcom/a/a/a/v;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/a/a/a/t;->a(Lcom/a/a/a/t;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/a/a/a/v;->d:Lcom/a/a/a/t;

    iget-object v1, p0, Lcom/a/a/a/v;->c:Lcom/a/a/a/y;

    invoke-static {v0, v1}, Lcom/a/a/a/t;->a(Lcom/a/a/a/t;Lcom/a/a/a/y;)V

    :goto_0
    return-void

    :cond_0
    sget-object v0, Lcom/a/a/a/d;->c:Lcom/a/a/a/d;

    invoke-virtual {v0}, Lcom/a/a/a/d;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string/jumbo v0, "CacheData"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "invalid or empty data returned for key: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/a/a/a/v;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/a/a/a/v;->d:Lcom/a/a/a/t;

    invoke-static {v0}, Lcom/a/a/a/t;->a(Lcom/a/a/a/t;)Ljava/util/HashSet;

    move-result-object v0

    iget-object v1, p0, Lcom/a/a/a/v;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/a/a/a/v;->d:Lcom/a/a/a/t;

    iget-object v1, p0, Lcom/a/a/a/v;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/a/a/a/t;->a(Lcom/a/a/a/t;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/a/a/a/v;->d:Lcom/a/a/a/t;

    iget-object v1, p0, Lcom/a/a/a/v;->c:Lcom/a/a/a/y;

    invoke-static {v0, v1}, Lcom/a/a/a/t;->a(Lcom/a/a/a/t;Lcom/a/a/a/y;)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/a/a/a/v;->b:Lorg/json/JSONObject;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/a/a/a/v;->b:Lorg/json/JSONObject;

    check-cast v0, Lcom/a/a/a/H;

    invoke-virtual {p1, v0}, Lcom/a/a/a/H;->a(Lcom/a/a/a/H;)V

    :cond_2
    iget-object v0, p0, Lcom/a/a/a/v;->d:Lcom/a/a/a/t;

    invoke-static {v0}, Lcom/a/a/a/t;->b(Lcom/a/a/a/t;)Lcom/a/a/a/H;

    move-result-object v0

    iget-object v1, p0, Lcom/a/a/a/v;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, p1}, Lcom/a/a/a/H;->a(Ljava/lang/String;Lorg/json/JSONObject;)V

    iget-object v0, p0, Lcom/a/a/a/v;->d:Lcom/a/a/a/t;

    iget-object v1, p0, Lcom/a/a/a/v;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/a/a/a/t;->a(Lcom/a/a/a/t;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/a/a/a/v;->d:Lcom/a/a/a/t;

    iget-object v1, p0, Lcom/a/a/a/v;->c:Lcom/a/a/a/y;

    invoke-static {v0, v1}, Lcom/a/a/a/t;->a(Lcom/a/a/a/t;Lcom/a/a/a/y;)V

    goto :goto_0
.end method
