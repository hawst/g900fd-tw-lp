.class public final enum Lcom/a/a/a/g;
.super Ljava/lang/Enum;
.source "AddressProblemType.java"


# static fields
.field public static final enum a:Lcom/a/a/a/g;

.field public static final enum b:Lcom/a/a/a/g;

.field public static final enum c:Lcom/a/a/a/g;

.field public static final enum d:Lcom/a/a/a/g;

.field public static final enum e:Lcom/a/a/a/g;

.field private static final synthetic f:[Lcom/a/a/a/g;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 29
    new-instance v0, Lcom/a/a/a/g;

    const-string/jumbo v1, "USING_UNUSED_FIELD"

    invoke-direct {v0, v1, v2}, Lcom/a/a/a/g;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/a/a/a/g;->a:Lcom/a/a/a/g;

    .line 36
    new-instance v0, Lcom/a/a/a/g;

    const-string/jumbo v1, "MISSING_REQUIRED_FIELD"

    invoke-direct {v0, v1, v3}, Lcom/a/a/a/g;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/a/a/a/g;->b:Lcom/a/a/a/g;

    .line 45
    new-instance v0, Lcom/a/a/a/g;

    const-string/jumbo v1, "UNKNOWN_VALUE"

    invoke-direct {v0, v1, v4}, Lcom/a/a/a/g;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/a/a/a/g;->c:Lcom/a/a/a/g;

    .line 54
    new-instance v0, Lcom/a/a/a/g;

    const-string/jumbo v1, "UNRECOGNIZED_FORMAT"

    invoke-direct {v0, v1, v5}, Lcom/a/a/a/g;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/a/a/a/g;->d:Lcom/a/a/a/g;

    .line 62
    new-instance v0, Lcom/a/a/a/g;

    const-string/jumbo v1, "MISMATCHING_VALUE"

    invoke-direct {v0, v1, v6}, Lcom/a/a/a/g;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/a/a/a/g;->e:Lcom/a/a/a/g;

    .line 22
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/a/a/a/g;

    sget-object v1, Lcom/a/a/a/g;->a:Lcom/a/a/a/g;

    aput-object v1, v0, v2

    sget-object v1, Lcom/a/a/a/g;->b:Lcom/a/a/a/g;

    aput-object v1, v0, v3

    sget-object v1, Lcom/a/a/a/g;->c:Lcom/a/a/a/g;

    aput-object v1, v0, v4

    sget-object v1, Lcom/a/a/a/g;->d:Lcom/a/a/a/g;

    aput-object v1, v0, v5

    sget-object v1, Lcom/a/a/a/g;->e:Lcom/a/a/a/g;

    aput-object v1, v0, v6

    sput-object v0, Lcom/a/a/a/g;->f:[Lcom/a/a/a/g;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/a/a/a/g;
    .locals 1

    .prologue
    .line 22
    const-class v0, Lcom/a/a/a/g;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/a/a/a/g;

    return-object v0
.end method

.method public static values()[Lcom/a/a/a/g;
    .locals 1

    .prologue
    .line 22
    sget-object v0, Lcom/a/a/a/g;->f:[Lcom/a/a/a/g;

    invoke-virtual {v0}, [Lcom/a/a/a/g;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/a/a/a/g;

    return-object v0
.end method
