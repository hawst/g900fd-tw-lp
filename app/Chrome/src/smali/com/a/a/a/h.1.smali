.class final Lcom/a/a/a/h;
.super Ljava/lang/Object;
.source "AddressUiComponent.java"


# instance fields
.field private a:Ljava/lang/String;

.field private b:Lcom/a/a/a/j;

.field private c:Ljava/util/List;

.field private d:Lcom/a/a/a/e;

.field private e:Lcom/a/a/a/e;

.field private f:Landroid/view/View;


# direct methods
.method constructor <init>(Lcom/a/a/a/e;)V
    .locals 1

    .prologue
    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/a/a/a/h;->c:Ljava/util/List;

    .line 58
    iput-object p1, p0, Lcom/a/a/a/h;->d:Lcom/a/a/a/e;

    .line 60
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/a/a/a/h;->e:Lcom/a/a/a/e;

    .line 61
    sget-object v0, Lcom/a/a/a/j;->a:Lcom/a/a/a/j;

    iput-object v0, p0, Lcom/a/a/a/h;->b:Lcom/a/a/a/j;

    .line 62
    return-void
.end method


# virtual methods
.method final a()Ljava/lang/String;
    .locals 2

    .prologue
    .line 92
    iget-object v0, p0, Lcom/a/a/a/h;->f:Landroid/view/View;

    if-nez v0, :cond_1

    .line 93
    iget-object v0, p0, Lcom/a/a/a/h;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_0

    const-string/jumbo v0, ""

    .line 105
    :goto_0
    return-object v0

    .line 93
    :cond_0
    iget-object v0, p0, Lcom/a/a/a/h;->c:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/a/a/Q;

    invoke-virtual {v0}, Lcom/a/a/a/Q;->b()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 95
    :cond_1
    sget-object v0, Lcom/a/a/a/i;->b:[I

    iget-object v1, p0, Lcom/a/a/a/h;->b:Lcom/a/a/a/j;

    invoke-virtual {v1}, Lcom/a/a/a/j;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 105
    const-string/jumbo v0, ""

    goto :goto_0

    .line 97
    :pswitch_0
    iget-object v0, p0, Lcom/a/a/a/h;->f:Landroid/view/View;

    check-cast v0, Landroid/widget/Spinner;

    invoke-virtual {v0}, Landroid/widget/Spinner;->getSelectedItem()Ljava/lang/Object;

    move-result-object v0

    .line 98
    if-nez v0, :cond_2

    .line 99
    const-string/jumbo v0, ""

    goto :goto_0

    .line 101
    :cond_2
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 103
    :pswitch_1
    iget-object v0, p0, Lcom/a/a/a/h;->f:Landroid/view/View;

    check-cast v0, Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 95
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method final a(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 150
    iput-object p1, p0, Lcom/a/a/a/h;->f:Landroid/view/View;

    .line 151
    return-void
.end method

.method final a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 114
    iput-object p1, p0, Lcom/a/a/a/h;->a:Ljava/lang/String;

    .line 115
    return-void
.end method

.method final a(Ljava/util/List;)V
    .locals 2

    .prologue
    .line 69
    iput-object p1, p0, Lcom/a/a/a/h;->c:Ljava/util/List;

    .line 70
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x1

    if-le v0, v1, :cond_0

    .line 71
    sget-object v0, Lcom/a/a/a/j;->b:Lcom/a/a/a/j;

    iput-object v0, p0, Lcom/a/a/a/h;->b:Lcom/a/a/a/j;

    .line 72
    sget-object v0, Lcom/a/a/a/i;->a:[I

    iget-object v1, p0, Lcom/a/a/a/h;->d:Lcom/a/a/a/e;

    invoke-virtual {v1}, Lcom/a/a/a/e;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 81
    :cond_0
    :goto_0
    return-void

    .line 74
    :pswitch_0
    sget-object v0, Lcom/a/a/a/e;->b:Lcom/a/a/a/e;

    iput-object v0, p0, Lcom/a/a/a/h;->e:Lcom/a/a/a/e;

    goto :goto_0

    .line 77
    :pswitch_1
    sget-object v0, Lcom/a/a/a/e;->a:Lcom/a/a/a/e;

    iput-object v0, p0, Lcom/a/a/a/h;->e:Lcom/a/a/a/e;

    goto :goto_0

    .line 80
    :pswitch_2
    sget-object v0, Lcom/a/a/a/e;->k:Lcom/a/a/a/e;

    iput-object v0, p0, Lcom/a/a/a/h;->e:Lcom/a/a/a/e;

    goto :goto_0

    .line 72
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 110
    iget-object v0, p0, Lcom/a/a/a/h;->a:Ljava/lang/String;

    return-object v0
.end method

.method final c()Lcom/a/a/a/j;
    .locals 1

    .prologue
    .line 118
    iget-object v0, p0, Lcom/a/a/a/h;->b:Lcom/a/a/a/j;

    return-object v0
.end method

.method final d()Ljava/util/List;
    .locals 1

    .prologue
    .line 126
    iget-object v0, p0, Lcom/a/a/a/h;->c:Ljava/util/List;

    return-object v0
.end method

.method final e()Lcom/a/a/a/e;
    .locals 1

    .prologue
    .line 134
    iget-object v0, p0, Lcom/a/a/a/h;->d:Lcom/a/a/a/e;

    return-object v0
.end method

.method final f()Lcom/a/a/a/e;
    .locals 1

    .prologue
    .line 142
    iget-object v0, p0, Lcom/a/a/a/h;->e:Lcom/a/a/a/e;

    return-object v0
.end method

.method final g()Landroid/view/View;
    .locals 1

    .prologue
    .line 154
    iget-object v0, p0, Lcom/a/a/a/h;->f:Landroid/view/View;

    return-object v0
.end method
