.class public final Lcom/a/a/a/E;
.super Ljava/lang/Object;
.source "FormOptions.java"


# instance fields
.field private a:Ljava/lang/String;

.field private final b:Ljava/util/EnumSet;

.field private final c:Ljava/util/EnumSet;

.field private final d:Ljava/util/EnumSet;

.field private final e:Ljava/util/EnumMap;

.field private final f:Ljava/util/Map;

.field private final g:Ljava/util/EnumMap;

.field private h:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 123
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 125
    const-string/jumbo v0, "addressform"

    iput-object v0, p0, Lcom/a/a/a/E;->a:Ljava/lang/String;

    .line 127
    const-class v0, Lcom/a/a/a/e;

    invoke-static {v0}, Ljava/util/EnumSet;->noneOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object v0

    iput-object v0, p0, Lcom/a/a/a/E;->b:Ljava/util/EnumSet;

    .line 130
    const-class v0, Lcom/a/a/a/e;

    invoke-static {v0}, Ljava/util/EnumSet;->noneOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object v0

    iput-object v0, p0, Lcom/a/a/a/E;->c:Ljava/util/EnumSet;

    .line 133
    const-class v0, Lcom/a/a/a/e;

    invoke-static {v0}, Ljava/util/EnumSet;->noneOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object v0

    iput-object v0, p0, Lcom/a/a/a/E;->d:Ljava/util/EnumSet;

    .line 136
    new-instance v0, Ljava/util/EnumMap;

    const-class v1, Lcom/a/a/a/e;

    invoke-direct {v0, v1}, Ljava/util/EnumMap;-><init>(Ljava/lang/Class;)V

    iput-object v0, p0, Lcom/a/a/a/E;->e:Ljava/util/EnumMap;

    .line 139
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/a/a/a/E;->f:Ljava/util/Map;

    .line 142
    new-instance v0, Ljava/util/EnumMap;

    const-class v1, Lcom/a/a/a/e;

    invoke-direct {v0, v1}, Ljava/util/EnumMap;-><init>(Ljava/lang/Class;)V

    iput-object v0, p0, Lcom/a/a/a/E;->g:Ljava/util/EnumMap;

    .line 148
    new-instance v0, Lcom/a/a/a/t;

    invoke-direct {v0}, Lcom/a/a/a/t;-><init>()V

    invoke-virtual {v0}, Lcom/a/a/a/t;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/a/a/a/E;->h:Ljava/lang/String;

    return-void
.end method

.method static synthetic a(Lcom/a/a/a/E;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 123
    iget-object v0, p0, Lcom/a/a/a/E;->a:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic b(Lcom/a/a/a/E;)Ljava/util/EnumSet;
    .locals 1

    .prologue
    .line 123
    iget-object v0, p0, Lcom/a/a/a/E;->c:Ljava/util/EnumSet;

    return-object v0
.end method

.method static synthetic c(Lcom/a/a/a/E;)Ljava/util/EnumSet;
    .locals 1

    .prologue
    .line 123
    iget-object v0, p0, Lcom/a/a/a/E;->d:Ljava/util/EnumSet;

    return-object v0
.end method

.method static synthetic d(Lcom/a/a/a/E;)Ljava/util/EnumSet;
    .locals 1

    .prologue
    .line 123
    iget-object v0, p0, Lcom/a/a/a/E;->b:Ljava/util/EnumSet;

    return-object v0
.end method

.method static synthetic e(Lcom/a/a/a/E;)Ljava/util/EnumMap;
    .locals 1

    .prologue
    .line 123
    iget-object v0, p0, Lcom/a/a/a/E;->e:Ljava/util/EnumMap;

    return-object v0
.end method

.method static synthetic f(Lcom/a/a/a/E;)Ljava/util/Map;
    .locals 1

    .prologue
    .line 123
    iget-object v0, p0, Lcom/a/a/a/E;->f:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic g(Lcom/a/a/a/E;)Ljava/util/EnumMap;
    .locals 1

    .prologue
    .line 123
    iget-object v0, p0, Lcom/a/a/a/E;->g:Ljava/util/EnumMap;

    return-object v0
.end method

.method static synthetic h(Lcom/a/a/a/E;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 123
    iget-object v0, p0, Lcom/a/a/a/E;->h:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/a/a/a/D;
    .locals 2

    .prologue
    .line 284
    new-instance v0, Lcom/a/a/a/D;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/a/a/a/D;-><init>(Lcom/a/a/a/E;B)V

    return-object v0
.end method
