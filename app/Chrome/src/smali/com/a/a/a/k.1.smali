.class public final Lcom/a/a/a/k;
.super Ljava/lang/Object;
.source "AddressVerificationNodeData.java"


# instance fields
.field private final a:Ljava/util/Map;


# direct methods
.method public constructor <init>(Ljava/util/Map;)V
    .locals 1

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    const-string/jumbo v0, "Cannot construct StandardNodeData with null map"

    invoke-static {v0}, Lcom/a/a/a/X;->a(Ljava/lang/Object;)V

    .line 35
    iput-object p1, p0, Lcom/a/a/a/k;->a:Ljava/util/Map;

    .line 36
    return-void
.end method


# virtual methods
.method public final a(Lcom/a/a/a/d;)Z
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/a/a/a/k;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final b(Lcom/a/a/a/d;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/a/a/a/k;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method
