.class final Lcom/a/a/a/M;
.super Ljava/lang/Object;
.source "LookupKey.java"


# instance fields
.field private a:Lcom/a/a/a/N;

.field private b:Lcom/a/a/a/O;

.field private c:Ljava/util/Map;

.field private d:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/a/a/a/L;)V
    .locals 6

    .prologue
    .line 301
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 284
    sget-object v0, Lcom/a/a/a/O;->b:Lcom/a/a/a/O;

    iput-object v0, p0, Lcom/a/a/a/M;->b:Lcom/a/a/a/O;

    .line 286
    new-instance v0, Ljava/util/EnumMap;

    const-class v1, Lcom/a/a/a/e;

    invoke-direct {v0, v1}, Ljava/util/EnumMap;-><init>(Ljava/lang/Class;)V

    iput-object v0, p0, Lcom/a/a/a/M;->c:Ljava/util/Map;

    .line 302
    invoke-static {p1}, Lcom/a/a/a/L;->a(Lcom/a/a/a/L;)Lcom/a/a/a/N;

    move-result-object v0

    iput-object v0, p0, Lcom/a/a/a/M;->a:Lcom/a/a/a/N;

    .line 303
    invoke-static {p1}, Lcom/a/a/a/L;->b(Lcom/a/a/a/L;)Lcom/a/a/a/O;

    move-result-object v0

    iput-object v0, p0, Lcom/a/a/a/M;->b:Lcom/a/a/a/O;

    .line 304
    invoke-static {p1}, Lcom/a/a/a/L;->c(Lcom/a/a/a/L;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/a/a/a/M;->d:Ljava/lang/String;

    .line 305
    invoke-static {}, Lcom/a/a/a/L;->b()[Lcom/a/a/a/e;

    move-result-object v1

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    .line 306
    invoke-static {p1}, Lcom/a/a/a/L;->d(Lcom/a/a/a/L;)Ljava/util/Map;

    move-result-object v4

    invoke-interface {v4, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 307
    iget-object v4, p0, Lcom/a/a/a/M;->c:Ljava/util/Map;

    invoke-static {p1}, Lcom/a/a/a/L;->d(Lcom/a/a/a/L;)Ljava/util/Map;

    move-result-object v5

    invoke-interface {v5, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    invoke-interface {v4, v3, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 305
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 311
    :cond_0
    return-void
.end method

.method constructor <init>(Lcom/a/a/a/N;)V
    .locals 2

    .prologue
    .line 294
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 284
    sget-object v0, Lcom/a/a/a/O;->b:Lcom/a/a/a/O;

    iput-object v0, p0, Lcom/a/a/a/M;->b:Lcom/a/a/a/O;

    .line 286
    new-instance v0, Ljava/util/EnumMap;

    const-class v1, Lcom/a/a/a/e;

    invoke-direct {v0, v1}, Ljava/util/EnumMap;-><init>(Ljava/lang/Class;)V

    iput-object v0, p0, Lcom/a/a/a/M;->c:Ljava/util/Map;

    .line 295
    iput-object p1, p0, Lcom/a/a/a/M;->a:Lcom/a/a/a/N;

    .line 296
    return-void
.end method

.method constructor <init>(Ljava/lang/String;)V
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v1, 0x2

    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 322
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 284
    sget-object v0, Lcom/a/a/a/O;->b:Lcom/a/a/a/O;

    iput-object v0, p0, Lcom/a/a/a/M;->b:Lcom/a/a/a/O;

    .line 286
    new-instance v0, Ljava/util/EnumMap;

    const-class v2, Lcom/a/a/a/e;

    invoke-direct {v0, v2}, Ljava/util/EnumMap;-><init>(Ljava/lang/Class;)V

    iput-object v0, p0, Lcom/a/a/a/M;->c:Ljava/util/Map;

    .line 323
    const-string/jumbo v0, "/"

    invoke-virtual {p1, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 325
    aget-object v0, v2, v5

    sget-object v3, Lcom/a/a/a/N;->a:Lcom/a/a/a/N;

    invoke-virtual {v3}, Lcom/a/a/a/N;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    aget-object v0, v2, v5

    sget-object v3, Lcom/a/a/a/N;->b:Lcom/a/a/a/N;

    invoke-virtual {v3}, Lcom/a/a/a/N;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 327
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "Wrong key type: "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    aget-object v2, v2, v5

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 329
    :cond_0
    array-length v0, v2

    invoke-static {}, Lcom/a/a/a/L;->b()[Lcom/a/a/a/e;

    move-result-object v3

    array-length v3, v3

    add-int/lit8 v3, v3, 0x1

    if-le v0, v3, :cond_1

    .line 330
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "input key \'"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "\' deeper than supported hierarchy"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 333
    :cond_1
    aget-object v0, v2, v5

    const-string/jumbo v3, "data"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 334
    sget-object v0, Lcom/a/a/a/N;->a:Lcom/a/a/a/N;

    iput-object v0, p0, Lcom/a/a/a/M;->a:Lcom/a/a/a/N;

    .line 337
    array-length v0, v2

    if-le v0, v4, :cond_4

    .line 338
    aget-object v0, v2, v4

    invoke-static {v0}, Lcom/a/a/a/X;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 339
    const-string/jumbo v3, "--"

    invoke-virtual {v0, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 340
    const-string/jumbo v3, "--"

    invoke-virtual {v0, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 341
    array-length v0, v3

    if-eq v0, v1, :cond_2

    .line 342
    new-instance v0, Ljava/lang/RuntimeException;

    const-string/jumbo v1, "Wrong format: Substring should be country code--language code"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 346
    :cond_2
    aget-object v0, v3, v5

    .line 347
    aget-object v3, v3, v4

    iput-object v3, p0, Lcom/a/a/a/M;->d:Ljava/lang/String;

    .line 349
    :cond_3
    iget-object v3, p0, Lcom/a/a/a/M;->c:Ljava/util/Map;

    invoke-static {}, Lcom/a/a/a/L;->b()[Lcom/a/a/a/e;

    move-result-object v4

    aget-object v4, v4, v5

    invoke-interface {v3, v4, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 353
    :cond_4
    array-length v0, v2

    if-le v0, v1, :cond_8

    move v0, v1

    .line 354
    :goto_0
    array-length v1, v2

    if-ge v0, v1, :cond_8

    .line 355
    aget-object v1, v2, v0

    invoke-static {v1}, Lcom/a/a/a/X;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 356
    if-eqz v1, :cond_8

    .line 357
    iget-object v3, p0, Lcom/a/a/a/M;->c:Ljava/util/Map;

    invoke-static {}, Lcom/a/a/a/L;->b()[Lcom/a/a/a/e;

    move-result-object v4

    add-int/lit8 v5, v0, -0x1

    aget-object v4, v4, v5

    invoke-interface {v3, v4, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 354
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 362
    :cond_5
    aget-object v0, v2, v5

    const-string/jumbo v3, "examples"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 363
    sget-object v0, Lcom/a/a/a/N;->b:Lcom/a/a/a/N;

    iput-object v0, p0, Lcom/a/a/a/M;->a:Lcom/a/a/a/N;

    .line 366
    array-length v0, v2

    if-le v0, v4, :cond_6

    .line 367
    iget-object v0, p0, Lcom/a/a/a/M;->c:Ljava/util/Map;

    sget-object v3, Lcom/a/a/a/e;->k:Lcom/a/a/a/e;

    aget-object v4, v2, v4

    invoke-interface {v0, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 371
    :cond_6
    array-length v0, v2

    if-le v0, v1, :cond_7

    .line 372
    aget-object v0, v2, v1

    .line 373
    const-string/jumbo v1, "local"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 374
    sget-object v0, Lcom/a/a/a/O;->b:Lcom/a/a/a/O;

    iput-object v0, p0, Lcom/a/a/a/M;->b:Lcom/a/a/a/O;

    .line 384
    :cond_7
    :goto_1
    array-length v0, v2

    if-le v0, v6, :cond_8

    aget-object v0, v2, v6

    const-string/jumbo v1, "_default"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_8

    .line 385
    aget-object v0, v2, v6

    iput-object v0, p0, Lcom/a/a/a/M;->d:Ljava/lang/String;

    .line 388
    :cond_8
    return-void

    .line 375
    :cond_9
    const-string/jumbo v1, "latin"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 376
    sget-object v0, Lcom/a/a/a/O;->a:Lcom/a/a/a/O;

    iput-object v0, p0, Lcom/a/a/a/M;->b:Lcom/a/a/a/O;

    goto :goto_1

    .line 378
    :cond_a
    new-instance v0, Ljava/lang/RuntimeException;

    const-string/jumbo v1, "Script type has to be either latin or local."

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method static synthetic a(Lcom/a/a/a/M;)Lcom/a/a/a/N;
    .locals 1

    .prologue
    .line 278
    iget-object v0, p0, Lcom/a/a/a/M;->a:Lcom/a/a/a/N;

    return-object v0
.end method

.method static synthetic a(Lcom/a/a/a/M;Lcom/a/a/a/O;)Lcom/a/a/a/O;
    .locals 0

    .prologue
    .line 278
    iput-object p1, p0, Lcom/a/a/a/M;->b:Lcom/a/a/a/O;

    return-object p1
.end method

.method static synthetic a(Lcom/a/a/a/M;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 278
    iput-object p1, p0, Lcom/a/a/a/M;->d:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic b(Lcom/a/a/a/M;)Lcom/a/a/a/O;
    .locals 1

    .prologue
    .line 278
    iget-object v0, p0, Lcom/a/a/a/M;->b:Lcom/a/a/a/O;

    return-object v0
.end method

.method static synthetic c(Lcom/a/a/a/M;)Ljava/util/Map;
    .locals 1

    .prologue
    .line 278
    iget-object v0, p0, Lcom/a/a/a/M;->c:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic d(Lcom/a/a/a/M;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 278
    iget-object v0, p0, Lcom/a/a/a/M;->d:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method final a()Lcom/a/a/a/L;
    .locals 2

    .prologue
    .line 433
    new-instance v0, Lcom/a/a/a/L;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/a/a/a/L;-><init>(Lcom/a/a/a/M;B)V

    return-object v0
.end method

.method final a(Lcom/a/a/a/a;)Lcom/a/a/a/M;
    .locals 3

    .prologue
    .line 403
    invoke-virtual {p1}, Lcom/a/a/a/a;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/a/a/a/M;->d:Ljava/lang/String;

    .line 404
    iget-object v0, p0, Lcom/a/a/a/M;->d:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 405
    iget-object v0, p0, Lcom/a/a/a/M;->d:Ljava/lang/String;

    invoke-static {v0}, Lcom/a/a/a/X;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 406
    sget-object v0, Lcom/a/a/a/O;->a:Lcom/a/a/a/O;

    iput-object v0, p0, Lcom/a/a/a/M;->b:Lcom/a/a/a/O;

    .line 410
    :cond_0
    invoke-virtual {p1}, Lcom/a/a/a/a;->a()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_2

    .line 429
    :cond_1
    :goto_0
    return-object p0

    .line 413
    :cond_2
    iget-object v0, p0, Lcom/a/a/a/M;->c:Ljava/util/Map;

    sget-object v1, Lcom/a/a/a/e;->k:Lcom/a/a/a/e;

    invoke-virtual {p1}, Lcom/a/a/a/a;->a()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 415
    invoke-virtual {p1}, Lcom/a/a/a/a;->d()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 418
    iget-object v0, p0, Lcom/a/a/a/M;->c:Ljava/util/Map;

    sget-object v1, Lcom/a/a/a/e;->a:Lcom/a/a/a/e;

    invoke-virtual {p1}, Lcom/a/a/a/a;->d()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 420
    invoke-virtual {p1}, Lcom/a/a/a/a;->e()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 423
    iget-object v0, p0, Lcom/a/a/a/M;->c:Ljava/util/Map;

    sget-object v1, Lcom/a/a/a/e;->b:Lcom/a/a/a/e;

    invoke-virtual {p1}, Lcom/a/a/a/a;->e()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 425
    invoke-virtual {p1}, Lcom/a/a/a/a;->f()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 428
    iget-object v0, p0, Lcom/a/a/a/M;->c:Ljava/util/Map;

    sget-object v1, Lcom/a/a/a/e;->g:Lcom/a/a/a/e;

    invoke-virtual {p1}, Lcom/a/a/a/a;->f()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method
