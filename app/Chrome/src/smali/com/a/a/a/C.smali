.class final Lcom/a/a/a/C;
.super Ljava/lang/Object;
.source "FormController.java"

# interfaces
.implements Lcom/a/a/a/y;


# instance fields
.field private synthetic a:Lcom/a/a/a/L;

.field private synthetic b:Lcom/a/a/a/y;

.field private synthetic c:Ljava/util/Queue;

.field private synthetic d:Lcom/a/a/a/B;


# direct methods
.method constructor <init>(Lcom/a/a/a/B;Lcom/a/a/a/L;Lcom/a/a/a/y;Ljava/util/Queue;)V
    .locals 0

    .prologue
    .line 136
    iput-object p1, p0, Lcom/a/a/a/C;->d:Lcom/a/a/a/B;

    iput-object p2, p0, Lcom/a/a/a/C;->a:Lcom/a/a/a/L;

    iput-object p3, p0, Lcom/a/a/a/C;->b:Lcom/a/a/a/y;

    iput-object p4, p0, Lcom/a/a/a/C;->c:Ljava/util/Queue;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 0

    .prologue
    .line 139
    return-void
.end method

.method public final b()V
    .locals 5

    .prologue
    .line 143
    iget-object v0, p0, Lcom/a/a/a/C;->d:Lcom/a/a/a/B;

    iget-object v1, p0, Lcom/a/a/a/C;->a:Lcom/a/a/a/L;

    invoke-virtual {v0, v1}, Lcom/a/a/a/B;->a(Lcom/a/a/a/L;)Ljava/util/List;

    move-result-object v2

    .line 144
    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 145
    iget-object v0, p0, Lcom/a/a/a/C;->b:Lcom/a/a/a/y;

    if-eqz v0, :cond_0

    .line 146
    iget-object v0, p0, Lcom/a/a/a/C;->b:Lcom/a/a/a/y;

    invoke-interface {v0}, Lcom/a/a/a/y;->b()V

    .line 167
    :cond_0
    :goto_0
    return-void

    .line 150
    :cond_1
    iget-object v0, p0, Lcom/a/a/a/C;->c:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->size()I

    move-result v0

    if-lez v0, :cond_3

    .line 151
    iget-object v0, p0, Lcom/a/a/a/C;->c:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->remove()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 152
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/a/a/a/Q;

    .line 153
    invoke-virtual {v1, v0}, Lcom/a/a/a/Q;->a(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 154
    iget-object v0, p0, Lcom/a/a/a/C;->d:Lcom/a/a/a/B;

    iget-object v2, p0, Lcom/a/a/a/C;->a:Lcom/a/a/a/L;

    invoke-virtual {v1}, Lcom/a/a/a/Q;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v2, v1}, Lcom/a/a/a/B;->a(Lcom/a/a/a/B;Lcom/a/a/a/L;Ljava/lang/String;)Lcom/a/a/a/L;

    move-result-object v0

    .line 155
    iget-object v1, p0, Lcom/a/a/a/C;->d:Lcom/a/a/a/B;

    iget-object v2, p0, Lcom/a/a/a/C;->c:Ljava/util/Queue;

    iget-object v3, p0, Lcom/a/a/a/C;->b:Lcom/a/a/a/y;

    invoke-static {v1, v0, v2, v3}, Lcom/a/a/a/B;->a(Lcom/a/a/a/B;Lcom/a/a/a/L;Ljava/util/Queue;Lcom/a/a/a/y;)V

    goto :goto_0

    .line 163
    :cond_3
    const/4 v0, 0x0

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/a/a/Q;

    invoke-virtual {v0}, Lcom/a/a/a/Q;->a()Ljava/lang/String;

    move-result-object v0

    .line 164
    iget-object v1, p0, Lcom/a/a/a/C;->d:Lcom/a/a/a/B;

    iget-object v2, p0, Lcom/a/a/a/C;->a:Lcom/a/a/a/L;

    invoke-static {v1, v2, v0}, Lcom/a/a/a/B;->a(Lcom/a/a/a/B;Lcom/a/a/a/L;Ljava/lang/String;)Lcom/a/a/a/L;

    move-result-object v0

    .line 165
    new-instance v1, Ljava/util/LinkedList;

    invoke-direct {v1}, Ljava/util/LinkedList;-><init>()V

    .line 166
    iget-object v2, p0, Lcom/a/a/a/C;->d:Lcom/a/a/a/B;

    iget-object v3, p0, Lcom/a/a/a/C;->b:Lcom/a/a/a/y;

    invoke-static {v2, v0, v1, v3}, Lcom/a/a/a/B;->a(Lcom/a/a/a/B;Lcom/a/a/a/L;Ljava/util/Queue;Lcom/a/a/a/y;)V

    goto :goto_0
.end method
