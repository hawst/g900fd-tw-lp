.class final enum Lcom/a/a/a/N;
.super Ljava/lang/Enum;
.source "LookupKey.java"


# static fields
.field public static final enum a:Lcom/a/a/a/N;

.field public static final enum b:Lcom/a/a/a/N;

.field private static final synthetic c:[Lcom/a/a/a/N;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 47
    new-instance v0, Lcom/a/a/a/N;

    const-string/jumbo v1, "DATA"

    invoke-direct {v0, v1, v2}, Lcom/a/a/a/N;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/a/a/a/N;->a:Lcom/a/a/a/N;

    .line 51
    new-instance v0, Lcom/a/a/a/N;

    const-string/jumbo v1, "EXAMPLES"

    invoke-direct {v0, v1, v3}, Lcom/a/a/a/N;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/a/a/a/N;->b:Lcom/a/a/a/N;

    .line 42
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/a/a/a/N;

    sget-object v1, Lcom/a/a/a/N;->a:Lcom/a/a/a/N;

    aput-object v1, v0, v2

    sget-object v1, Lcom/a/a/a/N;->b:Lcom/a/a/a/N;

    aput-object v1, v0, v3

    sput-object v0, Lcom/a/a/a/N;->c:[Lcom/a/a/a/N;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/a/a/a/N;
    .locals 1

    .prologue
    .line 42
    const-class v0, Lcom/a/a/a/N;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/a/a/a/N;

    return-object v0
.end method

.method public static values()[Lcom/a/a/a/N;
    .locals 1

    .prologue
    .line 42
    sget-object v0, Lcom/a/a/a/N;->c:[Lcom/a/a/a/N;

    invoke-virtual {v0}, [Lcom/a/a/a/N;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/a/a/a/N;

    return-object v0
.end method
