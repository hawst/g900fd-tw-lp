.class Lcom/a/a/a/J;
.super Ljava/lang/Object;
.source "CacheData.java"


# instance fields
.field final synthetic a:Lcom/a/a/a/L;

.field final synthetic b:Lcom/a/a/a/y;

.field final synthetic c:Lcom/a/a/a/v;

.field final synthetic d:Lcom/a/a/a/t;


# direct methods
.method constructor <init>(Lcom/a/a/a/t;Lcom/a/a/a/L;Lcom/a/a/a/y;Lcom/a/a/a/v;)V
    .locals 0

    .prologue
    .line 317
    iput-object p1, p0, Lcom/a/a/a/J;->d:Lcom/a/a/a/t;

    iput-object p2, p0, Lcom/a/a/a/J;->a:Lcom/a/a/a/L;

    iput-object p3, p0, Lcom/a/a/a/J;->b:Lcom/a/a/a/y;

    iput-object p4, p0, Lcom/a/a/a/J;->c:Lcom/a/a/a/v;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 3

    .prologue
    .line 320
    const-string/jumbo v0, "CacheData"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "Request for key "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/a/a/a/J;->a:Lcom/a/a/a/L;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " failed"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 321
    iget-object v0, p0, Lcom/a/a/a/J;->d:Lcom/a/a/a/t;

    invoke-static {v0}, Lcom/a/a/a/t;->c(Lcom/a/a/a/t;)Ljava/util/HashSet;

    move-result-object v0

    iget-object v1, p0, Lcom/a/a/a/J;->a:Lcom/a/a/a/L;

    invoke-virtual {v1}, Lcom/a/a/a/L;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    .line 322
    iget-object v0, p0, Lcom/a/a/a/J;->d:Lcom/a/a/a/t;

    iget-object v1, p0, Lcom/a/a/a/J;->a:Lcom/a/a/a/L;

    invoke-virtual {v1}, Lcom/a/a/a/L;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/a/a/a/t;->a(Lcom/a/a/a/t;Ljava/lang/String;)V

    .line 323
    iget-object v0, p0, Lcom/a/a/a/J;->d:Lcom/a/a/a/t;

    iget-object v1, p0, Lcom/a/a/a/J;->b:Lcom/a/a/a/y;

    invoke-static {v0, v1}, Lcom/a/a/a/t;->a(Lcom/a/a/a/t;Lcom/a/a/a/y;)V

    .line 324
    return-void
.end method

.method public a(Lcom/a/a/a/H;)V
    .locals 1

    .prologue
    .line 328
    iget-object v0, p0, Lcom/a/a/a/J;->c:Lcom/a/a/a/v;

    invoke-static {v0, p1}, Lcom/a/a/a/v;->a(Lcom/a/a/a/v;Lcom/a/a/a/H;)V

    .line 331
    invoke-virtual {p1}, Lcom/a/a/a/H;->toString()Ljava/lang/String;

    .line 332
    iget-object v0, p0, Lcom/a/a/a/J;->d:Lcom/a/a/a/t;

    invoke-static {v0}, Lcom/a/a/a/t;->d(Lcom/a/a/a/t;)Lcom/a/a/a/w;

    iget-object v0, p0, Lcom/a/a/a/J;->a:Lcom/a/a/a/L;

    invoke-virtual {v0}, Lcom/a/a/a/L;->toString()Ljava/lang/String;

    .line 333
    return-void
.end method

.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 317
    check-cast p1, Lcom/a/a/a/H;

    invoke-virtual {p0, p1}, Lcom/a/a/a/J;->a(Lcom/a/a/a/H;)V

    return-void
.end method
