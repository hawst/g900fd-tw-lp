.class public final Lcom/a/a/a/l;
.super Ljava/lang/Object;
.source "AddressWidget.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemSelectedListener;


# static fields
.field private static final n:Ljava/util/Map;

.field private static final o:Ljava/util/Map;


# instance fields
.field final a:Landroid/os/Handler;

.field final b:Ljava/lang/Runnable;

.field private c:Landroid/content/Context;

.field private d:Landroid/view/ViewGroup;

.field private e:Lcom/a/a/a/t;

.field private final f:Ljava/util/EnumMap;

.field private g:Lcom/a/a/a/B;

.field private h:Lcom/a/a/a/F;

.field private i:Lcom/a/a/a/D;

.field private j:Landroid/app/ProgressDialog;

.field private k:Ljava/lang/String;

.field private l:Ljava/lang/String;

.field private m:Lcom/a/a/a/O;

.field private final p:Ljava/util/ArrayList;

.field private q:Lcom/a/a/a/s;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const v4, 0x7f0a0026

    const/16 v3, 0xf

    .line 96
    new-instance v0, Lcom/a/a/a/E;

    invoke-direct {v0}, Lcom/a/a/a/E;-><init>()V

    invoke-virtual {v0}, Lcom/a/a/a/E;->a()Lcom/a/a/a/D;

    .line 107
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0, v3}, Ljava/util/HashMap;-><init>(I)V

    .line 108
    const-string/jumbo v1, "area"

    const v2, 0x7f0a002c

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 109
    const-string/jumbo v1, "county"

    const v2, 0x7f0a002d

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 110
    const-string/jumbo v1, "department"

    const v2, 0x7f0a002e

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 111
    const-string/jumbo v1, "district"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 112
    const-string/jumbo v1, "do_si"

    const v2, 0x7f0a002f

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 113
    const-string/jumbo v1, "emirate"

    const v2, 0x7f0a0030

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 114
    const-string/jumbo v1, "island"

    const v2, 0x7f0a0031

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 115
    const-string/jumbo v1, "oblast"

    const v2, 0x7f0a0032

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 116
    const-string/jumbo v1, "parish"

    const v2, 0x7f0a0033

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 117
    const-string/jumbo v1, "prefecture"

    const v2, 0x7f0a0034

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 118
    const-string/jumbo v1, "province"

    const v2, 0x7f0a0035

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 119
    const-string/jumbo v1, "state"

    const v2, 0x7f0a0036

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 120
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    sput-object v0, Lcom/a/a/a/l;->n:Ljava/util/Map;

    .line 122
    new-instance v0, Ljava/util/HashMap;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(I)V

    .line 123
    const-string/jumbo v1, "city"

    const v2, 0x7f0a0025

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 124
    const-string/jumbo v1, "district"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 125
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    sput-object v0, Lcom/a/a/a/l;->o:Ljava/util/Map;

    .line 127
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0, v3}, Ljava/util/HashMap;-><init>(I)V

    .line 128
    const-string/jumbo v1, "area"

    const v2, 0x7f0a003d

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 129
    const-string/jumbo v1, "county"

    const v2, 0x7f0a003e

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 130
    const-string/jumbo v1, "department"

    const v2, 0x7f0a003f

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 131
    const-string/jumbo v1, "district"

    const v2, 0x7f0a003a

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 132
    const-string/jumbo v1, "do_si"

    const v2, 0x7f0a0040

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 133
    const-string/jumbo v1, "emirate"

    const v2, 0x7f0a0041

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 134
    const-string/jumbo v1, "island"

    const v2, 0x7f0a0042

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 135
    const-string/jumbo v1, "oblast"

    const v2, 0x7f0a0043

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 136
    const-string/jumbo v1, "parish"

    const v2, 0x7f0a0044

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 137
    const-string/jumbo v1, "prefecture"

    const v2, 0x7f0a0045

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 138
    const-string/jumbo v1, "province"

    const v2, 0x7f0a0046

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 139
    const-string/jumbo v1, "state"

    const v2, 0x7f0a0047

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 140
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    .line 141
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/view/ViewGroup;Lcom/a/a/a/D;Lcom/a/a/a/w;Lcom/a/a/a/a;Lcom/a/a/a/s;)V
    .locals 4

    .prologue
    .line 583
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 63
    new-instance v0, Ljava/util/EnumMap;

    const-class v1, Lcom/a/a/a/e;

    invoke-direct {v0, v1}, Ljava/util/EnumMap;-><init>(Ljava/lang/Class;)V

    iput-object v0, p0, Lcom/a/a/a/l;->f:Ljava/util/EnumMap;

    .line 144
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/a/a/a/l;->a:Landroid/os/Handler;

    .line 146
    new-instance v0, Lcom/a/a/a/m;

    invoke-direct {v0, p0}, Lcom/a/a/a/m;-><init>(Lcom/a/a/a/l;)V

    iput-object v0, p0, Lcom/a/a/a/l;->b:Ljava/lang/Runnable;

    .line 220
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/a/a/a/l;->p:Ljava/util/ArrayList;

    .line 584
    iput-object p6, p0, Lcom/a/a/a/l;->q:Lcom/a/a/a/s;

    .line 585
    invoke-virtual {p5}, Lcom/a/a/a/a;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/a/a/a/l;->k:Ljava/lang/String;

    .line 587
    iget-object v0, p0, Lcom/a/a/a/l;->k:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/a/a/a/l;->k:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v1, 0x2

    if-eq v0, v1, :cond_1

    .line 588
    :cond_0
    const-string/jumbo v0, "US"

    iput-object v0, p0, Lcom/a/a/a/l;->k:Ljava/lang/String;

    .line 590
    :cond_1
    iput-object p1, p0, Lcom/a/a/a/l;->c:Landroid/content/Context;

    iput-object p2, p0, Lcom/a/a/a/l;->d:Landroid/view/ViewGroup;

    iput-object p3, p0, Lcom/a/a/a/l;->i:Lcom/a/a/a/D;

    new-instance v0, Lcom/a/a/a/t;

    invoke-direct {v0, p4}, Lcom/a/a/a/t;-><init>(Lcom/a/a/a/w;)V

    iput-object v0, p0, Lcom/a/a/a/l;->e:Lcom/a/a/a/t;

    const-string/jumbo v0, "layout_inflater"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    new-instance v0, Lcom/a/a/a/B;

    new-instance v1, Lcom/a/a/a/x;

    iget-object v2, p0, Lcom/a/a/a/l;->e:Lcom/a/a/a/t;

    invoke-direct {v1, v2}, Lcom/a/a/a/x;-><init>(Lcom/a/a/a/t;)V

    iget-object v2, p0, Lcom/a/a/a/l;->l:Ljava/lang/String;

    iget-object v3, p0, Lcom/a/a/a/l;->k:Ljava/lang/String;

    invoke-direct {v0, v1, v2, v3}, Lcom/a/a/a/B;-><init>(Lcom/a/a/a/x;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/a/a/a/l;->g:Lcom/a/a/a/B;

    new-instance v0, Lcom/a/a/a/F;

    iget-object v1, p0, Lcom/a/a/a/l;->i:Lcom/a/a/a/D;

    invoke-direct {v0, v1}, Lcom/a/a/a/F;-><init>(Lcom/a/a/a/D;)V

    iput-object v0, p0, Lcom/a/a/a/l;->h:Lcom/a/a/a/F;

    new-instance v0, Lcom/a/a/a/U;

    new-instance v1, Lcom/a/a/a/A;

    new-instance v2, Lcom/a/a/a/x;

    iget-object v3, p0, Lcom/a/a/a/l;->e:Lcom/a/a/a/t;

    invoke-direct {v2, v3}, Lcom/a/a/a/x;-><init>(Lcom/a/a/a/t;)V

    invoke-direct {v1, v2}, Lcom/a/a/a/A;-><init>(Lcom/a/a/a/z;)V

    invoke-direct {v0, v1}, Lcom/a/a/a/U;-><init>(Lcom/a/a/a/A;)V

    sget-object v0, Lcom/a/a/a/e;->k:Lcom/a/a/a/e;

    invoke-virtual {p3, v0}, Lcom/a/a/a/D;->a(Lcom/a/a/a/e;)Z

    move-result v0

    if-nez v0, :cond_2

    invoke-direct {p0}, Lcom/a/a/a/l;->c()V

    iget-object v1, p0, Lcom/a/a/a/l;->d:Landroid/view/ViewGroup;

    iget-object v0, p0, Lcom/a/a/a/l;->f:Ljava/util/EnumMap;

    sget-object v2, Lcom/a/a/a/e;->k:Lcom/a/a/a/e;

    invoke-virtual {v0, v2}, Ljava/util/EnumMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/a/a/h;

    iget-object v2, p0, Lcom/a/a/a/l;->k:Ljava/lang/String;

    invoke-static {v2}, Lcom/a/a/a/l;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/a/a/a/e;->k:Lcom/a/a/a/e;

    invoke-virtual {p3, v3}, Lcom/a/a/a/D;->b(Lcom/a/a/a/e;)Z

    move-result v3

    invoke-direct {p0, v1, v0, v2, v3}, Lcom/a/a/a/l;->a(Landroid/view/ViewGroup;Lcom/a/a/a/h;Ljava/lang/String;Z)V

    .line 591
    :cond_2
    invoke-direct {p0}, Lcom/a/a/a/l;->f()V

    invoke-direct {p0}, Lcom/a/a/a/l;->d()V

    invoke-direct {p0}, Lcom/a/a/a/l;->b()V

    invoke-direct {p0}, Lcom/a/a/a/l;->e()V

    invoke-direct {p0, p5}, Lcom/a/a/a/l;->a(Lcom/a/a/a/a;)V

    .line 592
    return-void
.end method

.method static synthetic a(Lcom/a/a/a/l;Landroid/app/ProgressDialog;)Landroid/app/ProgressDialog;
    .locals 0

    .prologue
    .line 53
    iput-object p1, p0, Lcom/a/a/a/l;->j:Landroid/app/ProgressDialog;

    return-object p1
.end method

.method private a(Landroid/view/View;)Lcom/a/a/a/p;
    .locals 3

    .prologue
    .line 392
    iget-object v0, p0, Lcom/a/a/a/l;->p:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/a/a/p;

    .line 393
    invoke-static {v0}, Lcom/a/a/a/p;->a(Lcom/a/a/a/p;)Landroid/widget/Spinner;

    move-result-object v2

    if-ne v2, p1, :cond_0

    .line 397
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 388
    new-instance v0, Ljava/util/Locale;

    const-string/jumbo v1, ""

    invoke-direct {v0, v1, p0}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/Locale;->getDisplayCountry(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private a(Landroid/view/ViewGroup;Lcom/a/a/a/h;Ljava/lang/String;Z)V
    .locals 5

    .prologue
    .line 232
    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v0, -0x1

    const/4 v2, -0x2

    invoke-direct {v1, v0, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 234
    invoke-virtual {p2}, Lcom/a/a/a/h;->b()Ljava/lang/String;

    move-result-object v0

    .line 235
    invoke-virtual {p2}, Lcom/a/a/a/h;->e()Lcom/a/a/a/e;

    move-result-object v2

    invoke-virtual {v2}, Lcom/a/a/a/e;->a()Lcom/a/a/a/f;

    move-result-object v2

    .line 237
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-lez v3, :cond_0

    .line 238
    iget-object v3, p0, Lcom/a/a/a/l;->q:Lcom/a/a/a/s;

    invoke-virtual {v3, v0, v2}, Lcom/a/a/a/s;->createUiLabel(Ljava/lang/CharSequence;Lcom/a/a/a/f;)Landroid/widget/TextView;

    move-result-object v3

    .line 239
    invoke-virtual {p1, v3, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 241
    :cond_0
    invoke-virtual {p2}, Lcom/a/a/a/h;->c()Lcom/a/a/a/j;

    move-result-object v3

    sget-object v4, Lcom/a/a/a/j;->a:Lcom/a/a/a/j;

    invoke-virtual {v3, v4}, Lcom/a/a/a/j;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 242
    iget-object v0, p0, Lcom/a/a/a/l;->q:Lcom/a/a/a/s;

    invoke-virtual {v0, v2}, Lcom/a/a/a/s;->createUiTextField(Lcom/a/a/a/f;)Landroid/widget/EditText;

    move-result-object v2

    .line 243
    invoke-virtual {p2, v2}, Lcom/a/a/a/h;->a(Landroid/view/View;)V

    .line 244
    if-nez p4, :cond_2

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v2, v0}, Landroid/widget/EditText;->setEnabled(Z)V

    .line 245
    invoke-virtual {p1, v2, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 263
    :cond_1
    :goto_1
    return-void

    .line 244
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 246
    :cond_3
    invoke-virtual {p2}, Lcom/a/a/a/h;->c()Lcom/a/a/a/j;

    move-result-object v3

    sget-object v4, Lcom/a/a/a/j;->b:Lcom/a/a/a/j;

    invoke-virtual {v3, v4}, Lcom/a/a/a/j;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 247
    iget-object v3, p0, Lcom/a/a/a/l;->q:Lcom/a/a/a/s;

    invoke-virtual {v3, v2}, Lcom/a/a/a/s;->createUiPickerAdapter(Lcom/a/a/a/f;)Landroid/widget/ArrayAdapter;

    move-result-object v3

    .line 248
    iget-object v4, p0, Lcom/a/a/a/l;->q:Lcom/a/a/a/s;

    invoke-virtual {v4, v2}, Lcom/a/a/a/s;->createUiPickerSpinner(Lcom/a/a/a/f;)Landroid/widget/Spinner;

    move-result-object v2

    .line 250
    invoke-virtual {p2, v2}, Lcom/a/a/a/h;->a(Landroid/view/View;)V

    .line 251
    invoke-virtual {p1, v2, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 252
    invoke-virtual {v2, v3}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 253
    new-instance v1, Lcom/a/a/a/p;

    invoke-virtual {p2}, Lcom/a/a/a/h;->e()Lcom/a/a/a/e;

    move-result-object v3

    invoke-virtual {p2}, Lcom/a/a/a/h;->f()Lcom/a/a/a/e;

    move-result-object v4

    invoke-direct {v1, v2, v3, v4}, Lcom/a/a/a/p;-><init>(Landroid/widget/Spinner;Lcom/a/a/a/e;Lcom/a/a/a/e;)V

    .line 255
    invoke-virtual {p2}, Lcom/a/a/a/h;->d()Ljava/util/List;

    move-result-object v3

    invoke-virtual {v1, v3, p3}, Lcom/a/a/a/p;->a(Ljava/util/List;Ljava/lang/String;)V

    .line 257
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-lez v3, :cond_4

    .line 258
    invoke-virtual {v2, v0}, Landroid/widget/Spinner;->setPrompt(Ljava/lang/CharSequence;)V

    .line 260
    :cond_4
    invoke-virtual {v2, p0}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 261
    iget-object v0, p0, Lcom/a/a/a/l;->p:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method

.method private a(Lcom/a/a/a/a;)V
    .locals 4

    .prologue
    .line 603
    iget-object v0, p0, Lcom/a/a/a/l;->h:Lcom/a/a/a/F;

    iget-object v1, p0, Lcom/a/a/a/l;->m:Lcom/a/a/a/O;

    iget-object v2, p0, Lcom/a/a/a/l;->k:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/a/a/a/F;->a(Lcom/a/a/a/O;Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/a/a/e;

    .line 605
    invoke-virtual {p1, v0}, Lcom/a/a/a/a;->a(Lcom/a/a/a/e;)Ljava/lang/String;

    move-result-object v1

    .line 606
    if-nez v1, :cond_1

    .line 607
    const-string/jumbo v1, ""

    .line 609
    :cond_1
    iget-object v3, p0, Lcom/a/a/a/l;->f:Ljava/util/EnumMap;

    invoke-virtual {v3, v0}, Ljava/util/EnumMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/a/a/h;

    .line 610
    invoke-virtual {v0}, Lcom/a/a/a/h;->g()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    .line 611
    if-eqz v0, :cond_0

    .line 612
    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 615
    :cond_2
    return-void
.end method

.method static synthetic a(Lcom/a/a/a/l;)V
    .locals 2

    .prologue
    .line 53
    invoke-direct {p0}, Lcom/a/a/a/l;->d()V

    invoke-direct {p0}, Lcom/a/a/a/l;->b()V

    iget-object v0, p0, Lcom/a/a/a/l;->f:Ljava/util/EnumMap;

    sget-object v1, Lcom/a/a/a/e;->a:Lcom/a/a/a/e;

    invoke-virtual {v0, v1}, Ljava/util/EnumMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/a/a/h;

    sget-object v1, Lcom/a/a/a/e;->k:Lcom/a/a/a/e;

    invoke-direct {p0, v1}, Lcom/a/a/a/l;->b(Lcom/a/a/a/e;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/a/a/a/h;->a(Ljava/util/List;)V

    iget-object v0, p0, Lcom/a/a/a/l;->f:Ljava/util/EnumMap;

    sget-object v1, Lcom/a/a/a/e;->b:Lcom/a/a/a/e;

    invoke-virtual {v0, v1}, Ljava/util/EnumMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/a/a/h;

    sget-object v1, Lcom/a/a/a/e;->a:Lcom/a/a/a/e;

    invoke-direct {p0, v1}, Lcom/a/a/a/l;->b(Lcom/a/a/a/e;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/a/a/a/h;->a(Ljava/util/List;)V

    invoke-direct {p0}, Lcom/a/a/a/l;->e()V

    return-void
.end method

.method static synthetic a(Lcom/a/a/a/l;Lcom/a/a/a/e;)V
    .locals 4

    .prologue
    .line 53
    iget-object v0, p0, Lcom/a/a/a/l;->p:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/a/a/p;

    invoke-static {v0}, Lcom/a/a/a/p;->c(Lcom/a/a/a/p;)Lcom/a/a/a/e;

    move-result-object v2

    if-ne v2, p1, :cond_0

    invoke-static {v0}, Lcom/a/a/a/p;->c(Lcom/a/a/a/p;)Lcom/a/a/a/e;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/a/a/a/l;->b(Lcom/a/a/a/e;)Ljava/util/List;

    move-result-object v2

    const-string/jumbo v3, ""

    invoke-virtual {v0, v2, v3}, Lcom/a/a/a/p;->a(Ljava/util/List;Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    return-void
.end method

.method static synthetic b(Lcom/a/a/a/l;)Lcom/a/a/a/s;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/a/a/a/l;->q:Lcom/a/a/a/s;

    return-object v0
.end method

.method private b(Lcom/a/a/a/e;)Ljava/util/List;
    .locals 3

    .prologue
    .line 513
    invoke-virtual {p0}, Lcom/a/a/a/l;->a()Lcom/a/a/a/a;

    move-result-object v0

    .line 518
    iget-object v1, p0, Lcom/a/a/a/l;->g:Lcom/a/a/a/B;

    invoke-virtual {v0}, Lcom/a/a/a/a;->k()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/a/a/a/B;->c(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 519
    new-instance v1, Lcom/a/a/a/c;

    invoke-direct {v1, v0}, Lcom/a/a/a/c;-><init>(Lcom/a/a/a/a;)V

    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Lcom/a/a/a/c;->b(Ljava/lang/String;)Lcom/a/a/a/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/a/a/a/c;->a()Lcom/a/a/a/a;

    move-result-object v0

    .line 522
    :cond_0
    iget-object v1, p0, Lcom/a/a/a/l;->g:Lcom/a/a/a/B;

    invoke-static {v0}, Lcom/a/a/a/B;->a(Lcom/a/a/a/a;)Lcom/a/a/a/L;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/a/a/a/L;->a(Lcom/a/a/a/e;)Lcom/a/a/a/L;

    move-result-object v0

    .line 526
    if-nez v0, :cond_1

    .line 527
    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "Can\'t build key with parent field "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ". One of the ancestor fields might be empty"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 533
    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 537
    :goto_0
    return-object v0

    .line 535
    :cond_1
    iget-object v1, p0, Lcom/a/a/a/l;->g:Lcom/a/a/a/B;

    invoke-virtual {v1, v0}, Lcom/a/a/a/B;->a(Lcom/a/a/a/L;)Ljava/util/List;

    move-result-object v0

    goto :goto_0
.end method

.method private b()V
    .locals 4

    .prologue
    .line 269
    new-instance v0, Lcom/a/a/a/c;

    invoke-direct {v0}, Lcom/a/a/a/c;-><init>()V

    iget-object v1, p0, Lcom/a/a/a/l;->k:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/a/a/a/c;->a(Ljava/lang/String;)Lcom/a/a/a/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/a/a/a/c;->a()Lcom/a/a/a/a;

    move-result-object v0

    .line 270
    new-instance v1, Lcom/a/a/a/M;

    sget-object v2, Lcom/a/a/a/N;->a:Lcom/a/a/a/N;

    invoke-direct {v1, v2}, Lcom/a/a/a/M;-><init>(Lcom/a/a/a/N;)V

    invoke-virtual {v1, v0}, Lcom/a/a/a/M;->a(Lcom/a/a/a/a;)Lcom/a/a/a/M;

    move-result-object v0

    invoke-virtual {v0}, Lcom/a/a/a/M;->a()Lcom/a/a/a/L;

    move-result-object v0

    .line 271
    new-instance v1, Lcom/a/a/a/x;

    iget-object v2, p0, Lcom/a/a/a/l;->e:Lcom/a/a/a/t;

    invoke-direct {v1, v2}, Lcom/a/a/a/x;-><init>(Lcom/a/a/a/t;)V

    invoke-virtual {v0}, Lcom/a/a/a/L;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/a/a/a/x;->b(Ljava/lang/String;)Lcom/a/a/a/k;

    move-result-object v1

    .line 275
    new-instance v2, Lcom/a/a/a/h;

    sget-object v0, Lcom/a/a/a/e;->a:Lcom/a/a/a/e;

    invoke-direct {v2, v0}, Lcom/a/a/a/h;-><init>(Lcom/a/a/a/e;)V

    .line 276
    sget-object v0, Lcom/a/a/a/d;->h:Lcom/a/a/a/d;

    invoke-virtual {v1, v0}, Lcom/a/a/a/k;->b(Lcom/a/a/a/d;)Ljava/lang/String;

    move-result-object v0

    sget-object v3, Lcom/a/a/a/l;->n:Ljava/util/Map;

    invoke-interface {v3, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    if-nez v0, :cond_0

    const v0, 0x7f0a0035

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    :cond_0
    iget-object v3, p0, Lcom/a/a/a/l;->c:Landroid/content/Context;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v3, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/a/a/a/h;->a(Ljava/lang/String;)V

    .line 277
    iget-object v0, p0, Lcom/a/a/a/l;->f:Ljava/util/EnumMap;

    sget-object v3, Lcom/a/a/a/e;->a:Lcom/a/a/a/e;

    invoke-virtual {v0, v3, v2}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 280
    new-instance v2, Lcom/a/a/a/h;

    sget-object v0, Lcom/a/a/a/e;->b:Lcom/a/a/a/e;

    invoke-direct {v2, v0}, Lcom/a/a/a/h;-><init>(Lcom/a/a/a/e;)V

    .line 281
    sget-object v0, Lcom/a/a/a/d;->f:Lcom/a/a/a/d;

    invoke-virtual {v1, v0}, Lcom/a/a/a/k;->b(Lcom/a/a/a/d;)Ljava/lang/String;

    move-result-object v0

    sget-object v3, Lcom/a/a/a/l;->o:Ljava/util/Map;

    invoke-interface {v3, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    if-nez v0, :cond_1

    const v0, 0x7f0a0025

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    :cond_1
    iget-object v3, p0, Lcom/a/a/a/l;->c:Landroid/content/Context;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v3, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/a/a/a/h;->a(Ljava/lang/String;)V

    .line 282
    iget-object v0, p0, Lcom/a/a/a/l;->f:Ljava/util/EnumMap;

    sget-object v3, Lcom/a/a/a/e;->b:Lcom/a/a/a/e;

    invoke-virtual {v0, v3, v2}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 285
    new-instance v0, Lcom/a/a/a/h;

    sget-object v2, Lcom/a/a/a/e;->g:Lcom/a/a/a/e;

    invoke-direct {v0, v2}, Lcom/a/a/a/h;-><init>(Lcom/a/a/a/e;)V

    .line 286
    iget-object v2, p0, Lcom/a/a/a/l;->c:Landroid/content/Context;

    const v3, 0x7f0a0026

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/a/a/a/h;->a(Ljava/lang/String;)V

    .line 287
    iget-object v2, p0, Lcom/a/a/a/l;->f:Ljava/util/EnumMap;

    sget-object v3, Lcom/a/a/a/e;->g:Lcom/a/a/a/e;

    invoke-virtual {v2, v3, v0}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 290
    new-instance v0, Lcom/a/a/a/h;

    sget-object v2, Lcom/a/a/a/e;->e:Lcom/a/a/a/e;

    invoke-direct {v0, v2}, Lcom/a/a/a/h;-><init>(Lcom/a/a/a/e;)V

    .line 291
    iget-object v2, p0, Lcom/a/a/a/l;->c:Landroid/content/Context;

    const v3, 0x7f0a0029

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/a/a/a/h;->a(Ljava/lang/String;)V

    .line 292
    iget-object v2, p0, Lcom/a/a/a/l;->f:Ljava/util/EnumMap;

    sget-object v3, Lcom/a/a/a/e;->e:Lcom/a/a/a/e;

    invoke-virtual {v2, v3, v0}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 295
    new-instance v0, Lcom/a/a/a/h;

    sget-object v2, Lcom/a/a/a/e;->f:Lcom/a/a/a/e;

    invoke-direct {v0, v2}, Lcom/a/a/a/h;-><init>(Lcom/a/a/a/e;)V

    .line 296
    const-string/jumbo v2, ""

    invoke-virtual {v0, v2}, Lcom/a/a/a/h;->a(Ljava/lang/String;)V

    .line 297
    iget-object v2, p0, Lcom/a/a/a/l;->f:Ljava/util/EnumMap;

    sget-object v3, Lcom/a/a/a/e;->f:Lcom/a/a/a/e;

    invoke-virtual {v2, v3, v0}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 300
    new-instance v0, Lcom/a/a/a/h;

    sget-object v2, Lcom/a/a/a/e;->d:Lcom/a/a/a/e;

    invoke-direct {v0, v2}, Lcom/a/a/a/h;-><init>(Lcom/a/a/a/e;)V

    .line 301
    iget-object v2, p0, Lcom/a/a/a/l;->c:Landroid/content/Context;

    const v3, 0x7f0a0027

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/a/a/a/h;->a(Ljava/lang/String;)V

    .line 302
    iget-object v2, p0, Lcom/a/a/a/l;->f:Ljava/util/EnumMap;

    sget-object v3, Lcom/a/a/a/e;->d:Lcom/a/a/a/e;

    invoke-virtual {v2, v3, v0}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 305
    new-instance v0, Lcom/a/a/a/h;

    sget-object v2, Lcom/a/a/a/e;->c:Lcom/a/a/a/e;

    invoke-direct {v0, v2}, Lcom/a/a/a/h;-><init>(Lcom/a/a/a/e;)V

    .line 306
    iget-object v2, p0, Lcom/a/a/a/l;->c:Landroid/content/Context;

    const v3, 0x7f0a0028

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/a/a/a/h;->a(Ljava/lang/String;)V

    .line 307
    iget-object v2, p0, Lcom/a/a/a/l;->f:Ljava/util/EnumMap;

    sget-object v3, Lcom/a/a/a/e;->c:Lcom/a/a/a/e;

    invoke-virtual {v2, v3, v0}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 310
    new-instance v2, Lcom/a/a/a/h;

    sget-object v0, Lcom/a/a/a/e;->h:Lcom/a/a/a/e;

    invoke-direct {v2, v0}, Lcom/a/a/a/h;-><init>(Lcom/a/a/a/e;)V

    .line 311
    sget-object v0, Lcom/a/a/a/d;->l:Lcom/a/a/a/d;

    invoke-virtual {v1, v0}, Lcom/a/a/a/k;->b(Lcom/a/a/a/d;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_2

    sget-object v0, Lcom/a/a/a/r;->b:Lcom/a/a/a/r;

    iget-object v0, p0, Lcom/a/a/a/l;->c:Landroid/content/Context;

    const v1, 0x7f0a002a

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v2, v0}, Lcom/a/a/a/h;->a(Ljava/lang/String;)V

    .line 312
    iget-object v0, p0, Lcom/a/a/a/l;->f:Ljava/util/EnumMap;

    sget-object v1, Lcom/a/a/a/e;->h:Lcom/a/a/a/e;

    invoke-virtual {v0, v1, v2}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 315
    new-instance v0, Lcom/a/a/a/h;

    sget-object v1, Lcom/a/a/a/e;->i:Lcom/a/a/a/e;

    invoke-direct {v0, v1}, Lcom/a/a/a/h;-><init>(Lcom/a/a/a/e;)V

    .line 316
    const-string/jumbo v1, "CEDEX"

    invoke-virtual {v0, v1}, Lcom/a/a/a/h;->a(Ljava/lang/String;)V

    .line 317
    iget-object v1, p0, Lcom/a/a/a/l;->f:Ljava/util/EnumMap;

    sget-object v2, Lcom/a/a/a/e;->i:Lcom/a/a/a/e;

    invoke-virtual {v1, v2, v0}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 318
    return-void

    .line 311
    :cond_2
    sget-object v0, Lcom/a/a/a/r;->a:Lcom/a/a/a/r;

    iget-object v0, p0, Lcom/a/a/a/l;->c:Landroid/content/Context;

    const v1, 0x7f0a002b

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method static synthetic c(Lcom/a/a/a/l;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/a/a/a/l;->c:Landroid/content/Context;

    return-object v0
.end method

.method private c()V
    .locals 6

    .prologue
    .line 369
    new-instance v1, Lcom/a/a/a/h;

    sget-object v0, Lcom/a/a/a/e;->k:Lcom/a/a/a/e;

    invoke-direct {v1, v0}, Lcom/a/a/a/h;-><init>(Lcom/a/a/a/e;)V

    .line 370
    iget-object v0, p0, Lcom/a/a/a/l;->c:Landroid/content/Context;

    const v2, 0x7f0a0024

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/a/a/a/h;->a(Ljava/lang/String;)V

    .line 371
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 372
    iget-object v0, p0, Lcom/a/a/a/l;->g:Lcom/a/a/a/B;

    new-instance v3, Lcom/a/a/a/M;

    sget-object v4, Lcom/a/a/a/N;->a:Lcom/a/a/a/N;

    invoke-direct {v3, v4}, Lcom/a/a/a/M;-><init>(Lcom/a/a/a/N;)V

    invoke-virtual {v3}, Lcom/a/a/a/M;->a()Lcom/a/a/a/L;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/a/a/a/B;->a(Lcom/a/a/a/L;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/a/a/Q;

    .line 374
    invoke-virtual {v0}, Lcom/a/a/a/Q;->a()Ljava/lang/String;

    move-result-object v0

    .line 376
    const-string/jumbo v4, "ZZ"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 377
    invoke-static {v0}, Lcom/a/a/a/l;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 378
    new-instance v5, Lcom/a/a/a/R;

    invoke-direct {v5}, Lcom/a/a/a/R;-><init>()V

    invoke-virtual {v5, v0}, Lcom/a/a/a/R;->a(Ljava/lang/String;)Lcom/a/a/a/R;

    move-result-object v0

    invoke-virtual {v0, v4}, Lcom/a/a/a/R;->b(Ljava/lang/String;)Lcom/a/a/a/R;

    move-result-object v0

    invoke-virtual {v0}, Lcom/a/a/a/R;->a()Lcom/a/a/a/Q;

    move-result-object v0

    .line 380
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 383
    :cond_1
    invoke-virtual {v1, v2}, Lcom/a/a/a/h;->a(Ljava/util/List;)V

    .line 384
    iget-object v0, p0, Lcom/a/a/a/l;->f:Ljava/util/EnumMap;

    sget-object v2, Lcom/a/a/a/e;->k:Lcom/a/a/a/e;

    invoke-virtual {v0, v2, v1}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 385
    return-void
.end method

.method static synthetic d(Lcom/a/a/a/l;)Landroid/app/ProgressDialog;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/a/a/a/l;->j:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method private d()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    .line 408
    iget-object v0, p0, Lcom/a/a/a/l;->d:Landroid/view/ViewGroup;

    if-nez v0, :cond_1

    .line 420
    :cond_0
    :goto_0
    return-void

    .line 411
    :cond_1
    iget-object v0, p0, Lcom/a/a/a/l;->d:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    .line 412
    iget-object v1, p0, Lcom/a/a/a/l;->i:Lcom/a/a/a/D;

    sget-object v2, Lcom/a/a/a/e;->k:Lcom/a/a/a/e;

    invoke-virtual {v1, v2}, Lcom/a/a/a/D;->a(Lcom/a/a/a/e;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 413
    if-lez v0, :cond_0

    .line 414
    iget-object v0, p0, Lcom/a/a/a/l;->d:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    goto :goto_0

    .line 416
    :cond_2
    if-le v0, v3, :cond_0

    .line 418
    iget-object v0, p0, Lcom/a/a/a/l;->d:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/a/a/a/l;->d:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v1

    add-int/lit8 v1, v1, -0x2

    invoke-virtual {v0, v3, v1}, Landroid/view/ViewGroup;->removeViews(II)V

    goto :goto_0
.end method

.method private e()V
    .locals 6

    .prologue
    .line 423
    iget-object v0, p0, Lcom/a/a/a/l;->h:Lcom/a/a/a/F;

    iget-object v1, p0, Lcom/a/a/a/l;->m:Lcom/a/a/a/O;

    iget-object v2, p0, Lcom/a/a/a/l;->k:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/a/a/a/F;->a(Lcom/a/a/a/O;Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/a/a/e;

    .line 425
    iget-object v1, p0, Lcom/a/a/a/l;->i:Lcom/a/a/a/D;

    invoke-virtual {v1, v0}, Lcom/a/a/a/D;->a(Lcom/a/a/a/e;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 426
    iget-object v3, p0, Lcom/a/a/a/l;->d:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/a/a/a/l;->f:Ljava/util/EnumMap;

    invoke-virtual {v1, v0}, Ljava/util/EnumMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/a/a/a/h;

    const-string/jumbo v4, ""

    iget-object v5, p0, Lcom/a/a/a/l;->i:Lcom/a/a/a/D;

    invoke-virtual {v5, v0}, Lcom/a/a/a/D;->b(Lcom/a/a/a/e;)Z

    move-result v0

    invoke-direct {p0, v3, v1, v4, v0}, Lcom/a/a/a/l;->a(Landroid/view/ViewGroup;Lcom/a/a/a/h;Ljava/lang/String;Z)V

    goto :goto_0

    .line 429
    :cond_1
    return-void
.end method

.method private f()V
    .locals 2

    .prologue
    .line 505
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    iget-object v1, p0, Lcom/a/a/a/l;->k:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/a/a/a/X;->a(Ljava/util/Locale;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/a/a/a/l;->l:Ljava/lang/String;

    .line 506
    iget-object v0, p0, Lcom/a/a/a/l;->g:Lcom/a/a/a/B;

    iget-object v1, p0, Lcom/a/a/a/l;->l:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/a/a/a/B;->a(Ljava/lang/String;)V

    .line 507
    iget-object v0, p0, Lcom/a/a/a/l;->l:Ljava/lang/String;

    invoke-static {v0}, Lcom/a/a/a/X;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/a/a/a/O;->a:Lcom/a/a/a/O;

    :goto_0
    iput-object v0, p0, Lcom/a/a/a/l;->m:Lcom/a/a/a/O;

    .line 510
    return-void

    .line 507
    :cond_0
    sget-object v0, Lcom/a/a/a/O;->b:Lcom/a/a/a/O;

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/a/a/a/e;)Landroid/view/View;
    .locals 1

    .prologue
    .line 762
    iget-object v0, p0, Lcom/a/a/a/l;->f:Ljava/util/EnumMap;

    invoke-virtual {v0, p1}, Ljava/util/EnumMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/a/a/h;

    .line 763
    if-nez v0, :cond_0

    .line 764
    const/4 v0, 0x0

    .line 766
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {v0}, Lcom/a/a/a/h;->g()Landroid/view/View;

    move-result-object v0

    goto :goto_0
.end method

.method public final a()Lcom/a/a/a/a;
    .locals 6

    .prologue
    .line 651
    new-instance v3, Lcom/a/a/a/c;

    invoke-direct {v3}, Lcom/a/a/a/c;-><init>()V

    .line 652
    iget-object v0, p0, Lcom/a/a/a/l;->k:Ljava/lang/String;

    invoke-virtual {v3, v0}, Lcom/a/a/a/c;->a(Ljava/lang/String;)Lcom/a/a/a/c;

    .line 653
    iget-object v0, p0, Lcom/a/a/a/l;->h:Lcom/a/a/a/F;

    iget-object v1, p0, Lcom/a/a/a/l;->m:Lcom/a/a/a/O;

    iget-object v2, p0, Lcom/a/a/a/l;->k:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/a/a/a/F;->a(Lcom/a/a/a/O;Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/a/a/e;

    .line 655
    iget-object v1, p0, Lcom/a/a/a/l;->f:Ljava/util/EnumMap;

    invoke-virtual {v1, v0}, Ljava/util/EnumMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/a/a/a/h;

    .line 656
    if-eqz v1, :cond_0

    .line 657
    invoke-virtual {v1}, Lcom/a/a/a/h;->a()Ljava/lang/String;

    move-result-object v2

    .line 658
    invoke-virtual {v1}, Lcom/a/a/a/h;->c()Lcom/a/a/a/j;

    move-result-object v1

    sget-object v5, Lcom/a/a/a/j;->b:Lcom/a/a/a/j;

    if-ne v1, v5, :cond_2

    .line 660
    invoke-virtual {p0, v0}, Lcom/a/a/a/l;->a(Lcom/a/a/a/e;)Landroid/view/View;

    move-result-object v1

    .line 661
    invoke-direct {p0, v1}, Lcom/a/a/a/l;->a(Landroid/view/View;)Lcom/a/a/a/p;

    move-result-object v1

    .line 662
    if-eqz v1, :cond_2

    .line 663
    invoke-virtual {v1, v2}, Lcom/a/a/a/p;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 666
    :goto_1
    invoke-virtual {v3, v0, v1}, Lcom/a/a/a/c;->a(Lcom/a/a/a/e;Ljava/lang/String;)Lcom/a/a/a/c;

    goto :goto_0

    .line 669
    :cond_1
    iget-object v0, p0, Lcom/a/a/a/l;->l:Ljava/lang/String;

    invoke-virtual {v3, v0}, Lcom/a/a/a/c;->b(Ljava/lang/String;)Lcom/a/a/a/c;

    .line 670
    invoke-virtual {v3}, Lcom/a/a/a/c;->a()Lcom/a/a/a/a;

    move-result-object v0

    return-object v0

    :cond_2
    move-object v1, v2

    goto :goto_1
.end method

.method public final onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 4

    .prologue
    .line 775
    invoke-direct {p0, p1}, Lcom/a/a/a/l;->a(Landroid/view/View;)Lcom/a/a/a/p;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-static {v0}, Lcom/a/a/a/p;->b(Lcom/a/a/a/p;)Lcom/a/a/a/e;

    move-result-object v1

    sget-object v2, Lcom/a/a/a/e;->k:Lcom/a/a/a/e;

    if-eq v1, v2, :cond_0

    sget-object v2, Lcom/a/a/a/e;->a:Lcom/a/a/a/e;

    if-eq v1, v2, :cond_0

    sget-object v2, Lcom/a/a/a/e;->b:Lcom/a/a/a/e;

    if-ne v1, v2, :cond_1

    :cond_0
    invoke-virtual {v0, p3}, Lcom/a/a/a/p;->a(I)Ljava/lang/String;

    move-result-object v0

    sget-object v2, Lcom/a/a/a/e;->k:Lcom/a/a/a/e;

    if-ne v1, v2, :cond_2

    iget-object v1, p0, Lcom/a/a/a/l;->k:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    iput-object v0, p0, Lcom/a/a/a/l;->k:Ljava/lang/String;

    iget-object v0, p0, Lcom/a/a/a/l;->g:Lcom/a/a/a/B;

    iget-object v1, p0, Lcom/a/a/a/l;->k:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/a/a/a/B;->b(Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/a/a/a/l;->f()V

    new-instance v0, Lcom/a/a/a/c;

    invoke-direct {v0}, Lcom/a/a/a/c;-><init>()V

    iget-object v1, p0, Lcom/a/a/a/l;->k:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/a/a/a/c;->a(Ljava/lang/String;)Lcom/a/a/a/c;

    move-result-object v0

    iget-object v1, p0, Lcom/a/a/a/l;->l:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/a/a/a/c;->b(Ljava/lang/String;)Lcom/a/a/a/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/a/a/a/c;->a()Lcom/a/a/a/a;

    move-result-object v0

    iget-object v1, p0, Lcom/a/a/a/l;->g:Lcom/a/a/a/B;

    new-instance v2, Lcom/a/a/a/o;

    invoke-direct {v2, p0}, Lcom/a/a/a/o;-><init>(Lcom/a/a/a/l;)V

    invoke-virtual {v1, v0, v2}, Lcom/a/a/a/B;->a(Lcom/a/a/a/a;Lcom/a/a/a/y;)V

    .line 776
    :cond_1
    :goto_0
    return-void

    .line 775
    :cond_2
    iget-object v0, p0, Lcom/a/a/a/l;->g:Lcom/a/a/a/B;

    invoke-virtual {p0}, Lcom/a/a/a/l;->a()Lcom/a/a/a/a;

    move-result-object v2

    new-instance v3, Lcom/a/a/a/n;

    invoke-direct {v3, p0, v1}, Lcom/a/a/a/n;-><init>(Lcom/a/a/a/l;Lcom/a/a/a/e;)V

    invoke-virtual {v0, v2, v3}, Lcom/a/a/a/B;->a(Lcom/a/a/a/a;Lcom/a/a/a/y;)V

    goto :goto_0
.end method

.method public final onNothingSelected(Landroid/widget/AdapterView;)V
    .locals 0

    .prologue
    .line 771
    return-void
.end method
