.class public final Lcom/a/a/a/x;
.super Ljava/lang/Object;
.source "ClientData.java"

# interfaces
.implements Lcom/a/a/a/z;


# instance fields
.field private final a:Ljava/util/Map;

.field private b:Lcom/a/a/a/t;


# direct methods
.method public constructor <init>(Lcom/a/a/a/t;)V
    .locals 1

    .prologue
    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/a/a/a/x;->a:Ljava/util/Map;

    .line 49
    iput-object p1, p0, Lcom/a/a/a/x;->b:Lcom/a/a/a/t;

    .line 50
    invoke-direct {p0}, Lcom/a/a/a/x;->a()V

    .line 51
    return-void
.end method

.method private static a(Lcom/a/a/a/H;)Lcom/a/a/a/k;
    .locals 5

    .prologue
    .line 111
    new-instance v1, Ljava/util/EnumMap;

    const-class v0, Lcom/a/a/a/d;

    invoke-direct {v1, v0}, Ljava/util/EnumMap;-><init>(Ljava/lang/Class;)V

    .line 114
    invoke-virtual {p0}, Lcom/a/a/a/H;->b()Lorg/json/JSONArray;

    move-result-object v2

    .line 115
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v2}, Lorg/json/JSONArray;->length()I

    move-result v3

    if-ge v0, v3, :cond_1

    .line 117
    :try_start_0
    invoke-virtual {v2, v0}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/a/a/a/d;->a(Ljava/lang/String;)Lcom/a/a/a/d;

    move-result-object v3

    .line 119
    if-nez v3, :cond_0

    .line 115
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 125
    :cond_0
    invoke-virtual {v3}, Lcom/a/a/a/d;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/a/a/a/H;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 126
    invoke-interface {v1, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 127
    :catch_0
    move-exception v3

    goto :goto_1

    .line 133
    :cond_1
    new-instance v0, Lcom/a/a/a/k;

    invoke-direct {v0, v1}, Lcom/a/a/a/k;-><init>(Ljava/util/Map;)V

    return-object v0
.end method

.method private a()V
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 148
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 150
    invoke-static {}, Lcom/a/a/a/S;->a()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 151
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v5, "~"

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 152
    invoke-static {}, Lcom/a/a/a/S;->a()Ljava/util/Map;

    move-result-object v1

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 155
    :try_start_0
    invoke-static {v1}, Lcom/a/a/a/H;->a(Ljava/lang/String;)Lcom/a/a/a/H;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 160
    :goto_1
    new-instance v5, Lcom/a/a/a/c;

    invoke-direct {v5}, Lcom/a/a/a/c;-><init>()V

    invoke-virtual {v5, v0}, Lcom/a/a/a/c;->a(Ljava/lang/String;)Lcom/a/a/a/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/a/a/a/c;->a()Lcom/a/a/a/a;

    move-result-object v0

    .line 161
    new-instance v5, Lcom/a/a/a/M;

    sget-object v6, Lcom/a/a/a/N;->a:Lcom/a/a/a/N;

    invoke-direct {v5, v6}, Lcom/a/a/a/M;-><init>(Lcom/a/a/a/N;)V

    invoke-virtual {v5, v0}, Lcom/a/a/a/M;->a(Lcom/a/a/a/a;)Lcom/a/a/a/M;

    move-result-object v0

    invoke-virtual {v0}, Lcom/a/a/a/M;->a()Lcom/a/a/a/L;

    move-result-object v0

    .line 162
    iget-object v5, p0, Lcom/a/a/a/x;->a:Ljava/util/Map;

    invoke-virtual {v0}, Lcom/a/a/a/L;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v5, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :catch_0
    move-exception v1

    move-object v1, v2

    goto :goto_1

    .line 164
    :cond_0
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 170
    new-instance v0, Ljava/lang/StringBuilder;

    const-string/jumbo v1, "{\"id\":\"data\",\""

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v1, Lcom/a/a/a/d;->a:Lcom/a/a/a/d;

    invoke-virtual {v1}, Lcom/a/a/a/d;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "\": \""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "\"}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 175
    :try_start_1
    invoke-static {v0}, Lcom/a/a/a/H;->a(Ljava/lang/String;)Lcom/a/a/a/H;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v2

    .line 179
    :goto_2
    iget-object v0, p0, Lcom/a/a/a/x;->a:Ljava/util/Map;

    const-string/jumbo v1, "data"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 180
    return-void

    :catch_1
    move-exception v0

    goto :goto_2
.end method

.method private static c(Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 102
    const-string/jumbo v0, "Cannot use null as a key"

    invoke-static {p0, v0}, Lcom/a/a/a/X;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 103
    const-string/jumbo v0, "/"

    invoke-virtual {p0, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    array-length v0, v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Lcom/a/a/a/k;
    .locals 4

    .prologue
    .line 55
    iget-object v0, p0, Lcom/a/a/a/x;->b:Lcom/a/a/a/t;

    invoke-virtual {v0, p1}, Lcom/a/a/a/t;->a(Ljava/lang/String;)Lcom/a/a/a/H;

    move-result-object v0

    .line 56
    if-nez v0, :cond_1

    .line 57
    iget-object v0, p0, Lcom/a/a/a/x;->b:Lcom/a/a/a/t;

    invoke-virtual {v0, p1}, Lcom/a/a/a/t;->a(Ljava/lang/String;)Lcom/a/a/a/H;

    move-result-object v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/a/a/a/x;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/a/a/H;

    new-instance v1, Lcom/a/a/a/P;

    invoke-direct {v1, p0}, Lcom/a/a/a/P;-><init>(Ljava/lang/Object;)V

    invoke-static {p1}, Lcom/a/a/a/L;->a(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    new-instance v2, Lcom/a/a/a/M;

    invoke-direct {v2, p1}, Lcom/a/a/a/M;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Lcom/a/a/a/M;->a()Lcom/a/a/a/L;

    move-result-object v2

    iget-object v3, p0, Lcom/a/a/a/x;->b:Lcom/a/a/a/t;

    invoke-virtual {v3, v2, v0, v1}, Lcom/a/a/a/t;->a(Lcom/a/a/a/L;Lorg/json/JSONObject;Lcom/a/a/a/y;)V

    :try_start_0
    invoke-virtual {v1}, Lcom/a/a/a/P;->c()V

    iget-object v0, p0, Lcom/a/a/a/x;->b:Lcom/a/a/a/t;

    invoke-virtual {v0, p1}, Lcom/a/a/a/t;->a(Ljava/lang/String;)Lcom/a/a/a/H;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-static {p1}, Lcom/a/a/a/x;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "ClientData"

    const-string/jumbo v1, "Server failure: looking up key in region data constants."

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/a/a/a/x;->b:Lcom/a/a/a/t;

    invoke-virtual {v0, v2}, Lcom/a/a/a/t;->a(Lcom/a/a/a/L;)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 58
    :cond_0
    iget-object v0, p0, Lcom/a/a/a/x;->b:Lcom/a/a/a/t;

    invoke-virtual {v0, p1}, Lcom/a/a/a/t;->a(Ljava/lang/String;)Lcom/a/a/a/H;

    move-result-object v0

    .line 60
    :cond_1
    if-eqz v0, :cond_2

    const-string/jumbo v1, "data"

    invoke-virtual {p1, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 61
    invoke-static {v0}, Lcom/a/a/a/x;->a(Lcom/a/a/a/H;)Lcom/a/a/a/k;

    move-result-object v0

    .line 63
    :goto_0
    return-object v0

    .line 57
    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 63
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Lcom/a/a/a/L;Lcom/a/a/a/y;)V
    .locals 2

    .prologue
    .line 214
    const-string/jumbo v0, "Null lookup key not allowed"

    invoke-static {p1, v0}, Lcom/a/a/a/X;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 215
    iget-object v0, p0, Lcom/a/a/a/x;->a:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/a/a/a/L;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/a/a/H;

    .line 216
    iget-object v1, p0, Lcom/a/a/a/x;->b:Lcom/a/a/a/t;

    invoke-virtual {v1, p1, v0, p2}, Lcom/a/a/a/t;->a(Lcom/a/a/a/L;Lorg/json/JSONObject;Lcom/a/a/a/y;)V

    .line 217
    return-void
.end method

.method public final b(Ljava/lang/String;)Lcom/a/a/a/k;
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 69
    const-string/jumbo v0, "/"

    invoke-virtual {p1, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    array-length v0, v0

    if-ne v0, v3, :cond_2

    .line 70
    iget-object v0, p0, Lcom/a/a/a/x;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/a/a/H;

    .line 71
    if-eqz v0, :cond_0

    const-string/jumbo v1, "data"

    invoke-virtual {p1, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 72
    :cond_0
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "key "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " does not have bootstrap data"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 74
    :cond_1
    invoke-static {v0}, Lcom/a/a/a/x;->a(Lcom/a/a/a/H;)Lcom/a/a/a/k;

    move-result-object v0

    .line 82
    :goto_0
    return-object v0

    .line 77
    :cond_2
    const-string/jumbo v0, "/"

    invoke-virtual {p1, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    array-length v0, v0

    if-gt v0, v3, :cond_3

    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "Cannot get country key with key \'"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    invoke-static {p1}, Lcom/a/a/a/x;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 78
    :goto_1
    iget-object v0, p0, Lcom/a/a/a/x;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/a/a/H;

    .line 79
    if-eqz v0, :cond_4

    const-string/jumbo v1, "data"

    invoke-virtual {p1, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_6

    .line 80
    :cond_4
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "key "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " does not have bootstrap data"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 77
    :cond_5
    const-string/jumbo v0, "/"

    invoke-virtual {p1, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v2, 0x0

    aget-object v2, v0, v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    aget-object v0, v0, v3

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    goto :goto_1

    .line 82
    :cond_6
    invoke-static {v0}, Lcom/a/a/a/x;->a(Lcom/a/a/a/H;)Lcom/a/a/a/k;

    move-result-object v0

    goto/16 :goto_0
.end method
