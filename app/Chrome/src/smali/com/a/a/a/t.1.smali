.class public final Lcom/a/a/a/t;
.super Ljava/lang/Object;
.source "CacheData.java"


# instance fields
.field private a:Ljava/lang/String;

.field private final b:Lcom/a/a/a/H;

.field private final c:Lcom/a/a/a/w;

.field private final d:Ljava/util/HashSet;

.field private final e:Ljava/util/HashSet;

.field private final f:Ljava/util/HashMap;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 86
    new-instance v0, Lcom/a/a/a/w;

    invoke-direct {v0}, Lcom/a/a/a/w;-><init>()V

    invoke-direct {p0, v0}, Lcom/a/a/a/t;-><init>(Lcom/a/a/a/w;)V

    .line 87
    return-void
.end method

.method public constructor <init>(Lcom/a/a/a/w;)V
    .locals 2

    .prologue
    .line 93
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 65
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/a/a/a/t;->d:Ljava/util/HashSet;

    .line 71
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/a/a/a/t;->e:Ljava/util/HashSet;

    .line 78
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/a/a/a/t;->f:Ljava/util/HashMap;

    .line 94
    iput-object p1, p0, Lcom/a/a/a/t;->c:Lcom/a/a/a/w;

    .line 95
    iget-object v0, p0, Lcom/a/a/a/t;->c:Lcom/a/a/a/w;

    invoke-virtual {v0}, Lcom/a/a/a/w;->b()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "Cannot set URL of address data server to null."

    invoke-static {v0, v1}, Lcom/a/a/a/X;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/a/a/a/t;->a:Ljava/lang/String;

    .line 96
    invoke-static {}, Lcom/a/a/a/H;->a()Lcom/a/a/a/H;

    move-result-object v0

    iput-object v0, p0, Lcom/a/a/a/t;->b:Lcom/a/a/a/H;

    .line 97
    return-void
.end method

.method static synthetic a(Lcom/a/a/a/t;)Ljava/util/HashSet;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/a/a/a/t;->e:Ljava/util/HashSet;

    return-object v0
.end method

.method static synthetic a(Lcom/a/a/a/t;Lcom/a/a/a/y;)V
    .locals 0

    .prologue
    .line 35
    invoke-static {p1}, Lcom/a/a/a/t;->a(Lcom/a/a/a/y;)V

    return-void
.end method

.method static synthetic a(Lcom/a/a/a/t;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 35
    new-instance v0, Lcom/a/a/a/M;

    invoke-direct {v0, p1}, Lcom/a/a/a/M;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/a/a/a/M;->a()Lcom/a/a/a/L;

    move-result-object v0

    iget-object v1, p0, Lcom/a/a/a/t;->f:Ljava/util/HashMap;

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/HashSet;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/a/a/a/u;

    invoke-virtual {p1}, Ljava/lang/String;->toString()Ljava/lang/String;

    invoke-virtual {v1}, Lcom/a/a/a/u;->a()V

    goto :goto_0

    :cond_0
    invoke-virtual {v0}, Ljava/util/HashSet;->clear()V

    :cond_1
    return-void
.end method

.method private static a(Lcom/a/a/a/y;)V
    .locals 0

    .prologue
    .line 252
    if-eqz p0, :cond_0

    .line 253
    invoke-interface {p0}, Lcom/a/a/a/y;->b()V

    .line 255
    :cond_0
    return-void
.end method

.method static synthetic b(Lcom/a/a/a/t;)Lcom/a/a/a/H;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/a/a/a/t;->b:Lcom/a/a/a/H;

    return-object v0
.end method

.method static synthetic c(Lcom/a/a/a/t;)Ljava/util/HashSet;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/a/a/a/t;->d:Ljava/util/HashSet;

    return-object v0
.end method

.method static synthetic d(Lcom/a/a/a/t;)Lcom/a/a/a/w;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/a/a/a/t;->c:Lcom/a/a/a/w;

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Lcom/a/a/a/H;
    .locals 1

    .prologue
    .line 374
    const-string/jumbo v0, "null key not allowed"

    invoke-static {p1, v0}, Lcom/a/a/a/X;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 375
    iget-object v0, p0, Lcom/a/a/a/t;->b:Lcom/a/a/a/H;

    invoke-virtual {v0, p1}, Lcom/a/a/a/H;->c(Ljava/lang/String;)Lcom/a/a/a/H;

    move-result-object v0

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 230
    iget-object v0, p0, Lcom/a/a/a/t;->a:Ljava/lang/String;

    return-object v0
.end method

.method final a(Lcom/a/a/a/L;)V
    .locals 3

    .prologue
    .line 343
    const-string/jumbo v0, "null key not allowed."

    invoke-static {p1, v0}, Lcom/a/a/a/X;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 344
    invoke-static {}, Lcom/a/a/a/S;->a()Ljava/util/Map;

    move-result-object v0

    sget-object v1, Lcom/a/a/a/e;->k:Lcom/a/a/a/e;

    invoke-virtual {p1, v1}, Lcom/a/a/a/L;->b(Lcom/a/a/a/e;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 346
    if-eqz v0, :cond_0

    .line 348
    :try_start_0
    iget-object v1, p0, Lcom/a/a/a/t;->b:Lcom/a/a/a/H;

    invoke-virtual {p1}, Lcom/a/a/a/L;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0}, Lcom/a/a/a/H;->a(Ljava/lang/String;)Lcom/a/a/a/H;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lcom/a/a/a/H;->a(Ljava/lang/String;Lorg/json/JSONObject;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 354
    :cond_0
    :goto_0
    return-void

    .line 350
    :catch_0
    move-exception v0

    const-string/jumbo v0, "CacheData"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "Failed to parse data for key "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " from RegionDataConstants"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method final a(Lcom/a/a/a/L;Lorg/json/JSONObject;Lcom/a/a/a/y;)V
    .locals 8

    .prologue
    const/16 v7, 0x1388

    const/4 v5, 0x0

    .line 267
    const-string/jumbo v0, "null key not allowed."

    invoke-static {p1, v0}, Lcom/a/a/a/X;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 269
    if-eqz p3, :cond_0

    .line 270
    invoke-interface {p3}, Lcom/a/a/a/y;->a()V

    .line 274
    :cond_0
    iget-object v0, p0, Lcom/a/a/a/t;->b:Lcom/a/a/a/H;

    invoke-virtual {p1}, Lcom/a/a/a/L;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/a/a/a/H;->d(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 275
    invoke-static {p3}, Lcom/a/a/a/t;->a(Lcom/a/a/a/y;)V

    .line 335
    :goto_0
    return-void

    .line 280
    :cond_1
    iget-object v0, p0, Lcom/a/a/a/t;->e:Ljava/util/HashSet;

    invoke-virtual {p1}, Lcom/a/a/a/L;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 281
    invoke-static {p3}, Lcom/a/a/a/t;->a(Lcom/a/a/a/y;)V

    goto :goto_0

    .line 286
    :cond_2
    iget-object v0, p0, Lcom/a/a/a/t;->d:Ljava/util/HashSet;

    invoke-virtual {p1}, Lcom/a/a/a/L;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 287
    const-string/jumbo v0, "CacheData"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "data for key "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " requested but not cached yet"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 288
    new-instance v1, Lcom/a/a/a/u;

    invoke-direct {v1, p0, p3}, Lcom/a/a/a/u;-><init>(Lcom/a/a/a/t;Lcom/a/a/a/y;)V

    invoke-static {p1}, Lcom/a/a/a/X;->a(Ljava/lang/Object;)V

    invoke-static {v1}, Lcom/a/a/a/X;->a(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/a/a/a/t;->f:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/HashSet;

    if-nez v0, :cond_3

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iget-object v2, p0, Lcom/a/a/a/t;->f:Ljava/util/HashMap;

    invoke-virtual {v2, p1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_3
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 298
    :cond_4
    iget-object v0, p0, Lcom/a/a/a/t;->c:Lcom/a/a/a/w;

    invoke-virtual {p1}, Lcom/a/a/a/L;->toString()Ljava/lang/String;

    invoke-virtual {v0}, Lcom/a/a/a/w;->a()Ljava/lang/String;

    move-result-object v6

    .line 299
    if-eqz v6, :cond_5

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_5

    .line 300
    new-instance v0, Lcom/a/a/a/v;

    invoke-virtual {p1}, Lcom/a/a/a/L;->toString()Ljava/lang/String;

    move-result-object v2

    move-object v1, p0

    move-object v3, p2

    move-object v4, p3

    invoke-direct/range {v0 .. v5}, Lcom/a/a/a/v;-><init>(Lcom/a/a/a/t;Ljava/lang/String;Lorg/json/JSONObject;Lcom/a/a/a/y;B)V

    .line 303
    :try_start_0
    invoke-static {v6}, Lcom/a/a/a/H;->a(Ljava/lang/String;)Lcom/a/a/a/H;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/a/a/a/v;->a(Lcom/a/a/a/v;Lcom/a/a/a/H;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 306
    :catch_0
    move-exception v0

    const-string/jumbo v0, "CacheData"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "Data from client\'s cache is in the wrong format: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 312
    :cond_5
    new-instance v0, Lcom/a/a/a/I;

    invoke-direct {v0}, Lcom/a/a/a/I;-><init>()V

    .line 313
    invoke-static {}, Lcom/a/a/a/K;->a()Lorg/apache/http/client/HttpClient;

    move-result-object v0

    invoke-interface {v0}, Lorg/apache/http/client/HttpClient;->getParams()Lorg/apache/http/params/HttpParams;

    move-result-object v0

    invoke-static {v0, v7}, Lorg/apache/http/params/HttpConnectionParams;->setConnectionTimeout(Lorg/apache/http/params/HttpParams;I)V

    invoke-static {v0, v7}, Lorg/apache/http/params/HttpConnectionParams;->setSoTimeout(Lorg/apache/http/params/HttpParams;I)V

    .line 314
    new-instance v0, Lcom/a/a/a/v;

    invoke-virtual {p1}, Lcom/a/a/a/L;->toString()Ljava/lang/String;

    move-result-object v2

    move-object v1, p0

    move-object v3, p2

    move-object v4, p3

    invoke-direct/range {v0 .. v5}, Lcom/a/a/a/v;-><init>(Lcom/a/a/a/t;Ljava/lang/String;Lorg/json/JSONObject;Lcom/a/a/a/y;B)V

    .line 316
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/a/a/a/t;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/a/a/a/L;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/a/a/a/J;

    invoke-direct {v2, p0, p1, p3, v0}, Lcom/a/a/a/J;-><init>(Lcom/a/a/a/t;Lcom/a/a/a/L;Lcom/a/a/a/y;Lcom/a/a/a/v;)V

    invoke-static {v1, v2}, Lcom/a/a/a/I;->a(Ljava/lang/String;Lcom/a/a/a/J;)V

    goto/16 :goto_0
.end method
