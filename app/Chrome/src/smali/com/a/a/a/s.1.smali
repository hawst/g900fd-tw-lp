.class public Lcom/a/a/a/s;
.super Ljava/lang/Object;
.source "AddressWidgetUiComponentProvider.java"


# instance fields
.field protected mContext:Landroid/content/Context;

.field protected mInflater:Landroid/view/LayoutInflater;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    iput-object p1, p0, Lcom/a/a/a/s;->mContext:Landroid/content/Context;

    .line 45
    const-string/jumbo v0, "layout_inflater"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/a/a/a/s;->mInflater:Landroid/view/LayoutInflater;

    .line 46
    return-void
.end method


# virtual methods
.method protected createUiLabel(Ljava/lang/CharSequence;Lcom/a/a/a/f;)Landroid/widget/TextView;
    .locals 4

    .prologue
    .line 56
    iget-object v0, p0, Lcom/a/a/a/s;->mInflater:Landroid/view/LayoutInflater;

    const v1, 0x7f040021

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 57
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 58
    return-object v0
.end method

.method protected createUiPickerAdapter(Lcom/a/a/a/f;)Landroid/widget/ArrayAdapter;
    .locals 3

    .prologue
    .line 89
    new-instance v0, Landroid/widget/ArrayAdapter;

    iget-object v1, p0, Lcom/a/a/a/s;->mContext:Landroid/content/Context;

    const v2, 0x1090008

    invoke-direct {v0, v1, v2}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    .line 91
    const v1, 0x1090009

    invoke-virtual {v0, v1}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    .line 92
    return-object v0
.end method

.method protected createUiPickerSpinner(Lcom/a/a/a/f;)Landroid/widget/Spinner;
    .locals 4

    .prologue
    .line 78
    iget-object v0, p0, Lcom/a/a/a/s;->mInflater:Landroid/view/LayoutInflater;

    const v1, 0x7f040020

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    return-object v0
.end method

.method protected createUiTextField(Lcom/a/a/a/f;)Landroid/widget/EditText;
    .locals 4

    .prologue
    .line 68
    iget-object v0, p0, Lcom/a/a/a/s;->mInflater:Landroid/view/LayoutInflater;

    const v1, 0x7f04001e

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    return-object v0
.end method

.method protected getUiActivityIndicatorView()Landroid/app/ProgressDialog;
    .locals 2

    .prologue
    .line 97
    new-instance v0, Landroid/app/ProgressDialog;

    iget-object v1, p0, Lcom/a/a/a/s;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    return-object v0
.end method
