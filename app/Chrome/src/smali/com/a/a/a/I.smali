.class final Lcom/a/a/a/I;
.super Ljava/lang/Object;
.source "JsonpRequestBuilder.java"


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 66
    return-void
.end method

.method private static a(Ljava/lang/String;)Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v3, 0x0

    .line 100
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v4

    .line 101
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    move v1, v3

    .line 104
    :goto_0
    if-ge v1, v4, :cond_1

    move v2, v3

    move v0, v1

    .line 107
    :goto_1
    if-ge v0, v4, :cond_0

    .line 108
    :try_start_0
    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    move-result v2

    .line 109
    const/16 v6, 0x3a

    if-eq v2, v6, :cond_0

    const/16 v6, 0x2f

    if-eq v2, v6, :cond_0

    .line 110
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 113
    :cond_0
    if-ne v0, v4, :cond_2

    .line 114
    invoke-virtual {p0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "UTF-8"

    invoke-static {v0, v1}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    .line 128
    :cond_1
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 116
    :cond_2
    if-le v0, v1, :cond_3

    .line 117
    :try_start_1
    invoke-virtual {p0, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v6, "UTF-8"

    invoke-static {v1, v6}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 118
    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 104
    :goto_2
    add-int/lit8 v1, v0, 0x1

    goto :goto_0

    .line 121
    :cond_3
    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
    :try_end_1
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_1 .. :try_end_1} :catch_0

    move v0, v1

    goto :goto_2

    .line 124
    :catch_0
    move-exception v0

    .line 125
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method static a(Ljava/lang/String;Lcom/a/a/a/J;)V
    .locals 2

    .prologue
    .line 59
    new-instance v0, Lorg/apache/http/client/methods/HttpGet;

    invoke-static {p0}, Lcom/a/a/a/I;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/lang/String;)V

    .line 60
    new-instance v1, Lcom/a/a/a/K;

    invoke-direct {v1, v0, p1}, Lcom/a/a/a/K;-><init>(Lorg/apache/http/client/methods/HttpUriRequest;Lcom/a/a/a/J;)V

    invoke-virtual {v1}, Lcom/a/a/a/K;->start()V

    .line 61
    return-void
.end method
