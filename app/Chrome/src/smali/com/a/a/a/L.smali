.class final Lcom/a/a/a/L;
.super Ljava/lang/Object;
.source "LookupKey.java"


# static fields
.field private static final a:[Lcom/a/a/a/e;


# instance fields
.field private final b:Lcom/a/a/a/N;

.field private final c:Lcom/a/a/a/O;

.field private final d:Ljava/util/Map;

.field private final e:Ljava/lang/String;

.field private final f:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 85
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/a/a/a/e;

    const/4 v1, 0x0

    sget-object v2, Lcom/a/a/a/e;->k:Lcom/a/a/a/e;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, Lcom/a/a/a/e;->a:Lcom/a/a/a/e;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    sget-object v2, Lcom/a/a/a/e;->b:Lcom/a/a/a/e;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    sget-object v2, Lcom/a/a/a/e;->g:Lcom/a/a/a/e;

    aput-object v2, v0, v1

    sput-object v0, Lcom/a/a/a/L;->a:[Lcom/a/a/a/e;

    return-void
.end method

.method private constructor <init>(Lcom/a/a/a/M;)V
    .locals 1

    .prologue
    .line 108
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 109
    invoke-static {p1}, Lcom/a/a/a/M;->a(Lcom/a/a/a/M;)Lcom/a/a/a/N;

    move-result-object v0

    iput-object v0, p0, Lcom/a/a/a/L;->b:Lcom/a/a/a/N;

    .line 110
    invoke-static {p1}, Lcom/a/a/a/M;->b(Lcom/a/a/a/M;)Lcom/a/a/a/O;

    move-result-object v0

    iput-object v0, p0, Lcom/a/a/a/L;->c:Lcom/a/a/a/O;

    .line 111
    invoke-static {p1}, Lcom/a/a/a/M;->c(Lcom/a/a/a/M;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/a/a/a/L;->d:Ljava/util/Map;

    .line 112
    invoke-static {p1}, Lcom/a/a/a/M;->d(Lcom/a/a/a/M;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/a/a/a/L;->f:Ljava/lang/String;

    .line 113
    invoke-direct {p0}, Lcom/a/a/a/L;->c()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/a/a/a/L;->e:Ljava/lang/String;

    .line 114
    return-void
.end method

.method synthetic constructor <init>(Lcom/a/a/a/M;B)V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0, p1}, Lcom/a/a/a/L;-><init>(Lcom/a/a/a/M;)V

    return-void
.end method

.method static synthetic a(Lcom/a/a/a/L;)Lcom/a/a/a/N;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/a/a/a/L;->b:Lcom/a/a/a/N;

    return-object v0
.end method

.method static a(Ljava/lang/String;)Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 267
    invoke-static {}, Lcom/a/a/a/N;->values()[Lcom/a/a/a/N;

    move-result-object v2

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v4, v2, v1

    .line 268
    invoke-virtual {v4}, Lcom/a/a/a/N;->name()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 269
    const/4 v0, 0x1

    .line 272
    :cond_0
    return v0

    .line 267
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method static synthetic b(Lcom/a/a/a/L;)Lcom/a/a/a/O;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/a/a/a/L;->c:Lcom/a/a/a/O;

    return-object v0
.end method

.method static synthetic b()[Lcom/a/a/a/e;
    .locals 1

    .prologue
    .line 35
    sget-object v0, Lcom/a/a/a/L;->a:[Lcom/a/a/a/e;

    return-object v0
.end method

.method private c()Ljava/lang/String;
    .locals 7

    .prologue
    .line 214
    new-instance v2, Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/a/a/a/L;->b:Lcom/a/a/a/N;

    invoke-virtual {v0}, Lcom/a/a/a/N;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 216
    iget-object v0, p0, Lcom/a/a/a/L;->b:Lcom/a/a/a/N;

    sget-object v1, Lcom/a/a/a/N;->a:Lcom/a/a/a/N;

    if-ne v0, v1, :cond_1

    .line 217
    sget-object v3, Lcom/a/a/a/L;->a:[Lcom/a/a/a/e;

    array-length v4, v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_2

    aget-object v0, v3, v1

    .line 218
    iget-object v5, p0, Lcom/a/a/a/L;->d:Ljava/util/Map;

    invoke-interface {v5, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 219
    sget-object v5, Lcom/a/a/a/e;->k:Lcom/a/a/a/e;

    if-ne v0, v5, :cond_0

    iget-object v5, p0, Lcom/a/a/a/L;->f:Ljava/lang/String;

    if-eqz v5, :cond_0

    .line 222
    const-string/jumbo v5, "/"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/a/a/a/L;->d:Ljava/util/Map;

    invoke-interface {v6, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v5, "--"

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v5, p0, Lcom/a/a/a/L;->f:Ljava/lang/String;

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 217
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 226
    :cond_0
    const-string/jumbo v5, "/"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/a/a/a/L;->d:Ljava/util/Map;

    invoke-interface {v6, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 230
    :cond_1
    iget-object v0, p0, Lcom/a/a/a/L;->d:Ljava/util/Map;

    sget-object v1, Lcom/a/a/a/e;->k:Lcom/a/a/a/e;

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 232
    const-string/jumbo v0, "/"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v0, p0, Lcom/a/a/a/L;->d:Ljava/util/Map;

    sget-object v3, Lcom/a/a/a/e;->k:Lcom/a/a/a/e;

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/a/a/a/L;->c:Lcom/a/a/a/O;

    invoke-virtual {v1}, Lcom/a/a/a/O;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "/_default"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 238
    :cond_2
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic c(Lcom/a/a/a/L;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/a/a/a/L;->f:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic d(Lcom/a/a/a/L;)Ljava/util/Map;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/a/a/a/L;->d:Ljava/util/Map;

    return-object v0
.end method


# virtual methods
.method final a(Lcom/a/a/a/e;)Lcom/a/a/a/L;
    .locals 10

    .prologue
    const/4 v3, 0x0

    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 127
    iget-object v2, p0, Lcom/a/a/a/L;->b:Lcom/a/a/a/N;

    sget-object v4, Lcom/a/a/a/N;->a:Lcom/a/a/a/N;

    if-eq v2, v4, :cond_0

    .line 129
    new-instance v0, Ljava/lang/RuntimeException;

    const-string/jumbo v1, "Only support getting parent keys for the data key type."

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 131
    :cond_0
    new-instance v5, Lcom/a/a/a/M;

    invoke-direct {v5, p0}, Lcom/a/a/a/M;-><init>(Lcom/a/a/a/L;)V

    .line 135
    sget-object v6, Lcom/a/a/a/L;->a:[Lcom/a/a/a/e;

    array-length v7, v6

    move v4, v0

    move v2, v0

    :goto_0
    if-ge v4, v7, :cond_4

    aget-object v8, v6, v4

    .line 136
    if-eqz v2, :cond_1

    .line 137
    invoke-static {v5}, Lcom/a/a/a/M;->c(Lcom/a/a/a/M;)Ljava/util/Map;

    move-result-object v9

    invoke-interface {v9, v8}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_1

    .line 138
    invoke-static {v5}, Lcom/a/a/a/M;->c(Lcom/a/a/a/M;)Ljava/util/Map;

    move-result-object v9

    invoke-interface {v9, v8}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 141
    :cond_1
    if-ne v8, p1, :cond_3

    .line 142
    invoke-static {v5}, Lcom/a/a/a/M;->c(Lcom/a/a/a/M;)Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0, v8}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    move-object v0, v3

    .line 157
    :goto_1
    return-object v0

    :cond_2
    move v0, v1

    move v2, v1

    .line 135
    :cond_3
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 150
    :cond_4
    if-nez v0, :cond_5

    move-object v0, v3

    .line 151
    goto :goto_1

    .line 154
    :cond_5
    iget-object v0, p0, Lcom/a/a/a/L;->f:Ljava/lang/String;

    invoke-static {v5, v0}, Lcom/a/a/a/M;->a(Lcom/a/a/a/M;Ljava/lang/String;)Ljava/lang/String;

    .line 155
    iget-object v0, p0, Lcom/a/a/a/L;->c:Lcom/a/a/a/O;

    invoke-static {v5, v0}, Lcom/a/a/a/M;->a(Lcom/a/a/a/M;Lcom/a/a/a/O;)Lcom/a/a/a/O;

    .line 157
    invoke-virtual {v5}, Lcom/a/a/a/M;->a()Lcom/a/a/a/L;

    move-result-object v0

    goto :goto_1
.end method

.method final a()Lcom/a/a/a/N;
    .locals 1

    .prologue
    .line 207
    iget-object v0, p0, Lcom/a/a/a/L;->b:Lcom/a/a/a/N;

    return-object v0
.end method

.method final b(Lcom/a/a/a/e;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 168
    invoke-virtual {p0, p1}, Lcom/a/a/a/L;->a(Lcom/a/a/a/e;)Lcom/a/a/a/L;

    move-result-object v0

    .line 170
    if-eqz v0, :cond_0

    .line 171
    invoke-virtual {v0}, Lcom/a/a/a/L;->toString()Ljava/lang/String;

    move-result-object v0

    .line 172
    const-string/jumbo v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v1

    .line 173
    if-lez v1, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eq v1, v2, :cond_0

    .line 174
    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 177
    :goto_0
    return-object v0

    :cond_0
    const-string/jumbo v0, ""

    goto :goto_0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 251
    if-ne p0, p1, :cond_0

    .line 252
    const/4 v0, 0x1

    .line 258
    :goto_0
    return v0

    .line 254
    :cond_0
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    if-eq v0, v1, :cond_2

    .line 255
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 258
    :cond_2
    check-cast p1, Lcom/a/a/a/L;

    invoke-virtual {p1}, Lcom/a/a/a/L;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/a/a/a/L;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 263
    iget-object v0, p0, Lcom/a/a/a/L;->e:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 246
    iget-object v0, p0, Lcom/a/a/a/L;->e:Ljava/lang/String;

    return-object v0
.end method
