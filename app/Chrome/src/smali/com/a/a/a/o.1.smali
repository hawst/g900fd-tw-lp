.class final Lcom/a/a/a/o;
.super Ljava/lang/Object;
.source "AddressWidget.java"

# interfaces
.implements Lcom/a/a/a/y;


# instance fields
.field private synthetic a:Lcom/a/a/a/l;


# direct methods
.method constructor <init>(Lcom/a/a/a/l;)V
    .locals 0

    .prologue
    .line 487
    iput-object p1, p0, Lcom/a/a/a/o;->a:Lcom/a/a/a/l;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 490
    iget-object v0, p0, Lcom/a/a/a/o;->a:Lcom/a/a/a/l;

    iget-object v1, p0, Lcom/a/a/a/o;->a:Lcom/a/a/a/l;

    invoke-static {v1}, Lcom/a/a/a/l;->b(Lcom/a/a/a/l;)Lcom/a/a/a/s;

    move-result-object v1

    invoke-virtual {v1}, Lcom/a/a/a/s;->getUiActivityIndicatorView()Landroid/app/ProgressDialog;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/a/a/a/l;->a(Lcom/a/a/a/l;Landroid/app/ProgressDialog;)Landroid/app/ProgressDialog;

    .line 491
    iget-object v0, p0, Lcom/a/a/a/o;->a:Lcom/a/a/a/l;

    invoke-static {v0}, Lcom/a/a/a/l;->d(Lcom/a/a/a/l;)Landroid/app/ProgressDialog;

    move-result-object v0

    iget-object v1, p0, Lcom/a/a/a/o;->a:Lcom/a/a/a/l;

    invoke-static {v1}, Lcom/a/a/a/l;->c(Lcom/a/a/a/l;)Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0a0023

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 492
    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "Progress dialog started."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 493
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 496
    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "Data loading completed."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 497
    iget-object v0, p0, Lcom/a/a/a/o;->a:Lcom/a/a/a/l;

    invoke-static {v0}, Lcom/a/a/a/l;->d(Lcom/a/a/a/l;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 498
    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "Progress dialog stopped."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 499
    iget-object v0, p0, Lcom/a/a/a/o;->a:Lcom/a/a/a/l;

    iget-object v0, v0, Lcom/a/a/a/l;->a:Landroid/os/Handler;

    iget-object v1, p0, Lcom/a/a/a/o;->a:Lcom/a/a/a/l;

    iget-object v1, v1, Lcom/a/a/a/l;->b:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 500
    return-void
.end method
