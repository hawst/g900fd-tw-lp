.class public final Lcom/a/a/a/D;
.super Ljava/lang/Object;
.source "FormOptions.java"


# instance fields
.field private final a:Ljava/util/EnumSet;

.field private final b:Ljava/util/EnumSet;

.field private final c:Ljava/util/EnumMap;

.field private final d:Ljava/util/Map;

.field private final e:Ljava/util/EnumMap;


# direct methods
.method private constructor <init>(Lcom/a/a/a/E;)V
    .locals 2

    .prologue
    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    new-instance v0, Ljava/util/EnumMap;

    const-class v1, Lcom/a/a/a/e;

    invoke-direct {v0, v1}, Ljava/util/EnumMap;-><init>(Ljava/lang/Class;)V

    iput-object v0, p0, Lcom/a/a/a/D;->c:Ljava/util/EnumMap;

    .line 45
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/a/a/a/D;->d:Ljava/util/Map;

    .line 48
    new-instance v0, Ljava/util/EnumMap;

    const-class v1, Lcom/a/a/a/e;

    invoke-direct {v0, v1}, Ljava/util/EnumMap;-><init>(Ljava/lang/Class;)V

    iput-object v0, p0, Lcom/a/a/a/D;->e:Ljava/util/EnumMap;

    .line 55
    invoke-static {p1}, Lcom/a/a/a/E;->a(Lcom/a/a/a/E;)Ljava/lang/String;

    .line 56
    invoke-static {p1}, Lcom/a/a/a/E;->b(Lcom/a/a/a/E;)Ljava/util/EnumSet;

    move-result-object v0

    invoke-static {v0}, Ljava/util/EnumSet;->copyOf(Ljava/util/EnumSet;)Ljava/util/EnumSet;

    move-result-object v0

    iput-object v0, p0, Lcom/a/a/a/D;->a:Ljava/util/EnumSet;

    .line 57
    invoke-static {p1}, Lcom/a/a/a/E;->c(Lcom/a/a/a/E;)Ljava/util/EnumSet;

    move-result-object v0

    invoke-static {v0}, Ljava/util/EnumSet;->copyOf(Ljava/util/EnumSet;)Ljava/util/EnumSet;

    move-result-object v0

    iput-object v0, p0, Lcom/a/a/a/D;->b:Ljava/util/EnumSet;

    .line 58
    invoke-static {p1}, Lcom/a/a/a/E;->d(Lcom/a/a/a/E;)Ljava/util/EnumSet;

    move-result-object v0

    invoke-static {v0}, Ljava/util/EnumSet;->copyOf(Ljava/util/EnumSet;)Ljava/util/EnumSet;

    .line 59
    iget-object v0, p0, Lcom/a/a/a/D;->c:Ljava/util/EnumMap;

    invoke-static {p1}, Lcom/a/a/a/E;->e(Lcom/a/a/a/E;)Ljava/util/EnumMap;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/EnumMap;->putAll(Ljava/util/Map;)V

    .line 60
    iget-object v0, p0, Lcom/a/a/a/D;->d:Ljava/util/Map;

    invoke-static {p1}, Lcom/a/a/a/E;->f(Lcom/a/a/a/E;)Ljava/util/Map;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 61
    iget-object v0, p0, Lcom/a/a/a/D;->e:Ljava/util/EnumMap;

    invoke-static {p1}, Lcom/a/a/a/E;->g(Lcom/a/a/a/E;)Ljava/util/EnumMap;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/EnumMap;->putAll(Ljava/util/Map;)V

    .line 62
    invoke-static {p1}, Lcom/a/a/a/E;->h(Lcom/a/a/a/E;)Ljava/lang/String;

    .line 63
    return-void
.end method

.method synthetic constructor <init>(Lcom/a/a/a/E;B)V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0, p1}, Lcom/a/a/a/D;-><init>(Lcom/a/a/a/E;)V

    return-void
.end method


# virtual methods
.method final a(Lcom/a/a/a/e;)Z
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lcom/a/a/a/D;->a:Ljava/util/EnumSet;

    invoke-virtual {v0, p1}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method final a(Ljava/lang/String;)[Lcom/a/a/a/e;
    .locals 2

    .prologue
    .line 107
    if-nez p1, :cond_0

    .line 108
    new-instance v0, Ljava/lang/RuntimeException;

    const-string/jumbo v1, "regionCode cannot be null."

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 110
    :cond_0
    iget-object v0, p0, Lcom/a/a/a/D;->d:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/a/a/a/e;

    return-object v0
.end method

.method final b(Lcom/a/a/a/e;)Z
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lcom/a/a/a/D;->b:Ljava/util/EnumSet;

    invoke-virtual {v0, p1}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method
