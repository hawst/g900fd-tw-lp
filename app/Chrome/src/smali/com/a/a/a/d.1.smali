.class final enum Lcom/a/a/a/d;
.super Ljava/lang/Enum;
.source "AddressDataKey.java"


# static fields
.field public static final enum a:Lcom/a/a/a/d;

.field public static final enum b:Lcom/a/a/a/d;

.field public static final enum c:Lcom/a/a/a/d;

.field public static final enum d:Lcom/a/a/a/d;

.field public static final enum e:Lcom/a/a/a/d;

.field public static final enum f:Lcom/a/a/a/d;

.field public static final enum g:Lcom/a/a/a/d;

.field public static final enum h:Lcom/a/a/a/d;

.field public static final enum i:Lcom/a/a/a/d;

.field public static final enum j:Lcom/a/a/a/d;

.field public static final enum k:Lcom/a/a/a/d;

.field public static final enum l:Lcom/a/a/a/d;

.field private static enum m:Lcom/a/a/a/d;

.field private static enum n:Lcom/a/a/a/d;

.field private static enum o:Lcom/a/a/a/d;

.field private static enum p:Lcom/a/a/a/d;

.field private static final q:Ljava/util/Map;

.field private static final synthetic r:[Lcom/a/a/a/d;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v0, 0x0

    .line 30
    new-instance v1, Lcom/a/a/a/d;

    const-string/jumbo v2, "COUNTRIES"

    invoke-direct {v1, v2, v0}, Lcom/a/a/a/d;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/a/a/a/d;->a:Lcom/a/a/a/d;

    .line 37
    new-instance v1, Lcom/a/a/a/d;

    const-string/jumbo v2, "FMT"

    invoke-direct {v1, v2, v4}, Lcom/a/a/a/d;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/a/a/a/d;->b:Lcom/a/a/a/d;

    .line 41
    new-instance v1, Lcom/a/a/a/d;

    const-string/jumbo v2, "ID"

    invoke-direct {v1, v2, v5}, Lcom/a/a/a/d;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/a/a/a/d;->c:Lcom/a/a/a/d;

    .line 48
    new-instance v1, Lcom/a/a/a/d;

    const-string/jumbo v2, "KEY"

    invoke-direct {v1, v2, v6}, Lcom/a/a/a/d;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/a/a/a/d;->m:Lcom/a/a/a/d;

    .line 52
    new-instance v1, Lcom/a/a/a/d;

    const-string/jumbo v2, "LANG"

    invoke-direct {v1, v2, v7}, Lcom/a/a/a/d;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/a/a/a/d;->d:Lcom/a/a/a/d;

    .line 57
    new-instance v1, Lcom/a/a/a/d;

    const-string/jumbo v2, "LFMT"

    const/4 v3, 0x5

    invoke-direct {v1, v2, v3}, Lcom/a/a/a/d;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/a/a/a/d;->e:Lcom/a/a/a/d;

    .line 61
    new-instance v1, Lcom/a/a/a/d;

    const-string/jumbo v2, "LOCALITY_NAME_TYPE"

    const/4 v3, 0x6

    invoke-direct {v1, v2, v3}, Lcom/a/a/a/d;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/a/a/a/d;->f:Lcom/a/a/a/d;

    .line 65
    new-instance v1, Lcom/a/a/a/d;

    const-string/jumbo v2, "REQUIRE"

    const/4 v3, 0x7

    invoke-direct {v1, v2, v3}, Lcom/a/a/a/d;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/a/a/a/d;->g:Lcom/a/a/a/d;

    .line 69
    new-instance v1, Lcom/a/a/a/d;

    const-string/jumbo v2, "STATE_NAME_TYPE"

    const/16 v3, 0x8

    invoke-direct {v1, v2, v3}, Lcom/a/a/a/d;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/a/a/a/d;->h:Lcom/a/a/a/d;

    .line 73
    new-instance v1, Lcom/a/a/a/d;

    const-string/jumbo v2, "SUB_KEYS"

    const/16 v3, 0x9

    invoke-direct {v1, v2, v3}, Lcom/a/a/a/d;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/a/a/a/d;->i:Lcom/a/a/a/d;

    .line 78
    new-instance v1, Lcom/a/a/a/d;

    const-string/jumbo v2, "SUB_LNAMES"

    const/16 v3, 0xa

    invoke-direct {v1, v2, v3}, Lcom/a/a/a/d;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/a/a/a/d;->j:Lcom/a/a/a/d;

    .line 82
    new-instance v1, Lcom/a/a/a/d;

    const-string/jumbo v2, "SUB_MORES"

    const/16 v3, 0xb

    invoke-direct {v1, v2, v3}, Lcom/a/a/a/d;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/a/a/a/d;->n:Lcom/a/a/a/d;

    .line 86
    new-instance v1, Lcom/a/a/a/d;

    const-string/jumbo v2, "SUB_NAMES"

    const/16 v3, 0xc

    invoke-direct {v1, v2, v3}, Lcom/a/a/a/d;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/a/a/a/d;->k:Lcom/a/a/a/d;

    .line 90
    new-instance v1, Lcom/a/a/a/d;

    const-string/jumbo v2, "XZIP"

    const/16 v3, 0xd

    invoke-direct {v1, v2, v3}, Lcom/a/a/a/d;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/a/a/a/d;->o:Lcom/a/a/a/d;

    .line 95
    new-instance v1, Lcom/a/a/a/d;

    const-string/jumbo v2, "ZIP"

    const/16 v3, 0xe

    invoke-direct {v1, v2, v3}, Lcom/a/a/a/d;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/a/a/a/d;->p:Lcom/a/a/a/d;

    .line 99
    new-instance v1, Lcom/a/a/a/d;

    const-string/jumbo v2, "ZIP_NAME_TYPE"

    const/16 v3, 0xf

    invoke-direct {v1, v2, v3}, Lcom/a/a/a/d;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/a/a/a/d;->l:Lcom/a/a/a/d;

    .line 26
    const/16 v1, 0x10

    new-array v1, v1, [Lcom/a/a/a/d;

    sget-object v2, Lcom/a/a/a/d;->a:Lcom/a/a/a/d;

    aput-object v2, v1, v0

    sget-object v2, Lcom/a/a/a/d;->b:Lcom/a/a/a/d;

    aput-object v2, v1, v4

    sget-object v2, Lcom/a/a/a/d;->c:Lcom/a/a/a/d;

    aput-object v2, v1, v5

    sget-object v2, Lcom/a/a/a/d;->m:Lcom/a/a/a/d;

    aput-object v2, v1, v6

    sget-object v2, Lcom/a/a/a/d;->d:Lcom/a/a/a/d;

    aput-object v2, v1, v7

    const/4 v2, 0x5

    sget-object v3, Lcom/a/a/a/d;->e:Lcom/a/a/a/d;

    aput-object v3, v1, v2

    const/4 v2, 0x6

    sget-object v3, Lcom/a/a/a/d;->f:Lcom/a/a/a/d;

    aput-object v3, v1, v2

    const/4 v2, 0x7

    sget-object v3, Lcom/a/a/a/d;->g:Lcom/a/a/a/d;

    aput-object v3, v1, v2

    const/16 v2, 0x8

    sget-object v3, Lcom/a/a/a/d;->h:Lcom/a/a/a/d;

    aput-object v3, v1, v2

    const/16 v2, 0x9

    sget-object v3, Lcom/a/a/a/d;->i:Lcom/a/a/a/d;

    aput-object v3, v1, v2

    const/16 v2, 0xa

    sget-object v3, Lcom/a/a/a/d;->j:Lcom/a/a/a/d;

    aput-object v3, v1, v2

    const/16 v2, 0xb

    sget-object v3, Lcom/a/a/a/d;->n:Lcom/a/a/a/d;

    aput-object v3, v1, v2

    const/16 v2, 0xc

    sget-object v3, Lcom/a/a/a/d;->k:Lcom/a/a/a/d;

    aput-object v3, v1, v2

    const/16 v2, 0xd

    sget-object v3, Lcom/a/a/a/d;->o:Lcom/a/a/a/d;

    aput-object v3, v1, v2

    const/16 v2, 0xe

    sget-object v3, Lcom/a/a/a/d;->p:Lcom/a/a/a/d;

    aput-object v3, v1, v2

    const/16 v2, 0xf

    sget-object v3, Lcom/a/a/a/d;->l:Lcom/a/a/a/d;

    aput-object v3, v1, v2

    sput-object v1, Lcom/a/a/a/d;->r:[Lcom/a/a/a/d;

    .line 109
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    sput-object v1, Lcom/a/a/a/d;->q:Ljava/util/Map;

    .line 114
    invoke-static {}, Lcom/a/a/a/d;->values()[Lcom/a/a/a/d;

    move-result-object v1

    array-length v2, v1

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    .line 115
    sget-object v4, Lcom/a/a/a/d;->q:Ljava/util/Map;

    invoke-virtual {v3}, Ljava/lang/Enum;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 114
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 117
    :cond_0
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method static a(Ljava/lang/String;)Lcom/a/a/a/d;
    .locals 2

    .prologue
    .line 106
    sget-object v0, Lcom/a/a/a/d;->q:Ljava/util/Map;

    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/a/a/d;

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/a/a/a/d;
    .locals 1

    .prologue
    .line 26
    const-class v0, Lcom/a/a/a/d;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/a/a/a/d;

    return-object v0
.end method

.method public static values()[Lcom/a/a/a/d;
    .locals 1

    .prologue
    .line 26
    sget-object v0, Lcom/a/a/a/d;->r:[Lcom/a/a/a/d;

    invoke-virtual {v0}, [Lcom/a/a/a/d;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/a/a/a/d;

    return-object v0
.end method
