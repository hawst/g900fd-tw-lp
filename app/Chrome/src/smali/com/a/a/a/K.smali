.class final Lcom/a/a/a/K;
.super Ljava/lang/Thread;
.source "JsonpRequestBuilder.java"


# static fields
.field private static final a:Lorg/apache/http/client/HttpClient;


# instance fields
.field private b:Lorg/apache/http/client/methods/HttpUriRequest;

.field private c:Lcom/a/a/a/J;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 68
    new-instance v0, Lorg/apache/http/impl/client/DefaultHttpClient;

    invoke-direct {v0}, Lorg/apache/http/impl/client/DefaultHttpClient;-><init>()V

    sput-object v0, Lcom/a/a/a/K;->a:Lorg/apache/http/client/HttpClient;

    return-void
.end method

.method protected constructor <init>(Lorg/apache/http/client/methods/HttpUriRequest;Lcom/a/a/a/J;)V
    .locals 0

    .prologue
    .line 74
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 75
    iput-object p1, p0, Lcom/a/a/a/K;->b:Lorg/apache/http/client/methods/HttpUriRequest;

    .line 76
    iput-object p2, p0, Lcom/a/a/a/K;->c:Lcom/a/a/a/J;

    .line 77
    return-void
.end method

.method static synthetic a()Lorg/apache/http/client/HttpClient;
    .locals 1

    .prologue
    .line 66
    sget-object v0, Lcom/a/a/a/K;->a:Lorg/apache/http/client/HttpClient;

    return-object v0
.end method


# virtual methods
.method public final run()V
    .locals 4

    .prologue
    .line 83
    :try_start_0
    sget-object v1, Lcom/a/a/a/K;->a:Lorg/apache/http/client/HttpClient;

    monitor-enter v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 84
    :try_start_1
    sget-object v0, Lcom/a/a/a/K;->a:Lorg/apache/http/client/HttpClient;

    iget-object v2, p0, Lcom/a/a/a/K;->b:Lorg/apache/http/client/methods/HttpUriRequest;

    new-instance v3, Lorg/apache/http/impl/client/BasicResponseHandler;

    invoke-direct {v3}, Lorg/apache/http/impl/client/BasicResponseHandler;-><init>()V

    invoke-interface {v0, v2, v3}, Lorg/apache/http/client/HttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;Lorg/apache/http/client/ResponseHandler;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 85
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 86
    :try_start_2
    iget-object v1, p0, Lcom/a/a/a/K;->c:Lcom/a/a/a/J;

    invoke-static {v0}, Lcom/a/a/a/H;->a(Ljava/lang/String;)Lcom/a/a/a/H;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/a/a/a/J;->a(Ljava/lang/Object;)V

    .line 90
    :goto_0
    return-void

    .line 85
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    .line 87
    :catch_0
    move-exception v0

    .line 88
    iget-object v0, p0, Lcom/a/a/a/K;->c:Lcom/a/a/a/J;

    invoke-virtual {v0}, Lcom/a/a/a/J;->a()V

    goto :goto_0
.end method
