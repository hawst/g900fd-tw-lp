.class public final Lcom/a/a/a/A;
.super Ljava/lang/Object;
.source "FieldVerifier.java"


# instance fields
.field private a:Ljava/lang/String;

.field private b:Lcom/a/a/a/z;

.field private c:[Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/a/a/a/z;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 66
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 67
    iput-object p1, p0, Lcom/a/a/a/A;->b:Lcom/a/a/a/z;

    .line 68
    const-string/jumbo v0, "data"

    iput-object v0, p0, Lcom/a/a/a/A;->a:Ljava/lang/String;

    iget-object v0, p0, Lcom/a/a/a/A;->b:Lcom/a/a/a/z;

    const-string/jumbo v1, "data"

    invoke-interface {v0, v1}, Lcom/a/a/a/z;->b(Ljava/lang/String;)Lcom/a/a/a/k;

    move-result-object v0

    sget-object v1, Lcom/a/a/a/d;->a:Lcom/a/a/a/d;

    invoke-virtual {v0, v1}, Lcom/a/a/a/k;->a(Lcom/a/a/a/d;)Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v1, Lcom/a/a/a/d;->a:Lcom/a/a/a/d;

    invoke-virtual {v0, v1}, Lcom/a/a/a/k;->b(Lcom/a/a/a/d;)Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "~"

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/a/a/a/A;->c:[Ljava/lang/String;

    :cond_0
    iget-object v0, p0, Lcom/a/a/a/A;->c:[Ljava/lang/String;

    invoke-static {v0, v2, v2}, Lcom/a/a/a/X;->a([Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;)Ljava/util/Map;

    iget-object v0, p0, Lcom/a/a/a/A;->b:Lcom/a/a/a/z;

    const-string/jumbo v1, "data/ZZ"

    invoke-interface {v0, v1}, Lcom/a/a/a/z;->b(Ljava/lang/String;)Lcom/a/a/a/k;

    move-result-object v0

    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    sget-object v1, Lcom/a/a/a/d;->b:Lcom/a/a/a/d;

    invoke-virtual {v0, v1}, Lcom/a/a/a/k;->a(Lcom/a/a/a/d;)Z

    move-result v1

    if-eqz v1, :cond_1

    sget-object v1, Lcom/a/a/a/d;->b:Lcom/a/a/a/d;

    invoke-virtual {v0, v1}, Lcom/a/a/a/k;->b(Lcom/a/a/a/d;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/a/a/a/A;->a(Ljava/lang/String;)Ljava/util/Set;

    :cond_1
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    sget-object v1, Lcom/a/a/a/d;->g:Lcom/a/a/a/d;

    invoke-virtual {v0, v1}, Lcom/a/a/a/k;->a(Lcom/a/a/a/d;)Z

    move-result v1

    if-eqz v1, :cond_2

    sget-object v1, Lcom/a/a/a/d;->g:Lcom/a/a/a/d;

    invoke-virtual {v0, v1}, Lcom/a/a/a/k;->b(Lcom/a/a/a/d;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/a/a/a/A;->b(Ljava/lang/String;)Ljava/util/Set;

    .line 69
    :cond_2
    return-void
.end method

.method private static a(Ljava/lang/String;)Ljava/util/Set;
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 298
    sget-object v0, Lcom/a/a/a/e;->k:Lcom/a/a/a/e;

    invoke-static {v0}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v3

    .line 300
    invoke-virtual {p0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v4

    array-length v5, v4

    move v2, v1

    move v0, v1

    :goto_0
    if-ge v2, v5, :cond_3

    aget-char v6, v4, v2

    .line 301
    if-eqz v0, :cond_2

    .line 303
    const/16 v0, 0x6e

    if-eq v6, v0, :cond_4

    .line 304
    invoke-static {v6}, Lcom/a/a/a/e;->a(C)Lcom/a/a/a/e;

    move-result-object v0

    .line 307
    if-nez v0, :cond_0

    .line 308
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "Unrecognized character \'"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "\' in format pattern: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 311
    :cond_0
    invoke-virtual {v3, v0}, Ljava/util/EnumSet;->add(Ljava/lang/Object;)Z

    move v0, v1

    .line 300
    :cond_1
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 312
    :cond_2
    const/16 v7, 0x25

    if-ne v6, v7, :cond_1

    .line 313
    const/4 v0, 0x1

    goto :goto_1

    .line 319
    :cond_3
    sget-object v0, Lcom/a/a/a/e;->e:Lcom/a/a/a/e;

    invoke-virtual {v3, v0}, Ljava/util/EnumSet;->remove(Ljava/lang/Object;)Z

    .line 320
    sget-object v0, Lcom/a/a/a/e;->f:Lcom/a/a/a/e;

    invoke-virtual {v3, v0}, Ljava/util/EnumSet;->remove(Ljava/lang/Object;)Z

    .line 322
    return-object v3

    :cond_4
    move v0, v1

    goto :goto_1
.end method

.method private static b(Ljava/lang/String;)Ljava/util/Set;
    .locals 6

    .prologue
    .line 331
    sget-object v0, Lcom/a/a/a/e;->k:Lcom/a/a/a/e;

    invoke-static {v0}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v1

    .line 333
    invoke-virtual {p0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_1

    aget-char v4, v2, v0

    .line 334
    invoke-static {v4}, Lcom/a/a/a/e;->a(C)Lcom/a/a/a/e;

    move-result-object v5

    .line 335
    if-nez v5, :cond_0

    .line 336
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "Unrecognized character \'"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "\' in require pattern: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 339
    :cond_0
    invoke-virtual {v1, v5}, Ljava/util/EnumSet;->add(Ljava/lang/Object;)Z

    .line 333
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 344
    :cond_1
    sget-object v0, Lcom/a/a/a/e;->e:Lcom/a/a/a/e;

    invoke-virtual {v1, v0}, Ljava/util/EnumSet;->remove(Ljava/lang/Object;)Z

    .line 345
    sget-object v0, Lcom/a/a/a/e;->f:Lcom/a/a/a/e;

    invoke-virtual {v1, v0}, Ljava/util/EnumSet;->remove(Ljava/lang/Object;)Z

    .line 347
    return-object v1
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 203
    iget-object v0, p0, Lcom/a/a/a/A;->a:Ljava/lang/String;

    return-object v0
.end method
