.class public Lcom/a/a/a/a;
.super Ljava/lang/Object;
.source "RegionData.java"


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Ljava/lang/String;

.field private final c:Ljava/lang/String;

.field private final d:Ljava/lang/String;

.field private final e:Ljava/lang/String;

.field private final f:Ljava/lang/String;

.field private final g:Ljava/lang/String;

.field private final h:Ljava/lang/String;

.field private final i:Ljava/lang/String;

.field private final j:Ljava/lang/String;

.field private final k:Ljava/lang/String;


# direct methods
.method private constructor <init>(Lcom/a/a/a/c;)V
    .locals 2

    .prologue
    .line 112
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 113
    invoke-static {p1}, Lcom/a/a/a/c;->a(Lcom/a/a/a/c;)Ljava/util/Map;

    move-result-object v0

    sget-object v1, Lcom/a/a/a/e;->k:Lcom/a/a/a/e;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/a/a/a/a;->a:Ljava/lang/String;

    .line 114
    invoke-static {p1}, Lcom/a/a/a/c;->a(Lcom/a/a/a/c;)Ljava/util/Map;

    move-result-object v0

    sget-object v1, Lcom/a/a/a/e;->a:Lcom/a/a/a/e;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/a/a/a/a;->d:Ljava/lang/String;

    .line 115
    invoke-static {p1}, Lcom/a/a/a/c;->a(Lcom/a/a/a/c;)Ljava/util/Map;

    move-result-object v0

    sget-object v1, Lcom/a/a/a/e;->b:Lcom/a/a/a/e;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/a/a/a/a;->e:Ljava/lang/String;

    .line 116
    invoke-static {p1}, Lcom/a/a/a/c;->a(Lcom/a/a/a/c;)Ljava/util/Map;

    move-result-object v0

    sget-object v1, Lcom/a/a/a/e;->g:Lcom/a/a/a/e;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/a/a/a/a;->f:Ljava/lang/String;

    .line 117
    invoke-static {p1}, Lcom/a/a/a/c;->a(Lcom/a/a/a/c;)Ljava/util/Map;

    move-result-object v0

    sget-object v1, Lcom/a/a/a/e;->h:Lcom/a/a/a/e;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/a/a/a/a;->g:Ljava/lang/String;

    .line 118
    invoke-static {p1}, Lcom/a/a/a/c;->a(Lcom/a/a/a/c;)Ljava/util/Map;

    move-result-object v0

    sget-object v1, Lcom/a/a/a/e;->i:Lcom/a/a/a/e;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/a/a/a/a;->h:Ljava/lang/String;

    .line 119
    invoke-static {p1}, Lcom/a/a/a/c;->a(Lcom/a/a/a/c;)Ljava/util/Map;

    move-result-object v0

    sget-object v1, Lcom/a/a/a/e;->d:Lcom/a/a/a/e;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/a/a/a/a;->i:Ljava/lang/String;

    .line 120
    invoke-static {p1}, Lcom/a/a/a/c;->a(Lcom/a/a/a/c;)Ljava/util/Map;

    move-result-object v0

    sget-object v1, Lcom/a/a/a/e;->c:Lcom/a/a/a/e;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/a/a/a/a;->j:Ljava/lang/String;

    .line 121
    invoke-static {p1}, Lcom/a/a/a/c;->a(Lcom/a/a/a/c;)Ljava/util/Map;

    move-result-object v0

    sget-object v1, Lcom/a/a/a/e;->e:Lcom/a/a/a/e;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/a/a/a/a;->b:Ljava/lang/String;

    .line 122
    invoke-static {p1}, Lcom/a/a/a/c;->a(Lcom/a/a/a/c;)Ljava/util/Map;

    move-result-object v0

    sget-object v1, Lcom/a/a/a/e;->f:Lcom/a/a/a/e;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/a/a/a/a;->c:Ljava/lang/String;

    .line 123
    invoke-static {p1}, Lcom/a/a/a/c;->b(Lcom/a/a/a/c;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/a/a/a/a;->k:Ljava/lang/String;

    .line 124
    return-void
.end method

.method synthetic constructor <init>(Lcom/a/a/a/c;B)V
    .locals 0

    .prologue
    .line 56
    invoke-direct {p0, p1}, Lcom/a/a/a/a;-><init>(Lcom/a/a/a/c;)V

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 133
    iget-object v0, p0, Lcom/a/a/a/a;->a:Ljava/lang/String;

    return-object v0
.end method

.method public final a(Lcom/a/a/a/e;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 208
    sget-object v0, Lcom/a/a/a/b;->a:[I

    invoke-virtual {p1}, Lcom/a/a/a/e;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 230
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "unrecognized key: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 210
    :pswitch_0
    iget-object v0, p0, Lcom/a/a/a/a;->a:Ljava/lang/String;

    .line 228
    :goto_0
    return-object v0

    .line 212
    :pswitch_1
    iget-object v0, p0, Lcom/a/a/a/a;->d:Ljava/lang/String;

    goto :goto_0

    .line 214
    :pswitch_2
    iget-object v0, p0, Lcom/a/a/a/a;->e:Ljava/lang/String;

    goto :goto_0

    .line 216
    :pswitch_3
    iget-object v0, p0, Lcom/a/a/a/a;->f:Ljava/lang/String;

    goto :goto_0

    .line 218
    :pswitch_4
    iget-object v0, p0, Lcom/a/a/a/a;->g:Ljava/lang/String;

    goto :goto_0

    .line 220
    :pswitch_5
    iget-object v0, p0, Lcom/a/a/a/a;->h:Ljava/lang/String;

    goto :goto_0

    .line 222
    :pswitch_6
    iget-object v0, p0, Lcom/a/a/a/a;->b:Ljava/lang/String;

    goto :goto_0

    .line 224
    :pswitch_7
    iget-object v0, p0, Lcom/a/a/a/a;->c:Ljava/lang/String;

    goto :goto_0

    .line 226
    :pswitch_8
    iget-object v0, p0, Lcom/a/a/a/a;->i:Ljava/lang/String;

    goto :goto_0

    .line 228
    :pswitch_9
    iget-object v0, p0, Lcom/a/a/a/a;->j:Ljava/lang/String;

    goto :goto_0

    .line 208
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
    .end packed-switch
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 137
    iget-object v0, p0, Lcom/a/a/a/a;->b:Ljava/lang/String;

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 141
    iget-object v0, p0, Lcom/a/a/a/a;->c:Ljava/lang/String;

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 151
    iget-object v0, p0, Lcom/a/a/a/a;->d:Ljava/lang/String;

    return-object v0
.end method

.method public final e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 161
    iget-object v0, p0, Lcom/a/a/a/a;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 174
    iget-object v0, p0, Lcom/a/a/a/a;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 181
    iget-object v0, p0, Lcom/a/a/a/a;->i:Ljava/lang/String;

    return-object v0
.end method

.method public final h()Ljava/lang/String;
    .locals 1

    .prologue
    .line 188
    iget-object v0, p0, Lcom/a/a/a/a;->j:Ljava/lang/String;

    return-object v0
.end method

.method public final i()Ljava/lang/String;
    .locals 1

    .prologue
    .line 196
    iget-object v0, p0, Lcom/a/a/a/a;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final j()Ljava/lang/String;
    .locals 1

    .prologue
    .line 204
    iget-object v0, p0, Lcom/a/a/a/a;->h:Ljava/lang/String;

    return-object v0
.end method

.method public final k()Ljava/lang/String;
    .locals 1

    .prologue
    .line 241
    iget-object v0, p0, Lcom/a/a/a/a;->k:Ljava/lang/String;

    return-object v0
.end method
