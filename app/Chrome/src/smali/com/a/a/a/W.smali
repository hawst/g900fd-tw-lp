.class public final Lcom/a/a/a/W;
.super Ljava/lang/Object;
.source "StandardChecks.java"


# static fields
.field public static final a:Ljava/util/Map;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 36
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 39
    sget-object v1, Lcom/a/a/a/e;->k:Lcom/a/a/a/e;

    new-array v2, v7, [Lcom/a/a/a/g;

    sget-object v3, Lcom/a/a/a/g;->a:Lcom/a/a/a/g;

    aput-object v3, v2, v4

    sget-object v3, Lcom/a/a/a/g;->b:Lcom/a/a/a/g;

    aput-object v3, v2, v5

    sget-object v3, Lcom/a/a/a/g;->c:Lcom/a/a/a/g;

    aput-object v3, v2, v6

    invoke-static {v0, v1, v2}, Lcom/a/a/a/W;->a(Ljava/util/Map;Lcom/a/a/a/e;[Lcom/a/a/a/g;)V

    .line 41
    sget-object v1, Lcom/a/a/a/e;->a:Lcom/a/a/a/e;

    new-array v2, v7, [Lcom/a/a/a/g;

    sget-object v3, Lcom/a/a/a/g;->a:Lcom/a/a/a/g;

    aput-object v3, v2, v4

    sget-object v3, Lcom/a/a/a/g;->b:Lcom/a/a/a/g;

    aput-object v3, v2, v5

    sget-object v3, Lcom/a/a/a/g;->c:Lcom/a/a/a/g;

    aput-object v3, v2, v6

    invoke-static {v0, v1, v2}, Lcom/a/a/a/W;->a(Ljava/util/Map;Lcom/a/a/a/e;[Lcom/a/a/a/g;)V

    .line 43
    sget-object v1, Lcom/a/a/a/e;->b:Lcom/a/a/a/e;

    new-array v2, v7, [Lcom/a/a/a/g;

    sget-object v3, Lcom/a/a/a/g;->a:Lcom/a/a/a/g;

    aput-object v3, v2, v4

    sget-object v3, Lcom/a/a/a/g;->b:Lcom/a/a/a/g;

    aput-object v3, v2, v5

    sget-object v3, Lcom/a/a/a/g;->c:Lcom/a/a/a/g;

    aput-object v3, v2, v6

    invoke-static {v0, v1, v2}, Lcom/a/a/a/W;->a(Ljava/util/Map;Lcom/a/a/a/e;[Lcom/a/a/a/g;)V

    .line 45
    sget-object v1, Lcom/a/a/a/e;->g:Lcom/a/a/a/e;

    new-array v2, v7, [Lcom/a/a/a/g;

    sget-object v3, Lcom/a/a/a/g;->a:Lcom/a/a/a/g;

    aput-object v3, v2, v4

    sget-object v3, Lcom/a/a/a/g;->b:Lcom/a/a/a/g;

    aput-object v3, v2, v5

    sget-object v3, Lcom/a/a/a/g;->c:Lcom/a/a/a/g;

    aput-object v3, v2, v6

    invoke-static {v0, v1, v2}, Lcom/a/a/a/W;->a(Ljava/util/Map;Lcom/a/a/a/e;[Lcom/a/a/a/g;)V

    .line 47
    sget-object v1, Lcom/a/a/a/e;->h:Lcom/a/a/a/e;

    const/4 v2, 0x4

    new-array v2, v2, [Lcom/a/a/a/g;

    sget-object v3, Lcom/a/a/a/g;->a:Lcom/a/a/a/g;

    aput-object v3, v2, v4

    sget-object v3, Lcom/a/a/a/g;->b:Lcom/a/a/a/g;

    aput-object v3, v2, v5

    sget-object v3, Lcom/a/a/a/g;->d:Lcom/a/a/a/g;

    aput-object v3, v2, v6

    sget-object v3, Lcom/a/a/a/g;->e:Lcom/a/a/a/g;

    aput-object v3, v2, v7

    invoke-static {v0, v1, v2}, Lcom/a/a/a/W;->a(Ljava/util/Map;Lcom/a/a/a/e;[Lcom/a/a/a/g;)V

    .line 50
    sget-object v1, Lcom/a/a/a/e;->j:Lcom/a/a/a/e;

    new-array v2, v6, [Lcom/a/a/a/g;

    sget-object v3, Lcom/a/a/a/g;->a:Lcom/a/a/a/g;

    aput-object v3, v2, v4

    sget-object v3, Lcom/a/a/a/g;->b:Lcom/a/a/a/g;

    aput-object v3, v2, v5

    invoke-static {v0, v1, v2}, Lcom/a/a/a/W;->a(Ljava/util/Map;Lcom/a/a/a/e;[Lcom/a/a/a/g;)V

    .line 52
    sget-object v1, Lcom/a/a/a/e;->i:Lcom/a/a/a/e;

    new-array v2, v6, [Lcom/a/a/a/g;

    sget-object v3, Lcom/a/a/a/g;->a:Lcom/a/a/a/g;

    aput-object v3, v2, v4

    sget-object v3, Lcom/a/a/a/g;->b:Lcom/a/a/a/g;

    aput-object v3, v2, v5

    invoke-static {v0, v1, v2}, Lcom/a/a/a/W;->a(Ljava/util/Map;Lcom/a/a/a/e;[Lcom/a/a/a/g;)V

    .line 54
    sget-object v1, Lcom/a/a/a/e;->d:Lcom/a/a/a/e;

    new-array v2, v6, [Lcom/a/a/a/g;

    sget-object v3, Lcom/a/a/a/g;->a:Lcom/a/a/a/g;

    aput-object v3, v2, v4

    sget-object v3, Lcom/a/a/a/g;->b:Lcom/a/a/a/g;

    aput-object v3, v2, v5

    invoke-static {v0, v1, v2}, Lcom/a/a/a/W;->a(Ljava/util/Map;Lcom/a/a/a/e;[Lcom/a/a/a/g;)V

    .line 56
    sget-object v1, Lcom/a/a/a/e;->c:Lcom/a/a/a/e;

    new-array v2, v6, [Lcom/a/a/a/g;

    sget-object v3, Lcom/a/a/a/g;->a:Lcom/a/a/a/g;

    aput-object v3, v2, v4

    sget-object v3, Lcom/a/a/a/g;->b:Lcom/a/a/a/g;

    aput-object v3, v2, v5

    invoke-static {v0, v1, v2}, Lcom/a/a/a/W;->a(Ljava/util/Map;Lcom/a/a/a/e;[Lcom/a/a/a/g;)V

    .line 59
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    sput-object v0, Lcom/a/a/a/W;->a:Ljava/util/Map;

    .line 60
    return-void
.end method

.method private static varargs a(Ljava/util/Map;Lcom/a/a/a/e;[Lcom/a/a/a/g;)V
    .locals 1

    .prologue
    .line 65
    invoke-static {p2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    invoke-interface {p0, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 66
    return-void
.end method
