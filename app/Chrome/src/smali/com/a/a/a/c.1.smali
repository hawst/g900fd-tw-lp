.class public final Lcom/a/a/a/c;
.super Ljava/lang/Object;
.source "AddressData.java"


# instance fields
.field private final a:Ljava/util/Map;

.field private b:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 253
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 251
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/a/a/a/c;->b:Ljava/lang/String;

    .line 254
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/a/a/a/c;->a:Ljava/util/Map;

    .line 255
    return-void
.end method

.method public constructor <init>(Lcom/a/a/a/a;)V
    .locals 1

    .prologue
    .line 263
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 251
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/a/a/a/c;->b:Ljava/lang/String;

    .line 264
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/a/a/a/c;->a:Ljava/util/Map;

    .line 265
    invoke-direct {p0, p1}, Lcom/a/a/a/c;->a(Lcom/a/a/a/a;)Lcom/a/a/a/c;

    .line 266
    return-void
.end method

.method private a(Lcom/a/a/a/a;)Lcom/a/a/a/c;
    .locals 5

    .prologue
    .line 322
    iget-object v0, p0, Lcom/a/a/a/c;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 323
    invoke-static {}, Lcom/a/a/a/e;->values()[Lcom/a/a/a/e;

    move-result-object v1

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 324
    sget-object v4, Lcom/a/a/a/e;->j:Lcom/a/a/a/e;

    if-eq v3, v4, :cond_0

    .line 325
    invoke-virtual {p1, v3}, Lcom/a/a/a/a;->a(Lcom/a/a/a/e;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v3, v4}, Lcom/a/a/a/c;->a(Lcom/a/a/a/e;Ljava/lang/String;)Lcom/a/a/a/c;

    .line 323
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 330
    :cond_1
    invoke-direct {p0}, Lcom/a/a/a/c;->b()V

    .line 331
    invoke-virtual {p1}, Lcom/a/a/a/a;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/a/a/a/c;->b:Ljava/lang/String;

    .line 332
    return-object p0
.end method

.method static synthetic a(Lcom/a/a/a/c;)Ljava/util/Map;
    .locals 1

    .prologue
    .line 247
    iget-object v0, p0, Lcom/a/a/a/c;->a:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic b(Lcom/a/a/a/c;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 247
    iget-object v0, p0, Lcom/a/a/a/c;->b:Ljava/lang/String;

    return-object v0
.end method

.method private b()V
    .locals 6

    .prologue
    const/4 v4, 0x1

    .line 377
    iget-object v0, p0, Lcom/a/a/a/c;->a:Ljava/util/Map;

    sget-object v1, Lcom/a/a/a/e;->e:Lcom/a/a/a/e;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 378
    iget-object v1, p0, Lcom/a/a/a/c;->a:Ljava/util/Map;

    sget-object v2, Lcom/a/a/a/e;->f:Lcom/a/a/a/e;

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 379
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_2

    .line 381
    :cond_0
    const/4 v0, 0x0

    .line 383
    :goto_0
    if-eqz v1, :cond_1

    .line 384
    const-string/jumbo v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 385
    array-length v3, v2

    if-le v3, v4, :cond_1

    .line 386
    const/4 v0, 0x0

    aget-object v1, v2, v0

    .line 387
    aget-object v0, v2, v4

    .line 390
    :cond_1
    iget-object v2, p0, Lcom/a/a/a/c;->a:Ljava/util/Map;

    sget-object v3, Lcom/a/a/a/e;->e:Lcom/a/a/a/e;

    invoke-interface {v2, v3, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 391
    iget-object v1, p0, Lcom/a/a/a/c;->a:Ljava/util/Map;

    sget-object v2, Lcom/a/a/a/e;->f:Lcom/a/a/a/e;

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 392
    return-void

    :cond_2
    move-object v5, v1

    move-object v1, v0

    move-object v0, v5

    goto :goto_0
.end method


# virtual methods
.method public final a()Lcom/a/a/a/a;
    .locals 2

    .prologue
    .line 367
    new-instance v0, Lcom/a/a/a/a;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/a/a/a/a;-><init>(Lcom/a/a/a/c;B)V

    return-object v0
.end method

.method public final a(Lcom/a/a/a/e;Ljava/lang/String;)Lcom/a/a/a/c;
    .locals 2

    .prologue
    .line 357
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_1

    .line 358
    :cond_0
    iget-object v0, p0, Lcom/a/a/a/c;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 362
    :goto_0
    invoke-direct {p0}, Lcom/a/a/a/c;->b()V

    .line 363
    return-object p0

    .line 360
    :cond_1
    iget-object v0, p0, Lcom/a/a/a/c;->a:Ljava/util/Map;

    invoke-virtual {p2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)Lcom/a/a/a/c;
    .locals 1

    .prologue
    .line 269
    sget-object v0, Lcom/a/a/a/e;->k:Lcom/a/a/a/e;

    invoke-virtual {p0, v0, p1}, Lcom/a/a/a/c;->a(Lcom/a/a/a/e;Ljava/lang/String;)Lcom/a/a/a/c;

    move-result-object v0

    return-object v0
.end method

.method public final b(Ljava/lang/String;)Lcom/a/a/a/c;
    .locals 0

    .prologue
    .line 298
    iput-object p1, p0, Lcom/a/a/a/c;->b:Ljava/lang/String;

    .line 299
    return-object p0
.end method
