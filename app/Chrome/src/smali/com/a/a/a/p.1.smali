.class final Lcom/a/a/a/p;
.super Ljava/lang/Object;
.source "AddressWidget.java"


# instance fields
.field private a:Landroid/widget/Spinner;

.field private b:Lcom/a/a/a/e;

.field private c:Lcom/a/a/a/e;

.field private d:Landroid/widget/ArrayAdapter;

.field private e:Ljava/util/List;


# direct methods
.method public constructor <init>(Landroid/widget/Spinner;Lcom/a/a/a/e;Lcom/a/a/a/e;)V
    .locals 1

    .prologue
    .line 178
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 179
    iput-object p1, p0, Lcom/a/a/a/p;->a:Landroid/widget/Spinner;

    .line 180
    iput-object p2, p0, Lcom/a/a/a/p;->b:Lcom/a/a/a/e;

    .line 181
    iput-object p3, p0, Lcom/a/a/a/p;->c:Lcom/a/a/a/e;

    .line 182
    invoke-virtual {p1}, Landroid/widget/Spinner;->getAdapter()Landroid/widget/SpinnerAdapter;

    move-result-object v0

    check-cast v0, Landroid/widget/ArrayAdapter;

    iput-object v0, p0, Lcom/a/a/a/p;->d:Landroid/widget/ArrayAdapter;

    .line 183
    return-void
.end method

.method static synthetic a(Lcom/a/a/a/p;)Landroid/widget/Spinner;
    .locals 1

    .prologue
    .line 166
    iget-object v0, p0, Lcom/a/a/a/p;->a:Landroid/widget/Spinner;

    return-object v0
.end method

.method static synthetic b(Lcom/a/a/a/p;)Lcom/a/a/a/e;
    .locals 1

    .prologue
    .line 166
    iget-object v0, p0, Lcom/a/a/a/p;->b:Lcom/a/a/a/e;

    return-object v0
.end method

.method static synthetic c(Lcom/a/a/a/p;)Lcom/a/a/a/e;
    .locals 1

    .prologue
    .line 166
    iget-object v0, p0, Lcom/a/a/a/p;->c:Lcom/a/a/a/e;

    return-object v0
.end method


# virtual methods
.method public final a(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 202
    iget-object v0, p0, Lcom/a/a/a/p;->d:Landroid/widget/ArrayAdapter;

    invoke-virtual {v0}, Landroid/widget/ArrayAdapter;->getCount()I

    move-result v0

    if-gt v0, p1, :cond_0

    .line 203
    const-string/jumbo v0, ""

    .line 206
    :goto_0
    return-object v0

    .line 205
    :cond_0
    iget-object v0, p0, Lcom/a/a/a/p;->d:Landroid/widget/ArrayAdapter;

    invoke-virtual {v0, p1}, Landroid/widget/ArrayAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 206
    invoke-virtual {p0, v0}, Lcom/a/a/a/p;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 211
    iget-object v0, p0, Lcom/a/a/a/p;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/a/a/Q;

    .line 212
    invoke-virtual {v0}, Lcom/a/a/a/Q;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 213
    invoke-virtual {v0}, Lcom/a/a/a/Q;->a()Ljava/lang/String;

    move-result-object v0

    .line 216
    :goto_0
    return-object v0

    :cond_1
    const-string/jumbo v0, ""

    goto :goto_0
.end method

.method public final a(Ljava/util/List;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 186
    iput-object p1, p0, Lcom/a/a/a/p;->e:Ljava/util/List;

    .line 187
    iget-object v0, p0, Lcom/a/a/a/p;->d:Landroid/widget/ArrayAdapter;

    invoke-virtual {v0}, Landroid/widget/ArrayAdapter;->clear()V

    .line 188
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/a/a/Q;

    .line 189
    iget-object v2, p0, Lcom/a/a/a/p;->d:Landroid/widget/ArrayAdapter;

    invoke-virtual {v0}, Lcom/a/a/a/Q;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    goto :goto_0

    .line 191
    :cond_0
    iget-object v0, p0, Lcom/a/a/a/p;->d:Landroid/widget/ArrayAdapter;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    invoke-static {v1}, Ljava/text/Collator;->getInstance(Ljava/util/Locale;)Ljava/text/Collator;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ArrayAdapter;->sort(Ljava/util/Comparator;)V

    .line 192
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_1

    .line 193
    iget-object v0, p0, Lcom/a/a/a/p;->a:Landroid/widget/Spinner;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setSelection(I)V

    .line 198
    :goto_1
    return-void

    .line 195
    :cond_1
    iget-object v0, p0, Lcom/a/a/a/p;->d:Landroid/widget/ArrayAdapter;

    invoke-virtual {v0, p2}, Landroid/widget/ArrayAdapter;->getPosition(Ljava/lang/Object;)I

    move-result v0

    .line 196
    iget-object v1, p0, Lcom/a/a/a/p;->a:Landroid/widget/Spinner;

    invoke-virtual {v1, v0}, Landroid/widget/Spinner;->setSelection(I)V

    goto :goto_1
.end method
