.class public final enum Lcom/a/a/a/e;
.super Ljava/lang/Enum;
.source "AddressField.java"


# static fields
.field public static final enum a:Lcom/a/a/a/e;

.field public static final enum b:Lcom/a/a/a/e;

.field public static final enum c:Lcom/a/a/a/e;

.field public static final enum d:Lcom/a/a/a/e;

.field public static final enum e:Lcom/a/a/a/e;

.field public static final enum f:Lcom/a/a/a/e;

.field public static final enum g:Lcom/a/a/a/e;

.field public static final enum h:Lcom/a/a/a/e;

.field public static final enum i:Lcom/a/a/a/e;

.field public static final enum j:Lcom/a/a/a/e;

.field public static final enum k:Lcom/a/a/a/e;

.field private static final l:Ljava/util/Map;

.field private static final synthetic n:[Lcom/a/a/a/e;


# instance fields
.field private final m:C


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v0, 0x0

    .line 27
    new-instance v1, Lcom/a/a/a/e;

    const-string/jumbo v2, "ADMIN_AREA"

    const/16 v3, 0x53

    invoke-direct {v1, v2, v0, v3}, Lcom/a/a/a/e;-><init>(Ljava/lang/String;IC)V

    sput-object v1, Lcom/a/a/a/e;->a:Lcom/a/a/a/e;

    .line 28
    new-instance v1, Lcom/a/a/a/e;

    const-string/jumbo v2, "LOCALITY"

    const/16 v3, 0x43

    invoke-direct {v1, v2, v5, v3}, Lcom/a/a/a/e;-><init>(Ljava/lang/String;IC)V

    sput-object v1, Lcom/a/a/a/e;->b:Lcom/a/a/a/e;

    .line 29
    new-instance v1, Lcom/a/a/a/e;

    const-string/jumbo v2, "RECIPIENT"

    const/16 v3, 0x4e

    invoke-direct {v1, v2, v6, v3}, Lcom/a/a/a/e;-><init>(Ljava/lang/String;IC)V

    sput-object v1, Lcom/a/a/a/e;->c:Lcom/a/a/a/e;

    .line 30
    new-instance v1, Lcom/a/a/a/e;

    const-string/jumbo v2, "ORGANIZATION"

    const/16 v3, 0x4f

    invoke-direct {v1, v2, v7, v3}, Lcom/a/a/a/e;-><init>(Ljava/lang/String;IC)V

    sput-object v1, Lcom/a/a/a/e;->d:Lcom/a/a/a/e;

    .line 32
    new-instance v1, Lcom/a/a/a/e;

    const-string/jumbo v2, "ADDRESS_LINE_1"

    const/16 v3, 0x31

    invoke-direct {v1, v2, v8, v3}, Lcom/a/a/a/e;-><init>(Ljava/lang/String;IC)V

    sput-object v1, Lcom/a/a/a/e;->e:Lcom/a/a/a/e;

    .line 34
    new-instance v1, Lcom/a/a/a/e;

    const-string/jumbo v2, "ADDRESS_LINE_2"

    const/4 v3, 0x5

    const/16 v4, 0x32

    invoke-direct {v1, v2, v3, v4}, Lcom/a/a/a/e;-><init>(Ljava/lang/String;IC)V

    sput-object v1, Lcom/a/a/a/e;->f:Lcom/a/a/a/e;

    .line 35
    new-instance v1, Lcom/a/a/a/e;

    const-string/jumbo v2, "DEPENDENT_LOCALITY"

    const/4 v3, 0x6

    const/16 v4, 0x44

    invoke-direct {v1, v2, v3, v4}, Lcom/a/a/a/e;-><init>(Ljava/lang/String;IC)V

    sput-object v1, Lcom/a/a/a/e;->g:Lcom/a/a/a/e;

    .line 36
    new-instance v1, Lcom/a/a/a/e;

    const-string/jumbo v2, "POSTAL_CODE"

    const/4 v3, 0x7

    const/16 v4, 0x5a

    invoke-direct {v1, v2, v3, v4}, Lcom/a/a/a/e;-><init>(Ljava/lang/String;IC)V

    sput-object v1, Lcom/a/a/a/e;->h:Lcom/a/a/a/e;

    .line 37
    new-instance v1, Lcom/a/a/a/e;

    const-string/jumbo v2, "SORTING_CODE"

    const/16 v3, 0x8

    const/16 v4, 0x58

    invoke-direct {v1, v2, v3, v4}, Lcom/a/a/a/e;-><init>(Ljava/lang/String;IC)V

    sput-object v1, Lcom/a/a/a/e;->i:Lcom/a/a/a/e;

    .line 38
    new-instance v1, Lcom/a/a/a/e;

    const-string/jumbo v2, "STREET_ADDRESS"

    const/16 v3, 0x9

    const/16 v4, 0x41

    invoke-direct {v1, v2, v3, v4}, Lcom/a/a/a/e;-><init>(Ljava/lang/String;IC)V

    sput-object v1, Lcom/a/a/a/e;->j:Lcom/a/a/a/e;

    .line 40
    new-instance v1, Lcom/a/a/a/e;

    const-string/jumbo v2, "COUNTRY"

    const/16 v3, 0xa

    const/16 v4, 0x52

    invoke-direct {v1, v2, v3, v4}, Lcom/a/a/a/e;-><init>(Ljava/lang/String;IC)V

    sput-object v1, Lcom/a/a/a/e;->k:Lcom/a/a/a/e;

    .line 26
    const/16 v1, 0xb

    new-array v1, v1, [Lcom/a/a/a/e;

    sget-object v2, Lcom/a/a/a/e;->a:Lcom/a/a/a/e;

    aput-object v2, v1, v0

    sget-object v2, Lcom/a/a/a/e;->b:Lcom/a/a/a/e;

    aput-object v2, v1, v5

    sget-object v2, Lcom/a/a/a/e;->c:Lcom/a/a/a/e;

    aput-object v2, v1, v6

    sget-object v2, Lcom/a/a/a/e;->d:Lcom/a/a/a/e;

    aput-object v2, v1, v7

    sget-object v2, Lcom/a/a/a/e;->e:Lcom/a/a/a/e;

    aput-object v2, v1, v8

    const/4 v2, 0x5

    sget-object v3, Lcom/a/a/a/e;->f:Lcom/a/a/a/e;

    aput-object v3, v1, v2

    const/4 v2, 0x6

    sget-object v3, Lcom/a/a/a/e;->g:Lcom/a/a/a/e;

    aput-object v3, v1, v2

    const/4 v2, 0x7

    sget-object v3, Lcom/a/a/a/e;->h:Lcom/a/a/a/e;

    aput-object v3, v1, v2

    const/16 v2, 0x8

    sget-object v3, Lcom/a/a/a/e;->i:Lcom/a/a/a/e;

    aput-object v3, v1, v2

    const/16 v2, 0x9

    sget-object v3, Lcom/a/a/a/e;->j:Lcom/a/a/a/e;

    aput-object v3, v1, v2

    const/16 v2, 0xa

    sget-object v3, Lcom/a/a/a/e;->k:Lcom/a/a/a/e;

    aput-object v3, v1, v2

    sput-object v1, Lcom/a/a/a/e;->n:[Lcom/a/a/a/e;

    .line 50
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    sput-object v1, Lcom/a/a/a/e;->l:Ljava/util/Map;

    .line 54
    invoke-static {}, Lcom/a/a/a/e;->values()[Lcom/a/a/a/e;

    move-result-object v1

    array-length v2, v1

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    .line 55
    sget-object v4, Lcom/a/a/a/e;->l:Ljava/util/Map;

    iget-char v5, v3, Lcom/a/a/a/e;->m:C

    invoke-static {v5}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v5

    invoke-interface {v4, v5, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 54
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 57
    :cond_0
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IC)V
    .locals 0

    .prologue
    .line 61
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 62
    iput-char p3, p0, Lcom/a/a/a/e;->m:C

    .line 63
    return-void
.end method

.method static a(C)Lcom/a/a/a/e;
    .locals 2

    .prologue
    .line 70
    sget-object v0, Lcom/a/a/a/e;->l:Ljava/util/Map;

    invoke-static {p0}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/a/a/e;

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/a/a/a/e;
    .locals 1

    .prologue
    .line 26
    const-class v0, Lcom/a/a/a/e;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/a/a/a/e;

    return-object v0
.end method

.method public static values()[Lcom/a/a/a/e;
    .locals 1

    .prologue
    .line 26
    sget-object v0, Lcom/a/a/a/e;->n:[Lcom/a/a/a/e;

    invoke-virtual {v0}, [Lcom/a/a/a/e;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/a/a/a/e;

    return-object v0
.end method


# virtual methods
.method final a()Lcom/a/a/a/f;
    .locals 1

    .prologue
    .line 84
    sget-object v0, Lcom/a/a/a/e;->h:Lcom/a/a/a/e;

    invoke-virtual {p0, v0}, Lcom/a/a/a/e;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/a/a/a/e;->i:Lcom/a/a/a/e;

    invoke-virtual {p0, v0}, Lcom/a/a/a/e;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/a/a/a/f;->b:Lcom/a/a/a/f;

    :goto_0
    return-object v0

    :cond_1
    sget-object v0, Lcom/a/a/a/f;->a:Lcom/a/a/a/f;

    goto :goto_0
.end method
