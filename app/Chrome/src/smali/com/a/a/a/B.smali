.class final Lcom/a/a/a/B;
.super Ljava/lang/Object;
.source "FormController.java"


# static fields
.field private static final a:Lcom/a/a/a/L;

.field private static final b:[Lcom/a/a/a/e;


# instance fields
.field private c:Ljava/lang/String;

.field private d:Lcom/a/a/a/x;

.field private e:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 38
    new-instance v0, Lcom/a/a/a/c;

    invoke-direct {v0}, Lcom/a/a/a/c;-><init>()V

    invoke-virtual {v0}, Lcom/a/a/a/c;->a()Lcom/a/a/a/a;

    move-result-object v0

    new-instance v1, Lcom/a/a/a/M;

    sget-object v2, Lcom/a/a/a/N;->a:Lcom/a/a/a/N;

    invoke-direct {v1, v2}, Lcom/a/a/a/M;-><init>(Lcom/a/a/a/N;)V

    invoke-virtual {v1, v0}, Lcom/a/a/a/M;->a(Lcom/a/a/a/a;)Lcom/a/a/a/M;

    move-result-object v0

    invoke-virtual {v0}, Lcom/a/a/a/M;->a()Lcom/a/a/a/L;

    move-result-object v0

    sput-object v0, Lcom/a/a/a/B;->a:Lcom/a/a/a/L;

    .line 40
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/a/a/a/e;

    const/4 v1, 0x0

    sget-object v2, Lcom/a/a/a/e;->k:Lcom/a/a/a/e;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, Lcom/a/a/a/e;->a:Lcom/a/a/a/e;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    sget-object v2, Lcom/a/a/a/e;->b:Lcom/a/a/a/e;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    sget-object v2, Lcom/a/a/a/e;->g:Lcom/a/a/a/e;

    aput-object v2, v0, v1

    sput-object v0, Lcom/a/a/a/B;->b:[Lcom/a/a/a/e;

    return-void
.end method

.method constructor <init>(Lcom/a/a/a/x;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 58
    const-string/jumbo v0, "null data not allowed"

    invoke-static {p1, v0}, Lcom/a/a/a/X;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 59
    iput-object p2, p0, Lcom/a/a/a/B;->c:Ljava/lang/String;

    .line 60
    iput-object p3, p0, Lcom/a/a/a/B;->e:Ljava/lang/String;

    .line 62
    new-instance v0, Lcom/a/a/a/c;

    invoke-direct {v0}, Lcom/a/a/a/c;-><init>()V

    const-string/jumbo v1, "ZZ"

    invoke-virtual {v0, v1}, Lcom/a/a/a/c;->a(Ljava/lang/String;)Lcom/a/a/a/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/a/a/a/c;->a()Lcom/a/a/a/a;

    move-result-object v0

    .line 63
    invoke-static {v0}, Lcom/a/a/a/B;->a(Lcom/a/a/a/a;)Lcom/a/a/a/L;

    move-result-object v0

    .line 65
    invoke-virtual {v0}, Lcom/a/a/a/L;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/a/a/a/x;->b(Ljava/lang/String;)Lcom/a/a/a/k;

    move-result-object v1

    .line 67
    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "require data for default country key: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/a/a/a/X;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 69
    iput-object p1, p0, Lcom/a/a/a/B;->d:Lcom/a/a/a/x;

    .line 70
    return-void
.end method

.method static synthetic a(Lcom/a/a/a/B;Lcom/a/a/a/L;Ljava/lang/String;)Lcom/a/a/a/L;
    .locals 4

    .prologue
    .line 31
    invoke-virtual {p1}, Lcom/a/a/a/L;->toString()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Lcom/a/a/a/B;->c:Ljava/lang/String;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Lcom/a/a/a/L;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v3, "/"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    array-length v2, v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_1

    if-eqz v0, :cond_1

    invoke-virtual {p0, v0}, Lcom/a/a/a/B;->c(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "--"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_1
    new-instance v1, Lcom/a/a/a/M;

    invoke-direct {v1, v0}, Lcom/a/a/a/M;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/a/a/a/M;->a()Lcom/a/a/a/L;

    move-result-object v0

    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/a/a/a/B;->c:Ljava/lang/String;

    invoke-static {v0}, Lcom/a/a/a/X;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    move-object v0, v1

    goto :goto_1
.end method

.method static a(Lcom/a/a/a/a;)Lcom/a/a/a/L;
    .locals 2

    .prologue
    .line 93
    new-instance v0, Lcom/a/a/a/M;

    sget-object v1, Lcom/a/a/a/N;->a:Lcom/a/a/a/N;

    invoke-direct {v0, v1}, Lcom/a/a/a/M;-><init>(Lcom/a/a/a/N;)V

    invoke-virtual {v0, p0}, Lcom/a/a/a/M;->a(Lcom/a/a/a/a;)Lcom/a/a/a/M;

    move-result-object v0

    invoke-virtual {v0}, Lcom/a/a/a/M;->a()Lcom/a/a/a/L;

    move-result-object v0

    return-object v0
.end method

.method private a(Lcom/a/a/a/L;Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 275
    invoke-virtual {p0, p1}, Lcom/a/a/a/B;->a(Lcom/a/a/a/L;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/a/a/Q;

    .line 276
    invoke-virtual {v0, p2}, Lcom/a/a/a/Q;->a(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 277
    invoke-virtual {v0}, Lcom/a/a/a/Q;->a()Ljava/lang/String;

    move-result-object v0

    .line 280
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic a(Lcom/a/a/a/B;Lcom/a/a/a/L;Ljava/util/Queue;Lcom/a/a/a/y;)V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0, p1, p2, p3}, Lcom/a/a/a/B;->a(Lcom/a/a/a/L;Ljava/util/Queue;Lcom/a/a/a/y;)V

    return-void
.end method

.method private a(Lcom/a/a/a/L;Ljava/util/Queue;Lcom/a/a/a/y;)V
    .locals 2

    .prologue
    .line 133
    const-string/jumbo v0, "Null key not allowed"

    invoke-static {p1, v0}, Lcom/a/a/a/X;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 134
    const-string/jumbo v0, "Null subkeys not allowed"

    invoke-static {p2, v0}, Lcom/a/a/a/X;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 136
    iget-object v0, p0, Lcom/a/a/a/B;->d:Lcom/a/a/a/x;

    new-instance v1, Lcom/a/a/a/C;

    invoke-direct {v1, p0, p1, p3, p2}, Lcom/a/a/a/C;-><init>(Lcom/a/a/a/B;Lcom/a/a/a/L;Lcom/a/a/a/y;Ljava/util/Queue;)V

    invoke-virtual {v0, p1, v1}, Lcom/a/a/a/x;->a(Lcom/a/a/a/L;Lcom/a/a/a/y;)V

    .line 169
    return-void
.end method

.method private b(Lcom/a/a/a/L;)Lcom/a/a/a/L;
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v1, 0x1

    .line 294
    invoke-static {p1}, Lcom/a/a/a/X;->a(Ljava/lang/Object;)V

    .line 295
    invoke-virtual {p1}, Lcom/a/a/a/L;->a()Lcom/a/a/a/N;

    move-result-object v0

    sget-object v2, Lcom/a/a/a/N;->a:Lcom/a/a/a/N;

    if-eq v0, v2, :cond_0

    .line 296
    new-instance v0, Ljava/lang/RuntimeException;

    const-string/jumbo v1, "Only DATA keyType is supported"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 299
    :cond_0
    invoke-virtual {p1}, Lcom/a/a/a/L;->toString()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v2, "/"

    invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 302
    array-length v0, v3

    const/4 v2, 0x2

    if-ge v0, v2, :cond_1

    .line 332
    :goto_0
    return-object p1

    .line 306
    :cond_1
    new-instance v4, Ljava/lang/StringBuilder;

    aget-object v0, v3, v7

    invoke-direct {v4, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move v0, v1

    .line 307
    :goto_1
    array-length v2, v3

    if-ge v0, v2, :cond_5

    .line 309
    const/4 v2, 0x0

    .line 310
    if-ne v0, v1, :cond_2

    aget-object v5, v3, v0

    const-string/jumbo v6, "--"

    invoke-virtual {v5, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 311
    aget-object v2, v3, v0

    const-string/jumbo v5, "--"

    invoke-virtual {v2, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 312
    aget-object v5, v2, v7

    aput-object v5, v3, v0

    .line 313
    aget-object v2, v2, v1

    .line 316
    :cond_2
    new-instance v5, Lcom/a/a/a/M;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Lcom/a/a/a/M;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5}, Lcom/a/a/a/M;->a()Lcom/a/a/a/L;

    move-result-object v5

    aget-object v6, v3, v0

    invoke-direct {p0, v5, v6}, Lcom/a/a/a/B;->a(Lcom/a/a/a/L;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 321
    if-nez v5, :cond_3

    .line 322
    :goto_2
    array-length v1, v3

    if-ge v0, v1, :cond_5

    .line 323
    const-string/jumbo v1, "/"

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    aget-object v2, v3, v0

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 322
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 327
    :cond_3
    const-string/jumbo v6, "/"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 328
    if-eqz v2, :cond_4

    .line 329
    const-string/jumbo v5, "--"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 307
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 332
    :cond_5
    new-instance v0, Lcom/a/a/a/M;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/a/a/a/M;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/a/a/a/M;->a()Lcom/a/a/a/L;

    move-result-object p1

    goto :goto_0
.end method

.method private static d(Ljava/lang/String;)[Ljava/lang/String;
    .locals 1

    .prologue
    .line 268
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_1

    .line 269
    :cond_0
    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/String;

    .line 271
    :goto_0
    return-object v0

    :cond_1
    const-string/jumbo v0, "~"

    invoke-virtual {p0, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method final a(Lcom/a/a/a/L;)Ljava/util/List;
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 218
    invoke-virtual {p1}, Lcom/a/a/a/L;->a()Lcom/a/a/a/N;

    move-result-object v1

    sget-object v2, Lcom/a/a/a/N;->b:Lcom/a/a/a/N;

    if-ne v1, v2, :cond_0

    .line 219
    new-instance v0, Ljava/lang/RuntimeException;

    const-string/jumbo v1, "example key not allowed for getting region data"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 221
    :cond_0
    const-string/jumbo v1, "null regionKey not allowed"

    invoke-static {p1, v1}, Lcom/a/a/a/X;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 222
    invoke-direct {p0, p1}, Lcom/a/a/a/B;->b(Lcom/a/a/a/L;)Lcom/a/a/a/L;

    move-result-object v1

    .line 223
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 226
    sget-object v2, Lcom/a/a/a/B;->a:Lcom/a/a/a/L;

    invoke-virtual {v1, v2}, Lcom/a/a/a/L;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 227
    iget-object v2, p0, Lcom/a/a/a/B;->d:Lcom/a/a/a/x;

    invoke-virtual {v1}, Lcom/a/a/a/L;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Lcom/a/a/a/x;->b(Ljava/lang/String;)Lcom/a/a/a/k;

    move-result-object v1

    .line 229
    sget-object v2, Lcom/a/a/a/d;->a:Lcom/a/a/a/d;

    invoke-virtual {v1, v2}, Lcom/a/a/a/k;->b(Lcom/a/a/a/d;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/a/a/a/B;->d(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 230
    :goto_0
    array-length v2, v1

    if-ge v0, v2, :cond_1

    .line 231
    new-instance v2, Lcom/a/a/a/R;

    invoke-direct {v2}, Lcom/a/a/a/R;-><init>()V

    aget-object v4, v1, v0

    invoke-virtual {v2, v4}, Lcom/a/a/a/R;->a(Ljava/lang/String;)Lcom/a/a/a/R;

    move-result-object v2

    aget-object v4, v1, v0

    invoke-virtual {v2, v4}, Lcom/a/a/a/R;->b(Ljava/lang/String;)Lcom/a/a/a/R;

    move-result-object v2

    invoke-virtual {v2}, Lcom/a/a/a/R;->a()Lcom/a/a/a/Q;

    move-result-object v2

    .line 235
    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 230
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    move-object v0, v3

    .line 257
    :goto_1
    return-object v0

    .line 240
    :cond_2
    iget-object v2, p0, Lcom/a/a/a/B;->d:Lcom/a/a/a/x;

    invoke-virtual {v1}, Lcom/a/a/a/L;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Lcom/a/a/a/x;->a(Ljava/lang/String;)Lcom/a/a/a/k;

    move-result-object v2

    .line 242
    if-eqz v2, :cond_6

    .line 243
    sget-object v1, Lcom/a/a/a/d;->i:Lcom/a/a/a/d;

    invoke-virtual {v2, v1}, Lcom/a/a/a/k;->b(Lcom/a/a/a/d;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/a/a/a/B;->d(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .line 244
    iget-object v1, p0, Lcom/a/a/a/B;->c:Ljava/lang/String;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/a/a/a/B;->c:Ljava/lang/String;

    invoke-static {v1}, Lcom/a/a/a/X;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    sget-object v1, Lcom/a/a/a/O;->a:Lcom/a/a/a/O;

    :goto_2
    sget-object v5, Lcom/a/a/a/O;->b:Lcom/a/a/a/O;

    if-ne v1, v5, :cond_4

    sget-object v1, Lcom/a/a/a/d;->k:Lcom/a/a/a/d;

    invoke-virtual {v2, v1}, Lcom/a/a/a/k;->b(Lcom/a/a/a/d;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/a/a/a/B;->d(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 248
    :goto_3
    array-length v2, v4

    if-ge v0, v2, :cond_6

    .line 249
    new-instance v2, Lcom/a/a/a/R;

    invoke-direct {v2}, Lcom/a/a/a/R;-><init>()V

    aget-object v5, v4, v0

    invoke-virtual {v2, v5}, Lcom/a/a/a/R;->a(Ljava/lang/String;)Lcom/a/a/a/R;

    move-result-object v5

    array-length v2, v1

    if-ge v0, v2, :cond_5

    aget-object v2, v1, v0

    :goto_4
    invoke-virtual {v5, v2}, Lcom/a/a/a/R;->b(Ljava/lang/String;)Lcom/a/a/a/R;

    move-result-object v2

    invoke-virtual {v2}, Lcom/a/a/a/R;->a()Lcom/a/a/a/Q;

    move-result-object v2

    .line 254
    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 248
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 244
    :cond_3
    sget-object v1, Lcom/a/a/a/O;->b:Lcom/a/a/a/O;

    goto :goto_2

    :cond_4
    sget-object v1, Lcom/a/a/a/d;->j:Lcom/a/a/a/d;

    invoke-virtual {v2, v1}, Lcom/a/a/a/k;->b(Lcom/a/a/a/d;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/a/a/a/B;->d(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    goto :goto_3

    .line 249
    :cond_5
    aget-object v2, v4, v0

    goto :goto_4

    :cond_6
    move-object v0, v3

    .line 257
    goto :goto_1
.end method

.method final a(Lcom/a/a/a/a;Lcom/a/a/a/y;)V
    .locals 5

    .prologue
    .line 109
    invoke-virtual {p1}, Lcom/a/a/a/a;->a()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "null country not allowed"

    invoke-static {v0, v1}, Lcom/a/a/a/X;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 112
    new-instance v1, Ljava/util/LinkedList;

    invoke-direct {v1}, Ljava/util/LinkedList;-><init>()V

    .line 114
    sget-object v2, Lcom/a/a/a/B;->b:[Lcom/a/a/a/e;

    array-length v3, v2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_0

    aget-object v4, v2, v0

    .line 115
    invoke-virtual {p1, v4}, Lcom/a/a/a/a;->a(Lcom/a/a/a/e;)Ljava/lang/String;

    move-result-object v4

    .line 116
    if-eqz v4, :cond_0

    .line 117
    invoke-interface {v1, v4}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    .line 114
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 121
    :cond_0
    invoke-interface {v1}, Ljava/util/Queue;->size()I

    move-result v0

    if-nez v0, :cond_1

    .line 122
    new-instance v0, Ljava/lang/RuntimeException;

    const-string/jumbo v1, "Need at least country level info"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 125
    :cond_1
    if-eqz p2, :cond_2

    .line 126
    invoke-interface {p2}, Lcom/a/a/a/y;->a()V

    .line 128
    :cond_2
    sget-object v0, Lcom/a/a/a/B;->a:Lcom/a/a/a/L;

    invoke-direct {p0, v0, v1, p2}, Lcom/a/a/a/B;->a(Lcom/a/a/a/L;Ljava/util/Queue;Lcom/a/a/a/y;)V

    .line 129
    return-void
.end method

.method final a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 73
    iput-object p1, p0, Lcom/a/a/a/B;->c:Ljava/lang/String;

    .line 74
    return-void
.end method

.method final b(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 77
    iput-object p1, p0, Lcom/a/a/a/B;->e:Ljava/lang/String;

    .line 78
    return-void
.end method

.method final c(Ljava/lang/String;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 190
    if-nez p1, :cond_1

    .line 204
    :cond_0
    :goto_0
    return v0

    .line 193
    :cond_1
    new-instance v1, Lcom/a/a/a/c;

    invoke-direct {v1}, Lcom/a/a/a/c;-><init>()V

    iget-object v2, p0, Lcom/a/a/a/B;->e:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/a/a/a/c;->a(Ljava/lang/String;)Lcom/a/a/a/c;

    move-result-object v1

    invoke-virtual {v1}, Lcom/a/a/a/c;->a()Lcom/a/a/a/a;

    move-result-object v1

    .line 194
    invoke-static {v1}, Lcom/a/a/a/B;->a(Lcom/a/a/a/a;)Lcom/a/a/a/L;

    move-result-object v1

    .line 195
    iget-object v2, p0, Lcom/a/a/a/B;->d:Lcom/a/a/a/x;

    invoke-virtual {v1}, Lcom/a/a/a/L;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Lcom/a/a/a/x;->b(Ljava/lang/String;)Lcom/a/a/a/k;

    move-result-object v1

    .line 197
    sget-object v2, Lcom/a/a/a/d;->d:Lcom/a/a/a/d;

    invoke-virtual {v1, v2}, Lcom/a/a/a/k;->b(Lcom/a/a/a/d;)Ljava/lang/String;

    move-result-object v1

    .line 200
    invoke-static {v1}, Lcom/a/a/a/X;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {p1}, Lcom/a/a/a/X;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {p1}, Lcom/a/a/a/X;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 202
    const/4 v0, 0x0

    goto :goto_0
.end method
