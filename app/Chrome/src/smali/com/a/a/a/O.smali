.class final enum Lcom/a/a/a/O;
.super Ljava/lang/Enum;
.source "LookupKey.java"


# static fields
.field public static final enum a:Lcom/a/a/a/O;

.field public static final enum b:Lcom/a/a/a/O;

.field private static final synthetic c:[Lcom/a/a/a/O;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 69
    new-instance v0, Lcom/a/a/a/O;

    const-string/jumbo v1, "LATIN"

    invoke-direct {v0, v1, v2}, Lcom/a/a/a/O;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/a/a/a/O;->a:Lcom/a/a/a/O;

    .line 78
    new-instance v0, Lcom/a/a/a/O;

    const-string/jumbo v1, "LOCAL"

    invoke-direct {v0, v1, v3}, Lcom/a/a/a/O;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/a/a/a/O;->b:Lcom/a/a/a/O;

    .line 63
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/a/a/a/O;

    sget-object v1, Lcom/a/a/a/O;->a:Lcom/a/a/a/O;

    aput-object v1, v0, v2

    sget-object v1, Lcom/a/a/a/O;->b:Lcom/a/a/a/O;

    aput-object v1, v0, v3

    sput-object v0, Lcom/a/a/a/O;->c:[Lcom/a/a/a/O;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 63
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/a/a/a/O;
    .locals 1

    .prologue
    .line 63
    const-class v0, Lcom/a/a/a/O;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/a/a/a/O;

    return-object v0
.end method

.method public static values()[Lcom/a/a/a/O;
    .locals 1

    .prologue
    .line 63
    sget-object v0, Lcom/a/a/a/O;->c:[Lcom/a/a/a/O;

    invoke-virtual {v0}, [Lcom/a/a/a/O;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/a/a/a/O;

    return-object v0
.end method
