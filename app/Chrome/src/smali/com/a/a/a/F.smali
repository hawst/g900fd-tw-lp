.class final Lcom/a/a/a/F;
.super Ljava/lang/Object;
.source "FormatInterpreter.java"


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Lcom/a/a/a/D;


# direct methods
.method constructor <init>(Lcom/a/a/a/D;)V
    .locals 2

    .prologue
    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    invoke-static {}, Lcom/a/a/a/S;->a()Ljava/util/Map;

    move-result-object v0

    const-string/jumbo v1, "null country name map not allowed"

    invoke-static {v0, v1}, Lcom/a/a/a/X;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 49
    invoke-static {p1}, Lcom/a/a/a/X;->a(Ljava/lang/Object;)V

    .line 50
    iput-object p1, p0, Lcom/a/a/a/F;->b:Lcom/a/a/a/D;

    .line 51
    const-string/jumbo v0, "ZZ"

    sget-object v1, Lcom/a/a/a/d;->b:Lcom/a/a/a/d;

    invoke-static {v0, v1}, Lcom/a/a/a/F;->a(Ljava/lang/String;Lcom/a/a/a/d;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/a/a/a/F;->a:Ljava/lang/String;

    .line 52
    iget-object v0, p0, Lcom/a/a/a/F;->a:Ljava/lang/String;

    const-string/jumbo v1, "null default format not allowed"

    invoke-static {v0, v1}, Lcom/a/a/a/X;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 53
    return-void
.end method

.method private static a(Ljava/lang/String;Lcom/a/a/a/d;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 266
    invoke-static {p0}, Lcom/a/a/a/X;->a(Ljava/lang/Object;)V

    .line 267
    invoke-static {}, Lcom/a/a/a/S;->a()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 268
    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "no json data for region code "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/a/a/a/X;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 271
    :try_start_0
    new-instance v1, Lorg/json/JSONObject;

    new-instance v2, Lorg/json/JSONTokener;

    invoke-direct {v2, v0}, Lorg/json/JSONTokener;-><init>(Ljava/lang/String;)V

    invoke-direct {v1, v2}, Lorg/json/JSONObject;-><init>(Lorg/json/JSONTokener;)V

    .line 272
    invoke-virtual {p1}, Lcom/a/a/a/d;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 274
    const/4 v0, 0x0

    .line 278
    :goto_0
    return-object v0

    .line 277
    :cond_0
    invoke-virtual {p1}, Lcom/a/a/a/d;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 280
    :catch_0
    move-exception v1

    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "Invalid json for region code "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ": "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method private a(Ljava/lang/String;Ljava/util/List;)V
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 100
    iget-object v0, p0, Lcom/a/a/a/F;->b:Lcom/a/a/a/D;

    invoke-virtual {v0, p1}, Lcom/a/a/a/D;->a(Ljava/lang/String;)[Lcom/a/a/a/e;

    move-result-object v0

    if-nez v0, :cond_1

    .line 136
    :cond_0
    return-void

    .line 105
    :cond_1
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    .line 107
    iget-object v0, p0, Lcom/a/a/a/F;->b:Lcom/a/a/a/D;

    invoke-virtual {v0, p1}, Lcom/a/a/a/D;->a(Ljava/lang/String;)[Lcom/a/a/a/e;

    move-result-object v4

    array-length v5, v4

    move v0, v2

    move v1, v2

    :goto_0
    if-ge v0, v5, :cond_2

    aget-object v6, v4, v0

    .line 108
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-interface {v3, v6, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 109
    add-int/lit8 v1, v1, 0x1

    .line 107
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 113
    :cond_2
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 114
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 116
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    move v1, v2

    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/a/a/e;

    .line 117
    invoke-interface {v3, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 118
    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 119
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 121
    :cond_3
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    .line 122
    goto :goto_1

    .line 125
    :cond_4
    new-instance v0, Lcom/a/a/a/G;

    invoke-direct {v0, p0, v3}, Lcom/a/a/a/G;-><init>(Lcom/a/a/a/F;Ljava/util/Map;)V

    invoke-static {v4, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 133
    :goto_2
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_0

    .line 134
    invoke-interface {v5, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-interface {v4, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    invoke-interface {p2, v0, v1}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 133
    add-int/lit8 v2, v2, 0x1

    goto :goto_2
.end method

.method private static b(Lcom/a/a/a/O;Ljava/lang/String;)Ljava/util/List;
    .locals 10

    .prologue
    const/4 v2, 0x0

    .line 223
    sget-object v0, Lcom/a/a/a/O;->b:Lcom/a/a/a/O;

    if-ne p0, v0, :cond_1

    sget-object v0, Lcom/a/a/a/d;->b:Lcom/a/a/a/d;

    invoke-static {p1, v0}, Lcom/a/a/a/F;->a(Ljava/lang/String;Lcom/a/a/a/d;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    if-nez v0, :cond_0

    const-string/jumbo v0, "ZZ"

    sget-object v1, Lcom/a/a/a/d;->b:Lcom/a/a/a/d;

    invoke-static {v0, v1}, Lcom/a/a/a/F;->a(Ljava/lang/String;Lcom/a/a/a/d;)Ljava/lang/String;

    move-result-object v0

    .line 224
    :cond_0
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 227
    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v5

    array-length v6, v5

    move v3, v2

    move v1, v2

    :goto_1
    if-ge v3, v6, :cond_5

    aget-char v7, v5, v3

    .line 228
    if-eqz v1, :cond_3

    .line 230
    const-string/jumbo v1, "%n"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string/jumbo v9, "%"

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v1, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 231
    const-string/jumbo v1, "%n"

    invoke-interface {v4, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move v1, v2

    .line 227
    :goto_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 223
    :cond_1
    sget-object v0, Lcom/a/a/a/d;->e:Lcom/a/a/a/d;

    invoke-static {p1, v0}, Lcom/a/a/a/F;->a(Ljava/lang/String;Lcom/a/a/a/d;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 233
    :cond_2
    invoke-static {v7}, Lcom/a/a/a/e;->a(C)Lcom/a/a/a/e;

    move-result-object v1

    new-instance v8, Ljava/lang/StringBuilder;

    const-string/jumbo v9, "Unrecognized character \'"

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string/jumbo v9, "\' in format pattern: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v1, v8}, Lcom/a/a/a/X;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 235
    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v8, "%"

    invoke-direct {v1, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v4, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move v1, v2

    goto :goto_2

    .line 237
    :cond_3
    const/16 v8, 0x25

    if-ne v7, v8, :cond_4

    .line 238
    const/4 v1, 0x1

    goto :goto_2

    .line 240
    :cond_4
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v4, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 243
    :cond_5
    return-object v4
.end method


# virtual methods
.method final a(Lcom/a/a/a/O;Ljava/lang/String;)Ljava/util/List;
    .locals 4

    .prologue
    .line 62
    invoke-static {p1}, Lcom/a/a/a/X;->a(Ljava/lang/Object;)V

    .line 63
    invoke-static {p2}, Lcom/a/a/a/X;->a(Ljava/lang/Object;)V

    .line 64
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 65
    invoke-static {p1, p2}, Lcom/a/a/a/F;->b(Lcom/a/a/a/O;Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 67
    const-string/jumbo v3, "%."

    invoke-virtual {v0, v3}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    const-string/jumbo v3, "%n"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 68
    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Ljava/lang/String;->charAt(I)C

    move-result v0

    invoke-static {v0}, Lcom/a/a/a/e;->a(C)Lcom/a/a/a/e;

    move-result-object v0

    .line 72
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 75
    :cond_1
    invoke-direct {p0, p2, v1}, Lcom/a/a/a/F;->a(Ljava/lang/String;Ljava/util/List;)V

    .line 78
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 79
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/a/a/e;

    .line 80
    sget-object v3, Lcom/a/a/a/e;->j:Lcom/a/a/a/e;

    if-ne v0, v3, :cond_2

    .line 81
    sget-object v0, Lcom/a/a/a/e;->e:Lcom/a/a/a/e;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 82
    sget-object v0, Lcom/a/a/a/e;->f:Lcom/a/a/a/e;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 84
    :cond_2
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 87
    :cond_3
    return-object v2
.end method
