.class public final Lcom/google/b/a/a/r;
.super Lcom/google/protobuf/nano/ExtendableMessageNano;
.source "NanoAndroidService.java"


# instance fields
.field public a:Ljava/lang/Integer;

.field public b:[B

.field public c:Lcom/google/b/a/a/I;

.field public d:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 738
    invoke-direct {p0}, Lcom/google/protobuf/nano/ExtendableMessageNano;-><init>()V

    .line 739
    iput-object v0, p0, Lcom/google/b/a/a/r;->a:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/google/b/a/a/r;->b:[B

    iput-object v0, p0, Lcom/google/b/a/a/r;->c:Lcom/google/b/a/a/I;

    iput-object v0, p0, Lcom/google/b/a/a/r;->d:Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/google/b/a/a/r;->unknownFieldData:Ljava/util/List;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/b/a/a/r;->cachedSize:I

    .line 740
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 772
    invoke-super {p0}, Lcom/google/protobuf/nano/ExtendableMessageNano;->computeSerializedSize()I

    move-result v0

    .line 773
    iget-object v1, p0, Lcom/google/b/a/a/r;->a:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    .line 774
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/b/a/a/r;->a:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->c(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 777
    :cond_0
    iget-object v1, p0, Lcom/google/b/a/a/r;->b:[B

    if-eqz v1, :cond_1

    .line 778
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/b/a/a/r;->b:[B

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(I[B)I

    move-result v1

    add-int/2addr v0, v1

    .line 781
    :cond_1
    iget-object v1, p0, Lcom/google/b/a/a/r;->c:Lcom/google/b/a/a/I;

    if-eqz v1, :cond_2

    .line 782
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/b/a/a/r;->c:Lcom/google/b/a/a/I;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 785
    :cond_2
    iget-object v1, p0, Lcom/google/b/a/a/r;->d:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    .line 786
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/b/a/a/r;->d:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 789
    :cond_3
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1

    .prologue
    .line 709
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/b/a/a/r;->storeUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->c()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/a/a/r;->a:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->f()[B

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/a/a/r;->b:[B

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lcom/google/b/a/a/r;->c:Lcom/google/b/a/a/I;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/b/a/a/I;

    invoke-direct {v0}, Lcom/google/b/a/a/I;-><init>()V

    iput-object v0, p0, Lcom/google/b/a/a/r;->c:Lcom/google/b/a/a/I;

    :cond_1
    iget-object v0, p0, Lcom/google/b/a/a/r;->c:Lcom/google/b/a/a/I;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->a(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/a/a/r;->d:Ljava/lang/Boolean;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2

    .prologue
    .line 755
    iget-object v0, p0, Lcom/google/b/a/a/r;->a:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 756
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/b/a/a/r;->a:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(II)V

    .line 758
    :cond_0
    iget-object v0, p0, Lcom/google/b/a/a/r;->b:[B

    if-eqz v0, :cond_1

    .line 759
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/b/a/a/r;->b:[B

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(I[B)V

    .line 761
    :cond_1
    iget-object v0, p0, Lcom/google/b/a/a/r;->c:Lcom/google/b/a/a/I;

    if-eqz v0, :cond_2

    .line 762
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/b/a/a/r;->c:Lcom/google/b/a/a/I;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/MessageNano;)V

    .line 764
    :cond_2
    iget-object v0, p0, Lcom/google/b/a/a/r;->d:Ljava/lang/Boolean;

    if-eqz v0, :cond_3

    .line 765
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/b/a/a/r;->d:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(IZ)V

    .line 767
    :cond_3
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/ExtendableMessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 768
    return-void
.end method
