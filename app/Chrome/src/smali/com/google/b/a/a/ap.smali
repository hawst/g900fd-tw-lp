.class public final Lcom/google/b/a/a/ap;
.super Lcom/google/protobuf/nano/ExtendableMessageNano;
.source "NanoJavaClient.java"


# instance fields
.field public a:[Lcom/google/b/a/a/T;

.field public b:Lcom/google/b/a/a/ad;

.field public c:[Lcom/google/b/a/a/Z;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 437
    invoke-direct {p0}, Lcom/google/protobuf/nano/ExtendableMessageNano;-><init>()V

    .line 438
    invoke-static {}, Lcom/google/b/a/a/T;->a()[Lcom/google/b/a/a/T;

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/a/a/ap;->a:[Lcom/google/b/a/a/T;

    iput-object v1, p0, Lcom/google/b/a/a/ap;->b:Lcom/google/b/a/a/ad;

    invoke-static {}, Lcom/google/b/a/a/Z;->a()[Lcom/google/b/a/a/Z;

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/a/a/ap;->c:[Lcom/google/b/a/a/Z;

    iput-object v1, p0, Lcom/google/b/a/a/ap;->unknownFieldData:Ljava/util/List;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/b/a/a/ap;->cachedSize:I

    .line 439
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 477
    invoke-super {p0}, Lcom/google/protobuf/nano/ExtendableMessageNano;->computeSerializedSize()I

    move-result v0

    .line 478
    iget-object v2, p0, Lcom/google/b/a/a/ap;->a:[Lcom/google/b/a/a/T;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/b/a/a/ap;->a:[Lcom/google/b/a/a/T;

    array-length v2, v2

    if-lez v2, :cond_2

    move v2, v0

    move v0, v1

    .line 479
    :goto_0
    iget-object v3, p0, Lcom/google/b/a/a/ap;->a:[Lcom/google/b/a/a/T;

    array-length v3, v3

    if-ge v0, v3, :cond_1

    .line 480
    iget-object v3, p0, Lcom/google/b/a/a/ap;->a:[Lcom/google/b/a/a/T;

    aget-object v3, v3, v0

    .line 481
    if-eqz v3, :cond_0

    .line 482
    const/4 v4, 0x1

    invoke-static {v4, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 479
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    move v0, v2

    .line 487
    :cond_2
    iget-object v2, p0, Lcom/google/b/a/a/ap;->b:Lcom/google/b/a/a/ad;

    if-eqz v2, :cond_3

    .line 488
    const/4 v2, 0x2

    iget-object v3, p0, Lcom/google/b/a/a/ap;->b:Lcom/google/b/a/a/ad;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v2

    add-int/2addr v0, v2

    .line 491
    :cond_3
    iget-object v2, p0, Lcom/google/b/a/a/ap;->c:[Lcom/google/b/a/a/Z;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/google/b/a/a/ap;->c:[Lcom/google/b/a/a/Z;

    array-length v2, v2

    if-lez v2, :cond_5

    .line 492
    :goto_1
    iget-object v2, p0, Lcom/google/b/a/a/ap;->c:[Lcom/google/b/a/a/Z;

    array-length v2, v2

    if-ge v1, v2, :cond_5

    .line 493
    iget-object v2, p0, Lcom/google/b/a/a/ap;->c:[Lcom/google/b/a/a/Z;

    aget-object v2, v2, v1

    .line 494
    if-eqz v2, :cond_4

    .line 495
    const/4 v3, 0x3

    invoke-static {v3, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v2

    add-int/2addr v0, v2

    .line 492
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 500
    :cond_5
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 411
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/b/a/a/ap;->storeUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->a(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/b/a/a/ap;->a:[Lcom/google/b/a/a/T;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/b/a/a/T;

    if-eqz v0, :cond_1

    iget-object v3, p0, Lcom/google/b/a/a/ap;->a:[Lcom/google/b/a/a/T;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    new-instance v3, Lcom/google/b/a/a/T;

    invoke-direct {v3}, Lcom/google/b/a/a/T;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->a(Lcom/google/protobuf/nano/MessageNano;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Lcom/google/b/a/a/ap;->a:[Lcom/google/b/a/a/T;

    array-length v0, v0

    goto :goto_1

    :cond_3
    new-instance v3, Lcom/google/b/a/a/T;

    invoke-direct {v3}, Lcom/google/b/a/a/T;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->a(Lcom/google/protobuf/nano/MessageNano;)V

    iput-object v2, p0, Lcom/google/b/a/a/ap;->a:[Lcom/google/b/a/a/T;

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lcom/google/b/a/a/ap;->b:Lcom/google/b/a/a/ad;

    if-nez v0, :cond_4

    new-instance v0, Lcom/google/b/a/a/ad;

    invoke-direct {v0}, Lcom/google/b/a/a/ad;-><init>()V

    iput-object v0, p0, Lcom/google/b/a/a/ap;->b:Lcom/google/b/a/a/ad;

    :cond_4
    iget-object v0, p0, Lcom/google/b/a/a/ap;->b:Lcom/google/b/a/a/ad;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->a(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    :sswitch_3
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->a(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/b/a/a/ap;->c:[Lcom/google/b/a/a/Z;

    if-nez v0, :cond_6

    move v0, v1

    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/b/a/a/Z;

    if-eqz v0, :cond_5

    iget-object v3, p0, Lcom/google/b/a/a/ap;->c:[Lcom/google/b/a/a/Z;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_5
    :goto_4
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_7

    new-instance v3, Lcom/google/b/a/a/Z;

    invoke-direct {v3}, Lcom/google/b/a/a/Z;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->a(Lcom/google/protobuf/nano/MessageNano;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_6
    iget-object v0, p0, Lcom/google/b/a/a/ap;->c:[Lcom/google/b/a/a/Z;

    array-length v0, v0

    goto :goto_3

    :cond_7
    new-instance v3, Lcom/google/b/a/a/Z;

    invoke-direct {v3}, Lcom/google/b/a/a/Z;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->a(Lcom/google/protobuf/nano/MessageNano;)V

    iput-object v2, p0, Lcom/google/b/a/a/ap;->c:[Lcom/google/b/a/a/Z;

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 453
    iget-object v0, p0, Lcom/google/b/a/a/ap;->a:[Lcom/google/b/a/a/T;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/b/a/a/ap;->a:[Lcom/google/b/a/a/T;

    array-length v0, v0

    if-lez v0, :cond_1

    move v0, v1

    .line 454
    :goto_0
    iget-object v2, p0, Lcom/google/b/a/a/ap;->a:[Lcom/google/b/a/a/T;

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 455
    iget-object v2, p0, Lcom/google/b/a/a/ap;->a:[Lcom/google/b/a/a/T;

    aget-object v2, v2, v0

    .line 456
    if-eqz v2, :cond_0

    .line 457
    const/4 v3, 0x1

    invoke-virtual {p1, v3, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/MessageNano;)V

    .line 454
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 461
    :cond_1
    iget-object v0, p0, Lcom/google/b/a/a/ap;->b:Lcom/google/b/a/a/ad;

    if-eqz v0, :cond_2

    .line 462
    const/4 v0, 0x2

    iget-object v2, p0, Lcom/google/b/a/a/ap;->b:Lcom/google/b/a/a/ad;

    invoke-virtual {p1, v0, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/MessageNano;)V

    .line 464
    :cond_2
    iget-object v0, p0, Lcom/google/b/a/a/ap;->c:[Lcom/google/b/a/a/Z;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/b/a/a/ap;->c:[Lcom/google/b/a/a/Z;

    array-length v0, v0

    if-lez v0, :cond_4

    .line 465
    :goto_1
    iget-object v0, p0, Lcom/google/b/a/a/ap;->c:[Lcom/google/b/a/a/Z;

    array-length v0, v0

    if-ge v1, v0, :cond_4

    .line 466
    iget-object v0, p0, Lcom/google/b/a/a/ap;->c:[Lcom/google/b/a/a/Z;

    aget-object v0, v0, v1

    .line 467
    if-eqz v0, :cond_3

    .line 468
    const/4 v2, 0x3

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/MessageNano;)V

    .line 465
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 472
    :cond_4
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/ExtendableMessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 473
    return-void
.end method
