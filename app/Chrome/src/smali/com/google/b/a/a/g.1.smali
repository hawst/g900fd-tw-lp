.class public final Lcom/google/b/a/a/g;
.super Lcom/google/protobuf/nano/ExtendableMessageNano;
.source "NanoAndroidService.java"


# instance fields
.field public a:Lcom/google/b/a/a/ak;

.field public b:[B


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1978
    invoke-direct {p0}, Lcom/google/protobuf/nano/ExtendableMessageNano;-><init>()V

    .line 1979
    iput-object v0, p0, Lcom/google/b/a/a/g;->a:Lcom/google/b/a/a/ak;

    iput-object v0, p0, Lcom/google/b/a/a/g;->b:[B

    iput-object v0, p0, Lcom/google/b/a/a/g;->unknownFieldData:Ljava/util/List;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/b/a/a/g;->cachedSize:I

    .line 1980
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 2004
    invoke-super {p0}, Lcom/google/protobuf/nano/ExtendableMessageNano;->computeSerializedSize()I

    move-result v0

    .line 2005
    iget-object v1, p0, Lcom/google/b/a/a/g;->a:Lcom/google/b/a/a/ak;

    if-eqz v1, :cond_0

    .line 2006
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/b/a/a/g;->a:Lcom/google/b/a/a/ak;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2009
    :cond_0
    iget-object v1, p0, Lcom/google/b/a/a/g;->b:[B

    if-eqz v1, :cond_1

    .line 2010
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/b/a/a/g;->b:[B

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(I[B)I

    move-result v1

    add-int/2addr v0, v1

    .line 2013
    :cond_1
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1

    .prologue
    .line 1955
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/b/a/a/g;->storeUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lcom/google/b/a/a/g;->a:Lcom/google/b/a/a/ak;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/b/a/a/ak;

    invoke-direct {v0}, Lcom/google/b/a/a/ak;-><init>()V

    iput-object v0, p0, Lcom/google/b/a/a/g;->a:Lcom/google/b/a/a/ak;

    :cond_1
    iget-object v0, p0, Lcom/google/b/a/a/g;->a:Lcom/google/b/a/a/ak;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->a(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->f()[B

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/a/a/g;->b:[B

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2

    .prologue
    .line 1993
    iget-object v0, p0, Lcom/google/b/a/a/g;->a:Lcom/google/b/a/a/ak;

    if-eqz v0, :cond_0

    .line 1994
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/b/a/a/g;->a:Lcom/google/b/a/a/ak;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/MessageNano;)V

    .line 1996
    :cond_0
    iget-object v0, p0, Lcom/google/b/a/a/g;->b:[B

    if-eqz v0, :cond_1

    .line 1997
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/b/a/a/g;->b:[B

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(I[B)V

    .line 1999
    :cond_1
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/ExtendableMessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 2000
    return-void
.end method
