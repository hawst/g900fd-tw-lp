.class public final Lcom/google/b/a/a/ae;
.super Lcom/google/protobuf/nano/ExtendableMessageNano;
.source "NanoClientProtocol.java"


# instance fields
.field public a:[Lcom/google/b/a/a/ac;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1650
    invoke-direct {p0}, Lcom/google/protobuf/nano/ExtendableMessageNano;-><init>()V

    .line 1651
    invoke-static {}, Lcom/google/b/a/a/ac;->a()[Lcom/google/b/a/a/ac;

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/a/a/ae;->a:[Lcom/google/b/a/a/ac;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/b/a/a/ae;->unknownFieldData:Ljava/util/List;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/b/a/a/ae;->cachedSize:I

    .line 1652
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 4

    .prologue
    .line 1677
    invoke-super {p0}, Lcom/google/protobuf/nano/ExtendableMessageNano;->computeSerializedSize()I

    move-result v1

    .line 1678
    iget-object v0, p0, Lcom/google/b/a/a/ae;->a:[Lcom/google/b/a/a/ac;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/b/a/a/ae;->a:[Lcom/google/b/a/a/ac;

    array-length v0, v0

    if-lez v0, :cond_1

    .line 1679
    const/4 v0, 0x0

    :goto_0
    iget-object v2, p0, Lcom/google/b/a/a/ae;->a:[Lcom/google/b/a/a/ac;

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 1680
    iget-object v2, p0, Lcom/google/b/a/a/ae;->a:[Lcom/google/b/a/a/ac;

    aget-object v2, v2, v0

    .line 1681
    if-eqz v2, :cond_0

    .line 1682
    const/4 v3, 0x1

    invoke-static {v3, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v2

    add-int/2addr v1, v2

    .line 1679
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1687
    :cond_1
    return v1
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1630
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/b/a/a/ae;->storeUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->a(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/b/a/a/ae;->a:[Lcom/google/b/a/a/ac;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/b/a/a/ac;

    if-eqz v0, :cond_1

    iget-object v3, p0, Lcom/google/b/a/a/ae;->a:[Lcom/google/b/a/a/ac;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    new-instance v3, Lcom/google/b/a/a/ac;

    invoke-direct {v3}, Lcom/google/b/a/a/ac;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->a(Lcom/google/protobuf/nano/MessageNano;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Lcom/google/b/a/a/ae;->a:[Lcom/google/b/a/a/ac;

    array-length v0, v0

    goto :goto_1

    :cond_3
    new-instance v3, Lcom/google/b/a/a/ac;

    invoke-direct {v3}, Lcom/google/b/a/a/ac;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->a(Lcom/google/protobuf/nano/MessageNano;)V

    iput-object v2, p0, Lcom/google/b/a/a/ae;->a:[Lcom/google/b/a/a/ac;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 3

    .prologue
    .line 1664
    iget-object v0, p0, Lcom/google/b/a/a/ae;->a:[Lcom/google/b/a/a/ac;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/b/a/a/ae;->a:[Lcom/google/b/a/a/ac;

    array-length v0, v0

    if-lez v0, :cond_1

    .line 1665
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/b/a/a/ae;->a:[Lcom/google/b/a/a/ac;

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 1666
    iget-object v1, p0, Lcom/google/b/a/a/ae;->a:[Lcom/google/b/a/a/ac;

    aget-object v1, v1, v0

    .line 1667
    if-eqz v1, :cond_0

    .line 1668
    const/4 v2, 0x1

    invoke-virtual {p1, v2, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/MessageNano;)V

    .line 1665
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1672
    :cond_1
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/ExtendableMessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 1673
    return-void
.end method
