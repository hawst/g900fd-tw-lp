.class public final Lcom/google/b/a/a/J;
.super Lcom/google/protobuf/nano/ExtendableMessageNano;
.source "NanoClientProtocol.java"


# instance fields
.field public a:Lcom/google/b/a/a/W;

.field public b:[B

.field public c:Lcom/google/b/a/a/ad;

.field public d:Ljava/lang/Long;

.field public e:Ljava/lang/Long;

.field public f:Ljava/lang/String;

.field public g:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1055
    invoke-direct {p0}, Lcom/google/protobuf/nano/ExtendableMessageNano;-><init>()V

    .line 1056
    iput-object v0, p0, Lcom/google/b/a/a/J;->a:Lcom/google/b/a/a/W;

    iput-object v0, p0, Lcom/google/b/a/a/J;->b:[B

    iput-object v0, p0, Lcom/google/b/a/a/J;->c:Lcom/google/b/a/a/ad;

    iput-object v0, p0, Lcom/google/b/a/a/J;->d:Ljava/lang/Long;

    iput-object v0, p0, Lcom/google/b/a/a/J;->e:Ljava/lang/Long;

    iput-object v0, p0, Lcom/google/b/a/a/J;->f:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/b/a/a/J;->g:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/google/b/a/a/J;->unknownFieldData:Ljava/util/List;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/b/a/a/J;->cachedSize:I

    .line 1057
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 4

    .prologue
    .line 1101
    invoke-super {p0}, Lcom/google/protobuf/nano/ExtendableMessageNano;->computeSerializedSize()I

    move-result v0

    .line 1102
    iget-object v1, p0, Lcom/google/b/a/a/J;->a:Lcom/google/b/a/a/W;

    if-eqz v1, :cond_0

    .line 1103
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/b/a/a/J;->a:Lcom/google/b/a/a/W;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1106
    :cond_0
    iget-object v1, p0, Lcom/google/b/a/a/J;->b:[B

    if-eqz v1, :cond_1

    .line 1107
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/b/a/a/J;->b:[B

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(I[B)I

    move-result v1

    add-int/2addr v0, v1

    .line 1110
    :cond_1
    iget-object v1, p0, Lcom/google/b/a/a/J;->c:Lcom/google/b/a/a/ad;

    if-eqz v1, :cond_2

    .line 1111
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/b/a/a/J;->c:Lcom/google/b/a/a/ad;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1114
    :cond_2
    iget-object v1, p0, Lcom/google/b/a/a/J;->d:Ljava/lang/Long;

    if-eqz v1, :cond_3

    .line 1115
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/b/a/a/J;->d:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 1118
    :cond_3
    iget-object v1, p0, Lcom/google/b/a/a/J;->e:Ljava/lang/Long;

    if-eqz v1, :cond_4

    .line 1119
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/b/a/a/J;->e:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 1122
    :cond_4
    iget-object v1, p0, Lcom/google/b/a/a/J;->f:Ljava/lang/String;

    if-eqz v1, :cond_5

    .line 1123
    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/b/a/a/J;->f:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1126
    :cond_5
    iget-object v1, p0, Lcom/google/b/a/a/J;->g:Ljava/lang/Integer;

    if-eqz v1, :cond_6

    .line 1127
    const/4 v1, 0x7

    iget-object v2, p0, Lcom/google/b/a/a/J;->g:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->c(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1130
    :cond_6
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 2

    .prologue
    .line 1017
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/b/a/a/J;->storeUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lcom/google/b/a/a/J;->a:Lcom/google/b/a/a/W;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/b/a/a/W;

    invoke-direct {v0}, Lcom/google/b/a/a/W;-><init>()V

    iput-object v0, p0, Lcom/google/b/a/a/J;->a:Lcom/google/b/a/a/W;

    :cond_1
    iget-object v0, p0, Lcom/google/b/a/a/J;->a:Lcom/google/b/a/a/W;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->a(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->f()[B

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/a/a/J;->b:[B

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lcom/google/b/a/a/J;->c:Lcom/google/b/a/a/ad;

    if-nez v0, :cond_2

    new-instance v0, Lcom/google/b/a/a/ad;

    invoke-direct {v0}, Lcom/google/b/a/a/ad;-><init>()V

    iput-object v0, p0, Lcom/google/b/a/a/J;->c:Lcom/google/b/a/a/ad;

    :cond_2
    iget-object v0, p0, Lcom/google/b/a/a/J;->c:Lcom/google/b/a/a/ad;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->a(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->b()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/a/a/J;->d:Ljava/lang/Long;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->b()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/a/a/J;->e:Ljava/lang/Long;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/a/a/J;->f:Ljava/lang/String;

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->c()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/a/a/J;->g:Ljava/lang/Integer;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x32 -> :sswitch_6
        0x38 -> :sswitch_7
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 4

    .prologue
    .line 1075
    iget-object v0, p0, Lcom/google/b/a/a/J;->a:Lcom/google/b/a/a/W;

    if-eqz v0, :cond_0

    .line 1076
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/b/a/a/J;->a:Lcom/google/b/a/a/W;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/MessageNano;)V

    .line 1078
    :cond_0
    iget-object v0, p0, Lcom/google/b/a/a/J;->b:[B

    if-eqz v0, :cond_1

    .line 1079
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/b/a/a/J;->b:[B

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(I[B)V

    .line 1081
    :cond_1
    iget-object v0, p0, Lcom/google/b/a/a/J;->c:Lcom/google/b/a/a/ad;

    if-eqz v0, :cond_2

    .line 1082
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/b/a/a/J;->c:Lcom/google/b/a/a/ad;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/MessageNano;)V

    .line 1084
    :cond_2
    iget-object v0, p0, Lcom/google/b/a/a/J;->d:Ljava/lang/Long;

    if-eqz v0, :cond_3

    .line 1085
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/b/a/a/J;->d:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(IJ)V

    .line 1087
    :cond_3
    iget-object v0, p0, Lcom/google/b/a/a/J;->e:Ljava/lang/Long;

    if-eqz v0, :cond_4

    .line 1088
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/b/a/a/J;->e:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(IJ)V

    .line 1090
    :cond_4
    iget-object v0, p0, Lcom/google/b/a/a/J;->f:Ljava/lang/String;

    if-eqz v0, :cond_5

    .line 1091
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/b/a/a/J;->f:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILjava/lang/String;)V

    .line 1093
    :cond_5
    iget-object v0, p0, Lcom/google/b/a/a/J;->g:Ljava/lang/Integer;

    if-eqz v0, :cond_6

    .line 1094
    const/4 v0, 0x7

    iget-object v1, p0, Lcom/google/b/a/a/J;->g:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(II)V

    .line 1096
    :cond_6
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/ExtendableMessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 1097
    return-void
.end method
