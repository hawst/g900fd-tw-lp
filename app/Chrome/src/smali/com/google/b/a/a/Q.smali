.class public final Lcom/google/b/a/a/Q;
.super Lcom/google/protobuf/nano/ExtendableMessageNano;
.source "NanoClientProtocol.java"


# instance fields
.field public a:Ljava/lang/Integer;

.field public b:[B

.field public c:Lcom/google/b/a/a/H;

.field public d:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1409
    invoke-direct {p0}, Lcom/google/protobuf/nano/ExtendableMessageNano;-><init>()V

    .line 1410
    iput-object v0, p0, Lcom/google/b/a/a/Q;->a:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/google/b/a/a/Q;->b:[B

    iput-object v0, p0, Lcom/google/b/a/a/Q;->c:Lcom/google/b/a/a/H;

    iput-object v0, p0, Lcom/google/b/a/a/Q;->d:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/google/b/a/a/Q;->unknownFieldData:Ljava/util/List;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/b/a/a/Q;->cachedSize:I

    .line 1411
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 1443
    invoke-super {p0}, Lcom/google/protobuf/nano/ExtendableMessageNano;->computeSerializedSize()I

    move-result v0

    .line 1444
    iget-object v1, p0, Lcom/google/b/a/a/Q;->a:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    .line 1445
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/b/a/a/Q;->a:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->c(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1448
    :cond_0
    iget-object v1, p0, Lcom/google/b/a/a/Q;->b:[B

    if-eqz v1, :cond_1

    .line 1449
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/b/a/a/Q;->b:[B

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(I[B)I

    move-result v1

    add-int/2addr v0, v1

    .line 1452
    :cond_1
    iget-object v1, p0, Lcom/google/b/a/a/Q;->c:Lcom/google/b/a/a/H;

    if-eqz v1, :cond_2

    .line 1453
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/b/a/a/Q;->c:Lcom/google/b/a/a/H;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1456
    :cond_2
    iget-object v1, p0, Lcom/google/b/a/a/Q;->d:Ljava/lang/Integer;

    if-eqz v1, :cond_3

    .line 1457
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/b/a/a/Q;->d:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->c(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1460
    :cond_3
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1

    .prologue
    .line 1376
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/b/a/a/Q;->storeUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->c()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/a/a/Q;->a:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->f()[B

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/a/a/Q;->b:[B

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lcom/google/b/a/a/Q;->c:Lcom/google/b/a/a/H;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/b/a/a/H;

    invoke-direct {v0}, Lcom/google/b/a/a/H;-><init>()V

    iput-object v0, p0, Lcom/google/b/a/a/Q;->c:Lcom/google/b/a/a/H;

    :cond_1
    iget-object v0, p0, Lcom/google/b/a/a/Q;->c:Lcom/google/b/a/a/H;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->a(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->c()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/a/a/Q;->d:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2

    .prologue
    .line 1426
    iget-object v0, p0, Lcom/google/b/a/a/Q;->a:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 1427
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/b/a/a/Q;->a:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(II)V

    .line 1429
    :cond_0
    iget-object v0, p0, Lcom/google/b/a/a/Q;->b:[B

    if-eqz v0, :cond_1

    .line 1430
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/b/a/a/Q;->b:[B

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(I[B)V

    .line 1432
    :cond_1
    iget-object v0, p0, Lcom/google/b/a/a/Q;->c:Lcom/google/b/a/a/H;

    if-eqz v0, :cond_2

    .line 1433
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/b/a/a/Q;->c:Lcom/google/b/a/a/H;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/MessageNano;)V

    .line 1435
    :cond_2
    iget-object v0, p0, Lcom/google/b/a/a/Q;->d:Ljava/lang/Integer;

    if-eqz v0, :cond_3

    .line 1436
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/b/a/a/Q;->d:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(II)V

    .line 1438
    :cond_3
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/ExtendableMessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 1439
    return-void
.end method
