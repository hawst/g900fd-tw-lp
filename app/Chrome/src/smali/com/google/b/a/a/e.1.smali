.class public final Lcom/google/b/a/a/e;
.super Lcom/google/protobuf/nano/ExtendableMessageNano;
.source "NanoAndroidListenerProtocol.java"


# instance fields
.field public a:Ljava/lang/Boolean;

.field public b:[Lcom/google/b/a/a/T;

.field public c:[B

.field public d:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 325
    invoke-direct {p0}, Lcom/google/protobuf/nano/ExtendableMessageNano;-><init>()V

    .line 326
    iput-object v1, p0, Lcom/google/b/a/a/e;->a:Ljava/lang/Boolean;

    invoke-static {}, Lcom/google/b/a/a/T;->a()[Lcom/google/b/a/a/T;

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/a/a/e;->b:[Lcom/google/b/a/a/T;

    iput-object v1, p0, Lcom/google/b/a/a/e;->c:[B

    iput-object v1, p0, Lcom/google/b/a/a/e;->d:Ljava/lang/Boolean;

    iput-object v1, p0, Lcom/google/b/a/a/e;->unknownFieldData:Ljava/util/List;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/b/a/a/e;->cachedSize:I

    .line 327
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 5

    .prologue
    .line 364
    invoke-super {p0}, Lcom/google/protobuf/nano/ExtendableMessageNano;->computeSerializedSize()I

    move-result v0

    .line 365
    iget-object v1, p0, Lcom/google/b/a/a/e;->a:Ljava/lang/Boolean;

    if-eqz v1, :cond_0

    .line 366
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/b/a/a/e;->a:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 369
    :cond_0
    iget-object v1, p0, Lcom/google/b/a/a/e;->b:[Lcom/google/b/a/a/T;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/google/b/a/a/e;->b:[Lcom/google/b/a/a/T;

    array-length v1, v1

    if-lez v1, :cond_3

    .line 370
    const/4 v1, 0x0

    move v4, v1

    move v1, v0

    move v0, v4

    :goto_0
    iget-object v2, p0, Lcom/google/b/a/a/e;->b:[Lcom/google/b/a/a/T;

    array-length v2, v2

    if-ge v0, v2, :cond_2

    .line 371
    iget-object v2, p0, Lcom/google/b/a/a/e;->b:[Lcom/google/b/a/a/T;

    aget-object v2, v2, v0

    .line 372
    if-eqz v2, :cond_1

    .line 373
    const/4 v3, 0x2

    invoke-static {v3, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v2

    add-int/2addr v1, v2

    .line 370
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    move v0, v1

    .line 378
    :cond_3
    iget-object v1, p0, Lcom/google/b/a/a/e;->c:[B

    if-eqz v1, :cond_4

    .line 379
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/b/a/a/e;->c:[B

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(I[B)I

    move-result v1

    add-int/2addr v0, v1

    .line 382
    :cond_4
    iget-object v1, p0, Lcom/google/b/a/a/e;->d:Ljava/lang/Boolean;

    if-eqz v1, :cond_5

    .line 383
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/b/a/a/e;->d:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 386
    :cond_5
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 296
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/b/a/a/e;->storeUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/a/a/e;->a:Ljava/lang/Boolean;

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->a(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/b/a/a/e;->b:[Lcom/google/b/a/a/T;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/b/a/a/T;

    if-eqz v0, :cond_1

    iget-object v3, p0, Lcom/google/b/a/a/e;->b:[Lcom/google/b/a/a/T;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    new-instance v3, Lcom/google/b/a/a/T;

    invoke-direct {v3}, Lcom/google/b/a/a/T;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->a(Lcom/google/protobuf/nano/MessageNano;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Lcom/google/b/a/a/e;->b:[Lcom/google/b/a/a/T;

    array-length v0, v0

    goto :goto_1

    :cond_3
    new-instance v3, Lcom/google/b/a/a/T;

    invoke-direct {v3}, Lcom/google/b/a/a/T;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->a(Lcom/google/protobuf/nano/MessageNano;)V

    iput-object v2, p0, Lcom/google/b/a/a/e;->b:[Lcom/google/b/a/a/T;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->f()[B

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/a/a/e;->c:[B

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/a/a/e;->d:Ljava/lang/Boolean;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 3

    .prologue
    .line 342
    iget-object v0, p0, Lcom/google/b/a/a/e;->a:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    .line 343
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/b/a/a/e;->a:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(IZ)V

    .line 345
    :cond_0
    iget-object v0, p0, Lcom/google/b/a/a/e;->b:[Lcom/google/b/a/a/T;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/b/a/a/e;->b:[Lcom/google/b/a/a/T;

    array-length v0, v0

    if-lez v0, :cond_2

    .line 346
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/b/a/a/e;->b:[Lcom/google/b/a/a/T;

    array-length v1, v1

    if-ge v0, v1, :cond_2

    .line 347
    iget-object v1, p0, Lcom/google/b/a/a/e;->b:[Lcom/google/b/a/a/T;

    aget-object v1, v1, v0

    .line 348
    if-eqz v1, :cond_1

    .line 349
    const/4 v2, 0x2

    invoke-virtual {p1, v2, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/MessageNano;)V

    .line 346
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 353
    :cond_2
    iget-object v0, p0, Lcom/google/b/a/a/e;->c:[B

    if-eqz v0, :cond_3

    .line 354
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/b/a/a/e;->c:[B

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(I[B)V

    .line 356
    :cond_3
    iget-object v0, p0, Lcom/google/b/a/a/e;->d:Ljava/lang/Boolean;

    if-eqz v0, :cond_4

    .line 357
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/b/a/a/e;->d:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(IZ)V

    .line 359
    :cond_4
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/ExtendableMessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 360
    return-void
.end method
