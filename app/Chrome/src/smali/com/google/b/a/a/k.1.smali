.class public final Lcom/google/b/a/a/k;
.super Lcom/google/protobuf/nano/ExtendableMessageNano;
.source "NanoAndroidService.java"


# instance fields
.field public a:Lcom/google/b/a/a/i;

.field public b:[B


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2338
    invoke-direct {p0}, Lcom/google/protobuf/nano/ExtendableMessageNano;-><init>()V

    .line 2339
    iput-object v0, p0, Lcom/google/b/a/a/k;->a:Lcom/google/b/a/a/i;

    iput-object v0, p0, Lcom/google/b/a/a/k;->b:[B

    iput-object v0, p0, Lcom/google/b/a/a/k;->unknownFieldData:Ljava/util/List;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/b/a/a/k;->cachedSize:I

    .line 2340
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 2364
    invoke-super {p0}, Lcom/google/protobuf/nano/ExtendableMessageNano;->computeSerializedSize()I

    move-result v0

    .line 2365
    iget-object v1, p0, Lcom/google/b/a/a/k;->a:Lcom/google/b/a/a/i;

    if-eqz v1, :cond_0

    .line 2366
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/b/a/a/k;->a:Lcom/google/b/a/a/i;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2369
    :cond_0
    iget-object v1, p0, Lcom/google/b/a/a/k;->b:[B

    if-eqz v1, :cond_1

    .line 2370
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/b/a/a/k;->b:[B

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(I[B)I

    move-result v1

    add-int/2addr v0, v1

    .line 2373
    :cond_1
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1

    .prologue
    .line 2315
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/b/a/a/k;->storeUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lcom/google/b/a/a/k;->a:Lcom/google/b/a/a/i;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/b/a/a/i;

    invoke-direct {v0}, Lcom/google/b/a/a/i;-><init>()V

    iput-object v0, p0, Lcom/google/b/a/a/k;->a:Lcom/google/b/a/a/i;

    :cond_1
    iget-object v0, p0, Lcom/google/b/a/a/k;->a:Lcom/google/b/a/a/i;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->a(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->f()[B

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/a/a/k;->b:[B

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2

    .prologue
    .line 2353
    iget-object v0, p0, Lcom/google/b/a/a/k;->a:Lcom/google/b/a/a/i;

    if-eqz v0, :cond_0

    .line 2354
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/b/a/a/k;->a:Lcom/google/b/a/a/i;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/MessageNano;)V

    .line 2356
    :cond_0
    iget-object v0, p0, Lcom/google/b/a/a/k;->b:[B

    if-eqz v0, :cond_1

    .line 2357
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/b/a/a/k;->b:[B

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(I[B)V

    .line 2359
    :cond_1
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/ExtendableMessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 2360
    return-void
.end method
