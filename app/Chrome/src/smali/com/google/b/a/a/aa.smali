.class public final Lcom/google/b/a/a/aa;
.super Lcom/google/protobuf/nano/ExtendableMessageNano;
.source "NanoClientProtocol.java"


# static fields
.field private static volatile c:[Lcom/google/b/a/a/aa;


# instance fields
.field public a:Lcom/google/b/a/a/Z;

.field public b:Lcom/google/b/a/a/ai;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2628
    invoke-direct {p0}, Lcom/google/protobuf/nano/ExtendableMessageNano;-><init>()V

    .line 2629
    iput-object v0, p0, Lcom/google/b/a/a/aa;->a:Lcom/google/b/a/a/Z;

    iput-object v0, p0, Lcom/google/b/a/a/aa;->b:Lcom/google/b/a/a/ai;

    iput-object v0, p0, Lcom/google/b/a/a/aa;->unknownFieldData:Ljava/util/List;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/b/a/a/aa;->cachedSize:I

    .line 2630
    return-void
.end method

.method public static a()[Lcom/google/b/a/a/aa;
    .locals 2

    .prologue
    .line 2611
    sget-object v0, Lcom/google/b/a/a/aa;->c:[Lcom/google/b/a/a/aa;

    if-nez v0, :cond_1

    .line 2612
    sget-object v1, Lcom/google/protobuf/nano/InternalNano;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 2614
    :try_start_0
    sget-object v0, Lcom/google/b/a/a/aa;->c:[Lcom/google/b/a/a/aa;

    if-nez v0, :cond_0

    .line 2615
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/b/a/a/aa;

    sput-object v0, Lcom/google/b/a/a/aa;->c:[Lcom/google/b/a/a/aa;

    .line 2617
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2619
    :cond_1
    sget-object v0, Lcom/google/b/a/a/aa;->c:[Lcom/google/b/a/a/aa;

    return-object v0

    .line 2617
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 2654
    invoke-super {p0}, Lcom/google/protobuf/nano/ExtendableMessageNano;->computeSerializedSize()I

    move-result v0

    .line 2655
    iget-object v1, p0, Lcom/google/b/a/a/aa;->a:Lcom/google/b/a/a/Z;

    if-eqz v1, :cond_0

    .line 2656
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/b/a/a/aa;->a:Lcom/google/b/a/a/Z;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2659
    :cond_0
    iget-object v1, p0, Lcom/google/b/a/a/aa;->b:Lcom/google/b/a/a/ai;

    if-eqz v1, :cond_1

    .line 2660
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/b/a/a/aa;->b:Lcom/google/b/a/a/ai;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2663
    :cond_1
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1

    .prologue
    .line 2605
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/b/a/a/aa;->storeUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lcom/google/b/a/a/aa;->a:Lcom/google/b/a/a/Z;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/b/a/a/Z;

    invoke-direct {v0}, Lcom/google/b/a/a/Z;-><init>()V

    iput-object v0, p0, Lcom/google/b/a/a/aa;->a:Lcom/google/b/a/a/Z;

    :cond_1
    iget-object v0, p0, Lcom/google/b/a/a/aa;->a:Lcom/google/b/a/a/Z;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->a(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lcom/google/b/a/a/aa;->b:Lcom/google/b/a/a/ai;

    if-nez v0, :cond_2

    new-instance v0, Lcom/google/b/a/a/ai;

    invoke-direct {v0}, Lcom/google/b/a/a/ai;-><init>()V

    iput-object v0, p0, Lcom/google/b/a/a/aa;->b:Lcom/google/b/a/a/ai;

    :cond_2
    iget-object v0, p0, Lcom/google/b/a/a/aa;->b:Lcom/google/b/a/a/ai;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->a(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2

    .prologue
    .line 2643
    iget-object v0, p0, Lcom/google/b/a/a/aa;->a:Lcom/google/b/a/a/Z;

    if-eqz v0, :cond_0

    .line 2644
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/b/a/a/aa;->a:Lcom/google/b/a/a/Z;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/MessageNano;)V

    .line 2646
    :cond_0
    iget-object v0, p0, Lcom/google/b/a/a/aa;->b:Lcom/google/b/a/a/ai;

    if-eqz v0, :cond_1

    .line 2647
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/b/a/a/aa;->b:Lcom/google/b/a/a/ai;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/MessageNano;)V

    .line 2649
    :cond_1
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/ExtendableMessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 2650
    return-void
.end method
