.class public final Lcom/google/b/a/a/ag;
.super Lcom/google/protobuf/nano/ExtendableMessageNano;
.source "NanoClientProtocol.java"


# instance fields
.field public a:Lcom/google/b/a/a/W;

.field public b:[B

.field public c:Lcom/google/b/a/a/ad;

.field public d:Ljava/lang/Long;

.field public e:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2187
    invoke-direct {p0}, Lcom/google/protobuf/nano/ExtendableMessageNano;-><init>()V

    .line 2188
    iput-object v0, p0, Lcom/google/b/a/a/ag;->a:Lcom/google/b/a/a/W;

    iput-object v0, p0, Lcom/google/b/a/a/ag;->b:[B

    iput-object v0, p0, Lcom/google/b/a/a/ag;->c:Lcom/google/b/a/a/ad;

    iput-object v0, p0, Lcom/google/b/a/a/ag;->d:Ljava/lang/Long;

    iput-object v0, p0, Lcom/google/b/a/a/ag;->e:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/b/a/a/ag;->unknownFieldData:Ljava/util/List;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/b/a/a/ag;->cachedSize:I

    .line 2189
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 4

    .prologue
    .line 2225
    invoke-super {p0}, Lcom/google/protobuf/nano/ExtendableMessageNano;->computeSerializedSize()I

    move-result v0

    .line 2226
    iget-object v1, p0, Lcom/google/b/a/a/ag;->a:Lcom/google/b/a/a/W;

    if-eqz v1, :cond_0

    .line 2227
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/b/a/a/ag;->a:Lcom/google/b/a/a/W;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2230
    :cond_0
    iget-object v1, p0, Lcom/google/b/a/a/ag;->b:[B

    if-eqz v1, :cond_1

    .line 2231
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/b/a/a/ag;->b:[B

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(I[B)I

    move-result v1

    add-int/2addr v0, v1

    .line 2234
    :cond_1
    iget-object v1, p0, Lcom/google/b/a/a/ag;->c:Lcom/google/b/a/a/ad;

    if-eqz v1, :cond_2

    .line 2235
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/b/a/a/ag;->c:Lcom/google/b/a/a/ad;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2238
    :cond_2
    iget-object v1, p0, Lcom/google/b/a/a/ag;->d:Ljava/lang/Long;

    if-eqz v1, :cond_3

    .line 2239
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/b/a/a/ag;->d:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 2242
    :cond_3
    iget-object v1, p0, Lcom/google/b/a/a/ag;->e:Ljava/lang/String;

    if-eqz v1, :cond_4

    .line 2243
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/b/a/a/ag;->e:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2246
    :cond_4
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 2

    .prologue
    .line 2155
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/b/a/a/ag;->storeUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lcom/google/b/a/a/ag;->a:Lcom/google/b/a/a/W;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/b/a/a/W;

    invoke-direct {v0}, Lcom/google/b/a/a/W;-><init>()V

    iput-object v0, p0, Lcom/google/b/a/a/ag;->a:Lcom/google/b/a/a/W;

    :cond_1
    iget-object v0, p0, Lcom/google/b/a/a/ag;->a:Lcom/google/b/a/a/W;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->a(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->f()[B

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/a/a/ag;->b:[B

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lcom/google/b/a/a/ag;->c:Lcom/google/b/a/a/ad;

    if-nez v0, :cond_2

    new-instance v0, Lcom/google/b/a/a/ad;

    invoke-direct {v0}, Lcom/google/b/a/a/ad;-><init>()V

    iput-object v0, p0, Lcom/google/b/a/a/ag;->c:Lcom/google/b/a/a/ad;

    :cond_2
    iget-object v0, p0, Lcom/google/b/a/a/ag;->c:Lcom/google/b/a/a/ad;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->a(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->b()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/a/a/ag;->d:Ljava/lang/Long;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/a/a/ag;->e:Ljava/lang/String;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 4

    .prologue
    .line 2205
    iget-object v0, p0, Lcom/google/b/a/a/ag;->a:Lcom/google/b/a/a/W;

    if-eqz v0, :cond_0

    .line 2206
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/b/a/a/ag;->a:Lcom/google/b/a/a/W;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/MessageNano;)V

    .line 2208
    :cond_0
    iget-object v0, p0, Lcom/google/b/a/a/ag;->b:[B

    if-eqz v0, :cond_1

    .line 2209
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/b/a/a/ag;->b:[B

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(I[B)V

    .line 2211
    :cond_1
    iget-object v0, p0, Lcom/google/b/a/a/ag;->c:Lcom/google/b/a/a/ad;

    if-eqz v0, :cond_2

    .line 2212
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/b/a/a/ag;->c:Lcom/google/b/a/a/ad;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/MessageNano;)V

    .line 2214
    :cond_2
    iget-object v0, p0, Lcom/google/b/a/a/ag;->d:Ljava/lang/Long;

    if-eqz v0, :cond_3

    .line 2215
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/b/a/a/ag;->d:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(IJ)V

    .line 2217
    :cond_3
    iget-object v0, p0, Lcom/google/b/a/a/ag;->e:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 2218
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/b/a/a/ag;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILjava/lang/String;)V

    .line 2220
    :cond_4
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/ExtendableMessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 2221
    return-void
.end method
