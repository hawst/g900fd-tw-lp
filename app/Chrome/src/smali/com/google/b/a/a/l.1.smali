.class public final Lcom/google/b/a/a/l;
.super Lcom/google/protobuf/nano/ExtendableMessageNano;
.source "NanoAndroidService.java"


# instance fields
.field public a:Ljava/lang/Long;

.field public b:Lcom/google/b/a/a/ak;

.field public c:Lcom/google/b/a/a/o;

.field public d:Lcom/google/b/a/a/p;

.field public e:Lcom/google/b/a/a/m;

.field public f:Lcom/google/b/a/a/n;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 396
    invoke-direct {p0}, Lcom/google/protobuf/nano/ExtendableMessageNano;-><init>()V

    .line 397
    iput-object v0, p0, Lcom/google/b/a/a/l;->a:Ljava/lang/Long;

    iput-object v0, p0, Lcom/google/b/a/a/l;->b:Lcom/google/b/a/a/ak;

    iput-object v0, p0, Lcom/google/b/a/a/l;->c:Lcom/google/b/a/a/o;

    iput-object v0, p0, Lcom/google/b/a/a/l;->d:Lcom/google/b/a/a/p;

    iput-object v0, p0, Lcom/google/b/a/a/l;->e:Lcom/google/b/a/a/m;

    iput-object v0, p0, Lcom/google/b/a/a/l;->f:Lcom/google/b/a/a/n;

    iput-object v0, p0, Lcom/google/b/a/a/l;->unknownFieldData:Ljava/util/List;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/b/a/a/l;->cachedSize:I

    .line 398
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 4

    .prologue
    .line 438
    invoke-super {p0}, Lcom/google/protobuf/nano/ExtendableMessageNano;->computeSerializedSize()I

    move-result v0

    .line 439
    iget-object v1, p0, Lcom/google/b/a/a/l;->a:Ljava/lang/Long;

    if-eqz v1, :cond_0

    .line 440
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/b/a/a/l;->a:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 443
    :cond_0
    iget-object v1, p0, Lcom/google/b/a/a/l;->b:Lcom/google/b/a/a/ak;

    if-eqz v1, :cond_1

    .line 444
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/b/a/a/l;->b:Lcom/google/b/a/a/ak;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 447
    :cond_1
    iget-object v1, p0, Lcom/google/b/a/a/l;->c:Lcom/google/b/a/a/o;

    if-eqz v1, :cond_2

    .line 448
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/b/a/a/l;->c:Lcom/google/b/a/a/o;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 451
    :cond_2
    iget-object v1, p0, Lcom/google/b/a/a/l;->d:Lcom/google/b/a/a/p;

    if-eqz v1, :cond_3

    .line 452
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/b/a/a/l;->d:Lcom/google/b/a/a/p;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 455
    :cond_3
    iget-object v1, p0, Lcom/google/b/a/a/l;->e:Lcom/google/b/a/a/m;

    if-eqz v1, :cond_4

    .line 456
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/b/a/a/l;->e:Lcom/google/b/a/a/m;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 459
    :cond_4
    iget-object v1, p0, Lcom/google/b/a/a/l;->f:Lcom/google/b/a/a/n;

    if-eqz v1, :cond_5

    .line 460
    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/b/a/a/l;->f:Lcom/google/b/a/a/n;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 463
    :cond_5
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 2

    .prologue
    .line 8
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/b/a/a/l;->storeUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->b()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/a/a/l;->a:Ljava/lang/Long;

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lcom/google/b/a/a/l;->b:Lcom/google/b/a/a/ak;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/b/a/a/ak;

    invoke-direct {v0}, Lcom/google/b/a/a/ak;-><init>()V

    iput-object v0, p0, Lcom/google/b/a/a/l;->b:Lcom/google/b/a/a/ak;

    :cond_1
    iget-object v0, p0, Lcom/google/b/a/a/l;->b:Lcom/google/b/a/a/ak;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->a(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lcom/google/b/a/a/l;->c:Lcom/google/b/a/a/o;

    if-nez v0, :cond_2

    new-instance v0, Lcom/google/b/a/a/o;

    invoke-direct {v0}, Lcom/google/b/a/a/o;-><init>()V

    iput-object v0, p0, Lcom/google/b/a/a/l;->c:Lcom/google/b/a/a/o;

    :cond_2
    iget-object v0, p0, Lcom/google/b/a/a/l;->c:Lcom/google/b/a/a/o;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->a(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Lcom/google/b/a/a/l;->d:Lcom/google/b/a/a/p;

    if-nez v0, :cond_3

    new-instance v0, Lcom/google/b/a/a/p;

    invoke-direct {v0}, Lcom/google/b/a/a/p;-><init>()V

    iput-object v0, p0, Lcom/google/b/a/a/l;->d:Lcom/google/b/a/a/p;

    :cond_3
    iget-object v0, p0, Lcom/google/b/a/a/l;->d:Lcom/google/b/a/a/p;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->a(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    :sswitch_5
    iget-object v0, p0, Lcom/google/b/a/a/l;->e:Lcom/google/b/a/a/m;

    if-nez v0, :cond_4

    new-instance v0, Lcom/google/b/a/a/m;

    invoke-direct {v0}, Lcom/google/b/a/a/m;-><init>()V

    iput-object v0, p0, Lcom/google/b/a/a/l;->e:Lcom/google/b/a/a/m;

    :cond_4
    iget-object v0, p0, Lcom/google/b/a/a/l;->e:Lcom/google/b/a/a/m;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->a(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    :sswitch_6
    iget-object v0, p0, Lcom/google/b/a/a/l;->f:Lcom/google/b/a/a/n;

    if-nez v0, :cond_5

    new-instance v0, Lcom/google/b/a/a/n;

    invoke-direct {v0}, Lcom/google/b/a/a/n;-><init>()V

    iput-object v0, p0, Lcom/google/b/a/a/l;->f:Lcom/google/b/a/a/n;

    :cond_5
    iget-object v0, p0, Lcom/google/b/a/a/l;->f:Lcom/google/b/a/a/n;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->a(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 4

    .prologue
    .line 415
    iget-object v0, p0, Lcom/google/b/a/a/l;->a:Ljava/lang/Long;

    if-eqz v0, :cond_0

    .line 416
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/b/a/a/l;->a:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(IJ)V

    .line 418
    :cond_0
    iget-object v0, p0, Lcom/google/b/a/a/l;->b:Lcom/google/b/a/a/ak;

    if-eqz v0, :cond_1

    .line 419
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/b/a/a/l;->b:Lcom/google/b/a/a/ak;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/MessageNano;)V

    .line 421
    :cond_1
    iget-object v0, p0, Lcom/google/b/a/a/l;->c:Lcom/google/b/a/a/o;

    if-eqz v0, :cond_2

    .line 422
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/b/a/a/l;->c:Lcom/google/b/a/a/o;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/MessageNano;)V

    .line 424
    :cond_2
    iget-object v0, p0, Lcom/google/b/a/a/l;->d:Lcom/google/b/a/a/p;

    if-eqz v0, :cond_3

    .line 425
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/b/a/a/l;->d:Lcom/google/b/a/a/p;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/MessageNano;)V

    .line 427
    :cond_3
    iget-object v0, p0, Lcom/google/b/a/a/l;->e:Lcom/google/b/a/a/m;

    if-eqz v0, :cond_4

    .line 428
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/b/a/a/l;->e:Lcom/google/b/a/a/m;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/MessageNano;)V

    .line 430
    :cond_4
    iget-object v0, p0, Lcom/google/b/a/a/l;->f:Lcom/google/b/a/a/n;

    if-eqz v0, :cond_5

    .line 431
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/b/a/a/l;->f:Lcom/google/b/a/a/n;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/MessageNano;)V

    .line 433
    :cond_5
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/ExtendableMessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 434
    return-void
.end method
