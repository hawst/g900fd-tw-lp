.class public final Lcom/google/b/a/a/I;
.super Lcom/google/protobuf/nano/ExtendableMessageNano;
.source "NanoClientProtocol.java"


# instance fields
.field public a:Lcom/google/b/a/a/ak;

.field public b:Ljava/lang/Integer;

.field public c:Ljava/lang/Integer;

.field public d:Ljava/lang/Integer;

.field public e:Ljava/lang/Integer;

.field public f:Ljava/lang/Integer;

.field public g:Ljava/lang/Integer;

.field public h:Ljava/lang/Boolean;

.field public i:Ljava/lang/Integer;

.field public j:Lcom/google/b/a/a/V;

.field public k:Ljava/lang/Boolean;

.field public l:Ljava/lang/Integer;

.field public m:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 3429
    invoke-direct {p0}, Lcom/google/protobuf/nano/ExtendableMessageNano;-><init>()V

    .line 3430
    iput-object v0, p0, Lcom/google/b/a/a/I;->a:Lcom/google/b/a/a/ak;

    iput-object v0, p0, Lcom/google/b/a/a/I;->b:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/google/b/a/a/I;->c:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/google/b/a/a/I;->d:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/google/b/a/a/I;->e:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/google/b/a/a/I;->f:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/google/b/a/a/I;->g:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/google/b/a/a/I;->h:Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/google/b/a/a/I;->i:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/google/b/a/a/I;->j:Lcom/google/b/a/a/V;

    iput-object v0, p0, Lcom/google/b/a/a/I;->k:Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/google/b/a/a/I;->l:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/google/b/a/a/I;->m:Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/google/b/a/a/I;->unknownFieldData:Ljava/util/List;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/b/a/a/I;->cachedSize:I

    .line 3431
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 3499
    invoke-super {p0}, Lcom/google/protobuf/nano/ExtendableMessageNano;->computeSerializedSize()I

    move-result v0

    .line 3500
    iget-object v1, p0, Lcom/google/b/a/a/I;->a:Lcom/google/b/a/a/ak;

    if-eqz v1, :cond_0

    .line 3501
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/b/a/a/I;->a:Lcom/google/b/a/a/ak;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3504
    :cond_0
    iget-object v1, p0, Lcom/google/b/a/a/I;->b:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    .line 3505
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/b/a/a/I;->b:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->c(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 3508
    :cond_1
    iget-object v1, p0, Lcom/google/b/a/a/I;->c:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    .line 3509
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/b/a/a/I;->c:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->c(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 3512
    :cond_2
    iget-object v1, p0, Lcom/google/b/a/a/I;->d:Ljava/lang/Integer;

    if-eqz v1, :cond_3

    .line 3513
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/b/a/a/I;->d:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->c(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 3516
    :cond_3
    iget-object v1, p0, Lcom/google/b/a/a/I;->e:Ljava/lang/Integer;

    if-eqz v1, :cond_4

    .line 3517
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/b/a/a/I;->e:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->c(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 3520
    :cond_4
    iget-object v1, p0, Lcom/google/b/a/a/I;->f:Ljava/lang/Integer;

    if-eqz v1, :cond_5

    .line 3521
    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/b/a/a/I;->f:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->c(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 3524
    :cond_5
    iget-object v1, p0, Lcom/google/b/a/a/I;->g:Ljava/lang/Integer;

    if-eqz v1, :cond_6

    .line 3525
    const/4 v1, 0x7

    iget-object v2, p0, Lcom/google/b/a/a/I;->g:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->c(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 3528
    :cond_6
    iget-object v1, p0, Lcom/google/b/a/a/I;->h:Ljava/lang/Boolean;

    if-eqz v1, :cond_7

    .line 3529
    const/16 v1, 0x8

    iget-object v2, p0, Lcom/google/b/a/a/I;->h:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 3532
    :cond_7
    iget-object v1, p0, Lcom/google/b/a/a/I;->i:Ljava/lang/Integer;

    if-eqz v1, :cond_8

    .line 3533
    const/16 v1, 0x9

    iget-object v2, p0, Lcom/google/b/a/a/I;->i:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->c(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 3536
    :cond_8
    iget-object v1, p0, Lcom/google/b/a/a/I;->j:Lcom/google/b/a/a/V;

    if-eqz v1, :cond_9

    .line 3537
    const/16 v1, 0xa

    iget-object v2, p0, Lcom/google/b/a/a/I;->j:Lcom/google/b/a/a/V;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3540
    :cond_9
    iget-object v1, p0, Lcom/google/b/a/a/I;->k:Ljava/lang/Boolean;

    if-eqz v1, :cond_a

    .line 3541
    const/16 v1, 0xb

    iget-object v2, p0, Lcom/google/b/a/a/I;->k:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 3544
    :cond_a
    iget-object v1, p0, Lcom/google/b/a/a/I;->l:Ljava/lang/Integer;

    if-eqz v1, :cond_b

    .line 3545
    const/16 v1, 0xc

    iget-object v2, p0, Lcom/google/b/a/a/I;->l:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->c(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 3548
    :cond_b
    iget-object v1, p0, Lcom/google/b/a/a/I;->m:Ljava/lang/Boolean;

    if-eqz v1, :cond_c

    .line 3549
    const/16 v1, 0xd

    iget-object v2, p0, Lcom/google/b/a/a/I;->m:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 3552
    :cond_c
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1

    .prologue
    .line 3373
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/b/a/a/I;->storeUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lcom/google/b/a/a/I;->a:Lcom/google/b/a/a/ak;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/b/a/a/ak;

    invoke-direct {v0}, Lcom/google/b/a/a/ak;-><init>()V

    iput-object v0, p0, Lcom/google/b/a/a/I;->a:Lcom/google/b/a/a/ak;

    :cond_1
    iget-object v0, p0, Lcom/google/b/a/a/I;->a:Lcom/google/b/a/a/ak;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->a(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->c()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/a/a/I;->b:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->c()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/a/a/I;->c:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->c()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/a/a/I;->d:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->c()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/a/a/I;->e:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->c()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/a/a/I;->f:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->c()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/a/a/I;->g:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/a/a/I;->h:Ljava/lang/Boolean;

    goto :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->c()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/a/a/I;->i:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_a
    iget-object v0, p0, Lcom/google/b/a/a/I;->j:Lcom/google/b/a/a/V;

    if-nez v0, :cond_2

    new-instance v0, Lcom/google/b/a/a/V;

    invoke-direct {v0}, Lcom/google/b/a/a/V;-><init>()V

    iput-object v0, p0, Lcom/google/b/a/a/I;->j:Lcom/google/b/a/a/V;

    :cond_2
    iget-object v0, p0, Lcom/google/b/a/a/I;->j:Lcom/google/b/a/a/V;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->a(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    :sswitch_b
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/a/a/I;->k:Ljava/lang/Boolean;

    goto/16 :goto_0

    :sswitch_c
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->c()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/a/a/I;->l:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_d
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/a/a/I;->m:Ljava/lang/Boolean;

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
        0x38 -> :sswitch_7
        0x40 -> :sswitch_8
        0x48 -> :sswitch_9
        0x52 -> :sswitch_a
        0x58 -> :sswitch_b
        0x60 -> :sswitch_c
        0x68 -> :sswitch_d
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2

    .prologue
    .line 3455
    iget-object v0, p0, Lcom/google/b/a/a/I;->a:Lcom/google/b/a/a/ak;

    if-eqz v0, :cond_0

    .line 3456
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/b/a/a/I;->a:Lcom/google/b/a/a/ak;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/MessageNano;)V

    .line 3458
    :cond_0
    iget-object v0, p0, Lcom/google/b/a/a/I;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 3459
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/b/a/a/I;->b:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(II)V

    .line 3461
    :cond_1
    iget-object v0, p0, Lcom/google/b/a/a/I;->c:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    .line 3462
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/b/a/a/I;->c:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(II)V

    .line 3464
    :cond_2
    iget-object v0, p0, Lcom/google/b/a/a/I;->d:Ljava/lang/Integer;

    if-eqz v0, :cond_3

    .line 3465
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/b/a/a/I;->d:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(II)V

    .line 3467
    :cond_3
    iget-object v0, p0, Lcom/google/b/a/a/I;->e:Ljava/lang/Integer;

    if-eqz v0, :cond_4

    .line 3468
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/b/a/a/I;->e:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(II)V

    .line 3470
    :cond_4
    iget-object v0, p0, Lcom/google/b/a/a/I;->f:Ljava/lang/Integer;

    if-eqz v0, :cond_5

    .line 3471
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/b/a/a/I;->f:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(II)V

    .line 3473
    :cond_5
    iget-object v0, p0, Lcom/google/b/a/a/I;->g:Ljava/lang/Integer;

    if-eqz v0, :cond_6

    .line 3474
    const/4 v0, 0x7

    iget-object v1, p0, Lcom/google/b/a/a/I;->g:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(II)V

    .line 3476
    :cond_6
    iget-object v0, p0, Lcom/google/b/a/a/I;->h:Ljava/lang/Boolean;

    if-eqz v0, :cond_7

    .line 3477
    const/16 v0, 0x8

    iget-object v1, p0, Lcom/google/b/a/a/I;->h:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(IZ)V

    .line 3479
    :cond_7
    iget-object v0, p0, Lcom/google/b/a/a/I;->i:Ljava/lang/Integer;

    if-eqz v0, :cond_8

    .line 3480
    const/16 v0, 0x9

    iget-object v1, p0, Lcom/google/b/a/a/I;->i:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(II)V

    .line 3482
    :cond_8
    iget-object v0, p0, Lcom/google/b/a/a/I;->j:Lcom/google/b/a/a/V;

    if-eqz v0, :cond_9

    .line 3483
    const/16 v0, 0xa

    iget-object v1, p0, Lcom/google/b/a/a/I;->j:Lcom/google/b/a/a/V;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/MessageNano;)V

    .line 3485
    :cond_9
    iget-object v0, p0, Lcom/google/b/a/a/I;->k:Ljava/lang/Boolean;

    if-eqz v0, :cond_a

    .line 3486
    const/16 v0, 0xb

    iget-object v1, p0, Lcom/google/b/a/a/I;->k:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(IZ)V

    .line 3488
    :cond_a
    iget-object v0, p0, Lcom/google/b/a/a/I;->l:Ljava/lang/Integer;

    if-eqz v0, :cond_b

    .line 3489
    const/16 v0, 0xc

    iget-object v1, p0, Lcom/google/b/a/a/I;->l:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(II)V

    .line 3491
    :cond_b
    iget-object v0, p0, Lcom/google/b/a/a/I;->m:Ljava/lang/Boolean;

    if-eqz v0, :cond_c

    .line 3492
    const/16 v0, 0xd

    iget-object v1, p0, Lcom/google/b/a/a/I;->m:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(IZ)V

    .line 3494
    :cond_c
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/ExtendableMessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 3495
    return-void
.end method
