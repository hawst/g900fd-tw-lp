.class public final Lcom/google/b/a/a/K;
.super Lcom/google/protobuf/nano/ExtendableMessageNano;
.source "NanoClientProtocol.java"


# instance fields
.field public a:Lcom/google/b/a/a/J;

.field public b:Lcom/google/b/a/a/Q;

.field public c:Lcom/google/b/a/a/Y;

.field public d:Lcom/google/b/a/a/ae;

.field public e:Lcom/google/b/a/a/R;

.field public f:Lcom/google/b/a/a/O;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1233
    invoke-direct {p0}, Lcom/google/protobuf/nano/ExtendableMessageNano;-><init>()V

    .line 1234
    iput-object v0, p0, Lcom/google/b/a/a/K;->a:Lcom/google/b/a/a/J;

    iput-object v0, p0, Lcom/google/b/a/a/K;->b:Lcom/google/b/a/a/Q;

    iput-object v0, p0, Lcom/google/b/a/a/K;->c:Lcom/google/b/a/a/Y;

    iput-object v0, p0, Lcom/google/b/a/a/K;->d:Lcom/google/b/a/a/ae;

    iput-object v0, p0, Lcom/google/b/a/a/K;->e:Lcom/google/b/a/a/R;

    iput-object v0, p0, Lcom/google/b/a/a/K;->f:Lcom/google/b/a/a/O;

    iput-object v0, p0, Lcom/google/b/a/a/K;->unknownFieldData:Ljava/util/List;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/b/a/a/K;->cachedSize:I

    .line 1235
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 1275
    invoke-super {p0}, Lcom/google/protobuf/nano/ExtendableMessageNano;->computeSerializedSize()I

    move-result v0

    .line 1276
    iget-object v1, p0, Lcom/google/b/a/a/K;->a:Lcom/google/b/a/a/J;

    if-eqz v1, :cond_0

    .line 1277
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/b/a/a/K;->a:Lcom/google/b/a/a/J;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1280
    :cond_0
    iget-object v1, p0, Lcom/google/b/a/a/K;->b:Lcom/google/b/a/a/Q;

    if-eqz v1, :cond_1

    .line 1281
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/b/a/a/K;->b:Lcom/google/b/a/a/Q;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1284
    :cond_1
    iget-object v1, p0, Lcom/google/b/a/a/K;->c:Lcom/google/b/a/a/Y;

    if-eqz v1, :cond_2

    .line 1285
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/b/a/a/K;->c:Lcom/google/b/a/a/Y;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1288
    :cond_2
    iget-object v1, p0, Lcom/google/b/a/a/K;->d:Lcom/google/b/a/a/ae;

    if-eqz v1, :cond_3

    .line 1289
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/b/a/a/K;->d:Lcom/google/b/a/a/ae;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1292
    :cond_3
    iget-object v1, p0, Lcom/google/b/a/a/K;->e:Lcom/google/b/a/a/R;

    if-eqz v1, :cond_4

    .line 1293
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/b/a/a/K;->e:Lcom/google/b/a/a/R;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1296
    :cond_4
    iget-object v1, p0, Lcom/google/b/a/a/K;->f:Lcom/google/b/a/a/O;

    if-eqz v1, :cond_5

    .line 1297
    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/b/a/a/K;->f:Lcom/google/b/a/a/O;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1300
    :cond_5
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1

    .prologue
    .line 1198
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/b/a/a/K;->storeUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lcom/google/b/a/a/K;->a:Lcom/google/b/a/a/J;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/b/a/a/J;

    invoke-direct {v0}, Lcom/google/b/a/a/J;-><init>()V

    iput-object v0, p0, Lcom/google/b/a/a/K;->a:Lcom/google/b/a/a/J;

    :cond_1
    iget-object v0, p0, Lcom/google/b/a/a/K;->a:Lcom/google/b/a/a/J;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->a(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lcom/google/b/a/a/K;->b:Lcom/google/b/a/a/Q;

    if-nez v0, :cond_2

    new-instance v0, Lcom/google/b/a/a/Q;

    invoke-direct {v0}, Lcom/google/b/a/a/Q;-><init>()V

    iput-object v0, p0, Lcom/google/b/a/a/K;->b:Lcom/google/b/a/a/Q;

    :cond_2
    iget-object v0, p0, Lcom/google/b/a/a/K;->b:Lcom/google/b/a/a/Q;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->a(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lcom/google/b/a/a/K;->c:Lcom/google/b/a/a/Y;

    if-nez v0, :cond_3

    new-instance v0, Lcom/google/b/a/a/Y;

    invoke-direct {v0}, Lcom/google/b/a/a/Y;-><init>()V

    iput-object v0, p0, Lcom/google/b/a/a/K;->c:Lcom/google/b/a/a/Y;

    :cond_3
    iget-object v0, p0, Lcom/google/b/a/a/K;->c:Lcom/google/b/a/a/Y;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->a(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Lcom/google/b/a/a/K;->d:Lcom/google/b/a/a/ae;

    if-nez v0, :cond_4

    new-instance v0, Lcom/google/b/a/a/ae;

    invoke-direct {v0}, Lcom/google/b/a/a/ae;-><init>()V

    iput-object v0, p0, Lcom/google/b/a/a/K;->d:Lcom/google/b/a/a/ae;

    :cond_4
    iget-object v0, p0, Lcom/google/b/a/a/K;->d:Lcom/google/b/a/a/ae;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->a(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    :sswitch_5
    iget-object v0, p0, Lcom/google/b/a/a/K;->e:Lcom/google/b/a/a/R;

    if-nez v0, :cond_5

    new-instance v0, Lcom/google/b/a/a/R;

    invoke-direct {v0}, Lcom/google/b/a/a/R;-><init>()V

    iput-object v0, p0, Lcom/google/b/a/a/K;->e:Lcom/google/b/a/a/R;

    :cond_5
    iget-object v0, p0, Lcom/google/b/a/a/K;->e:Lcom/google/b/a/a/R;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->a(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    :sswitch_6
    iget-object v0, p0, Lcom/google/b/a/a/K;->f:Lcom/google/b/a/a/O;

    if-nez v0, :cond_6

    new-instance v0, Lcom/google/b/a/a/O;

    invoke-direct {v0}, Lcom/google/b/a/a/O;-><init>()V

    iput-object v0, p0, Lcom/google/b/a/a/K;->f:Lcom/google/b/a/a/O;

    :cond_6
    iget-object v0, p0, Lcom/google/b/a/a/K;->f:Lcom/google/b/a/a/O;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->a(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2

    .prologue
    .line 1252
    iget-object v0, p0, Lcom/google/b/a/a/K;->a:Lcom/google/b/a/a/J;

    if-eqz v0, :cond_0

    .line 1253
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/b/a/a/K;->a:Lcom/google/b/a/a/J;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/MessageNano;)V

    .line 1255
    :cond_0
    iget-object v0, p0, Lcom/google/b/a/a/K;->b:Lcom/google/b/a/a/Q;

    if-eqz v0, :cond_1

    .line 1256
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/b/a/a/K;->b:Lcom/google/b/a/a/Q;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/MessageNano;)V

    .line 1258
    :cond_1
    iget-object v0, p0, Lcom/google/b/a/a/K;->c:Lcom/google/b/a/a/Y;

    if-eqz v0, :cond_2

    .line 1259
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/b/a/a/K;->c:Lcom/google/b/a/a/Y;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/MessageNano;)V

    .line 1261
    :cond_2
    iget-object v0, p0, Lcom/google/b/a/a/K;->d:Lcom/google/b/a/a/ae;

    if-eqz v0, :cond_3

    .line 1262
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/b/a/a/K;->d:Lcom/google/b/a/a/ae;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/MessageNano;)V

    .line 1264
    :cond_3
    iget-object v0, p0, Lcom/google/b/a/a/K;->e:Lcom/google/b/a/a/R;

    if-eqz v0, :cond_4

    .line 1265
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/b/a/a/K;->e:Lcom/google/b/a/a/R;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/MessageNano;)V

    .line 1267
    :cond_4
    iget-object v0, p0, Lcom/google/b/a/a/K;->f:Lcom/google/b/a/a/O;

    if-eqz v0, :cond_5

    .line 1268
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/b/a/a/K;->f:Lcom/google/b/a/a/O;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/MessageNano;)V

    .line 1270
    :cond_5
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/ExtendableMessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 1271
    return-void
.end method
