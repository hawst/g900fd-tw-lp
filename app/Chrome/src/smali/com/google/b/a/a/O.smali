.class public final Lcom/google/b/a/a/O;
.super Lcom/google/protobuf/nano/ExtendableMessageNano;
.source "NanoClientProtocol.java"


# instance fields
.field public a:Lcom/google/b/a/a/L;

.field public b:[Lcom/google/b/a/a/U;

.field public c:[Lcom/google/b/a/a/U;

.field public d:Ljava/lang/Boolean;

.field public e:Lcom/google/b/a/a/I;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1884
    invoke-direct {p0}, Lcom/google/protobuf/nano/ExtendableMessageNano;-><init>()V

    .line 1885
    iput-object v1, p0, Lcom/google/b/a/a/O;->a:Lcom/google/b/a/a/L;

    invoke-static {}, Lcom/google/b/a/a/U;->a()[Lcom/google/b/a/a/U;

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/a/a/O;->b:[Lcom/google/b/a/a/U;

    invoke-static {}, Lcom/google/b/a/a/U;->a()[Lcom/google/b/a/a/U;

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/a/a/O;->c:[Lcom/google/b/a/a/U;

    iput-object v1, p0, Lcom/google/b/a/a/O;->d:Ljava/lang/Boolean;

    iput-object v1, p0, Lcom/google/b/a/a/O;->e:Lcom/google/b/a/a/I;

    iput-object v1, p0, Lcom/google/b/a/a/O;->unknownFieldData:Ljava/util/List;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/b/a/a/O;->cachedSize:I

    .line 1886
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 1932
    invoke-super {p0}, Lcom/google/protobuf/nano/ExtendableMessageNano;->computeSerializedSize()I

    move-result v0

    .line 1933
    iget-object v2, p0, Lcom/google/b/a/a/O;->a:Lcom/google/b/a/a/L;

    if-eqz v2, :cond_0

    .line 1934
    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/b/a/a/O;->a:Lcom/google/b/a/a/L;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v2

    add-int/2addr v0, v2

    .line 1937
    :cond_0
    iget-object v2, p0, Lcom/google/b/a/a/O;->b:[Lcom/google/b/a/a/U;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/b/a/a/O;->b:[Lcom/google/b/a/a/U;

    array-length v2, v2

    if-lez v2, :cond_3

    move v2, v0

    move v0, v1

    .line 1938
    :goto_0
    iget-object v3, p0, Lcom/google/b/a/a/O;->b:[Lcom/google/b/a/a/U;

    array-length v3, v3

    if-ge v0, v3, :cond_2

    .line 1939
    iget-object v3, p0, Lcom/google/b/a/a/O;->b:[Lcom/google/b/a/a/U;

    aget-object v3, v3, v0

    .line 1940
    if-eqz v3, :cond_1

    .line 1941
    const/4 v4, 0x2

    invoke-static {v4, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 1938
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    move v0, v2

    .line 1946
    :cond_3
    iget-object v2, p0, Lcom/google/b/a/a/O;->c:[Lcom/google/b/a/a/U;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/google/b/a/a/O;->c:[Lcom/google/b/a/a/U;

    array-length v2, v2

    if-lez v2, :cond_5

    .line 1947
    :goto_1
    iget-object v2, p0, Lcom/google/b/a/a/O;->c:[Lcom/google/b/a/a/U;

    array-length v2, v2

    if-ge v1, v2, :cond_5

    .line 1948
    iget-object v2, p0, Lcom/google/b/a/a/O;->c:[Lcom/google/b/a/a/U;

    aget-object v2, v2, v1

    .line 1949
    if-eqz v2, :cond_4

    .line 1950
    const/4 v3, 0x3

    invoke-static {v3, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v2

    add-int/2addr v0, v2

    .line 1947
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1955
    :cond_5
    iget-object v1, p0, Lcom/google/b/a/a/O;->d:Ljava/lang/Boolean;

    if-eqz v1, :cond_6

    .line 1956
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/b/a/a/O;->d:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 1959
    :cond_6
    iget-object v1, p0, Lcom/google/b/a/a/O;->e:Lcom/google/b/a/a/I;

    if-eqz v1, :cond_7

    .line 1960
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/b/a/a/O;->e:Lcom/google/b/a/a/I;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1963
    :cond_7
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1852
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/b/a/a/O;->storeUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lcom/google/b/a/a/O;->a:Lcom/google/b/a/a/L;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/b/a/a/L;

    invoke-direct {v0}, Lcom/google/b/a/a/L;-><init>()V

    iput-object v0, p0, Lcom/google/b/a/a/O;->a:Lcom/google/b/a/a/L;

    :cond_1
    iget-object v0, p0, Lcom/google/b/a/a/O;->a:Lcom/google/b/a/a/L;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->a(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->a(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/b/a/a/O;->b:[Lcom/google/b/a/a/U;

    if-nez v0, :cond_3

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/b/a/a/U;

    if-eqz v0, :cond_2

    iget-object v3, p0, Lcom/google/b/a/a/O;->b:[Lcom/google/b/a/a/U;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_2
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_4

    new-instance v3, Lcom/google/b/a/a/U;

    invoke-direct {v3}, Lcom/google/b/a/a/U;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->a(Lcom/google/protobuf/nano/MessageNano;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_3
    iget-object v0, p0, Lcom/google/b/a/a/O;->b:[Lcom/google/b/a/a/U;

    array-length v0, v0

    goto :goto_1

    :cond_4
    new-instance v3, Lcom/google/b/a/a/U;

    invoke-direct {v3}, Lcom/google/b/a/a/U;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->a(Lcom/google/protobuf/nano/MessageNano;)V

    iput-object v2, p0, Lcom/google/b/a/a/O;->b:[Lcom/google/b/a/a/U;

    goto :goto_0

    :sswitch_3
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->a(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/b/a/a/O;->c:[Lcom/google/b/a/a/U;

    if-nez v0, :cond_6

    move v0, v1

    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/b/a/a/U;

    if-eqz v0, :cond_5

    iget-object v3, p0, Lcom/google/b/a/a/O;->c:[Lcom/google/b/a/a/U;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_5
    :goto_4
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_7

    new-instance v3, Lcom/google/b/a/a/U;

    invoke-direct {v3}, Lcom/google/b/a/a/U;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->a(Lcom/google/protobuf/nano/MessageNano;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_6
    iget-object v0, p0, Lcom/google/b/a/a/O;->c:[Lcom/google/b/a/a/U;

    array-length v0, v0

    goto :goto_3

    :cond_7
    new-instance v3, Lcom/google/b/a/a/U;

    invoke-direct {v3}, Lcom/google/b/a/a/U;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->a(Lcom/google/protobuf/nano/MessageNano;)V

    iput-object v2, p0, Lcom/google/b/a/a/O;->c:[Lcom/google/b/a/a/U;

    goto/16 :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/a/a/O;->d:Ljava/lang/Boolean;

    goto/16 :goto_0

    :sswitch_5
    iget-object v0, p0, Lcom/google/b/a/a/O;->e:Lcom/google/b/a/a/I;

    if-nez v0, :cond_8

    new-instance v0, Lcom/google/b/a/a/I;

    invoke-direct {v0}, Lcom/google/b/a/a/I;-><init>()V

    iput-object v0, p0, Lcom/google/b/a/a/O;->e:Lcom/google/b/a/a/I;

    :cond_8
    iget-object v0, p0, Lcom/google/b/a/a/O;->e:Lcom/google/b/a/a/I;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->a(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1902
    iget-object v0, p0, Lcom/google/b/a/a/O;->a:Lcom/google/b/a/a/L;

    if-eqz v0, :cond_0

    .line 1903
    const/4 v0, 0x1

    iget-object v2, p0, Lcom/google/b/a/a/O;->a:Lcom/google/b/a/a/L;

    invoke-virtual {p1, v0, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/MessageNano;)V

    .line 1905
    :cond_0
    iget-object v0, p0, Lcom/google/b/a/a/O;->b:[Lcom/google/b/a/a/U;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/b/a/a/O;->b:[Lcom/google/b/a/a/U;

    array-length v0, v0

    if-lez v0, :cond_2

    move v0, v1

    .line 1906
    :goto_0
    iget-object v2, p0, Lcom/google/b/a/a/O;->b:[Lcom/google/b/a/a/U;

    array-length v2, v2

    if-ge v0, v2, :cond_2

    .line 1907
    iget-object v2, p0, Lcom/google/b/a/a/O;->b:[Lcom/google/b/a/a/U;

    aget-object v2, v2, v0

    .line 1908
    if-eqz v2, :cond_1

    .line 1909
    const/4 v3, 0x2

    invoke-virtual {p1, v3, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/MessageNano;)V

    .line 1906
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1913
    :cond_2
    iget-object v0, p0, Lcom/google/b/a/a/O;->c:[Lcom/google/b/a/a/U;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/b/a/a/O;->c:[Lcom/google/b/a/a/U;

    array-length v0, v0

    if-lez v0, :cond_4

    .line 1914
    :goto_1
    iget-object v0, p0, Lcom/google/b/a/a/O;->c:[Lcom/google/b/a/a/U;

    array-length v0, v0

    if-ge v1, v0, :cond_4

    .line 1915
    iget-object v0, p0, Lcom/google/b/a/a/O;->c:[Lcom/google/b/a/a/U;

    aget-object v0, v0, v1

    .line 1916
    if-eqz v0, :cond_3

    .line 1917
    const/4 v2, 0x3

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/MessageNano;)V

    .line 1914
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1921
    :cond_4
    iget-object v0, p0, Lcom/google/b/a/a/O;->d:Ljava/lang/Boolean;

    if-eqz v0, :cond_5

    .line 1922
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/b/a/a/O;->d:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(IZ)V

    .line 1924
    :cond_5
    iget-object v0, p0, Lcom/google/b/a/a/O;->e:Lcom/google/b/a/a/I;

    if-eqz v0, :cond_6

    .line 1925
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/b/a/a/O;->e:Lcom/google/b/a/a/I;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/MessageNano;)V

    .line 1927
    :cond_6
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/ExtendableMessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 1928
    return-void
.end method
