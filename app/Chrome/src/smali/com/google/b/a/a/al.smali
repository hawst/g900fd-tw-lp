.class public final Lcom/google/b/a/a/al;
.super Lcom/google/protobuf/nano/ExtendableMessageNano;
.source "NanoJavaClient.java"


# instance fields
.field public a:[Lcom/google/b/a/a/T;

.field public b:[Lcom/google/b/a/a/T;

.field public c:[Lcom/google/b/a/a/S;

.field public d:[Lcom/google/b/a/a/ac;

.field public e:Lcom/google/b/a/a/Q;

.field public f:Lcom/google/b/a/a/O;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 43
    invoke-direct {p0}, Lcom/google/protobuf/nano/ExtendableMessageNano;-><init>()V

    .line 44
    invoke-static {}, Lcom/google/b/a/a/T;->a()[Lcom/google/b/a/a/T;

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/a/a/al;->a:[Lcom/google/b/a/a/T;

    invoke-static {}, Lcom/google/b/a/a/T;->a()[Lcom/google/b/a/a/T;

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/a/a/al;->b:[Lcom/google/b/a/a/T;

    invoke-static {}, Lcom/google/b/a/a/S;->a()[Lcom/google/b/a/a/S;

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/a/a/al;->c:[Lcom/google/b/a/a/S;

    invoke-static {}, Lcom/google/b/a/a/ac;->a()[Lcom/google/b/a/a/ac;

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/a/a/al;->d:[Lcom/google/b/a/a/ac;

    iput-object v1, p0, Lcom/google/b/a/a/al;->e:Lcom/google/b/a/a/Q;

    iput-object v1, p0, Lcom/google/b/a/a/al;->f:Lcom/google/b/a/a/O;

    iput-object v1, p0, Lcom/google/b/a/a/al;->unknownFieldData:Ljava/util/List;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/b/a/a/al;->cachedSize:I

    .line 45
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 105
    invoke-super {p0}, Lcom/google/protobuf/nano/ExtendableMessageNano;->computeSerializedSize()I

    move-result v0

    .line 106
    iget-object v2, p0, Lcom/google/b/a/a/al;->a:[Lcom/google/b/a/a/T;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/b/a/a/al;->a:[Lcom/google/b/a/a/T;

    array-length v2, v2

    if-lez v2, :cond_2

    move v2, v0

    move v0, v1

    .line 107
    :goto_0
    iget-object v3, p0, Lcom/google/b/a/a/al;->a:[Lcom/google/b/a/a/T;

    array-length v3, v3

    if-ge v0, v3, :cond_1

    .line 108
    iget-object v3, p0, Lcom/google/b/a/a/al;->a:[Lcom/google/b/a/a/T;

    aget-object v3, v3, v0

    .line 109
    if-eqz v3, :cond_0

    .line 110
    const/4 v4, 0x1

    invoke-static {v4, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 107
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    move v0, v2

    .line 115
    :cond_2
    iget-object v2, p0, Lcom/google/b/a/a/al;->b:[Lcom/google/b/a/a/T;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/google/b/a/a/al;->b:[Lcom/google/b/a/a/T;

    array-length v2, v2

    if-lez v2, :cond_5

    move v2, v0

    move v0, v1

    .line 116
    :goto_1
    iget-object v3, p0, Lcom/google/b/a/a/al;->b:[Lcom/google/b/a/a/T;

    array-length v3, v3

    if-ge v0, v3, :cond_4

    .line 117
    iget-object v3, p0, Lcom/google/b/a/a/al;->b:[Lcom/google/b/a/a/T;

    aget-object v3, v3, v0

    .line 118
    if-eqz v3, :cond_3

    .line 119
    const/4 v4, 0x2

    invoke-static {v4, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 116
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_4
    move v0, v2

    .line 124
    :cond_5
    iget-object v2, p0, Lcom/google/b/a/a/al;->c:[Lcom/google/b/a/a/S;

    if-eqz v2, :cond_8

    iget-object v2, p0, Lcom/google/b/a/a/al;->c:[Lcom/google/b/a/a/S;

    array-length v2, v2

    if-lez v2, :cond_8

    move v2, v0

    move v0, v1

    .line 125
    :goto_2
    iget-object v3, p0, Lcom/google/b/a/a/al;->c:[Lcom/google/b/a/a/S;

    array-length v3, v3

    if-ge v0, v3, :cond_7

    .line 126
    iget-object v3, p0, Lcom/google/b/a/a/al;->c:[Lcom/google/b/a/a/S;

    aget-object v3, v3, v0

    .line 127
    if-eqz v3, :cond_6

    .line 128
    const/4 v4, 0x3

    invoke-static {v4, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 125
    :cond_6
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_7
    move v0, v2

    .line 133
    :cond_8
    iget-object v2, p0, Lcom/google/b/a/a/al;->d:[Lcom/google/b/a/a/ac;

    if-eqz v2, :cond_a

    iget-object v2, p0, Lcom/google/b/a/a/al;->d:[Lcom/google/b/a/a/ac;

    array-length v2, v2

    if-lez v2, :cond_a

    .line 134
    :goto_3
    iget-object v2, p0, Lcom/google/b/a/a/al;->d:[Lcom/google/b/a/a/ac;

    array-length v2, v2

    if-ge v1, v2, :cond_a

    .line 135
    iget-object v2, p0, Lcom/google/b/a/a/al;->d:[Lcom/google/b/a/a/ac;

    aget-object v2, v2, v1

    .line 136
    if-eqz v2, :cond_9

    .line 137
    const/4 v3, 0x4

    invoke-static {v3, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v2

    add-int/2addr v0, v2

    .line 134
    :cond_9
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 142
    :cond_a
    iget-object v1, p0, Lcom/google/b/a/a/al;->e:Lcom/google/b/a/a/Q;

    if-eqz v1, :cond_b

    .line 143
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/b/a/a/al;->e:Lcom/google/b/a/a/Q;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 146
    :cond_b
    iget-object v1, p0, Lcom/google/b/a/a/al;->f:Lcom/google/b/a/a/O;

    if-eqz v1, :cond_c

    .line 147
    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/b/a/a/al;->f:Lcom/google/b/a/a/O;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 150
    :cond_c
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 8
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/b/a/a/al;->storeUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->a(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/b/a/a/al;->a:[Lcom/google/b/a/a/T;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/b/a/a/T;

    if-eqz v0, :cond_1

    iget-object v3, p0, Lcom/google/b/a/a/al;->a:[Lcom/google/b/a/a/T;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    new-instance v3, Lcom/google/b/a/a/T;

    invoke-direct {v3}, Lcom/google/b/a/a/T;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->a(Lcom/google/protobuf/nano/MessageNano;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Lcom/google/b/a/a/al;->a:[Lcom/google/b/a/a/T;

    array-length v0, v0

    goto :goto_1

    :cond_3
    new-instance v3, Lcom/google/b/a/a/T;

    invoke-direct {v3}, Lcom/google/b/a/a/T;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->a(Lcom/google/protobuf/nano/MessageNano;)V

    iput-object v2, p0, Lcom/google/b/a/a/al;->a:[Lcom/google/b/a/a/T;

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->a(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/b/a/a/al;->b:[Lcom/google/b/a/a/T;

    if-nez v0, :cond_5

    move v0, v1

    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/b/a/a/T;

    if-eqz v0, :cond_4

    iget-object v3, p0, Lcom/google/b/a/a/al;->b:[Lcom/google/b/a/a/T;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_4
    :goto_4
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_6

    new-instance v3, Lcom/google/b/a/a/T;

    invoke-direct {v3}, Lcom/google/b/a/a/T;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->a(Lcom/google/protobuf/nano/MessageNano;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_5
    iget-object v0, p0, Lcom/google/b/a/a/al;->b:[Lcom/google/b/a/a/T;

    array-length v0, v0

    goto :goto_3

    :cond_6
    new-instance v3, Lcom/google/b/a/a/T;

    invoke-direct {v3}, Lcom/google/b/a/a/T;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->a(Lcom/google/protobuf/nano/MessageNano;)V

    iput-object v2, p0, Lcom/google/b/a/a/al;->b:[Lcom/google/b/a/a/T;

    goto/16 :goto_0

    :sswitch_3
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->a(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/b/a/a/al;->c:[Lcom/google/b/a/a/S;

    if-nez v0, :cond_8

    move v0, v1

    :goto_5
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/b/a/a/S;

    if-eqz v0, :cond_7

    iget-object v3, p0, Lcom/google/b/a/a/al;->c:[Lcom/google/b/a/a/S;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_7
    :goto_6
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_9

    new-instance v3, Lcom/google/b/a/a/S;

    invoke-direct {v3}, Lcom/google/b/a/a/S;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->a(Lcom/google/protobuf/nano/MessageNano;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    :cond_8
    iget-object v0, p0, Lcom/google/b/a/a/al;->c:[Lcom/google/b/a/a/S;

    array-length v0, v0

    goto :goto_5

    :cond_9
    new-instance v3, Lcom/google/b/a/a/S;

    invoke-direct {v3}, Lcom/google/b/a/a/S;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->a(Lcom/google/protobuf/nano/MessageNano;)V

    iput-object v2, p0, Lcom/google/b/a/a/al;->c:[Lcom/google/b/a/a/S;

    goto/16 :goto_0

    :sswitch_4
    const/16 v0, 0x22

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->a(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/b/a/a/al;->d:[Lcom/google/b/a/a/ac;

    if-nez v0, :cond_b

    move v0, v1

    :goto_7
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/b/a/a/ac;

    if-eqz v0, :cond_a

    iget-object v3, p0, Lcom/google/b/a/a/al;->d:[Lcom/google/b/a/a/ac;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_a
    :goto_8
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_c

    new-instance v3, Lcom/google/b/a/a/ac;

    invoke-direct {v3}, Lcom/google/b/a/a/ac;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->a(Lcom/google/protobuf/nano/MessageNano;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_8

    :cond_b
    iget-object v0, p0, Lcom/google/b/a/a/al;->d:[Lcom/google/b/a/a/ac;

    array-length v0, v0

    goto :goto_7

    :cond_c
    new-instance v3, Lcom/google/b/a/a/ac;

    invoke-direct {v3}, Lcom/google/b/a/a/ac;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->a(Lcom/google/protobuf/nano/MessageNano;)V

    iput-object v2, p0, Lcom/google/b/a/a/al;->d:[Lcom/google/b/a/a/ac;

    goto/16 :goto_0

    :sswitch_5
    iget-object v0, p0, Lcom/google/b/a/a/al;->e:Lcom/google/b/a/a/Q;

    if-nez v0, :cond_d

    new-instance v0, Lcom/google/b/a/a/Q;

    invoke-direct {v0}, Lcom/google/b/a/a/Q;-><init>()V

    iput-object v0, p0, Lcom/google/b/a/a/al;->e:Lcom/google/b/a/a/Q;

    :cond_d
    iget-object v0, p0, Lcom/google/b/a/a/al;->e:Lcom/google/b/a/a/Q;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->a(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    :sswitch_6
    iget-object v0, p0, Lcom/google/b/a/a/al;->f:Lcom/google/b/a/a/O;

    if-nez v0, :cond_e

    new-instance v0, Lcom/google/b/a/a/O;

    invoke-direct {v0}, Lcom/google/b/a/a/O;-><init>()V

    iput-object v0, p0, Lcom/google/b/a/a/al;->f:Lcom/google/b/a/a/O;

    :cond_e
    iget-object v0, p0, Lcom/google/b/a/a/al;->f:Lcom/google/b/a/a/O;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->a(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 62
    iget-object v0, p0, Lcom/google/b/a/a/al;->a:[Lcom/google/b/a/a/T;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/b/a/a/al;->a:[Lcom/google/b/a/a/T;

    array-length v0, v0

    if-lez v0, :cond_1

    move v0, v1

    .line 63
    :goto_0
    iget-object v2, p0, Lcom/google/b/a/a/al;->a:[Lcom/google/b/a/a/T;

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 64
    iget-object v2, p0, Lcom/google/b/a/a/al;->a:[Lcom/google/b/a/a/T;

    aget-object v2, v2, v0

    .line 65
    if-eqz v2, :cond_0

    .line 66
    const/4 v3, 0x1

    invoke-virtual {p1, v3, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/MessageNano;)V

    .line 63
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 70
    :cond_1
    iget-object v0, p0, Lcom/google/b/a/a/al;->b:[Lcom/google/b/a/a/T;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/b/a/a/al;->b:[Lcom/google/b/a/a/T;

    array-length v0, v0

    if-lez v0, :cond_3

    move v0, v1

    .line 71
    :goto_1
    iget-object v2, p0, Lcom/google/b/a/a/al;->b:[Lcom/google/b/a/a/T;

    array-length v2, v2

    if-ge v0, v2, :cond_3

    .line 72
    iget-object v2, p0, Lcom/google/b/a/a/al;->b:[Lcom/google/b/a/a/T;

    aget-object v2, v2, v0

    .line 73
    if-eqz v2, :cond_2

    .line 74
    const/4 v3, 0x2

    invoke-virtual {p1, v3, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/MessageNano;)V

    .line 71
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 78
    :cond_3
    iget-object v0, p0, Lcom/google/b/a/a/al;->c:[Lcom/google/b/a/a/S;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/b/a/a/al;->c:[Lcom/google/b/a/a/S;

    array-length v0, v0

    if-lez v0, :cond_5

    move v0, v1

    .line 79
    :goto_2
    iget-object v2, p0, Lcom/google/b/a/a/al;->c:[Lcom/google/b/a/a/S;

    array-length v2, v2

    if-ge v0, v2, :cond_5

    .line 80
    iget-object v2, p0, Lcom/google/b/a/a/al;->c:[Lcom/google/b/a/a/S;

    aget-object v2, v2, v0

    .line 81
    if-eqz v2, :cond_4

    .line 82
    const/4 v3, 0x3

    invoke-virtual {p1, v3, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/MessageNano;)V

    .line 79
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 86
    :cond_5
    iget-object v0, p0, Lcom/google/b/a/a/al;->d:[Lcom/google/b/a/a/ac;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/google/b/a/a/al;->d:[Lcom/google/b/a/a/ac;

    array-length v0, v0

    if-lez v0, :cond_7

    .line 87
    :goto_3
    iget-object v0, p0, Lcom/google/b/a/a/al;->d:[Lcom/google/b/a/a/ac;

    array-length v0, v0

    if-ge v1, v0, :cond_7

    .line 88
    iget-object v0, p0, Lcom/google/b/a/a/al;->d:[Lcom/google/b/a/a/ac;

    aget-object v0, v0, v1

    .line 89
    if-eqz v0, :cond_6

    .line 90
    const/4 v2, 0x4

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/MessageNano;)V

    .line 87
    :cond_6
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 94
    :cond_7
    iget-object v0, p0, Lcom/google/b/a/a/al;->e:Lcom/google/b/a/a/Q;

    if-eqz v0, :cond_8

    .line 95
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/b/a/a/al;->e:Lcom/google/b/a/a/Q;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/MessageNano;)V

    .line 97
    :cond_8
    iget-object v0, p0, Lcom/google/b/a/a/al;->f:Lcom/google/b/a/a/O;

    if-eqz v0, :cond_9

    .line 98
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/b/a/a/al;->f:Lcom/google/b/a/a/O;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/MessageNano;)V

    .line 100
    :cond_9
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/ExtendableMessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 101
    return-void
.end method
