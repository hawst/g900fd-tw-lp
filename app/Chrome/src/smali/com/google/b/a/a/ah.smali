.class public final Lcom/google/b/a/a/ah;
.super Lcom/google/protobuf/nano/ExtendableMessageNano;
.source "NanoClientProtocol.java"


# instance fields
.field public a:Lcom/google/b/a/a/ag;

.field public b:Lcom/google/b/a/a/aj;

.field public c:Lcom/google/b/a/a/R;

.field public d:Lcom/google/b/a/a/ab;

.field public e:Lcom/google/b/a/a/af;

.field public f:Lcom/google/b/a/a/M;

.field public g:Lcom/google/b/a/a/P;

.field public h:Lcom/google/b/a/a/N;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2347
    invoke-direct {p0}, Lcom/google/protobuf/nano/ExtendableMessageNano;-><init>()V

    .line 2348
    iput-object v0, p0, Lcom/google/b/a/a/ah;->a:Lcom/google/b/a/a/ag;

    iput-object v0, p0, Lcom/google/b/a/a/ah;->b:Lcom/google/b/a/a/aj;

    iput-object v0, p0, Lcom/google/b/a/a/ah;->c:Lcom/google/b/a/a/R;

    iput-object v0, p0, Lcom/google/b/a/a/ah;->d:Lcom/google/b/a/a/ab;

    iput-object v0, p0, Lcom/google/b/a/a/ah;->e:Lcom/google/b/a/a/af;

    iput-object v0, p0, Lcom/google/b/a/a/ah;->f:Lcom/google/b/a/a/M;

    iput-object v0, p0, Lcom/google/b/a/a/ah;->g:Lcom/google/b/a/a/P;

    iput-object v0, p0, Lcom/google/b/a/a/ah;->h:Lcom/google/b/a/a/N;

    iput-object v0, p0, Lcom/google/b/a/a/ah;->unknownFieldData:Ljava/util/List;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/b/a/a/ah;->cachedSize:I

    .line 2349
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 2397
    invoke-super {p0}, Lcom/google/protobuf/nano/ExtendableMessageNano;->computeSerializedSize()I

    move-result v0

    .line 2398
    iget-object v1, p0, Lcom/google/b/a/a/ah;->a:Lcom/google/b/a/a/ag;

    if-eqz v1, :cond_0

    .line 2399
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/b/a/a/ah;->a:Lcom/google/b/a/a/ag;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2402
    :cond_0
    iget-object v1, p0, Lcom/google/b/a/a/ah;->b:Lcom/google/b/a/a/aj;

    if-eqz v1, :cond_1

    .line 2403
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/b/a/a/ah;->b:Lcom/google/b/a/a/aj;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2406
    :cond_1
    iget-object v1, p0, Lcom/google/b/a/a/ah;->c:Lcom/google/b/a/a/R;

    if-eqz v1, :cond_2

    .line 2407
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/b/a/a/ah;->c:Lcom/google/b/a/a/R;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2410
    :cond_2
    iget-object v1, p0, Lcom/google/b/a/a/ah;->d:Lcom/google/b/a/a/ab;

    if-eqz v1, :cond_3

    .line 2411
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/b/a/a/ah;->d:Lcom/google/b/a/a/ab;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2414
    :cond_3
    iget-object v1, p0, Lcom/google/b/a/a/ah;->e:Lcom/google/b/a/a/af;

    if-eqz v1, :cond_4

    .line 2415
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/b/a/a/ah;->e:Lcom/google/b/a/a/af;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2418
    :cond_4
    iget-object v1, p0, Lcom/google/b/a/a/ah;->f:Lcom/google/b/a/a/M;

    if-eqz v1, :cond_5

    .line 2419
    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/b/a/a/ah;->f:Lcom/google/b/a/a/M;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2422
    :cond_5
    iget-object v1, p0, Lcom/google/b/a/a/ah;->g:Lcom/google/b/a/a/P;

    if-eqz v1, :cond_6

    .line 2423
    const/4 v1, 0x7

    iget-object v2, p0, Lcom/google/b/a/a/ah;->g:Lcom/google/b/a/a/P;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2426
    :cond_6
    iget-object v1, p0, Lcom/google/b/a/a/ah;->h:Lcom/google/b/a/a/N;

    if-eqz v1, :cond_7

    .line 2427
    const/16 v1, 0x8

    iget-object v2, p0, Lcom/google/b/a/a/ah;->h:Lcom/google/b/a/a/N;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2430
    :cond_7
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1

    .prologue
    .line 2306
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/b/a/a/ah;->storeUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lcom/google/b/a/a/ah;->a:Lcom/google/b/a/a/ag;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/b/a/a/ag;

    invoke-direct {v0}, Lcom/google/b/a/a/ag;-><init>()V

    iput-object v0, p0, Lcom/google/b/a/a/ah;->a:Lcom/google/b/a/a/ag;

    :cond_1
    iget-object v0, p0, Lcom/google/b/a/a/ah;->a:Lcom/google/b/a/a/ag;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->a(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lcom/google/b/a/a/ah;->b:Lcom/google/b/a/a/aj;

    if-nez v0, :cond_2

    new-instance v0, Lcom/google/b/a/a/aj;

    invoke-direct {v0}, Lcom/google/b/a/a/aj;-><init>()V

    iput-object v0, p0, Lcom/google/b/a/a/ah;->b:Lcom/google/b/a/a/aj;

    :cond_2
    iget-object v0, p0, Lcom/google/b/a/a/ah;->b:Lcom/google/b/a/a/aj;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->a(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lcom/google/b/a/a/ah;->c:Lcom/google/b/a/a/R;

    if-nez v0, :cond_3

    new-instance v0, Lcom/google/b/a/a/R;

    invoke-direct {v0}, Lcom/google/b/a/a/R;-><init>()V

    iput-object v0, p0, Lcom/google/b/a/a/ah;->c:Lcom/google/b/a/a/R;

    :cond_3
    iget-object v0, p0, Lcom/google/b/a/a/ah;->c:Lcom/google/b/a/a/R;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->a(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Lcom/google/b/a/a/ah;->d:Lcom/google/b/a/a/ab;

    if-nez v0, :cond_4

    new-instance v0, Lcom/google/b/a/a/ab;

    invoke-direct {v0}, Lcom/google/b/a/a/ab;-><init>()V

    iput-object v0, p0, Lcom/google/b/a/a/ah;->d:Lcom/google/b/a/a/ab;

    :cond_4
    iget-object v0, p0, Lcom/google/b/a/a/ah;->d:Lcom/google/b/a/a/ab;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->a(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    :sswitch_5
    iget-object v0, p0, Lcom/google/b/a/a/ah;->e:Lcom/google/b/a/a/af;

    if-nez v0, :cond_5

    new-instance v0, Lcom/google/b/a/a/af;

    invoke-direct {v0}, Lcom/google/b/a/a/af;-><init>()V

    iput-object v0, p0, Lcom/google/b/a/a/ah;->e:Lcom/google/b/a/a/af;

    :cond_5
    iget-object v0, p0, Lcom/google/b/a/a/ah;->e:Lcom/google/b/a/a/af;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->a(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    :sswitch_6
    iget-object v0, p0, Lcom/google/b/a/a/ah;->f:Lcom/google/b/a/a/M;

    if-nez v0, :cond_6

    new-instance v0, Lcom/google/b/a/a/M;

    invoke-direct {v0}, Lcom/google/b/a/a/M;-><init>()V

    iput-object v0, p0, Lcom/google/b/a/a/ah;->f:Lcom/google/b/a/a/M;

    :cond_6
    iget-object v0, p0, Lcom/google/b/a/a/ah;->f:Lcom/google/b/a/a/M;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->a(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    :sswitch_7
    iget-object v0, p0, Lcom/google/b/a/a/ah;->g:Lcom/google/b/a/a/P;

    if-nez v0, :cond_7

    new-instance v0, Lcom/google/b/a/a/P;

    invoke-direct {v0}, Lcom/google/b/a/a/P;-><init>()V

    iput-object v0, p0, Lcom/google/b/a/a/ah;->g:Lcom/google/b/a/a/P;

    :cond_7
    iget-object v0, p0, Lcom/google/b/a/a/ah;->g:Lcom/google/b/a/a/P;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->a(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    :sswitch_8
    iget-object v0, p0, Lcom/google/b/a/a/ah;->h:Lcom/google/b/a/a/N;

    if-nez v0, :cond_8

    new-instance v0, Lcom/google/b/a/a/N;

    invoke-direct {v0}, Lcom/google/b/a/a/N;-><init>()V

    iput-object v0, p0, Lcom/google/b/a/a/ah;->h:Lcom/google/b/a/a/N;

    :cond_8
    iget-object v0, p0, Lcom/google/b/a/a/ah;->h:Lcom/google/b/a/a/N;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->a(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2

    .prologue
    .line 2368
    iget-object v0, p0, Lcom/google/b/a/a/ah;->a:Lcom/google/b/a/a/ag;

    if-eqz v0, :cond_0

    .line 2369
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/b/a/a/ah;->a:Lcom/google/b/a/a/ag;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/MessageNano;)V

    .line 2371
    :cond_0
    iget-object v0, p0, Lcom/google/b/a/a/ah;->b:Lcom/google/b/a/a/aj;

    if-eqz v0, :cond_1

    .line 2372
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/b/a/a/ah;->b:Lcom/google/b/a/a/aj;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/MessageNano;)V

    .line 2374
    :cond_1
    iget-object v0, p0, Lcom/google/b/a/a/ah;->c:Lcom/google/b/a/a/R;

    if-eqz v0, :cond_2

    .line 2375
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/b/a/a/ah;->c:Lcom/google/b/a/a/R;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/MessageNano;)V

    .line 2377
    :cond_2
    iget-object v0, p0, Lcom/google/b/a/a/ah;->d:Lcom/google/b/a/a/ab;

    if-eqz v0, :cond_3

    .line 2378
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/b/a/a/ah;->d:Lcom/google/b/a/a/ab;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/MessageNano;)V

    .line 2380
    :cond_3
    iget-object v0, p0, Lcom/google/b/a/a/ah;->e:Lcom/google/b/a/a/af;

    if-eqz v0, :cond_4

    .line 2381
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/b/a/a/ah;->e:Lcom/google/b/a/a/af;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/MessageNano;)V

    .line 2383
    :cond_4
    iget-object v0, p0, Lcom/google/b/a/a/ah;->f:Lcom/google/b/a/a/M;

    if-eqz v0, :cond_5

    .line 2384
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/b/a/a/ah;->f:Lcom/google/b/a/a/M;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/MessageNano;)V

    .line 2386
    :cond_5
    iget-object v0, p0, Lcom/google/b/a/a/ah;->g:Lcom/google/b/a/a/P;

    if-eqz v0, :cond_6

    .line 2387
    const/4 v0, 0x7

    iget-object v1, p0, Lcom/google/b/a/a/ah;->g:Lcom/google/b/a/a/P;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/MessageNano;)V

    .line 2389
    :cond_6
    iget-object v0, p0, Lcom/google/b/a/a/ah;->h:Lcom/google/b/a/a/N;

    if-eqz v0, :cond_7

    .line 2390
    const/16 v0, 0x8

    iget-object v1, p0, Lcom/google/b/a/a/ah;->h:Lcom/google/b/a/a/N;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/MessageNano;)V

    .line 2392
    :cond_7
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/ExtendableMessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 2393
    return-void
.end method
