.class public final Lcom/google/b/a/a/c;
.super Lcom/google/protobuf/nano/ExtendableMessageNano;
.source "NanoAndroidListenerProtocol.java"


# instance fields
.field public a:[Lcom/google/b/a/a/T;

.field public b:[Lcom/google/b/a/a/d;

.field public c:[B

.field public d:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 143
    invoke-direct {p0}, Lcom/google/protobuf/nano/ExtendableMessageNano;-><init>()V

    .line 144
    invoke-static {}, Lcom/google/b/a/a/T;->a()[Lcom/google/b/a/a/T;

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/a/a/c;->a:[Lcom/google/b/a/a/T;

    invoke-static {}, Lcom/google/b/a/a/d;->a()[Lcom/google/b/a/a/d;

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/a/a/c;->b:[Lcom/google/b/a/a/d;

    iput-object v1, p0, Lcom/google/b/a/a/c;->c:[B

    iput-object v1, p0, Lcom/google/b/a/a/c;->d:Ljava/lang/Integer;

    iput-object v1, p0, Lcom/google/b/a/a/c;->unknownFieldData:Ljava/util/List;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/b/a/a/c;->cachedSize:I

    .line 145
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 187
    invoke-super {p0}, Lcom/google/protobuf/nano/ExtendableMessageNano;->computeSerializedSize()I

    move-result v0

    .line 188
    iget-object v2, p0, Lcom/google/b/a/a/c;->a:[Lcom/google/b/a/a/T;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/b/a/a/c;->a:[Lcom/google/b/a/a/T;

    array-length v2, v2

    if-lez v2, :cond_2

    move v2, v0

    move v0, v1

    .line 189
    :goto_0
    iget-object v3, p0, Lcom/google/b/a/a/c;->a:[Lcom/google/b/a/a/T;

    array-length v3, v3

    if-ge v0, v3, :cond_1

    .line 190
    iget-object v3, p0, Lcom/google/b/a/a/c;->a:[Lcom/google/b/a/a/T;

    aget-object v3, v3, v0

    .line 191
    if-eqz v3, :cond_0

    .line 192
    const/4 v4, 0x1

    invoke-static {v4, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 189
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    move v0, v2

    .line 197
    :cond_2
    iget-object v2, p0, Lcom/google/b/a/a/c;->b:[Lcom/google/b/a/a/d;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/google/b/a/a/c;->b:[Lcom/google/b/a/a/d;

    array-length v2, v2

    if-lez v2, :cond_4

    .line 198
    :goto_1
    iget-object v2, p0, Lcom/google/b/a/a/c;->b:[Lcom/google/b/a/a/d;

    array-length v2, v2

    if-ge v1, v2, :cond_4

    .line 199
    iget-object v2, p0, Lcom/google/b/a/a/c;->b:[Lcom/google/b/a/a/d;

    aget-object v2, v2, v1

    .line 200
    if-eqz v2, :cond_3

    .line 201
    const/4 v3, 0x2

    invoke-static {v3, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v2

    add-int/2addr v0, v2

    .line 198
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 206
    :cond_4
    iget-object v1, p0, Lcom/google/b/a/a/c;->c:[B

    if-eqz v1, :cond_5

    .line 207
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/b/a/a/c;->c:[B

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(I[B)I

    move-result v1

    add-int/2addr v0, v1

    .line 210
    :cond_5
    iget-object v1, p0, Lcom/google/b/a/a/c;->d:Ljava/lang/Integer;

    if-eqz v1, :cond_6

    .line 211
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/b/a/a/c;->d:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->c(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 214
    :cond_6
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 8
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/b/a/a/c;->storeUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->a(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/b/a/a/c;->a:[Lcom/google/b/a/a/T;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/b/a/a/T;

    if-eqz v0, :cond_1

    iget-object v3, p0, Lcom/google/b/a/a/c;->a:[Lcom/google/b/a/a/T;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    new-instance v3, Lcom/google/b/a/a/T;

    invoke-direct {v3}, Lcom/google/b/a/a/T;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->a(Lcom/google/protobuf/nano/MessageNano;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Lcom/google/b/a/a/c;->a:[Lcom/google/b/a/a/T;

    array-length v0, v0

    goto :goto_1

    :cond_3
    new-instance v3, Lcom/google/b/a/a/T;

    invoke-direct {v3}, Lcom/google/b/a/a/T;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->a(Lcom/google/protobuf/nano/MessageNano;)V

    iput-object v2, p0, Lcom/google/b/a/a/c;->a:[Lcom/google/b/a/a/T;

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->a(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/b/a/a/c;->b:[Lcom/google/b/a/a/d;

    if-nez v0, :cond_5

    move v0, v1

    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/b/a/a/d;

    if-eqz v0, :cond_4

    iget-object v3, p0, Lcom/google/b/a/a/c;->b:[Lcom/google/b/a/a/d;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_4
    :goto_4
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_6

    new-instance v3, Lcom/google/b/a/a/d;

    invoke-direct {v3}, Lcom/google/b/a/a/d;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->a(Lcom/google/protobuf/nano/MessageNano;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_5
    iget-object v0, p0, Lcom/google/b/a/a/c;->b:[Lcom/google/b/a/a/d;

    array-length v0, v0

    goto :goto_3

    :cond_6
    new-instance v3, Lcom/google/b/a/a/d;

    invoke-direct {v3}, Lcom/google/b/a/a/d;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->a(Lcom/google/protobuf/nano/MessageNano;)V

    iput-object v2, p0, Lcom/google/b/a/a/c;->b:[Lcom/google/b/a/a/d;

    goto/16 :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->f()[B

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/a/a/c;->c:[B

    goto/16 :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->c()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/a/a/c;->d:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 160
    iget-object v0, p0, Lcom/google/b/a/a/c;->a:[Lcom/google/b/a/a/T;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/b/a/a/c;->a:[Lcom/google/b/a/a/T;

    array-length v0, v0

    if-lez v0, :cond_1

    move v0, v1

    .line 161
    :goto_0
    iget-object v2, p0, Lcom/google/b/a/a/c;->a:[Lcom/google/b/a/a/T;

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 162
    iget-object v2, p0, Lcom/google/b/a/a/c;->a:[Lcom/google/b/a/a/T;

    aget-object v2, v2, v0

    .line 163
    if-eqz v2, :cond_0

    .line 164
    const/4 v3, 0x1

    invoke-virtual {p1, v3, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/MessageNano;)V

    .line 161
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 168
    :cond_1
    iget-object v0, p0, Lcom/google/b/a/a/c;->b:[Lcom/google/b/a/a/d;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/b/a/a/c;->b:[Lcom/google/b/a/a/d;

    array-length v0, v0

    if-lez v0, :cond_3

    .line 169
    :goto_1
    iget-object v0, p0, Lcom/google/b/a/a/c;->b:[Lcom/google/b/a/a/d;

    array-length v0, v0

    if-ge v1, v0, :cond_3

    .line 170
    iget-object v0, p0, Lcom/google/b/a/a/c;->b:[Lcom/google/b/a/a/d;

    aget-object v0, v0, v1

    .line 171
    if-eqz v0, :cond_2

    .line 172
    const/4 v2, 0x2

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/MessageNano;)V

    .line 169
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 176
    :cond_3
    iget-object v0, p0, Lcom/google/b/a/a/c;->c:[B

    if-eqz v0, :cond_4

    .line 177
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/b/a/a/c;->c:[B

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(I[B)V

    .line 179
    :cond_4
    iget-object v0, p0, Lcom/google/b/a/a/c;->d:Ljava/lang/Integer;

    if-eqz v0, :cond_5

    .line 180
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/b/a/a/c;->d:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(II)V

    .line 182
    :cond_5
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/ExtendableMessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 183
    return-void
.end method
