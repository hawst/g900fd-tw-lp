.class public final Lcom/google/b/a/a/d;
.super Lcom/google/protobuf/nano/ExtendableMessageNano;
.source "NanoAndroidListenerProtocol.java"


# static fields
.field private static volatile c:[Lcom/google/b/a/a/d;


# instance fields
.field public a:Lcom/google/b/a/a/T;

.field public b:Lcom/google/b/a/a/D;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 34
    invoke-direct {p0}, Lcom/google/protobuf/nano/ExtendableMessageNano;-><init>()V

    .line 35
    iput-object v0, p0, Lcom/google/b/a/a/d;->a:Lcom/google/b/a/a/T;

    iput-object v0, p0, Lcom/google/b/a/a/d;->b:Lcom/google/b/a/a/D;

    iput-object v0, p0, Lcom/google/b/a/a/d;->unknownFieldData:Ljava/util/List;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/b/a/a/d;->cachedSize:I

    .line 36
    return-void
.end method

.method public static a()[Lcom/google/b/a/a/d;
    .locals 2

    .prologue
    .line 17
    sget-object v0, Lcom/google/b/a/a/d;->c:[Lcom/google/b/a/a/d;

    if-nez v0, :cond_1

    .line 18
    sget-object v1, Lcom/google/protobuf/nano/InternalNano;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 20
    :try_start_0
    sget-object v0, Lcom/google/b/a/a/d;->c:[Lcom/google/b/a/a/d;

    if-nez v0, :cond_0

    .line 21
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/b/a/a/d;

    sput-object v0, Lcom/google/b/a/a/d;->c:[Lcom/google/b/a/a/d;

    .line 23
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 25
    :cond_1
    sget-object v0, Lcom/google/b/a/a/d;->c:[Lcom/google/b/a/a/d;

    return-object v0

    .line 23
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 60
    invoke-super {p0}, Lcom/google/protobuf/nano/ExtendableMessageNano;->computeSerializedSize()I

    move-result v0

    .line 61
    iget-object v1, p0, Lcom/google/b/a/a/d;->a:Lcom/google/b/a/a/T;

    if-eqz v1, :cond_0

    .line 62
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/b/a/a/d;->a:Lcom/google/b/a/a/T;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 65
    :cond_0
    iget-object v1, p0, Lcom/google/b/a/a/d;->b:Lcom/google/b/a/a/D;

    if-eqz v1, :cond_1

    .line 66
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/b/a/a/d;->b:Lcom/google/b/a/a/D;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 69
    :cond_1
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1

    .prologue
    .line 11
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/b/a/a/d;->storeUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lcom/google/b/a/a/d;->a:Lcom/google/b/a/a/T;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/b/a/a/T;

    invoke-direct {v0}, Lcom/google/b/a/a/T;-><init>()V

    iput-object v0, p0, Lcom/google/b/a/a/d;->a:Lcom/google/b/a/a/T;

    :cond_1
    iget-object v0, p0, Lcom/google/b/a/a/d;->a:Lcom/google/b/a/a/T;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->a(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lcom/google/b/a/a/d;->b:Lcom/google/b/a/a/D;

    if-nez v0, :cond_2

    new-instance v0, Lcom/google/b/a/a/D;

    invoke-direct {v0}, Lcom/google/b/a/a/D;-><init>()V

    iput-object v0, p0, Lcom/google/b/a/a/d;->b:Lcom/google/b/a/a/D;

    :cond_2
    iget-object v0, p0, Lcom/google/b/a/a/d;->b:Lcom/google/b/a/a/D;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->a(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2

    .prologue
    .line 49
    iget-object v0, p0, Lcom/google/b/a/a/d;->a:Lcom/google/b/a/a/T;

    if-eqz v0, :cond_0

    .line 50
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/b/a/a/d;->a:Lcom/google/b/a/a/T;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/MessageNano;)V

    .line 52
    :cond_0
    iget-object v0, p0, Lcom/google/b/a/a/d;->b:Lcom/google/b/a/a/D;

    if-eqz v0, :cond_1

    .line 53
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/b/a/a/d;->b:Lcom/google/b/a/a/D;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/MessageNano;)V

    .line 55
    :cond_1
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/ExtendableMessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 56
    return-void
.end method
