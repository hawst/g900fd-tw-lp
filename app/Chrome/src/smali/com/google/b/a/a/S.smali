.class public final Lcom/google/b/a/a/S;
.super Lcom/google/protobuf/nano/ExtendableMessageNano;
.source "NanoClientProtocol.java"


# static fields
.field private static volatile g:[Lcom/google/b/a/a/S;


# instance fields
.field public a:Lcom/google/b/a/a/T;

.field public b:Ljava/lang/Boolean;

.field public c:Ljava/lang/Long;

.field public d:Ljava/lang/Boolean;

.field public e:[B

.field public f:Ljava/lang/Long;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 676
    invoke-direct {p0}, Lcom/google/protobuf/nano/ExtendableMessageNano;-><init>()V

    .line 677
    iput-object v0, p0, Lcom/google/b/a/a/S;->a:Lcom/google/b/a/a/T;

    iput-object v0, p0, Lcom/google/b/a/a/S;->b:Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/google/b/a/a/S;->c:Ljava/lang/Long;

    iput-object v0, p0, Lcom/google/b/a/a/S;->d:Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/google/b/a/a/S;->e:[B

    iput-object v0, p0, Lcom/google/b/a/a/S;->f:Ljava/lang/Long;

    iput-object v0, p0, Lcom/google/b/a/a/S;->unknownFieldData:Ljava/util/List;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/b/a/a/S;->cachedSize:I

    .line 678
    return-void
.end method

.method public static a()[Lcom/google/b/a/a/S;
    .locals 2

    .prologue
    .line 647
    sget-object v0, Lcom/google/b/a/a/S;->g:[Lcom/google/b/a/a/S;

    if-nez v0, :cond_1

    .line 648
    sget-object v1, Lcom/google/protobuf/nano/InternalNano;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 650
    :try_start_0
    sget-object v0, Lcom/google/b/a/a/S;->g:[Lcom/google/b/a/a/S;

    if-nez v0, :cond_0

    .line 651
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/b/a/a/S;

    sput-object v0, Lcom/google/b/a/a/S;->g:[Lcom/google/b/a/a/S;

    .line 653
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 655
    :cond_1
    sget-object v0, Lcom/google/b/a/a/S;->g:[Lcom/google/b/a/a/S;

    return-object v0

    .line 653
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 4

    .prologue
    .line 718
    invoke-super {p0}, Lcom/google/protobuf/nano/ExtendableMessageNano;->computeSerializedSize()I

    move-result v0

    .line 719
    iget-object v1, p0, Lcom/google/b/a/a/S;->a:Lcom/google/b/a/a/T;

    if-eqz v1, :cond_0

    .line 720
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/b/a/a/S;->a:Lcom/google/b/a/a/T;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 723
    :cond_0
    iget-object v1, p0, Lcom/google/b/a/a/S;->b:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    .line 724
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/b/a/a/S;->b:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 727
    :cond_1
    iget-object v1, p0, Lcom/google/b/a/a/S;->c:Ljava/lang/Long;

    if-eqz v1, :cond_2

    .line 728
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/b/a/a/S;->c:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 731
    :cond_2
    iget-object v1, p0, Lcom/google/b/a/a/S;->e:[B

    if-eqz v1, :cond_3

    .line 732
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/b/a/a/S;->e:[B

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(I[B)I

    move-result v1

    add-int/2addr v0, v1

    .line 735
    :cond_3
    iget-object v1, p0, Lcom/google/b/a/a/S;->f:Ljava/lang/Long;

    if-eqz v1, :cond_4

    .line 736
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/b/a/a/S;->f:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 739
    :cond_4
    iget-object v1, p0, Lcom/google/b/a/a/S;->d:Ljava/lang/Boolean;

    if-eqz v1, :cond_5

    .line 740
    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/b/a/a/S;->d:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 743
    :cond_5
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 2

    .prologue
    .line 641
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/b/a/a/S;->storeUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lcom/google/b/a/a/S;->a:Lcom/google/b/a/a/T;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/b/a/a/T;

    invoke-direct {v0}, Lcom/google/b/a/a/T;-><init>()V

    iput-object v0, p0, Lcom/google/b/a/a/S;->a:Lcom/google/b/a/a/T;

    :cond_1
    iget-object v0, p0, Lcom/google/b/a/a/S;->a:Lcom/google/b/a/a/T;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->a(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/a/a/S;->b:Ljava/lang/Boolean;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->b()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/a/a/S;->c:Ljava/lang/Long;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->f()[B

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/a/a/S;->e:[B

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->b()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/a/a/S;->f:Ljava/lang/Long;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/a/a/S;->d:Ljava/lang/Boolean;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 4

    .prologue
    .line 695
    iget-object v0, p0, Lcom/google/b/a/a/S;->a:Lcom/google/b/a/a/T;

    if-eqz v0, :cond_0

    .line 696
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/b/a/a/S;->a:Lcom/google/b/a/a/T;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/MessageNano;)V

    .line 698
    :cond_0
    iget-object v0, p0, Lcom/google/b/a/a/S;->b:Ljava/lang/Boolean;

    if-eqz v0, :cond_1

    .line 699
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/b/a/a/S;->b:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(IZ)V

    .line 701
    :cond_1
    iget-object v0, p0, Lcom/google/b/a/a/S;->c:Ljava/lang/Long;

    if-eqz v0, :cond_2

    .line 702
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/b/a/a/S;->c:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(IJ)V

    .line 704
    :cond_2
    iget-object v0, p0, Lcom/google/b/a/a/S;->e:[B

    if-eqz v0, :cond_3

    .line 705
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/b/a/a/S;->e:[B

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(I[B)V

    .line 707
    :cond_3
    iget-object v0, p0, Lcom/google/b/a/a/S;->f:Ljava/lang/Long;

    if-eqz v0, :cond_4

    .line 708
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/b/a/a/S;->f:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(IJ)V

    .line 710
    :cond_4
    iget-object v0, p0, Lcom/google/b/a/a/S;->d:Ljava/lang/Boolean;

    if-eqz v0, :cond_5

    .line 711
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/b/a/a/S;->d:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(IZ)V

    .line 713
    :cond_5
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/ExtendableMessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 714
    return-void
.end method
