.class public final Lcom/google/b/a/a/ac;
.super Lcom/google/protobuf/nano/ExtendableMessageNano;
.source "NanoClientProtocol.java"


# static fields
.field private static volatile b:[Lcom/google/b/a/a/ac;


# instance fields
.field public a:[Lcom/google/b/a/a/T;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1761
    invoke-direct {p0}, Lcom/google/protobuf/nano/ExtendableMessageNano;-><init>()V

    .line 1762
    invoke-static {}, Lcom/google/b/a/a/T;->a()[Lcom/google/b/a/a/T;

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/a/a/ac;->a:[Lcom/google/b/a/a/T;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/b/a/a/ac;->unknownFieldData:Ljava/util/List;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/b/a/a/ac;->cachedSize:I

    .line 1763
    return-void
.end method

.method public static a()[Lcom/google/b/a/a/ac;
    .locals 2

    .prologue
    .line 1747
    sget-object v0, Lcom/google/b/a/a/ac;->b:[Lcom/google/b/a/a/ac;

    if-nez v0, :cond_1

    .line 1748
    sget-object v1, Lcom/google/protobuf/nano/InternalNano;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 1750
    :try_start_0
    sget-object v0, Lcom/google/b/a/a/ac;->b:[Lcom/google/b/a/a/ac;

    if-nez v0, :cond_0

    .line 1751
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/b/a/a/ac;

    sput-object v0, Lcom/google/b/a/a/ac;->b:[Lcom/google/b/a/a/ac;

    .line 1753
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1755
    :cond_1
    sget-object v0, Lcom/google/b/a/a/ac;->b:[Lcom/google/b/a/a/ac;

    return-object v0

    .line 1753
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 4

    .prologue
    .line 1788
    invoke-super {p0}, Lcom/google/protobuf/nano/ExtendableMessageNano;->computeSerializedSize()I

    move-result v1

    .line 1789
    iget-object v0, p0, Lcom/google/b/a/a/ac;->a:[Lcom/google/b/a/a/T;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/b/a/a/ac;->a:[Lcom/google/b/a/a/T;

    array-length v0, v0

    if-lez v0, :cond_1

    .line 1790
    const/4 v0, 0x0

    :goto_0
    iget-object v2, p0, Lcom/google/b/a/a/ac;->a:[Lcom/google/b/a/a/T;

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 1791
    iget-object v2, p0, Lcom/google/b/a/a/ac;->a:[Lcom/google/b/a/a/T;

    aget-object v2, v2, v0

    .line 1792
    if-eqz v2, :cond_0

    .line 1793
    const/4 v3, 0x1

    invoke-static {v3, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v2

    add-int/2addr v1, v2

    .line 1790
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1798
    :cond_1
    return v1
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1741
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/b/a/a/ac;->storeUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->a(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/b/a/a/ac;->a:[Lcom/google/b/a/a/T;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/b/a/a/T;

    if-eqz v0, :cond_1

    iget-object v3, p0, Lcom/google/b/a/a/ac;->a:[Lcom/google/b/a/a/T;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    new-instance v3, Lcom/google/b/a/a/T;

    invoke-direct {v3}, Lcom/google/b/a/a/T;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->a(Lcom/google/protobuf/nano/MessageNano;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Lcom/google/b/a/a/ac;->a:[Lcom/google/b/a/a/T;

    array-length v0, v0

    goto :goto_1

    :cond_3
    new-instance v3, Lcom/google/b/a/a/T;

    invoke-direct {v3}, Lcom/google/b/a/a/T;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->a(Lcom/google/protobuf/nano/MessageNano;)V

    iput-object v2, p0, Lcom/google/b/a/a/ac;->a:[Lcom/google/b/a/a/T;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 3

    .prologue
    .line 1775
    iget-object v0, p0, Lcom/google/b/a/a/ac;->a:[Lcom/google/b/a/a/T;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/b/a/a/ac;->a:[Lcom/google/b/a/a/T;

    array-length v0, v0

    if-lez v0, :cond_1

    .line 1776
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/b/a/a/ac;->a:[Lcom/google/b/a/a/T;

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 1777
    iget-object v1, p0, Lcom/google/b/a/a/ac;->a:[Lcom/google/b/a/a/T;

    aget-object v1, v1, v0

    .line 1778
    if-eqz v1, :cond_0

    .line 1779
    const/4 v2, 0x1

    invoke-virtual {p1, v2, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/MessageNano;)V

    .line 1776
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1783
    :cond_1
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/ExtendableMessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 1784
    return-void
.end method
