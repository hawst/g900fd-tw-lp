.class public final Lcom/google/b/a/a/u;
.super Lcom/google/protobuf/nano/ExtendableMessageNano;
.source "NanoAndroidService.java"


# instance fields
.field public a:Ljava/lang/Long;

.field public b:Lcom/google/b/a/a/ak;

.field public c:Lcom/google/b/a/a/x;

.field public d:Lcom/google/b/a/a/w;

.field public e:Lcom/google/b/a/a/z;

.field public f:Lcom/google/b/a/a/y;

.field public g:Lcom/google/b/a/a/A;

.field public h:Lcom/google/b/a/a/v;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1667
    invoke-direct {p0}, Lcom/google/protobuf/nano/ExtendableMessageNano;-><init>()V

    .line 1668
    iput-object v0, p0, Lcom/google/b/a/a/u;->a:Ljava/lang/Long;

    iput-object v0, p0, Lcom/google/b/a/a/u;->b:Lcom/google/b/a/a/ak;

    iput-object v0, p0, Lcom/google/b/a/a/u;->c:Lcom/google/b/a/a/x;

    iput-object v0, p0, Lcom/google/b/a/a/u;->d:Lcom/google/b/a/a/w;

    iput-object v0, p0, Lcom/google/b/a/a/u;->e:Lcom/google/b/a/a/z;

    iput-object v0, p0, Lcom/google/b/a/a/u;->f:Lcom/google/b/a/a/y;

    iput-object v0, p0, Lcom/google/b/a/a/u;->g:Lcom/google/b/a/a/A;

    iput-object v0, p0, Lcom/google/b/a/a/u;->h:Lcom/google/b/a/a/v;

    iput-object v0, p0, Lcom/google/b/a/a/u;->unknownFieldData:Ljava/util/List;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/b/a/a/u;->cachedSize:I

    .line 1669
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 4

    .prologue
    .line 1717
    invoke-super {p0}, Lcom/google/protobuf/nano/ExtendableMessageNano;->computeSerializedSize()I

    move-result v0

    .line 1718
    iget-object v1, p0, Lcom/google/b/a/a/u;->a:Ljava/lang/Long;

    if-eqz v1, :cond_0

    .line 1719
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/b/a/a/u;->a:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 1722
    :cond_0
    iget-object v1, p0, Lcom/google/b/a/a/u;->b:Lcom/google/b/a/a/ak;

    if-eqz v1, :cond_1

    .line 1723
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/b/a/a/u;->b:Lcom/google/b/a/a/ak;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1726
    :cond_1
    iget-object v1, p0, Lcom/google/b/a/a/u;->c:Lcom/google/b/a/a/x;

    if-eqz v1, :cond_2

    .line 1727
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/b/a/a/u;->c:Lcom/google/b/a/a/x;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1730
    :cond_2
    iget-object v1, p0, Lcom/google/b/a/a/u;->d:Lcom/google/b/a/a/w;

    if-eqz v1, :cond_3

    .line 1731
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/b/a/a/u;->d:Lcom/google/b/a/a/w;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1734
    :cond_3
    iget-object v1, p0, Lcom/google/b/a/a/u;->e:Lcom/google/b/a/a/z;

    if-eqz v1, :cond_4

    .line 1735
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/b/a/a/u;->e:Lcom/google/b/a/a/z;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1738
    :cond_4
    iget-object v1, p0, Lcom/google/b/a/a/u;->f:Lcom/google/b/a/a/y;

    if-eqz v1, :cond_5

    .line 1739
    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/b/a/a/u;->f:Lcom/google/b/a/a/y;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1742
    :cond_5
    iget-object v1, p0, Lcom/google/b/a/a/u;->g:Lcom/google/b/a/a/A;

    if-eqz v1, :cond_6

    .line 1743
    const/4 v1, 0x7

    iget-object v2, p0, Lcom/google/b/a/a/u;->g:Lcom/google/b/a/a/A;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1746
    :cond_6
    iget-object v1, p0, Lcom/google/b/a/a/u;->h:Lcom/google/b/a/a/v;

    if-eqz v1, :cond_7

    .line 1747
    const/16 v1, 0x8

    iget-object v2, p0, Lcom/google/b/a/a/u;->h:Lcom/google/b/a/a/v;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1750
    :cond_7
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 2

    .prologue
    .line 996
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/b/a/a/u;->storeUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->b()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/a/a/u;->a:Ljava/lang/Long;

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lcom/google/b/a/a/u;->b:Lcom/google/b/a/a/ak;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/b/a/a/ak;

    invoke-direct {v0}, Lcom/google/b/a/a/ak;-><init>()V

    iput-object v0, p0, Lcom/google/b/a/a/u;->b:Lcom/google/b/a/a/ak;

    :cond_1
    iget-object v0, p0, Lcom/google/b/a/a/u;->b:Lcom/google/b/a/a/ak;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->a(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lcom/google/b/a/a/u;->c:Lcom/google/b/a/a/x;

    if-nez v0, :cond_2

    new-instance v0, Lcom/google/b/a/a/x;

    invoke-direct {v0}, Lcom/google/b/a/a/x;-><init>()V

    iput-object v0, p0, Lcom/google/b/a/a/u;->c:Lcom/google/b/a/a/x;

    :cond_2
    iget-object v0, p0, Lcom/google/b/a/a/u;->c:Lcom/google/b/a/a/x;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->a(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Lcom/google/b/a/a/u;->d:Lcom/google/b/a/a/w;

    if-nez v0, :cond_3

    new-instance v0, Lcom/google/b/a/a/w;

    invoke-direct {v0}, Lcom/google/b/a/a/w;-><init>()V

    iput-object v0, p0, Lcom/google/b/a/a/u;->d:Lcom/google/b/a/a/w;

    :cond_3
    iget-object v0, p0, Lcom/google/b/a/a/u;->d:Lcom/google/b/a/a/w;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->a(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    :sswitch_5
    iget-object v0, p0, Lcom/google/b/a/a/u;->e:Lcom/google/b/a/a/z;

    if-nez v0, :cond_4

    new-instance v0, Lcom/google/b/a/a/z;

    invoke-direct {v0}, Lcom/google/b/a/a/z;-><init>()V

    iput-object v0, p0, Lcom/google/b/a/a/u;->e:Lcom/google/b/a/a/z;

    :cond_4
    iget-object v0, p0, Lcom/google/b/a/a/u;->e:Lcom/google/b/a/a/z;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->a(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    :sswitch_6
    iget-object v0, p0, Lcom/google/b/a/a/u;->f:Lcom/google/b/a/a/y;

    if-nez v0, :cond_5

    new-instance v0, Lcom/google/b/a/a/y;

    invoke-direct {v0}, Lcom/google/b/a/a/y;-><init>()V

    iput-object v0, p0, Lcom/google/b/a/a/u;->f:Lcom/google/b/a/a/y;

    :cond_5
    iget-object v0, p0, Lcom/google/b/a/a/u;->f:Lcom/google/b/a/a/y;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->a(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    :sswitch_7
    iget-object v0, p0, Lcom/google/b/a/a/u;->g:Lcom/google/b/a/a/A;

    if-nez v0, :cond_6

    new-instance v0, Lcom/google/b/a/a/A;

    invoke-direct {v0}, Lcom/google/b/a/a/A;-><init>()V

    iput-object v0, p0, Lcom/google/b/a/a/u;->g:Lcom/google/b/a/a/A;

    :cond_6
    iget-object v0, p0, Lcom/google/b/a/a/u;->g:Lcom/google/b/a/a/A;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->a(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    :sswitch_8
    iget-object v0, p0, Lcom/google/b/a/a/u;->h:Lcom/google/b/a/a/v;

    if-nez v0, :cond_7

    new-instance v0, Lcom/google/b/a/a/v;

    invoke-direct {v0}, Lcom/google/b/a/a/v;-><init>()V

    iput-object v0, p0, Lcom/google/b/a/a/u;->h:Lcom/google/b/a/a/v;

    :cond_7
    iget-object v0, p0, Lcom/google/b/a/a/u;->h:Lcom/google/b/a/a/v;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->a(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 4

    .prologue
    .line 1688
    iget-object v0, p0, Lcom/google/b/a/a/u;->a:Ljava/lang/Long;

    if-eqz v0, :cond_0

    .line 1689
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/b/a/a/u;->a:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(IJ)V

    .line 1691
    :cond_0
    iget-object v0, p0, Lcom/google/b/a/a/u;->b:Lcom/google/b/a/a/ak;

    if-eqz v0, :cond_1

    .line 1692
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/b/a/a/u;->b:Lcom/google/b/a/a/ak;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/MessageNano;)V

    .line 1694
    :cond_1
    iget-object v0, p0, Lcom/google/b/a/a/u;->c:Lcom/google/b/a/a/x;

    if-eqz v0, :cond_2

    .line 1695
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/b/a/a/u;->c:Lcom/google/b/a/a/x;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/MessageNano;)V

    .line 1697
    :cond_2
    iget-object v0, p0, Lcom/google/b/a/a/u;->d:Lcom/google/b/a/a/w;

    if-eqz v0, :cond_3

    .line 1698
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/b/a/a/u;->d:Lcom/google/b/a/a/w;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/MessageNano;)V

    .line 1700
    :cond_3
    iget-object v0, p0, Lcom/google/b/a/a/u;->e:Lcom/google/b/a/a/z;

    if-eqz v0, :cond_4

    .line 1701
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/b/a/a/u;->e:Lcom/google/b/a/a/z;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/MessageNano;)V

    .line 1703
    :cond_4
    iget-object v0, p0, Lcom/google/b/a/a/u;->f:Lcom/google/b/a/a/y;

    if-eqz v0, :cond_5

    .line 1704
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/b/a/a/u;->f:Lcom/google/b/a/a/y;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/MessageNano;)V

    .line 1706
    :cond_5
    iget-object v0, p0, Lcom/google/b/a/a/u;->g:Lcom/google/b/a/a/A;

    if-eqz v0, :cond_6

    .line 1707
    const/4 v0, 0x7

    iget-object v1, p0, Lcom/google/b/a/a/u;->g:Lcom/google/b/a/a/A;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/MessageNano;)V

    .line 1709
    :cond_6
    iget-object v0, p0, Lcom/google/b/a/a/u;->h:Lcom/google/b/a/a/v;

    if-eqz v0, :cond_7

    .line 1710
    const/16 v0, 0x8

    iget-object v1, p0, Lcom/google/b/a/a/u;->h:Lcom/google/b/a/a/v;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/MessageNano;)V

    .line 1712
    :cond_7
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/ExtendableMessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 1713
    return-void
.end method
