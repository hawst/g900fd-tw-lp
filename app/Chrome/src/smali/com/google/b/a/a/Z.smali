.class public final Lcom/google/b/a/a/Z;
.super Lcom/google/protobuf/nano/ExtendableMessageNano;
.source "NanoClientProtocol.java"


# static fields
.field private static volatile c:[Lcom/google/b/a/a/Z;


# instance fields
.field public a:Lcom/google/b/a/a/T;

.field public b:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 831
    invoke-direct {p0}, Lcom/google/protobuf/nano/ExtendableMessageNano;-><init>()V

    .line 832
    iput-object v0, p0, Lcom/google/b/a/a/Z;->a:Lcom/google/b/a/a/T;

    iput-object v0, p0, Lcom/google/b/a/a/Z;->b:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/google/b/a/a/Z;->unknownFieldData:Ljava/util/List;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/b/a/a/Z;->cachedSize:I

    .line 833
    return-void
.end method

.method public static a()[Lcom/google/b/a/a/Z;
    .locals 2

    .prologue
    .line 814
    sget-object v0, Lcom/google/b/a/a/Z;->c:[Lcom/google/b/a/a/Z;

    if-nez v0, :cond_1

    .line 815
    sget-object v1, Lcom/google/protobuf/nano/InternalNano;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 817
    :try_start_0
    sget-object v0, Lcom/google/b/a/a/Z;->c:[Lcom/google/b/a/a/Z;

    if-nez v0, :cond_0

    .line 818
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/b/a/a/Z;

    sput-object v0, Lcom/google/b/a/a/Z;->c:[Lcom/google/b/a/a/Z;

    .line 820
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 822
    :cond_1
    sget-object v0, Lcom/google/b/a/a/Z;->c:[Lcom/google/b/a/a/Z;

    return-object v0

    .line 820
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 857
    invoke-super {p0}, Lcom/google/protobuf/nano/ExtendableMessageNano;->computeSerializedSize()I

    move-result v0

    .line 858
    iget-object v1, p0, Lcom/google/b/a/a/Z;->a:Lcom/google/b/a/a/T;

    if-eqz v1, :cond_0

    .line 859
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/b/a/a/Z;->a:Lcom/google/b/a/a/T;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 862
    :cond_0
    iget-object v1, p0, Lcom/google/b/a/a/Z;->b:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    .line 863
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/b/a/a/Z;->b:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->c(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 866
    :cond_1
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1

    .prologue
    .line 804
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/b/a/a/Z;->storeUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lcom/google/b/a/a/Z;->a:Lcom/google/b/a/a/T;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/b/a/a/T;

    invoke-direct {v0}, Lcom/google/b/a/a/T;-><init>()V

    iput-object v0, p0, Lcom/google/b/a/a/Z;->a:Lcom/google/b/a/a/T;

    :cond_1
    iget-object v0, p0, Lcom/google/b/a/a/Z;->a:Lcom/google/b/a/a/T;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->a(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->c()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/a/a/Z;->b:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2

    .prologue
    .line 846
    iget-object v0, p0, Lcom/google/b/a/a/Z;->a:Lcom/google/b/a/a/T;

    if-eqz v0, :cond_0

    .line 847
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/b/a/a/Z;->a:Lcom/google/b/a/a/T;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/MessageNano;)V

    .line 849
    :cond_0
    iget-object v0, p0, Lcom/google/b/a/a/Z;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 850
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/b/a/a/Z;->b:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(II)V

    .line 852
    :cond_1
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/ExtendableMessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 853
    return-void
.end method
