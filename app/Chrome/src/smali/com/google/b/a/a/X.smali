.class public final Lcom/google/b/a/a/X;
.super Lcom/google/protobuf/nano/ExtendableMessageNano;
.source "NanoClientProtocol.java"


# static fields
.field private static volatile c:[Lcom/google/b/a/a/X;


# instance fields
.field public a:Ljava/lang/Integer;

.field public b:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 3170
    invoke-direct {p0}, Lcom/google/protobuf/nano/ExtendableMessageNano;-><init>()V

    .line 3171
    iput-object v0, p0, Lcom/google/b/a/a/X;->a:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/google/b/a/a/X;->b:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/google/b/a/a/X;->unknownFieldData:Ljava/util/List;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/b/a/a/X;->cachedSize:I

    .line 3172
    return-void
.end method

.method public static a()[Lcom/google/b/a/a/X;
    .locals 2

    .prologue
    .line 3153
    sget-object v0, Lcom/google/b/a/a/X;->c:[Lcom/google/b/a/a/X;

    if-nez v0, :cond_1

    .line 3154
    sget-object v1, Lcom/google/protobuf/nano/InternalNano;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 3156
    :try_start_0
    sget-object v0, Lcom/google/b/a/a/X;->c:[Lcom/google/b/a/a/X;

    if-nez v0, :cond_0

    .line 3157
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/b/a/a/X;

    sput-object v0, Lcom/google/b/a/a/X;->c:[Lcom/google/b/a/a/X;

    .line 3159
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3161
    :cond_1
    sget-object v0, Lcom/google/b/a/a/X;->c:[Lcom/google/b/a/a/X;

    return-object v0

    .line 3159
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 3196
    invoke-super {p0}, Lcom/google/protobuf/nano/ExtendableMessageNano;->computeSerializedSize()I

    move-result v0

    .line 3197
    iget-object v1, p0, Lcom/google/b/a/a/X;->a:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    .line 3198
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/b/a/a/X;->a:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->c(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 3201
    :cond_0
    iget-object v1, p0, Lcom/google/b/a/a/X;->b:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    .line 3202
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/b/a/a/X;->b:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->c(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 3205
    :cond_1
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1

    .prologue
    .line 3147
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/b/a/a/X;->storeUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->c()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/a/a/X;->a:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->c()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/a/a/X;->b:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2

    .prologue
    .line 3185
    iget-object v0, p0, Lcom/google/b/a/a/X;->a:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 3186
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/b/a/a/X;->a:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(II)V

    .line 3188
    :cond_0
    iget-object v0, p0, Lcom/google/b/a/a/X;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 3189
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/b/a/a/X;->b:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(II)V

    .line 3191
    :cond_1
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/ExtendableMessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 3192
    return-void
.end method
