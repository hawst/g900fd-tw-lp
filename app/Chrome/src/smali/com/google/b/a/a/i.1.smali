.class public final Lcom/google/b/a/a/i;
.super Lcom/google/protobuf/nano/ExtendableMessageNano;
.source "NanoAndroidService.java"


# instance fields
.field public a:Lcom/google/b/a/a/ak;

.field public b:Lcom/google/b/a/a/am;

.field public c:Lcom/google/b/a/a/j;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2217
    invoke-direct {p0}, Lcom/google/protobuf/nano/ExtendableMessageNano;-><init>()V

    .line 2218
    iput-object v0, p0, Lcom/google/b/a/a/i;->a:Lcom/google/b/a/a/ak;

    iput-object v0, p0, Lcom/google/b/a/a/i;->b:Lcom/google/b/a/a/am;

    iput-object v0, p0, Lcom/google/b/a/a/i;->c:Lcom/google/b/a/a/j;

    iput-object v0, p0, Lcom/google/b/a/a/i;->unknownFieldData:Ljava/util/List;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/b/a/a/i;->cachedSize:I

    .line 2219
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 2247
    invoke-super {p0}, Lcom/google/protobuf/nano/ExtendableMessageNano;->computeSerializedSize()I

    move-result v0

    .line 2248
    iget-object v1, p0, Lcom/google/b/a/a/i;->a:Lcom/google/b/a/a/ak;

    if-eqz v1, :cond_0

    .line 2249
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/b/a/a/i;->a:Lcom/google/b/a/a/ak;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2252
    :cond_0
    iget-object v1, p0, Lcom/google/b/a/a/i;->b:Lcom/google/b/a/a/am;

    if-eqz v1, :cond_1

    .line 2253
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/b/a/a/i;->b:Lcom/google/b/a/a/am;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2256
    :cond_1
    iget-object v1, p0, Lcom/google/b/a/a/i;->c:Lcom/google/b/a/a/j;

    if-eqz v1, :cond_2

    .line 2257
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/b/a/a/i;->c:Lcom/google/b/a/a/j;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2260
    :cond_2
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1

    .prologue
    .line 2058
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/b/a/a/i;->storeUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lcom/google/b/a/a/i;->a:Lcom/google/b/a/a/ak;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/b/a/a/ak;

    invoke-direct {v0}, Lcom/google/b/a/a/ak;-><init>()V

    iput-object v0, p0, Lcom/google/b/a/a/i;->a:Lcom/google/b/a/a/ak;

    :cond_1
    iget-object v0, p0, Lcom/google/b/a/a/i;->a:Lcom/google/b/a/a/ak;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->a(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lcom/google/b/a/a/i;->b:Lcom/google/b/a/a/am;

    if-nez v0, :cond_2

    new-instance v0, Lcom/google/b/a/a/am;

    invoke-direct {v0}, Lcom/google/b/a/a/am;-><init>()V

    iput-object v0, p0, Lcom/google/b/a/a/i;->b:Lcom/google/b/a/a/am;

    :cond_2
    iget-object v0, p0, Lcom/google/b/a/a/i;->b:Lcom/google/b/a/a/am;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->a(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lcom/google/b/a/a/i;->c:Lcom/google/b/a/a/j;

    if-nez v0, :cond_3

    new-instance v0, Lcom/google/b/a/a/j;

    invoke-direct {v0}, Lcom/google/b/a/a/j;-><init>()V

    iput-object v0, p0, Lcom/google/b/a/a/i;->c:Lcom/google/b/a/a/j;

    :cond_3
    iget-object v0, p0, Lcom/google/b/a/a/i;->c:Lcom/google/b/a/a/j;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->a(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2

    .prologue
    .line 2233
    iget-object v0, p0, Lcom/google/b/a/a/i;->a:Lcom/google/b/a/a/ak;

    if-eqz v0, :cond_0

    .line 2234
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/b/a/a/i;->a:Lcom/google/b/a/a/ak;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/MessageNano;)V

    .line 2236
    :cond_0
    iget-object v0, p0, Lcom/google/b/a/a/i;->b:Lcom/google/b/a/a/am;

    if-eqz v0, :cond_1

    .line 2237
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/b/a/a/i;->b:Lcom/google/b/a/a/am;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/MessageNano;)V

    .line 2239
    :cond_1
    iget-object v0, p0, Lcom/google/b/a/a/i;->c:Lcom/google/b/a/a/j;

    if-eqz v0, :cond_2

    .line 2240
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/b/a/a/i;->c:Lcom/google/b/a/a/j;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/MessageNano;)V

    .line 2242
    :cond_2
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/ExtendableMessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 2243
    return-void
.end method
