.class public final Lcom/google/b/a/a/j;
.super Lcom/google/protobuf/nano/ExtendableMessageNano;
.source "NanoAndroidService.java"


# instance fields
.field public a:Ljava/lang/Integer;

.field public b:[B

.field public c:Ljava/lang/Long;

.field public d:Lcom/google/b/a/a/I;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2090
    invoke-direct {p0}, Lcom/google/protobuf/nano/ExtendableMessageNano;-><init>()V

    .line 2091
    iput-object v0, p0, Lcom/google/b/a/a/j;->a:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/google/b/a/a/j;->b:[B

    iput-object v0, p0, Lcom/google/b/a/a/j;->c:Ljava/lang/Long;

    iput-object v0, p0, Lcom/google/b/a/a/j;->d:Lcom/google/b/a/a/I;

    iput-object v0, p0, Lcom/google/b/a/a/j;->unknownFieldData:Ljava/util/List;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/b/a/a/j;->cachedSize:I

    .line 2092
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 4

    .prologue
    .line 2124
    invoke-super {p0}, Lcom/google/protobuf/nano/ExtendableMessageNano;->computeSerializedSize()I

    move-result v0

    .line 2125
    iget-object v1, p0, Lcom/google/b/a/a/j;->a:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    .line 2126
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/b/a/a/j;->a:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->c(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2129
    :cond_0
    iget-object v1, p0, Lcom/google/b/a/a/j;->b:[B

    if-eqz v1, :cond_1

    .line 2130
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/b/a/a/j;->b:[B

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(I[B)I

    move-result v1

    add-int/2addr v0, v1

    .line 2133
    :cond_1
    iget-object v1, p0, Lcom/google/b/a/a/j;->c:Ljava/lang/Long;

    if-eqz v1, :cond_2

    .line 2134
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/b/a/a/j;->c:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 2137
    :cond_2
    iget-object v1, p0, Lcom/google/b/a/a/j;->d:Lcom/google/b/a/a/I;

    if-eqz v1, :cond_3

    .line 2138
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/b/a/a/j;->d:Lcom/google/b/a/a/I;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2141
    :cond_3
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 2

    .prologue
    .line 2061
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/b/a/a/j;->storeUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->c()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/a/a/j;->a:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->f()[B

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/a/a/j;->b:[B

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->b()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/a/a/j;->c:Ljava/lang/Long;

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Lcom/google/b/a/a/j;->d:Lcom/google/b/a/a/I;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/b/a/a/I;

    invoke-direct {v0}, Lcom/google/b/a/a/I;-><init>()V

    iput-object v0, p0, Lcom/google/b/a/a/j;->d:Lcom/google/b/a/a/I;

    :cond_1
    iget-object v0, p0, Lcom/google/b/a/a/j;->d:Lcom/google/b/a/a/I;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->a(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 4

    .prologue
    .line 2107
    iget-object v0, p0, Lcom/google/b/a/a/j;->a:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 2108
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/b/a/a/j;->a:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(II)V

    .line 2110
    :cond_0
    iget-object v0, p0, Lcom/google/b/a/a/j;->b:[B

    if-eqz v0, :cond_1

    .line 2111
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/b/a/a/j;->b:[B

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(I[B)V

    .line 2113
    :cond_1
    iget-object v0, p0, Lcom/google/b/a/a/j;->c:Ljava/lang/Long;

    if-eqz v0, :cond_2

    .line 2114
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/b/a/a/j;->c:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(IJ)V

    .line 2116
    :cond_2
    iget-object v0, p0, Lcom/google/b/a/a/j;->d:Lcom/google/b/a/a/I;

    if-eqz v0, :cond_3

    .line 2117
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/b/a/a/j;->d:Lcom/google/b/a/a/I;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/MessageNano;)V

    .line 2119
    :cond_3
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/ExtendableMessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 2120
    return-void
.end method
