.class public final Lcom/google/b/a/a/am;
.super Lcom/google/protobuf/nano/ExtendableMessageNano;
.source "NanoJavaClient.java"


# instance fields
.field public a:Lcom/google/b/a/a/G;

.field public b:[B

.field public c:[B

.field public d:Ljava/lang/Boolean;

.field public e:Ljava/lang/Long;

.field public f:Ljava/lang/Boolean;

.field public g:Lcom/google/b/a/a/an;

.field public h:Lcom/google/b/a/a/ap;

.field public i:Lcom/google/b/a/a/ao;

.field public j:Lcom/google/b/a/a/ao;

.field public k:Lcom/google/b/a/a/ao;

.field public l:Lcom/google/b/a/a/ao;

.field public m:Lcom/google/b/a/a/ao;

.field public n:Lcom/google/b/a/a/F;

.field public o:Lcom/google/b/a/a/aq;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 887
    invoke-direct {p0}, Lcom/google/protobuf/nano/ExtendableMessageNano;-><init>()V

    .line 888
    iput-object v0, p0, Lcom/google/b/a/a/am;->a:Lcom/google/b/a/a/G;

    iput-object v0, p0, Lcom/google/b/a/a/am;->b:[B

    iput-object v0, p0, Lcom/google/b/a/a/am;->c:[B

    iput-object v0, p0, Lcom/google/b/a/a/am;->d:Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/google/b/a/a/am;->e:Ljava/lang/Long;

    iput-object v0, p0, Lcom/google/b/a/a/am;->f:Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/google/b/a/a/am;->g:Lcom/google/b/a/a/an;

    iput-object v0, p0, Lcom/google/b/a/a/am;->h:Lcom/google/b/a/a/ap;

    iput-object v0, p0, Lcom/google/b/a/a/am;->i:Lcom/google/b/a/a/ao;

    iput-object v0, p0, Lcom/google/b/a/a/am;->j:Lcom/google/b/a/a/ao;

    iput-object v0, p0, Lcom/google/b/a/a/am;->k:Lcom/google/b/a/a/ao;

    iput-object v0, p0, Lcom/google/b/a/a/am;->l:Lcom/google/b/a/a/ao;

    iput-object v0, p0, Lcom/google/b/a/a/am;->m:Lcom/google/b/a/a/ao;

    iput-object v0, p0, Lcom/google/b/a/a/am;->n:Lcom/google/b/a/a/F;

    iput-object v0, p0, Lcom/google/b/a/a/am;->o:Lcom/google/b/a/a/aq;

    iput-object v0, p0, Lcom/google/b/a/a/am;->unknownFieldData:Ljava/util/List;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/b/a/a/am;->cachedSize:I

    .line 889
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 4

    .prologue
    .line 965
    invoke-super {p0}, Lcom/google/protobuf/nano/ExtendableMessageNano;->computeSerializedSize()I

    move-result v0

    .line 966
    iget-object v1, p0, Lcom/google/b/a/a/am;->a:Lcom/google/b/a/a/G;

    if-eqz v1, :cond_0

    .line 967
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/b/a/a/am;->a:Lcom/google/b/a/a/G;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 970
    :cond_0
    iget-object v1, p0, Lcom/google/b/a/a/am;->b:[B

    if-eqz v1, :cond_1

    .line 971
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/b/a/a/am;->b:[B

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(I[B)I

    move-result v1

    add-int/2addr v0, v1

    .line 974
    :cond_1
    iget-object v1, p0, Lcom/google/b/a/a/am;->c:[B

    if-eqz v1, :cond_2

    .line 975
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/b/a/a/am;->c:[B

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(I[B)I

    move-result v1

    add-int/2addr v0, v1

    .line 978
    :cond_2
    iget-object v1, p0, Lcom/google/b/a/a/am;->d:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    .line 979
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/b/a/a/am;->d:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 982
    :cond_3
    iget-object v1, p0, Lcom/google/b/a/a/am;->e:Ljava/lang/Long;

    if-eqz v1, :cond_4

    .line 983
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/b/a/a/am;->e:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 986
    :cond_4
    iget-object v1, p0, Lcom/google/b/a/a/am;->f:Ljava/lang/Boolean;

    if-eqz v1, :cond_5

    .line 987
    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/b/a/a/am;->f:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 990
    :cond_5
    iget-object v1, p0, Lcom/google/b/a/a/am;->g:Lcom/google/b/a/a/an;

    if-eqz v1, :cond_6

    .line 991
    const/4 v1, 0x7

    iget-object v2, p0, Lcom/google/b/a/a/am;->g:Lcom/google/b/a/a/an;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 994
    :cond_6
    iget-object v1, p0, Lcom/google/b/a/a/am;->h:Lcom/google/b/a/a/ap;

    if-eqz v1, :cond_7

    .line 995
    const/16 v1, 0x8

    iget-object v2, p0, Lcom/google/b/a/a/am;->h:Lcom/google/b/a/a/ap;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 998
    :cond_7
    iget-object v1, p0, Lcom/google/b/a/a/am;->i:Lcom/google/b/a/a/ao;

    if-eqz v1, :cond_8

    .line 999
    const/16 v1, 0x9

    iget-object v2, p0, Lcom/google/b/a/a/am;->i:Lcom/google/b/a/a/ao;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1002
    :cond_8
    iget-object v1, p0, Lcom/google/b/a/a/am;->j:Lcom/google/b/a/a/ao;

    if-eqz v1, :cond_9

    .line 1003
    const/16 v1, 0xa

    iget-object v2, p0, Lcom/google/b/a/a/am;->j:Lcom/google/b/a/a/ao;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1006
    :cond_9
    iget-object v1, p0, Lcom/google/b/a/a/am;->k:Lcom/google/b/a/a/ao;

    if-eqz v1, :cond_a

    .line 1007
    const/16 v1, 0xb

    iget-object v2, p0, Lcom/google/b/a/a/am;->k:Lcom/google/b/a/a/ao;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1010
    :cond_a
    iget-object v1, p0, Lcom/google/b/a/a/am;->l:Lcom/google/b/a/a/ao;

    if-eqz v1, :cond_b

    .line 1011
    const/16 v1, 0xc

    iget-object v2, p0, Lcom/google/b/a/a/am;->l:Lcom/google/b/a/a/ao;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1014
    :cond_b
    iget-object v1, p0, Lcom/google/b/a/a/am;->m:Lcom/google/b/a/a/ao;

    if-eqz v1, :cond_c

    .line 1015
    const/16 v1, 0xd

    iget-object v2, p0, Lcom/google/b/a/a/am;->m:Lcom/google/b/a/a/ao;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1018
    :cond_c
    iget-object v1, p0, Lcom/google/b/a/a/am;->n:Lcom/google/b/a/a/F;

    if-eqz v1, :cond_d

    .line 1019
    const/16 v1, 0xe

    iget-object v2, p0, Lcom/google/b/a/a/am;->n:Lcom/google/b/a/a/F;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1022
    :cond_d
    iget-object v1, p0, Lcom/google/b/a/a/am;->o:Lcom/google/b/a/a/aq;

    if-eqz v1, :cond_e

    .line 1023
    const/16 v1, 0xf

    iget-object v2, p0, Lcom/google/b/a/a/am;->o:Lcom/google/b/a/a/aq;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1026
    :cond_e
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 2

    .prologue
    .line 825
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/b/a/a/am;->storeUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lcom/google/b/a/a/am;->a:Lcom/google/b/a/a/G;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/b/a/a/G;

    invoke-direct {v0}, Lcom/google/b/a/a/G;-><init>()V

    iput-object v0, p0, Lcom/google/b/a/a/am;->a:Lcom/google/b/a/a/G;

    :cond_1
    iget-object v0, p0, Lcom/google/b/a/a/am;->a:Lcom/google/b/a/a/G;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->a(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->f()[B

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/a/a/am;->b:[B

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->f()[B

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/a/a/am;->c:[B

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/a/a/am;->d:Ljava/lang/Boolean;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->b()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/a/a/am;->e:Ljava/lang/Long;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/b/a/a/am;->f:Ljava/lang/Boolean;

    goto :goto_0

    :sswitch_7
    iget-object v0, p0, Lcom/google/b/a/a/am;->g:Lcom/google/b/a/a/an;

    if-nez v0, :cond_2

    new-instance v0, Lcom/google/b/a/a/an;

    invoke-direct {v0}, Lcom/google/b/a/a/an;-><init>()V

    iput-object v0, p0, Lcom/google/b/a/a/am;->g:Lcom/google/b/a/a/an;

    :cond_2
    iget-object v0, p0, Lcom/google/b/a/a/am;->g:Lcom/google/b/a/a/an;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->a(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    :sswitch_8
    iget-object v0, p0, Lcom/google/b/a/a/am;->h:Lcom/google/b/a/a/ap;

    if-nez v0, :cond_3

    new-instance v0, Lcom/google/b/a/a/ap;

    invoke-direct {v0}, Lcom/google/b/a/a/ap;-><init>()V

    iput-object v0, p0, Lcom/google/b/a/a/am;->h:Lcom/google/b/a/a/ap;

    :cond_3
    iget-object v0, p0, Lcom/google/b/a/a/am;->h:Lcom/google/b/a/a/ap;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->a(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    :sswitch_9
    iget-object v0, p0, Lcom/google/b/a/a/am;->i:Lcom/google/b/a/a/ao;

    if-nez v0, :cond_4

    new-instance v0, Lcom/google/b/a/a/ao;

    invoke-direct {v0}, Lcom/google/b/a/a/ao;-><init>()V

    iput-object v0, p0, Lcom/google/b/a/a/am;->i:Lcom/google/b/a/a/ao;

    :cond_4
    iget-object v0, p0, Lcom/google/b/a/a/am;->i:Lcom/google/b/a/a/ao;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->a(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    :sswitch_a
    iget-object v0, p0, Lcom/google/b/a/a/am;->j:Lcom/google/b/a/a/ao;

    if-nez v0, :cond_5

    new-instance v0, Lcom/google/b/a/a/ao;

    invoke-direct {v0}, Lcom/google/b/a/a/ao;-><init>()V

    iput-object v0, p0, Lcom/google/b/a/a/am;->j:Lcom/google/b/a/a/ao;

    :cond_5
    iget-object v0, p0, Lcom/google/b/a/a/am;->j:Lcom/google/b/a/a/ao;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->a(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    :sswitch_b
    iget-object v0, p0, Lcom/google/b/a/a/am;->k:Lcom/google/b/a/a/ao;

    if-nez v0, :cond_6

    new-instance v0, Lcom/google/b/a/a/ao;

    invoke-direct {v0}, Lcom/google/b/a/a/ao;-><init>()V

    iput-object v0, p0, Lcom/google/b/a/a/am;->k:Lcom/google/b/a/a/ao;

    :cond_6
    iget-object v0, p0, Lcom/google/b/a/a/am;->k:Lcom/google/b/a/a/ao;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->a(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    :sswitch_c
    iget-object v0, p0, Lcom/google/b/a/a/am;->l:Lcom/google/b/a/a/ao;

    if-nez v0, :cond_7

    new-instance v0, Lcom/google/b/a/a/ao;

    invoke-direct {v0}, Lcom/google/b/a/a/ao;-><init>()V

    iput-object v0, p0, Lcom/google/b/a/a/am;->l:Lcom/google/b/a/a/ao;

    :cond_7
    iget-object v0, p0, Lcom/google/b/a/a/am;->l:Lcom/google/b/a/a/ao;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->a(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    :sswitch_d
    iget-object v0, p0, Lcom/google/b/a/a/am;->m:Lcom/google/b/a/a/ao;

    if-nez v0, :cond_8

    new-instance v0, Lcom/google/b/a/a/ao;

    invoke-direct {v0}, Lcom/google/b/a/a/ao;-><init>()V

    iput-object v0, p0, Lcom/google/b/a/a/am;->m:Lcom/google/b/a/a/ao;

    :cond_8
    iget-object v0, p0, Lcom/google/b/a/a/am;->m:Lcom/google/b/a/a/ao;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->a(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    :sswitch_e
    iget-object v0, p0, Lcom/google/b/a/a/am;->n:Lcom/google/b/a/a/F;

    if-nez v0, :cond_9

    new-instance v0, Lcom/google/b/a/a/F;

    invoke-direct {v0}, Lcom/google/b/a/a/F;-><init>()V

    iput-object v0, p0, Lcom/google/b/a/a/am;->n:Lcom/google/b/a/a/F;

    :cond_9
    iget-object v0, p0, Lcom/google/b/a/a/am;->n:Lcom/google/b/a/a/F;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->a(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    :sswitch_f
    iget-object v0, p0, Lcom/google/b/a/a/am;->o:Lcom/google/b/a/a/aq;

    if-nez v0, :cond_a

    new-instance v0, Lcom/google/b/a/a/aq;

    invoke-direct {v0}, Lcom/google/b/a/a/aq;-><init>()V

    iput-object v0, p0, Lcom/google/b/a/a/am;->o:Lcom/google/b/a/a/aq;

    :cond_a
    iget-object v0, p0, Lcom/google/b/a/a/am;->o:Lcom/google/b/a/a/aq;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->a(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
        0x52 -> :sswitch_a
        0x5a -> :sswitch_b
        0x62 -> :sswitch_c
        0x6a -> :sswitch_d
        0x72 -> :sswitch_e
        0x7a -> :sswitch_f
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 4

    .prologue
    .line 915
    iget-object v0, p0, Lcom/google/b/a/a/am;->a:Lcom/google/b/a/a/G;

    if-eqz v0, :cond_0

    .line 916
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/b/a/a/am;->a:Lcom/google/b/a/a/G;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/MessageNano;)V

    .line 918
    :cond_0
    iget-object v0, p0, Lcom/google/b/a/a/am;->b:[B

    if-eqz v0, :cond_1

    .line 919
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/b/a/a/am;->b:[B

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(I[B)V

    .line 921
    :cond_1
    iget-object v0, p0, Lcom/google/b/a/a/am;->c:[B

    if-eqz v0, :cond_2

    .line 922
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/b/a/a/am;->c:[B

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(I[B)V

    .line 924
    :cond_2
    iget-object v0, p0, Lcom/google/b/a/a/am;->d:Ljava/lang/Boolean;

    if-eqz v0, :cond_3

    .line 925
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/b/a/a/am;->d:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(IZ)V

    .line 927
    :cond_3
    iget-object v0, p0, Lcom/google/b/a/a/am;->e:Ljava/lang/Long;

    if-eqz v0, :cond_4

    .line 928
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/b/a/a/am;->e:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(IJ)V

    .line 930
    :cond_4
    iget-object v0, p0, Lcom/google/b/a/a/am;->f:Ljava/lang/Boolean;

    if-eqz v0, :cond_5

    .line 931
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/b/a/a/am;->f:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(IZ)V

    .line 933
    :cond_5
    iget-object v0, p0, Lcom/google/b/a/a/am;->g:Lcom/google/b/a/a/an;

    if-eqz v0, :cond_6

    .line 934
    const/4 v0, 0x7

    iget-object v1, p0, Lcom/google/b/a/a/am;->g:Lcom/google/b/a/a/an;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/MessageNano;)V

    .line 936
    :cond_6
    iget-object v0, p0, Lcom/google/b/a/a/am;->h:Lcom/google/b/a/a/ap;

    if-eqz v0, :cond_7

    .line 937
    const/16 v0, 0x8

    iget-object v1, p0, Lcom/google/b/a/a/am;->h:Lcom/google/b/a/a/ap;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/MessageNano;)V

    .line 939
    :cond_7
    iget-object v0, p0, Lcom/google/b/a/a/am;->i:Lcom/google/b/a/a/ao;

    if-eqz v0, :cond_8

    .line 940
    const/16 v0, 0x9

    iget-object v1, p0, Lcom/google/b/a/a/am;->i:Lcom/google/b/a/a/ao;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/MessageNano;)V

    .line 942
    :cond_8
    iget-object v0, p0, Lcom/google/b/a/a/am;->j:Lcom/google/b/a/a/ao;

    if-eqz v0, :cond_9

    .line 943
    const/16 v0, 0xa

    iget-object v1, p0, Lcom/google/b/a/a/am;->j:Lcom/google/b/a/a/ao;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/MessageNano;)V

    .line 945
    :cond_9
    iget-object v0, p0, Lcom/google/b/a/a/am;->k:Lcom/google/b/a/a/ao;

    if-eqz v0, :cond_a

    .line 946
    const/16 v0, 0xb

    iget-object v1, p0, Lcom/google/b/a/a/am;->k:Lcom/google/b/a/a/ao;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/MessageNano;)V

    .line 948
    :cond_a
    iget-object v0, p0, Lcom/google/b/a/a/am;->l:Lcom/google/b/a/a/ao;

    if-eqz v0, :cond_b

    .line 949
    const/16 v0, 0xc

    iget-object v1, p0, Lcom/google/b/a/a/am;->l:Lcom/google/b/a/a/ao;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/MessageNano;)V

    .line 951
    :cond_b
    iget-object v0, p0, Lcom/google/b/a/a/am;->m:Lcom/google/b/a/a/ao;

    if-eqz v0, :cond_c

    .line 952
    const/16 v0, 0xd

    iget-object v1, p0, Lcom/google/b/a/a/am;->m:Lcom/google/b/a/a/ao;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/MessageNano;)V

    .line 954
    :cond_c
    iget-object v0, p0, Lcom/google/b/a/a/am;->n:Lcom/google/b/a/a/F;

    if-eqz v0, :cond_d

    .line 955
    const/16 v0, 0xe

    iget-object v1, p0, Lcom/google/b/a/a/am;->n:Lcom/google/b/a/a/F;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/MessageNano;)V

    .line 957
    :cond_d
    iget-object v0, p0, Lcom/google/b/a/a/am;->o:Lcom/google/b/a/a/aq;

    if-eqz v0, :cond_e

    .line 958
    const/16 v0, 0xf

    iget-object v1, p0, Lcom/google/b/a/a/am;->o:Lcom/google/b/a/a/aq;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/MessageNano;)V

    .line 960
    :cond_e
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/ExtendableMessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 961
    return-void
.end method
