.class public final Lcom/google/a/a/a/a/a/a;
.super Lcom/google/android/gms/internal/cc;


# instance fields
.field private a:Ljava/lang/String;

.field private b:[Lcom/google/a/a/a/a/a/b;

.field private c:[B

.field private d:[B

.field private f:[B

.field private g:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/gms/internal/cc;-><init>()V

    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/google/a/a/a/a/a/a;->a:Ljava/lang/String;

    invoke-static {}, Lcom/google/a/a/a/a/a/b;->b()[Lcom/google/a/a/a/a/a/b;

    move-result-object v0

    iput-object v0, p0, Lcom/google/a/a/a/a/a/a;->b:[Lcom/google/a/a/a/a/a/b;

    sget-object v0, Lcom/google/android/gms/internal/cf;->c:[B

    iput-object v0, p0, Lcom/google/a/a/a/a/a/a;->c:[B

    sget-object v0, Lcom/google/android/gms/internal/cf;->c:[B

    iput-object v0, p0, Lcom/google/a/a/a/a/a/a;->d:[B

    sget-object v0, Lcom/google/android/gms/internal/cf;->c:[B

    iput-object v0, p0, Lcom/google/a/a/a/a/a/a;->f:[B

    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/google/a/a/a/a/a/a;->g:Ljava/lang/String;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/a/a/a/a/a/a;->e:I

    return-void
.end method


# virtual methods
.method protected final a()I
    .locals 6

    const-wide/16 v2, 0x0

    invoke-super {p0}, Lcom/google/android/gms/internal/cc;->a()I

    move-result v0

    cmp-long v1, v2, v2

    if-eqz v1, :cond_0

    invoke-static {v2, v3}, Lcom/google/android/gms/internal/ca;->b(J)I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    iget-object v1, p0, Lcom/google/a/a/a/a/a/a;->a:Ljava/lang/String;

    const-string/jumbo v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/a/a/a/a/a/a;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/android/gms/internal/ca;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    iget-object v1, p0, Lcom/google/a/a/a/a/a/a;->b:[Lcom/google/a/a/a/a/a/b;

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/google/a/a/a/a/a/a;->b:[Lcom/google/a/a/a/a/a/b;

    array-length v1, v1

    if-lez v1, :cond_4

    const/4 v1, 0x0

    move v5, v1

    move v1, v0

    move v0, v5

    :goto_0
    iget-object v2, p0, Lcom/google/a/a/a/a/a/a;->b:[Lcom/google/a/a/a/a/a/b;

    array-length v2, v2

    if-ge v0, v2, :cond_3

    iget-object v2, p0, Lcom/google/a/a/a/a/a/a;->b:[Lcom/google/a/a/a/a/a/b;

    aget-object v2, v2, v0

    if-eqz v2, :cond_2

    const/4 v3, 0x3

    invoke-static {v3}, Lcom/google/android/gms/internal/ca;->b(I)I

    move-result v3

    invoke-virtual {v2}, Lcom/google/android/gms/internal/ce;->d()I

    move-result v2

    invoke-static {v2}, Lcom/google/android/gms/internal/ca;->d(I)I

    move-result v4

    add-int/2addr v2, v4

    add-int/2addr v2, v3

    add-int/2addr v1, v2

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_3
    move v0, v1

    :cond_4
    iget-object v1, p0, Lcom/google/a/a/a/a/a/a;->c:[B

    sget-object v2, Lcom/google/android/gms/internal/cf;->c:[B

    invoke-static {v1, v2}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v1

    if-nez v1, :cond_5

    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/a/a/a/a/a/a;->c:[B

    invoke-static {v1, v2}, Lcom/google/android/gms/internal/ca;->b(I[B)I

    move-result v1

    add-int/2addr v0, v1

    :cond_5
    iget-object v1, p0, Lcom/google/a/a/a/a/a/a;->d:[B

    sget-object v2, Lcom/google/android/gms/internal/cf;->c:[B

    invoke-static {v1, v2}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v1

    if-nez v1, :cond_6

    const/16 v1, 0x8

    iget-object v2, p0, Lcom/google/a/a/a/a/a/a;->d:[B

    invoke-static {v1, v2}, Lcom/google/android/gms/internal/ca;->b(I[B)I

    move-result v1

    add-int/2addr v0, v1

    :cond_6
    iget-object v1, p0, Lcom/google/a/a/a/a/a/a;->f:[B

    sget-object v2, Lcom/google/android/gms/internal/cf;->c:[B

    invoke-static {v1, v2}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v1

    if-nez v1, :cond_7

    const/16 v1, 0xd

    iget-object v2, p0, Lcom/google/a/a/a/a/a/a;->f:[B

    invoke-static {v1, v2}, Lcom/google/android/gms/internal/ca;->b(I[B)I

    move-result v1

    add-int/2addr v0, v1

    :cond_7
    iget-object v1, p0, Lcom/google/a/a/a/a/a/a;->g:Ljava/lang/String;

    const-string/jumbo v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_8

    const/16 v1, 0xe

    iget-object v2, p0, Lcom/google/a/a/a/a/a/a;->g:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/android/gms/internal/ca;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_8
    return v0
.end method

.method public final a(Lcom/google/android/gms/internal/ca;)V
    .locals 5

    const/4 v4, 0x2

    const-wide/16 v2, 0x0

    cmp-long v0, v2, v2

    if-eqz v0, :cond_0

    invoke-virtual {p1, v2, v3}, Lcom/google/android/gms/internal/ca;->a(J)V

    :cond_0
    iget-object v0, p0, Lcom/google/a/a/a/a/a/a;->a:Ljava/lang/String;

    const-string/jumbo v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/a/a/a/a/a/a;->a:Ljava/lang/String;

    invoke-virtual {p1, v4, v0}, Lcom/google/android/gms/internal/ca;->a(ILjava/lang/String;)V

    :cond_1
    iget-object v0, p0, Lcom/google/a/a/a/a/a/a;->b:[Lcom/google/a/a/a/a/a/b;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/a/a/a/a/a/a;->b:[Lcom/google/a/a/a/a/a/b;

    array-length v0, v0

    if-lez v0, :cond_3

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/a/a/a/a/a/a;->b:[Lcom/google/a/a/a/a/a/b;

    array-length v1, v1

    if-ge v0, v1, :cond_3

    iget-object v1, p0, Lcom/google/a/a/a/a/a/a;->b:[Lcom/google/a/a/a/a/a/b;

    aget-object v1, v1, v0

    if-eqz v1, :cond_2

    const/4 v2, 0x3

    invoke-virtual {p1, v2, v4}, Lcom/google/android/gms/internal/ca;->b(II)V

    invoke-virtual {v1}, Lcom/google/android/gms/internal/ce;->c()I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/android/gms/internal/ca;->c(I)V

    invoke-virtual {v1, p1}, Lcom/google/android/gms/internal/ce;->a(Lcom/google/android/gms/internal/ca;)V

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/google/a/a/a/a/a/a;->c:[B

    sget-object v1, Lcom/google/android/gms/internal/cf;->c:[B

    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    if-nez v0, :cond_4

    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/a/a/a/a/a/a;->c:[B

    invoke-virtual {p1, v0, v1}, Lcom/google/android/gms/internal/ca;->a(I[B)V

    :cond_4
    iget-object v0, p0, Lcom/google/a/a/a/a/a/a;->d:[B

    sget-object v1, Lcom/google/android/gms/internal/cf;->c:[B

    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    if-nez v0, :cond_5

    const/16 v0, 0x8

    iget-object v1, p0, Lcom/google/a/a/a/a/a/a;->d:[B

    invoke-virtual {p1, v0, v1}, Lcom/google/android/gms/internal/ca;->a(I[B)V

    :cond_5
    iget-object v0, p0, Lcom/google/a/a/a/a/a/a;->f:[B

    sget-object v1, Lcom/google/android/gms/internal/cf;->c:[B

    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    if-nez v0, :cond_6

    const/16 v0, 0xd

    iget-object v1, p0, Lcom/google/a/a/a/a/a/a;->f:[B

    invoke-virtual {p1, v0, v1}, Lcom/google/android/gms/internal/ca;->a(I[B)V

    :cond_6
    iget-object v0, p0, Lcom/google/a/a/a/a/a/a;->g:Ljava/lang/String;

    const-string/jumbo v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_7

    const/16 v0, 0xe

    iget-object v1, p0, Lcom/google/a/a/a/a/a/a;->g:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/android/gms/internal/ca;->a(ILjava/lang/String;)V

    :cond_7
    invoke-super {p0, p1}, Lcom/google/android/gms/internal/cc;->a(Lcom/google/android/gms/internal/ca;)V

    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    const-wide/16 v4, 0x0

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p1, p0, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v2, p1, Lcom/google/a/a/a/a/a/a;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    check-cast p1, Lcom/google/a/a/a/a/a/a;

    cmp-long v2, v4, v4

    if-eqz v2, :cond_3

    move v0, v1

    goto :goto_0

    :cond_3
    iget-object v2, p0, Lcom/google/a/a/a/a/a/a;->a:Ljava/lang/String;

    if-nez v2, :cond_4

    iget-object v2, p1, Lcom/google/a/a/a/a/a/a;->a:Ljava/lang/String;

    if-eqz v2, :cond_5

    move v0, v1

    goto :goto_0

    :cond_4
    iget-object v2, p0, Lcom/google/a/a/a/a/a/a;->a:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/a/a/a/a/a/a;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    move v0, v1

    goto :goto_0

    :cond_5
    iget-object v2, p0, Lcom/google/a/a/a/a/a/a;->b:[Lcom/google/a/a/a/a/a/b;

    iget-object v3, p1, Lcom/google/a/a/a/a/a/a;->b:[Lcom/google/a/a/a/a/a/b;

    invoke-static {v2, v3}, Lcom/google/android/gms/internal/cd;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    move v0, v1

    goto :goto_0

    :cond_6
    iget-object v2, p0, Lcom/google/a/a/a/a/a/a;->c:[B

    iget-object v3, p1, Lcom/google/a/a/a/a/a/a;->c:[B

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v2

    if-nez v2, :cond_7

    move v0, v1

    goto :goto_0

    :cond_7
    iget-object v2, p0, Lcom/google/a/a/a/a/a/a;->d:[B

    iget-object v3, p1, Lcom/google/a/a/a/a/a/a;->d:[B

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v2

    if-nez v2, :cond_8

    move v0, v1

    goto :goto_0

    :cond_8
    iget-object v2, p0, Lcom/google/a/a/a/a/a/a;->f:[B

    iget-object v3, p1, Lcom/google/a/a/a/a/a/a;->f:[B

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v2

    if-nez v2, :cond_9

    move v0, v1

    goto :goto_0

    :cond_9
    iget-object v2, p0, Lcom/google/a/a/a/a/a/a;->g:Ljava/lang/String;

    if-nez v2, :cond_a

    iget-object v2, p1, Lcom/google/a/a/a/a/a/a;->g:Ljava/lang/String;

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0

    :cond_a
    iget-object v2, p0, Lcom/google/a/a/a/a/a/a;->g:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/a/a/a/a/a/a;->g:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/a/a/a/a/a/a;->a:Ljava/lang/String;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x3fd1

    mul-int/lit8 v0, v0, 0x1f

    mul-int/lit8 v0, v0, 0x1f

    mul-int/lit8 v0, v0, 0x1f

    add-int/lit16 v0, v0, 0x4d5

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/a/a/a/a/a/a;->b:[Lcom/google/a/a/a/a/a/b;

    invoke-static {v2}, Lcom/google/android/gms/internal/cd;->a([Ljava/lang/Object;)I

    move-result v2

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/a/a/a/a/a/a;->c:[B

    invoke-static {v2}, Ljava/util/Arrays;->hashCode([B)I

    move-result v2

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/a/a/a/a/a/a;->d:[B

    invoke-static {v2}, Ljava/util/Arrays;->hashCode([B)I

    move-result v2

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/a/a/a/a/a/a;->f:[B

    invoke-static {v2}, Ljava/util/Arrays;->hashCode([B)I

    move-result v2

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/a/a/a/a/a/a;->g:Ljava/lang/String;

    if-nez v2, :cond_1

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/a/a/a/a/a/a;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lcom/google/a/a/a/a/a/a;->g:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1
.end method
