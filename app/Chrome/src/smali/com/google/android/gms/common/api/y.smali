.class final Lcom/google/android/gms/common/api/y;
.super Landroid/support/v4/content/f;

# interfaces
.implements Lcom/google/android/gms/common/api/k;
.implements Lcom/google/android/gms/common/c;


# instance fields
.field public final a:Lcom/google/android/gms/common/api/i;

.field private b:Z

.field private c:Lcom/google/android/gms/common/ConnectionResult;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/common/api/i;)V
    .locals 0

    invoke-direct {p0, p1}, Landroid/support/v4/content/f;-><init>(Landroid/content/Context;)V

    iput-object p2, p0, Lcom/google/android/gms/common/api/y;->a:Lcom/google/android/gms/common/api/i;

    return-void
.end method

.method private a(Lcom/google/android/gms/common/ConnectionResult;)V
    .locals 1

    iput-object p1, p0, Lcom/google/android/gms/common/api/y;->c:Lcom/google/android/gms/common/ConnectionResult;

    invoke-virtual {p0}, Lcom/google/android/gms/common/api/y;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/common/api/y;->c()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0, p1}, Lcom/google/android/gms/common/api/y;->a(Ljava/lang/Object;)V

    :cond_0
    return-void
.end method


# virtual methods
.method protected final e()V
    .locals 1

    invoke-super {p0}, Landroid/support/v4/content/f;->e()V

    iget-object v0, p0, Lcom/google/android/gms/common/api/y;->a:Lcom/google/android/gms/common/api/i;

    invoke-interface {v0, p0}, Lcom/google/android/gms/common/api/i;->a(Lcom/google/android/gms/common/api/k;)V

    iget-object v0, p0, Lcom/google/android/gms/common/api/y;->a:Lcom/google/android/gms/common/api/i;

    invoke-interface {v0, p0}, Lcom/google/android/gms/common/api/i;->a(Lcom/google/android/gms/common/c;)V

    iget-object v0, p0, Lcom/google/android/gms/common/api/y;->c:Lcom/google/android/gms/common/ConnectionResult;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/common/api/y;->c:Lcom/google/android/gms/common/ConnectionResult;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/common/api/y;->a(Ljava/lang/Object;)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/common/api/y;->a:Lcom/google/android/gms/common/api/i;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/i;->c()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/common/api/y;->a:Lcom/google/android/gms/common/api/i;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/i;->d()Z

    move-result v0

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/google/android/gms/common/api/y;->b:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/common/api/y;->a:Lcom/google/android/gms/common/api/i;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/i;->a()V

    :cond_1
    return-void
.end method

.method protected final g()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/common/api/y;->a:Lcom/google/android/gms/common/api/i;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/i;->b()V

    return-void
.end method

.method protected final j()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/common/api/y;->c:Lcom/google/android/gms/common/ConnectionResult;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/common/api/y;->b:Z

    iget-object v0, p0, Lcom/google/android/gms/common/api/y;->a:Lcom/google/android/gms/common/api/i;

    invoke-interface {v0, p0}, Lcom/google/android/gms/common/api/i;->b(Lcom/google/android/gms/common/api/k;)V

    iget-object v0, p0, Lcom/google/android/gms/common/api/y;->a:Lcom/google/android/gms/common/api/i;

    invoke-interface {v0, p0}, Lcom/google/android/gms/common/api/i;->b(Lcom/google/android/gms/common/c;)V

    iget-object v0, p0, Lcom/google/android/gms/common/api/y;->a:Lcom/google/android/gms/common/api/i;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/i;->b()V

    return-void
.end method

.method public final k()V
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/common/api/y;->b:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/common/api/y;->b:Z

    invoke-virtual {p0}, Lcom/google/android/gms/common/api/y;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/common/api/y;->c()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/common/api/y;->a:Lcom/google/android/gms/common/api/i;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/i;->a()V

    :cond_0
    return-void
.end method

.method public final onConnected(Landroid/os/Bundle;)V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/common/api/y;->b:Z

    sget-object v0, Lcom/google/android/gms/common/ConnectionResult;->LP:Lcom/google/android/gms/common/ConnectionResult;

    invoke-direct {p0, v0}, Lcom/google/android/gms/common/api/y;->a(Lcom/google/android/gms/common/ConnectionResult;)V

    return-void
.end method

.method public final onConnectionFailed(Lcom/google/android/gms/common/ConnectionResult;)V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/common/api/y;->b:Z

    invoke-direct {p0, p1}, Lcom/google/android/gms/common/api/y;->a(Lcom/google/android/gms/common/ConnectionResult;)V

    return-void
.end method

.method public final onConnectionSuspended(I)V
    .locals 0

    return-void
.end method
