.class public Lcom/google/android/gms/location/copresence/OptInParams;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private final a:I

.field private final b:Ljava/lang/String;

.field private final c:I

.field private final d:I

.field private final e:I

.field private final f:I

.field private final g:Ljava/lang/String;

.field private final h:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/gms/location/copresence/l;

    invoke-direct {v0}, Lcom/google/android/gms/location/copresence/l;-><init>()V

    sput-object v0, Lcom/google/android/gms/location/copresence/OptInParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>(ILjava/lang/String;IIIILjava/lang/String;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/gms/location/copresence/OptInParams;->a:I

    iput-object p2, p0, Lcom/google/android/gms/location/copresence/OptInParams;->b:Ljava/lang/String;

    iput p3, p0, Lcom/google/android/gms/location/copresence/OptInParams;->c:I

    iput p4, p0, Lcom/google/android/gms/location/copresence/OptInParams;->d:I

    iput p5, p0, Lcom/google/android/gms/location/copresence/OptInParams;->e:I

    iput p6, p0, Lcom/google/android/gms/location/copresence/OptInParams;->f:I

    iput-object p7, p0, Lcom/google/android/gms/location/copresence/OptInParams;->g:Ljava/lang/String;

    iput-object p8, p0, Lcom/google/android/gms/location/copresence/OptInParams;->h:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method final a()I
    .locals 1

    iget v0, p0, Lcom/google/android/gms/location/copresence/OptInParams;->a:I

    return v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/location/copresence/OptInParams;->b:Ljava/lang/String;

    return-object v0
.end method

.method public final c()I
    .locals 1

    iget v0, p0, Lcom/google/android/gms/location/copresence/OptInParams;->c:I

    return v0
.end method

.method public final d()I
    .locals 1

    iget v0, p0, Lcom/google/android/gms/location/copresence/OptInParams;->d:I

    return v0
.end method

.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final e()I
    .locals 1

    iget v0, p0, Lcom/google/android/gms/location/copresence/OptInParams;->e:I

    return v0
.end method

.method public final f()I
    .locals 1

    iget v0, p0, Lcom/google/android/gms/location/copresence/OptInParams;->f:I

    return v0
.end method

.method public final g()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/location/copresence/OptInParams;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final h()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/location/copresence/OptInParams;->h:Ljava/lang/String;

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    invoke-static {p0, p1}, Lcom/google/android/gms/location/copresence/l;->a(Lcom/google/android/gms/location/copresence/OptInParams;Landroid/os/Parcel;)V

    return-void
.end method
