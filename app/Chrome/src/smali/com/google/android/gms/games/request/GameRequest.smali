.class public interface abstract Lcom/google/android/gms/games/request/GameRequest;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/os/Parcelable;
.implements Lcom/google/android/gms/common/data/d;


# virtual methods
.method public abstract a(Ljava/lang/String;)I
.end method

.method public abstract d()Ljava/lang/String;
.end method

.method public abstract e()Lcom/google/android/gms/games/Game;
.end method

.method public abstract f()Lcom/google/android/gms/games/Player;
.end method

.method public abstract g()[B
.end method

.method public abstract h()I
.end method

.method public abstract i()J
.end method

.method public abstract j()J
.end method

.method public abstract k()I
.end method

.method public abstract l()Ljava/util/List;
.end method
