.class public interface abstract Lcom/google/android/gms/reminders/model/Task;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/os/Parcelable;
.implements Lcom/google/android/gms/common/data/d;


# virtual methods
.method public abstract c()Lcom/google/android/gms/reminders/model/TaskId;
.end method

.method public abstract d()Lcom/google/android/gms/reminders/model/TaskList;
.end method

.method public abstract e()Ljava/lang/String;
.end method

.method public abstract f()Ljava/lang/Long;
.end method

.method public abstract g()Ljava/lang/Long;
.end method

.method public abstract h()Ljava/lang/Boolean;
.end method

.method public abstract i()Ljava/lang/Boolean;
.end method

.method public abstract j()Ljava/lang/Boolean;
.end method

.method public abstract k()Ljava/lang/Boolean;
.end method

.method public abstract l()Ljava/lang/Long;
.end method

.method public abstract m()Lcom/google/android/gms/reminders/model/DateTime;
.end method

.method public abstract n()Lcom/google/android/gms/reminders/model/DateTime;
.end method

.method public abstract o()Lcom/google/android/gms/reminders/model/Location;
.end method

.method public abstract p()Ljava/lang/Long;
.end method
