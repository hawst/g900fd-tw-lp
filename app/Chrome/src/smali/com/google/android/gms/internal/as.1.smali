.class public final Lcom/google/android/gms/internal/as;
.super Ljava/lang/Object;


# static fields
.field public static final a:Lcom/google/android/gms/internal/at;

.field public static final b:Lcom/google/android/gms/internal/au;

.field public static final c:Lcom/google/android/gms/internal/aw;

.field public static final d:Lcom/google/android/gms/internal/av;

.field public static final e:Lcom/google/android/gms/internal/ax;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/google/android/gms/internal/at;

    const-string/jumbo v1, "created"

    invoke-direct {v0, v1}, Lcom/google/android/gms/internal/at;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/internal/as;->a:Lcom/google/android/gms/internal/at;

    new-instance v0, Lcom/google/android/gms/internal/au;

    const-string/jumbo v1, "lastOpenedTime"

    invoke-direct {v0, v1}, Lcom/google/android/gms/internal/au;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/internal/as;->b:Lcom/google/android/gms/internal/au;

    new-instance v0, Lcom/google/android/gms/internal/aw;

    const-string/jumbo v1, "modified"

    invoke-direct {v0, v1}, Lcom/google/android/gms/internal/aw;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/internal/as;->c:Lcom/google/android/gms/internal/aw;

    new-instance v0, Lcom/google/android/gms/internal/av;

    const-string/jumbo v1, "modifiedByMe"

    invoke-direct {v0, v1}, Lcom/google/android/gms/internal/av;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/internal/as;->d:Lcom/google/android/gms/internal/av;

    new-instance v0, Lcom/google/android/gms/internal/ax;

    const-string/jumbo v1, "sharedWithMe"

    invoke-direct {v0, v1}, Lcom/google/android/gms/internal/ax;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/internal/as;->e:Lcom/google/android/gms/internal/ax;

    return-void
.end method
