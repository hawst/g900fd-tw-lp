.class public final Lcom/google/android/gms/internal/nq;
.super Lcom/google/android/gms/location/places/a;

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Lcom/google/android/gms/internal/aW;


# instance fields
.field final a:I

.field private final b:Ljava/lang/String;

.field private final c:Landroid/os/Bundle;

.field private final d:Lcom/google/android/gms/maps/model/LatLng;

.field private final e:F

.field private final f:Lcom/google/android/gms/maps/model/LatLngBounds;

.field private final g:Ljava/lang/String;

.field private final h:Landroid/net/Uri;

.field private final i:Z

.field private final j:F

.field private final k:I

.field private final l:J

.field private final m:Ljava/util/List;

.field private final n:Ljava/lang/String;

.field private final o:Ljava/lang/String;

.field private final p:Ljava/lang/String;

.field private final q:Ljava/lang/String;

.field private final r:Ljava/util/List;

.field private final s:Lcom/google/android/gms/internal/ns;

.field private final t:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/gms/internal/aW;

    invoke-direct {v0}, Lcom/google/android/gms/internal/aW;-><init>()V

    sput-object v0, Lcom/google/android/gms/internal/nq;->CREATOR:Lcom/google/android/gms/internal/aW;

    return-void
.end method

.method constructor <init>(ILjava/lang/String;Ljava/util/List;Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Lcom/google/android/gms/maps/model/LatLng;FLcom/google/android/gms/maps/model/LatLngBounds;Ljava/lang/String;Landroid/net/Uri;ZFIJZLcom/google/android/gms/internal/ns;)V
    .locals 6

    invoke-direct {p0}, Lcom/google/android/gms/location/places/a;-><init>()V

    iput p1, p0, Lcom/google/android/gms/internal/nq;->a:I

    iput-object p2, p0, Lcom/google/android/gms/internal/nq;->b:Ljava/lang/String;

    invoke-static {p3}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/gms/internal/nq;->m:Ljava/util/List;

    iput-object p4, p0, Lcom/google/android/gms/internal/nq;->c:Landroid/os/Bundle;

    iput-object p5, p0, Lcom/google/android/gms/internal/nq;->n:Ljava/lang/String;

    iput-object p6, p0, Lcom/google/android/gms/internal/nq;->o:Ljava/lang/String;

    iput-object p7, p0, Lcom/google/android/gms/internal/nq;->p:Ljava/lang/String;

    iput-object p8, p0, Lcom/google/android/gms/internal/nq;->q:Ljava/lang/String;

    iput-object p9, p0, Lcom/google/android/gms/internal/nq;->r:Ljava/util/List;

    move-object/from16 v0, p10

    iput-object v0, p0, Lcom/google/android/gms/internal/nq;->d:Lcom/google/android/gms/maps/model/LatLng;

    move/from16 v0, p11

    iput v0, p0, Lcom/google/android/gms/internal/nq;->e:F

    move-object/from16 v0, p12

    iput-object v0, p0, Lcom/google/android/gms/internal/nq;->f:Lcom/google/android/gms/maps/model/LatLngBounds;

    move-object/from16 v0, p13

    iput-object v0, p0, Lcom/google/android/gms/internal/nq;->g:Ljava/lang/String;

    move-object/from16 v0, p14

    iput-object v0, p0, Lcom/google/android/gms/internal/nq;->h:Landroid/net/Uri;

    move/from16 v0, p15

    iput-boolean v0, p0, Lcom/google/android/gms/internal/nq;->i:Z

    move/from16 v0, p16

    iput v0, p0, Lcom/google/android/gms/internal/nq;->j:F

    move/from16 v0, p17

    iput v0, p0, Lcom/google/android/gms/internal/nq;->k:I

    move-wide/from16 v0, p18

    iput-wide v0, p0, Lcom/google/android/gms/internal/nq;->l:J

    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    invoke-virtual {p4}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-static {v2}, Lcom/google/android/gms/location/places/PlaceType;->a(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    move-result-object v5

    invoke-virtual {p4, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v5, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_0
    invoke-static {v3}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    iget-object v2, p0, Lcom/google/android/gms/internal/nq;->g:Ljava/lang/String;

    invoke-static {v2}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move/from16 v0, p20

    iput-boolean v0, p0, Lcom/google/android/gms/internal/nq;->t:Z

    move-object/from16 v0, p21

    iput-object v0, p0, Lcom/google/android/gms/internal/nq;->s:Lcom/google/android/gms/internal/ns;

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/internal/nq;->t:Z

    iget-object v0, p0, Lcom/google/android/gms/internal/nq;->b:Ljava/lang/String;

    return-object v0
.end method

.method public final b()Ljava/util/List;
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/internal/nq;->t:Z

    iget-object v0, p0, Lcom/google/android/gms/internal/nq;->m:Ljava/util/List;

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/internal/nq;->t:Z

    iget-object v0, p0, Lcom/google/android/gms/internal/nq;->n:Ljava/lang/String;

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/internal/nq;->t:Z

    iget-object v0, p0, Lcom/google/android/gms/internal/nq;->o:Ljava/lang/String;

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    sget-object v0, Lcom/google/android/gms/internal/nq;->CREATOR:Lcom/google/android/gms/internal/aW;

    const/4 v0, 0x0

    return v0
.end method

.method public final e()Lcom/google/android/gms/maps/model/LatLng;
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/internal/nq;->t:Z

    iget-object v0, p0, Lcom/google/android/gms/internal/nq;->d:Lcom/google/android/gms/maps/model/LatLng;

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    const/4 v4, 0x0

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p0, p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v2, p1, Lcom/google/android/gms/internal/nq;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    check-cast p1, Lcom/google/android/gms/internal/nq;

    iget-object v2, p0, Lcom/google/android/gms/internal/nq;->b:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/gms/internal/nq;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-static {v4, v4}, Lcom/google/android/gms/common/internal/ClientSettings;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-wide v2, p0, Lcom/google/android/gms/internal/nq;->l:J

    iget-wide v4, p1, Lcom/google/android/gms/internal/nq;->l:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public final f()F
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/internal/nq;->t:Z

    iget v0, p0, Lcom/google/android/gms/internal/nq;->e:F

    return v0
.end method

.method public final g()Lcom/google/android/gms/maps/model/LatLngBounds;
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/internal/nq;->t:Z

    iget-object v0, p0, Lcom/google/android/gms/internal/nq;->f:Lcom/google/android/gms/maps/model/LatLngBounds;

    return-object v0
.end method

.method public final h()Landroid/net/Uri;
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/internal/nq;->t:Z

    iget-object v0, p0, Lcom/google/android/gms/internal/nq;->h:Landroid/net/Uri;

    return-object v0
.end method

.method public final hashCode()I
    .locals 4

    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/gms/internal/nq;->b:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const/4 v2, 0x0

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-wide v2, p0, Lcom/google/android/gms/internal/nq;->l:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final i()Ljava/lang/String;
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/internal/nq;->t:Z

    iget-object v0, p0, Lcom/google/android/gms/internal/nq;->p:Ljava/lang/String;

    return-object v0
.end method

.method public final j()Ljava/lang/String;
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/internal/nq;->t:Z

    iget-object v0, p0, Lcom/google/android/gms/internal/nq;->q:Ljava/lang/String;

    return-object v0
.end method

.method public final k()Ljava/util/List;
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/internal/nq;->t:Z

    iget-object v0, p0, Lcom/google/android/gms/internal/nq;->r:Ljava/util/List;

    return-object v0
.end method

.method public final l()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/internal/nq;->t:Z

    iget-boolean v0, p0, Lcom/google/android/gms/internal/nq;->i:Z

    return v0
.end method

.method public final m()F
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/internal/nq;->t:Z

    iget v0, p0, Lcom/google/android/gms/internal/nq;->j:F

    return v0
.end method

.method public final n()I
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/internal/nq;->t:Z

    iget v0, p0, Lcom/google/android/gms/internal/nq;->k:I

    return v0
.end method

.method public final o()J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/gms/internal/nq;->l:J

    return-wide v0
.end method

.method public final p()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/internal/nq;->t:Z

    return v0
.end method

.method public final q()Landroid/os/Bundle;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/internal/nq;->c:Landroid/os/Bundle;

    return-object v0
.end method

.method public final r()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/internal/nq;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final s()Lcom/google/android/gms/internal/ns;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/internal/nq;->s:Lcom/google/android/gms/internal/ns;

    return-object v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 4

    invoke-static {p0}, Lcom/google/android/gms/common/internal/ClientSettings;->a(Ljava/lang/Object;)Lcom/google/android/gms/common/internal/D;

    move-result-object v0

    const-string/jumbo v1, "id"

    iget-object v2, p0, Lcom/google/android/gms/internal/nq;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/common/internal/D;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/android/gms/common/internal/D;

    move-result-object v0

    const-string/jumbo v1, "types"

    iget-object v2, p0, Lcom/google/android/gms/internal/nq;->m:Ljava/util/List;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/common/internal/D;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/android/gms/common/internal/D;

    move-result-object v0

    const-string/jumbo v1, "locale"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/common/internal/D;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/android/gms/common/internal/D;

    move-result-object v0

    const-string/jumbo v1, "name"

    iget-object v2, p0, Lcom/google/android/gms/internal/nq;->n:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/common/internal/D;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/android/gms/common/internal/D;

    move-result-object v0

    const-string/jumbo v1, "address"

    iget-object v2, p0, Lcom/google/android/gms/internal/nq;->o:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/common/internal/D;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/android/gms/common/internal/D;

    move-result-object v0

    const-string/jumbo v1, "phoneNumber"

    iget-object v2, p0, Lcom/google/android/gms/internal/nq;->p:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/common/internal/D;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/android/gms/common/internal/D;

    move-result-object v0

    const-string/jumbo v1, "regularOpenHours"

    iget-object v2, p0, Lcom/google/android/gms/internal/nq;->q:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/common/internal/D;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/android/gms/common/internal/D;

    move-result-object v0

    const-string/jumbo v1, "latlng"

    iget-object v2, p0, Lcom/google/android/gms/internal/nq;->d:Lcom/google/android/gms/maps/model/LatLng;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/common/internal/D;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/android/gms/common/internal/D;

    move-result-object v0

    const-string/jumbo v1, "levelNumber"

    iget v2, p0, Lcom/google/android/gms/internal/nq;->e:F

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/common/internal/D;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/android/gms/common/internal/D;

    move-result-object v0

    const-string/jumbo v1, "viewport"

    iget-object v2, p0, Lcom/google/android/gms/internal/nq;->f:Lcom/google/android/gms/maps/model/LatLngBounds;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/common/internal/D;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/android/gms/common/internal/D;

    move-result-object v0

    const-string/jumbo v1, "timeZone"

    iget-object v2, p0, Lcom/google/android/gms/internal/nq;->g:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/common/internal/D;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/android/gms/common/internal/D;

    move-result-object v0

    const-string/jumbo v1, "websiteUri"

    iget-object v2, p0, Lcom/google/android/gms/internal/nq;->h:Landroid/net/Uri;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/common/internal/D;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/android/gms/common/internal/D;

    move-result-object v0

    const-string/jumbo v1, "isPermanentlyClosed"

    iget-boolean v2, p0, Lcom/google/android/gms/internal/nq;->i:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/common/internal/D;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/android/gms/common/internal/D;

    move-result-object v0

    const-string/jumbo v1, "priceLevel"

    iget v2, p0, Lcom/google/android/gms/internal/nq;->k:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/common/internal/D;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/android/gms/common/internal/D;

    move-result-object v0

    const-string/jumbo v1, "timestampSecs"

    iget-wide v2, p0, Lcom/google/android/gms/internal/nq;->l:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/common/internal/D;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/android/gms/common/internal/D;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/internal/D;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    sget-object v0, Lcom/google/android/gms/internal/nq;->CREATOR:Lcom/google/android/gms/internal/aW;

    invoke-static {p0, p1, p2}, Lcom/google/android/gms/internal/aW;->a(Lcom/google/android/gms/internal/nq;Landroid/os/Parcel;I)V

    return-void
.end method
