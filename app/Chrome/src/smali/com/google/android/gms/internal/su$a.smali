.class public Lcom/google/android/gms/internal/su$a;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Lcom/google/android/gms/internal/bO;


# instance fields
.field public a:Ljava/lang/String;

.field public b:J

.field public c:Z

.field public d:J

.field final e:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/gms/internal/bO;

    invoke-direct {v0}, Lcom/google/android/gms/internal/bO;-><init>()V

    sput-object v0, Lcom/google/android/gms/internal/su$a;->CREATOR:Lcom/google/android/gms/internal/bO;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/internal/su$a;->e:I

    return-void
.end method

.method constructor <init>(ILjava/lang/String;JZJ)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/gms/internal/su$a;->e:I

    iput-object p2, p0, Lcom/google/android/gms/internal/su$a;->a:Ljava/lang/String;

    iput-wide p3, p0, Lcom/google/android/gms/internal/su$a;->b:J

    iput-boolean p5, p0, Lcom/google/android/gms/internal/su$a;->c:Z

    iput-wide p6, p0, Lcom/google/android/gms/internal/su$a;->d:J

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    sget-object v0, Lcom/google/android/gms/internal/su$a;->CREATOR:Lcom/google/android/gms/internal/bO;

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    sget-object v0, Lcom/google/android/gms/internal/su$a;->CREATOR:Lcom/google/android/gms/internal/bO;

    invoke-static {p0, p1}, Lcom/google/android/gms/internal/bO;->a(Lcom/google/android/gms/internal/su$a;Landroid/os/Parcel;)V

    return-void
.end method
