.class public Lcom/google/android/gms/wallet/a;
.super Ljava/lang/Object;


# instance fields
.field final synthetic a:Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;


# direct methods
.method private constructor <init>(Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/wallet/a;->a:Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;B)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/gms/wallet/a;-><init>(Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;)V

    return-void
.end method


# virtual methods
.method public a()Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/wallet/a;->a:Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;

    return-object v0
.end method

.method public a(I)Lcom/google/android/gms/wallet/a;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/wallet/a;->a:Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;

    iput p1, v0, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->a:I

    return-object p0
.end method

.method public a(Landroid/accounts/Account;)Lcom/google/android/gms/wallet/a;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/wallet/a;->a:Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;

    iput-object p1, v0, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->b:Landroid/accounts/Account;

    return-object p0
.end method

.method public a(Ljava/lang/String;)Lcom/google/android/gms/wallet/a;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/wallet/a;->a:Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;

    iput-object p1, v0, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->c:Ljava/lang/String;

    return-object p0
.end method

.method public a(Z)Lcom/google/android/gms/wallet/a;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/wallet/a;->a:Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;

    iput-boolean p1, v0, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->e:Z

    return-object p0
.end method

.method public b(Ljava/lang/String;)Lcom/google/android/gms/wallet/a;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/wallet/a;->a:Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;

    iput-object p1, v0, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->h:Ljava/lang/String;

    return-object p0
.end method

.method public b(Z)Lcom/google/android/gms/wallet/a;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/wallet/a;->a:Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;

    iput-boolean p1, v0, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->f:Z

    return-object p0
.end method

.method public c(Z)Lcom/google/android/gms/wallet/a;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/wallet/a;->a:Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;

    iput-boolean p1, v0, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->g:Z

    return-object p0
.end method

.method public d(Z)Lcom/google/android/gms/wallet/a;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/wallet/a;->a:Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;

    iput-boolean p1, v0, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->i:Z

    return-object p0
.end method
