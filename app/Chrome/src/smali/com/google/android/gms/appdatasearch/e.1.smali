.class public final Lcom/google/android/gms/appdatasearch/e;
.super Ljava/lang/Object;


# instance fields
.field private final a:Lcom/google/android/gms/appdatasearch/f;

.field private final b:I

.field private synthetic c:Lcom/google/android/gms/appdatasearch/SearchResults;


# direct methods
.method constructor <init>(Lcom/google/android/gms/appdatasearch/SearchResults;ILcom/google/android/gms/appdatasearch/f;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/appdatasearch/e;->c:Lcom/google/android/gms/appdatasearch/SearchResults;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p3, p0, Lcom/google/android/gms/appdatasearch/e;->a:Lcom/google/android/gms/appdatasearch/f;

    iput p2, p0, Lcom/google/android/gms/appdatasearch/e;->b:I

    return-void
.end method

.method private d()Ljava/lang/String;
    .locals 3

    iget-object v0, p0, Lcom/google/android/gms/appdatasearch/e;->c:Lcom/google/android/gms/appdatasearch/SearchResults;

    iget-object v0, v0, Lcom/google/android/gms/appdatasearch/SearchResults;->j:[Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/gms/appdatasearch/e;->c:Lcom/google/android/gms/appdatasearch/SearchResults;

    iget-object v1, v1, Lcom/google/android/gms/appdatasearch/SearchResults;->i:[I

    iget v2, p0, Lcom/google/android/gms/appdatasearch/e;->b:I

    aget v1, v1, v2

    aget-object v0, v0, v1

    return-object v0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 3

    invoke-direct {p0}, Lcom/google/android/gms/appdatasearch/e;->d()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    const/16 v2, 0x2d

    invoke-virtual {v0, v2}, Ljava/lang/String;->indexOf(I)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 3

    invoke-direct {p0}, Lcom/google/android/gms/appdatasearch/e;->d()Ljava/lang/String;

    move-result-object v0

    const/16 v1, 0x2d

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 4

    iget-object v0, p0, Lcom/google/android/gms/appdatasearch/e;->a:Lcom/google/android/gms/appdatasearch/f;

    invoke-static {v0}, Lcom/google/android/gms/appdatasearch/f;->a(Lcom/google/android/gms/appdatasearch/f;)Lcom/google/android/gms/appdatasearch/g;

    move-result-object v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/appdatasearch/e;->c:Lcom/google/android/gms/appdatasearch/SearchResults;

    iget-object v0, v0, Lcom/google/android/gms/appdatasearch/SearchResults;->c:[I

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/appdatasearch/e;->c:Lcom/google/android/gms/appdatasearch/SearchResults;

    iget-object v0, v0, Lcom/google/android/gms/appdatasearch/SearchResults;->d:[B

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/appdatasearch/e;->a:Lcom/google/android/gms/appdatasearch/f;

    new-instance v1, Lcom/google/android/gms/appdatasearch/g;

    iget-object v2, p0, Lcom/google/android/gms/appdatasearch/e;->c:Lcom/google/android/gms/appdatasearch/SearchResults;

    iget-object v2, v2, Lcom/google/android/gms/appdatasearch/SearchResults;->c:[I

    iget-object v3, p0, Lcom/google/android/gms/appdatasearch/e;->c:Lcom/google/android/gms/appdatasearch/SearchResults;

    iget-object v3, v3, Lcom/google/android/gms/appdatasearch/SearchResults;->d:[B

    invoke-direct {v1, v2, v3}, Lcom/google/android/gms/appdatasearch/g;-><init>([I[B)V

    invoke-static {v0, v1}, Lcom/google/android/gms/appdatasearch/f;->a(Lcom/google/android/gms/appdatasearch/f;Lcom/google/android/gms/appdatasearch/g;)Lcom/google/android/gms/appdatasearch/g;

    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/appdatasearch/e;->a:Lcom/google/android/gms/appdatasearch/f;

    invoke-static {v0}, Lcom/google/android/gms/appdatasearch/f;->a(Lcom/google/android/gms/appdatasearch/f;)Lcom/google/android/gms/appdatasearch/g;

    move-result-object v0

    iget v1, p0, Lcom/google/android/gms/appdatasearch/e;->b:I

    invoke-virtual {v0, v1}, Lcom/google/android/gms/appdatasearch/g;->a(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
