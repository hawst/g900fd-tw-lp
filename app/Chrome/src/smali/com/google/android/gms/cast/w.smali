.class final Lcom/google/android/gms/cast/w;
.super Lcom/google/android/gms/cast/C;


# instance fields
.field private synthetic c:Lcom/google/android/gms/common/api/i;

.field private synthetic d:Lcom/google/android/gms/cast/u;


# direct methods
.method constructor <init>(Lcom/google/android/gms/cast/u;Lcom/google/android/gms/common/api/i;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/cast/w;->d:Lcom/google/android/gms/cast/u;

    iput-object p2, p0, Lcom/google/android/gms/cast/w;->c:Lcom/google/android/gms/common/api/i;

    invoke-direct {p0}, Lcom/google/android/gms/cast/C;-><init>()V

    return-void
.end method


# virtual methods
.method protected final synthetic a(Lcom/google/android/gms/common/api/b;)V
    .locals 4

    iget-object v0, p0, Lcom/google/android/gms/cast/w;->d:Lcom/google/android/gms/cast/u;

    invoke-static {v0}, Lcom/google/android/gms/cast/u;->b(Lcom/google/android/gms/cast/u;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/cast/w;->d:Lcom/google/android/gms/cast/u;

    invoke-static {v0}, Lcom/google/android/gms/cast/u;->c(Lcom/google/android/gms/cast/u;)Lcom/google/android/gms/cast/A;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/gms/cast/w;->c:Lcom/google/android/gms/common/api/i;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/cast/A;->a(Lcom/google/android/gms/common/api/i;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/cast/w;->d:Lcom/google/android/gms/cast/u;

    invoke-static {v0}, Lcom/google/android/gms/cast/u;->d(Lcom/google/android/gms/cast/u;)Lcom/google/android/gms/internal/r;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/gms/cast/w;->a:Lcom/google/android/gms/internal/u;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/internal/r;->a(Lcom/google/android/gms/internal/u;)J
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :try_start_2
    iget-object v0, p0, Lcom/google/android/gms/cast/w;->d:Lcom/google/android/gms/cast/u;

    invoke-static {v0}, Lcom/google/android/gms/cast/u;->c(Lcom/google/android/gms/cast/u;)Lcom/google/android/gms/cast/A;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcom/google/android/gms/cast/A;->a(Lcom/google/android/gms/common/api/i;)V

    :goto_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    return-void

    :catch_0
    move-exception v0

    :try_start_3
    new-instance v0, Lcom/google/android/gms/common/api/Status;

    const/16 v2, 0x834

    invoke-direct {v0, v2}, Lcom/google/android/gms/common/api/Status;-><init>(I)V

    invoke-virtual {p0, v0}, Lcom/google/android/gms/cast/w;->b(Lcom/google/android/gms/common/api/Status;)Lcom/google/android/gms/common/api/n;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/cast/w;->a(Lcom/google/android/gms/common/api/n;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :try_start_4
    iget-object v0, p0, Lcom/google/android/gms/cast/w;->d:Lcom/google/android/gms/cast/u;

    invoke-static {v0}, Lcom/google/android/gms/cast/u;->c(Lcom/google/android/gms/cast/u;)Lcom/google/android/gms/cast/A;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcom/google/android/gms/cast/A;->a(Lcom/google/android/gms/common/api/i;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :catchall_1
    move-exception v0

    :try_start_5
    iget-object v2, p0, Lcom/google/android/gms/cast/w;->d:Lcom/google/android/gms/cast/u;

    invoke-static {v2}, Lcom/google/android/gms/cast/u;->c(Lcom/google/android/gms/cast/u;)Lcom/google/android/gms/cast/A;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/google/android/gms/cast/A;->a(Lcom/google/android/gms/common/api/i;)V

    throw v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0
.end method
