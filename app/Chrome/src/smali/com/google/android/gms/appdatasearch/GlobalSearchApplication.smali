.class public Lcom/google/android/gms/appdatasearch/GlobalSearchApplication;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Lcom/google/android/gms/appdatasearch/G;


# instance fields
.field final a:I

.field public final b:Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;

.field final c:[Lcom/google/android/gms/appdatasearch/k;

.field public final d:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/gms/appdatasearch/G;

    invoke-direct {v0}, Lcom/google/android/gms/appdatasearch/G;-><init>()V

    sput-object v0, Lcom/google/android/gms/appdatasearch/GlobalSearchApplication;->CREATOR:Lcom/google/android/gms/appdatasearch/G;

    return-void
.end method

.method constructor <init>(ILcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;[Lcom/google/android/gms/appdatasearch/k;Z)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/gms/appdatasearch/GlobalSearchApplication;->a:I

    iput-object p2, p0, Lcom/google/android/gms/appdatasearch/GlobalSearchApplication;->b:Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;

    iput-object p3, p0, Lcom/google/android/gms/appdatasearch/GlobalSearchApplication;->c:[Lcom/google/android/gms/appdatasearch/k;

    iput-boolean p4, p0, Lcom/google/android/gms/appdatasearch/GlobalSearchApplication;->d:Z

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    sget-object v0, Lcom/google/android/gms/appdatasearch/GlobalSearchApplication;->CREATOR:Lcom/google/android/gms/appdatasearch/G;

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    sget-object v0, Lcom/google/android/gms/appdatasearch/GlobalSearchApplication;->CREATOR:Lcom/google/android/gms/appdatasearch/G;

    invoke-static {p0, p1, p2}, Lcom/google/android/gms/appdatasearch/G;->a(Lcom/google/android/gms/appdatasearch/GlobalSearchApplication;Landroid/os/Parcel;I)V

    return-void
.end method
