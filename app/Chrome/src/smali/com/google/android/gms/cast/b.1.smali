.class final Lcom/google/android/gms/cast/b;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/common/api/c;


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final synthetic a(Landroid/content/Context;Landroid/os/Looper;Lcom/google/android/gms/common/internal/ClientSettings;Ljava/lang/Object;Lcom/google/android/gms/common/api/k;Lcom/google/android/gms/common/c;)Lcom/google/android/gms/common/api/b;
    .locals 9

    check-cast p4, Lcom/google/android/gms/cast/h;

    const-string/jumbo v0, "Setting the API options is required."

    invoke-static {p4, v0}, Landroid/support/v4/app/c;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Lcom/google/android/gms/internal/c;

    iget-object v3, p4, Lcom/google/android/gms/cast/h;->a:Lcom/google/android/gms/cast/CastDevice;

    invoke-static {p4}, Lcom/google/android/gms/cast/h;->a(Lcom/google/android/gms/cast/h;)I

    move-result v1

    int-to-long v4, v1

    iget-object v6, p4, Lcom/google/android/gms/cast/h;->b:Lcom/google/android/gms/cast/j;

    move-object v1, p1

    move-object v2, p2

    move-object v7, p5

    move-object v8, p6

    invoke-direct/range {v0 .. v8}, Lcom/google/android/gms/internal/c;-><init>(Landroid/content/Context;Landroid/os/Looper;Lcom/google/android/gms/cast/CastDevice;JLcom/google/android/gms/cast/j;Lcom/google/android/gms/common/api/k;Lcom/google/android/gms/common/c;)V

    return-object v0
.end method
