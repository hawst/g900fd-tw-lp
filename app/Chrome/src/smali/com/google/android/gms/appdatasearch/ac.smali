.class public Lcom/google/android/gms/appdatasearch/ac;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Lcom/google/android/gms/appdatasearch/p;


# instance fields
.field final a:I

.field final b:Ljava/lang/String;

.field final c:[Lcom/google/android/gms/appdatasearch/DocumentId;

.field final d:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/gms/appdatasearch/p;

    invoke-direct {v0}, Lcom/google/android/gms/appdatasearch/p;-><init>()V

    sput-object v0, Lcom/google/android/gms/appdatasearch/ac;->CREATOR:Lcom/google/android/gms/appdatasearch/p;

    return-void
.end method

.method constructor <init>(ILjava/lang/String;[Lcom/google/android/gms/appdatasearch/DocumentId;I)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/gms/appdatasearch/ac;->a:I

    iput-object p2, p0, Lcom/google/android/gms/appdatasearch/ac;->b:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/gms/appdatasearch/ac;->c:[Lcom/google/android/gms/appdatasearch/DocumentId;

    iput p4, p0, Lcom/google/android/gms/appdatasearch/ac;->d:I

    return-void
.end method

.method constructor <init>(Ljava/lang/String;[Lcom/google/android/gms/appdatasearch/DocumentId;I)V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0, p1, p2, p3}, Lcom/google/android/gms/appdatasearch/ac;-><init>(ILjava/lang/String;[Lcom/google/android/gms/appdatasearch/DocumentId;I)V

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    sget-object v0, Lcom/google/android/gms/appdatasearch/ac;->CREATOR:Lcom/google/android/gms/appdatasearch/p;

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    sget-object v0, Lcom/google/android/gms/appdatasearch/ac;->CREATOR:Lcom/google/android/gms/appdatasearch/p;

    invoke-static {p0, p1, p2}, Lcom/google/android/gms/appdatasearch/p;->a(Lcom/google/android/gms/appdatasearch/ac;Landroid/os/Parcel;I)V

    return-void
.end method
