.class public final Lcom/google/android/gms/internal/c;
.super Lcom/google/android/gms/common/internal/d;


# static fields
.field private static final b:Lcom/google/android/gms/internal/q;

.field private static final y:Ljava/lang/Object;

.field private static final z:Ljava/lang/Object;


# instance fields
.field private final c:Lcom/google/android/gms/cast/CastDevice;

.field private final d:Lcom/google/android/gms/cast/j;

.field private final e:Landroid/os/Handler;

.field private final f:Ljava/util/Map;

.field private final g:J

.field private h:Lcom/google/android/gms/internal/g;

.field private i:Ljava/lang/String;

.field private j:Z

.field private k:Z

.field private l:Z

.field private m:Z

.field private n:D

.field private o:I

.field private p:I

.field private final q:Ljava/util/concurrent/atomic/AtomicLong;

.field private r:Ljava/lang/String;

.field private s:Ljava/lang/String;

.field private t:Landroid/os/Bundle;

.field private u:Ljava/util/Map;

.field private v:Lcom/google/android/gms/internal/f;

.field private w:Lcom/google/android/gms/common/api/h;

.field private x:Lcom/google/android/gms/common/api/h;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/google/android/gms/internal/q;

    const-string/jumbo v1, "CastClientImpl"

    invoke-direct {v0, v1}, Lcom/google/android/gms/internal/q;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/internal/c;->b:Lcom/google/android/gms/internal/q;

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/google/android/gms/internal/c;->y:Ljava/lang/Object;

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/google/android/gms/internal/c;->z:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/os/Looper;Lcom/google/android/gms/cast/CastDevice;JLcom/google/android/gms/cast/j;Lcom/google/android/gms/common/api/k;Lcom/google/android/gms/common/c;)V
    .locals 6

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p7

    move-object v4, p8

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/common/internal/d;-><init>(Landroid/content/Context;Landroid/os/Looper;Lcom/google/android/gms/common/api/k;Lcom/google/android/gms/common/c;[Ljava/lang/String;)V

    iput-object p3, p0, Lcom/google/android/gms/internal/c;->c:Lcom/google/android/gms/cast/CastDevice;

    iput-object p6, p0, Lcom/google/android/gms/internal/c;->d:Lcom/google/android/gms/cast/j;

    iput-wide p4, p0, Lcom/google/android/gms/internal/c;->g:J

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/google/android/gms/internal/c;->e:Landroid/os/Handler;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/internal/c;->f:Ljava/util/Map;

    new-instance v0, Ljava/util/concurrent/atomic/AtomicLong;

    const-wide/16 v2, 0x0

    invoke-direct {v0, v2, v3}, Ljava/util/concurrent/atomic/AtomicLong;-><init>(J)V

    iput-object v0, p0, Lcom/google/android/gms/internal/c;->q:Ljava/util/concurrent/atomic/AtomicLong;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/internal/c;->u:Ljava/util/Map;

    invoke-direct {p0}, Lcom/google/android/gms/internal/c;->n()V

    new-instance v0, Lcom/google/android/gms/internal/f;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/gms/internal/f;-><init>(Lcom/google/android/gms/internal/c;B)V

    iput-object v0, p0, Lcom/google/android/gms/internal/c;->v:Lcom/google/android/gms/internal/f;

    iget-object v0, p0, Lcom/google/android/gms/internal/c;->v:Lcom/google/android/gms/internal/f;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/internal/c;->a(Lcom/google/android/gms/common/c;)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/internal/c;Lcom/google/android/gms/cast/ApplicationMetadata;)Lcom/google/android/gms/cast/ApplicationMetadata;
    .locals 0

    return-object p1
.end method

.method static synthetic a(Lcom/google/android/gms/internal/c;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/internal/c;->r:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic a(Lcom/google/android/gms/internal/c;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/gms/internal/c;->o()V

    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/internal/c;Lcom/google/android/gms/internal/ih;)V
    .locals 7

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {p1}, Lcom/google/android/gms/internal/ih;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v3, p0, Lcom/google/android/gms/internal/c;->i:Ljava/lang/String;

    invoke-static {v0, v3}, Landroid/support/v4/app/b;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    iput-object v0, p0, Lcom/google/android/gms/internal/c;->i:Ljava/lang/String;

    move v0, v1

    :goto_0
    sget-object v3, Lcom/google/android/gms/internal/c;->b:Lcom/google/android/gms/internal/q;

    const-string/jumbo v4, "hasChanged=%b, mFirstApplicationStatusUpdate=%b"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    aput-object v6, v5, v2

    iget-boolean v6, p0, Lcom/google/android/gms/internal/c;->k:Z

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    aput-object v6, v5, v1

    invoke-virtual {v3, v4, v5}, Lcom/google/android/gms/internal/q;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v1, p0, Lcom/google/android/gms/internal/c;->d:Lcom/google/android/gms/cast/j;

    if-eqz v1, :cond_1

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/gms/internal/c;->k:Z

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/internal/c;->d:Lcom/google/android/gms/cast/j;

    invoke-virtual {v0}, Lcom/google/android/gms/cast/j;->onApplicationStatusChanged()V

    :cond_1
    iput-boolean v2, p0, Lcom/google/android/gms/internal/c;->k:Z

    return-void

    :cond_2
    move v0, v2

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/gms/internal/c;Lcom/google/android/gms/internal/im;)V
    .locals 9

    const/4 v8, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {p1}, Lcom/google/android/gms/internal/im;->f()Lcom/google/android/gms/cast/ApplicationMetadata;

    invoke-virtual {p1}, Lcom/google/android/gms/internal/im;->b()D

    move-result-wide v4

    const-wide/high16 v6, 0x7ff8000000000000L    # NaN

    cmpl-double v0, v4, v6

    if-eqz v0, :cond_9

    iget-wide v6, p0, Lcom/google/android/gms/internal/c;->n:D

    cmpl-double v0, v4, v6

    if-eqz v0, :cond_9

    iput-wide v4, p0, Lcom/google/android/gms/internal/c;->n:D

    move v0, v1

    :goto_0
    invoke-virtual {p1}, Lcom/google/android/gms/internal/im;->c()Z

    move-result v3

    iget-boolean v4, p0, Lcom/google/android/gms/internal/c;->j:Z

    if-eq v3, v4, :cond_0

    iput-boolean v3, p0, Lcom/google/android/gms/internal/c;->j:Z

    move v0, v1

    :cond_0
    sget-object v3, Lcom/google/android/gms/internal/c;->b:Lcom/google/android/gms/internal/q;

    const-string/jumbo v4, "hasVolumeChanged=%b, mFirstDeviceStatusUpdate=%b"

    new-array v5, v8, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    aput-object v6, v5, v2

    iget-boolean v6, p0, Lcom/google/android/gms/internal/c;->l:Z

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    aput-object v6, v5, v1

    invoke-virtual {v3, v4, v5}, Lcom/google/android/gms/internal/q;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v3, p0, Lcom/google/android/gms/internal/c;->d:Lcom/google/android/gms/cast/j;

    if-eqz v3, :cond_2

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/google/android/gms/internal/c;->l:Z

    if-eqz v0, :cond_2

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/internal/c;->d:Lcom/google/android/gms/cast/j;

    invoke-virtual {v0}, Lcom/google/android/gms/cast/j;->onVolumeChanged()V

    :cond_2
    invoke-virtual {p1}, Lcom/google/android/gms/internal/im;->d()I

    move-result v0

    iget v3, p0, Lcom/google/android/gms/internal/c;->o:I

    if-eq v0, v3, :cond_8

    iput v0, p0, Lcom/google/android/gms/internal/c;->o:I

    move v0, v1

    :goto_1
    sget-object v3, Lcom/google/android/gms/internal/c;->b:Lcom/google/android/gms/internal/q;

    const-string/jumbo v4, "hasActiveInputChanged=%b, mFirstDeviceStatusUpdate=%b"

    new-array v5, v8, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    aput-object v6, v5, v2

    iget-boolean v6, p0, Lcom/google/android/gms/internal/c;->l:Z

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    aput-object v6, v5, v1

    invoke-virtual {v3, v4, v5}, Lcom/google/android/gms/internal/q;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v3, p0, Lcom/google/android/gms/internal/c;->d:Lcom/google/android/gms/cast/j;

    if-eqz v3, :cond_4

    if-nez v0, :cond_3

    iget-boolean v0, p0, Lcom/google/android/gms/internal/c;->l:Z

    if-eqz v0, :cond_4

    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/internal/c;->d:Lcom/google/android/gms/cast/j;

    iget v3, p0, Lcom/google/android/gms/internal/c;->o:I

    invoke-virtual {v0, v3}, Lcom/google/android/gms/cast/j;->onActiveInputStateChanged(I)V

    :cond_4
    invoke-virtual {p1}, Lcom/google/android/gms/internal/im;->e()I

    move-result v0

    iget v3, p0, Lcom/google/android/gms/internal/c;->p:I

    if-eq v0, v3, :cond_7

    iput v0, p0, Lcom/google/android/gms/internal/c;->p:I

    move v0, v1

    :goto_2
    sget-object v3, Lcom/google/android/gms/internal/c;->b:Lcom/google/android/gms/internal/q;

    const-string/jumbo v4, "hasStandbyStateChanged=%b, mFirstDeviceStatusUpdate=%b"

    new-array v5, v8, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    aput-object v6, v5, v2

    iget-boolean v6, p0, Lcom/google/android/gms/internal/c;->l:Z

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    aput-object v6, v5, v1

    invoke-virtual {v3, v4, v5}, Lcom/google/android/gms/internal/q;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v1, p0, Lcom/google/android/gms/internal/c;->d:Lcom/google/android/gms/cast/j;

    if-eqz v1, :cond_6

    if-nez v0, :cond_5

    iget-boolean v0, p0, Lcom/google/android/gms/internal/c;->l:Z

    if-eqz v0, :cond_6

    :cond_5
    iget-object v0, p0, Lcom/google/android/gms/internal/c;->d:Lcom/google/android/gms/cast/j;

    iget v1, p0, Lcom/google/android/gms/internal/c;->p:I

    invoke-virtual {v0, v1}, Lcom/google/android/gms/cast/j;->onStandbyStateChanged(I)V

    :cond_6
    iput-boolean v2, p0, Lcom/google/android/gms/internal/c;->l:Z

    return-void

    :cond_7
    move v0, v2

    goto :goto_2

    :cond_8
    move v0, v2

    goto :goto_1

    :cond_9
    move v0, v2

    goto/16 :goto_0
.end method

.method static synthetic b(Lcom/google/android/gms/internal/c;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/internal/c;->s:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic b(Lcom/google/android/gms/internal/c;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/gms/internal/c;->n()V

    return-void
.end method

.method static synthetic c(Lcom/google/android/gms/internal/c;)Lcom/google/android/gms/common/api/h;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/internal/c;->w:Lcom/google/android/gms/common/api/h;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/gms/internal/c;)Lcom/google/android/gms/common/api/h;
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/internal/c;->w:Lcom/google/android/gms/common/api/h;

    return-object v0
.end method

.method static synthetic e(Lcom/google/android/gms/internal/c;)Lcom/google/android/gms/cast/j;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/internal/c;->d:Lcom/google/android/gms/cast/j;

    return-object v0
.end method

.method static synthetic f(Lcom/google/android/gms/internal/c;)Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/internal/c;->e:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic g(Lcom/google/android/gms/internal/c;)Ljava/util/Map;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/internal/c;->f:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic h(Lcom/google/android/gms/internal/c;)Lcom/google/android/gms/cast/CastDevice;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/internal/c;->c:Lcom/google/android/gms/cast/CastDevice;

    return-object v0
.end method

.method static synthetic i(Lcom/google/android/gms/internal/c;)Ljava/util/Map;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/internal/c;->u:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic j(Lcom/google/android/gms/internal/c;)Lcom/google/android/gms/common/api/h;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/internal/c;->x:Lcom/google/android/gms/common/api/h;

    return-object v0
.end method

.method static synthetic k(Lcom/google/android/gms/internal/c;)Lcom/google/android/gms/common/api/h;
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/internal/c;->x:Lcom/google/android/gms/common/api/h;

    return-object v0
.end method

.method static synthetic k()Lcom/google/android/gms/internal/q;
    .locals 1

    sget-object v0, Lcom/google/android/gms/internal/c;->b:Lcom/google/android/gms/internal/q;

    return-object v0
.end method

.method static synthetic l()Ljava/lang/Object;
    .locals 1

    sget-object v0, Lcom/google/android/gms/internal/c;->y:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic m()Ljava/lang/Object;
    .locals 1

    sget-object v0, Lcom/google/android/gms/internal/c;->z:Ljava/lang/Object;

    return-object v0
.end method

.method private n()V
    .locals 3

    const/4 v2, 0x0

    const/4 v0, -0x1

    iput-boolean v2, p0, Lcom/google/android/gms/internal/c;->m:Z

    iput v0, p0, Lcom/google/android/gms/internal/c;->o:I

    iput v0, p0, Lcom/google/android/gms/internal/c;->p:I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/internal/c;->i:Ljava/lang/String;

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/gms/internal/c;->n:D

    iput-boolean v2, p0, Lcom/google/android/gms/internal/c;->j:Z

    return-void
.end method

.method private o()V
    .locals 3

    sget-object v0, Lcom/google/android/gms/internal/c;->b:Lcom/google/android/gms/internal/q;

    const-string/jumbo v1, "removing all MessageReceivedCallbacks"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/internal/q;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v1, p0, Lcom/google/android/gms/internal/c;->f:Ljava/util/Map;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/internal/c;->f:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private p()V
    .locals 2

    iget-boolean v0, p0, Lcom/google/android/gms/internal/c;->m:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/internal/c;->h:Lcom/google/android/gms/internal/g;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/internal/c;->h:Lcom/google/android/gms/internal/g;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/g;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "Not connected to a device"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    return-void
.end method


# virtual methods
.method protected final synthetic a(Landroid/os/IBinder;)Landroid/os/IInterface;
    .locals 1

    invoke-static {p1}, Lcom/google/android/gms/internal/n;->a(Landroid/os/IBinder;)Lcom/google/android/gms/internal/m;

    move-result-object v0

    return-object v0
.end method

.method public final a(D)V
    .locals 7

    invoke-static {p1, p2}, Ljava/lang/Double;->isInfinite(D)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p1, p2}, Ljava/lang/Double;->isNaN(D)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "Volume cannot be "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/internal/c;->i()Landroid/os/IInterface;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/internal/m;

    iget-wide v4, p0, Lcom/google/android/gms/internal/c;->n:D

    iget-boolean v6, p0, Lcom/google/android/gms/internal/c;->j:Z

    move-wide v2, p1

    invoke-interface/range {v1 .. v6}, Lcom/google/android/gms/internal/m;->a(DDZ)V

    return-void
.end method

.method protected final a(ILandroid/os/IBinder;Landroid/os/Bundle;)V
    .locals 7

    const/16 v6, 0x3e9

    const/4 v0, 0x0

    const/4 v5, 0x1

    sget-object v1, Lcom/google/android/gms/internal/c;->b:Lcom/google/android/gms/internal/q;

    const-string/jumbo v2, "in onPostInitHandler; statusCode=%d"

    new-array v3, v5, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v0

    invoke-virtual {v1, v2, v3}, Lcom/google/android/gms/internal/q;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    if-eqz p1, :cond_0

    if-ne p1, v6, :cond_2

    :cond_0
    iput-boolean v5, p0, Lcom/google/android/gms/internal/c;->m:Z

    iput-boolean v5, p0, Lcom/google/android/gms/internal/c;->k:Z

    iput-boolean v5, p0, Lcom/google/android/gms/internal/c;->l:Z

    :goto_0
    if-ne p1, v6, :cond_1

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    iput-object v1, p0, Lcom/google/android/gms/internal/c;->t:Landroid/os/Bundle;

    iget-object v1, p0, Lcom/google/android/gms/internal/c;->t:Landroid/os/Bundle;

    const-string/jumbo v2, "com.google.android.gms.cast.EXTRA_APP_NO_LONGER_RUNNING"

    invoke-virtual {v1, v2, v5}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    move p1, v0

    :cond_1
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/gms/common/internal/d;->a(ILandroid/os/IBinder;Landroid/os/Bundle;)V

    return-void

    :cond_2
    iput-boolean v0, p0, Lcom/google/android/gms/internal/c;->m:Z

    goto :goto_0
.end method

.method protected final a(Lcom/google/android/gms/common/internal/A;Lcom/google/android/gms/common/internal/h;)V
    .locals 7

    const/4 v6, 0x0

    new-instance v5, Landroid/os/Bundle;

    invoke-direct {v5}, Landroid/os/Bundle;-><init>()V

    sget-object v0, Lcom/google/android/gms/internal/c;->b:Lcom/google/android/gms/internal/q;

    const-string/jumbo v1, "getServiceFromBroker(): mLastApplicationId=%s, mLastSessionId=%s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/google/android/gms/internal/c;->r:Ljava/lang/String;

    aput-object v3, v2, v6

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/google/android/gms/internal/c;->s:Ljava/lang/String;

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/internal/q;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/gms/internal/c;->c:Lcom/google/android/gms/cast/CastDevice;

    invoke-virtual {v0, v5}, Lcom/google/android/gms/cast/CastDevice;->a(Landroid/os/Bundle;)V

    const-string/jumbo v0, "com.google.android.gms.cast.EXTRA_CAST_FLAGS"

    iget-wide v2, p0, Lcom/google/android/gms/internal/c;->g:J

    invoke-virtual {v5, v0, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    iget-object v0, p0, Lcom/google/android/gms/internal/c;->r:Ljava/lang/String;

    if-eqz v0, :cond_0

    const-string/jumbo v0, "last_application_id"

    iget-object v1, p0, Lcom/google/android/gms/internal/c;->r:Ljava/lang/String;

    invoke-virtual {v5, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/gms/internal/c;->s:Ljava/lang/String;

    if-eqz v0, :cond_0

    const-string/jumbo v0, "last_session_id"

    iget-object v1, p0, Lcom/google/android/gms/internal/c;->s:Ljava/lang/String;

    invoke-virtual {v5, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    new-instance v0, Lcom/google/android/gms/internal/g;

    invoke-direct {v0, p0, v6}, Lcom/google/android/gms/internal/g;-><init>(Lcom/google/android/gms/internal/c;B)V

    iput-object v0, p0, Lcom/google/android/gms/internal/c;->h:Lcom/google/android/gms/internal/g;

    const v2, 0x5d2f78

    invoke-virtual {p0}, Lcom/google/android/gms/internal/c;->h()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    iget-object v0, p0, Lcom/google/android/gms/internal/c;->h:Lcom/google/android/gms/internal/g;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/g;->asBinder()Landroid/os/IBinder;

    move-result-object v4

    move-object v0, p1

    move-object v1, p2

    invoke-interface/range {v0 .. v5}, Lcom/google/android/gms/common/internal/A;->a(Lcom/google/android/gms/common/internal/x;ILjava/lang/String;Landroid/os/IBinder;Landroid/os/Bundle;)V

    return-void
.end method

.method public final a(Ljava/lang/String;Lcom/google/android/gms/cast/LaunchOptions;Lcom/google/android/gms/common/api/h;)V
    .locals 5

    sget-object v1, Lcom/google/android/gms/internal/c;->y:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/internal/c;->w:Lcom/google/android/gms/common/api/h;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/internal/c;->w:Lcom/google/android/gms/common/api/h;

    new-instance v2, Lcom/google/android/gms/internal/e;

    new-instance v3, Lcom/google/android/gms/common/api/Status;

    const/16 v4, 0x7d2

    invoke-direct {v3, v4}, Lcom/google/android/gms/common/api/Status;-><init>(I)V

    invoke-direct {v2, v3}, Lcom/google/android/gms/internal/e;-><init>(Lcom/google/android/gms/common/api/Status;)V

    invoke-interface {v0, v2}, Lcom/google/android/gms/common/api/h;->a(Ljava/lang/Object;)V

    :cond_0
    iput-object p3, p0, Lcom/google/android/gms/internal/c;->w:Lcom/google/android/gms/common/api/h;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {p0}, Lcom/google/android/gms/internal/c;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/internal/m;

    invoke-interface {v0, p1, p2}, Lcom/google/android/gms/internal/m;->a(Ljava/lang/String;Lcom/google/android/gms/cast/LaunchOptions;)V

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final a(Ljava/lang/String;Lcom/google/android/gms/cast/k;)V
    .locals 6

    invoke-static {p1}, Landroid/support/v4/app/b;->b(Ljava/lang/String;)V

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "Channel namespace cannot be null or empty"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/internal/c;->f:Ljava/util/Map;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/internal/c;->f:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/cast/k;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_1

    :try_start_1
    invoke-virtual {p0}, Lcom/google/android/gms/internal/c;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/internal/m;

    invoke-interface {v0, p1}, Lcom/google/android/gms/internal/m;->c(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_0

    :cond_1
    :goto_0
    if-eqz p2, :cond_2

    iget-object v1, p0, Lcom/google/android/gms/internal/c;->f:Ljava/util/Map;

    monitor-enter v1

    :try_start_2
    iget-object v0, p0, Lcom/google/android/gms/internal/c;->f:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    invoke-virtual {p0}, Lcom/google/android/gms/internal/c;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/internal/m;

    invoke-interface {v0, p1}, Lcom/google/android/gms/internal/m;->b(Ljava/lang/String;)V

    :cond_2
    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :catch_0
    move-exception v0

    sget-object v1, Lcom/google/android/gms/internal/c;->b:Lcom/google/android/gms/internal/q;

    const-string/jumbo v2, "Error unregistering namespace (%s): %s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    const/4 v4, 0x1

    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->getMessage()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v1, v0, v2, v3}, Lcom/google/android/gms/internal/q;->a(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :catchall_1
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final a(Ljava/lang/String;Lcom/google/android/gms/common/api/h;)V
    .locals 3

    sget-object v1, Lcom/google/android/gms/internal/c;->z:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/internal/c;->x:Lcom/google/android/gms/common/api/h;

    if-eqz v0, :cond_0

    new-instance v0, Lcom/google/android/gms/common/api/Status;

    const/16 v2, 0x7d1

    invoke-direct {v0, v2}, Lcom/google/android/gms/common/api/Status;-><init>(I)V

    invoke-interface {p2, v0}, Lcom/google/android/gms/common/api/h;->a(Ljava/lang/Object;)V

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    invoke-virtual {p0}, Lcom/google/android/gms/internal/c;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/internal/m;

    invoke-interface {v0, p1}, Lcom/google/android/gms/internal/m;->a(Ljava/lang/String;)V

    return-void

    :cond_0
    :try_start_1
    iput-object p2, p0, Lcom/google/android/gms/internal/c;->x:Lcom/google/android/gms/common/api/h;

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/common/api/h;)V
    .locals 4

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "The message payload cannot be null or empty"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v0

    const/high16 v1, 0x10000

    if-le v0, v1, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "Message exceeds maximum size"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    invoke-static {p1}, Landroid/support/v4/app/b;->b(Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/google/android/gms/internal/c;->p()V

    iget-object v0, p0, Lcom/google/android/gms/internal/c;->q:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicLong;->incrementAndGet()J

    move-result-wide v2

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/internal/c;->u:Ljava/util/Map;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1, p3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/google/android/gms/internal/c;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/internal/m;

    invoke-interface {v0, p1, p2, v2, v3}, Lcom/google/android/gms/internal/m;->a(Ljava/lang/String;Ljava/lang/String;J)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/gms/internal/c;->u:Ljava/util/Map;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    throw v0
.end method

.method public final a_()Landroid/os/Bundle;
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/internal/c;->t:Landroid/os/Bundle;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/internal/c;->t:Landroid/os/Bundle;

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/gms/internal/c;->t:Landroid/os/Bundle;

    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0}, Lcom/google/android/gms/common/internal/d;->a_()Landroid/os/Bundle;

    move-result-object v0

    goto :goto_0
.end method

.method public final b()V
    .locals 6

    const/4 v5, 0x1

    const/4 v4, 0x0

    sget-object v0, Lcom/google/android/gms/internal/c;->b:Lcom/google/android/gms/internal/q;

    const-string/jumbo v1, "disconnect(); ServiceListener=%s, isConnected=%b"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/google/android/gms/internal/c;->h:Lcom/google/android/gms/internal/g;

    aput-object v3, v2, v4

    invoke-virtual {p0}, Lcom/google/android/gms/internal/c;->c()Z

    move-result v3

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/internal/q;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/gms/internal/c;->h:Lcom/google/android/gms/internal/g;

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/gms/internal/c;->h:Lcom/google/android/gms/internal/g;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/gms/internal/g;->a()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    sget-object v0, Lcom/google/android/gms/internal/c;->b:Lcom/google/android/gms/internal/q;

    const-string/jumbo v1, "already disposed, so short-circuiting"

    new-array v2, v4, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/internal/q;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    :goto_0
    return-void

    :cond_1
    invoke-direct {p0}, Lcom/google/android/gms/internal/c;->o()V

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/internal/c;->c()Z

    move-result v0

    if-nez v0, :cond_2

    invoke-virtual {p0}, Lcom/google/android/gms/internal/c;->g()Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/gms/internal/c;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/internal/m;

    invoke-interface {v0}, Lcom/google/android/gms/internal/m;->a()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_3
    invoke-super {p0}, Lcom/google/android/gms/common/internal/d;->b()V

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_1
    sget-object v1, Lcom/google/android/gms/internal/c;->b:Lcom/google/android/gms/internal/q;

    const-string/jumbo v2, "Error while disconnecting the controller interface: %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v1, v0, v2, v3}, Lcom/google/android/gms/internal/q;->a(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-super {p0}, Lcom/google/android/gms/common/internal/d;->b()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-super {p0}, Lcom/google/android/gms/common/internal/d;->b()V

    throw v0
.end method

.method protected final e()Ljava/lang/String;
    .locals 1

    const-string/jumbo v0, "com.google.android.gms.cast.service.BIND_CAST_DEVICE_CONTROLLER_SERVICE"

    return-object v0
.end method

.method protected final f()Ljava/lang/String;
    .locals 1

    const-string/jumbo v0, "com.google.android.gms.cast.internal.ICastDeviceController"

    return-object v0
.end method

.method public final j()D
    .locals 2

    invoke-direct {p0}, Lcom/google/android/gms/internal/c;->p()V

    iget-wide v0, p0, Lcom/google/android/gms/internal/c;->n:D

    return-wide v0
.end method
