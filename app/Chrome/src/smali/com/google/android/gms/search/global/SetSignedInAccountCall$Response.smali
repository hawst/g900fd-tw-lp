.class public Lcom/google/android/gms/search/global/SetSignedInAccountCall$Response;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/common/api/n;
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Lcom/google/android/gms/search/global/m;


# instance fields
.field public a:Lcom/google/android/gms/common/api/Status;

.field final b:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/gms/search/global/m;

    invoke-direct {v0}, Lcom/google/android/gms/search/global/m;-><init>()V

    sput-object v0, Lcom/google/android/gms/search/global/SetSignedInAccountCall$Response;->CREATOR:Lcom/google/android/gms/search/global/m;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/search/global/SetSignedInAccountCall$Response;->b:I

    return-void
.end method

.method constructor <init>(ILcom/google/android/gms/common/api/Status;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/gms/search/global/SetSignedInAccountCall$Response;->b:I

    iput-object p2, p0, Lcom/google/android/gms/search/global/SetSignedInAccountCall$Response;->a:Lcom/google/android/gms/common/api/Status;

    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/gms/common/api/Status;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/search/global/SetSignedInAccountCall$Response;->a:Lcom/google/android/gms/common/api/Status;

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    sget-object v0, Lcom/google/android/gms/search/global/SetSignedInAccountCall$Response;->CREATOR:Lcom/google/android/gms/search/global/m;

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    sget-object v0, Lcom/google/android/gms/search/global/SetSignedInAccountCall$Response;->CREATOR:Lcom/google/android/gms/search/global/m;

    invoke-static {p0, p1, p2}, Lcom/google/android/gms/search/global/m;->a(Lcom/google/android/gms/search/global/SetSignedInAccountCall$Response;Landroid/os/Parcel;I)V

    return-void
.end method
