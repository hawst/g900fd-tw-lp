.class public abstract Lcom/google/android/gms/common/api/g;
.super Lcom/google/android/gms/common/api/e;

# interfaces
.implements Lcom/google/android/gms/common/api/w;


# instance fields
.field private final a:Lcom/google/android/gms/common/api/d;

.field private c:Lcom/google/android/gms/common/api/u;


# direct methods
.method protected constructor <init>(Lcom/google/android/gms/common/api/d;)V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/gms/common/api/e;-><init>()V

    invoke-static {p1}, Landroid/support/v4/app/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/api/d;

    iput-object v0, p0, Lcom/google/android/gms/common/api/g;->a:Lcom/google/android/gms/common/api/d;

    return-void
.end method

.method private a(Landroid/os/RemoteException;)V
    .locals 4

    new-instance v0, Lcom/google/android/gms/common/api/Status;

    const/16 v1, 0x8

    invoke-virtual {p1}, Landroid/os/RemoteException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/gms/common/api/Status;-><init>(ILjava/lang/String;Landroid/app/PendingIntent;)V

    invoke-virtual {p0, v0}, Lcom/google/android/gms/common/api/g;->c(Lcom/google/android/gms/common/api/Status;)V

    return-void
.end method


# virtual methods
.method protected abstract a(Lcom/google/android/gms/common/api/b;)V
.end method

.method public final a(Lcom/google/android/gms/common/api/u;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/common/api/g;->c:Lcom/google/android/gms/common/api/u;

    return-void
.end method

.method public final b(Lcom/google/android/gms/common/api/b;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/common/api/g;->b:Lcom/google/android/gms/common/api/f;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/gms/common/api/f;

    invoke-interface {p1}, Lcom/google/android/gms/common/api/b;->d()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/gms/common/api/f;-><init>(Landroid/os/Looper;)V

    invoke-virtual {p0, v0}, Lcom/google/android/gms/common/api/g;->a(Lcom/google/android/gms/common/api/f;)V

    :cond_0
    :try_start_0
    invoke-virtual {p0, p1}, Lcom/google/android/gms/common/api/g;->a(Lcom/google/android/gms/common/api/b;)V
    :try_end_0
    .catch Landroid/os/DeadObjectException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_1

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/common/api/g;->a(Landroid/os/RemoteException;)V

    throw v0

    :catch_1
    move-exception v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/common/api/g;->a(Landroid/os/RemoteException;)V

    goto :goto_0
.end method

.method protected final c()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/gms/common/api/e;->c()V

    iget-object v0, p0, Lcom/google/android/gms/common/api/g;->c:Lcom/google/android/gms/common/api/u;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/common/api/g;->c:Lcom/google/android/gms/common/api/u;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/common/api/u;->a(Lcom/google/android/gms/common/api/w;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/common/api/g;->c:Lcom/google/android/gms/common/api/u;

    :cond_0
    return-void
.end method

.method public final c(Lcom/google/android/gms/common/api/Status;)V
    .locals 2

    invoke-virtual {p1}, Lcom/google/android/gms/common/api/Status;->e()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string/jumbo v1, "Failed result must not be success"

    invoke-static {v0, v1}, Landroid/support/v4/app/c;->b(ZLjava/lang/Object;)V

    invoke-virtual {p0, p1}, Lcom/google/android/gms/common/api/g;->a(Lcom/google/android/gms/common/api/Status;)Lcom/google/android/gms/common/api/n;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/common/api/g;->a(Lcom/google/android/gms/common/api/n;)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()Lcom/google/android/gms/common/api/d;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/common/api/g;->a:Lcom/google/android/gms/common/api/d;

    return-object v0
.end method
