.class final Lcom/google/android/gms/cast/A;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/internal/t;


# instance fields
.field final synthetic a:Lcom/google/android/gms/cast/u;

.field private b:Lcom/google/android/gms/common/api/i;

.field private c:J


# direct methods
.method public constructor <init>(Lcom/google/android/gms/cast/u;)V
    .locals 2

    iput-object p1, p0, Lcom/google/android/gms/cast/A;->a:Lcom/google/android/gms/cast/u;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/gms/cast/A;->c:J

    return-void
.end method


# virtual methods
.method public final a()J
    .locals 4

    iget-wide v0, p0, Lcom/google/android/gms/cast/A;->c:J

    const-wide/16 v2, 0x1

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/google/android/gms/cast/A;->c:J

    return-wide v0
.end method

.method public final a(Lcom/google/android/gms/common/api/i;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/cast/A;->b:Lcom/google/android/gms/common/api/i;

    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;J)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/gms/cast/A;->b:Lcom/google/android/gms/common/api/i;

    if-nez v0, :cond_0

    new-instance v0, Ljava/io/IOException;

    const-string/jumbo v1, "No GoogleApiClient available"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    sget-object v0, Lcom/google/android/gms/cast/a;->c:Lcom/google/android/gms/cast/d;

    iget-object v1, p0, Lcom/google/android/gms/cast/A;->b:Lcom/google/android/gms/common/api/i;

    invoke-virtual {v0, v1, p1, p2}, Lcom/google/android/gms/cast/d;->a(Lcom/google/android/gms/common/api/i;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/api/l;

    move-result-object v0

    new-instance v1, Lcom/google/android/gms/cast/B;

    invoke-direct {v1, p0, p3, p4}, Lcom/google/android/gms/cast/B;-><init>(Lcom/google/android/gms/cast/A;J)V

    invoke-interface {v0, v1}, Lcom/google/android/gms/common/api/l;->a(Lcom/google/android/gms/common/api/o;)V

    return-void
.end method
