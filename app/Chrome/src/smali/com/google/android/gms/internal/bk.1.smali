.class public final Lcom/google/android/gms/internal/bk;
.super Lcom/google/android/gms/internal/aY;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/common/api/k;Lcom/google/android/gms/common/c;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/gms/internal/aY;-><init>(Landroid/content/Context;Lcom/google/android/gms/common/api/k;Lcom/google/android/gms/common/c;)V

    return-void
.end method


# virtual methods
.method protected final synthetic a(Landroid/os/IBinder;)Landroid/os/IInterface;
    .locals 1

    invoke-static {p1}, Lcom/google/android/gms/internal/bq;->a(Landroid/os/IBinder;)Lcom/google/android/gms/internal/bp;

    move-result-object v0

    return-object v0
.end method

.method protected final a(Lcom/google/android/gms/common/internal/A;Lcom/google/android/gms/common/internal/h;)V
    .locals 2

    const v0, 0x5d2f78

    invoke-virtual {p0}, Lcom/google/android/gms/internal/bk;->h()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, p2, v0, v1}, Lcom/google/android/gms/common/internal/A;->g(Lcom/google/android/gms/common/internal/x;ILjava/lang/String;)V

    return-void
.end method

.method protected final f()Ljava/lang/String;
    .locals 1

    const-string/jumbo v0, "com.google.android.gms.search.global.internal.IGlobalSearchAdminService"

    return-object v0
.end method
