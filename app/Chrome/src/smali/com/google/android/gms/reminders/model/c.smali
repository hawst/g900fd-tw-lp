.class public final Lcom/google/android/gms/reminders/model/c;
.super Ljava/lang/Object;


# instance fields
.field private a:Ljava/lang/Long;

.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/gms/reminders/model/TaskId;
    .locals 4

    new-instance v0, Lcom/google/android/gms/internal/sp;

    iget-object v1, p0, Lcom/google/android/gms/reminders/model/c;->a:Ljava/lang/Long;

    iget-object v2, p0, Lcom/google/android/gms/reminders/model/c;->b:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/gms/reminders/model/c;->c:Ljava/lang/String;

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/gms/internal/sp;-><init>(Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public final a(Ljava/lang/Long;)Lcom/google/android/gms/reminders/model/c;
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/reminders/model/c;->a:Ljava/lang/Long;

    return-object p0
.end method

.method public final a(Ljava/lang/String;)Lcom/google/android/gms/reminders/model/c;
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/reminders/model/c;->b:Ljava/lang/String;

    return-object p0
.end method

.method public final b(Ljava/lang/String;)Lcom/google/android/gms/reminders/model/c;
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/reminders/model/c;->c:Ljava/lang/String;

    return-object p0
.end method
