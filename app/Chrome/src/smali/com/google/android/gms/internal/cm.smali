.class public final Lcom/google/android/gms/internal/cm;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/os/Parcelable$Creator;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static a(Lcom/google/android/gms/internal/fp;Landroid/os/Parcel;)V
    .locals 5

    const/4 v4, 0x0

    invoke-static {p1}, Landroid/support/v4/app/b;->b(Landroid/os/Parcel;)I

    move-result v0

    const/4 v1, 0x1

    iget v2, p0, Lcom/google/android/gms/internal/fp;->a:I

    invoke-static {p1, v1, v2}, Landroid/support/v4/app/b;->a(Landroid/os/Parcel;II)V

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/gms/internal/fp;->b:Ljava/lang/String;

    invoke-static {p1, v1, v2, v4}, Landroid/support/v4/app/b;->a(Landroid/os/Parcel;ILjava/lang/String;Z)V

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/gms/internal/fp;->c:Ljava/lang/String;

    invoke-static {p1, v1, v2, v4}, Landroid/support/v4/app/b;->a(Landroid/os/Parcel;ILjava/lang/String;Z)V

    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/android/gms/internal/fp;->d:Ljava/util/List;

    invoke-static {p1, v1, v2, v4}, Landroid/support/v4/app/b;->c(Landroid/os/Parcel;ILjava/util/List;Z)V

    const/4 v1, 0x5

    iget v2, p0, Lcom/google/android/gms/internal/fp;->e:I

    invoke-static {p1, v1, v2}, Landroid/support/v4/app/b;->a(Landroid/os/Parcel;II)V

    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/android/gms/internal/fp;->f:Ljava/util/List;

    invoke-static {p1, v1, v2, v4}, Landroid/support/v4/app/b;->c(Landroid/os/Parcel;ILjava/util/List;Z)V

    const/4 v1, 0x7

    iget-wide v2, p0, Lcom/google/android/gms/internal/fp;->g:J

    invoke-static {p1, v1, v2, v3}, Landroid/support/v4/app/b;->a(Landroid/os/Parcel;IJ)V

    const/16 v1, 0x8

    iget-boolean v2, p0, Lcom/google/android/gms/internal/fp;->h:Z

    invoke-static {p1, v1, v2}, Landroid/support/v4/app/b;->a(Landroid/os/Parcel;IZ)V

    const/16 v1, 0x9

    iget-wide v2, p0, Lcom/google/android/gms/internal/fp;->i:J

    invoke-static {p1, v1, v2, v3}, Landroid/support/v4/app/b;->a(Landroid/os/Parcel;IJ)V

    const/16 v1, 0xa

    iget-object v2, p0, Lcom/google/android/gms/internal/fp;->j:Ljava/util/List;

    invoke-static {p1, v1, v2, v4}, Landroid/support/v4/app/b;->c(Landroid/os/Parcel;ILjava/util/List;Z)V

    const/16 v1, 0xb

    iget-wide v2, p0, Lcom/google/android/gms/internal/fp;->k:J

    invoke-static {p1, v1, v2, v3}, Landroid/support/v4/app/b;->a(Landroid/os/Parcel;IJ)V

    const/16 v1, 0xc

    iget v2, p0, Lcom/google/android/gms/internal/fp;->l:I

    invoke-static {p1, v1, v2}, Landroid/support/v4/app/b;->a(Landroid/os/Parcel;II)V

    const/16 v1, 0xd

    iget-object v2, p0, Lcom/google/android/gms/internal/fp;->m:Ljava/lang/String;

    invoke-static {p1, v1, v2, v4}, Landroid/support/v4/app/b;->a(Landroid/os/Parcel;ILjava/lang/String;Z)V

    const/16 v1, 0xe

    iget-wide v2, p0, Lcom/google/android/gms/internal/fp;->n:J

    invoke-static {p1, v1, v2, v3}, Landroid/support/v4/app/b;->a(Landroid/os/Parcel;IJ)V

    const/16 v1, 0xf

    iget-object v2, p0, Lcom/google/android/gms/internal/fp;->o:Ljava/lang/String;

    invoke-static {p1, v1, v2, v4}, Landroid/support/v4/app/b;->a(Landroid/os/Parcel;ILjava/lang/String;Z)V

    const/16 v1, 0x13

    iget-object v2, p0, Lcom/google/android/gms/internal/fp;->q:Ljava/lang/String;

    invoke-static {p1, v1, v2, v4}, Landroid/support/v4/app/b;->a(Landroid/os/Parcel;ILjava/lang/String;Z)V

    const/16 v1, 0x12

    iget-boolean v2, p0, Lcom/google/android/gms/internal/fp;->p:Z

    invoke-static {p1, v1, v2}, Landroid/support/v4/app/b;->a(Landroid/os/Parcel;IZ)V

    const/16 v1, 0x15

    iget-object v2, p0, Lcom/google/android/gms/internal/fp;->r:Ljava/lang/String;

    invoke-static {p1, v1, v2, v4}, Landroid/support/v4/app/b;->a(Landroid/os/Parcel;ILjava/lang/String;Z)V

    const/16 v1, 0x17

    iget-boolean v2, p0, Lcom/google/android/gms/internal/fp;->t:Z

    invoke-static {p1, v1, v2}, Landroid/support/v4/app/b;->a(Landroid/os/Parcel;IZ)V

    const/16 v1, 0x16

    iget-boolean v2, p0, Lcom/google/android/gms/internal/fp;->s:Z

    invoke-static {p1, v1, v2}, Landroid/support/v4/app/b;->a(Landroid/os/Parcel;IZ)V

    const/16 v1, 0x19

    iget-boolean v2, p0, Lcom/google/android/gms/internal/fp;->v:Z

    invoke-static {p1, v1, v2}, Landroid/support/v4/app/b;->a(Landroid/os/Parcel;IZ)V

    const/16 v1, 0x18

    iget-boolean v2, p0, Lcom/google/android/gms/internal/fp;->u:Z

    invoke-static {p1, v1, v2}, Landroid/support/v4/app/b;->a(Landroid/os/Parcel;IZ)V

    invoke-static {p1, v0}, Landroid/support/v4/app/b;->B(Landroid/os/Parcel;I)V

    return-void
.end method


# virtual methods
.method public final synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 31

    invoke-static/range {p1 .. p1}, Landroid/support/v4/app/b;->a(Landroid/os/Parcel;)I

    move-result v2

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const-wide/16 v10, 0x0

    const/4 v12, 0x0

    const-wide/16 v13, 0x0

    const/4 v15, 0x0

    const-wide/16 v16, 0x0

    const/16 v18, 0x0

    const/16 v19, 0x0

    const-wide/16 v20, 0x0

    const/16 v22, 0x0

    const/16 v23, 0x0

    const/16 v24, 0x0

    const/16 v25, 0x0

    const/16 v26, 0x0

    const/16 v27, 0x0

    const/16 v28, 0x0

    const/16 v29, 0x0

    :goto_0
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->dataPosition()I

    move-result v3

    if-ge v3, v2, :cond_0

    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    move-result v3

    const v30, 0xffff

    and-int v30, v30, v3

    packed-switch v30, :pswitch_data_0

    :pswitch_0
    move-object/from16 v0, p1

    invoke-static {v0, v3}, Landroid/support/v4/app/b;->b(Landroid/os/Parcel;I)V

    goto :goto_0

    :pswitch_1
    move-object/from16 v0, p1

    invoke-static {v0, v3}, Landroid/support/v4/app/b;->g(Landroid/os/Parcel;I)I

    move-result v4

    goto :goto_0

    :pswitch_2
    move-object/from16 v0, p1

    invoke-static {v0, v3}, Landroid/support/v4/app/b;->p(Landroid/os/Parcel;I)Ljava/lang/String;

    move-result-object v5

    goto :goto_0

    :pswitch_3
    move-object/from16 v0, p1

    invoke-static {v0, v3}, Landroid/support/v4/app/b;->p(Landroid/os/Parcel;I)Ljava/lang/String;

    move-result-object v6

    goto :goto_0

    :pswitch_4
    move-object/from16 v0, p1

    invoke-static {v0, v3}, Landroid/support/v4/app/b;->y(Landroid/os/Parcel;I)Ljava/util/ArrayList;

    move-result-object v7

    goto :goto_0

    :pswitch_5
    move-object/from16 v0, p1

    invoke-static {v0, v3}, Landroid/support/v4/app/b;->g(Landroid/os/Parcel;I)I

    move-result v8

    goto :goto_0

    :pswitch_6
    move-object/from16 v0, p1

    invoke-static {v0, v3}, Landroid/support/v4/app/b;->y(Landroid/os/Parcel;I)Ljava/util/ArrayList;

    move-result-object v9

    goto :goto_0

    :pswitch_7
    move-object/from16 v0, p1

    invoke-static {v0, v3}, Landroid/support/v4/app/b;->i(Landroid/os/Parcel;I)J

    move-result-wide v10

    goto :goto_0

    :pswitch_8
    move-object/from16 v0, p1

    invoke-static {v0, v3}, Landroid/support/v4/app/b;->c(Landroid/os/Parcel;I)Z

    move-result v12

    goto :goto_0

    :pswitch_9
    move-object/from16 v0, p1

    invoke-static {v0, v3}, Landroid/support/v4/app/b;->i(Landroid/os/Parcel;I)J

    move-result-wide v13

    goto :goto_0

    :pswitch_a
    move-object/from16 v0, p1

    invoke-static {v0, v3}, Landroid/support/v4/app/b;->y(Landroid/os/Parcel;I)Ljava/util/ArrayList;

    move-result-object v15

    goto :goto_0

    :pswitch_b
    move-object/from16 v0, p1

    invoke-static {v0, v3}, Landroid/support/v4/app/b;->i(Landroid/os/Parcel;I)J

    move-result-wide v16

    goto :goto_0

    :pswitch_c
    move-object/from16 v0, p1

    invoke-static {v0, v3}, Landroid/support/v4/app/b;->g(Landroid/os/Parcel;I)I

    move-result v18

    goto :goto_0

    :pswitch_d
    move-object/from16 v0, p1

    invoke-static {v0, v3}, Landroid/support/v4/app/b;->p(Landroid/os/Parcel;I)Ljava/lang/String;

    move-result-object v19

    goto :goto_0

    :pswitch_e
    move-object/from16 v0, p1

    invoke-static {v0, v3}, Landroid/support/v4/app/b;->i(Landroid/os/Parcel;I)J

    move-result-wide v20

    goto :goto_0

    :pswitch_f
    move-object/from16 v0, p1

    invoke-static {v0, v3}, Landroid/support/v4/app/b;->p(Landroid/os/Parcel;I)Ljava/lang/String;

    move-result-object v22

    goto :goto_0

    :pswitch_10
    move-object/from16 v0, p1

    invoke-static {v0, v3}, Landroid/support/v4/app/b;->p(Landroid/os/Parcel;I)Ljava/lang/String;

    move-result-object v24

    goto/16 :goto_0

    :pswitch_11
    move-object/from16 v0, p1

    invoke-static {v0, v3}, Landroid/support/v4/app/b;->c(Landroid/os/Parcel;I)Z

    move-result v23

    goto/16 :goto_0

    :pswitch_12
    move-object/from16 v0, p1

    invoke-static {v0, v3}, Landroid/support/v4/app/b;->p(Landroid/os/Parcel;I)Ljava/lang/String;

    move-result-object v25

    goto/16 :goto_0

    :pswitch_13
    move-object/from16 v0, p1

    invoke-static {v0, v3}, Landroid/support/v4/app/b;->c(Landroid/os/Parcel;I)Z

    move-result v27

    goto/16 :goto_0

    :pswitch_14
    move-object/from16 v0, p1

    invoke-static {v0, v3}, Landroid/support/v4/app/b;->c(Landroid/os/Parcel;I)Z

    move-result v26

    goto/16 :goto_0

    :pswitch_15
    move-object/from16 v0, p1

    invoke-static {v0, v3}, Landroid/support/v4/app/b;->c(Landroid/os/Parcel;I)Z

    move-result v29

    goto/16 :goto_0

    :pswitch_16
    move-object/from16 v0, p1

    invoke-static {v0, v3}, Landroid/support/v4/app/b;->c(Landroid/os/Parcel;I)Z

    move-result v28

    goto/16 :goto_0

    :cond_0
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->dataPosition()I

    move-result v3

    if-eq v3, v2, :cond_1

    new-instance v3, Landroid/support/v4/app/i;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "Overread allowed size end="

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-direct {v3, v2, v0}, Landroid/support/v4/app/i;-><init>(Ljava/lang/String;Landroid/os/Parcel;)V

    throw v3

    :cond_1
    new-instance v3, Lcom/google/android/gms/internal/fp;

    invoke-direct/range {v3 .. v29}, Lcom/google/android/gms/internal/fp;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/util/List;ILjava/util/List;JZJLjava/util/List;JILjava/lang/String;JLjava/lang/String;ZLjava/lang/String;Ljava/lang/String;ZZZZ)V

    return-object v3

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_0
        :pswitch_0
        :pswitch_11
        :pswitch_10
        :pswitch_0
        :pswitch_12
        :pswitch_14
        :pswitch_13
        :pswitch_16
        :pswitch_15
    .end packed-switch
.end method

.method public final synthetic newArray(I)[Ljava/lang/Object;
    .locals 1

    new-array v0, p1, [Lcom/google/android/gms/internal/fp;

    return-object v0
.end method
