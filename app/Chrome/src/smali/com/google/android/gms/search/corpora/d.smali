.class public Lcom/google/android/gms/search/corpora/d;
.super Ljava/lang/Object;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/google/android/gms/common/api/i;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/api/l;
    .locals 2

    new-instance v0, Lcom/google/android/gms/search/corpora/ClearCorpusCall$b;

    invoke-direct {v0}, Lcom/google/android/gms/search/corpora/ClearCorpusCall$b;-><init>()V

    iput-object p2, v0, Lcom/google/android/gms/search/corpora/ClearCorpusCall$b;->a:Ljava/lang/String;

    iput-object p3, v0, Lcom/google/android/gms/search/corpora/ClearCorpusCall$b;->b:Ljava/lang/String;

    new-instance v1, Lcom/google/android/gms/search/corpora/a;

    invoke-direct {v1, v0}, Lcom/google/android/gms/search/corpora/a;-><init>(Lcom/google/android/gms/search/corpora/ClearCorpusCall$b;)V

    invoke-interface {p1, v1}, Lcom/google/android/gms/common/api/i;->a(Lcom/google/android/gms/common/api/g;)Lcom/google/android/gms/common/api/g;

    move-result-object v0

    return-object v0
.end method

.method public a(Lcom/google/android/gms/common/api/i;Ljava/lang/String;Ljava/lang/String;J)Lcom/google/android/gms/common/api/l;
    .locals 2

    new-instance v0, Lcom/google/android/gms/search/corpora/RequestIndexingCall$b;

    invoke-direct {v0}, Lcom/google/android/gms/search/corpora/RequestIndexingCall$b;-><init>()V

    iput-object p2, v0, Lcom/google/android/gms/search/corpora/RequestIndexingCall$b;->a:Ljava/lang/String;

    iput-object p3, v0, Lcom/google/android/gms/search/corpora/RequestIndexingCall$b;->b:Ljava/lang/String;

    iput-wide p4, v0, Lcom/google/android/gms/search/corpora/RequestIndexingCall$b;->c:J

    new-instance v1, Lcom/google/android/gms/search/corpora/c;

    invoke-direct {v1, v0}, Lcom/google/android/gms/search/corpora/c;-><init>(Lcom/google/android/gms/search/corpora/RequestIndexingCall$b;)V

    invoke-interface {p1, v1}, Lcom/google/android/gms/common/api/i;->a(Lcom/google/android/gms/common/api/g;)Lcom/google/android/gms/common/api/g;

    move-result-object v0

    return-object v0
.end method

.method public b(Lcom/google/android/gms/common/api/i;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/api/l;
    .locals 2

    new-instance v0, Lcom/google/android/gms/search/corpora/GetCorpusStatusCall$b;

    invoke-direct {v0}, Lcom/google/android/gms/search/corpora/GetCorpusStatusCall$b;-><init>()V

    iput-object p2, v0, Lcom/google/android/gms/search/corpora/GetCorpusStatusCall$b;->a:Ljava/lang/String;

    iput-object p3, v0, Lcom/google/android/gms/search/corpora/GetCorpusStatusCall$b;->b:Ljava/lang/String;

    new-instance v1, Lcom/google/android/gms/search/corpora/b;

    invoke-direct {v1, v0}, Lcom/google/android/gms/search/corpora/b;-><init>(Lcom/google/android/gms/search/corpora/GetCorpusStatusCall$b;)V

    invoke-interface {p1, v1}, Lcom/google/android/gms/common/api/i;->a(Lcom/google/android/gms/common/api/g;)Lcom/google/android/gms/common/api/g;

    move-result-object v0

    return-object v0
.end method
