.class public final Lcom/google/android/gms/internal/dr;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Lcom/google/android/gms/internal/ci;


# instance fields
.field public final a:I

.field public final b:Lcom/google/android/gms/internal/do;

.field public final c:Ljava/lang/String;

.field public final d:Z

.field public final e:Ljava/lang/String;

.field public final f:I

.field public final g:I

.field public final h:Ljava/lang/String;

.field public final i:Lcom/google/android/gms/internal/gz;

.field public final j:Ljava/lang/String;

.field public final k:Lcom/google/android/gms/internal/ac;

.field private l:Lcom/google/android/gms/drive/metadata/a;

.field private m:Lcom/google/android/gms/drive/metadata/a;

.field private n:Lcom/google/android/gms/internal/co;

.field private o:Lcom/google/android/gms/drive/metadata/a;

.field private p:Lcom/google/android/gms/drive/metadata/a;

.field private q:Lcom/google/android/gms/drive/metadata/a;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/gms/internal/ci;

    invoke-direct {v0}, Lcom/google/android/gms/internal/ci;-><init>()V

    sput-object v0, Lcom/google/android/gms/internal/dr;->CREATOR:Lcom/google/android/gms/internal/ci;

    return-void
.end method

.method constructor <init>(ILcom/google/android/gms/internal/do;Landroid/os/IBinder;Landroid/os/IBinder;Landroid/os/IBinder;Landroid/os/IBinder;Ljava/lang/String;ZLjava/lang/String;Landroid/os/IBinder;IILjava/lang/String;Lcom/google/android/gms/internal/gz;Landroid/os/IBinder;Ljava/lang/String;Lcom/google/android/gms/internal/ac;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/gms/internal/dr;->a:I

    iput-object p2, p0, Lcom/google/android/gms/internal/dr;->b:Lcom/google/android/gms/internal/do;

    invoke-static {p3}, Lcom/google/android/gms/a/b;->a(Landroid/os/IBinder;)Lcom/google/android/gms/a/a;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/gms/a/d;->a(Lcom/google/android/gms/a/a;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/drive/metadata/a;

    iput-object v1, p0, Lcom/google/android/gms/internal/dr;->l:Lcom/google/android/gms/drive/metadata/a;

    invoke-static {p4}, Lcom/google/android/gms/a/b;->a(Landroid/os/IBinder;)Lcom/google/android/gms/a/a;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/gms/a/d;->a(Lcom/google/android/gms/a/a;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/drive/metadata/a;

    iput-object v1, p0, Lcom/google/android/gms/internal/dr;->m:Lcom/google/android/gms/drive/metadata/a;

    invoke-static {p5}, Lcom/google/android/gms/a/b;->a(Landroid/os/IBinder;)Lcom/google/android/gms/a/a;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/gms/a/d;->a(Lcom/google/android/gms/a/a;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/internal/co;

    iput-object v1, p0, Lcom/google/android/gms/internal/dr;->n:Lcom/google/android/gms/internal/co;

    invoke-static {p6}, Lcom/google/android/gms/a/b;->a(Landroid/os/IBinder;)Lcom/google/android/gms/a/a;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/gms/a/d;->a(Lcom/google/android/gms/a/a;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/drive/metadata/a;

    iput-object v1, p0, Lcom/google/android/gms/internal/dr;->o:Lcom/google/android/gms/drive/metadata/a;

    iput-object p7, p0, Lcom/google/android/gms/internal/dr;->c:Ljava/lang/String;

    iput-boolean p8, p0, Lcom/google/android/gms/internal/dr;->d:Z

    iput-object p9, p0, Lcom/google/android/gms/internal/dr;->e:Ljava/lang/String;

    invoke-static {p10}, Lcom/google/android/gms/a/b;->a(Landroid/os/IBinder;)Lcom/google/android/gms/a/a;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/gms/a/d;->a(Lcom/google/android/gms/a/a;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/drive/metadata/a;

    iput-object v1, p0, Lcom/google/android/gms/internal/dr;->p:Lcom/google/android/gms/drive/metadata/a;

    iput p11, p0, Lcom/google/android/gms/internal/dr;->f:I

    iput p12, p0, Lcom/google/android/gms/internal/dr;->g:I

    iput-object p13, p0, Lcom/google/android/gms/internal/dr;->h:Ljava/lang/String;

    move-object/from16 v0, p14

    iput-object v0, p0, Lcom/google/android/gms/internal/dr;->i:Lcom/google/android/gms/internal/gz;

    invoke-static/range {p15 .. p15}, Lcom/google/android/gms/a/b;->a(Landroid/os/IBinder;)Lcom/google/android/gms/a/a;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/gms/a/d;->a(Lcom/google/android/gms/a/a;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/drive/metadata/a;

    iput-object v1, p0, Lcom/google/android/gms/internal/dr;->q:Lcom/google/android/gms/drive/metadata/a;

    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/google/android/gms/internal/dr;->j:Ljava/lang/String;

    move-object/from16 v0, p17

    iput-object v0, p0, Lcom/google/android/gms/internal/dr;->k:Lcom/google/android/gms/internal/ac;

    return-void
.end method


# virtual methods
.method final a()Landroid/os/IBinder;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/internal/dr;->l:Lcom/google/android/gms/drive/metadata/a;

    invoke-static {v0}, Lcom/google/android/gms/a/d;->a(Ljava/lang/Object;)Lcom/google/android/gms/a/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/a/a;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    return-object v0
.end method

.method final b()Landroid/os/IBinder;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/internal/dr;->m:Lcom/google/android/gms/drive/metadata/a;

    invoke-static {v0}, Lcom/google/android/gms/a/d;->a(Ljava/lang/Object;)Lcom/google/android/gms/a/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/a/a;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    return-object v0
.end method

.method final c()Landroid/os/IBinder;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/internal/dr;->n:Lcom/google/android/gms/internal/co;

    invoke-static {v0}, Lcom/google/android/gms/a/d;->a(Ljava/lang/Object;)Lcom/google/android/gms/a/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/a/a;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    return-object v0
.end method

.method final d()Landroid/os/IBinder;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/internal/dr;->o:Lcom/google/android/gms/drive/metadata/a;

    invoke-static {v0}, Lcom/google/android/gms/a/d;->a(Ljava/lang/Object;)Lcom/google/android/gms/a/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/a/a;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method final e()Landroid/os/IBinder;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/internal/dr;->q:Lcom/google/android/gms/drive/metadata/a;

    invoke-static {v0}, Lcom/google/android/gms/a/d;->a(Ljava/lang/Object;)Lcom/google/android/gms/a/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/a/a;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    return-object v0
.end method

.method final f()Landroid/os/IBinder;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/internal/dr;->p:Lcom/google/android/gms/drive/metadata/a;

    invoke-static {v0}, Lcom/google/android/gms/a/d;->a(Ljava/lang/Object;)Lcom/google/android/gms/a/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/a/a;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    invoke-static {p0, p1, p2}, Lcom/google/android/gms/internal/ci;->a(Lcom/google/android/gms/internal/dr;Landroid/os/Parcel;I)V

    return-void
.end method
