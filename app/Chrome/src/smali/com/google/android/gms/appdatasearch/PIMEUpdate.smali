.class public Lcom/google/android/gms/appdatasearch/PIMEUpdate;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Lcom/google/android/gms/appdatasearch/L;


# instance fields
.field final a:I

.field final b:[B

.field final c:[B

.field public final d:I

.field public final e:Ljava/lang/String;

.field public final f:Ljava/lang/String;

.field public final g:Z

.field final h:Landroid/os/Bundle;

.field public final i:J

.field public final j:J

.field public final k:Landroid/accounts/Account;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/gms/appdatasearch/L;

    invoke-direct {v0}, Lcom/google/android/gms/appdatasearch/L;-><init>()V

    sput-object v0, Lcom/google/android/gms/appdatasearch/PIMEUpdate;->CREATOR:Lcom/google/android/gms/appdatasearch/L;

    return-void
.end method

.method constructor <init>(I[B[BILjava/lang/String;Ljava/lang/String;ZLandroid/os/Bundle;JJLandroid/accounts/Account;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/gms/appdatasearch/PIMEUpdate;->a:I

    iput-object p2, p0, Lcom/google/android/gms/appdatasearch/PIMEUpdate;->b:[B

    iput-object p3, p0, Lcom/google/android/gms/appdatasearch/PIMEUpdate;->c:[B

    iput p4, p0, Lcom/google/android/gms/appdatasearch/PIMEUpdate;->d:I

    iput-object p5, p0, Lcom/google/android/gms/appdatasearch/PIMEUpdate;->e:Ljava/lang/String;

    iput-object p6, p0, Lcom/google/android/gms/appdatasearch/PIMEUpdate;->f:Ljava/lang/String;

    iput-boolean p7, p0, Lcom/google/android/gms/appdatasearch/PIMEUpdate;->g:Z

    iput-object p8, p0, Lcom/google/android/gms/appdatasearch/PIMEUpdate;->h:Landroid/os/Bundle;

    iput-wide p9, p0, Lcom/google/android/gms/appdatasearch/PIMEUpdate;->i:J

    iput-wide p11, p0, Lcom/google/android/gms/appdatasearch/PIMEUpdate;->j:J

    iput-object p13, p0, Lcom/google/android/gms/appdatasearch/PIMEUpdate;->k:Landroid/accounts/Account;

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    sget-object v0, Lcom/google/android/gms/appdatasearch/PIMEUpdate;->CREATOR:Lcom/google/android/gms/appdatasearch/L;

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    sget-object v0, Lcom/google/android/gms/appdatasearch/PIMEUpdate;->CREATOR:Lcom/google/android/gms/appdatasearch/L;

    invoke-static {p0, p1, p2}, Lcom/google/android/gms/appdatasearch/L;->a(Lcom/google/android/gms/appdatasearch/PIMEUpdate;Landroid/os/Parcel;I)V

    return-void
.end method
