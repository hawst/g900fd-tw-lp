.class public final Lcom/google/android/gms/cast/h;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/support/v4/view/i;


# instance fields
.field final a:Lcom/google/android/gms/cast/CastDevice;

.field final b:Lcom/google/android/gms/cast/j;

.field private final c:I


# direct methods
.method private constructor <init>(Lcom/google/android/gms/cast/i;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iget-object v0, p1, Lcom/google/android/gms/cast/i;->a:Lcom/google/android/gms/cast/CastDevice;

    iput-object v0, p0, Lcom/google/android/gms/cast/h;->a:Lcom/google/android/gms/cast/CastDevice;

    iget-object v0, p1, Lcom/google/android/gms/cast/i;->b:Lcom/google/android/gms/cast/j;

    iput-object v0, p0, Lcom/google/android/gms/cast/h;->b:Lcom/google/android/gms/cast/j;

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/cast/h;->c:I

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/gms/cast/i;B)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/gms/cast/h;-><init>(Lcom/google/android/gms/cast/i;)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/cast/h;)I
    .locals 1

    iget v0, p0, Lcom/google/android/gms/cast/h;->c:I

    return v0
.end method

.method public static a(Lcom/google/android/gms/cast/CastDevice;Lcom/google/android/gms/cast/j;)Lcom/google/android/gms/cast/i;
    .locals 2

    new-instance v0, Lcom/google/android/gms/cast/i;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p1, v1}, Lcom/google/android/gms/cast/i;-><init>(Lcom/google/android/gms/cast/CastDevice;Lcom/google/android/gms/cast/j;B)V

    return-object v0
.end method
