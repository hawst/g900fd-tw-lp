.class public final Lcom/google/android/gms/fitness/request/F;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/os/Parcelable$Creator;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static a(Lcom/google/android/gms/fitness/request/SessionReadRequest;Landroid/os/Parcel;)V
    .locals 5

    const/4 v4, 0x0

    invoke-static {p1}, Landroid/support/v4/app/b;->b(Landroid/os/Parcel;)I

    move-result v0

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/android/gms/fitness/request/SessionReadRequest;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {p1, v1, v2, v4}, Landroid/support/v4/app/b;->a(Landroid/os/Parcel;ILjava/lang/String;Z)V

    const/16 v1, 0x3e8

    invoke-virtual {p0}, Lcom/google/android/gms/fitness/request/SessionReadRequest;->j()I

    move-result v2

    invoke-static {p1, v1, v2}, Landroid/support/v4/app/b;->a(Landroid/os/Parcel;II)V

    const/4 v1, 0x2

    invoke-virtual {p0}, Lcom/google/android/gms/fitness/request/SessionReadRequest;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {p1, v1, v2, v4}, Landroid/support/v4/app/b;->a(Landroid/os/Parcel;ILjava/lang/String;Z)V

    const/4 v1, 0x3

    invoke-virtual {p0}, Lcom/google/android/gms/fitness/request/SessionReadRequest;->c()J

    move-result-wide v2

    invoke-static {p1, v1, v2, v3}, Landroid/support/v4/app/b;->a(Landroid/os/Parcel;IJ)V

    const/4 v1, 0x4

    invoke-virtual {p0}, Lcom/google/android/gms/fitness/request/SessionReadRequest;->d()J

    move-result-wide v2

    invoke-static {p1, v1, v2, v3}, Landroid/support/v4/app/b;->a(Landroid/os/Parcel;IJ)V

    const/4 v1, 0x5

    invoke-virtual {p0}, Lcom/google/android/gms/fitness/request/SessionReadRequest;->e()Ljava/util/List;

    move-result-object v2

    invoke-static {p1, v1, v2, v4}, Landroid/support/v4/app/b;->d(Landroid/os/Parcel;ILjava/util/List;Z)V

    const/4 v1, 0x6

    invoke-virtual {p0}, Lcom/google/android/gms/fitness/request/SessionReadRequest;->f()Ljava/util/List;

    move-result-object v2

    invoke-static {p1, v1, v2, v4}, Landroid/support/v4/app/b;->d(Landroid/os/Parcel;ILjava/util/List;Z)V

    const/4 v1, 0x7

    invoke-virtual {p0}, Lcom/google/android/gms/fitness/request/SessionReadRequest;->g()Z

    move-result v2

    invoke-static {p1, v1, v2}, Landroid/support/v4/app/b;->a(Landroid/os/Parcel;IZ)V

    const/16 v1, 0x8

    invoke-virtual {p0}, Lcom/google/android/gms/fitness/request/SessionReadRequest;->i()Z

    move-result v2

    invoke-static {p1, v1, v2}, Landroid/support/v4/app/b;->a(Landroid/os/Parcel;IZ)V

    const/16 v1, 0x9

    invoke-virtual {p0}, Lcom/google/android/gms/fitness/request/SessionReadRequest;->h()Ljava/util/List;

    move-result-object v2

    invoke-static {p1, v1, v2, v4}, Landroid/support/v4/app/b;->c(Landroid/os/Parcel;ILjava/util/List;Z)V

    invoke-static {p1, v0}, Landroid/support/v4/app/b;->B(Landroid/os/Parcel;I)V

    return-void
.end method


# virtual methods
.method public final synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 17

    invoke-static/range {p1 .. p1}, Landroid/support/v4/app/b;->a(Landroid/os/Parcel;)I

    move-result v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const-wide/16 v6, 0x0

    const-wide/16 v8, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    :goto_0
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->dataPosition()I

    move-result v15

    if-ge v15, v2, :cond_0

    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    move-result v15

    const v16, 0xffff

    and-int v16, v16, v15

    sparse-switch v16, :sswitch_data_0

    move-object/from16 v0, p1

    invoke-static {v0, v15}, Landroid/support/v4/app/b;->b(Landroid/os/Parcel;I)V

    goto :goto_0

    :sswitch_0
    move-object/from16 v0, p1

    invoke-static {v0, v15}, Landroid/support/v4/app/b;->p(Landroid/os/Parcel;I)Ljava/lang/String;

    move-result-object v4

    goto :goto_0

    :sswitch_1
    move-object/from16 v0, p1

    invoke-static {v0, v15}, Landroid/support/v4/app/b;->g(Landroid/os/Parcel;I)I

    move-result v3

    goto :goto_0

    :sswitch_2
    move-object/from16 v0, p1

    invoke-static {v0, v15}, Landroid/support/v4/app/b;->p(Landroid/os/Parcel;I)Ljava/lang/String;

    move-result-object v5

    goto :goto_0

    :sswitch_3
    move-object/from16 v0, p1

    invoke-static {v0, v15}, Landroid/support/v4/app/b;->i(Landroid/os/Parcel;I)J

    move-result-wide v6

    goto :goto_0

    :sswitch_4
    move-object/from16 v0, p1

    invoke-static {v0, v15}, Landroid/support/v4/app/b;->i(Landroid/os/Parcel;I)J

    move-result-wide v8

    goto :goto_0

    :sswitch_5
    sget-object v10, Lcom/google/android/gms/fitness/data/DataType;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p1

    invoke-static {v0, v15, v10}, Landroid/support/v4/app/b;->c(Landroid/os/Parcel;ILandroid/os/Parcelable$Creator;)Ljava/util/ArrayList;

    move-result-object v10

    goto :goto_0

    :sswitch_6
    sget-object v11, Lcom/google/android/gms/fitness/data/DataSource;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p1

    invoke-static {v0, v15, v11}, Landroid/support/v4/app/b;->c(Landroid/os/Parcel;ILandroid/os/Parcelable$Creator;)Ljava/util/ArrayList;

    move-result-object v11

    goto :goto_0

    :sswitch_7
    move-object/from16 v0, p1

    invoke-static {v0, v15}, Landroid/support/v4/app/b;->c(Landroid/os/Parcel;I)Z

    move-result v12

    goto :goto_0

    :sswitch_8
    move-object/from16 v0, p1

    invoke-static {v0, v15}, Landroid/support/v4/app/b;->c(Landroid/os/Parcel;I)Z

    move-result v13

    goto :goto_0

    :sswitch_9
    move-object/from16 v0, p1

    invoke-static {v0, v15}, Landroid/support/v4/app/b;->y(Landroid/os/Parcel;I)Ljava/util/ArrayList;

    move-result-object v14

    goto :goto_0

    :cond_0
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->dataPosition()I

    move-result v15

    if-eq v15, v2, :cond_1

    new-instance v3, Landroid/support/v4/app/i;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "Overread allowed size end="

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-direct {v3, v2, v0}, Landroid/support/v4/app/i;-><init>(Ljava/lang/String;Landroid/os/Parcel;)V

    throw v3

    :cond_1
    new-instance v2, Lcom/google/android/gms/fitness/request/SessionReadRequest;

    invoke-direct/range {v2 .. v14}, Lcom/google/android/gms/fitness/request/SessionReadRequest;-><init>(ILjava/lang/String;Ljava/lang/String;JJLjava/util/List;Ljava/util/List;ZZLjava/util/List;)V

    return-object v2

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_2
        0x3 -> :sswitch_3
        0x4 -> :sswitch_4
        0x5 -> :sswitch_5
        0x6 -> :sswitch_6
        0x7 -> :sswitch_7
        0x8 -> :sswitch_8
        0x9 -> :sswitch_9
        0x3e8 -> :sswitch_1
    .end sparse-switch
.end method

.method public final synthetic newArray(I)[Ljava/lang/Object;
    .locals 1

    new-array v0, p1, [Lcom/google/android/gms/fitness/request/SessionReadRequest;

    return-object v0
.end method
