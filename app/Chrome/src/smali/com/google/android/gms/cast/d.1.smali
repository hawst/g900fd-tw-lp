.class public Lcom/google/android/gms/cast/d;
.super Ljava/lang/Object;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/google/android/gms/common/api/i;)D
    .locals 2

    sget-object v0, Lcom/google/android/gms/cast/a;->a:Lcom/google/android/gms/common/api/d;

    invoke-interface {p1, v0}, Lcom/google/android/gms/common/api/i;->a(Lcom/google/android/gms/common/api/d;)Lcom/google/android/gms/common/api/b;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/internal/c;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/c;->j()D

    move-result-wide v0

    return-wide v0
.end method

.method public a(Lcom/google/android/gms/common/api/i;Ljava/lang/String;)Lcom/google/android/gms/common/api/l;
    .locals 1

    new-instance v0, Lcom/google/android/gms/cast/g;

    invoke-direct {v0, p0, p2}, Lcom/google/android/gms/cast/g;-><init>(Lcom/google/android/gms/cast/d;Ljava/lang/String;)V

    invoke-interface {p1, v0}, Lcom/google/android/gms/common/api/i;->b(Lcom/google/android/gms/common/api/g;)Lcom/google/android/gms/common/api/g;

    move-result-object v0

    return-object v0
.end method

.method public a(Lcom/google/android/gms/common/api/i;Ljava/lang/String;Lcom/google/android/gms/cast/LaunchOptions;)Lcom/google/android/gms/common/api/l;
    .locals 1

    new-instance v0, Lcom/google/android/gms/cast/f;

    invoke-direct {v0, p0, p2, p3}, Lcom/google/android/gms/cast/f;-><init>(Lcom/google/android/gms/cast/d;Ljava/lang/String;Lcom/google/android/gms/cast/LaunchOptions;)V

    invoke-interface {p1, v0}, Lcom/google/android/gms/common/api/i;->b(Lcom/google/android/gms/common/api/g;)Lcom/google/android/gms/common/api/g;

    move-result-object v0

    return-object v0
.end method

.method public a(Lcom/google/android/gms/common/api/i;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/api/l;
    .locals 1

    new-instance v0, Lcom/google/android/gms/cast/e;

    invoke-direct {v0, p0, p2, p3}, Lcom/google/android/gms/cast/e;-><init>(Lcom/google/android/gms/cast/d;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {p1, v0}, Lcom/google/android/gms/common/api/i;->b(Lcom/google/android/gms/common/api/g;)Lcom/google/android/gms/common/api/g;

    move-result-object v0

    return-object v0
.end method

.method public a(Lcom/google/android/gms/common/api/i;D)V
    .locals 2

    :try_start_0
    sget-object v0, Lcom/google/android/gms/cast/a;->a:Lcom/google/android/gms/common/api/d;

    invoke-interface {p1, v0}, Lcom/google/android/gms/common/api/i;->a(Lcom/google/android/gms/common/api/d;)Lcom/google/android/gms/common/api/b;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/internal/c;

    invoke-virtual {v0, p2, p3}, Lcom/google/android/gms/internal/c;->a(D)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    new-instance v0, Ljava/io/IOException;

    const-string/jumbo v1, "service error"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public a(Lcom/google/android/gms/common/api/i;Ljava/lang/String;Lcom/google/android/gms/cast/k;)V
    .locals 2

    :try_start_0
    sget-object v0, Lcom/google/android/gms/cast/a;->a:Lcom/google/android/gms/common/api/d;

    invoke-interface {p1, v0}, Lcom/google/android/gms/common/api/i;->a(Lcom/google/android/gms/common/api/d;)Lcom/google/android/gms/common/api/b;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/internal/c;

    invoke-virtual {v0, p2, p3}, Lcom/google/android/gms/internal/c;->a(Ljava/lang/String;Lcom/google/android/gms/cast/k;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    new-instance v0, Ljava/io/IOException;

    const-string/jumbo v1, "service error"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
