.class public Lcom/google/android/gms/audiomodem/DsssEncoding;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private final a:I

.field private final b:I

.field private final c:Z

.field private final d:Z

.field private final e:I

.field private final f:I

.field private final g:F

.field private final h:I

.field private final i:F

.field private final j:I

.field private final k:I

.field private final l:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/gms/audiomodem/b;

    invoke-direct {v0}, Lcom/google/android/gms/audiomodem/b;-><init>()V

    sput-object v0, Lcom/google/android/gms/audiomodem/DsssEncoding;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>(IIZZIIFIFIII)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/gms/audiomodem/DsssEncoding;->a:I

    iput p2, p0, Lcom/google/android/gms/audiomodem/DsssEncoding;->b:I

    iput-boolean p3, p0, Lcom/google/android/gms/audiomodem/DsssEncoding;->c:Z

    iput-boolean p4, p0, Lcom/google/android/gms/audiomodem/DsssEncoding;->d:Z

    iput p5, p0, Lcom/google/android/gms/audiomodem/DsssEncoding;->e:I

    iput p6, p0, Lcom/google/android/gms/audiomodem/DsssEncoding;->f:I

    iput p7, p0, Lcom/google/android/gms/audiomodem/DsssEncoding;->g:F

    iput p8, p0, Lcom/google/android/gms/audiomodem/DsssEncoding;->h:I

    iput p9, p0, Lcom/google/android/gms/audiomodem/DsssEncoding;->i:F

    iput p10, p0, Lcom/google/android/gms/audiomodem/DsssEncoding;->j:I

    iput p11, p0, Lcom/google/android/gms/audiomodem/DsssEncoding;->k:I

    iput p12, p0, Lcom/google/android/gms/audiomodem/DsssEncoding;->l:I

    return-void
.end method


# virtual methods
.method final a()I
    .locals 1

    iget v0, p0, Lcom/google/android/gms/audiomodem/DsssEncoding;->a:I

    return v0
.end method

.method public final b()I
    .locals 1

    iget v0, p0, Lcom/google/android/gms/audiomodem/DsssEncoding;->b:I

    return v0
.end method

.method public final c()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/audiomodem/DsssEncoding;->c:Z

    return v0
.end method

.method public final d()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/audiomodem/DsssEncoding;->d:Z

    return v0
.end method

.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final e()I
    .locals 1

    iget v0, p0, Lcom/google/android/gms/audiomodem/DsssEncoding;->e:I

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p0, p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v2, p1, Lcom/google/android/gms/audiomodem/DsssEncoding;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    check-cast p1, Lcom/google/android/gms/audiomodem/DsssEncoding;

    iget v2, p0, Lcom/google/android/gms/audiomodem/DsssEncoding;->a:I

    iget v3, p1, Lcom/google/android/gms/audiomodem/DsssEncoding;->a:I

    if-ne v2, v3, :cond_3

    iget v2, p0, Lcom/google/android/gms/audiomodem/DsssEncoding;->b:I

    iget v3, p1, Lcom/google/android/gms/audiomodem/DsssEncoding;->b:I

    if-ne v2, v3, :cond_3

    iget-boolean v2, p0, Lcom/google/android/gms/audiomodem/DsssEncoding;->c:Z

    iget-boolean v3, p1, Lcom/google/android/gms/audiomodem/DsssEncoding;->c:Z

    if-ne v2, v3, :cond_3

    iget-boolean v2, p0, Lcom/google/android/gms/audiomodem/DsssEncoding;->d:Z

    iget-boolean v3, p1, Lcom/google/android/gms/audiomodem/DsssEncoding;->d:Z

    if-ne v2, v3, :cond_3

    iget v2, p0, Lcom/google/android/gms/audiomodem/DsssEncoding;->e:I

    iget v3, p1, Lcom/google/android/gms/audiomodem/DsssEncoding;->e:I

    if-ne v2, v3, :cond_3

    iget v2, p0, Lcom/google/android/gms/audiomodem/DsssEncoding;->f:I

    iget v3, p1, Lcom/google/android/gms/audiomodem/DsssEncoding;->f:I

    if-ne v2, v3, :cond_3

    iget v2, p0, Lcom/google/android/gms/audiomodem/DsssEncoding;->g:F

    iget v3, p1, Lcom/google/android/gms/audiomodem/DsssEncoding;->g:F

    cmpl-float v2, v2, v3

    if-nez v2, :cond_3

    iget v2, p0, Lcom/google/android/gms/audiomodem/DsssEncoding;->h:I

    iget v3, p1, Lcom/google/android/gms/audiomodem/DsssEncoding;->h:I

    if-ne v2, v3, :cond_3

    iget v2, p0, Lcom/google/android/gms/audiomodem/DsssEncoding;->i:F

    iget v3, p1, Lcom/google/android/gms/audiomodem/DsssEncoding;->i:F

    cmpl-float v2, v2, v3

    if-nez v2, :cond_3

    iget v2, p0, Lcom/google/android/gms/audiomodem/DsssEncoding;->j:I

    iget v3, p1, Lcom/google/android/gms/audiomodem/DsssEncoding;->j:I

    if-ne v2, v3, :cond_3

    iget v2, p0, Lcom/google/android/gms/audiomodem/DsssEncoding;->k:I

    iget v3, p1, Lcom/google/android/gms/audiomodem/DsssEncoding;->k:I

    if-ne v2, v3, :cond_3

    iget v2, p0, Lcom/google/android/gms/audiomodem/DsssEncoding;->l:I

    iget v3, p1, Lcom/google/android/gms/audiomodem/DsssEncoding;->l:I

    if-eq v2, v3, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public final f()I
    .locals 1

    iget v0, p0, Lcom/google/android/gms/audiomodem/DsssEncoding;->f:I

    return v0
.end method

.method public final g()F
    .locals 1

    iget v0, p0, Lcom/google/android/gms/audiomodem/DsssEncoding;->g:F

    return v0
.end method

.method public final h()I
    .locals 1

    iget v0, p0, Lcom/google/android/gms/audiomodem/DsssEncoding;->h:I

    return v0
.end method

.method public hashCode()I
    .locals 3

    const/16 v0, 0xc

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget v2, p0, Lcom/google/android/gms/audiomodem/DsssEncoding;->a:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget v2, p0, Lcom/google/android/gms/audiomodem/DsssEncoding;->b:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-boolean v2, p0, Lcom/google/android/gms/audiomodem/DsssEncoding;->c:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-boolean v2, p0, Lcom/google/android/gms/audiomodem/DsssEncoding;->d:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget v2, p0, Lcom/google/android/gms/audiomodem/DsssEncoding;->e:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget v2, p0, Lcom/google/android/gms/audiomodem/DsssEncoding;->f:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget v2, p0, Lcom/google/android/gms/audiomodem/DsssEncoding;->g:F

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x7

    iget v2, p0, Lcom/google/android/gms/audiomodem/DsssEncoding;->h:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x8

    iget v2, p0, Lcom/google/android/gms/audiomodem/DsssEncoding;->i:F

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x9

    iget v2, p0, Lcom/google/android/gms/audiomodem/DsssEncoding;->j:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xa

    iget v2, p0, Lcom/google/android/gms/audiomodem/DsssEncoding;->k:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xb

    iget v2, p0, Lcom/google/android/gms/audiomodem/DsssEncoding;->l:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final i()F
    .locals 1

    iget v0, p0, Lcom/google/android/gms/audiomodem/DsssEncoding;->i:F

    return v0
.end method

.method public final j()I
    .locals 1

    iget v0, p0, Lcom/google/android/gms/audiomodem/DsssEncoding;->j:I

    return v0
.end method

.method public final k()I
    .locals 1

    iget v0, p0, Lcom/google/android/gms/audiomodem/DsssEncoding;->k:I

    return v0
.end method

.method public final l()I
    .locals 1

    iget v0, p0, Lcom/google/android/gms/audiomodem/DsssEncoding;->l:I

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    invoke-static {p0, p1}, Lcom/google/android/gms/audiomodem/b;->a(Lcom/google/android/gms/audiomodem/DsssEncoding;Landroid/os/Parcel;)V

    return-void
.end method
