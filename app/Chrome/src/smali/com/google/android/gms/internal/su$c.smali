.class public Lcom/google/android/gms/internal/su$c;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/common/api/n;
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Lcom/google/android/gms/internal/bQ;


# instance fields
.field public a:Lcom/google/android/gms/common/api/Status;

.field public b:[Lcom/google/android/gms/internal/su$a;

.field public c:J

.field public d:J

.field public e:J

.field final f:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/gms/internal/bQ;

    invoke-direct {v0}, Lcom/google/android/gms/internal/bQ;-><init>()V

    sput-object v0, Lcom/google/android/gms/internal/su$c;->CREATOR:Lcom/google/android/gms/internal/bQ;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/internal/su$c;->f:I

    return-void
.end method

.method constructor <init>(ILcom/google/android/gms/common/api/Status;[Lcom/google/android/gms/internal/su$a;JJJ)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/gms/internal/su$c;->f:I

    iput-object p2, p0, Lcom/google/android/gms/internal/su$c;->a:Lcom/google/android/gms/common/api/Status;

    iput-object p3, p0, Lcom/google/android/gms/internal/su$c;->b:[Lcom/google/android/gms/internal/su$a;

    iput-wide p4, p0, Lcom/google/android/gms/internal/su$c;->c:J

    iput-wide p6, p0, Lcom/google/android/gms/internal/su$c;->d:J

    iput-wide p8, p0, Lcom/google/android/gms/internal/su$c;->e:J

    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/gms/common/api/Status;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/internal/su$c;->a:Lcom/google/android/gms/common/api/Status;

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    sget-object v0, Lcom/google/android/gms/internal/su$c;->CREATOR:Lcom/google/android/gms/internal/bQ;

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    sget-object v0, Lcom/google/android/gms/internal/su$c;->CREATOR:Lcom/google/android/gms/internal/bQ;

    invoke-static {p0, p1, p2}, Lcom/google/android/gms/internal/bQ;->a(Lcom/google/android/gms/internal/su$c;Landroid/os/Parcel;I)V

    return-void
.end method
