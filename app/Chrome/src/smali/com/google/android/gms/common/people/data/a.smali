.class public final Lcom/google/android/gms/common/people/data/a;
.super Ljava/lang/Object;


# instance fields
.field private a:Ljava/util/List;

.field private b:I

.field private c:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/gms/common/people/data/a;

    invoke-direct {v0}, Lcom/google/android/gms/common/people/data/a;-><init>()V

    invoke-virtual {v0}, Lcom/google/android/gms/common/people/data/a;->a()Lcom/google/android/gms/common/people/data/Audience;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/common/people/data/a;->a:Ljava/util/List;

    iput v1, p0, Lcom/google/android/gms/common/people/data/a;->b:I

    iput-boolean v1, p0, Lcom/google/android/gms/common/people/data/a;->c:Z

    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/gms/common/people/data/Audience;
    .locals 3

    const/4 v2, 0x0

    new-instance v0, Lcom/google/android/gms/common/people/data/Audience;

    iget-object v1, p0, Lcom/google/android/gms/common/people/data/a;->a:Ljava/util/List;

    invoke-direct {v0, v1, v2, v2}, Lcom/google/android/gms/common/people/data/Audience;-><init>(Ljava/util/List;IZ)V

    return-object v0
.end method

.method public final a(Ljava/util/Collection;)Lcom/google/android/gms/common/people/data/a;
    .locals 2

    new-instance v1, Ljava/util/ArrayList;

    const-string/jumbo v0, "Audience members must not be null."

    invoke-static {p1, v0}, Landroid/support/v4/app/c;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/common/people/data/a;->a:Ljava/util/List;

    return-object p0
.end method
