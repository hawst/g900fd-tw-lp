.class public abstract Lcom/google/android/gms/internal/b;
.super Ljava/lang/Object;


# instance fields
.field protected final a:Lcom/google/android/gms/internal/q;

.field private final b:Ljava/lang/String;

.field private c:Lcom/google/android/gms/internal/t;


# direct methods
.method protected constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Landroid/support/v4/app/b;->b(Ljava/lang/String;)V

    iput-object p1, p0, Lcom/google/android/gms/internal/b;->b:Ljava/lang/String;

    new-instance v0, Lcom/google/android/gms/internal/q;

    invoke-direct {v0, p2}, Lcom/google/android/gms/internal/q;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/gms/internal/b;->a:Lcom/google/android/gms/internal/q;

    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/internal/b;->a:Lcom/google/android/gms/internal/q;

    invoke-virtual {v0, p3}, Lcom/google/android/gms/internal/q;->a(Ljava/lang/String;)V

    :cond_0
    return-void
.end method


# virtual methods
.method public a(JI)V
    .locals 0

    return-void
.end method

.method public final a(Lcom/google/android/gms/internal/t;)V
    .locals 1

    iput-object p1, p0, Lcom/google/android/gms/internal/b;->c:Lcom/google/android/gms/internal/t;

    iget-object v0, p0, Lcom/google/android/gms/internal/b;->c:Lcom/google/android/gms/internal/t;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/internal/b;->e()V

    :cond_0
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 0

    return-void
.end method

.method protected final a(Ljava/lang/String;JLjava/lang/String;)V
    .locals 4

    iget-object v0, p0, Lcom/google/android/gms/internal/b;->a:Lcom/google/android/gms/internal/q;

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    const/4 v2, 0x1

    const/4 v3, 0x0

    aput-object v3, v1, v2

    invoke-virtual {v0}, Lcom/google/android/gms/internal/q;->a()V

    iget-object v0, p0, Lcom/google/android/gms/internal/b;->c:Lcom/google/android/gms/internal/t;

    iget-object v1, p0, Lcom/google/android/gms/internal/b;->b:Ljava/lang/String;

    invoke-interface {v0, v1, p1, p2, p3}, Lcom/google/android/gms/internal/t;->a(Ljava/lang/String;Ljava/lang/String;J)V

    return-void
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/internal/b;->b:Ljava/lang/String;

    return-object v0
.end method

.method protected final d()J
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/internal/b;->c:Lcom/google/android/gms/internal/t;

    invoke-interface {v0}, Lcom/google/android/gms/internal/t;->a()J

    move-result-wide v0

    return-wide v0
.end method

.method public e()V
    .locals 0

    return-void
.end method
