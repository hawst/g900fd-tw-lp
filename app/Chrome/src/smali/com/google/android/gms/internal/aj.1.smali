.class public final Lcom/google/android/gms/internal/aj;
.super Ljava/lang/Object;


# static fields
.field public static final A:Lcom/google/android/gms/internal/ap;

.field public static final B:Lcom/google/android/gms/internal/aq;

.field public static final C:Lcom/google/android/gms/drive/metadata/a;

.field public static final D:Lcom/google/android/gms/drive/metadata/a;

.field public static final E:Lcom/google/android/gms/drive/metadata/a;

.field public static final F:Lcom/google/android/gms/drive/metadata/internal/c;

.field public static final G:Lcom/google/android/gms/drive/metadata/a;

.field public static final a:Lcom/google/android/gms/drive/metadata/a;

.field public static final b:Lcom/google/android/gms/drive/metadata/a;

.field public static final c:Lcom/google/android/gms/drive/metadata/internal/n;

.field public static final d:Lcom/google/android/gms/drive/metadata/a;

.field public static final e:Lcom/google/android/gms/drive/metadata/a;

.field public static final f:Lcom/google/android/gms/drive/metadata/a;

.field public static final g:Lcom/google/android/gms/drive/metadata/a;

.field public static final h:Lcom/google/android/gms/drive/metadata/a;

.field public static final i:Lcom/google/android/gms/drive/metadata/a;

.field public static final j:Lcom/google/android/gms/drive/metadata/a;

.field public static final k:Lcom/google/android/gms/drive/metadata/a;

.field public static final l:Lcom/google/android/gms/drive/metadata/a;

.field public static final m:Lcom/google/android/gms/internal/ak;

.field public static final n:Lcom/google/android/gms/drive/metadata/a;

.field public static final o:Lcom/google/android/gms/drive/metadata/a;

.field public static final p:Lcom/google/android/gms/drive/metadata/a;

.field public static final q:Lcom/google/android/gms/drive/metadata/a;

.field public static final r:Lcom/google/android/gms/internal/al;

.field public static final s:Lcom/google/android/gms/drive/metadata/a;

.field public static final t:Lcom/google/android/gms/drive/metadata/c;

.field public static final u:Lcom/google/android/gms/drive/metadata/internal/n;

.field public static final v:Lcom/google/android/gms/drive/metadata/internal/n;

.field public static final w:Lcom/google/android/gms/internal/am;

.field public static final x:Lcom/google/android/gms/internal/an;

.field public static final y:Lcom/google/android/gms/internal/ao;

.field public static final z:Lcom/google/android/gms/drive/metadata/a;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const v5, 0x5b8d80

    const v4, 0x419ce0

    sget-object v0, Lcom/google/android/gms/internal/ay;->a:Lcom/google/android/gms/internal/ay;

    sput-object v0, Lcom/google/android/gms/internal/aj;->a:Lcom/google/android/gms/drive/metadata/a;

    new-instance v0, Lcom/google/android/gms/drive/metadata/internal/m;

    const-string/jumbo v1, "alternateLink"

    invoke-direct {v0, v1, v4}, Lcom/google/android/gms/drive/metadata/internal/m;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/internal/aj;->b:Lcom/google/android/gms/drive/metadata/a;

    new-instance v0, Lcom/google/android/gms/drive/metadata/internal/n;

    invoke-direct {v0}, Lcom/google/android/gms/drive/metadata/internal/n;-><init>()V

    sput-object v0, Lcom/google/android/gms/internal/aj;->c:Lcom/google/android/gms/drive/metadata/internal/n;

    new-instance v0, Lcom/google/android/gms/drive/metadata/internal/m;

    const-string/jumbo v1, "description"

    invoke-direct {v0, v1, v4}, Lcom/google/android/gms/drive/metadata/internal/m;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/internal/aj;->d:Lcom/google/android/gms/drive/metadata/a;

    new-instance v0, Lcom/google/android/gms/drive/metadata/internal/m;

    const-string/jumbo v1, "embedLink"

    invoke-direct {v0, v1, v4}, Lcom/google/android/gms/drive/metadata/internal/m;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/internal/aj;->e:Lcom/google/android/gms/drive/metadata/a;

    new-instance v0, Lcom/google/android/gms/drive/metadata/internal/m;

    const-string/jumbo v1, "fileExtension"

    invoke-direct {v0, v1, v4}, Lcom/google/android/gms/drive/metadata/internal/m;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/internal/aj;->f:Lcom/google/android/gms/drive/metadata/a;

    new-instance v0, Lcom/google/android/gms/drive/metadata/internal/h;

    const-string/jumbo v1, "fileSize"

    invoke-direct {v0, v1, v4}, Lcom/google/android/gms/drive/metadata/internal/h;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/internal/aj;->g:Lcom/google/android/gms/drive/metadata/a;

    new-instance v0, Lcom/google/android/gms/drive/metadata/internal/c;

    const-string/jumbo v1, "hasThumbnail"

    invoke-direct {v0, v1, v4}, Lcom/google/android/gms/drive/metadata/internal/c;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/internal/aj;->h:Lcom/google/android/gms/drive/metadata/a;

    new-instance v0, Lcom/google/android/gms/drive/metadata/internal/m;

    const-string/jumbo v1, "indexableText"

    invoke-direct {v0, v1, v4}, Lcom/google/android/gms/drive/metadata/internal/m;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/internal/aj;->i:Lcom/google/android/gms/drive/metadata/a;

    new-instance v0, Lcom/google/android/gms/drive/metadata/internal/c;

    const-string/jumbo v1, "isAppData"

    invoke-direct {v0, v1, v4}, Lcom/google/android/gms/drive/metadata/internal/c;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/internal/aj;->j:Lcom/google/android/gms/drive/metadata/a;

    new-instance v0, Lcom/google/android/gms/drive/metadata/internal/c;

    const-string/jumbo v1, "isCopyable"

    invoke-direct {v0, v1, v4}, Lcom/google/android/gms/drive/metadata/internal/c;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/internal/aj;->k:Lcom/google/android/gms/drive/metadata/a;

    new-instance v0, Lcom/google/android/gms/drive/metadata/internal/c;

    const-string/jumbo v1, "isEditable"

    const v2, 0x3e8fa0

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/drive/metadata/internal/c;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/internal/aj;->l:Lcom/google/android/gms/drive/metadata/a;

    new-instance v0, Lcom/google/android/gms/internal/ak;

    const-string/jumbo v1, "isPinned"

    invoke-direct {v0, v1}, Lcom/google/android/gms/internal/ak;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/internal/aj;->m:Lcom/google/android/gms/internal/ak;

    new-instance v0, Lcom/google/android/gms/drive/metadata/internal/c;

    const-string/jumbo v1, "isRestricted"

    invoke-direct {v0, v1, v4}, Lcom/google/android/gms/drive/metadata/internal/c;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/internal/aj;->n:Lcom/google/android/gms/drive/metadata/a;

    new-instance v0, Lcom/google/android/gms/drive/metadata/internal/c;

    const-string/jumbo v1, "isShared"

    invoke-direct {v0, v1, v4}, Lcom/google/android/gms/drive/metadata/internal/c;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/internal/aj;->o:Lcom/google/android/gms/drive/metadata/a;

    new-instance v0, Lcom/google/android/gms/drive/metadata/internal/c;

    const-string/jumbo v1, "isTrashable"

    const v2, 0x432380

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/drive/metadata/internal/c;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/internal/aj;->p:Lcom/google/android/gms/drive/metadata/a;

    new-instance v0, Lcom/google/android/gms/drive/metadata/internal/c;

    const-string/jumbo v1, "isViewed"

    invoke-direct {v0, v1, v4}, Lcom/google/android/gms/drive/metadata/internal/c;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/internal/aj;->q:Lcom/google/android/gms/drive/metadata/a;

    new-instance v0, Lcom/google/android/gms/internal/al;

    const-string/jumbo v1, "mimeType"

    invoke-direct {v0, v1}, Lcom/google/android/gms/internal/al;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/internal/aj;->r:Lcom/google/android/gms/internal/al;

    new-instance v0, Lcom/google/android/gms/drive/metadata/internal/m;

    const-string/jumbo v1, "originalFilename"

    invoke-direct {v0, v1, v4}, Lcom/google/android/gms/drive/metadata/internal/m;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/internal/aj;->s:Lcom/google/android/gms/drive/metadata/a;

    new-instance v0, Lcom/google/android/gms/drive/metadata/internal/l;

    const-string/jumbo v1, "ownerNames"

    invoke-direct {v0, v1}, Lcom/google/android/gms/drive/metadata/internal/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/internal/aj;->t:Lcom/google/android/gms/drive/metadata/c;

    new-instance v0, Lcom/google/android/gms/drive/metadata/internal/n;

    const-string/jumbo v1, "lastModifyingUser"

    invoke-direct {v0, v1}, Lcom/google/android/gms/drive/metadata/internal/n;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/internal/aj;->u:Lcom/google/android/gms/drive/metadata/internal/n;

    new-instance v0, Lcom/google/android/gms/drive/metadata/internal/n;

    const-string/jumbo v1, "sharingUser"

    invoke-direct {v0, v1}, Lcom/google/android/gms/drive/metadata/internal/n;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/internal/aj;->v:Lcom/google/android/gms/drive/metadata/internal/n;

    new-instance v0, Lcom/google/android/gms/internal/am;

    const-string/jumbo v1, "parents"

    invoke-direct {v0, v1}, Lcom/google/android/gms/internal/am;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/internal/aj;->w:Lcom/google/android/gms/internal/am;

    new-instance v0, Lcom/google/android/gms/internal/an;

    const-string/jumbo v1, "quotaBytesUsed"

    invoke-direct {v0, v1}, Lcom/google/android/gms/internal/an;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/internal/aj;->x:Lcom/google/android/gms/internal/an;

    new-instance v0, Lcom/google/android/gms/internal/ao;

    const-string/jumbo v1, "starred"

    invoke-direct {v0, v1}, Lcom/google/android/gms/internal/ao;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/internal/aj;->y:Lcom/google/android/gms/internal/ao;

    new-instance v0, Lcom/google/android/gms/internal/ar;

    const-string/jumbo v1, "thumbnail"

    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;

    move-result-object v2

    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/gms/internal/ar;-><init>(Ljava/lang/String;Ljava/util/Collection;Ljava/util/Collection;)V

    sput-object v0, Lcom/google/android/gms/internal/aj;->z:Lcom/google/android/gms/drive/metadata/a;

    new-instance v0, Lcom/google/android/gms/internal/ap;

    const-string/jumbo v1, "title"

    invoke-direct {v0, v1}, Lcom/google/android/gms/internal/ap;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/internal/aj;->A:Lcom/google/android/gms/internal/ap;

    new-instance v0, Lcom/google/android/gms/internal/aq;

    const-string/jumbo v1, "trashed"

    invoke-direct {v0, v1}, Lcom/google/android/gms/internal/aq;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/internal/aj;->B:Lcom/google/android/gms/internal/aq;

    new-instance v0, Lcom/google/android/gms/drive/metadata/internal/m;

    const-string/jumbo v1, "webContentLink"

    invoke-direct {v0, v1, v4}, Lcom/google/android/gms/drive/metadata/internal/m;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/internal/aj;->C:Lcom/google/android/gms/drive/metadata/a;

    new-instance v0, Lcom/google/android/gms/drive/metadata/internal/m;

    const-string/jumbo v1, "webViewLink"

    invoke-direct {v0, v1, v4}, Lcom/google/android/gms/drive/metadata/internal/m;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/internal/aj;->D:Lcom/google/android/gms/drive/metadata/a;

    new-instance v0, Lcom/google/android/gms/drive/metadata/internal/m;

    const-string/jumbo v1, "uniqueIdentifier"

    const v2, 0x4c4b40

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/drive/metadata/internal/m;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/internal/aj;->E:Lcom/google/android/gms/drive/metadata/a;

    new-instance v0, Lcom/google/android/gms/drive/metadata/internal/c;

    const-string/jumbo v1, "writersCanShare"

    invoke-direct {v0, v1, v5}, Lcom/google/android/gms/drive/metadata/internal/c;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/internal/aj;->F:Lcom/google/android/gms/drive/metadata/internal/c;

    new-instance v0, Lcom/google/android/gms/drive/metadata/internal/m;

    const-string/jumbo v1, "role"

    invoke-direct {v0, v1, v5}, Lcom/google/android/gms/drive/metadata/internal/m;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/internal/aj;->G:Lcom/google/android/gms/drive/metadata/a;

    return-void
.end method
