.class public Lcom/google/android/gms/appdatasearch/AppDataSearchClient;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/support/v4/view/i;


# static fields
.field public static final TAG:Ljava/lang/String; = "AppDataSearchClient"


# instance fields
.field private final BN:Landroid/os/ConditionVariable;

.field private BO:Lcom/google/android/gms/common/ConnectionResult;

.field private final BP:Lcom/google/android/gms/internal/cr;

.field private final mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 4

    const/4 v3, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Landroid/os/ConditionVariable;

    invoke-direct {v0}, Landroid/os/ConditionVariable;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/appdatasearch/AppDataSearchClient;->BN:Landroid/os/ConditionVariable;

    iput-object p1, p0, Lcom/google/android/gms/appdatasearch/AppDataSearchClient;->mContext:Landroid/content/Context;

    new-instance v0, Lcom/google/android/gms/internal/cr;

    new-instance v1, Lcom/google/android/gms/appdatasearch/AppDataSearchClient$a;

    invoke-direct {v1, p0, v3}, Lcom/google/android/gms/appdatasearch/AppDataSearchClient$a;-><init>(Lcom/google/android/gms/appdatasearch/AppDataSearchClient;Lcom/google/android/gms/appdatasearch/AppDataSearchClient$1;)V

    new-instance v2, Lcom/google/android/gms/appdatasearch/AppDataSearchClient$b;

    invoke-direct {v2, p0, v3}, Lcom/google/android/gms/appdatasearch/AppDataSearchClient$b;-><init>(Lcom/google/android/gms/appdatasearch/AppDataSearchClient;Lcom/google/android/gms/appdatasearch/AppDataSearchClient$1;)V

    invoke-direct {v0, p1, v1, v2}, Lcom/google/android/gms/internal/cr;-><init>(Landroid/content/Context;Lcom/google/android/gms/common/b;Lcom/google/android/gms/common/c;)V

    iput-object v0, p0, Lcom/google/android/gms/appdatasearch/AppDataSearchClient;->BP:Lcom/google/android/gms/internal/cr;

    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/appdatasearch/AppDataSearchClient;)Landroid/os/ConditionVariable;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/appdatasearch/AppDataSearchClient;->BN:Landroid/os/ConditionVariable;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/gms/appdatasearch/AppDataSearchClient;Lcom/google/android/gms/common/ConnectionResult;)Lcom/google/android/gms/common/ConnectionResult;
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/appdatasearch/AppDataSearchClient;->BO:Lcom/google/android/gms/common/ConnectionResult;

    return-object p1
.end method

.method private a(Lcom/google/android/gms/appdatasearch/ac;)Z
    .locals 3

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/appdatasearch/AppDataSearchClient;->BP:Lcom/google/android/gms/internal/cr;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/cr;->j()Lcom/google/android/gms/internal/cs;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/google/android/gms/internal/cs;->a(Lcom/google/android/gms/appdatasearch/ac;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    :goto_0
    return v0

    :catch_0
    move-exception v0

    const-string/jumbo v1, "AppDataSearchClient"

    const-string/jumbo v2, "ReportUserClick failed."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    const/4 v0, 0x0

    goto :goto_0
.end method

.method private f(Landroid/net/Uri;)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/gms/appdatasearch/AppDataSearchClient;->mContext:Landroid/content/Context;

    const-string/jumbo v1, "com.google.android.gms"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, p1, v2}, Landroid/content/Context;->grantUriPermission(Ljava/lang/String;Landroid/net/Uri;I)V

    return-void
.end method

.method public static verifyContentProviderClient(Landroid/content/Context;)V
    .locals 4

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v0

    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v1

    if-ne v0, v1, :cond_1

    const-string/jumbo v0, "AppDataSearchClient"

    const-string/jumbo v1, "verifyContentProviderClient: caller is current process"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    return-void

    :cond_1
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    const-string/jumbo v2, "com.google.android.gms"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v1

    iget v1, v1, Landroid/content/pm/ApplicationInfo;->uid:I

    if-eq v1, v0, :cond_2

    new-instance v1, Ljava/lang/SecurityException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "Calling UID "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v2, " is not Google Play Services."

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/SecurityException;

    const-string/jumbo v2, "Google Play Services not installed"

    invoke-direct {v1, v2, v0}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    :cond_2
    invoke-static {p0}, Lcom/google/android/gms/common/f;->a(Landroid/content/Context;)I

    move-result v0

    if-eqz v0, :cond_0

    new-instance v1, Ljava/lang/SecurityException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "Calling package problem: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcom/google/android/gms/common/f;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v1
.end method


# virtual methods
.method public blockPackages([Ljava/lang/String;)V
    .locals 3

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/appdatasearch/AppDataSearchClient;->BP:Lcom/google/android/gms/internal/cr;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/cr;->j()Lcom/google/android/gms/internal/cs;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/google/android/gms/internal/cs;->a([Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string/jumbo v1, "AppDataSearchClient"

    const-string/jumbo v2, "Block packages failed."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public clearUsageReportData()Z
    .locals 3

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/appdatasearch/AppDataSearchClient;->BP:Lcom/google/android/gms/internal/cr;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/cr;->j()Lcom/google/android/gms/internal/cs;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/internal/cs;->g()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    :goto_0
    return v0

    :catch_0
    move-exception v0

    const-string/jumbo v1, "AppDataSearchClient"

    const-string/jumbo v2, "clearUsageReportData failed."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public connect()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/appdatasearch/AppDataSearchClient;->BO:Lcom/google/android/gms/common/ConnectionResult;

    iget-object v0, p0, Lcom/google/android/gms/appdatasearch/AppDataSearchClient;->BN:Landroid/os/ConditionVariable;

    invoke-virtual {v0}, Landroid/os/ConditionVariable;->close()V

    iget-object v0, p0, Lcom/google/android/gms/appdatasearch/AppDataSearchClient;->BP:Lcom/google/android/gms/internal/cr;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/cr;->a()V

    return-void
.end method

.method public connectWithTimeout(J)Lcom/google/android/gms/common/ConnectionResult;
    .locals 3

    invoke-virtual {p0}, Lcom/google/android/gms/appdatasearch/AppDataSearchClient;->connect()V

    iget-object v0, p0, Lcom/google/android/gms/appdatasearch/AppDataSearchClient;->BN:Landroid/os/ConditionVariable;

    invoke-virtual {v0, p1, p2}, Landroid/os/ConditionVariable;->block(J)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/appdatasearch/AppDataSearchClient;->disconnect()V

    new-instance v0, Lcom/google/android/gms/common/ConnectionResult;

    const/16 v1, 0x8

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/common/ConnectionResult;-><init>(ILandroid/app/PendingIntent;)V

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/appdatasearch/AppDataSearchClient;->BO:Lcom/google/android/gms/common/ConnectionResult;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/appdatasearch/AppDataSearchClient;->BO:Lcom/google/android/gms/common/ConnectionResult;

    goto :goto_0

    :cond_1
    sget-object v0, Lcom/google/android/gms/common/ConnectionResult;->LP:Lcom/google/android/gms/common/ConnectionResult;

    goto :goto_0
.end method

.method public diagnostic(Landroid/os/Bundle;)Landroid/os/Bundle;
    .locals 3

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/appdatasearch/AppDataSearchClient;->BP:Lcom/google/android/gms/internal/cr;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/cr;->j()Lcom/google/android/gms/internal/cs;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/google/android/gms/internal/cs;->a(Landroid/os/Bundle;)Landroid/os/Bundle;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    const-string/jumbo v1, "AppDataSearchClient"

    const-string/jumbo v2, "Diagnostic failed."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public disconnect()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/appdatasearch/AppDataSearchClient;->BP:Lcom/google/android/gms/internal/cr;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/cr;->b()V

    return-void
.end method

.method public getContextualIMEUpdates(Ljava/lang/String;I[B)Lcom/google/android/gms/appdatasearch/PIMEUpdateResponse;
    .locals 3

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/appdatasearch/AppDataSearchClient;->BP:Lcom/google/android/gms/internal/cr;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/cr;->j()Lcom/google/android/gms/internal/cs;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/appdatasearch/AppDataSearchClient;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p1, p2, p3}, Lcom/google/android/gms/internal/cs;->a(Ljava/lang/String;Ljava/lang/String;I[B)Lcom/google/android/gms/appdatasearch/PIMEUpdateResponse;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    const-string/jumbo v1, "AppDataSearchClient"

    const-string/jumbo v2, "GetContextualIMEUpdates failed."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getCorpusHandlesRegisteredForIME()[Ljava/lang/String;
    .locals 3

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/appdatasearch/AppDataSearchClient;->BP:Lcom/google/android/gms/internal/cr;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/cr;->j()Lcom/google/android/gms/internal/cs;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/appdatasearch/AppDataSearchClient;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/gms/internal/cs;->e(Ljava/lang/String;)[Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    const-string/jumbo v1, "AppDataSearchClient"

    const-string/jumbo v2, "GetCorpusHandlesRegisteredForIME failed."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getCorpusInfo(Ljava/lang/String;)Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;
    .locals 3

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/appdatasearch/AppDataSearchClient;->BP:Lcom/google/android/gms/internal/cr;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/cr;->j()Lcom/google/android/gms/internal/cs;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/appdatasearch/AppDataSearchClient;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Lcom/google/android/gms/internal/cs;->c(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    const-string/jumbo v1, "AppDataSearchClient"

    const-string/jumbo v2, "Get corpus info failed."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getCorpusNames()[Ljava/lang/String;
    .locals 3

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/appdatasearch/AppDataSearchClient;->BP:Lcom/google/android/gms/internal/cr;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/cr;->j()Lcom/google/android/gms/internal/cs;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/appdatasearch/AppDataSearchClient;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/gms/internal/cs;->f(Ljava/lang/String;)[Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    const-string/jumbo v1, "AppDataSearchClient"

    const-string/jumbo v2, "Get corpus names failed."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getCorpusStatus(Ljava/lang/String;)Lcom/google/android/gms/appdatasearch/CorpusStatus;
    .locals 3

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/appdatasearch/AppDataSearchClient;->BP:Lcom/google/android/gms/internal/cr;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/cr;->j()Lcom/google/android/gms/internal/cs;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/appdatasearch/AppDataSearchClient;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Lcom/google/android/gms/internal/cs;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/appdatasearch/CorpusStatus;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    const-string/jumbo v1, "AppDataSearchClient"

    const-string/jumbo v2, "Get corpus status failed."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getCurrentExperimentIds()[I
    .locals 3

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/appdatasearch/AppDataSearchClient;->BP:Lcom/google/android/gms/internal/cr;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/cr;->j()Lcom/google/android/gms/internal/cs;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/internal/cs;->e()[I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    const-string/jumbo v1, "AppDataSearchClient"

    const-string/jumbo v2, "getCurrentExperimentIds failed."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getDocuments([Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/appdatasearch/QuerySpecification;)Lcom/google/android/gms/appdatasearch/DocumentResults;
    .locals 3

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/appdatasearch/AppDataSearchClient;->BP:Lcom/google/android/gms/internal/cr;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/cr;->j()Lcom/google/android/gms/internal/cs;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/appdatasearch/AppDataSearchClient;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, p1, v1, p2, p3}, Lcom/google/android/gms/internal/cs;->a([Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/appdatasearch/QuerySpecification;)Lcom/google/android/gms/appdatasearch/DocumentResults;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    const-string/jumbo v1, "AppDataSearchClient"

    const-string/jumbo v2, "Getting documents failed."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getGlobalSearchApplications()[Lcom/google/android/gms/appdatasearch/GlobalSearchApplication;
    .locals 3

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/appdatasearch/AppDataSearchClient;->BP:Lcom/google/android/gms/internal/cr;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/cr;->j()Lcom/google/android/gms/internal/cs;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/internal/cs;->d()[Lcom/google/android/gms/appdatasearch/GlobalSearchApplication;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    const-string/jumbo v1, "AppDataSearchClient"

    const-string/jumbo v2, "Get UniversalSearchableApps failed."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getGlobalSearchRegisteredApplications()[Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;
    .locals 3

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/appdatasearch/AppDataSearchClient;->BP:Lcom/google/android/gms/internal/cr;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/cr;->j()Lcom/google/android/gms/internal/cs;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/internal/cs;->a()[Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    const-string/jumbo v1, "AppDataSearchClient"

    const-string/jumbo v2, "Get UniversalSearchableApps failed."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getIMEUpdates(I[B)Lcom/google/android/gms/appdatasearch/PIMEUpdateResponse;
    .locals 4

    const/4 v0, 0x0

    :goto_0
    const/4 v1, 0x1

    if-le p1, v1, :cond_1

    :try_start_0
    iget-object v1, p0, Lcom/google/android/gms/appdatasearch/AppDataSearchClient;->BP:Lcom/google/android/gms/internal/cr;

    invoke-virtual {v1}, Lcom/google/android/gms/internal/cr;->j()Lcom/google/android/gms/internal/cs;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/appdatasearch/AppDataSearchClient;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2, p1, p2}, Lcom/google/android/gms/internal/cs;->a(Ljava/lang/String;I[B)Lcom/google/android/gms/appdatasearch/PIMEUpdateResponse;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_1
    return-object v0

    :catch_0
    move-exception v1

    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0xf

    if-lt v2, v3, :cond_0

    instance-of v2, v1, Landroid/os/TransactionTooLargeException;

    if-eqz v2, :cond_0

    const-string/jumbo v1, "AppDataSearchClient"

    const-string/jumbo v2, "Transaction failed. Re-trying GetIMEUpdates with fewer updates."

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    div-int/lit8 p1, p1, 0x2

    goto :goto_0

    :cond_0
    const-string/jumbo v2, "AppDataSearchClient"

    const-string/jumbo v3, "GetIMEUpdates failed."

    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1

    :cond_1
    const-string/jumbo v1, "AppDataSearchClient"

    const-string/jumbo v2, "GetIMEUpdates failed with smallest possible number of updates."

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public getNativeApiInfo()Lcom/google/android/gms/appdatasearch/NativeApiInfo;
    .locals 3

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/appdatasearch/AppDataSearchClient;->BP:Lcom/google/android/gms/internal/cr;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/cr;->j()Lcom/google/android/gms/internal/cs;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/internal/cs;->h()Lcom/google/android/gms/appdatasearch/NativeApiInfo;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    const-string/jumbo v1, "AppDataSearchClient"

    const-string/jumbo v2, "Getting native api failed."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getPendingExperimentIds()[I
    .locals 3

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/appdatasearch/AppDataSearchClient;->BP:Lcom/google/android/gms/internal/cr;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/cr;->j()Lcom/google/android/gms/internal/cs;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/internal/cs;->f()[I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    const-string/jumbo v1, "AppDataSearchClient"

    const-string/jumbo v2, "getCurrentExperimentIds failed."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getPhraseAffinity([Ljava/lang/String;Lcom/google/android/gms/appdatasearch/PhraseAffinitySpecification;)Lcom/google/android/gms/appdatasearch/PhraseAffinityResponse;
    .locals 3

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/appdatasearch/AppDataSearchClient;->BP:Lcom/google/android/gms/internal/cr;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/cr;->j()Lcom/google/android/gms/internal/cs;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Lcom/google/android/gms/internal/cs;->a([Ljava/lang/String;Lcom/google/android/gms/appdatasearch/PhraseAffinitySpecification;)Lcom/google/android/gms/appdatasearch/PhraseAffinityResponse;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    const-string/jumbo v1, "AppDataSearchClient"

    const-string/jumbo v2, "Getting phrase affinity failed."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getSearchService()Lcom/google/android/gms/internal/cs;
    .locals 3

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/appdatasearch/AppDataSearchClient;->BP:Lcom/google/android/gms/internal/cr;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/cr;->j()Lcom/google/android/gms/internal/cs;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    const-string/jumbo v1, "AppDataSearchClient"

    const-string/jumbo v2, "Getting search service failed."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getStorageStatistics()Lcom/google/android/gms/appdatasearch/StorageStats;
    .locals 3

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/appdatasearch/AppDataSearchClient;->BP:Lcom/google/android/gms/internal/cr;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/cr;->j()Lcom/google/android/gms/internal/cs;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/internal/cs;->c()Lcom/google/android/gms/appdatasearch/StorageStats;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    const-string/jumbo v1, "AppDataSearchClient"

    const-string/jumbo v2, "Get storage statistics failed."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isConnected()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/appdatasearch/AppDataSearchClient;->BP:Lcom/google/android/gms/internal/cr;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/cr;->c()Z

    move-result v0

    return v0
.end method

.method public isConnecting()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/appdatasearch/AppDataSearchClient;->BP:Lcom/google/android/gms/internal/cr;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/cr;->g()Z

    move-result v0

    return v0
.end method

.method public isConnectionCallbacksRegistered(Lcom/google/android/gms/common/b;)Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/appdatasearch/AppDataSearchClient;->BP:Lcom/google/android/gms/internal/cr;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/internal/cr;->b(Lcom/google/android/gms/common/b;)Z

    move-result v0

    return v0
.end method

.method public isConnectionFailedListenerRegistered(Lcom/google/android/gms/common/c;)Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/appdatasearch/AppDataSearchClient;->BP:Lcom/google/android/gms/internal/cr;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/internal/cr;->c(Lcom/google/android/gms/common/c;)Z

    move-result v0

    return v0
.end method

.method public query(Ljava/lang/String;[Ljava/lang/String;IILcom/google/android/gms/appdatasearch/QuerySpecification;)Lcom/google/android/gms/appdatasearch/SearchResults;
    .locals 7

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/appdatasearch/AppDataSearchClient;->BP:Lcom/google/android/gms/internal/cr;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/cr;->j()Lcom/google/android/gms/internal/cs;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/appdatasearch/AppDataSearchClient;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    move-object v1, p1

    move-object v3, p2

    move v4, p3

    move v5, p4

    move-object v6, p5

    invoke-interface/range {v0 .. v6}, Lcom/google/android/gms/internal/cs;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;IILcom/google/android/gms/appdatasearch/QuerySpecification;)Lcom/google/android/gms/appdatasearch/SearchResults;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    const-string/jumbo v1, "AppDataSearchClient"

    const-string/jumbo v2, "Query failed."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public queryGlobalSearch(Ljava/lang/String;IILcom/google/android/gms/appdatasearch/GlobalSearchQuerySpecification;)Lcom/google/android/gms/appdatasearch/SearchResults;
    .locals 3

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/appdatasearch/AppDataSearchClient;->BP:Lcom/google/android/gms/internal/cr;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/cr;->j()Lcom/google/android/gms/internal/cs;

    move-result-object v0

    invoke-interface {v0, p1, p2, p3, p4}, Lcom/google/android/gms/internal/cs;->a(Ljava/lang/String;IILcom/google/android/gms/appdatasearch/GlobalSearchQuerySpecification;)Lcom/google/android/gms/appdatasearch/SearchResults;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    const-string/jumbo v1, "AppDataSearchClient"

    const-string/jumbo v2, "Query failed."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public registerConnectionCallbacks(Lcom/google/android/gms/common/b;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/appdatasearch/AppDataSearchClient;->BP:Lcom/google/android/gms/internal/cr;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/internal/cr;->a(Lcom/google/android/gms/common/b;)V

    return-void
.end method

.method public registerConnectionFailedListener(Lcom/google/android/gms/common/c;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/appdatasearch/AppDataSearchClient;->BP:Lcom/google/android/gms/internal/cr;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/internal/cr;->b(Lcom/google/android/gms/common/c;)V

    return-void
.end method

.method public registerCorpus(Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;)Z
    .locals 3

    iget-object v0, p1, Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;->d:Landroid/net/Uri;

    invoke-direct {p0, v0}, Lcom/google/android/gms/appdatasearch/AppDataSearchClient;->f(Landroid/net/Uri;)V

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/appdatasearch/AppDataSearchClient;->BP:Lcom/google/android/gms/internal/cr;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/cr;->j()Lcom/google/android/gms/internal/cs;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/appdatasearch/AppDataSearchClient;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Lcom/google/android/gms/internal/cs;->b(Ljava/lang/String;Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    :goto_0
    return v0

    :catch_0
    move-exception v0

    const-string/jumbo v1, "AppDataSearchClient"

    const-string/jumbo v2, "Register corpus failed."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public registerGlobalSearchApplication(Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;)Z
    .locals 3

    iget-object v0, p0, Lcom/google/android/gms/appdatasearch/AppDataSearchClient;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;->a(Ljava/lang/String;)Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;

    move-result-object v0

    :try_start_0
    iget-object v1, p0, Lcom/google/android/gms/appdatasearch/AppDataSearchClient;->BP:Lcom/google/android/gms/internal/cr;

    invoke-virtual {v1}, Lcom/google/android/gms/internal/cr;->j()Lcom/google/android/gms/internal/cs;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/google/android/gms/internal/cs;->a(Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :catch_0
    move-exception v0

    const-string/jumbo v1, "AppDataSearchClient"

    const-string/jumbo v2, "Register UniversalSearchableAppInfo failed."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public reportResultClick(Ljava/lang/String;Lcom/google/android/gms/appdatasearch/SearchResults;I)Z
    .locals 6

    new-instance v1, Ljava/util/ArrayList;

    invoke-virtual {p2}, Lcom/google/android/gms/appdatasearch/SearchResults;->b()I

    move-result v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    invoke-virtual {p2}, Lcom/google/android/gms/appdatasearch/SearchResults;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/appdatasearch/e;

    new-instance v3, Lcom/google/android/gms/appdatasearch/DocumentId;

    invoke-virtual {v0}, Lcom/google/android/gms/appdatasearch/e;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0}, Lcom/google/android/gms/appdatasearch/e;->b()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0}, Lcom/google/android/gms/appdatasearch/e;->c()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v3, v4, v5, v0}, Lcom/google/android/gms/appdatasearch/DocumentId;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    invoke-virtual {p0, p1, v1, p3}, Lcom/google/android/gms/appdatasearch/AppDataSearchClient;->reportResultClick(Ljava/lang/String;Ljava/util/List;I)Z

    move-result v0

    return v0
.end method

.method public reportResultClick(Ljava/lang/String;Ljava/util/List;I)Z
    .locals 2

    new-instance v1, Lcom/google/android/gms/appdatasearch/ac;

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Lcom/google/android/gms/appdatasearch/DocumentId;

    invoke-interface {p2, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/gms/appdatasearch/DocumentId;

    invoke-direct {v1, p1, v0, p3}, Lcom/google/android/gms/appdatasearch/ac;-><init>(Ljava/lang/String;[Lcom/google/android/gms/appdatasearch/DocumentId;I)V

    invoke-direct {p0, v1}, Lcom/google/android/gms/appdatasearch/AppDataSearchClient;->a(Lcom/google/android/gms/appdatasearch/ac;)Z

    move-result v0

    return v0
.end method

.method public requestIndexing(Ljava/lang/String;J)Z
    .locals 8

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/appdatasearch/AppDataSearchClient;->BP:Lcom/google/android/gms/internal/cr;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/cr;->j()Lcom/google/android/gms/internal/cs;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/gms/appdatasearch/AppDataSearchClient;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    const/4 v6, 0x0

    move-object v3, p1

    move-wide v4, p2

    invoke-interface/range {v1 .. v6}, Lcom/google/android/gms/internal/cs;->a(Ljava/lang/String;Ljava/lang/String;JLcom/google/android/gms/appdatasearch/RequestIndexingSpecification;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    :goto_0
    return v0

    :catch_0
    move-exception v0

    const-string/jumbo v1, "AppDataSearchClient"

    const-string/jumbo v2, "Request indexing failed."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setExperimentIds([BZ)Z
    .locals 3

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/appdatasearch/AppDataSearchClient;->BP:Lcom/google/android/gms/internal/cr;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/cr;->j()Lcom/google/android/gms/internal/cs;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Lcom/google/android/gms/internal/cs;->a([BZ)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    :goto_0
    return v0

    :catch_0
    move-exception v0

    const-string/jumbo v1, "AppDataSearchClient"

    const-string/jumbo v2, "SetExperimentIds failed."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setIncludeInGlobalSearch(Ljava/lang/String;Z)V
    .locals 3

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/appdatasearch/AppDataSearchClient;->BP:Lcom/google/android/gms/internal/cr;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/cr;->j()Lcom/google/android/gms/internal/cs;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Lcom/google/android/gms/internal/cs;->a(Ljava/lang/String;Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string/jumbo v1, "AppDataSearchClient"

    const-string/jumbo v2, "Set include in global search failed."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public setRegisteredCorpora(Ljava/util/Collection;)Z
    .locals 8

    const/4 v1, 0x0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/appdatasearch/AppDataSearchClient;->BP:Lcom/google/android/gms/internal/cr;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/cr;->j()Lcom/google/android/gms/internal/cs;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/gms/appdatasearch/AppDataSearchClient;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Lcom/google/android/gms/internal/cs;->a(Ljava/lang/String;)[Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    new-instance v4, Ljava/util/HashSet;

    invoke-interface {p1}, Ljava/util/Collection;->size()I

    move-result v0

    invoke-direct {v4, v0}, Ljava/util/HashSet;-><init>(I)V

    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;

    iget-object v0, v0, Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;->b:Ljava/lang/String;

    invoke-interface {v4, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :catch_0
    move-exception v0

    const-string/jumbo v2, "AppDataSearchClient"

    const-string/jumbo v3, "Getting corpora failed."

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move v2, v1

    :cond_0
    return v2

    :cond_1
    const/4 v0, 0x1

    array-length v5, v3

    move v2, v1

    :goto_1
    if-ge v2, v5, :cond_3

    aget-object v6, v3, v2

    invoke-interface {v4, v6}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_2

    invoke-virtual {p0, v6}, Lcom/google/android/gms/appdatasearch/AppDataSearchClient;->unregisterCorpus(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_2

    move v0, v1

    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_3
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v2, v0

    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/appdatasearch/AppDataSearchClient;->registerCorpus(Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;)Z

    move-result v0

    if-nez v0, :cond_4

    move v0, v1

    :goto_3
    move v2, v0

    goto :goto_2

    :cond_4
    move v0, v2

    goto :goto_3
.end method

.method public suggest(Ljava/lang/String;[Ljava/lang/String;I)Lcom/google/android/gms/appdatasearch/SuggestionResults;
    .locals 7

    const/4 v6, 0x0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/appdatasearch/AppDataSearchClient;->BP:Lcom/google/android/gms/internal/cr;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/cr;->j()Lcom/google/android/gms/internal/cs;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/appdatasearch/AppDataSearchClient;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    const/4 v5, 0x0

    move-object v1, p1

    move-object v3, p2

    move v4, p3

    invoke-interface/range {v0 .. v5}, Lcom/google/android/gms/internal/cs;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;ILcom/google/android/gms/appdatasearch/SuggestSpecification;)Lcom/google/android/gms/appdatasearch/SuggestionResults;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    const-string/jumbo v1, "AppDataSearchClient"

    const-string/jumbo v2, "Suggest failed."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move-object v0, v6

    goto :goto_0
.end method

.method public triggerCompaction()V
    .locals 3

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/appdatasearch/AppDataSearchClient;->BP:Lcom/google/android/gms/internal/cr;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/cr;->j()Lcom/google/android/gms/internal/cs;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/internal/cs;->b()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string/jumbo v1, "AppDataSearchClient"

    const-string/jumbo v2, "TriggerCompaction failed."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public unblockPackages([Ljava/lang/String;)V
    .locals 3

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/appdatasearch/AppDataSearchClient;->BP:Lcom/google/android/gms/internal/cr;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/cr;->j()Lcom/google/android/gms/internal/cs;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/google/android/gms/internal/cs;->b([Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string/jumbo v1, "AppDataSearchClient"

    const-string/jumbo v2, "Unblock packages failed."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public unregisterAllCorpora()Z
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/appdatasearch/AppDataSearchClient;->unregisterCorpus(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public unregisterConnectionCallbacks(Lcom/google/android/gms/common/b;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/appdatasearch/AppDataSearchClient;->BP:Lcom/google/android/gms/internal/cr;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/internal/cr;->c(Lcom/google/android/gms/common/b;)V

    return-void
.end method

.method public unregisterConnectionFailedListener(Lcom/google/android/gms/common/c;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/appdatasearch/AppDataSearchClient;->BP:Lcom/google/android/gms/internal/cr;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/internal/cr;->d(Lcom/google/android/gms/common/c;)V

    return-void
.end method

.method public unregisterCorpus(Ljava/lang/String;)Z
    .locals 9

    const/4 v1, 0x1

    const/4 v0, 0x0

    :try_start_0
    iget-object v2, p0, Lcom/google/android/gms/appdatasearch/AppDataSearchClient;->BP:Lcom/google/android/gms/internal/cr;

    invoke-virtual {v2}, Lcom/google/android/gms/internal/cr;->j()Lcom/google/android/gms/internal/cs;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/appdatasearch/AppDataSearchClient;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3, p1}, Lcom/google/android/gms/internal/cs;->b(Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v3

    const-string/jumbo v2, "content_provider_uris"

    invoke-virtual {v3, v2}, Landroid/os/Bundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    array-length v5, v4

    move v2, v0

    :goto_0
    if-ge v2, v5, :cond_0

    aget-object v6, v4, v2

    iget-object v7, p0, Lcom/google/android/gms/appdatasearch/AppDataSearchClient;->mContext:Landroid/content/Context;

    invoke-static {v6}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    const/4 v8, 0x1

    invoke-virtual {v7, v6, v8}, Landroid/content/Context;->revokeUriPermission(Landroid/net/Uri;I)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    const-string/jumbo v2, "success"

    invoke-virtual {v3, v2}, Landroid/os/Bundle;->getBooleanArray(Ljava/lang/String;)[Z

    move-result-object v3

    move v2, v0

    :goto_1
    array-length v4, v3

    if-ge v2, v4, :cond_2

    aget-boolean v4, v3, v2
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v4, :cond_1

    :goto_2
    return v0

    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :catch_0
    move-exception v1

    const-string/jumbo v2, "AppDataSearchClient"

    const-string/jumbo v3, "Unregister corpus failed."

    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_2

    :cond_2
    move v0, v1

    goto :goto_2
.end method

.method public unregisterGlobalSearchApplication()Z
    .locals 3

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/appdatasearch/AppDataSearchClient;->BP:Lcom/google/android/gms/internal/cr;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/cr;->j()Lcom/google/android/gms/internal/cs;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/appdatasearch/AppDataSearchClient;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/gms/internal/cs;->b(Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :catch_0
    move-exception v0

    const-string/jumbo v1, "AppDataSearchClient"

    const-string/jumbo v2, "Unregister UniversalSearchableApp failed."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    const/4 v0, 0x0

    goto :goto_0
.end method
