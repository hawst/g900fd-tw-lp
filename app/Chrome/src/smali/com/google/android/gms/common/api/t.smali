.class final Lcom/google/android/gms/common/api/t;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/common/c;


# instance fields
.field private synthetic a:Lcom/google/android/gms/common/api/c;

.field private synthetic b:Lcom/google/android/gms/common/api/q;


# direct methods
.method constructor <init>(Lcom/google/android/gms/common/api/q;Lcom/google/android/gms/common/api/c;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/common/api/t;->b:Lcom/google/android/gms/common/api/q;

    iput-object p2, p0, Lcom/google/android/gms/common/api/t;->a:Lcom/google/android/gms/common/api/c;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onConnectionFailed(Lcom/google/android/gms/common/ConnectionResult;)V
    .locals 2

    const v1, 0x7fffffff

    iget-object v0, p0, Lcom/google/android/gms/common/api/t;->b:Lcom/google/android/gms/common/api/q;

    invoke-static {v0}, Lcom/google/android/gms/common/api/q;->a(Lcom/google/android/gms/common/api/q;)Ljava/util/concurrent/locks/Lock;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/common/api/t;->b:Lcom/google/android/gms/common/api/q;

    invoke-static {v0}, Lcom/google/android/gms/common/api/q;->i(Lcom/google/android/gms/common/api/q;)Lcom/google/android/gms/common/ConnectionResult;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/common/api/t;->a:Lcom/google/android/gms/common/api/c;

    iget-object v0, p0, Lcom/google/android/gms/common/api/t;->b:Lcom/google/android/gms/common/api/q;

    invoke-static {v0}, Lcom/google/android/gms/common/api/q;->j(Lcom/google/android/gms/common/api/q;)I

    move-result v0

    if-ge v1, v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/common/api/t;->b:Lcom/google/android/gms/common/api/q;

    invoke-static {v0, p1}, Lcom/google/android/gms/common/api/q;->a(Lcom/google/android/gms/common/api/q;Lcom/google/android/gms/common/ConnectionResult;)Lcom/google/android/gms/common/ConnectionResult;

    iget-object v0, p0, Lcom/google/android/gms/common/api/t;->b:Lcom/google/android/gms/common/api/q;

    iget-object v1, p0, Lcom/google/android/gms/common/api/t;->a:Lcom/google/android/gms/common/api/c;

    const v1, 0x7fffffff

    invoke-static {v0, v1}, Lcom/google/android/gms/common/api/q;->c(Lcom/google/android/gms/common/api/q;I)I

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/common/api/t;->b:Lcom/google/android/gms/common/api/q;

    invoke-static {v0}, Lcom/google/android/gms/common/api/q;->d(Lcom/google/android/gms/common/api/q;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lcom/google/android/gms/common/api/t;->b:Lcom/google/android/gms/common/api/q;

    invoke-static {v0}, Lcom/google/android/gms/common/api/q;->a(Lcom/google/android/gms/common/api/q;)Ljava/util/concurrent/locks/Lock;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    return-void

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/gms/common/api/t;->b:Lcom/google/android/gms/common/api/q;

    invoke-static {v1}, Lcom/google/android/gms/common/api/q;->a(Lcom/google/android/gms/common/api/q;)Ljava/util/concurrent/locks/Lock;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0
.end method
