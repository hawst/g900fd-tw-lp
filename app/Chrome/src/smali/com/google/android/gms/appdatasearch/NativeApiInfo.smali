.class public Lcom/google/android/gms/appdatasearch/NativeApiInfo;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Lcom/google/android/gms/appdatasearch/K;


# instance fields
.field final mVersionCode:I

.field public final sharedLibAbsoluteFilename:Ljava/lang/String;

.field public final sharedLibExtensionAbsoluteFilename:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/gms/appdatasearch/K;

    invoke-direct {v0}, Lcom/google/android/gms/appdatasearch/K;-><init>()V

    sput-object v0, Lcom/google/android/gms/appdatasearch/NativeApiInfo;->CREATOR:Lcom/google/android/gms/appdatasearch/K;

    return-void
.end method

.method constructor <init>(ILjava/lang/String;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/gms/appdatasearch/NativeApiInfo;->mVersionCode:I

    iput-object p2, p0, Lcom/google/android/gms/appdatasearch/NativeApiInfo;->sharedLibAbsoluteFilename:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/gms/appdatasearch/NativeApiInfo;->sharedLibExtensionAbsoluteFilename:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0, p1, p2}, Lcom/google/android/gms/appdatasearch/NativeApiInfo;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    sget-object v0, Lcom/google/android/gms/appdatasearch/NativeApiInfo;->CREATOR:Lcom/google/android/gms/appdatasearch/K;

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    sget-object v0, Lcom/google/android/gms/appdatasearch/NativeApiInfo;->CREATOR:Lcom/google/android/gms/appdatasearch/K;

    invoke-static {p0, p1}, Lcom/google/android/gms/appdatasearch/K;->a(Lcom/google/android/gms/appdatasearch/NativeApiInfo;Landroid/os/Parcel;)V

    return-void
.end method
