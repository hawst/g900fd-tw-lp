.class public final Lcom/google/android/gms/location/copresence/b;
.super Ljava/lang/Object;


# instance fields
.field private a:Z

.field private b:Z

.field private c:I


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/location/copresence/b;->c:I

    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/gms/location/copresence/b;
    .locals 2

    const/4 v1, 0x1

    const-string/jumbo v0, "Cannot call setNoOptInRequired() in conjunction with setLowPower()."

    invoke-static {v1, v0}, Landroid/support/v4/app/c;->a(ZLjava/lang/Object;)V

    iput-boolean v1, p0, Lcom/google/android/gms/location/copresence/b;->a:Z

    return-object p0
.end method

.method public final b()Lcom/google/android/gms/location/copresence/b;
    .locals 2

    const/4 v1, 0x1

    const-string/jumbo v0, "Cannot call setNoOptInRequired() in conjunction with setWakeUpOthers()."

    invoke-static {v1, v0}, Landroid/support/v4/app/c;->a(ZLjava/lang/Object;)V

    iput-boolean v1, p0, Lcom/google/android/gms/location/copresence/b;->b:Z

    return-object p0
.end method

.method public final c()Lcom/google/android/gms/location/copresence/a;
    .locals 6

    const/4 v4, 0x0

    new-instance v0, Lcom/google/android/gms/internal/mt;

    iget-boolean v1, p0, Lcom/google/android/gms/location/copresence/b;->a:Z

    if-nez v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    iget-boolean v2, p0, Lcom/google/android/gms/location/copresence/b;->b:Z

    iget v3, p0, Lcom/google/android/gms/location/copresence/b;->c:I

    move v5, v4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/internal/mt;-><init>(ZZIZZ)V

    return-object v0

    :cond_0
    move v1, v4

    goto :goto_0
.end method
