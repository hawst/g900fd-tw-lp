.class public final Lcom/google/android/gms/internal/aW;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/os/Parcelable$Creator;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static a(Lcom/google/android/gms/internal/nq;Landroid/os/Parcel;I)V
    .locals 5

    const/4 v4, 0x0

    invoke-static {p1}, Landroid/support/v4/app/b;->b(Landroid/os/Parcel;)I

    move-result v0

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/android/gms/internal/nq;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {p1, v1, v2, v4}, Landroid/support/v4/app/b;->a(Landroid/os/Parcel;ILjava/lang/String;Z)V

    const/4 v1, 0x2

    invoke-virtual {p0}, Lcom/google/android/gms/internal/nq;->q()Landroid/os/Bundle;

    move-result-object v2

    invoke-static {p1, v1, v2, v4}, Landroid/support/v4/app/b;->a(Landroid/os/Parcel;ILandroid/os/Bundle;Z)V

    const/4 v1, 0x3

    invoke-virtual {p0}, Lcom/google/android/gms/internal/nq;->s()Lcom/google/android/gms/internal/ns;

    move-result-object v2

    invoke-static {p1, v1, v2, p2, v4}, Landroid/support/v4/app/b;->a(Landroid/os/Parcel;ILandroid/os/Parcelable;IZ)V

    const/4 v1, 0x4

    invoke-virtual {p0}, Lcom/google/android/gms/internal/nq;->e()Lcom/google/android/gms/maps/model/LatLng;

    move-result-object v2

    invoke-static {p1, v1, v2, p2, v4}, Landroid/support/v4/app/b;->a(Landroid/os/Parcel;ILandroid/os/Parcelable;IZ)V

    const/4 v1, 0x5

    invoke-virtual {p0}, Lcom/google/android/gms/internal/nq;->f()F

    move-result v2

    invoke-static {p1, v1, v2}, Landroid/support/v4/app/b;->a(Landroid/os/Parcel;IF)V

    const/4 v1, 0x6

    invoke-virtual {p0}, Lcom/google/android/gms/internal/nq;->g()Lcom/google/android/gms/maps/model/LatLngBounds;

    move-result-object v2

    invoke-static {p1, v1, v2, p2, v4}, Landroid/support/v4/app/b;->a(Landroid/os/Parcel;ILandroid/os/Parcelable;IZ)V

    const/4 v1, 0x7

    invoke-virtual {p0}, Lcom/google/android/gms/internal/nq;->r()Ljava/lang/String;

    move-result-object v2

    invoke-static {p1, v1, v2, v4}, Landroid/support/v4/app/b;->a(Landroid/os/Parcel;ILjava/lang/String;Z)V

    const/16 v1, 0x8

    invoke-virtual {p0}, Lcom/google/android/gms/internal/nq;->h()Landroid/net/Uri;

    move-result-object v2

    invoke-static {p1, v1, v2, p2, v4}, Landroid/support/v4/app/b;->a(Landroid/os/Parcel;ILandroid/os/Parcelable;IZ)V

    const/16 v1, 0x9

    invoke-virtual {p0}, Lcom/google/android/gms/internal/nq;->l()Z

    move-result v2

    invoke-static {p1, v1, v2}, Landroid/support/v4/app/b;->a(Landroid/os/Parcel;IZ)V

    const/16 v1, 0xa

    invoke-virtual {p0}, Lcom/google/android/gms/internal/nq;->m()F

    move-result v2

    invoke-static {p1, v1, v2}, Landroid/support/v4/app/b;->a(Landroid/os/Parcel;IF)V

    const/16 v1, 0xb

    invoke-virtual {p0}, Lcom/google/android/gms/internal/nq;->n()I

    move-result v2

    invoke-static {p1, v1, v2}, Landroid/support/v4/app/b;->a(Landroid/os/Parcel;II)V

    const/16 v1, 0xc

    invoke-virtual {p0}, Lcom/google/android/gms/internal/nq;->o()J

    move-result-wide v2

    invoke-static {p1, v1, v2, v3}, Landroid/support/v4/app/b;->a(Landroid/os/Parcel;IJ)V

    const/16 v1, 0xd

    invoke-virtual {p0}, Lcom/google/android/gms/internal/nq;->b()Ljava/util/List;

    move-result-object v2

    invoke-static {p1, v1, v2, v4}, Landroid/support/v4/app/b;->d(Landroid/os/Parcel;ILjava/util/List;Z)V

    const/16 v1, 0xe

    invoke-virtual {p0}, Lcom/google/android/gms/internal/nq;->d()Ljava/lang/String;

    move-result-object v2

    invoke-static {p1, v1, v2, v4}, Landroid/support/v4/app/b;->a(Landroid/os/Parcel;ILjava/lang/String;Z)V

    const/16 v1, 0xf

    invoke-virtual {p0}, Lcom/google/android/gms/internal/nq;->i()Ljava/lang/String;

    move-result-object v2

    invoke-static {p1, v1, v2, v4}, Landroid/support/v4/app/b;->a(Landroid/os/Parcel;ILjava/lang/String;Z)V

    const/16 v1, 0x11

    invoke-virtual {p0}, Lcom/google/android/gms/internal/nq;->k()Ljava/util/List;

    move-result-object v2

    invoke-static {p1, v1, v2, v4}, Landroid/support/v4/app/b;->c(Landroid/os/Parcel;ILjava/util/List;Z)V

    const/16 v1, 0x10

    invoke-virtual {p0}, Lcom/google/android/gms/internal/nq;->j()Ljava/lang/String;

    move-result-object v2

    invoke-static {p1, v1, v2, v4}, Landroid/support/v4/app/b;->a(Landroid/os/Parcel;ILjava/lang/String;Z)V

    const/16 v1, 0x3e8

    iget v2, p0, Lcom/google/android/gms/internal/nq;->a:I

    invoke-static {p1, v1, v2}, Landroid/support/v4/app/b;->a(Landroid/os/Parcel;II)V

    const/16 v1, 0x13

    invoke-virtual {p0}, Lcom/google/android/gms/internal/nq;->c()Ljava/lang/String;

    move-result-object v2

    invoke-static {p1, v1, v2, v4}, Landroid/support/v4/app/b;->a(Landroid/os/Parcel;ILjava/lang/String;Z)V

    const/16 v1, 0x12

    invoke-virtual {p0}, Lcom/google/android/gms/internal/nq;->p()Z

    move-result v2

    invoke-static {p1, v1, v2}, Landroid/support/v4/app/b;->a(Landroid/os/Parcel;IZ)V

    invoke-static {p1, v0}, Landroid/support/v4/app/b;->B(Landroid/os/Parcel;I)V

    return-void
.end method


# virtual methods
.method public final synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 26

    invoke-static/range {p1 .. p1}, Landroid/support/v4/app/b;->a(Landroid/os/Parcel;)I

    move-result v24

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v18, 0x0

    const/16 v19, 0x0

    const-wide/16 v20, 0x0

    const/16 v22, 0x0

    const/16 v23, 0x0

    :goto_0
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->dataPosition()I

    move-result v2

    move/from16 v0, v24

    if-ge v2, v0, :cond_0

    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    const v25, 0xffff

    and-int v25, v25, v2

    sparse-switch v25, :sswitch_data_0

    move-object/from16 v0, p1

    invoke-static {v0, v2}, Landroid/support/v4/app/b;->b(Landroid/os/Parcel;I)V

    goto :goto_0

    :sswitch_0
    move-object/from16 v0, p1

    invoke-static {v0, v2}, Landroid/support/v4/app/b;->p(Landroid/os/Parcel;I)Ljava/lang/String;

    move-result-object v4

    goto :goto_0

    :sswitch_1
    move-object/from16 v0, p1

    invoke-static {v0, v2}, Landroid/support/v4/app/b;->r(Landroid/os/Parcel;I)Landroid/os/Bundle;

    move-result-object v6

    goto :goto_0

    :sswitch_2
    sget-object v23, Lcom/google/android/gms/internal/ns;->CREATOR:Lcom/google/android/gms/internal/E;

    move-object/from16 v0, p1

    move-object/from16 v1, v23

    invoke-static {v0, v2, v1}, Landroid/support/v4/app/b;->a(Landroid/os/Parcel;ILandroid/os/Parcelable$Creator;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/internal/ns;

    move-object/from16 v23, v2

    goto :goto_0

    :sswitch_3
    sget-object v12, Lcom/google/android/gms/maps/model/LatLng;->CREATOR:Lcom/google/android/gms/maps/model/h;

    move-object/from16 v0, p1

    invoke-static {v0, v2, v12}, Landroid/support/v4/app/b;->a(Landroid/os/Parcel;ILandroid/os/Parcelable$Creator;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/maps/model/LatLng;

    move-object v12, v2

    goto :goto_0

    :sswitch_4
    move-object/from16 v0, p1

    invoke-static {v0, v2}, Landroid/support/v4/app/b;->l(Landroid/os/Parcel;I)F

    move-result v13

    goto :goto_0

    :sswitch_5
    sget-object v14, Lcom/google/android/gms/maps/model/LatLngBounds;->CREATOR:Lcom/google/android/gms/maps/model/g;

    move-object/from16 v0, p1

    invoke-static {v0, v2, v14}, Landroid/support/v4/app/b;->a(Landroid/os/Parcel;ILandroid/os/Parcelable$Creator;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/maps/model/LatLngBounds;

    move-object v14, v2

    goto :goto_0

    :sswitch_6
    move-object/from16 v0, p1

    invoke-static {v0, v2}, Landroid/support/v4/app/b;->p(Landroid/os/Parcel;I)Ljava/lang/String;

    move-result-object v15

    goto :goto_0

    :sswitch_7
    sget-object v16, Landroid/net/Uri;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-static {v0, v2, v1}, Landroid/support/v4/app/b;->a(Landroid/os/Parcel;ILandroid/os/Parcelable$Creator;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Landroid/net/Uri;

    move-object/from16 v16, v2

    goto :goto_0

    :sswitch_8
    move-object/from16 v0, p1

    invoke-static {v0, v2}, Landroid/support/v4/app/b;->c(Landroid/os/Parcel;I)Z

    move-result v17

    goto :goto_0

    :sswitch_9
    move-object/from16 v0, p1

    invoke-static {v0, v2}, Landroid/support/v4/app/b;->l(Landroid/os/Parcel;I)F

    move-result v18

    goto :goto_0

    :sswitch_a
    move-object/from16 v0, p1

    invoke-static {v0, v2}, Landroid/support/v4/app/b;->g(Landroid/os/Parcel;I)I

    move-result v19

    goto :goto_0

    :sswitch_b
    move-object/from16 v0, p1

    invoke-static {v0, v2}, Landroid/support/v4/app/b;->i(Landroid/os/Parcel;I)J

    move-result-wide v20

    goto/16 :goto_0

    :sswitch_c
    sget-object v5, Lcom/google/android/gms/location/places/PlaceType;->CREATOR:Lcom/google/android/gms/location/places/i;

    move-object/from16 v0, p1

    invoke-static {v0, v2, v5}, Landroid/support/v4/app/b;->c(Landroid/os/Parcel;ILandroid/os/Parcelable$Creator;)Ljava/util/ArrayList;

    move-result-object v5

    goto/16 :goto_0

    :sswitch_d
    move-object/from16 v0, p1

    invoke-static {v0, v2}, Landroid/support/v4/app/b;->p(Landroid/os/Parcel;I)Ljava/lang/String;

    move-result-object v8

    goto/16 :goto_0

    :sswitch_e
    move-object/from16 v0, p1

    invoke-static {v0, v2}, Landroid/support/v4/app/b;->p(Landroid/os/Parcel;I)Ljava/lang/String;

    move-result-object v9

    goto/16 :goto_0

    :sswitch_f
    move-object/from16 v0, p1

    invoke-static {v0, v2}, Landroid/support/v4/app/b;->y(Landroid/os/Parcel;I)Ljava/util/ArrayList;

    move-result-object v11

    goto/16 :goto_0

    :sswitch_10
    move-object/from16 v0, p1

    invoke-static {v0, v2}, Landroid/support/v4/app/b;->p(Landroid/os/Parcel;I)Ljava/lang/String;

    move-result-object v10

    goto/16 :goto_0

    :sswitch_11
    move-object/from16 v0, p1

    invoke-static {v0, v2}, Landroid/support/v4/app/b;->g(Landroid/os/Parcel;I)I

    move-result v3

    goto/16 :goto_0

    :sswitch_12
    move-object/from16 v0, p1

    invoke-static {v0, v2}, Landroid/support/v4/app/b;->p(Landroid/os/Parcel;I)Ljava/lang/String;

    move-result-object v7

    goto/16 :goto_0

    :sswitch_13
    move-object/from16 v0, p1

    invoke-static {v0, v2}, Landroid/support/v4/app/b;->c(Landroid/os/Parcel;I)Z

    move-result v22

    goto/16 :goto_0

    :cond_0
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->dataPosition()I

    move-result v2

    move/from16 v0, v24

    if-eq v2, v0, :cond_1

    new-instance v2, Landroid/support/v4/app/i;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "Overread allowed size end="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move/from16 v0, v24

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-direct {v2, v3, v0}, Landroid/support/v4/app/i;-><init>(Ljava/lang/String;Landroid/os/Parcel;)V

    throw v2

    :cond_1
    new-instance v2, Lcom/google/android/gms/internal/nq;

    invoke-direct/range {v2 .. v23}, Lcom/google/android/gms/internal/nq;-><init>(ILjava/lang/String;Ljava/util/List;Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Lcom/google/android/gms/maps/model/LatLng;FLcom/google/android/gms/maps/model/LatLngBounds;Ljava/lang/String;Landroid/net/Uri;ZFIJZLcom/google/android/gms/internal/ns;)V

    return-object v2

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_1
        0x3 -> :sswitch_2
        0x4 -> :sswitch_3
        0x5 -> :sswitch_4
        0x6 -> :sswitch_5
        0x7 -> :sswitch_6
        0x8 -> :sswitch_7
        0x9 -> :sswitch_8
        0xa -> :sswitch_9
        0xb -> :sswitch_a
        0xc -> :sswitch_b
        0xd -> :sswitch_c
        0xe -> :sswitch_d
        0xf -> :sswitch_e
        0x10 -> :sswitch_10
        0x11 -> :sswitch_f
        0x12 -> :sswitch_13
        0x13 -> :sswitch_12
        0x3e8 -> :sswitch_11
    .end sparse-switch
.end method

.method public final synthetic newArray(I)[Ljava/lang/Object;
    .locals 1

    new-array v0, p1, [Lcom/google/android/gms/internal/nq;

    return-object v0
.end method
