.class public final Lcom/google/android/gms/feedback/e;
.super Ljava/lang/Object;


# instance fields
.field private a:Landroid/graphics/Bitmap;

.field private b:Ljava/lang/String;

.field private c:Landroid/os/Bundle;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/String;


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(B)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/gms/feedback/e;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/feedback/e;Landroid/graphics/Bitmap;)Lcom/google/android/gms/feedback/e;
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/feedback/e;->a:Landroid/graphics/Bitmap;

    return-object p0
.end method

.method static synthetic a(Lcom/google/android/gms/feedback/e;Landroid/os/Bundle;)Lcom/google/android/gms/feedback/e;
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/feedback/e;->c:Landroid/os/Bundle;

    return-object p0
.end method

.method static synthetic a(Lcom/google/android/gms/feedback/e;Ljava/lang/String;)Lcom/google/android/gms/feedback/e;
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/feedback/e;->b:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic b(Lcom/google/android/gms/feedback/e;Ljava/lang/String;)Lcom/google/android/gms/feedback/e;
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/feedback/e;->e:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic c(Lcom/google/android/gms/feedback/e;Ljava/lang/String;)Lcom/google/android/gms/feedback/e;
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/feedback/e;->d:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic d(Lcom/google/android/gms/feedback/e;Ljava/lang/String;)Lcom/google/android/gms/feedback/e;
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/feedback/e;->f:Ljava/lang/String;

    return-object p0
.end method


# virtual methods
.method public final a()Landroid/graphics/Bitmap;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/feedback/e;->a:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/feedback/e;->b:Ljava/lang/String;

    return-object v0
.end method

.method public final c()Landroid/os/Bundle;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/feedback/e;->c:Landroid/os/Bundle;

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/feedback/e;->d:Ljava/lang/String;

    return-object v0
.end method

.method public final e()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/feedback/e;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final f()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/feedback/e;->f:Ljava/lang/String;

    return-object v0
.end method
