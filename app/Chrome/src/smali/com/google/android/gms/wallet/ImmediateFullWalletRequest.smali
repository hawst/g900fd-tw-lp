.class public final Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field a:I

.field b:Landroid/accounts/Account;

.field c:Ljava/lang/String;

.field d:I

.field e:Z

.field f:Z

.field g:Z

.field h:Ljava/lang/String;

.field i:Z

.field j:[Lcom/google/android/gms/wallet/CountrySpecification;

.field k:Ljava/util/ArrayList;

.field private final l:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/gms/wallet/i;

    invoke-direct {v0}, Lcom/google/android/gms/wallet/i;-><init>()V

    sput-object v0, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x2

    iput v0, p0, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->l:I

    return-void
.end method

.method constructor <init>(IILandroid/accounts/Account;Ljava/lang/String;IZZZLjava/lang/String;Z[Lcom/google/android/gms/wallet/CountrySpecification;Ljava/util/ArrayList;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->l:I

    iput p2, p0, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->a:I

    iput-object p3, p0, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->b:Landroid/accounts/Account;

    iput-object p4, p0, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->c:Ljava/lang/String;

    iput p5, p0, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->d:I

    iput-boolean p6, p0, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->e:Z

    iput-boolean p7, p0, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->f:Z

    iput-boolean p8, p0, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->g:Z

    iput-object p9, p0, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->h:Ljava/lang/String;

    iput-boolean p10, p0, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->i:Z

    iput-object p11, p0, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->j:[Lcom/google/android/gms/wallet/CountrySpecification;

    iput-object p12, p0, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->k:Ljava/util/ArrayList;

    return-void
.end method

.method public static a()Lcom/google/android/gms/wallet/a;
    .locals 3

    new-instance v0, Lcom/google/android/gms/wallet/a;

    new-instance v1, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;

    invoke-direct {v1}, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;-><init>()V

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/wallet/a;-><init>(Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;B)V

    return-object v0
.end method


# virtual methods
.method public final b()I
    .locals 1

    iget v0, p0, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->l:I

    return v0
.end method

.method public final describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    invoke-static {p0, p1, p2}, Lcom/google/android/gms/wallet/i;->a(Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;Landroid/os/Parcel;I)V

    return-void
.end method
