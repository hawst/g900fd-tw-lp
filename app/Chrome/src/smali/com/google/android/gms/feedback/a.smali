.class public final Lcom/google/android/gms/feedback/a;
.super Ljava/lang/Object;


# static fields
.field public static final a:Lcom/google/android/gms/common/api/a;

.field private static final b:Lcom/google/android/gms/common/api/d;

.field private static final c:Lcom/google/android/gms/common/api/c;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    new-instance v0, Lcom/google/android/gms/common/api/d;

    invoke-direct {v0}, Lcom/google/android/gms/common/api/d;-><init>()V

    sput-object v0, Lcom/google/android/gms/feedback/a;->b:Lcom/google/android/gms/common/api/d;

    new-instance v0, Lcom/google/android/gms/feedback/b;

    invoke-direct {v0}, Lcom/google/android/gms/feedback/b;-><init>()V

    sput-object v0, Lcom/google/android/gms/feedback/a;->c:Lcom/google/android/gms/common/api/c;

    new-instance v0, Lcom/google/android/gms/common/api/a;

    sget-object v1, Lcom/google/android/gms/feedback/a;->c:Lcom/google/android/gms/common/api/c;

    sget-object v2, Lcom/google/android/gms/feedback/a;->b:Lcom/google/android/gms/common/api/d;

    const/4 v3, 0x0

    new-array v3, v3, [Landroid/support/v4/media/session/MediaSessionCompat;

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/gms/common/api/a;-><init>(Lcom/google/android/gms/common/api/c;Lcom/google/android/gms/common/api/d;[Landroid/support/v4/media/session/MediaSessionCompat;)V

    sput-object v0, Lcom/google/android/gms/feedback/a;->a:Lcom/google/android/gms/common/api/a;

    return-void
.end method

.method static synthetic a()Lcom/google/android/gms/common/api/d;
    .locals 1

    sget-object v0, Lcom/google/android/gms/feedback/a;->b:Lcom/google/android/gms/common/api/d;

    return-object v0
.end method

.method public static a(Lcom/google/android/gms/common/api/i;Landroid/graphics/Bitmap;Landroid/os/Bundle;)Lcom/google/android/gms/common/api/l;
    .locals 1

    new-instance v0, Lcom/google/android/gms/feedback/c;

    invoke-direct {v0, p1, p2}, Lcom/google/android/gms/feedback/c;-><init>(Landroid/graphics/Bitmap;Landroid/os/Bundle;)V

    invoke-interface {p0, v0}, Lcom/google/android/gms/common/api/i;->a(Lcom/google/android/gms/common/api/g;)Lcom/google/android/gms/common/api/g;

    move-result-object v0

    return-object v0
.end method
