.class public final Lcom/google/android/gms/internal/ac;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Lcom/google/android/gms/internal/D;


# instance fields
.field public final a:I

.field public final b:Z

.field public final c:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/gms/internal/D;

    invoke-direct {v0}, Lcom/google/android/gms/internal/D;-><init>()V

    sput-object v0, Lcom/google/android/gms/internal/ac;->CREATOR:Lcom/google/android/gms/internal/D;

    return-void
.end method

.method constructor <init>(IZZ)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/gms/internal/ac;->a:I

    iput-boolean p2, p0, Lcom/google/android/gms/internal/ac;->b:Z

    iput-boolean p3, p0, Lcom/google/android/gms/internal/ac;->c:Z

    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    invoke-static {p0, p1}, Lcom/google/android/gms/internal/D;->a(Lcom/google/android/gms/internal/ac;Landroid/os/Parcel;)V

    return-void
.end method
