.class public final Lcom/google/android/gms/search/corpora/a;
.super Lcom/google/android/gms/internal/aZ;


# instance fields
.field private final a:Lcom/google/android/gms/search/corpora/ClearCorpusCall$b;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/search/corpora/ClearCorpusCall$b;)V
    .locals 1

    sget-object v0, Lcom/google/android/gms/search/a;->a:Lcom/google/android/gms/common/api/d;

    invoke-direct {p0, v0}, Lcom/google/android/gms/internal/aZ;-><init>(Lcom/google/android/gms/common/api/d;)V

    iput-object p1, p0, Lcom/google/android/gms/search/corpora/a;->a:Lcom/google/android/gms/search/corpora/ClearCorpusCall$b;

    return-void
.end method

.method private a(Lcom/google/android/gms/internal/bi;)V
    .locals 4

    invoke-virtual {p1}, Lcom/google/android/gms/internal/bi;->j()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/internal/bf;

    iget-object v1, p0, Lcom/google/android/gms/search/corpora/a;->a:Lcom/google/android/gms/search/corpora/ClearCorpusCall$b;

    new-instance v2, Lcom/google/android/gms/internal/bj;

    const-class v3, Lcom/google/android/gms/search/corpora/ClearCorpusCall$Response;

    invoke-direct {v2, p0, v3}, Lcom/google/android/gms/internal/bj;-><init>(Lcom/google/android/gms/common/api/h;Ljava/lang/Class;)V

    invoke-interface {v0, v1, v2}, Lcom/google/android/gms/internal/bf;->a(Lcom/google/android/gms/search/corpora/ClearCorpusCall$b;Lcom/google/android/gms/internal/bb;)V

    return-void
.end method


# virtual methods
.method protected final synthetic a(Lcom/google/android/gms/common/api/Status;)Lcom/google/android/gms/common/api/n;
    .locals 1

    new-instance v0, Lcom/google/android/gms/search/corpora/ClearCorpusCall$Response;

    invoke-direct {v0}, Lcom/google/android/gms/search/corpora/ClearCorpusCall$Response;-><init>()V

    iput-object p1, v0, Lcom/google/android/gms/search/corpora/ClearCorpusCall$Response;->a:Lcom/google/android/gms/common/api/Status;

    return-object v0
.end method

.method protected final bridge synthetic a(Lcom/google/android/gms/common/api/b;)V
    .locals 0

    check-cast p1, Lcom/google/android/gms/internal/bi;

    invoke-direct {p0, p1}, Lcom/google/android/gms/search/corpora/a;->a(Lcom/google/android/gms/internal/bi;)V

    return-void
.end method

.method protected final bridge synthetic a(Lcom/google/android/gms/internal/aY;)V
    .locals 0

    check-cast p1, Lcom/google/android/gms/internal/bi;

    invoke-direct {p0, p1}, Lcom/google/android/gms/search/corpora/a;->a(Lcom/google/android/gms/internal/bi;)V

    return-void
.end method
