.class public final Lcom/google/android/gms/internal/L;
.super Ljava/lang/Object;


# static fields
.field public static final a:Ljava/util/Map;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/google/android/gms/internal/M;

    invoke-direct {v0}, Lcom/google/android/gms/internal/M;-><init>()V

    sput-object v0, Lcom/google/android/gms/internal/L;->a:Ljava/util/Map;

    new-instance v0, Lcom/google/android/gms/internal/J;

    invoke-direct {v0}, Lcom/google/android/gms/internal/J;-><init>()V

    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    const-string/jumbo v0, "\\,"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    const-string/jumbo v0, "[\u2028\u2029 \u00a0\u1680\u180e\u2000\u2001\u2002\u2003\u2004\u2005\u2006\u2007\u2008\u2009\u200a\u202f\u205f\u3000\t\u000b\u000c\u001c\u001d\u001e\u001f\n\r]+"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    new-instance v0, Ljava/security/SecureRandom;

    invoke-direct {v0}, Ljava/security/SecureRandom;-><init>()V

    new-instance v0, Lcom/google/android/gms/internal/N;

    invoke-direct {v0}, Lcom/google/android/gms/internal/N;-><init>()V

    new-instance v0, Lcom/google/android/gms/internal/O;

    invoke-direct {v0}, Lcom/google/android/gms/internal/O;-><init>()V

    new-instance v0, Lcom/google/android/gms/internal/P;

    invoke-direct {v0}, Lcom/google/android/gms/internal/P;-><init>()V

    new-instance v0, Lcom/google/android/gms/internal/Q;

    invoke-direct {v0}, Lcom/google/android/gms/internal/Q;-><init>()V

    new-instance v0, Lcom/google/android/gms/internal/R;

    invoke-direct {v0}, Lcom/google/android/gms/internal/R;-><init>()V

    new-instance v0, Lcom/google/android/gms/internal/S;

    invoke-direct {v0}, Lcom/google/android/gms/internal/S;-><init>()V

    return-void
.end method
