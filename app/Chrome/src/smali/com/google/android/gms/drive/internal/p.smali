.class public final Lcom/google/android/gms/drive/internal/p;
.super Lcom/google/android/gms/internal/cc;


# instance fields
.field public a:I

.field public b:Ljava/lang/String;

.field public c:J

.field public d:J


# direct methods
.method public constructor <init>()V
    .locals 4

    const-wide/16 v2, -0x1

    invoke-direct {p0}, Lcom/google/android/gms/internal/cc;-><init>()V

    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/drive/internal/p;->a:I

    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/google/android/gms/drive/internal/p;->b:Ljava/lang/String;

    iput-wide v2, p0, Lcom/google/android/gms/drive/internal/p;->c:J

    iput-wide v2, p0, Lcom/google/android/gms/drive/internal/p;->d:J

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/drive/internal/p;->e:I

    return-void
.end method


# virtual methods
.method protected final a()I
    .locals 4

    invoke-super {p0}, Lcom/google/android/gms/internal/cc;->a()I

    move-result v0

    iget v1, p0, Lcom/google/android/gms/drive/internal/p;->a:I

    const/4 v2, 0x1

    invoke-static {v2}, Lcom/google/android/gms/internal/ca;->b(I)I

    move-result v2

    invoke-static {v1}, Lcom/google/android/gms/internal/ca;->a(I)I

    move-result v1

    add-int/2addr v1, v2

    add-int/2addr v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/gms/drive/internal/p;->b:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/android/gms/internal/ca;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    const/4 v1, 0x3

    iget-wide v2, p0, Lcom/google/android/gms/drive/internal/p;->c:J

    invoke-static {v1, v2, v3}, Lcom/google/android/gms/internal/ca;->b(IJ)I

    move-result v1

    add-int/2addr v0, v1

    const/4 v1, 0x4

    iget-wide v2, p0, Lcom/google/android/gms/drive/internal/p;->d:J

    invoke-static {v1, v2, v3}, Lcom/google/android/gms/internal/ca;->b(IJ)I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public final a(Lcom/google/android/gms/internal/ca;)V
    .locals 4

    const/4 v0, 0x1

    iget v1, p0, Lcom/google/android/gms/drive/internal/p;->a:I

    invoke-virtual {p1, v0, v1}, Lcom/google/android/gms/internal/ca;->a(II)V

    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/gms/drive/internal/p;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/android/gms/internal/ca;->a(ILjava/lang/String;)V

    const/4 v0, 0x3

    iget-wide v2, p0, Lcom/google/android/gms/drive/internal/p;->c:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/android/gms/internal/ca;->a(IJ)V

    const/4 v0, 0x4

    iget-wide v2, p0, Lcom/google/android/gms/drive/internal/p;->d:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/android/gms/internal/ca;->a(IJ)V

    invoke-super {p0, p1}, Lcom/google/android/gms/internal/cc;->a(Lcom/google/android/gms/internal/ca;)V

    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p1, p0, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v2, p1, Lcom/google/android/gms/drive/internal/p;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    check-cast p1, Lcom/google/android/gms/drive/internal/p;

    iget v2, p0, Lcom/google/android/gms/drive/internal/p;->a:I

    iget v3, p1, Lcom/google/android/gms/drive/internal/p;->a:I

    if-eq v2, v3, :cond_3

    move v0, v1

    goto :goto_0

    :cond_3
    iget-object v2, p0, Lcom/google/android/gms/drive/internal/p;->b:Ljava/lang/String;

    if-nez v2, :cond_4

    iget-object v2, p1, Lcom/google/android/gms/drive/internal/p;->b:Ljava/lang/String;

    if-eqz v2, :cond_5

    move v0, v1

    goto :goto_0

    :cond_4
    iget-object v2, p0, Lcom/google/android/gms/drive/internal/p;->b:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/gms/drive/internal/p;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    move v0, v1

    goto :goto_0

    :cond_5
    iget-wide v2, p0, Lcom/google/android/gms/drive/internal/p;->c:J

    iget-wide v4, p1, Lcom/google/android/gms/drive/internal/p;->c:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_6

    move v0, v1

    goto :goto_0

    :cond_6
    iget-wide v2, p0, Lcom/google/android/gms/drive/internal/p;->d:J

    iget-wide v4, p1, Lcom/google/android/gms/drive/internal/p;->d:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 7

    const/16 v6, 0x20

    iget v0, p0, Lcom/google/android/gms/drive/internal/p;->a:I

    add-int/lit16 v0, v0, 0x20f

    mul-int/lit8 v1, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/gms/drive/internal/p;->b:Ljava/lang/String;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lcom/google/android/gms/drive/internal/p;->c:J

    iget-wide v4, p0, Lcom/google/android/gms/drive/internal/p;->c:J

    ushr-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v1, v2

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lcom/google/android/gms/drive/internal/p;->d:J

    iget-wide v4, p0, Lcom/google/android/gms/drive/internal/p;->d:J

    ushr-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v1, v2

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/drive/internal/p;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0
.end method
