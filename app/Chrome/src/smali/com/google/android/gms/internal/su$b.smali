.class public Lcom/google/android/gms/internal/su$b;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Lcom/google/android/gms/internal/bP;


# instance fields
.field final a:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/gms/internal/bP;

    invoke-direct {v0}, Lcom/google/android/gms/internal/bP;-><init>()V

    sput-object v0, Lcom/google/android/gms/internal/su$b;->CREATOR:Lcom/google/android/gms/internal/bP;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/internal/su$b;->a:I

    return-void
.end method

.method constructor <init>(I)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/gms/internal/su$b;->a:I

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    sget-object v0, Lcom/google/android/gms/internal/su$b;->CREATOR:Lcom/google/android/gms/internal/bP;

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    sget-object v0, Lcom/google/android/gms/internal/su$b;->CREATOR:Lcom/google/android/gms/internal/bP;

    invoke-static {p0, p1}, Lcom/google/android/gms/internal/bP;->a(Lcom/google/android/gms/internal/su$b;Landroid/os/Parcel;)V

    return-void
.end method
