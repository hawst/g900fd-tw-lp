.class public final Lcom/google/android/gms/internal/cr;
.super Lcom/google/android/gms/common/internal/d;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/common/b;Lcom/google/android/gms/common/c;)V
    .locals 1

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/String;

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/google/android/gms/common/internal/d;-><init>(Landroid/content/Context;Lcom/google/android/gms/common/b;Lcom/google/android/gms/common/c;[Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method protected final synthetic a(Landroid/os/IBinder;)Landroid/os/IInterface;
    .locals 1

    invoke-static {p1}, Lcom/google/android/gms/internal/ct;->a(Landroid/os/IBinder;)Lcom/google/android/gms/internal/cs;

    move-result-object v0

    return-object v0
.end method

.method protected final a(Lcom/google/android/gms/common/internal/A;Lcom/google/android/gms/common/internal/h;)V
    .locals 2

    const v0, 0x5d2f78

    invoke-virtual {p0}, Lcom/google/android/gms/internal/cr;->h()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, p2, v0, v1}, Lcom/google/android/gms/common/internal/A;->a(Lcom/google/android/gms/common/internal/x;ILjava/lang/String;)V

    return-void
.end method

.method protected final e()Ljava/lang/String;
    .locals 1

    const-string/jumbo v0, "com.google.android.gms.icing.INDEX_SERVICE"

    return-object v0
.end method

.method protected final f()Ljava/lang/String;
    .locals 1

    const-string/jumbo v0, "com.google.android.gms.appdatasearch.internal.IAppDataSearch"

    return-object v0
.end method

.method public final j()Lcom/google/android/gms/internal/cs;
    .locals 2

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/internal/cr;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/internal/cs;
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception v0

    new-instance v1, Landroid/os/RemoteException;

    invoke-direct {v1}, Landroid/os/RemoteException;-><init>()V

    invoke-virtual {v1, v0}, Landroid/os/RemoteException;->initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    throw v1
.end method
