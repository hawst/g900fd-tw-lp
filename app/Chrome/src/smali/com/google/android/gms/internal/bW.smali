.class public abstract Lcom/google/android/gms/internal/bW;
.super Landroid/os/Binder;

# interfaces
.implements Lcom/google/android/gms/internal/bV;


# direct methods
.method public static a(Landroid/os/IBinder;)Lcom/google/android/gms/internal/bV;
    .locals 2

    if-nez p0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    const-string/jumbo v0, "com.google.android.gms.search.administration.internal.ISearchAdministrationService"

    invoke-interface {p0, v0}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v0

    if-eqz v0, :cond_1

    instance-of v1, v0, Lcom/google/android/gms/internal/bV;

    if-eqz v1, :cond_1

    check-cast v0, Lcom/google/android/gms/internal/bV;

    goto :goto_0

    :cond_1
    new-instance v0, Lcom/google/android/gms/internal/bX;

    invoke-direct {v0, p0}, Lcom/google/android/gms/internal/bX;-><init>(Landroid/os/IBinder;)V

    goto :goto_0
.end method


# virtual methods
.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .locals 5

    const/4 v2, 0x0

    const/4 v3, 0x1

    sparse-switch p1, :sswitch_data_0

    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    move-result v0

    :goto_0
    return v0

    :sswitch_0
    const-string/jumbo v0, "com.google.android.gms.search.administration.internal.ISearchAdministrationService"

    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    move v0, v3

    goto :goto_0

    :sswitch_1
    const-string/jumbo v0, "com.google.android.gms.search.administration.internal.ISearchAdministrationService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/gms/internal/su$b;->CREATOR:Lcom/google/android/gms/internal/bP;

    invoke-static {p2}, Lcom/google/android/gms/internal/bP;->a(Landroid/os/Parcel;)Lcom/google/android/gms/internal/su$b;

    move-result-object v0

    move-object v1, v0

    :goto_1
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v4

    if-nez v4, :cond_1

    :goto_2
    invoke-virtual {p0, v1, v2}, Lcom/google/android/gms/internal/bW;->a(Lcom/google/android/gms/internal/su$b;Lcom/google/android/gms/internal/bS;)V

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    move v0, v3

    goto :goto_0

    :cond_0
    move-object v1, v2

    goto :goto_1

    :cond_1
    const-string/jumbo v0, "com.google.android.gms.search.administration.internal.ISearchAdministrationCallbacks"

    invoke-interface {v4, v0}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v0

    if-eqz v0, :cond_2

    instance-of v2, v0, Lcom/google/android/gms/internal/bS;

    if-eqz v2, :cond_2

    check-cast v0, Lcom/google/android/gms/internal/bS;

    move-object v2, v0

    goto :goto_2

    :cond_2
    new-instance v2, Lcom/google/android/gms/internal/bU;

    invoke-direct {v2, v4}, Lcom/google/android/gms/internal/bU;-><init>(Landroid/os/IBinder;)V

    goto :goto_2

    :sswitch_data_0
    .sparse-switch
        0x2 -> :sswitch_1
        0x5f4e5446 -> :sswitch_0
    .end sparse-switch
.end method
