.class public final Lcom/google/android/gms/internal/fn;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Lcom/google/android/gms/internal/cl;


# instance fields
.field public final a:I

.field public final b:Landroid/os/Bundle;

.field public final c:Lcom/google/android/gms/internal/ba;

.field public final d:Lcom/google/android/gms/internal/bd;

.field public final e:Ljava/lang/String;

.field public final f:Landroid/content/pm/ApplicationInfo;

.field public final g:Landroid/content/pm/PackageInfo;

.field public final h:Ljava/lang/String;

.field public final i:Ljava/lang/String;

.field public final j:Ljava/lang/String;

.field public final k:Lcom/google/android/gms/internal/gz;

.field public final l:Landroid/os/Bundle;

.field public final m:I

.field public final n:Ljava/util/List;

.field public final o:Landroid/os/Bundle;

.field public final p:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/gms/internal/cl;

    invoke-direct {v0}, Lcom/google/android/gms/internal/cl;-><init>()V

    sput-object v0, Lcom/google/android/gms/internal/fn;->CREATOR:Lcom/google/android/gms/internal/cl;

    return-void
.end method

.method constructor <init>(ILandroid/os/Bundle;Lcom/google/android/gms/internal/ba;Lcom/google/android/gms/internal/bd;Ljava/lang/String;Landroid/content/pm/ApplicationInfo;Landroid/content/pm/PackageInfo;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/internal/gz;Landroid/os/Bundle;ILjava/util/List;Landroid/os/Bundle;Z)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/gms/internal/fn;->a:I

    iput-object p2, p0, Lcom/google/android/gms/internal/fn;->b:Landroid/os/Bundle;

    iput-object p3, p0, Lcom/google/android/gms/internal/fn;->c:Lcom/google/android/gms/internal/ba;

    iput-object p4, p0, Lcom/google/android/gms/internal/fn;->d:Lcom/google/android/gms/internal/bd;

    iput-object p5, p0, Lcom/google/android/gms/internal/fn;->e:Ljava/lang/String;

    iput-object p6, p0, Lcom/google/android/gms/internal/fn;->f:Landroid/content/pm/ApplicationInfo;

    iput-object p7, p0, Lcom/google/android/gms/internal/fn;->g:Landroid/content/pm/PackageInfo;

    iput-object p8, p0, Lcom/google/android/gms/internal/fn;->h:Ljava/lang/String;

    iput-object p9, p0, Lcom/google/android/gms/internal/fn;->i:Ljava/lang/String;

    iput-object p10, p0, Lcom/google/android/gms/internal/fn;->j:Ljava/lang/String;

    iput-object p11, p0, Lcom/google/android/gms/internal/fn;->k:Lcom/google/android/gms/internal/gz;

    iput-object p12, p0, Lcom/google/android/gms/internal/fn;->l:Landroid/os/Bundle;

    iput p13, p0, Lcom/google/android/gms/internal/fn;->m:I

    iput-object p14, p0, Lcom/google/android/gms/internal/fn;->n:Ljava/util/List;

    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/google/android/gms/internal/fn;->o:Landroid/os/Bundle;

    move/from16 v0, p16

    iput-boolean v0, p0, Lcom/google/android/gms/internal/fn;->p:Z

    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    invoke-static {p0, p1, p2}, Lcom/google/android/gms/internal/cl;->a(Lcom/google/android/gms/internal/fn;Landroid/os/Parcel;I)V

    return-void
.end method
