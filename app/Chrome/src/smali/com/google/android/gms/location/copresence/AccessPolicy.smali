.class public Lcom/google/android/gms/location/copresence/AccessPolicy;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private final a:I

.field private final b:I

.field private final c:Ljava/lang/String;

.field private final d:J

.field private final e:Lcom/google/android/gms/location/copresence/AccessLock;

.field private final f:Lcom/google/android/gms/common/people/data/Audience;

.field private final g:I

.field private final h:I

.field private final i:Lcom/google/android/gms/location/copresence/AclResourceId;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    new-instance v0, Lcom/google/android/gms/location/copresence/e;

    invoke-direct {v0}, Lcom/google/android/gms/location/copresence/e;-><init>()V

    sput-object v0, Lcom/google/android/gms/location/copresence/AccessPolicy;->CREATOR:Landroid/os/Parcelable$Creator;

    new-instance v0, Lcom/google/android/gms/common/people/data/a;

    invoke-direct {v0}, Lcom/google/android/gms/common/people/data/a;-><init>()V

    const-string/jumbo v1, "public"

    const-string/jumbo v2, "Public"

    invoke-static {v1, v2}, Lcom/google/android/gms/common/people/data/AudienceMember;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/people/data/AudienceMember;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Collections;->singleton(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/people/data/a;->a(Ljava/util/Collection;)Lcom/google/android/gms/common/people/data/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/people/data/a;->a()Lcom/google/android/gms/common/people/data/Audience;

    return-void
.end method

.method constructor <init>(IILjava/lang/String;JLcom/google/android/gms/location/copresence/AccessLock;Lcom/google/android/gms/common/people/data/Audience;IILcom/google/android/gms/location/copresence/AclResourceId;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/gms/location/copresence/AccessPolicy;->a:I

    iput p2, p0, Lcom/google/android/gms/location/copresence/AccessPolicy;->b:I

    iput-object p3, p0, Lcom/google/android/gms/location/copresence/AccessPolicy;->c:Ljava/lang/String;

    iput-wide p4, p0, Lcom/google/android/gms/location/copresence/AccessPolicy;->d:J

    iput-object p6, p0, Lcom/google/android/gms/location/copresence/AccessPolicy;->e:Lcom/google/android/gms/location/copresence/AccessLock;

    iput-object p7, p0, Lcom/google/android/gms/location/copresence/AccessPolicy;->f:Lcom/google/android/gms/common/people/data/Audience;

    iput p8, p0, Lcom/google/android/gms/location/copresence/AccessPolicy;->g:I

    iput p9, p0, Lcom/google/android/gms/location/copresence/AccessPolicy;->h:I

    iput-object p10, p0, Lcom/google/android/gms/location/copresence/AccessPolicy;->i:Lcom/google/android/gms/location/copresence/AclResourceId;

    return-void
.end method


# virtual methods
.method final a()I
    .locals 1

    iget v0, p0, Lcom/google/android/gms/location/copresence/AccessPolicy;->a:I

    return v0
.end method

.method final b()I
    .locals 1

    iget v0, p0, Lcom/google/android/gms/location/copresence/AccessPolicy;->b:I

    return v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/location/copresence/AccessPolicy;->c:Ljava/lang/String;

    return-object v0
.end method

.method public final d()J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/gms/location/copresence/AccessPolicy;->d:J

    return-wide v0
.end method

.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final e()Lcom/google/android/gms/location/copresence/AccessLock;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/location/copresence/AccessPolicy;->e:Lcom/google/android/gms/location/copresence/AccessLock;

    return-object v0
.end method

.method public final f()Lcom/google/android/gms/common/people/data/Audience;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/location/copresence/AccessPolicy;->f:Lcom/google/android/gms/common/people/data/Audience;

    return-object v0
.end method

.method public final g()I
    .locals 1

    iget v0, p0, Lcom/google/android/gms/location/copresence/AccessPolicy;->g:I

    return v0
.end method

.method public final h()I
    .locals 1

    iget v0, p0, Lcom/google/android/gms/location/copresence/AccessPolicy;->h:I

    return v0
.end method

.method public final i()Lcom/google/android/gms/location/copresence/AclResourceId;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/location/copresence/AccessPolicy;->i:Lcom/google/android/gms/location/copresence/AclResourceId;

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    invoke-static {p0, p1, p2}, Lcom/google/android/gms/location/copresence/e;->a(Lcom/google/android/gms/location/copresence/AccessPolicy;Landroid/os/Parcel;I)V

    return-void
.end method
