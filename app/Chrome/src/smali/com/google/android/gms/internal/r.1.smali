.class public Lcom/google/android/gms/internal/r;
.super Lcom/google/android/gms/internal/b;


# static fields
.field private static final b:Ljava/lang/String;

.field private static final c:J

.field private static final d:J

.field private static final e:J

.field private static final f:J


# instance fields
.field private g:Lcom/google/android/gms/cast/s;

.field private final h:Landroid/os/Handler;

.field private final i:Lcom/google/android/gms/internal/v;

.field private final j:Lcom/google/android/gms/internal/v;

.field private final k:Lcom/google/android/gms/internal/v;

.field private final l:Lcom/google/android/gms/internal/v;

.field private final m:Lcom/google/android/gms/internal/v;

.field private final n:Lcom/google/android/gms/internal/v;

.field private final o:Lcom/google/android/gms/internal/v;

.field private final p:Lcom/google/android/gms/internal/v;

.field private final q:Lcom/google/android/gms/internal/v;

.field private final r:Lcom/google/android/gms/internal/v;

.field private final s:Ljava/util/List;

.field private final t:Ljava/lang/Runnable;

.field private u:Z


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const-wide/16 v4, 0x18

    const-string/jumbo v0, "com.google.cast.media"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "urn:x-cast:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/internal/r;->b:Ljava/lang/String;

    sget-object v0, Ljava/util/concurrent/TimeUnit;->HOURS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v4, v5}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lcom/google/android/gms/internal/r;->c:J

    sget-object v0, Ljava/util/concurrent/TimeUnit;->HOURS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v4, v5}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lcom/google/android/gms/internal/r;->d:J

    sget-object v0, Ljava/util/concurrent/TimeUnit;->HOURS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v4, v5}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lcom/google/android/gms/internal/r;->e:J

    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x1

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lcom/google/android/gms/internal/r;->f:J

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/gms/internal/r;-><init>(Ljava/lang/String;)V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;)V
    .locals 4

    sget-object v0, Lcom/google/android/gms/internal/r;->b:Ljava/lang/String;

    const-string/jumbo v1, "MediaControlChannel"

    const/4 v2, 0x0

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/gms/internal/b;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/google/android/gms/internal/r;->h:Landroid/os/Handler;

    new-instance v0, Lcom/google/android/gms/internal/s;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/gms/internal/s;-><init>(Lcom/google/android/gms/internal/r;B)V

    iput-object v0, p0, Lcom/google/android/gms/internal/r;->t:Ljava/lang/Runnable;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/internal/r;->s:Ljava/util/List;

    new-instance v0, Lcom/google/android/gms/internal/v;

    sget-wide v2, Lcom/google/android/gms/internal/r;->d:J

    invoke-direct {v0, v2, v3}, Lcom/google/android/gms/internal/v;-><init>(J)V

    iput-object v0, p0, Lcom/google/android/gms/internal/r;->i:Lcom/google/android/gms/internal/v;

    iget-object v0, p0, Lcom/google/android/gms/internal/r;->s:Ljava/util/List;

    iget-object v1, p0, Lcom/google/android/gms/internal/r;->i:Lcom/google/android/gms/internal/v;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v0, Lcom/google/android/gms/internal/v;

    sget-wide v2, Lcom/google/android/gms/internal/r;->c:J

    invoke-direct {v0, v2, v3}, Lcom/google/android/gms/internal/v;-><init>(J)V

    iput-object v0, p0, Lcom/google/android/gms/internal/r;->j:Lcom/google/android/gms/internal/v;

    iget-object v0, p0, Lcom/google/android/gms/internal/r;->s:Ljava/util/List;

    iget-object v1, p0, Lcom/google/android/gms/internal/r;->j:Lcom/google/android/gms/internal/v;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v0, Lcom/google/android/gms/internal/v;

    sget-wide v2, Lcom/google/android/gms/internal/r;->c:J

    invoke-direct {v0, v2, v3}, Lcom/google/android/gms/internal/v;-><init>(J)V

    iput-object v0, p0, Lcom/google/android/gms/internal/r;->k:Lcom/google/android/gms/internal/v;

    iget-object v0, p0, Lcom/google/android/gms/internal/r;->s:Ljava/util/List;

    iget-object v1, p0, Lcom/google/android/gms/internal/r;->k:Lcom/google/android/gms/internal/v;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v0, Lcom/google/android/gms/internal/v;

    sget-wide v2, Lcom/google/android/gms/internal/r;->c:J

    invoke-direct {v0, v2, v3}, Lcom/google/android/gms/internal/v;-><init>(J)V

    iput-object v0, p0, Lcom/google/android/gms/internal/r;->l:Lcom/google/android/gms/internal/v;

    iget-object v0, p0, Lcom/google/android/gms/internal/r;->s:Ljava/util/List;

    iget-object v1, p0, Lcom/google/android/gms/internal/r;->l:Lcom/google/android/gms/internal/v;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v0, Lcom/google/android/gms/internal/v;

    sget-wide v2, Lcom/google/android/gms/internal/r;->e:J

    invoke-direct {v0, v2, v3}, Lcom/google/android/gms/internal/v;-><init>(J)V

    iput-object v0, p0, Lcom/google/android/gms/internal/r;->m:Lcom/google/android/gms/internal/v;

    iget-object v0, p0, Lcom/google/android/gms/internal/r;->s:Ljava/util/List;

    iget-object v1, p0, Lcom/google/android/gms/internal/r;->m:Lcom/google/android/gms/internal/v;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v0, Lcom/google/android/gms/internal/v;

    sget-wide v2, Lcom/google/android/gms/internal/r;->c:J

    invoke-direct {v0, v2, v3}, Lcom/google/android/gms/internal/v;-><init>(J)V

    iput-object v0, p0, Lcom/google/android/gms/internal/r;->n:Lcom/google/android/gms/internal/v;

    iget-object v0, p0, Lcom/google/android/gms/internal/r;->s:Ljava/util/List;

    iget-object v1, p0, Lcom/google/android/gms/internal/r;->n:Lcom/google/android/gms/internal/v;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v0, Lcom/google/android/gms/internal/v;

    sget-wide v2, Lcom/google/android/gms/internal/r;->c:J

    invoke-direct {v0, v2, v3}, Lcom/google/android/gms/internal/v;-><init>(J)V

    iput-object v0, p0, Lcom/google/android/gms/internal/r;->o:Lcom/google/android/gms/internal/v;

    iget-object v0, p0, Lcom/google/android/gms/internal/r;->s:Ljava/util/List;

    iget-object v1, p0, Lcom/google/android/gms/internal/r;->o:Lcom/google/android/gms/internal/v;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v0, Lcom/google/android/gms/internal/v;

    sget-wide v2, Lcom/google/android/gms/internal/r;->c:J

    invoke-direct {v0, v2, v3}, Lcom/google/android/gms/internal/v;-><init>(J)V

    iput-object v0, p0, Lcom/google/android/gms/internal/r;->p:Lcom/google/android/gms/internal/v;

    iget-object v0, p0, Lcom/google/android/gms/internal/r;->s:Ljava/util/List;

    iget-object v1, p0, Lcom/google/android/gms/internal/r;->p:Lcom/google/android/gms/internal/v;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v0, Lcom/google/android/gms/internal/v;

    sget-wide v2, Lcom/google/android/gms/internal/r;->c:J

    invoke-direct {v0, v2, v3}, Lcom/google/android/gms/internal/v;-><init>(J)V

    iput-object v0, p0, Lcom/google/android/gms/internal/r;->q:Lcom/google/android/gms/internal/v;

    iget-object v0, p0, Lcom/google/android/gms/internal/r;->s:Ljava/util/List;

    iget-object v1, p0, Lcom/google/android/gms/internal/r;->q:Lcom/google/android/gms/internal/v;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v0, Lcom/google/android/gms/internal/v;

    sget-wide v2, Lcom/google/android/gms/internal/r;->c:J

    invoke-direct {v0, v2, v3}, Lcom/google/android/gms/internal/v;-><init>(J)V

    iput-object v0, p0, Lcom/google/android/gms/internal/r;->r:Lcom/google/android/gms/internal/v;

    iget-object v0, p0, Lcom/google/android/gms/internal/r;->s:Ljava/util/List;

    iget-object v1, p0, Lcom/google/android/gms/internal/r;->r:Lcom/google/android/gms/internal/v;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-direct {p0}, Lcom/google/android/gms/internal/r;->i()V

    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/internal/r;)Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/internal/r;->s:Ljava/util/List;

    return-object v0
.end method

.method private a(JLorg/json/JSONObject;)V
    .locals 5

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/gms/internal/r;->i:Lcom/google/android/gms/internal/v;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/internal/v;->a(J)Z

    move-result v3

    iget-object v0, p0, Lcom/google/android/gms/internal/r;->m:Lcom/google/android/gms/internal/v;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/v;->b()Z

    move-result v0

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/google/android/gms/internal/r;->m:Lcom/google/android/gms/internal/v;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/internal/v;->a(J)Z

    move-result v0

    if-nez v0, :cond_7

    move v0, v1

    :goto_0
    iget-object v4, p0, Lcom/google/android/gms/internal/r;->n:Lcom/google/android/gms/internal/v;

    invoke-virtual {v4}, Lcom/google/android/gms/internal/v;->b()Z

    move-result v4

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/google/android/gms/internal/r;->n:Lcom/google/android/gms/internal/v;

    invoke-virtual {v4, p1, p2}, Lcom/google/android/gms/internal/v;->a(J)Z

    move-result v4

    if-eqz v4, :cond_1

    :cond_0
    iget-object v4, p0, Lcom/google/android/gms/internal/r;->o:Lcom/google/android/gms/internal/v;

    invoke-virtual {v4}, Lcom/google/android/gms/internal/v;->b()Z

    move-result v4

    if-eqz v4, :cond_8

    iget-object v4, p0, Lcom/google/android/gms/internal/r;->o:Lcom/google/android/gms/internal/v;

    invoke-virtual {v4, p1, p2}, Lcom/google/android/gms/internal/v;->a(J)Z

    move-result v4

    if-nez v4, :cond_8

    :cond_1
    :goto_1
    if-eqz v0, :cond_b

    const/4 v0, 0x2

    :goto_2
    if-eqz v1, :cond_2

    or-int/lit8 v0, v0, 0x1

    :cond_2
    if-nez v3, :cond_3

    iget-object v1, p0, Lcom/google/android/gms/internal/r;->g:Lcom/google/android/gms/cast/s;

    if-nez v1, :cond_9

    :cond_3
    new-instance v0, Lcom/google/android/gms/cast/s;

    invoke-direct {v0, p3}, Lcom/google/android/gms/cast/s;-><init>(Lorg/json/JSONObject;)V

    iput-object v0, p0, Lcom/google/android/gms/internal/r;->g:Lcom/google/android/gms/cast/s;

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    const/4 v0, 0x7

    :goto_3
    and-int/lit8 v1, v0, 0x1

    if-eqz v1, :cond_4

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    invoke-virtual {p0}, Lcom/google/android/gms/internal/r;->a()V

    :cond_4
    and-int/lit8 v1, v0, 0x2

    if-eqz v1, :cond_5

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    invoke-virtual {p0}, Lcom/google/android/gms/internal/r;->a()V

    :cond_5
    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_6

    invoke-virtual {p0}, Lcom/google/android/gms/internal/r;->b()V

    :cond_6
    iget-object v0, p0, Lcom/google/android/gms/internal/r;->s:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_4
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_a

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/internal/v;

    invoke-virtual {v0, p1, p2, v2}, Lcom/google/android/gms/internal/v;->a(JI)Z

    goto :goto_4

    :cond_7
    move v0, v2

    goto :goto_0

    :cond_8
    move v1, v2

    goto :goto_1

    :cond_9
    iget-object v1, p0, Lcom/google/android/gms/internal/r;->g:Lcom/google/android/gms/cast/s;

    invoke-virtual {v1, p3, v0}, Lcom/google/android/gms/cast/s;->a(Lorg/json/JSONObject;I)I

    move-result v0

    goto :goto_3

    :cond_a
    return-void

    :cond_b
    move v0, v2

    goto :goto_2
.end method

.method private a(Z)V
    .locals 4

    iget-boolean v0, p0, Lcom/google/android/gms/internal/r;->u:Z

    if-eq v0, p1, :cond_0

    iput-boolean p1, p0, Lcom/google/android/gms/internal/r;->u:Z

    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/internal/r;->h:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/gms/internal/r;->t:Ljava/lang/Runnable;

    sget-wide v2, Lcom/google/android/gms/internal/r;->f:J

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/internal/r;->h:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/gms/internal/r;->t:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/gms/internal/r;Z)Z
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/internal/r;->u:Z

    return v0
.end method

.method static synthetic b(Lcom/google/android/gms/internal/r;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/gms/internal/r;->a(Z)V

    return-void
.end method

.method private h()J
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/internal/r;->g:Lcom/google/android/gms/cast/s;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "No current media session"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/internal/r;->g:Lcom/google/android/gms/cast/s;

    invoke-virtual {v0}, Lcom/google/android/gms/cast/s;->a()J

    move-result-wide v0

    return-wide v0
.end method

.method private i()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/gms/internal/r;->a(Z)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/internal/r;->g:Lcom/google/android/gms/cast/s;

    iget-object v0, p0, Lcom/google/android/gms/internal/r;->i:Lcom/google/android/gms/internal/v;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/v;->a()V

    iget-object v0, p0, Lcom/google/android/gms/internal/r;->m:Lcom/google/android/gms/internal/v;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/v;->a()V

    iget-object v0, p0, Lcom/google/android/gms/internal/r;->n:Lcom/google/android/gms/internal/v;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/v;->a()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/internal/u;)J
    .locals 6

    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    invoke-virtual {p0}, Lcom/google/android/gms/internal/r;->d()J

    move-result-wide v2

    iget-object v1, p0, Lcom/google/android/gms/internal/r;->p:Lcom/google/android/gms/internal/v;

    invoke-virtual {v1, v2, v3, p1}, Lcom/google/android/gms/internal/v;->a(JLcom/google/android/gms/internal/u;)V

    const/4 v1, 0x1

    invoke-direct {p0, v1}, Lcom/google/android/gms/internal/r;->a(Z)V

    :try_start_0
    const-string/jumbo v1, "requestId"

    invoke-virtual {v0, v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    const-string/jumbo v1, "type"

    const-string/jumbo v4, "GET_STATUS"

    invoke-virtual {v0, v1, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    iget-object v1, p0, Lcom/google/android/gms/internal/r;->g:Lcom/google/android/gms/cast/s;

    if-eqz v1, :cond_0

    const-string/jumbo v1, "mediaSessionId"

    iget-object v4, p0, Lcom/google/android/gms/internal/r;->g:Lcom/google/android/gms/cast/s;

    invoke-virtual {v4}, Lcom/google/android/gms/cast/s;->a()J

    move-result-wide v4

    invoke-virtual {v0, v1, v4, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v2, v3, v1}, Lcom/google/android/gms/internal/r;->a(Ljava/lang/String;JLjava/lang/String;)V

    return-wide v2

    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/internal/u;Lorg/json/JSONObject;)J
    .locals 6

    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    invoke-virtual {p0}, Lcom/google/android/gms/internal/r;->d()J

    move-result-wide v2

    iget-object v1, p0, Lcom/google/android/gms/internal/r;->j:Lcom/google/android/gms/internal/v;

    invoke-virtual {v1, v2, v3, p1}, Lcom/google/android/gms/internal/v;->a(JLcom/google/android/gms/internal/u;)V

    const/4 v1, 0x1

    invoke-direct {p0, v1}, Lcom/google/android/gms/internal/r;->a(Z)V

    :try_start_0
    const-string/jumbo v1, "requestId"

    invoke-virtual {v0, v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    const-string/jumbo v1, "type"

    const-string/jumbo v4, "PAUSE"

    invoke-virtual {v0, v1, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string/jumbo v1, "mediaSessionId"

    invoke-direct {p0}, Lcom/google/android/gms/internal/r;->h()J

    move-result-wide v4

    invoke-virtual {v0, v1, v4, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    if-eqz p2, :cond_0

    const-string/jumbo v1, "customData"

    invoke-virtual {v0, v1, p2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v2, v3, v1}, Lcom/google/android/gms/internal/r;->a(Ljava/lang/String;JLjava/lang/String;)V

    return-wide v2

    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method protected a()V
    .locals 0

    return-void
.end method

.method public final a(JI)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/gms/internal/r;->s:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/internal/v;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/gms/internal/v;->a(JI)Z

    goto :goto_0

    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 8

    const/4 v7, 0x1

    const/4 v6, 0x0

    iget-object v0, p0, Lcom/google/android/gms/internal/r;->a:Lcom/google/android/gms/internal/q;

    const-string/jumbo v1, "message received: %s"

    new-array v2, v7, [Ljava/lang/Object;

    aput-object p1, v2, v6

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/internal/q;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    :try_start_0
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    const-string/jumbo v1, "type"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "requestId"

    const-wide/16 v4, -0x1

    invoke-virtual {v0, v2, v4, v5}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;J)J

    move-result-wide v2

    const-string/jumbo v4, "MEDIA_STATUS"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    const-string/jumbo v1, "status"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v0

    invoke-virtual {v0}, Lorg/json/JSONArray;->length()I

    move-result v1

    if-lez v1, :cond_1

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v0

    invoke-direct {p0, v2, v3, v0}, Lcom/google/android/gms/internal/r;->a(JLorg/json/JSONObject;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/internal/r;->g:Lcom/google/android/gms/cast/s;

    invoke-virtual {p0}, Lcom/google/android/gms/internal/r;->a()V

    invoke-virtual {p0}, Lcom/google/android/gms/internal/r;->b()V

    iget-object v0, p0, Lcom/google/android/gms/internal/r;->p:Lcom/google/android/gms/internal/v;

    const/4 v1, 0x0

    invoke-virtual {v0, v2, v3, v1}, Lcom/google/android/gms/internal/v;->a(JI)Z
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/gms/internal/r;->a:Lcom/google/android/gms/internal/q;

    const-string/jumbo v2, "Message is malformed (%s); ignoring: %s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v0}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v6

    aput-object p1, v3, v7

    invoke-virtual {v1, v2, v3}, Lcom/google/android/gms/internal/q;->c(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :cond_2
    :try_start_1
    const-string/jumbo v4, "INVALID_PLAYER_STATE"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    iget-object v1, p0, Lcom/google/android/gms/internal/r;->a:Lcom/google/android/gms/internal/q;

    const-string/jumbo v4, "received unexpected error: Invalid Player State."

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Object;

    invoke-virtual {v1, v4, v5}, Lcom/google/android/gms/internal/q;->c(Ljava/lang/String;[Ljava/lang/Object;)V

    const-string/jumbo v1, "customData"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/gms/internal/r;->s:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/internal/v;

    const/16 v5, 0x834

    invoke-virtual {v0, v2, v3, v5, v1}, Lcom/google/android/gms/internal/v;->a(JILorg/json/JSONObject;)Z

    goto :goto_1

    :cond_3
    const-string/jumbo v4, "LOAD_FAILED"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    const-string/jumbo v1, "customData"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/internal/r;->i:Lcom/google/android/gms/internal/v;

    const/16 v4, 0x834

    invoke-virtual {v1, v2, v3, v4, v0}, Lcom/google/android/gms/internal/v;->a(JILorg/json/JSONObject;)Z

    goto :goto_0

    :cond_4
    const-string/jumbo v4, "LOAD_CANCELLED"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    const-string/jumbo v1, "customData"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/internal/r;->i:Lcom/google/android/gms/internal/v;

    const/16 v4, 0x835

    invoke-virtual {v1, v2, v3, v4, v0}, Lcom/google/android/gms/internal/v;->a(JILorg/json/JSONObject;)Z

    goto/16 :goto_0

    :cond_5
    const-string/jumbo v4, "INVALID_REQUEST"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/internal/r;->a:Lcom/google/android/gms/internal/q;

    const-string/jumbo v4, "received unexpected error: Invalid Request."

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Object;

    invoke-virtual {v1, v4, v5}, Lcom/google/android/gms/internal/q;->c(Ljava/lang/String;[Ljava/lang/Object;)V

    const-string/jumbo v1, "customData"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/gms/internal/r;->s:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/internal/v;

    const/16 v5, 0x834

    invoke-virtual {v0, v2, v3, v5, v1}, Lcom/google/android/gms/internal/v;->a(JILorg/json/JSONObject;)Z
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_2
.end method

.method public final b(Lcom/google/android/gms/internal/u;Lorg/json/JSONObject;)J
    .locals 6

    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    invoke-virtual {p0}, Lcom/google/android/gms/internal/r;->d()J

    move-result-wide v2

    iget-object v1, p0, Lcom/google/android/gms/internal/r;->k:Lcom/google/android/gms/internal/v;

    invoke-virtual {v1, v2, v3, p1}, Lcom/google/android/gms/internal/v;->a(JLcom/google/android/gms/internal/u;)V

    const/4 v1, 0x1

    invoke-direct {p0, v1}, Lcom/google/android/gms/internal/r;->a(Z)V

    :try_start_0
    const-string/jumbo v1, "requestId"

    invoke-virtual {v0, v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    const-string/jumbo v1, "type"

    const-string/jumbo v4, "PLAY"

    invoke-virtual {v0, v1, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string/jumbo v1, "mediaSessionId"

    invoke-direct {p0}, Lcom/google/android/gms/internal/r;->h()J

    move-result-wide v4

    invoke-virtual {v0, v1, v4, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    if-eqz p2, :cond_0

    const-string/jumbo v1, "customData"

    invoke-virtual {v0, v1, p2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v2, v3, v1}, Lcom/google/android/gms/internal/r;->a(Ljava/lang/String;JLjava/lang/String;)V

    return-wide v2

    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method protected b()V
    .locals 0

    return-void
.end method

.method public final e()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/gms/internal/r;->i()V

    return-void
.end method

.method public final f()Lcom/google/android/gms/cast/s;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/internal/r;->g:Lcom/google/android/gms/cast/s;

    return-object v0
.end method

.method public final g()Lcom/google/android/gms/cast/p;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/internal/r;->g:Lcom/google/android/gms/cast/s;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/internal/r;->g:Lcom/google/android/gms/cast/s;

    invoke-virtual {v0}, Lcom/google/android/gms/cast/s;->d()Lcom/google/android/gms/cast/p;

    move-result-object v0

    goto :goto_0
.end method
