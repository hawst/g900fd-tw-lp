.class public final Lcom/google/android/gms/drive/metadata/internal/f;
.super Ljava/lang/Object;


# static fields
.field private static a:Ljava/util/Map;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/google/android/gms/drive/metadata/internal/f;->a:Ljava/util/Map;

    sget-object v0, Lcom/google/android/gms/internal/aj;->a:Lcom/google/android/gms/drive/metadata/a;

    invoke-static {v0}, Lcom/google/android/gms/drive/metadata/internal/f;->a(Lcom/google/android/gms/drive/metadata/a;)V

    sget-object v0, Lcom/google/android/gms/internal/aj;->A:Lcom/google/android/gms/internal/ap;

    invoke-static {v0}, Lcom/google/android/gms/drive/metadata/internal/f;->a(Lcom/google/android/gms/drive/metadata/a;)V

    sget-object v0, Lcom/google/android/gms/internal/aj;->r:Lcom/google/android/gms/internal/al;

    invoke-static {v0}, Lcom/google/android/gms/drive/metadata/internal/f;->a(Lcom/google/android/gms/drive/metadata/a;)V

    sget-object v0, Lcom/google/android/gms/internal/aj;->y:Lcom/google/android/gms/internal/ao;

    invoke-static {v0}, Lcom/google/android/gms/drive/metadata/internal/f;->a(Lcom/google/android/gms/drive/metadata/a;)V

    sget-object v0, Lcom/google/android/gms/internal/aj;->B:Lcom/google/android/gms/internal/aq;

    invoke-static {v0}, Lcom/google/android/gms/drive/metadata/internal/f;->a(Lcom/google/android/gms/drive/metadata/a;)V

    sget-object v0, Lcom/google/android/gms/internal/aj;->l:Lcom/google/android/gms/drive/metadata/a;

    invoke-static {v0}, Lcom/google/android/gms/drive/metadata/internal/f;->a(Lcom/google/android/gms/drive/metadata/a;)V

    sget-object v0, Lcom/google/android/gms/internal/aj;->m:Lcom/google/android/gms/internal/ak;

    invoke-static {v0}, Lcom/google/android/gms/drive/metadata/internal/f;->a(Lcom/google/android/gms/drive/metadata/a;)V

    sget-object v0, Lcom/google/android/gms/internal/aj;->j:Lcom/google/android/gms/drive/metadata/a;

    invoke-static {v0}, Lcom/google/android/gms/drive/metadata/internal/f;->a(Lcom/google/android/gms/drive/metadata/a;)V

    sget-object v0, Lcom/google/android/gms/internal/aj;->o:Lcom/google/android/gms/drive/metadata/a;

    invoke-static {v0}, Lcom/google/android/gms/drive/metadata/internal/f;->a(Lcom/google/android/gms/drive/metadata/a;)V

    sget-object v0, Lcom/google/android/gms/internal/aj;->w:Lcom/google/android/gms/internal/am;

    invoke-static {v0}, Lcom/google/android/gms/drive/metadata/internal/f;->a(Lcom/google/android/gms/drive/metadata/a;)V

    sget-object v0, Lcom/google/android/gms/internal/aj;->b:Lcom/google/android/gms/drive/metadata/a;

    invoke-static {v0}, Lcom/google/android/gms/drive/metadata/internal/f;->a(Lcom/google/android/gms/drive/metadata/a;)V

    sget-object v0, Lcom/google/android/gms/internal/aj;->t:Lcom/google/android/gms/drive/metadata/c;

    invoke-static {v0}, Lcom/google/android/gms/drive/metadata/internal/f;->a(Lcom/google/android/gms/drive/metadata/a;)V

    sget-object v0, Lcom/google/android/gms/internal/aj;->d:Lcom/google/android/gms/drive/metadata/a;

    invoke-static {v0}, Lcom/google/android/gms/drive/metadata/internal/f;->a(Lcom/google/android/gms/drive/metadata/a;)V

    sget-object v0, Lcom/google/android/gms/internal/aj;->k:Lcom/google/android/gms/drive/metadata/a;

    invoke-static {v0}, Lcom/google/android/gms/drive/metadata/internal/f;->a(Lcom/google/android/gms/drive/metadata/a;)V

    sget-object v0, Lcom/google/android/gms/internal/aj;->e:Lcom/google/android/gms/drive/metadata/a;

    invoke-static {v0}, Lcom/google/android/gms/drive/metadata/internal/f;->a(Lcom/google/android/gms/drive/metadata/a;)V

    sget-object v0, Lcom/google/android/gms/internal/aj;->f:Lcom/google/android/gms/drive/metadata/a;

    invoke-static {v0}, Lcom/google/android/gms/drive/metadata/internal/f;->a(Lcom/google/android/gms/drive/metadata/a;)V

    sget-object v0, Lcom/google/android/gms/internal/aj;->g:Lcom/google/android/gms/drive/metadata/a;

    invoke-static {v0}, Lcom/google/android/gms/drive/metadata/internal/f;->a(Lcom/google/android/gms/drive/metadata/a;)V

    sget-object v0, Lcom/google/android/gms/internal/aj;->q:Lcom/google/android/gms/drive/metadata/a;

    invoke-static {v0}, Lcom/google/android/gms/drive/metadata/internal/f;->a(Lcom/google/android/gms/drive/metadata/a;)V

    sget-object v0, Lcom/google/android/gms/internal/aj;->n:Lcom/google/android/gms/drive/metadata/a;

    invoke-static {v0}, Lcom/google/android/gms/drive/metadata/internal/f;->a(Lcom/google/android/gms/drive/metadata/a;)V

    sget-object v0, Lcom/google/android/gms/internal/aj;->s:Lcom/google/android/gms/drive/metadata/a;

    invoke-static {v0}, Lcom/google/android/gms/drive/metadata/internal/f;->a(Lcom/google/android/gms/drive/metadata/a;)V

    sget-object v0, Lcom/google/android/gms/internal/aj;->u:Lcom/google/android/gms/drive/metadata/internal/n;

    invoke-static {v0}, Lcom/google/android/gms/drive/metadata/internal/f;->a(Lcom/google/android/gms/drive/metadata/a;)V

    sget-object v0, Lcom/google/android/gms/internal/aj;->v:Lcom/google/android/gms/drive/metadata/internal/n;

    invoke-static {v0}, Lcom/google/android/gms/drive/metadata/internal/f;->a(Lcom/google/android/gms/drive/metadata/a;)V

    sget-object v0, Lcom/google/android/gms/internal/aj;->x:Lcom/google/android/gms/internal/an;

    invoke-static {v0}, Lcom/google/android/gms/drive/metadata/internal/f;->a(Lcom/google/android/gms/drive/metadata/a;)V

    sget-object v0, Lcom/google/android/gms/internal/aj;->C:Lcom/google/android/gms/drive/metadata/a;

    invoke-static {v0}, Lcom/google/android/gms/drive/metadata/internal/f;->a(Lcom/google/android/gms/drive/metadata/a;)V

    sget-object v0, Lcom/google/android/gms/internal/aj;->D:Lcom/google/android/gms/drive/metadata/a;

    invoke-static {v0}, Lcom/google/android/gms/drive/metadata/internal/f;->a(Lcom/google/android/gms/drive/metadata/a;)V

    sget-object v0, Lcom/google/android/gms/internal/aj;->i:Lcom/google/android/gms/drive/metadata/a;

    invoke-static {v0}, Lcom/google/android/gms/drive/metadata/internal/f;->a(Lcom/google/android/gms/drive/metadata/a;)V

    sget-object v0, Lcom/google/android/gms/internal/aj;->h:Lcom/google/android/gms/drive/metadata/a;

    invoke-static {v0}, Lcom/google/android/gms/drive/metadata/internal/f;->a(Lcom/google/android/gms/drive/metadata/a;)V

    sget-object v0, Lcom/google/android/gms/internal/aj;->z:Lcom/google/android/gms/drive/metadata/a;

    invoke-static {v0}, Lcom/google/android/gms/drive/metadata/internal/f;->a(Lcom/google/android/gms/drive/metadata/a;)V

    sget-object v0, Lcom/google/android/gms/internal/aj;->p:Lcom/google/android/gms/drive/metadata/a;

    invoke-static {v0}, Lcom/google/android/gms/drive/metadata/internal/f;->a(Lcom/google/android/gms/drive/metadata/a;)V

    sget-object v0, Lcom/google/android/gms/internal/aj;->c:Lcom/google/android/gms/drive/metadata/internal/n;

    invoke-static {v0}, Lcom/google/android/gms/drive/metadata/internal/f;->a(Lcom/google/android/gms/drive/metadata/a;)V

    sget-object v0, Lcom/google/android/gms/internal/aj;->E:Lcom/google/android/gms/drive/metadata/a;

    invoke-static {v0}, Lcom/google/android/gms/drive/metadata/internal/f;->a(Lcom/google/android/gms/drive/metadata/a;)V

    sget-object v0, Lcom/google/android/gms/internal/aj;->F:Lcom/google/android/gms/drive/metadata/internal/c;

    invoke-static {v0}, Lcom/google/android/gms/drive/metadata/internal/f;->a(Lcom/google/android/gms/drive/metadata/a;)V

    sget-object v0, Lcom/google/android/gms/internal/aj;->G:Lcom/google/android/gms/drive/metadata/a;

    invoke-static {v0}, Lcom/google/android/gms/drive/metadata/internal/f;->a(Lcom/google/android/gms/drive/metadata/a;)V

    sget-object v0, Lcom/google/android/gms/internal/as;->a:Lcom/google/android/gms/internal/at;

    invoke-static {v0}, Lcom/google/android/gms/drive/metadata/internal/f;->a(Lcom/google/android/gms/drive/metadata/a;)V

    sget-object v0, Lcom/google/android/gms/internal/as;->c:Lcom/google/android/gms/internal/aw;

    invoke-static {v0}, Lcom/google/android/gms/drive/metadata/internal/f;->a(Lcom/google/android/gms/drive/metadata/a;)V

    sget-object v0, Lcom/google/android/gms/internal/as;->d:Lcom/google/android/gms/internal/av;

    invoke-static {v0}, Lcom/google/android/gms/drive/metadata/internal/f;->a(Lcom/google/android/gms/drive/metadata/a;)V

    sget-object v0, Lcom/google/android/gms/internal/as;->e:Lcom/google/android/gms/internal/ax;

    invoke-static {v0}, Lcom/google/android/gms/drive/metadata/internal/f;->a(Lcom/google/android/gms/drive/metadata/a;)V

    sget-object v0, Lcom/google/android/gms/internal/as;->b:Lcom/google/android/gms/internal/au;

    invoke-static {v0}, Lcom/google/android/gms/drive/metadata/internal/f;->a(Lcom/google/android/gms/drive/metadata/a;)V

    sget-object v0, Lcom/google/android/gms/internal/az;->a:Lcom/google/android/gms/drive/metadata/a;

    invoke-static {v0}, Lcom/google/android/gms/drive/metadata/internal/f;->a(Lcom/google/android/gms/drive/metadata/a;)V

    sget-object v0, Lcom/google/android/gms/internal/az;->b:Lcom/google/android/gms/drive/metadata/a;

    invoke-static {v0}, Lcom/google/android/gms/drive/metadata/internal/f;->a(Lcom/google/android/gms/drive/metadata/a;)V

    return-void
.end method

.method public static a(Ljava/lang/String;)Lcom/google/android/gms/drive/metadata/a;
    .locals 1

    sget-object v0, Lcom/google/android/gms/drive/metadata/internal/f;->a:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/metadata/a;

    return-object v0
.end method

.method private static a(Lcom/google/android/gms/drive/metadata/a;)V
    .locals 3

    sget-object v0, Lcom/google/android/gms/drive/metadata/internal/f;->a:Ljava/util/Map;

    invoke-interface {p0}, Lcom/google/android/gms/drive/metadata/a;->a()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "Duplicate field name registered: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {p0}, Lcom/google/android/gms/drive/metadata/a;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    sget-object v0, Lcom/google/android/gms/drive/metadata/internal/f;->a:Ljava/util/Map;

    invoke-interface {p0}, Lcom/google/android/gms/drive/metadata/a;->a()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method
