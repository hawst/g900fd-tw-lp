.class public final Lcom/google/android/gms/internal/Y;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/common/b;
.implements Lcom/google/android/gms/common/c;


# instance fields
.field private final a:Lcom/google/android/gms/b/b;

.field private b:Lcom/google/android/gms/internal/aa;

.field private c:Z


# direct methods
.method public constructor <init>(Lcom/google/android/gms/b/b;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/gms/internal/Y;->a:Lcom/google/android/gms/b/b;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/internal/Y;->b:Lcom/google/android/gms/internal/aa;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/internal/Y;->c:Z

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/internal/aa;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/internal/Y;->b:Lcom/google/android/gms/internal/aa;

    return-void
.end method

.method public final a(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/gms/internal/Y;->c:Z

    return-void
.end method

.method public final onConnected(Landroid/os/Bundle;)V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/gms/internal/Y;->b:Lcom/google/android/gms/internal/aa;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/internal/aa;->a(Z)V

    iget-boolean v0, p0, Lcom/google/android/gms/internal/Y;->c:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/internal/Y;->a:Lcom/google/android/gms/b/b;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/internal/Y;->a:Lcom/google/android/gms/b/b;

    invoke-interface {v0}, Lcom/google/android/gms/b/b;->onLoggerConnected()V

    :cond_0
    iput-boolean v1, p0, Lcom/google/android/gms/internal/Y;->c:Z

    return-void
.end method

.method public final onConnectionFailed(Lcom/google/android/gms/common/ConnectionResult;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/internal/Y;->b:Lcom/google/android/gms/internal/aa;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/internal/aa;->a(Z)V

    iget-boolean v0, p0, Lcom/google/android/gms/internal/Y;->c:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/internal/Y;->a:Lcom/google/android/gms/b/b;

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/google/android/gms/common/ConnectionResult;->hasResolution()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/internal/Y;->a:Lcom/google/android/gms/b/b;

    invoke-virtual {p1}, Lcom/google/android/gms/common/ConnectionResult;->getResolution()Landroid/app/PendingIntent;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/gms/b/b;->onLoggerFailedConnectionWithResolution(Landroid/app/PendingIntent;)V

    :cond_0
    :goto_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/internal/Y;->c:Z

    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/internal/Y;->a:Lcom/google/android/gms/b/b;

    invoke-interface {v0}, Lcom/google/android/gms/b/b;->onLoggerFailedConnection()V

    goto :goto_0
.end method

.method public final onDisconnected()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/internal/Y;->b:Lcom/google/android/gms/internal/aa;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/internal/aa;->a(Z)V

    return-void
.end method
