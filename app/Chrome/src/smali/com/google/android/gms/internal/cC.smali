.class public final Lcom/google/android/gms/internal/cC;
.super Lcom/google/android/gms/common/internal/d;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/os/Looper;Lcom/google/android/gms/common/api/k;Lcom/google/android/gms/common/c;)V
    .locals 6

    const/4 v0, 0x0

    new-array v5, v0, [Ljava/lang/String;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/common/internal/d;-><init>(Landroid/content/Context;Landroid/os/Looper;Lcom/google/android/gms/common/api/k;Lcom/google/android/gms/common/c;[Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method protected final synthetic a(Landroid/os/IBinder;)Landroid/os/IInterface;
    .locals 1

    invoke-static {p1}, Lcom/google/android/gms/internal/cw;->a(Landroid/os/IBinder;)Lcom/google/android/gms/internal/cv;

    move-result-object v0

    return-object v0
.end method

.method protected final a(Lcom/google/android/gms/common/internal/A;Lcom/google/android/gms/common/internal/h;)V
    .locals 2

    const v0, 0x5d2f78

    invoke-virtual {p0}, Lcom/google/android/gms/internal/cC;->h()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, p2, v0, v1}, Lcom/google/android/gms/common/internal/A;->b(Lcom/google/android/gms/common/internal/x;ILjava/lang/String;)V

    return-void
.end method

.method protected final e()Ljava/lang/String;
    .locals 1

    const-string/jumbo v0, "com.google.android.gms.icing.LIGHTWEIGHT_INDEX_SERVICE"

    return-object v0
.end method

.method protected final f()Ljava/lang/String;
    .locals 1

    const-string/jumbo v0, "com.google.android.gms.appdatasearch.internal.ILightweightAppDataSearch"

    return-object v0
.end method

.method public final j()Lcom/google/android/gms/internal/cv;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/gms/internal/cC;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/internal/cv;

    return-object v0
.end method
