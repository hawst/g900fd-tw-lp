.class public Lcom/google/android/gms/appdatasearch/SearchResults;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;
.implements Ljava/lang/Iterable;


# static fields
.field public static final CREATOR:Lcom/google/android/gms/appdatasearch/q;


# instance fields
.field final a:I

.field final b:Ljava/lang/String;

.field final c:[I

.field final d:[B

.field final e:[Landroid/os/Bundle;

.field final f:[Landroid/os/Bundle;

.field final g:[Landroid/os/Bundle;

.field final h:I

.field final i:[I

.field final j:[Ljava/lang/String;

.field final k:[B

.field final l:[D


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/gms/appdatasearch/q;

    invoke-direct {v0}, Lcom/google/android/gms/appdatasearch/q;-><init>()V

    sput-object v0, Lcom/google/android/gms/appdatasearch/SearchResults;->CREATOR:Lcom/google/android/gms/appdatasearch/q;

    return-void
.end method

.method constructor <init>(ILjava/lang/String;[I[B[Landroid/os/Bundle;[Landroid/os/Bundle;[Landroid/os/Bundle;I[I[Ljava/lang/String;[B[D)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/gms/appdatasearch/SearchResults;->a:I

    iput-object p2, p0, Lcom/google/android/gms/appdatasearch/SearchResults;->b:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/gms/appdatasearch/SearchResults;->c:[I

    iput-object p4, p0, Lcom/google/android/gms/appdatasearch/SearchResults;->d:[B

    iput-object p5, p0, Lcom/google/android/gms/appdatasearch/SearchResults;->e:[Landroid/os/Bundle;

    iput-object p6, p0, Lcom/google/android/gms/appdatasearch/SearchResults;->f:[Landroid/os/Bundle;

    iput-object p7, p0, Lcom/google/android/gms/appdatasearch/SearchResults;->g:[Landroid/os/Bundle;

    iput p8, p0, Lcom/google/android/gms/appdatasearch/SearchResults;->h:I

    iput-object p9, p0, Lcom/google/android/gms/appdatasearch/SearchResults;->i:[I

    iput-object p10, p0, Lcom/google/android/gms/appdatasearch/SearchResults;->j:[Ljava/lang/String;

    iput-object p11, p0, Lcom/google/android/gms/appdatasearch/SearchResults;->k:[B

    iput-object p12, p0, Lcom/google/android/gms/appdatasearch/SearchResults;->l:[D

    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/appdatasearch/SearchResults;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()I
    .locals 1

    iget v0, p0, Lcom/google/android/gms/appdatasearch/SearchResults;->h:I

    return v0
.end method

.method public describeContents()I
    .locals 1

    sget-object v0, Lcom/google/android/gms/appdatasearch/SearchResults;->CREATOR:Lcom/google/android/gms/appdatasearch/q;

    const/4 v0, 0x0

    return v0
.end method

.method public synthetic iterator()Ljava/util/Iterator;
    .locals 1

    new-instance v0, Lcom/google/android/gms/appdatasearch/f;

    invoke-direct {v0, p0}, Lcom/google/android/gms/appdatasearch/f;-><init>(Lcom/google/android/gms/appdatasearch/SearchResults;)V

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    sget-object v0, Lcom/google/android/gms/appdatasearch/SearchResults;->CREATOR:Lcom/google/android/gms/appdatasearch/q;

    invoke-static {p0, p1, p2}, Lcom/google/android/gms/appdatasearch/q;->a(Lcom/google/android/gms/appdatasearch/SearchResults;Landroid/os/Parcel;I)V

    return-void
.end method
