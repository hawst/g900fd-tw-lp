.class public abstract Lcom/google/android/gms/common/internal/d;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/common/api/b;
.implements Lcom/google/android/gms/common/internal/n;


# instance fields
.field final a:Landroid/os/Handler;

.field private final b:Landroid/content/Context;

.field private final c:Landroid/os/Looper;

.field private d:Landroid/os/IInterface;

.field private final e:Ljava/util/ArrayList;

.field private f:Lcom/google/android/gms/common/internal/i;

.field private volatile g:I

.field private h:Z

.field private final i:Lcom/google/android/gms/common/internal/l;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string/jumbo v2, "service_esmobile"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string/jumbo v2, "service_googleme"

    aput-object v2, v0, v1

    return-void
.end method

.method protected varargs constructor <init>(Landroid/content/Context;Landroid/os/Looper;Lcom/google/android/gms/common/api/k;Lcom/google/android/gms/common/c;[Ljava/lang/String;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/common/internal/d;->e:Ljava/util/ArrayList;

    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/common/internal/d;->g:I

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/common/internal/d;->h:Z

    invoke-static {p1}, Landroid/support/v4/app/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lcom/google/android/gms/common/internal/d;->b:Landroid/content/Context;

    const-string/jumbo v0, "Looper must not be null"

    invoke-static {p2, v0}, Landroid/support/v4/app/c;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Looper;

    iput-object v0, p0, Lcom/google/android/gms/common/internal/d;->c:Landroid/os/Looper;

    new-instance v0, Lcom/google/android/gms/common/internal/l;

    invoke-direct {v0, p2, p0}, Lcom/google/android/gms/common/internal/l;-><init>(Landroid/os/Looper;Lcom/google/android/gms/common/internal/n;)V

    iput-object v0, p0, Lcom/google/android/gms/common/internal/d;->i:Lcom/google/android/gms/common/internal/l;

    new-instance v0, Lcom/google/android/gms/common/internal/e;

    invoke-direct {v0, p0, p2}, Lcom/google/android/gms/common/internal/e;-><init>(Lcom/google/android/gms/common/internal/d;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/google/android/gms/common/internal/d;->a:Landroid/os/Handler;

    invoke-static {p3}, Landroid/support/v4/app/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/api/k;

    iget-object v1, p0, Lcom/google/android/gms/common/internal/d;->i:Lcom/google/android/gms/common/internal/l;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/common/internal/l;->a(Lcom/google/android/gms/common/api/k;)V

    invoke-static {p4}, Landroid/support/v4/app/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/c;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/common/internal/d;->a(Lcom/google/android/gms/common/c;)V

    return-void
.end method

.method protected varargs constructor <init>(Landroid/content/Context;Lcom/google/android/gms/common/b;Lcom/google/android/gms/common/c;[Ljava/lang/String;)V
    .locals 6

    invoke-virtual {p1}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    new-instance v3, Lcom/google/android/gms/common/internal/g;

    invoke-direct {v3, p2}, Lcom/google/android/gms/common/internal/g;-><init>(Lcom/google/android/gms/common/b;)V

    new-instance v4, Lcom/google/android/gms/common/internal/j;

    invoke-direct {v4, p3}, Lcom/google/android/gms/common/internal/j;-><init>(Lcom/google/android/gms/common/c;)V

    move-object v0, p0

    move-object v1, p1

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/common/internal/d;-><init>(Landroid/content/Context;Landroid/os/Looper;Lcom/google/android/gms/common/api/k;Lcom/google/android/gms/common/c;[Ljava/lang/String;)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/common/internal/d;Landroid/os/IInterface;)Landroid/os/IInterface;
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/common/internal/d;->d:Landroid/os/IInterface;

    return-object p1
.end method

.method static synthetic a(Lcom/google/android/gms/common/internal/d;Lcom/google/android/gms/common/internal/i;)Lcom/google/android/gms/common/internal/i;
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/common/internal/d;->f:Lcom/google/android/gms/common/internal/i;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/gms/common/internal/d;)Lcom/google/android/gms/common/internal/l;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/common/internal/d;->i:Lcom/google/android/gms/common/internal/l;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/gms/common/internal/d;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/gms/common/internal/d;->b(I)V

    return-void
.end method

.method static synthetic b(Lcom/google/android/gms/common/internal/d;)Ljava/util/ArrayList;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/common/internal/d;->e:Ljava/util/ArrayList;

    return-object v0
.end method

.method private b(I)V
    .locals 1

    iget v0, p0, Lcom/google/android/gms/common/internal/d;->g:I

    iput p1, p0, Lcom/google/android/gms/common/internal/d;->g:I

    if-eq v0, p1, :cond_0

    const/4 v0, 0x3

    if-ne p1, v0, :cond_0

    :cond_0
    return-void
.end method

.method static synthetic c(Lcom/google/android/gms/common/internal/d;)Landroid/os/IInterface;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/common/internal/d;->d:Landroid/os/IInterface;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/gms/common/internal/d;)Lcom/google/android/gms/common/internal/i;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/common/internal/d;->f:Lcom/google/android/gms/common/internal/i;

    return-object v0
.end method

.method static synthetic e(Lcom/google/android/gms/common/internal/d;)Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/common/internal/d;->b:Landroid/content/Context;

    return-object v0
.end method


# virtual methods
.method protected abstract a(Landroid/os/IBinder;)Landroid/os/IInterface;
.end method

.method public final a()V
    .locals 4

    const/4 v3, 0x3

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/gms/common/internal/d;->h:Z

    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/google/android/gms/common/internal/d;->b(I)V

    iget-object v0, p0, Lcom/google/android/gms/common/internal/d;->b:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/common/f;->a(Landroid/content/Context;)I

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0, v1}, Lcom/google/android/gms/common/internal/d;->b(I)V

    iget-object v1, p0, Lcom/google/android/gms/common/internal/d;->a:Landroid/os/Handler;

    iget-object v2, p0, Lcom/google/android/gms/common/internal/d;->a:Landroid/os/Handler;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v2, v3, v0}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/common/internal/d;->f:Lcom/google/android/gms/common/internal/i;

    if-eqz v0, :cond_2

    const-string/jumbo v0, "GmsClient"

    const-string/jumbo v1, "Calling connect() while still connected, missing disconnect()."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/common/internal/d;->d:Landroid/os/IInterface;

    iget-object v0, p0, Lcom/google/android/gms/common/internal/d;->b:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/common/internal/r;->a(Landroid/content/Context;)Lcom/google/android/gms/common/internal/r;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/gms/common/internal/d;->e()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/common/internal/d;->f:Lcom/google/android/gms/common/internal/i;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/common/internal/r;->b(Ljava/lang/String;Lcom/google/android/gms/common/internal/i;)V

    :cond_2
    new-instance v0, Lcom/google/android/gms/common/internal/i;

    invoke-direct {v0, p0}, Lcom/google/android/gms/common/internal/i;-><init>(Lcom/google/android/gms/common/internal/d;)V

    iput-object v0, p0, Lcom/google/android/gms/common/internal/d;->f:Lcom/google/android/gms/common/internal/i;

    iget-object v0, p0, Lcom/google/android/gms/common/internal/d;->b:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/common/internal/r;->a(Landroid/content/Context;)Lcom/google/android/gms/common/internal/r;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/gms/common/internal/d;->e()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/common/internal/d;->f:Lcom/google/android/gms/common/internal/i;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/common/internal/r;->a(Ljava/lang/String;Lcom/google/android/gms/common/internal/i;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string/jumbo v0, "GmsClient"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "unable to connect to service: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/gms/common/internal/d;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/google/android/gms/common/internal/d;->a:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/gms/common/internal/d;->a:Landroid/os/Handler;

    const/16 v2, 0x9

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v3, v2}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method public final a(I)V
    .locals 4

    iget-object v0, p0, Lcom/google/android/gms/common/internal/d;->a:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/gms/common/internal/d;->a:Landroid/os/Handler;

    const/4 v2, 0x4

    const/4 v3, 0x2

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    return-void
.end method

.method protected a(ILandroid/os/IBinder;Landroid/os/Bundle;)V
    .locals 4

    iget-object v0, p0, Lcom/google/android/gms/common/internal/d;->a:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/gms/common/internal/d;->a:Landroid/os/Handler;

    const/4 v2, 0x1

    new-instance v3, Lcom/google/android/gms/common/internal/k;

    invoke-direct {v3, p0, p1, p2, p3}, Lcom/google/android/gms/common/internal/k;-><init>(Lcom/google/android/gms/common/internal/d;ILandroid/os/IBinder;Landroid/os/Bundle;)V

    invoke-virtual {v1, v2, v3}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    return-void
.end method

.method public final a(Lcom/google/android/gms/common/b;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/common/internal/d;->i:Lcom/google/android/gms/common/internal/l;

    new-instance v1, Lcom/google/android/gms/common/internal/g;

    invoke-direct {v1, p1}, Lcom/google/android/gms/common/internal/g;-><init>(Lcom/google/android/gms/common/b;)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/internal/l;->a(Lcom/google/android/gms/common/api/k;)V

    return-void
.end method

.method public final a(Lcom/google/android/gms/common/c;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/common/internal/d;->i:Lcom/google/android/gms/common/internal/l;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/common/internal/l;->a(Lcom/google/android/gms/common/c;)V

    return-void
.end method

.method protected abstract a(Lcom/google/android/gms/common/internal/A;Lcom/google/android/gms/common/internal/h;)V
.end method

.method public a_()Landroid/os/Bundle;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public b()V
    .locals 5

    const/4 v4, 0x0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/common/internal/d;->h:Z

    iget-object v2, p0, Lcom/google/android/gms/common/internal/d;->e:Ljava/util/ArrayList;

    monitor-enter v2

    :try_start_0
    iget-object v1, p0, Lcom/google/android/gms/common/internal/d;->e:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v3

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/common/internal/d;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/internal/f;

    invoke-virtual {v0}, Lcom/google/android/gms/common/internal/f;->c()V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/common/internal/d;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/gms/common/internal/d;->b(I)V

    iput-object v4, p0, Lcom/google/android/gms/common/internal/d;->d:Landroid/os/IInterface;

    iget-object v0, p0, Lcom/google/android/gms/common/internal/d;->f:Lcom/google/android/gms/common/internal/i;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/common/internal/d;->b:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/common/internal/r;->a(Landroid/content/Context;)Lcom/google/android/gms/common/internal/r;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/gms/common/internal/d;->e()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/common/internal/d;->f:Lcom/google/android/gms/common/internal/i;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/common/internal/r;->b(Ljava/lang/String;Lcom/google/android/gms/common/internal/i;)V

    iput-object v4, p0, Lcom/google/android/gms/common/internal/d;->f:Lcom/google/android/gms/common/internal/i;

    :cond_1
    return-void

    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0
.end method

.method protected final b(Landroid/os/IBinder;)V
    .locals 2

    :try_start_0
    invoke-static {p1}, Lcom/google/android/gms/common/internal/B;->a(Landroid/os/IBinder;)Lcom/google/android/gms/common/internal/A;

    move-result-object v0

    new-instance v1, Lcom/google/android/gms/common/internal/h;

    invoke-direct {v1, p0}, Lcom/google/android/gms/common/internal/h;-><init>(Lcom/google/android/gms/common/internal/d;)V

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/common/internal/d;->a(Lcom/google/android/gms/common/internal/A;Lcom/google/android/gms/common/internal/h;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string/jumbo v0, "GmsClient"

    const-string/jumbo v1, "service died"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public final b(Lcom/google/android/gms/common/c;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/common/internal/d;->i:Lcom/google/android/gms/common/internal/l;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/common/internal/l;->a(Lcom/google/android/gms/common/c;)V

    return-void
.end method

.method public final b(Lcom/google/android/gms/common/b;)Z
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/common/internal/d;->i:Lcom/google/android/gms/common/internal/l;

    new-instance v1, Lcom/google/android/gms/common/internal/g;

    invoke-direct {v1, p1}, Lcom/google/android/gms/common/internal/g;-><init>(Lcom/google/android/gms/common/b;)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/internal/l;->b(Lcom/google/android/gms/common/api/k;)Z

    move-result v0

    return v0
.end method

.method public final b_()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/common/internal/d;->h:Z

    return v0
.end method

.method public final c(Lcom/google/android/gms/common/b;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/common/internal/d;->i:Lcom/google/android/gms/common/internal/l;

    new-instance v1, Lcom/google/android/gms/common/internal/g;

    invoke-direct {v1, p1}, Lcom/google/android/gms/common/internal/g;-><init>(Lcom/google/android/gms/common/b;)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/internal/l;->c(Lcom/google/android/gms/common/api/k;)V

    return-void
.end method

.method public final c()Z
    .locals 2

    iget v0, p0, Lcom/google/android/gms/common/internal/d;->g:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c(Lcom/google/android/gms/common/c;)Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/common/internal/d;->i:Lcom/google/android/gms/common/internal/l;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/common/internal/l;->b(Lcom/google/android/gms/common/c;)Z

    move-result v0

    return v0
.end method

.method public final d()Landroid/os/Looper;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/common/internal/d;->c:Landroid/os/Looper;

    return-object v0
.end method

.method public final d(Lcom/google/android/gms/common/c;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/common/internal/d;->i:Lcom/google/android/gms/common/internal/l;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/common/internal/l;->c(Lcom/google/android/gms/common/c;)V

    return-void
.end method

.method protected abstract e()Ljava/lang/String;
.end method

.method protected abstract f()Ljava/lang/String;
.end method

.method public final g()Z
    .locals 2

    iget v0, p0, Lcom/google/android/gms/common/internal/d;->g:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final h()Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/common/internal/d;->b:Landroid/content/Context;

    return-object v0
.end method

.method public final i()Landroid/os/IInterface;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/common/internal/d;->c()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "Not connected. Call connect() and wait for onConnected() to be called."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/common/internal/d;->d:Landroid/os/IInterface;

    return-object v0
.end method
