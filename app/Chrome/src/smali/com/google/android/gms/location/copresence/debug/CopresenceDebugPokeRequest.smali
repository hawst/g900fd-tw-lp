.class public Lcom/google/android/gms/location/copresence/debug/CopresenceDebugPokeRequest;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Lcom/google/android/gms/location/copresence/debug/a;


# instance fields
.field private final a:I

.field private b:I

.field private c:[B

.field private d:Lcom/google/android/gms/internal/aK;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/gms/location/copresence/debug/a;

    invoke-direct {v0}, Lcom/google/android/gms/location/copresence/debug/a;-><init>()V

    sput-object v0, Lcom/google/android/gms/location/copresence/debug/CopresenceDebugPokeRequest;->CREATOR:Lcom/google/android/gms/location/copresence/debug/a;

    return-void
.end method

.method constructor <init>(II[BLandroid/os/IBinder;)V
    .locals 1

    if-nez p4, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-direct {p0, p1, p2, p3, v0}, Lcom/google/android/gms/location/copresence/debug/CopresenceDebugPokeRequest;-><init>(II[BLcom/google/android/gms/internal/aK;)V

    return-void

    :cond_0
    invoke-static {p4}, Lcom/google/android/gms/internal/aL;->a(Landroid/os/IBinder;)Lcom/google/android/gms/internal/aK;

    move-result-object v0

    goto :goto_0
.end method

.method private constructor <init>(II[BLcom/google/android/gms/internal/aK;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/gms/location/copresence/debug/CopresenceDebugPokeRequest;->a:I

    iput p2, p0, Lcom/google/android/gms/location/copresence/debug/CopresenceDebugPokeRequest;->b:I

    iput-object p3, p0, Lcom/google/android/gms/location/copresence/debug/CopresenceDebugPokeRequest;->c:[B

    iput-object p4, p0, Lcom/google/android/gms/location/copresence/debug/CopresenceDebugPokeRequest;->d:Lcom/google/android/gms/internal/aK;

    return-void
.end method


# virtual methods
.method final a()I
    .locals 1

    iget v0, p0, Lcom/google/android/gms/location/copresence/debug/CopresenceDebugPokeRequest;->a:I

    return v0
.end method

.method public final b()I
    .locals 1

    iget v0, p0, Lcom/google/android/gms/location/copresence/debug/CopresenceDebugPokeRequest;->b:I

    return v0
.end method

.method public final c()[B
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/location/copresence/debug/CopresenceDebugPokeRequest;->c:[B

    return-object v0
.end method

.method final d()Landroid/os/IBinder;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/location/copresence/debug/CopresenceDebugPokeRequest;->d:Lcom/google/android/gms/internal/aK;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/location/copresence/debug/CopresenceDebugPokeRequest;->d:Lcom/google/android/gms/internal/aK;

    invoke-interface {v0}, Lcom/google/android/gms/internal/aK;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    goto :goto_0
.end method

.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    invoke-static {p0, p1}, Lcom/google/android/gms/location/copresence/debug/a;->a(Lcom/google/android/gms/location/copresence/debug/CopresenceDebugPokeRequest;Landroid/os/Parcel;)V

    return-void
.end method
