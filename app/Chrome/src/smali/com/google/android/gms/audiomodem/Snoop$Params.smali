.class public Lcom/google/android/gms/audiomodem/Snoop$Params;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private final a:I

.field private final b:[Lcom/google/android/gms/audiomodem/Encoding;

.field private final c:Z

.field private final d:Z

.field private final e:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/gms/audiomodem/e;

    invoke-direct {v0}, Lcom/google/android/gms/audiomodem/e;-><init>()V

    sput-object v0, Lcom/google/android/gms/audiomodem/Snoop$Params;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>(I[Lcom/google/android/gms/audiomodem/Encoding;ZZJ)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/gms/audiomodem/Snoop$Params;->a:I

    iput-object p2, p0, Lcom/google/android/gms/audiomodem/Snoop$Params;->b:[Lcom/google/android/gms/audiomodem/Encoding;

    iput-boolean p3, p0, Lcom/google/android/gms/audiomodem/Snoop$Params;->c:Z

    iput-boolean p4, p0, Lcom/google/android/gms/audiomodem/Snoop$Params;->d:Z

    iput-wide p5, p0, Lcom/google/android/gms/audiomodem/Snoop$Params;->e:J

    return-void
.end method


# virtual methods
.method final a()I
    .locals 1

    iget v0, p0, Lcom/google/android/gms/audiomodem/Snoop$Params;->a:I

    return v0
.end method

.method public final b()[Lcom/google/android/gms/audiomodem/Encoding;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/audiomodem/Snoop$Params;->b:[Lcom/google/android/gms/audiomodem/Encoding;

    return-object v0
.end method

.method public final c()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/audiomodem/Snoop$Params;->c:Z

    return v0
.end method

.method public final d()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/audiomodem/Snoop$Params;->d:Z

    return v0
.end method

.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final e()J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/gms/audiomodem/Snoop$Params;->e:J

    return-wide v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    invoke-static {p0, p1, p2}, Lcom/google/android/gms/audiomodem/e;->a(Lcom/google/android/gms/audiomodem/Snoop$Params;Landroid/os/Parcel;I)V

    return-void
.end method
