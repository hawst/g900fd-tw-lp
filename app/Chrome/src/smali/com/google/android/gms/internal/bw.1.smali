.class public abstract Lcom/google/android/gms/internal/bw;
.super Landroid/os/Binder;

# interfaces
.implements Lcom/google/android/gms/internal/bv;


# direct methods
.method public static a(Landroid/os/IBinder;)Lcom/google/android/gms/internal/bv;
    .locals 2

    if-nez p0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    const-string/jumbo v0, "com.google.android.gms.search.queries.internal.ISearchQueriesService"

    invoke-interface {p0, v0}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v0

    if-eqz v0, :cond_1

    instance-of v1, v0, Lcom/google/android/gms/internal/bv;

    if-eqz v1, :cond_1

    check-cast v0, Lcom/google/android/gms/internal/bv;

    goto :goto_0

    :cond_1
    new-instance v0, Lcom/google/android/gms/internal/bx;

    invoke-direct {v0, p0}, Lcom/google/android/gms/internal/bx;-><init>(Landroid/os/IBinder;)V

    goto :goto_0
.end method


# virtual methods
.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .locals 3

    const/4 v0, 0x0

    const/4 v1, 0x1

    sparse-switch p1, :sswitch_data_0

    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    move-result v0

    :goto_0
    return v0

    :sswitch_0
    const-string/jumbo v0, "com.google.android.gms.search.queries.internal.ISearchQueriesService"

    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    move v0, v1

    goto :goto_0

    :sswitch_1
    const-string/jumbo v2, "com.google.android.gms.search.queries.internal.ISearchQueriesService"

    invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_0

    sget-object v0, Lcom/google/android/gms/search/queries/QueryCall$b;->CREATOR:Lcom/google/android/gms/search/queries/h;

    invoke-static {p2}, Lcom/google/android/gms/search/queries/h;->a(Landroid/os/Parcel;)Lcom/google/android/gms/search/queries/QueryCall$b;

    move-result-object v0

    :cond_0
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/gms/internal/bt;->a(Landroid/os/IBinder;)Lcom/google/android/gms/internal/bs;

    move-result-object v2

    invoke-virtual {p0, v0, v2}, Lcom/google/android/gms/internal/bw;->a(Lcom/google/android/gms/search/queries/QueryCall$b;Lcom/google/android/gms/internal/bs;)V

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    move v0, v1

    goto :goto_0

    :sswitch_2
    const-string/jumbo v2, "com.google.android.gms.search.queries.internal.ISearchQueriesService"

    invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_1

    sget-object v0, Lcom/google/android/gms/search/queries/GlobalQueryCall$b;->CREATOR:Lcom/google/android/gms/search/queries/f;

    invoke-static {p2}, Lcom/google/android/gms/search/queries/f;->a(Landroid/os/Parcel;)Lcom/google/android/gms/search/queries/GlobalQueryCall$b;

    move-result-object v0

    :cond_1
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/gms/internal/bt;->a(Landroid/os/IBinder;)Lcom/google/android/gms/internal/bs;

    move-result-object v2

    invoke-virtual {p0, v0, v2}, Lcom/google/android/gms/internal/bw;->a(Lcom/google/android/gms/search/queries/GlobalQueryCall$b;Lcom/google/android/gms/internal/bs;)V

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    move v0, v1

    goto :goto_0

    :sswitch_3
    const-string/jumbo v2, "com.google.android.gms.search.queries.internal.ISearchQueriesService"

    invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_2

    sget-object v0, Lcom/google/android/gms/search/queries/GetDocumentsCall$b;->CREATOR:Lcom/google/android/gms/search/queries/b;

    invoke-static {p2}, Lcom/google/android/gms/search/queries/b;->a(Landroid/os/Parcel;)Lcom/google/android/gms/search/queries/GetDocumentsCall$b;

    move-result-object v0

    :cond_2
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/gms/internal/bt;->a(Landroid/os/IBinder;)Lcom/google/android/gms/internal/bs;

    move-result-object v2

    invoke-virtual {p0, v0, v2}, Lcom/google/android/gms/internal/bw;->a(Lcom/google/android/gms/search/queries/GetDocumentsCall$b;Lcom/google/android/gms/internal/bs;)V

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    move v0, v1

    goto :goto_0

    :sswitch_4
    const-string/jumbo v2, "com.google.android.gms.search.queries.internal.ISearchQueriesService"

    invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_3

    sget-object v0, Lcom/google/android/gms/search/queries/GetPhraseAffinityCall$b;->CREATOR:Lcom/google/android/gms/search/queries/d;

    invoke-static {p2}, Lcom/google/android/gms/search/queries/d;->a(Landroid/os/Parcel;)Lcom/google/android/gms/search/queries/GetPhraseAffinityCall$b;

    move-result-object v0

    :cond_3
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/gms/internal/bt;->a(Landroid/os/IBinder;)Lcom/google/android/gms/internal/bs;

    move-result-object v2

    invoke-virtual {p0, v0, v2}, Lcom/google/android/gms/internal/bw;->a(Lcom/google/android/gms/search/queries/GetPhraseAffinityCall$b;Lcom/google/android/gms/internal/bs;)V

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    move v0, v1

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x2 -> :sswitch_1
        0x3 -> :sswitch_2
        0x4 -> :sswitch_3
        0x5 -> :sswitch_4
        0x5f4e5446 -> :sswitch_0
    .end sparse-switch
.end method
