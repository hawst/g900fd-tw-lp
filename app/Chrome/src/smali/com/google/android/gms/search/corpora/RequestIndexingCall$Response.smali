.class public Lcom/google/android/gms/search/corpora/RequestIndexingCall$Response;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/common/api/n;
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Lcom/google/android/gms/search/corpora/l;


# instance fields
.field public a:Lcom/google/android/gms/common/api/Status;

.field public b:Z

.field final c:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/gms/search/corpora/l;

    invoke-direct {v0}, Lcom/google/android/gms/search/corpora/l;-><init>()V

    sput-object v0, Lcom/google/android/gms/search/corpora/RequestIndexingCall$Response;->CREATOR:Lcom/google/android/gms/search/corpora/l;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/search/corpora/RequestIndexingCall$Response;->c:I

    return-void
.end method

.method constructor <init>(ILcom/google/android/gms/common/api/Status;Z)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/gms/search/corpora/RequestIndexingCall$Response;->c:I

    iput-object p2, p0, Lcom/google/android/gms/search/corpora/RequestIndexingCall$Response;->a:Lcom/google/android/gms/common/api/Status;

    iput-boolean p3, p0, Lcom/google/android/gms/search/corpora/RequestIndexingCall$Response;->b:Z

    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/gms/common/api/Status;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/search/corpora/RequestIndexingCall$Response;->a:Lcom/google/android/gms/common/api/Status;

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    sget-object v0, Lcom/google/android/gms/search/corpora/RequestIndexingCall$Response;->CREATOR:Lcom/google/android/gms/search/corpora/l;

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    sget-object v0, Lcom/google/android/gms/search/corpora/RequestIndexingCall$Response;->CREATOR:Lcom/google/android/gms/search/corpora/l;

    invoke-static {p0, p1, p2}, Lcom/google/android/gms/search/corpora/l;->a(Lcom/google/android/gms/search/corpora/RequestIndexingCall$Response;Landroid/os/Parcel;I)V

    return-void
.end method
