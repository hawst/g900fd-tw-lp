.class public final Lcom/google/android/gms/internal/aa;
.super Lcom/google/android/gms/common/internal/d;


# instance fields
.field private final b:Ljava/lang/String;

.field private final c:Lcom/google/android/gms/internal/Y;

.field private final d:Lcom/google/android/gms/internal/d;

.field private final e:Ljava/lang/Object;

.field private f:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/internal/Y;)V
    .locals 1

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/String;

    invoke-direct {p0, p1, p2, p2, v0}, Lcom/google/android/gms/common/internal/d;-><init>(Landroid/content/Context;Lcom/google/android/gms/common/b;Lcom/google/android/gms/common/c;[Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/internal/aa;->b:Ljava/lang/String;

    invoke-static {p2}, Landroid/support/v4/app/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/internal/Y;

    iput-object v0, p0, Lcom/google/android/gms/internal/aa;->c:Lcom/google/android/gms/internal/Y;

    iget-object v0, p0, Lcom/google/android/gms/internal/aa;->c:Lcom/google/android/gms/internal/Y;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/internal/Y;->a(Lcom/google/android/gms/internal/aa;)V

    new-instance v0, Lcom/google/android/gms/internal/d;

    invoke-direct {v0}, Lcom/google/android/gms/internal/d;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/internal/aa;->d:Lcom/google/android/gms/internal/d;

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/internal/aa;->e:Ljava/lang/Object;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/internal/aa;->f:Z

    return-void
.end method

.method private b(Lcom/google/android/gms/internal/qu;Lcom/google/android/gms/internal/qq;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/internal/aa;->d:Lcom/google/android/gms/internal/d;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/internal/d;->a(Lcom/google/android/gms/internal/qu;Lcom/google/android/gms/internal/qq;)V

    return-void
.end method

.method private l()V
    .locals 6

    iget-boolean v0, p0, Lcom/google/android/gms/internal/aa;->f:Z

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Landroid/support/v4/app/b;->a(Z)V

    iget-object v0, p0, Lcom/google/android/gms/internal/aa;->d:Lcom/google/android/gms/internal/d;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/d;->c()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :try_start_0
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iget-object v1, p0, Lcom/google/android/gms/internal/aa;->d:Lcom/google/android/gms/internal/d;

    invoke-virtual {v1}, Lcom/google/android/gms/internal/d;->a()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move-object v2, v0

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/internal/W;

    iget-object v1, v0, Lcom/google/android/gms/internal/W;->a:Lcom/google/android/gms/internal/qu;

    invoke-virtual {v1, v2}, Lcom/google/android/gms/internal/qu;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v0, v0, Lcom/google/android/gms/internal/W;->b:Lcom/google/android/gms/internal/qq;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v0

    const-string/jumbo v0, "PlayLoggerImpl"

    const-string/jumbo v1, "Couldn\'t send cached log events to AndroidLog service.  Retaining in memory cache."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_2
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    :try_start_1
    invoke-virtual {v3}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_3

    invoke-virtual {p0}, Lcom/google/android/gms/internal/aa;->i()Landroid/os/IInterface;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/internal/T;

    iget-object v5, p0, Lcom/google/android/gms/internal/aa;->b:Ljava/lang/String;

    invoke-interface {v1, v5, v2, v3}, Lcom/google/android/gms/internal/T;->a(Ljava/lang/String;Lcom/google/android/gms/internal/qu;Ljava/util/List;)V

    invoke-virtual {v3}, Ljava/util/ArrayList;->clear()V

    :cond_3
    iget-object v1, v0, Lcom/google/android/gms/internal/W;->a:Lcom/google/android/gms/internal/qu;

    iget-object v0, v0, Lcom/google/android/gms/internal/W;->b:Lcom/google/android/gms/internal/qq;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move-object v2, v1

    goto :goto_1

    :cond_4
    invoke-virtual {v3}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_5

    invoke-virtual {p0}, Lcom/google/android/gms/internal/aa;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/internal/T;

    iget-object v1, p0, Lcom/google/android/gms/internal/aa;->b:Ljava/lang/String;

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/gms/internal/T;->a(Ljava/lang/String;Lcom/google/android/gms/internal/qu;Ljava/util/List;)V

    :cond_5
    iget-object v0, p0, Lcom/google/android/gms/internal/aa;->d:Lcom/google/android/gms/internal/d;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/d;->b()V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_2
.end method


# virtual methods
.method protected final synthetic a(Landroid/os/IBinder;)Landroid/os/IInterface;
    .locals 1

    invoke-static {p1}, Lcom/google/android/gms/internal/U;->a(Landroid/os/IBinder;)Lcom/google/android/gms/internal/T;

    move-result-object v0

    return-object v0
.end method

.method protected final a(Lcom/google/android/gms/common/internal/A;Lcom/google/android/gms/common/internal/h;)V
    .locals 3

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const v1, 0x5d2f78

    invoke-virtual {p0}, Lcom/google/android/gms/internal/aa;->h()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1, p2, v1, v2, v0}, Lcom/google/android/gms/common/internal/A;->f(Lcom/google/android/gms/common/internal/x;ILjava/lang/String;Landroid/os/Bundle;)V

    return-void
.end method

.method public final a(Lcom/google/android/gms/internal/qu;Lcom/google/android/gms/internal/qq;)V
    .locals 3

    iget-object v1, p0, Lcom/google/android/gms/internal/aa;->e:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/gms/internal/aa;->f:Z

    if-eqz v0, :cond_0

    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/internal/aa;->b(Lcom/google/android/gms/internal/qu;Lcom/google/android/gms/internal/qq;)V

    :goto_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :cond_0
    :try_start_1
    invoke-direct {p0}, Lcom/google/android/gms/internal/aa;->l()V

    invoke-virtual {p0}, Lcom/google/android/gms/internal/aa;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/internal/T;

    iget-object v2, p0, Lcom/google/android/gms/internal/aa;->b:Ljava/lang/String;

    invoke-interface {v0, v2, p1, p2}, Lcom/google/android/gms/internal/T;->a(Ljava/lang/String;Lcom/google/android/gms/internal/qu;Lcom/google/android/gms/internal/qq;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_2
    const-string/jumbo v0, "PlayLoggerImpl"

    const-string/jumbo v2, "Couldn\'t send log event.  Will try caching."

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/internal/aa;->b(Lcom/google/android/gms/internal/qu;Lcom/google/android/gms/internal/qq;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :catch_1
    move-exception v0

    :try_start_3
    const-string/jumbo v0, "PlayLoggerImpl"

    const-string/jumbo v2, "Service was disconnected.  Will try caching."

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/internal/aa;->b(Lcom/google/android/gms/internal/qu;Lcom/google/android/gms/internal/qq;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0
.end method

.method final a(Z)V
    .locals 2

    iget-object v1, p0, Lcom/google/android/gms/internal/aa;->e:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/gms/internal/aa;->f:Z

    iput-boolean p1, p0, Lcom/google/android/gms/internal/aa;->f:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/gms/internal/aa;->f:Z

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/gms/internal/aa;->l()V

    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method protected final e()Ljava/lang/String;
    .locals 1

    const-string/jumbo v0, "com.google.android.gms.playlog.service.START"

    return-object v0
.end method

.method protected final f()Ljava/lang/String;
    .locals 1

    const-string/jumbo v0, "com.google.android.gms.playlog.internal.IPlayLogService"

    return-object v0
.end method

.method public final j()V
    .locals 3

    iget-object v1, p0, Lcom/google/android/gms/internal/aa;->e:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/internal/aa;->g()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/internal/aa;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    monitor-exit v1

    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/internal/aa;->c:Lcom/google/android/gms/internal/Y;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lcom/google/android/gms/internal/Y;->a(Z)V

    invoke-virtual {p0}, Lcom/google/android/gms/internal/aa;->a()V

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final k()V
    .locals 3

    iget-object v1, p0, Lcom/google/android/gms/internal/aa;->e:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/internal/aa;->c:Lcom/google/android/gms/internal/Y;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcom/google/android/gms/internal/Y;->a(Z)V

    invoke-virtual {p0}, Lcom/google/android/gms/internal/aa;->b()V

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
