.class public final Lcom/google/android/gms/cast/u;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/cast/k;


# instance fields
.field private final a:Ljava/lang/Object;

.field private final b:Lcom/google/android/gms/internal/r;

.field private final c:Lcom/google/android/gms/cast/A;

.field private d:Lcom/google/android/gms/cast/z;


# direct methods
.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/cast/u;->a:Ljava/lang/Object;

    new-instance v0, Lcom/google/android/gms/cast/A;

    invoke-direct {v0, p0}, Lcom/google/android/gms/cast/A;-><init>(Lcom/google/android/gms/cast/u;)V

    iput-object v0, p0, Lcom/google/android/gms/cast/u;->c:Lcom/google/android/gms/cast/A;

    new-instance v0, Lcom/google/android/gms/cast/v;

    invoke-direct {v0, p0}, Lcom/google/android/gms/cast/v;-><init>(Lcom/google/android/gms/cast/u;)V

    iput-object v0, p0, Lcom/google/android/gms/cast/u;->b:Lcom/google/android/gms/internal/r;

    iget-object v0, p0, Lcom/google/android/gms/cast/u;->b:Lcom/google/android/gms/internal/r;

    iget-object v1, p0, Lcom/google/android/gms/cast/u;->c:Lcom/google/android/gms/cast/A;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/internal/r;->a(Lcom/google/android/gms/internal/t;)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/cast/u;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/cast/u;->d:Lcom/google/android/gms/cast/z;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/cast/u;->d:Lcom/google/android/gms/cast/z;

    invoke-interface {v0}, Lcom/google/android/gms/cast/z;->onStatusUpdated()V

    :cond_0
    return-void
.end method

.method static synthetic b(Lcom/google/android/gms/cast/u;)Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/cast/u;->a:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/gms/cast/u;)Lcom/google/android/gms/cast/A;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/cast/u;->c:Lcom/google/android/gms/cast/A;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/gms/cast/u;)Lcom/google/android/gms/internal/r;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/cast/u;->b:Lcom/google/android/gms/internal/r;

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/android/gms/cast/s;
    .locals 2

    iget-object v1, p0, Lcom/google/android/gms/cast/u;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/cast/u;->b:Lcom/google/android/gms/internal/r;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/r;->f()Lcom/google/android/gms/cast/s;

    move-result-object v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final a(Lcom/google/android/gms/common/api/i;)Lcom/google/android/gms/common/api/l;
    .locals 1

    new-instance v0, Lcom/google/android/gms/cast/w;

    invoke-direct {v0, p0, p1}, Lcom/google/android/gms/cast/w;-><init>(Lcom/google/android/gms/cast/u;Lcom/google/android/gms/common/api/i;)V

    invoke-interface {p1, v0}, Lcom/google/android/gms/common/api/i;->b(Lcom/google/android/gms/common/api/g;)Lcom/google/android/gms/common/api/g;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/common/api/i;Lorg/json/JSONObject;)Lcom/google/android/gms/common/api/l;
    .locals 2

    new-instance v0, Lcom/google/android/gms/cast/x;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p1, v1}, Lcom/google/android/gms/cast/x;-><init>(Lcom/google/android/gms/cast/u;Lcom/google/android/gms/common/api/i;Lorg/json/JSONObject;)V

    invoke-interface {p1, v0}, Lcom/google/android/gms/common/api/i;->b(Lcom/google/android/gms/common/api/g;)Lcom/google/android/gms/common/api/g;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/cast/z;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/cast/u;->d:Lcom/google/android/gms/cast/z;

    return-void
.end method

.method public final b()Lcom/google/android/gms/cast/p;
    .locals 2

    iget-object v1, p0, Lcom/google/android/gms/cast/u;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/cast/u;->b:Lcom/google/android/gms/internal/r;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/r;->g()Lcom/google/android/gms/cast/p;

    move-result-object v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final b(Lcom/google/android/gms/common/api/i;Lorg/json/JSONObject;)Lcom/google/android/gms/common/api/l;
    .locals 2

    new-instance v0, Lcom/google/android/gms/cast/y;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p1, v1}, Lcom/google/android/gms/cast/y;-><init>(Lcom/google/android/gms/cast/u;Lcom/google/android/gms/common/api/i;Lorg/json/JSONObject;)V

    invoke-interface {p1, v0}, Lcom/google/android/gms/common/api/i;->b(Lcom/google/android/gms/common/api/g;)Lcom/google/android/gms/common/api/g;

    move-result-object v0

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/cast/u;->b:Lcom/google/android/gms/internal/r;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/r;->c()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final onMessageReceived(Lcom/google/android/gms/cast/CastDevice;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/cast/u;->b:Lcom/google/android/gms/internal/r;

    invoke-virtual {v0, p3}, Lcom/google/android/gms/internal/r;->a(Ljava/lang/String;)V

    return-void
.end method
