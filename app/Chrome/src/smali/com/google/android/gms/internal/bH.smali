.class public Lcom/google/android/gms/internal/bH;
.super Lcom/google/android/gms/common/data/f;


# virtual methods
.method protected final b(Ljava/lang/String;)J
    .locals 2

    invoke-super {p0, p1}, Lcom/google/android/gms/common/data/f;->b(Ljava/lang/String;)J

    move-result-wide v0

    return-wide v0
.end method

.method protected final c(Ljava/lang/String;)I
    .locals 1

    invoke-super {p0, p1}, Lcom/google/android/gms/common/data/f;->c(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method protected final d(Ljava/lang/String;)Z
    .locals 1

    invoke-super {p0, p1}, Lcom/google/android/gms/common/data/f;->d(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method protected final f(Ljava/lang/String;)F
    .locals 1

    invoke-super {p0, p1}, Lcom/google/android/gms/common/data/f;->f(Ljava/lang/String;)F

    move-result v0

    return v0
.end method

.method protected final g(Ljava/lang/String;)D
    .locals 2

    invoke-super {p0, p1}, Lcom/google/android/gms/common/data/f;->g(Ljava/lang/String;)D

    move-result-wide v0

    return-wide v0
.end method

.method protected final k(Ljava/lang/String;)Ljava/lang/Long;
    .locals 2

    invoke-virtual {p0, p1}, Lcom/google/android/gms/internal/bH;->j(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0, p1}, Lcom/google/android/gms/common/data/f;->b(Ljava/lang/String;)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    goto :goto_0
.end method

.method protected final l(Ljava/lang/String;)Ljava/lang/Integer;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/google/android/gms/internal/bH;->j(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0, p1}, Lcom/google/android/gms/common/data/f;->c(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0
.end method

.method protected final m(Ljava/lang/String;)Ljava/lang/Boolean;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/google/android/gms/internal/bH;->j(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0, p1}, Lcom/google/android/gms/common/data/f;->d(Ljava/lang/String;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0
.end method

.method protected final n(Ljava/lang/String;)Ljava/lang/Double;
    .locals 2

    invoke-virtual {p0, p1}, Lcom/google/android/gms/internal/bH;->j(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0, p1}, Lcom/google/android/gms/common/data/f;->g(Ljava/lang/String;)D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    goto :goto_0
.end method
