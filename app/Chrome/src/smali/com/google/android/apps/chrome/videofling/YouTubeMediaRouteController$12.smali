.class Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController$12;
.super Ljava/lang/Object;
.source "YouTubeMediaRouteController.java"

# interfaces
.implements Lcom/google/android/gms/cast/z;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;

.field final synthetic val$remoteMediaPlayer:Lcom/google/android/gms/cast/u;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;Lcom/google/android/gms/cast/u;)V
    .locals 0

    .prologue
    .line 831
    iput-object p1, p0, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController$12;->this$0:Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;

    iput-object p2, p0, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController$12;->val$remoteMediaPlayer:Lcom/google/android/gms/cast/u;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onStatusUpdated()V
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 834
    iget-object v1, p0, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController$12;->this$0:Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;

    iget-object v2, p0, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController$12;->val$remoteMediaPlayer:Lcom/google/android/gms/cast/u;

    # setter for: Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->mConnectedRemoteMediaPlayer:Lcom/google/android/gms/cast/u;
    invoke-static {v1, v2}, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->access$1902(Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;Lcom/google/android/gms/cast/u;)Lcom/google/android/gms/cast/u;

    .line 835
    iget-object v1, p0, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController$12;->this$0:Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;

    # getter for: Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->mConnectedRemoteMediaPlayer:Lcom/google/android/gms/cast/u;
    invoke-static {v1}, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->access$1900(Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;)Lcom/google/android/gms/cast/u;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/cast/u;->a()Lcom/google/android/gms/cast/s;

    move-result-object v2

    .line 836
    iget-object v1, p0, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController$12;->this$0:Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;

    # getter for: Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->mConnectedRemoteMediaPlayer:Lcom/google/android/gms/cast/u;
    invoke-static {v1}, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->access$1900(Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;)Lcom/google/android/gms/cast/u;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/cast/u;->b()Lcom/google/android/gms/cast/p;

    move-result-object v1

    .line 837
    iget-object v3, p0, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController$12;->this$0:Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;

    const-string/jumbo v4, "306d8b5f-b273-4c3a-815c-c96726e499d9"

    iput-object v4, v3, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->mCurrentItemId:Ljava/lang/String;

    .line 840
    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/google/android/gms/cast/p;->a()Lcom/google/android/gms/cast/q;

    move-result-object v1

    .line 842
    :goto_0
    if-eqz v1, :cond_0

    const-string/jumbo v0, "com.google.android.gms.cast.metadata.TITLE"

    invoke-virtual {v1, v0}, Lcom/google/android/gms/cast/q;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 843
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController$12;->this$0:Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;

    if-eqz v0, :cond_2

    :goto_1
    invoke-virtual {v1, v0}, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->updateTitle(Ljava/lang/String;)V

    .line 845
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController$12;->this$0:Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;

    # getter for: Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->mDebug:Z
    invoke-static {v0}, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->access$1100(Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string/jumbo v0, "YouTubeMediaRouteController"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "Received item status: "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 847
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController$12;->this$0:Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;

    iget-object v1, p0, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController$12;->this$0:Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;

    invoke-virtual {v2}, Lcom/google/android/gms/cast/s;->b()I

    move-result v3

    invoke-virtual {v2}, Lcom/google/android/gms/cast/s;->c()I

    move-result v2

    # invokes: Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->playerStateToPlaybackState(II)I
    invoke-static {v1, v3, v2}, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->access$2000(Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;II)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->updateState(I)V

    .line 849
    return-void

    .line 843
    :cond_2
    const-string/jumbo v0, "YouTube"

    goto :goto_1

    :cond_3
    move-object v1, v0

    goto :goto_0
.end method
