.class public Lcom/google/android/apps/chrome/bookmark/AndroidBrowserImporter;
.super Lcom/google/android/apps/chrome/bookmark/BookmarkImporter;
.source "AndroidBrowserImporter.java"


# instance fields
.field private mDatabaseFile:Ljava/io/File;

.field private mIgnoreAvailableProvidersForTestPurposes:Z

.field private mInputResolver:Landroid/content/ContentResolver;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 36
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/bookmark/BookmarkImporter;-><init>(Landroid/content/Context;)V

    .line 37
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/bookmark/AndroidBrowserImporter;->mInputResolver:Landroid/content/ContentResolver;

    .line 38
    const-string/jumbo v0, "android_browser.db"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getDatabasePath(Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/bookmark/AndroidBrowserImporter;->mDatabaseFile:Ljava/io/File;

    .line 39
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/chrome/bookmark/AndroidBrowserImporter;)Ljava/io/File;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lcom/google/android/apps/chrome/bookmark/AndroidBrowserImporter;->mDatabaseFile:Ljava/io/File;

    return-object v0
.end method

.method private areProvidersValid()Z
    .locals 7

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 119
    iget-boolean v2, p0, Lcom/google/android/apps/chrome/bookmark/AndroidBrowserImporter;->mIgnoreAvailableProvidersForTestPurposes:Z

    if-eqz v2, :cond_1

    .line 142
    :cond_0
    :goto_0
    return v0

    .line 124
    :cond_1
    :try_start_0
    iget-object v2, p0, Lcom/google/android/apps/chrome/bookmark/AndroidBrowserImporter;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    const-string/jumbo v3, "com.android.browser.provider"

    const/16 v4, 0x8

    invoke-virtual {v2, v3, v4}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v2

    .line 127
    if-eqz v2, :cond_0

    .line 129
    iget-object v3, v2, Landroid/content/pm/PackageInfo;->providers:[Landroid/content/pm/ProviderInfo;

    .line 131
    if-nez v3, :cond_2

    move v0, v1

    goto :goto_0

    .line 135
    :cond_2
    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_0

    aget-object v5, v3, v2

    .line 136
    if-eqz v5, :cond_3

    iget-object v6, v5, Landroid/content/pm/ProviderInfo;->authority:Ljava/lang/String;

    if-eqz v6, :cond_3

    iget-object v5, v5, Landroid/content/pm/ProviderInfo;->authority:Ljava/lang/String;

    const-string/jumbo v6, "com.android.browser;browser"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v5

    if-eqz v5, :cond_3

    move v0, v1

    .line 138
    goto :goto_0

    .line 135
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 142
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public static isDatabaseFilePresent(Landroid/content/Context;)Z
    .locals 1

    .prologue
    .line 47
    const-string/jumbo v0, "android_browser.db"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getDatabasePath(Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    return v0
.end method


# virtual methods
.method public areBookmarksAccessible()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 95
    iget-object v2, p0, Lcom/google/android/apps/chrome/bookmark/AndroidBrowserImporter;->mDatabaseFile:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 97
    :cond_0
    :goto_0
    return v0

    .line 96
    :cond_1
    invoke-direct {p0}, Lcom/google/android/apps/chrome/bookmark/AndroidBrowserImporter;->areProvidersValid()Z

    move-result v2

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    .line 97
    :cond_2
    iget-object v2, p0, Lcom/google/android/apps/chrome/bookmark/AndroidBrowserImporter;->mInputResolver:Landroid/content/ContentResolver;

    invoke-static {v2}, Lcom/google/android/apps/chrome/bookmark/AndroidBrowserPrivateProviderIterator;->isProviderAvailable(Landroid/content/ContentResolver;)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/chrome/bookmark/AndroidBrowserImporter;->mInputResolver:Landroid/content/ContentResolver;

    invoke-static {v2}, Lcom/google/android/apps/chrome/bookmark/AndroidBrowserPublicProviderIterator;->isProviderAvailable(Landroid/content/ContentResolver;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method protected availableBookmarks()[Lcom/google/android/apps/chrome/bookmark/BookmarkImporter$BookmarkIterator;
    .locals 5

    .prologue
    const/4 v0, 0x0

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 149
    iget-object v1, p0, Lcom/google/android/apps/chrome/bookmark/AndroidBrowserImporter;->mDatabaseFile:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 150
    iget-object v1, p0, Lcom/google/android/apps/chrome/bookmark/AndroidBrowserImporter;->mDatabaseFile:Ljava/io/File;

    invoke-static {v1}, Lcom/google/android/apps/chrome/bookmark/AndroidBrowserDatabaseProvider;->createIfAvailable(Ljava/io/File;)Lcom/google/android/apps/chrome/bookmark/AndroidBrowserDatabaseProvider;

    move-result-object v1

    .line 155
    if-nez v1, :cond_1

    .line 156
    const-string/jumbo v1, "AndroidBrowserImporter"

    const-string/jumbo v2, "Error accessing Android Browser database."

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 171
    :cond_0
    :goto_0
    return-object v0

    .line 160
    :cond_1
    const-string/jumbo v0, "AndroidBrowserImporter"

    const-string/jumbo v2, "Importing bookmarks from Android Browser\'s database."

    invoke-static {v0, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 161
    new-array v0, v4, [Lcom/google/android/apps/chrome/bookmark/BookmarkImporter$BookmarkIterator;

    invoke-static {v1}, Lcom/google/android/apps/chrome/bookmark/AndroidBrowserPrivateProviderIterator;->createIfAvailable(Lcom/google/android/apps/chrome/bookmark/AndroidBrowserDatabaseProvider;)Lcom/google/android/apps/chrome/bookmark/AndroidBrowserPrivateProviderIterator;

    move-result-object v1

    aput-object v1, v0, v3

    goto :goto_0

    .line 167
    :cond_2
    invoke-direct {p0}, Lcom/google/android/apps/chrome/bookmark/AndroidBrowserImporter;->areProvidersValid()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 171
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/google/android/apps/chrome/bookmark/BookmarkImporter$BookmarkIterator;

    iget-object v1, p0, Lcom/google/android/apps/chrome/bookmark/AndroidBrowserImporter;->mInputResolver:Landroid/content/ContentResolver;

    invoke-static {v1}, Lcom/google/android/apps/chrome/bookmark/AndroidBrowserPrivateProviderIterator;->createIfAvailable(Landroid/content/ContentResolver;)Lcom/google/android/apps/chrome/bookmark/AndroidBrowserPrivateProviderIterator;

    move-result-object v1

    aput-object v1, v0, v3

    iget-object v1, p0, Lcom/google/android/apps/chrome/bookmark/AndroidBrowserImporter;->mInputResolver:Landroid/content/ContentResolver;

    invoke-static {v1}, Lcom/google/android/apps/chrome/bookmark/AndroidBrowserPublicProviderIterator;->createIfAvailable(Landroid/content/ContentResolver;)Lcom/google/android/apps/chrome/bookmark/AndroidBrowserPublicProviderIterator;

    move-result-object v1

    aput-object v1, v0, v4

    goto :goto_0
.end method

.method public importFromDatabaseAfterOTA()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 60
    iget-object v1, p0, Lcom/google/android/apps/chrome/bookmark/AndroidBrowserImporter;->mContext:Landroid/content/Context;

    const-string/jumbo v2, "android_browser_temp.db"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getDatabasePath(Ljava/lang/String;)Ljava/io/File;

    move-result-object v1

    .line 61
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    move-result v2

    if-nez v2, :cond_1

    .line 62
    const-string/jumbo v1, "AndroidBrowserImporter"

    const-string/jumbo v2, "Temporary file deletion failed."

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 87
    :cond_0
    :goto_0
    return v0

    .line 65
    :cond_1
    iget-object v2, p0, Lcom/google/android/apps/chrome/bookmark/AndroidBrowserImporter;->mDatabaseFile:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 67
    iget-object v2, p0, Lcom/google/android/apps/chrome/bookmark/AndroidBrowserImporter;->mDatabaseFile:Ljava/io/File;

    invoke-virtual {v2, v1}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 68
    const-string/jumbo v1, "AndroidBrowserImporter"

    const-string/jumbo v2, "Database file renaming failed."

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 72
    :cond_2
    iput-object v1, p0, Lcom/google/android/apps/chrome/bookmark/AndroidBrowserImporter;->mDatabaseFile:Ljava/io/File;

    .line 73
    new-instance v0, Lcom/google/android/apps/chrome/bookmark/AndroidBrowserImporter$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/bookmark/AndroidBrowserImporter$1;-><init>(Lcom/google/android/apps/chrome/bookmark/AndroidBrowserImporter;)V

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/bookmark/AndroidBrowserImporter;->importBookmarks(Lcom/google/android/apps/chrome/bookmark/BookmarkImporter$OnBookmarksImportedListener;)V

    .line 87
    const/4 v0, 0x1

    goto :goto_0
.end method

.method setDatabaseFile(Ljava/io/File;)V
    .locals 0

    .prologue
    .line 114
    iput-object p1, p0, Lcom/google/android/apps/chrome/bookmark/AndroidBrowserImporter;->mDatabaseFile:Ljava/io/File;

    .line 115
    return-void
.end method

.method setIgnoreAvailableProvidersForTestPurposes(Z)V
    .locals 0

    .prologue
    .line 109
    iput-boolean p1, p0, Lcom/google/android/apps/chrome/bookmark/AndroidBrowserImporter;->mIgnoreAvailableProvidersForTestPurposes:Z

    .line 110
    return-void
.end method

.method setInputResolver(Landroid/content/ContentResolver;)V
    .locals 0

    .prologue
    .line 104
    iput-object p1, p0, Lcom/google/android/apps/chrome/bookmark/AndroidBrowserImporter;->mInputResolver:Landroid/content/ContentResolver;

    .line 105
    return-void
.end method
