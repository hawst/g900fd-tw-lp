.class public Lcom/google/android/apps/chrome/knoxsettings/KnoxAuditLogger;
.super Ljava/lang/Object;
.source "KnoxAuditLogger.java"


# static fields
.field public static final AUTOFILL_COMPONENT:Ljava/lang/String; = "Chrome_module"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    return-void
.end method

.method static synthetic access$000(Landroid/content/Context;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 15
    invoke-static {p0, p1}, Lcom/google/android/apps/chrome/knoxsettings/KnoxAuditLogger;->logURLBlockedReport(Landroid/content/Context;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$100(Landroid/content/Context;Lcom/google/android/apps/chrome/knoxsettings/KnoxAuditLogger$AuditEvent;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 15
    invoke-static {p0, p1, p2, p3, p4}, Lcom/google/android/apps/chrome/knoxsettings/KnoxAuditLogger;->log(Landroid/content/Context;Lcom/google/android/apps/chrome/knoxsettings/KnoxAuditLogger$AuditEvent;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private static log(Landroid/content/Context;Lcom/google/android/apps/chrome/knoxsettings/KnoxAuditLogger$AuditEvent;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 129
    new-instance v0, Lcom/google/android/apps/chrome/knoxsettings/KnoxAuditLogger$5;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/chrome/knoxsettings/KnoxAuditLogger$5;-><init>(Landroid/content/Context;Lcom/google/android/apps/chrome/knoxsettings/KnoxAuditLogger$AuditEvent;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 143
    sget-object v1, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Void;

    invoke-virtual {v0, v1, v2}, Landroid/os/AsyncTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 144
    return-void
.end method

.method public static logCertificateFailure(Landroid/content/Context;I)V
    .locals 3

    .prologue
    .line 75
    new-instance v0, Lcom/google/android/apps/chrome/knoxsettings/KnoxAuditLogger$1;

    invoke-direct {v0, p0, p1}, Lcom/google/android/apps/chrome/knoxsettings/KnoxAuditLogger$1;-><init>(Landroid/content/Context;I)V

    .line 83
    sget-object v1, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Void;

    invoke-virtual {v0, v1, v2}, Landroid/os/AsyncTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 84
    return-void
.end method

.method public static logIfNecessary(Landroid/content/Context;Lcom/google/android/apps/chrome/knoxsettings/KnoxAuditLogger$AuditEvent;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 7

    .prologue
    .line 114
    invoke-static {p0}, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings;

    move-result-object v6

    .line 115
    new-instance v0, Lcom/google/android/apps/chrome/knoxsettings/KnoxAuditLogger$4;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/chrome/knoxsettings/KnoxAuditLogger$4;-><init>(Landroid/content/Context;Lcom/google/android/apps/chrome/knoxsettings/KnoxAuditLogger$AuditEvent;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings;)V

    .line 124
    invoke-virtual {v6, v0}, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings;->addObserver(Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Observer;)V

    .line 125
    return-void
.end method

.method private static logURLBlockedReport(Landroid/content/Context;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 101
    new-instance v0, Lcom/google/android/apps/chrome/knoxsettings/KnoxAuditLogger$3;

    invoke-direct {v0, p0, p1}, Lcom/google/android/apps/chrome/knoxsettings/KnoxAuditLogger$3;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 109
    sget-object v1, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Void;

    invoke-virtual {v0, v1, v2}, Landroid/os/AsyncTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 110
    return-void
.end method

.method public static logURLBlockedReportIfNecessary(Landroid/content/Context;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 87
    invoke-static {p0}, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings;

    move-result-object v0

    .line 88
    new-instance v1, Lcom/google/android/apps/chrome/knoxsettings/KnoxAuditLogger$2;

    invoke-direct {v1, p0, p1, v0}, Lcom/google/android/apps/chrome/knoxsettings/KnoxAuditLogger$2;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings;)V

    .line 97
    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings;->addObserver(Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Observer;)V

    .line 98
    return-void
.end method
