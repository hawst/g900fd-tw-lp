.class public Lcom/google/android/apps/chrome/videofling/ExpandedControllerActivity;
.super Landroid/support/v4/app/k;
.source "ExpandedControllerActivity.java"

# interfaces
.implements Lcom/google/android/apps/chrome/videofling/MediaRouteController$Listener;


# instance fields
.field private mHandler:Landroid/os/Handler;

.field private mMediaController:Lcom/google/android/apps/chrome/third_party/MediaController;

.field private mMediaRouteButton:Lcom/google/android/apps/chrome/videofling/FullscreenMediaRouteButton;

.field private mMediaRouteController:Lcom/google/android/apps/chrome/videofling/MediaRouteController;

.field private mProgressUpdater:Ljava/lang/Runnable;

.field private mRemoteMediaPlayerController:Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerController;

.field private mScreenName:Ljava/lang/String;

.field private mTransportMediator:Landroid/support/v4/media/d;

.field private mTransportPerformer:Landroid/support/v4/media/l;

.field private mVideoInfo:Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 37
    invoke-direct {p0}, Landroid/support/v4/app/k;-><init>()V

    .line 57
    new-instance v0, Lcom/google/android/apps/chrome/videofling/ExpandedControllerActivity$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/videofling/ExpandedControllerActivity$1;-><init>(Lcom/google/android/apps/chrome/videofling/ExpandedControllerActivity;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/videofling/ExpandedControllerActivity;->mTransportPerformer:Landroid/support/v4/media/l;

    .line 107
    new-instance v0, Lcom/google/android/apps/chrome/videofling/ExpandedControllerActivity$2;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/videofling/ExpandedControllerActivity$2;-><init>(Lcom/google/android/apps/chrome/videofling/ExpandedControllerActivity;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/videofling/ExpandedControllerActivity;->mProgressUpdater:Ljava/lang/Runnable;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/chrome/videofling/ExpandedControllerActivity;)Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerController;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/ExpandedControllerActivity;->mRemoteMediaPlayerController:Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerController;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/chrome/videofling/ExpandedControllerActivity;)Lcom/google/android/apps/chrome/videofling/MediaRouteController;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/ExpandedControllerActivity;->mMediaRouteController:Lcom/google/android/apps/chrome/videofling/MediaRouteController;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/apps/chrome/videofling/ExpandedControllerActivity;)Lcom/google/android/apps/chrome/third_party/MediaController;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/ExpandedControllerActivity;->mMediaController:Lcom/google/android/apps/chrome/third_party/MediaController;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/apps/chrome/videofling/ExpandedControllerActivity;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/ExpandedControllerActivity;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method private cleanup()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 207
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/ExpandedControllerActivity;->mHandler:Landroid/os/Handler;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/ExpandedControllerActivity;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/chrome/videofling/ExpandedControllerActivity;->mProgressUpdater:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 208
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/ExpandedControllerActivity;->mMediaRouteController:Lcom/google/android/apps/chrome/videofling/MediaRouteController;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/ExpandedControllerActivity;->mMediaRouteController:Lcom/google/android/apps/chrome/videofling/MediaRouteController;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->removeListener(Lcom/google/android/apps/chrome/videofling/MediaRouteController$Listener;)V

    .line 209
    :cond_1
    iput-object v2, p0, Lcom/google/android/apps/chrome/videofling/ExpandedControllerActivity;->mMediaRouteController:Lcom/google/android/apps/chrome/videofling/MediaRouteController;

    .line 210
    iput-object v2, p0, Lcom/google/android/apps/chrome/videofling/ExpandedControllerActivity;->mProgressUpdater:Ljava/lang/Runnable;

    .line 211
    return-void
.end method

.method private scheduleProgressUpdate()V
    .locals 2

    .prologue
    .line 224
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/ExpandedControllerActivity;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/chrome/videofling/ExpandedControllerActivity;->mProgressUpdater:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 225
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/ExpandedControllerActivity;->mRemoteMediaPlayerController:Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerController;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerController;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 226
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/ExpandedControllerActivity;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/chrome/videofling/ExpandedControllerActivity;->mProgressUpdater:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 228
    :cond_0
    return-void
.end method

.method private setScreenName(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 234
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/ExpandedControllerActivity;->mScreenName:Ljava/lang/String;

    invoke-static {v0, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 238
    :goto_0
    return-void

    .line 236
    :cond_0
    iput-object p1, p0, Lcom/google/android/apps/chrome/videofling/ExpandedControllerActivity;->mScreenName:Ljava/lang/String;

    .line 237
    invoke-direct {p0}, Lcom/google/android/apps/chrome/videofling/ExpandedControllerActivity;->updateUi()V

    goto :goto_0
.end method

.method private final setVideoInfo(Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo;)V
    .locals 1

    .prologue
    .line 217
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/ExpandedControllerActivity;->mVideoInfo:Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo;

    if-nez v0, :cond_1

    if-nez p1, :cond_2

    .line 221
    :cond_0
    :goto_0
    return-void

    .line 217
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/ExpandedControllerActivity;->mVideoInfo:Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 219
    :cond_2
    iput-object p1, p0, Lcom/google/android/apps/chrome/videofling/ExpandedControllerActivity;->mVideoInfo:Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo;

    .line 220
    invoke-direct {p0}, Lcom/google/android/apps/chrome/videofling/ExpandedControllerActivity;->updateUi()V

    goto :goto_0
.end method

.method public static startActivity(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 331
    if-nez p0, :cond_0

    .line 336
    :goto_0
    return-void

    .line 333
    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/apps/chrome/videofling/ExpandedControllerActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 334
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 335
    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method private updateUi()V
    .locals 5

    .prologue
    .line 249
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/ExpandedControllerActivity;->mMediaController:Lcom/google/android/apps/chrome/third_party/MediaController;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/ExpandedControllerActivity;->mMediaRouteController:Lcom/google/android/apps/chrome/videofling/MediaRouteController;

    if-nez v0, :cond_1

    .line 260
    :cond_0
    :goto_0
    return-void

    .line 251
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/ExpandedControllerActivity;->mMediaRouteController:Lcom/google/android/apps/chrome/videofling/MediaRouteController;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->getRouteName()Ljava/lang/String;

    move-result-object v1

    .line 252
    const-string/jumbo v0, ""

    .line 253
    if-eqz v1, :cond_2

    .line 254
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/videofling/ExpandedControllerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v2, Lcom/google/android/apps/chrome/R$string;->athome_casting_video:I

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v1, v3, v4

    invoke-virtual {v0, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    .line 256
    :goto_1
    sget v0, Lcom/google/android/apps/chrome/R$id;->cast_screen_title:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/videofling/ExpandedControllerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 257
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 259
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/ExpandedControllerActivity;->mMediaController:Lcom/google/android/apps/chrome/third_party/MediaController;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/third_party/MediaController;->refresh()V

    goto :goto_0

    :cond_2
    move-object v1, v0

    goto :goto_1
.end method


# virtual methods
.method public dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 2

    .prologue
    .line 197
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v0

    .line 198
    const/16 v1, 0x19

    if-eq v0, v1, :cond_0

    const/16 v1, 0x18

    if-ne v0, v1, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/ExpandedControllerActivity;->mVideoInfo:Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo;

    iget-object v0, v0, Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo;->state:Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;

    sget-object v1, Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;->FINISHED:Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;

    if-ne v0, v1, :cond_2

    .line 200
    :cond_1
    invoke-super {p0, p1}, Landroid/support/v4/app/k;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v0

    .line 203
    :goto_0
    return v0

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/ExpandedControllerActivity;->mMediaRouteController:Lcom/google/android/apps/chrome/videofling/MediaRouteController;

    invoke-static {v0, p1}, Lcom/google/android/apps/chrome/videofling/RemoteMediaUtils;->handleVolumeKeyEvent(Lcom/google/android/apps/chrome/videofling/MediaRouteController;Landroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    const/16 v3, 0x400

    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 121
    invoke-super {p0, p1}, Landroid/support/v4/app/k;->onCreate(Landroid/os/Bundle;)V

    .line 123
    const-wide/16 v4, 0x0

    invoke-static {v4, v5}, Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerController;->instance(J)Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerController;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/videofling/ExpandedControllerActivity;->mRemoteMediaPlayerController:Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerController;

    .line 124
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/ExpandedControllerActivity;->mRemoteMediaPlayerController:Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerController;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerController;->getCurrentlyPlayingMediaRouteController()Lcom/google/android/apps/chrome/videofling/MediaRouteController;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/videofling/ExpandedControllerActivity;->mMediaRouteController:Lcom/google/android/apps/chrome/videofling/MediaRouteController;

    .line 127
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/ExpandedControllerActivity;->mMediaRouteController:Lcom/google/android/apps/chrome/videofling/MediaRouteController;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/ExpandedControllerActivity;->mMediaRouteController:Lcom/google/android/apps/chrome/videofling/MediaRouteController;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->routeIsDefaultRoute()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 129
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/videofling/ExpandedControllerActivity;->finish()V

    .line 173
    :goto_0
    return-void

    .line 134
    :cond_1
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/videofling/ExpandedControllerActivity;->requestWindowFeature(I)Z

    .line 135
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/videofling/ExpandedControllerActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v3, v3}, Landroid/view/Window;->setFlags(II)V

    .line 139
    sget v0, Lcom/google/android/apps/chrome/R$layout;->expanded_cast_controller:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/videofling/ExpandedControllerActivity;->setContentView(I)V

    .line 140
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/videofling/ExpandedControllerActivity;->mHandler:Landroid/os/Handler;

    .line 142
    const v0, 0x1020002

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/videofling/ExpandedControllerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 143
    const/high16 v3, -0x1000000

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->setBackgroundColor(I)V

    .line 145
    iget-object v3, p0, Lcom/google/android/apps/chrome/videofling/ExpandedControllerActivity;->mMediaRouteController:Lcom/google/android/apps/chrome/videofling/MediaRouteController;

    invoke-virtual {v3, p0}, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->addListener(Lcom/google/android/apps/chrome/videofling/MediaRouteController$Listener;)V

    .line 149
    new-instance v3, Landroid/support/v4/media/d;

    iget-object v4, p0, Lcom/google/android/apps/chrome/videofling/ExpandedControllerActivity;->mTransportPerformer:Landroid/support/v4/media/l;

    invoke-direct {v3, p0, v4}, Landroid/support/v4/media/d;-><init>(Landroid/app/Activity;Landroid/support/v4/media/l;)V

    iput-object v3, p0, Lcom/google/android/apps/chrome/videofling/ExpandedControllerActivity;->mTransportMediator:Landroid/support/v4/media/d;

    .line 152
    sget v3, Lcom/google/android/apps/chrome/R$id;->cast_media_controller:I

    invoke-virtual {p0, v3}, Lcom/google/android/apps/chrome/videofling/ExpandedControllerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/chrome/third_party/MediaController;

    iput-object v3, p0, Lcom/google/android/apps/chrome/videofling/ExpandedControllerActivity;->mMediaController:Lcom/google/android/apps/chrome/third_party/MediaController;

    .line 153
    iget-object v3, p0, Lcom/google/android/apps/chrome/videofling/ExpandedControllerActivity;->mMediaController:Lcom/google/android/apps/chrome/third_party/MediaController;

    iget-object v4, p0, Lcom/google/android/apps/chrome/videofling/ExpandedControllerActivity;->mTransportMediator:Landroid/support/v4/media/d;

    invoke-virtual {v3, v4}, Lcom/google/android/apps/chrome/third_party/MediaController;->setMediaPlayer(Landroid/support/v4/media/c;)V

    .line 155
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/videofling/ExpandedControllerActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v3

    sget v4, Lcom/google/android/apps/chrome/R$layout;->cast_controller_media_route_button:I

    invoke-virtual {v3, v4, v0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    .line 158
    instance-of v4, v3, Lcom/google/android/apps/chrome/videofling/FullscreenMediaRouteButton;

    if-eqz v4, :cond_2

    .line 159
    check-cast v3, Lcom/google/android/apps/chrome/videofling/FullscreenMediaRouteButton;

    iput-object v3, p0, Lcom/google/android/apps/chrome/videofling/ExpandedControllerActivity;->mMediaRouteButton:Lcom/google/android/apps/chrome/videofling/FullscreenMediaRouteButton;

    .line 160
    iget-object v3, p0, Lcom/google/android/apps/chrome/videofling/ExpandedControllerActivity;->mMediaRouteButton:Lcom/google/android/apps/chrome/videofling/FullscreenMediaRouteButton;

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 161
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/ExpandedControllerActivity;->mMediaRouteButton:Lcom/google/android/apps/chrome/videofling/FullscreenMediaRouteButton;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/videofling/FullscreenMediaRouteButton;->bringToFront()V

    .line 162
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/ExpandedControllerActivity;->mMediaRouteButton:Lcom/google/android/apps/chrome/videofling/FullscreenMediaRouteButton;

    iget-object v3, p0, Lcom/google/android/apps/chrome/videofling/ExpandedControllerActivity;->mMediaRouteController:Lcom/google/android/apps/chrome/videofling/MediaRouteController;

    invoke-virtual {v0, v3}, Lcom/google/android/apps/chrome/videofling/FullscreenMediaRouteButton;->initialize(Lcom/google/android/apps/chrome/videofling/MediaRouteController;)V

    .line 168
    :goto_1
    new-instance v0, Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo;

    sget-object v3, Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;->STOPPED:Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;

    move v4, v2

    move-object v5, v1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo;-><init>(Ljava/lang/String;ILcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;ILjava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/videofling/ExpandedControllerActivity;->setVideoInfo(Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo;)V

    .line 170
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/ExpandedControllerActivity;->mMediaController:Lcom/google/android/apps/chrome/third_party/MediaController;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/third_party/MediaController;->refresh()V

    .line 172
    invoke-direct {p0}, Lcom/google/android/apps/chrome/videofling/ExpandedControllerActivity;->scheduleProgressUpdate()V

    goto :goto_0

    .line 164
    :cond_2
    iput-object v1, p0, Lcom/google/android/apps/chrome/videofling/ExpandedControllerActivity;->mMediaRouteButton:Lcom/google/android/apps/chrome/videofling/FullscreenMediaRouteButton;

    goto :goto_1
.end method

.method protected onDestroy()V
    .locals 0

    .prologue
    .line 191
    invoke-direct {p0}, Lcom/google/android/apps/chrome/videofling/ExpandedControllerActivity;->cleanup()V

    .line 192
    invoke-super {p0}, Landroid/support/v4/app/k;->onDestroy()V

    .line 193
    return-void
.end method

.method public onDurationUpdated(I)V
    .locals 2

    .prologue
    .line 306
    new-instance v0, Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo;

    iget-object v1, p0, Lcom/google/android/apps/chrome/videofling/ExpandedControllerActivity;->mVideoInfo:Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo;

    invoke-direct {v0, v1}, Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo;-><init>(Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo;)V

    .line 307
    iput p1, v0, Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo;->durationMillis:I

    .line 308
    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/videofling/ExpandedControllerActivity;->setVideoInfo(Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo;)V

    .line 309
    return-void
.end method

.method public onError(ILjava/lang/String;)V
    .locals 1

    .prologue
    .line 286
    const/4 v0, 0x2

    if-ne p1, v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/videofling/ExpandedControllerActivity;->finish()V

    .line 287
    :cond_0
    return-void
.end method

.method public onPlaybackStateChanged(Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;Lorg/chromium/chrome/browser/Tab;I)V
    .locals 2

    .prologue
    .line 292
    new-instance v0, Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo;

    iget-object v1, p0, Lcom/google/android/apps/chrome/videofling/ExpandedControllerActivity;->mVideoInfo:Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo;

    invoke-direct {v0, v1}, Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo;-><init>(Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo;)V

    .line 293
    iput-object p2, v0, Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo;->state:Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;

    .line 294
    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/videofling/ExpandedControllerActivity;->setVideoInfo(Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo;)V

    .line 296
    invoke-direct {p0}, Lcom/google/android/apps/chrome/videofling/ExpandedControllerActivity;->scheduleProgressUpdate()V

    .line 298
    sget-object v0, Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;->FINISHED:Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;

    if-eq p2, v0, :cond_0

    sget-object v0, Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;->INVALIDATED:Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;

    if-ne p2, v0, :cond_1

    .line 300
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/videofling/ExpandedControllerActivity;->finish()V

    .line 302
    :cond_1
    return-void
.end method

.method public onPositionChanged(I)V
    .locals 2

    .prologue
    .line 313
    new-instance v0, Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo;

    iget-object v1, p0, Lcom/google/android/apps/chrome/videofling/ExpandedControllerActivity;->mVideoInfo:Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo;

    invoke-direct {v0, v1}, Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo;-><init>(Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo;)V

    .line 314
    iput p1, v0, Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo;->currentTimeMillis:I

    .line 315
    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/videofling/ExpandedControllerActivity;->setVideoInfo(Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo;)V

    .line 316
    return-void
.end method

.method public onPrepared(Lcom/google/android/apps/chrome/videofling/MediaRouteController;)V
    .locals 0

    .prologue
    .line 277
    return-void
.end method

.method protected onResume()V
    .locals 2

    .prologue
    .line 177
    invoke-super {p0}, Landroid/support/v4/app/k;->onResume()V

    .line 178
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/ExpandedControllerActivity;->mVideoInfo:Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo;

    iget-object v0, v0, Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo;->state:Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;

    sget-object v1, Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;->FINISHED:Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/videofling/ExpandedControllerActivity;->finish()V

    .line 179
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/ExpandedControllerActivity;->mMediaRouteController:Lcom/google/android/apps/chrome/videofling/MediaRouteController;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->startDiscovery()V

    .line 181
    sget v0, Lcom/google/android/apps/chrome/R$id;->cast_background_image:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/videofling/ExpandedControllerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 182
    if-nez v0, :cond_1

    .line 187
    :goto_0
    return-void

    .line 183
    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/chrome/videofling/ExpandedControllerActivity;->mRemoteMediaPlayerController:Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerController;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerController;->getPoster()Landroid/graphics/Bitmap;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 184
    iget-object v1, p0, Lcom/google/android/apps/chrome/videofling/ExpandedControllerActivity;->mRemoteMediaPlayerController:Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerController;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerController;->getPoster()Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 186
    :cond_2
    const/16 v1, 0xc8

    invoke-static {v0, v1}, Lorg/chromium/base/ApiCompatibilityUtils;->setImageAlpha(Landroid/widget/ImageView;I)V

    goto :goto_0
.end method

.method public onRouteSelected(Ljava/lang/String;Lorg/chromium/chrome/browser/Tab;ILcom/google/android/apps/chrome/videofling/MediaRouteController;)V
    .locals 0

    .prologue
    .line 265
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/videofling/ExpandedControllerActivity;->setScreenName(Ljava/lang/String;)V

    .line 266
    return-void
.end method

.method public onRouteUnselected(Lorg/chromium/chrome/browser/Tab;ILcom/google/android/apps/chrome/videofling/MediaRouteController;)V
    .locals 0

    .prologue
    .line 271
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/videofling/ExpandedControllerActivity;->finish()V

    .line 272
    return-void
.end method

.method public onSeekCompleted()V
    .locals 0

    .prologue
    .line 282
    return-void
.end method

.method public onTitleChanged(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 320
    new-instance v0, Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo;

    iget-object v1, p0, Lcom/google/android/apps/chrome/videofling/ExpandedControllerActivity;->mVideoInfo:Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo;

    invoke-direct {v0, v1}, Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo;-><init>(Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo;)V

    .line 321
    iput-object p1, v0, Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo;->title:Ljava/lang/String;

    .line 322
    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/videofling/ExpandedControllerActivity;->setVideoInfo(Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo;)V

    .line 323
    return-void
.end method
