.class Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar$7;
.super Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;
.source "FindToolbar.java"


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;)V
    .locals 0

    .prologue
    .line 230
    iput-object p1, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar$7;->this$0:Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;

    invoke-direct {p0}, Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3

    .prologue
    .line 232
    iget v0, p1, Landroid/os/Message;->what:I

    sparse-switch v0, :sswitch_data_0

    .line 250
    :cond_0
    :goto_0
    return-void

    .line 235
    :sswitch_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar$7;->this$0:Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;

    # invokes: Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->sendHideFindToolbarNotification()V
    invoke-static {v0}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->access$000(Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;)V

    goto :goto_0

    .line 238
    :sswitch_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar$7;->this$0:Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;

    # invokes: Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->sendHideFindToolbarNotification()V
    invoke-static {v0}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->access$000(Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;)V

    .line 239
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar$7;->this$0:Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v1

    const-string/jumbo v2, "incognito"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->updateVisualsForTabModel(Z)V

    goto :goto_0

    .line 243
    :sswitch_2
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    const-string/jumbo v1, "tabId"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    .line 244
    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar$7;->this$0:Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;

    iget-object v1, v1, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->mTabModelSelector:Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

    invoke-interface {v1}, Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;->getCurrentTab()Lorg/chromium/chrome/browser/Tab;

    move-result-object v1

    .line 245
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lorg/chromium/chrome/browser/Tab;->getId()I

    move-result v1

    if-ne v0, v1, :cond_0

    .line 246
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar$7;->this$0:Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;

    # invokes: Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->sendHideFindToolbarNotification()V
    invoke-static {v0}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->access$000(Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;)V

    goto :goto_0

    .line 232
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x3 -> :sswitch_0
        0x8 -> :sswitch_2
        0xc -> :sswitch_1
        0x24 -> :sswitch_2
    .end sparse-switch
.end method
