.class public Lcom/google/android/apps/chrome/tabmodel/SingleTabModelSelector;
.super Lorg/chromium/chrome/browser/tabmodel/TabModelSelectorBase;
.source "SingleTabModelSelector.java"


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private final mApplicationContext:Landroid/content/Context;

.field private final mTabModel:Lcom/google/android/apps/chrome/tabmodel/SingleTabModel;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 21
    const-class v0, Lcom/google/android/apps/chrome/tabmodel/SingleTabModelSelector;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/tabmodel/SingleTabModelSelector;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Landroid/app/Activity;ZZ)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 25
    invoke-direct {p0}, Lorg/chromium/chrome/browser/tabmodel/TabModelSelectorBase;-><init>()V

    .line 26
    invoke-virtual {p1}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabmodel/SingleTabModelSelector;->mApplicationContext:Landroid/content/Context;

    .line 27
    new-instance v0, Lcom/google/android/apps/chrome/tabmodel/SingleTabModel;

    invoke-direct {v0, p1, p2, p3}, Lcom/google/android/apps/chrome/tabmodel/SingleTabModel;-><init>(Landroid/app/Activity;ZZ)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabmodel/SingleTabModelSelector;->mTabModel:Lcom/google/android/apps/chrome/tabmodel/SingleTabModel;

    .line 28
    const/4 v0, 0x1

    new-array v0, v0, [Lorg/chromium/chrome/browser/tabmodel/TabModel;

    iget-object v1, p0, Lcom/google/android/apps/chrome/tabmodel/SingleTabModelSelector;->mTabModel:Lcom/google/android/apps/chrome/tabmodel/SingleTabModel;

    aput-object v1, v0, v2

    invoke-virtual {p0, v2, v0}, Lcom/google/android/apps/chrome/tabmodel/SingleTabModelSelector;->initialize(Z[Lorg/chromium/chrome/browser/tabmodel/TabModel;)V

    .line 29
    return-void
.end method


# virtual methods
.method public closeAllTabs()V
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabmodel/SingleTabModelSelector;->mTabModel:Lcom/google/android/apps/chrome/tabmodel/SingleTabModel;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabmodel/SingleTabModel;->closeAllTabs()V

    .line 69
    return-void
.end method

.method public getCurrentModel()Lorg/chromium/chrome/browser/tabmodel/TabModel;
    .locals 2

    .prologue
    .line 47
    sget-boolean v0, Lcom/google/android/apps/chrome/tabmodel/SingleTabModelSelector;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    invoke-super {p0}, Lorg/chromium/chrome/browser/tabmodel/TabModelSelectorBase;->getCurrentModel()Lorg/chromium/chrome/browser/tabmodel/TabModel;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/tabmodel/SingleTabModelSelector;->mTabModel:Lcom/google/android/apps/chrome/tabmodel/SingleTabModel;

    if-eq v0, v1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 48
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabmodel/SingleTabModelSelector;->mTabModel:Lcom/google/android/apps/chrome/tabmodel/SingleTabModel;

    return-object v0
.end method

.method public getModel(Z)Lorg/chromium/chrome/browser/tabmodel/TabModel;
    .locals 1

    .prologue
    .line 42
    invoke-super {p0, p1}, Lorg/chromium/chrome/browser/tabmodel/TabModelSelectorBase;->getModel(Z)Lorg/chromium/chrome/browser/tabmodel/TabModel;

    move-result-object v0

    return-object v0
.end method

.method public getModelAt(I)Lorg/chromium/chrome/browser/tabmodel/TabModel;
    .locals 1

    .prologue
    .line 86
    sget-boolean v0, Lcom/google/android/apps/chrome/tabmodel/SingleTabModelSelector;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    if-eq p1, v0, :cond_0

    if-eqz p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 87
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabmodel/SingleTabModelSelector;->mTabModel:Lcom/google/android/apps/chrome/tabmodel/SingleTabModel;

    return-object v0
.end method

.method public getTabById(I)Lorg/chromium/chrome/browser/Tab;
    .locals 2

    .prologue
    .line 79
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabmodel/SingleTabModelSelector;->getCurrentTab()Lorg/chromium/chrome/browser/Tab;

    move-result-object v0

    .line 80
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/Tab;->getId()I

    move-result v1

    if-ne v1, p1, :cond_0

    .line 81
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getTotalTabCount()I
    .locals 1

    .prologue
    .line 73
    sget-boolean v0, Lcom/google/android/apps/chrome/tabmodel/SingleTabModelSelector;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabmodel/SingleTabModelSelector;->mTabModel:Lcom/google/android/apps/chrome/tabmodel/SingleTabModel;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 74
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabmodel/SingleTabModelSelector;->mTabModel:Lcom/google/android/apps/chrome/tabmodel/SingleTabModel;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabmodel/SingleTabModel;->getCount()I

    move-result v0

    return v0
.end method

.method public isIncognitoSelected()Z
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabmodel/SingleTabModelSelector;->mTabModel:Lcom/google/android/apps/chrome/tabmodel/SingleTabModel;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabmodel/SingleTabModel;->isIncognito()Z

    move-result v0

    return v0
.end method

.method public openNewTab(Lorg/chromium/content_public/browser/LoadUrlParams;Lorg/chromium/chrome/browser/tabmodel/TabModel$TabLaunchType;Lorg/chromium/chrome/browser/Tab;Z)Lorg/chromium/chrome/browser/Tab;
    .locals 3

    .prologue
    .line 59
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "android.intent.action.VIEW"

    invoke-virtual {p1}, Lorg/chromium/content_public/browser/LoadUrlParams;->getUrl()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 60
    iget-object v1, p0, Lcom/google/android/apps/chrome/tabmodel/SingleTabModelSelector;->mApplicationContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 61
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 62
    iget-object v1, p0, Lcom/google/android/apps/chrome/tabmodel/SingleTabModelSelector;->mApplicationContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 63
    const/4 v0, 0x0

    return-object v0
.end method

.method public selectModel(Z)V
    .locals 1

    .prologue
    .line 37
    sget-boolean v0, Lcom/google/android/apps/chrome/tabmodel/SingleTabModelSelector;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabmodel/SingleTabModelSelector;->mTabModel:Lcom/google/android/apps/chrome/tabmodel/SingleTabModel;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabmodel/SingleTabModel;->isIncognito()Z

    move-result v0

    if-eq p1, v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 38
    :cond_0
    return-void
.end method

.method public setTab(Lorg/chromium/chrome/browser/Tab;)V
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabmodel/SingleTabModelSelector;->mTabModel:Lcom/google/android/apps/chrome/tabmodel/SingleTabModel;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/tabmodel/SingleTabModel;->setTab(Lorg/chromium/chrome/browser/Tab;)V

    .line 33
    return-void
.end method
