.class Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog$1;
.super Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkModelObserver;
.source "EnhancedBookmarkDetailDialog.java"


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;)V
    .locals 0

    .prologue
    .line 78
    iput-object p1, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog$1;->this$0:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;

    invoke-direct {p0}, Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkModelObserver;-><init>()V

    return-void
.end method


# virtual methods
.method public bookmarkModelChanged()V
    .locals 1

    .prologue
    .line 103
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog$1;->this$0:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;

    # invokes: Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;->updateViews()V
    invoke-static {v0}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;->access$300(Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;)V

    .line 106
    return-void
.end method

.method public bookmarkNodeAdded(Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkItem;I)V
    .locals 0

    .prologue
    .line 82
    return-void
.end method

.method public bookmarkNodeChanged(Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkItem;)V
    .locals 2

    .prologue
    .line 93
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog$1;->this$0:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;

    # getter for: Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;->mBookmarkId:Lorg/chromium/components/bookmarks/BookmarkId;
    invoke-static {v0}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;->access$100(Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;)Lorg/chromium/components/bookmarks/BookmarkId;

    move-result-object v0

    invoke-virtual {p1}, Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkItem;->getId()Lorg/chromium/components/bookmarks/BookmarkId;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/chromium/components/bookmarks/BookmarkId;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog$1;->this$0:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;

    # invokes: Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;->updateViews(Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkItem;)V
    invoke-static {v0, p1}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;->access$200(Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkItem;)V

    .line 94
    :cond_0
    return-void
.end method

.method public bookmarkNodeChildrenReordered(Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkItem;)V
    .locals 0

    .prologue
    .line 99
    return-void
.end method

.method public bookmarkNodeRemoved(Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkItem;ILorg/chromium/chrome/browser/BookmarksBridge$BookmarkItem;Z)V
    .locals 2

    .prologue
    .line 88
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog$1;->this$0:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;

    # getter for: Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;->mBookmarkId:Lorg/chromium/components/bookmarks/BookmarkId;
    invoke-static {v0}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;->access$100(Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;)Lorg/chromium/components/bookmarks/BookmarkId;

    move-result-object v0

    invoke-virtual {p3}, Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkItem;->getId()Lorg/chromium/components/bookmarks/BookmarkId;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/chromium/components/bookmarks/BookmarkId;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog$1;->this$0:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;->dismiss()V

    .line 89
    :cond_0
    return-void
.end method
