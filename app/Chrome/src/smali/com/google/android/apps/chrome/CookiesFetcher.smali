.class Lcom/google/android/apps/chrome/CookiesFetcher;
.super Ljava/lang/Object;
.source "CookiesFetcher.java"


# static fields
.field public static final DEFAULT_COOKIE_FILE_NAME:Ljava/lang/String; = "COOKIES.DAT"

.field public static final TAG:Ljava/lang/String; = "CookiesFetcher"


# instance fields
.field private final mCleanupReference:Lorg/chromium/content/common/CleanupReference;

.field private final mFileName:Ljava/lang/String;

.field private final mNativeCookiesFetcher:J


# direct methods
.method private constructor <init>(Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 65
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 66
    invoke-direct {p0}, Lcom/google/android/apps/chrome/CookiesFetcher;->nativeInit()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/chrome/CookiesFetcher;->mNativeCookiesFetcher:J

    .line 67
    iput-object p1, p0, Lcom/google/android/apps/chrome/CookiesFetcher;->mFileName:Ljava/lang/String;

    .line 68
    new-instance v0, Lorg/chromium/content/common/CleanupReference;

    new-instance v1, Lcom/google/android/apps/chrome/CookiesFetcher$DestroyRunnable;

    iget-wide v2, p0, Lcom/google/android/apps/chrome/CookiesFetcher;->mNativeCookiesFetcher:J

    const/4 v4, 0x0

    invoke-direct {v1, v2, v3, v4}, Lcom/google/android/apps/chrome/CookiesFetcher$DestroyRunnable;-><init>(JLcom/google/android/apps/chrome/CookiesFetcher$1;)V

    invoke-direct {v0, p0, v1}, Lorg/chromium/content/common/CleanupReference;-><init>(Ljava/lang/Object;Ljava/lang/Runnable;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/CookiesFetcher;->mCleanupReference:Lorg/chromium/content/common/CleanupReference;

    .line 70
    return-void
.end method

.method static synthetic access$100(Lcom/google/android/apps/chrome/CookiesFetcher;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/google/android/apps/chrome/CookiesFetcher;->mFileName:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/apps/chrome/CookiesFetcher;)V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Lcom/google/android/apps/chrome/CookiesFetcher;->scheduleDeleteCookiesFile()V

    return-void
.end method

.method static synthetic access$300(Lcom/google/android/apps/chrome/CookiesFetcher;)J
    .locals 2

    .prologue
    .line 34
    iget-wide v0, p0, Lcom/google/android/apps/chrome/CookiesFetcher;->mNativeCookiesFetcher:J

    return-wide v0
.end method

.method static synthetic access$400(Lcom/google/android/apps/chrome/CookiesFetcher;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJJZZI)V
    .locals 0

    .prologue
    .line 34
    invoke-direct/range {p0 .. p16}, Lcom/google/android/apps/chrome/CookiesFetcher;->nativeRestoreToCookieJar(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJJZZI)V

    return-void
.end method

.method static synthetic access$500(Lcom/google/android/apps/chrome/CookiesFetcher;[Lcom/google/android/apps/chrome/CanonicalCookie;)V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/CookiesFetcher;->saveFetchedCookiesToDisk([Lcom/google/android/apps/chrome/CanonicalCookie;)V

    return-void
.end method

.method static synthetic access$600(J)V
    .locals 0

    .prologue
    .line 34
    invoke-static {p0, p1}, Lcom/google/android/apps/chrome/CookiesFetcher;->nativeDestroy(J)V

    return-void
.end method

.method private createCookie(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJJZZI)Lcom/google/android/apps/chrome/CanonicalCookie;
    .locals 16

    .prologue
    .line 200
    new-instance v0, Lcom/google/android/apps/chrome/CanonicalCookie;

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-wide/from16 v6, p6

    move-wide/from16 v8, p8

    move-wide/from16 v10, p10

    move/from16 v12, p12

    move/from16 v13, p13

    move/from16 v14, p14

    invoke-direct/range {v0 .. v14}, Lcom/google/android/apps/chrome/CanonicalCookie;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJJZZI)V

    return-object v0
.end method

.method private createCookiesArray(I)[Lcom/google/android/apps/chrome/CanonicalCookie;
    .locals 1

    .prologue
    .line 294
    new-array v0, p1, [Lcom/google/android/apps/chrome/CanonicalCookie;

    return-object v0
.end method

.method static fetchFromCookieJar(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 80
    :try_start_0
    new-instance v0, Lcom/google/android/apps/chrome/CookiesFetcher;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/CookiesFetcher;-><init>(Ljava/lang/String;)V

    invoke-direct {v0}, Lcom/google/android/apps/chrome/CookiesFetcher;->fetchFromCookieJarInternal()V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 84
    :goto_0
    return-void

    .line 81
    :catch_0
    move-exception v0

    .line 82
    invoke-virtual {v0}, Ljava/lang/RuntimeException;->printStackTrace()V

    goto :goto_0
.end method

.method private fetchFromCookieJarInternal()V
    .locals 2

    .prologue
    .line 87
    iget-wide v0, p0, Lcom/google/android/apps/chrome/CookiesFetcher;->mNativeCookiesFetcher:J

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/chrome/CookiesFetcher;->nativeFetchFromCookieJar(J)V

    .line 88
    return-void
.end method

.method private static native nativeDestroy(J)V
.end method

.method private native nativeFetchFromCookieJar(J)V
.end method

.method private native nativeInit()J
.end method

.method private native nativeRestoreToCookieJar(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJJZZI)V
.end method

.method private onCookieFetchFinished([Lcom/google/android/apps/chrome/CanonicalCookie;)V
    .locals 3

    .prologue
    .line 218
    new-instance v0, Lcom/google/android/apps/chrome/CookiesFetcher$2;

    invoke-direct {v0, p0, p1}, Lcom/google/android/apps/chrome/CookiesFetcher$2;-><init>(Lcom/google/android/apps/chrome/CookiesFetcher;[Lcom/google/android/apps/chrome/CanonicalCookie;)V

    sget-object v1, Landroid/os/AsyncTask;->SERIAL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Void;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/chrome/CookiesFetcher$2;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 225
    return-void
.end method

.method static restoreToCookieJar(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 98
    :try_start_0
    new-instance v0, Lcom/google/android/apps/chrome/CookiesFetcher;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/CookiesFetcher;-><init>(Ljava/lang/String;)V

    .line 99
    invoke-static {}, Lorg/chromium/chrome/browser/profiles/Profile;->getLastUsedProfile()Lorg/chromium/chrome/browser/profiles/Profile;

    move-result-object v1

    invoke-virtual {v1}, Lorg/chromium/chrome/browser/profiles/Profile;->hasOffTheRecordProfile()Z

    move-result v1

    if-nez v1, :cond_0

    .line 102
    invoke-direct {v0}, Lcom/google/android/apps/chrome/CookiesFetcher;->scheduleDeleteCookiesFile()V

    .line 109
    :goto_0
    return-void

    .line 105
    :cond_0
    invoke-direct {v0}, Lcom/google/android/apps/chrome/CookiesFetcher;->restoreToCookieJarInternal()V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 106
    :catch_0
    move-exception v0

    .line 107
    invoke-virtual {v0}, Ljava/lang/RuntimeException;->printStackTrace()V

    goto :goto_0
.end method

.method private restoreToCookieJarInternal()V
    .locals 3

    .prologue
    .line 114
    new-instance v0, Lcom/google/android/apps/chrome/CookiesFetcher$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/CookiesFetcher$1;-><init>(Lcom/google/android/apps/chrome/CookiesFetcher;)V

    sget-object v1, Landroid/os/AsyncTask;->SERIAL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Void;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/chrome/CookiesFetcher$1;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 186
    return-void
.end method

.method private saveFetchedCookiesToDisk([Lcom/google/android/apps/chrome/CanonicalCookie;)V
    .locals 6

    .prologue
    .line 228
    const/4 v1, 0x0

    .line 230
    :try_start_0
    invoke-static {}, Lorg/chromium/content/browser/crypto/CipherFactory;->getInstance()Lorg/chromium/content/browser/crypto/CipherFactory;

    move-result-object v0

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lorg/chromium/content/browser/crypto/CipherFactory;->getCipher(I)Ljavax/crypto/Cipher;

    move-result-object v0

    .line 231
    if-nez v0, :cond_1

    .line 259
    :cond_0
    :goto_0
    return-void

    .line 256
    :cond_1
    new-instance v2, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v2}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 237
    new-instance v3, Ljavax/crypto/CipherOutputStream;

    invoke-direct {v3, v2, v0}, Ljavax/crypto/CipherOutputStream;-><init>(Ljava/io/OutputStream;Ljavax/crypto/Cipher;)V

    .line 239
    new-instance v0, Ljava/io/DataOutputStream;

    invoke-direct {v0, v3}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_6
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 241
    :try_start_1
    const-string/jumbo v1, "c0Ok135"

    invoke-virtual {v0, v1}, Ljava/io/DataOutputStream;->writeUTF(Ljava/lang/String;)V

    .line 242
    array-length v3, p1

    const/4 v1, 0x0

    :goto_1
    if-ge v1, v3, :cond_2

    aget-object v4, p1, v1

    .line 243
    invoke-virtual {v4, v0}, Lcom/google/android/apps/chrome/CanonicalCookie;->saveToStream(Ljava/io/DataOutputStream;)V

    .line 242
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 245
    :cond_2
    invoke-virtual {v0}, Ljava/io/DataOutputStream;->close()V

    .line 246
    iget-object v1, p0, Lcom/google/android/apps/chrome/CookiesFetcher;->mFileName:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v2

    invoke-static {v1, v2}, Lorg/chromium/base/ImportantFileWriterAndroid;->writeFileAtomically(Ljava/lang/String;[B)Z
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_5
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto :goto_0

    .line 256
    :catch_0
    move-exception v1

    .line 250
    :goto_2
    :try_start_2
    const-string/jumbo v1, "CookiesFetcher"

    const-string/jumbo v2, "IOException during Cookie Fetch"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 255
    if-eqz v0, :cond_0

    :try_start_3
    invoke-virtual {v0}, Ljava/io/DataOutputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_0

    .line 257
    :catch_1
    move-exception v0

    const-string/jumbo v0, "CookiesFetcher"

    const-string/jumbo v1, "IOException during Cookie Fetch"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 251
    :catch_2
    move-exception v0

    .line 252
    :goto_3
    :try_start_4
    const-string/jumbo v2, "CookiesFetcher"

    const-string/jumbo v3, "Error storing cookies."

    invoke-static {v2, v3, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 255
    if-eqz v1, :cond_0

    :try_start_5
    invoke-virtual {v1}, Ljava/io/DataOutputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    goto :goto_0

    .line 257
    :catch_3
    move-exception v0

    const-string/jumbo v0, "CookiesFetcher"

    const-string/jumbo v1, "IOException during Cookie Fetch"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 254
    :catchall_0
    move-exception v0

    .line 255
    :goto_4
    if-eqz v1, :cond_3

    :try_start_6
    invoke-virtual {v1}, Ljava/io/DataOutputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_4

    .line 258
    :cond_3
    :goto_5
    throw v0

    .line 257
    :catch_4
    move-exception v1

    const-string/jumbo v1, "CookiesFetcher"

    const-string/jumbo v2, "IOException during Cookie Fetch"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_5

    .line 254
    :catchall_1
    move-exception v1

    move-object v5, v1

    move-object v1, v0

    move-object v0, v5

    goto :goto_4

    .line 251
    :catch_5
    move-exception v1

    move-object v5, v1

    move-object v1, v0

    move-object v0, v5

    goto :goto_3

    .line 256
    :catch_6
    move-exception v0

    move-object v0, v1

    goto :goto_2
.end method

.method private scheduleDeleteCookiesFile()V
    .locals 3

    .prologue
    .line 267
    new-instance v0, Lcom/google/android/apps/chrome/CookiesFetcher$3;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/CookiesFetcher$3;-><init>(Lcom/google/android/apps/chrome/CookiesFetcher;)V

    sget-object v1, Landroid/os/AsyncTask;->SERIAL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Void;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/chrome/CookiesFetcher$3;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 277
    return-void
.end method
