.class public Lcom/google/android/apps/chrome/widget/ContextualSearchControl;
.super Landroid/widget/FrameLayout;
.source "ContextualSearchControl.java"


# instance fields
.field private mEndText:Landroid/widget/TextView;

.field private mForwardingDirection:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;

.field private final mGestureDetector:Landroid/view/GestureDetector;

.field private mManagementDelegate:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManagementDelegate;

.field private final mPxToDp:F

.field private mResourceAdapter:Lcom/google/android/apps/chrome/compositor/resources/dynamics/ViewResourceAdapter;

.field private mSelectionText:Landroid/widget/TextView;

.field private mStartText:Landroid/widget/TextView;

.field private mSwipeHandler:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeHandler;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3

    .prologue
    .line 63
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 46
    sget-object v0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;->UNKNOWN:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;

    iput-object v0, p0, Lcom/google/android/apps/chrome/widget/ContextualSearchControl;->mForwardingDirection:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;

    .line 64
    const/high16 v0, 0x3f800000    # 1.0f

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->density:F

    div-float/2addr v0, v1

    iput v0, p0, Lcom/google/android/apps/chrome/widget/ContextualSearchControl;->mPxToDp:F

    .line 65
    new-instance v0, Landroid/view/GestureDetector;

    new-instance v1, Lcom/google/android/apps/chrome/widget/ContextualSearchControl$GestureListenerImpl;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/google/android/apps/chrome/widget/ContextualSearchControl$GestureListenerImpl;-><init>(Lcom/google/android/apps/chrome/widget/ContextualSearchControl;Lcom/google/android/apps/chrome/widget/ContextualSearchControl$1;)V

    invoke-direct {v0, p1, v1}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/widget/ContextualSearchControl;->mGestureDetector:Landroid/view/GestureDetector;

    .line 66
    return-void
.end method

.method static synthetic access$100(Lcom/google/android/apps/chrome/widget/ContextualSearchControl;)Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeHandler;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/ContextualSearchControl;->mSwipeHandler:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeHandler;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/apps/chrome/widget/ContextualSearchControl;)F
    .locals 1

    .prologue
    .line 32
    iget v0, p0, Lcom/google/android/apps/chrome/widget/ContextualSearchControl;->mPxToDp:F

    return v0
.end method

.method static synthetic access$300(Lcom/google/android/apps/chrome/widget/ContextualSearchControl;)Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/ContextualSearchControl;->mForwardingDirection:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;

    return-object v0
.end method

.method static synthetic access$302(Lcom/google/android/apps/chrome/widget/ContextualSearchControl;Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;)Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;
    .locals 0

    .prologue
    .line 32
    iput-object p1, p0, Lcom/google/android/apps/chrome/widget/ContextualSearchControl;->mForwardingDirection:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;

    return-object p1
.end method


# virtual methods
.method public getResourceAdapter()Lcom/google/android/apps/chrome/compositor/resources/dynamics/ViewResourceAdapter;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/ContextualSearchControl;->mResourceAdapter:Lcom/google/android/apps/chrome/compositor/resources/dynamics/ViewResourceAdapter;

    return-object v0
.end method

.method public invalidateChildInParent([ILandroid/graphics/Rect;)Landroid/view/ViewParent;
    .locals 3

    .prologue
    .line 225
    invoke-super {p0, p1, p2}, Landroid/widget/FrameLayout;->invalidateChildInParent([ILandroid/graphics/Rect;)Landroid/view/ViewParent;

    move-result-object v0

    .line 232
    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/ContextualSearchControl;->mResourceAdapter:Lcom/google/android/apps/chrome/compositor/resources/dynamics/ViewResourceAdapter;

    if-eqz v1, :cond_0

    .line 233
    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/ContextualSearchControl;->mResourceAdapter:Lcom/google/android/apps/chrome/compositor/resources/dynamics/ViewResourceAdapter;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/google/android/apps/chrome/compositor/resources/dynamics/ViewResourceAdapter;->invalidate(Landroid/graphics/Rect;)V

    .line 235
    :cond_0
    return-object v0
.end method

.method public onFinishInflate()V
    .locals 2

    .prologue
    .line 77
    invoke-super {p0}, Landroid/widget/FrameLayout;->onFinishInflate()V

    .line 79
    sget v0, Lcom/google/android/apps/chrome/R$id;->main_text:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/widget/ContextualSearchControl;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/chrome/widget/ContextualSearchControl;->mSelectionText:Landroid/widget/TextView;

    .line 80
    sget v0, Lcom/google/android/apps/chrome/R$id;->surrounding_text_start:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/widget/ContextualSearchControl;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/chrome/widget/ContextualSearchControl;->mStartText:Landroid/widget/TextView;

    .line 81
    sget v0, Lcom/google/android/apps/chrome/R$id;->surrounding_text_end:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/widget/ContextualSearchControl;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/chrome/widget/ContextualSearchControl;->mEndText:Landroid/widget/TextView;

    .line 83
    new-instance v0, Lcom/google/android/apps/chrome/compositor/resources/dynamics/ViewResourceAdapter;

    sget v1, Lcom/google/android/apps/chrome/R$id;->contextual_search_bar_text:I

    invoke-virtual {p0, v1}, Lcom/google/android/apps/chrome/widget/ContextualSearchControl;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/apps/chrome/compositor/resources/dynamics/ViewResourceAdapter;-><init>(Landroid/view/View;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/widget/ContextualSearchControl;->mResourceAdapter:Lcom/google/android/apps/chrome/compositor/resources/dynamics/ViewResourceAdapter;

    .line 84
    return-void
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 185
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/ContextualSearchControl;->mGestureDetector:Landroid/view/GestureDetector;

    invoke-virtual {v0, p1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 159
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v0

    if-nez v0, :cond_0

    .line 164
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/ContextualSearchControl;->mManagementDelegate:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManagementDelegate;

    invoke-interface {v0, v1}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManagementDelegate;->setSearchContentViewVisibility(Z)V

    .line 180
    :goto_0
    return v1

    .line 168
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/ContextualSearchControl;->mGestureDetector:Landroid/view/GestureDetector;

    invoke-virtual {v0, p1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    .line 170
    iget-object v2, p0, Lcom/google/android/apps/chrome/widget/ContextualSearchControl;->mSwipeHandler:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeHandler;

    if-eqz v2, :cond_2

    .line 171
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    .line 172
    if-eq v2, v1, :cond_1

    const/4 v3, 0x3

    if-ne v2, v3, :cond_2

    :cond_1
    iget-object v2, p0, Lcom/google/android/apps/chrome/widget/ContextualSearchControl;->mForwardingDirection:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;

    sget-object v3, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;->UNKNOWN:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;

    if-eq v2, v3, :cond_2

    .line 174
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/ContextualSearchControl;->mSwipeHandler:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeHandler;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeHandler;->swipeFinished()V

    .line 175
    sget-object v0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;->UNKNOWN:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;

    iput-object v0, p0, Lcom/google/android/apps/chrome/widget/ContextualSearchControl;->mForwardingDirection:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;

    move v0, v1

    :cond_2
    move v1, v0

    .line 180
    goto :goto_0
.end method

.method public setContextualSearchManagementDelegate(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManagementDelegate;)V
    .locals 0

    .prologue
    .line 243
    iput-object p1, p0, Lcom/google/android/apps/chrome/widget/ContextualSearchControl;->mManagementDelegate:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManagementDelegate;

    .line 244
    return-void
.end method

.method public setFirstRunText(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 193
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/widget/ContextualSearchControl;->getContext()Landroid/content/Context;

    move-result-object v0

    sget v1, Lcom/google/android/apps/chrome/R$string;->contextual_search_action_bar:I

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 195
    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/ContextualSearchControl;->mSelectionText:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 196
    return-void
.end method

.method public setResolvedSearchTerm(Ljava/lang/String;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 216
    const/high16 v0, 0x42200000    # 40.0f

    iget v1, p0, Lcom/google/android/apps/chrome/widget/ContextualSearchControl;->mPxToDp:F

    div-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    .line 217
    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/ContextualSearchControl;->mSelectionText:Landroid/widget/TextView;

    invoke-virtual {v1, v0, v2, v0, v2}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 218
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/ContextualSearchControl;->mSelectionText:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 219
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/ContextualSearchControl;->mStartText:Landroid/widget/TextView;

    const-string/jumbo v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 220
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/ContextualSearchControl;->mEndText:Landroid/widget/TextView;

    const-string/jumbo v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 221
    return-void
.end method

.method public setSearchContext(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 205
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/ContextualSearchControl;->mSelectionText:Landroid/widget/TextView;

    invoke-virtual {v0, v1, v1, v1, v1}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 206
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/ContextualSearchControl;->mSelectionText:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 207
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/ContextualSearchControl;->mStartText:Landroid/widget/TextView;

    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 208
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/ContextualSearchControl;->mEndText:Landroid/widget/TextView;

    invoke-virtual {v0, p3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 209
    return-void
.end method

.method public setSwipeHandler(Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeHandler;)V
    .locals 0

    .prologue
    .line 90
    iput-object p1, p0, Lcom/google/android/apps/chrome/widget/ContextualSearchControl;->mSwipeHandler:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeHandler;

    .line 91
    return-void
.end method
