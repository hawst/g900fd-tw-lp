.class public Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerController;
.super Ljava/lang/Object;
.source "RemoteMediaPlayerController.java"

# interfaces
.implements Lcom/google/android/apps/chrome/videofling/MediaRouteController$Listener;
.implements Lcom/google/android/apps/chrome/videofling/TransportControl$Listener;


# static fields
.field static final synthetic $assertionsDisabled:Z

.field private static sInstance:Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerController;


# instance fields
.field private mCastContextApplicationContext:Landroid/content/Context;

.field private mCastTabManager:Lcom/google/android/apps/chrome/videofling/PosterManager;

.field private mCurrentRouteController:Lcom/google/android/apps/chrome/videofling/MediaRouteController;

.field private final mDebug:Z

.field private mDefaultMediaRouteListener:Lcom/google/android/apps/chrome/videofling/MediaRouteListener;

.field private mDefaultRouteController:Lcom/google/android/apps/chrome/videofling/MediaRouteController;

.field private mFirstConnection:Z

.field private mFullscreenedVideoActivity:Ljava/lang/ref/WeakReference;

.field private mLockScreenControl:Lcom/google/android/apps/chrome/videofling/TransportControl;

.field private mNativeRemoteMediaPlayerController:J

.field private mNotificationControl:Lcom/google/android/apps/chrome/videofling/TransportControl;

.field private mVideoIsFullscreen:Z

.field private mYouTubeMediaRouteListener:Lcom/google/android/apps/chrome/videofling/MediaRouteListener;

.field private mYouTubeRouteController:Lcom/google/android/apps/chrome/videofling/MediaRouteController;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 38
    const-class v0, Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerController;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerController;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    .line 199
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 74
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerController;->mVideoIsFullscreen:Z

    .line 76
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerController;->mFirstConnection:Z

    .line 200
    invoke-static {}, Lorg/chromium/base/CommandLine;->getInstance()Lorg/chromium/base/CommandLine;

    move-result-object v0

    const-string/jumbo v1, "enable-cast-debug"

    invoke-virtual {v0, v1}, Lorg/chromium/base/CommandLine;->hasSwitch(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerController;->mDebug:Z

    .line 202
    new-instance v0, Lcom/google/android/apps/chrome/videofling/PosterManager;

    invoke-direct {v0}, Lcom/google/android/apps/chrome/videofling/PosterManager;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerController;->mCastTabManager:Lcom/google/android/apps/chrome/videofling/PosterManager;

    .line 203
    return-void
.end method

.method private createLockScreen()V
    .locals 2

    .prologue
    .line 507
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerController;->mFullscreenedVideoActivity:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerController;->mCurrentRouteController:Lcom/google/android/apps/chrome/videofling/MediaRouteController;

    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/videofling/LockScreenTransportControl;->getOrCreate(Landroid/content/Context;Lcom/google/android/apps/chrome/videofling/MediaRouteController;)Lcom/google/android/apps/chrome/videofling/LockScreenTransportControl;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerController;->mLockScreenControl:Lcom/google/android/apps/chrome/videofling/TransportControl;

    .line 509
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerController;->mLockScreenControl:Lcom/google/android/apps/chrome/videofling/TransportControl;

    if-eqz v0, :cond_0

    .line 510
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerController;->mLockScreenControl:Lcom/google/android/apps/chrome/videofling/TransportControl;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/videofling/TransportControl;->setError(Ljava/lang/String;)V

    .line 511
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerController;->mLockScreenControl:Lcom/google/android/apps/chrome/videofling/TransportControl;

    iget-object v1, p0, Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerController;->mCurrentRouteController:Lcom/google/android/apps/chrome/videofling/MediaRouteController;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->getRouteName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/videofling/TransportControl;->setScreenName(Ljava/lang/String;)V

    .line 512
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerController;->mLockScreenControl:Lcom/google/android/apps/chrome/videofling/TransportControl;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/chrome/videofling/TransportControl;->addListener(Lcom/google/android/apps/chrome/videofling/TransportControl$Listener;)V

    .line 514
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerController;->mLockScreenControl:Lcom/google/android/apps/chrome/videofling/TransportControl;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerController;->mLockScreenControl:Lcom/google/android/apps/chrome/videofling/TransportControl;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerController;->getPoster()Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/videofling/TransportControl;->setPosterBitmap(Landroid/graphics/Bitmap;)V

    .line 515
    :cond_1
    return-void
.end method

.method private createNotificationControl()V
    .locals 2

    .prologue
    .line 492
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerController;->mFullscreenedVideoActivity:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerController;->mCurrentRouteController:Lcom/google/android/apps/chrome/videofling/MediaRouteController;

    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/videofling/NotificationTransportControl;->getOrCreate(Landroid/content/Context;Lcom/google/android/apps/chrome/videofling/MediaRouteController;)Lcom/google/android/apps/chrome/videofling/NotificationTransportControl;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerController;->mNotificationControl:Lcom/google/android/apps/chrome/videofling/TransportControl;

    .line 494
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerController;->mNotificationControl:Lcom/google/android/apps/chrome/videofling/TransportControl;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/videofling/TransportControl;->setError(Ljava/lang/String;)V

    .line 495
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerController;->mNotificationControl:Lcom/google/android/apps/chrome/videofling/TransportControl;

    iget-object v1, p0, Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerController;->mCurrentRouteController:Lcom/google/android/apps/chrome/videofling/MediaRouteController;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->getRouteName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/videofling/TransportControl;->setScreenName(Ljava/lang/String;)V

    .line 496
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerController;->mNotificationControl:Lcom/google/android/apps/chrome/videofling/TransportControl;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/chrome/videofling/TransportControl;->addListener(Lcom/google/android/apps/chrome/videofling/TransportControl$Listener;)V

    .line 497
    return-void
.end method

.method static getIfExists()Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerController;
    .locals 1

    .prologue
    .line 631
    sget-object v0, Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerController;->sInstance:Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerController;

    return-object v0
.end method

.method private getLockScreen()Lcom/google/android/apps/chrome/videofling/TransportControl;
    .locals 1

    .prologue
    .line 500
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerController;->mLockScreenControl:Lcom/google/android/apps/chrome/videofling/TransportControl;

    return-object v0
.end method

.method private getNotification()Lcom/google/android/apps/chrome/videofling/TransportControl;
    .locals 1

    .prologue
    .line 485
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerController;->mNotificationControl:Lcom/google/android/apps/chrome/videofling/TransportControl;

    return-object v0
.end method

.method public static instance(J)Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerController;
    .locals 2

    .prologue
    .line 163
    invoke-static {}, Lorg/chromium/base/ThreadUtils;->assertOnUiThread()V

    .line 165
    sget-object v0, Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerController;->sInstance:Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerController;

    if-nez v0, :cond_0

    .line 166
    new-instance v0, Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerController;

    invoke-direct {v0}, Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerController;-><init>()V

    sput-object v0, Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerController;->sInstance:Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerController;

    .line 169
    :cond_0
    const-wide/16 v0, 0x0

    cmp-long v0, p0, v0

    if-eqz v0, :cond_1

    .line 170
    sget-object v0, Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerController;->sInstance:Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerController;

    invoke-direct {v0, p0, p1}, Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerController;->linkToChromeActivity(J)V

    .line 173
    :cond_1
    sget-object v0, Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerController;->sInstance:Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerController;

    return-object v0
.end method

.method public static isRemotePlaybackEnabled()Z
    .locals 2

    .prologue
    .line 85
    invoke-static {}, Lorg/chromium/base/CommandLine;->getInstance()Lorg/chromium/base/CommandLine;

    move-result-object v0

    const-string/jumbo v1, "disable-cast"

    invoke-virtual {v0, v1}, Lorg/chromium/base/CommandLine;->hasSwitch(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isYouTubeUrl(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 617
    invoke-static {p1}, Lcom/google/android/apps/chrome/videofling/RemoteMediaUtils;->isYouTubeUrl(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method private linkToChromeActivity(J)V
    .locals 3

    .prologue
    .line 211
    iput-wide p1, p0, Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerController;->mNativeRemoteMediaPlayerController:J

    .line 212
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerController;->mFullscreenedVideoActivity:Ljava/lang/ref/WeakReference;

    if-nez v0, :cond_1

    .line 213
    invoke-static {}, Lcom/google/android/apps/chrome/videofling/RemoteMediaUtils;->getCurrentActivity()Lcom/google/android/apps/chrome/ChromeActivity;

    move-result-object v0

    .line 214
    new-instance v1, Ljava/lang/ref/WeakReference;

    invoke-direct {v1, v0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v1, p0, Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerController;->mFullscreenedVideoActivity:Ljava/lang/ref/WeakReference;

    .line 216
    sget-boolean v1, Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerController;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 217
    :cond_0
    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ChromeActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerController;->mCastContextApplicationContext:Landroid/content/Context;

    .line 219
    new-instance v0, Lcom/google/android/apps/chrome/videofling/MediaRouteListener;

    invoke-static {}, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->buildDefaultMediaRouteSelector()Landroid/support/v7/media/e;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerController;->mCastContextApplicationContext:Landroid/content/Context;

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/chrome/videofling/MediaRouteListener;-><init>(Landroid/support/v7/media/e;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerController;->mDefaultMediaRouteListener:Lcom/google/android/apps/chrome/videofling/MediaRouteListener;

    .line 222
    new-instance v0, Lcom/google/android/apps/chrome/videofling/MediaRouteListener;

    invoke-static {}, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->buildYouTubeMediaRouteSelector()Landroid/support/v7/media/e;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerController;->mCastContextApplicationContext:Landroid/content/Context;

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/chrome/videofling/MediaRouteListener;-><init>(Landroid/support/v7/media/e;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerController;->mYouTubeMediaRouteListener:Lcom/google/android/apps/chrome/videofling/MediaRouteListener;

    .line 226
    :cond_1
    return-void
.end method

.method public static native nativeGetBrowserUserAgent()Ljava/lang/String;
.end method

.method private native nativeOnPlaybackFinished(JII)V
.end method

.method private native nativeOnRouteAvailabilityChanged(JIIZ)V
.end method

.method private native nativeOnRouteSelected(JIILjava/lang/String;)V
.end method

.method private native nativeOnRouteUnselected(JII)V
.end method

.method private onPlayerCreated(IILjava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 135
    invoke-static {p1}, Lcom/google/android/apps/chrome/videofling/RemoteMediaUtils;->getTabById(I)Lorg/chromium/chrome/browser/Tab;

    move-result-object v0

    .line 136
    if-nez v0, :cond_0

    .line 143
    :goto_0
    return-void

    .line 138
    :cond_0
    invoke-direct {p0, p4}, Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerController;->isYouTubeUrl(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 139
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerController;->mYouTubeMediaRouteListener:Lcom/google/android/apps/chrome/videofling/MediaRouteListener;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/chrome/videofling/MediaRouteListener;->addListeningNativePlayer(II)V

    goto :goto_0

    .line 141
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerController;->mDefaultMediaRouteListener:Lcom/google/android/apps/chrome/videofling/MediaRouteListener;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/chrome/videofling/MediaRouteListener;->addListeningNativePlayer(II)V

    goto :goto_0
.end method

.method private onPlayerDestroyed(II)V
    .locals 1

    .prologue
    .line 153
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerController;->mYouTubeMediaRouteListener:Lcom/google/android/apps/chrome/videofling/MediaRouteListener;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/chrome/videofling/MediaRouteListener;->removeListeningNativePlayer(II)V

    .line 154
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerController;->mDefaultMediaRouteListener:Lcom/google/android/apps/chrome/videofling/MediaRouteListener;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/chrome/videofling/MediaRouteListener;->removeListeningNativePlayer(II)V

    .line 155
    return-void
.end method

.method private onStateReset(Lcom/google/android/apps/chrome/videofling/MediaRouteController;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 231
    invoke-virtual {p1, p2}, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->initialize(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 247
    :goto_0
    return-void

    .line 233
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerController;->mFirstConnection:Z

    if-eqz v0, :cond_1

    .line 234
    invoke-virtual {p1}, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->reconnectAnyExistingRoute()V

    .line 235
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerController;->mFirstConnection:Z

    .line 238
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerController;->mNotificationControl:Lcom/google/android/apps/chrome/videofling/TransportControl;

    if-eqz v0, :cond_2

    .line 239
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerController;->mNotificationControl:Lcom/google/android/apps/chrome/videofling/TransportControl;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/videofling/TransportControl;->setRouteController(Lcom/google/android/apps/chrome/videofling/MediaRouteController;)V

    .line 241
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerController;->mLockScreenControl:Lcom/google/android/apps/chrome/videofling/TransportControl;

    if-eqz v0, :cond_3

    .line 242
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerController;->mLockScreenControl:Lcom/google/android/apps/chrome/videofling/TransportControl;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/videofling/TransportControl;->setRouteController(Lcom/google/android/apps/chrome/videofling/MediaRouteController;)V

    .line 244
    :cond_3
    invoke-virtual {p1}, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->prepareMediaRoute()V

    .line 246
    invoke-virtual {p1, p0}, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->addListener(Lcom/google/android/apps/chrome/videofling/MediaRouteController$Listener;)V

    goto :goto_0
.end method

.method private requestRemotePlayback(IILjava/lang/String;)V
    .locals 7

    .prologue
    .line 314
    invoke-static {}, Lcom/google/android/apps/chrome/videofling/RemoteMediaUtils;->getCurrentActivity()Lcom/google/android/apps/chrome/ChromeActivity;

    move-result-object v6

    .line 315
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, v6}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerController;->mFullscreenedVideoActivity:Ljava/lang/ref/WeakReference;

    .line 318
    invoke-virtual {p0, p3}, Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerController;->getMediaRouteController(Ljava/lang/String;)Lcom/google/android/apps/chrome/videofling/MediaRouteController;

    move-result-object v1

    .line 320
    const/4 v4, -0x1

    .line 321
    invoke-virtual {v1}, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->getOwnerTab()Lorg/chromium/chrome/browser/Tab;

    move-result-object v0

    .line 323
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/Tab;->getId()I

    move-result v4

    .line 324
    :cond_0
    invoke-virtual {v1}, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->getNativePlayerId()I

    move-result v2

    .line 326
    invoke-direct {p0, v1, p3}, Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerController;->onStateReset(Lcom/google/android/apps/chrome/videofling/MediaRouteController;Ljava/lang/String;)V

    move-object v0, p0

    move v3, p2

    move v5, p1

    .line 327
    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerController;->shouldResetState(Lcom/google/android/apps/chrome/videofling/MediaRouteController;IIII)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 328
    invoke-static {p1}, Lcom/google/android/apps/chrome/videofling/RemoteMediaUtils;->getTabById(I)Lorg/chromium/chrome/browser/Tab;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->setOwnerTab(Lorg/chromium/chrome/browser/Tab;)V

    .line 329
    invoke-virtual {v1, p2}, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->setNativePlayerId(I)V

    .line 330
    invoke-direct {p0, v1, v6}, Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerController;->showMediaRouteDialog(Lcom/google/android/apps/chrome/videofling/MediaRouteController;Landroid/app/Activity;)V

    .line 333
    :cond_1
    return-void
.end method

.method private requestRemotePlaybackControl(II)V
    .locals 2

    .prologue
    .line 349
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerController;->mCurrentRouteController:Lcom/google/android/apps/chrome/videofling/MediaRouteController;

    if-nez v0, :cond_1

    .line 355
    :cond_0
    :goto_0
    return-void

    .line 350
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerController;->mCurrentRouteController:Lcom/google/android/apps/chrome/videofling/MediaRouteController;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->getOwnerTab()Lorg/chromium/chrome/browser/Tab;

    move-result-object v0

    .line 351
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/Tab;->getId()I

    move-result v0

    if-ne v0, p1, :cond_0

    .line 352
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerController;->mCurrentRouteController:Lcom/google/android/apps/chrome/videofling/MediaRouteController;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->getNativePlayerId()I

    move-result v0

    if-ne v0, p2, :cond_0

    .line 354
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerController;->mCurrentRouteController:Lcom/google/android/apps/chrome/videofling/MediaRouteController;

    invoke-static {}, Lcom/google/android/apps/chrome/videofling/RemoteMediaUtils;->getCurrentActivity()Lcom/google/android/apps/chrome/ChromeActivity;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerController;->showMediaRouteControlDialog(Lcom/google/android/apps/chrome/videofling/MediaRouteController;Landroid/app/Activity;)V

    goto :goto_0
.end method

.method private resetPlayingVideo()V
    .locals 2

    .prologue
    .line 621
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerController;->mNotificationControl:Lcom/google/android/apps/chrome/videofling/TransportControl;

    if-eqz v0, :cond_0

    .line 622
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerController;->mNotificationControl:Lcom/google/android/apps/chrome/videofling/TransportControl;

    iget-object v1, p0, Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerController;->mCurrentRouteController:Lcom/google/android/apps/chrome/videofling/MediaRouteController;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/videofling/TransportControl;->setRouteController(Lcom/google/android/apps/chrome/videofling/MediaRouteController;)V

    .line 624
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerController;->mLockScreenControl:Lcom/google/android/apps/chrome/videofling/TransportControl;

    if-eqz v0, :cond_1

    .line 625
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerController;->mLockScreenControl:Lcom/google/android/apps/chrome/videofling/TransportControl;

    iget-object v1, p0, Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerController;->mCurrentRouteController:Lcom/google/android/apps/chrome/videofling/MediaRouteController;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/videofling/TransportControl;->setRouteController(Lcom/google/android/apps/chrome/videofling/MediaRouteController;)V

    .line 627
    :cond_1
    return-void
.end method

.method private shouldResetState(Lcom/google/android/apps/chrome/videofling/MediaRouteController;IIII)Z
    .locals 1

    .prologue
    .line 302
    invoke-virtual {p1}, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->isBeingCast()Z

    move-result v0

    if-eqz v0, :cond_0

    if-ne p2, p3, :cond_0

    if-eq p4, p5, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private showMediaRouteControlDialog(Lcom/google/android/apps/chrome/videofling/MediaRouteController;Landroid/app/Activity;)V
    .locals 3

    .prologue
    .line 379
    check-cast p2, Landroid/support/v4/app/k;

    invoke-virtual {p2}, Landroid/support/v4/app/k;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    .line 380
    if-nez v0, :cond_0

    .line 381
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "The activity must be a subclass of FragmentActivity"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 383
    :cond_0
    new-instance v1, Lcom/google/android/apps/chrome/videofling/ChromeMediaRouteDialogFactory;

    invoke-direct {v1, p1}, Lcom/google/android/apps/chrome/videofling/ChromeMediaRouteDialogFactory;-><init>(Lcom/google/android/apps/chrome/third_party/ChromeMediaRouteControllerDialog$DisconnectListener;)V

    .line 385
    const-string/jumbo v2, "android.support.v7.mediarouter:MediaRouteControllerDialogFragment"

    invoke-virtual {v0, v2}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 387
    const-string/jumbo v0, "VideoFling"

    const-string/jumbo v1, "showDialog(): Route controller dialog already showing!"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 393
    :goto_0
    return-void

    .line 390
    :cond_1
    invoke-virtual {v1}, Landroid/support/v7/app/B;->onCreateControllerDialogFragment()Landroid/support/v7/app/A;

    move-result-object v1

    .line 392
    const-string/jumbo v2, "android.support.v7.mediarouter:MediaRouteControllerDialogFragment"

    invoke-virtual {v1, v0, v2}, Landroid/support/v7/app/A;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private showMediaRouteDialog(Lcom/google/android/apps/chrome/videofling/MediaRouteController;Landroid/app/Activity;)V
    .locals 3

    .prologue
    .line 359
    check-cast p2, Landroid/support/v4/app/k;

    invoke-virtual {p2}, Landroid/support/v4/app/k;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    .line 360
    if-nez v0, :cond_0

    .line 361
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "The activity must be a subclass of FragmentActivity"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 364
    :cond_0
    new-instance v1, Lcom/google/android/apps/chrome/videofling/ChromeMediaRouteDialogFactory;

    invoke-direct {v1, p1}, Lcom/google/android/apps/chrome/videofling/ChromeMediaRouteDialogFactory;-><init>(Lcom/google/android/apps/chrome/third_party/ChromeMediaRouteControllerDialog$DisconnectListener;)V

    .line 366
    const-string/jumbo v2, "android.support.v7.mediarouter:MediaRouteChooserDialogFragment"

    invoke-virtual {v0, v2}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 368
    const-string/jumbo v0, "VideoFling"

    const-string/jumbo v1, "showDialog(): Route chooser dialog already showing!"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 375
    :goto_0
    return-void

    .line 371
    :cond_1
    invoke-virtual {v1}, Landroid/support/v7/app/B;->onCreateChooserDialogFragment()Landroid/support/v7/app/u;

    move-result-object v1

    .line 373
    invoke-virtual {p1}, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->buildMediaRouteSelector()Landroid/support/v7/media/e;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/support/v7/app/u;->setRouteSelector(Landroid/support/v7/media/e;)V

    .line 374
    const-string/jumbo v2, "android.support.v7.mediarouter:MediaRouteChooserDialogFragment"

    invoke-virtual {v1, v0, v2}, Landroid/support/v7/app/u;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private showMessageToast(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 603
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerController;->mCastContextApplicationContext:Landroid/content/Context;

    const/4 v1, 0x0

    invoke-static {v0, p1, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    .line 604
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 605
    return-void
.end method


# virtual methods
.method public getCurrentlyPlayingMediaRouteController()Lcom/google/android/apps/chrome/videofling/MediaRouteController;
    .locals 1

    .prologue
    .line 477
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerController;->mCurrentRouteController:Lcom/google/android/apps/chrome/videofling/MediaRouteController;

    return-object v0
.end method

.method public getDefaultMediaRouteController()Lcom/google/android/apps/chrome/videofling/MediaRouteController;
    .locals 1

    .prologue
    .line 264
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerController;->mDefaultRouteController:Lcom/google/android/apps/chrome/videofling/MediaRouteController;

    if-nez v0, :cond_0

    .line 265
    new-instance v0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;

    invoke-direct {v0}, Lcom/google/android/apps/chrome/videofling/MediaRouteController;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerController;->mDefaultRouteController:Lcom/google/android/apps/chrome/videofling/MediaRouteController;

    .line 267
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerController;->mDefaultRouteController:Lcom/google/android/apps/chrome/videofling/MediaRouteController;

    return-object v0
.end method

.method public getDuration()I
    .locals 1

    .prologue
    .line 457
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerController;->mCurrentRouteController:Lcom/google/android/apps/chrome/videofling/MediaRouteController;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->getDuration()I

    move-result v0

    return v0
.end method

.method public getMediaRouteController(Ljava/lang/String;)Lcom/google/android/apps/chrome/videofling/MediaRouteController;
    .locals 1

    .prologue
    .line 250
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerController;->isYouTubeUrl(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 251
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerController;->mYouTubeRouteController:Lcom/google/android/apps/chrome/videofling/MediaRouteController;

    if-nez v0, :cond_0

    .line 252
    new-instance v0, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;

    invoke-direct {v0}, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerController;->mYouTubeRouteController:Lcom/google/android/apps/chrome/videofling/MediaRouteController;

    .line 254
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerController;->mYouTubeRouteController:Lcom/google/android/apps/chrome/videofling/MediaRouteController;

    .line 259
    :goto_0
    return-object v0

    .line 256
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerController;->mDefaultRouteController:Lcom/google/android/apps/chrome/videofling/MediaRouteController;

    if-nez v0, :cond_2

    .line 257
    new-instance v0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;

    invoke-direct {v0}, Lcom/google/android/apps/chrome/videofling/MediaRouteController;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerController;->mDefaultRouteController:Lcom/google/android/apps/chrome/videofling/MediaRouteController;

    .line 259
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerController;->mDefaultRouteController:Lcom/google/android/apps/chrome/videofling/MediaRouteController;

    goto :goto_0
.end method

.method public getPosition()I
    .locals 1

    .prologue
    .line 450
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerController;->mCurrentRouteController:Lcom/google/android/apps/chrome/videofling/MediaRouteController;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->getPosition()I

    move-result v0

    return v0
.end method

.method public getPoster()Landroid/graphics/Bitmap;
    .locals 3

    .prologue
    .line 120
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerController;->mCurrentRouteController:Lcom/google/android/apps/chrome/videofling/MediaRouteController;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerController;->mCurrentRouteController:Lcom/google/android/apps/chrome/videofling/MediaRouteController;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->getOwnerTab()Lorg/chromium/chrome/browser/Tab;

    move-result-object v0

    if-nez v0, :cond_1

    .line 121
    :cond_0
    const/4 v0, 0x0

    .line 123
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerController;->mCastTabManager:Lcom/google/android/apps/chrome/videofling/PosterManager;

    iget-object v1, p0, Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerController;->mCurrentRouteController:Lcom/google/android/apps/chrome/videofling/MediaRouteController;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->getOwnerTab()Lorg/chromium/chrome/browser/Tab;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerController;->mCurrentRouteController:Lcom/google/android/apps/chrome/videofling/MediaRouteController;

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->getNativePlayerId()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/chrome/videofling/PosterManager;->getPoster(Lorg/chromium/chrome/browser/Tab;I)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0
.end method

.method public handleVolumeKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 183
    iget-object v1, p0, Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerController;->mCurrentRouteController:Lcom/google/android/apps/chrome/videofling/MediaRouteController;

    if-nez v1, :cond_1

    .line 193
    :cond_0
    :goto_0
    return v0

    .line 185
    :cond_1
    invoke-static {}, Lcom/google/android/apps/chrome/videofling/RemoteMediaUtils;->getCurrentActivity()Lcom/google/android/apps/chrome/ChromeActivity;

    move-result-object v1

    .line 188
    if-eqz v1, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerController;->mCurrentRouteController:Lcom/google/android/apps/chrome/videofling/MediaRouteController;

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->getOwnerTab()Lorg/chromium/chrome/browser/Tab;

    move-result-object v2

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/ChromeActivity;->getCurrentTab()Lcom/google/android/apps/chrome/tab/ChromeTab;

    move-result-object v1

    if-ne v2, v1, :cond_0

    .line 193
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerController;->mCurrentRouteController:Lcom/google/android/apps/chrome/videofling/MediaRouteController;

    invoke-static {v0, p1}, Lcom/google/android/apps/chrome/videofling/RemoteMediaUtils;->handleVolumeKeyEvent(Lcom/google/android/apps/chrome/videofling/MediaRouteController;Landroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public isPlaying()Z
    .locals 1

    .prologue
    .line 464
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerController;->mCurrentRouteController:Lcom/google/android/apps/chrome/videofling/MediaRouteController;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->isPlaying()Z

    move-result v0

    return v0
.end method

.method public isRemotePlaybackAvailable()Z
    .locals 1

    .prologue
    .line 275
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerController;->mCurrentRouteController:Lcom/google/android/apps/chrome/videofling/MediaRouteController;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerController;->mCurrentRouteController:Lcom/google/android/apps/chrome/videofling/MediaRouteController;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->isRemotePlaybackAvailable()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isRemotePlaybackPreferredForFrame(Ljava/lang/String;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 285
    invoke-static {}, Lcom/google/android/apps/chrome/videofling/RemoteMediaUtils;->getCurrentActivity()Lcom/google/android/apps/chrome/ChromeActivity;

    move-result-object v1

    .line 286
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/ChromeActivity;->getCurrentTab()Lcom/google/android/apps/chrome/tab/ChromeTab;

    move-result-object v1

    if-nez v1, :cond_1

    .line 288
    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerController;->mCurrentRouteController:Lcom/google/android/apps/chrome/videofling/MediaRouteController;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerController;->mCurrentRouteController:Lcom/google/android/apps/chrome/videofling/MediaRouteController;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->routeIsDefaultRoute()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerController;->mCurrentRouteController:Lcom/google/android/apps/chrome/videofling/MediaRouteController;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->currentRouteSupportsRemotePlayback()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerController;->mCurrentRouteController:Lcom/google/android/apps/chrome/videofling/MediaRouteController;

    invoke-virtual {v1, p1}, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->currentRouteSupportsDomain(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public onDurationUpdated(I)V
    .locals 0

    .prologue
    .line 554
    return-void
.end method

.method public onError(ILjava/lang/String;)V
    .locals 1

    .prologue
    .line 545
    const/4 v0, 0x2

    if-ne p1, v0, :cond_0

    .line 546
    invoke-direct {p0, p2}, Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerController;->showMessageToast(Ljava/lang/String;)V

    .line 548
    :cond_0
    return-void
.end method

.method public onMute()V
    .locals 0

    .prologue
    .line 635
    return-void
.end method

.method public onPause()V
    .locals 0

    .prologue
    .line 639
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerController;->pause()V

    .line 640
    return-void
.end method

.method public onPlay()V
    .locals 0

    .prologue
    .line 644
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerController;->resume()V

    .line 645
    return-void
.end method

.method public onPlaybackStateChanged(Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;Lorg/chromium/chrome/browser/Tab;I)V
    .locals 4

    .prologue
    .line 529
    sget-object v0, Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;->PLAYING:Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;

    if-eq p2, v0, :cond_0

    sget-object v0, Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;->LOADING:Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;

    if-eq p2, v0, :cond_0

    sget-object v0, Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;->PAUSED:Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;

    if-ne p2, v0, :cond_3

    .line 531
    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerController;->getNotification()Lcom/google/android/apps/chrome/videofling/TransportControl;

    move-result-object v0

    .line 532
    if-eqz v0, :cond_1

    invoke-virtual {v0, p2}, Lcom/google/android/apps/chrome/videofling/TransportControl;->show(Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;)V

    .line 533
    :cond_1
    invoke-direct {p0}, Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerController;->getLockScreen()Lcom/google/android/apps/chrome/videofling/TransportControl;

    move-result-object v0

    .line 534
    if-eqz v0, :cond_2

    invoke-virtual {v0, p2}, Lcom/google/android/apps/chrome/videofling/TransportControl;->show(Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;)V

    .line 541
    :cond_2
    :goto_0
    return-void

    .line 535
    :cond_3
    sget-object v0, Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;->FINISHED:Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;

    if-eq p2, v0, :cond_4

    sget-object v0, Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;->INVALIDATED:Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;

    if-ne p2, v0, :cond_2

    .line 536
    :cond_4
    iget-wide v0, p0, Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerController;->mNativeRemoteMediaPlayerController:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_2

    if-eqz p3, :cond_2

    .line 537
    iget-wide v0, p0, Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerController;->mNativeRemoteMediaPlayerController:J

    invoke-virtual {p3}, Lorg/chromium/chrome/browser/Tab;->getId()I

    move-result v2

    invoke-direct {p0, v0, v1, v2, p4}, Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerController;->nativeOnPlaybackFinished(JII)V

    goto :goto_0
.end method

.method public onPositionChanged(I)V
    .locals 0

    .prologue
    .line 557
    return-void
.end method

.method public onPrepared(Lcom/google/android/apps/chrome/videofling/MediaRouteController;)V
    .locals 1

    .prologue
    .line 521
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerController;->mCastTabManager:Lcom/google/android/apps/chrome/videofling/PosterManager;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/videofling/PosterManager;->clearCurrentBitmap()V

    .line 523
    sget-object v0, Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;->PLAYING:Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;

    invoke-virtual {p0, v0, p1}, Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerController;->startNotificationAndLockScreen(Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;Lcom/google/android/apps/chrome/videofling/MediaRouteController;)V

    .line 524
    return-void
.end method

.method public onRouteAvailabilityChanged(IIZ)V
    .locals 7

    .prologue
    .line 598
    iget-wide v2, p0, Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerController;->mNativeRemoteMediaPlayerController:J

    move-object v1, p0

    move v4, p1

    move v5, p2

    move v6, p3

    invoke-direct/range {v1 .. v6}, Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerController;->nativeOnRouteAvailabilityChanged(JIIZ)V

    .line 600
    return-void
.end method

.method public onRouteSelected(Ljava/lang/String;Lorg/chromium/chrome/browser/Tab;ILcom/google/android/apps/chrome/videofling/MediaRouteController;)V
    .locals 7

    .prologue
    .line 565
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerController;->mCurrentRouteController:Lcom/google/android/apps/chrome/videofling/MediaRouteController;

    if-eq v0, p4, :cond_0

    .line 566
    iput-object p4, p0, Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerController;->mCurrentRouteController:Lcom/google/android/apps/chrome/videofling/MediaRouteController;

    .line 567
    invoke-direct {p0}, Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerController;->resetPlayingVideo()V

    .line 570
    :cond_0
    iget-wide v0, p0, Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerController;->mNativeRemoteMediaPlayerController:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_2

    if-eqz p2, :cond_2

    .line 571
    const-string/jumbo v6, "Casting to Chromecast"

    .line 572
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerController;->mCastContextApplicationContext:Landroid/content/Context;

    if-eqz v0, :cond_1

    .line 573
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerController;->mCastContextApplicationContext:Landroid/content/Context;

    sget v1, Lcom/google/android/apps/chrome/R$string;->athome_casting_video:I

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    .line 576
    :cond_1
    iget-wide v2, p0, Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerController;->mNativeRemoteMediaPlayerController:J

    invoke-virtual {p2}, Lorg/chromium/chrome/browser/Tab;->getId()I

    move-result v4

    move-object v1, p0

    move v5, p3

    invoke-direct/range {v1 .. v6}, Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerController;->nativeOnRouteSelected(JIILjava/lang/String;)V

    .line 579
    :cond_2
    return-void
.end method

.method public onRouteUnselected(Lorg/chromium/chrome/browser/Tab;ILcom/google/android/apps/chrome/videofling/MediaRouteController;)V
    .locals 4

    .prologue
    .line 587
    iget-wide v0, p0, Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerController;->mNativeRemoteMediaPlayerController:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    .line 588
    iget-wide v0, p0, Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerController;->mNativeRemoteMediaPlayerController:J

    invoke-virtual {p1}, Lorg/chromium/chrome/browser/Tab;->getId()I

    move-result v2

    invoke-direct {p0, v0, v1, v2, p2}, Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerController;->nativeOnPlaybackFinished(JII)V

    .line 590
    iget-wide v0, p0, Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerController;->mNativeRemoteMediaPlayerController:J

    invoke-virtual {p1}, Lorg/chromium/chrome/browser/Tab;->getId()I

    move-result v2

    invoke-direct {p0, v0, v1, v2, p2}, Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerController;->nativeOnRouteUnselected(JII)V

    .line 593
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p3, v0}, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->setOwnerTab(Lorg/chromium/chrome/browser/Tab;)V

    .line 594
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerController;->mCastTabManager:Lcom/google/android/apps/chrome/videofling/PosterManager;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/videofling/PosterManager;->clearCurrentBitmap()V

    .line 595
    return-void
.end method

.method public onSeek(I)V
    .locals 0

    .prologue
    .line 649
    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerController;->seekTo(I)V

    .line 650
    return-void
.end method

.method public onSeekCompleted()V
    .locals 0

    .prologue
    .line 551
    return-void
.end method

.method public onSelect(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 654
    invoke-static {p1}, Lcom/google/android/apps/chrome/videofling/ExpandedControllerActivity;->startActivity(Landroid/content/Context;)V

    .line 655
    return-void
.end method

.method public onStop()V
    .locals 1

    .prologue
    .line 659
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerController;->mCurrentRouteController:Lcom/google/android/apps/chrome/videofling/MediaRouteController;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->release()V

    .line 660
    return-void
.end method

.method public onTitleChanged(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 560
    return-void
.end method

.method public onUnmute()V
    .locals 0

    .prologue
    .line 663
    return-void
.end method

.method public pause()V
    .locals 1

    .prologue
    .line 437
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerController;->mCurrentRouteController:Lcom/google/android/apps/chrome/videofling/MediaRouteController;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->pause()V

    .line 438
    return-void
.end method

.method public resume()V
    .locals 1

    .prologue
    .line 399
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerController;->mCurrentRouteController:Lcom/google/android/apps/chrome/videofling/MediaRouteController;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->resume()V

    .line 400
    return-void
.end method

.method public seekTo(I)V
    .locals 1

    .prologue
    .line 473
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerController;->mCurrentRouteController:Lcom/google/android/apps/chrome/videofling/MediaRouteController;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->seekTo(I)V

    .line 474
    return-void
.end method

.method public setCurrentMediaRouteController(Lcom/google/android/apps/chrome/videofling/MediaRouteController;)V
    .locals 0

    .prologue
    .line 481
    iput-object p1, p0, Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerController;->mCurrentRouteController:Lcom/google/android/apps/chrome/videofling/MediaRouteController;

    .line 482
    return-void
.end method

.method public setNativePlayer(IILjava/lang/String;)V
    .locals 2

    .prologue
    .line 408
    invoke-static {p1}, Lcom/google/android/apps/chrome/videofling/RemoteMediaUtils;->getTabById(I)Lorg/chromium/chrome/browser/Tab;

    move-result-object v0

    .line 409
    if-nez v0, :cond_0

    .line 415
    :goto_0
    return-void

    .line 411
    :cond_0
    invoke-virtual {p0, p3}, Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerController;->getMediaRouteController(Ljava/lang/String;)Lcom/google/android/apps/chrome/videofling/MediaRouteController;

    move-result-object v1

    .line 413
    invoke-virtual {v1, v0}, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->setOwnerTab(Lorg/chromium/chrome/browser/Tab;)V

    .line 414
    invoke-virtual {v1, p2}, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->setNativePlayerId(I)V

    goto :goto_0
.end method

.method public setPosterBitmap(Ljava/lang/String;Landroid/graphics/Bitmap;)V
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerController;->mCastTabManager:Lcom/google/android/apps/chrome/videofling/PosterManager;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/chrome/videofling/PosterManager;->setPosterBitmap(Ljava/lang/String;Landroid/graphics/Bitmap;)V

    .line 99
    return-void
.end method

.method public setPosterUrlForPlayer(IILjava/lang/String;)V
    .locals 2

    .prologue
    .line 111
    invoke-static {p1}, Lcom/google/android/apps/chrome/videofling/RemoteMediaUtils;->getTabById(I)Lorg/chromium/chrome/browser/Tab;

    move-result-object v0

    .line 112
    if-nez v0, :cond_0

    .line 114
    :goto_0
    return-void

    .line 113
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerController;->mCastTabManager:Lcom/google/android/apps/chrome/videofling/PosterManager;

    invoke-virtual {v1, v0, p2, p3}, Lcom/google/android/apps/chrome/videofling/PosterManager;->setPosterUrlForPlayer(Lorg/chromium/chrome/browser/Tab;ILjava/lang/String;)V

    goto :goto_0
.end method

.method public startNotificationAndLockScreen(Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;Lcom/google/android/apps/chrome/videofling/MediaRouteController;)V
    .locals 1

    .prologue
    .line 425
    iput-object p2, p0, Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerController;->mCurrentRouteController:Lcom/google/android/apps/chrome/videofling/MediaRouteController;

    .line 426
    invoke-direct {p0}, Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerController;->createNotificationControl()V

    .line 427
    invoke-direct {p0}, Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerController;->getNotification()Lcom/google/android/apps/chrome/videofling/TransportControl;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/videofling/TransportControl;->show(Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;)V

    .line 428
    invoke-direct {p0}, Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerController;->createLockScreen()V

    .line 429
    invoke-direct {p0}, Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerController;->getLockScreen()Lcom/google/android/apps/chrome/videofling/TransportControl;

    move-result-object v0

    .line 430
    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/videofling/TransportControl;->show(Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;)V

    .line 431
    :cond_0
    return-void
.end method
