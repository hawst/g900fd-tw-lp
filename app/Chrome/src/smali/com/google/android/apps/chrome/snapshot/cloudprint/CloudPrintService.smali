.class public Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintService;
.super Lcom/google/android/apps/chrome/snapshot/IntentServiceWithWakeLock;
.source "CloudPrintService.java"


# static fields
.field public static final ACTION_CONTROL:Ljava/lang/String; = "com.google.android.apps.chrome.snapshot.cloudprint.ACTION_CONTROL"

.field public static final ACTION_FETCH:Ljava/lang/String; = "com.google.android.apps.chrome.snapshot.cloudprint.ACTION_FETCH"

.field public static final ACTION_SYNC_STATE_WITH_SERVER:Ljava/lang/String; = "com.google.android.apps.chrome.snapshot.cloudprint.ACTION_SYNC_STATE_WITH_SERVER"

.field public static final EXTRA_C2DM_REGISTRATION_ID:Ljava/lang/String; = "com.google.android.apps.chrome.snapshot.cloudprint.EXTRA_C2DM_REGISTRATION_ID"

.field private static sHttpClientFactory:Lcom/google/android/apps/chrome/utilities/HttpClientFactory;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 80
    const/4 v0, 0x0

    sput-object v0, Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintService;->sHttpClientFactory:Lcom/google/android/apps/chrome/utilities/HttpClientFactory;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 88
    const-string/jumbo v0, "CloudPrintService"

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/snapshot/IntentServiceWithWakeLock;-><init>(Ljava/lang/String;)V

    .line 89
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintService;->setIntentRedelivery(Z)V

    .line 90
    return-void
.end method

.method private clearNeedsUpdatingAndStoreApplicationVersionAndTimestamp()V
    .locals 2

    .prologue
    .line 219
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/snapshot/SnapshotSettings;->setNeedsUpdating(Landroid/content/Context;Z)V

    .line 220
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lorg/chromium/base/BuildInfo;->getPackageVersionName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/snapshot/SnapshotSettings;->setApplicationVersion(Landroid/content/Context;Ljava/lang/String;)V

    .line 222
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintService;->setLastUpdatedTimestamp(Landroid/content/Context;)V

    .line 223
    return-void
.end method

.method public static createControlIntent(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJobStatus;)Landroid/content/Intent;
    .locals 3

    .prologue
    .line 258
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 259
    const-string/jumbo v1, "com.google.android.apps.chrome.snapshot.cloudprint.ACTION_CONTROL"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 260
    const-string/jumbo v1, "com.google.android.apps.chrome.snapshot.cloudprint.EXTRA_JOB_ID"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 261
    const-string/jumbo v1, "com.google.android.apps.chrome.snapshot.cloudprint.EXTRA_STATUS"

    invoke-virtual {p2}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJobStatus;->getStatus()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 262
    return-object v0
.end method

.method public static createDeleteJobIntent(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 286
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 287
    const-string/jumbo v1, "com.google.android.apps.chrome.snapshot.cloudprint.ACTION_DELETE_JOB"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 288
    const-string/jumbo v1, "com.google.android.apps.chrome.snapshot.cloudprint.EXTRA_JOB_ID"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 289
    return-object v0
.end method

.method public static createFetchIntent(Landroid/content/Context;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 243
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 244
    const-string/jumbo v1, "com.google.android.apps.chrome.snapshot.cloudprint.ACTION_FETCH"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 245
    return-object v0
.end method

.method public static createSyncStateWithServerIntent(Landroid/content/Context;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 143
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 144
    const-string/jumbo v1, "com.google.android.apps.chrome.snapshot.cloudprint.ACTION_SYNC_STATE_WITH_SERVER"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 145
    return-object v0
.end method

.method public static createSyncStateWithServerWithC2dmRegistrationIdIntent(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 150
    invoke-static {p0}, Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintService;->createSyncStateWithServerIntent(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    .line 151
    const-string/jumbo v1, "com.google.android.apps.chrome.snapshot.cloudprint.EXTRA_C2DM_REGISTRATION_ID"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 152
    return-object v0
.end method

.method public static getDownloadRequest(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;)Lcom/google/android/apps/chrome/snapshot/DownloadManagerRequestData;
    .locals 1

    .prologue
    .line 106
    invoke-static {p0, p1, p2}, Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintClient;->createDownloadRequest(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;)Lcom/google/android/apps/chrome/snapshot/DownloadManagerRequestData;

    move-result-object v0

    return-object v0
.end method

.method private getLocalPrinterData(Landroid/content/Intent;)Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;
    .locals 2

    .prologue
    .line 232
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintService;->getGcmRegistrationId()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterDataFactory;->createDefaultPrinterData(Landroid/content/Context;Ljava/lang/String;)Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;

    move-result-object v0

    .line 235
    const-string/jumbo v1, "com.google.android.apps.chrome.snapshot.cloudprint.EXTRA_C2DM_REGISTRATION_ID"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 236
    const-string/jumbo v1, "com.google.android.apps.chrome.snapshot.cloudprint.EXTRA_C2DM_REGISTRATION_ID"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->withC2dmRegistrationIdTag(Ljava/lang/String;)Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;

    move-result-object v0

    .line 239
    :cond_0
    return-object v0
.end method

.method private handleControl(Landroid/content/Intent;)V
    .locals 5

    .prologue
    .line 266
    const-string/jumbo v0, "com.google.android.apps.chrome.snapshot.cloudprint.EXTRA_STATUS"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJobStatus;->valueOf(Ljava/lang/String;)Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJobStatus;

    move-result-object v0

    .line 267
    const-string/jumbo v1, "com.google.android.apps.chrome.snapshot.cloudprint.EXTRA_JOB_ID"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 268
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 269
    :cond_0
    const-string/jumbo v1, "CloudPrintService"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "handleControl(): Unable to find jobid when trying to set print job status to "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 283
    :goto_0
    return-void

    .line 273
    :cond_1
    const-string/jumbo v2, "CloudPrintService"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "handleControl(): Trying to set print job status to "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, " for print job = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 275
    invoke-virtual {p0, v1, v0}, Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintService;->controlPrintJob(Ljava/lang/String;Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJobStatus;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 276
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintService;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2, v1, v0}, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;->createPrintJobStatusChangedIntent(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJobStatus;)Landroid/content/Intent;

    move-result-object v0

    .line 279
    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintService;->startServiceWithWakeLockInternal(Landroid/content/Intent;)V

    goto :goto_0

    .line 281
    :cond_2
    const-string/jumbo v2, "CloudPrintService"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "Unable to set job status to "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v3, " for jobid "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private handleDeleteJob(Landroid/content/Intent;)V
    .locals 4

    .prologue
    .line 293
    const-string/jumbo v0, "com.google.android.apps.chrome.snapshot.cloudprint.EXTRA_JOB_ID"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 294
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 295
    :cond_0
    const-string/jumbo v1, "CloudPrintService"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "handleDeleteJob(): Unable to find jobid when trying to delete print job = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 307
    :goto_0
    return-void

    .line 299
    :cond_1
    const-string/jumbo v1, "CloudPrintService"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "handleDeleteJob(): Trying to delete print job = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 300
    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintService;->deletePrintJob(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 301
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;->createJobDeletedIntent(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 303
    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintService;->startServiceWithWakeLockInternal(Landroid/content/Intent;)V

    goto :goto_0

    .line 305
    :cond_2
    const-string/jumbo v1, "CloudPrintService"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "handleDeleteJob(): Unable to delete print job "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private handleFetch()V
    .locals 3

    .prologue
    .line 249
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 250
    const-string/jumbo v1, "CloudPrintService"

    const-string/jumbo v2, "Fetching external print jobs"

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 251
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintService;->fetchPrintJobs()Ljava/util/Set;

    move-result-object v1

    .line 252
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;->createPrintJobFetchResultIntent(Landroid/content/Context;Ljava/util/Set;)Landroid/content/Intent;

    move-result-object v0

    .line 254
    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintService;->startServiceWithWakeLockInternal(Landroid/content/Intent;)V

    .line 255
    return-void
.end method

.method private handleIntent(Landroid/content/Intent;)V
    .locals 3

    .prologue
    .line 129
    const-string/jumbo v0, "com.google.android.apps.chrome.snapshot.cloudprint.ACTION_SYNC_STATE_WITH_SERVER"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 130
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintService;->handleSyncStateWithServer(Landroid/content/Intent;)V

    .line 140
    :goto_0
    return-void

    .line 131
    :cond_0
    const-string/jumbo v0, "com.google.android.apps.chrome.snapshot.cloudprint.ACTION_FETCH"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 132
    invoke-direct {p0}, Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintService;->handleFetch()V

    goto :goto_0

    .line 133
    :cond_1
    const-string/jumbo v0, "com.google.android.apps.chrome.snapshot.cloudprint.ACTION_CONTROL"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 134
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintService;->handleControl(Landroid/content/Intent;)V

    goto :goto_0

    .line 135
    :cond_2
    const-string/jumbo v0, "com.google.android.apps.chrome.snapshot.cloudprint.ACTION_DELETE_JOB"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 136
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintService;->handleDeleteJob(Landroid/content/Intent;)V

    goto :goto_0

    .line 138
    :cond_3
    const-string/jumbo v0, "CloudPrintService"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "Got unknown action from intent: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private handleSyncStateWithServer(Landroid/content/Intent;)V
    .locals 4

    .prologue
    .line 156
    const-string/jumbo v0, "CloudPrintService"

    const-string/jumbo v1, "Syncing state with Cloud Print server"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 157
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintService;->getLocalPrinterData(Landroid/content/Intent;)Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;

    move-result-object v1

    .line 158
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintService;->getServerPrinterData()Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;

    move-result-object v0

    .line 159
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintService;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/chrome/snapshot/ChromeToMobileFeature;->isEnabled(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 160
    if-eqz v0, :cond_2

    .line 163
    invoke-virtual {v0}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintService;->deletePrinter(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 205
    :cond_0
    :goto_0
    return-void

    .line 166
    :cond_1
    const-string/jumbo v0, "CloudPrintService"

    const-string/jumbo v1, "Successfully deleted printer registration."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 168
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/chrome/snapshot/SnapshotSettings;->clearPrinterId(Landroid/content/Context;)V

    .line 169
    invoke-direct {p0}, Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintService;->clearNeedsUpdatingAndStoreApplicationVersionAndTimestamp()V

    goto :goto_0

    .line 173
    :cond_3
    if-nez v0, :cond_6

    .line 176
    invoke-virtual {v1}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->hasC2dmRegistrationIdTag()Z

    move-result v0

    if-nez v0, :cond_4

    .line 177
    const-string/jumbo v0, "CloudPrintService"

    const-string/jumbo v1, "No C2DM registration ID found. Not registering printer."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 180
    :cond_4
    invoke-virtual {p0, v1}, Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintService;->registerPrinter(Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;)Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;

    move-result-object v0

    .line 181
    if-nez v0, :cond_5

    .line 182
    const-string/jumbo v0, "CloudPrintService"

    const-string/jumbo v1, "Failed to register printer. Giving up for now."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 185
    :cond_5
    const-string/jumbo v2, "CloudPrintService"

    const-string/jumbo v3, "Successfully registered mobile device as printer."

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 190
    invoke-virtual {v0}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->withId(Ljava/lang/String;)Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;

    move-result-object v1

    .line 194
    :cond_6
    invoke-virtual {v1, v0}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 196
    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintService;->setStateSynchronizedAndFetchPrintJobs(Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;)V

    goto :goto_0

    .line 199
    :cond_7
    invoke-virtual {v0}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->withId(Ljava/lang/String;)Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;

    move-result-object v0

    .line 201
    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintService;->updatePrinter(Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 202
    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintService;->setStateSynchronizedAndFetchPrintJobs(Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;)V

    goto :goto_0
.end method

.method public static setHttpClientFactoryForTest(Lcom/google/android/apps/chrome/utilities/HttpClientFactory;)V
    .locals 0

    .prologue
    .line 84
    sput-object p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintService;->sHttpClientFactory:Lcom/google/android/apps/chrome/utilities/HttpClientFactory;

    .line 85
    return-void
.end method

.method private setLastUpdatedTimestamp(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 226
    new-instance v0, Landroid/text/format/Time;

    const-string/jumbo v1, "UTC"

    invoke-direct {v0, v1}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    .line 227
    invoke-virtual {v0}, Landroid/text/format/Time;->setToNow()V

    .line 228
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/text/format/Time;->format3339(Z)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/google/android/apps/chrome/snapshot/SnapshotSettings;->setLastUpdatedTimestamp(Landroid/content/Context;Ljava/lang/String;)V

    .line 229
    return-void
.end method

.method private setStateSynchronizedAndFetchPrintJobs(Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;)V
    .locals 2

    .prologue
    .line 208
    const-string/jumbo v0, "CloudPrintService"

    const-string/jumbo v1, "Synchronization of state with Cloud Print server complete."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 209
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/snapshot/SnapshotSettings;->setPrinterId(Landroid/content/Context;Ljava/lang/String;)V

    .line 210
    invoke-direct {p0}, Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintService;->clearNeedsUpdatingAndStoreApplicationVersionAndTimestamp()V

    .line 213
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;->createFetchPrintJobsIntent(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    .line 215
    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintService;->startServiceWithWakeLockInternal(Landroid/content/Intent;)V

    .line 216
    return-void
.end method


# virtual methods
.method controlPrintJob(Ljava/lang/String;Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJobStatus;)Z
    .locals 2

    .prologue
    .line 396
    sget-object v0, Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintService;->sHttpClientFactory:Lcom/google/android/apps/chrome/utilities/HttpClientFactory;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/utilities/HttpClientFactory;->newInstance()Lcom/google/android/apps/chrome/utilities/HttpClientWrapper;

    move-result-object v0

    .line 397
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v0, p1, p2}, Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintClient;->controlJob(Landroid/content/Context;Lcom/google/android/apps/chrome/utilities/HttpClientWrapper;Ljava/lang/String;Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJobStatus;)Z

    move-result v1

    .line 399
    invoke-interface {v0}, Lcom/google/android/apps/chrome/utilities/HttpClientWrapper;->close()V

    .line 400
    return v1
.end method

.method deletePrintJob(Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 405
    sget-object v0, Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintService;->sHttpClientFactory:Lcom/google/android/apps/chrome/utilities/HttpClientFactory;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/utilities/HttpClientFactory;->newInstance()Lcom/google/android/apps/chrome/utilities/HttpClientWrapper;

    move-result-object v0

    .line 406
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v0, p1}, Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintClient;->deleteJob(Landroid/content/Context;Lcom/google/android/apps/chrome/utilities/HttpClientWrapper;Ljava/lang/String;)Z

    move-result v1

    .line 407
    invoke-interface {v0}, Lcom/google/android/apps/chrome/utilities/HttpClientWrapper;->close()V

    .line 408
    return v1
.end method

.method deletePrinter(Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 362
    sget-object v0, Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintService;->sHttpClientFactory:Lcom/google/android/apps/chrome/utilities/HttpClientFactory;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/utilities/HttpClientFactory;->newInstance()Lcom/google/android/apps/chrome/utilities/HttpClientWrapper;

    move-result-object v0

    .line 363
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v0, p1}, Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintClient;->delete(Landroid/content/Context;Lcom/google/android/apps/chrome/utilities/HttpClientWrapper;Ljava/lang/String;)Z

    move-result v1

    .line 364
    invoke-interface {v0}, Lcom/google/android/apps/chrome/utilities/HttpClientWrapper;->close()V

    .line 365
    return v1
.end method

.method fetchPrintJobs()Ljava/util/Set;
    .locals 2

    .prologue
    .line 387
    sget-object v0, Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintService;->sHttpClientFactory:Lcom/google/android/apps/chrome/utilities/HttpClientFactory;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/utilities/HttpClientFactory;->newInstance()Lcom/google/android/apps/chrome/utilities/HttpClientWrapper;

    move-result-object v0

    .line 388
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintClient;->fetchJobs(Landroid/content/Context;Lcom/google/android/apps/chrome/utilities/HttpClientWrapper;)Ljava/util/Set;

    move-result-object v1

    .line 389
    invoke-interface {v0}, Lcom/google/android/apps/chrome/utilities/HttpClientWrapper;->close()V

    .line 390
    return-object v1
.end method

.method findPrinter(Ljava/lang/String;)Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;
    .locals 2

    .prologue
    .line 370
    sget-object v0, Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintService;->sHttpClientFactory:Lcom/google/android/apps/chrome/utilities/HttpClientFactory;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/utilities/HttpClientFactory;->newInstance()Lcom/google/android/apps/chrome/utilities/HttpClientWrapper;

    move-result-object v0

    .line 371
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v0, p1}, Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintClient;->find(Landroid/content/Context;Lcom/google/android/apps/chrome/utilities/HttpClientWrapper;Ljava/lang/String;)Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;

    move-result-object v1

    .line 372
    invoke-interface {v0}, Lcom/google/android/apps/chrome/utilities/HttpClientWrapper;->close()V

    .line 373
    return-object v1
.end method

.method generateProxyId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 311
    invoke-static {}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterDataFactory;->getDefaultProxy()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method getGcmRegistrationId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 413
    invoke-static {p0}, Lcom/google/android/a/c;->f(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getServerPrinterData()Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;
    .locals 5
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 322
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/chrome/snapshot/SnapshotSettings;->hasPrinterId(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 323
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/chrome/snapshot/SnapshotSettings;->getPrinterId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 324
    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintService;->findPrinter(Ljava/lang/String;)Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;

    move-result-object v0

    .line 325
    if-eqz v0, :cond_0

    .line 340
    :goto_0
    return-object v0

    .line 331
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintService;->generateProxyId()Ljava/lang/String;

    move-result-object v0

    .line 332
    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintService;->listPrinters(Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    .line 333
    if-eqz v1, :cond_2

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_2

    .line 334
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    const/4 v3, 0x1

    if-le v2, v3, :cond_1

    .line 335
    const-string/jumbo v2, "CloudPrintService"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "Size of printers for Gservices ID proxy ID = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v3, " too large: "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v3, ". Taking the first result."

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 338
    :cond_1
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;

    goto :goto_0

    .line 340
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method listPrinters(Ljava/lang/String;)Ljava/util/List;
    .locals 2

    .prologue
    .line 378
    sget-object v0, Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintService;->sHttpClientFactory:Lcom/google/android/apps/chrome/utilities/HttpClientFactory;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/utilities/HttpClientFactory;->newInstance()Lcom/google/android/apps/chrome/utilities/HttpClientWrapper;

    move-result-object v0

    .line 379
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v0, p1}, Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintClient;->list(Landroid/content/Context;Lcom/google/android/apps/chrome/utilities/HttpClientWrapper;Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    .line 381
    invoke-interface {v0}, Lcom/google/android/apps/chrome/utilities/HttpClientWrapper;->close()V

    .line 382
    return-object v1
.end method

.method public onCreate()V
    .locals 3

    .prologue
    .line 94
    invoke-super {p0}, Lcom/google/android/apps/chrome/snapshot/IntentServiceWithWakeLock;->onCreate()V

    .line 95
    sget-object v0, Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintService;->sHttpClientFactory:Lcom/google/android/apps/chrome/utilities/HttpClientFactory;

    if-nez v0, :cond_0

    .line 96
    new-instance v0, Lcom/google/android/apps/chrome/utilities/HttpClientFactoryImpl;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/apps/chrome/utilities/HttpClientFactoryImpl;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintService;->sHttpClientFactory:Lcom/google/android/apps/chrome/utilities/HttpClientFactory;

    .line 98
    :cond_0
    const-string/jumbo v0, "GSERVICES_ANDROID_ID"

    new-instance v1, Lcom/google/android/apps/chrome/identity/GservicesAndPackageNameBasedIdentificationGenerator;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintService;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/google/android/apps/chrome/identity/GservicesAndPackageNameBasedIdentificationGenerator;-><init>(Landroid/content/Context;)V

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lorg/chromium/chrome/browser/identity/UniqueIdentificationGeneratorFactory;->registerGenerator(Ljava/lang/String;Lorg/chromium/chrome/browser/identity/UniqueIdentificationGenerator;Z)V

    .line 102
    return-void
.end method

.method protected onHandleIntentWithWakeLock(Landroid/content/Intent;)V
    .locals 3

    .prologue
    .line 112
    :try_start_0
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintService;->handleIntent(Landroid/content/Intent;)V
    :try_end_0
    .catch Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintAuthenticationException; {:try_start_0 .. :try_end_0} :catch_0

    .line 126
    :goto_0
    return-void

    .line 113
    :catch_0
    move-exception v0

    .line 114
    const-string/jumbo v1, "CloudPrintService"

    const-string/jumbo v2, "CloudPrint authentication failed:"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 115
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/chrome/snapshot/cloudprint/AuthTokenHelper;->invalidateAndAcquireNewAuthToken(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 118
    :try_start_1
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintService;->handleIntent(Landroid/content/Intent;)V
    :try_end_1
    .catch Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintAuthenticationException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 119
    :catch_1
    move-exception v0

    .line 120
    const-string/jumbo v1, "CloudPrintService"

    const-string/jumbo v2, "Hit Exception with new auth token: "

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 123
    :cond_0
    const-string/jumbo v0, "CloudPrintService"

    const-string/jumbo v1, "Failed to get new auth token."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method registerPrinter(Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;)Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;
    .locals 2

    .prologue
    .line 345
    sget-object v0, Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintService;->sHttpClientFactory:Lcom/google/android/apps/chrome/utilities/HttpClientFactory;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/utilities/HttpClientFactory;->newInstance()Lcom/google/android/apps/chrome/utilities/HttpClientWrapper;

    move-result-object v0

    .line 346
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v0, p1}, Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintClient;->register(Landroid/content/Context;Lcom/google/android/apps/chrome/utilities/HttpClientWrapper;Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;)Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;

    move-result-object v1

    .line 348
    invoke-interface {v0}, Lcom/google/android/apps/chrome/utilities/HttpClientWrapper;->close()V

    .line 349
    return-object v1
.end method

.method startServiceWithWakeLockInternal(Landroid/content/Intent;)V
    .locals 0

    .prologue
    .line 316
    invoke-static {p0, p1}, Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintService;->startServiceWithWakeLock(Landroid/content/Context;Landroid/content/Intent;)V

    .line 317
    return-void
.end method

.method updatePrinter(Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;)Z
    .locals 2

    .prologue
    .line 354
    sget-object v0, Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintService;->sHttpClientFactory:Lcom/google/android/apps/chrome/utilities/HttpClientFactory;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/utilities/HttpClientFactory;->newInstance()Lcom/google/android/apps/chrome/utilities/HttpClientWrapper;

    move-result-object v0

    .line 355
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v0, p1}, Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintClient;->update(Landroid/content/Context;Lcom/google/android/apps/chrome/utilities/HttpClientWrapper;Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;)Z

    move-result v1

    .line 356
    invoke-interface {v0}, Lcom/google/android/apps/chrome/utilities/HttpClientWrapper;->close()V

    .line 357
    return v1
.end method
