.class Lcom/google/android/apps/chrome/tabmodel/TabModelImpl$1;
.super Ljava/lang/Object;
.source "TabModelImpl.java"

# interfaces
.implements Lorg/chromium/chrome/browser/tabmodel/TabModelObserver;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/tabmodel/TabModelImpl;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/tabmodel/TabModelImpl;)V
    .locals 0

    .prologue
    .line 57
    iput-object p1, p0, Lcom/google/android/apps/chrome/tabmodel/TabModelImpl$1;->this$0:Lcom/google/android/apps/chrome/tabmodel/TabModelImpl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public didAddTab(Lorg/chromium/chrome/browser/Tab;Lorg/chromium/chrome/browser/tabmodel/TabModel$TabLaunchType;)V
    .locals 6

    .prologue
    .line 92
    const/4 v2, -0x1

    .line 93
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabmodel/TabModelImpl$1;->this$0:Lcom/google/android/apps/chrome/tabmodel/TabModelImpl;

    # getter for: Lcom/google/android/apps/chrome/tabmodel/TabModelImpl;->mModelDelegate:Lorg/chromium/chrome/browser/tabmodel/TabModelDelegate;
    invoke-static {v0}, Lcom/google/android/apps/chrome/tabmodel/TabModelImpl;->access$600(Lcom/google/android/apps/chrome/tabmodel/TabModelImpl;)Lorg/chromium/chrome/browser/tabmodel/TabModelDelegate;

    move-result-object v0

    invoke-interface {v0}, Lorg/chromium/chrome/browser/tabmodel/TabModelDelegate;->getCurrentModel()Lorg/chromium/chrome/browser/tabmodel/TabModel;

    move-result-object v0

    invoke-static {v0}, Lorg/chromium/chrome/browser/tabmodel/TabModelUtils;->getCurrentTab(Lorg/chromium/chrome/browser/tabmodel/TabList;)Lorg/chromium/chrome/browser/Tab;

    move-result-object v0

    .line 94
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/Tab;->getId()I

    move-result v2

    .line 95
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabmodel/TabModelImpl$1;->this$0:Lcom/google/android/apps/chrome/tabmodel/TabModelImpl;

    invoke-virtual {p1}, Lorg/chromium/chrome/browser/Tab;->getId()I

    move-result v1

    invoke-virtual {p1}, Lorg/chromium/chrome/browser/Tab;->getUrl()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/chrome/tabmodel/TabModelImpl$1;->this$0:Lcom/google/android/apps/chrome/tabmodel/TabModelImpl;

    invoke-virtual {v4}, Lcom/google/android/apps/chrome/tabmodel/TabModelImpl;->isIncognito()Z

    move-result v5

    move-object v4, p2

    # invokes: Lcom/google/android/apps/chrome/tabmodel/TabModelImpl;->notifyTabCreated(IILjava/lang/String;Lorg/chromium/chrome/browser/tabmodel/TabModel$TabLaunchType;Z)V
    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/chrome/tabmodel/TabModelImpl;->access$700(Lcom/google/android/apps/chrome/tabmodel/TabModelImpl;IILjava/lang/String;Lorg/chromium/chrome/browser/tabmodel/TabModel$TabLaunchType;Z)V

    .line 96
    return-void
.end method

.method public didCloseTab(Lorg/chromium/chrome/browser/Tab;)V
    .locals 2

    .prologue
    .line 84
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabmodel/TabModelImpl$1;->this$0:Lcom/google/android/apps/chrome/tabmodel/TabModelImpl;

    # getter for: Lcom/google/android/apps/chrome/tabmodel/TabModelImpl;->mTabContentManager:Lcom/google/android/apps/chrome/compositor/TabContentManager;
    invoke-static {v0}, Lcom/google/android/apps/chrome/tabmodel/TabModelImpl;->access$400(Lcom/google/android/apps/chrome/tabmodel/TabModelImpl;)Lcom/google/android/apps/chrome/compositor/TabContentManager;

    move-result-object v0

    invoke-virtual {p1}, Lorg/chromium/chrome/browser/Tab;->getId()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/compositor/TabContentManager;->closeTab(I)V

    .line 85
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabmodel/TabModelImpl$1;->this$0:Lcom/google/android/apps/chrome/tabmodel/TabModelImpl;

    # getter for: Lcom/google/android/apps/chrome/tabmodel/TabModelImpl;->mTabSaver:Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;
    invoke-static {v0}, Lcom/google/android/apps/chrome/tabmodel/TabModelImpl;->access$500(Lcom/google/android/apps/chrome/tabmodel/TabModelImpl;)Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->removeTabFromQueues(Lorg/chromium/chrome/browser/Tab;)V

    .line 87
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabmodel/TabModelImpl$1;->this$0:Lcom/google/android/apps/chrome/tabmodel/TabModelImpl;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabmodel/TabModelImpl;->isIncognito()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p1}, Lcom/google/android/apps/chrome/tab/ChromeTab;->fromTab(Lorg/chromium/chrome/browser/Tab;)Lcom/google/android/apps/chrome/tab/ChromeTab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->createHistoricalTab()V

    .line 88
    :cond_0
    return-void
.end method

.method public didMoveTab(Lorg/chromium/chrome/browser/Tab;II)V
    .locals 3

    .prologue
    .line 108
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabmodel/TabModelImpl$1;->this$0:Lcom/google/android/apps/chrome/tabmodel/TabModelImpl;

    invoke-virtual {p1}, Lorg/chromium/chrome/browser/Tab;->getId()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/apps/chrome/tabmodel/TabModelImpl$1;->this$0:Lcom/google/android/apps/chrome/tabmodel/TabModelImpl;

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/tabmodel/TabModelImpl;->isIncognito()Z

    move-result v2

    # invokes: Lcom/google/android/apps/chrome/tabmodel/TabModelImpl;->notifyTabMoved(IIIZ)V
    invoke-static {v0, v1, p3, p2, v2}, Lcom/google/android/apps/chrome/tabmodel/TabModelImpl;->access$1000(Lcom/google/android/apps/chrome/tabmodel/TabModelImpl;IIIZ)V

    .line 109
    return-void
.end method

.method public didSelectTab(Lorg/chromium/chrome/browser/Tab;Lorg/chromium/chrome/browser/tabmodel/TabModel$TabSelectionType;I)V
    .locals 3

    .prologue
    .line 60
    invoke-virtual {p1}, Lorg/chromium/chrome/browser/Tab;->getId()I

    move-result v0

    if-ne v0, p3, :cond_1

    const/4 v0, 0x1

    .line 64
    :goto_0
    if-nez v0, :cond_0

    .line 65
    sget-object v0, Lorg/chromium/chrome/browser/tabmodel/TabModel$TabSelectionType;->FROM_USER:Lorg/chromium/chrome/browser/tabmodel/TabModel$TabSelectionType;

    if-ne p2, v0, :cond_0

    .line 67
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabmodel/TabModelImpl$1;->this$0:Lcom/google/android/apps/chrome/tabmodel/TabModelImpl;

    # getter for: Lcom/google/android/apps/chrome/tabmodel/TabModelImpl;->mUma:Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorUma;
    invoke-static {v0}, Lcom/google/android/apps/chrome/tabmodel/TabModelImpl;->access$000(Lcom/google/android/apps/chrome/tabmodel/TabModelImpl;)Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorUma;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorUma;->userSwitchedToTab()V

    .line 71
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabmodel/TabModelImpl$1;->this$0:Lcom/google/android/apps/chrome/tabmodel/TabModelImpl;

    invoke-virtual {p1}, Lorg/chromium/chrome/browser/Tab;->getId()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/apps/chrome/tabmodel/TabModelImpl$1;->this$0:Lcom/google/android/apps/chrome/tabmodel/TabModelImpl;

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/tabmodel/TabModelImpl;->isIncognito()Z

    move-result v2

    # invokes: Lcom/google/android/apps/chrome/tabmodel/TabModelImpl;->notifyTabSelected(IILorg/chromium/chrome/browser/tabmodel/TabModel$TabSelectionType;Z)V
    invoke-static {v0, v1, p3, p2, v2}, Lcom/google/android/apps/chrome/tabmodel/TabModelImpl;->access$100(Lcom/google/android/apps/chrome/tabmodel/TabModelImpl;IILorg/chromium/chrome/browser/tabmodel/TabModel$TabSelectionType;Z)V

    .line 72
    return-void

    .line 60
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public tabClosureCommitted(Lorg/chromium/chrome/browser/Tab;)V
    .locals 3

    .prologue
    .line 118
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabmodel/TabModelImpl$1;->this$0:Lcom/google/android/apps/chrome/tabmodel/TabModelImpl;

    invoke-virtual {p1}, Lorg/chromium/chrome/browser/Tab;->getId()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/apps/chrome/tabmodel/TabModelImpl$1;->this$0:Lcom/google/android/apps/chrome/tabmodel/TabModelImpl;

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/tabmodel/TabModelImpl;->isIncognito()Z

    move-result v2

    # invokes: Lcom/google/android/apps/chrome/tabmodel/TabModelImpl;->notifyTabClosureCommitted(IZ)V
    invoke-static {v0, v1, v2}, Lcom/google/android/apps/chrome/tabmodel/TabModelImpl;->access$1200(Lcom/google/android/apps/chrome/tabmodel/TabModelImpl;IZ)V

    .line 119
    return-void
.end method

.method public tabClosureUndone(Lorg/chromium/chrome/browser/Tab;)V
    .locals 3

    .prologue
    .line 113
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabmodel/TabModelImpl$1;->this$0:Lcom/google/android/apps/chrome/tabmodel/TabModelImpl;

    invoke-virtual {p1}, Lorg/chromium/chrome/browser/Tab;->getId()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/apps/chrome/tabmodel/TabModelImpl$1;->this$0:Lcom/google/android/apps/chrome/tabmodel/TabModelImpl;

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/tabmodel/TabModelImpl;->isIncognito()Z

    move-result v2

    # invokes: Lcom/google/android/apps/chrome/tabmodel/TabModelImpl;->notifyTabClosureUndone(IZ)V
    invoke-static {v0, v1, v2}, Lcom/google/android/apps/chrome/tabmodel/TabModelImpl;->access$1100(Lcom/google/android/apps/chrome/tabmodel/TabModelImpl;IZ)V

    .line 114
    return-void
.end method

.method public tabPendingClosure(Lorg/chromium/chrome/browser/Tab;)V
    .locals 4

    .prologue
    .line 123
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabmodel/TabModelImpl$1;->this$0:Lcom/google/android/apps/chrome/tabmodel/TabModelImpl;

    invoke-virtual {p1}, Lorg/chromium/chrome/browser/Tab;->getId()I

    move-result v1

    invoke-virtual {p1}, Lorg/chromium/chrome/browser/Tab;->getTitle()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/chrome/tabmodel/TabModelImpl$1;->this$0:Lcom/google/android/apps/chrome/tabmodel/TabModelImpl;

    invoke-virtual {v3}, Lcom/google/android/apps/chrome/tabmodel/TabModelImpl;->isIncognito()Z

    move-result v3

    # invokes: Lcom/google/android/apps/chrome/tabmodel/TabModelImpl;->notifyTabPendingClosure(ILjava/lang/String;Z)V
    invoke-static {v0, v1, v2, v3}, Lcom/google/android/apps/chrome/tabmodel/TabModelImpl;->access$1300(Lcom/google/android/apps/chrome/tabmodel/TabModelImpl;ILjava/lang/String;Z)V

    .line 124
    return-void
.end method

.method public willAddTab(Lorg/chromium/chrome/browser/Tab;Lorg/chromium/chrome/browser/tabmodel/TabModel$TabLaunchType;)V
    .locals 6

    .prologue
    .line 100
    const/4 v2, -0x1

    .line 101
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabmodel/TabModelImpl$1;->this$0:Lcom/google/android/apps/chrome/tabmodel/TabModelImpl;

    # getter for: Lcom/google/android/apps/chrome/tabmodel/TabModelImpl;->mModelDelegate:Lorg/chromium/chrome/browser/tabmodel/TabModelDelegate;
    invoke-static {v0}, Lcom/google/android/apps/chrome/tabmodel/TabModelImpl;->access$800(Lcom/google/android/apps/chrome/tabmodel/TabModelImpl;)Lorg/chromium/chrome/browser/tabmodel/TabModelDelegate;

    move-result-object v0

    invoke-interface {v0}, Lorg/chromium/chrome/browser/tabmodel/TabModelDelegate;->getCurrentModel()Lorg/chromium/chrome/browser/tabmodel/TabModel;

    move-result-object v0

    invoke-static {v0}, Lorg/chromium/chrome/browser/tabmodel/TabModelUtils;->getCurrentTab(Lorg/chromium/chrome/browser/tabmodel/TabList;)Lorg/chromium/chrome/browser/Tab;

    move-result-object v0

    .line 102
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/Tab;->getId()I

    move-result v2

    .line 103
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabmodel/TabModelImpl$1;->this$0:Lcom/google/android/apps/chrome/tabmodel/TabModelImpl;

    invoke-virtual {p1}, Lorg/chromium/chrome/browser/Tab;->getId()I

    move-result v1

    invoke-virtual {p1}, Lorg/chromium/chrome/browser/Tab;->getUrl()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/chrome/tabmodel/TabModelImpl$1;->this$0:Lcom/google/android/apps/chrome/tabmodel/TabModelImpl;

    invoke-virtual {v4}, Lcom/google/android/apps/chrome/tabmodel/TabModelImpl;->isIncognito()Z

    move-result v5

    move-object v4, p2

    # invokes: Lcom/google/android/apps/chrome/tabmodel/TabModelImpl;->notifyTabCreating(IILjava/lang/String;Lorg/chromium/chrome/browser/tabmodel/TabModel$TabLaunchType;Z)V
    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/chrome/tabmodel/TabModelImpl;->access$900(Lcom/google/android/apps/chrome/tabmodel/TabModelImpl;IILjava/lang/String;Lorg/chromium/chrome/browser/tabmodel/TabModel$TabLaunchType;Z)V

    .line 104
    return-void
.end method

.method public willCloseTab(Lorg/chromium/chrome/browser/Tab;Z)V
    .locals 3

    .prologue
    .line 76
    invoke-virtual {p1}, Lorg/chromium/chrome/browser/Tab;->getId()I

    move-result v0

    .line 77
    iget-object v1, p0, Lcom/google/android/apps/chrome/tabmodel/TabModelImpl$1;->this$0:Lcom/google/android/apps/chrome/tabmodel/TabModelImpl;

    # invokes: Lcom/google/android/apps/chrome/tabmodel/TabModelImpl;->blockingNotifyTabClosing(IZ)V
    invoke-static {v1, v0, p2}, Lcom/google/android/apps/chrome/tabmodel/TabModelImpl;->access$200(Lcom/google/android/apps/chrome/tabmodel/TabModelImpl;IZ)V

    .line 79
    iget-object v1, p0, Lcom/google/android/apps/chrome/tabmodel/TabModelImpl$1;->this$0:Lcom/google/android/apps/chrome/tabmodel/TabModelImpl;

    iget-object v2, p0, Lcom/google/android/apps/chrome/tabmodel/TabModelImpl$1;->this$0:Lcom/google/android/apps/chrome/tabmodel/TabModelImpl;

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/tabmodel/TabModelImpl;->isIncognito()Z

    move-result v2

    # invokes: Lcom/google/android/apps/chrome/tabmodel/TabModelImpl;->notifyTabClosed(IZ)V
    invoke-static {v1, v0, v2}, Lcom/google/android/apps/chrome/tabmodel/TabModelImpl;->access$300(Lcom/google/android/apps/chrome/tabmodel/TabModelImpl;IZ)V

    .line 80
    return-void
.end method
