.class public Lcom/google/android/apps/chrome/snapshot/cloudprint/AuthTokenHelper;
.super Ljava/lang/Object;
.source "AuthTokenHelper.java"


# static fields
.field private static final OAUTH2_TIMEOUT_TIMEUNIT:Ljava/util/concurrent/TimeUnit;


# instance fields
.field protected mBackoffScheduler:Lcom/google/android/apps/chrome/utilities/ExponentialBackoffScheduler;

.field private mContext:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 39
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    sput-object v0, Lcom/google/android/apps/chrome/snapshot/cloudprint/AuthTokenHelper;->OAUTH2_TIMEOUT_TIMEUNIT:Ljava/util/concurrent/TimeUnit;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 8

    .prologue
    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    iput-object p1, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/AuthTokenHelper;->mContext:Landroid/content/Context;

    .line 47
    const-string/jumbo v2, "AuthTokenHelper"

    iget-object v3, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/AuthTokenHelper;->mContext:Landroid/content/Context;

    const-wide/32 v4, 0xea60

    const-wide/32 v6, 0x36ee80

    move-object v1, p0

    invoke-virtual/range {v1 .. v7}, Lcom/google/android/apps/chrome/snapshot/cloudprint/AuthTokenHelper;->createBackoffScheduler(Ljava/lang/String;Landroid/content/Context;JJ)V

    .line 48
    return-void
.end method

.method public static getCloudPrintAuthToken(Landroid/app/Activity;Landroid/accounts/Account;Ljava/lang/Runnable;)V
    .locals 3

    .prologue
    .line 165
    invoke-static {}, Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintContract;->isChromeToMobileOAuth2Enabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 166
    const-string/jumbo v0, "https://www.googleapis.com/auth/cloudprint"

    new-instance v1, Lcom/google/android/apps/chrome/snapshot/cloudprint/AuthTokenHelper$1;

    invoke-direct {v1, p2}, Lcom/google/android/apps/chrome/snapshot/cloudprint/AuthTokenHelper$1;-><init>(Ljava/lang/Runnable;)V

    invoke-static {p0, p0, p1, v0, v1}, Lorg/chromium/chrome/browser/signin/OAuth2TokenService;->getOAuth2AccessToken(Landroid/content/Context;Landroid/app/Activity;Landroid/accounts/Account;Ljava/lang/String;Lorg/chromium/sync/signin/AccountManagerHelper$GetAuthTokenCallback;)V

    .line 184
    :goto_0
    return-void

    .line 175
    :cond_0
    invoke-static {p0}, Lorg/chromium/sync/signin/AccountManagerHelper;->get(Landroid/content/Context;)Lorg/chromium/sync/signin/AccountManagerHelper;

    move-result-object v0

    const-string/jumbo v1, "cloudprint"

    new-instance v2, Lcom/google/android/apps/chrome/snapshot/cloudprint/AuthTokenHelper$2;

    invoke-direct {v2, p2}, Lcom/google/android/apps/chrome/snapshot/cloudprint/AuthTokenHelper$2;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0, p0, p1, v1, v2}, Lorg/chromium/sync/signin/AccountManagerHelper;->getAuthTokenFromForeground(Landroid/app/Activity;Landroid/accounts/Account;Ljava/lang/String;Lorg/chromium/sync/signin/AccountManagerHelper$GetAuthTokenCallback;)V

    goto :goto_0
.end method

.method private static getNewAuthToken(Landroid/content/Context;Landroid/accounts/Account;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 140
    invoke-static {p0}, Lcom/google/android/apps/chrome/snapshot/SnapshotSettings;->getAuthToken(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 141
    invoke-static {}, Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintContract;->isChromeToMobileOAuth2Enabled()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 142
    invoke-static {p0, v0}, Lorg/chromium/chrome/browser/signin/OAuth2TokenService;->invalidateOAuth2AuthToken(Landroid/content/Context;Ljava/lang/String;)V

    .line 143
    const/4 v0, 0x0

    invoke-static {p0, v0, p1}, Lcom/google/android/apps/chrome/snapshot/cloudprint/AuthTokenHelper;->getOAuth2AccessTokenWithTimeout(Landroid/content/Context;Landroid/app/Activity;Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v0

    .line 145
    :goto_0
    return-object v0

    :cond_0
    invoke-static {p0}, Lorg/chromium/sync/signin/AccountManagerHelper;->get(Landroid/content/Context;)Lorg/chromium/sync/signin/AccountManagerHelper;

    move-result-object v1

    const-string/jumbo v2, "cloudprint"

    invoke-virtual {v1, p1, v0, v2}, Lorg/chromium/sync/signin/AccountManagerHelper;->getNewAuthToken(Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private static getOAuth2AccessTokenWithTimeout(Landroid/content/Context;Landroid/app/Activity;Landroid/accounts/Account;)Ljava/lang/String;
    .locals 7

    .prologue
    .line 153
    const-string/jumbo v3, "https://www.googleapis.com/auth/cloudprint"

    const-wide/16 v4, 0x5

    sget-object v6, Lcom/google/android/apps/chrome/snapshot/cloudprint/AuthTokenHelper;->OAUTH2_TIMEOUT_TIMEUNIT:Ljava/util/concurrent/TimeUnit;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-static/range {v0 .. v6}, Lorg/chromium/chrome/browser/signin/OAuth2TokenService;->getOAuth2AccessTokenWithTimeout(Landroid/content/Context;Landroid/app/Activity;Landroid/accounts/Account;Ljava/lang/String;JLjava/util/concurrent/TimeUnit;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static invalidateAndAcquireNewAuthToken(Landroid/content/Context;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 98
    const-string/jumbo v2, "AuthTokenHelper"

    const-string/jumbo v3, "Invalidating auth token for cloudprint"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 99
    invoke-static {p0}, Lorg/chromium/sync/signin/ChromeSigninController;->get(Landroid/content/Context;)Lorg/chromium/sync/signin/ChromeSigninController;

    move-result-object v2

    invoke-virtual {v2}, Lorg/chromium/sync/signin/ChromeSigninController;->getSignedInUser()Landroid/accounts/Account;

    move-result-object v2

    .line 100
    if-nez v2, :cond_1

    .line 101
    const-string/jumbo v1, "AuthTokenHelper"

    const-string/jumbo v2, "Cannot invalidate an auth token. Account is not known"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 120
    :cond_0
    :goto_0
    return v0

    .line 104
    :cond_1
    invoke-static {p0, v2}, Lcom/google/android/apps/chrome/snapshot/cloudprint/AuthTokenHelper;->getNewAuthToken(Landroid/content/Context;Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v2

    .line 108
    invoke-static {p0, v2}, Lcom/google/android/apps/chrome/snapshot/SnapshotSettings;->setAuthToken(Landroid/content/Context;Ljava/lang/String;)V

    .line 113
    if-eqz v2, :cond_2

    move v0, v1

    .line 114
    :cond_2
    if-nez v0, :cond_0

    .line 115
    invoke-static {p0, v1}, Lcom/google/android/apps/chrome/snapshot/SnapshotSettings;->setNeedsUpdating(Landroid/content/Context;Z)V

    .line 116
    invoke-static {p0}, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;->createInitializeIntent(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v1

    .line 118
    invoke-virtual {p0, v1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    goto :goto_0
.end method

.method private isAuthTokenAvailable()Z
    .locals 1

    .prologue
    .line 190
    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/AuthTokenHelper;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/apps/chrome/snapshot/SnapshotSettings;->getAuthToken(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private scheduleGetAuthToken(Landroid/content/Intent;)V
    .locals 6

    .prologue
    .line 198
    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/AuthTokenHelper;->mBackoffScheduler:Lcom/google/android/apps/chrome/utilities/ExponentialBackoffScheduler;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/utilities/ExponentialBackoffScheduler;->createAlarm(Landroid/content/Intent;)J

    move-result-wide v0

    .line 199
    iget-object v2, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/AuthTokenHelper;->mBackoffScheduler:Lcom/google/android/apps/chrome/utilities/ExponentialBackoffScheduler;

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/utilities/ExponentialBackoffScheduler;->getCurrentTime()J

    move-result-wide v2

    sub-long/2addr v0, v2

    .line 200
    const-string/jumbo v2, "AuthTokenHelper"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    new-instance v4, Ljava/util/Date;

    invoke-direct {v4}, Ljava/util/Date;-><init>()V

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, ": Failed to get CloudPrint auth token.  Rescheduling in "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-wide/16 v4, 0x3e8

    div-long/2addr v0, v4

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " seconds."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 202
    return-void
.end method

.method private tryToGetAuthToken(Landroid/accounts/Account;)V
    .locals 2

    .prologue
    .line 129
    invoke-static {}, Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintContract;->isChromeToMobileOAuth2Enabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 130
    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/AuthTokenHelper;->mContext:Landroid/content/Context;

    const/4 v1, 0x0

    invoke-static {v0, v1, p1}, Lcom/google/android/apps/chrome/snapshot/cloudprint/AuthTokenHelper;->getOAuth2AccessTokenWithTimeout(Landroid/content/Context;Landroid/app/Activity;Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v0

    .line 135
    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/AuthTokenHelper;->mContext:Landroid/content/Context;

    invoke-static {v1, v0}, Lcom/google/android/apps/chrome/snapshot/SnapshotSettings;->setAuthToken(Landroid/content/Context;Ljava/lang/String;)V

    .line 136
    return-void

    .line 132
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/AuthTokenHelper;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lorg/chromium/sync/signin/AccountManagerHelper;->get(Landroid/content/Context;)Lorg/chromium/sync/signin/AccountManagerHelper;

    move-result-object v0

    const-string/jumbo v1, "cloudprint"

    invoke-virtual {v0, p1, v1}, Lorg/chromium/sync/signin/AccountManagerHelper;->getAuthTokenFromBackground(Landroid/accounts/Account;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public acquireAuthToken(Landroid/accounts/Account;Landroid/content/Intent;)Z
    .locals 1

    .prologue
    .line 71
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/snapshot/cloudprint/AuthTokenHelper;->tryToGetAuthToken(Landroid/accounts/Account;)V

    .line 72
    invoke-direct {p0}, Lcom/google/android/apps/chrome/snapshot/cloudprint/AuthTokenHelper;->isAuthTokenAvailable()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 73
    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/AuthTokenHelper;->mBackoffScheduler:Lcom/google/android/apps/chrome/utilities/ExponentialBackoffScheduler;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/utilities/ExponentialBackoffScheduler;->resetFailedAttempts()V

    .line 74
    const/4 v0, 0x1

    .line 81
    :goto_0
    return v0

    .line 76
    :cond_0
    invoke-direct {p0, p2}, Lcom/google/android/apps/chrome/snapshot/cloudprint/AuthTokenHelper;->scheduleGetAuthToken(Landroid/content/Intent;)V

    .line 80
    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/AuthTokenHelper;->mBackoffScheduler:Lcom/google/android/apps/chrome/utilities/ExponentialBackoffScheduler;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/utilities/ExponentialBackoffScheduler;->increaseFailedAttempts()V

    .line 81
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public createBackoffScheduler(Ljava/lang/String;Landroid/content/Context;JJ)V
    .locals 9

    .prologue
    .line 56
    new-instance v1, Lcom/google/android/apps/chrome/utilities/ExponentialBackoffScheduler;

    move-object v2, p1

    move-object v3, p2

    move-wide v4, p3

    move-wide v6, p5

    invoke-direct/range {v1 .. v7}, Lcom/google/android/apps/chrome/utilities/ExponentialBackoffScheduler;-><init>(Ljava/lang/String;Landroid/content/Context;JJ)V

    iput-object v1, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/AuthTokenHelper;->mBackoffScheduler:Lcom/google/android/apps/chrome/utilities/ExponentialBackoffScheduler;

    .line 58
    return-void
.end method

.method public stopAcquiringAuthToken(Landroid/content/Intent;)V
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/AuthTokenHelper;->mBackoffScheduler:Lcom/google/android/apps/chrome/utilities/ExponentialBackoffScheduler;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/utilities/ExponentialBackoffScheduler;->cancelAlarm(Landroid/content/Intent;)Z

    .line 91
    return-void
.end method
