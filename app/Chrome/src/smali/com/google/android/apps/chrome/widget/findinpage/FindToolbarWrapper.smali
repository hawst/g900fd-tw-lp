.class public Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarWrapper;
.super Lcom/google/android/apps/chrome/widget/lazyloading/LazyViewLoader;
.source "FindToolbarWrapper.java"


# static fields
.field private static final NOTIFICATIONS:[I


# instance fields
.field private final mCallback:Landroid/view/ActionMode$Callback;

.field private mFindToolbar:Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;

.field private final mTabModelSelector:Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 99
    const/4 v0, 0x2

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarWrapper;->NOTIFICATIONS:[I

    return-void

    nop

    :array_0
    .array-data 4
        0x2d
        0x2c
    .end array-data
.end method

.method public constructor <init>(Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;Landroid/view/ActionMode$Callback;Landroid/app/Activity;II)V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0, p3, p4, p5}, Lcom/google/android/apps/chrome/widget/lazyloading/LazyViewLoader;-><init>(Landroid/app/Activity;II)V

    .line 40
    iput-object p1, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarWrapper;->mTabModelSelector:Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

    .line 41
    iput-object p2, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarWrapper;->mCallback:Landroid/view/ActionMode$Callback;

    .line 42
    return-void
.end method


# virtual methods
.method protected getNotificationsToRegister()[I
    .locals 1

    .prologue
    .line 77
    sget-object v0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarWrapper;->NOTIFICATIONS:[I

    return-object v0
.end method

.method protected handleNotificationMessage(Landroid/os/Message;)V
    .locals 2

    .prologue
    .line 89
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 97
    :goto_0
    return-void

    .line 91
    :pswitch_0
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    const-string/jumbo v1, "text"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarWrapper;->showToolbar(Ljava/lang/String;)V

    goto :goto_0

    .line 94
    :pswitch_1
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarWrapper;->hideToolbar()V

    goto :goto_0

    .line 89
    nop

    :pswitch_data_0
    .packed-switch 0x2c
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public hideToolbar()V
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarWrapper;->mFindToolbar:Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarWrapper;->mFindToolbar:Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->deactivate()V

    .line 68
    :cond_0
    return-void
.end method

.method public isShowing()Z
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarWrapper;->mFindToolbar:Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarWrapper;->mFindToolbar:Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected shouldInflate()Z
    .locals 1

    .prologue
    .line 72
    const/4 v0, 0x1

    return v0
.end method

.method public showToolbar(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 53
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarWrapper;->inflateIfNecessary()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 54
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 p1, 0x0

    .line 55
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarWrapper;->mFindToolbar:Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_2

    .line 56
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarWrapper;->mFindToolbar:Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->activate(Ljava/lang/String;)V

    .line 61
    :cond_1
    :goto_0
    return-void

    .line 57
    :cond_2
    if-eqz p1, :cond_1

    .line 58
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarWrapper;->mFindToolbar:Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->setFindText(Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected bridge synthetic viewInflated(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 20
    check-cast p1, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarWrapper;->viewInflated(Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;)V

    return-void
.end method

.method protected viewInflated(Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;)V
    .locals 2

    .prologue
    .line 82
    iput-object p1, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarWrapper;->mFindToolbar:Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;

    .line 83
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarWrapper;->mFindToolbar:Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;

    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarWrapper;->mTabModelSelector:Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->setTabModelSelector(Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;)V

    .line 84
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarWrapper;->mFindToolbar:Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;

    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarWrapper;->mCallback:Landroid/view/ActionMode$Callback;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->setActionModeCallbackForTextEdit(Landroid/view/ActionMode$Callback;)V

    .line 85
    return-void
.end method
