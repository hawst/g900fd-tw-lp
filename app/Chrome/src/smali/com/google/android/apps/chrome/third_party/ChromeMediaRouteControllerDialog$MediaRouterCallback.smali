.class final Lcom/google/android/apps/chrome/third_party/ChromeMediaRouteControllerDialog$MediaRouterCallback;
.super Landroid/support/v7/media/g;
.source "ChromeMediaRouteControllerDialog.java"


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/third_party/ChromeMediaRouteControllerDialog;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/chrome/third_party/ChromeMediaRouteControllerDialog;)V
    .locals 0

    .prologue
    .line 328
    iput-object p1, p0, Lcom/google/android/apps/chrome/third_party/ChromeMediaRouteControllerDialog$MediaRouterCallback;->this$0:Lcom/google/android/apps/chrome/third_party/ChromeMediaRouteControllerDialog;

    invoke-direct {p0}, Landroid/support/v7/media/g;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/chrome/third_party/ChromeMediaRouteControllerDialog;Lcom/google/android/apps/chrome/third_party/ChromeMediaRouteControllerDialog$1;)V
    .locals 0

    .prologue
    .line 328
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/third_party/ChromeMediaRouteControllerDialog$MediaRouterCallback;-><init>(Lcom/google/android/apps/chrome/third_party/ChromeMediaRouteControllerDialog;)V

    return-void
.end method


# virtual methods
.method public final onRouteChanged(Landroid/support/v7/media/MediaRouter;Landroid/support/v7/media/MediaRouter$RouteInfo;)V
    .locals 1

    .prologue
    .line 336
    iget-object v0, p0, Lcom/google/android/apps/chrome/third_party/ChromeMediaRouteControllerDialog$MediaRouterCallback;->this$0:Lcom/google/android/apps/chrome/third_party/ChromeMediaRouteControllerDialog;

    # invokes: Lcom/google/android/apps/chrome/third_party/ChromeMediaRouteControllerDialog;->update()Z
    invoke-static {v0}, Lcom/google/android/apps/chrome/third_party/ChromeMediaRouteControllerDialog;->access$500(Lcom/google/android/apps/chrome/third_party/ChromeMediaRouteControllerDialog;)Z

    .line 337
    return-void
.end method

.method public final onRouteUnselected(Landroid/support/v7/media/MediaRouter;Landroid/support/v7/media/MediaRouter$RouteInfo;)V
    .locals 1

    .prologue
    .line 331
    iget-object v0, p0, Lcom/google/android/apps/chrome/third_party/ChromeMediaRouteControllerDialog$MediaRouterCallback;->this$0:Lcom/google/android/apps/chrome/third_party/ChromeMediaRouteControllerDialog;

    # invokes: Lcom/google/android/apps/chrome/third_party/ChromeMediaRouteControllerDialog;->update()Z
    invoke-static {v0}, Lcom/google/android/apps/chrome/third_party/ChromeMediaRouteControllerDialog;->access$500(Lcom/google/android/apps/chrome/third_party/ChromeMediaRouteControllerDialog;)Z

    .line 332
    return-void
.end method

.method public final onRouteVolumeChanged(Landroid/support/v7/media/MediaRouter;Landroid/support/v7/media/MediaRouter$RouteInfo;)V
    .locals 1

    .prologue
    .line 341
    iget-object v0, p0, Lcom/google/android/apps/chrome/third_party/ChromeMediaRouteControllerDialog$MediaRouterCallback;->this$0:Lcom/google/android/apps/chrome/third_party/ChromeMediaRouteControllerDialog;

    # getter for: Lcom/google/android/apps/chrome/third_party/ChromeMediaRouteControllerDialog;->mRoute:Landroid/support/v7/media/MediaRouter$RouteInfo;
    invoke-static {v0}, Lcom/google/android/apps/chrome/third_party/ChromeMediaRouteControllerDialog;->access$300(Lcom/google/android/apps/chrome/third_party/ChromeMediaRouteControllerDialog;)Landroid/support/v7/media/MediaRouter$RouteInfo;

    move-result-object v0

    if-ne p2, v0, :cond_0

    .line 342
    iget-object v0, p0, Lcom/google/android/apps/chrome/third_party/ChromeMediaRouteControllerDialog$MediaRouterCallback;->this$0:Lcom/google/android/apps/chrome/third_party/ChromeMediaRouteControllerDialog;

    # invokes: Lcom/google/android/apps/chrome/third_party/ChromeMediaRouteControllerDialog;->updateVolume()V
    invoke-static {v0}, Lcom/google/android/apps/chrome/third_party/ChromeMediaRouteControllerDialog;->access$200(Lcom/google/android/apps/chrome/third_party/ChromeMediaRouteControllerDialog;)V

    .line 344
    :cond_0
    return-void
.end method
