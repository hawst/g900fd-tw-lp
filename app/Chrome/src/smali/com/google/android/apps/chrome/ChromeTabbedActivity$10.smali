.class Lcom/google/android/apps/chrome/ChromeTabbedActivity$10;
.super Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;
.source "ChromeTabbedActivity.java"


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/ChromeTabbedActivity;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 951
    const-class v0, Lcom/google/android/apps/chrome/ChromeTabbedActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/ChromeTabbedActivity$10;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>(Lcom/google/android/apps/chrome/ChromeTabbedActivity;)V
    .locals 0

    .prologue
    .line 951
    iput-object p1, p0, Lcom/google/android/apps/chrome/ChromeTabbedActivity$10;->this$0:Lcom/google/android/apps/chrome/ChromeTabbedActivity;

    invoke-direct {p0}, Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 954
    iget v0, p1, Landroid/os/Message;->what:I

    sparse-switch v0, :sswitch_data_0

    .line 1002
    sget-boolean v0, Lcom/google/android/apps/chrome/ChromeTabbedActivity$10;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 956
    :sswitch_0
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    const-string/jumbo v1, "type"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lorg/chromium/chrome/browser/tabmodel/TabModel$TabLaunchType;->valueOf(Ljava/lang/String;)Lorg/chromium/chrome/browser/tabmodel/TabModel$TabLaunchType;

    move-result-object v0

    .line 958
    sget-object v1, Lorg/chromium/chrome/browser/tabmodel/TabModel$TabLaunchType;->FROM_LONGPRESS_BACKGROUND:Lorg/chromium/chrome/browser/tabmodel/TabModel$TabLaunchType;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeTabbedActivity$10;->this$0:Lcom/google/android/apps/chrome/ChromeTabbedActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/chrome/device/DeviceClassManager;->enableAnimations(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 960
    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeTabbedActivity$10;->this$0:Lcom/google/android/apps/chrome/ChromeTabbedActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    sget v1, Lcom/google/android/apps/chrome/R$string;->open_in_new_tab_toast:I

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 1004
    :cond_0
    :goto_0
    return-void

    .line 969
    :sswitch_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeTabbedActivity$10;->this$0:Lcom/google/android/apps/chrome/ChromeTabbedActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/chrome/partnercustomizations/HomepageManager;->isHomepageEnabled(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeTabbedActivity$10;->this$0:Lcom/google/android/apps/chrome/ChromeTabbedActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->getTabModelSelector()Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

    move-result-object v0

    invoke-interface {v0}, Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;->getTotalTabCount()I

    move-result v0

    if-nez v0, :cond_0

    .line 971
    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeTabbedActivity$10;->this$0:Lcom/google/android/apps/chrome/ChromeTabbedActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->finish()V

    goto :goto_0

    .line 975
    :sswitch_2
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    const-string/jumbo v1, "shown"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    .line 976
    if-eqz v0, :cond_1

    .line 977
    invoke-static {}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->recordActionBarShown()V

    .line 979
    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/chrome/ChromeTabbedActivity$10;->this$0:Lcom/google/android/apps/chrome/ChromeTabbedActivity;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v1

    .line 980
    if-nez v0, :cond_2

    if-eqz v1, :cond_2

    .line 981
    invoke-virtual {v1}, Landroid/app/ActionBar;->hide()V

    .line 983
    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/chrome/ChromeTabbedActivity$10;->this$0:Lcom/google/android/apps/chrome/ChromeTabbedActivity;

    invoke-static {v1}, Lorg/chromium/ui/base/DeviceFormFactor;->isTablet(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 984
    if-eqz v0, :cond_3

    .line 985
    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeTabbedActivity$10;->this$0:Lcom/google/android/apps/chrome/ChromeTabbedActivity;

    # getter for: Lcom/google/android/apps/chrome/ChromeTabbedActivity;->mContextualMenuBar:Lcom/google/android/apps/chrome/ContextualMenuBar;
    invoke-static {v0}, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->access$1100(Lcom/google/android/apps/chrome/ChromeTabbedActivity;)Lcom/google/android/apps/chrome/ContextualMenuBar;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ContextualMenuBar;->showControls()V

    goto :goto_0

    .line 987
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeTabbedActivity$10;->this$0:Lcom/google/android/apps/chrome/ChromeTabbedActivity;

    # getter for: Lcom/google/android/apps/chrome/ChromeTabbedActivity;->mContextualMenuBar:Lcom/google/android/apps/chrome/ContextualMenuBar;
    invoke-static {v0}, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->access$1100(Lcom/google/android/apps/chrome/ChromeTabbedActivity;)Lcom/google/android/apps/chrome/ContextualMenuBar;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ContextualMenuBar;->hideControls()V

    goto :goto_0

    .line 994
    :sswitch_3
    invoke-static {}, Lcom/google/android/apps/chrome/partnercustomizations/PartnerBrowserCustomizations;->isIncognitoDisabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 995
    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeTabbedActivity$10;->this$0:Lcom/google/android/apps/chrome/ChromeTabbedActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->getTabModelSelector()Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

    move-result-object v0

    invoke-interface {v0, v1}, Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;->getModel(Z)Lorg/chromium/chrome/browser/tabmodel/TabModel;

    move-result-object v0

    invoke-interface {v0}, Lorg/chromium/chrome/browser/tabmodel/TabModel;->closeAllTabs()V

    goto :goto_0

    .line 999
    :sswitch_4
    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeTabbedActivity$10;->this$0:Lcom/google/android/apps/chrome/ChromeTabbedActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->getTabModelSelector()Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

    move-result-object v0

    invoke-interface {v0, v1}, Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;->getModel(Z)Lorg/chromium/chrome/browser/tabmodel/TabModel;

    move-result-object v0

    invoke-interface {v0}, Lorg/chromium/chrome/browser/tabmodel/TabModel;->closeAllTabs()V

    goto :goto_0

    .line 954
    nop

    :sswitch_data_0
    .sparse-switch
        0x2 -> :sswitch_0
        0x5 -> :sswitch_1
        0x22 -> :sswitch_2
        0x48 -> :sswitch_3
        0x4a -> :sswitch_4
    .end sparse-switch
.end method
