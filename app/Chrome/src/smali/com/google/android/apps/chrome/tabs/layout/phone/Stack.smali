.class public Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;
.super Ljava/lang/Object;
.source "Stack.java"


# static fields
.field static final synthetic $assertionsDisabled:Z

.field public static final DISCARD_RANGE_SCREEN:F = 0.7f

.field private static final DRAG_ANGLE_THRESHOLD:F

.field public static final MAX_NUMBER_OF_STACKED_TABS_BOTTOM:I = 0x3

.field public static final MAX_NUMBER_OF_STACKED_TABS_TOP:I = 0x3

.field public static final SPACING_SCREEN:F = 0.26f


# instance fields
.field private mAnimationFactory:Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation;

.field private mBorderLeftPadding:F

.field private mBorderTopPadding:F

.field private mBorderTransparentSide:F

.field private mBorderTransparentTop:F

.field private mCurrentMode:I

.field private mCurrentScrollDirection:F

.field private mDiscardDirection:F

.field private mDiscardingTab:Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

.field private mDragLock:Lcom/google/android/apps/chrome/tabs/layout/phone/Stack$DragLock;

.field private mEvenOutProgress:F

.field private mEvenOutRate:F

.field private mInSwipe:Z

.field private mIsDying:Z

.field private mLastPinch0Offset:F

.field private mLastPinch1Offset:F

.field private mLastScrollUpdate:J

.field private final mLayout:Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;

.field private mLongPressSelected:I

.field private mMaxOverScroll:F

.field private mMaxOverScrollAngle:F

.field private mMaxOverScrollSlide:F

.field private mMaxUnderScroll:F

.field private mMinScrollMotion:F

.field private mMinSpacing:F

.field private final mOverScrollAngleInterpolator:Landroid/view/animation/Interpolator;

.field private mOverScrollCounter:I

.field private mOverScrollDerivative:I

.field private mOverScrollOffset:F

.field private final mOverscrollSlideInterpolator:Landroid/view/animation/Interpolator;

.field private mOverviewAnimationType:Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;

.field private mPinch0TabIndex:I

.field private mPinch1TabIndex:I

.field private mRecomputePosition:Z

.field private mReferenceOrderIndex:I

.field private mScrollOffset:F

.field private mScrollOffsetForDyingTabs:F

.field private mScrollTarget:F

.field private mScroller:Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller;

.field private mScrollingTab:Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

.field private mSpacing:I

.field private mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

.field private mSwipeBoundedScrollOffset:F

.field private mSwipeCanScroll:Z

.field private mSwipeIsCancelable:Z

.field private mSwipeUnboundScrollOffset:F

.field private mTabAnimations:Lcom/google/android/apps/chrome/ChromeAnimation;

.field private mTabModel:Lorg/chromium/chrome/browser/tabmodel/TabModel;

.field private final mUnderScrollAngleInterpolator:Landroid/view/animation/Interpolator;

.field private mViewAnimationFactory:Lcom/google/android/apps/chrome/tabs/layout/phone/StackViewAnimation;

.field private mViewAnimations:Landroid/animation/AnimatorSet;

.field private final mViewAnimatorListener:Landroid/animation/AnimatorListenerAdapter;

.field private mWarpSize:F


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 38
    const-class v0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->$assertionsDisabled:Z

    .line 121
    const-wide/high16 v0, 0x403e000000000000L    # 30.0

    invoke-static {v0, v1}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Math;->tan(D)D

    move-result-wide v0

    double-to-float v0, v0

    sput v0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->DRAG_ANGLE_THRESHOLD:F

    return-void

    .line 38
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/high16 v0, 0x3f800000    # 1.0f

    const/4 v3, -0x1

    const/4 v2, 0x0

    .line 264
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 165
    iput v3, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mLongPressSelected:I

    .line 169
    iput v3, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mPinch0TabIndex:I

    .line 170
    iput v3, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mPinch1TabIndex:I

    .line 175
    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mEvenOutProgress:F

    .line 177
    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mEvenOutRate:F

    .line 188
    new-instance v0, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v0}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mOverScrollAngleInterpolator:Landroid/view/animation/Interpolator;

    .line 190
    invoke-static {}, Lcom/google/android/apps/chrome/ChromeAnimation;->getDecelerateInterpolator()Landroid/view/animation/DecelerateInterpolator;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mUnderScrollAngleInterpolator:Landroid/view/animation/Interpolator;

    .line 192
    new-instance v0, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v0}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mOverscrollSlideInterpolator:Landroid/view/animation/Interpolator;

    .line 196
    sget-object v0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack$DragLock;->NONE:Lcom/google/android/apps/chrome/tabs/layout/phone/Stack$DragLock;

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mDragLock:Lcom/google/android/apps/chrome/tabs/layout/phone/Stack$DragLock;

    .line 197
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mLastScrollUpdate:J

    .line 198
    iput v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mMinScrollMotion:F

    .line 201
    iput v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mScrollTarget:F

    .line 202
    iput v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mScrollOffset:F

    .line 203
    iput v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mScrollOffsetForDyingTabs:F

    .line 204
    iput v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mCurrentScrollDirection:F

    .line 205
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mScrollingTab:Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    .line 219
    const/high16 v0, 0x7fc00000    # NaNf

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mDiscardDirection:F

    .line 223
    iput-boolean v4, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mRecomputePosition:Z

    .line 225
    iput v3, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mReferenceOrderIndex:I

    .line 228
    iput v4, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mCurrentMode:I

    .line 231
    sget-object v0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;->NONE:Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mOverviewAnimationType:Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;

    .line 249
    new-instance v0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack$1;-><init>(Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mViewAnimatorListener:Landroid/animation/AnimatorListenerAdapter;

    .line 265
    iput-object p2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mLayout:Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;

    .line 266
    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->contextChanged(Landroid/content/Context;)V

    .line 267
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;)Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mLayout:Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;

    return-object v0
.end method

.method private allowOverscroll()Z
    .locals 2

    .prologue
    .line 2136
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mOverviewAnimationType:Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;

    sget-object v1, Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;->NONE:Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mOverviewAnimationType:Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;

    sget-object v1, Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;->VIEW_MORE:Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mOverviewAnimationType:Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;

    sget-object v1, Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;->ENTER_STACK:Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;

    if-ne v0, v1, :cond_1

    :cond_0
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mPinch0TabIndex:I

    if-gez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private approxScreen(Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;F)F
    .locals 2

    .prologue
    .line 2238
    invoke-virtual {p1}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getScrollOffset()F

    move-result v0

    add-float/2addr v0, p2

    iget v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mWarpSize:F

    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->scrollToScreen(FF)F

    move-result v0

    return v0
.end method

.method private canUpdateAnimation(JLcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;IZ)Z
    .locals 2

    .prologue
    .line 656
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mAnimationFactory:Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation;

    if-eqz v0, :cond_2

    .line 657
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mOverviewAnimationType:Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;

    sget-object v1, Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;->DISCARD:Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mOverviewAnimationType:Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;

    sget-object v1, Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;->UNDISCARD:Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mOverviewAnimationType:Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;

    sget-object v1, Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;->DISCARD_ALL:Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;

    if-ne v0, v1, :cond_2

    :cond_0
    sget-object v0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;->DISCARD:Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;

    if-eq p3, v0, :cond_1

    sget-object v0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;->UNDISCARD:Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;

    if-eq p3, v0, :cond_1

    sget-object v0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;->DISCARD_ALL:Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;

    if-ne p3, v0, :cond_2

    .line 663
    :cond_1
    const/4 v0, 0x1

    .line 666
    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private cancelDiscardScrollingAnimation()Z
    .locals 3

    .prologue
    .line 674
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mOverviewAnimationType:Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;

    sget-object v1, Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;->DISCARD:Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mOverviewAnimationType:Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;

    sget-object v1, Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;->UNDISCARD:Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mOverviewAnimationType:Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;

    sget-object v1, Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;->DISCARD_ALL:Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;

    if-ne v0, v1, :cond_1

    .line 677
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mTabAnimations:Lcom/google/android/apps/chrome/ChromeAnimation;

    const/4 v1, 0x0

    sget-object v2, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab$Property;->SCROLL_OFFSET:Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab$Property;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/chrome/ChromeAnimation;->cancel(Ljava/lang/Object;Ljava/lang/Enum;)V

    .line 678
    const/4 v0, 0x1

    .line 680
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private commitDiscard(JZ)V
    .locals 3

    .prologue
    .line 1229
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mDiscardingTab:Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    if-nez v0, :cond_0

    .line 1242
    :goto_0
    return-void

    .line 1231
    :cond_0
    sget-boolean v0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 1232
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mDiscardingTab:Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    .line 1233
    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getDiscardAmount()F

    move-result v1

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    invoke-direct {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->getDiscardRange()F

    move-result v2

    div-float/2addr v1, v2

    const v2, 0x3ecccccd    # 0.4f

    cmpl-float v1, v1, v2

    if-lez v1, :cond_2

    if-eqz p3, :cond_2

    .line 1235
    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mLayout:Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getId()I

    move-result v0

    invoke-virtual {v1, p1, p2, v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->uiRequestingCloseTab(JI)V

    .line 1236
    invoke-static {}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->stackViewSwipeCloseTab()V

    .line 1240
    :goto_1
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mDiscardingTab:Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    .line 1241
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->requestUpdate()V

    goto :goto_0

    .line 1238
    :cond_2
    sget-object v0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;->UNDISCARD:Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->startAnimation(JLcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;)V

    goto :goto_1
.end method

.method public static computeDiscardAlpha(FF)F
    .locals 3

    .prologue
    const/high16 v0, 0x3f800000    # 1.0f

    .line 2286
    invoke-static {p0}, Ljava/lang/Math;->abs(F)F

    move-result v1

    cmpg-float v1, v1, v0

    if-gez v1, :cond_0

    .line 2289
    :goto_0
    return v0

    .line 2287
    :cond_0
    div-float v1, p0, p1

    .line 2288
    const/high16 v2, -0x40800000    # -1.0f

    invoke-static {v1, v2, v0}, Lorg/chromium/chrome/browser/util/MathUtils;->clamp(FFF)F

    move-result v1

    .line 2289
    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    sub-float/2addr v0, v1

    goto :goto_0
.end method

.method public static computeDiscardScale(FFZ)F
    .locals 3

    .prologue
    const/high16 v1, 0x3f800000    # 1.0f

    .line 2272
    invoke-static {p0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    cmpg-float v0, v0, v1

    if-gez v0, :cond_0

    move v0, v1

    .line 2275
    :goto_0
    return v0

    .line 2273
    :cond_0
    div-float v2, p0, p1

    .line 2274
    if-eqz p2, :cond_1

    const v0, 0x3f333333    # 0.7f

    .line 2275
    :goto_1
    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    invoke-static {v1, v0, v2}, Lcom/google/android/apps/chrome/utilities/ChromeMathUtils;->interpolate(FFF)F

    move-result v0

    goto :goto_0

    .line 2274
    :cond_1
    const/high16 v0, 0x3f000000    # 0.5f

    goto :goto_1
.end method

.method private computeDragLock(FF)Lcom/google/android/apps/chrome/tabs/layout/phone/Stack$DragLock;
    .locals 10

    .prologue
    .line 744
    invoke-static {p1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    .line 745
    invoke-static {p2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    .line 746
    sget v0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->DRAG_ANGLE_THRESHOLD:F

    mul-float/2addr v0, v2

    cmpl-float v0, v0, v1

    if-lez v0, :cond_5

    sget-object v0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack$DragLock;->DISCARD:Lcom/google/android/apps/chrome/tabs/layout/phone/Stack$DragLock;

    .line 749
    :goto_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    .line 750
    iget-wide v6, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mLastScrollUpdate:J

    sub-long v6, v4, v6

    const-wide/16 v8, 0x190

    cmp-long v3, v6, v8

    if-lez v3, :cond_0

    .line 751
    sget-object v3, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack$DragLock;->NONE:Lcom/google/android/apps/chrome/tabs/layout/phone/Stack$DragLock;

    iput-object v3, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mDragLock:Lcom/google/android/apps/chrome/tabs/layout/phone/Stack$DragLock;

    .line 754
    :cond_0
    iget-object v3, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mDragLock:Lcom/google/android/apps/chrome/tabs/layout/phone/Stack$DragLock;

    sget-object v6, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack$DragLock;->NONE:Lcom/google/android/apps/chrome/tabs/layout/phone/Stack$DragLock;

    if-ne v3, v6, :cond_1

    sub-float v3, v1, v2

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    iget v6, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mMinScrollMotion:F

    cmpl-float v3, v3, v6

    if-gtz v3, :cond_3

    :cond_1
    iget-object v3, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mDragLock:Lcom/google/android/apps/chrome/tabs/layout/phone/Stack$DragLock;

    sget-object v6, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack$DragLock;->DISCARD:Lcom/google/android/apps/chrome/tabs/layout/phone/Stack$DragLock;

    if-ne v3, v6, :cond_2

    iget v3, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mMinScrollMotion:F

    cmpl-float v2, v2, v3

    if-gtz v2, :cond_3

    :cond_2
    iget-object v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mDragLock:Lcom/google/android/apps/chrome/tabs/layout/phone/Stack$DragLock;

    sget-object v3, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack$DragLock;->SCROLL:Lcom/google/android/apps/chrome/tabs/layout/phone/Stack$DragLock;

    if-ne v2, v3, :cond_4

    iget v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mMinScrollMotion:F

    cmpl-float v1, v1, v2

    if-lez v1, :cond_4

    .line 757
    :cond_3
    iput-wide v4, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mLastScrollUpdate:J

    .line 758
    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mDragLock:Lcom/google/android/apps/chrome/tabs/layout/phone/Stack$DragLock;

    sget-object v2, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack$DragLock;->NONE:Lcom/google/android/apps/chrome/tabs/layout/phone/Stack$DragLock;

    if-ne v1, v2, :cond_4

    .line 759
    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mDragLock:Lcom/google/android/apps/chrome/tabs/layout/phone/Stack$DragLock;

    .line 763
    :cond_4
    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mDragLock:Lcom/google/android/apps/chrome/tabs/layout/phone/Stack$DragLock;

    sget-object v2, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack$DragLock;->NONE:Lcom/google/android/apps/chrome/tabs/layout/phone/Stack$DragLock;

    if-ne v1, v2, :cond_6

    :goto_1
    return-object v0

    .line 746
    :cond_5
    sget-object v0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack$DragLock;->SCROLL:Lcom/google/android/apps/chrome/tabs/layout/phone/Stack$DragLock;

    goto :goto_0

    .line 763
    :cond_6
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mDragLock:Lcom/google/android/apps/chrome/tabs/layout/phone/Stack$DragLock;

    goto :goto_1
.end method

.method private computeOverscrollPercent()F
    .locals 2

    .prologue
    .line 1780
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mOverScrollOffset:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-ltz v0, :cond_0

    .line 1781
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mOverScrollOffset:F

    iget v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mMaxOverScroll:F

    div-float/2addr v0, v1

    .line 1783
    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mOverScrollOffset:F

    iget v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mMaxUnderScroll:F

    div-float/2addr v0, v1

    goto :goto_0
.end method

.method private computeSpacing(I)I
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 2054
    .line 2055
    const/4 v1, 0x1

    if-le p1, v1, :cond_3

    .line 2056
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->getScrollDimensionSize()F

    move-result v2

    .line 2057
    const v1, 0x3e851eb8    # 0.26f

    mul-float/2addr v1, v2

    iget v3, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mMinSpacing:F

    invoke-static {v1, v3}, Ljava/lang/Math;->max(FF)F

    move-result v1

    float-to-int v1, v1

    .line 2058
    iget-object v3, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    if-eqz v3, :cond_2

    .line 2059
    :goto_0
    iget-object v3, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    array-length v3, v3

    if-ge v0, v3, :cond_2

    .line 2060
    sget-boolean v3, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->$assertionsDisabled:Z

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    aget-object v3, v3, v0

    if-nez v3, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 2061
    :cond_0
    iget-object v3, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    aget-object v3, v3, v0

    invoke-virtual {v3}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->isDying()Z

    move-result v3

    if-nez v3, :cond_1

    .line 2062
    int-to-float v1, v1

    iget-object v3, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    aget-object v3, v3, v0

    iget v4, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mCurrentMode:I

    invoke-virtual {v3, v4}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getSizeInScrollDirection(I)F

    move-result v3

    invoke-static {v1, v3}, Ljava/lang/Math;->min(FF)F

    move-result v1

    float-to-int v1, v1

    .line 2059
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2067
    :cond_2
    const/high16 v0, 0x41a00000    # 20.0f

    sub-float v0, v2, v0

    int-to-float v2, p1

    const v3, 0x3f4ccccd    # 0.8f

    mul-float/2addr v2, v3

    div-float/2addr v0, v2

    float-to-int v0, v0

    .line 2068
    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 2070
    :cond_3
    return v0
.end method

.method private computeTabClippingVisibilityHelper()V
    .locals 12

    .prologue
    .line 1625
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mCurrentMode:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    const/4 v0, 0x1

    move v9, v0

    .line 1632
    :goto_0
    if-eqz v9, :cond_2

    .line 1634
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mLayout:Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->getHeight()F

    move-result v0

    sget v1, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->sStackedTabVisibleSize:F

    add-float/2addr v0, v1

    .line 1643
    :goto_1
    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    move v8, v1

    move v6, v0

    :goto_2
    if-ltz v8, :cond_f

    .line 1644
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    aget-object v0, v0, v8

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getLayoutTab()Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    move-result-object v10

    .line 1645
    const/4 v0, 0x1

    invoke-virtual {v10, v0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->setVisible(Z)V

    .line 1649
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    aget-object v0, v0, v8

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->isDying()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    aget-object v0, v0, v8

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getXInStackOffset()F

    move-result v0

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    invoke-virtual {v10}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getAlpha()F

    move-result v0

    const/high16 v1, 0x3f800000    # 1.0f

    cmpg-float v0, v0, v1

    if-gez v0, :cond_4

    .line 1652
    :cond_0
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-virtual {v10, v0, v1}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->setClipOffset(FF)V

    .line 1653
    const v0, 0x7f7fffff    # Float.MAX_VALUE

    const v1, 0x7f7fffff    # Float.MAX_VALUE

    invoke-virtual {v10, v0, v1}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->setClipSize(FF)V

    move v0, v6

    .line 1643
    :goto_3
    add-int/lit8 v1, v8, -0x1

    move v8, v1

    move v6, v0

    goto :goto_2

    .line 1625
    :cond_1
    const/4 v0, 0x0

    move v9, v0

    goto :goto_0

    .line 1635
    :cond_2
    invoke-static {}, Lorg/chromium/ui/base/LocalizationUtils;->isLayoutRtl()Z

    move-result v0

    if-nez v0, :cond_3

    .line 1637
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mLayout:Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->getWidth()F

    move-result v0

    sget v1, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->sStackedTabVisibleSize:F

    add-float/2addr v0, v1

    goto :goto_1

    .line 1640
    :cond_3
    sget v0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->sStackedTabVisibleSize:F

    neg-float v0, v0

    goto :goto_1

    .line 1659
    :cond_4
    if-eqz v9, :cond_5

    .line 1661
    invoke-virtual {v10}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getY()F

    move-result v4

    .line 1662
    invoke-virtual {v10}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getScaledContentHeight()F

    move-result v3

    .line 1663
    sub-float v0, v6, v4

    invoke-static {v3, v0}, Ljava/lang/Math;->min(FF)F

    move-result v2

    .line 1664
    iget v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mBorderTransparentTop:F

    .line 1665
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mBorderTopPadding:F

    .line 1682
    :goto_4
    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v5

    .line 1684
    cmpg-float v7, v2, v5

    if-gtz v7, :cond_7

    .line 1686
    const/4 v3, 0x0

    invoke-virtual {v10, v3}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->setVisible(Z)V

    .line 1687
    const/4 v3, 0x1

    invoke-virtual {v10, v3}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->setDrawDecoration(Z)V

    .line 1720
    :goto_5
    if-lez v8, :cond_11

    .line 1721
    iget-object v3, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    add-int/lit8 v5, v8, -0x1

    aget-object v3, v3, v5

    invoke-virtual {v3}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getLayoutTab()Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    move-result-object v3

    .line 1722
    invoke-virtual {v3}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getScale()F

    move-result v3

    invoke-virtual {v10}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getScale()F

    move-result v5

    cmpg-float v3, v3, v5

    if-gtz v3, :cond_e

    .line 1731
    :goto_6
    add-float/2addr v1, v4

    .line 1733
    invoke-virtual {v10}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getBorderAlpha()F

    move-result v2

    const/high16 v3, 0x3f800000    # 1.0f

    cmpg-float v2, v2, v3

    if-gez v2, :cond_10

    invoke-virtual {v10}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getToolbarAlpha()F

    move-result v2

    const/high16 v3, 0x3f800000    # 1.0f

    cmpg-float v2, v2, v3

    if-gez v2, :cond_10

    .line 1734
    add-float/2addr v0, v1

    goto :goto_3

    .line 1666
    :cond_5
    invoke-static {}, Lorg/chromium/ui/base/LocalizationUtils;->isLayoutRtl()Z

    move-result v0

    if-nez v0, :cond_6

    .line 1668
    invoke-virtual {v10}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getX()F

    move-result v4

    .line 1669
    invoke-virtual {v10}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getScaledContentWidth()F

    move-result v3

    .line 1670
    sub-float v0, v6, v4

    invoke-static {v3, v0}, Ljava/lang/Math;->min(FF)F

    move-result v2

    .line 1671
    iget v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mBorderTransparentSide:F

    .line 1672
    const/4 v0, 0x0

    goto :goto_4

    .line 1675
    :cond_6
    invoke-virtual {v10}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getX()F

    move-result v0

    invoke-virtual {v10}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getScaledContentWidth()F

    move-result v1

    add-float v4, v0, v1

    .line 1676
    invoke-virtual {v10}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getScaledContentWidth()F

    move-result v3

    .line 1677
    sub-float v0, v4, v6

    invoke-static {v3, v0}, Ljava/lang/Math;->min(FF)F

    move-result v2

    .line 1678
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mBorderTransparentSide:F

    neg-float v1, v0

    .line 1679
    const/4 v0, 0x0

    goto :goto_4

    .line 1691
    :cond_7
    sub-float v5, v2, v5

    sget v7, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->sStackedTabVisibleSize:F

    div-float/2addr v5, v7

    const/4 v7, 0x0

    const/high16 v11, 0x3f800000    # 1.0f

    invoke-static {v5, v7, v11}, Lorg/chromium/chrome/browser/util/MathUtils;->clamp(FFF)F

    move-result v5

    .line 1693
    invoke-virtual {v10, v5}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->setDecorationAlpha(F)V

    .line 1699
    const/high16 v5, 0x3f800000    # 1.0f

    .line 1700
    invoke-virtual {v10}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getTiltX()F

    move-result v7

    const/4 v11, 0x0

    cmpl-float v7, v7, v11

    if-gtz v7, :cond_8

    if-nez v9, :cond_a

    invoke-static {}, Lorg/chromium/ui/base/LocalizationUtils;->isLayoutRtl()Z

    move-result v7

    if-eqz v7, :cond_a

    invoke-virtual {v10}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getTiltY()F

    move-result v7

    const/4 v11, 0x0

    cmpg-float v7, v7, v11

    if-gez v7, :cond_9

    .line 1704
    :cond_8
    :goto_7
    invoke-virtual {v10}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getTiltX()F

    move-result v5

    invoke-virtual {v10}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getTiltY()F

    move-result v7

    invoke-static {v7}, Ljava/lang/Math;->abs(F)F

    move-result v7

    invoke-static {v5, v7}, Ljava/lang/Math;->max(FF)F

    move-result v5

    .line 1706
    const/high16 v7, 0x3f800000    # 1.0f

    iget v11, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mMaxOverScrollAngle:F

    div-float/2addr v5, v11

    const v11, 0x3f19999a    # 0.6f

    mul-float/2addr v5, v11

    add-float/2addr v5, v7

    .line 1709
    :cond_9
    mul-float/2addr v5, v2

    invoke-static {v5, v3}, Ljava/lang/Math;->min(FF)F

    move-result v5

    .line 1711
    if-nez v9, :cond_b

    invoke-static {}, Lorg/chromium/ui/base/LocalizationUtils;->isLayoutRtl()Z

    move-result v7

    if-eqz v7, :cond_b

    sub-float/2addr v3, v5

    :goto_8
    const/4 v7, 0x0

    invoke-virtual {v10, v3, v7}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->setClipOffset(FF)V

    .line 1714
    if-eqz v9, :cond_c

    const v3, 0x7f7fffff    # Float.MAX_VALUE

    move v7, v3

    :goto_9
    if-eqz v9, :cond_d

    move v3, v5

    :goto_a
    invoke-virtual {v10, v7, v3}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->setClipSize(FF)V

    goto/16 :goto_5

    .line 1700
    :cond_a
    invoke-virtual {v10}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getTiltY()F

    move-result v7

    const/4 v11, 0x0

    cmpl-float v7, v7, v11

    if-lez v7, :cond_9

    goto :goto_7

    .line 1711
    :cond_b
    const/4 v3, 0x0

    goto :goto_8

    :cond_c
    move v7, v5

    .line 1714
    goto :goto_9

    :cond_d
    const v3, 0x7f7fffff    # Float.MAX_VALUE

    goto :goto_a

    .line 1725
    :cond_e
    invoke-virtual {v10}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getScale()F

    move-result v3

    mul-float/2addr v2, v3

    add-float/2addr v4, v2

    goto/16 :goto_6

    .line 1738
    :cond_f
    return-void

    :cond_10
    move v0, v1

    goto/16 :goto_3

    :cond_11
    move v0, v6

    goto/16 :goto_3
.end method

.method private computeTabOffsetHelper(Landroid/graphics/RectF;)V
    .locals 19

    .prologue
    .line 1472
    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mCurrentMode:I

    const/4 v3, 0x1

    if-ne v2, v3, :cond_0

    const/4 v2, 0x1

    .line 1475
    :goto_0
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/RectF;->width()F

    move-result v9

    .line 1476
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/RectF;->height()F

    move-result v10

    .line 1477
    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->computeOverscrollPercent()F

    move-result v11

    .line 1478
    move-object/from16 v0, p0

    iget v3, v0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mScrollOffset:F

    const/4 v4, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->getMinScroll(Z)F

    move-result v4

    const/4 v5, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->getMaxScroll(Z)F

    move-result v5

    invoke-static {v3, v4, v5}, Lorg/chromium/chrome/browser/util/MathUtils;->clamp(FFF)F

    move-result v7

    .line 1480
    invoke-direct/range {p0 .. p1}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->getStackScale(Landroid/graphics/RectF;)F

    move-result v12

    .line 1482
    const/4 v5, 0x0

    .line 1483
    const/4 v4, 0x0

    .line 1484
    const/4 v3, 0x0

    :goto_1
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    array-length v6, v6

    if-ge v3, v6, :cond_8

    .line 1485
    sget-boolean v6, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->$assertionsDisabled:Z

    if-nez v6, :cond_1

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    aget-object v6, v6, v3

    if-nez v6, :cond_1

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    .line 1472
    :cond_0
    const/4 v2, 0x0

    goto :goto_0

    .line 1486
    :cond_1
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    aget-object v8, v6, v3

    .line 1487
    invoke-virtual {v8}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getLayoutTab()Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    move-result-object v13

    .line 1490
    invoke-virtual {v8}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->isDying()Z

    move-result v6

    if-eqz v6, :cond_4

    move-object/from16 v0, p0

    iget v6, v0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mScrollOffsetForDyingTabs:F

    .line 1492
    :goto_2
    move-object/from16 v0, p0

    invoke-direct {v0, v8, v6}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->approxScreen(Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;F)F

    move-result v6

    .line 1495
    invoke-static {v4, v6}, Ljava/lang/Math;->max(FF)F

    move-result v6

    .line 1496
    const/4 v14, 0x3

    if-ge v5, v14, :cond_2

    .line 1499
    invoke-virtual {v13}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getTiltX()F

    move-result v14

    float-to-double v14, v14

    invoke-static {v14, v15}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v14

    invoke-static {v14, v15}, Ljava/lang/Math;->cos(D)D

    move-result-wide v14

    double-to-float v14, v14

    .line 1500
    invoke-virtual {v13}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getTiltY()F

    move-result v15

    float-to-double v0, v15

    move-wide/from16 v16, v0

    invoke-static/range {v16 .. v17}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v16

    invoke-static/range {v16 .. v17}, Ljava/lang/Math;->cos(D)D

    move-result-wide v16

    move-wide/from16 v0, v16

    double-to-float v15, v0

    .line 1501
    invoke-static {v14}, Ljava/lang/Math;->abs(F)F

    move-result v14

    invoke-static {v15}, Ljava/lang/Math;->abs(F)F

    move-result v15

    invoke-static {v14, v15}, Ljava/lang/Math;->min(FF)F

    move-result v14

    .line 1502
    invoke-virtual {v13}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getAlpha()F

    move-result v15

    mul-float/2addr v14, v15

    .line 1503
    sget v15, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->sStackedTabVisibleSize:F

    mul-float/2addr v14, v15

    add-float/2addr v4, v14

    .line 1505
    :cond_2
    invoke-virtual {v8}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->isDying()Z

    move-result v8

    if-eqz v8, :cond_5

    const/4 v8, 0x0

    :goto_3
    add-int/2addr v5, v8

    .line 1506
    const/4 v8, 0x0

    cmpg-float v8, v11, v8

    if-gez v8, :cond_3

    .line 1510
    const/high16 v8, 0x3e800000    # 0.25f

    div-float v8, v11, v8

    mul-float/2addr v8, v6

    add-float/2addr v6, v8

    .line 1512
    const/4 v8, 0x0

    invoke-static {v8, v6}, Ljava/lang/Math;->max(FF)F

    move-result v6

    .line 1519
    :cond_3
    invoke-virtual {v13}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getScaledContentWidth()F

    move-result v8

    sub-float v8, v9, v8

    const/high16 v14, 0x40000000    # 2.0f

    div-float/2addr v8, v14

    .line 1520
    invoke-virtual {v13}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getScaledContentHeight()F

    move-result v14

    sub-float v14, v10, v14

    const/high16 v15, 0x40000000    # 2.0f

    div-float/2addr v14, v15

    .line 1525
    invoke-virtual {v13}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getOriginalContentWidth()F

    move-result v15

    const v16, 0x3f666666    # 0.9f

    mul-float v15, v15, v16

    mul-float/2addr v15, v12

    sub-float v15, v9, v15

    const/high16 v16, 0x40000000    # 2.0f

    div-float v15, v15, v16

    .line 1528
    invoke-virtual {v13}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getOriginalContentHeight()F

    move-result v16

    const v17, 0x3f666666    # 0.9f

    mul-float v16, v16, v17

    mul-float v16, v16, v12

    sub-float v16, v10, v16

    const/high16 v17, 0x40000000    # 2.0f

    div-float v16, v16, v17

    .line 1532
    if-eqz v2, :cond_6

    .line 1533
    const v15, -0x40b33333    # -0.8f

    mul-float v15, v15, v16

    add-float/2addr v14, v15

    .line 1534
    add-float/2addr v6, v14

    .line 1546
    :goto_4
    invoke-virtual {v13, v8}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->setX(F)V

    .line 1547
    invoke-virtual {v13, v6}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->setY(F)V

    .line 1484
    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_1

    :cond_4
    move v6, v7

    .line 1490
    goto/16 :goto_2

    .line 1505
    :cond_5
    const/4 v8, 0x1

    goto :goto_3

    .line 1536
    :cond_6
    invoke-static {}, Lorg/chromium/ui/base/LocalizationUtils;->isLayoutRtl()Z

    move-result v17

    if-eqz v17, :cond_7

    .line 1537
    const v17, -0x40cccccd    # -0.7f

    mul-float v15, v15, v17

    sub-float/2addr v8, v15

    .line 1538
    sub-float v6, v8, v6

    .line 1543
    :goto_5
    const/high16 v8, -0x41000000    # -0.5f

    mul-float v8, v8, v16

    add-float/2addr v8, v14

    move/from16 v18, v8

    move v8, v6

    move/from16 v6, v18

    goto :goto_4

    .line 1540
    :cond_7
    const v17, -0x40cccccd    # -0.7f

    mul-float v15, v15, v17

    add-float/2addr v8, v15

    .line 1541
    add-float/2addr v6, v8

    goto :goto_5

    .line 1551
    :cond_8
    const/4 v5, 0x0

    .line 1552
    if-eqz v2, :cond_9

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mLayout:Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;

    invoke-virtual {v3}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->getHeightMinusTopControls()F

    move-result v3

    .line 1554
    :goto_6
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    array-length v4, v4

    add-int/lit8 v4, v4, -0x1

    move v6, v4

    move v4, v5

    :goto_7
    if-ltz v6, :cond_e

    .line 1555
    sget-boolean v5, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->$assertionsDisabled:Z

    if-nez v5, :cond_a

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    aget-object v5, v5, v6

    if-nez v5, :cond_a

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    .line 1552
    :cond_9
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mLayout:Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;

    invoke-virtual {v3}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->getWidth()F

    move-result v3

    goto :goto_6

    .line 1556
    :cond_a
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    aget-object v5, v5, v6

    .line 1557
    invoke-virtual {v5}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getLayoutTab()Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    move-result-object v7

    .line 1558
    invoke-virtual {v5}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->isDying()Z

    move-result v5

    if-nez v5, :cond_b

    .line 1561
    if-eqz v2, :cond_c

    .line 1562
    invoke-virtual {v7}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getY()F

    move-result v5

    .line 1563
    invoke-static {v5, v3}, Ljava/lang/Math;->min(FF)F

    move-result v8

    invoke-virtual {v7, v8}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->setY(F)V

    .line 1574
    :goto_8
    cmpl-float v5, v5, v3

    if-ltz v5, :cond_b

    const/4 v5, 0x3

    if-ge v4, v5, :cond_b

    .line 1575
    sget v5, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->sStackedTabVisibleSize:F

    sub-float/2addr v3, v5

    .line 1576
    add-int/lit8 v4, v4, 0x1

    .line 1554
    :cond_b
    add-int/lit8 v5, v6, -0x1

    move v6, v5

    goto :goto_7

    .line 1564
    :cond_c
    invoke-static {}, Lorg/chromium/ui/base/LocalizationUtils;->isLayoutRtl()Z

    move-result v5

    if-eqz v5, :cond_d

    .line 1566
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mLayout:Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;

    invoke-virtual {v5}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->getWidth()F

    move-result v5

    invoke-virtual {v7}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getOriginalContentWidth()F

    move-result v8

    const v9, 0x3f666666    # 0.9f

    mul-float/2addr v8, v9

    mul-float/2addr v8, v12

    sub-float v8, v5, v8

    .line 1568
    invoke-virtual {v7}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getX()F

    move-result v5

    neg-float v5, v5

    add-float/2addr v5, v8

    .line 1569
    invoke-static {v5, v3}, Ljava/lang/Math;->min(FF)F

    move-result v9

    neg-float v9, v9

    add-float/2addr v8, v9

    invoke-virtual {v7, v8}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->setX(F)V

    goto :goto_8

    .line 1571
    :cond_d
    invoke-virtual {v7}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getX()F

    move-result v5

    .line 1572
    invoke-static {v5, v3}, Ljava/lang/Math;->min(FF)F

    move-result v8

    invoke-virtual {v7, v8}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->setX(F)V

    goto :goto_8

    .line 1581
    :cond_e
    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->getDiscardRange()F

    move-result v7

    .line 1582
    const/4 v3, 0x0

    move v5, v3

    :goto_9
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    array-length v3, v3

    if-ge v5, v3, :cond_13

    .line 1583
    sget-boolean v3, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->$assertionsDisabled:Z

    if-nez v3, :cond_f

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    aget-object v3, v3, v5

    if-nez v3, :cond_f

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    .line 1584
    :cond_f
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    aget-object v8, v3, v5

    .line 1585
    invoke-virtual {v8}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getLayoutTab()Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    move-result-object v9

    .line 1587
    invoke-virtual {v9}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getX()F

    move-result v3

    invoke-virtual {v8}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getXInStackOffset()F

    move-result v4

    add-float/2addr v3, v4

    .line 1588
    invoke-virtual {v9}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getY()F

    move-result v4

    invoke-virtual {v8}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getYInStackOffset()F

    move-result v6

    add-float/2addr v6, v4

    .line 1589
    invoke-virtual {v8}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getXOutOfStack()F

    move-result v4

    .line 1590
    invoke-virtual {v8}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getYOutOfStack()F

    move-result v10

    .line 1591
    invoke-virtual {v8}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getXInStackInfluence()F

    move-result v11

    invoke-static {v4, v3, v11}, Lcom/google/android/apps/chrome/utilities/ChromeMathUtils;->interpolate(FFF)F

    move-result v4

    .line 1592
    invoke-virtual {v8}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getYInStackInfluence()F

    move-result v3

    invoke-static {v10, v6, v3}, Lcom/google/android/apps/chrome/utilities/ChromeMathUtils;->interpolate(FFF)F

    move-result v3

    .line 1595
    invoke-virtual {v8}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getDiscardAmount()F

    move-result v6

    const/4 v10, 0x0

    cmpl-float v6, v6, v10

    if-eqz v6, :cond_11

    .line 1596
    invoke-virtual {v8}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getDiscardAmount()F

    move-result v6

    .line 1597
    invoke-virtual {v8}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getDiscardFromClick()Z

    move-result v10

    .line 1598
    invoke-static {v6, v7, v10}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->computeDiscardScale(FFZ)F

    move-result v11

    .line 1599
    invoke-virtual {v8}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getDiscardOriginX()F

    move-result v12

    invoke-virtual {v8}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getLayoutTab()Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    move-result-object v13

    invoke-virtual {v13}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getOriginalContentWidth()F

    move-result v13

    const/high16 v14, 0x40000000    # 2.0f

    div-float/2addr v13, v14

    sub-float/2addr v12, v13

    .line 1601
    invoke-virtual {v8}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getDiscardOriginY()F

    move-result v13

    invoke-virtual {v8}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getLayoutTab()Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    move-result-object v8

    invoke-virtual {v8}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getOriginalContentHeight()F

    move-result v8

    const/high16 v14, 0x40000000    # 2.0f

    div-float/2addr v8, v14

    sub-float v8, v13, v8

    .line 1603
    if-eqz v10, :cond_10

    const/4 v6, 0x0

    .line 1604
    :cond_10
    if-eqz v2, :cond_12

    .line 1605
    const/high16 v10, 0x3f800000    # 1.0f

    sub-float/2addr v10, v11

    mul-float/2addr v10, v12

    add-float/2addr v6, v10

    add-float/2addr v4, v6

    .line 1606
    const/high16 v6, 0x3f800000    # 1.0f

    sub-float/2addr v6, v11

    mul-float/2addr v6, v8

    add-float/2addr v3, v6

    .line 1614
    :cond_11
    :goto_a
    move-object/from16 v0, p1

    iget v6, v0, Landroid/graphics/RectF;->left:F

    add-float/2addr v4, v6

    invoke-virtual {v9, v4}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->setX(F)V

    .line 1615
    move-object/from16 v0, p1

    iget v4, v0, Landroid/graphics/RectF;->top:F

    add-float/2addr v3, v4

    invoke-virtual {v9, v3}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->setY(F)V

    .line 1582
    add-int/lit8 v3, v5, 0x1

    move v5, v3

    goto/16 :goto_9

    .line 1608
    :cond_12
    const/high16 v10, 0x3f800000    # 1.0f

    sub-float/2addr v10, v11

    mul-float/2addr v10, v12

    add-float/2addr v4, v10

    .line 1609
    const/high16 v10, 0x3f800000    # 1.0f

    sub-float/2addr v10, v11

    mul-float/2addr v8, v10

    add-float/2addr v6, v8

    add-float/2addr v3, v6

    goto :goto_a

    .line 1617
    :cond_13
    return-void
.end method

.method private computeTabScaleAlphaDepthHelper(Landroid/graphics/RectF;)V
    .locals 8

    .prologue
    .line 1426
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->getStackScale(Landroid/graphics/RectF;)F

    move-result v1

    .line 1427
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->getDiscardRange()F

    move-result v2

    .line 1429
    const/4 v0, 0x0

    :goto_0
    iget-object v3, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    array-length v3, v3

    if-ge v0, v3, :cond_1

    .line 1430
    sget-boolean v3, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->$assertionsDisabled:Z

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    aget-object v3, v3, v0

    if-nez v3, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 1431
    :cond_0
    iget-object v3, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    aget-object v3, v3, v0

    .line 1432
    invoke-virtual {v3}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getLayoutTab()Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    move-result-object v4

    .line 1433
    invoke-virtual {v3}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getDiscardAmount()F

    move-result v5

    .line 1436
    invoke-virtual {v3}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getDiscardFromClick()Z

    move-result v6

    invoke-static {v5, v2, v6}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->computeDiscardScale(FFZ)F

    move-result v6

    .line 1438
    invoke-virtual {v3}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getScale()F

    move-result v7

    mul-float/2addr v7, v6

    mul-float/2addr v7, v1

    invoke-virtual {v4, v7}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->setScale(F)V

    .line 1439
    invoke-virtual {v4, v6}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->setBorderScale(F)V

    .line 1442
    invoke-static {v5, v2}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->computeDiscardAlpha(FF)F

    move-result v5

    .line 1443
    invoke-virtual {v3}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getAlpha()F

    move-result v3

    mul-float/2addr v3, v5

    invoke-virtual {v4, v3}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->setAlpha(F)V

    .line 1429
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1445
    :cond_1
    return-void
.end method

.method private computeTabScrollOffsetHelper()V
    .locals 5

    .prologue
    .line 1452
    const v1, 0x7f7fffff    # Float.MAX_VALUE

    .line 1453
    const/4 v0, 0x0

    :goto_0
    iget-object v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 1454
    iget-object v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->isDying()Z

    move-result v2

    if-nez v2, :cond_0

    .line 1456
    iget-object v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getScrollOffset()F

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->min(FF)F

    move-result v1

    .line 1457
    iget-object v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    aget-object v2, v2, v0

    invoke-virtual {v2, v1}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->setScrollOffset(F)V

    .line 1459
    iget v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mScrollOffset:F

    add-float/2addr v1, v2

    invoke-direct {p0, v1}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->scrollToScreen(F)F

    move-result v1

    .line 1460
    iget v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mScrollOffset:F

    neg-float v2, v2

    iget-object v3, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    aget-object v3, v3, v0

    iget v4, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mCurrentMode:I

    invoke-virtual {v3, v4}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getSizeInScrollDirection(I)F

    move-result v3

    add-float/2addr v1, v3

    invoke-direct {p0, v1}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->screenToScroll(F)F

    move-result v1

    add-float/2addr v1, v2

    .line 1453
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1463
    :cond_1
    return-void
.end method

.method private computeTabTiltHelper(JLandroid/graphics/RectF;)V
    .locals 11

    .prologue
    .line 1795
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mCurrentMode:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    const/4 v0, 0x1

    .line 1796
    :goto_0
    invoke-virtual {p3}, Landroid/graphics/RectF;->width()F

    move-result v2

    .line 1797
    invoke-virtual {p3}, Landroid/graphics/RectF;->height()F

    move-result v3

    .line 1798
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->computeOverscrollPercent()F

    move-result v4

    .line 1801
    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mOverviewAnimationType:Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;

    sget-object v5, Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;->START_PINCH:Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;

    if-eq v1, v5, :cond_3

    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mOverviewAnimationType:Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;

    sget-object v5, Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;->DISCARD:Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;

    if-eq v1, v5, :cond_3

    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mOverviewAnimationType:Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;

    sget-object v5, Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;->FULL_ROLL:Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;

    if-eq v1, v5, :cond_3

    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mOverviewAnimationType:Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;

    sget-object v5, Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;->TAB_FOCUSED:Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;

    if-eq v1, v5, :cond_3

    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mOverviewAnimationType:Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;

    sget-object v5, Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;->UNDISCARD:Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;

    if-eq v1, v5, :cond_3

    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mOverviewAnimationType:Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;

    sget-object v5, Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;->DISCARD_ALL:Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;

    if-eq v1, v5, :cond_3

    .line 1808
    iget v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mPinch0TabIndex:I

    if-gez v1, :cond_0

    const/4 v1, 0x0

    cmpl-float v1, v4, v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mOverviewAnimationType:Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;

    sget-object v5, Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;->REACH_TOP:Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;

    if-ne v1, v5, :cond_2

    .line 1811
    :cond_0
    const/4 v0, 0x0

    :goto_1
    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    array-length v1, v1

    if-ge v0, v1, :cond_3

    .line 1812
    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    aget-object v1, v1, v0

    .line 1813
    invoke-virtual {v1}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getLayoutTab()Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    move-result-object v1

    .line 1814
    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->setTiltX(FF)V

    .line 1815
    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->setTiltY(FF)V

    .line 1811
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1795
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 1817
    :cond_2
    const/4 v1, 0x0

    cmpg-float v1, v4, v1

    if-gez v1, :cond_9

    .line 1818
    iget v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mOverScrollCounter:I

    const/4 v2, 0x5

    if-lt v1, v2, :cond_4

    .line 1819
    sget-object v0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;->FULL_ROLL:Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->startAnimation(JLcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;)V

    .line 1820
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mOverScrollCounter:I

    .line 1823
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mScrollOffset:F

    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->getMinScroll(Z)F

    move-result v1

    const/4 v2, 0x0

    invoke-direct {p0, v2}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->getMaxScroll(Z)F

    move-result v2

    invoke-static {v0, v1, v2}, Lorg/chromium/chrome/browser/util/MathUtils;->clamp(FFF)F

    move-result v0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->setScrollTarget(FZ)V

    .line 1891
    :cond_3
    return-void

    .line 1832
    :cond_4
    const/4 v1, 0x0

    .line 1833
    const/high16 v2, -0x41800000    # -0.25f

    cmpg-float v2, v4, v2

    if-gez v2, :cond_5

    .line 1835
    const/high16 v1, 0x3e800000    # 0.25f

    add-float/2addr v1, v4

    const/high16 v2, 0x3f400000    # 0.75f

    div-float/2addr v1, v2

    .line 1838
    iget-object v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mUnderScrollAngleInterpolator:Landroid/view/animation/Interpolator;

    neg-float v1, v1

    invoke-interface {v2, v1}, Landroid/view/animation/Interpolator;->getInterpolation(F)F

    move-result v1

    iget v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mMaxOverScrollAngle:F

    neg-float v2, v2

    mul-float/2addr v1, v2

    const/high16 v2, 0x3f000000    # 0.5f

    mul-float/2addr v1, v2

    .line 1842
    :cond_5
    iget-object v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    iget-object v3, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    array-length v3, v3

    add-int/lit8 v3, v3, -0x1

    aget-object v2, v2, v3

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getLayoutTab()Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    move-result-object v2

    .line 1844
    if-eqz v0, :cond_6

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getScaledContentHeight()F

    move-result v3

    const/high16 v4, 0x40000000    # 2.0f

    div-float/2addr v3, v4

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getY()F

    move-result v2

    add-float/2addr v2, v3

    .line 1848
    :goto_2
    const/4 v3, 0x0

    :goto_3
    iget-object v4, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    array-length v4, v4

    if-ge v3, v4, :cond_3

    .line 1849
    iget-object v4, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    aget-object v4, v4, v3

    .line 1850
    invoke-virtual {v4}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getLayoutTab()Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    move-result-object v5

    .line 1851
    if-eqz v0, :cond_7

    .line 1852
    invoke-virtual {v5}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getY()F

    move-result v4

    sub-float v4, v2, v4

    invoke-virtual {v5, v1, v4}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->setTiltX(FF)V

    .line 1848
    :goto_4
    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    .line 1844
    :cond_6
    invoke-virtual {v2}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getScaledContentWidth()F

    move-result v3

    const/high16 v4, 0x40000000    # 2.0f

    div-float/2addr v3, v4

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getX()F

    move-result v2

    add-float/2addr v2, v3

    goto :goto_2

    .line 1854
    :cond_7
    invoke-static {}, Lorg/chromium/ui/base/LocalizationUtils;->isLayoutRtl()Z

    move-result v4

    if-eqz v4, :cond_8

    neg-float v4, v1

    :goto_5
    invoke-virtual {v5}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getX()F

    move-result v6

    sub-float v6, v2, v6

    invoke-virtual {v5, v4, v6}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->setTiltY(FF)V

    goto :goto_4

    :cond_8
    move v4, v1

    goto :goto_5

    .line 1865
    :cond_9
    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mOverScrollAngleInterpolator:Landroid/view/animation/Interpolator;

    invoke-interface {v1, v4}, Landroid/view/animation/Interpolator;->getInterpolation(F)F

    move-result v1

    iget v5, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mMaxOverScrollAngle:F

    mul-float/2addr v5, v1

    .line 1867
    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mOverscrollSlideInterpolator:Landroid/view/animation/Interpolator;

    invoke-interface {v1, v4}, Landroid/view/animation/Interpolator;->getInterpolation(F)F

    move-result v1

    iget v4, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mMaxOverScrollSlide:F

    mul-float/2addr v4, v1

    .line 1870
    const/4 v1, 0x0

    :goto_6
    iget-object v6, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    array-length v6, v6

    if-ge v1, v6, :cond_3

    .line 1871
    iget-object v6, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    aget-object v6, v6, v1

    .line 1872
    invoke-virtual {v6}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getLayoutTab()Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    move-result-object v6

    .line 1873
    if-eqz v0, :cond_a

    .line 1875
    invoke-virtual {v6}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getY()F

    move-result v7

    div-float/2addr v7, v3

    const/high16 v8, 0x3f000000    # 0.5f

    add-float/2addr v7, v8

    const/4 v8, 0x0

    const/high16 v9, 0x3f800000    # 1.0f

    invoke-static {v7, v8, v9}, Lorg/chromium/chrome/browser/util/MathUtils;->clamp(FFF)F

    move-result v7

    .line 1876
    mul-float/2addr v7, v5

    invoke-virtual {v6}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getScaledContentHeight()F

    move-result v8

    const/high16 v9, 0x40400000    # 3.0f

    div-float/2addr v8, v9

    invoke-virtual {v6, v7, v8}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->setTiltX(FF)V

    .line 1877
    invoke-virtual {v6}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getY()F

    move-result v7

    add-float/2addr v7, v4

    invoke-virtual {v6, v7}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->setY(F)V

    .line 1870
    :goto_7
    add-int/lit8 v1, v1, 0x1

    goto :goto_6

    .line 1878
    :cond_a
    invoke-static {}, Lorg/chromium/ui/base/LocalizationUtils;->isLayoutRtl()Z

    move-result v7

    if-eqz v7, :cond_b

    .line 1880
    invoke-virtual {v6}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getX()F

    move-result v7

    div-float/2addr v7, v2

    neg-float v7, v7

    const/high16 v8, 0x3f000000    # 0.5f

    add-float/2addr v7, v8

    const/4 v8, 0x0

    const/high16 v9, 0x3f800000    # 1.0f

    invoke-static {v7, v8, v9}, Lorg/chromium/chrome/browser/util/MathUtils;->clamp(FFF)F

    move-result v7

    .line 1881
    neg-float v8, v5

    mul-float/2addr v7, v8

    invoke-virtual {v6}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getScaledContentWidth()F

    move-result v8

    const/high16 v9, 0x40000000    # 2.0f

    mul-float/2addr v8, v9

    const/high16 v9, 0x40400000    # 3.0f

    div-float/2addr v8, v9

    invoke-virtual {v6, v7, v8}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->setTiltY(FF)V

    .line 1882
    invoke-virtual {v6}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getX()F

    move-result v7

    sub-float/2addr v7, v4

    invoke-virtual {v6, v7}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->setX(F)V

    goto :goto_7

    .line 1885
    :cond_b
    invoke-virtual {v6}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getX()F

    move-result v7

    div-float/2addr v7, v2

    const/high16 v8, 0x3f000000    # 0.5f

    add-float/2addr v7, v8

    const/4 v8, 0x0

    const/high16 v9, 0x3f800000    # 1.0f

    invoke-static {v7, v8, v9}, Lorg/chromium/chrome/browser/util/MathUtils;->clamp(FFF)F

    move-result v7

    .line 1886
    mul-float/2addr v7, v5

    invoke-virtual {v6}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getScaledContentWidth()F

    move-result v8

    const/high16 v9, 0x40400000    # 3.0f

    div-float/2addr v8, v9

    invoke-virtual {v6, v7, v8}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->setTiltY(FF)V

    .line 1887
    invoke-virtual {v6}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getX()F

    move-result v7

    add-float/2addr v7, v4

    invoke-virtual {v6, v7}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->setX(F)V

    goto :goto_7
.end method

.method private computeTabVisibilitySortingHelper(Landroid/graphics/RectF;)V
    .locals 9

    .prologue
    const/4 v1, 0x0

    const/high16 v3, 0x40000000    # 2.0f

    const/4 v8, 0x0

    .line 1747
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mReferenceOrderIndex:I

    .line 1748
    const/4 v2, -0x1

    if-ne v0, v2, :cond_2

    .line 1749
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mLayout:Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->getWidth()F

    move-result v0

    div-float/2addr v0, v3

    iget-object v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mLayout:Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->getHeight()F

    move-result v2

    div-float/2addr v2, v3

    invoke-direct {p0, v0, v2}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->getTabIndexAtPositon(FF)I

    move-result v0

    .line 1752
    iget v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mCurrentScrollDirection:F

    cmpl-float v2, v2, v8

    if-lez v2, :cond_0

    add-int/lit8 v0, v0, 0x1

    .line 1753
    :cond_0
    iget v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mCurrentScrollDirection:F

    cmpg-float v2, v2, v8

    if-gez v2, :cond_1

    add-int/lit8 v0, v0, -0x1

    .line 1754
    :cond_1
    iget-object v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    invoke-static {v0, v1, v2}, Lorg/chromium/chrome/browser/util/MathUtils;->clamp(III)I

    move-result v0

    .line 1757
    :cond_2
    iget-object v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mLayout:Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->getWidth()F

    move-result v2

    .line 1758
    iget-object v3, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mLayout:Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;

    invoke-virtual {v3}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->getHeight()F

    move-result v3

    .line 1759
    iget v4, p1, Landroid/graphics/RectF;->left:F

    invoke-static {v4, v8, v2}, Lorg/chromium/chrome/browser/util/MathUtils;->clamp(FFF)F

    move-result v4

    .line 1760
    iget v5, p1, Landroid/graphics/RectF;->right:F

    invoke-static {v5, v8, v2}, Lorg/chromium/chrome/browser/util/MathUtils;->clamp(FFF)F

    move-result v5

    .line 1761
    iget v6, p1, Landroid/graphics/RectF;->top:F

    invoke-static {v6, v8, v3}, Lorg/chromium/chrome/browser/util/MathUtils;->clamp(FFF)F

    move-result v6

    .line 1762
    iget v7, p1, Landroid/graphics/RectF;->bottom:F

    invoke-static {v7, v8, v3}, Lorg/chromium/chrome/browser/util/MathUtils;->clamp(FFF)F

    move-result v7

    .line 1763
    sub-float v4, v5, v4

    sub-float v5, v7, v6

    mul-float/2addr v4, v5

    .line 1764
    mul-float/2addr v2, v3

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-static {v2, v3}, Ljava/lang/Math;->max(FF)F

    move-result v2

    .line 1765
    div-float v2, v4, v2

    .line 1767
    :goto_0
    iget-object v3, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    array-length v3, v3

    if-ge v1, v3, :cond_3

    .line 1768
    iget-object v3, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    aget-object v3, v3, v1

    invoke-virtual {v3, v2}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->updateStackVisiblityValue(F)V

    .line 1769
    iget-object v3, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    aget-object v3, v3, v1

    invoke-virtual {v3, v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->updateVisiblityValue(I)V

    .line 1767
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1771
    :cond_3
    return-void
.end method

.method private createStackTabs(Z)V
    .locals 12

    .prologue
    const/high16 v8, -0x40800000    # -1.0f

    const/4 v7, 0x0

    const/4 v3, 0x1

    .line 1971
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mTabModel:Lorg/chromium/chrome/browser/tabmodel/TabModel;

    invoke-interface {v0}, Lorg/chromium/chrome/browser/tabmodel/TabModel;->getCount()I

    move-result v10

    .line 1972
    if-nez v10, :cond_1

    .line 1973
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->cleanupTabs()V

    .line 2011
    :cond_0
    return-void

    .line 1975
    :cond_1
    iget-object v11, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    .line 1976
    new-array v0, v10, [Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    .line 1978
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mTabModel:Lorg/chromium/chrome/browser/tabmodel/TabModel;

    invoke-interface {v0}, Lorg/chromium/chrome/browser/tabmodel/TabModel;->isIncognito()Z

    move-result v2

    .line 1979
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mLayout:Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->isHiding()Z

    move-result v0

    if-nez v0, :cond_2

    move v4, v3

    :goto_0
    move v9, v7

    .line 1980
    :goto_1
    if-ge v9, v10, :cond_0

    .line 1981
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mTabModel:Lorg/chromium/chrome/browser/tabmodel/TabModel;

    invoke-interface {v0, v9}, Lorg/chromium/chrome/browser/tabmodel/TabModel;->getTabAt(I)Lorg/chromium/chrome/browser/Tab;

    move-result-object v0

    .line 1982
    if-eqz v0, :cond_3

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/Tab;->getId()I

    move-result v1

    .line 1983
    :goto_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    invoke-direct {p0, v11, v1}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->findTabById([Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;I)Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    move-result-object v5

    aput-object v5, v0, v9

    .line 1988
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    aget-object v0, v0, v9

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    aget-object v0, v0, v9

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getLayoutTab()Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    move-result-object v0

    if-eqz v0, :cond_6

    if-eqz p1, :cond_6

    .line 1989
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    aget-object v0, v0, v9

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getLayoutTab()Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getMaxContentWidth()F

    move-result v5

    .line 1990
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    aget-object v0, v0, v9

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getLayoutTab()Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getMaxContentHeight()F

    move-result v6

    .line 1993
    :goto_3
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mLayout:Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->createLayoutTab(IZZZFF)Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    move-result-object v1

    .line 1995
    invoke-virtual {v1, v3}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->setInsetBorderVertical(Z)V

    .line 1996
    invoke-virtual {v1, v3}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->setShowToolbar(Z)V

    .line 1997
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->setToolbarAlpha(F)V

    .line 1998
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mTabModel:Lorg/chromium/chrome/browser/tabmodel/TabModel;

    invoke-interface {v0}, Lorg/chromium/chrome/browser/tabmodel/TabModel;->index()I

    move-result v0

    if-eq v0, v9, :cond_4

    move v0, v3

    :goto_4
    invoke-virtual {v1, v0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->setAnonymizeToolbar(Z)V

    .line 2000
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    aget-object v0, v0, v9

    if-nez v0, :cond_5

    .line 2001
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    new-instance v5, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    invoke-direct {v5, v1}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;-><init>(Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;)V

    aput-object v5, v0, v9

    .line 2006
    :goto_5
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    aget-object v0, v0, v9

    invoke-virtual {v0, v9}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->setNewIndex(I)V

    .line 1980
    add-int/lit8 v0, v9, 0x1

    move v9, v0

    goto :goto_1

    :cond_2
    move v4, v7

    .line 1979
    goto :goto_0

    .line 1982
    :cond_3
    const/4 v1, -0x1

    goto :goto_2

    :cond_4
    move v0, v7

    .line 1998
    goto :goto_4

    .line 2003
    :cond_5
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    aget-object v0, v0, v9

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->setLayoutTab(Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;)V

    goto :goto_5

    :cond_6
    move v6, v8

    move v5, v8

    goto :goto_3
.end method

.method private createTabHelper(I)Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 2030
    iget-object v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mTabModel:Lorg/chromium/chrome/browser/tabmodel/TabModel;

    invoke-static {v2, p1}, Lorg/chromium/chrome/browser/tabmodel/TabModelUtils;->getTabById(Lorg/chromium/chrome/browser/tabmodel/TabList;I)Lorg/chromium/chrome/browser/Tab;

    move-result-object v2

    if-nez v2, :cond_1

    .line 2046
    :cond_0
    :goto_0
    return v0

    .line 2035
    :cond_1
    iget-object v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    if-eqz v2, :cond_2

    .line 2036
    iget-object v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    array-length v3, v2

    move v2, v0

    .line 2037
    :goto_1
    if-ge v2, v3, :cond_2

    .line 2038
    iget-object v4, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    aget-object v4, v4, v2

    invoke-virtual {v4}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getId()I

    move-result v4

    if-eq v4, p1, :cond_0

    .line 2037
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 2044
    :cond_2
    invoke-direct {p0, v1}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->createStackTabs(Z)V

    move v0, v1

    .line 2046
    goto :goto_0
.end method

.method private discard(FFFF)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/high16 v4, 0x3f800000    # 1.0f

    .line 818
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mOverviewAnimationType:Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;

    sget-object v1, Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;->NONE:Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mOverviewAnimationType:Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;

    sget-object v1, Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;->DISCARD:Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mOverviewAnimationType:Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;

    sget-object v1, Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;->DISCARD_ALL:Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mOverviewAnimationType:Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;

    sget-object v1, Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;->UNDISCARD:Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;

    if-eq v0, v1, :cond_1

    .line 868
    :cond_0
    :goto_0
    return-void

    .line 826
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mDiscardingTab:Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    if-nez v0, :cond_2

    .line 827
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mInSwipe:Z

    if-nez v0, :cond_3

    .line 828
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->getTabAtPositon(FF)Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mDiscardingTab:Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    .line 834
    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mDiscardingTab:Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    if-eqz v0, :cond_2

    .line 835
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->cancelDiscardScrollingAnimation()Z

    .line 838
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mDiscardingTab:Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getLayoutTab()Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getClickTargetBounds()Landroid/graphics/RectF;

    move-result-object v1

    .line 842
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mCurrentMode:I

    if-ne v0, v6, :cond_4

    .line 843
    iput v4, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mDiscardDirection:F

    .line 844
    iget v0, v1, Landroid/graphics/RectF;->left:F

    sub-float/2addr v0, p1

    iget v2, v1, Landroid/graphics/RectF;->right:F

    sub-float v2, p1, v2

    invoke-static {v0, v2}, Ljava/lang/Math;->max(FF)F

    move-result v0

    .line 845
    invoke-virtual {v1}, Landroid/graphics/RectF;->width()F

    move-result v1

    .line 853
    :goto_2
    iget-object v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mDiscardingTab:Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getLayoutTab()Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getX()F

    move-result v2

    sub-float v2, p1, v2

    .line 854
    iget-object v3, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mDiscardingTab:Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    invoke-virtual {v3}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getLayoutTab()Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getY()F

    move-result v3

    sub-float v3, p2, v3

    .line 855
    iget-object v4, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mDiscardingTab:Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    iget-object v5, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mDiscardingTab:Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    invoke-virtual {v5}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getScale()F

    move-result v5

    div-float/2addr v2, v5

    invoke-virtual {v4, v2}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->setDiscardOriginX(F)V

    .line 856
    iget-object v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mDiscardingTab:Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    iget-object v4, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mDiscardingTab:Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    invoke-virtual {v4}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getScale()F

    move-result v4

    div-float/2addr v3, v4

    invoke-virtual {v2, v3}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->setDiscardOriginY(F)V

    .line 857
    iget-object v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mDiscardingTab:Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->setDiscardFromClick(Z)V

    .line 859
    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    const v2, 0x3dcccccd    # 0.1f

    mul-float/2addr v1, v2

    cmpg-float v0, v0, v1

    if-gez v0, :cond_2

    .line 860
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mDiscardingTab:Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    .line 864
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mDiscardingTab:Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    if-eqz v0, :cond_0

    .line 865
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mCurrentMode:I

    if-ne v0, v6, :cond_5

    .line 866
    :goto_3
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mDiscardingTab:Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    invoke-virtual {v0, p3}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->addToDiscardAmount(F)V

    goto/16 :goto_0

    .line 830
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mTabModel:Lorg/chromium/chrome/browser/tabmodel/TabModel;

    invoke-interface {v0}, Lorg/chromium/chrome/browser/tabmodel/TabModel;->index()I

    move-result v0

    if-ltz v0, :cond_0

    .line 831
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mTabModel:Lorg/chromium/chrome/browser/tabmodel/TabModel;

    invoke-interface {v1}, Lorg/chromium/chrome/browser/tabmodel/TabModel;->index()I

    move-result v1

    aget-object v0, v0, v1

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mDiscardingTab:Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    goto/16 :goto_1

    .line 847
    :cond_4
    const/high16 v0, 0x40000000    # 2.0f

    const/high16 v2, 0x40800000    # 4.0f

    iget-object v3, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mLayout:Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;

    invoke-virtual {v3}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->getWidth()F

    move-result v3

    div-float v3, p1, v3

    mul-float/2addr v2, v3

    sub-float/2addr v0, v2

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mDiscardDirection:F

    .line 848
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mDiscardDirection:F

    const/high16 v2, -0x40800000    # -1.0f

    invoke-static {v0, v2, v4}, Lorg/chromium/chrome/browser/util/MathUtils;->clamp(FFF)F

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mDiscardDirection:F

    .line 849
    iget v0, v1, Landroid/graphics/RectF;->top:F

    sub-float/2addr v0, p2

    iget v2, v1, Landroid/graphics/RectF;->bottom:F

    sub-float v2, p2, v2

    invoke-static {v0, v2}, Ljava/lang/Math;->max(FF)F

    move-result v0

    .line 850
    invoke-virtual {v1}, Landroid/graphics/RectF;->height()F

    move-result v1

    goto/16 :goto_2

    :cond_5
    move p3, p4

    .line 865
    goto :goto_3
.end method

.method private evenOutTabs(FZ)Z
    .locals 13

    .prologue
    const/4 v3, 0x1

    const/4 v12, 0x0

    const/high16 v5, 0x3f800000    # 1.0f

    const/4 v0, 0x0

    .line 946
    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mOverviewAnimationType:Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;

    sget-object v2, Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;->NONE:Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;

    if-ne v1, v2, :cond_0

    iget v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mEvenOutProgress:F

    cmpl-float v1, v1, v5

    if-gez v1, :cond_0

    cmpl-float v1, p1, v12

    if-nez v1, :cond_2

    :cond_0
    move v2, v0

    .line 997
    :cond_1
    :goto_0
    return v2

    .line 959
    :cond_2
    invoke-static {p1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    iget v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mEvenOutRate:F

    mul-float/2addr v1, v2

    iget v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mEvenOutProgress:F

    sub-float v2, v5, v2

    invoke-static {v1, v2}, Ljava/lang/Math;->min(FF)F

    move-result v4

    .line 961
    iget v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mEvenOutProgress:F

    sub-float v1, v5, v1

    div-float v5, v4, v1

    .line 963
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->getScrollDimensionSize()F

    move-result v6

    move v1, v0

    move v2, v0

    .line 964
    :goto_1
    iget-object v7, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    array-length v7, v7

    if-ge v0, v7, :cond_7

    .line 965
    iget-object v7, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    aget-object v7, v7, v0

    invoke-virtual {v7}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getScrollOffset()F

    move-result v7

    .line 966
    iget v8, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mSpacing:I

    mul-int/2addr v8, v0

    int-to-float v8, v8

    invoke-direct {p0, v8}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->screenToScroll(F)F

    move-result v8

    .line 967
    iget v9, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mScrollTarget:F

    add-float/2addr v9, v7

    invoke-direct {p0, v9}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->scrollToScreen(F)F

    move-result v9

    invoke-static {v6, v9}, Ljava/lang/Math;->min(FF)F

    move-result v9

    .line 968
    iget v10, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mScrollTarget:F

    add-float/2addr v10, v8

    invoke-direct {p0, v10}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->scrollToScreen(F)F

    move-result v10

    invoke-static {v6, v10}, Ljava/lang/Math;->min(FF)F

    move-result v10

    .line 971
    cmpl-float v11, v9, v10

    if-nez v11, :cond_3

    .line 972
    iget-object v7, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    aget-object v7, v7, v0

    invoke-virtual {v7, v8}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->setScrollOffset(F)V

    .line 964
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 975
    :cond_3
    sub-float/2addr v8, v7

    mul-float/2addr v8, v5

    add-float/2addr v7, v8

    .line 976
    iget v8, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mScrollTarget:F

    add-float/2addr v8, v7

    invoke-direct {p0, v8}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->scrollToScreen(F)F

    move-result v8

    invoke-static {v6, v8}, Ljava/lang/Math;->min(FF)F

    move-result v8

    .line 978
    cmpl-float v8, v9, v8

    if-nez v8, :cond_4

    .line 979
    iget-object v8, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    aget-object v8, v8, v0

    invoke-virtual {v8, v7}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->setScrollOffset(F)V

    goto :goto_2

    .line 983
    :cond_4
    sub-float v8, v10, v9

    mul-float/2addr v8, p1

    cmpl-float v8, v8, v12

    if-gtz v8, :cond_5

    if-eqz p2, :cond_6

    .line 984
    :cond_5
    iget-object v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    aget-object v2, v2, v0

    invoke-virtual {v2, v7}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->setScrollOffset(F)V

    move v2, v3

    .line 985
    goto :goto_2

    :cond_6
    move v1, v3

    .line 987
    goto :goto_2

    .line 994
    :cond_7
    if-nez v1, :cond_1

    .line 995
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mEvenOutProgress:F

    add-float/2addr v0, v4

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mEvenOutProgress:F

    goto/16 :goto_0
.end method

.method private findTabById([Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;I)Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 2014
    if-nez p1, :cond_1

    .line 2019
    :cond_0
    :goto_0
    return-object v0

    .line 2015
    :cond_1
    array-length v2, p1

    .line 2016
    const/4 v1, 0x0

    :goto_1
    if-ge v1, v2, :cond_0

    .line 2017
    aget-object v3, p1, v1

    invoke-virtual {v3}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getId()I

    move-result v3

    if-ne v3, p2, :cond_2

    aget-object v0, p1, v1

    goto :goto_0

    .line 2016
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method private finishAnimation(J)V
    .locals 11

    .prologue
    const/4 v9, 0x0

    const/4 v0, 0x1

    const/4 v7, 0x0

    .line 554
    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mTabAnimations:Lcom/google/android/apps/chrome/ChromeAnimation;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mTabAnimations:Lcom/google/android/apps/chrome/ChromeAnimation;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/ChromeAnimation;->updateAndFinish()V

    .line 555
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mViewAnimations:Landroid/animation/AnimatorSet;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mViewAnimations:Landroid/animation/AnimatorSet;

    invoke-virtual {v1}, Landroid/animation/AnimatorSet;->end()V

    .line 556
    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mTabAnimations:Lcom/google/android/apps/chrome/ChromeAnimation;

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mViewAnimations:Landroid/animation/AnimatorSet;

    if-eqz v1, :cond_3

    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mLayout:Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->onStackAnimationFinished()V

    .line 558
    :cond_3
    sget-object v1, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack$2;->$SwitchMap$com$google$android$apps$chrome$tabs$layout$phone$StackAnimation$OverviewAnimationType:[I

    iget-object v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mOverviewAnimationType:Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 622
    :goto_0
    :pswitch_0
    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mOverviewAnimationType:Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;

    sget-object v2, Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;->NONE:Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;

    if-eq v1, v2, :cond_4

    .line 629
    iget v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mScrollOffset:F

    invoke-direct {p0, v1, v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->setScrollTarget(FZ)V

    .line 630
    sget-object v0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;->NONE:Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mOverviewAnimationType:Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;

    .line 632
    :cond_4
    iput-object v9, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mTabAnimations:Lcom/google/android/apps/chrome/ChromeAnimation;

    .line 633
    iput-object v9, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mViewAnimations:Landroid/animation/AnimatorSet;

    .line 634
    return-void

    .line 560
    :pswitch_1
    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mLayout:Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    const/16 v2, 0xd

    invoke-static {v1, v2}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->broadcastNotification(Landroid/content/Context;I)V

    goto :goto_0

    .line 564
    :pswitch_2
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->springBack(J)V

    goto :goto_0

    .line 579
    :pswitch_3
    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    if-eqz v1, :cond_a

    .line 580
    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mOverviewAnimationType:Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;

    sget-object v2, Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;->DISCARD_ALL:Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;

    if-eq v1, v2, :cond_6

    move v5, v0

    :goto_1
    move v8, v7

    .line 582
    :goto_2
    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    array-length v1, v1

    if-ge v8, v1, :cond_7

    .line 583
    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    aget-object v2, v1, v8

    .line 584
    invoke-virtual {v2}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->isDying()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 585
    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mLayout:Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getId()I

    move-result v4

    iget-object v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mTabModel:Lorg/chromium/chrome/browser/tabmodel/TabModel;

    invoke-interface {v2}, Lorg/chromium/chrome/browser/tabmodel/TabModel;->isIncognito()Z

    move-result v6

    move-wide v2, p1

    invoke-virtual/range {v1 .. v6}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->uiDoneClosingTab(JIZZ)V

    .line 582
    :cond_5
    add-int/lit8 v1, v8, 0x1

    move v8, v1

    goto :goto_2

    :cond_6
    move v5, v7

    .line 580
    goto :goto_1

    :cond_7
    move v1, v7

    move v2, v7

    .line 592
    :goto_3
    iget-object v3, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    array-length v3, v3

    if-ge v1, v3, :cond_9

    .line 593
    iget-object v3, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    aget-object v3, v3, v1

    invoke-virtual {v3}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->isDying()Z

    move-result v3

    if-eqz v3, :cond_8

    .line 594
    iget-object v3, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mLayout:Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;

    iget-object v4, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    aget-object v4, v4, v1

    invoke-virtual {v4}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getLayoutTab()Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->releaseTabLayout(Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;)V

    .line 592
    :goto_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 596
    :cond_8
    add-int/lit8 v2, v2, 0x1

    goto :goto_4

    .line 600
    :cond_9
    if-nez v2, :cond_b

    .line 602
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->cleanupTabs()V

    .line 621
    :cond_a
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->getDefaultDiscardDirection()F

    move-result v1

    iput v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mDiscardDirection:F

    goto/16 :goto_0

    .line 603
    :cond_b
    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    array-length v1, v1

    if-ge v2, v1, :cond_a

    .line 606
    iget-object v3, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    .line 607
    new-array v1, v2, [Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    iput-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    move v1, v7

    .line 610
    :goto_5
    array-length v4, v3

    if-ge v7, v4, :cond_d

    .line 611
    aget-object v4, v3, v7

    invoke-virtual {v4}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->isDying()Z

    move-result v4

    if-nez v4, :cond_c

    .line 612
    iget-object v4, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    aget-object v5, v3, v7

    aput-object v5, v4, v1

    .line 613
    iget-object v4, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    aget-object v4, v4, v1

    invoke-virtual {v4, v1}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->setNewIndex(I)V

    .line 614
    add-int/lit8 v1, v1, 0x1

    .line 610
    :cond_c
    add-int/lit8 v7, v7, 0x1

    goto :goto_5

    .line 617
    :cond_d
    sget-boolean v3, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->$assertionsDisabled:Z

    if-nez v3, :cond_a

    if-eq v1, v2, :cond_a

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 558
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_3
        :pswitch_3
    .end packed-switch
.end method

.method private finishAnimationsIfDone(JZ)V
    .locals 9

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 722
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mViewAnimations:Landroid/animation/AnimatorSet;

    if-eqz v0, :cond_4

    move v0, v1

    .line 723
    :goto_0
    iget-object v3, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mTabAnimations:Lcom/google/android/apps/chrome/ChromeAnimation;

    if-eqz v3, :cond_5

    move v7, v1

    .line 724
    :goto_1
    if-nez v0, :cond_0

    if-eqz v7, :cond_6

    :cond_0
    move v6, v1

    .line 725
    :goto_2
    if-eqz v0, :cond_7

    iget-object v3, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mViewAnimations:Landroid/animation/AnimatorSet;

    invoke-virtual {v3}, Landroid/animation/AnimatorSet;->isRunning()Z

    move-result v3

    if-eqz v3, :cond_7

    move v5, v2

    .line 726
    :goto_3
    if-eqz v7, :cond_8

    iget-object v3, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mTabAnimations:Lcom/google/android/apps/chrome/ChromeAnimation;

    invoke-virtual {v3}, Lcom/google/android/apps/chrome/ChromeAnimation;->finished()Z

    move-result v3

    move v4, v3

    .line 728
    :goto_4
    if-eqz p3, :cond_9

    if-eqz v6, :cond_9

    move v3, v1

    .line 729
    :goto_5
    if-eqz v6, :cond_a

    if-eqz v0, :cond_1

    if-eqz v5, :cond_a

    :cond_1
    if-eqz v7, :cond_2

    if-eqz v4, :cond_a

    :cond_2
    :goto_6
    or-int v0, v3, v1

    .line 732
    if-eqz v0, :cond_3

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->finishAnimation(J)V

    .line 733
    :cond_3
    return-void

    :cond_4
    move v0, v2

    .line 722
    goto :goto_0

    :cond_5
    move v7, v2

    .line 723
    goto :goto_1

    :cond_6
    move v6, v2

    .line 724
    goto :goto_2

    :cond_7
    move v5, v1

    .line 725
    goto :goto_3

    :cond_8
    move v4, v1

    .line 726
    goto :goto_4

    :cond_9
    move v3, v2

    .line 728
    goto :goto_5

    :cond_a
    move v1, v2

    .line 729
    goto :goto_6
.end method

.method private forceScrollStop()V
    .locals 2

    .prologue
    .line 2159
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mScroller:Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller;->forceFinished(Z)V

    .line 2160
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->updateOverscrollOffset()V

    .line 2161
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mScrollOffset:F

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mScrollTarget:F

    .line 2162
    return-void
.end method

.method private getDefaultDiscardDirection()F
    .locals 2

    .prologue
    .line 444
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mCurrentMode:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    invoke-static {}, Lorg/chromium/ui/base/LocalizationUtils;->isLayoutRtl()Z

    move-result v0

    if-eqz v0, :cond_0

    const/high16 v0, -0x40800000    # -1.0f

    :goto_0
    return v0

    :cond_0
    const/high16 v0, 0x3f800000    # 1.0f

    goto :goto_0
.end method

.method private getDiscardRange()F
    .locals 1

    .prologue
    .line 2254
    const v0, 0x3f333333    # 0.7f

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->getRange(F)F

    move-result v0

    return v0
.end method

.method private getMaxScroll(Z)F
    .locals 1

    .prologue
    .line 2110
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    if-eqz v0, :cond_0

    if-nez p1, :cond_1

    .line 2111
    :cond_0
    const/4 v0, 0x0

    .line 2113
    :goto_0
    return v0

    :cond_1
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mMaxOverScroll:F

    goto :goto_0
.end method

.method private getMinScroll(Z)F
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 2092
    .line 2093
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    if-eqz v0, :cond_1

    .line 2095
    const/4 v0, 0x0

    move v1, v2

    :goto_0
    iget-object v3, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    array-length v3, v3

    if-ge v0, v3, :cond_2

    .line 2096
    iget-object v3, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    aget-object v3, v3, v0

    invoke-virtual {v3}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->isDying()Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    aget-object v3, v3, v0

    invoke-virtual {v3}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getLayoutTab()Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->isVisible()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 2097
    iget-object v3, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    aget-object v3, v3, v0

    invoke-virtual {v3}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getScrollOffset()F

    move-result v3

    invoke-static {v3, v1}, Ljava/lang/Math;->max(FF)F

    move-result v1

    .line 2095
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    move v1, v2

    .line 2101
    :cond_2
    if-eqz p1, :cond_3

    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mMaxUnderScroll:F

    neg-float v2, v0

    :cond_3
    sub-float v0, v2, v1

    return v0
.end method

.method private getRange(F)F
    .locals 2

    .prologue
    .line 2258
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mCurrentMode:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mLayout:Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->getWidth()F

    move-result v0

    :goto_0
    mul-float/2addr v0, p1

    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mLayout:Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->getHeightMinusTopControls()F

    move-result v0

    goto :goto_0
.end method

.method private getScrollDimensionSize()F
    .locals 2

    .prologue
    .line 1362
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mCurrentMode:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mLayout:Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->getHeightMinusTopControls()F

    move-result v0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mLayout:Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->getWidth()F

    move-result v0

    goto :goto_0
.end method

.method private getStackScale(Landroid/graphics/RectF;)F
    .locals 2

    .prologue
    .line 2074
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mCurrentMode:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    invoke-virtual {p1}, Landroid/graphics/RectF;->width()F

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mLayout:Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->getWidth()F

    move-result v1

    div-float/2addr v0, v1

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p1}, Landroid/graphics/RectF;->height()F

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mLayout:Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->getHeightMinusTopControls()F

    move-result v1

    div-float/2addr v0, v1

    goto :goto_0
.end method

.method private getTabAtPositon(FF)Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;
    .locals 2

    .prologue
    .line 1374
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->getTabIndexAtPositon(FFF)I

    move-result v0

    .line 1375
    if-gez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    aget-object v0, v1, v0

    goto :goto_0
.end method

.method private getTabIndexAtPositon(FF)I
    .locals 1

    .prologue
    .line 1386
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->getTabIndexAtPositon(FFF)I

    move-result v0

    return v0
.end method

.method private getTabIndexAtPositon(FFF)I
    .locals 6

    .prologue
    const/4 v4, -0x1

    .line 1398
    .line 1399
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mLayout:Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->getHeight()F

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mLayout:Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->getWidth()F

    move-result v1

    add-float/2addr v0, v1

    .line 1400
    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    if-eqz v1, :cond_4

    .line 1401
    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    array-length v1, v1

    add-int/lit8 v2, v1, -0x1

    move v1, v0

    move v3, v4

    :goto_0
    if-ltz v2, :cond_3

    .line 1404
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    aget-object v0, v0, v2

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->isDying()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    aget-object v0, v0, v2

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getLayoutTab()Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->isVisible()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1405
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    aget-object v0, v0, v2

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getLayoutTab()Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->computeDistanceTo(FF)F

    move-result v0

    .line 1408
    cmpg-float v5, v0, v1

    if-gez v5, :cond_2

    .line 1411
    const/4 v1, 0x0

    cmpl-float v1, v0, v1

    if-eqz v1, :cond_0

    move v1, v2

    .line 1401
    :goto_1
    add-int/lit8 v2, v2, -0x1

    move v3, v1

    move v1, v0

    goto :goto_0

    :cond_0
    move v1, v0

    move v0, v2

    .line 1416
    :goto_2
    cmpg-float v1, v1, p3

    if-gtz v1, :cond_1

    :goto_3
    return v0

    :cond_1
    move v0, v4

    goto :goto_3

    :cond_2
    move v0, v1

    move v1, v3

    goto :goto_1

    :cond_3
    move v0, v3

    goto :goto_2

    :cond_4
    move v1, v0

    move v0, v4

    goto :goto_2
.end method

.method private resetAllScrollOffset()V
    .locals 10

    .prologue
    const/4 v1, 0x0

    .line 2210
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mTabModel:Lorg/chromium/chrome/browser/tabmodel/TabModel;

    if-nez v0, :cond_0

    .line 2235
    :goto_0
    return-void

    .line 2215
    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->getScrollDimensionSize()F

    move-result v0

    iget v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mSpacing:I

    int-to-float v2, v2

    div-float/2addr v0, v2

    .line 2216
    const/high16 v2, 0x40000000    # 2.0f

    div-float v2, v0, v2

    const/high16 v3, 0x3f000000    # 0.5f

    sub-float/2addr v2, v3

    .line 2217
    iget-object v3, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mTabModel:Lorg/chromium/chrome/browser/tabmodel/TabModel;

    invoke-interface {v3}, Lorg/chromium/chrome/browser/tabmodel/TabModel;->getCount()I

    move-result v3

    .line 2218
    iget-object v4, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mTabModel:Lorg/chromium/chrome/browser/tabmodel/TabModel;

    invoke-interface {v4}, Lorg/chromium/chrome/browser/tabmodel/TabModel;->index()I

    move-result v4

    .line 2219
    int-to-float v5, v4

    cmpg-float v5, v5, v2

    if-ltz v5, :cond_1

    int-to-float v5, v3

    cmpg-float v5, v5, v0

    if-gtz v5, :cond_2

    .line 2220
    :cond_1
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mScrollOffset:F

    .line 2229
    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    if-eqz v0, :cond_5

    move v0, v1

    .line 2230
    :goto_2
    iget-object v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    array-length v2, v2

    if-ge v0, v2, :cond_5

    .line 2231
    iget-object v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    aget-object v2, v2, v0

    iget v3, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mSpacing:I

    mul-int/2addr v3, v0

    int-to-float v3, v3

    invoke-direct {p0, v3}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->screenToScroll(F)F

    move-result v3

    invoke-virtual {v2, v3}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->setScrollOffset(F)V

    .line 2230
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 2221
    :cond_2
    add-int/lit8 v5, v3, -0x1

    if-ne v4, v5, :cond_3

    float-to-double v6, v0

    invoke-static {v6, v7}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v6

    int-to-double v8, v3

    cmpg-double v5, v6, v8

    if-gez v5, :cond_3

    .line 2222
    int-to-float v2, v3

    sub-float/2addr v0, v2

    const/high16 v2, 0x3f800000    # 1.0f

    sub-float/2addr v0, v2

    iget v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mSpacing:I

    int-to-float v2, v2

    mul-float/2addr v0, v2

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mScrollOffset:F

    goto :goto_1

    .line 2223
    :cond_3
    sub-int v5, v3, v4

    add-int/lit8 v5, v5, -0x1

    int-to-float v5, v5

    cmpg-float v5, v5, v2

    if-gez v5, :cond_4

    .line 2224
    int-to-float v2, v3

    sub-float/2addr v0, v2

    iget v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mSpacing:I

    int-to-float v2, v2

    mul-float/2addr v0, v2

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mScrollOffset:F

    goto :goto_1

    .line 2226
    :cond_4
    int-to-float v0, v4

    sub-float v0, v2, v0

    iget v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mSpacing:I

    int-to-float v2, v2

    mul-float/2addr v0, v2

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mScrollOffset:F

    goto :goto_1

    .line 2234
    :cond_5
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mScrollOffset:F

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->setScrollTarget(FZ)V

    goto :goto_0
.end method

.method private resetInputActionIndices()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, -0x1

    .line 2325
    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mPinch0TabIndex:I

    .line 2326
    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mPinch1TabIndex:I

    .line 2327
    iput-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mScrollingTab:Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    .line 2328
    iput-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mDiscardingTab:Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    .line 2329
    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mLongPressSelected:I

    .line 2330
    return-void
.end method

.method private screenToScroll(F)F
    .locals 1

    .prologue
    .line 2246
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mWarpSize:F

    invoke-static {p1, v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->screenToScroll(FF)F

    move-result v0

    return v0
.end method

.method private scroll(FFFFZ)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v0, 0x0

    .line 880
    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mScroller:Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller;->isFinished()Z

    move-result v1

    if-nez v1, :cond_0

    if-nez p5, :cond_1

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mOverviewAnimationType:Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;

    sget-object v2, Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;->NONE:Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;

    if-eq v1, v2, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mOverviewAnimationType:Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;

    sget-object v2, Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;->DISCARD:Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;

    if-eq v1, v2, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mOverviewAnimationType:Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;

    sget-object v2, Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;->UNDISCARD:Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;

    if-eq v1, v2, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mOverviewAnimationType:Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;

    sget-object v2, Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;->DISCARD_ALL:Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;

    if-eq v1, v2, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mOverviewAnimationType:Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;

    sget-object v2, Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;->ENTER_STACK:Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;

    if-eq v1, v2, :cond_2

    .line 934
    :cond_1
    :goto_0
    return-void

    .line 889
    :cond_2
    iget v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mCurrentMode:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_7

    .line 895
    :goto_1
    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mScrollingTab:Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    if-eqz v1, :cond_3

    if-eqz p5, :cond_4

    .line 896
    :cond_3
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->getTabAtPositon(FF)Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mScrollingTab:Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    .line 899
    :cond_4
    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mScrollingTab:Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    if-nez v1, :cond_5

    iget-boolean v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mInSwipe:Z

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    if-eqz v1, :cond_5

    .line 900
    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mTabModel:Lorg/chromium/chrome/browser/tabmodel/TabModel;

    invoke-interface {v1}, Lorg/chromium/chrome/browser/tabmodel/TabModel;->index()I

    move-result v1

    .line 901
    if-ltz v1, :cond_5

    iget-object v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    array-length v2, v2

    if-gt v1, v2, :cond_5

    .line 902
    iget-object v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    aget-object v1, v2, v1

    iput-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mScrollingTab:Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    .line 905
    :cond_5
    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mScrollingTab:Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    if-nez v1, :cond_8

    .line 906
    if-nez p5, :cond_a

    move p4, v0

    move v1, v0

    .line 926
    :goto_2
    invoke-direct {p0, p4, v6}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->evenOutTabs(FZ)Z

    move-result v2

    if-eqz v2, :cond_6

    iget-object v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mScrollingTab:Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getIndex()I

    move-result v2

    if-lez v2, :cond_6

    .line 928
    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mScrollingTab:Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getScrollOffset()F

    move-result v1

    iget v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mScrollOffset:F

    add-float/2addr v1, v2

    .line 929
    sub-float v1, v0, v1

    .line 933
    :cond_6
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mScrollTarget:F

    add-float/2addr v0, v1

    invoke-direct {p0, v0, v6}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->setScrollTarget(FZ)V

    goto :goto_0

    :cond_7
    move p4, p3

    .line 889
    goto :goto_1

    .line 910
    :cond_8
    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mScrollingTab:Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getIndex()I

    move-result v1

    if-nez v1, :cond_9

    move v1, p4

    move p4, v0

    .line 911
    goto :goto_2

    .line 915
    :cond_9
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mScrollingTab:Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getScrollOffset()F

    move-result v0

    iget v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mScrollOffset:F

    add-float/2addr v1, v0

    .line 916
    invoke-direct {p0, v1}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->scrollToScreen(F)F

    move-result v0

    .line 917
    add-float/2addr v0, p4

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->screenToScroll(F)F

    move-result v0

    .line 918
    sub-float v1, v0, v1

    .line 921
    invoke-static {p4}, Ljava/lang/Math;->signum(F)F

    move-result v2

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    invoke-static {p4}, Ljava/lang/Math;->abs(F)F

    move-result v3

    const/high16 v4, 0x3f000000    # 0.5f

    mul-float/2addr v3, v4

    invoke-static {p4}, Ljava/lang/Math;->abs(F)F

    move-result v4

    const/high16 v5, 0x40000000    # 2.0f

    mul-float/2addr v4, v5

    invoke-static {v1, v3, v4}, Lorg/chromium/chrome/browser/util/MathUtils;->clamp(FFF)F

    move-result v1

    mul-float/2addr v1, v2

    goto :goto_2

    :cond_a
    move v1, p4

    goto :goto_2
.end method

.method private scrollToScreen(F)F
    .locals 1

    .prologue
    .line 2242
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mWarpSize:F

    invoke-static {p1, v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->scrollToScreen(FF)F

    move-result v0

    return v0
.end method

.method private setScrollTarget(FZ)V
    .locals 2

    .prologue
    .line 2085
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->allowOverscroll()Z

    move-result v0

    .line 2086
    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->getMinScroll(Z)F

    move-result v1

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->getMaxScroll(Z)F

    move-result v0

    invoke-static {p1, v1, v0}, Lorg/chromium/chrome/browser/util/MathUtils;->clamp(FFF)F

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mScrollTarget:F

    .line 2087
    if-eqz p2, :cond_0

    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mScrollTarget:F

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mScrollOffset:F

    .line 2088
    :cond_0
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mScrollTarget:F

    iget v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mScrollOffset:F

    sub-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->signum(F)F

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mCurrentScrollDirection:F

    .line 2089
    return-void
.end method

.method private setWarpState(ZZ)V
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 2357
    if-eqz p1, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->getScrollDimensionSize()F

    move-result v0

    const v2, 0x3ecccccd    # 0.4f

    mul-float/2addr v0, v2

    .line 2359
    :goto_0
    iget-object v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    if-eqz v2, :cond_1

    if-eqz p2, :cond_1

    iget v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mWarpSize:F

    invoke-static {v0, v2}, Ljava/lang/Float;->compare(FF)I

    move-result v2

    if-eqz v2, :cond_1

    .line 2360
    iget v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mScrollOffset:F

    invoke-direct {p0, v1}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->getMinScroll(Z)F

    move-result v3

    invoke-direct {p0, v1}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->getMaxScroll(Z)F

    move-result v4

    invoke-static {v2, v3, v4}, Lorg/chromium/chrome/browser/util/MathUtils;->clamp(FFF)F

    move-result v2

    .line 2362
    :goto_1
    iget-object v3, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    array-length v3, v3

    if-ge v1, v3, :cond_1

    .line 2363
    iget-object v3, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    aget-object v3, v3, v1

    .line 2364
    invoke-virtual {v3}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getScrollOffset()F

    move-result v4

    .line 2365
    add-float v5, v4, v2

    .line 2366
    iget v6, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mWarpSize:F

    invoke-static {v5, v6}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->scrollToScreen(FF)F

    move-result v6

    .line 2367
    invoke-static {v6, v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->screenToScroll(FF)F

    move-result v6

    .line 2368
    sub-float v5, v6, v5

    .line 2369
    add-float/2addr v4, v5

    invoke-virtual {v3, v4}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->setScrollOffset(F)V

    .line 2362
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 2357
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 2373
    :cond_1
    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mWarpSize:F

    .line 2374
    return-void
.end method

.method private smoothInput(FF)F
    .locals 2

    .prologue
    const/high16 v1, 0x41a00000    # 20.0f

    .line 2154
    sub-float v0, p2, v1

    add-float/2addr v1, p2

    invoke-static {p1, v0, v1}, Lorg/chromium/chrome/browser/util/MathUtils;->clamp(FFF)F

    move-result v0

    .line 2155
    const v1, 0x3f666666    # 0.9f

    invoke-static {v0, p2, v1}, Lcom/google/android/apps/chrome/utilities/ChromeMathUtils;->interpolate(FFF)F

    move-result v0

    return v0
.end method

.method private springBack(J)V
    .locals 11

    .prologue
    const/4 v2, 0x0

    .line 1265
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mScroller:Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller;->isFinished()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1266
    invoke-direct {p0, v2}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->getMinScroll(Z)F

    move-result v0

    float-to-int v6, v0

    .line 1267
    invoke-direct {p0, v2}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->getMaxScroll(Z)F

    move-result v0

    float-to-int v7, v0

    .line 1268
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mScrollTarget:F

    int-to-float v1, v6

    cmpg-float v0, v0, v1

    if-ltz v0, :cond_0

    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mScrollTarget:F

    int-to-float v1, v7

    cmpl-float v0, v0, v1

    if-lez v0, :cond_1

    .line 1269
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mScroller:Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller;

    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mScrollTarget:F

    float-to-int v3, v0

    move v4, v2

    move v5, v2

    move-wide v8, p1

    invoke-virtual/range {v1 .. v9}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller;->springBack(IIIIIIJ)Z

    .line 1270
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mScrollTarget:F

    int-to-float v1, v6

    int-to-float v3, v7

    invoke-static {v0, v1, v3}, Lorg/chromium/chrome/browser/util/MathUtils;->clamp(FFF)F

    move-result v0

    invoke-direct {p0, v0, v2}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->setScrollTarget(FZ)V

    .line 1271
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->requestUpdate()V

    .line 1274
    :cond_1
    return-void
.end method

.method private startAnimation(JLcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;)V
    .locals 7

    .prologue
    .line 477
    const/4 v5, -0x1

    const/4 v6, 0x0

    move-object v1, p0

    move-wide v2, p1

    move-object v4, p3

    invoke-direct/range {v1 .. v6}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->startAnimation(JLcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;IZ)V

    .line 478
    return-void
.end method

.method private startAnimation(JLcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;IIZ)V
    .locals 9

    .prologue
    .line 507
    move-object v1, p0

    move-wide v2, p1

    move-object v4, p3

    move v5, p5

    move v6, p6

    invoke-direct/range {v1 .. v6}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->canUpdateAnimation(JLcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;IZ)Z

    move-result v0

    if-nez v0, :cond_0

    .line 510
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->finishAnimation(J)V

    .line 512
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->stopScrollingMovement(J)V

    .line 515
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mAnimationFactory:Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mViewAnimationFactory:Lcom/google/android/apps/chrome/tabs/layout/phone/StackViewAnimation;

    if-eqz v0, :cond_7

    .line 516
    iput-object p3, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mOverviewAnimationType:Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;

    .line 520
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mViewAnimationFactory:Lcom/google/android/apps/chrome/tabs/layout/phone/StackViewAnimation;

    iget-object v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mLayout:Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->getViewContainer()Landroid/view/ViewGroup;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mTabModel:Lorg/chromium/chrome/browser/tabmodel/TabModel;

    move-object v1, p3

    move v5, p4

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackViewAnimation;->createAnimatorSetForType(Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;Landroid/view/ViewGroup;Lorg/chromium/chrome/browser/tabmodel/TabModel;I)Landroid/animation/AnimatorSet;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mViewAnimations:Landroid/animation/AnimatorSet;

    .line 523
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mViewAnimations:Landroid/animation/AnimatorSet;

    if-eqz v0, :cond_8

    .line 524
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mViewAnimations:Landroid/animation/AnimatorSet;

    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mViewAnimatorListener:Landroid/animation/AnimatorListenerAdapter;

    invoke-virtual {v0, v1}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 534
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mTabAnimations:Lcom/google/android/apps/chrome/ChromeAnimation;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mTabAnimations:Lcom/google/android/apps/chrome/ChromeAnimation;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ChromeAnimation;->start()V

    .line 535
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mViewAnimations:Landroid/animation/AnimatorSet;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mViewAnimations:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->start()V

    .line 536
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mTabAnimations:Lcom/google/android/apps/chrome/ChromeAnimation;

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mViewAnimations:Landroid/animation/AnimatorSet;

    if-eqz v0, :cond_4

    .line 537
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mLayout:Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->onStackAnimationStarted()V

    .line 540
    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mTabAnimations:Lcom/google/android/apps/chrome/ChromeAnimation;

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mViewAnimations:Landroid/animation/AnimatorSet;

    if-eqz v0, :cond_6

    :cond_5
    if-eqz p6, :cond_7

    .line 541
    :cond_6
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->finishAnimation(J)V

    .line 545
    :cond_7
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->requestUpdate()V

    .line 546
    return-void

    .line 529
    :cond_8
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mAnimationFactory:Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation;

    iget-object v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    iget v5, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mSpacing:I

    iget v6, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mScrollOffset:F

    iget v7, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mWarpSize:F

    invoke-direct {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->getDiscardRange()F

    move-result v8

    move-object v1, p3

    move v3, p4

    move v4, p5

    invoke-virtual/range {v0 .. v8}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation;->createAnimatorSetForType(Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;IIIFFF)Lcom/google/android/apps/chrome/ChromeAnimation;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mTabAnimations:Lcom/google/android/apps/chrome/ChromeAnimation;

    goto :goto_0
.end method

.method private startAnimation(JLcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;IZ)V
    .locals 9

    .prologue
    .line 501
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mTabModel:Lorg/chromium/chrome/browser/tabmodel/TabModel;

    invoke-interface {v0}, Lorg/chromium/chrome/browser/tabmodel/TabModel;->index()I

    move-result v5

    move-object v1, p0

    move-wide v2, p1

    move-object v4, p3

    move v6, p4

    move v7, p5

    invoke-direct/range {v1 .. v7}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->startAnimation(JLcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;IIZ)V

    .line 503
    return-void
.end method

.method private startAnimation(JLcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;Z)V
    .locals 7

    .prologue
    .line 488
    const/4 v5, -0x1

    move-object v1, p0

    move-wide v2, p1

    move-object v4, p3

    move v6, p4

    invoke-direct/range {v1 .. v6}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->startAnimation(JLcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;IZ)V

    .line 489
    return-void
.end method

.method private stopScrollingMovement(J)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 2119
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mScroller:Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller;->computeScrollOffset(J)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2122
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mScroller:Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller;->getCurrY()I

    move-result v0

    int-to-float v0, v0

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->setScrollTarget(FZ)V

    .line 2125
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mScroller:Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller;->forceFinished(Z)V

    .line 2131
    :goto_0
    return-void

    .line 2129
    :cond_0
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mScrollOffset:F

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->setScrollTarget(FZ)V

    goto :goto_0
.end method

.method private updateCurrentMode(I)V
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 2293
    iput p1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mCurrentMode:I

    .line 2294
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->getDefaultDiscardDirection()F

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mDiscardDirection:F

    .line 2295
    const/4 v0, 0x1

    invoke-direct {p0, v0, v7}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->setWarpState(ZZ)V

    .line 2296
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mBorderTopPadding:F

    iget v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mBorderTransparentTop:F

    sub-float v4, v0, v1

    .line 2297
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mLayout:Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->getWidth()F

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mLayout:Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->getHeight()F

    move-result v1

    iget-object v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mLayout:Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->getHeightMinusTopControls()F

    move-result v2

    iget v3, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mBorderTopPadding:F

    iget v5, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mBorderLeftPadding:F

    iget v6, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mCurrentMode:I

    invoke-static/range {v0 .. v6}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation;->createAnimationFactory(FFFFFFI)Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mAnimationFactory:Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation;

    .line 2300
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mLayout:Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    .line 2301
    new-instance v1, Lcom/google/android/apps/chrome/tabs/layout/phone/StackViewAnimation;

    iget-object v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mLayout:Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->getWidth()F

    move-result v2

    invoke-direct {v1, v0, v2}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackViewAnimation;-><init>(FF)V

    iput-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mViewAnimationFactory:Lcom/google/android/apps/chrome/tabs/layout/phone/StackViewAnimation;

    .line 2302
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    if-nez v0, :cond_1

    .line 2311
    :cond_0
    return-void

    .line 2303
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mLayout:Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->getWidth()F

    move-result v1

    .line 2304
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mLayout:Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->getHeightMinusTopControls()F

    move-result v2

    move v0, v7

    .line 2305
    :goto_0
    iget-object v3, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    array-length v3, v3

    if-ge v0, v3, :cond_0

    .line 2306
    iget-object v3, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    aget-object v3, v3, v0

    invoke-virtual {v3}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getLayoutTab()Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    move-result-object v3

    .line 2307
    if-eqz v3, :cond_2

    .line 2308
    invoke-virtual {v3, v1}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->setMaxContentWidth(F)V

    .line 2309
    invoke-virtual {v3, v2}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->setMaxContentHeight(F)V

    .line 2305
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private updateOverscrollOffset()V
    .locals 5

    .prologue
    const/4 v3, 0x0

    const/4 v4, 0x0

    .line 2191
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mScrollOffset:F

    invoke-direct {p0, v4}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->getMinScroll(Z)F

    move-result v1

    invoke-direct {p0, v4}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->getMaxScroll(Z)F

    move-result v2

    invoke-static {v0, v1, v2}, Lorg/chromium/chrome/browser/util/MathUtils;->clamp(FFF)F

    move-result v0

    .line 2192
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->allowOverscroll()Z

    move-result v1

    if-nez v1, :cond_0

    .line 2193
    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mScrollOffset:F

    .line 2195
    :cond_0
    iget v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mScrollOffset:F

    sub-float v0, v1, v0

    .line 2198
    iget v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mOverScrollOffset:F

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v2

    sub-float/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Math;->signum(F)F

    move-result v1

    float-to-int v1, v1

    .line 2199
    iget v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mOverScrollDerivative:I

    if-eq v1, v2, :cond_2

    const/4 v2, 0x1

    if-ne v1, v2, :cond_2

    cmpg-float v2, v0, v3

    if-gez v2, :cond_2

    .line 2200
    iget v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mOverScrollCounter:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mOverScrollCounter:I

    .line 2204
    :cond_1
    :goto_0
    iput v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mOverScrollDerivative:I

    .line 2206
    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mOverScrollOffset:F

    .line 2207
    return-void

    .line 2201
    :cond_2
    cmpl-float v2, v0, v3

    if-gtz v2, :cond_3

    iget v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mCurrentMode:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_1

    .line 2202
    :cond_3
    iput v4, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mOverScrollCounter:I

    goto :goto_0
.end method

.method private updateScrollOffset(J)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 2168
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mScrollOffset:F

    iget v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mScrollTarget:F

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_1

    .line 2169
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mScroller:Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller;->computeScrollOffset(J)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2170
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mScroller:Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller;->getCurrY()I

    move-result v0

    int-to-float v0, v0

    .line 2171
    iget v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mScrollOffset:F

    sub-float v1, v0, v1

    invoke-direct {p0, v1, v2}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->evenOutTabs(FZ)Z

    .line 2174
    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mScrollOffset:F

    .line 2182
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->requestUpdate()V

    .line 2187
    :goto_1
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->updateOverscrollOffset()V

    .line 2188
    return-void

    .line 2180
    :cond_0
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mScrollOffset:F

    iget v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mScrollTarget:F

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->smoothInput(FF)F

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mScrollOffset:F

    goto :goto_0

    .line 2185
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mScroller:Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller;->forceFinished(Z)V

    goto :goto_1
.end method


# virtual methods
.method public cleanupTabs()V
    .locals 1

    .prologue
    .line 2317
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    .line 2318
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->resetInputActionIndices()V

    .line 2319
    return-void
.end method

.method public click(JFF)V
    .locals 7

    .prologue
    const/4 v2, 0x0

    const/high16 v3, 0x41900000    # 18.0f

    const/4 v1, 0x1

    .line 1284
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mOverviewAnimationType:Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;

    sget-object v4, Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;->NONE:Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;

    if-eq v0, v4, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mOverviewAnimationType:Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;

    sget-object v4, Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;->DISCARD:Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;

    if-eq v0, v4, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mOverviewAnimationType:Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;

    sget-object v4, Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;->UNDISCARD:Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;

    if-eq v0, v4, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mOverviewAnimationType:Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;

    sget-object v4, Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;->DISCARD_ALL:Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;

    if-eq v0, v4, :cond_1

    .line 1317
    :cond_0
    :goto_0
    return-void

    .line 1290
    :cond_1
    invoke-static {}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getTouchSlop()F

    move-result v0

    invoke-direct {p0, p3, p4, v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->getTabIndexAtPositon(FFF)I

    move-result v4

    .line 1291
    if-ltz v4, :cond_0

    .line 1294
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mCurrentMode:I

    if-ne v0, v1, :cond_3

    move v0, v1

    :goto_1
    invoke-static {}, Lorg/chromium/ui/base/LocalizationUtils;->isLayoutRtl()Z

    move-result v5

    xor-int/2addr v0, v5

    if-nez v0, :cond_2

    move v2, v1

    .line 1296
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    aget-object v0, v0, v4

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getLayoutTab()Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    move-result-object v0

    invoke-virtual {v0, p3, p4, v2}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->checkCloseHitTest(FFZ)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1300
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    aget-object v4, v0, v4

    .line 1301
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mBorderTopPadding:F

    const/high16 v5, 0x40000000    # 2.0f

    div-float/2addr v0, v5

    .line 1303
    invoke-virtual {v4}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getLayoutTab()Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getOriginalContentWidth()F

    move-result v5

    .line 1305
    invoke-virtual {v4, v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->setDiscardOriginY(F)V

    .line 1306
    if-eqz v2, :cond_4

    move v0, v3

    :goto_2
    invoke-virtual {v4, v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->setDiscardOriginX(F)V

    .line 1307
    invoke-virtual {v4, v1}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->setDiscardFromClick(Z)V

    .line 1308
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mLayout:Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;

    invoke-virtual {v4}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getId()I

    move-result v1

    invoke-virtual {v0, p1, p2, v1}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->uiRequestingCloseTab(JI)V

    .line 1309
    invoke-static {}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->stackViewCloseTab()V

    goto :goto_0

    :cond_3
    move v0, v2

    .line 1294
    goto :goto_1

    .line 1306
    :cond_4
    sub-float v0, v5, v3

    goto :goto_2

    .line 1314
    :cond_5
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mLayout:Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;

    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    aget-object v1, v1, v4

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getId()I

    move-result v1

    invoke-virtual {v0, p1, p2, v1}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->uiSelectingTab(JI)V

    goto :goto_0
.end method

.method public computeTabPosition(JLandroid/graphics/RectF;)V
    .locals 1

    .prologue
    .line 1900
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    array-length v0, v0

    if-nez v0, :cond_1

    .line 1922
    :cond_0
    :goto_0
    return-void

    .line 1902
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mRecomputePosition:Z

    if-eqz v0, :cond_0

    .line 1903
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mRecomputePosition:Z

    .line 1906
    invoke-direct {p0, p3}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->computeTabScaleAlphaDepthHelper(Landroid/graphics/RectF;)V

    .line 1909
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->computeTabScrollOffsetHelper()V

    .line 1912
    invoke-direct {p0, p3}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->computeTabOffsetHelper(Landroid/graphics/RectF;)V

    .line 1915
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->computeTabTiltHelper(JLandroid/graphics/RectF;)V

    .line 1918
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->computeTabClippingVisibilityHelper()V

    .line 1921
    invoke-direct {p0, p3}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->computeTabVisibilitySortingHelper(Landroid/graphics/RectF;)V

    goto :goto_0
.end method

.method public contextChanged(Landroid/content/Context;)V
    .locals 5

    .prologue
    const/high16 v4, 0x3f800000    # 1.0f

    .line 1327
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 1328
    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->density:F

    div-float v1, v4, v1

    .line 1330
    const/high16 v2, 0x3fa00000    # 1.25f

    iput v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mMinScrollMotion:F

    .line 1331
    sget v2, Lcom/google/android/apps/chrome/R$dimen;->over_scroll:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v2

    int-to-float v2, v2

    .line 1332
    const/high16 v3, 0x40000000    # 2.0f

    mul-float/2addr v3, v2

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v3

    int-to-float v3, v3

    .line 1333
    mul-float/2addr v2, v1

    iput v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mMaxOverScroll:F

    .line 1334
    mul-float v2, v3, v1

    iput v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mMaxUnderScroll:F

    .line 1335
    sget v2, Lcom/google/android/apps/chrome/R$integer;->over_scroll_angle:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v2

    int-to-float v2, v2

    iput v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mMaxOverScrollAngle:F

    .line 1336
    sget v2, Lcom/google/android/apps/chrome/R$dimen;->over_scroll_slide:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v2

    int-to-float v2, v2

    mul-float/2addr v2, v1

    iput v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mMaxOverScrollSlide:F

    .line 1337
    sget v2, Lcom/google/android/apps/chrome/R$dimen;->even_out_scrolling:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    mul-float/2addr v2, v1

    div-float v2, v4, v2

    iput v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mEvenOutRate:F

    .line 1338
    sget v2, Lcom/google/android/apps/chrome/R$dimen;->min_spacing:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v2

    int-to-float v2, v2

    mul-float/2addr v2, v1

    iput v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mMinSpacing:F

    .line 1339
    sget v2, Lcom/google/android/apps/chrome/R$dimen;->tabswitcher_border_frame_transparent_top:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    mul-float/2addr v2, v1

    iput v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mBorderTransparentTop:F

    .line 1341
    sget v2, Lcom/google/android/apps/chrome/R$dimen;->tabswitcher_border_frame_transparent_side:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    mul-float/2addr v2, v1

    iput v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mBorderTransparentSide:F

    .line 1343
    sget v2, Lcom/google/android/apps/chrome/R$dimen;->tabswitcher_border_frame_padding_top:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    mul-float/2addr v2, v1

    iput v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mBorderTopPadding:F

    .line 1345
    sget v2, Lcom/google/android/apps/chrome/R$dimen;->tabswitcher_border_frame_padding_left:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    mul-float/2addr v0, v1

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mBorderLeftPadding:F

    .line 1349
    new-instance v0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller;

    invoke-direct {v0, p1}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mScroller:Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller;

    .line 1350
    return-void
.end method

.method public drag(JFFFF)V
    .locals 7

    .prologue
    const/4 v5, 0x0

    .line 788
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mCurrentMode:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    move v0, p6

    move v1, p5

    .line 795
    :goto_0
    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->computeDragLock(FF)Lcom/google/android/apps/chrome/tabs/layout/phone/Stack$DragLock;

    move-result-object v0

    .line 796
    sget-object v1, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack$DragLock;->DISCARD:Lcom/google/android/apps/chrome/tabs/layout/phone/Stack$DragLock;

    if-ne v0, v1, :cond_2

    .line 797
    invoke-direct {p0, p3, p4, p5, p6}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->discard(FFFF)V

    .line 806
    :goto_1
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->requestUpdate()V

    .line 807
    return-void

    .line 793
    :cond_0
    invoke-static {}, Lorg/chromium/ui/base/LocalizationUtils;->isLayoutRtl()Z

    move-result v0

    if-eqz v0, :cond_1

    neg-float v0, p5

    move v1, p6

    goto :goto_0

    :cond_1
    move v0, p5

    move v1, p6

    goto :goto_0

    .line 801
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mDragLock:Lcom/google/android/apps/chrome/tabs/layout/phone/Stack$DragLock;

    sget-object v1, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack$DragLock;->SCROLL:Lcom/google/android/apps/chrome/tabs/layout/phone/Stack$DragLock;

    if-ne v0, v1, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mDiscardingTab:Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    if-eqz v0, :cond_3

    .line 802
    invoke-direct {p0, p1, p2, v5}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->commitDiscard(JZ)V

    .line 804
    :cond_3
    invoke-static {}, Lorg/chromium/ui/base/LocalizationUtils;->isLayoutRtl()Z

    move-result v0

    if-eqz v0, :cond_4

    neg-float v3, p5

    :goto_2
    move-object v0, p0

    move v1, p3

    move v2, p4

    move v4, p6

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->scroll(FFFFZ)V

    goto :goto_1

    :cond_4
    move v3, p5

    goto :goto_2
.end method

.method public ensureCleaningUpDyingTabs(J)V
    .locals 1

    .prologue
    .line 642
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->finishAnimation(J)V

    .line 643
    return-void
.end method

.method public fling(JFFFF)V
    .locals 19

    .prologue
    .line 1010
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mDragLock:Lcom/google/android/apps/chrome/tabs/layout/phone/Stack$DragLock;

    sget-object v5, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack$DragLock;->SCROLL:Lcom/google/android/apps/chrome/tabs/layout/phone/Stack$DragLock;

    if-eq v4, v5, :cond_2

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mDiscardingTab:Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    if-eqz v4, :cond_2

    .line 1011
    move-object/from16 v0, p0

    iget v4, v0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mCurrentMode:I

    const/4 v5, 0x1

    if-ne v4, v5, :cond_1

    .line 1012
    :goto_0
    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->getDiscardRange()F

    move-result v4

    const v5, 0x3ecccccd    # 0.4f

    mul-float/2addr v4, v5

    .line 1013
    const v5, 0x3cb60b61

    mul-float v5, v5, p5

    neg-float v6, v4

    invoke-static {v5, v6, v4}, Lorg/chromium/chrome/browser/util/MathUtils;->clamp(FFF)F

    move-result v4

    .line 1014
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mDiscardingTab:Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    invoke-virtual {v5, v4}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->addToDiscardAmount(F)V

    .line 1036
    :cond_0
    :goto_1
    return-void

    :cond_1
    move/from16 p5, p6

    .line 1011
    goto :goto_0

    .line 1015
    :cond_2
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mOverviewAnimationType:Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;

    sget-object v5, Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;->NONE:Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;

    if-ne v4, v5, :cond_0

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mScroller:Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller;

    invoke-virtual {v4}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller;->isFinished()Z

    move-result v4

    if-eqz v4, :cond_0

    move-object/from16 v0, p0

    iget v4, v0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mOverScrollOffset:F

    const/4 v5, 0x0

    cmpl-float v4, v4, v5

    if-nez v4, :cond_0

    move-object/from16 v0, p0

    move/from16 v1, p3

    move/from16 v2, p4

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->getTabIndexAtPositon(FF)I

    move-result v4

    if-ltz v4, :cond_0

    .line 1019
    move-object/from16 v0, p0

    iget v4, v0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mCurrentMode:I

    const/4 v5, 0x1

    if-ne v4, v5, :cond_3

    .line 1023
    :goto_2
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mScroller:Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller;

    const/4 v6, 0x0

    move-object/from16 v0, p0

    iget v4, v0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mScrollTarget:F

    float-to-int v7, v4

    const/4 v8, 0x0

    move/from16 v0, p6

    float-to-int v9, v0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v4, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->getMinScroll(Z)F

    move-result v4

    float-to-int v12, v4

    const/4 v4, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->getMaxScroll(Z)F

    move-result v4

    float-to-int v13, v4

    const/4 v14, 0x0

    const/4 v4, 0x0

    cmpl-float v4, p6, v4

    if-lez v4, :cond_5

    move-object/from16 v0, p0

    iget v4, v0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mMaxOverScroll:F

    :goto_3
    const/high16 v15, 0x3f000000    # 0.5f

    mul-float/2addr v4, v15

    float-to-int v15, v4

    move-wide/from16 v16, p1

    invoke-virtual/range {v5 .. v17}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller;->fling(IIIIIIIIIIJ)V

    .line 1034
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mScroller:Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller;

    invoke-virtual {v4}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller;->getFinalY()I

    move-result v4

    int-to-float v4, v4

    const/4 v5, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v5}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->setScrollTarget(FZ)V

    goto :goto_1

    .line 1019
    :cond_3
    invoke-static {}, Lorg/chromium/ui/base/LocalizationUtils;->isLayoutRtl()Z

    move-result v4

    if-eqz v4, :cond_4

    move/from16 v0, p5

    neg-float v0, v0

    move/from16 p6, v0

    goto :goto_2

    :cond_4
    move/from16 p6, p5

    goto :goto_2

    .line 1023
    :cond_5
    move-object/from16 v0, p0

    iget v4, v0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mMaxUnderScroll:F

    goto :goto_3
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 289
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    array-length v0, v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getTabs()[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;
    .locals 1

    .prologue
    .line 281
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    return-object v0
.end method

.method public getVisibleCount()I
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 296
    .line 297
    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    if-eqz v1, :cond_1

    move v1, v0

    .line 298
    :goto_0
    iget-object v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    array-length v2, v2

    if-ge v0, v2, :cond_2

    .line 299
    iget-object v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getLayoutTab()Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->isVisible()Z

    move-result v2

    if-eqz v2, :cond_0

    add-int/lit8 v1, v1, 0x1

    .line 298
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    move v1, v0

    .line 302
    :cond_2
    return v1
.end method

.method public isDisplayable()Z
    .locals 1

    .prologue
    .line 440
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mTabModel:Lorg/chromium/chrome/browser/tabmodel/TabModel;

    invoke-interface {v0}, Lorg/chromium/chrome/browser/tabmodel/TabModel;->isIncognito()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mIsDying:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mTabModel:Lorg/chromium/chrome/browser/tabmodel/TabModel;

    invoke-interface {v0}, Lorg/chromium/chrome/browser/tabmodel/TabModel;->getCount()I

    move-result v0

    if-lez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public notifySizeChanged(FFI)V
    .locals 0

    .prologue
    .line 1358
    invoke-direct {p0, p3}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->updateCurrentMode(I)V

    .line 1359
    return-void
.end method

.method public onDown(J)V
    .locals 3

    .prologue
    .line 1044
    sget-object v0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack$DragLock;->NONE:Lcom/google/android/apps/chrome/tabs/layout/phone/Stack$DragLock;

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mDragLock:Lcom/google/android/apps/chrome/tabs/layout/phone/Stack$DragLock;

    .line 1045
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mOverviewAnimationType:Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;

    sget-object v1, Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;->NONE:Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;

    if-ne v0, v1, :cond_0

    .line 1046
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->stopScrollingMovement(J)V

    .line 1049
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mScrollingTab:Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    .line 1050
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->commitDiscard(JZ)V

    .line 1051
    return-void
.end method

.method public onLongPress(JFF)V
    .locals 7

    .prologue
    .line 1061
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mOverviewAnimationType:Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;

    sget-object v1, Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;->NONE:Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;

    if-ne v0, v1, :cond_0

    .line 1062
    invoke-direct {p0, p3, p4}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->getTabIndexAtPositon(FF)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mLongPressSelected:I

    .line 1063
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mLongPressSelected:I

    if-ltz v0, :cond_0

    .line 1064
    sget-object v4, Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;->VIEW_MORE:Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;

    iget v5, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mLongPressSelected:I

    const/4 v6, 0x0

    move-object v1, p0

    move-wide v2, p1

    invoke-direct/range {v1 .. v6}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->startAnimation(JLcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;IZ)V

    .line 1065
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mEvenOutProgress:F

    .line 1068
    :cond_0
    return-void
.end method

.method public onPinch(JFFFFZ)V
    .locals 17

    .prologue
    .line 1081
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mOverviewAnimationType:Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;

    sget-object v4, Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;->START_PINCH:Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;

    if-eq v3, v4, :cond_0

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mOverviewAnimationType:Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;

    sget-object v4, Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;->NONE:Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;

    if-ne v3, v4, :cond_1

    :cond_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    if-nez v3, :cond_2

    .line 1218
    :cond_1
    :goto_0
    return-void

    .line 1084
    :cond_2
    move-object/from16 v0, p0

    iget v3, v0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mPinch0TabIndex:I

    if-gez v3, :cond_3

    sget-object v3, Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;->START_PINCH:Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;

    move-object/from16 v0, p0

    move-wide/from16 v1, p1

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->startAnimation(JLcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;)V

    .line 1090
    :cond_3
    move-object/from16 v0, p0

    iget v3, v0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mCurrentMode:I

    const/4 v4, 0x1

    if-ne v3, v4, :cond_8

    cmpl-float v3, p4, p6

    if-lez v3, :cond_7

    const/4 v3, 0x1

    .line 1093
    :goto_1
    if-eqz v3, :cond_c

    move/from16 v5, p5

    .line 1094
    :goto_2
    if-eqz v3, :cond_d

    move/from16 v4, p6

    .line 1095
    :goto_3
    if-eqz v3, :cond_e

    move/from16 v7, p3

    .line 1096
    :goto_4
    if-eqz v3, :cond_f

    .line 1097
    :goto_5
    move-object/from16 v0, p0

    iget v3, v0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mCurrentMode:I

    const/4 v6, 0x1

    if-ne v3, v6, :cond_10

    move v3, v4

    .line 1100
    :goto_6
    move-object/from16 v0, p0

    iget v6, v0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mCurrentMode:I

    const/4 v8, 0x1

    if-ne v6, v8, :cond_12

    move/from16 v11, p4

    .line 1104
    :goto_7
    if-eqz p7, :cond_4

    .line 1106
    const/4 v6, -0x1

    move-object/from16 v0, p0

    iput v6, v0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mPinch0TabIndex:I

    .line 1107
    const/4 v6, -0x1

    move-object/from16 v0, p0

    iput v6, v0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mPinch1TabIndex:I

    .line 1108
    const/4 v6, 0x0

    move-object/from16 v0, p0

    iput-object v6, v0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mScrollingTab:Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    .line 1109
    const/4 v6, 0x0

    move-object/from16 v0, p0

    move-wide/from16 v1, p1

    invoke-direct {v0, v1, v2, v6}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->commitDiscard(JZ)V

    .line 1111
    :cond_4
    move-object/from16 v0, p0

    iget v8, v0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mPinch0TabIndex:I

    .line 1112
    move-object/from16 v0, p0

    iget v6, v0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mPinch1TabIndex:I

    .line 1113
    move-object/from16 v0, p0

    iget v9, v0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mPinch0TabIndex:I

    if-gez v9, :cond_19

    .line 1114
    move-object/from16 v0, p0

    invoke-direct {v0, v5, v4}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->getTabIndexAtPositon(FF)I

    move-result v8

    .line 1115
    move-object/from16 v0, p0

    move/from16 v1, p4

    invoke-direct {v0, v7, v1}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->getTabIndexAtPositon(FF)I

    move-result v4

    .line 1117
    if-ltz v8, :cond_5

    if-gez v4, :cond_18

    .line 1118
    :cond_5
    const/4 v8, -0x1

    .line 1119
    const/4 v4, -0x1

    move v10, v4

    .line 1123
    :goto_8
    if-ltz v8, :cond_6

    move-object/from16 v0, p0

    iget v4, v0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mPinch0TabIndex:I

    if-ne v4, v8, :cond_6

    move-object/from16 v0, p0

    iget v4, v0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mPinch1TabIndex:I

    if-ne v4, v10, :cond_6

    .line 1126
    const/4 v4, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->getMinScroll(Z)F

    move-result v4

    .line 1127
    const/4 v5, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->getMaxScroll(Z)F

    move-result v5

    .line 1128
    move-object/from16 v0, p0

    iget v6, v0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mScrollTarget:F

    invoke-static {v6, v4, v5}, Lorg/chromium/chrome/browser/util/MathUtils;->clamp(FFF)F

    move-result v12

    .line 1132
    if-lt v8, v10, :cond_15

    .line 1134
    move-object/from16 v0, p0

    iget v4, v0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mLastPinch0Offset:F

    sub-float v4, v3, v4

    .line 1135
    if-nez v8, :cond_14

    .line 1137
    add-float/2addr v4, v12

    const/4 v5, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v5}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->setScrollTarget(FZ)V

    .line 1212
    :cond_6
    :goto_9
    move-object/from16 v0, p0

    iput v8, v0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mPinch0TabIndex:I

    .line 1213
    move-object/from16 v0, p0

    iput v10, v0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mPinch1TabIndex:I

    .line 1214
    move-object/from16 v0, p0

    iput v3, v0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mLastPinch0Offset:F

    .line 1215
    move-object/from16 v0, p0

    iput v11, v0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mLastPinch1Offset:F

    .line 1216
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput v3, v0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mEvenOutProgress:F

    .line 1217
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->requestUpdate()V

    goto/16 :goto_0

    .line 1090
    :cond_7
    const/4 v3, 0x0

    goto/16 :goto_1

    :cond_8
    invoke-static {}, Lorg/chromium/ui/base/LocalizationUtils;->isLayoutRtl()Z

    move-result v3

    if-eqz v3, :cond_a

    cmpg-float v3, p3, p5

    if-gtz v3, :cond_9

    const/4 v3, 0x1

    goto/16 :goto_1

    :cond_9
    const/4 v3, 0x0

    goto/16 :goto_1

    :cond_a
    cmpl-float v3, p3, p5

    if-lez v3, :cond_b

    const/4 v3, 0x1

    goto/16 :goto_1

    :cond_b
    const/4 v3, 0x0

    goto/16 :goto_1

    :cond_c
    move/from16 v5, p3

    .line 1093
    goto/16 :goto_2

    :cond_d
    move/from16 v4, p4

    .line 1094
    goto/16 :goto_3

    :cond_e
    move/from16 v7, p5

    .line 1095
    goto/16 :goto_4

    :cond_f
    move/from16 p4, p6

    .line 1096
    goto/16 :goto_5

    .line 1097
    :cond_10
    invoke-static {}, Lorg/chromium/ui/base/LocalizationUtils;->isLayoutRtl()Z

    move-result v3

    if-eqz v3, :cond_11

    neg-float v3, v5

    goto/16 :goto_6

    :cond_11
    move v3, v5

    goto/16 :goto_6

    .line 1100
    :cond_12
    invoke-static {}, Lorg/chromium/ui/base/LocalizationUtils;->isLayoutRtl()Z

    move-result v6

    if-eqz v6, :cond_13

    neg-float v6, v7

    move v11, v6

    goto/16 :goto_7

    :cond_13
    move v11, v7

    goto/16 :goto_7

    .line 1139
    :cond_14
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    aget-object v5, v5, v8

    invoke-virtual {v5}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getScrollOffset()F

    move-result v5

    add-float/2addr v5, v12

    .line 1141
    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->scrollToScreen(F)F

    move-result v5

    .line 1142
    add-float/2addr v4, v5

    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->screenToScroll(F)F

    move-result v4

    .line 1143
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    aget-object v5, v5, v8

    invoke-virtual {v5}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getScrollOffset()F

    move-result v5

    sub-float/2addr v4, v5

    const/4 v5, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v5}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->setScrollTarget(FZ)V

    goto/16 :goto_9

    .line 1150
    :cond_15
    move-object/from16 v0, p0

    iget v4, v0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mLastPinch0Offset:F

    sub-float v4, v3, v4

    .line 1151
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    aget-object v5, v5, v8

    move-object/from16 v0, p0

    invoke-direct {v0, v5, v12}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->approxScreen(Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;F)F

    move-result v13

    .line 1152
    add-float v9, v13, v4

    .line 1156
    move-object/from16 v0, p0

    iget v4, v0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mLastPinch1Offset:F

    sub-float v4, v11, v4

    .line 1157
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    aget-object v5, v5, v10

    move-object/from16 v0, p0

    invoke-direct {v0, v5, v12}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->approxScreen(Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;F)F

    move-result v5

    .line 1158
    add-float v14, v5, v4

    .line 1164
    move-object/from16 v0, p0

    invoke-direct {v0, v13}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->screenToScroll(F)F

    move-result v4

    .line 1165
    add-float v6, v9, v13

    const/high16 v7, 0x40000000    # 2.0f

    div-float/2addr v6, v7

    move-object/from16 v0, p0

    invoke-direct {v0, v6}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->screenToScroll(F)F

    move-result v6

    .line 1166
    add-float/2addr v6, v12

    sub-float v4, v6, v4

    const/4 v6, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v6}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->setScrollTarget(FZ)V

    move v4, v8

    move v6, v9

    move v7, v9

    .line 1171
    :goto_a
    if-gt v4, v10, :cond_16

    .line 1172
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    aget-object v15, v15, v4

    move-object/from16 v0, p0

    invoke-direct {v0, v15, v12}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->approxScreen(Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;F)F

    move-result v15

    .line 1173
    sub-float/2addr v15, v13

    sub-float v16, v5, v13

    div-float v15, v15, v16

    .line 1175
    const/high16 v16, 0x3f800000    # 1.0f

    sub-float v16, v16, v15

    mul-float v16, v16, v9

    mul-float/2addr v15, v14

    add-float v15, v15, v16

    .line 1176
    invoke-static {v7, v15}, Ljava/lang/Math;->max(FF)F

    move-result v7

    .line 1177
    invoke-static {v6, v7}, Ljava/lang/Math;->min(FF)F

    move-result v15

    .line 1178
    sget v6, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->sStackedTabVisibleSize:F

    add-float v7, v15, v6

    .line 1179
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    aget-object v6, v6, v4

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mCurrentMode:I

    move/from16 v16, v0

    move/from16 v0, v16

    invoke-virtual {v6, v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getSizeInScrollDirection(I)F

    move-result v6

    add-float/2addr v6, v15

    .line 1180
    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->screenToScroll(F)F

    move-result v15

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mScrollTarget:F

    move/from16 v16, v0

    sub-float v15, v15, v16

    .line 1181
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    move-object/from16 v16, v0

    aget-object v16, v16, v4

    move-object/from16 v0, v16

    invoke-virtual {v0, v15}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->setScrollOffset(F)V

    .line 1171
    add-int/lit8 v4, v4, 0x1

    goto :goto_a

    .line 1185
    :cond_16
    sub-float v5, v14, v5

    .line 1186
    add-int/lit8 v4, v10, 0x1

    :goto_b
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    array-length v14, v14

    if-ge v4, v14, :cond_17

    .line 1187
    const/high16 v14, 0x40000000    # 2.0f

    div-float/2addr v5, v14

    .line 1188
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    aget-object v14, v14, v4

    move-object/from16 v0, p0

    invoke-direct {v0, v14, v12}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->approxScreen(Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;F)F

    move-result v14

    add-float/2addr v14, v5

    .line 1189
    invoke-static {v7, v14}, Ljava/lang/Math;->max(FF)F

    move-result v7

    .line 1190
    invoke-static {v6, v7}, Ljava/lang/Math;->min(FF)F

    move-result v14

    .line 1191
    sget v6, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->sStackedTabVisibleSize:F

    add-float v7, v14, v6

    .line 1192
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    aget-object v6, v6, v4

    move-object/from16 v0, p0

    iget v15, v0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mCurrentMode:I

    invoke-virtual {v6, v15}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getSizeInScrollDirection(I)F

    move-result v6

    add-float/2addr v6, v14

    .line 1193
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    aget-object v15, v15, v4

    move-object/from16 v0, p0

    invoke-direct {v0, v14}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->screenToScroll(F)F

    move-result v14

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mScrollTarget:F

    move/from16 v16, v0

    sub-float v14, v14, v16

    invoke-virtual {v15, v14}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->setScrollOffset(F)V

    .line 1186
    add-int/lit8 v4, v4, 0x1

    goto :goto_b

    .line 1200
    :cond_17
    sub-float v5, v9, v13

    .line 1201
    add-int/lit8 v4, v8, -0x1

    :goto_c
    if-lez v4, :cond_6

    .line 1202
    const/high16 v6, 0x40000000    # 2.0f

    div-float/2addr v5, v6

    .line 1203
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    aget-object v6, v6, v4

    move-object/from16 v0, p0

    iget v7, v0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mCurrentMode:I

    invoke-virtual {v6, v7}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getSizeInScrollDirection(I)F

    move-result v6

    sub-float v6, v9, v6

    .line 1204
    sget v7, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->sStackedTabVisibleSize:F

    sub-float v7, v9, v7

    .line 1205
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    aget-object v13, v13, v4

    move-object/from16 v0, p0

    invoke-direct {v0, v13, v12}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->approxScreen(Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;F)F

    move-result v13

    add-float/2addr v13, v5

    .line 1206
    invoke-static {v6, v13}, Ljava/lang/Math;->max(FF)F

    move-result v6

    .line 1207
    invoke-static {v7, v6}, Ljava/lang/Math;->min(FF)F

    move-result v6

    .line 1208
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    aget-object v7, v7, v4

    move-object/from16 v0, p0

    invoke-direct {v0, v6}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->screenToScroll(F)F

    move-result v6

    move-object/from16 v0, p0

    iget v13, v0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mScrollTarget:F

    sub-float/2addr v6, v13

    invoke-virtual {v7, v6}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->setScrollOffset(F)V

    .line 1201
    add-int/lit8 v4, v4, -0x1

    goto :goto_c

    :cond_18
    move v10, v4

    goto/16 :goto_8

    :cond_19
    move v10, v6

    goto/16 :goto_8
.end method

.method public onUpOrCancel(J)V
    .locals 1

    .prologue
    .line 1249
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mPinch0TabIndex:I

    if-ltz v0, :cond_0

    .line 1250
    sget-object v0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;->REACH_TOP:Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->startAnimation(JLcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;)V

    .line 1251
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->requestUpdate()V

    .line 1254
    :cond_0
    const/4 v0, 0x1

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->commitDiscard(JZ)V

    .line 1256
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->resetInputActionIndices()V

    .line 1258
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->springBack(J)V

    .line 1259
    return-void
.end method

.method public onUpdateCompositorAnimations(JZ)Z
    .locals 3

    .prologue
    .line 705
    if-nez p3, :cond_0

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->updateScrollOffset(J)V

    .line 707
    :cond_0
    const/4 v0, 0x1

    .line 708
    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mTabAnimations:Lcom/google/android/apps/chrome/ChromeAnimation;

    if-eqz v1, :cond_1

    .line 709
    if-eqz p3, :cond_3

    .line 710
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mTabAnimations:Lcom/google/android/apps/chrome/ChromeAnimation;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ChromeAnimation;->finished()Z

    move-result v0

    .line 714
    :goto_0
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->finishAnimationsIfDone(JZ)V

    .line 717
    :cond_1
    if-eqz p3, :cond_2

    invoke-direct {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->forceScrollStop()V

    .line 718
    :cond_2
    return v0

    .line 712
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mTabAnimations:Lcom/google/android/apps/chrome/ChromeAnimation;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/chrome/ChromeAnimation;->update(J)Z

    move-result v0

    goto :goto_0
.end method

.method public onUpdateViewAnimation(JZ)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 690
    .line 691
    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mViewAnimations:Landroid/animation/AnimatorSet;

    if-eqz v1, :cond_0

    .line 692
    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mViewAnimations:Landroid/animation/AnimatorSet;

    invoke-virtual {v1}, Landroid/animation/AnimatorSet;->isRunning()Z

    move-result v1

    if-nez v1, :cond_1

    .line 693
    :goto_0
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->finishAnimationsIfDone(JZ)V

    .line 695
    :cond_0
    return v0

    .line 692
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public requestUpdate()V
    .locals 1

    .prologue
    .line 2336
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mRecomputePosition:Z

    .line 2337
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mLayout:Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->requestUpdate()V

    .line 2338
    return-void
.end method

.method public reset()V
    .locals 1

    .prologue
    .line 2345
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mIsDying:Z

    .line 2346
    return-void
.end method

.method public setStackFocusInfo(FI)V
    .locals 2

    .prologue
    .line 1930
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    if-nez v0, :cond_1

    .line 1935
    :cond_0
    return-void

    .line 1931
    :cond_1
    iput p2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mReferenceOrderIndex:I

    .line 1932
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 1933
    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getLayoutTab()Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->setBorderCloseButtonAlpha(F)V

    .line 1932
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public setTabModel(Lorg/chromium/chrome/browser/tabmodel/TabModel;)V
    .locals 0

    .prologue
    .line 273
    iput-object p1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mTabModel:Lorg/chromium/chrome/browser/tabmodel/TabModel;

    .line 274
    return-void
.end method

.method public show()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 453
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->getDefaultDiscardDirection()F

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mDiscardDirection:F

    .line 456
    iput v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mOverScrollCounter:I

    .line 460
    invoke-direct {p0, v1}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->createStackTabs(Z)V

    .line 461
    return-void
.end method

.method public stackEntered(JZ)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 430
    if-nez p3, :cond_1

    const/4 v0, 0x1

    .line 431
    :goto_0
    iget-object v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    if-eqz v2, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    array-length v1, v1

    :cond_0
    invoke-direct {p0, v1}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->computeSpacing(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mSpacing:I

    .line 432
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->resetAllScrollOffset()V

    .line 433
    sget-object v1, Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;->ENTER_STACK:Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;

    invoke-direct {p0, p1, p2, v1, v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->startAnimation(JLcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;Z)V

    .line 434
    return-void

    :cond_1
    move v0, v1

    .line 430
    goto :goto_0
.end method

.method public swipeCancelled(J)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 2512
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mInSwipe:Z

    if-nez v0, :cond_0

    .line 2524
    :goto_0
    return-void

    .line 2514
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mDiscardingTab:Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    .line 2516
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mInSwipe:Z

    .line 2518
    invoke-direct {p0, v1, v1}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->setWarpState(ZZ)V

    .line 2519
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mEvenOutProgress:F

    .line 2522
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mTabModel:Lorg/chromium/chrome/browser/tabmodel/TabModel;

    invoke-static {v0}, Lorg/chromium/chrome/browser/tabmodel/TabModelUtils;->getCurrentTab(Lorg/chromium/chrome/browser/tabmodel/TabList;)Lorg/chromium/chrome/browser/Tab;

    move-result-object v0

    .line 2523
    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mLayout:Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/Tab;->getId()I

    move-result v0

    :goto_1
    invoke-virtual {v1, p1, p2, v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->uiSelectingTab(JI)V

    goto :goto_0

    :cond_1
    const/4 v0, -0x1

    goto :goto_1
.end method

.method public swipeFinished(J)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 2495
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mInSwipe:Z

    if-nez v0, :cond_0

    .line 2504
    :goto_0
    return-void

    .line 2497
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mInSwipe:Z

    .line 2500
    invoke-direct {p0, v1, v1}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->setWarpState(ZZ)V

    .line 2501
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mEvenOutProgress:F

    .line 2503
    invoke-virtual {p0, p1, p2}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->onUpOrCancel(J)V

    goto :goto_0
.end method

.method public swipeFlingOccurred(JFFFFFF)V
    .locals 9

    .prologue
    .line 2538
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mInSwipe:Z

    if-nez v0, :cond_0

    .line 2544
    :goto_0
    return-void

    :cond_0
    move-object v1, p0

    move-wide v2, p1

    move v4, p3

    move v5, p4

    move/from16 v6, p7

    move/from16 v7, p8

    .line 2541
    invoke-virtual/range {v1 .. v7}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->fling(JFFFF)V

    .line 2543
    invoke-virtual {p0, p1, p2}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->onUpOrCancel(J)V

    goto :goto_0
.end method

.method public swipeStarted(JLcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;FF)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2384
    sget-object v0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;->DOWN:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;

    if-eq p3, v0, :cond_0

    .line 2415
    :goto_0
    return-void

    .line 2387
    :cond_0
    invoke-direct {p0, v3, v3}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->setWarpState(ZZ)V

    .line 2390
    sget-object v0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;->ENTER_STACK:Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->startAnimation(JLcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;)V

    .line 2393
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mTabModel:Lorg/chromium/chrome/browser/tabmodel/TabModel;

    invoke-interface {v0}, Lorg/chromium/chrome/browser/tabmodel/TabModel;->index()I

    move-result v0

    .line 2395
    iget v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mCurrentMode:I

    if-ne v1, v4, :cond_1

    .line 2396
    neg-int v0, v0

    iget v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mSpacing:I

    mul-int/2addr v0, v1

    int-to-float v0, v0

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mScrollOffset:F

    .line 2402
    :goto_1
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mScrollOffset:F

    invoke-direct {p0, v0, v4}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->setScrollTarget(FZ)V

    .line 2405
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mEvenOutProgress:F

    .line 2408
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mScrollOffset:F

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mSwipeUnboundScrollOffset:F

    .line 2409
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mScrollOffset:F

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mSwipeBoundedScrollOffset:F

    .line 2412
    iput-boolean v3, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mSwipeIsCancelable:Z

    .line 2413
    iput-boolean v3, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mSwipeCanScroll:Z

    .line 2414
    iput-boolean v4, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mInSwipe:Z

    goto :goto_0

    .line 2398
    :cond_1
    neg-int v0, v0

    iget v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mSpacing:I

    mul-int/2addr v0, v1

    int-to-float v0, v0

    add-float/2addr v0, p4

    const/high16 v1, 0x42200000    # 40.0f

    sub-float/2addr v0, v1

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mScrollOffset:F

    .line 2399
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mScrollOffset:F

    invoke-direct {p0, v3}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->getMinScroll(Z)F

    move-result v1

    invoke-direct {p0, v3}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->getMaxScroll(Z)F

    move-result v2

    invoke-static {v0, v1, v2}, Lorg/chromium/chrome/browser/util/MathUtils;->clamp(FFF)F

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mScrollOffset:F

    goto :goto_1
.end method

.method public swipeUpdated(JFFFFFF)V
    .locals 9

    .prologue
    .line 2428
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mInSwipe:Z

    if-nez v0, :cond_1

    .line 2487
    :cond_0
    :goto_0
    return-void

    .line 2430
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mLayout:Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->getHeight()F

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mLayout:Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->getHeightMinusTopControls()F

    move-result v1

    sub-float/2addr v0, v1

    .line 2431
    cmpl-float v0, p8, v0

    if-lez v0, :cond_2

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mSwipeCanScroll:Z

    .line 2432
    :cond_2
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mSwipeCanScroll:Z

    if-eqz v0, :cond_0

    .line 2434
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mTabModel:Lorg/chromium/chrome/browser/tabmodel/TabModel;

    invoke-interface {v0}, Lorg/chromium/chrome/browser/tabmodel/TabModel;->index()I

    move-result v8

    .line 2437
    if-ltz v8, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    array-length v0, v0

    if-lt v8, v0, :cond_4

    .line 2438
    :cond_3
    sget-boolean v0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    const-string/jumbo v1, "Tab index out of bounds in Stack#swipeUpdated()"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 2442
    :cond_4
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mCurrentMode:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_5

    move v0, p6

    .line 2445
    :goto_1
    iget v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mSwipeUnboundScrollOffset:F

    add-float/2addr v0, v1

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mSwipeUnboundScrollOffset:F

    .line 2448
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->getMinScroll(Z)F

    move-result v0

    .line 2449
    const/4 v1, 0x1

    invoke-direct {p0, v1}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->getMaxScroll(Z)F

    move-result v1

    .line 2450
    iget v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mSwipeUnboundScrollOffset:F

    invoke-static {v2, v0, v1}, Lorg/chromium/chrome/browser/util/MathUtils;->clamp(FFF)F

    move-result v0

    .line 2452
    iget v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mSwipeBoundedScrollOffset:F

    sub-float v7, v0, v1

    .line 2453
    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mSwipeBoundedScrollOffset:F

    .line 2455
    const/4 v0, 0x0

    cmpl-float v0, v7, v0

    if-eqz v0, :cond_0

    .line 2457
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mCurrentMode:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_6

    move v6, p5

    :goto_2
    move-object v1, p0

    move-wide v2, p1

    move v4, p3

    move v5, p4

    .line 2464
    invoke-virtual/range {v1 .. v7}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->drag(JFFFF)V

    .line 2467
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mCurrentMode:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_8

    .line 2469
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    aget-object v0, v0, v8

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getScrollOffset()F

    move-result v0

    .line 2470
    iget v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mScrollOffset:F

    neg-float v1, v1

    cmpg-float v0, v1, v0

    if-gez v0, :cond_7

    const/4 v0, 0x1

    .line 2473
    :goto_3
    iget-boolean v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mSwipeIsCancelable:Z

    or-int/2addr v1, v0

    iput-boolean v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mSwipeIsCancelable:Z

    .line 2476
    iget-boolean v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mSwipeIsCancelable:Z

    if-eqz v1, :cond_0

    if-nez v0, :cond_0

    invoke-virtual {p0, p1, p2}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->swipeCancelled(J)V

    goto/16 :goto_0

    :cond_5
    move v0, p5

    .line 2442
    goto :goto_1

    :cond_6
    move v6, v7

    move v7, p6

    .line 2460
    goto :goto_2

    .line 2470
    :cond_7
    const/4 v0, 0x0

    goto :goto_3

    .line 2479
    :cond_8
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    aget-object v0, v0, v8

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getLayoutTab()Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getY()F

    move-result v0

    .line 2481
    const v1, 0x3e428f5c    # 0.19f

    invoke-direct {p0, v1}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->getRange(F)F

    move-result v1

    cmpg-float v0, v0, v1

    if-gez v0, :cond_9

    const/4 v0, 0x1

    .line 2483
    :goto_4
    iget-boolean v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mSwipeIsCancelable:Z

    if-nez v0, :cond_a

    const/4 v1, 0x1

    :goto_5
    or-int/2addr v1, v2

    iput-boolean v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mSwipeIsCancelable:Z

    .line 2485
    iget-boolean v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mSwipeIsCancelable:Z

    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    invoke-virtual {p0, p1, p2}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->swipeCancelled(J)V

    goto/16 :goto_0

    .line 2481
    :cond_9
    const/4 v0, 0x0

    goto :goto_4

    .line 2483
    :cond_a
    const/4 v1, 0x0

    goto :goto_5
.end method

.method public tabClosingEffect(JI)V
    .locals 7

    .prologue
    const/4 v5, 0x1

    const/4 v1, 0x0

    .line 323
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    if-nez v0, :cond_1

    .line 354
    :cond_0
    :goto_0
    return-void

    :cond_1
    move v0, v1

    move v2, v1

    move v3, v1

    .line 330
    :goto_1
    iget-object v4, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    array-length v4, v4

    if-ge v0, v4, :cond_4

    .line 331
    iget-object v4, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    aget-object v4, v4, v0

    invoke-virtual {v4}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getId()I

    move-result v4

    if-ne v4, p3, :cond_3

    .line 335
    iget-object v4, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    aget-object v4, v4, v0

    invoke-virtual {v4}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->isDying()Z

    move-result v4

    if-nez v4, :cond_2

    move v4, v5

    :goto_2
    or-int/2addr v2, v4

    .line 336
    iget-object v4, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    aget-object v4, v4, v0

    invoke-virtual {v4, v5}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->setDying(Z)V

    .line 330
    :goto_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    move v4, v1

    .line 335
    goto :goto_2

    .line 340
    :cond_3
    iget-object v4, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    aget-object v6, v4, v0

    add-int/lit8 v4, v3, 0x1

    invoke-virtual {v6, v3}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->setNewIndex(I)V

    move v3, v4

    goto :goto_3

    .line 344
    :cond_4
    if-eqz v2, :cond_5

    .line 345
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mScrollOffset:F

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mScrollOffsetForDyingTabs:F

    .line 346
    invoke-direct {p0, v3}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->computeSpacing(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mSpacing:I

    .line 348
    sget-object v0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;->DISCARD:Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->startAnimation(JLcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;)V

    .line 351
    :cond_5
    if-nez v3, :cond_0

    .line 352
    iput-boolean v5, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mIsDying:Z

    goto :goto_0
.end method

.method public tabCreated(JI)V
    .locals 1

    .prologue
    .line 403
    invoke-direct {p0, p3}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->createTabHelper(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 408
    :goto_0
    return-void

    .line 405
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mIsDying:Z

    .line 406
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->finishAnimation(J)V

    .line 407
    sget-object v0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;->NEW_TAB_OPENED:Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->startAnimation(JLcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;)V

    goto :goto_0
.end method

.method public tabSelectingEffect(JI)V
    .locals 9

    .prologue
    .line 417
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mTabModel:Lorg/chromium/chrome/browser/tabmodel/TabModel;

    invoke-static {v0, p3}, Lorg/chromium/chrome/browser/tabmodel/TabModelUtils;->getTabIndexById(Lorg/chromium/chrome/browser/tabmodel/TabList;I)I

    move-result v5

    .line 418
    sget-object v4, Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;->TAB_FOCUSED:Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;

    const/4 v6, -0x1

    const/4 v7, 0x0

    move-object v1, p0

    move-wide v2, p1

    invoke-direct/range {v1 .. v7}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->startAnimation(JLcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;IIZ)V

    .line 419
    return-void
.end method

.method public tabsAllClosingEffect(J)V
    .locals 7

    .prologue
    const/4 v5, 0x0

    const/4 v1, 0x0

    const/4 v4, 0x1

    .line 362
    .line 364
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    if-eqz v0, :cond_1

    move v0, v1

    move v2, v1

    .line 365
    :goto_0
    iget-object v3, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    array-length v3, v3

    if-ge v0, v3, :cond_2

    .line 366
    iget-object v3, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    aget-object v3, v3, v0

    invoke-virtual {v3}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->isDying()Z

    move-result v3

    if-nez v3, :cond_0

    move v3, v4

    :goto_1
    or-int/2addr v2, v3

    .line 367
    iget-object v3, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    aget-object v3, v3, v0

    invoke-virtual {v3, v4}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->setDying(Z)V

    .line 365
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    move v3, v1

    .line 366
    goto :goto_1

    :cond_1
    move v2, v4

    .line 375
    :cond_2
    if-eqz v2, :cond_7

    .line 376
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mScrollOffset:F

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mScrollOffsetForDyingTabs:F

    .line 377
    invoke-direct {p0, v1}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->computeSpacing(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mSpacing:I

    .line 379
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    if-eqz v0, :cond_6

    .line 380
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mCurrentMode:I

    if-ne v0, v4, :cond_3

    move v0, v4

    :goto_2
    invoke-static {}, Lorg/chromium/ui/base/LocalizationUtils;->isLayoutRtl()Z

    move-result v2

    xor-int/2addr v0, v2

    if-nez v0, :cond_4

    move v0, v4

    .line 382
    :goto_3
    iget-object v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    array-length v2, v2

    if-ge v1, v2, :cond_6

    .line 383
    iget-object v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    aget-object v3, v2, v1

    .line 384
    invoke-virtual {v3, v5}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->setDiscardOriginY(F)V

    .line 385
    if-eqz v0, :cond_5

    move v2, v5

    :goto_4
    invoke-virtual {v3, v2}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->setDiscardOriginX(F)V

    .line 387
    invoke-virtual {v3, v4}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->setDiscardFromClick(Z)V

    .line 382
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    :cond_3
    move v0, v1

    .line 380
    goto :goto_2

    :cond_4
    move v0, v1

    goto :goto_3

    .line 385
    :cond_5
    invoke-virtual {v3}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getLayoutTab()Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getOriginalContentWidth()F

    move-result v2

    goto :goto_4

    .line 390
    :cond_6
    sget-object v0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;->DISCARD_ALL:Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->startAnimation(JLcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;)V

    .line 393
    :cond_7
    iput-boolean v4, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mIsDying:Z

    .line 394
    return-void
.end method

.method public undoClosure(JI)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 1944
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->createStackTabs(Z)V

    .line 1945
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    if-nez v0, :cond_0

    .line 1959
    :goto_0
    return-void

    :cond_0
    move v0, v1

    .line 1947
    :goto_1
    iget-object v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    array-length v2, v2

    if-ge v0, v2, :cond_2

    .line 1948
    iget-object v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    aget-object v2, v2, v0

    .line 1950
    invoke-virtual {v2}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getId()I

    move-result v3

    if-ne v3, p3, :cond_1

    .line 1951
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->getDiscardRange()F

    move-result v3

    invoke-virtual {v2, v3}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->setDiscardAmount(F)V

    .line 1952
    invoke-virtual {v2, v1}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->setDying(Z)V

    .line 1953
    invoke-virtual {v2}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getLayoutTab()Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mLayout:Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;

    invoke-virtual {v3}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->getHeightMinusTopControls()F

    move-result v3

    invoke-virtual {v2, v3}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->setMaxContentHeight(F)V

    .line 1947
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1957
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mStackTabs:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    array-length v0, v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->computeSpacing(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->mSpacing:I

    .line 1958
    sget-object v0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;->UNDISCARD:Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->startAnimation(JLcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;)V

    goto :goto_0
.end method
