.class public Lcom/google/android/apps/chrome/compositor/resources/dynamics/ViewResourceAdapter;
.super Ljava/lang/Object;
.source "ViewResourceAdapter.java"

# interfaces
.implements Landroid/view/View$OnLayoutChangeListener;
.implements Lcom/google/android/apps/chrome/compositor/resources/dynamics/DynamicResource;


# instance fields
.field private mBitmap:Landroid/graphics/Bitmap;

.field private mBitmapSize:Landroid/graphics/Rect;

.field private final mContentAperture:Landroid/graphics/Rect;

.field private final mContentPadding:Landroid/graphics/Rect;

.field private final mDirtyRect:Landroid/graphics/Rect;

.field private final mView:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/compositor/resources/dynamics/ViewResourceAdapter;->mDirtyRect:Landroid/graphics/Rect;

    .line 24
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/compositor/resources/dynamics/ViewResourceAdapter;->mContentPadding:Landroid/graphics/Rect;

    .line 25
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/compositor/resources/dynamics/ViewResourceAdapter;->mContentAperture:Landroid/graphics/Rect;

    .line 28
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/compositor/resources/dynamics/ViewResourceAdapter;->mBitmapSize:Landroid/graphics/Rect;

    .line 35
    iput-object p1, p0, Lcom/google/android/apps/chrome/compositor/resources/dynamics/ViewResourceAdapter;->mView:Landroid/view/View;

    .line 36
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/resources/dynamics/ViewResourceAdapter;->mView:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->addOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    .line 37
    return-void
.end method

.method private validateBitmap()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 147
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/resources/dynamics/ViewResourceAdapter;->mBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/resources/dynamics/ViewResourceAdapter;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/compositor/resources/dynamics/ViewResourceAdapter;->mView:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getWidth()I

    move-result v1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/resources/dynamics/ViewResourceAdapter;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/compositor/resources/dynamics/ViewResourceAdapter;->mView:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    move-result v1

    if-eq v0, v1, :cond_1

    .line 150
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/resources/dynamics/ViewResourceAdapter;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 151
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/chrome/compositor/resources/dynamics/ViewResourceAdapter;->mBitmap:Landroid/graphics/Bitmap;

    .line 154
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/resources/dynamics/ViewResourceAdapter;->mView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/resources/dynamics/ViewResourceAdapter;->mView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v0

    if-nez v0, :cond_3

    .line 163
    :cond_2
    :goto_0
    return-void

    .line 156
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/resources/dynamics/ViewResourceAdapter;->mBitmap:Landroid/graphics/Bitmap;

    if-nez v0, :cond_2

    .line 157
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/resources/dynamics/ViewResourceAdapter;->mView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/compositor/resources/dynamics/ViewResourceAdapter;->mView:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    move-result v1

    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, v1, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/compositor/resources/dynamics/ViewResourceAdapter;->mBitmap:Landroid/graphics/Bitmap;

    .line 159
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/resources/dynamics/ViewResourceAdapter;->mBitmap:Landroid/graphics/Bitmap;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Bitmap;->setHasAlpha(Z)V

    .line 160
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/resources/dynamics/ViewResourceAdapter;->mDirtyRect:Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/google/android/apps/chrome/compositor/resources/dynamics/ViewResourceAdapter;->mView:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getWidth()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/apps/chrome/compositor/resources/dynamics/ViewResourceAdapter;->mView:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getHeight()I

    move-result v2

    invoke-virtual {v0, v3, v3, v1, v2}, Landroid/graphics/Rect;->set(IIII)V

    .line 161
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/resources/dynamics/ViewResourceAdapter;->mBitmapSize:Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/google/android/apps/chrome/compositor/resources/dynamics/ViewResourceAdapter;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/apps/chrome/compositor/resources/dynamics/ViewResourceAdapter;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    invoke-virtual {v0, v3, v3, v1, v2}, Landroid/graphics/Rect;->set(IIII)V

    goto :goto_0
.end method


# virtual methods
.method protected computeContentAperture(Landroid/graphics/Rect;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 143
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/resources/dynamics/ViewResourceAdapter;->mView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/compositor/resources/dynamics/ViewResourceAdapter;->mView:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    move-result v1

    invoke-virtual {p1, v2, v2, v0, v1}, Landroid/graphics/Rect;->set(IIII)V

    .line 144
    return-void
.end method

.method protected computeContentPadding(Landroid/graphics/Rect;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 135
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/resources/dynamics/ViewResourceAdapter;->mView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/compositor/resources/dynamics/ViewResourceAdapter;->mView:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    move-result v1

    invoke-virtual {p1, v2, v2, v0, v1}, Landroid/graphics/Rect;->set(IIII)V

    .line 136
    return-void
.end method

.method public getAperture()Landroid/graphics/Rect;
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/resources/dynamics/ViewResourceAdapter;->mContentAperture:Landroid/graphics/Rect;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/compositor/resources/dynamics/ViewResourceAdapter;->computeContentAperture(Landroid/graphics/Rect;)V

    .line 82
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/resources/dynamics/ViewResourceAdapter;->mContentAperture:Landroid/graphics/Rect;

    return-object v0
.end method

.method public getBitmap()Landroid/graphics/Bitmap;
    .locals 2

    .prologue
    .line 48
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/compositor/resources/dynamics/ViewResourceAdapter;->isDirty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/resources/dynamics/ViewResourceAdapter;->mBitmap:Landroid/graphics/Bitmap;

    .line 63
    :goto_0
    return-object v0

    .line 49
    :cond_0
    const-string/jumbo v0, "ViewResourceAdapter:getBitmap"

    invoke-static {v0}, Lorg/chromium/base/TraceEvent;->begin(Ljava/lang/String;)V

    .line 50
    invoke-direct {p0}, Lcom/google/android/apps/chrome/compositor/resources/dynamics/ViewResourceAdapter;->validateBitmap()V

    .line 52
    new-instance v1, Landroid/graphics/Canvas;

    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/resources/dynamics/ViewResourceAdapter;->mBitmap:Landroid/graphics/Bitmap;

    invoke-direct {v1, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 54
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/resources/dynamics/ViewResourceAdapter;->mDirtyRect:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x0

    :goto_1
    invoke-virtual {p0, v1, v0}, Lcom/google/android/apps/chrome/compositor/resources/dynamics/ViewResourceAdapter;->onCaptureStart(Landroid/graphics/Canvas;Landroid/graphics/Rect;)V

    .line 56
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/resources/dynamics/ViewResourceAdapter;->mDirtyRect:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/resources/dynamics/ViewResourceAdapter;->mDirtyRect:Landroid/graphics/Rect;

    invoke-virtual {v1, v0}, Landroid/graphics/Canvas;->clipRect(Landroid/graphics/Rect;)Z

    .line 57
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/resources/dynamics/ViewResourceAdapter;->mView:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->draw(Landroid/graphics/Canvas;)V

    .line 59
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/compositor/resources/dynamics/ViewResourceAdapter;->onCaptureEnd()V

    .line 61
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/resources/dynamics/ViewResourceAdapter;->mDirtyRect:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->setEmpty()V

    .line 62
    const-string/jumbo v0, "ViewResourceAdapter:getBitmap"

    invoke-static {v0}, Lorg/chromium/base/TraceEvent;->end(Ljava/lang/String;)V

    .line 63
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/resources/dynamics/ViewResourceAdapter;->mBitmap:Landroid/graphics/Bitmap;

    goto :goto_0

    .line 54
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/resources/dynamics/ViewResourceAdapter;->mDirtyRect:Landroid/graphics/Rect;

    goto :goto_1
.end method

.method public getBitmapSize()Landroid/graphics/Rect;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/resources/dynamics/ViewResourceAdapter;->mBitmapSize:Landroid/graphics/Rect;

    return-object v0
.end method

.method public getPadding()Landroid/graphics/Rect;
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/resources/dynamics/ViewResourceAdapter;->mContentPadding:Landroid/graphics/Rect;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/compositor/resources/dynamics/ViewResourceAdapter;->computeContentPadding(Landroid/graphics/Rect;)V

    .line 75
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/resources/dynamics/ViewResourceAdapter;->mContentPadding:Landroid/graphics/Rect;

    return-object v0
.end method

.method public invalidate(Landroid/graphics/Rect;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 109
    if-nez p1, :cond_0

    .line 110
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/resources/dynamics/ViewResourceAdapter;->mDirtyRect:Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/google/android/apps/chrome/compositor/resources/dynamics/ViewResourceAdapter;->mView:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getWidth()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/apps/chrome/compositor/resources/dynamics/ViewResourceAdapter;->mView:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getHeight()I

    move-result v2

    invoke-virtual {v0, v3, v3, v1, v2}, Landroid/graphics/Rect;->set(IIII)V

    .line 114
    :goto_0
    return-void

    .line 112
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/resources/dynamics/ViewResourceAdapter;->mDirtyRect:Landroid/graphics/Rect;

    invoke-virtual {v0, p1}, Landroid/graphics/Rect;->union(Landroid/graphics/Rect;)V

    goto :goto_0
.end method

.method public isDirty()Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 87
    iget-object v1, p0, Lcom/google/android/apps/chrome/compositor/resources/dynamics/ViewResourceAdapter;->mBitmap:Landroid/graphics/Bitmap;

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/chrome/compositor/resources/dynamics/ViewResourceAdapter;->mDirtyRect:Landroid/graphics/Rect;

    iget-object v2, p0, Lcom/google/android/apps/chrome/compositor/resources/dynamics/ViewResourceAdapter;->mView:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getWidth()I

    move-result v2

    iget-object v3, p0, Lcom/google/android/apps/chrome/compositor/resources/dynamics/ViewResourceAdapter;->mView:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getHeight()I

    move-result v3

    invoke-virtual {v1, v0, v0, v2, v3}, Landroid/graphics/Rect;->set(IIII)V

    .line 89
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/chrome/compositor/resources/dynamics/ViewResourceAdapter;->mDirtyRect:Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/graphics/Rect;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    const/4 v0, 0x1

    :cond_1
    return v0
.end method

.method protected onCaptureEnd()V
    .locals 0

    .prologue
    .line 128
    return-void
.end method

.method protected onCaptureStart(Landroid/graphics/Canvas;Landroid/graphics/Rect;)V
    .locals 0

    .prologue
    .line 122
    return-void
.end method

.method public onLayoutChange(Landroid/view/View;IIIIIIII)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 95
    sub-int v0, p4, p2

    .line 96
    sub-int v1, p5, p3

    .line 97
    sub-int v2, p8, p6

    .line 98
    sub-int v3, p9, p7

    .line 100
    if-ne v0, v2, :cond_0

    if-eq v1, v3, :cond_1

    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/chrome/compositor/resources/dynamics/ViewResourceAdapter;->mDirtyRect:Landroid/graphics/Rect;

    invoke-virtual {v2, v4, v4, v0, v1}, Landroid/graphics/Rect;->set(IIII)V

    .line 101
    :cond_1
    return-void
.end method
