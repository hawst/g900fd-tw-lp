.class Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$TabViewHandlerImpl$1;
.super Ljava/lang/Object;
.source "TabStrip.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic this$1:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$TabViewHandlerImpl;

.field final synthetic val$tabView:Lcom/google/android/apps/chrome/widget/tabstrip/TabView;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$TabViewHandlerImpl;Lcom/google/android/apps/chrome/widget/tabstrip/TabView;)V
    .locals 0

    .prologue
    .line 1186
    iput-object p1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$TabViewHandlerImpl$1;->this$1:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$TabViewHandlerImpl;

    iput-object p2, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$TabViewHandlerImpl$1;->val$tabView:Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 1190
    const-string/jumbo v0, "TabStrip:TabClosed"

    invoke-static {v0}, Lorg/chromium/base/PerfTraceEvent;->begin(Ljava/lang/String;)V

    .line 1191
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$TabViewHandlerImpl$1;->this$1:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$TabViewHandlerImpl;

    iget-object v0, v0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$TabViewHandlerImpl;->this$0:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;

    # getter for: Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mTabViews:Ljava/util/List;
    invoke-static {v0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->access$1100(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ne v0, v3, :cond_0

    .line 1195
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$TabViewHandlerImpl$1;->this$1:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$TabViewHandlerImpl;

    iget-object v0, v0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$TabViewHandlerImpl;->this$0:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;

    # getter for: Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mTabModel:Lorg/chromium/chrome/browser/tabmodel/TabModel;
    invoke-static {v0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->access$1200(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;)Lorg/chromium/chrome/browser/tabmodel/TabModel;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$TabViewHandlerImpl$1;->val$tabView:Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->getTabId()I

    move-result v1

    invoke-static {v0, v1, v3}, Lorg/chromium/chrome/browser/tabmodel/TabModelUtils;->closeTabById(Lorg/chromium/chrome/browser/tabmodel/TabModel;IZ)Z

    .line 1215
    :goto_0
    return-void

    .line 1201
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$TabViewHandlerImpl$1;->val$tabView:Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->close()V

    .line 1203
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$TabViewHandlerImpl$1;->val$tabView:Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$TabViewHandlerImpl$1;->this$1:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$TabViewHandlerImpl;

    iget-object v1, v1, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$TabViewHandlerImpl;->this$0:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;

    # getter for: Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mCurrentlySelectedView:Lcom/google/android/apps/chrome/widget/tabstrip/TabView;
    invoke-static {v1}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->access$1300(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;)Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    move-result-object v1

    if-ne v0, v1, :cond_1

    .line 1204
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$TabViewHandlerImpl$1;->this$1:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$TabViewHandlerImpl;

    iget-object v0, v0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$TabViewHandlerImpl;->this$0:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;

    # getter for: Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mTabModel:Lorg/chromium/chrome/browser/tabmodel/TabModel;
    invoke-static {v0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->access$1200(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;)Lorg/chromium/chrome/browser/tabmodel/TabModel;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$TabViewHandlerImpl$1;->val$tabView:Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->getTabId()I

    move-result v1

    invoke-interface {v0, v1}, Lorg/chromium/chrome/browser/tabmodel/TabModel;->getNextTabIfClosed(I)Lorg/chromium/chrome/browser/Tab;

    move-result-object v0

    .line 1205
    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$TabViewHandlerImpl$1;->this$1:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$TabViewHandlerImpl;

    iget-object v1, v1, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$TabViewHandlerImpl;->this$0:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/Tab;->getId()I

    move-result v0

    const/4 v2, 0x0

    # invokes: Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->tabSelected(IZ)V
    invoke-static {v1, v0, v2}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->access$1400(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;IZ)V

    .line 1208
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$TabViewHandlerImpl$1;->this$1:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$TabViewHandlerImpl;

    iget-object v0, v0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$TabViewHandlerImpl;->this$0:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;

    # getter for: Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mTabViews:Ljava/util/List;
    invoke-static {v0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->access$1100(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;)Ljava/util/List;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$TabViewHandlerImpl$1;->this$1:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$TabViewHandlerImpl;

    iget-object v1, v1, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$TabViewHandlerImpl;->this$0:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;

    # getter for: Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mTabViews:Ljava/util/List;
    invoke-static {v1}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->access$1100(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$TabViewHandlerImpl$1;->val$tabView:Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    if-ne v0, v1, :cond_2

    .line 1209
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$TabViewHandlerImpl$1;->this$1:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$TabViewHandlerImpl;

    iget-object v0, v0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$TabViewHandlerImpl;->this$0:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;

    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$TabViewHandlerImpl$1;->this$1:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$TabViewHandlerImpl;

    iget-object v1, v1, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$TabViewHandlerImpl;->this$0:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;

    # getter for: Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mTabViews:Ljava/util/List;
    invoke-static {v1}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->access$1100(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    # invokes: Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->updateCurrentTabWidth(IZ)V
    invoke-static {v0, v1, v3}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->access$1500(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;IZ)V

    .line 1213
    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$TabViewHandlerImpl$1;->this$1:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$TabViewHandlerImpl;

    iget-object v0, v0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$TabViewHandlerImpl;->this$0:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;

    invoke-static {v0}, Lorg/chromium/base/ApiCompatibilityUtils;->postInvalidateOnAnimation(Landroid/view/View;)V

    goto :goto_0

    .line 1211
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$TabViewHandlerImpl$1;->this$1:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$TabViewHandlerImpl;

    iget-object v0, v0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$TabViewHandlerImpl;->this$0:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;

    # invokes: Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->resetResizeTimeout(Z)V
    invoke-static {v0, v3}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->access$1600(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;Z)V

    goto :goto_1
.end method
