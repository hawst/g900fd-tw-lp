.class public Lcom/google/android/apps/chrome/uma/MemoryUma;
.super Ljava/lang/Object;
.source "MemoryUma.java"


# static fields
.field public static final BACKGROUND_MAX:I = 0x4

.field public static final BACKGROUND_TRIM_BACKGROUND:I = 0x1

.field public static final BACKGROUND_TRIM_COMPLETE:I = 0x3

.field public static final BACKGROUND_TRIM_MODERATE:I = 0x2

.field public static final BACKGROUND_TRIM_UI_HIDDEN:I = 0x0

.field public static final FOREGROUND_LOW:I = 0x3

.field public static final FOREGROUND_MAX:I = 0x4

.field public static final FOREGROUND_TRIM_CRITICAL:I = 0x2

.field public static final FOREGROUND_TRIM_LOW:I = 0x1

.field public static final FOREGROUND_TRIM_MODERATE:I


# instance fields
.field private mLastLowMemoryMsec:J


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/apps/chrome/uma/MemoryUma;->mLastLowMemoryMsec:J

    return-void
.end method


# virtual methods
.method public onLowMemory()V
    .locals 6

    .prologue
    .line 43
    const/4 v0, 0x3

    invoke-static {v0}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->memoryNotificationForeground(I)V

    .line 44
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    .line 45
    iget-wide v2, p0, Lcom/google/android/apps/chrome/uma/MemoryUma;->mLastLowMemoryMsec:J

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-ltz v2, :cond_0

    .line 46
    iget-wide v2, p0, Lcom/google/android/apps/chrome/uma/MemoryUma;->mLastLowMemoryMsec:J

    sub-long v2, v0, v2

    invoke-static {v2, v3}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->lowMemoryTimeBetween(J)V

    .line 48
    :cond_0
    iput-wide v0, p0, Lcom/google/android/apps/chrome/uma/MemoryUma;->mLastLowMemoryMsec:J

    .line 49
    return-void
.end method

.method public onStop()V
    .locals 2

    .prologue
    .line 39
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/apps/chrome/uma/MemoryUma;->mLastLowMemoryMsec:J

    .line 40
    return-void
.end method

.method public onTrimMemory(I)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 52
    const/16 v0, 0x50

    if-lt p1, v0, :cond_1

    .line 53
    const/4 v0, 0x3

    invoke-static {v0}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->memoryNotificationBackground(I)V

    .line 67
    :cond_0
    :goto_0
    return-void

    .line 54
    :cond_1
    const/16 v0, 0x3c

    if-lt p1, v0, :cond_2

    .line 55
    invoke-static {v3}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->memoryNotificationBackground(I)V

    goto :goto_0

    .line 56
    :cond_2
    const/16 v0, 0x28

    if-lt p1, v0, :cond_3

    .line 57
    invoke-static {v2}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->memoryNotificationBackground(I)V

    goto :goto_0

    .line 58
    :cond_3
    const/16 v0, 0x14

    if-lt p1, v0, :cond_4

    .line 59
    invoke-static {v1}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->memoryNotificationBackground(I)V

    goto :goto_0

    .line 60
    :cond_4
    const/16 v0, 0xf

    if-lt p1, v0, :cond_5

    .line 61
    invoke-static {v3}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->memoryNotificationForeground(I)V

    goto :goto_0

    .line 62
    :cond_5
    const/16 v0, 0xa

    if-lt p1, v0, :cond_6

    .line 63
    invoke-static {v2}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->memoryNotificationForeground(I)V

    goto :goto_0

    .line 64
    :cond_6
    const/4 v0, 0x5

    if-lt p1, v0, :cond_0

    .line 65
    invoke-static {v1}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->memoryNotificationForeground(I)V

    goto :goto_0
.end method
