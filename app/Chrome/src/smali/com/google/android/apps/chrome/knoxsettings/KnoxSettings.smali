.class public Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings;
.super Ljava/lang/Object;
.source "KnoxSettings.java"


# static fields
.field static final synthetic $assertionsDisabled:Z

.field private static sKnoxSettings:Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings;


# instance fields
.field private final mContext:Landroid/content/Context;

.field private final mObserverHandle:Lcom/google/android/apps/chrome/knoxsettings/KnoxSettingsProvider$ObserverHandle;

.field private final mObservers:Lorg/chromium/base/ObserverList;

.field private mSettings:Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;

.field private final mSettingsUpdater:Lcom/google/android/apps/chrome/knoxsettings/KnoxSettingsProvider$Observer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 25
    const-class v0, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 424
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    new-instance v0, Lorg/chromium/base/ObserverList;

    invoke-direct {v0}, Lorg/chromium/base/ObserverList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings;->mObservers:Lorg/chromium/base/ObserverList;

    .line 45
    new-instance v0, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$1;-><init>(Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings;->mSettingsUpdater:Lcom/google/android/apps/chrome/knoxsettings/KnoxSettingsProvider$Observer;

    .line 425
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings;->mContext:Landroid/content/Context;

    .line 426
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings;->refreshSettings()V

    .line 427
    iget-object v0, p0, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings;->mSettingsUpdater:Lcom/google/android/apps/chrome/knoxsettings/KnoxSettingsProvider$Observer;

    invoke-static {p1, v0}, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettingsProvider;->addObserver(Landroid/content/Context;Lcom/google/android/apps/chrome/knoxsettings/KnoxSettingsProvider$Observer;)Lcom/google/android/apps/chrome/knoxsettings/KnoxSettingsProvider$ObserverHandle;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings;->mObserverHandle:Lcom/google/android/apps/chrome/knoxsettings/KnoxSettingsProvider$ObserverHandle;

    .line 428
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings;Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;)V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings;->updateSettings(Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;)V

    return-void
.end method

.method static synthetic access$1200(Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings;
    .locals 1

    .prologue
    .line 417
    sget-boolean v0, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    invoke-static {}, Lorg/chromium/base/ThreadUtils;->runningOnUiThread()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 418
    :cond_0
    sget-object v0, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings;->sKnoxSettings:Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings;

    if-nez v0, :cond_1

    .line 419
    new-instance v0, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings;->sKnoxSettings:Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings;

    .line 421
    :cond_1
    sget-object v0, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings;->sKnoxSettings:Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings;

    return-object v0
.end method

.method private notifyObservers()V
    .locals 3

    .prologue
    .line 465
    iget-object v0, p0, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings;->mObservers:Lorg/chromium/base/ObserverList;

    invoke-virtual {v0}, Lorg/chromium/base/ObserverList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Observer;

    .line 466
    iget-object v2, p0, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings;->mSettings:Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;

    invoke-interface {v0, v2}, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Observer;->onSettingsAvailable(Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;)V

    goto :goto_0

    .line 468
    :cond_0
    return-void
.end method

.method private updateSettings(Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;)V
    .locals 0

    .prologue
    .line 460
    iput-object p1, p0, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings;->mSettings:Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;

    .line 461
    invoke-direct {p0}, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings;->notifyObservers()V

    .line 462
    return-void
.end method


# virtual methods
.method public addObserver(Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Observer;)V
    .locals 1

    .prologue
    .line 450
    iget-object v0, p0, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings;->mObservers:Lorg/chromium/base/ObserverList;

    invoke-virtual {v0, p1}, Lorg/chromium/base/ObserverList;->addObserver(Ljava/lang/Object;)Z

    .line 451
    iget-object v0, p0, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings;->mSettings:Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;

    if-eqz v0, :cond_0

    .line 452
    iget-object v0, p0, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings;->mSettings:Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;

    invoke-interface {p1, v0}, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Observer;->onSettingsAvailable(Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;)V

    .line 453
    :cond_0
    return-void
.end method

.method public destroy()V
    .locals 2

    .prologue
    .line 431
    iget-object v0, p0, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings;->mObserverHandle:Lcom/google/android/apps/chrome/knoxsettings/KnoxSettingsProvider$ObserverHandle;

    if-eqz v0, :cond_0

    .line 432
    iget-object v0, p0, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings;->mObserverHandle:Lcom/google/android/apps/chrome/knoxsettings/KnoxSettingsProvider$ObserverHandle;

    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettingsProvider;->removeObserver(Landroid/content/Context;Lcom/google/android/apps/chrome/knoxsettings/KnoxSettingsProvider$ObserverHandle;)V

    .line 434
    :cond_0
    return-void
.end method

.method protected postUpdateSettingsTask(Landroid/os/AsyncTask;)V
    .locals 4

    .prologue
    .line 446
    sget-object v0, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings;->mSettings:Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;

    aput-object v3, v1, v2

    invoke-virtual {p1, v0, v1}, Landroid/os/AsyncTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 447
    return-void
.end method

.method public refreshSettings()V
    .locals 1

    .prologue
    .line 437
    new-instance v0, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$2;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$2;-><init>(Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings;)V

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings;->postUpdateSettingsTask(Landroid/os/AsyncTask;)V

    .line 443
    return-void
.end method

.method public removeObserver(Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Observer;)V
    .locals 1

    .prologue
    .line 456
    iget-object v0, p0, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings;->mObservers:Lorg/chromium/base/ObserverList;

    invoke-virtual {v0, p1}, Lorg/chromium/base/ObserverList;->removeObserver(Ljava/lang/Object;)Z

    .line 457
    return-void
.end method
