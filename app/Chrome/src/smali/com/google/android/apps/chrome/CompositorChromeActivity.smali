.class public abstract Lcom/google/android/apps/chrome/CompositorChromeActivity;
.super Lcom/google/android/apps/chrome/ChromeActivity;
.source "CompositorChromeActivity.java"

# interfaces
.implements Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager$ContextualSearchTabPromotionDelegate;


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private mCompositorViewHolder:Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;

.field private mContextualSearchManager:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;

.field private mFullscreenManager:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;

.field private mWindowAndroid:Lorg/chromium/ui/base/WindowAndroid;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 41
    const-class v0, Lcom/google/android/apps/chrome/CompositorChromeActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/CompositorChromeActivity;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 41
    invoke-direct {p0}, Lcom/google/android/apps/chrome/ChromeActivity;-><init>()V

    return-void
.end method

.method private setWindowFlags()V
    .locals 4

    .prologue
    const/high16 v3, 0x1000000

    const/4 v0, 0x0

    .line 257
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    const-string/jumbo v2, "hardware_acceleration"

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {}, Lorg/chromium/base/CommandLine;->getInstance()Lorg/chromium/base/CommandLine;

    move-result-object v1

    const-string/jumbo v2, "hardware-acceleration"

    invoke-virtual {v1, v2}, Lorg/chromium/base/CommandLine;->hasSwitch(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 266
    :cond_1
    sget-boolean v1, Lcom/google/android/apps/chrome/CompositorChromeActivity;->$assertionsDisabled:Z

    if-nez v1, :cond_2

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/CompositorChromeActivity;->getCompositorViewHolder()Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/CompositorChromeActivity;->getCompositorViewHolder()Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->getChildCount()I

    move-result v1

    if-eqz v1, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 268
    :cond_2
    if-eqz v0, :cond_3

    .line 269
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/CompositorChromeActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v3, v3}, Landroid/view/Window;->setFlags(II)V

    .line 273
    :cond_3
    return-void
.end method


# virtual methods
.method protected buildCompositorViewHolder()Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;
    .locals 1

    .prologue
    .line 178
    new-instance v0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method public createContextualSearchTab(Lorg/chromium/content/browser/ContentViewCore;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 277
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/CompositorChromeActivity;->getCurrentTab()Lcom/google/android/apps/chrome/tab/ChromeTab;

    move-result-object v1

    .line 278
    if-nez v1, :cond_1

    .line 285
    :cond_0
    :goto_0
    return v0

    .line 280
    :cond_1
    invoke-virtual {v1}, Lorg/chromium/chrome/browser/Tab;->isIncognito()Z

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/android/apps/chrome/CompositorChromeActivity;->getTabCreator(Z)Lcom/google/android/apps/chrome/tabmodel/ChromeTabCreator;

    move-result-object v2

    .line 281
    if-eqz v2, :cond_0

    .line 283
    invoke-virtual {v1}, Lorg/chromium/chrome/browser/Tab;->getId()I

    move-result v0

    sget-object v1, Lorg/chromium/chrome/browser/tabmodel/TabModel$TabLaunchType;->FROM_LONGPRESS_FOREGROUND:Lorg/chromium/chrome/browser/tabmodel/TabModel$TabLaunchType;

    invoke-virtual {v2, p1, v0, v1}, Lcom/google/android/apps/chrome/tabmodel/ChromeTabCreator;->createTabWithContentViewCore(Lorg/chromium/content/browser/ContentViewCore;ILorg/chromium/chrome/browser/tabmodel/TabModel$TabLaunchType;)Lcom/google/android/apps/chrome/tab/ChromeTab;

    .line 285
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public getCompositorViewHolder()Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;
    .locals 1

    .prologue
    .line 146
    iget-object v0, p0, Lcom/google/android/apps/chrome/CompositorChromeActivity;->mCompositorViewHolder:Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;

    return-object v0
.end method

.method public getContentOffsetProvider()Lcom/google/android/apps/chrome/compositor/ContentOffsetProvider;
    .locals 1

    .prologue
    .line 156
    iget-object v0, p0, Lcom/google/android/apps/chrome/CompositorChromeActivity;->mCompositorViewHolder:Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->getContentOffsetProvider()Lcom/google/android/apps/chrome/compositor/ContentOffsetProvider;

    move-result-object v0

    return-object v0
.end method

.method public getContentReadbackHandler()Lorg/chromium/content/browser/ContentReadbackHandler;
    .locals 1

    .prologue
    .line 161
    iget-object v0, p0, Lcom/google/android/apps/chrome/CompositorChromeActivity;->mCompositorViewHolder:Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->getContentReadbackHandler()Lorg/chromium/content/browser/ContentReadbackHandler;

    move-result-object v0

    return-object v0
.end method

.method public getContextualSearchManager()Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;
    .locals 1

    .prologue
    .line 169
    iget-object v0, p0, Lcom/google/android/apps/chrome/CompositorChromeActivity;->mContextualSearchManager:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;

    return-object v0
.end method

.method public getFullscreenManager()Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;
    .locals 1

    .prologue
    .line 151
    iget-object v0, p0, Lcom/google/android/apps/chrome/CompositorChromeActivity;->mFullscreenManager:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;

    return-object v0
.end method

.method public bridge synthetic getFullscreenManager()Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;
    .locals 1

    .prologue
    .line 41
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/CompositorChromeActivity;->getFullscreenManager()Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;

    move-result-object v0

    return-object v0
.end method

.method public getWindowAndroid()Lorg/chromium/ui/base/WindowAndroid;
    .locals 1

    .prologue
    .line 139
    iget-object v0, p0, Lcom/google/android/apps/chrome/CompositorChromeActivity;->mWindowAndroid:Lorg/chromium/ui/base/WindowAndroid;

    return-object v0
.end method

.method protected handleBackPressed()Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 230
    iget-object v2, p0, Lcom/google/android/apps/chrome/CompositorChromeActivity;->mCompositorViewHolder:Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;

    if-nez v2, :cond_1

    .line 240
    :cond_0
    :goto_0
    return v0

    .line 232
    :cond_1
    iget-object v2, p0, Lcom/google/android/apps/chrome/CompositorChromeActivity;->mCompositorViewHolder:Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->getLayoutManager()Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerDocument;

    move-result-object v2

    .line 233
    if-eqz v2, :cond_3

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerDocument;->onBackPressed()Z

    move-result v2

    if-eqz v2, :cond_3

    move v2, v1

    .line 234
    :goto_1
    if-nez v2, :cond_2

    iget-object v2, p0, Lcom/google/android/apps/chrome/CompositorChromeActivity;->mContextualSearchManager:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/chrome/CompositorChromeActivity;->mContextualSearchManager:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->onBackPressed()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 236
    :cond_2
    invoke-static {}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->systemBack()V

    move v0, v1

    .line 237
    goto :goto_0

    :cond_3
    move v2, v0

    .line 233
    goto :goto_1
.end method

.method public initializeCompositor()V
    .locals 4

    .prologue
    .line 69
    const-string/jumbo v0, "CompositorChromeActivity:CompositorInitialization"

    invoke-static {v0}, Lorg/chromium/base/TraceEvent;->begin(Ljava/lang/String;)V

    .line 70
    invoke-super {p0}, Lcom/google/android/apps/chrome/ChromeActivity;->initializeCompositor()V

    .line 72
    new-instance v0, Lcom/google/android/apps/chrome/compositor/TabContentManager;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/CompositorChromeActivity;->getContentOffsetProvider()Lcom/google/android/apps/chrome/compositor/ContentOffsetProvider;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/chrome/compositor/TabContentManager;-><init>(Landroid/content/Context;Lcom/google/android/apps/chrome/compositor/ContentOffsetProvider;)V

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/CompositorChromeActivity;->setTabContentManager(Lcom/google/android/apps/chrome/compositor/TabContentManager;)V

    .line 73
    iget-object v0, p0, Lcom/google/android/apps/chrome/CompositorChromeActivity;->mCompositorViewHolder:Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;

    new-instance v1, Lcom/google/android/apps/chrome/WindowDelegate;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/CompositorChromeActivity;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/google/android/apps/chrome/WindowDelegate;-><init>(Landroid/view/Window;)V

    iget-object v2, p0, Lcom/google/android/apps/chrome/CompositorChromeActivity;->mWindowAndroid:Lorg/chromium/ui/base/WindowAndroid;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/CompositorChromeActivity;->getTabContentManager()Lcom/google/android/apps/chrome/compositor/TabContentManager;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->onNativeLibraryReady(Lcom/google/android/apps/chrome/WindowDelegate;Lorg/chromium/ui/base/WindowAndroid;Lcom/google/android/apps/chrome/compositor/TabContentManager;)V

    .line 76
    invoke-static {p0}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchFieldTrial;->isEnabled(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p0}, Lorg/chromium/ui/base/DeviceFormFactor;->isTablet(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 77
    new-instance v0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;

    iget-object v1, p0, Lcom/google/android/apps/chrome/CompositorChromeActivity;->mWindowAndroid:Lorg/chromium/ui/base/WindowAndroid;

    invoke-direct {v0, p0, v1, p0}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;-><init>(Lcom/google/android/apps/chrome/ChromeActivity;Lorg/chromium/ui/base/WindowAndroid;Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager$ContextualSearchTabPromotionDelegate;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/CompositorChromeActivity;->mContextualSearchManager:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;

    .line 79
    :cond_0
    const-string/jumbo v0, "CompositorChromeActivity:CompositorInitialization"

    invoke-static {v0}, Lorg/chromium/base/TraceEvent;->end(Ljava/lang/String;)V

    .line 80
    return-void
.end method

.method protected initializeCompositorContent(Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerDocument;Landroid/view/View;Landroid/view/ViewGroup;Lcom/google/android/apps/chrome/widget/ControlContainer;I)V
    .locals 8

    .prologue
    const/4 v4, 0x1

    const/4 v7, 0x0

    .line 198
    invoke-static {}, Lorg/chromium/base/CommandLine;->getInstance()Lorg/chromium/base/CommandLine;

    move-result-object v0

    .line 199
    const-string/jumbo v1, "disable-fullscreen"

    invoke-virtual {v0, v1}, Lorg/chromium/base/CommandLine;->hasSwitch(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    move v3, v4

    .line 201
    :goto_0
    if-eqz v3, :cond_2

    const-string/jumbo v1, "disable-persistent-fullscreen"

    invoke-virtual {v0, v1}, Lorg/chromium/base/CommandLine;->hasSwitch(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 204
    :goto_1
    new-instance v0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/CompositorChromeActivity;->getTabModelSelector()Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

    move-result-object v5

    move-object v1, p0

    move-object v2, p4

    move v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;-><init>(Landroid/app/Activity;Lcom/google/android/apps/chrome/widget/ControlContainer;ZZLorg/chromium/chrome/browser/tabmodel/TabModelSelector;I)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/CompositorChromeActivity;->mFullscreenManager:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;

    .line 207
    iget-object v0, p0, Lcom/google/android/apps/chrome/CompositorChromeActivity;->mContextualSearchManager:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;

    if-eqz v0, :cond_0

    .line 208
    iget-object v0, p0, Lcom/google/android/apps/chrome/CompositorChromeActivity;->mContextualSearchManager:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;

    invoke-virtual {v0, p3}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->initialize(Landroid/view/ViewGroup;)V

    .line 209
    iget-object v0, p0, Lcom/google/android/apps/chrome/CompositorChromeActivity;->mContextualSearchManager:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;

    invoke-virtual {p1}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerDocument;->getContextualSearchEdgeSwipeHandler()Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeHandler;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->setEdgeSwipeHandler(Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeHandler;)V

    .line 211
    iget-object v0, p0, Lcom/google/android/apps/chrome/CompositorChromeActivity;->mContextualSearchManager:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;

    iget-object v1, p0, Lcom/google/android/apps/chrome/CompositorChromeActivity;->mCompositorViewHolder:Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->getDynamicResourceLoader()Lcom/google/android/apps/chrome/compositor/resources/dynamics/DynamicResourceLoader;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->setDynamicResourceLoader(Lcom/google/android/apps/chrome/compositor/resources/dynamics/DynamicResourceLoader;)V

    .line 215
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/CompositorChromeActivity;->mCompositorViewHolder:Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->setLayoutManager(Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerDocument;)V

    .line 216
    iget-object v0, p0, Lcom/google/android/apps/chrome/CompositorChromeActivity;->mCompositorViewHolder:Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;

    invoke-virtual {v0, v7}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->setFocusable(Z)V

    .line 217
    iget-object v0, p0, Lcom/google/android/apps/chrome/CompositorChromeActivity;->mCompositorViewHolder:Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;

    invoke-virtual {v0, p4}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->setControlContainer(Lcom/google/android/apps/chrome/widget/ControlContainer;)V

    .line 218
    iget-object v0, p0, Lcom/google/android/apps/chrome/CompositorChromeActivity;->mCompositorViewHolder:Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;

    iget-object v1, p0, Lcom/google/android/apps/chrome/CompositorChromeActivity;->mFullscreenManager:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->setFullscreenHandler(Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;)V

    .line 219
    iget-object v0, p0, Lcom/google/android/apps/chrome/CompositorChromeActivity;->mCompositorViewHolder:Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;

    invoke-virtual {v0, p2}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->setUrlBar(Landroid/view/View;)V

    .line 220
    iget-object v0, p0, Lcom/google/android/apps/chrome/CompositorChromeActivity;->mCompositorViewHolder:Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/CompositorChromeActivity;->getTabModelSelector()Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/CompositorChromeActivity;->getTabContentManager()Lcom/google/android/apps/chrome/compositor/TabContentManager;

    move-result-object v3

    iget-object v5, p0, Lcom/google/android/apps/chrome/CompositorChromeActivity;->mContextualSearchManager:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;

    move-object v2, p0

    move-object v4, p3

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->onFinishNativeInitialization(Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;Lcom/google/android/apps/chrome/tabmodel/ChromeTabCreatorManager;Lcom/google/android/apps/chrome/compositor/TabContentManager;Landroid/view/ViewGroup;Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;)V

    .line 222
    return-void

    :cond_1
    move v3, v7

    .line 199
    goto :goto_0

    :cond_2
    move v4, v7

    .line 201
    goto :goto_1
.end method

.method public onActivityResultWithNative(IILandroid/content/Intent;)Z
    .locals 1

    .prologue
    .line 125
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/apps/chrome/ChromeActivity;->onActivityResultWithNative(IILandroid/content/Intent;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 126
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/CompositorChromeActivity;->mWindowAndroid:Lorg/chromium/ui/base/WindowAndroid;

    invoke-virtual {v0, p1, p2, p3}, Lorg/chromium/ui/base/WindowAndroid;->onActivityResult(IILandroid/content/Intent;)Z

    move-result v0

    goto :goto_0
.end method

.method public final onBackPressed()V
    .locals 0

    .prologue
    .line 252
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/CompositorChromeActivity;->handleBackPressed()Z

    .line 253
    return-void
.end method

.method protected final onDestroy()V
    .locals 1

    .prologue
    .line 106
    iget-object v0, p0, Lcom/google/android/apps/chrome/CompositorChromeActivity;->mCompositorViewHolder:Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/CompositorChromeActivity;->mCompositorViewHolder:Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->shutDown()V

    .line 107
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/CompositorChromeActivity;->onDestroyInternal()V

    .line 108
    iget-object v0, p0, Lcom/google/android/apps/chrome/CompositorChromeActivity;->mWindowAndroid:Lorg/chromium/ui/base/WindowAndroid;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/CompositorChromeActivity;->mWindowAndroid:Lorg/chromium/ui/base/WindowAndroid;

    invoke-virtual {v0}, Lorg/chromium/ui/base/WindowAndroid;->destroy()V

    .line 109
    :cond_1
    invoke-super {p0}, Lcom/google/android/apps/chrome/ChromeActivity;->onDestroy()V

    .line 110
    return-void
.end method

.method protected onDestroyInternal()V
    .locals 0

    .prologue
    .line 117
    return-void
.end method

.method public onOrientationChange(I)V
    .locals 2

    .prologue
    .line 245
    iget-object v0, p0, Lcom/google/android/apps/chrome/CompositorChromeActivity;->mContextualSearchManager:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;

    if-eqz v0, :cond_0

    .line 246
    iget-object v0, p0, Lcom/google/android/apps/chrome/CompositorChromeActivity;->mContextualSearchManager:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;

    sget-object v1, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;->UNKNOWN:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->hideContextualSearch(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;)V

    .line 248
    :cond_0
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 131
    invoke-super {p0, p1}, Lcom/google/android/apps/chrome/ChromeActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 132
    iget-object v0, p0, Lcom/google/android/apps/chrome/CompositorChromeActivity;->mWindowAndroid:Lorg/chromium/ui/base/WindowAndroid;

    invoke-virtual {v0, p1}, Lorg/chromium/ui/base/WindowAndroid;->saveInstanceState(Landroid/os/Bundle;)V

    .line 133
    return-void
.end method

.method public onStart()V
    .locals 1

    .prologue
    .line 90
    invoke-super {p0}, Lcom/google/android/apps/chrome/ChromeActivity;->onStart()V

    .line 91
    iget-object v0, p0, Lcom/google/android/apps/chrome/CompositorChromeActivity;->mCompositorViewHolder:Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/CompositorChromeActivity;->mCompositorViewHolder:Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->onStart()V

    .line 92
    :cond_0
    return-void
.end method

.method public onStartWithNative()V
    .locals 1

    .prologue
    .line 84
    invoke-super {p0}, Lcom/google/android/apps/chrome/ChromeActivity;->onStartWithNative()V

    .line 85
    iget-object v0, p0, Lcom/google/android/apps/chrome/CompositorChromeActivity;->mCompositorViewHolder:Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->resetFlags()V

    .line 86
    return-void
.end method

.method public onStop()V
    .locals 1

    .prologue
    .line 96
    invoke-super {p0}, Lcom/google/android/apps/chrome/ChromeActivity;->onStop()V

    .line 97
    iget-object v0, p0, Lcom/google/android/apps/chrome/CompositorChromeActivity;->mCompositorViewHolder:Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/CompositorChromeActivity;->mCompositorViewHolder:Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->onStop()V

    .line 98
    :cond_0
    return-void
.end method

.method public postInflationStartup()V
    .locals 2

    .prologue
    .line 50
    new-instance v0, Lcom/google/android/apps/chrome/ChromeWindow;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/ChromeWindow;-><init>(Lcom/google/android/apps/chrome/ChromeActivity;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/CompositorChromeActivity;->mWindowAndroid:Lorg/chromium/ui/base/WindowAndroid;

    .line 51
    iget-object v0, p0, Lcom/google/android/apps/chrome/CompositorChromeActivity;->mWindowAndroid:Lorg/chromium/ui/base/WindowAndroid;

    iget-object v1, p0, Lcom/google/android/apps/chrome/CompositorChromeActivity;->mSavedInstanceState:Landroid/os/Bundle;

    invoke-virtual {v0, v1}, Lorg/chromium/ui/base/WindowAndroid;->restoreInstanceState(Landroid/os/Bundle;)V

    .line 52
    invoke-super {p0}, Lcom/google/android/apps/chrome/ChromeActivity;->postInflationStartup()V

    .line 53
    return-void
.end method

.method protected setContentView()V
    .locals 2

    .prologue
    .line 62
    invoke-direct {p0}, Lcom/google/android/apps/chrome/CompositorChromeActivity;->setWindowFlags()V

    .line 63
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/CompositorChromeActivity;->buildCompositorViewHolder()Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/CompositorChromeActivity;->mCompositorViewHolder:Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;

    .line 64
    iget-object v0, p0, Lcom/google/android/apps/chrome/CompositorChromeActivity;->mCompositorViewHolder:Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/CompositorChromeActivity;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getRootView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->setRootView(Landroid/view/View;)V

    .line 65
    return-void
.end method
