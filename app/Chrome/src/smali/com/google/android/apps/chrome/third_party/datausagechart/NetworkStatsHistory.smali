.class public Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;
.super Ljava/lang/Object;
.source "NetworkStatsHistory.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;

.field public static final FIELD_ACTIVE_TIME:I = 0x1

.field public static final FIELD_ALL:I = -0x1

.field public static final FIELD_OPERATIONS:I = 0x20

.field public static final FIELD_RX_BYTES:I = 0x2

.field public static final FIELD_RX_PACKETS:I = 0x4

.field public static final FIELD_TX_BYTES:I = 0x8

.field public static final FIELD_TX_PACKETS:I = 0x10

.field public static final IFACE_ALL:Ljava/lang/String;

.field public static final SET_DEFAULT:I = 0x0

.field public static final TAG_NONE:I = 0x0

.field public static final UID_ALL:I = -0x1


# instance fields
.field private activeTime:[J

.field private bucketCount:I

.field private bucketDuration:J

.field private bucketStart:[J

.field private operations:[J

.field private rxBytes:[J

.field private rxPackets:[J

.field private totalBytes:J

.field private txBytes:[J

.field private txPackets:[J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 65
    const/4 v0, 0x0

    sput-object v0, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;->IFACE_ALL:Ljava/lang/String;

    .line 609
    new-instance v0, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory$1;

    invoke-direct {v0}, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory$1;-><init>()V

    sput-object v0, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(JII)V
    .locals 3

    .prologue
    .line 102
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 103
    iput-wide p1, p0, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;->bucketDuration:J

    .line 104
    new-array v0, p3, [J

    iput-object v0, p0, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;->bucketStart:[J

    .line 105
    and-int/lit8 v0, p4, 0x1

    if-eqz v0, :cond_0

    new-array v0, p3, [J

    iput-object v0, p0, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;->activeTime:[J

    .line 106
    :cond_0
    and-int/lit8 v0, p4, 0x2

    if-eqz v0, :cond_1

    new-array v0, p3, [J

    iput-object v0, p0, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;->rxBytes:[J

    .line 107
    :cond_1
    and-int/lit8 v0, p4, 0x4

    if-eqz v0, :cond_2

    new-array v0, p3, [J

    iput-object v0, p0, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;->rxPackets:[J

    .line 108
    :cond_2
    and-int/lit8 v0, p4, 0x8

    if-eqz v0, :cond_3

    new-array v0, p3, [J

    iput-object v0, p0, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;->txBytes:[J

    .line 109
    :cond_3
    and-int/lit8 v0, p4, 0x10

    if-eqz v0, :cond_4

    new-array v0, p3, [J

    iput-object v0, p0, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;->txPackets:[J

    .line 110
    :cond_4
    and-int/lit8 v0, p4, 0x20

    if-eqz v0, :cond_5

    new-array v0, p3, [J

    iput-object v0, p0, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;->operations:[J

    .line 111
    :cond_5
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;->bucketCount:I

    .line 112
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;->totalBytes:J

    .line 113
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 120
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 121
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;->bucketDuration:J

    .line 122
    invoke-static {p1}, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory$ParcelUtils;->readLongArray(Landroid/os/Parcel;)[J

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;->bucketStart:[J

    .line 123
    invoke-static {p1}, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory$ParcelUtils;->readLongArray(Landroid/os/Parcel;)[J

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;->activeTime:[J

    .line 124
    invoke-static {p1}, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory$ParcelUtils;->readLongArray(Landroid/os/Parcel;)[J

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;->rxBytes:[J

    .line 125
    invoke-static {p1}, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory$ParcelUtils;->readLongArray(Landroid/os/Parcel;)[J

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;->rxPackets:[J

    .line 126
    invoke-static {p1}, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory$ParcelUtils;->readLongArray(Landroid/os/Parcel;)[J

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;->txBytes:[J

    .line 127
    invoke-static {p1}, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory$ParcelUtils;->readLongArray(Landroid/os/Parcel;)[J

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;->txPackets:[J

    .line 128
    invoke-static {p1}, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory$ParcelUtils;->readLongArray(Landroid/os/Parcel;)[J

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;->operations:[J

    .line 129
    iget-object v0, p0, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;->bucketStart:[J

    array-length v0, v0

    iput v0, p0, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;->bucketCount:I

    .line 130
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;->totalBytes:J

    .line 131
    return-void
.end method

.method private static addLong([JIJ)V
    .locals 2

    .prologue
    .line 630
    if-eqz p0, :cond_0

    aget-wide v0, p0, p1

    add-long/2addr v0, p2

    aput-wide v0, p0, p1

    .line 631
    :cond_0
    return-void
.end method

.method private ensureBuckets(JJ)V
    .locals 7

    .prologue
    .line 389
    iget-wide v0, p0, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;->bucketDuration:J

    rem-long v0, p1, v0

    sub-long v0, p1, v0

    .line 390
    iget-wide v2, p0, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;->bucketDuration:J

    iget-wide v4, p0, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;->bucketDuration:J

    rem-long v4, p3, v4

    sub-long/2addr v2, v4

    iget-wide v4, p0, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;->bucketDuration:J

    rem-long/2addr v2, v4

    add-long/2addr v2, p3

    .line 392
    :goto_0
    cmp-long v4, v0, v2

    if-gez v4, :cond_1

    .line 394
    iget-object v4, p0, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;->bucketStart:[J

    const/4 v5, 0x0

    iget v6, p0, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;->bucketCount:I

    invoke-static {v4, v5, v6, v0, v1}, Ljava/util/Arrays;->binarySearch([JIIJ)I

    move-result v4

    .line 395
    if-gez v4, :cond_0

    .line 397
    xor-int/lit8 v4, v4, -0x1

    invoke-direct {p0, v4, v0, v1}, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;->insertBucket(IJ)V

    .line 392
    :cond_0
    iget-wide v4, p0, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;->bucketDuration:J

    add-long/2addr v0, v4

    goto :goto_0

    .line 400
    :cond_1
    return-void
.end method

.method private static getLong([JIJ)J
    .locals 0

    .prologue
    .line 622
    if-eqz p0, :cond_0

    aget-wide p2, p0, p1

    :cond_0
    return-wide p2
.end method

.method private insertBucket(IJ)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 407
    iget v0, p0, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;->bucketCount:I

    iget-object v1, p0, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;->bucketStart:[J

    array-length v1, v1

    if-lt v0, v1, :cond_5

    .line 408
    iget-object v0, p0, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;->bucketStart:[J

    array-length v0, v0

    const/16 v1, 0xa

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    mul-int/lit8 v0, v0, 0x3

    div-int/lit8 v0, v0, 0x2

    .line 409
    iget-object v1, p0, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;->bucketStart:[J

    invoke-static {v1, v0}, Ljava/util/Arrays;->copyOf([JI)[J

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;->bucketStart:[J

    .line 410
    iget-object v1, p0, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;->activeTime:[J

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;->activeTime:[J

    invoke-static {v1, v0}, Ljava/util/Arrays;->copyOf([JI)[J

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;->activeTime:[J

    .line 411
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;->rxBytes:[J

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;->rxBytes:[J

    invoke-static {v1, v0}, Ljava/util/Arrays;->copyOf([JI)[J

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;->rxBytes:[J

    .line 412
    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;->rxPackets:[J

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;->rxPackets:[J

    invoke-static {v1, v0}, Ljava/util/Arrays;->copyOf([JI)[J

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;->rxPackets:[J

    .line 413
    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;->txBytes:[J

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;->txBytes:[J

    invoke-static {v1, v0}, Ljava/util/Arrays;->copyOf([JI)[J

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;->txBytes:[J

    .line 414
    :cond_3
    iget-object v1, p0, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;->txPackets:[J

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;->txPackets:[J

    invoke-static {v1, v0}, Ljava/util/Arrays;->copyOf([JI)[J

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;->txPackets:[J

    .line 415
    :cond_4
    iget-object v1, p0, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;->operations:[J

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;->operations:[J

    invoke-static {v1, v0}, Ljava/util/Arrays;->copyOf([JI)[J

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;->operations:[J

    .line 419
    :cond_5
    iget v0, p0, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;->bucketCount:I

    if-ge p1, v0, :cond_b

    .line 420
    add-int/lit8 v0, p1, 0x1

    .line 421
    iget v1, p0, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;->bucketCount:I

    sub-int/2addr v1, p1

    .line 423
    iget-object v2, p0, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;->bucketStart:[J

    iget-object v3, p0, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;->bucketStart:[J

    invoke-static {v2, p1, v3, v0, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 424
    iget-object v2, p0, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;->activeTime:[J

    if-eqz v2, :cond_6

    iget-object v2, p0, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;->activeTime:[J

    iget-object v3, p0, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;->activeTime:[J

    invoke-static {v2, p1, v3, v0, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 425
    :cond_6
    iget-object v2, p0, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;->rxBytes:[J

    if-eqz v2, :cond_7

    iget-object v2, p0, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;->rxBytes:[J

    iget-object v3, p0, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;->rxBytes:[J

    invoke-static {v2, p1, v3, v0, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 426
    :cond_7
    iget-object v2, p0, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;->rxPackets:[J

    if-eqz v2, :cond_8

    iget-object v2, p0, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;->rxPackets:[J

    iget-object v3, p0, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;->rxPackets:[J

    invoke-static {v2, p1, v3, v0, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 427
    :cond_8
    iget-object v2, p0, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;->txBytes:[J

    if-eqz v2, :cond_9

    iget-object v2, p0, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;->txBytes:[J

    iget-object v3, p0, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;->txBytes:[J

    invoke-static {v2, p1, v3, v0, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 428
    :cond_9
    iget-object v2, p0, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;->txPackets:[J

    if-eqz v2, :cond_a

    iget-object v2, p0, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;->txPackets:[J

    iget-object v3, p0, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;->txPackets:[J

    invoke-static {v2, p1, v3, v0, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 429
    :cond_a
    iget-object v2, p0, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;->operations:[J

    if-eqz v2, :cond_b

    iget-object v2, p0, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;->operations:[J

    iget-object v3, p0, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;->operations:[J

    invoke-static {v2, p1, v3, v0, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 432
    :cond_b
    iget-object v0, p0, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;->bucketStart:[J

    aput-wide p2, v0, p1

    .line 433
    iget-object v0, p0, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;->activeTime:[J

    invoke-static {v0, p1, v4, v5}, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;->setLong([JIJ)V

    .line 434
    iget-object v0, p0, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;->rxBytes:[J

    invoke-static {v0, p1, v4, v5}, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;->setLong([JIJ)V

    .line 435
    iget-object v0, p0, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;->rxPackets:[J

    invoke-static {v0, p1, v4, v5}, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;->setLong([JIJ)V

    .line 436
    iget-object v0, p0, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;->txBytes:[J

    invoke-static {v0, p1, v4, v5}, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;->setLong([JIJ)V

    .line 437
    iget-object v0, p0, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;->txPackets:[J

    invoke-static {v0, p1, v4, v5}, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;->setLong([JIJ)V

    .line 438
    iget-object v0, p0, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;->operations:[J

    invoke-static {v0, p1, v4, v5}, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;->setLong([JIJ)V

    .line 439
    iget v0, p0, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;->bucketCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;->bucketCount:I

    .line 440
    return-void
.end method

.method private static setLong([JIJ)V
    .locals 0

    .prologue
    .line 626
    if-eqz p0, :cond_0

    aput-wide p2, p0, p1

    .line 627
    :cond_0
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 210
    const/4 v0, 0x0

    return v0
.end method

.method public dump(Ljava/io/PrintWriter;Z)V
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 583
    const-string/jumbo v1, "NetworkStatsHistory: bucketDuration="

    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-wide v2, p0, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;->bucketDuration:J

    invoke-virtual {p1, v2, v3}, Ljava/io/PrintWriter;->println(J)V

    .line 585
    if-eqz p2, :cond_7

    .line 586
    :goto_0
    if-lez v0, :cond_0

    .line 587
    const-string/jumbo v1, "(omitting "

    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(I)V

    const-string/jumbo v1, " buckets)"

    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 590
    :cond_0
    :goto_1
    iget v1, p0, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;->bucketCount:I

    if-ge v0, v1, :cond_8

    .line 591
    const-string/jumbo v1, "bucketStart="

    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;->bucketStart:[J

    aget-wide v2, v1, v0

    invoke-virtual {p1, v2, v3}, Ljava/io/PrintWriter;->print(J)V

    .line 592
    iget-object v1, p0, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;->activeTime:[J

    if-eqz v1, :cond_1

    const-string/jumbo v1, " activeTime="

    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;->activeTime:[J

    aget-wide v2, v1, v0

    invoke-virtual {p1, v2, v3}, Ljava/io/PrintWriter;->print(J)V

    .line 593
    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;->rxBytes:[J

    if-eqz v1, :cond_2

    const-string/jumbo v1, " rxBytes="

    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;->rxBytes:[J

    aget-wide v2, v1, v0

    invoke-virtual {p1, v2, v3}, Ljava/io/PrintWriter;->print(J)V

    .line 594
    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;->rxPackets:[J

    if-eqz v1, :cond_3

    const-string/jumbo v1, " rxPackets="

    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;->rxPackets:[J

    aget-wide v2, v1, v0

    invoke-virtual {p1, v2, v3}, Ljava/io/PrintWriter;->print(J)V

    .line 595
    :cond_3
    iget-object v1, p0, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;->txBytes:[J

    if-eqz v1, :cond_4

    const-string/jumbo v1, " txBytes="

    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;->txBytes:[J

    aget-wide v2, v1, v0

    invoke-virtual {p1, v2, v3}, Ljava/io/PrintWriter;->print(J)V

    .line 596
    :cond_4
    iget-object v1, p0, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;->txPackets:[J

    if-eqz v1, :cond_5

    const-string/jumbo v1, " txPackets="

    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;->txPackets:[J

    aget-wide v2, v1, v0

    invoke-virtual {p1, v2, v3}, Ljava/io/PrintWriter;->print(J)V

    .line 597
    :cond_5
    iget-object v1, p0, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;->operations:[J

    if-eqz v1, :cond_6

    const-string/jumbo v1, " operations="

    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;->operations:[J

    aget-wide v2, v1, v0

    invoke-virtual {p1, v2, v3}, Ljava/io/PrintWriter;->print(J)V

    .line 598
    :cond_6
    invoke-virtual {p1}, Ljava/io/PrintWriter;->println()V

    .line 590
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 585
    :cond_7
    iget v1, p0, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;->bucketCount:I

    add-int/lit8 v1, v1, -0x20

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    goto/16 :goto_0

    .line 600
    :cond_8
    return-void
.end method

.method public getEnd()J
    .locals 4

    .prologue
    .line 230
    iget v0, p0, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;->bucketCount:I

    if-lez v0, :cond_0

    .line 231
    iget-object v0, p0, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;->bucketStart:[J

    iget v1, p0, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;->bucketCount:I

    add-int/lit8 v1, v1, -0x1

    aget-wide v0, v0, v1

    iget-wide v2, p0, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;->bucketDuration:J

    add-long/2addr v0, v2

    .line 233
    :goto_0
    return-wide v0

    :cond_0
    const-wide/high16 v0, -0x8000000000000000L

    goto :goto_0
.end method

.method public getIndexAfter(J)I
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 263
    iget-object v0, p0, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;->bucketStart:[J

    iget v1, p0, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;->bucketCount:I

    invoke-static {v0, v2, v1, p1, p2}, Ljava/util/Arrays;->binarySearch([JIIJ)I

    move-result v0

    .line 264
    if-gez v0, :cond_0

    .line 265
    xor-int/lit8 v0, v0, -0x1

    .line 269
    :goto_0
    iget v1, p0, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;->bucketCount:I

    add-int/lit8 v1, v1, -0x1

    invoke-static {v0, v2, v1}, Lorg/chromium/chrome/browser/util/MathUtils;->clamp(III)I

    move-result v0

    return v0

    .line 267
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public getIndexBefore(J)I
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 249
    iget-object v0, p0, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;->bucketStart:[J

    iget v1, p0, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;->bucketCount:I

    invoke-static {v0, v2, v1, p1, p2}, Ljava/util/Arrays;->binarySearch([JIIJ)I

    move-result v0

    .line 250
    if-gez v0, :cond_0

    .line 251
    xor-int/lit8 v0, v0, -0x1

    add-int/lit8 v0, v0, -0x1

    .line 255
    :goto_0
    iget v1, p0, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;->bucketCount:I

    add-int/lit8 v1, v1, -0x1

    invoke-static {v0, v2, v1}, Lorg/chromium/chrome/browser/util/MathUtils;->clamp(III)I

    move-result v0

    return v0

    .line 253
    :cond_0
    add-int/lit8 v0, v0, -0x1

    goto :goto_0
.end method

.method public getValues(ILcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory$Entry;)Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory$Entry;
    .locals 4

    .prologue
    const-wide/16 v2, -0x1

    .line 276
    if-eqz p2, :cond_0

    .line 277
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;->bucketStart:[J

    aget-wide v0, v0, p1

    iput-wide v0, p2, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory$Entry;->bucketStart:J

    .line 278
    iget-wide v0, p0, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;->bucketDuration:J

    iput-wide v0, p2, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory$Entry;->bucketDuration:J

    .line 279
    iget-object v0, p0, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;->activeTime:[J

    invoke-static {v0, p1, v2, v3}, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;->getLong([JIJ)J

    move-result-wide v0

    iput-wide v0, p2, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory$Entry;->activeTime:J

    .line 280
    iget-object v0, p0, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;->rxBytes:[J

    invoke-static {v0, p1, v2, v3}, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;->getLong([JIJ)J

    move-result-wide v0

    iput-wide v0, p2, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory$Entry;->rxBytes:J

    .line 281
    iget-object v0, p0, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;->rxPackets:[J

    invoke-static {v0, p1, v2, v3}, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;->getLong([JIJ)J

    move-result-wide v0

    iput-wide v0, p2, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory$Entry;->rxPackets:J

    .line 282
    iget-object v0, p0, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;->txBytes:[J

    invoke-static {v0, p1, v2, v3}, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;->getLong([JIJ)J

    move-result-wide v0

    iput-wide v0, p2, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory$Entry;->txBytes:J

    .line 283
    iget-object v0, p0, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;->txPackets:[J

    invoke-static {v0, p1, v2, v3}, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;->getLong([JIJ)J

    move-result-wide v0

    iput-wide v0, p2, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory$Entry;->txPackets:J

    .line 284
    iget-object v0, p0, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;->operations:[J

    invoke-static {v0, p1, v2, v3}, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;->getLong([JIJ)J

    move-result-wide v0

    iput-wide v0, p2, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory$Entry;->operations:J

    .line 285
    return-object p2

    .line 276
    :cond_0
    new-instance p2, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory$Entry;

    invoke-direct {p2}, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory$Entry;-><init>()V

    goto :goto_0
.end method

.method public getValues(JJJLcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory$Entry;)Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory$Entry;
    .locals 13

    .prologue
    .line 485
    if-eqz p7, :cond_6

    .line 486
    :goto_0
    sub-long v2, p3, p1

    move-object/from16 v0, p7

    iput-wide v2, v0, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory$Entry;->bucketDuration:J

    .line 487
    move-object/from16 v0, p7

    iput-wide p1, v0, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory$Entry;->bucketStart:J

    .line 488
    iget-object v2, p0, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;->activeTime:[J

    if-eqz v2, :cond_7

    const-wide/16 v2, 0x0

    :goto_1
    move-object/from16 v0, p7

    iput-wide v2, v0, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory$Entry;->activeTime:J

    .line 489
    iget-object v2, p0, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;->rxBytes:[J

    if-eqz v2, :cond_8

    const-wide/16 v2, 0x0

    :goto_2
    move-object/from16 v0, p7

    iput-wide v2, v0, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory$Entry;->rxBytes:J

    .line 490
    iget-object v2, p0, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;->rxPackets:[J

    if-eqz v2, :cond_9

    const-wide/16 v2, 0x0

    :goto_3
    move-object/from16 v0, p7

    iput-wide v2, v0, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory$Entry;->rxPackets:J

    .line 491
    iget-object v2, p0, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;->txBytes:[J

    if-eqz v2, :cond_a

    const-wide/16 v2, 0x0

    :goto_4
    move-object/from16 v0, p7

    iput-wide v2, v0, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory$Entry;->txBytes:J

    .line 492
    iget-object v2, p0, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;->txPackets:[J

    if-eqz v2, :cond_b

    const-wide/16 v2, 0x0

    :goto_5
    move-object/from16 v0, p7

    iput-wide v2, v0, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory$Entry;->txPackets:J

    .line 493
    iget-object v2, p0, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;->operations:[J

    if-eqz v2, :cond_c

    const-wide/16 v2, 0x0

    :goto_6
    move-object/from16 v0, p7

    iput-wide v2, v0, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory$Entry;->operations:J

    .line 495
    move-wide/from16 v0, p3

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;->getIndexAfter(J)I

    move-result v2

    move v7, v2

    .line 496
    :goto_7
    if-ltz v7, :cond_11

    .line 497
    iget-object v2, p0, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;->bucketStart:[J

    aget-wide v2, v2, v7

    .line 498
    iget-wide v4, p0, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;->bucketDuration:J

    add-long/2addr v4, v2

    .line 501
    cmp-long v6, v4, p1

    if-lez v6, :cond_11

    .line 503
    cmp-long v6, v2, p3

    if-gez v6, :cond_5

    .line 506
    cmp-long v6, v2, p5

    if-gez v6, :cond_d

    cmp-long v6, v4, p5

    if-lez v6, :cond_d

    const/4 v6, 0x1

    .line 508
    :goto_8
    if-eqz v6, :cond_e

    .line 509
    iget-wide v2, p0, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;->bucketDuration:J

    .line 515
    :goto_9
    const-wide/16 v4, 0x0

    cmp-long v4, v2, v4

    if-lez v4, :cond_5

    .line 518
    iget-object v4, p0, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;->activeTime:[J

    if-eqz v4, :cond_0

    move-object/from16 v0, p7

    iget-wide v4, v0, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory$Entry;->activeTime:J

    iget-object v6, p0, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;->activeTime:[J

    aget-wide v8, v6, v7

    mul-long/2addr v8, v2

    iget-wide v10, p0, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;->bucketDuration:J

    div-long/2addr v8, v10

    add-long/2addr v4, v8

    move-object/from16 v0, p7

    iput-wide v4, v0, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory$Entry;->activeTime:J

    .line 519
    :cond_0
    iget-object v4, p0, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;->rxBytes:[J

    if-eqz v4, :cond_1

    move-object/from16 v0, p7

    iget-wide v4, v0, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory$Entry;->rxBytes:J

    iget-object v6, p0, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;->rxBytes:[J

    aget-wide v8, v6, v7

    mul-long/2addr v8, v2

    iget-wide v10, p0, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;->bucketDuration:J

    div-long/2addr v8, v10

    add-long/2addr v4, v8

    move-object/from16 v0, p7

    iput-wide v4, v0, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory$Entry;->rxBytes:J

    .line 520
    :cond_1
    iget-object v4, p0, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;->rxPackets:[J

    if-eqz v4, :cond_2

    move-object/from16 v0, p7

    iget-wide v4, v0, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory$Entry;->rxPackets:J

    iget-object v6, p0, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;->rxPackets:[J

    aget-wide v8, v6, v7

    mul-long/2addr v8, v2

    iget-wide v10, p0, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;->bucketDuration:J

    div-long/2addr v8, v10

    add-long/2addr v4, v8

    move-object/from16 v0, p7

    iput-wide v4, v0, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory$Entry;->rxPackets:J

    .line 521
    :cond_2
    iget-object v4, p0, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;->txBytes:[J

    if-eqz v4, :cond_3

    move-object/from16 v0, p7

    iget-wide v4, v0, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory$Entry;->txBytes:J

    iget-object v6, p0, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;->txBytes:[J

    aget-wide v8, v6, v7

    mul-long/2addr v8, v2

    iget-wide v10, p0, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;->bucketDuration:J

    div-long/2addr v8, v10

    add-long/2addr v4, v8

    move-object/from16 v0, p7

    iput-wide v4, v0, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory$Entry;->txBytes:J

    .line 522
    :cond_3
    iget-object v4, p0, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;->txPackets:[J

    if-eqz v4, :cond_4

    move-object/from16 v0, p7

    iget-wide v4, v0, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory$Entry;->txPackets:J

    iget-object v6, p0, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;->txPackets:[J

    aget-wide v8, v6, v7

    mul-long/2addr v8, v2

    iget-wide v10, p0, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;->bucketDuration:J

    div-long/2addr v8, v10

    add-long/2addr v4, v8

    move-object/from16 v0, p7

    iput-wide v4, v0, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory$Entry;->txPackets:J

    .line 523
    :cond_4
    iget-object v4, p0, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;->operations:[J

    if-eqz v4, :cond_5

    move-object/from16 v0, p7

    iget-wide v4, v0, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory$Entry;->operations:J

    iget-object v6, p0, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;->operations:[J

    aget-wide v8, v6, v7

    mul-long/2addr v2, v8

    iget-wide v8, p0, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;->bucketDuration:J

    div-long/2addr v2, v8

    add-long/2addr v2, v4

    move-object/from16 v0, p7

    iput-wide v2, v0, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory$Entry;->operations:J

    .line 496
    :cond_5
    add-int/lit8 v2, v7, -0x1

    move v7, v2

    goto/16 :goto_7

    .line 485
    :cond_6
    new-instance p7, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory$Entry;

    invoke-direct/range {p7 .. p7}, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory$Entry;-><init>()V

    goto/16 :goto_0

    .line 488
    :cond_7
    const-wide/16 v2, -0x1

    goto/16 :goto_1

    .line 489
    :cond_8
    const-wide/16 v2, -0x1

    goto/16 :goto_2

    .line 490
    :cond_9
    const-wide/16 v2, -0x1

    goto/16 :goto_3

    .line 491
    :cond_a
    const-wide/16 v2, -0x1

    goto/16 :goto_4

    .line 492
    :cond_b
    const-wide/16 v2, -0x1

    goto/16 :goto_5

    .line 493
    :cond_c
    const-wide/16 v2, -0x1

    goto/16 :goto_6

    .line 506
    :cond_d
    const/4 v6, 0x0

    goto/16 :goto_8

    .line 511
    :cond_e
    cmp-long v6, v4, p3

    if-gez v6, :cond_f

    .line 512
    :goto_a
    cmp-long v6, v2, p1

    if-lez v6, :cond_10

    .line 513
    :goto_b
    sub-long v2, v4, v2

    goto/16 :goto_9

    :cond_f
    move-wide/from16 v4, p3

    .line 511
    goto :goto_a

    :cond_10
    move-wide v2, p1

    .line 512
    goto :goto_b

    .line 525
    :cond_11
    return-object p7
.end method

.method public getValues(JJLcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory$Entry;)Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory$Entry;
    .locals 9

    .prologue
    .line 477
    const-wide v6, 0x7fffffffffffffffL

    move-object v1, p0

    move-wide v2, p1

    move-wide v4, p3

    move-object v8, p5

    invoke-virtual/range {v1 .. v8}, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;->getValues(JJJLcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory$Entry;)Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory$Entry;

    move-result-object v0

    return-object v0
.end method

.method public recordData(JJLcom/google/android/apps/chrome/third_party/datausagechart/NetworkStats$Entry;)V
    .locals 31

    .prologue
    .line 303
    move-object/from16 v0, p5

    iget-wide v0, v0, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStats$Entry;->rxBytes:J

    move-wide/from16 v16, v0

    .line 304
    move-object/from16 v0, p5

    iget-wide v14, v0, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStats$Entry;->rxPackets:J

    .line 305
    move-object/from16 v0, p5

    iget-wide v12, v0, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStats$Entry;->txBytes:J

    .line 306
    move-object/from16 v0, p5

    iget-wide v10, v0, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStats$Entry;->txPackets:J

    .line 307
    move-object/from16 v0, p5

    iget-wide v8, v0, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStats$Entry;->operations:J

    .line 309
    invoke-virtual/range {p5 .. p5}, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStats$Entry;->isNegative()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 310
    new-instance v4, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v5, "tried recording negative data"

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 314
    :cond_0
    invoke-direct/range {p0 .. p4}, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;->ensureBuckets(JJ)V

    .line 317
    sub-long v6, p3, p1

    .line 318
    move-object/from16 v0, p0

    move-wide/from16 v1, p3

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;->getIndexAfter(J)I

    move-result v4

    move/from16 v30, v4

    move-wide v4, v6

    move-wide v6, v8

    move-wide v8, v10

    move-wide v10, v12

    move-wide v12, v14

    move-wide/from16 v14, v16

    move/from16 v16, v30

    .line 319
    :goto_0
    if-ltz v16, :cond_2

    .line 320
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;->bucketStart:[J

    move-object/from16 v17, v0

    aget-wide v18, v17, v16

    .line 321
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;->bucketDuration:J

    move-wide/from16 v20, v0

    add-long v20, v20, v18

    .line 324
    cmp-long v17, v20, p1

    if-ltz v17, :cond_2

    .line 326
    cmp-long v17, v18, p3

    if-gtz v17, :cond_1

    .line 328
    move-wide/from16 v0, v20

    move-wide/from16 v2, p3

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v20

    move-wide/from16 v0, v18

    move-wide/from16 v2, p1

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v18

    sub-long v18, v20, v18

    .line 329
    const-wide/16 v20, 0x0

    cmp-long v17, v18, v20

    if-lez v17, :cond_1

    .line 332
    mul-long v20, v14, v18

    div-long v20, v20, v4

    .line 333
    mul-long v22, v12, v18

    div-long v22, v22, v4

    .line 334
    mul-long v24, v10, v18

    div-long v24, v24, v4

    .line 335
    mul-long v26, v8, v18

    div-long v26, v26, v4

    .line 336
    mul-long v28, v6, v18

    div-long v28, v28, v4

    .line 338
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;->activeTime:[J

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    move/from16 v1, v16

    move-wide/from16 v2, v18

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;->addLong([JIJ)V

    .line 339
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;->rxBytes:[J

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    move/from16 v1, v16

    move-wide/from16 v2, v20

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;->addLong([JIJ)V

    sub-long v14, v14, v20

    .line 340
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;->rxPackets:[J

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    move/from16 v1, v16

    move-wide/from16 v2, v22

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;->addLong([JIJ)V

    sub-long v12, v12, v22

    .line 341
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;->txBytes:[J

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    move/from16 v1, v16

    move-wide/from16 v2, v24

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;->addLong([JIJ)V

    sub-long v10, v10, v24

    .line 342
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;->txPackets:[J

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    move/from16 v1, v16

    move-wide/from16 v2, v26

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;->addLong([JIJ)V

    sub-long v8, v8, v26

    .line 343
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;->operations:[J

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    move/from16 v1, v16

    move-wide/from16 v2, v28

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;->addLong([JIJ)V

    sub-long v6, v6, v28

    .line 345
    sub-long v4, v4, v18

    .line 319
    :cond_1
    add-int/lit8 v16, v16, -0x1

    goto/16 :goto_0

    .line 348
    :cond_2
    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;->totalBytes:J

    move-object/from16 v0, p5

    iget-wide v6, v0, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStats$Entry;->rxBytes:J

    move-object/from16 v0, p5

    iget-wide v8, v0, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStats$Entry;->txBytes:J

    add-long/2addr v6, v8

    add-long/2addr v4, v6

    move-object/from16 v0, p0

    iput-wide v4, v0, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;->totalBytes:J

    .line 349
    return-void
.end method

.method public size()I
    .locals 1

    .prologue
    .line 214
    iget v0, p0, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;->bucketCount:I

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 604
    new-instance v0, Ljava/io/CharArrayWriter;

    invoke-direct {v0}, Ljava/io/CharArrayWriter;-><init>()V

    .line 605
    new-instance v1, Ljava/io/PrintWriter;

    invoke-direct {v1, v0}, Ljava/io/PrintWriter;-><init>(Ljava/io/Writer;)V

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;->dump(Ljava/io/PrintWriter;Z)V

    .line 606
    invoke-virtual {v0}, Ljava/io/CharArrayWriter;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 135
    iget-wide v0, p0, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;->bucketDuration:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 136
    iget-object v0, p0, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;->bucketStart:[J

    iget v1, p0, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;->bucketCount:I

    invoke-static {p1, v0, v1}, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory$ParcelUtils;->writeLongArray(Landroid/os/Parcel;[JI)V

    .line 137
    iget-object v0, p0, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;->activeTime:[J

    iget v1, p0, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;->bucketCount:I

    invoke-static {p1, v0, v1}, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory$ParcelUtils;->writeLongArray(Landroid/os/Parcel;[JI)V

    .line 138
    iget-object v0, p0, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;->rxBytes:[J

    iget v1, p0, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;->bucketCount:I

    invoke-static {p1, v0, v1}, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory$ParcelUtils;->writeLongArray(Landroid/os/Parcel;[JI)V

    .line 139
    iget-object v0, p0, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;->rxPackets:[J

    iget v1, p0, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;->bucketCount:I

    invoke-static {p1, v0, v1}, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory$ParcelUtils;->writeLongArray(Landroid/os/Parcel;[JI)V

    .line 140
    iget-object v0, p0, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;->txBytes:[J

    iget v1, p0, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;->bucketCount:I

    invoke-static {p1, v0, v1}, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory$ParcelUtils;->writeLongArray(Landroid/os/Parcel;[JI)V

    .line 141
    iget-object v0, p0, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;->txPackets:[J

    iget v1, p0, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;->bucketCount:I

    invoke-static {p1, v0, v1}, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory$ParcelUtils;->writeLongArray(Landroid/os/Parcel;[JI)V

    .line 142
    iget-object v0, p0, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;->operations:[J

    iget v1, p0, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;->bucketCount:I

    invoke-static {p1, v0, v1}, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory$ParcelUtils;->writeLongArray(Landroid/os/Parcel;[JI)V

    .line 143
    iget-wide v0, p0, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;->totalBytes:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 144
    return-void
.end method
