.class public Lcom/google/android/apps/chrome/widget/tabstrip/TabStripManager;
.super Landroid/widget/LinearLayout;
.source "TabStripManager.java"

# interfaces
.implements Lcom/google/android/apps/chrome/OverviewBehavior;


# static fields
.field static final synthetic $assertionsDisabled:Z

.field private static final NOTIFICATIONS:[I


# instance fields
.field private mFaded:Z

.field private mFullscreenManager:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;

.field private mIncognitoButton:Lcom/google/android/apps/chrome/widget/incognitotoggle/IncognitoToggleButtonTablet;

.field private mIncognitoStrip:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;

.field private mIncognitoTabCreator:Lcom/google/android/apps/chrome/tabmodel/ChromeTabCreator;

.field private mIsIncognito:Z

.field private mKeyboardHider:Lcom/google/android/apps/chrome/KeyboardHider;

.field private mNormalStrip:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;

.field private final mNotificationHandler:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;

.field private final mObserver:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$TabStripObserver;

.field private mTabContentManager:Lcom/google/android/apps/chrome/compositor/TabContentManager;

.field private mTabModelSelector:Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 33
    const-class v0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStripManager;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStripManager;->$assertionsDisabled:Z

    .line 197
    const/4 v0, 0x3

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStripManager;->NOTIFICATIONS:[I

    return-void

    .line 33
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 197
    :array_0
    .array-data 4
        0xc
        0x11
        0x12
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 52
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 44
    iput-boolean v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStripManager;->mIsIncognito:Z

    .line 45
    iput-boolean v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStripManager;->mFaded:Z

    .line 203
    new-instance v0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStripManager$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStripManager$1;-><init>(Lcom/google/android/apps/chrome/widget/tabstrip/TabStripManager;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStripManager;->mNotificationHandler:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;

    .line 220
    new-instance v0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStripManager$2;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStripManager$2;-><init>(Lcom/google/android/apps/chrome/widget/tabstrip/TabStripManager;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStripManager;->mObserver:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$TabStripObserver;

    .line 54
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Lcom/google/android/apps/chrome/R$layout;->tab_strip_manager:I

    const/4 v2, 0x1

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 55
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/chrome/widget/tabstrip/TabStripManager;Z)V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStripManager;->handleTabModelSelection(Z)V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/apps/chrome/widget/tabstrip/TabStripManager;Z)V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStripManager;->setFadeRequired(Z)V

    return-void
.end method

.method private createAllIncognitoAssetsIfNecessary()V
    .locals 5

    .prologue
    const/4 v2, 0x1

    .line 156
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStripManager;->mTabModelSelector:Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStripManager;->mTabModelSelector:Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

    invoke-interface {v0, v2}, Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;->getModel(Z)Lorg/chromium/chrome/browser/tabmodel/TabModel;

    move-result-object v0

    invoke-interface {v0}, Lorg/chromium/chrome/browser/tabmodel/TabModel;->getCount()I

    move-result v0

    if-nez v0, :cond_1

    .line 176
    :cond_0
    :goto_0
    return-void

    .line 158
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStripManager;->mIncognitoStrip:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;

    if-nez v0, :cond_2

    .line 159
    sget v0, Lcom/google/android/apps/chrome/R$id;->tab_strip_incognito_stub:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStripManager;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;

    iput-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStripManager;->mIncognitoStrip:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;

    .line 161
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStripManager;->mIncognitoStrip:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;

    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStripManager;->mObserver:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$TabStripObserver;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->registerObserver(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$TabStripObserver;)V

    .line 162
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStripManager;->mIncognitoStrip:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;

    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStripManager;->mKeyboardHider:Lcom/google/android/apps/chrome/KeyboardHider;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->setKeyboardHider(Lcom/google/android/apps/chrome/KeyboardHider;)V

    .line 163
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStripManager;->mIncognitoStrip:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;

    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStripManager;->mTabModelSelector:Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

    invoke-interface {v1, v2}, Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;->getModel(Z)Lorg/chromium/chrome/browser/tabmodel/TabModel;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStripManager;->mTabContentManager:Lcom/google/android/apps/chrome/compositor/TabContentManager;

    iget-object v3, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStripManager;->mIncognitoTabCreator:Lcom/google/android/apps/chrome/tabmodel/ChromeTabCreator;

    iget-object v4, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStripManager;->mFullscreenManager:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->setTabModel(Lorg/chromium/chrome/browser/tabmodel/TabModel;Lcom/google/android/apps/chrome/compositor/TabContentManager;Lcom/google/android/apps/chrome/tabmodel/ChromeTabCreator;Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;)V

    .line 170
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStripManager;->mIncognitoButton:Lcom/google/android/apps/chrome/widget/incognitotoggle/IncognitoToggleButtonTablet;

    if-nez v0, :cond_0

    .line 171
    sget v0, Lcom/google/android/apps/chrome/R$id;->incognito_button_stub:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStripManager;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/widget/incognitotoggle/IncognitoToggleButtonTablet;

    iput-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStripManager;->mIncognitoButton:Lcom/google/android/apps/chrome/widget/incognitotoggle/IncognitoToggleButtonTablet;

    .line 173
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStripManager;->mIncognitoButton:Lcom/google/android/apps/chrome/widget/incognitotoggle/IncognitoToggleButtonTablet;

    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStripManager;->mTabModelSelector:Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/widget/incognitotoggle/IncognitoToggleButtonTablet;->setTabModelSelector(Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;)V

    goto :goto_0
.end method

.method private handleTabModelSelection(Z)V
    .locals 3

    .prologue
    .line 74
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStripManager;->mIsIncognito:Z

    if-ne p1, v0, :cond_0

    .line 90
    :goto_0
    return-void

    .line 76
    :cond_0
    iput-boolean p1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStripManager;->mIsIncognito:Z

    .line 78
    if-eqz p1, :cond_1

    .line 79
    invoke-direct {p0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStripManager;->createAllIncognitoAssetsIfNecessary()V

    .line 82
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStripManager;->mIsIncognito:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStripManager;->mNormalStrip:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;

    .line 83
    :goto_1
    iget-boolean v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStripManager;->mIsIncognito:Z

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStripManager;->mIncognitoStrip:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;

    .line 84
    :goto_2
    invoke-virtual {v1}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->refreshSelection()V

    .line 86
    if-eqz v0, :cond_2

    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->setVisibility(I)V

    .line 87
    :cond_2
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->setVisibility(I)V

    .line 89
    invoke-virtual {p0, v1}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStripManager;->bringChildToFront(Landroid/view/View;)V

    goto :goto_0

    .line 82
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStripManager;->mIncognitoStrip:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;

    goto :goto_1

    .line 83
    :cond_4
    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStripManager;->mNormalStrip:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;

    goto :goto_2
.end method

.method private setFadeRequired(Z)V
    .locals 1

    .prologue
    .line 179
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStripManager;->mFaded:Z

    if-ne p1, v0, :cond_0

    .line 181
    :goto_0
    return-void

    .line 180
    :cond_0
    iput-boolean p1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStripManager;->mFaded:Z

    goto :goto_0
.end method


# virtual methods
.method public hideOverview(Z)V
    .locals 0

    .prologue
    .line 148
    return-void
.end method

.method protected onAttachedToWindow()V
    .locals 3

    .prologue
    .line 185
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStripManager;->getContext()Landroid/content/Context;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/chrome/widget/tabstrip/TabStripManager;->NOTIFICATIONS:[I

    iget-object v2, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStripManager;->mNotificationHandler:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->registerForNotifications(Landroid/content/Context;[ILcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;)V

    .line 187
    invoke-super {p0}, Landroid/widget/LinearLayout;->onAttachedToWindow()V

    .line 188
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 3

    .prologue
    .line 192
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStripManager;->getContext()Landroid/content/Context;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/chrome/widget/tabstrip/TabStripManager;->NOTIFICATIONS:[I

    iget-object v2, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStripManager;->mNotificationHandler:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->unregisterForNotifications(Landroid/content/Context;[ILcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;)V

    .line 194
    invoke-super {p0}, Landroid/widget/LinearLayout;->onDetachedFromWindow()V

    .line 195
    return-void
.end method

.method public onFinishInflate()V
    .locals 2

    .prologue
    .line 59
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 61
    sget v0, Lcom/google/android/apps/chrome/R$id;->tab_strip:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStripManager;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;

    iput-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStripManager;->mNormalStrip:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;

    .line 62
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStripManager;->mNormalStrip:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;

    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStripManager;->mObserver:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$TabStripObserver;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->registerObserver(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$TabStripObserver;)V

    .line 64
    sget v0, Lcom/google/android/apps/chrome/R$color;->bg_tabstrip_normal:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStripManager;->setBackgroundResource(I)V

    .line 65
    return-void
.end method

.method public overviewVisible()Z
    .locals 1

    .prologue
    .line 152
    const/4 v0, 0x0

    return v0
.end method

.method public setEnableAnimations(Z)V
    .locals 0

    .prologue
    .line 142
    return-void
.end method

.method public setFullscreenManager(Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;)V
    .locals 0

    .prologue
    .line 97
    iput-object p1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStripManager;->mFullscreenManager:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;

    .line 98
    return-void
.end method

.method public setKeyboardHider(Lcom/google/android/apps/chrome/KeyboardHider;)V
    .locals 2

    .prologue
    .line 136
    iput-object p1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStripManager;->mKeyboardHider:Lcom/google/android/apps/chrome/KeyboardHider;

    .line 137
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStripManager;->mIncognitoStrip:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStripManager;->mIncognitoStrip:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;

    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStripManager;->mKeyboardHider:Lcom/google/android/apps/chrome/KeyboardHider;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->setKeyboardHider(Lcom/google/android/apps/chrome/KeyboardHider;)V

    .line 138
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStripManager;->mNormalStrip:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStripManager;->mNormalStrip:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;

    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStripManager;->mKeyboardHider:Lcom/google/android/apps/chrome/KeyboardHider;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->setKeyboardHider(Lcom/google/android/apps/chrome/KeyboardHider;)V

    .line 139
    :cond_1
    return-void
.end method

.method public setTabModelSelector(Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;Lcom/google/android/apps/chrome/tabmodel/ChromeTabCreatorManager;Lcom/google/android/apps/chrome/compositor/TabContentManager;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v3, 0x1

    .line 109
    sget-boolean v0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStripManager;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStripManager;->mFullscreenManager:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    const-string/jumbo v1, "setFullscreenManager must be called before setTabModelSelector"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 110
    :cond_0
    iput-object p1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStripManager;->mTabModelSelector:Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

    .line 112
    invoke-interface {p2, v3}, Lcom/google/android/apps/chrome/tabmodel/ChromeTabCreatorManager;->getTabCreator(Z)Lcom/google/android/apps/chrome/tabmodel/ChromeTabCreator;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStripManager;->mIncognitoTabCreator:Lcom/google/android/apps/chrome/tabmodel/ChromeTabCreator;

    .line 113
    iput-object p3, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStripManager;->mTabContentManager:Lcom/google/android/apps/chrome/compositor/TabContentManager;

    .line 115
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStripManager;->mIncognitoStrip:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;

    if-eqz v0, :cond_1

    .line 116
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStripManager;->mIncognitoStrip:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;

    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStripManager;->mTabModelSelector:Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

    invoke-interface {v1, v3}, Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;->getModel(Z)Lorg/chromium/chrome/browser/tabmodel/TabModel;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStripManager;->mTabContentManager:Lcom/google/android/apps/chrome/compositor/TabContentManager;

    invoke-interface {p2, v3}, Lcom/google/android/apps/chrome/tabmodel/ChromeTabCreatorManager;->getTabCreator(Z)Lcom/google/android/apps/chrome/tabmodel/ChromeTabCreator;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStripManager;->mFullscreenManager:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->setTabModel(Lorg/chromium/chrome/browser/tabmodel/TabModel;Lcom/google/android/apps/chrome/compositor/TabContentManager;Lcom/google/android/apps/chrome/tabmodel/ChromeTabCreator;Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;)V

    .line 122
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStripManager;->mNormalStrip:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;

    if-eqz v0, :cond_2

    .line 123
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStripManager;->mNormalStrip:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;

    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStripManager;->mTabModelSelector:Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

    invoke-interface {v1, v5}, Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;->getModel(Z)Lorg/chromium/chrome/browser/tabmodel/TabModel;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStripManager;->mTabContentManager:Lcom/google/android/apps/chrome/compositor/TabContentManager;

    invoke-interface {p2, v5}, Lcom/google/android/apps/chrome/tabmodel/ChromeTabCreatorManager;->getTabCreator(Z)Lcom/google/android/apps/chrome/tabmodel/ChromeTabCreator;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStripManager;->mFullscreenManager:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->setTabModel(Lorg/chromium/chrome/browser/tabmodel/TabModel;Lcom/google/android/apps/chrome/compositor/TabContentManager;Lcom/google/android/apps/chrome/tabmodel/ChromeTabCreator;Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;)V

    .line 129
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStripManager;->mIncognitoButton:Lcom/google/android/apps/chrome/widget/incognitotoggle/IncognitoToggleButtonTablet;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStripManager;->mIncognitoButton:Lcom/google/android/apps/chrome/widget/incognitotoggle/IncognitoToggleButtonTablet;

    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStripManager;->mTabModelSelector:Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/widget/incognitotoggle/IncognitoToggleButtonTablet;->setTabModelSelector(Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;)V

    .line 131
    :cond_3
    invoke-interface {p1}, Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;->isIncognitoSelected()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStripManager;->handleTabModelSelection(Z)V

    .line 132
    return-void
.end method

.method public showOverview(Z)V
    .locals 0

    .prologue
    .line 145
    return-void
.end method
