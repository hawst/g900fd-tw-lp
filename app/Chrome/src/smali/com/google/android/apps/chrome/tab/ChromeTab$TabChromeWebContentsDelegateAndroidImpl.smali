.class public Lcom/google/android/apps/chrome/tab/ChromeTab$TabChromeWebContentsDelegateAndroidImpl;
.super Lorg/chromium/chrome/browser/Tab$TabChromeWebContentsDelegateAndroid;
.source "ChromeTab.java"


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/chrome/tab/ChromeTab;)V
    .locals 0

    .prologue
    .line 842
    iput-object p1, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$TabChromeWebContentsDelegateAndroidImpl;->this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;

    invoke-direct {p0, p1}, Lorg/chromium/chrome/browser/Tab$TabChromeWebContentsDelegateAndroid;-><init>(Lorg/chromium/chrome/browser/Tab;)V

    return-void
.end method

.method private handleMediaKey(Landroid/view/KeyEvent;)V
    .locals 2

    .prologue
    .line 994
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-ge v0, v1, :cond_0

    .line 1013
    :goto_0
    return-void

    .line 995
    :cond_0
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    goto :goto_0

    .line 1010
    :sswitch_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$TabChromeWebContentsDelegateAndroidImpl;->this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;

    # getter for: Lcom/google/android/apps/chrome/tab/ChromeTab;->mActivity:Lcom/google/android/apps/chrome/ChromeActivity;
    invoke-static {v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->access$900(Lcom/google/android/apps/chrome/tab/ChromeTab;)Lcom/google/android/apps/chrome/ChromeActivity;

    move-result-object v0

    const-string/jumbo v1, "audio"

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/ChromeActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    .line 1012
    invoke-virtual {v0, p1}, Landroid/media/AudioManager;->dispatchMediaKeyEvent(Landroid/view/KeyEvent;)V

    goto :goto_0

    .line 995
    :sswitch_data_0
    .sparse-switch
        0x4f -> :sswitch_0
        0x55 -> :sswitch_0
        0x56 -> :sswitch_0
        0x57 -> :sswitch_0
        0x58 -> :sswitch_0
        0x59 -> :sswitch_0
        0x5a -> :sswitch_0
        0x5b -> :sswitch_0
        0x7e -> :sswitch_0
        0x7f -> :sswitch_0
        0x80 -> :sswitch_0
        0x81 -> :sswitch_0
        0x82 -> :sswitch_0
        0xde -> :sswitch_0
    .end sparse-switch
.end method


# virtual methods
.method public activateContents()V
    .locals 3

    .prologue
    .line 874
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$TabChromeWebContentsDelegateAndroidImpl;->this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;

    # getter for: Lcom/google/android/apps/chrome/tab/ChromeTab;->mActivity:Lcom/google/android/apps/chrome/ChromeActivity;
    invoke-static {v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->access$900(Lcom/google/android/apps/chrome/tab/ChromeTab;)Lcom/google/android/apps/chrome/ChromeActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ChromeActivity;->getTabModelSelector()Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$TabChromeWebContentsDelegateAndroidImpl;->this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/tab/ChromeTab;->isIncognito()Z

    move-result v1

    invoke-interface {v0, v1}, Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;->getModel(Z)Lorg/chromium/chrome/browser/tabmodel/TabModel;

    move-result-object v0

    .line 875
    iget-object v1, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$TabChromeWebContentsDelegateAndroidImpl;->this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;

    invoke-interface {v0, v1}, Lorg/chromium/chrome/browser/tabmodel/TabModel;->indexOf(Lorg/chromium/chrome/browser/Tab;)I

    move-result v1

    .line 876
    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    invoke-static {v0, v1}, Lorg/chromium/chrome/browser/tabmodel/TabModelUtils;->setIndex(Lorg/chromium/chrome/browser/tabmodel/TabModel;I)V

    .line 877
    :cond_0
    return-void
.end method

.method public addNewContents(JJILandroid/graphics/Rect;Z)Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 855
    iget-object v1, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$TabChromeWebContentsDelegateAndroidImpl;->this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/tab/ChromeTab;->isClosing()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 869
    :cond_0
    :goto_0
    return v0

    .line 858
    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$TabChromeWebContentsDelegateAndroidImpl;->this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;

    # getter for: Lcom/google/android/apps/chrome/tab/ChromeTab;->mActivity:Lcom/google/android/apps/chrome/ChromeActivity;
    invoke-static {v1}, Lcom/google/android/apps/chrome/tab/ChromeTab;->access$900(Lcom/google/android/apps/chrome/tab/ChromeTab;)Lcom/google/android/apps/chrome/ChromeActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$TabChromeWebContentsDelegateAndroidImpl;->this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/tab/ChromeTab;->isIncognito()Z

    move-result v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/chrome/ChromeActivity;->getTabCreator(Z)Lcom/google/android/apps/chrome/tabmodel/ChromeTabCreator;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$TabChromeWebContentsDelegateAndroidImpl;->this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getId()I

    move-result v2

    sget-object v3, Lorg/chromium/chrome/browser/tabmodel/TabModel$TabLaunchType;->FROM_LONGPRESS_FOREGROUND:Lorg/chromium/chrome/browser/tabmodel/TabModel$TabLaunchType;

    invoke-virtual {v1, p3, p4, v2, v3}, Lcom/google/android/apps/chrome/tabmodel/ChromeTabCreator;->createTabWithNativeContents(JILorg/chromium/chrome/browser/tabmodel/TabModel$TabLaunchType;)Lcom/google/android/apps/chrome/tab/ChromeTab;

    move-result-object v1

    .line 861
    if-eqz v1, :cond_0

    .line 863
    const/4 v0, 0x6

    if-ne p5, v0, :cond_2

    .line 864
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$TabChromeWebContentsDelegateAndroidImpl;->this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;

    # invokes: Lcom/google/android/apps/chrome/tab/ChromeTab;->getApplicationContext()Landroid/content/Context;
    invoke-static {v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->access$1800(Lcom/google/android/apps/chrome/tab/ChromeTab;)Landroid/content/Context;

    move-result-object v0

    sget-object v2, Lcom/google/android/apps/chrome/knoxsettings/KnoxAuditLogger$AuditEvent;->OPEN_POPUP_URL_SUCCESS:Lcom/google/android/apps/chrome/knoxsettings/KnoxAuditLogger$AuditEvent;

    const-string/jumbo v3, "ChromeTab"

    invoke-virtual {v1}, Lorg/chromium/chrome/browser/Tab;->getUrl()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v4, ""

    invoke-static {v0, v2, v3, v1, v4}, Lcom/google/android/apps/chrome/knoxsettings/KnoxAuditLogger;->logIfNecessary(Landroid/content/Context;Lcom/google/android/apps/chrome/knoxsettings/KnoxAuditLogger$AuditEvent;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 869
    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public closeContents()V
    .locals 2

    .prologue
    .line 946
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$TabChromeWebContentsDelegateAndroidImpl;->this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;

    # getter for: Lcom/google/android/apps/chrome/tab/ChromeTab;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->access$1500(Lcom/google/android/apps/chrome/tab/ChromeTab;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$TabChromeWebContentsDelegateAndroidImpl;->this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;

    # getter for: Lcom/google/android/apps/chrome/tab/ChromeTab;->mCloseContentsRunnable:Ljava/lang/Runnable;
    invoke-static {v1}, Lcom/google/android/apps/chrome/tab/ChromeTab;->access$2800(Lcom/google/android/apps/chrome/tab/ChromeTab;)Ljava/lang/Runnable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 947
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$TabChromeWebContentsDelegateAndroidImpl;->this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;

    # getter for: Lcom/google/android/apps/chrome/tab/ChromeTab;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->access$1500(Lcom/google/android/apps/chrome/tab/ChromeTab;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$TabChromeWebContentsDelegateAndroidImpl;->this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;

    # getter for: Lcom/google/android/apps/chrome/tab/ChromeTab;->mCloseContentsRunnable:Ljava/lang/Runnable;
    invoke-static {v1}, Lcom/google/android/apps/chrome/tab/ChromeTab;->access$2800(Lcom/google/android/apps/chrome/tab/ChromeTab;)Ljava/lang/Runnable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 948
    return-void
.end method

.method public handleKeyboardEvent(Landroid/view/KeyEvent;)V
    .locals 2

    .prologue
    .line 974
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_2

    .line 975
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$TabChromeWebContentsDelegateAndroidImpl;->this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;

    # getter for: Lcom/google/android/apps/chrome/tab/ChromeTab;->mActivity:Lcom/google/android/apps/chrome/ChromeActivity;
    invoke-static {v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->access$900(Lcom/google/android/apps/chrome/tab/ChromeTab;)Lcom/google/android/apps/chrome/ChromeActivity;

    move-result-object v0

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v1

    invoke-virtual {v0, v1, p1}, Lcom/google/android/apps/chrome/ChromeActivity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 986
    :cond_0
    :goto_0
    return-void

    .line 979
    :cond_1
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v0

    const/16 v1, 0x6f

    if-ne v0, v1, :cond_2

    invoke-virtual {p1}, Landroid/view/KeyEvent;->hasNoModifiers()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 980
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$TabChromeWebContentsDelegateAndroidImpl;->this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getContentViewCore()Lorg/chromium/content/browser/ContentViewCore;

    move-result-object v0

    .line 981
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lorg/chromium/content/browser/ContentViewCore;->getWebContents()Lorg/chromium/content_public/browser/WebContents;

    move-result-object v0

    invoke-interface {v0}, Lorg/chromium/content_public/browser/WebContents;->stop()V

    goto :goto_0

    .line 985
    :cond_2
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/tab/ChromeTab$TabChromeWebContentsDelegateAndroidImpl;->handleMediaKey(Landroid/view/KeyEvent;)V

    goto :goto_0
.end method

.method public isFullscreenForTabOrPending()Z
    .locals 1

    .prologue
    .line 968
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$TabChromeWebContentsDelegateAndroidImpl;->this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;

    # getter for: Lcom/google/android/apps/chrome/tab/ChromeTab;->mFullscreenManager:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;
    invoke-static {v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->access$2400(Lcom/google/android/apps/chrome/tab/ChromeTab;)Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$TabChromeWebContentsDelegateAndroidImpl;->this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;

    # getter for: Lcom/google/android/apps/chrome/tab/ChromeTab;->mFullscreenManager:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;
    invoke-static {v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->access$2400(Lcom/google/android/apps/chrome/tab/ChromeTab;)Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->getPersistentFullscreenMode()Z

    move-result v0

    goto :goto_0
.end method

.method public navigationStateChanged(I)V
    .locals 5

    .prologue
    .line 893
    and-int/lit8 v0, p1, 0x2

    if-eqz v0, :cond_0

    .line 894
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$TabChromeWebContentsDelegateAndroidImpl;->this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;

    # invokes: Lcom/google/android/apps/chrome/tab/ChromeTab;->getApplicationContext()Landroid/content/Context;
    invoke-static {v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->access$2000(Lcom/google/android/apps/chrome/tab/ChromeTab;)Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$TabChromeWebContentsDelegateAndroidImpl;->this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getId()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$TabChromeWebContentsDelegateAndroidImpl;->this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;

    # invokes: Lcom/google/android/apps/chrome/tab/ChromeTab;->isCapturingAudio()Z
    invoke-static {v2}, Lcom/google/android/apps/chrome/tab/ChromeTab;->access$2100(Lcom/google/android/apps/chrome/tab/ChromeTab;)Z

    move-result v2

    iget-object v3, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$TabChromeWebContentsDelegateAndroidImpl;->this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;

    # invokes: Lcom/google/android/apps/chrome/tab/ChromeTab;->isCapturingVideo()Z
    invoke-static {v3}, Lcom/google/android/apps/chrome/tab/ChromeTab;->access$2200(Lcom/google/android/apps/chrome/tab/ChromeTab;)Z

    move-result v3

    iget-object v4, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$TabChromeWebContentsDelegateAndroidImpl;->this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;

    invoke-virtual {v4}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getUrl()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v1, v2, v3, v4}, Lcom/google/android/apps/chrome/MediaCaptureNotificationService;->updateMediaNotificationForTab(Landroid/content/Context;IZZLjava/lang/String;)V

    .line 898
    :cond_0
    invoke-super {p0, p1}, Lorg/chromium/chrome/browser/Tab$TabChromeWebContentsDelegateAndroid;->navigationStateChanged(I)V

    .line 899
    return-void
.end method

.method public onFindMatchRectsAvailable(Lorg/chromium/chrome/browser/FindMatchRectsDetails;)V
    .locals 1

    .prologue
    .line 937
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$TabChromeWebContentsDelegateAndroidImpl;->this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;

    # getter for: Lcom/google/android/apps/chrome/tab/ChromeTab;->mFindMatchRectsListener:Lcom/google/android/apps/chrome/tab/ChromeTab$FindMatchRectsListener;
    invoke-static {v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->access$2700(Lcom/google/android/apps/chrome/tab/ChromeTab;)Lcom/google/android/apps/chrome/tab/ChromeTab$FindMatchRectsListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 938
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$TabChromeWebContentsDelegateAndroidImpl;->this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;

    # getter for: Lcom/google/android/apps/chrome/tab/ChromeTab;->mFindMatchRectsListener:Lcom/google/android/apps/chrome/tab/ChromeTab$FindMatchRectsListener;
    invoke-static {v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->access$2700(Lcom/google/android/apps/chrome/tab/ChromeTab;)Lcom/google/android/apps/chrome/tab/ChromeTab$FindMatchRectsListener;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/google/android/apps/chrome/tab/ChromeTab$FindMatchRectsListener;->onFindMatchRects(Lorg/chromium/chrome/browser/FindMatchRectsDetails;)V

    .line 940
    :cond_0
    return-void
.end method

.method public onFindResultAvailable(Lorg/chromium/chrome/browser/FindNotificationDetails;)V
    .locals 1

    .prologue
    .line 930
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$TabChromeWebContentsDelegateAndroidImpl;->this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;

    # getter for: Lcom/google/android/apps/chrome/tab/ChromeTab;->mFindResultListener:Lcom/google/android/apps/chrome/tab/ChromeTab$FindResultListener;
    invoke-static {v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->access$2600(Lcom/google/android/apps/chrome/tab/ChromeTab;)Lcom/google/android/apps/chrome/tab/ChromeTab$FindResultListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 931
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$TabChromeWebContentsDelegateAndroidImpl;->this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;

    # getter for: Lcom/google/android/apps/chrome/tab/ChromeTab;->mFindResultListener:Lcom/google/android/apps/chrome/tab/ChromeTab$FindResultListener;
    invoke-static {v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->access$2600(Lcom/google/android/apps/chrome/tab/ChromeTab;)Lcom/google/android/apps/chrome/tab/ChromeTab$FindResultListener;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/google/android/apps/chrome/tab/ChromeTab$FindResultListener;->onFindResult(Lorg/chromium/chrome/browser/FindNotificationDetails;)V

    .line 933
    :cond_0
    return-void
.end method

.method public onLoadProgressChanged(I)V
    .locals 2

    .prologue
    .line 903
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$TabChromeWebContentsDelegateAndroidImpl;->this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;

    # getter for: Lcom/google/android/apps/chrome/tab/ChromeTab;->mIsLoading:Z
    invoke-static {v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->access$2300(Lcom/google/android/apps/chrome/tab/ChromeTab;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 909
    :goto_0
    return-void

    .line 904
    :cond_0
    invoke-super {p0, p1}, Lorg/chromium/chrome/browser/Tab$TabChromeWebContentsDelegateAndroid;->onLoadProgressChanged(I)V

    .line 905
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$TabChromeWebContentsDelegateAndroidImpl;->this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;

    # getter for: Lcom/google/android/apps/chrome/tab/ChromeTab;->mLoadProgressAtViewSwapInTime:I
    invoke-static {v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->access$1300(Lcom/google/android/apps/chrome/tab/ChromeTab;)I

    move-result v0

    if-lt p1, v0, :cond_1

    .line 906
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$TabChromeWebContentsDelegateAndroidImpl;->this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;

    const/4 v1, 0x0

    # setter for: Lcom/google/android/apps/chrome/tab/ChromeTab;->mLoadProgressAtViewSwapInTime:I
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/tab/ChromeTab;->access$1302(Lcom/google/android/apps/chrome/tab/ChromeTab;I)I

    .line 908
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$TabChromeWebContentsDelegateAndroidImpl;->this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;

    # invokes: Lcom/google/android/apps/chrome/tab/ChromeTab;->notifyLoadProgress()V
    invoke-static {v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->access$1600(Lcom/google/android/apps/chrome/tab/ChromeTab;)V

    goto :goto_0
.end method

.method public onLoadStarted()V
    .locals 2

    .prologue
    .line 881
    invoke-super {p0}, Lorg/chromium/chrome/browser/Tab$TabChromeWebContentsDelegateAndroid;->onLoadStarted()V

    .line 882
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$TabChromeWebContentsDelegateAndroidImpl;->this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;

    const/16 v1, 0x1a

    # invokes: Lcom/google/android/apps/chrome/tab/ChromeTab;->notifyPageLoad(I)V
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/tab/ChromeTab;->access$1900(Lcom/google/android/apps/chrome/tab/ChromeTab;I)V

    .line 883
    return-void
.end method

.method public onLoadStopped()V
    .locals 2

    .prologue
    .line 887
    invoke-super {p0}, Lorg/chromium/chrome/browser/Tab$TabChromeWebContentsDelegateAndroid;->onLoadStopped()V

    .line 888
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$TabChromeWebContentsDelegateAndroidImpl;->this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;

    const/16 v1, 0x1b

    # invokes: Lcom/google/android/apps/chrome/tab/ChromeTab;->notifyPageLoad(I)V
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/tab/ChromeTab;->access$1900(Lcom/google/android/apps/chrome/tab/ChromeTab;I)V

    .line 889
    return-void
.end method

.method public openNewTab(Ljava/lang/String;Ljava/lang/String;[BIZ)V
    .locals 7

    .prologue
    .line 847
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$TabChromeWebContentsDelegateAndroidImpl;->this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;

    const/4 v5, 0x1

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move v6, p5

    # invokes: Lcom/google/android/apps/chrome/tab/ChromeTab;->openNewTab(Ljava/lang/String;Ljava/lang/String;[BIZZ)V
    invoke-static/range {v0 .. v6}, Lcom/google/android/apps/chrome/tab/ChromeTab;->access$1700(Lcom/google/android/apps/chrome/tab/ChromeTab;Ljava/lang/String;Ljava/lang/String;[BIZZ)V

    .line 849
    return-void
.end method

.method public rendererResponsive()V
    .locals 2

    .prologue
    .line 922
    invoke-super {p0}, Lorg/chromium/chrome/browser/Tab$TabChromeWebContentsDelegateAndroid;->rendererResponsive()V

    .line 923
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$TabChromeWebContentsDelegateAndroidImpl;->this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;

    # getter for: Lcom/google/android/apps/chrome/tab/ChromeTab;->mFullscreenManager:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;
    invoke-static {v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->access$2400(Lcom/google/android/apps/chrome/tab/ChromeTab;)Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;

    move-result-object v0

    if-nez v0, :cond_0

    .line 926
    :goto_0
    return-void

    .line 924
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$TabChromeWebContentsDelegateAndroidImpl;->this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;

    # getter for: Lcom/google/android/apps/chrome/tab/ChromeTab;->mFullscreenManager:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;
    invoke-static {v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->access$2400(Lcom/google/android/apps/chrome/tab/ChromeTab;)Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$TabChromeWebContentsDelegateAndroidImpl;->this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;

    # getter for: Lcom/google/android/apps/chrome/tab/ChromeTab;->mFullscreenHungRendererToken:I
    invoke-static {v1}, Lcom/google/android/apps/chrome/tab/ChromeTab;->access$2500(Lcom/google/android/apps/chrome/tab/ChromeTab;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->hideControlsPersistent(I)V

    .line 925
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$TabChromeWebContentsDelegateAndroidImpl;->this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;

    const/4 v1, -0x1

    # setter for: Lcom/google/android/apps/chrome/tab/ChromeTab;->mFullscreenHungRendererToken:I
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/tab/ChromeTab;->access$2502(Lcom/google/android/apps/chrome/tab/ChromeTab;I)I

    goto :goto_0
.end method

.method public rendererUnresponsive()V
    .locals 3

    .prologue
    .line 913
    invoke-super {p0}, Lorg/chromium/chrome/browser/Tab$TabChromeWebContentsDelegateAndroid;->rendererUnresponsive()V

    .line 914
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$TabChromeWebContentsDelegateAndroidImpl;->this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;

    # getter for: Lcom/google/android/apps/chrome/tab/ChromeTab;->mFullscreenManager:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;
    invoke-static {v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->access$2400(Lcom/google/android/apps/chrome/tab/ChromeTab;)Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;

    move-result-object v0

    if-nez v0, :cond_0

    .line 918
    :goto_0
    return-void

    .line 915
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$TabChromeWebContentsDelegateAndroidImpl;->this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;

    iget-object v1, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$TabChromeWebContentsDelegateAndroidImpl;->this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;

    # getter for: Lcom/google/android/apps/chrome/tab/ChromeTab;->mFullscreenManager:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;
    invoke-static {v1}, Lcom/google/android/apps/chrome/tab/ChromeTab;->access$2400(Lcom/google/android/apps/chrome/tab/ChromeTab;)Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$TabChromeWebContentsDelegateAndroidImpl;->this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;

    # getter for: Lcom/google/android/apps/chrome/tab/ChromeTab;->mFullscreenHungRendererToken:I
    invoke-static {v2}, Lcom/google/android/apps/chrome/tab/ChromeTab;->access$2500(Lcom/google/android/apps/chrome/tab/ChromeTab;)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->showControlsPersistentAndClearOldToken(I)I

    move-result v1

    # setter for: Lcom/google/android/apps/chrome/tab/ChromeTab;->mFullscreenHungRendererToken:I
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/tab/ChromeTab;->access$2502(Lcom/google/android/apps/chrome/tab/ChromeTab;I)I

    goto :goto_0
.end method

.method public takeFocus(Z)Z
    .locals 2

    .prologue
    .line 952
    if-eqz p1, :cond_1

    .line 953
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$TabChromeWebContentsDelegateAndroidImpl;->this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;

    # getter for: Lcom/google/android/apps/chrome/tab/ChromeTab;->mActivity:Lcom/google/android/apps/chrome/ChromeActivity;
    invoke-static {v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->access$900(Lcom/google/android/apps/chrome/tab/ChromeTab;)Lcom/google/android/apps/chrome/ChromeActivity;

    move-result-object v0

    sget v1, Lcom/google/android/apps/chrome/R$id;->menu_button:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/ChromeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 954
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/view/View;->isShown()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 955
    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    move-result v0

    .line 963
    :goto_0
    return v0

    .line 956
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$TabChromeWebContentsDelegateAndroidImpl;->this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;

    # getter for: Lcom/google/android/apps/chrome/tab/ChromeTab;->mActivity:Lcom/google/android/apps/chrome/ChromeActivity;
    invoke-static {v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->access$900(Lcom/google/android/apps/chrome/tab/ChromeTab;)Lcom/google/android/apps/chrome/ChromeActivity;

    move-result-object v0

    sget v1, Lcom/google/android/apps/chrome/R$id;->tab_switcher_button:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/ChromeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 957
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$TabChromeWebContentsDelegateAndroidImpl;->this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;

    # getter for: Lcom/google/android/apps/chrome/tab/ChromeTab;->mActivity:Lcom/google/android/apps/chrome/ChromeActivity;
    invoke-static {v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->access$900(Lcom/google/android/apps/chrome/tab/ChromeTab;)Lcom/google/android/apps/chrome/ChromeActivity;

    move-result-object v0

    sget v1, Lcom/google/android/apps/chrome/R$id;->tab_switcher_button:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/ChromeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    move-result v0

    goto :goto_0

    .line 960
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$TabChromeWebContentsDelegateAndroidImpl;->this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;

    # getter for: Lcom/google/android/apps/chrome/tab/ChromeTab;->mActivity:Lcom/google/android/apps/chrome/ChromeActivity;
    invoke-static {v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->access$900(Lcom/google/android/apps/chrome/tab/ChromeTab;)Lcom/google/android/apps/chrome/ChromeActivity;

    move-result-object v0

    sget v1, Lcom/google/android/apps/chrome/R$id;->url_bar:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/ChromeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 961
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    move-result v0

    goto :goto_0

    .line 963
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method
