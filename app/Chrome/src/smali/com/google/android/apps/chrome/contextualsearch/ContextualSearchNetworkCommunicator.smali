.class public interface abstract Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchNetworkCommunicator;
.super Ljava/lang/Object;
.source "ContextualSearchNetworkCommunicator.java"


# virtual methods
.method public abstract continueSearchTermResolutionRequest()V
.end method

.method public abstract handleSearchTermResolutionResponse(ZILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
.end method

.method public abstract loadUrl(Ljava/lang/String;)V
.end method

.method public abstract startSearchTermResolutionRequest(Ljava/lang/String;)V
.end method
