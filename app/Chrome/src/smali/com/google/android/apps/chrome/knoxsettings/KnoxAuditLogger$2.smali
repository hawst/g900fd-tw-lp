.class final Lcom/google/android/apps/chrome/knoxsettings/KnoxAuditLogger$2;
.super Ljava/lang/Object;
.source "KnoxAuditLogger.java"

# interfaces
.implements Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Observer;


# instance fields
.field final synthetic val$context:Landroid/content/Context;

.field final synthetic val$knoxSettings:Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings;

.field final synthetic val$url:Ljava/lang/String;


# direct methods
.method constructor <init>(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings;)V
    .locals 0

    .prologue
    .line 88
    iput-object p1, p0, Lcom/google/android/apps/chrome/knoxsettings/KnoxAuditLogger$2;->val$context:Landroid/content/Context;

    iput-object p2, p0, Lcom/google/android/apps/chrome/knoxsettings/KnoxAuditLogger$2;->val$url:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/apps/chrome/knoxsettings/KnoxAuditLogger$2;->val$knoxSettings:Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onSettingsAvailable(Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;)V
    .locals 2

    .prologue
    .line 91
    invoke-virtual {p1}, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;->getUrlFilterReportEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 92
    iget-object v0, p0, Lcom/google/android/apps/chrome/knoxsettings/KnoxAuditLogger$2;->val$context:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/apps/chrome/knoxsettings/KnoxAuditLogger$2;->val$url:Ljava/lang/String;

    # invokes: Lcom/google/android/apps/chrome/knoxsettings/KnoxAuditLogger;->logURLBlockedReport(Landroid/content/Context;Ljava/lang/String;)V
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/knoxsettings/KnoxAuditLogger;->access$000(Landroid/content/Context;Ljava/lang/String;)V

    .line 94
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/knoxsettings/KnoxAuditLogger$2;->val$knoxSettings:Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings;->removeObserver(Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Observer;)V

    .line 95
    return-void
.end method
