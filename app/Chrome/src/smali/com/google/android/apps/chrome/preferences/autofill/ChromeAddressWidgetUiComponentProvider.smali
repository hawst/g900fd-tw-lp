.class Lcom/google/android/apps/chrome/preferences/autofill/ChromeAddressWidgetUiComponentProvider;
.super Lcom/a/a/a/s;
.source "ChromeAddressWidgetUiComponentProvider.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0, p1}, Lcom/a/a/a/s;-><init>(Landroid/content/Context;)V

    .line 22
    return-void
.end method


# virtual methods
.method protected createUiLabel(Ljava/lang/CharSequence;Lcom/a/a/a/f;)Landroid/widget/TextView;
    .locals 3

    .prologue
    .line 25
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/autofill/ChromeAddressWidgetUiComponentProvider;->mContext:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Lcom/google/android/apps/chrome/R$layout;->autofill_profile_label:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 27
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 28
    return-object v0
.end method
