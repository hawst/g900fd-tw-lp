.class Lcom/google/android/apps/chrome/feedback/LegacyFeedbackReporter;
.super Ljava/lang/Object;
.source "LegacyFeedbackReporter.java"

# interfaces
.implements Lcom/google/android/apps/chrome/feedback/FeedbackReporter;


# instance fields
.field private final mApplicationContext:Landroid/content/Context;


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/feedback/LegacyFeedbackReporter;->mApplicationContext:Landroid/content/Context;

    .line 38
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/chrome/feedback/LegacyFeedbackReporter;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lcom/google/android/apps/chrome/feedback/LegacyFeedbackReporter;->mApplicationContext:Landroid/content/Context;

    return-object v0
.end method

.method private static logFeedbackExtraInfo(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    .line 81
    if-nez p0, :cond_1

    .line 85
    :cond_0
    return-void

    .line 82
    :cond_1
    invoke-virtual {p0}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 83
    const-string/jumbo v2, "LegacyFeedbackReporter"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "FEEDBACK DATA: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, " = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method


# virtual methods
.method public reportFeedback(Landroid/graphics/Bitmap;Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 42
    invoke-static {}, Lorg/chromium/base/ThreadUtils;->assertOnUiThread()V

    .line 43
    invoke-static {p2}, Lcom/google/android/apps/chrome/feedback/LegacyFeedbackReporter;->logFeedbackExtraInfo(Landroid/os/Bundle;)V

    .line 45
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "android.intent.action.BUG_REPORT"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 46
    new-instance v1, Landroid/content/ComponentName;

    const-string/jumbo v2, "com.google.android.gms"

    const-string/jumbo v3, "com.google.android.gms.feedback.LegacyBugReportService"

    invoke-direct {v1, v2, v3}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 47
    iget-object v1, p0, Lcom/google/android/apps/chrome/feedback/LegacyFeedbackReporter;->mApplicationContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/content/pm/PackageManager;->resolveService(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;

    move-result-object v1

    if-nez v1, :cond_0

    .line 48
    const-string/jumbo v0, "LegacyFeedbackReporter"

    const-string/jumbo v1, "Unable to resolve Feedback service."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 73
    :goto_0
    return-void

    .line 52
    :cond_0
    new-instance v1, Lcom/google/android/apps/chrome/feedback/LegacyFeedbackReporter$1;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/chrome/feedback/LegacyFeedbackReporter$1;-><init>(Lcom/google/android/apps/chrome/feedback/LegacyFeedbackReporter;Landroid/graphics/Bitmap;)V

    .line 72
    iget-object v2, p0, Lcom/google/android/apps/chrome/feedback/LegacyFeedbackReporter;->mApplicationContext:Landroid/content/Context;

    const/4 v3, 0x1

    invoke-virtual {v2, v0, v1, v3}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    goto :goto_0
.end method
