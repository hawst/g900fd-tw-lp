.class public interface abstract Lcom/google/android/apps/chrome/tabs/layout/LayoutUpdateHost;
.super Ljava/lang/Object;
.source "LayoutUpdateHost.java"


# virtual methods
.method public abstract createLayoutTab(IZZZFF)Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;
.end method

.method public abstract doneHiding()V
.end method

.method public abstract initLayoutTabFromHost(I)V
.end method

.method public abstract isActiveLayout(Lcom/google/android/apps/chrome/tabs/layout/Layout;)Z
.end method

.method public abstract releaseTabLayout(I)V
.end method

.method public abstract requestUpdate()V
.end method

.method public abstract startHiding(I)V
.end method
