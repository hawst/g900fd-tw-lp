.class public Lcom/google/android/apps/chrome/omnibox/LocationBar;
.super Landroid/widget/FrameLayout;
.source "LocationBar.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/google/android/apps/chrome/omnibox/UrlBar$UrlBarDelegate;
.implements Lorg/chromium/chrome/browser/omnibox/AutocompleteController$OnSuggestionsReceivedListener;
.implements Lorg/chromium/ui/base/WindowAndroid$IntentCallback;


# static fields
.field static final synthetic $assertionsDisabled:Z

.field private static final ACCEPTED_SCHEMES:Ljava/util/HashSet;

.field private static final CONTENT_OVERLAY_COLOR:I

.field private static final INVALID_CORPUS_CHIPS:[Ljava/lang/String;

.field private static final OMNIBOX_INCOGNITO_RESULTS_BG_COLOR:I

.field private static final OMNIBOX_RESULTS_BG_COLOR:I


# instance fields
.field mAutocomplete:Lcom/google/android/apps/chrome/omnibox/ChromeAutocompleteController;

.field private mContextualMenuBar:Lcom/google/android/apps/chrome/ContextualMenuBar;

.field private final mCorpusChipTextView:Landroid/widget/TextView;

.field private final mDeferredNativeRunnables:Ljava/util/List;

.field protected mDeleteButton:Landroid/widget/ImageButton;

.field private mDeleteCorpusChipText:Z

.field private mFadeInOmniboxBackgroundAnimator:Landroid/animation/ObjectAnimator;

.field private mFadeOutOmniboxBackgroundAnimator:Landroid/animation/ObjectAnimator;

.field private mFirstFocusTimeMs:J

.field private mHasRecordedUrlFocusSource:Z

.field private mHasStartedNewOmniboxEditSession:Z

.field private mIgnoreOmniboxItemSelection:Z

.field private mIgnoreURLBarModification:Z

.field private mLastUrlEditWasDelete:Z

.field private mLoadUrlDelegate:Lcom/google/android/apps/chrome/omnibox/LocationBar$LoadUrlDelegate;

.field private mLocationBarIconActiveAnimator:Landroid/animation/AnimatorSet;

.field protected mMicButton:Landroid/widget/ImageButton;

.field private mNativeInitialized:Z

.field protected mNavigationButton:Landroid/widget/ImageView;

.field private mNavigationButtonType:Lcom/google/android/apps/chrome/omnibox/LocationBar$NavigationButtonType;

.field private mNavigationIconShowAnimator:Landroid/animation/AnimatorSet;

.field private mNewOmniboxEditSessionTimestamp:J

.field private mOmniboxBackgroundAnimator:Landroid/animation/Animator;

.field private mOmniboxPrerender:Lorg/chromium/chrome/browser/omnibox/OmniboxPrerender;

.field private mOmniboxResultsContainer:Landroid/view/ViewGroup;

.field private mOriginalUrl:Ljava/lang/String;

.field private mQueryInTheOmnibox:Z

.field private final mReaderModeLabel:Landroid/widget/TextView;

.field private mRequestSuggestions:Ljava/lang/Runnable;

.field protected mSecurityButton:Landroid/widget/ImageButton;

.field private mSecurityButtonShowAnimator:Landroid/animation/AnimatorSet;

.field private mSecurityButtonShown:Z

.field private mSecurityIconType:I

.field private mShouldSkipAnimation:Z

.field private mShowingOriginalUrlForPreview:Z

.field private final mSnapshotPrefixLabel:Landroid/widget/TextView;

.field private final mSuggestionItems:Ljava/util/List;

.field private mSuggestionList:Lcom/google/android/apps/chrome/omnibox/LocationBar$OmniboxSuggestionsList;

.field private final mSuggestionListAdapter:Lcom/google/android/apps/chrome/omnibox/OmniboxResultsAdapter;

.field private mSuggestionModalShown:Z

.field private mSuggestionSelectionInProgress:Z

.field private mSuggestionsShown:Z

.field private mTextWatcher:Landroid/text/TextWatcher;

.field protected mToolbar:Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;

.field private mToolbarDataProvider:Lcom/google/android/apps/chrome/toolbar/ToolbarDataProvider;

.field protected mUrlBar:Lcom/google/android/apps/chrome/omnibox/UrlBar;

.field private mUrlContainer:Lcom/google/android/apps/chrome/omnibox/UrlContainer;

.field private mUrlFocusChangeListener:Lcom/google/android/apps/chrome/omnibox/UrlFocusChangeListener;

.field private mUrlFocusedFromFakebox:Z

.field private mUrlHasFocus:Z

.field private mUrlTextAfterSuggestionsReceived:Ljava/lang/String;

.field private mUseDarkColors:Z

.field private mWindowAndroid:Lorg/chromium/ui/base/WindowAndroid;

.field private mWindowDelegate:Lcom/google/android/apps/chrome/WindowDelegate;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x3

    const/4 v6, 0x2

    const/16 v5, 0x32

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 122
    const-class v0, Lcom/google/android/apps/chrome/omnibox/LocationBar;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->$assertionsDisabled:Z

    .line 140
    const/16 v0, 0xa6

    invoke-static {v0, v2, v2, v2}, Landroid/graphics/Color;->argb(IIII)I

    move-result v0

    sput v0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->CONTENT_OVERLAY_COLOR:I

    .line 141
    const/16 v0, 0xf5

    const/16 v3, 0xf5

    const/16 v4, 0xf6

    invoke-static {v0, v3, v4}, Landroid/graphics/Color;->rgb(III)I

    move-result v0

    sput v0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->OMNIBOX_RESULTS_BG_COLOR:I

    .line 142
    invoke-static {v5, v5, v5}, Landroid/graphics/Color;->rgb(III)I

    move-result v0

    sput v0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->OMNIBOX_INCOGNITO_RESULTS_BG_COLOR:I

    .line 152
    const/16 v0, 0x9

    new-array v0, v0, [Ljava/lang/String;

    const-string/jumbo v3, "about"

    aput-object v3, v0, v2

    const-string/jumbo v3, "data"

    aput-object v3, v0, v1

    const-string/jumbo v3, "file"

    aput-object v3, v0, v6

    const-string/jumbo v3, "ftp"

    aput-object v3, v0, v7

    const/4 v3, 0x4

    const-string/jumbo v4, "http"

    aput-object v4, v0, v3

    const/4 v3, 0x5

    const-string/jumbo v4, "https"

    aput-object v4, v0, v3

    const/4 v3, 0x6

    const-string/jumbo v4, "inline"

    aput-object v4, v0, v3

    const/4 v3, 0x7

    const-string/jumbo v4, "javascript"

    aput-object v4, v0, v3

    const/16 v3, 0x8

    const-string/jumbo v4, "chrome"

    aput-object v4, v0, v3

    invoke-static {v0}, Lorg/chromium/base/CollectionUtil;->newHashSet([Ljava/lang/Object;)Ljava/util/HashSet;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->ACCEPTED_SCHEMES:Ljava/util/HashSet;

    .line 155
    const/16 v0, 0x8

    new-array v0, v0, [Ljava/lang/String;

    const-string/jumbo v3, "http"

    aput-object v3, v0, v2

    const-string/jumbo v2, "https"

    aput-object v2, v0, v1

    const-string/jumbo v1, "chrome"

    aput-object v1, v0, v6

    const-string/jumbo v1, "chrome-search"

    aput-object v1, v0, v7

    const/4 v1, 0x4

    const-string/jumbo v2, "about"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string/jumbo v2, "ftp"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string/jumbo v2, "ws"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string/jumbo v2, "wss"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->INVALID_CORPUS_CHIPS:[Ljava/lang/String;

    return-void

    :cond_0
    move v0, v2

    .line 122
    goto/16 :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 756
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 164
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mContextualMenuBar:Lcom/google/android/apps/chrome/ContextualMenuBar;

    .line 180
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mDeferredNativeRunnables:Ljava/util/List;

    .line 202
    iput-boolean v3, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mIgnoreURLBarModification:Z

    .line 203
    iput-boolean v3, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mIgnoreOmniboxItemSelection:Z

    .line 205
    iput-boolean v2, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mLastUrlEditWasDelete:Z

    .line 208
    iput-boolean v2, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mQueryInTheOmnibox:Z

    .line 213
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mOriginalUrl:Ljava/lang/String;

    .line 235
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mNewOmniboxEditSessionTimestamp:J

    .line 758
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Lcom/google/android/apps/chrome/R$layout;->location_bar:I

    invoke-virtual {v0, v1, p0, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 759
    sget v0, Lcom/google/android/apps/chrome/R$id;->navigation_button:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mNavigationButton:Landroid/widget/ImageView;

    .line 760
    sget-boolean v0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mNavigationButton:Landroid/widget/ImageView;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    const-string/jumbo v1, "Missing navigation type view."

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 761
    :cond_0
    invoke-static {p1}, Lorg/chromium/ui/base/DeviceFormFactor;->isTablet(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Lcom/google/android/apps/chrome/omnibox/LocationBar$NavigationButtonType;->PAGE:Lcom/google/android/apps/chrome/omnibox/LocationBar$NavigationButtonType;

    :goto_0
    iput-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mNavigationButtonType:Lcom/google/android/apps/chrome/omnibox/LocationBar$NavigationButtonType;

    .line 764
    sget v0, Lcom/google/android/apps/chrome/R$id;->security_button:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mSecurityButton:Landroid/widget/ImageButton;

    .line 765
    iput v2, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mSecurityIconType:I

    .line 767
    sget v0, Lcom/google/android/apps/chrome/R$id;->reader_mode_label:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mReaderModeLabel:Landroid/widget/TextView;

    .line 768
    sget-boolean v0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->$assertionsDisabled:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mReaderModeLabel:Landroid/widget/TextView;

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    const-string/jumbo v1, "Missing reader mode view."

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 761
    :cond_1
    sget-object v0, Lcom/google/android/apps/chrome/omnibox/LocationBar$NavigationButtonType;->EMPTY:Lcom/google/android/apps/chrome/omnibox/LocationBar$NavigationButtonType;

    goto :goto_0

    .line 770
    :cond_2
    sget v0, Lcom/google/android/apps/chrome/R$id;->snapshot_prefix_label:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mSnapshotPrefixLabel:Landroid/widget/TextView;

    .line 771
    sget-boolean v0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->$assertionsDisabled:Z

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mSnapshotPrefixLabel:Landroid/widget/TextView;

    if-nez v0, :cond_3

    new-instance v0, Ljava/lang/AssertionError;

    const-string/jumbo v1, "Missing snapshot prefix view."

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 773
    :cond_3
    sget v0, Lcom/google/android/apps/chrome/R$id;->corpus_chip_text_view:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mCorpusChipTextView:Landroid/widget/TextView;

    .line 775
    sget v0, Lcom/google/android/apps/chrome/R$id;->delete_button:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mDeleteButton:Landroid/widget/ImageButton;

    .line 777
    sget v0, Lcom/google/android/apps/chrome/R$id;->url_bar:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/omnibox/UrlBar;

    iput-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mUrlBar:Lcom/google/android/apps/chrome/omnibox/UrlBar;

    .line 778
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mUrlBar:Lcom/google/android/apps/chrome/omnibox/UrlBar;

    sget v1, Lcom/google/android/apps/chrome/R$string;->search_or_type_url:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/omnibox/UrlBar;->setHint(I)V

    .line 786
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v1, "default_input_method"

    invoke-static {v0, v1}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 788
    if-eqz v0, :cond_4

    const-string/jumbo v1, "com.htc.android.htcime"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 789
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mUrlBar:Lcom/google/android/apps/chrome/omnibox/UrlBar;

    iget-object v1, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mUrlBar:Lcom/google/android/apps/chrome/omnibox/UrlBar;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/omnibox/UrlBar;->getInputType()I

    move-result v1

    or-int/lit16 v1, v1, 0xb0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/omnibox/UrlBar;->setInputType(I)V

    .line 791
    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mUrlBar:Lcom/google/android/apps/chrome/omnibox/UrlBar;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/chrome/omnibox/UrlBar;->setDelegate(Lcom/google/android/apps/chrome/omnibox/UrlBar$UrlBarDelegate;)V

    .line 793
    sget v0, Lcom/google/android/apps/chrome/R$id;->url_container:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/omnibox/UrlContainer;

    iput-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mUrlContainer:Lcom/google/android/apps/chrome/omnibox/UrlContainer;

    .line 795
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mSuggestionItems:Ljava/util/List;

    .line 796
    new-instance v0, Lcom/google/android/apps/chrome/omnibox/OmniboxResultsAdapter;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mSuggestionItems:Ljava/util/List;

    invoke-direct {v0, v1, p0, v2}, Lcom/google/android/apps/chrome/omnibox/OmniboxResultsAdapter;-><init>(Landroid/content/Context;Lcom/google/android/apps/chrome/omnibox/LocationBar;Ljava/util/List;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mSuggestionListAdapter:Lcom/google/android/apps/chrome/omnibox/OmniboxResultsAdapter;

    .line 798
    sget v0, Lcom/google/android/apps/chrome/R$id;->mic_button:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mMicButton:Landroid/widget/ImageButton;

    .line 799
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/chrome/omnibox/LocationBar;)Z
    .locals 1

    .prologue
    .line 122
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mDeleteCorpusChipText:Z

    return v0
.end method

.method static synthetic access$002(Lcom/google/android/apps/chrome/omnibox/LocationBar;Z)Z
    .locals 0

    .prologue
    .line 122
    iput-boolean p1, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mDeleteCorpusChipText:Z

    return p1
.end method

.method static synthetic access$100(Lcom/google/android/apps/chrome/omnibox/LocationBar;)V
    .locals 0

    .prologue
    .line 122
    invoke-direct {p0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->updateDeleteButtonVisibility()V

    return-void
.end method

.method static synthetic access$1000(Lcom/google/android/apps/chrome/omnibox/LocationBar;)Ljava/lang/Runnable;
    .locals 1

    .prologue
    .line 122
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mRequestSuggestions:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$1002(Lcom/google/android/apps/chrome/omnibox/LocationBar;Ljava/lang/Runnable;)Ljava/lang/Runnable;
    .locals 0

    .prologue
    .line 122
    iput-object p1, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mRequestSuggestions:Ljava/lang/Runnable;

    return-object p1
.end method

.method static synthetic access$1100(Lcom/google/android/apps/chrome/omnibox/LocationBar;)Z
    .locals 1

    .prologue
    .line 122
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mLastUrlEditWasDelete:Z

    return v0
.end method

.method static synthetic access$1102(Lcom/google/android/apps/chrome/omnibox/LocationBar;Z)Z
    .locals 0

    .prologue
    .line 122
    iput-boolean p1, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mLastUrlEditWasDelete:Z

    return p1
.end method

.method static synthetic access$1200(Lcom/google/android/apps/chrome/omnibox/LocationBar;)Z
    .locals 1

    .prologue
    .line 122
    invoke-direct {p0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->shouldAutocomplete()Z

    move-result v0

    return v0
.end method

.method static synthetic access$1300(Lcom/google/android/apps/chrome/omnibox/LocationBar;)Ljava/util/List;
    .locals 1

    .prologue
    .line 122
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mDeferredNativeRunnables:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/google/android/apps/chrome/omnibox/LocationBar;)V
    .locals 0

    .prologue
    .line 122
    invoke-direct {p0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->cancelPendingAutocompleteStart()V

    return-void
.end method

.method static synthetic access$1500(Lcom/google/android/apps/chrome/omnibox/LocationBar;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 122
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mCorpusChipTextView:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$1600(Lcom/google/android/apps/chrome/omnibox/LocationBar;)Lcom/google/android/apps/chrome/omnibox/OmniboxResultsAdapter;
    .locals 1

    .prologue
    .line 122
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mSuggestionListAdapter:Lcom/google/android/apps/chrome/omnibox/OmniboxResultsAdapter;

    return-object v0
.end method

.method static synthetic access$1700(Lcom/google/android/apps/chrome/omnibox/LocationBar;)Z
    .locals 1

    .prologue
    .line 122
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mIgnoreOmniboxItemSelection:Z

    return v0
.end method

.method static synthetic access$1702(Lcom/google/android/apps/chrome/omnibox/LocationBar;Z)Z
    .locals 0

    .prologue
    .line 122
    iput-boolean p1, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mIgnoreOmniboxItemSelection:Z

    return p1
.end method

.method static synthetic access$1800(Lcom/google/android/apps/chrome/omnibox/LocationBar;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 122
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->setUrlBarText(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$1902(Lcom/google/android/apps/chrome/omnibox/LocationBar;Z)Z
    .locals 0

    .prologue
    .line 122
    iput-boolean p1, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mSuggestionSelectionInProgress:Z

    return p1
.end method

.method static synthetic access$200(Lcom/google/android/apps/chrome/omnibox/LocationBar;)V
    .locals 0

    .prologue
    .line 122
    invoke-direct {p0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->updateNavigationButton()V

    return-void
.end method

.method static synthetic access$2100(Lcom/google/android/apps/chrome/omnibox/LocationBar;)Ljava/util/List;
    .locals 1

    .prologue
    .line 122
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mSuggestionItems:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$2200(Lcom/google/android/apps/chrome/omnibox/LocationBar;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 122
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mUrlTextAfterSuggestionsReceived:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$2300(Lcom/google/android/apps/chrome/omnibox/LocationBar;Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion;I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 122
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->updateSuggestionUrlIfNeeded(Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion;I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$2400(Lcom/google/android/apps/chrome/omnibox/LocationBar;Ljava/lang/String;IILorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;)V
    .locals 0

    .prologue
    .line 122
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->loadUrlFromOmniboxMatch(Ljava/lang/String;IILorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;)V

    return-void
.end method

.method static synthetic access$2500(Lcom/google/android/apps/chrome/omnibox/LocationBar;I)V
    .locals 0

    .prologue
    .line 122
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->updateSuggestionsLayoutDirection(I)V

    return-void
.end method

.method static synthetic access$2600(Lcom/google/android/apps/chrome/omnibox/LocationBar;)Z
    .locals 1

    .prologue
    .line 122
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mSuggestionModalShown:Z

    return v0
.end method

.method static synthetic access$2602(Lcom/google/android/apps/chrome/omnibox/LocationBar;Z)Z
    .locals 0

    .prologue
    .line 122
    iput-boolean p1, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mSuggestionModalShown:Z

    return p1
.end method

.method static synthetic access$2700(Lcom/google/android/apps/chrome/omnibox/LocationBar;)Landroid/animation/AnimatorSet;
    .locals 1

    .prologue
    .line 122
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mSecurityButtonShowAnimator:Landroid/animation/AnimatorSet;

    return-object v0
.end method

.method static synthetic access$2800(Lcom/google/android/apps/chrome/omnibox/LocationBar;)Landroid/animation/AnimatorSet;
    .locals 1

    .prologue
    .line 122
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mNavigationIconShowAnimator:Landroid/animation/AnimatorSet;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/apps/chrome/omnibox/LocationBar;)Z
    .locals 1

    .prologue
    .line 122
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mIgnoreURLBarModification:Z

    return v0
.end method

.method static synthetic access$3100(Lcom/google/android/apps/chrome/omnibox/LocationBar;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 122
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mOriginalUrl:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$3400(Lcom/google/android/apps/chrome/omnibox/LocationBar;Z)V
    .locals 0

    .prologue
    .line 122
    iput-boolean p1, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mShouldSkipAnimation:Z

    return-void
.end method

.method static synthetic access$3500(Lcom/google/android/apps/chrome/omnibox/LocationBar;)Lcom/google/android/apps/chrome/omnibox/LocationBar$LoadUrlDelegate;
    .locals 1

    .prologue
    .line 122
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mLoadUrlDelegate:Lcom/google/android/apps/chrome/omnibox/LocationBar$LoadUrlDelegate;

    return-object v0
.end method

.method static synthetic access$3600(Lcom/google/android/apps/chrome/omnibox/LocationBar;Z)V
    .locals 0

    .prologue
    .line 122
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->updateOmniboxResultsContainerBackground(Z)V

    return-void
.end method

.method static synthetic access$3700(Lcom/google/android/apps/chrome/omnibox/LocationBar;)Landroid/view/ViewGroup;
    .locals 1

    .prologue
    .line 122
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mOmniboxResultsContainer:Landroid/view/ViewGroup;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/apps/chrome/omnibox/LocationBar;)Z
    .locals 1

    .prologue
    .line 122
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mHasStartedNewOmniboxEditSession:Z

    return v0
.end method

.method static synthetic access$402(Lcom/google/android/apps/chrome/omnibox/LocationBar;Z)Z
    .locals 0

    .prologue
    .line 122
    iput-boolean p1, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mHasStartedNewOmniboxEditSession:Z

    return p1
.end method

.method static synthetic access$500(Lcom/google/android/apps/chrome/omnibox/LocationBar;)Z
    .locals 1

    .prologue
    .line 122
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mNativeInitialized:Z

    return v0
.end method

.method static synthetic access$602(Lcom/google/android/apps/chrome/omnibox/LocationBar;J)J
    .locals 1

    .prologue
    .line 122
    iput-wide p1, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mNewOmniboxEditSessionTimestamp:J

    return-wide p1
.end method

.method static synthetic access$700(Lcom/google/android/apps/chrome/omnibox/LocationBar;)Lcom/google/android/apps/chrome/omnibox/LocationBar$OmniboxSuggestionsList;
    .locals 1

    .prologue
    .line 122
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mSuggestionList:Lcom/google/android/apps/chrome/omnibox/LocationBar$OmniboxSuggestionsList;

    return-object v0
.end method

.method static synthetic access$800(Lcom/google/android/apps/chrome/omnibox/LocationBar;Z)V
    .locals 0

    .prologue
    .line 122
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->stopAutocomplete(Z)V

    return-void
.end method

.method static synthetic access$900(Lcom/google/android/apps/chrome/omnibox/LocationBar;)V
    .locals 0

    .prologue
    .line 122
    invoke-direct {p0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->startZeroSuggest()V

    return-void
.end method

.method private addCorpusChipSpan()V
    .locals 2

    .prologue
    .line 512
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->isCorpusChipTextDisplayed()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mUrlBar:Lcom/google/android/apps/chrome/omnibox/UrlBar;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/omnibox/UrlBar;->getEditableText()Landroid/text/Editable;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/chrome/omnibox/UrlBar$CorpusChipPlaceholderSpan;->doesSpanExist(Landroid/text/Editable;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 514
    new-instance v0, Lcom/google/android/apps/chrome/omnibox/UrlBar$CorpusChipPlaceholderSpan;

    invoke-direct {v0}, Lcom/google/android/apps/chrome/omnibox/UrlBar$CorpusChipPlaceholderSpan;-><init>()V

    iget-object v1, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mUrlBar:Lcom/google/android/apps/chrome/omnibox/UrlBar;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/omnibox/UrlBar;->getEditableText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/omnibox/UrlBar$CorpusChipPlaceholderSpan;->addSpan(Landroid/text/Editable;)V

    .line 516
    :cond_0
    return-void
.end method

.method private cancelPendingAutocompleteStart()V
    .locals 2

    .prologue
    .line 1778
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mRequestSuggestions:Ljava/lang/Runnable;

    if-eqz v0, :cond_1

    .line 1781
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mDeferredNativeRunnables:Ljava/util/List;

    iget-object v1, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mRequestSuggestions:Ljava/lang/Runnable;

    invoke-interface {v0, v1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1782
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mRequestSuggestions:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 1784
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mRequestSuggestions:Ljava/lang/Runnable;

    .line 1786
    :cond_1
    return-void
.end method

.method private changeLocationBarIcon(Z)V
    .locals 4

    .prologue
    .line 1007
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mLocationBarIconActiveAnimator:Landroid/animation/AnimatorSet;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mLocationBarIconActiveAnimator:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1008
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mLocationBarIconActiveAnimator:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->cancel()V

    .line 1010
    :cond_0
    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mSecurityButton:Landroid/widget/ImageButton;

    .line 1011
    :goto_0
    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v1

    if-nez v1, :cond_2

    invoke-virtual {v0}, Landroid/view/View;->getAlpha()F

    move-result v0

    const/high16 v1, 0x3f800000    # 1.0f

    cmpl-float v0, v0, v1

    if-nez v0, :cond_2

    .line 1025
    :goto_1
    return-void

    .line 1010
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mNavigationButton:Landroid/widget/ImageView;

    goto :goto_0

    .line 1014
    :cond_2
    if-eqz p1, :cond_3

    .line 1015
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mSecurityButtonShowAnimator:Landroid/animation/AnimatorSet;

    iput-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mLocationBarIconActiveAnimator:Landroid/animation/AnimatorSet;

    .line 1019
    :goto_2
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->shouldAnimateIconChanges()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1020
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mLocationBarIconActiveAnimator:Landroid/animation/AnimatorSet;

    const-wide/16 v2, 0xfa

    invoke-virtual {v0, v2, v3}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    .line 1024
    :goto_3
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mLocationBarIconActiveAnimator:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->start()V

    goto :goto_1

    .line 1017
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mNavigationIconShowAnimator:Landroid/animation/AnimatorSet;

    iput-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mLocationBarIconActiveAnimator:Landroid/animation/AnimatorSet;

    goto :goto_2

    .line 1022
    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mLocationBarIconActiveAnimator:Landroid/animation/AnimatorSet;

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    goto :goto_3
.end method

.method private clearSuggestions(Z)V
    .locals 1

    .prologue
    .line 1705
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mSuggestionItems:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 1709
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mSuggestionListAdapter:Lcom/google/android/apps/chrome/omnibox/OmniboxResultsAdapter;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/omnibox/OmniboxResultsAdapter;->notifyDataSetChanged()V

    .line 1710
    :cond_0
    return-void
.end method

.method private fadeOutOmniboxResultsContainerBackground()V
    .locals 4

    .prologue
    .line 2399
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mFadeOutOmniboxBackgroundAnimator:Landroid/animation/ObjectAnimator;

    if-nez v0, :cond_0

    .line 2400
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->getRootView()Landroid/view/View;

    move-result-object v0

    sget v1, Lcom/google/android/apps/chrome/R$id;->omnibox_results_container:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    const-string/jumbo v1, "alpha"

    const/4 v2, 0x2

    new-array v2, v2, [I

    fill-array-data v2, :array_0

    invoke-static {v0, v1, v2}, Landroid/animation/ObjectAnimator;->ofInt(Ljava/lang/Object;Ljava/lang/String;[I)Landroid/animation/ObjectAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mFadeOutOmniboxBackgroundAnimator:Landroid/animation/ObjectAnimator;

    .line 2403
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mFadeOutOmniboxBackgroundAnimator:Landroid/animation/ObjectAnimator;

    const-wide/16 v2, 0xfa

    invoke-virtual {v0, v2, v3}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 2404
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mFadeOutOmniboxBackgroundAnimator:Landroid/animation/ObjectAnimator;

    sget-object v1, Lorg/chromium/ui/interpolators/BakedBezierInterpolator;->FADE_OUT_CURVE:Lorg/chromium/ui/interpolators/BakedBezierInterpolator;

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 2406
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mFadeOutOmniboxBackgroundAnimator:Landroid/animation/ObjectAnimator;

    new-instance v1, Lcom/google/android/apps/chrome/omnibox/LocationBar$13;

    invoke-direct {v1, p0}, Lcom/google/android/apps/chrome/omnibox/LocationBar$13;-><init>(Lcom/google/android/apps/chrome/omnibox/LocationBar;)V

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 2426
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mFadeOutOmniboxBackgroundAnimator:Landroid/animation/ObjectAnimator;

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->runOmniboxResultsFadeAnimation(Landroid/animation/Animator;)V

    .line 2427
    return-void

    .line 2400
    nop

    :array_0
    .array-data 4
        0xff
        0x0
    .end array-data
.end method

.method private getContentViewCore()Lorg/chromium/content/browser/ContentViewCore;
    .locals 1

    .prologue
    .line 2328
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->getCurrentTab()Lcom/google/android/apps/chrome/tab/ChromeTab;

    move-result-object v0

    .line 2329
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/Tab;->getContentViewCore()Lorg/chromium/content/browser/ContentViewCore;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static getSecurityIconResource(I)I
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1285
    packed-switch p0, :pswitch_data_0

    .line 1296
    :pswitch_0
    sget-boolean v1, Lcom/google/android/apps/chrome/omnibox/LocationBar;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 1289
    :pswitch_1
    sget v0, Lcom/google/android/apps/chrome/R$drawable;->omnibox_https_warning:I

    .line 1298
    :cond_0
    :goto_0
    :pswitch_2
    return v0

    .line 1291
    :pswitch_3
    sget v0, Lcom/google/android/apps/chrome/R$drawable;->omnibox_https_invalid:I

    goto :goto_0

    .line 1294
    :pswitch_4
    sget v0, Lcom/google/android/apps/chrome/R$drawable;->omnibox_https_valid:I

    goto :goto_0

    .line 1285
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_4
        :pswitch_4
        :pswitch_1
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method private getSecurityLevel()I
    .locals 1

    .prologue
    .line 1276
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->getCurrentTab()Lcom/google/android/apps/chrome/tab/ChromeTab;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 1277
    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->getCurrentTab()Lcom/google/android/apps/chrome/tab/ChromeTab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getSecurityLevel()I

    move-result v0

    goto :goto_0
.end method

.method private initSuggestionList()V
    .locals 3

    .prologue
    .line 1546
    sget-boolean v0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mNativeInitialized:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    const-string/jumbo v1, "Trying to initialize suggestions list before native init"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 1547
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mSuggestionList:Lcom/google/android/apps/chrome/omnibox/LocationBar$OmniboxSuggestionsList;

    if-eqz v0, :cond_1

    .line 1632
    :goto_0
    return-void

    .line 1549
    :cond_1
    new-instance v0, Lcom/google/android/apps/chrome/omnibox/LocationBar$7;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/omnibox/LocationBar$7;-><init>(Lcom/google/android/apps/chrome/omnibox/LocationBar;)V

    .line 1563
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->getRootView()Landroid/view/View;

    move-result-object v1

    sget v2, Lcom/google/android/apps/chrome/R$id;->control_container:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/View;->addOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    .line 1566
    new-instance v0, Lcom/google/android/apps/chrome/omnibox/LocationBar$OmniboxSuggestionsList;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/chrome/omnibox/LocationBar$OmniboxSuggestionsList;-><init>(Lcom/google/android/apps/chrome/omnibox/LocationBar;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mSuggestionList:Lcom/google/android/apps/chrome/omnibox/LocationBar$OmniboxSuggestionsList;

    .line 1567
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mOmniboxResultsContainer:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mSuggestionList:Lcom/google/android/apps/chrome/omnibox/LocationBar$OmniboxSuggestionsList;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 1568
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mSuggestionList:Lcom/google/android/apps/chrome/omnibox/LocationBar$OmniboxSuggestionsList;

    iget-object v1, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mSuggestionListAdapter:Lcom/google/android/apps/chrome/omnibox/OmniboxResultsAdapter;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/omnibox/LocationBar$OmniboxSuggestionsList;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 1569
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mSuggestionList:Lcom/google/android/apps/chrome/omnibox/LocationBar$OmniboxSuggestionsList;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/omnibox/LocationBar$OmniboxSuggestionsList;->setClipToPadding(Z)V

    .line 1570
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mSuggestionListAdapter:Lcom/google/android/apps/chrome/omnibox/OmniboxResultsAdapter;

    new-instance v1, Lcom/google/android/apps/chrome/omnibox/LocationBar$8;

    invoke-direct {v1, p0}, Lcom/google/android/apps/chrome/omnibox/LocationBar$8;-><init>(Lcom/google/android/apps/chrome/omnibox/LocationBar;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/omnibox/OmniboxResultsAdapter;->setSuggestionDelegate(Lcom/google/android/apps/chrome/omnibox/OmniboxResultsAdapter$OmniboxSuggestionDelegate;)V

    goto :goto_0
.end method

.method private isStoredArticle(Ljava/lang/String;)Z
    .locals 3

    .prologue
    .line 1417
    invoke-static {}, Lorg/chromium/chrome/browser/profiles/Profile;->getLastUsedProfile()Lorg/chromium/chrome/browser/profiles/Profile;

    move-result-object v0

    invoke-static {v0}, Lorg/chromium/chrome/browser/dom_distiller/DomDistillerServiceFactory;->getForProfile(Lorg/chromium/chrome/browser/profiles/Profile;)Lorg/chromium/components/dom_distiller/core/DomDistillerService;

    move-result-object v0

    .line 1419
    const-string/jumbo v1, "entry_id"

    invoke-static {p1, v1}, Lorg/chromium/components/dom_distiller/core/DomDistillerUrlUtils;->getValueForKeyInUrl(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1420
    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v0, 0x0

    .line 1421
    :goto_0
    return v0

    :cond_0
    invoke-virtual {v0, v1}, Lorg/chromium/components/dom_distiller/core/DomDistillerService;->hasEntry(Ljava/lang/String;)Z

    move-result v0

    goto :goto_0
.end method

.method private isUrl(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 1990
    :try_start_0
    new-instance v0, Ljava/net/URL;

    invoke-direct {v0, p1}, Ljava/net/URL;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1994
    const/4 v0, 0x1

    :goto_0
    return v0

    .line 1992
    :catch_0
    move-exception v0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method private loadUrl(Ljava/lang/String;ILorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;)V
    .locals 7

    .prologue
    const/4 v3, 0x1

    .line 2231
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->getCurrentTab()Lcom/google/android/apps/chrome/tab/ChromeTab;

    move-result-object v0

    .line 2234
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getUrl()Ljava/lang/String;

    move-result-object v2

    .line 2235
    :goto_0
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->isIncognito()Z

    move-result v4

    .line 2236
    :goto_1
    iget-object v1, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mLoadUrlDelegate:Lcom/google/android/apps/chrome/omnibox/LocationBar$LoadUrlDelegate;

    if-eqz v1, :cond_2

    invoke-direct {p0, p3}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->shouldPassLoadUrlToDelegate(Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mLoadUrlDelegate:Lcom/google/android/apps/chrome/omnibox/LocationBar$LoadUrlDelegate;

    invoke-interface {v1, v2, p1, v4}, Lcom/google/android/apps/chrome/omnibox/LocationBar$LoadUrlDelegate;->canHandleLoadUrl(Ljava/lang/String;Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2238
    iput-boolean v3, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mShouldSkipAnimation:Z

    .line 2239
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mUrlBar:Lcom/google/android/apps/chrome/omnibox/UrlBar;

    invoke-static {v0}, Lorg/chromium/ui/UiUtils;->hideKeyboard(Landroid/view/View;)Z

    .line 2240
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->getCurrentTab()Lcom/google/android/apps/chrome/tab/ChromeTab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->requestFocus()V

    .line 2241
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p1, v4}, Lcom/google/android/apps/chrome/omnibox/GeolocationHeader;->getGeoHeader(Landroid/content/Context;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v6

    .line 2244
    new-instance v0, Lcom/google/android/apps/chrome/omnibox/LocationBar$11;

    move-object v1, p0

    move-object v3, p1

    move v5, p2

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/chrome/omnibox/LocationBar$11;-><init>(Lcom/google/android/apps/chrome/omnibox/LocationBar;Ljava/lang/String;Ljava/lang/String;ZILjava/lang/String;)V

    .line 2252
    const-wide/16 v2, 0x28

    invoke-virtual {p0, v0, v2, v3}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 2289
    :goto_2
    return-void

    .line 2234
    :cond_0
    const-string/jumbo v2, ""

    goto :goto_0

    .line 2235
    :cond_1
    const/4 v4, 0x0

    goto :goto_1

    .line 2258
    :cond_2
    sget-boolean v1, Lcom/google/android/apps/chrome/omnibox/LocationBar;->$assertionsDisabled:Z

    if-nez v1, :cond_3

    iget-boolean v1, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mNativeInitialized:Z

    if-nez v1, :cond_3

    new-instance v0, Ljava/lang/AssertionError;

    const-string/jumbo v1, "Loading URL before native side initialized"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 2260
    :cond_3
    if-eqz v0, :cond_5

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->isNativePage()Z

    move-result v1

    if-nez v1, :cond_4

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getUrl()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/chrome/ntp/NewTabPage;->isNTPUrl(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 2262
    :cond_4
    invoke-static {p1, p2}, Lcom/google/android/apps/chrome/ntp/NewTabPageUma;->recordOmniboxNavigation(Ljava/lang/String;I)V

    .line 2266
    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getUrl()Ljava/lang/String;

    move-result-object p1

    .line 2270
    :cond_5
    if-eqz v0, :cond_7

    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_7

    .line 2271
    new-instance v1, Lorg/chromium/content_public/browser/LoadUrlParams;

    invoke-direct {v1, p1}, Lorg/chromium/content_public/browser/LoadUrlParams;-><init>(Ljava/lang/String;)V

    .line 2272
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2, p1, v4}, Lcom/google/android/apps/chrome/omnibox/GeolocationHeader;->getGeoHeader(Landroid/content/Context;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lorg/chromium/content_public/browser/LoadUrlParams;->setVerbatimHeaders(Ljava/lang/String;)V

    .line 2274
    const/high16 v2, 0x2000000

    or-int/2addr v2, p2

    invoke-virtual {v1, v2}, Lorg/chromium/content_public/browser/LoadUrlParams;->setTransitionType(I)V

    .line 2276
    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/tab/ChromeTab;->loadUrl(Lorg/chromium/content_public/browser/LoadUrlParams;)I

    .line 2278
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->setUrlToPageUrl()V

    .line 2279
    invoke-static {}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->omniboxSearch()V

    .line 2284
    :goto_3
    if-eqz v0, :cond_6

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->requestFocus()V

    .line 2288
    :cond_6
    invoke-direct {p0, v3}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->stopAutocomplete(Z)V

    goto :goto_2

    .line 2281
    :cond_7
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->setUrlToPageUrl()V

    goto :goto_3
.end method

.method private loadUrlFromOmniboxMatch(Ljava/lang/String;IILorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;)V
    .locals 9

    .prologue
    .line 2219
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->getCurrentTab()Lcom/google/android/apps/chrome/tab/ChromeTab;

    move-result-object v0

    .line 2220
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/Tab;->getUrl()Ljava/lang/String;

    move-result-object v3

    .line 2221
    :goto_0
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/Tab;->getWebContents()Lorg/chromium/content_public/browser/WebContents;

    move-result-object v8

    .line 2222
    :goto_1
    iget-wide v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mNewOmniboxEditSessionTimestamp:J

    const-wide/16 v4, 0x0

    cmp-long v0, v0, v4

    if-lez v0, :cond_2

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iget-wide v4, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mNewOmniboxEditSessionTimestamp:J

    sub-long v6, v0, v4

    .line 2224
    :goto_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mAutocomplete:Lcom/google/android/apps/chrome/omnibox/ChromeAutocompleteController;

    iget-boolean v4, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mQueryInTheOmnibox:Z

    iget-boolean v5, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mUrlFocusedFromFakebox:Z

    move v1, p3

    move-object v2, p4

    invoke-virtual/range {v0 .. v8}, Lcom/google/android/apps/chrome/omnibox/ChromeAutocompleteController;->onSuggestionSelected(ILorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;Ljava/lang/String;ZZJLorg/chromium/content_public/browser/WebContents;)V

    .line 2227
    invoke-direct {p0, p1, p2, p4}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->loadUrl(Ljava/lang/String;ILorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;)V

    .line 2228
    return-void

    .line 2220
    :cond_0
    const-string/jumbo v3, ""

    goto :goto_0

    .line 2221
    :cond_1
    const/4 v8, 0x0

    goto :goto_1

    .line 2222
    :cond_2
    const-wide/16 v6, -0x1

    goto :goto_2
.end method

.method private recordSuggestionsDismissed()V
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 1744
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mSuggestionSelectionInProgress:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mSuggestionItems:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_1

    .line 1761
    :cond_0
    :goto_0
    return-void

    :cond_1
    move v1, v2

    .line 1747
    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mSuggestionItems:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 1748
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mSuggestionItems:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/omnibox/OmniboxResultsAdapter$OmniboxResultItem;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/omnibox/OmniboxResultsAdapter$OmniboxResultItem;->getSuggestion()Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion;

    move-result-object v0

    .line 1749
    invoke-virtual {v0}, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion;->hasAnswer()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 1751
    :try_start_0
    invoke-virtual {v0}, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion;->getAnswerType()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 1760
    :cond_2
    :goto_2
    invoke-static {v2}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->omniboxSuggestionsDismissed(I)V

    goto :goto_0

    .line 1753
    :catch_0
    move-exception v1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "Answer type in dismissed suggestions is not an int: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion;->getAnswerType()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 1747
    :cond_3
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1
.end method

.method private runOmniboxResultsFadeAnimation(Landroid/animation/Animator;)V
    .locals 1

    .prologue
    .line 2430
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mOmniboxBackgroundAnimator:Landroid/animation/Animator;

    if-ne v0, p1, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mOmniboxBackgroundAnimator:Landroid/animation/Animator;

    invoke-virtual {v0}, Landroid/animation/Animator;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2439
    :cond_0
    :goto_0
    return-void

    .line 2433
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mOmniboxBackgroundAnimator:Landroid/animation/Animator;

    if-eqz v0, :cond_2

    .line 2434
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mOmniboxBackgroundAnimator:Landroid/animation/Animator;

    invoke-virtual {v0}, Landroid/animation/Animator;->cancel()V

    .line 2436
    :cond_2
    iput-object p1, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mOmniboxBackgroundAnimator:Landroid/animation/Animator;

    .line 2437
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mOmniboxBackgroundAnimator:Landroid/animation/Animator;

    invoke-virtual {v0}, Landroid/animation/Animator;->start()V

    .line 2438
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->shouldSkipAnimation()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mOmniboxBackgroundAnimator:Landroid/animation/Animator;

    invoke-virtual {v0}, Landroid/animation/Animator;->end()V

    goto :goto_0
.end method

.method static sanitizePastedText(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2542
    invoke-static {p0}, Lorg/chromium/chrome/browser/omnibox/OmniboxViewUtil;->sanitizeTextForPaste(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private setNavigationButtonType(Lcom/google/android/apps/chrome/omnibox/LocationBar$NavigationButtonType;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1357
    sget-object v0, Lcom/google/android/apps/chrome/omnibox/LocationBar$14;->$SwitchMap$com$google$android$apps$chrome$omnibox$LocationBar$NavigationButtonType:[I

    invoke-virtual {p1}, Lcom/google/android/apps/chrome/omnibox/LocationBar$NavigationButtonType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1371
    sget-boolean v0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 1361
    :pswitch_0
    iget-object v1, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mNavigationButton:Landroid/widget/ImageView;

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mUseDarkColors:Z

    if-eqz v0, :cond_2

    sget v0, Lcom/google/android/apps/chrome/R$drawable;->ic_omnibox_page:I

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1374
    :cond_0
    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mNavigationButton:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_1

    .line 1375
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mNavigationButton:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1377
    :cond_1
    iput-object p1, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mNavigationButtonType:Lcom/google/android/apps/chrome/omnibox/LocationBar$NavigationButtonType;

    .line 1378
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->updateLocationBarIconContainerVisibility()V

    .line 1379
    return-void

    .line 1361
    :cond_2
    sget v0, Lcom/google/android/apps/chrome/R$drawable;->ic_suggestion_page_white:I

    goto :goto_0

    .line 1365
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mNavigationButton:Landroid/widget/ImageView;

    sget v1, Lcom/google/android/apps/chrome/R$drawable;->ic_omnibox_magnifier:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_1

    .line 1368
    :pswitch_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mNavigationButton:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_1

    .line 1357
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private setUrlBarText(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2150
    iput-boolean v1, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mQueryInTheOmnibox:Z

    .line 2151
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mIgnoreURLBarModification:Z

    .line 2152
    invoke-virtual {p0, v1}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->updateCorpusChipText(Z)V

    .line 2153
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mUrlContainer:Lcom/google/android/apps/chrome/omnibox/UrlContainer;

    invoke-virtual {v0, p2, p3, p1}, Lcom/google/android/apps/chrome/omnibox/UrlContainer;->setUrlText(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    .line 2154
    iput-boolean v1, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mIgnoreURLBarModification:Z

    .line 2155
    return v0
.end method

.method private shouldAutocomplete()Z
    .locals 2

    .prologue
    .line 1878
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mUrlBar:Lcom/google/android/apps/chrome/omnibox/UrlBar;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/omnibox/UrlBar;->getText()Landroid/text/Editable;

    move-result-object v0

    .line 1880
    invoke-static {v0}, Landroid/view/inputmethod/BaseInputConnection;->getComposingSpanEnd(Landroid/text/Spannable;)I

    move-result v1

    invoke-static {v0}, Landroid/view/inputmethod/BaseInputConnection;->getComposingSpanStart(Landroid/text/Spannable;)I

    move-result v0

    if-ne v1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private shouldPassLoadUrlToDelegate(Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;)Z
    .locals 1

    .prologue
    .line 2292
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->showingQueryInTheOmnibox()Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;->SEARCH_HISTORY:Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;

    if-eq p1, v0, :cond_0

    sget-object v0, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;->SEARCH_OTHER_ENGINE:Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;

    if-eq p1, v0, :cond_0

    sget-object v0, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;->SEARCH_SUGGEST:Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;

    if-eq p1, v0, :cond_0

    sget-object v0, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;->SEARCH_SUGGEST_ANSWER:Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;

    if-eq p1, v0, :cond_0

    sget-object v0, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;->SEARCH_SUGGEST_ENTITY:Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;

    if-eq p1, v0, :cond_0

    sget-object v0, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;->SEARCH_SUGGEST_INFINITE:Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;

    if-eq p1, v0, :cond_0

    sget-object v0, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;->SEARCH_SUGGEST_PERSONALIZED:Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;

    if-eq p1, v0, :cond_0

    sget-object v0, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;->SEARCH_SUGGEST_PROFILE:Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;

    if-eq p1, v0, :cond_0

    sget-object v0, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;->SEARCH_WHAT_YOU_TYPED:Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;

    if-ne p1, v0, :cond_1

    .line 2302
    :cond_0
    const/4 v0, 0x0

    .line 2304
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private shouldShowReaderModeLabel()Z
    .locals 1

    .prologue
    .line 1413
    const/4 v0, 0x0

    return v0
.end method

.method static splitPathFromUrlDisplayText(Ljava/lang/String;)Landroid/util/Pair;
    .locals 7

    .prologue
    const/4 v2, 0x0

    const/16 v6, 0x2f

    const/4 v1, 0x0

    const/4 v3, -0x1

    .line 2008
    .line 2009
    invoke-static {p0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 2010
    invoke-virtual {v0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v0

    .line 2011
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_1

    sget-object v4, Lcom/google/android/apps/chrome/omnibox/LocationBar;->ACCEPTED_SCHEMES:Ljava/util/HashSet;

    invoke-virtual {v4, v0}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 2012
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    .line 2013
    :goto_0
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v4

    if-ge v0, v4, :cond_2

    .line 2015
    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    move-result v4

    .line 2016
    const/16 v5, 0x3a

    if-eq v4, v5, :cond_0

    if-ne v4, v6, :cond_2

    .line 2014
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    move v0, v1

    .line 2020
    :cond_2
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v4

    if-ge v0, v4, :cond_5

    .line 2021
    invoke-virtual {p0, v6, v0}, Ljava/lang/String;->indexOf(II)I

    move-result v0

    .line 2023
    :goto_1
    if-eq v0, v3, :cond_4

    .line 2024
    invoke-virtual {p0, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 2027
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    if-ne v0, v3, :cond_3

    move-object v0, v2

    .line 2029
    :goto_2
    invoke-static {v1, v0}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    .line 2031
    :goto_3
    return-object v0

    .line 2027
    :cond_3
    invoke-virtual {p0, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    .line 2031
    :cond_4
    invoke-static {p0, v2}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    goto :goto_3

    :cond_5
    move v0, v3

    goto :goto_1
.end method

.method private startZeroSuggest()V
    .locals 6

    .prologue
    .line 1130
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mHasStartedNewOmniboxEditSession:Z

    .line 1131
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mNewOmniboxEditSessionTimestamp:J

    .line 1132
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->getCurrentTab()Lcom/google/android/apps/chrome/tab/ChromeTab;

    move-result-object v3

    .line 1133
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mNativeInitialized:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mUrlHasFocus:Z

    if-eqz v0, :cond_0

    if-eqz v3, :cond_0

    invoke-virtual {v3}, Lorg/chromium/chrome/browser/Tab;->isIncognito()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1137
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mAutocomplete:Lcom/google/android/apps/chrome/omnibox/ChromeAutocompleteController;

    invoke-virtual {v3}, Lorg/chromium/chrome/browser/Tab;->getProfile()Lorg/chromium/chrome/browser/profiles/Profile;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->getUrlBar()Lcom/google/android/apps/chrome/omnibox/UrlBar;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/omnibox/UrlBar;->getQueryText()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3}, Lorg/chromium/chrome/browser/Tab;->getUrl()Ljava/lang/String;

    move-result-object v3

    iget-boolean v4, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mQueryInTheOmnibox:Z

    iget-boolean v5, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mUrlFocusedFromFakebox:Z

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/apps/chrome/omnibox/ChromeAutocompleteController;->startZeroSuggest(Lorg/chromium/chrome/browser/profiles/Profile;Ljava/lang/String;Ljava/lang/String;ZZ)V

    .line 1140
    :cond_0
    return-void
.end method

.method private stopAutocomplete(Z)V
    .locals 1

    .prologue
    .line 1770
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mAutocomplete:Lcom/google/android/apps/chrome/omnibox/ChromeAutocompleteController;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mAutocomplete:Lcom/google/android/apps/chrome/omnibox/ChromeAutocompleteController;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/omnibox/ChromeAutocompleteController;->stop(Z)V

    .line 1771
    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->cancelPendingAutocompleteStart()V

    .line 1772
    return-void
.end method

.method private static suggestionTypeToNavigationButtonType(Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;)Lcom/google/android/apps/chrome/omnibox/LocationBar$NavigationButtonType;
    .locals 2

    .prologue
    .line 1216
    sget-object v0, Lcom/google/android/apps/chrome/omnibox/LocationBar$14;->$SwitchMap$org$chromium$chrome$browser$omnibox$OmniboxSuggestion$Type:[I

    invoke-virtual {p0}, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1238
    sget-boolean v0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 1223
    :pswitch_0
    sget-object v0, Lcom/google/android/apps/chrome/omnibox/LocationBar$NavigationButtonType;->PAGE:Lcom/google/android/apps/chrome/omnibox/LocationBar$NavigationButtonType;

    .line 1239
    :goto_0
    return-object v0

    .line 1236
    :pswitch_1
    sget-object v0, Lcom/google/android/apps/chrome/omnibox/LocationBar$NavigationButtonType;->MAGNIFIER:Lcom/google/android/apps/chrome/omnibox/LocationBar$NavigationButtonType;

    goto :goto_0

    .line 1239
    :cond_0
    sget-object v0, Lcom/google/android/apps/chrome/omnibox/LocationBar$NavigationButtonType;->MAGNIFIER:Lcom/google/android/apps/chrome/omnibox/LocationBar$NavigationButtonType;

    goto :goto_0

    .line 1216
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method private updateCustomSelectionActionModeCallback()V
    .locals 2

    .prologue
    .line 1155
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mQueryInTheOmnibox:Z

    if-eqz v0, :cond_0

    .line 1156
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mUrlBar:Lcom/google/android/apps/chrome/omnibox/UrlBar;

    iget-object v1, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mContextualMenuBar:Lcom/google/android/apps/chrome/ContextualMenuBar;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/ContextualMenuBar;->getCustomSelectionActionModeCallback()Lcom/google/android/apps/chrome/CustomSelectionActionModeCallback;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/omnibox/UrlBar;->setCustomSelectionActionModeCallback(Landroid/view/ActionMode$Callback;)V

    .line 1162
    :goto_0
    return-void

    .line 1159
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mUrlBar:Lcom/google/android/apps/chrome/omnibox/UrlBar;

    iget-object v1, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mToolbar:Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;->getDefaultActionModeCallbackForTextEdit()Lcom/google/android/apps/chrome/CustomSelectionActionModeCallback;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/omnibox/UrlBar;->setCustomSelectionActionModeCallback(Landroid/view/ActionMode$Callback;)V

    goto :goto_0
.end method

.method private updateDeleteButtonVisibility()V
    .locals 1

    .prologue
    .line 1393
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mUrlBar:Lcom/google/android/apps/chrome/omnibox/UrlBar;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/omnibox/UrlBar;->hasFocus()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mUrlBar:Lcom/google/android/apps/chrome/omnibox/UrlBar;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/omnibox/UrlBar;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->updateDeleteButton(Z)V

    .line 1394
    return-void

    .line 1393
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private updateFocusSource(Z)V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2551
    if-nez p1, :cond_1

    .line 2552
    iput-boolean v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mUrlFocusedFromFakebox:Z

    .line 2553
    iput-boolean v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mHasRecordedUrlFocusSource:Z

    .line 2576
    :cond_0
    :goto_0
    return-void

    .line 2558
    :cond_1
    sget-boolean v0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->$assertionsDisabled:Z

    if-nez v0, :cond_2

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mHasRecordedUrlFocusSource:Z

    if-eqz v0, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 2559
    :cond_2
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mHasRecordedUrlFocusSource:Z

    if-nez v0, :cond_0

    .line 2561
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->getCurrentTab()Lcom/google/android/apps/chrome/tab/ChromeTab;

    move-result-object v0

    .line 2562
    if-eqz v0, :cond_0

    .line 2564
    invoke-virtual {v0}, Lorg/chromium/chrome/browser/Tab;->getUrl()Ljava/lang/String;

    move-result-object v1

    .line 2565
    iget-boolean v2, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mUrlFocusedFromFakebox:Z

    if-eqz v2, :cond_5

    .line 2566
    sget-boolean v2, Lcom/google/android/apps/chrome/omnibox/LocationBar;->$assertionsDisabled:Z

    if-nez v2, :cond_4

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/Tab;->isNativePage()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-static {v1}, Lcom/google/android/apps/chrome/ntp/NewTabPage;->isNTPUrl(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_4

    :cond_3
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 2567
    :cond_4
    invoke-static {}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->focusedFakeboxOnNtp()V

    .line 2575
    :goto_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mHasRecordedUrlFocusSource:Z

    goto :goto_0

    .line 2569
    :cond_5
    invoke-virtual {v0}, Lorg/chromium/chrome/browser/Tab;->isNativePage()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-static {v1}, Lcom/google/android/apps/chrome/ntp/NewTabPage;->isNTPUrl(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 2570
    invoke-static {}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->focusedOmniboxOnNtp()V

    goto :goto_1

    .line 2572
    :cond_6
    invoke-static {}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->focusedOmniboxNotOnNtp()V

    goto :goto_1
.end method

.method private updateNavigationButton()V
    .locals 3

    .prologue
    .line 1245
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lorg/chromium/ui/base/DeviceFormFactor;->isTablet(Landroid/content/Context;)Z

    move-result v1

    .line 1246
    sget-object v0, Lcom/google/android/apps/chrome/omnibox/LocationBar$NavigationButtonType;->EMPTY:Lcom/google/android/apps/chrome/omnibox/LocationBar$NavigationButtonType;

    .line 1247
    if-eqz v1, :cond_2

    iget-object v2, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mSuggestionItems:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_2

    .line 1249
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mSuggestionItems:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/omnibox/OmniboxResultsAdapter$OmniboxResultItem;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/omnibox/OmniboxResultsAdapter$OmniboxResultItem;->getSuggestion()Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion;->getType()Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->suggestionTypeToNavigationButtonType(Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;)Lcom/google/android/apps/chrome/omnibox/LocationBar$NavigationButtonType;

    move-result-object v0

    .line 1257
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mNavigationButtonType:Lcom/google/android/apps/chrome/omnibox/LocationBar$NavigationButtonType;

    if-eq v0, v1, :cond_1

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->setNavigationButtonType(Lcom/google/android/apps/chrome/omnibox/LocationBar$NavigationButtonType;)V

    .line 1258
    :cond_1
    return-void

    .line 1251
    :cond_2
    iget-boolean v2, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mQueryInTheOmnibox:Z

    if-eqz v2, :cond_3

    .line 1252
    sget-object v0, Lcom/google/android/apps/chrome/omnibox/LocationBar$NavigationButtonType;->MAGNIFIER:Lcom/google/android/apps/chrome/omnibox/LocationBar$NavigationButtonType;

    goto :goto_0

    .line 1253
    :cond_3
    if-eqz v1, :cond_0

    .line 1254
    sget-object v0, Lcom/google/android/apps/chrome/omnibox/LocationBar$NavigationButtonType;->PAGE:Lcom/google/android/apps/chrome/omnibox/LocationBar$NavigationButtonType;

    goto :goto_0
.end method

.method private updateOmniboxResultsContainer()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2333
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mSuggestionsShown:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mUrlHasFocus:Z

    if-eqz v0, :cond_3

    .line 2334
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mOmniboxResultsContainer:Landroid/view/ViewGroup;

    if-nez v0, :cond_1

    .line 2335
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->getRootView()Landroid/view/View;

    move-result-object v0

    sget v1, Lcom/google/android/apps/chrome/R$id;->omnibox_results_container_stub:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    .line 2337
    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mOmniboxResultsContainer:Landroid/view/ViewGroup;

    .line 2338
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mOmniboxResultsContainer:Landroid/view/ViewGroup;

    sget v1, Lcom/google/android/apps/chrome/omnibox/LocationBar;->CONTENT_OVERLAY_COLOR:I

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setBackgroundColor(I)V

    .line 2340
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mOmniboxResultsContainer:Landroid/view/ViewGroup;

    new-instance v1, Lcom/google/android/apps/chrome/omnibox/LocationBar$12;

    invoke-direct {v1, p0}, Lcom/google/android/apps/chrome/omnibox/LocationBar$12;-><init>(Lcom/google/android/apps/chrome/omnibox/LocationBar;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 2353
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mOmniboxResultsContainer:Landroid/view/ViewGroup;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 2357
    :cond_2
    :goto_0
    return-void

    .line 2354
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mOmniboxResultsContainer:Landroid/view/ViewGroup;

    if-eqz v0, :cond_2

    .line 2355
    invoke-direct {p0, v2}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->updateOmniboxResultsContainerBackground(Z)V

    goto :goto_0
.end method

.method private updateOmniboxResultsContainerBackground(Z)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2364
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->getToolbarDataProvider()Lcom/google/android/apps/chrome/toolbar/ToolbarDataProvider;

    move-result-object v0

    if-nez v0, :cond_0

    .line 2381
    :goto_0
    return-void

    .line 2366
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->getToolbarDataProvider()Lcom/google/android/apps/chrome/toolbar/ToolbarDataProvider;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarDataProvider;->getNewTabPageForCurrentTab()Lcom/google/android/apps/chrome/ntp/NewTabPage;

    move-result-object v0

    .line 2367
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ntp/NewTabPage;->isLocationBarShownInNTP()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    .line 2368
    :goto_1
    if-eqz p1, :cond_3

    .line 2369
    if-eqz v0, :cond_2

    .line 2370
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mOmniboxResultsContainer:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    goto :goto_0

    :cond_1
    move v0, v1

    .line 2367
    goto :goto_1

    .line 2372
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->fadeInOmniboxResultsContainerBackground()V

    goto :goto_0

    .line 2375
    :cond_3
    if-eqz v0, :cond_4

    .line 2376
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mOmniboxResultsContainer:Landroid/view/ViewGroup;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    goto :goto_0

    .line 2378
    :cond_4
    invoke-direct {p0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->fadeOutOmniboxResultsContainerBackground()V

    goto :goto_0
.end method

.method private updateReaderModeLabelVisibility()V
    .locals 2

    .prologue
    const/16 v1, 0x8

    .line 1400
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mUrlHasFocus:Z

    if-nez v0, :cond_1

    invoke-direct {p0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->shouldShowReaderModeLabel()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1401
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mReaderModeLabel:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getVisibility()I

    move-result v0

    if-ne v0, v1, :cond_0

    .line 1402
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mReaderModeLabel:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1407
    :cond_0
    :goto_0
    return-void

    .line 1404
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mReaderModeLabel:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 1405
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mReaderModeLabel:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method private updateSecurityButton(Z)V
    .locals 1

    .prologue
    .line 1337
    if-eqz p1, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lorg/chromium/ui/base/DeviceFormFactor;->isTablet(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mUrlHasFocus:Z

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->changeLocationBarIcon(Z)V

    .line 1339
    iput-boolean p1, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mSecurityButtonShown:Z

    .line 1340
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->updateLocationBarIconContainerVisibility()V

    .line 1341
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mToolbar:Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;->requestLayout()V

    .line 1342
    return-void

    .line 1337
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private updateSuggestionUrlIfNeeded(Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion;I)Ljava/lang/String;
    .locals 4

    .prologue
    .line 1681
    sget-boolean v0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mNativeInitialized:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    const-string/jumbo v1, "updateSuggestionUrlIfNeeded called before native initialization"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 1683
    :cond_0
    const/4 v0, 0x0

    .line 1684
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->isCorpusChipTextDisplayed()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-virtual {p1}, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion;->isUrlSuggestion()Z

    move-result v1

    if-nez v1, :cond_3

    .line 1685
    invoke-virtual {p1}, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion;->getFillIntoEdit()Ljava/lang/String;

    move-result-object v1

    .line 1686
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->getCurrentTab()Lcom/google/android/apps/chrome/tab/ChromeTab;

    move-result-object v2

    .line 1687
    if-eqz v2, :cond_1

    invoke-virtual {v2}, Lorg/chromium/chrome/browser/Tab;->getUrl()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 1689
    invoke-static {}, Lorg/chromium/chrome/browser/search_engines/TemplateUrlService;->getInstance()Lorg/chromium/chrome/browser/search_engines/TemplateUrlService;

    move-result-object v0

    invoke-virtual {v2}, Lorg/chromium/chrome/browser/Tab;->getUrl()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/chromium/chrome/browser/search_engines/TemplateUrlService;->replaceSearchTermsInUrl(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1701
    :cond_1
    :goto_0
    if-nez v0, :cond_2

    invoke-virtual {p1}, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion;->getUrl()Ljava/lang/String;

    move-result-object v0

    :cond_2
    return-object v0

    .line 1692
    :cond_3
    invoke-virtual {p1}, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion;->getType()Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;

    move-result-object v1

    sget-object v2, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;->VOICE_SUGGEST:Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;

    if-eq v1, v2, :cond_1

    .line 1695
    iget-wide v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mNewOmniboxEditSessionTimestamp:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_4

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mNewOmniboxEditSessionTimestamp:J

    sub-long/2addr v0, v2

    .line 1697
    :goto_1
    iget-object v2, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mAutocomplete:Lcom/google/android/apps/chrome/omnibox/ChromeAutocompleteController;

    invoke-virtual {v2, p2, v0, v1}, Lcom/google/android/apps/chrome/omnibox/ChromeAutocompleteController;->updateMatchDestinationUrlWithQueryFormulationTime(IJ)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1695
    :cond_4
    const-wide/16 v0, -0x1

    goto :goto_1
.end method

.method private updateSuggestionsLayoutDirection(I)V
    .locals 3

    .prologue
    .line 876
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mSuggestionList:Lcom/google/android/apps/chrome/omnibox/LocationBar$OmniboxSuggestionsList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mSuggestionList:Lcom/google/android/apps/chrome/omnibox/LocationBar$OmniboxSuggestionsList;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/omnibox/LocationBar$OmniboxSuggestionsList;->isShown()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 877
    iget-object v1, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mSuggestionList:Lcom/google/android/apps/chrome/omnibox/LocationBar$OmniboxSuggestionsList;

    .line 878
    invoke-static {v1, p1}, Lorg/chromium/base/ApiCompatibilityUtils;->setLayoutDirection(Landroid/view/View;I)V

    .line 879
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1}, Landroid/widget/ListView;->getChildCount()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 880
    invoke-virtual {v1, v0}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-static {v2, p1}, Lorg/chromium/base/ApiCompatibilityUtils;->setLayoutDirection(Landroid/view/View;I)V

    .line 879
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 884
    :cond_0
    return-void
.end method


# virtual methods
.method public backKeyPressed()V
    .locals 1

    .prologue
    .line 1966
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->hideSuggestions()V

    .line 1967
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mUrlBar:Lcom/google/android/apps/chrome/omnibox/UrlBar;

    invoke-static {v0}, Lorg/chromium/ui/UiUtils;->hideKeyboard(Landroid/view/View;)Z

    .line 1969
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->setUrlToPageUrl()V

    .line 1971
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->getCurrentTab()Lcom/google/android/apps/chrome/tab/ChromeTab;

    move-result-object v0

    .line 1972
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->requestFocus()V

    .line 1973
    :cond_0
    return-void
.end method

.method public createContextualMenuBar(Lcom/google/android/apps/chrome/ContextualMenuBar$ActionBarDelegate;)V
    .locals 2

    .prologue
    .line 947
    new-instance v0, Lcom/google/android/apps/chrome/ContextualMenuBar;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Lcom/google/android/apps/chrome/ContextualMenuBar;-><init>(Landroid/content/Context;Lcom/google/android/apps/chrome/ContextualMenuBar$ActionBarDelegate;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mContextualMenuBar:Lcom/google/android/apps/chrome/ContextualMenuBar;

    .line 948
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mContextualMenuBar:Lcom/google/android/apps/chrome/ContextualMenuBar;

    new-instance v1, Lcom/google/android/apps/chrome/omnibox/LocationBar$4;

    invoke-direct {v1, p0}, Lcom/google/android/apps/chrome/omnibox/LocationBar$4;-><init>(Lcom/google/android/apps/chrome/omnibox/LocationBar;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/ContextualMenuBar;->setCustomSelectionActionModeCallback(Lcom/google/android/apps/chrome/CustomSelectionActionModeCallback;)V

    .line 972
    return-void
.end method

.method protected dispatchRestoreInstanceState(Landroid/util/SparseArray;)V
    .locals 0

    .prologue
    .line 1793
    return-void
.end method

.method protected fadeInOmniboxResultsContainerBackground()V
    .locals 4

    .prologue
    .line 2387
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mFadeInOmniboxBackgroundAnimator:Landroid/animation/ObjectAnimator;

    if-nez v0, :cond_0

    .line 2388
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->getRootView()Landroid/view/View;

    move-result-object v0

    sget v1, Lcom/google/android/apps/chrome/R$id;->omnibox_results_container:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    const-string/jumbo v1, "alpha"

    const/4 v2, 0x2

    new-array v2, v2, [I

    fill-array-data v2, :array_0

    invoke-static {v0, v1, v2}, Landroid/animation/ObjectAnimator;->ofInt(Ljava/lang/Object;Ljava/lang/String;[I)Landroid/animation/ObjectAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mFadeInOmniboxBackgroundAnimator:Landroid/animation/ObjectAnimator;

    .line 2391
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mFadeInOmniboxBackgroundAnimator:Landroid/animation/ObjectAnimator;

    const-wide/16 v2, 0xfa

    invoke-virtual {v0, v2, v3}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 2392
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mFadeInOmniboxBackgroundAnimator:Landroid/animation/ObjectAnimator;

    sget-object v1, Lorg/chromium/ui/interpolators/BakedBezierInterpolator;->FADE_IN_CURVE:Lorg/chromium/ui/interpolators/BakedBezierInterpolator;

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 2395
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mFadeInOmniboxBackgroundAnimator:Landroid/animation/ObjectAnimator;

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->runOmniboxResultsFadeAnimation(Landroid/animation/Animator;)V

    .line 2396
    return-void

    .line 2388
    nop

    :array_0
    .array-data 4
        0x0
        0xff
    .end array-data
.end method

.method public getContentRect(Landroid/graphics/Rect;)V
    .locals 5

    .prologue
    .line 489
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->getPaddingLeft()I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->getPaddingTop()I

    move-result v1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->getWidth()I

    move-result v2

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->getPaddingRight()I

    move-result v3

    sub-int/2addr v2, v3

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->getHeight()I

    move-result v3

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->getPaddingBottom()I

    move-result v4

    sub-int/2addr v3, v4

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Rect;->set(IIII)V

    .line 491
    return-void
.end method

.method public getCurrentTab()Lcom/google/android/apps/chrome/tab/ChromeTab;
    .locals 1

    .prologue
    .line 2322
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mToolbarDataProvider:Lcom/google/android/apps/chrome/toolbar/ToolbarDataProvider;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 2323
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mToolbarDataProvider:Lcom/google/android/apps/chrome/toolbar/ToolbarDataProvider;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarDataProvider;->getTab()Lorg/chromium/chrome/browser/Tab;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->fromTab(Lorg/chromium/chrome/browser/Tab;)Lcom/google/android/apps/chrome/tab/ChromeTab;

    move-result-object v0

    goto :goto_0
.end method

.method public getFirstFocusTime()J
    .locals 2

    .prologue
    .line 1147
    iget-wide v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mFirstFocusTimeMs:J

    return-wide v0
.end method

.method public getOriginalUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1979
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mOriginalUrl:Ljava/lang/String;

    return-object v0
.end method

.method public getSuggestionList()Lcom/google/android/apps/chrome/omnibox/LocationBar$OmniboxSuggestionsList;
    .locals 1

    .prologue
    .line 1536
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mSuggestionList:Lcom/google/android/apps/chrome/omnibox/LocationBar$OmniboxSuggestionsList;

    return-object v0
.end method

.method protected getSuggestionPopupBackground()Landroid/graphics/drawable/Drawable;
    .locals 2

    .prologue
    .line 1645
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mToolbarDataProvider:Lcom/google/android/apps/chrome/toolbar/ToolbarDataProvider;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarDataProvider;->isIncognito()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1646
    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    sget v1, Lcom/google/android/apps/chrome/omnibox/LocationBar;->OMNIBOX_INCOGNITO_RESULTS_BG_COLOR:I

    invoke-direct {v0, v1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    .line 1648
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    sget v1, Lcom/google/android/apps/chrome/omnibox/LocationBar;->OMNIBOX_RESULTS_BG_COLOR:I

    invoke-direct {v0, v1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    goto :goto_0
.end method

.method protected getToolbarDataProvider()Lcom/google/android/apps/chrome/toolbar/ToolbarDataProvider;
    .locals 1

    .prologue
    .line 1211
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mToolbarDataProvider:Lcom/google/android/apps/chrome/toolbar/ToolbarDataProvider;

    return-object v0
.end method

.method public getUrlBar()Lcom/google/android/apps/chrome/omnibox/UrlBar;
    .locals 1

    .prologue
    .line 481
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mUrlBar:Lcom/google/android/apps/chrome/omnibox/UrlBar;

    return-object v0
.end method

.method protected getWindowDelegate()Lcom/google/android/apps/chrome/WindowDelegate;
    .locals 1

    .prologue
    .line 907
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mWindowDelegate:Lcom/google/android/apps/chrome/WindowDelegate;

    return-object v0
.end method

.method public hideSuggestions()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1721
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mAutocomplete:Lcom/google/android/apps/chrome/omnibox/ChromeAutocompleteController;

    if-nez v0, :cond_0

    .line 1732
    :goto_0
    return-void

    .line 1723
    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->recordSuggestionsDismissed()V

    .line 1725
    invoke-direct {p0, v2}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->stopAutocomplete(Z)V

    .line 1727
    invoke-virtual {p0, v1}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->setSuggestionsListVisibility(Z)V

    .line 1728
    invoke-direct {p0, v2}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->clearSuggestions(Z)V

    .line 1729
    invoke-direct {p0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->updateNavigationButton()V

    .line 1731
    iput-boolean v1, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mSuggestionSelectionInProgress:Z

    goto :goto_0
.end method

.method public isCorpusChipTextDisplayed()Z
    .locals 1

    .prologue
    .line 497
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mCorpusChipTextView:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mCorpusChipTextView:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->isShown()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isInvalidCorpusChip()Z
    .locals 2

    .prologue
    .line 556
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mToolbarDataProvider:Lcom/google/android/apps/chrome/toolbar/ToolbarDataProvider;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarDataProvider;->getCorpusChipText()Ljava/lang/String;

    move-result-object v0

    .line 557
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x0

    .line 559
    :goto_0
    return v0

    .line 558
    :cond_0
    sget-object v1, Lcom/google/android/apps/chrome/omnibox/LocationBar;->INVALID_CORPUS_CHIPS:[Ljava/lang/String;

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    .line 559
    invoke-interface {v1, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public isSecurityButtonShown()Z
    .locals 1

    .prologue
    .line 1349
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mSecurityButtonShown:Z

    return v0
.end method

.method protected isVoiceSearchEnabled()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 2445
    iget-object v1, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mToolbarDataProvider:Lcom/google/android/apps/chrome/toolbar/ToolbarDataProvider;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mToolbarDataProvider:Lcom/google/android/apps/chrome/toolbar/ToolbarDataProvider;

    invoke-interface {v1}, Lcom/google/android/apps/chrome/toolbar/ToolbarDataProvider;->isIncognito()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v0}, Lorg/chromium/chrome/browser/util/FeatureUtilities;->isRecognitionIntentPresent(Landroid/content/Context;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1852
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mDeleteButton:Landroid/widget/ImageButton;

    if-ne p1, v0, :cond_3

    .line 1853
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mUrlBar:Lcom/google/android/apps/chrome/omnibox/UrlBar;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/omnibox/UrlBar;->getQueryText()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 1854
    const-string/jumbo v0, ""

    invoke-direct {p0, v0, v1, v1}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->setUrlBarText(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    .line 1855
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->hideSuggestions()V

    .line 1862
    :cond_0
    :goto_0
    invoke-direct {p0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->startZeroSuggest()V

    .line 1873
    :cond_1
    :goto_1
    return-void

    .line 1856
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->isCorpusChipTextDisplayed()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1857
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mUrlBar:Lcom/google/android/apps/chrome/omnibox/UrlBar;

    const-string/jumbo v1, ""

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/omnibox/UrlBar;->setText(Ljava/lang/CharSequence;)V

    .line 1858
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->updateCorpusChipTextVisibility(Z)V

    .line 1859
    invoke-direct {p0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->updateDeleteButtonVisibility()V

    goto :goto_0

    .line 1864
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mSecurityButton:Landroid/widget/ImageButton;

    if-ne p1, v0, :cond_4

    .line 1865
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->getCurrentTab()Lcom/google/android/apps/chrome/tab/ChromeTab;

    move-result-object v0

    .line 1866
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/Tab;->getWebContents()Lorg/chromium/content_public/browser/WebContents;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 1867
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/Tab;->getWebContents()Lorg/chromium/content_public/browser/WebContents;

    move-result-object v0

    invoke-static {v1, v0}, Lorg/chromium/chrome/browser/WebsiteSettingsPopup;->show(Landroid/content/Context;Lorg/chromium/content_public/browser/WebContents;)V

    goto :goto_1

    .line 1869
    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mMicButton:Landroid/widget/ImageButton;

    if-ne p1, v0, :cond_1

    .line 1870
    invoke-static {}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->omniboxVoiceSearch()V

    .line 1871
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->startVoiceRecognition()V

    goto :goto_1
.end method

.method protected onDetachedFromWindow()V
    .locals 0

    .prologue
    .line 912
    invoke-super {p0}, Landroid/widget/FrameLayout;->onDetachedFromWindow()V

    .line 913
    return-void
.end method

.method protected onFinishInflate()V
    .locals 11

    .prologue
    const/high16 v10, 0x3f800000    # 1.0f

    const/4 v9, 0x0

    const/4 v8, 0x0

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 803
    invoke-super {p0}, Landroid/widget/FrameLayout;->onFinishInflate()V

    .line 805
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mUrlBar:Lcom/google/android/apps/chrome/omnibox/UrlBar;

    invoke-virtual {v0, v6}, Lcom/google/android/apps/chrome/omnibox/UrlBar;->setCursorVisible(Z)V

    .line 806
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mNavigationButton:Landroid/widget/ImageView;

    invoke-virtual {v0, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 807
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mSecurityButton:Landroid/widget/ImageButton;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 809
    invoke-virtual {p0, v8}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->setLayoutTransition(Landroid/animation/LayoutTransition;)V

    .line 811
    new-instance v0, Lcom/google/android/apps/chrome/omnibox/LocationBar$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/omnibox/LocationBar$1;-><init>(Lcom/google/android/apps/chrome/omnibox/LocationBar;)V

    .line 831
    new-instance v1, Landroid/animation/AnimatorSet;

    invoke-direct {v1}, Landroid/animation/AnimatorSet;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mSecurityButtonShowAnimator:Landroid/animation/AnimatorSet;

    .line 832
    iget-object v1, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mSecurityButtonShowAnimator:Landroid/animation/AnimatorSet;

    const/4 v2, 0x2

    new-array v2, v2, [Landroid/animation/Animator;

    iget-object v3, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mNavigationButton:Landroid/widget/ImageView;

    sget-object v4, Lcom/google/android/apps/chrome/omnibox/LocationBar;->ALPHA:Landroid/util/Property;

    new-array v5, v7, [F

    aput v9, v5, v6

    invoke-static {v3, v4, v5}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v3

    aput-object v3, v2, v6

    iget-object v3, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mSecurityButton:Landroid/widget/ImageButton;

    sget-object v4, Lcom/google/android/apps/chrome/omnibox/LocationBar;->ALPHA:Landroid/util/Property;

    new-array v5, v7, [F

    aput v10, v5, v6

    invoke-static {v3, v4, v5}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v3

    aput-object v3, v2, v7

    invoke-virtual {v1, v2}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    .line 835
    iget-object v1, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mSecurityButtonShowAnimator:Landroid/animation/AnimatorSet;

    const-wide/16 v2, 0xfa

    invoke-virtual {v1, v2, v3}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    .line 836
    iget-object v1, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mSecurityButtonShowAnimator:Landroid/animation/AnimatorSet;

    invoke-virtual {v1, v0}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 838
    new-instance v1, Landroid/animation/AnimatorSet;

    invoke-direct {v1}, Landroid/animation/AnimatorSet;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mNavigationIconShowAnimator:Landroid/animation/AnimatorSet;

    .line 839
    iget-object v1, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mNavigationIconShowAnimator:Landroid/animation/AnimatorSet;

    const/4 v2, 0x2

    new-array v2, v2, [Landroid/animation/Animator;

    iget-object v3, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mNavigationButton:Landroid/widget/ImageView;

    sget-object v4, Lcom/google/android/apps/chrome/omnibox/LocationBar;->ALPHA:Landroid/util/Property;

    new-array v5, v7, [F

    aput v10, v5, v6

    invoke-static {v3, v4, v5}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v3

    aput-object v3, v2, v6

    iget-object v3, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mSecurityButton:Landroid/widget/ImageButton;

    sget-object v4, Lcom/google/android/apps/chrome/omnibox/LocationBar;->ALPHA:Landroid/util/Property;

    new-array v5, v7, [F

    aput v9, v5, v6

    invoke-static {v3, v4, v5}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v3

    aput-object v3, v2, v7

    invoke-virtual {v1, v2}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    .line 842
    iget-object v1, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mNavigationIconShowAnimator:Landroid/animation/AnimatorSet;

    const-wide/16 v2, 0xfa

    invoke-virtual {v1, v2, v3}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    .line 843
    iget-object v1, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mNavigationIconShowAnimator:Landroid/animation/AnimatorSet;

    invoke-virtual {v1, v0}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 845
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mUrlBar:Lcom/google/android/apps/chrome/omnibox/UrlBar;

    new-instance v1, Lcom/google/android/apps/chrome/omnibox/LocationBar$UrlBarKeyListener;

    invoke-direct {v1, p0, v8}, Lcom/google/android/apps/chrome/omnibox/LocationBar$UrlBarKeyListener;-><init>(Lcom/google/android/apps/chrome/omnibox/LocationBar;Lcom/google/android/apps/chrome/omnibox/LocationBar$1;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/omnibox/UrlBar;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 847
    new-instance v0, Lcom/google/android/apps/chrome/omnibox/LocationBar$UrlBarTextWatcher;

    invoke-direct {v0, p0, v8}, Lcom/google/android/apps/chrome/omnibox/LocationBar$UrlBarTextWatcher;-><init>(Lcom/google/android/apps/chrome/omnibox/LocationBar;Lcom/google/android/apps/chrome/omnibox/LocationBar$1;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mTextWatcher:Landroid/text/TextWatcher;

    .line 848
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mUrlBar:Lcom/google/android/apps/chrome/omnibox/UrlBar;

    iget-object v1, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mTextWatcher:Landroid/text/TextWatcher;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/omnibox/UrlBar;->setLocationBarTextWatcher(Landroid/text/TextWatcher;)V

    .line 852
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mUrlBar:Lcom/google/android/apps/chrome/omnibox/UrlBar;

    new-instance v1, Lcom/google/android/apps/chrome/omnibox/LocationBar$2;

    invoke-direct {v1, p0}, Lcom/google/android/apps/chrome/omnibox/LocationBar$2;-><init>(Lcom/google/android/apps/chrome/omnibox/LocationBar;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/omnibox/UrlBar;->setUrlDirectionListener(Lcom/google/android/apps/chrome/omnibox/UrlBar$UrlDirectionListener;)V

    .line 860
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mUrlBar:Lcom/google/android/apps/chrome/omnibox/UrlBar;

    invoke-virtual {v0, v7}, Lcom/google/android/apps/chrome/omnibox/UrlBar;->setSelectAllOnFocus(Z)V

    .line 861
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mUrlBar:Lcom/google/android/apps/chrome/omnibox/UrlBar;

    new-instance v1, Lcom/google/android/apps/chrome/omnibox/LocationBar$3;

    invoke-direct {v1, p0}, Lcom/google/android/apps/chrome/omnibox/LocationBar$3;-><init>(Lcom/google/android/apps/chrome/omnibox/LocationBar;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/omnibox/UrlBar;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 867
    return-void
.end method

.method public onFinishSetUrl()V
    .locals 0

    .prologue
    .line 502
    invoke-direct {p0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->addCorpusChipSpan()V

    .line 503
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->updateSnapshotLabelVisibility()V

    .line 504
    invoke-direct {p0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->updateReaderModeLabelVisibility()V

    .line 505
    return-void
.end method

.method public onIntentCompleted(Lorg/chromium/ui/base/WindowAndroid;ILandroid/content/ContentResolver;Landroid/content/Intent;)V
    .locals 3

    .prologue
    .line 2512
    const/4 v0, -0x1

    if-eq p2, v0, :cond_1

    .line 2534
    :cond_0
    :goto_0
    return-void

    .line 2513
    :cond_1
    invoke-virtual {p4}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2515
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mAutocomplete:Lcom/google/android/apps/chrome/omnibox/ChromeAutocompleteController;

    invoke-virtual {p4}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/omnibox/ChromeAutocompleteController;->onVoiceResults(Landroid/os/Bundle;)Lcom/google/android/apps/chrome/omnibox/VoiceSuggestionProvider$VoiceResult;

    move-result-object v0

    .line 2516
    if-eqz v0, :cond_0

    .line 2518
    invoke-virtual {v0}, Lcom/google/android/apps/chrome/omnibox/VoiceSuggestionProvider$VoiceResult;->getMatch()Ljava/lang/String;

    move-result-object v2

    .line 2519
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2521
    invoke-virtual {v0}, Lcom/google/android/apps/chrome/omnibox/VoiceSuggestionProvider$VoiceResult;->getConfidence()F

    move-result v0

    const v1, 0x3f666666    # 0.9f

    cmpg-float v0, v0, v1

    if-gez v0, :cond_2

    .line 2522
    invoke-virtual {p0, v2}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->setSearchQuery(Ljava/lang/String;)V

    goto :goto_0

    .line 2526
    :cond_2
    invoke-static {v2}, Lorg/chromium/chrome/browser/omnibox/AutocompleteController;->nativeQualifyPartialURLQuery(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2527
    sget-object v0, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;->NAVSUGGEST:Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;

    .line 2528
    if-nez v1, :cond_3

    .line 2529
    invoke-static {}, Lorg/chromium/chrome/browser/search_engines/TemplateUrlService;->getInstance()Lorg/chromium/chrome/browser/search_engines/TemplateUrlService;

    move-result-object v0

    invoke-virtual {v0, v2}, Lorg/chromium/chrome/browser/search_engines/TemplateUrlService;->getUrlForVoiceSearchQuery(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2531
    sget-object v0, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;->SEARCH_WHAT_YOU_TYPED:Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;

    .line 2533
    :cond_3
    const/4 v2, 0x1

    invoke-direct {p0, v1, v2, v0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->loadUrl(Ljava/lang/String;ILorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;)V

    goto :goto_0
.end method

.method protected onMeasure(II)V
    .locals 0

    .prologue
    .line 871
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->updateLayoutParams()V

    .line 872
    invoke-super {p0, p1, p2}, Landroid/widget/FrameLayout;->onMeasure(II)V

    .line 873
    return-void
.end method

.method public onNativeLibraryReady()V
    .locals 2

    .prologue
    .line 919
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mNativeInitialized:Z

    .line 921
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mSecurityButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 922
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mMicButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 923
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->updateMicButtonState()V

    .line 924
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mDeleteButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 926
    new-instance v0, Lcom/google/android/apps/chrome/omnibox/ChromeAutocompleteController;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/omnibox/ChromeAutocompleteController;-><init>(Lorg/chromium/chrome/browser/omnibox/AutocompleteController$OnSuggestionsReceivedListener;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mAutocomplete:Lcom/google/android/apps/chrome/omnibox/ChromeAutocompleteController;

    .line 928
    new-instance v0, Lorg/chromium/chrome/browser/omnibox/OmniboxPrerender;

    invoke-direct {v0}, Lorg/chromium/chrome/browser/omnibox/OmniboxPrerender;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mOmniboxPrerender:Lorg/chromium/chrome/browser/omnibox/OmniboxPrerender;

    .line 930
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mDeferredNativeRunnables:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Runnable;

    .line 931
    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    .line 933
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mDeferredNativeRunnables:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 936
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->getContext()Landroid/content/Context;

    move-result-object v0

    const/16 v1, 0x42

    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->broadcastImmediateNotification(Landroid/content/Context;I)V

    .line 939
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->updateVisualsForState()V

    .line 940
    return-void
.end method

.method public onSuggestionsReceived(Ljava/util/List;Ljava/lang/String;)V
    .locals 10

    .prologue
    const/4 v6, 0x1

    const/4 v4, 0x0

    .line 1889
    sget-boolean v0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mNativeInitialized:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    const-string/jumbo v1, "Suggestions received before native side intialialized"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 1891
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mUrlBar:Lcom/google/android/apps/chrome/omnibox/UrlBar;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/omnibox/UrlBar;->getTextWithoutAutocomplete()Ljava/lang/String;

    move-result-object v2

    .line 1892
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mUrlTextAfterSuggestionsReceived:Ljava/lang/String;

    .line 1898
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mSuggestionItems:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    if-ne v0, v1, :cond_2

    move v3, v4

    move v5, v4

    .line 1899
    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-ge v3, v0, :cond_4

    .line 1900
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mSuggestionItems:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/omnibox/OmniboxResultsAdapter$OmniboxResultItem;

    .line 1901
    invoke-virtual {v0}, Lcom/google/android/apps/chrome/omnibox/OmniboxResultsAdapter$OmniboxResultItem;->getSuggestion()Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion;

    move-result-object v7

    .line 1902
    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion;

    .line 1905
    invoke-virtual {v7, v1}, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_1

    invoke-virtual {v7}, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion;->getType()Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;

    move-result-object v8

    sget-object v9, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;->SEARCH_SUGGEST_INFINITE:Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;

    if-eq v8, v9, :cond_1

    .line 1907
    invoke-virtual {v0}, Lcom/google/android/apps/chrome/omnibox/OmniboxResultsAdapter$OmniboxResultItem;->getMatchedQuery()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_a

    .line 1908
    invoke-virtual {v7}, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion;->getDisplayText()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {v7}, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion;->getUrl()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 1911
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mSuggestionItems:Ljava/util/List;

    new-instance v5, Lcom/google/android/apps/chrome/omnibox/OmniboxResultsAdapter$OmniboxResultItem;

    invoke-direct {v5, v1, v2}, Lcom/google/android/apps/chrome/omnibox/OmniboxResultsAdapter$OmniboxResultItem;-><init>(Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion;Ljava/lang/String;)V

    invoke-interface {v0, v3, v5}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    move v1, v6

    .line 1899
    :goto_1
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    move v5, v1

    goto :goto_0

    .line 1920
    :cond_2
    invoke-direct {p0, v4}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->clearSuggestions(Z)V

    .line 1921
    :goto_2
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-ge v4, v0, :cond_3

    .line 1922
    iget-object v1, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mSuggestionItems:Ljava/util/List;

    new-instance v3, Lcom/google/android/apps/chrome/omnibox/OmniboxResultsAdapter$OmniboxResultItem;

    invoke-interface {p1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion;

    invoke-direct {v3, v0, v2}, Lcom/google/android/apps/chrome/omnibox/OmniboxResultsAdapter$OmniboxResultItem;-><init>(Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion;Ljava/lang/String;)V

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1921
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    :cond_3
    move v4, v6

    move v5, v6

    .line 1926
    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mSuggestionItems:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1927
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->hideSuggestions()V

    .line 1962
    :cond_5
    :goto_3
    return-void

    .line 1932
    :cond_6
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mLastUrlEditWasDelete:Z

    if-nez v0, :cond_7

    invoke-direct {p0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->shouldAutocomplete()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 1933
    const-string/jumbo v0, ""

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 1934
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mUrlBar:Lcom/google/android/apps/chrome/omnibox/UrlBar;

    invoke-virtual {v0, v2, p2}, Lcom/google/android/apps/chrome/omnibox/UrlBar;->setAutocompleteText(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    .line 1939
    :cond_7
    invoke-direct {p0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->initSuggestionList()V

    .line 1940
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mSuggestionList:Lcom/google/android/apps/chrome/omnibox/LocationBar$OmniboxSuggestionsList;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/omnibox/LocationBar$OmniboxSuggestionsList;->resetMaxTextWidths()V

    .line 1942
    if-eqz v5, :cond_8

    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mSuggestionListAdapter:Lcom/google/android/apps/chrome/omnibox/OmniboxResultsAdapter;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/omnibox/OmniboxResultsAdapter;->notifySuggestionsChanged()V

    .line 1943
    :cond_8
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mUrlBar:Lcom/google/android/apps/chrome/omnibox/UrlBar;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/omnibox/UrlBar;->hasFocus()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 1944
    invoke-virtual {p0, v6}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->setSuggestionsListVisibility(Z)V

    .line 1945
    if-eqz v4, :cond_9

    .line 1946
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mSuggestionList:Lcom/google/android/apps/chrome/omnibox/LocationBar$OmniboxSuggestionsList;

    # invokes: Lcom/google/android/apps/chrome/omnibox/LocationBar$OmniboxSuggestionsList;->updateLayoutParams()V
    invoke-static {v0}, Lcom/google/android/apps/chrome/omnibox/LocationBar$OmniboxSuggestionsList;->access$3200(Lcom/google/android/apps/chrome/omnibox/LocationBar$OmniboxSuggestionsList;)V

    .line 1951
    :cond_9
    invoke-direct {p0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->updateNavigationButton()V

    .line 1953
    invoke-static {}, Lorg/chromium/base/CommandLine;->getInstance()Lorg/chromium/base/CommandLine;

    move-result-object v0

    const-string/jumbo v1, "disable-instant"

    invoke-virtual {v0, v1}, Lorg/chromium/base/CommandLine;->hasSwitch(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_5

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->shouldPrerender()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1955
    iget-object v1, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mOmniboxPrerender:Lorg/chromium/chrome/browser/omnibox/OmniboxPrerender;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->getOriginalUrl()Ljava/lang/String;

    move-result-object v3

    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mAutocomplete:Lcom/google/android/apps/chrome/omnibox/ChromeAutocompleteController;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/omnibox/ChromeAutocompleteController;->getCurrentNativeAutocompleteResult()J

    move-result-wide v4

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->getCurrentTab()Lcom/google/android/apps/chrome/tab/ChromeTab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getProfile()Lorg/chromium/chrome/browser/profiles/Profile;

    move-result-object v6

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->getCurrentTab()Lcom/google/android/apps/chrome/tab/ChromeTab;

    move-result-object v7

    invoke-virtual/range {v1 .. v7}, Lorg/chromium/chrome/browser/omnibox/OmniboxPrerender;->prerenderMaybe(Ljava/lang/String;Ljava/lang/String;JLorg/chromium/chrome/browser/profiles/Profile;Lorg/chromium/chrome/browser/Tab;)V

    goto :goto_3

    :cond_a
    move v1, v5

    goto/16 :goto_1
.end method

.method public onUrlFocusChange(Z)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1043
    iput-boolean p1, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mUrlHasFocus:Z

    .line 1044
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->updateFocusSource(Z)V

    .line 1045
    invoke-virtual {p0, v2}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->updateCorpusChipTextVisibility(Z)V

    .line 1046
    invoke-direct {p0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->updateDeleteButtonVisibility()V

    .line 1047
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->updateSnapshotLabelVisibility()V

    .line 1048
    invoke-direct {p0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->updateReaderModeLabelVisibility()V

    .line 1049
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->getCurrentTab()Lcom/google/android/apps/chrome/tab/ChromeTab;

    move-result-object v3

    .line 1050
    if-eqz p1, :cond_8

    .line 1051
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mUrlBar:Lcom/google/android/apps/chrome/omnibox/UrlBar;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/omnibox/UrlBar;->deEmphasizeUrl()V

    .line 1062
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->getToolbarDataProvider()Lcom/google/android/apps/chrome/toolbar/ToolbarDataProvider;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->getToolbarDataProvider()Lcom/google/android/apps/chrome/toolbar/ToolbarDataProvider;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarDataProvider;->isUsingBrandColor()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1063
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->updateVisualsForState()V

    .line 1064
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mUrlHasFocus:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mUrlBar:Lcom/google/android/apps/chrome/omnibox/UrlBar;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/omnibox/UrlBar;->selectAll()V

    .line 1067
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mUrlFocusChangeListener:Lcom/google/android/apps/chrome/omnibox/UrlFocusChangeListener;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mUrlFocusChangeListener:Lcom/google/android/apps/chrome/omnibox/UrlFocusChangeListener;

    invoke-interface {v0, p1}, Lcom/google/android/apps/chrome/omnibox/UrlFocusChangeListener;->onUrlFocusChange(Z)V

    .line 1068
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lorg/chromium/ui/base/DeviceFormFactor;->isTablet(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_3

    if-nez p1, :cond_9

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->isSecurityButtonShown()Z

    move-result v0

    if-eqz v0, :cond_9

    move v0, v1

    :goto_1
    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->changeLocationBarIcon(Z)V

    .line 1070
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mUrlBar:Lcom/google/android/apps/chrome/omnibox/UrlBar;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/omnibox/UrlBar;->setCursorVisible(Z)V

    .line 1071
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mQueryInTheOmnibox:Z

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mUrlBar:Lcom/google/android/apps/chrome/omnibox/UrlBar;

    iget-object v4, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mUrlBar:Lcom/google/android/apps/chrome/omnibox/UrlBar;

    invoke-virtual {v4}, Lcom/google/android/apps/chrome/omnibox/UrlBar;->getSelectionEnd()I

    move-result v4

    invoke-virtual {v0, v4}, Lcom/google/android/apps/chrome/omnibox/UrlBar;->setSelection(I)V

    .line 1073
    :cond_4
    invoke-direct {p0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->updateOmniboxResultsContainer()V

    .line 1074
    if-eqz p1, :cond_5

    invoke-direct {p0, v1}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->updateOmniboxResultsContainerBackground(Z)V

    .line 1076
    :cond_5
    if-eqz p1, :cond_6

    if-eqz v3, :cond_6

    invoke-virtual {v3}, Lorg/chromium/chrome/browser/Tab;->isIncognito()Z

    move-result v0

    if-nez v0, :cond_6

    .line 1077
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mNativeInitialized:Z

    if-eqz v0, :cond_a

    invoke-static {}, Lorg/chromium/chrome/browser/search_engines/TemplateUrlService;->getInstance()Lorg/chromium/chrome/browser/search_engines/TemplateUrlService;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/search_engines/TemplateUrlService;->isDefaultSearchEngineGoogle()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 1079
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/chrome/omnibox/GeolocationHeader;->primeLocationForGeoHeader(Landroid/content/Context;)V

    .line 1092
    :cond_6
    :goto_2
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mNativeInitialized:Z

    if-eqz v0, :cond_b

    .line 1093
    invoke-direct {p0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->startZeroSuggest()V

    .line 1107
    :goto_3
    if-eqz p1, :cond_c

    .line 1108
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mUrlBar:Lcom/google/android/apps/chrome/omnibox/UrlBar;

    iget-object v1, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mTextWatcher:Landroid/text/TextWatcher;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/omnibox/UrlBar;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 1113
    :goto_4
    if-eqz p1, :cond_d

    .line 1114
    iget-wide v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mFirstFocusTimeMs:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_7

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mFirstFocusTimeMs:J

    .line 1115
    :cond_7
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->getContext()Landroid/content/Context;

    move-result-object v0

    const/16 v1, 0x43

    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->broadcastImmediateNotification(Landroid/content/Context;I)V

    .line 1121
    :goto_5
    return-void

    .line 1053
    :cond_8
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->hideSuggestions()V

    .line 1056
    if-eqz v3, :cond_0

    .line 1057
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->setUrlToPageUrl()V

    .line 1058
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mUrlBar:Lcom/google/android/apps/chrome/omnibox/UrlBar;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/omnibox/UrlBar;->emphasizeUrl()V

    goto/16 :goto_0

    :cond_9
    move v0, v2

    .line 1068
    goto :goto_1

    .line 1081
    :cond_a
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mDeferredNativeRunnables:Ljava/util/List;

    new-instance v1, Lcom/google/android/apps/chrome/omnibox/LocationBar$5;

    invoke-direct {v1, p0}, Lcom/google/android/apps/chrome/omnibox/LocationBar$5;-><init>(Lcom/google/android/apps/chrome/omnibox/LocationBar;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 1095
    :cond_b
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mDeferredNativeRunnables:Ljava/util/List;

    new-instance v1, Lcom/google/android/apps/chrome/omnibox/LocationBar$6;

    invoke-direct {v1, p0}, Lcom/google/android/apps/chrome/omnibox/LocationBar$6;-><init>(Lcom/google/android/apps/chrome/omnibox/LocationBar;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 1110
    :cond_c
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mUrlBar:Lcom/google/android/apps/chrome/omnibox/UrlBar;

    iget-object v1, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mTextWatcher:Landroid/text/TextWatcher;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/omnibox/UrlBar;->removeTextChangedListener(Landroid/text/TextWatcher;)V

    goto :goto_4

    .line 1118
    :cond_d
    iput-boolean v2, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mHasStartedNewOmniboxEditSession:Z

    .line 1119
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mNewOmniboxEditSessionTimestamp:J

    goto :goto_5
.end method

.method public onUrlPreFocusChanged(Z)V
    .locals 3

    .prologue
    .line 1029
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mToolbarDataProvider:Lcom/google/android/apps/chrome/toolbar/ToolbarDataProvider;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mToolbarDataProvider:Lcom/google/android/apps/chrome/toolbar/ToolbarDataProvider;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarDataProvider;->getTab()Lorg/chromium/chrome/browser/Tab;

    move-result-object v0

    if-nez v0, :cond_1

    .line 1036
    :cond_0
    :goto_0
    return-void

    .line 1031
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mQueryInTheOmnibox:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/chrome/utilities/FeatureUtilitiesInternal;->isDocumentMode(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mUrlBar:Lcom/google/android/apps/chrome/omnibox/UrlBar;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/omnibox/UrlBar;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1034
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mUrlBar:Lcom/google/android/apps/chrome/omnibox/UrlBar;

    iget-object v1, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mToolbarDataProvider:Lcom/google/android/apps/chrome/toolbar/ToolbarDataProvider;

    invoke-interface {v1}, Lcom/google/android/apps/chrome/toolbar/ToolbarDataProvider;->getTab()Lorg/chromium/chrome/browser/Tab;

    move-result-object v1

    invoke-virtual {v1}, Lorg/chromium/chrome/browser/Tab;->getUrl()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/chrome/omnibox/UrlBar;->setUrl(Ljava/lang/String;Ljava/lang/String;)Z

    goto :goto_0
.end method

.method performSearchQueryForTest(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 1803
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1813
    :goto_0
    return-void

    .line 1805
    :cond_0
    invoke-static {}, Lorg/chromium/chrome/browser/search_engines/TemplateUrlService;->getInstance()Lorg/chromium/chrome/browser/search_engines/TemplateUrlService;

    move-result-object v0

    invoke-virtual {v0, p1}, Lorg/chromium/chrome/browser/search_engines/TemplateUrlService;->getUrlForSearchQuery(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1807
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1808
    const/4 v1, 0x5

    sget-object v2, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;->SEARCH_WHAT_YOU_TYPED:Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->loadUrl(Ljava/lang/String;ILorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;)V

    goto :goto_0

    .line 1811
    :cond_1
    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->setSearchQuery(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public requestUrlFocus()V
    .locals 1

    .prologue
    .line 1168
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mUrlBar:Lcom/google/android/apps/chrome/omnibox/UrlBar;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/omnibox/UrlBar;->requestFocus()Z

    .line 1169
    return-void
.end method

.method public requestUrlFocusFromFakebox(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1180
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mUrlFocusedFromFakebox:Z

    .line 1181
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->requestUrlFocus()V

    .line 1183
    if-eqz p1, :cond_0

    .line 1185
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mUrlBar:Lcom/google/android/apps/chrome/omnibox/UrlBar;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Lcom/google/android/apps/chrome/omnibox/UrlBar;->setUrl(Ljava/lang/String;Ljava/lang/String;)Z

    .line 1186
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mUrlBar:Lcom/google/android/apps/chrome/omnibox/UrlBar;

    iget-object v1, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mUrlBar:Lcom/google/android/apps/chrome/omnibox/UrlBar;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/omnibox/UrlBar;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->length()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/omnibox/UrlBar;->setSelection(I)V

    .line 1188
    :cond_0
    return-void
.end method

.method public setAutocompleteController(Lcom/google/android/apps/chrome/omnibox/ChromeAutocompleteController;)V
    .locals 0

    .prologue
    .line 989
    iput-object p1, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mAutocomplete:Lcom/google/android/apps/chrome/omnibox/ChromeAutocompleteController;

    .line 990
    return-void
.end method

.method public setAutocompleteProfile(Lorg/chromium/chrome/browser/profiles/Profile;)V
    .locals 2

    .prologue
    .line 1001
    sget-boolean v0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mNativeInitialized:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    const-string/jumbo v1, "Setting Autocomplete Profile before native side initialized"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 1002
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mAutocomplete:Lcom/google/android/apps/chrome/omnibox/ChromeAutocompleteController;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/omnibox/ChromeAutocompleteController;->setProfile(Lorg/chromium/chrome/browser/profiles/Profile;)V

    .line 1003
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mOmniboxPrerender:Lorg/chromium/chrome/browser/omnibox/OmniboxPrerender;

    invoke-virtual {v0, p1}, Lorg/chromium/chrome/browser/omnibox/OmniboxPrerender;->initializeForProfile(Lorg/chromium/chrome/browser/profiles/Profile;)V

    .line 1004
    return-void
.end method

.method public setCorpusChipText(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 522
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 523
    const-string/jumbo v0, ""

    .line 531
    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mCorpusChipTextView:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 532
    return-void

    .line 525
    :cond_0
    invoke-static {p0}, Lorg/chromium/base/ApiCompatibilityUtils;->isLayoutRtl(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 526
    new-instance v0, Ljava/lang/StringBuilder;

    const-string/jumbo v1, " :"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 528
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ": "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public setIgnoreURLBarModification(Z)V
    .locals 0

    .prologue
    .line 2163
    iput-boolean p1, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mIgnoreURLBarModification:Z

    .line 2164
    return-void
.end method

.method public setSearchQuery(Ljava/lang/String;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v3, 0x0

    .line 1822
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1848
    :goto_0
    return-void

    .line 1824
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mNativeInitialized:Z

    if-nez v0, :cond_1

    .line 1825
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mDeferredNativeRunnables:Ljava/util/List;

    new-instance v1, Lcom/google/android/apps/chrome/omnibox/LocationBar$9;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/chrome/omnibox/LocationBar$9;-><init>(Lcom/google/android/apps/chrome/omnibox/LocationBar;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1834
    :cond_1
    invoke-direct {p0, p1, v1, v1}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->setUrlBarText(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    .line 1835
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mUrlBar:Lcom/google/android/apps/chrome/omnibox/UrlBar;

    iget-object v1, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mUrlBar:Lcom/google/android/apps/chrome/omnibox/UrlBar;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/omnibox/UrlBar;->getQueryTextOffset()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mUrlBar:Lcom/google/android/apps/chrome/omnibox/UrlBar;

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/omnibox/UrlBar;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-interface {v2}, Landroid/text/Editable;->length()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/chrome/omnibox/UrlBar;->setSelection(II)V

    .line 1836
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mUrlBar:Lcom/google/android/apps/chrome/omnibox/UrlBar;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/omnibox/UrlBar;->requestFocus()Z

    .line 1837
    invoke-direct {p0, v3}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->stopAutocomplete(Z)V

    .line 1838
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->getCurrentTab()Lcom/google/android/apps/chrome/tab/ChromeTab;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1839
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mAutocomplete:Lcom/google/android/apps/chrome/omnibox/ChromeAutocompleteController;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->getCurrentTab()Lcom/google/android/apps/chrome/tab/ChromeTab;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getProfile()Lorg/chromium/chrome/browser/profiles/Profile;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->getCurrentTab()Lcom/google/android/apps/chrome/tab/ChromeTab;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getUrl()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2, p1, v3}, Lcom/google/android/apps/chrome/omnibox/ChromeAutocompleteController;->start(Lorg/chromium/chrome/browser/profiles/Profile;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 1842
    :cond_2
    new-instance v0, Lcom/google/android/apps/chrome/omnibox/LocationBar$10;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/omnibox/LocationBar$10;-><init>(Lcom/google/android/apps/chrome/omnibox/LocationBar;)V

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method protected setSuggestionsListVisibility(Z)V
    .locals 2

    .prologue
    .line 1657
    iput-boolean p1, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mSuggestionsShown:Z

    .line 1658
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mSuggestionList:Lcom/google/android/apps/chrome/omnibox/LocationBar$OmniboxSuggestionsList;

    if-eqz v0, :cond_0

    .line 1659
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mSuggestionList:Lcom/google/android/apps/chrome/omnibox/LocationBar$OmniboxSuggestionsList;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/omnibox/LocationBar$OmniboxSuggestionsList;->isShown()Z

    move-result v0

    .line 1660
    if-eqz p1, :cond_1

    if-nez v0, :cond_1

    .line 1661
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mSuggestionList:Lcom/google/android/apps/chrome/omnibox/LocationBar$OmniboxSuggestionsList;

    # invokes: Lcom/google/android/apps/chrome/omnibox/LocationBar$OmniboxSuggestionsList;->show()V
    invoke-static {v0}, Lcom/google/android/apps/chrome/omnibox/LocationBar$OmniboxSuggestionsList;->access$3300(Lcom/google/android/apps/chrome/omnibox/LocationBar$OmniboxSuggestionsList;)V

    .line 1666
    :cond_0
    :goto_0
    invoke-direct {p0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->updateOmniboxResultsContainer()V

    .line 1667
    return-void

    .line 1662
    :cond_1
    if-nez p1, :cond_0

    if-eqz v0, :cond_0

    .line 1663
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mSuggestionList:Lcom/google/android/apps/chrome/omnibox/LocationBar$OmniboxSuggestionsList;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/omnibox/LocationBar$OmniboxSuggestionsList;->setVisibility(I)V

    goto :goto_0
.end method

.method public setToolbar(Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;Lcom/google/android/apps/chrome/toolbar/ToolbarDataProvider;)V
    .locals 0

    .prologue
    .line 1194
    iput-object p1, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mToolbar:Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;

    .line 1195
    iput-object p2, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mToolbarDataProvider:Lcom/google/android/apps/chrome/toolbar/ToolbarDataProvider;

    .line 1196
    return-void
.end method

.method public setUrlFocusChangeListener(Lcom/google/android/apps/chrome/omnibox/UrlFocusChangeListener;)V
    .locals 0

    .prologue
    .line 1203
    iput-object p1, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mUrlFocusChangeListener:Lcom/google/android/apps/chrome/omnibox/UrlFocusChangeListener;

    .line 1204
    return-void
.end method

.method public setUrlToPageUrl()V
    .locals 10

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    const/4 v5, 0x0

    .line 2044
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mUrlBar:Lcom/google/android/apps/chrome/omnibox/UrlBar;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/omnibox/UrlBar;->hasFocus()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2140
    :goto_0
    return-void

    .line 2046
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->getCurrentTab()Lcom/google/android/apps/chrome/tab/ChromeTab;

    move-result-object v0

    if-nez v0, :cond_1

    .line 2047
    const-string/jumbo v0, ""

    invoke-direct {p0, v0, v5, v5}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->setUrlBarText(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    goto :goto_0

    .line 2052
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->getCurrentTab()Lcom/google/android/apps/chrome/tab/ChromeTab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getProfile()Lorg/chromium/chrome/browser/profiles/Profile;

    move-result-object v8

    .line 2053
    if-eqz v8, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mOmniboxPrerender:Lorg/chromium/chrome/browser/omnibox/OmniboxPrerender;

    invoke-virtual {v0, v8}, Lorg/chromium/chrome/browser/omnibox/OmniboxPrerender;->clear(Lorg/chromium/chrome/browser/profiles/Profile;)V

    .line 2055
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->getCurrentTab()Lcom/google/android/apps/chrome/tab/ChromeTab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getUrl()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    .line 2056
    iput-object v1, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mOriginalUrl:Ljava/lang/String;

    .line 2059
    invoke-static {v1}, Lcom/google/android/apps/chrome/ntp/NewTabPage;->isNTPUrl(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->getCurrentTab()Lcom/google/android/apps/chrome/tab/ChromeTab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->isIncognito()Z

    move-result v0

    invoke-static {v1, v0}, Lcom/google/android/apps/chrome/tab/NativePageFactory;->isNativePageUrl(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_3

    const-string/jumbo v0, "chrome://welcome/"

    invoke-virtual {v1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2063
    :cond_3
    const-string/jumbo v0, ""

    invoke-direct {p0, v0, v5, v5}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->setUrlBarText(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    goto :goto_0

    .line 2069
    :cond_4
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->getCurrentTab()Lcom/google/android/apps/chrome/tab/ChromeTab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->isShowingSnapshot()Z

    move-result v0

    if-eqz v0, :cond_f

    .line 2070
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->getCurrentTab()Lcom/google/android/apps/chrome/tab/ChromeTab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getBaseUrl()Ljava/lang/String;

    move-result-object v0

    .line 2073
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_9

    .line 2083
    :goto_1
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->getCurrentTab()Lcom/google/android/apps/chrome/tab/ChromeTab;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getBackgroundContentViewHelper()Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;

    move-result-object v1

    .line 2085
    if-eqz v1, :cond_a

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->hasPendingBackgroundPage()Z

    move-result v1

    if-eqz v1, :cond_a

    move v1, v2

    .line 2087
    :goto_2
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->showingOriginalUrlForPreview()Z

    move-result v4

    if-eqz v4, :cond_b

    if-nez v1, :cond_b

    move v4, v2

    .line 2089
    :goto_3
    iput-boolean v1, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mShowingOriginalUrlForPreview:Z

    .line 2092
    iget-object v1, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mToolbarDataProvider:Lcom/google/android/apps/chrome/toolbar/ToolbarDataProvider;

    invoke-interface {v1}, Lcom/google/android/apps/chrome/toolbar/ToolbarDataProvider;->getQueryExtractionParam()Ljava/lang/String;

    move-result-object v6

    .line 2093
    iget-object v1, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mToolbarDataProvider:Lcom/google/android/apps/chrome/toolbar/ToolbarDataProvider;

    invoke-interface {v1}, Lcom/google/android/apps/chrome/toolbar/ToolbarDataProvider;->getText()Ljava/lang/String;

    move-result-object v1

    .line 2094
    invoke-direct {p0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->getSecurityLevel()I

    move-result v9

    .line 2095
    const/4 v7, 0x5

    if-eq v9, v7, :cond_e

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_e

    invoke-virtual {v0, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_e

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->isInvalidCorpusChip()Z

    move-result v6

    if-nez v6, :cond_e

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_e

    invoke-direct {p0, v1}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->isUrl(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_e

    .line 2101
    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    move v6, v2

    move-object v7, v0

    .line 2105
    :goto_4
    if-nez v6, :cond_d

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/chrome/utilities/FeatureUtilitiesInternal;->isDocumentMode(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 2106
    invoke-static {v1}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->splitPathFromUrlDisplayText(Ljava/lang/String;)Landroid/util/Pair;

    move-result-object v1

    .line 2107
    iget-object v0, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    .line 2108
    iget-object v1, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Ljava/lang/String;

    .line 2111
    :goto_5
    invoke-static {v7}, Lorg/chromium/components/dom_distiller/core/DomDistillerUrlUtils;->isDistilledPage(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 2112
    invoke-direct {p0, v7}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->isStoredArticle(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_c

    .line 2113
    invoke-static {v8}, Lorg/chromium/chrome/browser/dom_distiller/DomDistillerServiceFactory;->getForProfile(Lorg/chromium/chrome/browser/profiles/Profile;)Lorg/chromium/components/dom_distiller/core/DomDistillerService;

    move-result-object v0

    .line 2115
    const-string/jumbo v5, "entry_id"

    invoke-static {v7, v5}, Lorg/chromium/components/dom_distiller/core/DomDistillerUrlUtils;->getValueForKeyInUrl(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Lorg/chromium/components/dom_distiller/core/DomDistillerService;->getUrlForEntry(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2117
    invoke-static {v0}, Lorg/chromium/chrome/browser/dom_distiller/DomDistillerTabUtils;->getFormattedUrlFromOriginalDistillerUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2126
    :cond_5
    :goto_6
    invoke-direct {p0, v7, v0, v1}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->setUrlBarText(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_6

    if-eqz v4, :cond_7

    .line 2127
    :cond_6
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mUrlBar:Lcom/google/android/apps/chrome/omnibox/UrlBar;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/omnibox/UrlBar;->deEmphasizeUrl()V

    .line 2128
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mUrlBar:Lcom/google/android/apps/chrome/omnibox/UrlBar;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/omnibox/UrlBar;->emphasizeUrl()V

    .line 2130
    :cond_7
    if-eqz v6, :cond_8

    .line 2131
    iput-boolean v2, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mQueryInTheOmnibox:Z

    .line 2132
    invoke-direct {p0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->updateNavigationButton()V

    .line 2135
    const/4 v0, 0x3

    if-ne v9, v0, :cond_8

    .line 2136
    invoke-virtual {p0, v3}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->updateCorpusChipText(Z)V

    .line 2139
    :cond_8
    invoke-direct {p0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->updateCustomSelectionActionModeCallback()V

    goto/16 :goto_0

    .line 2075
    :cond_9
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->getCurrentTab()Lcom/google/android/apps/chrome/tab/ChromeTab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->isLoading()Z

    move-result v0

    if-eqz v0, :cond_f

    .line 2078
    const-string/jumbo v0, ""

    invoke-direct {p0, v0, v5, v5}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->setUrlBarText(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    goto/16 :goto_0

    :cond_a
    move v1, v3

    .line 2085
    goto/16 :goto_2

    :cond_b
    move v4, v3

    .line 2087
    goto/16 :goto_3

    .line 2119
    :cond_c
    invoke-static {v7}, Lorg/chromium/components/dom_distiller/core/DomDistillerUrlUtils;->getOriginalUrlFromDistillerUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_5

    .line 2120
    invoke-static {v7}, Lorg/chromium/components/dom_distiller/core/DomDistillerUrlUtils;->getOriginalUrlFromDistillerUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2121
    invoke-static {v0}, Lorg/chromium/chrome/browser/dom_distiller/DomDistillerTabUtils;->getFormattedUrlFromOriginalDistillerUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_6

    :cond_d
    move-object v0, v1

    move-object v1, v5

    goto :goto_5

    :cond_e
    move v6, v3

    move-object v7, v0

    goto/16 :goto_4

    :cond_f
    move-object v0, v1

    goto/16 :goto_1
.end method

.method public setWindowAndroid(Lorg/chromium/ui/base/WindowAndroid;)V
    .locals 0

    .prologue
    .line 899
    iput-object p1, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mWindowAndroid:Lorg/chromium/ui/base/WindowAndroid;

    .line 900
    return-void
.end method

.method public setWindowDelegate(Lcom/google/android/apps/chrome/WindowDelegate;)V
    .locals 0

    .prologue
    .line 891
    iput-object p1, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mWindowDelegate:Lcom/google/android/apps/chrome/WindowDelegate;

    .line 892
    return-void
.end method

.method protected shouldAnimateIconChanges()Z
    .locals 1

    .prologue
    .line 978
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mUrlHasFocus:Z

    return v0
.end method

.method public shouldSkipAnimation()Z
    .locals 1

    .prologue
    .line 2204
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mShouldSkipAnimation:Z

    return v0
.end method

.method public showingOriginalUrlForPreview()Z
    .locals 1

    .prologue
    .line 1272
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mShowingOriginalUrlForPreview:Z

    return v0
.end method

.method public showingQueryInTheOmnibox()Z
    .locals 1

    .prologue
    .line 1264
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mQueryInTheOmnibox:Z

    return v0
.end method

.method public startVoiceRecognition()V
    .locals 4

    .prologue
    .line 2490
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mWindowAndroid:Lorg/chromium/ui/base/WindowAndroid;

    invoke-virtual {v0}, Lorg/chromium/ui/base/WindowAndroid;->getActivity()Ljava/lang/ref/WeakReference;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    .line 2491
    if-nez v0, :cond_1

    .line 2505
    :cond_0
    :goto_0
    return-void

    .line 2493
    :cond_1
    new-instance v1, Landroid/content/Intent;

    const-string/jumbo v2, "android.speech.action.RECOGNIZE_SPEECH"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2494
    const-string/jumbo v2, "android.speech.extra.LANGUAGE_MODEL"

    const-string/jumbo v3, "web_search"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2496
    const-string/jumbo v2, "calling_package"

    invoke-virtual {v0}, Landroid/app/Activity;->getComponentName()Landroid/content/ComponentName;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/ComponentName;->flattenToString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2498
    const-string/jumbo v2, "android.speech.extra.WEB_SEARCH_ONLY"

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2500
    iget-object v2, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mWindowAndroid:Lorg/chromium/ui/base/WindowAndroid;

    sget v3, Lcom/google/android/apps/chrome/R$string;->voice_search_error:I

    invoke-virtual {v2, v1, p0, v3}, Lorg/chromium/ui/base/WindowAndroid;->showCancelableIntent(Landroid/content/Intent;Lorg/chromium/ui/base/WindowAndroid$IntentCallback;I)I

    move-result v1

    if-gez v1, :cond_0

    .line 2502
    const/4 v1, 0x0

    invoke-static {v0, v1}, Lorg/chromium/chrome/browser/util/FeatureUtilities;->isRecognitionIntentPresent(Landroid/content/Context;Z)Z

    .line 2503
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->updateMicButtonState()V

    goto :goto_0
.end method

.method public updateCorpusChipText(Z)V
    .locals 2

    .prologue
    .line 539
    if-eqz p1, :cond_0

    .line 540
    const-string/jumbo v0, ""

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->setCorpusChipText(Ljava/lang/String;)V

    .line 548
    :goto_0
    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->updateCorpusChipTextVisibility(Z)V

    .line 549
    return-void

    .line 541
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->getCurrentTab()Lcom/google/android/apps/chrome/tab/ChromeTab;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->showingQueryInTheOmnibox()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->getCurrentTab()Lcom/google/android/apps/chrome/tab/ChromeTab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getSecurityLevel()I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_1

    .line 544
    const-string/jumbo v0, "Google"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->setCorpusChipText(Ljava/lang/String;)V

    goto :goto_0

    .line 546
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mToolbarDataProvider:Lcom/google/android/apps/chrome/toolbar/ToolbarDataProvider;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarDataProvider;->getCorpusChipText()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->setCorpusChipText(Ljava/lang/String;)V

    goto :goto_0
.end method

.method updateCorpusChipTextVisibility(Z)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1442
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->getCurrentTab()Lcom/google/android/apps/chrome/tab/ChromeTab;

    move-result-object v0

    .line 1443
    if-nez v0, :cond_1

    .line 1455
    :cond_0
    :goto_0
    return-void

    .line 1444
    :cond_1
    iget-boolean v2, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mUrlHasFocus:Z

    if-nez v2, :cond_2

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/Tab;->getSecurityLevel()I

    move-result v0

    const/4 v2, 0x3

    if-ne v0, v2, :cond_4

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mCorpusChipTextView:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    const/4 v0, 0x1

    .line 1447
    :goto_1
    if-eqz v0, :cond_3

    if-eqz p1, :cond_5

    .line 1448
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mCorpusChipTextView:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    :cond_4
    move v0, v1

    .line 1444
    goto :goto_1

    .line 1449
    :cond_5
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->isCorpusChipTextDisplayed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1450
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mCorpusChipTextView:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1451
    invoke-direct {p0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->addCorpusChipSpan()V

    .line 1452
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mUrlBar:Lcom/google/android/apps/chrome/omnibox/UrlBar;

    iget-boolean v1, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mUrlHasFocus:Z

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/omnibox/UrlBar;->setCursorVisible(Z)V

    .line 1453
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mUrlBar:Lcom/google/android/apps/chrome/omnibox/UrlBar;

    iget-object v1, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mUrlBar:Lcom/google/android/apps/chrome/omnibox/UrlBar;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/omnibox/UrlBar;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->length()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/omnibox/UrlBar;->setSelection(I)V

    goto :goto_0
.end method

.method protected updateDeleteButton(Z)V
    .locals 2

    .prologue
    .line 1527
    iget-object v1, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mDeleteButton:Landroid/widget/ImageButton;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 1528
    return-void

    .line 1527
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method protected updateLayoutParams()V
    .locals 11

    .prologue
    const/4 v10, -0x2

    const/high16 v9, -0x80000000

    const/4 v2, 0x0

    const/high16 v8, 0x40000000    # 2.0f

    const/4 v5, -0x1

    .line 1461
    move v1, v2

    move v3, v2

    .line 1463
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->getChildCount()I

    move-result v0

    if-ge v1, v0, :cond_b

    .line 1464
    invoke-virtual {p0, v1}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->getChildAt(I)Landroid/view/View;

    move-result-object v6

    .line 1465
    invoke-virtual {v6}, Landroid/view/View;->getVisibility()I

    move-result v0

    const/16 v4, 0x8

    if-eq v0, v4, :cond_1

    .line 1466
    invoke-virtual {v6}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    .line 1467
    invoke-static {v0}, Lorg/chromium/base/ApiCompatibilityUtils;->getMarginStart(Landroid/view/ViewGroup$MarginLayoutParams;)I

    move-result v4

    if-eq v4, v3, :cond_0

    .line 1468
    invoke-static {v0, v3}, Lorg/chromium/base/ApiCompatibilityUtils;->setMarginStart(Landroid/view/ViewGroup$MarginLayoutParams;I)V

    .line 1469
    invoke-virtual {v6, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1473
    :cond_0
    iget v4, v0, Landroid/widget/FrameLayout$LayoutParams;->width:I

    if-ne v4, v10, :cond_2

    .line 1474
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->getMeasuredWidth()I

    move-result v4

    invoke-static {v4, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    .line 1483
    :goto_1
    iget v7, v0, Landroid/widget/FrameLayout$LayoutParams;->height:I

    if-ne v7, v10, :cond_4

    .line 1484
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->getMeasuredHeight()I

    move-result v0

    invoke-static {v0, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    .line 1493
    :goto_2
    invoke-virtual {v6, v4, v0}, Landroid/view/View;->measure(II)V

    .line 1494
    invoke-virtual {v6}, Landroid/view/View;->getMeasuredWidth()I

    move-result v0

    add-int/2addr v3, v0

    .line 1497
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mUrlContainer:Lcom/google/android/apps/chrome/omnibox/UrlContainer;

    if-ne v6, v0, :cond_6

    .line 1503
    :goto_3
    sget-boolean v0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->$assertionsDisabled:Z

    if-nez v0, :cond_7

    if-ne v1, v5, :cond_7

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 1476
    :cond_2
    iget v4, v0, Landroid/widget/FrameLayout$LayoutParams;->width:I

    if-ne v4, v5, :cond_3

    .line 1477
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->getMeasuredWidth()I

    move-result v4

    invoke-static {v4, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    goto :goto_1

    .line 1480
    :cond_3
    iget v4, v0, Landroid/widget/FrameLayout$LayoutParams;->width:I

    invoke-static {v4, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    goto :goto_1

    .line 1486
    :cond_4
    iget v7, v0, Landroid/widget/FrameLayout$LayoutParams;->height:I

    if-ne v7, v5, :cond_5

    .line 1487
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->getMeasuredHeight()I

    move-result v0

    invoke-static {v0, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    goto :goto_2

    .line 1490
    :cond_5
    iget v0, v0, Landroid/widget/FrameLayout$LayoutParams;->height:I

    invoke-static {v0, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    goto :goto_2

    .line 1463
    :cond_6
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1505
    :cond_7
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    :goto_4
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->getChildCount()I

    move-result v0

    if-ge v1, v0, :cond_9

    .line 1506
    invoke-virtual {p0, v1}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 1507
    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v3

    const/16 v4, 0x8

    if-eq v3, v4, :cond_8

    .line 1508
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    .line 1509
    iget v3, v0, Landroid/widget/FrameLayout$LayoutParams;->width:I

    invoke-static {v0}, Lorg/chromium/base/ApiCompatibilityUtils;->getMarginStart(Landroid/view/ViewGroup$MarginLayoutParams;)I

    move-result v4

    add-int/2addr v3, v4

    invoke-static {v0}, Lorg/chromium/base/ApiCompatibilityUtils;->getMarginEnd(Landroid/view/ViewGroup$MarginLayoutParams;)I

    move-result v0

    add-int/2addr v0, v3

    invoke-static {v2, v0}, Ljava/lang/Math;->max(II)I

    move-result v2

    .line 1505
    :cond_8
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_4

    .line 1515
    :cond_9
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mUrlContainer:Lcom/google/android/apps/chrome/omnibox/UrlContainer;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/omnibox/UrlContainer;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    .line 1516
    invoke-static {v0}, Lorg/chromium/base/ApiCompatibilityUtils;->getMarginEnd(Landroid/view/ViewGroup$MarginLayoutParams;)I

    move-result v1

    if-eq v1, v2, :cond_a

    .line 1517
    invoke-static {v0, v2}, Lorg/chromium/base/ApiCompatibilityUtils;->setMarginEnd(Landroid/view/ViewGroup$MarginLayoutParams;I)V

    .line 1518
    iget-object v1, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mUrlContainer:Lcom/google/android/apps/chrome/omnibox/UrlContainer;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/chrome/omnibox/UrlContainer;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1520
    :cond_a
    return-void

    :cond_b
    move v1, v5

    goto :goto_3
.end method

.method public updateLoadingState(Z)V
    .locals 1

    .prologue
    .line 2312
    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->setUrlToPageUrl()V

    .line 2313
    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->updateNavigationButton()V

    .line 2314
    invoke-direct {p0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->getSecurityLevel()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->updateSecurityIcon(I)V

    .line 2315
    return-void
.end method

.method protected updateLocationBarIconContainerVisibility()V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1386
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mSecurityButtonShown:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mNavigationButtonType:Lcom/google/android/apps/chrome/omnibox/LocationBar$NavigationButtonType;

    sget-object v2, Lcom/google/android/apps/chrome/omnibox/LocationBar$NavigationButtonType;->EMPTY:Lcom/google/android/apps/chrome/omnibox/LocationBar$NavigationButtonType;

    if-eq v0, v2, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 1388
    :goto_0
    sget v2, Lcom/google/android/apps/chrome/R$id;->location_bar_icon:I

    invoke-virtual {p0, v2}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->findViewById(I)Landroid/view/View;

    move-result-object v2

    if-eqz v0, :cond_2

    :goto_1
    invoke-virtual {v2, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1389
    return-void

    :cond_1
    move v0, v1

    .line 1386
    goto :goto_0

    .line 1388
    :cond_2
    const/16 v1, 0x8

    goto :goto_1
.end method

.method public updateMicButtonState()V
    .locals 2

    .prologue
    .line 2454
    iget-object v1, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mMicButton:Landroid/widget/ImageButton;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->isVoiceSearchEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 2455
    return-void

    .line 2454
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public updateSecurityIcon(I)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v0, 0x0

    .line 1305
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->showingOriginalUrlForPreview()Z

    move-result v1

    if-eqz v1, :cond_0

    move p1, v0

    .line 1308
    :cond_0
    iget-boolean v1, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mQueryInTheOmnibox:Z

    if-eqz v1, :cond_2

    .line 1309
    const/4 v1, 0x2

    if-eq p1, v1, :cond_1

    if-ne p1, v3, :cond_3

    :cond_1
    move p1, v0

    .line 1317
    :cond_2
    :goto_0
    iget v1, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mSecurityIconType:I

    if-ne v1, p1, :cond_5

    .line 1330
    :goto_1
    return-void

    .line 1312
    :cond_3
    const/4 v1, 0x3

    if-eq p1, v1, :cond_4

    const/4 v1, 0x5

    if-ne p1, v1, :cond_2

    .line 1314
    :cond_4
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->setUrlToPageUrl()V

    goto :goto_0

    .line 1318
    :cond_5
    iput p1, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mSecurityIconType:I

    .line 1319
    iget-object v1, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mSecurityButton:Landroid/widget/ImageButton;

    invoke-static {p1}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->getSecurityIconResource(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 1321
    if-nez p1, :cond_6

    .line 1322
    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->updateSecurityButton(Z)V

    .line 1329
    :goto_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mUrlBar:Lcom/google/android/apps/chrome/omnibox/UrlBar;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/omnibox/UrlBar;->emphasizeUrl()V

    goto :goto_1

    .line 1324
    :cond_6
    invoke-direct {p0, v3}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->updateSecurityButton(Z)V

    .line 1327
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mUrlBar:Lcom/google/android/apps/chrome/omnibox/UrlBar;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/omnibox/UrlBar;->deEmphasizeUrl()V

    goto :goto_2
.end method

.method updateSnapshotLabelVisibility()V
    .locals 2

    .prologue
    const/16 v1, 0x8

    .line 1428
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mUrlHasFocus:Z

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->getCurrentTab()Lcom/google/android/apps/chrome/tab/ChromeTab;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->getCurrentTab()Lcom/google/android/apps/chrome/tab/ChromeTab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->isShowingSnapshot()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mUrlBar:Lcom/google/android/apps/chrome/omnibox/UrlBar;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/omnibox/UrlBar;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v0

    if-lez v0, :cond_1

    .line 1430
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mSnapshotPrefixLabel:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getVisibility()I

    move-result v0

    if-ne v0, v1, :cond_0

    .line 1431
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mSnapshotPrefixLabel:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1436
    :cond_0
    :goto_0
    return-void

    .line 1433
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mSnapshotPrefixLabel:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 1434
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mSnapshotPrefixLabel:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method public updateVisualsForState()V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2462
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->getCurrentTab()Lcom/google/android/apps/chrome/tab/ChromeTab;

    move-result-object v2

    .line 2464
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->getToolbarDataProvider()Lcom/google/android/apps/chrome/toolbar/ToolbarDataProvider;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/apps/chrome/toolbar/ToolbarDataProvider;->isUsingBrandColor()Z

    move-result v1

    if-eqz v1, :cond_5

    iget-boolean v1, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mUrlHasFocus:Z

    if-nez v1, :cond_5

    .line 2465
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->getToolbarDataProvider()Lcom/google/android/apps/chrome/toolbar/ToolbarDataProvider;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/apps/chrome/toolbar/ToolbarDataProvider;->getPrimaryColor()I

    move-result v1

    .line 2466
    invoke-static {v1}, Lcom/google/android/apps/chrome/document/BrandColorUtils;->getLightnessForColor(I)F

    move-result v1

    .line 2467
    invoke-static {v1}, Lcom/google/android/apps/chrome/document/BrandColorUtils;->shouldUseLightDrawablesForToolbar(F)Z

    move-result v1

    .line 2470
    :goto_0
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Lorg/chromium/chrome/browser/Tab;->isIncognito()Z

    move-result v2

    if-nez v2, :cond_1

    if-nez v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :cond_1
    iput-boolean v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mUseDarkColors:Z

    .line 2471
    iget-object v1, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mMicButton:Landroid/widget/ImageButton;

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mUseDarkColors:Z

    if-eqz v0, :cond_3

    sget v0, Lcom/google/android/apps/chrome/R$drawable;->btn_omnibox_mic:I

    :goto_1
    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 2473
    iget-object v1, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mDeleteButton:Landroid/widget/ImageButton;

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mUseDarkColors:Z

    if-eqz v0, :cond_4

    sget v0, Lcom/google/android/apps/chrome/R$drawable;->btn_delete_url:I

    :goto_2
    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 2476
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mNavigationButtonType:Lcom/google/android/apps/chrome/omnibox/LocationBar$NavigationButtonType;

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->setNavigationButtonType(Lcom/google/android/apps/chrome/omnibox/LocationBar$NavigationButtonType;)V

    .line 2477
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mUrlBar:Lcom/google/android/apps/chrome/omnibox/UrlBar;

    iget-boolean v1, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mUseDarkColors:Z

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/omnibox/UrlBar;->setUseDarkTextColors(Z)V

    .line 2479
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mSuggestionList:Lcom/google/android/apps/chrome/omnibox/LocationBar$OmniboxSuggestionsList;

    if-eqz v0, :cond_2

    .line 2480
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mSuggestionList:Lcom/google/android/apps/chrome/omnibox/LocationBar$OmniboxSuggestionsList;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->getSuggestionPopupBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-static {v0, v1}, Lorg/chromium/base/ApiCompatibilityUtils;->setBackgroundForView(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 2483
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mSuggestionListAdapter:Lcom/google/android/apps/chrome/omnibox/OmniboxResultsAdapter;

    iget-boolean v1, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mUseDarkColors:Z

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/omnibox/OmniboxResultsAdapter;->setUseDarkColors(Z)V

    .line 2484
    return-void

    .line 2471
    :cond_3
    sget v0, Lcom/google/android/apps/chrome/R$drawable;->btn_omnibox_mic_white:I

    goto :goto_1

    .line 2473
    :cond_4
    sget v0, Lcom/google/android/apps/chrome/R$drawable;->btn_delete_url_white:I

    goto :goto_2

    :cond_5
    move v1, v0

    goto :goto_0
.end method
