.class Lcom/google/android/apps/chrome/ChromeMobileApplication$BackgroundProcessing;
.super Ljava/lang/Object;
.source "ChromeMobileApplication.java"


# instance fields
.field private final mHandler:Landroid/os/Handler;

.field private mSuspendRunnable:Lcom/google/android/apps/chrome/ChromeMobileApplication$BackgroundProcessing$SuspendRunnable;

.field private mWebKitTimersAreSuspended:Z


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 109
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 121
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/ChromeMobileApplication$BackgroundProcessing;->mHandler:Landroid/os/Handler;

    .line 122
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/ChromeMobileApplication$BackgroundProcessing;->mWebKitTimersAreSuspended:Z

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/chrome/ChromeMobileApplication$1;)V
    .locals 0

    .prologue
    .line 109
    invoke-direct {p0}, Lcom/google/android/apps/chrome/ChromeMobileApplication$BackgroundProcessing;-><init>()V

    return-void
.end method

.method static synthetic access$002(Lcom/google/android/apps/chrome/ChromeMobileApplication$BackgroundProcessing;Lcom/google/android/apps/chrome/ChromeMobileApplication$BackgroundProcessing$SuspendRunnable;)Lcom/google/android/apps/chrome/ChromeMobileApplication$BackgroundProcessing$SuspendRunnable;
    .locals 0

    .prologue
    .line 109
    iput-object p1, p0, Lcom/google/android/apps/chrome/ChromeMobileApplication$BackgroundProcessing;->mSuspendRunnable:Lcom/google/android/apps/chrome/ChromeMobileApplication$BackgroundProcessing$SuspendRunnable;

    return-object p1
.end method

.method static synthetic access$100(Lcom/google/android/apps/chrome/ChromeMobileApplication$BackgroundProcessing;)Z
    .locals 1

    .prologue
    .line 109
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/ChromeMobileApplication$BackgroundProcessing;->mWebKitTimersAreSuspended:Z

    return v0
.end method

.method static synthetic access$102(Lcom/google/android/apps/chrome/ChromeMobileApplication$BackgroundProcessing;Z)Z
    .locals 0

    .prologue
    .line 109
    iput-boolean p1, p0, Lcom/google/android/apps/chrome/ChromeMobileApplication$BackgroundProcessing;->mWebKitTimersAreSuspended:Z

    return p1
.end method

.method static synthetic access$400(Lcom/google/android/apps/chrome/ChromeMobileApplication$BackgroundProcessing;)V
    .locals 0

    .prologue
    .line 109
    invoke-direct {p0}, Lcom/google/android/apps/chrome/ChromeMobileApplication$BackgroundProcessing;->startTimers()V

    return-void
.end method

.method static synthetic access$500(Lcom/google/android/apps/chrome/ChromeMobileApplication$BackgroundProcessing;)V
    .locals 0

    .prologue
    .line 109
    invoke-direct {p0}, Lcom/google/android/apps/chrome/ChromeMobileApplication$BackgroundProcessing;->suspendTimers()V

    return-void
.end method

.method static synthetic access$600(Lcom/google/android/apps/chrome/ChromeMobileApplication$BackgroundProcessing;)V
    .locals 0

    .prologue
    .line 109
    invoke-direct {p0}, Lcom/google/android/apps/chrome/ChromeMobileApplication$BackgroundProcessing;->onDestroy()V

    return-void
.end method

.method private onDestroy()V
    .locals 2

    .prologue
    .line 126
    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeMobileApplication$BackgroundProcessing;->mSuspendRunnable:Lcom/google/android/apps/chrome/ChromeMobileApplication$BackgroundProcessing$SuspendRunnable;

    if-eqz v0, :cond_0

    .line 127
    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeMobileApplication$BackgroundProcessing;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/chrome/ChromeMobileApplication$BackgroundProcessing;->mSuspendRunnable:Lcom/google/android/apps/chrome/ChromeMobileApplication$BackgroundProcessing$SuspendRunnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 128
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/chrome/ChromeMobileApplication$BackgroundProcessing;->mSuspendRunnable:Lcom/google/android/apps/chrome/ChromeMobileApplication$BackgroundProcessing$SuspendRunnable;

    .line 130
    :cond_0
    return-void
.end method

.method private startTimers()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 140
    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeMobileApplication$BackgroundProcessing;->mSuspendRunnable:Lcom/google/android/apps/chrome/ChromeMobileApplication$BackgroundProcessing$SuspendRunnable;

    if-eqz v0, :cond_1

    .line 141
    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeMobileApplication$BackgroundProcessing;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/chrome/ChromeMobileApplication$BackgroundProcessing;->mSuspendRunnable:Lcom/google/android/apps/chrome/ChromeMobileApplication$BackgroundProcessing$SuspendRunnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 142
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/chrome/ChromeMobileApplication$BackgroundProcessing;->mSuspendRunnable:Lcom/google/android/apps/chrome/ChromeMobileApplication$BackgroundProcessing$SuspendRunnable;

    .line 147
    :cond_0
    :goto_0
    return-void

    .line 143
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/ChromeMobileApplication$BackgroundProcessing;->mWebKitTimersAreSuspended:Z

    if-eqz v0, :cond_0

    .line 144
    invoke-static {v1}, Lorg/chromium/content/browser/ContentViewStatics;->setWebKitSharedTimersSuspended(Z)V

    .line 145
    iput-boolean v1, p0, Lcom/google/android/apps/chrome/ChromeMobileApplication$BackgroundProcessing;->mWebKitTimersAreSuspended:Z

    goto :goto_0
.end method

.method private suspendTimers()V
    .locals 4

    .prologue
    .line 133
    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeMobileApplication$BackgroundProcessing;->mSuspendRunnable:Lcom/google/android/apps/chrome/ChromeMobileApplication$BackgroundProcessing$SuspendRunnable;

    if-nez v0, :cond_0

    .line 134
    new-instance v0, Lcom/google/android/apps/chrome/ChromeMobileApplication$BackgroundProcessing$SuspendRunnable;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/chrome/ChromeMobileApplication$BackgroundProcessing$SuspendRunnable;-><init>(Lcom/google/android/apps/chrome/ChromeMobileApplication$BackgroundProcessing;Lcom/google/android/apps/chrome/ChromeMobileApplication$1;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/ChromeMobileApplication$BackgroundProcessing;->mSuspendRunnable:Lcom/google/android/apps/chrome/ChromeMobileApplication$BackgroundProcessing$SuspendRunnable;

    .line 135
    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeMobileApplication$BackgroundProcessing;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/chrome/ChromeMobileApplication$BackgroundProcessing;->mSuspendRunnable:Lcom/google/android/apps/chrome/ChromeMobileApplication$BackgroundProcessing$SuspendRunnable;

    const-wide/32 v2, 0x493e0

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 137
    :cond_0
    return-void
.end method
