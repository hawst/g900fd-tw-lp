.class Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItemsAdapter;
.super Landroid/widget/BaseAdapter;
.source "EnhancedBookmarkItemsAdapter.java"


# static fields
.field static final synthetic $assertionsDisabled:Z

.field public static final BOOKMARK_VIEW:I = 0x2

.field public static final FOLDER_VIEW:I = 0x0

.field public static final PADDING_VIEW:I = 0x1


# instance fields
.field private mBookmarks:Ljava/util/List;

.field private mColumnCount:I

.field private mDelegate:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDelegate;

.field private mFolders:Ljava/util/List;

.field private mPaddingViewCount:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 23
    const-class v0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItemsAdapter;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItemsAdapter;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>()V
    .locals 1

    .prologue
    .line 38
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 29
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItemsAdapter;->mFolders:Ljava/util/List;

    .line 30
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItemsAdapter;->mBookmarks:Ljava/util/List;

    .line 35
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItemsAdapter;->mColumnCount:I

    .line 39
    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 2

    .prologue
    .line 79
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItemsAdapter;->mFolders:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    iget v1, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItemsAdapter;->mPaddingViewCount:I

    add-int/2addr v0, v1

    iget-object v1, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItemsAdapter;->mBookmarks:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 84
    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItemsAdapter;->getItemViewType(I)I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 92
    sget-boolean v1, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItemsAdapter;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 86
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItemsAdapter;->mFolders:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    .line 93
    :cond_0
    :goto_0
    :pswitch_1
    return-object v0

    .line 90
    :pswitch_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItemsAdapter;->mBookmarks:Ljava/util/List;

    iget-object v1, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItemsAdapter;->mFolders:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    sub-int v1, p1, v1

    iget v2, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItemsAdapter;->mPaddingViewCount:I

    sub-int/2addr v1, v2

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    .line 84
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public getItemId(I)J
    .locals 2

    .prologue
    .line 100
    int-to-long v0, p1

    return-wide v0
.end method

.method public getItemViewType(I)I
    .locals 2

    .prologue
    .line 105
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItemsAdapter;->mFolders:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge p1, v0, :cond_0

    const/4 v0, 0x0

    .line 116
    :goto_0
    return v0

    .line 108
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItemsAdapter;->mFolders:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    sub-int v0, p1, v0

    .line 110
    iget v1, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItemsAdapter;->mPaddingViewCount:I

    if-ge v0, v1, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    .line 111
    :cond_1
    iget v1, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItemsAdapter;->mPaddingViewCount:I

    sub-int/2addr v0, v1

    .line 113
    iget-object v1, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItemsAdapter;->mBookmarks:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_2

    const/4 v0, 0x2

    goto :goto_0

    .line 115
    :cond_2
    sget-boolean v0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItemsAdapter;->$assertionsDisabled:Z

    if-nez v0, :cond_3

    new-instance v0, Ljava/lang/AssertionError;

    const-string/jumbo v1, "Invalid position requested"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 116
    :cond_3
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 131
    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItemsAdapter;->getItemViewType(I)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 171
    sget-boolean v0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItemsAdapter;->$assertionsDisabled:Z

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 133
    :pswitch_0
    check-cast p2, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkFolder;

    .line 134
    if-nez p2, :cond_0

    .line 135
    invoke-virtual {p3}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Lcom/google/android/apps/chrome/R$layout;->eb_folder:I

    invoke-virtual {v0, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkFolder;

    move-object p2, v0

    .line 140
    :cond_0
    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItemsAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/chromium/components/bookmarks/BookmarkId;

    .line 141
    invoke-virtual {p2, v0}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkFolder;->setBookmarkId(Lorg/chromium/components/bookmarks/BookmarkId;)V

    .line 142
    iget-object v1, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItemsAdapter;->mDelegate:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDelegate;

    invoke-interface {v1}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDelegate;->getModel()Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarksModel;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarksModel;->getBookmarkById(Lorg/chromium/components/bookmarks/BookmarkId;)Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkItem;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkItem;->getTitle()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkFolder;->setText(Ljava/lang/CharSequence;)V

    .line 172
    :goto_0
    return-object p2

    .line 149
    :pswitch_1
    check-cast p2, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkFolder;

    .line 150
    if-nez p2, :cond_3

    .line 151
    invoke-virtual {p3}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Lcom/google/android/apps/chrome/R$layout;->eb_folder:I

    invoke-virtual {v0, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkFolder;

    .line 155
    :goto_1
    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkFolder;->setVisibility(I)V

    move-object p2, v0

    .line 156
    goto :goto_0

    .line 159
    :pswitch_2
    check-cast p2, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem;

    .line 160
    if-nez p2, :cond_1

    .line 161
    invoke-virtual {p3}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Lcom/google/android/apps/chrome/R$layout;->eb_item:I

    invoke-virtual {v0, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem;

    move-object p2, v0

    .line 166
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItemsAdapter;->mDelegate:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDelegate;

    invoke-virtual {p2, v0}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem;->setEnhancedBookmarkDelegate(Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDelegate;)V

    .line 167
    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItemsAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/chromium/components/bookmarks/BookmarkId;

    invoke-virtual {p2, v0}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem;->setBookmarkId(Lorg/chromium/components/bookmarks/BookmarkId;)V

    goto :goto_0

    .line 172
    :cond_2
    const/4 p2, 0x0

    goto :goto_0

    :cond_3
    move-object v0, p2

    goto :goto_1

    .line 131
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public getViewTypeCount()I
    .locals 1

    .prologue
    .line 121
    const/4 v0, 0x3

    return v0
.end method

.method public hasStableIds()Z
    .locals 1

    .prologue
    .line 126
    const/4 v0, 0x0

    return v0
.end method

.method setBookmarks(Ljava/util/List;Ljava/util/List;)V
    .locals 0

    .prologue
    .line 50
    if-nez p1, :cond_0

    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    .line 52
    :cond_0
    iput-object p1, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItemsAdapter;->mFolders:Ljava/util/List;

    .line 53
    iput-object p2, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItemsAdapter;->mBookmarks:Ljava/util/List;

    .line 54
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItemsAdapter;->updateDataset()V

    .line 55
    return-void
.end method

.method setEnhancedBookmarkDelegate(Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDelegate;)V
    .locals 0

    .prologue
    .line 42
    iput-object p1, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItemsAdapter;->mDelegate:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDelegate;

    .line 43
    return-void
.end method

.method setNumColumns(I)V
    .locals 0

    .prologue
    .line 66
    iput p1, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItemsAdapter;->mColumnCount:I

    .line 67
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItemsAdapter;->updateDataset()V

    .line 68
    return-void
.end method

.method updateDataset()V
    .locals 2

    .prologue
    .line 71
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItemsAdapter;->mFolders:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    neg-int v0, v0

    iget v1, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItemsAdapter;->mColumnCount:I

    invoke-static {v0, v1}, Lorg/chromium/chrome/browser/util/MathUtils;->positiveModulo(II)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItemsAdapter;->mPaddingViewCount:I

    .line 72
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItemsAdapter;->notifyDataSetChanged()V

    .line 73
    return-void
.end method
