.class Lcom/google/android/apps/chrome/document/home/MostVisitedPage$1;
.super Ljava/lang/Object;
.source "MostVisitedPage.java"

# interfaces
.implements Lcom/google/android/apps/chrome/ntp/NewTabPageView$NewTabPageManager;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/document/home/MostVisitedPage;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/document/home/MostVisitedPage;)V
    .locals 0

    .prologue
    .line 72
    iput-object p1, p0, Lcom/google/android/apps/chrome/document/home/MostVisitedPage$1;->this$0:Lcom/google/android/apps/chrome/document/home/MostVisitedPage;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private recordOpenedMostVisitedItem(Lcom/google/android/apps/chrome/ntp/MostVisitedItem;)V
    .locals 2

    .prologue
    .line 74
    const/4 v0, 0x3

    invoke-static {v0}, Lcom/google/android/apps/chrome/ntp/NewTabPageUma;->recordAction(I)V

    .line 75
    invoke-virtual {p1}, Lcom/google/android/apps/chrome/ntp/MostVisitedItem;->getIndex()I

    move-result v0

    const/4 v1, 0x6

    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->ntpMostVisitedIndex(II)V

    .line 77
    const/4 v0, 0x1

    invoke-static {v0}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->recordDocumentActivityHomeExitAction(I)V

    .line 79
    return-void
.end method


# virtual methods
.method public focusSearchBox(ZLjava/lang/String;)V
    .locals 0

    .prologue
    .line 172
    return-void
.end method

.method public getLocalFaviconImageForURL(Ljava/lang/String;ILorg/chromium/chrome/browser/favicon/FaviconHelper$FaviconImageCallback;)V
    .locals 6

    .prologue
    .line 142
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/home/MostVisitedPage$1;->this$0:Lcom/google/android/apps/chrome/document/home/MostVisitedPage;

    # getter for: Lcom/google/android/apps/chrome/document/home/MostVisitedPage;->mFaviconHelper:Lorg/chromium/chrome/browser/favicon/FaviconHelper;
    invoke-static {v0}, Lcom/google/android/apps/chrome/document/home/MostVisitedPage;->access$400(Lcom/google/android/apps/chrome/document/home/MostVisitedPage;)Lorg/chromium/chrome/browser/favicon/FaviconHelper;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/document/home/MostVisitedPage$1;->this$0:Lcom/google/android/apps/chrome/document/home/MostVisitedPage;

    # getter for: Lcom/google/android/apps/chrome/document/home/MostVisitedPage;->mProfile:Lorg/chromium/chrome/browser/profiles/Profile;
    invoke-static {v1}, Lcom/google/android/apps/chrome/document/home/MostVisitedPage;->access$300(Lcom/google/android/apps/chrome/document/home/MostVisitedPage;)Lorg/chromium/chrome/browser/profiles/Profile;

    move-result-object v1

    const/4 v3, 0x7

    move-object v2, p1

    move v4, p2

    move-object v5, p3

    invoke-virtual/range {v0 .. v5}, Lorg/chromium/chrome/browser/favicon/FaviconHelper;->getLocalFaviconImageForURL(Lorg/chromium/chrome/browser/profiles/Profile;Ljava/lang/String;IILorg/chromium/chrome/browser/favicon/FaviconHelper$FaviconImageCallback;)Z

    .line 145
    return-void
.end method

.method public getSearchProviderLogo(Lorg/chromium/chrome/browser/LogoBridge$LogoObserver;)V
    .locals 0

    .prologue
    .line 180
    return-void
.end method

.method public getURLThumbnail(Ljava/lang/String;Lorg/chromium/chrome/browser/profiles/MostVisitedSites$ThumbnailCallback;)V
    .locals 1

    .prologue
    .line 136
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/home/MostVisitedPage$1;->this$0:Lcom/google/android/apps/chrome/document/home/MostVisitedPage;

    # getter for: Lcom/google/android/apps/chrome/document/home/MostVisitedPage;->mMostVisitedSites:Lorg/chromium/chrome/browser/profiles/MostVisitedSites;
    invoke-static {v0}, Lcom/google/android/apps/chrome/document/home/MostVisitedPage;->access$200(Lcom/google/android/apps/chrome/document/home/MostVisitedPage;)Lorg/chromium/chrome/browser/profiles/MostVisitedSites;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lorg/chromium/chrome/browser/profiles/MostVisitedSites;->getURLThumbnail(Ljava/lang/String;Lorg/chromium/chrome/browser/profiles/MostVisitedSites$ThumbnailCallback;)V

    .line 137
    return-void
.end method

.method public isLocationBarShownInNTP()Z
    .locals 1

    .prologue
    .line 167
    const/4 v0, 0x0

    return v0
.end method

.method public navigateToBookmarks()V
    .locals 2

    .prologue
    .line 153
    const/4 v0, 0x3

    invoke-static {v0}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->recordDocumentActivityHomeExitAction(I)V

    .line 155
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/home/MostVisitedPage$1;->this$0:Lcom/google/android/apps/chrome/document/home/MostVisitedPage;

    # getter for: Lcom/google/android/apps/chrome/document/home/MostVisitedPage;->mActivity:Lcom/google/android/apps/chrome/document/DocumentActivity;
    invoke-static {v0}, Lcom/google/android/apps/chrome/document/home/MostVisitedPage;->access$100(Lcom/google/android/apps/chrome/document/home/MostVisitedPage;)Lcom/google/android/apps/chrome/document/DocumentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/document/home/MostVisitedPage$1;->this$0:Lcom/google/android/apps/chrome/document/home/MostVisitedPage;

    # getter for: Lcom/google/android/apps/chrome/document/home/MostVisitedPage;->mTab:Lcom/google/android/apps/chrome/tab/ChromeTab;
    invoke-static {v1}, Lcom/google/android/apps/chrome/document/home/MostVisitedPage;->access$000(Lcom/google/android/apps/chrome/document/home/MostVisitedPage;)Lcom/google/android/apps/chrome/tab/ChromeTab;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/document/home/MostVisitedPage;->launchBookmarksDialog(Lcom/google/android/apps/chrome/document/DocumentActivity;Lorg/chromium/chrome/browser/Tab;)V

    .line 156
    return-void
.end method

.method public navigateToRecentTabs()V
    .locals 3

    .prologue
    .line 160
    const/4 v0, 0x4

    invoke-static {v0}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->recordDocumentActivityHomeExitAction(I)V

    .line 162
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/home/MostVisitedPage$1;->this$0:Lcom/google/android/apps/chrome/document/home/MostVisitedPage;

    # getter for: Lcom/google/android/apps/chrome/document/home/MostVisitedPage;->mActivity:Lcom/google/android/apps/chrome/document/DocumentActivity;
    invoke-static {v0}, Lcom/google/android/apps/chrome/document/home/MostVisitedPage;->access$100(Lcom/google/android/apps/chrome/document/home/MostVisitedPage;)Lcom/google/android/apps/chrome/document/DocumentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/document/home/MostVisitedPage$1;->this$0:Lcom/google/android/apps/chrome/document/home/MostVisitedPage;

    # getter for: Lcom/google/android/apps/chrome/document/home/MostVisitedPage;->mTab:Lcom/google/android/apps/chrome/tab/ChromeTab;
    invoke-static {v1}, Lcom/google/android/apps/chrome/document/home/MostVisitedPage;->access$000(Lcom/google/android/apps/chrome/document/home/MostVisitedPage;)Lcom/google/android/apps/chrome/tab/ChromeTab;

    move-result-object v1

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/chrome/document/home/MostVisitedPage;->launchRecentTabsDialog(Landroid/app/Activity;Lcom/google/android/apps/chrome/tab/ChromeTab;Z)V

    .line 163
    return-void
.end method

.method public notifyLoadingComplete()V
    .locals 0

    .prologue
    .line 149
    return-void
.end method

.method public onCreateContextMenu(Landroid/view/ContextMenu;Landroid/view/MenuItem$OnMenuItemClickListener;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 89
    sget v0, Lcom/google/android/apps/chrome/R$string;->contextmenu_open_in_new_tab:I

    invoke-interface {p1, v2, v2, v2, v0}, Landroid/view/ContextMenu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, p2}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 91
    invoke-static {}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->getInstance()Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->isIncognitoModeEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 92
    const/4 v0, 0x1

    sget v1, Lcom/google/android/apps/chrome/R$string;->contextmenu_open_in_incognito_tab:I

    invoke-interface {p1, v2, v0, v2, v1}, Landroid/view/ContextMenu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, p2}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 96
    :cond_0
    const/4 v0, 0x2

    sget v1, Lcom/google/android/apps/chrome/R$string;->remove:I

    invoke-interface {p1, v2, v0, v2, v1}, Landroid/view/ContextMenu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, p2}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 98
    return-void
.end method

.method public onMenuItemClick(ILcom/google/android/apps/chrome/ntp/MostVisitedItem;)Z
    .locals 8

    .prologue
    .line 102
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/home/MostVisitedPage$1;->this$0:Lcom/google/android/apps/chrome/document/home/MostVisitedPage;

    # getter for: Lcom/google/android/apps/chrome/document/home/MostVisitedPage;->mTab:Lcom/google/android/apps/chrome/tab/ChromeTab;
    invoke-static {v0}, Lcom/google/android/apps/chrome/document/home/MostVisitedPage;->access$000(Lcom/google/android/apps/chrome/document/home/MostVisitedPage;)Lcom/google/android/apps/chrome/tab/ChromeTab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getUseDesktopUserAgent()Z

    move-result v6

    .line 103
    packed-switch p1, :pswitch_data_0

    .line 125
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 105
    :pswitch_0
    invoke-direct {p0, p2}, Lcom/google/android/apps/chrome/document/home/MostVisitedPage$1;->recordOpenedMostVisitedItem(Lcom/google/android/apps/chrome/ntp/MostVisitedItem;)V

    .line 106
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/home/MostVisitedPage$1;->this$0:Lcom/google/android/apps/chrome/document/home/MostVisitedPage;

    # getter for: Lcom/google/android/apps/chrome/document/home/MostVisitedPage;->mActivity:Lcom/google/android/apps/chrome/document/DocumentActivity;
    invoke-static {v0}, Lcom/google/android/apps/chrome/document/home/MostVisitedPage;->access$100(Lcom/google/android/apps/chrome/document/home/MostVisitedPage;)Lcom/google/android/apps/chrome/document/DocumentActivity;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {p2}, Lcom/google/android/apps/chrome/ntp/MostVisitedItem;->getUrl()Ljava/lang/String;

    move-result-object v3

    const/16 v4, 0x64

    const/4 v5, 0x6

    const/4 v7, 0x0

    invoke-static/range {v0 .. v7}, Lcom/google/android/apps/chrome/document/ChromeLauncherActivity;->launchInstance(Landroid/app/Activity;ZILjava/lang/String;IIZLcom/google/android/apps/chrome/document/DocumentActivityDatabase$PendingDocumentData;)V

    .line 111
    const/4 v0, 0x1

    goto :goto_0

    .line 113
    :pswitch_1
    invoke-direct {p0, p2}, Lcom/google/android/apps/chrome/document/home/MostVisitedPage$1;->recordOpenedMostVisitedItem(Lcom/google/android/apps/chrome/ntp/MostVisitedItem;)V

    .line 114
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/home/MostVisitedPage$1;->this$0:Lcom/google/android/apps/chrome/document/home/MostVisitedPage;

    # getter for: Lcom/google/android/apps/chrome/document/home/MostVisitedPage;->mActivity:Lcom/google/android/apps/chrome/document/DocumentActivity;
    invoke-static {v0}, Lcom/google/android/apps/chrome/document/home/MostVisitedPage;->access$100(Lcom/google/android/apps/chrome/document/home/MostVisitedPage;)Lcom/google/android/apps/chrome/document/DocumentActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/document/DocumentActivity;->finishAndRemoveTask()V

    .line 115
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/home/MostVisitedPage$1;->this$0:Lcom/google/android/apps/chrome/document/home/MostVisitedPage;

    # getter for: Lcom/google/android/apps/chrome/document/home/MostVisitedPage;->mActivity:Lcom/google/android/apps/chrome/document/DocumentActivity;
    invoke-static {v0}, Lcom/google/android/apps/chrome/document/home/MostVisitedPage;->access$100(Lcom/google/android/apps/chrome/document/home/MostVisitedPage;)Lcom/google/android/apps/chrome/document/DocumentActivity;

    move-result-object v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {p2}, Lcom/google/android/apps/chrome/ntp/MostVisitedItem;->getUrl()Ljava/lang/String;

    move-result-object v3

    const/16 v4, 0x64

    const/4 v5, 0x6

    const/4 v7, 0x0

    invoke-static/range {v0 .. v7}, Lcom/google/android/apps/chrome/document/ChromeLauncherActivity;->launchInstance(Landroid/app/Activity;ZILjava/lang/String;IIZLcom/google/android/apps/chrome/document/DocumentActivityDatabase$PendingDocumentData;)V

    .line 120
    const/4 v0, 0x1

    goto :goto_0

    .line 122
    :pswitch_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/home/MostVisitedPage$1;->this$0:Lcom/google/android/apps/chrome/document/home/MostVisitedPage;

    # getter for: Lcom/google/android/apps/chrome/document/home/MostVisitedPage;->mMostVisitedSites:Lorg/chromium/chrome/browser/profiles/MostVisitedSites;
    invoke-static {v0}, Lcom/google/android/apps/chrome/document/home/MostVisitedPage;->access$200(Lcom/google/android/apps/chrome/document/home/MostVisitedPage;)Lorg/chromium/chrome/browser/profiles/MostVisitedSites;

    move-result-object v0

    invoke-virtual {p2}, Lcom/google/android/apps/chrome/ntp/MostVisitedItem;->getUrl()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/chromium/chrome/browser/profiles/MostVisitedSites;->blacklistUrl(Ljava/lang/String;)V

    .line 123
    const/4 v0, 0x1

    goto :goto_0

    .line 103
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public open(Lcom/google/android/apps/chrome/ntp/MostVisitedItem;)V
    .locals 4

    .prologue
    .line 83
    invoke-virtual {p1}, Lcom/google/android/apps/chrome/ntp/MostVisitedItem;->getUrl()Ljava/lang/String;

    move-result-object v0

    .line 84
    iget-object v1, p0, Lcom/google/android/apps/chrome/document/home/MostVisitedPage$1;->this$0:Lcom/google/android/apps/chrome/document/home/MostVisitedPage;

    # getter for: Lcom/google/android/apps/chrome/document/home/MostVisitedPage;->mTab:Lcom/google/android/apps/chrome/tab/ChromeTab;
    invoke-static {v1}, Lcom/google/android/apps/chrome/document/home/MostVisitedPage;->access$000(Lcom/google/android/apps/chrome/document/home/MostVisitedPage;)Lcom/google/android/apps/chrome/tab/ChromeTab;

    move-result-object v1

    new-instance v2, Lorg/chromium/content_public/browser/LoadUrlParams;

    const/4 v3, 0x0

    invoke-direct {v2, v0, v3}, Lorg/chromium/content_public/browser/LoadUrlParams;-><init>(Ljava/lang/String;I)V

    invoke-virtual {v1, v2}, Lcom/google/android/apps/chrome/tab/ChromeTab;->loadUrl(Lorg/chromium/content_public/browser/LoadUrlParams;)I

    .line 85
    return-void
.end method

.method public openLogoLink()V
    .locals 0

    .prologue
    .line 176
    return-void
.end method

.method public optOutPromoSettingsSelected()V
    .locals 3

    .prologue
    .line 192
    const/4 v0, 0x1

    invoke-static {v0}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->recordDocumentOptOutPromoClick(I)V

    .line 194
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 195
    iget-object v1, p0, Lcom/google/android/apps/chrome/document/home/MostVisitedPage$1;->this$0:Lcom/google/android/apps/chrome/document/home/MostVisitedPage;

    # getter for: Lcom/google/android/apps/chrome/document/home/MostVisitedPage;->mActivity:Lcom/google/android/apps/chrome/document/DocumentActivity;
    invoke-static {v1}, Lcom/google/android/apps/chrome/document/home/MostVisitedPage;->access$100(Lcom/google/android/apps/chrome/document/home/MostVisitedPage;)Lcom/google/android/apps/chrome/document/DocumentActivity;

    move-result-object v1

    const-class v2, Lcom/google/android/apps/chrome/preferences/Preferences;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    .line 196
    const/high16 v1, 0x20020000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 198
    const-string/jumbo v1, ":android:show_fragment"

    const-class v2, Lcom/google/android/apps/chrome/preferences/DocumentModePreference;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 200
    const-string/jumbo v1, ":android:show_fragment_title"

    sget v2, Lcom/google/android/apps/chrome/R$string;->tabs_and_apps_title:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 202
    const-string/jumbo v1, "display_home_as_up"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 203
    iget-object v1, p0, Lcom/google/android/apps/chrome/document/home/MostVisitedPage$1;->this$0:Lcom/google/android/apps/chrome/document/home/MostVisitedPage;

    # getter for: Lcom/google/android/apps/chrome/document/home/MostVisitedPage;->mActivity:Lcom/google/android/apps/chrome/document/DocumentActivity;
    invoke-static {v1}, Lcom/google/android/apps/chrome/document/home/MostVisitedPage;->access$100(Lcom/google/android/apps/chrome/document/home/MostVisitedPage;)Lcom/google/android/apps/chrome/document/DocumentActivity;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/android/apps/chrome/document/DocumentActivity;->startActivity(Landroid/content/Intent;)V

    .line 204
    return-void
.end method

.method public setMostVisitedURLsObserver(Lorg/chromium/chrome/browser/profiles/MostVisitedSites$MostVisitedURLsObserver;I)V
    .locals 1

    .prologue
    .line 131
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/home/MostVisitedPage$1;->this$0:Lcom/google/android/apps/chrome/document/home/MostVisitedPage;

    # getter for: Lcom/google/android/apps/chrome/document/home/MostVisitedPage;->mMostVisitedSites:Lorg/chromium/chrome/browser/profiles/MostVisitedSites;
    invoke-static {v0}, Lcom/google/android/apps/chrome/document/home/MostVisitedPage;->access$200(Lcom/google/android/apps/chrome/document/home/MostVisitedPage;)Lorg/chromium/chrome/browser/profiles/MostVisitedSites;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lorg/chromium/chrome/browser/profiles/MostVisitedSites;->setMostVisitedURLsObserver(Lorg/chromium/chrome/browser/profiles/MostVisitedSites$MostVisitedURLsObserver;I)V

    .line 132
    return-void
.end method

.method public shouldShowOptOutPromo()Z
    .locals 4

    .prologue
    .line 184
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/home/MostVisitedPage$1;->this$0:Lcom/google/android/apps/chrome/document/home/MostVisitedPage;

    # getter for: Lcom/google/android/apps/chrome/document/home/MostVisitedPage;->mActivity:Lcom/google/android/apps/chrome/document/DocumentActivity;
    invoke-static {v0}, Lcom/google/android/apps/chrome/document/home/MostVisitedPage;->access$100(Lcom/google/android/apps/chrome/document/home/MostVisitedPage;)Lcom/google/android/apps/chrome/document/DocumentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;

    move-result-object v0

    .line 186
    invoke-virtual {v0}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->isOptOutPromoDismissed()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->getOptOutShownCount()J

    move-result-wide v0

    const-wide/16 v2, 0xa

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
