.class public Lcom/google/android/apps/chrome/compositor/resources/BitmapResources;
.super Ljava/lang/Object;
.source "BitmapResources.java"


# static fields
.field private static final BITMAP_CONFIG:Landroid/graphics/Bitmap$Config;


# instance fields
.field private final mBitmapLoaders:Landroid/util/SparseArray;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 26
    sget-object v0, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    sput-object v0, Lcom/google/android/apps/chrome/compositor/resources/BitmapResources;->BITMAP_CONFIG:Landroid/graphics/Bitmap$Config;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 79
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 80
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/compositor/resources/BitmapResources;->mBitmapLoaders:Landroid/util/SparseArray;

    .line 81
    return-void
.end method

.method public static load(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;
    .locals 9

    .prologue
    const/16 v7, 0x31

    const/4 v2, 0x0

    .line 36
    .line 38
    :try_start_0
    new-instance v0, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v0}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 39
    sget-object v1, Lcom/google/android/apps/chrome/compositor/resources/BitmapResources;->BITMAP_CONFIG:Landroid/graphics/Bitmap$Config;

    iput-object v1, v0, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    .line 40
    invoke-static {p0, p1, v0}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;ILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 43
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getConfig()Landroid/graphics/Bitmap$Config;

    move-result-object v1

    sget-object v3, Lcom/google/android/apps/chrome/compositor/resources/BitmapResources;->BITMAP_CONFIG:Landroid/graphics/Bitmap$Config;

    if-ne v1, v3, :cond_1

    .line 74
    :cond_0
    :goto_0
    return-object v0

    .line 48
    :cond_1
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    sget-object v4, Lcom/google/android/apps/chrome/compositor/resources/BitmapResources;->BITMAP_CONFIG:Landroid/graphics/Bitmap$Config;

    invoke-static {v1, v3, v4}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/view/InflateException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    move-result-object v1

    .line 50
    :try_start_1
    new-instance v3, Landroid/graphics/Canvas;

    invoke-direct {v3, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 51
    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual {v3, v0, v4, v5, v6}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 52
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V
    :try_end_1
    .catch Ljava/lang/OutOfMemoryError; {:try_start_1 .. :try_end_1} :catch_5
    .catch Landroid/view/InflateException; {:try_start_1 .. :try_end_1} :catch_4
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_3

    move-object v0, v1

    .line 73
    goto :goto_0

    .line 54
    :catch_0
    move-exception v0

    move-object v1, v2

    .line 55
    :goto_1
    const-string/jumbo v0, "BitmapResources"

    const-string/jumbo v3, "OutOfMemoryError while trying to load resource."

    invoke-static {v0, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 56
    if-eqz v1, :cond_2

    .line 57
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V

    .line 59
    :cond_2
    invoke-static {v7}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->broadcastImmediateNotification(I)V

    move-object v0, v2

    .line 73
    goto :goto_0

    .line 61
    :catch_1
    move-exception v0

    move-object v1, v2

    .line 62
    :goto_2
    const-string/jumbo v0, "BitmapResources"

    const-string/jumbo v3, "InflateException while trying to load resource."

    invoke-static {v0, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 63
    if-eqz v1, :cond_3

    .line 64
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V

    .line 66
    :cond_3
    invoke-static {v7}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->broadcastImmediateNotification(I)V

    move-object v0, v2

    .line 73
    goto :goto_0

    .line 68
    :catch_2
    move-exception v0

    move-object v1, v0

    move-object v0, v2

    .line 70
    :goto_3
    const-string/jumbo v2, "BitmapResources"

    const-string/jumbo v3, "Unable to load bitmap"

    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 71
    if-eqz v0, :cond_0

    .line 72
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    goto :goto_0

    .line 68
    :catch_3
    move-exception v0

    move-object v8, v0

    move-object v0, v1

    move-object v1, v8

    goto :goto_3

    .line 61
    :catch_4
    move-exception v0

    goto :goto_2

    .line 54
    :catch_5
    move-exception v0

    goto :goto_1
.end method
