.class Lcom/google/android/apps/chrome/tab/ChromeTab$1;
.super Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;
.source "ChromeTab.java"


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/tab/ChromeTab;)V
    .locals 0

    .prologue
    .line 409
    iput-object p1, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$1;->this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;

    invoke-direct {p0}, Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 2

    .prologue
    .line 412
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$1;->this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;

    # invokes: Lcom/google/android/apps/chrome/tab/ChromeTab;->isContextualSearchActive()Z
    invoke-static {v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->access$100(Lcom/google/android/apps/chrome/tab/ChromeTab;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 413
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$1;->this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;

    iget-object v1, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$1;->this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getContentViewCore()Lorg/chromium/content/browser/ContentViewCore;

    move-result-object v1

    # invokes: Lcom/google/android/apps/chrome/tab/ChromeTab;->addContextualSearchListener(Lorg/chromium/content/browser/ContentViewCore;)V
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/tab/ChromeTab;->access$200(Lcom/google/android/apps/chrome/tab/ChromeTab;Lorg/chromium/content/browser/ContentViewCore;)V

    .line 417
    :goto_0
    return-void

    .line 415
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$1;->this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;

    iget-object v1, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$1;->this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getContentViewCore()Lorg/chromium/content/browser/ContentViewCore;

    move-result-object v1

    # invokes: Lcom/google/android/apps/chrome/tab/ChromeTab;->removeContextualSearchListener(Lorg/chromium/content/browser/ContentViewCore;)V
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/tab/ChromeTab;->access$300(Lcom/google/android/apps/chrome/tab/ChromeTab;Lorg/chromium/content/browser/ContentViewCore;)V

    goto :goto_0
.end method
