.class final Lcom/google/android/apps/chrome/omnibox/LocationBar$UrlBarTextWatcher;
.super Ljava/lang/Object;
.source "LocationBar.java"

# interfaces
.implements Landroid/text/TextWatcher;


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/omnibox/LocationBar;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 272
    const-class v0, Lcom/google/android/apps/chrome/omnibox/LocationBar;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/omnibox/LocationBar$UrlBarTextWatcher;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private constructor <init>(Lcom/google/android/apps/chrome/omnibox/LocationBar;)V
    .locals 0

    .prologue
    .line 272
    iput-object p1, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar$UrlBarTextWatcher;->this$0:Lcom/google/android/apps/chrome/omnibox/LocationBar;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/chrome/omnibox/LocationBar;Lcom/google/android/apps/chrome/omnibox/LocationBar$1;)V
    .locals 0

    .prologue
    .line 272
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/omnibox/LocationBar$UrlBarTextWatcher;-><init>(Lcom/google/android/apps/chrome/omnibox/LocationBar;)V

    return-void
.end method


# virtual methods
.method public final afterTextChanged(Landroid/text/Editable;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v4, 0x0

    .line 275
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar$UrlBarTextWatcher;->this$0:Lcom/google/android/apps/chrome/omnibox/LocationBar;

    # getter for: Lcom/google/android/apps/chrome/omnibox/LocationBar;->mDeleteCorpusChipText:Z
    invoke-static {v0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->access$000(Lcom/google/android/apps/chrome/omnibox/LocationBar;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 276
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar$UrlBarTextWatcher;->this$0:Lcom/google/android/apps/chrome/omnibox/LocationBar;

    # setter for: Lcom/google/android/apps/chrome/omnibox/LocationBar;->mDeleteCorpusChipText:Z
    invoke-static {v0, v4}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->access$002(Lcom/google/android/apps/chrome/omnibox/LocationBar;Z)Z

    .line 277
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar$UrlBarTextWatcher;->this$0:Lcom/google/android/apps/chrome/omnibox/LocationBar;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->updateCorpusChipTextVisibility(Z)V

    .line 330
    :cond_0
    :goto_0
    return-void

    .line 280
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar$UrlBarTextWatcher;->this$0:Lcom/google/android/apps/chrome/omnibox/LocationBar;

    # invokes: Lcom/google/android/apps/chrome/omnibox/LocationBar;->updateDeleteButtonVisibility()V
    invoke-static {v0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->access$100(Lcom/google/android/apps/chrome/omnibox/LocationBar;)V

    .line 281
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar$UrlBarTextWatcher;->this$0:Lcom/google/android/apps/chrome/omnibox/LocationBar;

    # invokes: Lcom/google/android/apps/chrome/omnibox/LocationBar;->updateNavigationButton()V
    invoke-static {v0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->access$200(Lcom/google/android/apps/chrome/omnibox/LocationBar;)V

    .line 283
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar$UrlBarTextWatcher;->this$0:Lcom/google/android/apps/chrome/omnibox/LocationBar;

    # getter for: Lcom/google/android/apps/chrome/omnibox/LocationBar;->mIgnoreURLBarModification:Z
    invoke-static {v0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->access$300(Lcom/google/android/apps/chrome/omnibox/LocationBar;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 285
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar$UrlBarTextWatcher;->this$0:Lcom/google/android/apps/chrome/omnibox/LocationBar;

    # getter for: Lcom/google/android/apps/chrome/omnibox/LocationBar;->mHasStartedNewOmniboxEditSession:Z
    invoke-static {v0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->access$400(Lcom/google/android/apps/chrome/omnibox/LocationBar;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar$UrlBarTextWatcher;->this$0:Lcom/google/android/apps/chrome/omnibox/LocationBar;

    # getter for: Lcom/google/android/apps/chrome/omnibox/LocationBar;->mNativeInitialized:Z
    invoke-static {v0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->access$500(Lcom/google/android/apps/chrome/omnibox/LocationBar;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 286
    invoke-static {}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->firstEditInOmnibox()V

    .line 287
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar$UrlBarTextWatcher;->this$0:Lcom/google/android/apps/chrome/omnibox/LocationBar;

    iget-object v0, v0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mAutocomplete:Lcom/google/android/apps/chrome/omnibox/ChromeAutocompleteController;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/omnibox/ChromeAutocompleteController;->resetSession()V

    .line 288
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar$UrlBarTextWatcher;->this$0:Lcom/google/android/apps/chrome/omnibox/LocationBar;

    # setter for: Lcom/google/android/apps/chrome/omnibox/LocationBar;->mHasStartedNewOmniboxEditSession:Z
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->access$402(Lcom/google/android/apps/chrome/omnibox/LocationBar;Z)Z

    .line 289
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar$UrlBarTextWatcher;->this$0:Lcom/google/android/apps/chrome/omnibox/LocationBar;

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    # setter for: Lcom/google/android/apps/chrome/omnibox/LocationBar;->mNewOmniboxEditSessionTimestamp:J
    invoke-static {v0, v2, v3}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->access$602(Lcom/google/android/apps/chrome/omnibox/LocationBar;J)J

    .line 292
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar$UrlBarTextWatcher;->this$0:Lcom/google/android/apps/chrome/omnibox/LocationBar;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->isInTouchMode()Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar$UrlBarTextWatcher;->this$0:Lcom/google/android/apps/chrome/omnibox/LocationBar;

    # getter for: Lcom/google/android/apps/chrome/omnibox/LocationBar;->mSuggestionList:Lcom/google/android/apps/chrome/omnibox/LocationBar$OmniboxSuggestionsList;
    invoke-static {v0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->access$700(Lcom/google/android/apps/chrome/omnibox/LocationBar;)Lcom/google/android/apps/chrome/omnibox/LocationBar$OmniboxSuggestionsList;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 293
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar$UrlBarTextWatcher;->this$0:Lcom/google/android/apps/chrome/omnibox/LocationBar;

    # getter for: Lcom/google/android/apps/chrome/omnibox/LocationBar;->mSuggestionList:Lcom/google/android/apps/chrome/omnibox/LocationBar$OmniboxSuggestionsList;
    invoke-static {v0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->access$700(Lcom/google/android/apps/chrome/omnibox/LocationBar;)Lcom/google/android/apps/chrome/omnibox/LocationBar$OmniboxSuggestionsList;

    move-result-object v0

    invoke-virtual {v0, v4}, Lcom/google/android/apps/chrome/omnibox/LocationBar$OmniboxSuggestionsList;->setSelection(I)V

    .line 296
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar$UrlBarTextWatcher;->this$0:Lcom/google/android/apps/chrome/omnibox/LocationBar;

    iget-object v0, v0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mUrlBar:Lcom/google/android/apps/chrome/omnibox/UrlBar;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/omnibox/UrlBar;->getTextWithoutAutocomplete()Ljava/lang/String;

    move-result-object v0

    .line 298
    iget-object v1, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar$UrlBarTextWatcher;->this$0:Lcom/google/android/apps/chrome/omnibox/LocationBar;

    # invokes: Lcom/google/android/apps/chrome/omnibox/LocationBar;->stopAutocomplete(Z)V
    invoke-static {v1, v4}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->access$800(Lcom/google/android/apps/chrome/omnibox/LocationBar;Z)V

    .line 299
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 300
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar$UrlBarTextWatcher;->this$0:Lcom/google/android/apps/chrome/omnibox/LocationBar;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->hideSuggestions()V

    .line 301
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar$UrlBarTextWatcher;->this$0:Lcom/google/android/apps/chrome/omnibox/LocationBar;

    # invokes: Lcom/google/android/apps/chrome/omnibox/LocationBar;->startZeroSuggest()V
    invoke-static {v0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->access$900(Lcom/google/android/apps/chrome/omnibox/LocationBar;)V

    .line 329
    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar$UrlBarTextWatcher;->this$0:Lcom/google/android/apps/chrome/omnibox/LocationBar;

    # getter for: Lcom/google/android/apps/chrome/omnibox/LocationBar;->mLastUrlEditWasDelete:Z
    invoke-static {v0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->access$1100(Lcom/google/android/apps/chrome/omnibox/LocationBar;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar$UrlBarTextWatcher;->this$0:Lcom/google/android/apps/chrome/omnibox/LocationBar;

    iget-object v0, v0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mUrlBar:Lcom/google/android/apps/chrome/omnibox/UrlBar;

    iget-object v1, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar$UrlBarTextWatcher;->this$0:Lcom/google/android/apps/chrome/omnibox/LocationBar;

    iget-object v1, v1, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mUrlBar:Lcom/google/android/apps/chrome/omnibox/UrlBar;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/omnibox/UrlBar;->getSelectionStart()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/omnibox/UrlBar;->setSelection(I)V

    goto/16 :goto_0

    .line 303
    :cond_4
    sget-boolean v1, Lcom/google/android/apps/chrome/omnibox/LocationBar$UrlBarTextWatcher;->$assertionsDisabled:Z

    if-nez v1, :cond_5

    iget-object v1, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar$UrlBarTextWatcher;->this$0:Lcom/google/android/apps/chrome/omnibox/LocationBar;

    # getter for: Lcom/google/android/apps/chrome/omnibox/LocationBar;->mRequestSuggestions:Ljava/lang/Runnable;
    invoke-static {v1}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->access$1000(Lcom/google/android/apps/chrome/omnibox/LocationBar;)Ljava/lang/Runnable;

    move-result-object v1

    if-eqz v1, :cond_5

    new-instance v0, Ljava/lang/AssertionError;

    const-string/jumbo v1, "Multiple omnibox requests in flight."

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 304
    :cond_5
    iget-object v1, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar$UrlBarTextWatcher;->this$0:Lcom/google/android/apps/chrome/omnibox/LocationBar;

    new-instance v2, Lcom/google/android/apps/chrome/omnibox/LocationBar$UrlBarTextWatcher$1;

    invoke-direct {v2, p0, p1, v0}, Lcom/google/android/apps/chrome/omnibox/LocationBar$UrlBarTextWatcher$1;-><init>(Lcom/google/android/apps/chrome/omnibox/LocationBar$UrlBarTextWatcher;Landroid/text/Editable;Ljava/lang/String;)V

    # setter for: Lcom/google/android/apps/chrome/omnibox/LocationBar;->mRequestSuggestions:Ljava/lang/Runnable;
    invoke-static {v1, v2}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->access$1002(Lcom/google/android/apps/chrome/omnibox/LocationBar;Ljava/lang/Runnable;)Ljava/lang/Runnable;

    .line 319
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar$UrlBarTextWatcher;->this$0:Lcom/google/android/apps/chrome/omnibox/LocationBar;

    # getter for: Lcom/google/android/apps/chrome/omnibox/LocationBar;->mNativeInitialized:Z
    invoke-static {v0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->access$500(Lcom/google/android/apps/chrome/omnibox/LocationBar;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 320
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar$UrlBarTextWatcher;->this$0:Lcom/google/android/apps/chrome/omnibox/LocationBar;

    iget-object v1, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar$UrlBarTextWatcher;->this$0:Lcom/google/android/apps/chrome/omnibox/LocationBar;

    # getter for: Lcom/google/android/apps/chrome/omnibox/LocationBar;->mRequestSuggestions:Ljava/lang/Runnable;
    invoke-static {v1}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->access$1000(Lcom/google/android/apps/chrome/omnibox/LocationBar;)Ljava/lang/Runnable;

    move-result-object v1

    const-wide/16 v2, 0x1e

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_1

    .line 322
    :cond_6
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar$UrlBarTextWatcher;->this$0:Lcom/google/android/apps/chrome/omnibox/LocationBar;

    # getter for: Lcom/google/android/apps/chrome/omnibox/LocationBar;->mDeferredNativeRunnables:Ljava/util/List;
    invoke-static {v0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->access$1300(Lcom/google/android/apps/chrome/omnibox/LocationBar;)Ljava/util/List;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar$UrlBarTextWatcher;->this$0:Lcom/google/android/apps/chrome/omnibox/LocationBar;

    # getter for: Lcom/google/android/apps/chrome/omnibox/LocationBar;->mRequestSuggestions:Ljava/lang/Runnable;
    invoke-static {v1}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->access$1000(Lcom/google/android/apps/chrome/omnibox/LocationBar;)Ljava/lang/Runnable;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method

.method public final beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 334
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar$UrlBarTextWatcher;->this$0:Lcom/google/android/apps/chrome/omnibox/LocationBar;

    # invokes: Lcom/google/android/apps/chrome/omnibox/LocationBar;->cancelPendingAutocompleteStart()V
    invoke-static {v0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->access$1400(Lcom/google/android/apps/chrome/omnibox/LocationBar;)V

    .line 335
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar$UrlBarTextWatcher;->this$0:Lcom/google/android/apps/chrome/omnibox/LocationBar;

    # getter for: Lcom/google/android/apps/chrome/omnibox/LocationBar;->mCorpusChipTextView:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->access$1500(Lcom/google/android/apps/chrome/omnibox/LocationBar;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/TextView;->isShown()Z

    move-result v0

    if-eqz v0, :cond_0

    if-nez p2, :cond_0

    if-ne p3, v1, :cond_0

    if-nez p4, :cond_0

    .line 336
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar$UrlBarTextWatcher;->this$0:Lcom/google/android/apps/chrome/omnibox/LocationBar;

    # setter for: Lcom/google/android/apps/chrome/omnibox/LocationBar;->mDeleteCorpusChipText:Z
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->access$002(Lcom/google/android/apps/chrome/omnibox/LocationBar;Z)Z

    .line 338
    :cond_0
    return-void
.end method

.method public final onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 2

    .prologue
    .line 346
    iget-object v1, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar$UrlBarTextWatcher;->this$0:Lcom/google/android/apps/chrome/omnibox/LocationBar;

    if-nez p4, :cond_0

    const/4 v0, 0x1

    :goto_0
    # setter for: Lcom/google/android/apps/chrome/omnibox/LocationBar;->mLastUrlEditWasDelete:Z
    invoke-static {v1, v0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->access$1102(Lcom/google/android/apps/chrome/omnibox/LocationBar;Z)Z

    .line 347
    return-void

    .line 346
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
