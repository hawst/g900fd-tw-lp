.class public Lcom/google/android/apps/chrome/ntp/MostVisitedItem;
.super Landroid/widget/LinearLayout;
.source "MostVisitedItem.java"

# interfaces
.implements Landroid/view/MenuItem$OnMenuItemClickListener;
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/View$OnCreateContextMenuListener;


# instance fields
.field private mFaviconSize:I

.field private mIndex:I

.field private mLastDrawnPressed:Z

.field private mManager:Lcom/google/android/apps/chrome/ntp/MostVisitedItem$MostVisitedItemManager;

.field private mThumbnailView:Lcom/google/android/apps/chrome/ntp/MostVisitedThumbnail;

.field private mTitle:Ljava/lang/String;

.field private mTitlePaddingStart:I

.field private mTitleView:Landroid/widget/TextView;

.field private mUrl:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 83
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 84
    invoke-virtual {p0, p0}, Lcom/google/android/apps/chrome/ntp/MostVisitedItem;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 85
    invoke-virtual {p0, p0}, Lcom/google/android/apps/chrome/ntp/MostVisitedItem;->setOnCreateContextMenuListener(Landroid/view/View$OnCreateContextMenuListener;)V

    .line 86
    return-void
.end method


# virtual methods
.method protected dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 178
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->dispatchDraw(Landroid/graphics/Canvas;)V

    .line 181
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ntp/MostVisitedItem;->isPressed()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 182
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    .line 183
    const v1, 0x550099cc

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 184
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 185
    new-instance v1, Landroid/graphics/RectF;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ntp/MostVisitedItem;->getWidth()I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ntp/MostVisitedItem;->getHeight()I

    move-result v3

    int-to-float v3, v3

    invoke-direct {v1, v4, v4, v2, v3}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 186
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ntp/MostVisitedItem;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/google/android/apps/chrome/R$dimen;->most_visited_bg_corner_radius:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v2

    .line 188
    int-to-float v3, v2

    int-to-float v2, v2

    invoke-virtual {p1, v1, v3, v2, v0}, Landroid/graphics/Canvas;->drawRoundRect(Landroid/graphics/RectF;FFLandroid/graphics/Paint;)V

    .line 190
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ntp/MostVisitedItem;->isPressed()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/ntp/MostVisitedItem;->mLastDrawnPressed:Z

    .line 191
    return-void
.end method

.method public getIndex()I
    .locals 1

    .prologue
    .line 137
    iget v0, p0, Lcom/google/android/apps/chrome/ntp/MostVisitedItem;->mIndex:I

    return v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 130
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/MostVisitedItem;->mTitle:Ljava/lang/String;

    return-object v0
.end method

.method public getUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 123
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/MostVisitedItem;->mUrl:Ljava/lang/String;

    return-object v0
.end method

.method public init(Lcom/google/android/apps/chrome/ntp/MostVisitedItem$MostVisitedItemManager;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 98
    sget v0, Lcom/google/android/apps/chrome/R$id;->most_visited_title:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/ntp/MostVisitedItem;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/chrome/ntp/MostVisitedItem;->mTitleView:Landroid/widget/TextView;

    .line 99
    sget v0, Lcom/google/android/apps/chrome/R$id;->most_visited_thumbnail:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/ntp/MostVisitedItem;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/ntp/MostVisitedThumbnail;

    iput-object v0, p0, Lcom/google/android/apps/chrome/ntp/MostVisitedItem;->mThumbnailView:Lcom/google/android/apps/chrome/ntp/MostVisitedThumbnail;

    .line 101
    iput-object p1, p0, Lcom/google/android/apps/chrome/ntp/MostVisitedItem;->mManager:Lcom/google/android/apps/chrome/ntp/MostVisitedItem$MostVisitedItemManager;

    .line 102
    invoke-static {p2, p3}, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->getTitleForDisplay(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/ntp/MostVisitedItem;->mTitle:Ljava/lang/String;

    .line 103
    iput-object p3, p0, Lcom/google/android/apps/chrome/ntp/MostVisitedItem;->mUrl:Ljava/lang/String;

    .line 104
    iput p4, p0, Lcom/google/android/apps/chrome/ntp/MostVisitedItem;->mIndex:I

    .line 106
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/MostVisitedItem;->mTitleView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/apps/chrome/ntp/MostVisitedItem;->mTitle:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 112
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/MostVisitedItem;->mTitleView:Landroid/widget/TextView;

    invoke-static {v0}, Lorg/chromium/base/ApiCompatibilityUtils;->getPaddingStart(Landroid/view/View;)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/ntp/MostVisitedItem;->mTitlePaddingStart:I

    .line 113
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ntp/MostVisitedItem;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/chrome/R$dimen;->most_visited_favicon_size:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/ntp/MostVisitedItem;->mFaviconSize:I

    .line 114
    iget v0, p0, Lcom/google/android/apps/chrome/ntp/MostVisitedItem;->mFaviconSize:I

    iget-object v1, p0, Lcom/google/android/apps/chrome/ntp/MostVisitedItem;->mTitleView:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getCompoundDrawablePadding()I

    move-result v1

    add-int/2addr v0, v1

    .line 115
    iget-object v1, p0, Lcom/google/android/apps/chrome/ntp/MostVisitedItem;->mTitleView:Landroid/widget/TextView;

    iget v2, p0, Lcom/google/android/apps/chrome/ntp/MostVisitedItem;->mTitlePaddingStart:I

    add-int/2addr v0, v2

    invoke-static {v1, v0, v3, v3, v3}, Lorg/chromium/base/ApiCompatibilityUtils;->setPaddingRelative(Landroid/view/View;IIII)V

    .line 117
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 205
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/MostVisitedItem;->mManager:Lcom/google/android/apps/chrome/ntp/MostVisitedItem$MostVisitedItemManager;

    invoke-interface {v0, p0}, Lcom/google/android/apps/chrome/ntp/MostVisitedItem$MostVisitedItemManager;->open(Lcom/google/android/apps/chrome/ntp/MostVisitedItem;)V

    .line 206
    return-void
.end method

.method public onCreateContextMenu(Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;)V
    .locals 1

    .prologue
    .line 195
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/MostVisitedItem;->mManager:Lcom/google/android/apps/chrome/ntp/MostVisitedItem$MostVisitedItemManager;

    invoke-interface {v0, p1, p0}, Lcom/google/android/apps/chrome/ntp/MostVisitedItem$MostVisitedItemManager;->onCreateContextMenu(Landroid/view/ContextMenu;Landroid/view/MenuItem$OnMenuItemClickListener;)V

    .line 196
    return-void
.end method

.method public onMenuItemClick(Landroid/view/MenuItem;)Z
    .locals 2

    .prologue
    .line 200
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/MostVisitedItem;->mManager:Lcom/google/android/apps/chrome/ntp/MostVisitedItem$MostVisitedItemManager;

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    invoke-interface {v0, v1, p0}, Lcom/google/android/apps/chrome/ntp/MostVisitedItem$MostVisitedItemManager;->onMenuItemClick(ILcom/google/android/apps/chrome/ntp/MostVisitedItem;)Z

    move-result v0

    return v0
.end method

.method public setFavicon(Landroid/graphics/Bitmap;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 158
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ntp/MostVisitedItem;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 160
    if-eqz p1, :cond_0

    .line 161
    new-instance v0, Landroid/graphics/drawable/BitmapDrawable;

    invoke-direct {v0, v1, p1}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    .line 165
    :goto_0
    iget v1, p0, Lcom/google/android/apps/chrome/ntp/MostVisitedItem;->mFaviconSize:I

    iget v2, p0, Lcom/google/android/apps/chrome/ntp/MostVisitedItem;->mFaviconSize:I

    invoke-virtual {v0, v3, v3, v1, v2}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 166
    iget-object v1, p0, Lcom/google/android/apps/chrome/ntp/MostVisitedItem;->mTitleView:Landroid/widget/TextView;

    invoke-static {v1, v0, v4, v4, v4}, Lorg/chromium/base/ApiCompatibilityUtils;->setCompoundDrawablesRelative(Landroid/widget/TextView;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 167
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/MostVisitedItem;->mTitleView:Landroid/widget/TextView;

    iget v1, p0, Lcom/google/android/apps/chrome/ntp/MostVisitedItem;->mTitlePaddingStart:I

    invoke-static {v0, v1, v3, v3, v3}, Lorg/chromium/base/ApiCompatibilityUtils;->setPaddingRelative(Landroid/view/View;IIII)V

    .line 168
    return-void

    .line 163
    :cond_0
    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    const v1, -0x191918

    invoke-direct {v0, v1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    goto :goto_0
.end method

.method public setIndex(I)V
    .locals 0

    .prologue
    .line 144
    iput p1, p0, Lcom/google/android/apps/chrome/ntp/MostVisitedItem;->mIndex:I

    .line 145
    return-void
.end method

.method public setPressed(Z)V
    .locals 2

    .prologue
    .line 172
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->setPressed(Z)V

    .line 173
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ntp/MostVisitedItem;->isPressed()Z

    move-result v0

    iget-boolean v1, p0, Lcom/google/android/apps/chrome/ntp/MostVisitedItem;->mLastDrawnPressed:Z

    if-eq v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ntp/MostVisitedItem;->invalidate()V

    .line 174
    :cond_0
    return-void
.end method

.method public setThumbnail(Landroid/graphics/Bitmap;)V
    .locals 1

    .prologue
    .line 151
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/MostVisitedItem;->mThumbnailView:Lcom/google/android/apps/chrome/ntp/MostVisitedThumbnail;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/ntp/MostVisitedThumbnail;->setThumbnail(Landroid/graphics/Bitmap;)V

    .line 152
    return-void
.end method
