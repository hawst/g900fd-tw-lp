.class public Lcom/google/android/apps/chrome/ntp/MostVisitedLayout;
.super Landroid/widget/FrameLayout;
.source "MostVisitedLayout.java"


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private mDefaultChildWidth:I

.field private mEmptyTileDrawable:Landroid/graphics/drawable/Drawable;

.field private mEmptyTileTop:I

.field private mFirstEmptyTileLeft:I

.field private mHorizontalSpacing:I

.field private mMaxColumns:I

.field private mMinColumns:I

.field private mNumEmptyTiles:I

.field private mVerticalSpacing:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 21
    const-class v0, Lcom/google/android/apps/chrome/ntp/MostVisitedLayout;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/ntp/MostVisitedLayout;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 50
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 51
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/ntp/MostVisitedLayout;->setWillNotDraw(Z)V

    .line 52
    return-void
.end method


# virtual methods
.method public init(IIIII)V
    .locals 0

    .prologue
    .line 64
    iput p1, p0, Lcom/google/android/apps/chrome/ntp/MostVisitedLayout;->mHorizontalSpacing:I

    .line 65
    iput p2, p0, Lcom/google/android/apps/chrome/ntp/MostVisitedLayout;->mVerticalSpacing:I

    .line 66
    iput p3, p0, Lcom/google/android/apps/chrome/ntp/MostVisitedLayout;->mMinColumns:I

    .line 67
    iput p4, p0, Lcom/google/android/apps/chrome/ntp/MostVisitedLayout;->mMaxColumns:I

    .line 68
    iput p5, p0, Lcom/google/android/apps/chrome/ntp/MostVisitedLayout;->mDefaultChildWidth:I

    .line 69
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 8

    .prologue
    const/4 v0, 0x0

    .line 155
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onDraw(Landroid/graphics/Canvas;)V

    .line 156
    iget v1, p0, Lcom/google/android/apps/chrome/ntp/MostVisitedLayout;->mNumEmptyTiles:I

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ntp/MostVisitedLayout;->getChildCount()I

    move-result v1

    if-nez v1, :cond_1

    .line 174
    :cond_0
    return-void

    .line 159
    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/chrome/ntp/MostVisitedLayout;->mEmptyTileDrawable:Landroid/graphics/drawable/Drawable;

    if-nez v1, :cond_2

    .line 160
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ntp/MostVisitedLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/apps/chrome/R$drawable;->most_visited_item_empty:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/chrome/ntp/MostVisitedLayout;->mEmptyTileDrawable:Landroid/graphics/drawable/Drawable;

    .line 162
    :cond_2
    iget v1, p0, Lcom/google/android/apps/chrome/ntp/MostVisitedLayout;->mFirstEmptyTileLeft:I

    .line 163
    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/ntp/MostVisitedLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getMeasuredWidth()I

    move-result v2

    .line 164
    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/ntp/MostVisitedLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/View;->getMeasuredHeight()I

    move-result v3

    .line 165
    :goto_0
    iget v4, p0, Lcom/google/android/apps/chrome/ntp/MostVisitedLayout;->mNumEmptyTiles:I

    if-ge v0, v4, :cond_0

    .line 166
    iget-object v4, p0, Lcom/google/android/apps/chrome/ntp/MostVisitedLayout;->mEmptyTileDrawable:Landroid/graphics/drawable/Drawable;

    iget v5, p0, Lcom/google/android/apps/chrome/ntp/MostVisitedLayout;->mEmptyTileTop:I

    add-int v6, v1, v2

    iget v7, p0, Lcom/google/android/apps/chrome/ntp/MostVisitedLayout;->mEmptyTileTop:I

    add-int/2addr v7, v3

    invoke-virtual {v4, v1, v5, v6, v7}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 171
    iget-object v4, p0, Lcom/google/android/apps/chrome/ntp/MostVisitedLayout;->mEmptyTileDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v4, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 172
    iget v4, p0, Lcom/google/android/apps/chrome/ntp/MostVisitedLayout;->mHorizontalSpacing:I

    add-int/2addr v4, v2

    add-int/2addr v1, v4

    .line 165
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method protected onMeasure(II)V
    .locals 16

    .prologue
    .line 74
    sget-boolean v1, Lcom/google/android/apps/chrome/ntp/MostVisitedLayout;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    move-object/from16 v0, p0

    iget v1, v0, Lcom/google/android/apps/chrome/ntp/MostVisitedLayout;->mMaxColumns:I

    if-nez v1, :cond_0

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 77
    :cond_0
    invoke-static/range {p1 .. p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v5

    .line 78
    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/apps/chrome/ntp/MostVisitedLayout;->mDefaultChildWidth:I

    .line 79
    move-object/from16 v0, p0

    iget v1, v0, Lcom/google/android/apps/chrome/ntp/MostVisitedLayout;->mHorizontalSpacing:I

    add-int/2addr v1, v2

    .line 80
    move-object/from16 v0, p0

    iget v3, v0, Lcom/google/android/apps/chrome/ntp/MostVisitedLayout;->mHorizontalSpacing:I

    add-int/2addr v3, v5

    div-int v4, v3, v1

    .line 83
    move-object/from16 v0, p0

    iget v3, v0, Lcom/google/android/apps/chrome/ntp/MostVisitedLayout;->mMaxColumns:I

    if-lt v4, v3, :cond_1

    .line 84
    move-object/from16 v0, p0

    iget v3, v0, Lcom/google/android/apps/chrome/ntp/MostVisitedLayout;->mMaxColumns:I

    move v10, v3

    .line 96
    :goto_0
    if-le v10, v4, :cond_b

    .line 98
    move-object/from16 v0, p0

    iget v1, v0, Lcom/google/android/apps/chrome/ntp/MostVisitedLayout;->mHorizontalSpacing:I

    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/apps/chrome/ntp/MostVisitedLayout;->mHorizontalSpacing:I

    add-int/2addr v2, v5

    div-int/2addr v2, v10

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 100
    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/apps/chrome/ntp/MostVisitedLayout;->mHorizontalSpacing:I

    sub-int v2, v1, v2

    move v15, v1

    move v1, v2

    move v2, v15

    .line 103
    :goto_1
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/chrome/ntp/MostVisitedLayout;->getChildCount()I

    move-result v11

    .line 104
    const/4 v3, 0x0

    .line 105
    if-lez v11, :cond_4

    .line 107
    const/high16 v3, 0x40000000    # 2.0f

    invoke-static {v1, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    .line 108
    const/4 v1, 0x0

    const/4 v4, 0x0

    invoke-static {v1, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    .line 109
    const/4 v1, 0x0

    :goto_2
    if-ge v1, v11, :cond_3

    .line 110
    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/ntp/MostVisitedLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v5, v3, v4}, Landroid/view/View;->measure(II)V

    .line 109
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 85
    :cond_1
    move-object/from16 v0, p0

    iget v3, v0, Lcom/google/android/apps/chrome/ntp/MostVisitedLayout;->mMinColumns:I

    if-ge v4, v3, :cond_2

    .line 86
    move-object/from16 v0, p0

    iget v3, v0, Lcom/google/android/apps/chrome/ntp/MostVisitedLayout;->mMinColumns:I

    move v10, v3

    goto :goto_0

    .line 90
    :cond_2
    mul-int v3, v4, v1

    move-object/from16 v0, p0

    iget v6, v0, Lcom/google/android/apps/chrome/ntp/MostVisitedLayout;->mHorizontalSpacing:I

    sub-int/2addr v3, v6

    .line 91
    int-to-float v3, v3

    const/high16 v6, 0x3f400000    # 0.75f

    int-to-float v7, v5

    mul-float/2addr v6, v7

    cmpg-float v3, v3, v6

    if-gez v3, :cond_c

    .line 92
    add-int/lit8 v3, v4, 0x1

    move v10, v3

    goto :goto_0

    .line 112
    :cond_3
    const/4 v1, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/ntp/MostVisitedLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    move v3, v1

    .line 116
    :cond_4
    const/4 v5, 0x0

    .line 117
    const/4 v6, 0x0

    .line 118
    const/4 v4, 0x0

    .line 119
    invoke-static/range {p0 .. p0}, Lorg/chromium/base/ApiCompatibilityUtils;->isLayoutRtl(Landroid/view/View;)Z

    move-result v12

    .line 120
    const/4 v1, 0x0

    move v8, v1

    move v9, v4

    :goto_3
    if-ge v8, v11, :cond_8

    .line 121
    move-object/from16 v0, p0

    invoke-virtual {v0, v8}, Lcom/google/android/apps/chrome/ntp/MostVisitedLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v13

    .line 122
    invoke-virtual {v13}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 123
    if-eqz v12, :cond_5

    const/4 v4, 0x0

    move v7, v4

    :goto_4
    if-eqz v12, :cond_6

    move v4, v5

    :goto_5
    const/4 v14, 0x0

    invoke-virtual {v1, v7, v6, v4, v14}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    .line 125
    invoke-virtual {v13, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 126
    add-int/lit8 v1, v9, 0x1

    .line 127
    if-ne v1, v10, :cond_7

    .line 128
    const/4 v1, 0x0

    .line 129
    const/4 v5, 0x0

    .line 130
    move-object/from16 v0, p0

    iget v4, v0, Lcom/google/android/apps/chrome/ntp/MostVisitedLayout;->mVerticalSpacing:I

    add-int/2addr v4, v3

    add-int/2addr v4, v6

    .line 120
    :goto_6
    add-int/lit8 v6, v8, 0x1

    move v8, v6

    move v9, v1

    move v6, v4

    goto :goto_3

    :cond_5
    move v7, v5

    .line 123
    goto :goto_4

    :cond_6
    const/4 v4, 0x0

    goto :goto_5

    .line 132
    :cond_7
    add-int v4, v5, v2

    move v5, v4

    move v4, v6

    goto :goto_6

    .line 137
    :cond_8
    if-eqz v9, :cond_a

    .line 138
    sub-int v1, v10, v9

    move-object/from16 v0, p0

    iput v1, v0, Lcom/google/android/apps/chrome/ntp/MostVisitedLayout;->mNumEmptyTiles:I

    .line 139
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/chrome/ntp/MostVisitedLayout;->getPaddingTop()I

    move-result v1

    add-int/2addr v1, v6

    move-object/from16 v0, p0

    iput v1, v0, Lcom/google/android/apps/chrome/ntp/MostVisitedLayout;->mEmptyTileTop:I

    .line 140
    if-eqz v12, :cond_9

    const/4 v5, 0x0

    :cond_9
    move-object/from16 v0, p0

    iput v5, v0, Lcom/google/android/apps/chrome/ntp/MostVisitedLayout;->mFirstEmptyTileLeft:I

    .line 145
    :goto_7
    move-object/from16 v0, p0

    iget v1, v0, Lcom/google/android/apps/chrome/ntp/MostVisitedLayout;->mNumEmptyTiles:I

    add-int/2addr v1, v11

    div-int/2addr v1, v10

    .line 146
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/chrome/ntp/MostVisitedLayout;->getPaddingTop()I

    move-result v4

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/chrome/ntp/MostVisitedLayout;->getPaddingBottom()I

    move-result v5

    add-int/2addr v4, v5

    mul-int/2addr v3, v1

    add-int/2addr v3, v4

    add-int/lit8 v1, v1, -0x1

    move-object/from16 v0, p0

    iget v4, v0, Lcom/google/android/apps/chrome/ntp/MostVisitedLayout;->mVerticalSpacing:I

    mul-int/2addr v1, v4

    add-int/2addr v1, v3

    .line 149
    mul-int/2addr v2, v10

    move-object/from16 v0, p0

    iget v3, v0, Lcom/google/android/apps/chrome/ntp/MostVisitedLayout;->mHorizontalSpacing:I

    sub-int/2addr v2, v3

    .line 150
    move/from16 v0, p2

    invoke-static {v1, v0}, Lcom/google/android/apps/chrome/ntp/MostVisitedLayout;->resolveSize(II)I

    move-result v1

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v1}, Lcom/google/android/apps/chrome/ntp/MostVisitedLayout;->setMeasuredDimension(II)V

    .line 151
    return-void

    .line 142
    :cond_a
    const/4 v1, 0x0

    move-object/from16 v0, p0

    iput v1, v0, Lcom/google/android/apps/chrome/ntp/MostVisitedLayout;->mNumEmptyTiles:I

    goto :goto_7

    :cond_b
    move v15, v1

    move v1, v2

    move v2, v15

    goto/16 :goto_1

    :cond_c
    move v10, v4

    goto/16 :goto_0
.end method
