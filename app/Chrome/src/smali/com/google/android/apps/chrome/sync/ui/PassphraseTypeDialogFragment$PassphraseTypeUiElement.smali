.class Lcom/google/android/apps/chrome/sync/ui/PassphraseTypeDialogFragment$PassphraseTypeUiElement;
.super Ljava/lang/Object;
.source "PassphraseTypeDialogFragment.java"


# instance fields
.field private final mDisplayName:Ljava/lang/String;

.field private final mPassphraseType:Lorg/chromium/sync/internal_api/pub/SyncDecryptionPassphraseType;

.field private mPosition:I


# direct methods
.method private constructor <init>(Lorg/chromium/sync/internal_api/pub/SyncDecryptionPassphraseType;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    iput-object p1, p0, Lcom/google/android/apps/chrome/sync/ui/PassphraseTypeDialogFragment$PassphraseTypeUiElement;->mPassphraseType:Lorg/chromium/sync/internal_api/pub/SyncDecryptionPassphraseType;

    .line 52
    iput-object p2, p0, Lcom/google/android/apps/chrome/sync/ui/PassphraseTypeDialogFragment$PassphraseTypeUiElement;->mDisplayName:Ljava/lang/String;

    .line 54
    sget-object v0, Lorg/chromium/sync/internal_api/pub/SyncDecryptionPassphraseType;->INVALID:Lorg/chromium/sync/internal_api/pub/SyncDecryptionPassphraseType;

    invoke-virtual {v0}, Lorg/chromium/sync/internal_api/pub/SyncDecryptionPassphraseType;->internalValue()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/sync/ui/PassphraseTypeDialogFragment$PassphraseTypeUiElement;->mPosition:I

    .line 55
    return-void
.end method

.method synthetic constructor <init>(Lorg/chromium/sync/internal_api/pub/SyncDecryptionPassphraseType;Ljava/lang/String;Lcom/google/android/apps/chrome/sync/ui/PassphraseTypeDialogFragment$1;)V
    .locals 0

    .prologue
    .line 44
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/chrome/sync/ui/PassphraseTypeDialogFragment$PassphraseTypeUiElement;-><init>(Lorg/chromium/sync/internal_api/pub/SyncDecryptionPassphraseType;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/chrome/sync/ui/PassphraseTypeDialogFragment$PassphraseTypeUiElement;)I
    .locals 1

    .prologue
    .line 44
    iget v0, p0, Lcom/google/android/apps/chrome/sync/ui/PassphraseTypeDialogFragment$PassphraseTypeUiElement;->mPosition:I

    return v0
.end method

.method static synthetic access$002(Lcom/google/android/apps/chrome/sync/ui/PassphraseTypeDialogFragment$PassphraseTypeUiElement;I)I
    .locals 0

    .prologue
    .line 44
    iput p1, p0, Lcom/google/android/apps/chrome/sync/ui/PassphraseTypeDialogFragment$PassphraseTypeUiElement;->mPosition:I

    return p1
.end method

.method static synthetic access$100(Lcom/google/android/apps/chrome/sync/ui/PassphraseTypeDialogFragment$PassphraseTypeUiElement;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/ui/PassphraseTypeDialogFragment$PassphraseTypeUiElement;->mDisplayName:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/apps/chrome/sync/ui/PassphraseTypeDialogFragment$PassphraseTypeUiElement;)Lorg/chromium/sync/internal_api/pub/SyncDecryptionPassphraseType;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/ui/PassphraseTypeDialogFragment$PassphraseTypeUiElement;->mPassphraseType:Lorg/chromium/sync/internal_api/pub/SyncDecryptionPassphraseType;

    return-object v0
.end method
