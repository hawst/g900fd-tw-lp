.class public Lcom/google/android/apps/chrome/knoxsettings/KnoxSettingsProvider$Observer;
.super Ljava/lang/Object;
.source "KnoxSettingsProvider.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 69
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAuditLogEnabledChanged()V
    .locals 0

    .prologue
    .line 93
    return-void
.end method

.method public onAutofillEnabledChanged()V
    .locals 0

    .prologue
    .line 75
    return-void
.end method

.method public onCertificateRemoved()V
    .locals 0

    .prologue
    .line 95
    return-void
.end method

.method public onClientCertificateAliasChanged()V
    .locals 0

    .prologue
    .line 85
    return-void
.end method

.method public onCookiesEnabledChanged()V
    .locals 0

    .prologue
    .line 77
    return-void
.end method

.method public onDeviceAdminRemoved()V
    .locals 0

    .prologue
    .line 71
    return-void
.end method

.method public onHttpProxyHostPortChanged()V
    .locals 0

    .prologue
    .line 73
    return-void
.end method

.method public onJavascriptEnabledChanged()V
    .locals 0

    .prologue
    .line 79
    return-void
.end method

.method public onPopupsEnabledChanged()V
    .locals 0

    .prologue
    .line 81
    return-void
.end method

.method public onSmartCardAuthenticationEnabledChanged()V
    .locals 0

    .prologue
    .line 83
    return-void
.end method

.method public onUrlBlacklistChanged()V
    .locals 0

    .prologue
    .line 87
    return-void
.end method

.method public onUrlFilterEnabledChanged()V
    .locals 0

    .prologue
    .line 89
    return-void
.end method

.method public onUrlFilterReportEnabledChanged()V
    .locals 0

    .prologue
    .line 91
    return-void
.end method
