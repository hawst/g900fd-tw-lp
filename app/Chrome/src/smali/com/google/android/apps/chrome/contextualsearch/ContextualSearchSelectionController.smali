.class public Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchSelectionController;
.super Ljava/lang/Object;
.source "ContextualSearchSelectionController.java"


# instance fields
.field private mActivity:Lcom/google/android/apps/chrome/ChromeActivity;

.field private mHandler:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchSelectionHandler;

.field private mIsSelectionBeingModified:Z

.field private mLastSelectedText:Ljava/lang/String;

.field private mLastSelectionType:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchSelectionController$SelectionType;

.field private mWasTapGestureDetected:Z

.field private mX:F

.field private mY:F


# direct methods
.method public constructor <init>(Lcom/google/android/apps/chrome/ChromeActivity;Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchSelectionHandler;)V
    .locals 0

    .prologue
    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 58
    iput-object p1, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchSelectionController;->mActivity:Lcom/google/android/apps/chrome/ChromeActivity;

    .line 59
    iput-object p2, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchSelectionController;->mHandler:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchSelectionHandler;

    .line 60
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchSelectionController;)Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchSelectionHandler;
    .locals 1

    .prologue
    .line 16
    iget-object v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchSelectionController;->mHandler:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchSelectionHandler;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchSelectionController;ZII)V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchSelectionController;->handleSingleTap(ZII)V

    return-void
.end method

.method private getBaseContentView()Lorg/chromium/content/browser/ContentViewCore;
    .locals 1

    .prologue
    .line 198
    iget-object v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchSelectionController;->mActivity:Lcom/google/android/apps/chrome/ChromeActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ChromeActivity;->getCurrentTab()Lcom/google/android/apps/chrome/tab/ChromeTab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getContentViewCore()Lorg/chromium/content/browser/ContentViewCore;

    move-result-object v0

    return-object v0
.end method

.method private handleSingleTap(ZII)V
    .locals 3

    .prologue
    .line 175
    invoke-direct {p0}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchSelectionController;->getBaseContentView()Lorg/chromium/content/browser/ContentViewCore;

    move-result-object v0

    .line 176
    if-nez v0, :cond_0

    .line 192
    :goto_0
    return-void

    .line 178
    :cond_0
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchSelectionController;->mWasTapGestureDetected:Z

    .line 180
    if-nez p1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchSelectionController;->mLastSelectionType:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchSelectionController$SelectionType;

    sget-object v2, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchSelectionController$SelectionType;->LONG_PRESS:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchSelectionController$SelectionType;

    if-eq v1, v2, :cond_1

    invoke-virtual {v0}, Lorg/chromium/content/browser/ContentViewCore;->isFocusedNodeEditable()Z

    move-result v1

    if-nez v1, :cond_1

    .line 185
    int-to-float v1, p2

    iput v1, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchSelectionController;->mX:F

    .line 186
    int-to-float v1, p3

    iput v1, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchSelectionController;->mY:F

    .line 187
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchSelectionController;->mWasTapGestureDetected:Z

    .line 188
    invoke-virtual {v0}, Lorg/chromium/content/browser/ContentViewCore;->selectWordAroundCaret()V

    goto :goto_0

    .line 190
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchSelectionController;->mHandler:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchSelectionHandler;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchSelectionHandler;->handleInvalidTap()V

    goto :goto_0
.end method

.method private reset()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 161
    sget-object v0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchSelectionController$SelectionType;->UNDETERMINED:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchSelectionController$SelectionType;

    iput-object v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchSelectionController;->mLastSelectionType:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchSelectionController$SelectionType;

    .line 162
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchSelectionController;->mLastSelectedText:Ljava/lang/String;

    .line 164
    iput-boolean v1, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchSelectionController;->mWasTapGestureDetected:Z

    .line 165
    iput-boolean v1, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchSelectionController;->mIsSelectionBeingModified:Z

    .line 166
    return-void
.end method


# virtual methods
.method public clearSelection()V
    .locals 1

    .prologue
    .line 89
    invoke-direct {p0}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchSelectionController;->getBaseContentView()Lorg/chromium/content/browser/ContentViewCore;

    move-result-object v0

    .line 90
    if-eqz v0, :cond_0

    .line 91
    invoke-virtual {v0}, Lorg/chromium/content/browser/ContentViewCore;->clearSelection()V

    .line 93
    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchSelectionController;->reset()V

    .line 94
    return-void
.end method

.method public getGestureStateListener()Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchSelectionController$ContextualSearchGestureStateListener;
    .locals 2

    .prologue
    .line 68
    new-instance v0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchSelectionController$ContextualSearchGestureStateListener;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchSelectionController$ContextualSearchGestureStateListener;-><init>(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchSelectionController;Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchSelectionController$1;)V

    return-object v0
.end method

.method public getLastSelectedText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchSelectionController;->mLastSelectedText:Ljava/lang/String;

    return-object v0
.end method

.method public getLastSelectionType()Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchSelectionController$SelectionType;
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchSelectionController;->mLastSelectionType:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchSelectionController$SelectionType;

    return-object v0
.end method

.method public handleSelectionChanged(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 103
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 114
    :cond_0
    :goto_0
    return-void

    .line 105
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchSelectionController;->mIsSelectionBeingModified:Z

    if-eqz v0, :cond_2

    .line 106
    iput-object p1, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchSelectionController;->mLastSelectedText:Ljava/lang/String;

    .line 107
    iget-object v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchSelectionController;->mHandler:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchSelectionHandler;

    iget v1, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchSelectionController;->mX:F

    iget v2, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchSelectionController;->mY:F

    invoke-interface {v0, p1, v1, v2}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchSelectionHandler;->handleSelectionModification(Ljava/lang/String;FF)V

    goto :goto_0

    .line 108
    :cond_2
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchSelectionController;->mWasTapGestureDetected:Z

    if-eqz v0, :cond_0

    .line 109
    iput-object p1, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchSelectionController;->mLastSelectedText:Ljava/lang/String;

    .line 110
    sget-object v0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchSelectionController$SelectionType;->TAP:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchSelectionController$SelectionType;

    iput-object v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchSelectionController;->mLastSelectionType:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchSelectionController$SelectionType;

    .line 111
    iget-object v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchSelectionController;->mHandler:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchSelectionHandler;

    iget-object v1, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchSelectionController;->mLastSelectionType:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchSelectionController$SelectionType;

    iget v2, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchSelectionController;->mX:F

    iget v3, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchSelectionController;->mY:F

    invoke-interface {v0, p1, v1, v2, v3}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchSelectionHandler;->handleSelection(Ljava/lang/String;Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchSelectionController$SelectionType;FF)V

    .line 112
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchSelectionController;->mWasTapGestureDetected:Z

    goto :goto_0
.end method

.method public handleSelectionEvent(IFF)V
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 123
    .line 124
    packed-switch p1, :pswitch_data_0

    move v0, v1

    .line 139
    :goto_0
    if-eqz v0, :cond_0

    .line 144
    invoke-direct {p0}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchSelectionController;->getBaseContentView()Lorg/chromium/content/browser/ContentViewCore;

    move-result-object v0

    .line 145
    if-eqz v0, :cond_0

    .line 146
    invoke-virtual {v0}, Lorg/chromium/content/browser/ContentViewCore;->getSelectedText()Ljava/lang/String;

    move-result-object v0

    .line 147
    if-eqz v0, :cond_0

    .line 148
    iput p2, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchSelectionController;->mX:F

    .line 149
    iput p3, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchSelectionController;->mY:F

    .line 150
    iput-object v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchSelectionController;->mLastSelectedText:Ljava/lang/String;

    .line 151
    iget-object v1, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchSelectionController;->mHandler:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchSelectionHandler;

    sget-object v2, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchSelectionController$SelectionType;->LONG_PRESS:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchSelectionController$SelectionType;

    iget v3, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchSelectionController;->mX:F

    iget v4, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchSelectionController;->mY:F

    invoke-interface {v1, v0, v2, v3, v4}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchSelectionHandler;->handleSelection(Ljava/lang/String;Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchSelectionController$SelectionType;FF)V

    .line 155
    :cond_0
    return-void

    .line 126
    :pswitch_0
    iput-boolean v1, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchSelectionController;->mWasTapGestureDetected:Z

    .line 127
    sget-object v1, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchSelectionController$SelectionType;->LONG_PRESS:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchSelectionController$SelectionType;

    iput-object v1, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchSelectionController;->mLastSelectionType:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchSelectionController$SelectionType;

    goto :goto_0

    .line 131
    :pswitch_1
    invoke-direct {p0}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchSelectionController;->reset()V

    move v0, v1

    .line 132
    goto :goto_0

    .line 134
    :pswitch_2
    iput-boolean v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchSelectionController;->mIsSelectionBeingModified:Z

    move v0, v1

    .line 135
    goto :goto_0

    .line 137
    :pswitch_3
    iput-boolean v1, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchSelectionController;->mIsSelectionBeingModified:Z

    goto :goto_0

    .line 124
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
