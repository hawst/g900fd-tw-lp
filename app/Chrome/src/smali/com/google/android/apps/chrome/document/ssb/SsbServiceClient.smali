.class public Lcom/google/android/apps/chrome/document/ssb/SsbServiceClient;
.super Ljava/lang/Object;
.source "SsbServiceClient.java"


# instance fields
.field private mCallback:Lcom/google/android/apps/chrome/document/ssb/SsbServiceCallback;

.field private final mConnection:Lcom/google/android/apps/chrome/document/ssb/SsbServiceClient$SsbServiceConnection;

.field private final mContext:Landroid/content/Context;

.field private final mMessenger:Landroid/os/Messenger;

.field private mPendingSsbContext:[B

.field private mService:Landroid/os/Messenger;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 66
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    new-instance v0, Landroid/os/Messenger;

    new-instance v1, Lcom/google/android/apps/chrome/document/ssb/SsbServiceClient$IncomingHandler;

    invoke-direct {v1, p0, v2}, Lcom/google/android/apps/chrome/document/ssb/SsbServiceClient$IncomingHandler;-><init>(Lcom/google/android/apps/chrome/document/ssb/SsbServiceClient;Lcom/google/android/apps/chrome/document/ssb/SsbServiceClient$1;)V

    invoke-direct {v0, v1}, Landroid/os/Messenger;-><init>(Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/document/ssb/SsbServiceClient;->mMessenger:Landroid/os/Messenger;

    .line 35
    iput-object v2, p0, Lcom/google/android/apps/chrome/document/ssb/SsbServiceClient;->mService:Landroid/os/Messenger;

    .line 67
    iput-object p1, p0, Lcom/google/android/apps/chrome/document/ssb/SsbServiceClient;->mContext:Landroid/content/Context;

    .line 68
    new-instance v0, Lcom/google/android/apps/chrome/document/ssb/SsbServiceClient$SsbServiceConnection;

    invoke-direct {v0, p0, v2}, Lcom/google/android/apps/chrome/document/ssb/SsbServiceClient$SsbServiceConnection;-><init>(Lcom/google/android/apps/chrome/document/ssb/SsbServiceClient;Lcom/google/android/apps/chrome/document/ssb/SsbServiceClient$1;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/document/ssb/SsbServiceClient;->mConnection:Lcom/google/android/apps/chrome/document/ssb/SsbServiceClient$SsbServiceConnection;

    .line 69
    return-void
.end method

.method static synthetic access$100(Lcom/google/android/apps/chrome/document/ssb/SsbServiceClient;)Lcom/google/android/apps/chrome/document/ssb/SsbServiceCallback;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/ssb/SsbServiceClient;->mCallback:Lcom/google/android/apps/chrome/document/ssb/SsbServiceCallback;

    return-object v0
.end method

.method static synthetic access$302(Lcom/google/android/apps/chrome/document/ssb/SsbServiceClient;Landroid/os/Messenger;)Landroid/os/Messenger;
    .locals 0

    .prologue
    .line 24
    iput-object p1, p0, Lcom/google/android/apps/chrome/document/ssb/SsbServiceClient;->mService:Landroid/os/Messenger;

    return-object p1
.end method

.method static synthetic access$400(Lcom/google/android/apps/chrome/document/ssb/SsbServiceClient;)V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Lcom/google/android/apps/chrome/document/ssb/SsbServiceClient;->prepareOverlayInternal()V

    return-void
.end method

.method static synthetic access$500(Lcom/google/android/apps/chrome/document/ssb/SsbServiceClient;)Landroid/os/Messenger;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/ssb/SsbServiceClient;->mMessenger:Landroid/os/Messenger;

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/android/apps/chrome/document/ssb/SsbServiceClient;Landroid/os/Message;)V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/document/ssb/SsbServiceClient;->sendMessage(Landroid/os/Message;)V

    return-void
.end method

.method private prepareOverlayInternal()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 120
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/ssb/SsbServiceClient;->mPendingSsbContext:[B

    if-nez v0, :cond_0

    .line 132
    :goto_0
    return-void

    .line 122
    :cond_0
    const/4 v0, 0x1

    invoke-static {v4, v0}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v0

    .line 123
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 124
    const-string/jumbo v2, "ssb_service:ssb_context"

    iget-object v3, p0, Lcom/google/android/apps/chrome/document/ssb/SsbServiceClient;->mPendingSsbContext:[B

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V

    .line 125
    invoke-virtual {v0, v1}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 127
    :try_start_0
    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/document/ssb/SsbServiceClient;->sendMessage(Landroid/os/Message;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 131
    :goto_1
    iput-object v4, p0, Lcom/google/android/apps/chrome/document/ssb/SsbServiceClient;->mPendingSsbContext:[B

    goto :goto_0

    .line 128
    :catch_0
    move-exception v0

    .line 129
    const-string/jumbo v1, "SsbServiceClient"

    const-string/jumbo v2, "REQUEST_PREPARE_OVERLAY message failed"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method private sendMessage(Landroid/os/Message;)V
    .locals 3

    .prologue
    .line 162
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    const-string/jumbo v1, "ssb_service:ssb_package_name"

    iget-object v2, p0, Lcom/google/android/apps/chrome/document/ssb/SsbServiceClient;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 163
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/ssb/SsbServiceClient;->mService:Landroid/os/Messenger;

    invoke-virtual {v0, p1}, Landroid/os/Messenger;->send(Landroid/os/Message;)V

    .line 164
    return-void
.end method


# virtual methods
.method public connect(Lcom/google/android/apps/chrome/document/ssb/SsbServiceCallback;)Z
    .locals 4

    .prologue
    .line 81
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/ssb/SsbServiceClient;->mCallback:Lcom/google/android/apps/chrome/document/ssb/SsbServiceCallback;

    if-eqz v0, :cond_0

    const-string/jumbo v0, "SsbServiceClient"

    const-string/jumbo v1, "Already connected."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 82
    :cond_0
    iput-object p1, p0, Lcom/google/android/apps/chrome/document/ssb/SsbServiceClient;->mCallback:Lcom/google/android/apps/chrome/document/ssb/SsbServiceCallback;

    .line 83
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "com.google.android.ssb.action.SSB_SERVICE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string/jumbo v1, "com.google.android.googlequicksearchbox"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 84
    iget-object v1, p0, Lcom/google/android/apps/chrome/document/ssb/SsbServiceClient;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/apps/chrome/document/ssb/SsbServiceClient;->mConnection:Lcom/google/android/apps/chrome/document/ssb/SsbServiceClient$SsbServiceConnection;

    const/4 v3, 0x1

    invoke-virtual {v1, v0, v2, v3}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    move-result v0

    return v0
.end method

.method public disconnect()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 91
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/ssb/SsbServiceClient;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/apps/chrome/document/ssb/SsbServiceClient;->mConnection:Lcom/google/android/apps/chrome/document/ssb/SsbServiceClient$SsbServiceConnection;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    .line 92
    iput-object v2, p0, Lcom/google/android/apps/chrome/document/ssb/SsbServiceClient;->mService:Landroid/os/Messenger;

    .line 93
    iput-object v2, p0, Lcom/google/android/apps/chrome/document/ssb/SsbServiceClient;->mCallback:Lcom/google/android/apps/chrome/document/ssb/SsbServiceCallback;

    .line 94
    iput-object v2, p0, Lcom/google/android/apps/chrome/document/ssb/SsbServiceClient;->mPendingSsbContext:[B

    .line 95
    return-void
.end method

.method public isConnected()Z
    .locals 1

    .prologue
    .line 102
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/ssb/SsbServiceClient;->mService:Landroid/os/Messenger;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public prepareOverlay([B)V
    .locals 2

    .prologue
    .line 114
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    const-string/jumbo v1, "SsbContext is null."

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 115
    :cond_0
    iput-object p1, p0, Lcom/google/android/apps/chrome/document/ssb/SsbServiceClient;->mPendingSsbContext:[B

    .line 116
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/document/ssb/SsbServiceClient;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/google/android/apps/chrome/document/ssb/SsbServiceClient;->prepareOverlayInternal()V

    .line 117
    :cond_1
    return-void
.end method
