.class public Lcom/google/android/apps/chrome/preferences/FontSizePreview;
.super Landroid/preference/Preference;
.source "FontSizePreview.java"


# instance fields
.field private mFontSizeObserver:Lcom/google/android/apps/chrome/preferences/FontSizePreview$FontSizeObserver;

.field private mFontSizePrefs:Lorg/chromium/chrome/browser/accessibility/FontSizePrefs;

.field private mPreview:Landroid/widget/TextView;

.field private mView:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 44
    invoke-direct {p0, p1, p2}, Landroid/preference/Preference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 45
    sget v0, Lcom/google/android/apps/chrome/R$layout;->custom_preference:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/preferences/FontSizePreview;->setLayoutResource(I)V

    .line 46
    sget v0, Lcom/google/android/apps/chrome/R$layout;->preference_font_size_preview:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/preferences/FontSizePreview;->setWidgetLayoutResource(I)V

    .line 47
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/chrome/preferences/FontSizePreview;)V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Lcom/google/android/apps/chrome/preferences/FontSizePreview;->updatePreview()V

    return-void
.end method

.method private updatePreview()V
    .locals 4

    .prologue
    .line 84
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/FontSizePreview;->mPreview:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 89
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/FontSizePreview;->mPreview:Landroid/widget/TextView;

    const/4 v1, 0x1

    const/high16 v2, 0x41500000    # 13.0f

    iget-object v3, p0, Lcom/google/android/apps/chrome/preferences/FontSizePreview;->mFontSizePrefs:Lorg/chromium/chrome/browser/accessibility/FontSizePrefs;

    invoke-virtual {v3}, Lorg/chromium/chrome/browser/accessibility/FontSizePrefs;->getFontScaleFactor()F

    move-result v3

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 93
    :cond_0
    return-void
.end method


# virtual methods
.method protected onBindView(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 59
    invoke-super {p0, p1}, Landroid/preference/Preference;->onBindView(Landroid/view/View;)V

    .line 60
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/FontSizePreview;->mPreview:Landroid/widget/TextView;

    if-nez v0, :cond_0

    .line 61
    sget v0, Lcom/google/android/apps/chrome/R$id;->preview:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/chrome/preferences/FontSizePreview;->mPreview:Landroid/widget/TextView;

    .line 62
    invoke-direct {p0}, Lcom/google/android/apps/chrome/preferences/FontSizePreview;->updatePreview()V

    .line 64
    :cond_0
    return-void
.end method

.method protected onCreateView(Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/FontSizePreview;->mView:Landroid/view/View;

    if-nez v0, :cond_0

    .line 52
    invoke-super {p0, p1}, Landroid/preference/Preference;->onCreateView(Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/preferences/FontSizePreview;->mView:Landroid/view/View;

    .line 54
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/FontSizePreview;->mView:Landroid/view/View;

    return-object v0
.end method

.method public startObservingFontPrefs()V
    .locals 2

    .prologue
    .line 70
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/FontSizePreview;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lorg/chromium/chrome/browser/accessibility/FontSizePrefs;->getInstance(Landroid/content/Context;)Lorg/chromium/chrome/browser/accessibility/FontSizePrefs;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/preferences/FontSizePreview;->mFontSizePrefs:Lorg/chromium/chrome/browser/accessibility/FontSizePrefs;

    .line 71
    new-instance v0, Lcom/google/android/apps/chrome/preferences/FontSizePreview$FontSizeObserver;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/chrome/preferences/FontSizePreview$FontSizeObserver;-><init>(Lcom/google/android/apps/chrome/preferences/FontSizePreview;Lcom/google/android/apps/chrome/preferences/FontSizePreview$1;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/preferences/FontSizePreview;->mFontSizeObserver:Lcom/google/android/apps/chrome/preferences/FontSizePreview$FontSizeObserver;

    .line 72
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/FontSizePreview;->mFontSizePrefs:Lorg/chromium/chrome/browser/accessibility/FontSizePrefs;

    iget-object v1, p0, Lcom/google/android/apps/chrome/preferences/FontSizePreview;->mFontSizeObserver:Lcom/google/android/apps/chrome/preferences/FontSizePreview$FontSizeObserver;

    invoke-virtual {v0, v1}, Lorg/chromium/chrome/browser/accessibility/FontSizePrefs;->addObserver(Lorg/chromium/chrome/browser/accessibility/FontSizePrefs$Observer;)Z

    .line 73
    invoke-direct {p0}, Lcom/google/android/apps/chrome/preferences/FontSizePreview;->updatePreview()V

    .line 74
    return-void
.end method

.method public stopObservingFontPrefs()V
    .locals 2

    .prologue
    .line 80
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/FontSizePreview;->mFontSizePrefs:Lorg/chromium/chrome/browser/accessibility/FontSizePrefs;

    iget-object v1, p0, Lcom/google/android/apps/chrome/preferences/FontSizePreview;->mFontSizeObserver:Lcom/google/android/apps/chrome/preferences/FontSizePreview$FontSizeObserver;

    invoke-virtual {v0, v1}, Lorg/chromium/chrome/browser/accessibility/FontSizePrefs;->removeObserver(Lorg/chromium/chrome/browser/accessibility/FontSizePrefs$Observer;)Z

    .line 81
    return-void
.end method
