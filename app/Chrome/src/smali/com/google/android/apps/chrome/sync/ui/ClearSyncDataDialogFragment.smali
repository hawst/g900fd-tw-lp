.class public Lcom/google/android/apps/chrome/sync/ui/ClearSyncDataDialogFragment;
.super Lcom/google/android/apps/chrome/preferences/ClearBrowsingDataDialogFragment;
.source "ClearSyncDataDialogFragment.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Lcom/google/android/apps/chrome/preferences/ClearBrowsingDataDialogFragment;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/chrome/sync/ui/ClearSyncDataDialogFragment;Ljava/util/EnumSet;)V
    .locals 0

    .prologue
    .line 20
    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/sync/ui/ClearSyncDataDialogFragment;->clearBrowsingData(Ljava/util/EnumSet;)V

    return-void
.end method


# virtual methods
.method protected getDefaultDialogOptionsSelections()Ljava/util/EnumSet;
    .locals 1

    .prologue
    .line 29
    const-class v0, Lcom/google/android/apps/chrome/preferences/ClearBrowsingDataDialogFragment$DialogOption;

    invoke-static {v0}, Ljava/util/EnumSet;->allOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object v0

    return-object v0
.end method

.method protected getDialogOptions()[Lcom/google/android/apps/chrome/preferences/ClearBrowsingDataDialogFragment$DialogOption;
    .locals 1

    .prologue
    .line 24
    invoke-static {}, Lcom/google/android/apps/chrome/preferences/ClearBrowsingDataDialogFragment$DialogOption;->values()[Lcom/google/android/apps/chrome/preferences/ClearBrowsingDataDialogFragment$DialogOption;

    move-result-object v0

    return-object v0
.end method

.method protected onOptionSelected(Ljava/util/EnumSet;)V
    .locals 2

    .prologue
    .line 34
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/sync/ui/ClearSyncDataDialogFragment;->showProgressDialog()V

    .line 40
    new-instance v0, Lcom/google/android/apps/chrome/sync/ui/ClearSyncDataDialogFragment$1;

    invoke-direct {v0, p0, p1}, Lcom/google/android/apps/chrome/sync/ui/ClearSyncDataDialogFragment$1;-><init>(Lcom/google/android/apps/chrome/sync/ui/ClearSyncDataDialogFragment;Ljava/util/EnumSet;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/sync/ui/ClearSyncDataDialogFragment$1;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 59
    return-void
.end method
