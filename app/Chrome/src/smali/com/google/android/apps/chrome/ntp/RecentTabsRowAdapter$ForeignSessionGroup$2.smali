.class Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$ForeignSessionGroup$2;
.super Ljava/lang/Object;
.source "RecentTabsRowAdapter.java"

# interfaces
.implements Landroid/view/MenuItem$OnMenuItemClickListener;


# instance fields
.field final synthetic this$1:Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$ForeignSessionGroup;

.field final synthetic val$foreignSessionTab:Lorg/chromium/chrome/browser/ForeignSessionHelper$ForeignSessionTab;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$ForeignSessionGroup;Lorg/chromium/chrome/browser/ForeignSessionHelper$ForeignSessionTab;)V
    .locals 0

    .prologue
    .line 367
    iput-object p1, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$ForeignSessionGroup$2;->this$1:Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$ForeignSessionGroup;

    iput-object p2, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$ForeignSessionGroup$2;->val$foreignSessionTab:Lorg/chromium/chrome/browser/ForeignSessionHelper$ForeignSessionTab;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onMenuItemClick(Landroid/view/MenuItem;)Z
    .locals 4

    .prologue
    .line 370
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$ForeignSessionGroup$2;->this$1:Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$ForeignSessionGroup;

    iget-object v0, v0, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$ForeignSessionGroup;->this$0:Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;

    # getter for: Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;->mRecentTabsManager:Lcom/google/android/apps/chrome/ntp/RecentTabsManager;
    invoke-static {v0}, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;->access$300(Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;)Lcom/google/android/apps/chrome/ntp/RecentTabsManager;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$ForeignSessionGroup$2;->this$1:Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$ForeignSessionGroup;

    # getter for: Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$ForeignSessionGroup;->mForeignSession:Lorg/chromium/chrome/browser/ForeignSessionHelper$ForeignSession;
    invoke-static {v1}, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$ForeignSessionGroup;->access$600(Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$ForeignSessionGroup;)Lorg/chromium/chrome/browser/ForeignSessionHelper$ForeignSession;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$ForeignSessionGroup$2;->val$foreignSessionTab:Lorg/chromium/chrome/browser/ForeignSessionHelper$ForeignSessionTab;

    const/4 v3, 0x5

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/apps/chrome/ntp/RecentTabsManager;->openForeignSessionTab(Lorg/chromium/chrome/browser/ForeignSessionHelper$ForeignSession;Lorg/chromium/chrome/browser/ForeignSessionHelper$ForeignSessionTab;I)V

    .line 372
    const/4 v0, 0x1

    return v0
.end method
