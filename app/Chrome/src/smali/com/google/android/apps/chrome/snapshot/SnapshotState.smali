.class final enum Lcom/google/android/apps/chrome/snapshot/SnapshotState;
.super Ljava/lang/Enum;
.source "SnapshotState.java"


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/apps/chrome/snapshot/SnapshotState;

.field public static final enum CREATED:Lcom/google/android/apps/chrome/snapshot/SnapshotState;

.field public static final enum UPDATED:Lcom/google/android/apps/chrome/snapshot/SnapshotState;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 14
    new-instance v0, Lcom/google/android/apps/chrome/snapshot/SnapshotState;

    const-string/jumbo v1, "CREATED"

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/chrome/snapshot/SnapshotState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/chrome/snapshot/SnapshotState;->CREATED:Lcom/google/android/apps/chrome/snapshot/SnapshotState;

    .line 15
    new-instance v0, Lcom/google/android/apps/chrome/snapshot/SnapshotState;

    const-string/jumbo v1, "UPDATED"

    invoke-direct {v0, v1, v3}, Lcom/google/android/apps/chrome/snapshot/SnapshotState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/chrome/snapshot/SnapshotState;->UPDATED:Lcom/google/android/apps/chrome/snapshot/SnapshotState;

    .line 13
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/google/android/apps/chrome/snapshot/SnapshotState;

    sget-object v1, Lcom/google/android/apps/chrome/snapshot/SnapshotState;->CREATED:Lcom/google/android/apps/chrome/snapshot/SnapshotState;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/apps/chrome/snapshot/SnapshotState;->UPDATED:Lcom/google/android/apps/chrome/snapshot/SnapshotState;

    aput-object v1, v0, v3

    sput-object v0, Lcom/google/android/apps/chrome/snapshot/SnapshotState;->$VALUES:[Lcom/google/android/apps/chrome/snapshot/SnapshotState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/chrome/snapshot/SnapshotState;
    .locals 1

    .prologue
    .line 13
    const-class v0, Lcom/google/android/apps/chrome/snapshot/SnapshotState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/snapshot/SnapshotState;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/chrome/snapshot/SnapshotState;
    .locals 1

    .prologue
    .line 13
    sget-object v0, Lcom/google/android/apps/chrome/snapshot/SnapshotState;->$VALUES:[Lcom/google/android/apps/chrome/snapshot/SnapshotState;

    invoke-virtual {v0}, [Lcom/google/android/apps/chrome/snapshot/SnapshotState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/chrome/snapshot/SnapshotState;

    return-object v0
.end method
