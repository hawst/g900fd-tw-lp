.class Lcom/google/android/apps/chrome/dom_distiller/ReaderModeManager$2;
.super Ljava/lang/Object;
.source "ReaderModeManager.java"

# interfaces
.implements Lorg/chromium/content_public/browser/JavaScriptCallback;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/dom_distiller/ReaderModeManager;

.field final synthetic val$forceRecord:Z


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/dom_distiller/ReaderModeManager;Z)V
    .locals 0

    .prologue
    .line 145
    iput-object p1, p0, Lcom/google/android/apps/chrome/dom_distiller/ReaderModeManager$2;->this$0:Lcom/google/android/apps/chrome/dom_distiller/ReaderModeManager;

    iput-boolean p2, p0, Lcom/google/android/apps/chrome/dom_distiller/ReaderModeManager$2;->val$forceRecord:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public handleJavaScriptResult(Ljava/lang/String;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 148
    const-string/jumbo v2, "true"

    invoke-static {p1, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 149
    iget-object v2, p0, Lcom/google/android/apps/chrome/dom_distiller/ReaderModeManager$2;->this$0:Lcom/google/android/apps/chrome/dom_distiller/ReaderModeManager;

    # setter for: Lcom/google/android/apps/chrome/dom_distiller/ReaderModeManager;->mReaderModeStatus:I
    invoke-static {v2, v1}, Lcom/google/android/apps/chrome/dom_distiller/ReaderModeManager;->access$202(Lcom/google/android/apps/chrome/dom_distiller/ReaderModeManager;I)I

    .line 153
    :goto_0
    iget-object v2, p0, Lcom/google/android/apps/chrome/dom_distiller/ReaderModeManager$2;->this$0:Lcom/google/android/apps/chrome/dom_distiller/ReaderModeManager;

    # getter for: Lcom/google/android/apps/chrome/dom_distiller/ReaderModeManager;->mIsUmaRecorded:Z
    invoke-static {v2}, Lcom/google/android/apps/chrome/dom_distiller/ReaderModeManager;->access$500(Lcom/google/android/apps/chrome/dom_distiller/ReaderModeManager;)Z

    move-result v2

    if-nez v2, :cond_1

    iget-object v2, p0, Lcom/google/android/apps/chrome/dom_distiller/ReaderModeManager$2;->this$0:Lcom/google/android/apps/chrome/dom_distiller/ReaderModeManager;

    # getter for: Lcom/google/android/apps/chrome/dom_distiller/ReaderModeManager;->mReaderModeStatus:I
    invoke-static {v2}, Lcom/google/android/apps/chrome/dom_distiller/ReaderModeManager;->access$200(Lcom/google/android/apps/chrome/dom_distiller/ReaderModeManager;)I

    move-result v2

    if-eqz v2, :cond_0

    iget-boolean v2, p0, Lcom/google/android/apps/chrome/dom_distiller/ReaderModeManager$2;->val$forceRecord:Z

    if-eqz v2, :cond_1

    .line 154
    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/chrome/dom_distiller/ReaderModeManager$2;->this$0:Lcom/google/android/apps/chrome/dom_distiller/ReaderModeManager;

    # setter for: Lcom/google/android/apps/chrome/dom_distiller/ReaderModeManager;->mIsUmaRecorded:Z
    invoke-static {v2, v0}, Lcom/google/android/apps/chrome/dom_distiller/ReaderModeManager;->access$502(Lcom/google/android/apps/chrome/dom_distiller/ReaderModeManager;Z)Z

    .line 155
    iget-object v2, p0, Lcom/google/android/apps/chrome/dom_distiller/ReaderModeManager$2;->this$0:Lcom/google/android/apps/chrome/dom_distiller/ReaderModeManager;

    # getter for: Lcom/google/android/apps/chrome/dom_distiller/ReaderModeManager;->mReaderModeStatus:I
    invoke-static {v2}, Lcom/google/android/apps/chrome/dom_distiller/ReaderModeManager;->access$200(Lcom/google/android/apps/chrome/dom_distiller/ReaderModeManager;)I

    move-result v2

    if-nez v2, :cond_3

    :goto_1
    invoke-static {v0}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->readerModeEligible(Z)V

    .line 157
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/dom_distiller/ReaderModeManager$2;->this$0:Lcom/google/android/apps/chrome/dom_distiller/ReaderModeManager;

    # invokes: Lcom/google/android/apps/chrome/dom_distiller/ReaderModeManager;->sendReaderModeStatusChangedNotification()V
    invoke-static {v0}, Lcom/google/android/apps/chrome/dom_distiller/ReaderModeManager;->access$300(Lcom/google/android/apps/chrome/dom_distiller/ReaderModeManager;)V

    .line 158
    return-void

    .line 151
    :cond_2
    iget-object v2, p0, Lcom/google/android/apps/chrome/dom_distiller/ReaderModeManager$2;->this$0:Lcom/google/android/apps/chrome/dom_distiller/ReaderModeManager;

    # setter for: Lcom/google/android/apps/chrome/dom_distiller/ReaderModeManager;->mReaderModeStatus:I
    invoke-static {v2, v0}, Lcom/google/android/apps/chrome/dom_distiller/ReaderModeManager;->access$202(Lcom/google/android/apps/chrome/dom_distiller/ReaderModeManager;I)I

    goto :goto_0

    :cond_3
    move v0, v1

    .line 155
    goto :goto_1
.end method
