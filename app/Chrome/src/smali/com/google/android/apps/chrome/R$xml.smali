.class public final Lcom/google/android/apps/chrome/R$xml;
.super Ljava/lang/Object;
.source "R.java"


# static fields
.field public static final about_chrome_preferences:I = 0x7f080000

.field public static final accessibility_preferences:I = 0x7f080001

.field public static final advanced_preferences:I = 0x7f080002

.field public static final autofill_preferences:I = 0x7f080003

.field public static final bandwidth_reduction_preferences:I = 0x7f080004

.field public static final bandwidth_reduction_preferences_off:I = 0x7f080005

.field public static final basics_preferences:I = 0x7f080006

.field public static final bookmark_thumbnail_widget_info:I = 0x7f080007

.field public static final content_preferences:I = 0x7f080008

.field public static final contextual_search_preferences:I = 0x7f080009

.field public static final do_not_track_preferences:I = 0x7f08000a

.field public static final document_mode_preferences:I = 0x7f08000b

.field public static final file_paths:I = 0x7f08000c

.field public static final legal_information_preferences:I = 0x7f08000d

.field public static final preference_headers_phone:I = 0x7f08000e

.field public static final preference_headers_tablet:I = 0x7f08000f

.field public static final privacy_preferences:I = 0x7f080010

.field public static final protected_content_preferences:I = 0x7f080011

.field public static final search_corpora:I = 0x7f080012

.field public static final search_engine_preferences:I = 0x7f080013

.field public static final searchable:I = 0x7f080014

.field public static final sign_in_preferences:I = 0x7f080015

.field public static final single_website_settings_preferences:I = 0x7f080016

.field public static final sync_customization_preferences:I = 0x7f080017

.field public static final syncadapter:I = 0x7f080018

.field public static final translate_preferences:I = 0x7f080019

.field public static final users_preferences:I = 0x7f08001a

.field public static final website_settings_preferences:I = 0x7f08001b


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 5706
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
