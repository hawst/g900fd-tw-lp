.class Lcom/google/android/apps/chrome/CookiesFetcher$3;
.super Landroid/os/AsyncTask;
.source "CookiesFetcher.java"


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/CookiesFetcher;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/CookiesFetcher;)V
    .locals 0

    .prologue
    .line 267
    iput-object p1, p0, Lcom/google/android/apps/chrome/CookiesFetcher$3;->this$0:Lcom/google/android/apps/chrome/CookiesFetcher;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 267
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/CookiesFetcher$3;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 3

    .prologue
    .line 270
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/google/android/apps/chrome/CookiesFetcher$3;->this$0:Lcom/google/android/apps/chrome/CookiesFetcher;

    # getter for: Lcom/google/android/apps/chrome/CookiesFetcher;->mFileName:Ljava/lang/String;
    invoke-static {v1}, Lcom/google/android/apps/chrome/CookiesFetcher;->access$100(Lcom/google/android/apps/chrome/CookiesFetcher;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 271
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 272
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    move-result v0

    if-nez v0, :cond_0

    const-string/jumbo v0, "CookiesFetcher"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "Failed to delete "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/apps/chrome/CookiesFetcher$3;->this$0:Lcom/google/android/apps/chrome/CookiesFetcher;

    # getter for: Lcom/google/android/apps/chrome/CookiesFetcher;->mFileName:Ljava/lang/String;
    invoke-static {v2}, Lcom/google/android/apps/chrome/CookiesFetcher;->access$100(Lcom/google/android/apps/chrome/CookiesFetcher;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 274
    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method
