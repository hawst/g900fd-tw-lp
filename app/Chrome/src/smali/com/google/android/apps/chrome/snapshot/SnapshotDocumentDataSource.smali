.class public Lcom/google/android/apps/chrome/snapshot/SnapshotDocumentDataSource;
.super Ljava/lang/Object;
.source "SnapshotDocumentDataSource.java"


# instance fields
.field private final mContext:Landroid/content/Context;

.field private final mHandler:Landroid/os/Handler;

.field private mIsActive:Z

.field private final mListener:Lcom/google/android/apps/chrome/snapshot/SnapshotDocumentDataSource$SnapshotDocumentDataSourceListener;

.field private final mSnapshotStateBroadcastReceiver:Landroid/content/BroadcastReceiver;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/chrome/snapshot/SnapshotDocumentDataSource$SnapshotDocumentDataSourceListener;)V
    .locals 2

    .prologue
    .line 65
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocumentDataSource;->mHandler:Landroid/os/Handler;

    .line 66
    iput-object p1, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocumentDataSource;->mContext:Landroid/content/Context;

    .line 67
    iput-object p2, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocumentDataSource;->mListener:Lcom/google/android/apps/chrome/snapshot/SnapshotDocumentDataSource$SnapshotDocumentDataSourceListener;

    .line 68
    invoke-direct {p0}, Lcom/google/android/apps/chrome/snapshot/SnapshotDocumentDataSource;->buildBroadcastReceiver()Landroid/content/BroadcastReceiver;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocumentDataSource;->mSnapshotStateBroadcastReceiver:Landroid/content/BroadcastReceiver;

    .line 69
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/chrome/snapshot/SnapshotDocumentDataSource;)V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Lcom/google/android/apps/chrome/snapshot/SnapshotDocumentDataSource;->updateSnapshotDocuments()V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/apps/chrome/snapshot/SnapshotDocumentDataSource;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocumentDataSource;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/apps/chrome/snapshot/SnapshotDocumentDataSource;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocumentDataSource;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/apps/chrome/snapshot/SnapshotDocumentDataSource;)Lcom/google/android/apps/chrome/snapshot/SnapshotDocumentDataSource$SnapshotDocumentDataSourceListener;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocumentDataSource;->mListener:Lcom/google/android/apps/chrome/snapshot/SnapshotDocumentDataSource$SnapshotDocumentDataSourceListener;

    return-object v0
.end method

.method private buildBroadcastReceiver()Landroid/content/BroadcastReceiver;
    .locals 1

    .prologue
    .line 72
    new-instance v0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocumentDataSource$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/snapshot/SnapshotDocumentDataSource$1;-><init>(Lcom/google/android/apps/chrome/snapshot/SnapshotDocumentDataSource;)V

    return-object v0
.end method

.method private registerForNotifications()V
    .locals 4

    .prologue
    .line 89
    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocumentDataSource;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocumentDataSource;->mSnapshotStateBroadcastReceiver:Landroid/content/BroadcastReceiver;

    new-instance v2, Landroid/content/IntentFilter;

    const-string/jumbo v3, "com.google.android.apps.chrome.snapshot.ACTION_SNAPSHOT_STATE_UPDATE"

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 91
    return-void
.end method

.method private unregisterForNotifications()V
    .locals 2

    .prologue
    .line 94
    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocumentDataSource;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocumentDataSource;->mSnapshotStateBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 95
    return-void
.end method

.method private updateSnapshotDocuments()V
    .locals 2

    .prologue
    .line 98
    new-instance v0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocumentDataSource$2;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/snapshot/SnapshotDocumentDataSource$2;-><init>(Lcom/google/android/apps/chrome/snapshot/SnapshotDocumentDataSource;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/snapshot/SnapshotDocumentDataSource$2;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 109
    return-void
.end method


# virtual methods
.method public setActive(Z)V
    .locals 1

    .prologue
    .line 48
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocumentDataSource;->mIsActive:Z

    if-ne v0, p1, :cond_0

    .line 56
    :goto_0
    return-void

    .line 49
    :cond_0
    iput-boolean p1, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocumentDataSource;->mIsActive:Z

    .line 50
    if-eqz p1, :cond_1

    .line 51
    invoke-direct {p0}, Lcom/google/android/apps/chrome/snapshot/SnapshotDocumentDataSource;->registerForNotifications()V

    .line 52
    invoke-direct {p0}, Lcom/google/android/apps/chrome/snapshot/SnapshotDocumentDataSource;->updateSnapshotDocuments()V

    goto :goto_0

    .line 54
    :cond_1
    invoke-direct {p0}, Lcom/google/android/apps/chrome/snapshot/SnapshotDocumentDataSource;->unregisterForNotifications()V

    goto :goto_0
.end method
