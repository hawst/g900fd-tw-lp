.class public Lcom/google/android/apps/chrome/help/GoogleHelpUtils;
.super Ljava/lang/Object;
.source "GoogleHelpUtils.java"


# static fields
.field private static final FALLBACK_SUPPORT_URI:Landroid/net/Uri;

.field public static final HELP_CONTEXT_ID_BOOKMARKS:Ljava/lang/String; = "mobile_bookmarks"

.field public static final HELP_CONTEXT_ID_ERROR:Ljava/lang/String; = "mobile_error"

.field public static final HELP_CONTEXT_ID_GENERAL:Ljava/lang/String; = "mobile_general"

.field public static final HELP_CONTEXT_ID_HISTORY:Ljava/lang/String; = "mobile_history"

.field public static final HELP_CONTEXT_ID_INCOGNITO:Ljava/lang/String; = "mobile_incognito"

.field public static final HELP_CONTEXT_ID_NEW_PAGE:Ljava/lang/String; = "mobile_new_tab"

.field public static final HELP_CONTEXT_ID_PRIVACY:Ljava/lang/String; = "mobile_privacy"

.field public static final HELP_CONTEXT_ID_SEARCH_RESULTS:Ljava/lang/String; = "mobile_search_results"

.field public static final HELP_CONTEXT_ID_SETTINGS:Ljava/lang/String; = "mobile_settings"

.field public static final HELP_CONTEXT_ID_TRANSLATE:Ljava/lang/String; = "mobile_translate"

.field public static final HELP_CONTEXT_ID_WEBPAGE:Ljava/lang/String; = "mobile_webpage"


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 45
    const-string/jumbo v0, "https://support.google.com/chrome/topic/6069782"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/chrome/help/GoogleHelpUtils;->FALLBACK_SUPPORT_URI:Landroid/net/Uri;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getHelpContextIdFromUrl(Ljava/lang/String;Z)Ljava/lang/String;
    .locals 1

    .prologue
    .line 85
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 86
    const-string/jumbo v0, "mobile_general"

    .line 100
    :goto_0
    return-object v0

    .line 87
    :cond_0
    const-string/jumbo v0, "chrome-native://bookmarks/"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 88
    const-string/jumbo v0, "mobile_bookmarks"

    goto :goto_0

    .line 89
    :cond_1
    const-string/jumbo v0, "chrome://history/"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 90
    const-string/jumbo v0, "mobile_history"

    goto :goto_0

    .line 92
    :cond_2
    invoke-static {p0}, Lorg/chromium/chrome/browser/UrlUtilities;->nativeIsGoogleSearchUrl(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 93
    const-string/jumbo v0, "mobile_search_results"

    goto :goto_0

    .line 95
    :cond_3
    if-eqz p1, :cond_4

    .line 96
    const-string/jumbo v0, "mobile_incognito"

    goto :goto_0

    .line 97
    :cond_4
    const-string/jumbo v0, "chrome-native://newtab/"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 98
    const-string/jumbo v0, "mobile_new_tab"

    goto :goto_0

    .line 100
    :cond_5
    const-string/jumbo v0, "mobile_webpage"

    goto :goto_0
.end method

.method private static launchFallbackSupportUri(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 104
    const-string/jumbo v0, "GoogleHelpUtils"

    const-string/jumbo v1, "Launching fallback support URI."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 105
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "android.intent.action.VIEW"

    sget-object v2, Lcom/google/android/apps/chrome/help/GoogleHelpUtils;->FALLBACK_SUPPORT_URI:Landroid/net/Uri;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 108
    const-string/jumbo v1, "com.android.browser.application_id"

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 109
    const-string/jumbo v1, "create_new_tab"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 110
    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 111
    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 112
    return-void
.end method

.method public static show(Landroid/app/Activity;Ljava/lang/String;Landroid/graphics/Bitmap;)V
    .locals 3

    .prologue
    .line 59
    const-string/jumbo v0, "GoogleHelpUtils"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "About to show help for the context ID: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 60
    invoke-static {p0}, Lcom/google/android/apps/chrome/externalauth/ExternalAuthUtils;->canUseFirstPartyGooglePlayServices(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 61
    invoke-static {p0}, Lcom/google/android/apps/chrome/help/GoogleHelpUtils;->launchFallbackSupportUri(Landroid/content/Context;)V

    .line 75
    :goto_0
    return-void

    .line 65
    :cond_0
    new-instance v0, Lcom/google/android/gms/googlehelp/GoogleHelp;

    invoke-direct {v0, p1}, Lcom/google/android/gms/googlehelp/GoogleHelp;-><init>(Ljava/lang/String;)V

    .line 67
    if-nez p2, :cond_1

    .line 68
    invoke-static {p0}, Lcom/google/android/gms/googlehelp/GoogleHelp;->a(Landroid/app/Activity;)Landroid/graphics/Bitmap;

    move-result-object p2

    .line 70
    :cond_1
    invoke-virtual {v0, p2}, Lcom/google/android/gms/googlehelp/GoogleHelp;->a(Landroid/graphics/Bitmap;)Lcom/google/android/gms/googlehelp/GoogleHelp;

    .line 71
    sget-object v1, Lcom/google/android/apps/chrome/help/GoogleHelpUtils;->FALLBACK_SUPPORT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/googlehelp/GoogleHelp;->a(Landroid/net/Uri;)Lcom/google/android/gms/googlehelp/GoogleHelp;

    .line 73
    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/GoogleHelp;->a()Landroid/content/Intent;

    move-result-object v0

    .line 74
    new-instance v1, Lcom/google/android/gms/googlehelp/a;

    invoke-direct {v1, p0}, Lcom/google/android/gms/googlehelp/a;-><init>(Landroid/app/Activity;)V

    invoke-virtual {v1, v0}, Lcom/google/android/gms/googlehelp/a;->a(Landroid/content/Intent;)V

    goto :goto_0
.end method
