.class public Lcom/google/android/apps/chrome/document/ssb/ChromeSsbServiceCallback;
.super Ljava/lang/Object;
.source "ChromeSsbServiceCallback.java"


# static fields
.field public static final KEY_SHOW_HOTWORD_ENABLED:Ljava/lang/String; = "show_hotword_enabled"

.field private static sChromeSsbServiceCallback:Lcom/google/android/apps/chrome/document/ssb/ChromeSsbServiceCallback;


# instance fields
.field private final mCallback:Lcom/google/android/apps/chrome/document/ssb/SsbServiceCallback;

.field private final mListeners:Lorg/chromium/base/ObserverList;

.field private final mPreferences:Landroid/content/SharedPreferences;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 72
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    new-instance v0, Lorg/chromium/base/ObserverList;

    invoke-direct {v0}, Lorg/chromium/base/ObserverList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/document/ssb/ChromeSsbServiceCallback;->mListeners:Lorg/chromium/base/ObserverList;

    .line 30
    new-instance v0, Lcom/google/android/apps/chrome/document/ssb/ChromeSsbServiceCallback$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/document/ssb/ChromeSsbServiceCallback$1;-><init>(Lcom/google/android/apps/chrome/document/ssb/ChromeSsbServiceCallback;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/document/ssb/ChromeSsbServiceCallback;->mCallback:Lcom/google/android/apps/chrome/document/ssb/SsbServiceCallback;

    .line 73
    invoke-static {p1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/document/ssb/ChromeSsbServiceCallback;->mPreferences:Landroid/content/SharedPreferences;

    .line 74
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/chrome/document/ssb/ChromeSsbServiceCallback;)Landroid/content/SharedPreferences;
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/ssb/ChromeSsbServiceCallback;->mPreferences:Landroid/content/SharedPreferences;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/chrome/document/ssb/ChromeSsbServiceCallback;Z)V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/document/ssb/ChromeSsbServiceCallback;->notifyShowHotwordEnabledChange(Z)V

    return-void
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/google/android/apps/chrome/document/ssb/ChromeSsbServiceCallback;
    .locals 1

    .prologue
    .line 54
    sget-object v0, Lcom/google/android/apps/chrome/document/ssb/ChromeSsbServiceCallback;->sChromeSsbServiceCallback:Lcom/google/android/apps/chrome/document/ssb/ChromeSsbServiceCallback;

    if-nez v0, :cond_0

    .line 55
    new-instance v0, Lcom/google/android/apps/chrome/document/ssb/ChromeSsbServiceCallback;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/document/ssb/ChromeSsbServiceCallback;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/google/android/apps/chrome/document/ssb/ChromeSsbServiceCallback;->sChromeSsbServiceCallback:Lcom/google/android/apps/chrome/document/ssb/ChromeSsbServiceCallback;

    .line 57
    :cond_0
    sget-object v0, Lcom/google/android/apps/chrome/document/ssb/ChromeSsbServiceCallback;->sChromeSsbServiceCallback:Lcom/google/android/apps/chrome/document/ssb/ChromeSsbServiceCallback;

    return-object v0
.end method

.method private notifyShowHotwordEnabledChange(Z)V
    .locals 2

    .prologue
    .line 77
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/ssb/ChromeSsbServiceCallback;->mListeners:Lorg/chromium/base/ObserverList;

    invoke-virtual {v0}, Lorg/chromium/base/ObserverList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/document/ssb/SsbStateListener;

    .line 78
    invoke-interface {v0, p1}, Lcom/google/android/apps/chrome/document/ssb/SsbStateListener;->onHotwordStateChange(Z)V

    goto :goto_0

    .line 80
    :cond_0
    return-void
.end method


# virtual methods
.method public addListener(Lcom/google/android/apps/chrome/document/ssb/SsbStateListener;)V
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/ssb/ChromeSsbServiceCallback;->mListeners:Lorg/chromium/base/ObserverList;

    invoke-virtual {v0, p1}, Lorg/chromium/base/ObserverList;->addObserver(Ljava/lang/Object;)Z

    .line 62
    return-void
.end method

.method public getCallback()Lcom/google/android/apps/chrome/document/ssb/SsbServiceCallback;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/ssb/ChromeSsbServiceCallback;->mCallback:Lcom/google/android/apps/chrome/document/ssb/SsbServiceCallback;

    return-object v0
.end method

.method public removeListener(Lcom/google/android/apps/chrome/document/ssb/SsbStateListener;)V
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/ssb/ChromeSsbServiceCallback;->mListeners:Lorg/chromium/base/ObserverList;

    invoke-virtual {v0, p1}, Lorg/chromium/base/ObserverList;->removeObserver(Ljava/lang/Object;)Z

    .line 66
    return-void
.end method
