.class Lcom/google/android/apps/chrome/videofling/LockScreenTransportControlV18;
.super Lcom/google/android/apps/chrome/videofling/LockScreenTransportControlV16;
.source "LockScreenTransportControlV18.java"


# instance fields
.field private final mGetPlaybackPositionUpdateListener:Lcom/google/android/apps/chrome/videofling/LockScreenTransportControlV18$GetPlaybackPositionUpdateListener;

.field private final mPlaybackPositionUpdateListener:Lcom/google/android/apps/chrome/videofling/LockScreenTransportControlV18$PlaybackPositionUpdateListener;


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 25
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/videofling/LockScreenTransportControlV16;-><init>(Landroid/content/Context;)V

    .line 26
    new-instance v0, Lcom/google/android/apps/chrome/videofling/LockScreenTransportControlV18$PlaybackPositionUpdateListener;

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/chrome/videofling/LockScreenTransportControlV18$PlaybackPositionUpdateListener;-><init>(Lcom/google/android/apps/chrome/videofling/LockScreenTransportControlV18;Lcom/google/android/apps/chrome/videofling/LockScreenTransportControlV18$1;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/videofling/LockScreenTransportControlV18;->mPlaybackPositionUpdateListener:Lcom/google/android/apps/chrome/videofling/LockScreenTransportControlV18$PlaybackPositionUpdateListener;

    .line 27
    new-instance v0, Lcom/google/android/apps/chrome/videofling/LockScreenTransportControlV18$GetPlaybackPositionUpdateListener;

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/chrome/videofling/LockScreenTransportControlV18$GetPlaybackPositionUpdateListener;-><init>(Lcom/google/android/apps/chrome/videofling/LockScreenTransportControlV18;Lcom/google/android/apps/chrome/videofling/LockScreenTransportControlV18$1;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/videofling/LockScreenTransportControlV18;->mGetPlaybackPositionUpdateListener:Lcom/google/android/apps/chrome/videofling/LockScreenTransportControlV18$GetPlaybackPositionUpdateListener;

    .line 28
    return-void
.end method


# virtual methods
.method protected getTransportControlFlags()I
    .locals 1

    .prologue
    .line 57
    invoke-super {p0}, Lcom/google/android/apps/chrome/videofling/LockScreenTransportControlV16;->getTransportControlFlags()I

    move-result v0

    or-int/lit16 v0, v0, 0x100

    return v0
.end method

.method protected register()V
    .locals 2

    .prologue
    .line 32
    invoke-super {p0}, Lcom/google/android/apps/chrome/videofling/LockScreenTransportControlV16;->register()V

    .line 33
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/videofling/LockScreenTransportControlV18;->getRemoteControlClient()Landroid/media/RemoteControlClient;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/videofling/LockScreenTransportControlV18;->mPlaybackPositionUpdateListener:Lcom/google/android/apps/chrome/videofling/LockScreenTransportControlV18$PlaybackPositionUpdateListener;

    invoke-virtual {v0, v1}, Landroid/media/RemoteControlClient;->setPlaybackPositionUpdateListener(Landroid/media/RemoteControlClient$OnPlaybackPositionUpdateListener;)V

    .line 34
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/videofling/LockScreenTransportControlV18;->getRemoteControlClient()Landroid/media/RemoteControlClient;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/videofling/LockScreenTransportControlV18;->mGetPlaybackPositionUpdateListener:Lcom/google/android/apps/chrome/videofling/LockScreenTransportControlV18$GetPlaybackPositionUpdateListener;

    invoke-virtual {v0, v1}, Landroid/media/RemoteControlClient;->setOnGetPlaybackPositionListener(Landroid/media/RemoteControlClient$OnGetPlaybackPositionListener;)V

    .line 36
    return-void
.end method

.method protected unregister()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 40
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/videofling/LockScreenTransportControlV18;->getRemoteControlClient()Landroid/media/RemoteControlClient;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/media/RemoteControlClient;->setOnGetPlaybackPositionListener(Landroid/media/RemoteControlClient$OnGetPlaybackPositionListener;)V

    .line 41
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/videofling/LockScreenTransportControlV18;->getRemoteControlClient()Landroid/media/RemoteControlClient;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/media/RemoteControlClient;->setPlaybackPositionUpdateListener(Landroid/media/RemoteControlClient$OnPlaybackPositionUpdateListener;)V

    .line 42
    invoke-super {p0}, Lcom/google/android/apps/chrome/videofling/LockScreenTransportControlV16;->unregister()V

    .line 43
    return-void
.end method

.method protected updatePlaybackState(I)V
    .locals 4

    .prologue
    .line 47
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/videofling/LockScreenTransportControlV18;->getVideoInfo()Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo;

    move-result-object v0

    .line 48
    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/videofling/LockScreenTransportControlV18;->getRemoteControlClient()Landroid/media/RemoteControlClient;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 49
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/videofling/LockScreenTransportControlV18;->getRemoteControlClient()Landroid/media/RemoteControlClient;

    move-result-object v1

    iget v0, v0, Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo;->currentTimeMillis:I

    int-to-long v2, v0

    const/high16 v0, 0x3f800000    # 1.0f

    invoke-virtual {v1, p1, v2, v3, v0}, Landroid/media/RemoteControlClient;->setPlaybackState(IJF)V

    .line 53
    :goto_0
    return-void

    .line 51
    :cond_0
    invoke-super {p0, p1}, Lcom/google/android/apps/chrome/videofling/LockScreenTransportControlV16;->updatePlaybackState(I)V

    goto :goto_0
.end method
