.class Lcom/google/android/apps/chrome/preferences/website/WebsitePreference;
.super Landroid/preference/Preference;
.source "WebsitePreference.java"


# static fields
.field private static final LOCATION_ALLOWED:[I

.field private static final LOCATION_DENIED:[I

.field private static final MIDI_ALLOWED:[I

.field private static final MIDI_DENIED:[I

.field private static final POPUPS_ALLOWED:[I

.field private static final POPUPS_DENIED:[I

.field private static final PROTECTED_MEDIA_IDENTIFIER_ALLOWED:[I

.field private static final PROTECTED_MEDIA_IDENTIFIER_DENIED:[I


# instance fields
.field private final mSite:Lcom/google/android/apps/chrome/preferences/website/Website;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 19
    new-array v0, v3, [I

    sget v1, Lcom/google/android/apps/chrome/R$attr;->location_allowed:I

    aput v1, v0, v2

    sput-object v0, Lcom/google/android/apps/chrome/preferences/website/WebsitePreference;->LOCATION_ALLOWED:[I

    .line 22
    new-array v0, v2, [I

    sput-object v0, Lcom/google/android/apps/chrome/preferences/website/WebsitePreference;->LOCATION_DENIED:[I

    .line 23
    new-array v0, v3, [I

    sget v1, Lcom/google/android/apps/chrome/R$attr;->midi_allowed:I

    aput v1, v0, v2

    sput-object v0, Lcom/google/android/apps/chrome/preferences/website/WebsitePreference;->MIDI_ALLOWED:[I

    .line 26
    new-array v0, v2, [I

    sput-object v0, Lcom/google/android/apps/chrome/preferences/website/WebsitePreference;->MIDI_DENIED:[I

    .line 27
    new-array v0, v3, [I

    sget v1, Lcom/google/android/apps/chrome/R$attr;->popups_allowed:I

    aput v1, v0, v2

    sput-object v0, Lcom/google/android/apps/chrome/preferences/website/WebsitePreference;->POPUPS_ALLOWED:[I

    .line 30
    new-array v0, v2, [I

    sput-object v0, Lcom/google/android/apps/chrome/preferences/website/WebsitePreference;->POPUPS_DENIED:[I

    .line 31
    new-array v0, v3, [I

    sget v1, Lcom/google/android/apps/chrome/R$attr;->protected_media_identifier_allowed:I

    aput v1, v0, v2

    sput-object v0, Lcom/google/android/apps/chrome/preferences/website/WebsitePreference;->PROTECTED_MEDIA_IDENTIFIER_ALLOWED:[I

    .line 34
    new-array v0, v2, [I

    sput-object v0, Lcom/google/android/apps/chrome/preferences/website/WebsitePreference;->PROTECTED_MEDIA_IDENTIFIER_DENIED:[I

    return-void
.end method

.method constructor <init>(Landroid/content/Context;Lcom/google/android/apps/chrome/preferences/website/Website;)V
    .locals 1

    .prologue
    .line 37
    invoke-direct {p0, p1}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    .line 38
    iput-object p2, p0, Lcom/google/android/apps/chrome/preferences/website/WebsitePreference;->mSite:Lcom/google/android/apps/chrome/preferences/website/Website;

    .line 39
    sget v0, Lcom/google/android/apps/chrome/R$layout;->website_features:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/preferences/website/WebsitePreference;->setWidgetLayoutResource(I)V

    .line 40
    invoke-direct {p0}, Lcom/google/android/apps/chrome/preferences/website/WebsitePreference;->refresh()V

    .line 41
    return-void
.end method

.method private static determineMediaIconToDisplay(I)I
    .locals 1

    .prologue
    .line 160
    packed-switch p0, :pswitch_data_0

    .line 172
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 163
    :pswitch_0
    const/4 v0, 0x1

    goto :goto_0

    .line 166
    :pswitch_1
    const/4 v0, 0x2

    goto :goto_0

    .line 168
    :pswitch_2
    const/4 v0, 0x3

    goto :goto_0

    .line 170
    :pswitch_3
    const/4 v0, 0x4

    goto :goto_0

    .line 160
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_1
        :pswitch_3
    .end packed-switch
.end method

.method private static getStorageUsageLevel(J)I
    .locals 6

    .prologue
    .line 183
    long-to-float v0, p0

    const/high16 v1, 0x49800000    # 1048576.0f

    div-float/2addr v0, v1

    .line 184
    float-to-double v2, v0

    const-wide v4, 0x3fb999999999999aL    # 0.1

    cmpg-double v1, v2, v4

    if-gtz v1, :cond_0

    .line 185
    const/4 v0, 0x0

    .line 189
    :goto_0
    return v0

    .line 186
    :cond_0
    const/high16 v1, 0x40a00000    # 5.0f

    cmpg-float v0, v0, v1

    if-gtz v0, :cond_1

    .line 187
    const/4 v0, 0x1

    goto :goto_0

    .line 189
    :cond_1
    const/4 v0, 0x2

    goto :goto_0
.end method

.method private refresh()V
    .locals 4

    .prologue
    .line 48
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/website/WebsitePreference;->mSite:Lcom/google/android/apps/chrome/preferences/website/Website;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/preferences/website/Website;->getTitle()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/preferences/website/WebsitePreference;->setTitle(Ljava/lang/CharSequence;)V

    .line 49
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/website/WebsitePreference;->mSite:Lcom/google/android/apps/chrome/preferences/website/Website;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/preferences/website/Website;->getSummary()Ljava/lang/String;

    move-result-object v0

    .line 50
    if-eqz v0, :cond_0

    .line 51
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/website/WebsitePreference;->getContext()Landroid/content/Context;

    move-result-object v1

    sget v2, Lcom/google/android/apps/chrome/R$string;->website_settings_embedded_in:I

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/preferences/website/WebsitePreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 54
    :cond_0
    return-void
.end method

.method static sizeValueToString(Landroid/content/Context;J)Ljava/lang/String;
    .locals 9

    .prologue
    const/high16 v6, 0x44800000    # 1024.0f

    const/4 v1, 0x0

    .line 193
    const/4 v0, 0x5

    new-array v2, v0, [Ljava/lang/String;

    sget v0, Lcom/google/android/apps/chrome/R$string;->origin_settings_storage_bytes:I

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v1

    const/4 v0, 0x1

    sget v3, Lcom/google/android/apps/chrome/R$string;->origin_settings_storage_kbytes:I

    invoke-virtual {p0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    const/4 v0, 0x2

    sget v3, Lcom/google/android/apps/chrome/R$string;->origin_settings_storage_mbytes:I

    invoke-virtual {p0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    const/4 v0, 0x3

    sget v3, Lcom/google/android/apps/chrome/R$string;->origin_settings_storage_gbytes:I

    invoke-virtual {p0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    const/4 v0, 0x4

    sget v3, Lcom/google/android/apps/chrome/R$string;->origin_settings_storage_tbytes:I

    invoke-virtual {p0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    .line 201
    const-wide/16 v4, 0x0

    cmp-long v0, p1, v4

    if-gtz v0, :cond_0

    .line 202
    new-instance v0, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "0"

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    aget-object v1, v2, v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 214
    :goto_0
    return-object v0

    .line 205
    :cond_0
    long-to-float v0, p1

    .line 207
    :goto_1
    array-length v3, v2

    if-ge v1, v3, :cond_1

    .line 208
    cmpg-float v3, v0, v6

    if-ltz v3, :cond_1

    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-eq v1, v3, :cond_1

    .line 209
    div-float/2addr v0, v6

    .line 207
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 213
    :cond_1
    new-instance v3, Ljava/text/DecimalFormat;

    const-string/jumbo v4, "#.##"

    invoke-direct {v3, v4}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;)V

    .line 214
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    float-to-double v6, v0

    invoke-virtual {v3, v6, v7}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    aget-object v1, v2, v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public compareTo(Landroid/preference/Preference;)I
    .locals 2

    .prologue
    .line 58
    instance-of v0, p1, Lcom/google/android/apps/chrome/preferences/website/WebsitePreference;

    if-nez v0, :cond_0

    .line 59
    const/4 v0, 0x1

    .line 62
    :goto_0
    return v0

    .line 61
    :cond_0
    check-cast p1, Lcom/google/android/apps/chrome/preferences/website/WebsitePreference;

    .line 62
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/website/WebsitePreference;->mSite:Lcom/google/android/apps/chrome/preferences/website/Website;

    iget-object v1, p1, Lcom/google/android/apps/chrome/preferences/website/WebsitePreference;->mSite:Lcom/google/android/apps/chrome/preferences/website/Website;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/preferences/website/Website;->compareByAddressTo(Lcom/google/android/apps/chrome/preferences/website/Website;)I

    move-result v0

    goto :goto_0
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 16
    check-cast p1, Landroid/preference/Preference;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/preferences/website/WebsitePreference;->compareTo(Landroid/preference/Preference;)I

    move-result v0

    return v0
.end method

.method protected onBindView(Landroid/view/View;)V
    .locals 9

    .prologue
    const/16 v8, 0x8

    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 67
    invoke-super {p0, p1}, Landroid/preference/Preference;->onBindView(Landroid/view/View;)V

    .line 69
    sget v0, Lcom/google/android/apps/chrome/R$id;->usage_icon:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 70
    invoke-virtual {v0, v8}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 71
    iget-object v1, p0, Lcom/google/android/apps/chrome/preferences/website/WebsitePreference;->mSite:Lcom/google/android/apps/chrome/preferences/website/Website;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/preferences/website/Website;->getTotalUsage()J

    move-result-wide v2

    .line 72
    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-lez v1, :cond_0

    .line 73
    invoke-static {v2, v3}, Lcom/google/android/apps/chrome/preferences/website/WebsitePreference;->getStorageUsageLevel(J)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageLevel(I)V

    .line 74
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/website/WebsitePreference;->getContext()Landroid/content/Context;

    move-result-object v1

    sget v4, Lcom/google/android/apps/chrome/R$string;->origin_settings_storage_usage:I

    invoke-virtual {v1, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-array v4, v6, [Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/website/WebsitePreference;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5, v2, v3}, Lcom/google/android/apps/chrome/preferences/website/WebsitePreference;->sizeValueToString(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v4, v7

    invoke-static {v1, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 77
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 78
    invoke-virtual {v0, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 81
    :cond_0
    sget v0, Lcom/google/android/apps/chrome/R$id;->location_icon:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 82
    invoke-virtual {v0, v8}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 83
    iget-object v1, p0, Lcom/google/android/apps/chrome/preferences/website/WebsitePreference;->mSite:Lcom/google/android/apps/chrome/preferences/website/Website;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/preferences/website/Website;->isGeolocationAccessAllowed()Ljava/lang/Boolean;

    move-result-object v1

    .line 84
    if-eqz v1, :cond_1

    .line 85
    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 86
    sget-object v1, Lcom/google/android/apps/chrome/preferences/website/WebsitePreference;->LOCATION_ALLOWED:[I

    invoke-virtual {v0, v1, v6}, Landroid/widget/ImageView;->setImageState([IZ)V

    .line 87
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/website/WebsitePreference;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/apps/chrome/R$string;->accessibility_website_location_allowed:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 94
    :goto_0
    invoke-virtual {v0, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 97
    :cond_1
    sget v0, Lcom/google/android/apps/chrome/R$id;->midi_icon:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 98
    invoke-virtual {v0, v8}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 99
    iget-object v1, p0, Lcom/google/android/apps/chrome/preferences/website/WebsitePreference;->mSite:Lcom/google/android/apps/chrome/preferences/website/Website;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/preferences/website/Website;->isMidiAccessAllowed()Ljava/lang/Boolean;

    move-result-object v1

    .line 100
    if-eqz v1, :cond_2

    .line 101
    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_7

    .line 102
    sget-object v1, Lcom/google/android/apps/chrome/preferences/website/WebsitePreference;->MIDI_ALLOWED:[I

    invoke-virtual {v0, v1, v6}, Landroid/widget/ImageView;->setImageState([IZ)V

    .line 103
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/website/WebsitePreference;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/apps/chrome/R$string;->accessibility_website_midi_allowed:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 110
    :goto_1
    invoke-virtual {v0, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 113
    :cond_2
    sget v0, Lcom/google/android/apps/chrome/R$id;->popups_icon:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 114
    invoke-virtual {v0, v8}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 115
    iget-object v1, p0, Lcom/google/android/apps/chrome/preferences/website/WebsitePreference;->mSite:Lcom/google/android/apps/chrome/preferences/website/Website;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/preferences/website/Website;->isPopupExceptionAllowed()Ljava/lang/Boolean;

    move-result-object v1

    .line 116
    if-eqz v1, :cond_3

    .line 117
    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_8

    .line 118
    sget-object v1, Lcom/google/android/apps/chrome/preferences/website/WebsitePreference;->POPUPS_ALLOWED:[I

    invoke-virtual {v0, v1, v6}, Landroid/widget/ImageView;->setImageState([IZ)V

    .line 119
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/website/WebsitePreference;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/apps/chrome/R$string;->accessibility_website_popups_allowed:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 126
    :goto_2
    invoke-virtual {v0, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 129
    :cond_3
    sget v0, Lcom/google/android/apps/chrome/R$id;->protected_media_identifier_icon:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 131
    invoke-virtual {v0, v8}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 132
    iget-object v1, p0, Lcom/google/android/apps/chrome/preferences/website/WebsitePreference;->mSite:Lcom/google/android/apps/chrome/preferences/website/Website;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/preferences/website/Website;->isProtectedMediaIdentifierAccessAllowed()Ljava/lang/Boolean;

    move-result-object v1

    .line 133
    if-eqz v1, :cond_4

    .line 134
    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_9

    sget-object v1, Lcom/google/android/apps/chrome/preferences/website/WebsitePreference;->PROTECTED_MEDIA_IDENTIFIER_ALLOWED:[I

    :goto_3
    invoke-virtual {v0, v1, v6}, Landroid/widget/ImageView;->setImageState([IZ)V

    .line 137
    invoke-virtual {v0, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 140
    :cond_4
    sget v0, Lcom/google/android/apps/chrome/R$id;->voice_and_video_capture_icon:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 142
    invoke-virtual {v0, v8}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 143
    iget-object v1, p0, Lcom/google/android/apps/chrome/preferences/website/WebsitePreference;->mSite:Lcom/google/android/apps/chrome/preferences/website/Website;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/preferences/website/Website;->getMediaAccessType()I

    move-result v1

    invoke-static {v1}, Lcom/google/android/apps/chrome/preferences/website/WebsitePreference;->determineMediaIconToDisplay(I)I

    move-result v1

    .line 144
    if-lez v1, :cond_5

    .line 145
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageLevel(I)V

    .line 146
    invoke-virtual {v0, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 148
    :cond_5
    return-void

    .line 90
    :cond_6
    sget-object v1, Lcom/google/android/apps/chrome/preferences/website/WebsitePreference;->LOCATION_DENIED:[I

    invoke-virtual {v0, v1, v6}, Landroid/widget/ImageView;->setImageState([IZ)V

    .line 91
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/website/WebsitePreference;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/apps/chrome/R$string;->accessibility_website_location_denied:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 106
    :cond_7
    sget-object v1, Lcom/google/android/apps/chrome/preferences/website/WebsitePreference;->MIDI_DENIED:[I

    invoke-virtual {v0, v1, v6}, Landroid/widget/ImageView;->setImageState([IZ)V

    .line 107
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/website/WebsitePreference;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/apps/chrome/R$string;->accessibility_website_midi_denied:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 122
    :cond_8
    sget-object v1, Lcom/google/android/apps/chrome/preferences/website/WebsitePreference;->POPUPS_DENIED:[I

    invoke-virtual {v0, v1, v6}, Landroid/widget/ImageView;->setImageState([IZ)V

    .line 123
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/website/WebsitePreference;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/apps/chrome/R$string;->accessibility_website_popups_denied:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_2

    .line 134
    :cond_9
    sget-object v1, Lcom/google/android/apps/chrome/preferences/website/WebsitePreference;->PROTECTED_MEDIA_IDENTIFIER_DENIED:[I

    goto :goto_3
.end method

.method public putSiteIntoExtras(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 44
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/website/WebsitePreference;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/preferences/website/WebsitePreference;->mSite:Lcom/google/android/apps/chrome/preferences/website/Website;

    invoke-virtual {v0, p1, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 45
    return-void
.end method
