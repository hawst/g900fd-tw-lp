.class Lcom/google/android/apps/chrome/tab/ChromeTab$ChromeTabChromeContextMenuItemDelegate;
.super Lorg/chromium/chrome/browser/Tab$TabChromeContextMenuItemDelegate;
.source "ChromeTab.java"


# instance fields
.field private mIsImage:Z

.field private mIsVideo:Z

.field final synthetic this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/chrome/tab/ChromeTab;)V
    .locals 0

    .prologue
    .line 1030
    iput-object p1, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$ChromeTabChromeContextMenuItemDelegate;->this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;

    invoke-direct {p0, p1}, Lorg/chromium/chrome/browser/Tab$TabChromeContextMenuItemDelegate;-><init>(Lorg/chromium/chrome/browser/Tab;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/chrome/tab/ChromeTab;Lcom/google/android/apps/chrome/tab/ChromeTab$1;)V
    .locals 0

    .prologue
    .line 1030
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/tab/ChromeTab$ChromeTabChromeContextMenuItemDelegate;-><init>(Lcom/google/android/apps/chrome/tab/ChromeTab;)V

    return-void
.end method


# virtual methods
.method public canLoadOriginalImage()Z
    .locals 1

    .prologue
    .line 1046
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$ChromeTabChromeContextMenuItemDelegate;->this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;

    # getter for: Lcom/google/android/apps/chrome/tab/ChromeTab;->mUsedSpdyProxy:Z
    invoke-static {v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->access$2900(Lcom/google/android/apps/chrome/tab/ChromeTab;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$ChromeTabChromeContextMenuItemDelegate;->this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;

    # getter for: Lcom/google/android/apps/chrome/tab/ChromeTab;->mUsedSpdyProxyWithPassthrough:Z
    invoke-static {v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->access$3000(Lcom/google/android/apps/chrome/tab/ChromeTab;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isIncognitoSupported()Z
    .locals 1

    .prologue
    .line 1041
    invoke-static {}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->getInstance()Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->isIncognitoModeEnabled()Z

    move-result v0

    return v0
.end method

.method public onOpenImageInNewTab(Ljava/lang/String;Lorg/chromium/content_public/common/Referrer;)V
    .locals 5

    .prologue
    .line 1107
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$ChromeTabChromeContextMenuItemDelegate;->this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/tab/ChromeTab;->isSpdyProxyEnabledForUrl(Ljava/lang/String;)Z

    move-result v0

    .line 1108
    invoke-static {v0}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->recordContextMenuOpenImageInNewTab(Z)V

    .line 1110
    new-instance v1, Lorg/chromium/content_public/browser/LoadUrlParams;

    invoke-direct {v1, p1}, Lorg/chromium/content_public/browser/LoadUrlParams;-><init>(Ljava/lang/String;)V

    .line 1111
    if-eqz v0, :cond_0

    const-string/jumbo v0, "X-PSA-Client-Options: v=1,m=1\nCache-Control: no-cache"

    :goto_0
    invoke-virtual {v1, v0}, Lorg/chromium/content_public/browser/LoadUrlParams;->setVerbatimHeaders(Ljava/lang/String;)V

    .line 1112
    invoke-virtual {v1, p2}, Lorg/chromium/content_public/browser/LoadUrlParams;->setReferrer(Lorg/chromium/content_public/common/Referrer;)V

    .line 1113
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$ChromeTabChromeContextMenuItemDelegate;->this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;

    # getter for: Lcom/google/android/apps/chrome/tab/ChromeTab;->mActivity:Lcom/google/android/apps/chrome/ChromeActivity;
    invoke-static {v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->access$900(Lcom/google/android/apps/chrome/tab/ChromeTab;)Lcom/google/android/apps/chrome/ChromeActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ChromeActivity;->getTabModelSelector()Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

    move-result-object v0

    sget-object v2, Lorg/chromium/chrome/browser/tabmodel/TabModel$TabLaunchType;->FROM_LONGPRESS_BACKGROUND:Lorg/chromium/chrome/browser/tabmodel/TabModel$TabLaunchType;

    iget-object v3, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$ChromeTabChromeContextMenuItemDelegate;->this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tab/ChromeTab$ChromeTabChromeContextMenuItemDelegate;->isIncognito()Z

    move-result v4

    invoke-interface {v0, v1, v2, v3, v4}, Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;->openNewTab(Lorg/chromium/content_public/browser/LoadUrlParams;Lorg/chromium/chrome/browser/tabmodel/TabModel$TabLaunchType;Lorg/chromium/chrome/browser/Tab;Z)Lorg/chromium/chrome/browser/Tab;

    .line 1115
    return-void

    .line 1111
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onOpenImageUrl(Ljava/lang/String;Lorg/chromium/content_public/common/Referrer;)V
    .locals 2

    .prologue
    .line 1098
    invoke-static {}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->recordContextMenuOpenImageUrl()V

    .line 1099
    new-instance v0, Lorg/chromium/content_public/browser/LoadUrlParams;

    invoke-direct {v0, p1}, Lorg/chromium/content_public/browser/LoadUrlParams;-><init>(Ljava/lang/String;)V

    .line 1100
    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lorg/chromium/content_public/browser/LoadUrlParams;->setTransitionType(I)V

    .line 1101
    invoke-virtual {v0, p2}, Lorg/chromium/content_public/browser/LoadUrlParams;->setReferrer(Lorg/chromium/content_public/common/Referrer;)V

    .line 1102
    iget-object v1, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$ChromeTabChromeContextMenuItemDelegate;->this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->loadUrl(Lorg/chromium/content_public/browser/LoadUrlParams;)I

    .line 1103
    return-void
.end method

.method public onOpenInNewIncognitoTab(Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 1091
    invoke-static {}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->recordContextMenuOpenInNewIncognitoTab()V

    .line 1092
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$ChromeTabChromeContextMenuItemDelegate;->this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;

    # getter for: Lcom/google/android/apps/chrome/tab/ChromeTab;->mActivity:Lcom/google/android/apps/chrome/ChromeActivity;
    invoke-static {v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->access$900(Lcom/google/android/apps/chrome/tab/ChromeTab;)Lcom/google/android/apps/chrome/ChromeActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ChromeActivity;->getTabModelSelector()Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

    move-result-object v0

    new-instance v1, Lorg/chromium/content_public/browser/LoadUrlParams;

    invoke-direct {v1, p1}, Lorg/chromium/content_public/browser/LoadUrlParams;-><init>(Ljava/lang/String;)V

    sget-object v2, Lorg/chromium/chrome/browser/tabmodel/TabModel$TabLaunchType;->FROM_LONGPRESS_FOREGROUND:Lorg/chromium/chrome/browser/tabmodel/TabModel$TabLaunchType;

    iget-object v3, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$ChromeTabChromeContextMenuItemDelegate;->this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;

    const/4 v4, 0x1

    invoke-interface {v0, v1, v2, v3, v4}, Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;->openNewTab(Lorg/chromium/content_public/browser/LoadUrlParams;Lorg/chromium/chrome/browser/tabmodel/TabModel$TabLaunchType;Lorg/chromium/chrome/browser/Tab;Z)Lorg/chromium/chrome/browser/Tab;

    .line 1094
    return-void
.end method

.method public onOpenInNewTab(Ljava/lang/String;Lorg/chromium/content_public/common/Referrer;)V
    .locals 5

    .prologue
    .line 1082
    invoke-static {}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->recordContextMenuOpenInNewTab()V

    .line 1083
    new-instance v0, Lorg/chromium/content_public/browser/LoadUrlParams;

    invoke-direct {v0, p1}, Lorg/chromium/content_public/browser/LoadUrlParams;-><init>(Ljava/lang/String;)V

    .line 1084
    invoke-virtual {v0, p2}, Lorg/chromium/content_public/browser/LoadUrlParams;->setReferrer(Lorg/chromium/content_public/common/Referrer;)V

    .line 1085
    iget-object v1, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$ChromeTabChromeContextMenuItemDelegate;->this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;

    # getter for: Lcom/google/android/apps/chrome/tab/ChromeTab;->mActivity:Lcom/google/android/apps/chrome/ChromeActivity;
    invoke-static {v1}, Lcom/google/android/apps/chrome/tab/ChromeTab;->access$900(Lcom/google/android/apps/chrome/tab/ChromeTab;)Lcom/google/android/apps/chrome/ChromeActivity;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/ChromeActivity;->getTabModelSelector()Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

    move-result-object v1

    sget-object v2, Lorg/chromium/chrome/browser/tabmodel/TabModel$TabLaunchType;->FROM_LONGPRESS_BACKGROUND:Lorg/chromium/chrome/browser/tabmodel/TabModel$TabLaunchType;

    iget-object v3, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$ChromeTabChromeContextMenuItemDelegate;->this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tab/ChromeTab$ChromeTabChromeContextMenuItemDelegate;->isIncognito()Z

    move-result v4

    invoke-interface {v1, v0, v2, v3, v4}, Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;->openNewTab(Lorg/chromium/content_public/browser/LoadUrlParams;Lorg/chromium/chrome/browser/tabmodel/TabModel$TabLaunchType;Lorg/chromium/chrome/browser/Tab;Z)Lorg/chromium/chrome/browser/Tab;

    .line 1087
    return-void
.end method

.method public onSaveImageToClipboard(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1076
    invoke-static {}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->recordContextMenuSaveImage()V

    .line 1077
    invoke-super {p0, p1}, Lorg/chromium/chrome/browser/Tab$TabChromeContextMenuItemDelegate;->onSaveImageToClipboard(Ljava/lang/String;)V

    .line 1078
    return-void
.end method

.method public onSaveToClipboard(Ljava/lang/String;Z)V
    .locals 0

    .prologue
    .line 1066
    if-eqz p2, :cond_0

    .line 1067
    invoke-static {}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->recordContextMenuCopyLinkAddress()V

    .line 1071
    :goto_0
    invoke-super {p0, p1, p2}, Lorg/chromium/chrome/browser/Tab$TabChromeContextMenuItemDelegate;->onSaveToClipboard(Ljava/lang/String;Z)V

    .line 1072
    return-void

    .line 1069
    :cond_0
    invoke-static {}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->recordContextMenuCopyLinkText()V

    goto :goto_0
.end method

.method public onSearchByImageInNewTab()V
    .locals 1

    .prologue
    .line 1119
    invoke-static {}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->recordContextMenuSearchByImage()V

    .line 1120
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$ChromeTabChromeContextMenuItemDelegate;->this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->triggerSearchByImage()V

    .line 1121
    return-void
.end method

.method public setParamsInfo(ZZ)V
    .locals 0

    .prologue
    .line 1035
    iput-boolean p1, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$ChromeTabChromeContextMenuItemDelegate;->mIsImage:Z

    .line 1036
    iput-boolean p2, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$ChromeTabChromeContextMenuItemDelegate;->mIsVideo:Z

    .line 1037
    return-void
.end method

.method public startDownload(Ljava/lang/String;Z)Z
    .locals 1

    .prologue
    .line 1051
    if-eqz p2, :cond_0

    .line 1052
    invoke-static {}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->recordContextMenuDownloadLink()V

    .line 1053
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$ChromeTabChromeContextMenuItemDelegate;->this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/tab/ChromeTab;->shouldInterceptContextMenuDownload(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1054
    const/4 v0, 0x0

    .line 1061
    :goto_0
    return v0

    .line 1056
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$ChromeTabChromeContextMenuItemDelegate;->mIsImage:Z

    if-eqz v0, :cond_2

    .line 1057
    invoke-static {}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->recordContextMenuDownloadImage()V

    .line 1061
    :cond_1
    :goto_1
    const/4 v0, 0x1

    goto :goto_0

    .line 1058
    :cond_2
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$ChromeTabChromeContextMenuItemDelegate;->mIsVideo:Z

    if-eqz v0, :cond_1

    .line 1059
    invoke-static {}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->recordContextMenuDownloadVideo()V

    goto :goto_1
.end method
