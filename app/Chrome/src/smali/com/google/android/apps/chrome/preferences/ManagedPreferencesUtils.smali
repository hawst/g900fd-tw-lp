.class public Lcom/google/android/apps/chrome/preferences/ManagedPreferencesUtils;
.super Ljava/lang/Object;
.source "ManagedPreferencesUtils.java"


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    return-void
.end method

.method public static disableIfManagedByPolicy(Landroid/content/Context;JLandroid/preference/PreferenceScreen;)V
    .locals 1

    .prologue
    .line 209
    invoke-static {p0, p1, p2}, Lcom/google/android/apps/chrome/preferences/ManagedPreferencesUtils;->isIdControlledByPolicy(Landroid/content/Context;J)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 210
    invoke-static {}, Lcom/google/android/apps/chrome/preferences/ManagedPreferencesUtils;->getManagedByEnterpriseIconId()I

    move-result v0

    invoke-virtual {p3, v0}, Landroid/preference/PreferenceScreen;->setIcon(I)V

    .line 212
    :cond_0
    invoke-static {p0, p1, p2}, Lcom/google/android/apps/chrome/preferences/ManagedPreferencesUtils;->isIdDisabledByPolicy(Landroid/content/Context;J)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 213
    const/4 v0, 0x0

    invoke-virtual {p3, v0}, Landroid/preference/PreferenceScreen;->setEnabled(Z)V

    .line 215
    :cond_1
    return-void
.end method

.method private static disableView(Landroid/view/View;)V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 221
    invoke-virtual {p0, v0}, Landroid/view/View;->setEnabled(Z)V

    .line 222
    instance-of v1, p0, Landroid/view/ViewGroup;

    if-eqz v1, :cond_0

    .line 223
    check-cast p0, Landroid/view/ViewGroup;

    .line 224
    :goto_0
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 225
    invoke-virtual {p0, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/chrome/preferences/ManagedPreferencesUtils;->disableView(Landroid/view/View;)V

    .line 224
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 228
    :cond_0
    return-void
.end method

.method public static getManagedByEnterpriseIcon(Landroid/content/Context;)Landroid/graphics/Bitmap;
    .locals 2

    .prologue
    .line 156
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {}, Lcom/google/android/apps/chrome/preferences/ManagedPreferencesUtils;->getManagedByEnterpriseIconId()I

    move-result v1

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public static getManagedByEnterpriseIconId()I
    .locals 1

    .prologue
    .line 149
    sget v0, Lcom/google/android/apps/chrome/R$drawable;->controlled_setting_mandatory:I

    return v0
.end method

.method public static isIdControlledByPolicy(Landroid/content/Context;J)Z
    .locals 3

    .prologue
    .line 39
    sget v0, Lcom/google/android/apps/chrome/R$id;->search_engine:I

    int-to-long v0, v0

    cmp-long v0, p1, v0

    if-nez v0, :cond_0

    .line 40
    invoke-static {}, Lorg/chromium/chrome/browser/search_engines/TemplateUrlService;->getInstance()Lorg/chromium/chrome/browser/search_engines/TemplateUrlService;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/search_engines/TemplateUrlService;->isSearchProviderManaged()Z

    move-result v0

    .line 50
    :goto_0
    return v0

    .line 41
    :cond_0
    sget v0, Lcom/google/android/apps/chrome/R$id;->sync_account:I

    int-to-long v0, v0

    cmp-long v0, p1, v0

    if-nez v0, :cond_1

    .line 42
    invoke-static {p0}, Lorg/chromium/chrome/browser/signin/SigninManager;->get(Landroid/content/Context;)Lorg/chromium/chrome/browser/signin/SigninManager;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/signin/SigninManager;->isSigninDisabledByPolicy()Z

    move-result v0

    goto :goto_0

    .line 43
    :cond_1
    sget v0, Lcom/google/android/apps/chrome/R$id;->autofill_settings:I

    int-to-long v0, v0

    cmp-long v0, p1, v0

    if-nez v0, :cond_2

    .line 44
    invoke-static {}, Lorg/chromium/chrome/browser/autofill/PersonalDataManager;->isAutofillManaged()Z

    move-result v0

    goto :goto_0

    .line 45
    :cond_2
    sget v0, Lcom/google/android/apps/chrome/R$id;->manage_saved_passwords:I

    int-to-long v0, v0

    cmp-long v0, p1, v0

    if-nez v0, :cond_3

    .line 46
    invoke-static {}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->getInstance()Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->isRememberPasswordsManaged()Z

    move-result v0

    goto :goto_0

    .line 47
    :cond_3
    sget v0, Lcom/google/android/apps/chrome/R$id;->reduce_data_usage_settings:I

    int-to-long v0, v0

    cmp-long v0, p1, v0

    if-nez v0, :cond_4

    .line 48
    invoke-static {}, Lorg/chromium/chrome/browser/net/spdyproxy/DataReductionProxySettings;->getInstance()Lorg/chromium/chrome/browser/net/spdyproxy/DataReductionProxySettings;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/net/spdyproxy/DataReductionProxySettings;->isDataReductionProxyManaged()Z

    move-result v0

    goto :goto_0

    .line 50
    :cond_4
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isIdDisabledByPolicy(Landroid/content/Context;J)Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 61
    invoke-static {p0, p1, p2}, Lcom/google/android/apps/chrome/preferences/ManagedPreferencesUtils;->isIdControlledByPolicy(Landroid/content/Context;J)Z

    move-result v1

    if-nez v1, :cond_1

    .line 76
    :cond_0
    :goto_0
    return v0

    .line 66
    :cond_1
    sget v1, Lcom/google/android/apps/chrome/R$id;->manage_saved_passwords:I

    int-to-long v2, v1

    cmp-long v1, p1, v2

    if-nez v1, :cond_2

    invoke-static {}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->getInstance()Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->isRememberPasswordsEnabled()Z

    move-result v1

    if-nez v1, :cond_0

    .line 71
    :cond_2
    sget v1, Lcom/google/android/apps/chrome/R$id;->reduce_data_usage_settings:I

    int-to-long v2, v1

    cmp-long v1, p1, v2

    if-nez v1, :cond_3

    invoke-static {}, Lorg/chromium/chrome/browser/net/spdyproxy/DataReductionProxySettings;->getInstance()Lorg/chromium/chrome/browser/net/spdyproxy/DataReductionProxySettings;

    move-result-object v1

    invoke-virtual {v1}, Lorg/chromium/chrome/browser/net/spdyproxy/DataReductionProxySettings;->isDataReductionProxyEnabled()Z

    move-result v1

    if-nez v1, :cond_0

    .line 76
    :cond_3
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static isPreferenceDisabledByPolicy(Landroid/preference/Preference;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 86
    invoke-static {}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->getInstance()Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;

    move-result-object v1

    .line 87
    invoke-virtual {p0}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v2

    .line 89
    if-nez v2, :cond_1

    .line 128
    :cond_0
    :goto_0
    return v0

    .line 93
    :cond_1
    const-string/jumbo v3, "navigation_error"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 94
    invoke-virtual {v1}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->isResolveNavigationErrorManaged()Z

    move-result v0

    goto :goto_0

    .line 97
    :cond_2
    const-string/jumbo v3, "search_suggestions"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 98
    invoke-virtual {v1}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->isSearchSuggestManaged()Z

    move-result v0

    goto :goto_0

    .line 101
    :cond_3
    const-string/jumbo v3, "network_predictions"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_4

    const-string/jumbo v3, "network_predictions_no_cellular"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 103
    :cond_4
    invoke-virtual {v1}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->isNetworkPredictionManaged()Z

    move-result v0

    goto :goto_0

    .line 106
    :cond_5
    const-string/jumbo v3, "crash_dump_upload"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_6

    const-string/jumbo v3, "crash_dump_upload_no_cellular"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 108
    :cond_6
    invoke-virtual {v1}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->isCrashReportManaged()Z

    move-result v0

    goto :goto_0

    .line 111
    :cond_7
    const-string/jumbo v3, "accept_cookies"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_8

    .line 112
    invoke-virtual {v1}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->isAcceptCookiesManaged()Z

    move-result v0

    goto :goto_0

    .line 115
    :cond_8
    const-string/jumbo v3, "enable_javascript"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_9

    .line 116
    invoke-virtual {v1}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->javaScriptManaged()Z

    move-result v0

    goto :goto_0

    .line 119
    :cond_9
    const-string/jumbo v3, "block_popups"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_a

    .line 120
    invoke-virtual {v1}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->isPopupsManaged()Z

    move-result v0

    goto :goto_0

    .line 123
    :cond_a
    const-string/jumbo v3, "enable_location"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_b

    const-string/jumbo v3, "google_location_settings"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 125
    :cond_b
    invoke-virtual {v1}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->isAllowLocationManaged()Z

    move-result v0

    goto/16 :goto_0
.end method

.method public static onBindViewToPreference(Landroid/preference/Preference;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 174
    invoke-static {p0}, Lcom/google/android/apps/chrome/preferences/ManagedPreferencesUtils;->isPreferenceDisabledByPolicy(Landroid/preference/Preference;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 175
    invoke-static {}, Lcom/google/android/apps/chrome/preferences/ManagedPreferencesUtils;->getManagedByEnterpriseIconId()I

    move-result v0

    invoke-virtual {p0, v0}, Landroid/preference/Preference;->setIcon(I)V

    .line 176
    invoke-static {p1}, Lcom/google/android/apps/chrome/preferences/ManagedPreferencesUtils;->disableView(Landroid/view/View;)V

    .line 178
    :cond_0
    return-void
.end method

.method public static onClickPreference(Landroid/preference/Preference;)Z
    .locals 1

    .prologue
    .line 190
    invoke-static {p0}, Lcom/google/android/apps/chrome/preferences/ManagedPreferencesUtils;->isPreferenceDisabledByPolicy(Landroid/preference/Preference;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 191
    const/4 v0, 0x0

    .line 195
    :goto_0
    return v0

    .line 194
    :cond_0
    invoke-virtual {p0}, Landroid/preference/Preference;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/chrome/preferences/ManagedPreferencesUtils;->showManagedByAdministratorToast(Landroid/content/Context;)V

    .line 195
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static showManagedByAdministratorToast(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 139
    sget v0, Lcom/google/android/apps/chrome/R$string;->managed_by_your_administrator:I

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 143
    return-void
.end method
