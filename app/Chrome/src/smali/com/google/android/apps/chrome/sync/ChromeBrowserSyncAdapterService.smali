.class public Lcom/google/android/apps/chrome/sync/ChromeBrowserSyncAdapterService;
.super Lorg/chromium/chrome/browser/sync/ChromiumSyncAdapterService;
.source "ChromeBrowserSyncAdapterService.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Lorg/chromium/chrome/browser/sync/ChromiumSyncAdapterService;-><init>()V

    return-void
.end method


# virtual methods
.method protected createChromiumSyncAdapter(Landroid/content/Context;Landroid/app/Application;)Lorg/chromium/chrome/browser/sync/ChromiumSyncAdapter;
    .locals 2

    .prologue
    .line 17
    new-instance v0, Lcom/google/android/apps/chrome/sync/ChromeBrowserSyncAdapter;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/sync/ChromeBrowserSyncAdapterService;->getApplication()Landroid/app/Application;

    move-result-object v1

    invoke-direct {v0, p1, v1}, Lcom/google/android/apps/chrome/sync/ChromeBrowserSyncAdapter;-><init>(Landroid/content/Context;Landroid/app/Application;)V

    return-object v0
.end method
