.class public Lcom/google/android/apps/chrome/policy/PolicyManager;
.super Ljava/lang/Object;
.source "PolicyManager.java"

# interfaces
.implements Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Observer;


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private mAppRestrictionsChangedReceiver:Landroid/content/BroadcastReceiver;

.field private final mApplicationRestrictionsUtil:Lcom/google/android/apps/chrome/policy/ApplicationRestrictionsHelper;

.field private mCachedKnoxSettings:Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;

.field private final mContext:Landroid/content/Context;

.field private final mKnoxSettings:Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings;

.field private final mNativePolicyManager:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 27
    const-class v0, Lcom/google/android/apps/chrome/policy/PolicyManager;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/policy/PolicyManager;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 4

    .prologue
    .line 72
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 65
    new-instance v0, Lcom/google/android/apps/chrome/policy/PolicyManager$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/policy/PolicyManager$1;-><init>(Lcom/google/android/apps/chrome/policy/PolicyManager;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/policy/PolicyManager;->mAppRestrictionsChangedReceiver:Landroid/content/BroadcastReceiver;

    .line 73
    invoke-static {}, Lorg/chromium/base/ThreadUtils;->assertOnUiThread()V

    .line 74
    iput-object p1, p0, Lcom/google/android/apps/chrome/policy/PolicyManager;->mContext:Landroid/content/Context;

    .line 75
    invoke-direct {p0}, Lcom/google/android/apps/chrome/policy/PolicyManager;->nativeInit()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/chrome/policy/PolicyManager;->mNativePolicyManager:J

    .line 76
    new-instance v0, Lcom/google/android/apps/chrome/policy/ApplicationRestrictionsHelper;

    invoke-direct {v0, p1}, Lcom/google/android/apps/chrome/policy/ApplicationRestrictionsHelper;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/policy/PolicyManager;->mApplicationRestrictionsUtil:Lcom/google/android/apps/chrome/policy/ApplicationRestrictionsHelper;

    .line 77
    iget-object v0, p0, Lcom/google/android/apps/chrome/policy/PolicyManager;->mApplicationRestrictionsUtil:Lcom/google/android/apps/chrome/policy/ApplicationRestrictionsHelper;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/policy/ApplicationRestrictionsHelper;->isSupported()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 78
    iget-object v0, p0, Lcom/google/android/apps/chrome/policy/PolicyManager;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/apps/chrome/policy/PolicyManager;->mAppRestrictionsChangedReceiver:Landroid/content/BroadcastReceiver;

    new-instance v2, Landroid/content/IntentFilter;

    const-string/jumbo v3, "android.intent.action.APPLICATION_RESTRICTIONS_CHANGED"

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 82
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/policy/PolicyManager;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/policy/PolicyManager;->mKnoxSettings:Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings;

    .line 83
    iget-object v0, p0, Lcom/google/android/apps/chrome/policy/PolicyManager;->mKnoxSettings:Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings;->addObserver(Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Observer;)V

    .line 85
    invoke-direct {p0}, Lcom/google/android/apps/chrome/policy/PolicyManager;->refreshPolicies()V

    .line 86
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/chrome/policy/PolicyManager;)V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/google/android/apps/chrome/policy/PolicyManager;->applyPolicies()V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/apps/chrome/policy/PolicyManager;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lcom/google/android/apps/chrome/policy/PolicyManager;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method private applyAppRestrictions()V
    .locals 6

    .prologue
    .line 199
    iget-object v0, p0, Lcom/google/android/apps/chrome/policy/PolicyManager;->mApplicationRestrictionsUtil:Lcom/google/android/apps/chrome/policy/ApplicationRestrictionsHelper;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/policy/ApplicationRestrictionsHelper;->isSupported()Z

    move-result v0

    if-nez v0, :cond_1

    .line 223
    :cond_0
    return-void

    .line 201
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/policy/PolicyManager;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    .line 202
    iget-object v1, p0, Lcom/google/android/apps/chrome/policy/PolicyManager;->mApplicationRestrictionsUtil:Lcom/google/android/apps/chrome/policy/ApplicationRestrictionsHelper;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/chrome/policy/ApplicationRestrictionsHelper;->getRestrictions(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v2

    .line 203
    invoke-virtual {v2}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 204
    invoke-virtual {v2, v0}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    .line 205
    instance-of v4, v1, Ljava/lang/Boolean;

    if-eqz v4, :cond_3

    .line 206
    iget-wide v4, p0, Lcom/google/android/apps/chrome/policy/PolicyManager;->mNativePolicyManager:J

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-direct {p0, v4, v5, v0, v1}, Lcom/google/android/apps/chrome/policy/PolicyManager;->nativeSetPolicyBoolean(JLjava/lang/String;Z)V

    goto :goto_0

    .line 209
    :cond_3
    instance-of v4, v1, Ljava/lang/String;

    if-eqz v4, :cond_4

    .line 210
    iget-wide v4, p0, Lcom/google/android/apps/chrome/policy/PolicyManager;->mNativePolicyManager:J

    check-cast v1, Ljava/lang/String;

    invoke-direct {p0, v4, v5, v0, v1}, Lcom/google/android/apps/chrome/policy/PolicyManager;->nativeSetPolicyString(JLjava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 213
    :cond_4
    instance-of v4, v1, Ljava/lang/Integer;

    if-eqz v4, :cond_5

    .line 214
    iget-wide v4, p0, Lcom/google/android/apps/chrome/policy/PolicyManager;->mNativePolicyManager:J

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-direct {p0, v4, v5, v0, v1}, Lcom/google/android/apps/chrome/policy/PolicyManager;->nativeSetPolicyInteger(JLjava/lang/String;I)V

    goto :goto_0

    .line 217
    :cond_5
    instance-of v4, v1, [Ljava/lang/String;

    if-eqz v4, :cond_6

    .line 218
    iget-wide v4, p0, Lcom/google/android/apps/chrome/policy/PolicyManager;->mNativePolicyManager:J

    check-cast v1, [Ljava/lang/String;

    invoke-direct {p0, v4, v5, v0, v1}, Lcom/google/android/apps/chrome/policy/PolicyManager;->nativeSetPolicyStringArray(JLjava/lang/String;[Ljava/lang/String;)V

    goto :goto_0

    .line 221
    :cond_6
    sget-boolean v4, Lcom/google/android/apps/chrome/policy/PolicyManager;->$assertionsDisabled:Z

    if-nez v4, :cond_2

    new-instance v2, Ljava/lang/AssertionError;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "Invalid restriction "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v3, " for key "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v2
.end method

.method private applyKnoxSettings()V
    .locals 6

    .prologue
    const/4 v1, 0x0

    const/4 v5, 0x2

    .line 118
    iget-object v0, p0, Lcom/google/android/apps/chrome/policy/PolicyManager;->mCachedKnoxSettings:Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;->getHttpProxyHostPort()Ljava/lang/String;

    move-result-object v0

    .line 119
    if-eqz v0, :cond_0

    .line 120
    iget-wide v2, p0, Lcom/google/android/apps/chrome/policy/PolicyManager;->mNativePolicyManager:J

    const-string/jumbo v4, "ProxyServer"

    invoke-direct {p0, v2, v3, v4, v0}, Lcom/google/android/apps/chrome/policy/PolicyManager;->nativeSetPolicyString(JLjava/lang/String;Ljava/lang/String;)V

    .line 121
    iget-wide v2, p0, Lcom/google/android/apps/chrome/policy/PolicyManager;->mNativePolicyManager:J

    const-string/jumbo v0, "ProxyMode"

    const-string/jumbo v4, "fixed_servers"

    invoke-direct {p0, v2, v3, v0, v4}, Lcom/google/android/apps/chrome/policy/PolicyManager;->nativeSetPolicyString(JLjava/lang/String;Ljava/lang/String;)V

    .line 124
    :cond_0
    const/4 v0, 0x0

    .line 126
    iget-object v2, p0, Lcom/google/android/apps/chrome/policy/PolicyManager;->mCachedKnoxSettings:Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;->getAutofillEnabled()Z

    move-result v2

    if-nez v2, :cond_4

    .line 127
    iget-wide v2, p0, Lcom/google/android/apps/chrome/policy/PolicyManager;->mNativePolicyManager:J

    const-string/jumbo v4, "AutoFillEnabled"

    invoke-direct {p0, v2, v3, v4, v1}, Lcom/google/android/apps/chrome/policy/PolicyManager;->nativeSetPolicyBoolean(JLjava/lang/String;Z)V

    .line 141
    :goto_0
    invoke-static {v0}, Lorg/chromium/chrome/browser/autofill/AutofillLogger;->setLogger(Lorg/chromium/chrome/browser/autofill/AutofillLogger$Logger;)V

    .line 143
    iget-object v0, p0, Lcom/google/android/apps/chrome/policy/PolicyManager;->mCachedKnoxSettings:Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;->getCookiesEnabled()Z

    move-result v0

    if-nez v0, :cond_1

    .line 144
    iget-wide v2, p0, Lcom/google/android/apps/chrome/policy/PolicyManager;->mNativePolicyManager:J

    const-string/jumbo v0, "DefaultCookiesSetting"

    invoke-direct {p0, v2, v3, v0, v5}, Lcom/google/android/apps/chrome/policy/PolicyManager;->nativeSetPolicyInteger(JLjava/lang/String;I)V

    .line 148
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/policy/PolicyManager;->mCachedKnoxSettings:Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;->getJavascriptEnabled()Z

    move-result v0

    if-nez v0, :cond_2

    .line 149
    iget-wide v2, p0, Lcom/google/android/apps/chrome/policy/PolicyManager;->mNativePolicyManager:J

    const-string/jumbo v0, "DefaultJavaScriptSetting"

    invoke-direct {p0, v2, v3, v0, v5}, Lcom/google/android/apps/chrome/policy/PolicyManager;->nativeSetPolicyInteger(JLjava/lang/String;I)V

    .line 153
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/policy/PolicyManager;->mCachedKnoxSettings:Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;->getPopupsEnabled()Z

    move-result v0

    if-nez v0, :cond_3

    .line 154
    iget-wide v2, p0, Lcom/google/android/apps/chrome/policy/PolicyManager;->mNativePolicyManager:J

    const-string/jumbo v0, "DefaultPopupsSetting"

    invoke-direct {p0, v2, v3, v0, v5}, Lcom/google/android/apps/chrome/policy/PolicyManager;->nativeSetPolicyInteger(JLjava/lang/String;I)V

    .line 159
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/chrome/policy/PolicyManager;->mCachedKnoxSettings:Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;->getUrlFilterEnabled()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 160
    iget-object v0, p0, Lcom/google/android/apps/chrome/policy/PolicyManager;->mCachedKnoxSettings:Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;->getUrlBlacklist()Ljava/util/List;

    move-result-object v0

    .line 161
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    new-array v3, v2, [Ljava/lang/String;

    .line 163
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 164
    add-int/lit8 v2, v1, 0x1

    invoke-static {v0}, Lcom/google/android/apps/chrome/policy/PolicyManager;->urlFilterPatternToPolicyPattern(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v1

    move v1, v2

    .line 165
    goto :goto_1

    .line 130
    :cond_4
    new-instance v0, Lcom/google/android/apps/chrome/policy/PolicyManager$2;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/policy/PolicyManager$2;-><init>(Lcom/google/android/apps/chrome/policy/PolicyManager;)V

    goto :goto_0

    .line 166
    :cond_5
    iget-wide v0, p0, Lcom/google/android/apps/chrome/policy/PolicyManager;->mNativePolicyManager:J

    const-string/jumbo v2, "URLBlacklist"

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/google/android/apps/chrome/policy/PolicyManager;->nativeSetPolicyStringArray(JLjava/lang/String;[Ljava/lang/String;)V

    .line 169
    :cond_6
    iget-object v0, p0, Lcom/google/android/apps/chrome/policy/PolicyManager;->mCachedKnoxSettings:Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;->getAuditLogEnabled()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 170
    iget-wide v0, p0, Lcom/google/android/apps/chrome/policy/PolicyManager;->mNativePolicyManager:J

    const-string/jumbo v2, "IncognitoModeAvailability"

    const/4 v3, 0x1

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/google/android/apps/chrome/policy/PolicyManager;->nativeSetPolicyInteger(JLjava/lang/String;I)V

    .line 172
    const/16 v0, 0x4a

    invoke-static {v0}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->broadcastNotification(I)V

    .line 175
    :cond_7
    return-void
.end method

.method private applyPolicies()V
    .locals 2

    .prologue
    .line 108
    iget-object v0, p0, Lcom/google/android/apps/chrome/policy/PolicyManager;->mCachedKnoxSettings:Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;

    if-nez v0, :cond_0

    .line 115
    :goto_0
    return-void

    .line 110
    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/chrome/policy/PolicyManager;->applyKnoxSettings()V

    .line 112
    invoke-direct {p0}, Lcom/google/android/apps/chrome/policy/PolicyManager;->applyAppRestrictions()V

    .line 114
    iget-wide v0, p0, Lcom/google/android/apps/chrome/policy/PolicyManager;->mNativePolicyManager:J

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/chrome/policy/PolicyManager;->nativeFlushPolicies(J)V

    goto :goto_0
.end method

.method private native nativeDestroy(J)V
.end method

.method private native nativeFlushPolicies(J)V
.end method

.method private native nativeInit()J
.end method

.method private native nativeSetPolicyBoolean(JLjava/lang/String;Z)V
.end method

.method private native nativeSetPolicyInteger(JLjava/lang/String;I)V
.end method

.method private native nativeSetPolicyString(JLjava/lang/String;Ljava/lang/String;)V
.end method

.method private native nativeSetPolicyStringArray(JLjava/lang/String;[Ljava/lang/String;)V
.end method

.method private static onPrefsChanged()V
    .locals 1

    .prologue
    .line 232
    invoke-static {}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->getInstance()Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->update()V

    .line 233
    return-void
.end method

.method private refreshPolicies()V
    .locals 1

    .prologue
    .line 227
    iget-object v0, p0, Lcom/google/android/apps/chrome/policy/PolicyManager;->mKnoxSettings:Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings;->refreshSettings()V

    .line 228
    return-void
.end method

.method private static urlFilterPatternToPolicyPattern(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 185
    const-string/jumbo v0, "http://"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 186
    const/4 v0, 0x7

    invoke-virtual {p0, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p0

    .line 190
    :cond_0
    :goto_0
    const-string/jumbo v0, "*."

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 191
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 195
    :goto_1
    return-object v0

    .line 187
    :cond_1
    const-string/jumbo v0, "https://"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 188
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    .line 193
    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    const-string/jumbo v1, "."

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method


# virtual methods
.method public destroy()V
    .locals 2

    .prologue
    .line 89
    iget-object v0, p0, Lcom/google/android/apps/chrome/policy/PolicyManager;->mKnoxSettings:Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings;->removeObserver(Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Observer;)V

    .line 90
    iget-object v0, p0, Lcom/google/android/apps/chrome/policy/PolicyManager;->mKnoxSettings:Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings;->destroy()V

    .line 91
    iget-object v0, p0, Lcom/google/android/apps/chrome/policy/PolicyManager;->mApplicationRestrictionsUtil:Lcom/google/android/apps/chrome/policy/ApplicationRestrictionsHelper;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/policy/ApplicationRestrictionsHelper;->isSupported()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 92
    iget-object v0, p0, Lcom/google/android/apps/chrome/policy/PolicyManager;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/apps/chrome/policy/PolicyManager;->mAppRestrictionsChangedReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 94
    :cond_0
    iget-wide v0, p0, Lcom/google/android/apps/chrome/policy/PolicyManager;->mNativePolicyManager:J

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/chrome/policy/PolicyManager;->nativeDestroy(J)V

    .line 95
    return-void
.end method

.method public onSettingsAvailable(Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;)V
    .locals 0

    .prologue
    .line 99
    iput-object p1, p0, Lcom/google/android/apps/chrome/policy/PolicyManager;->mCachedKnoxSettings:Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;

    .line 100
    invoke-direct {p0}, Lcom/google/android/apps/chrome/policy/PolicyManager;->applyPolicies()V

    .line 101
    return-void
.end method
