.class public Lcom/google/android/apps/chrome/UrlConstants;
.super Ljava/lang/Object;
.source "UrlConstants.java"


# static fields
.field public static final BOOKMARKS_FOLDER_URL:Ljava/lang/String; = "chrome-native://bookmarks/#"

.field public static final BOOKMARKS_HOST:Ljava/lang/String; = "bookmarks"

.field public static final BOOKMARKS_URL:Ljava/lang/String; = "chrome-native://bookmarks/"

.field public static final CHROME_NATIVE_SCHEME:Ljava/lang/String; = "chrome-native://"

.field public static final CHROME_SCHEME:Ljava/lang/String; = "chrome://"

.field public static final CRASH_REASON_URL:Ljava/lang/String; = "https://support.google.com/chrome/?p=mobile_awsnap"

.field public static final HISTORY_URL:Ljava/lang/String; = "chrome://history/"

.field public static final HTTPS_SCHEME:Ljava/lang/String; = "https://"

.field public static final HTTP_SCHEME:Ljava/lang/String; = "http://"

.field public static final MOBILE_BOOKMARKS_URL:Ljava/lang/String; = "chrome-native://bookmarks/#2"

.field public static final NTP_HOST:Ljava/lang/String; = "newtab"

.field public static final NTP_URL:Ljava/lang/String; = "chrome-native://newtab/"

.field public static final RECENT_TABS_HOST:Ljava/lang/String; = "recent-tabs"

.field public static final RECENT_TABS_URL:Ljava/lang/String; = "chrome-native://recent-tabs/"

.field public static final WELCOME_URL:Ljava/lang/String; = "chrome://welcome/"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
