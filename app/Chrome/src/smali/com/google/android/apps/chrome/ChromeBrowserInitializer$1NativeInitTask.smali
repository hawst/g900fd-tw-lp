.class abstract Lcom/google/android/apps/chrome/ChromeBrowserInitializer$1NativeInitTask;
.super Ljava/lang/Object;
.source "ChromeBrowserInitializer.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/ChromeBrowserInitializer;

.field final synthetic val$initQueue:Ljava/util/LinkedList;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/ChromeBrowserInitializer;Ljava/util/LinkedList;)V
    .locals 0

    .prologue
    .line 218
    iput-object p1, p0, Lcom/google/android/apps/chrome/ChromeBrowserInitializer$1NativeInitTask;->this$0:Lcom/google/android/apps/chrome/ChromeBrowserInitializer;

    iput-object p2, p0, Lcom/google/android/apps/chrome/ChromeBrowserInitializer$1NativeInitTask;->val$initQueue:Ljava/util/LinkedList;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract initFunction()V
.end method

.method public final run()V
    .locals 2

    .prologue
    .line 224
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ChromeBrowserInitializer$1NativeInitTask;->initFunction()V

    .line 225
    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeBrowserInitializer$1NativeInitTask;->val$initQueue:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 226
    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeBrowserInitializer$1NativeInitTask;->this$0:Lcom/google/android/apps/chrome/ChromeBrowserInitializer;

    # getter for: Lcom/google/android/apps/chrome/ChromeBrowserInitializer;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/google/android/apps/chrome/ChromeBrowserInitializer;->access$000(Lcom/google/android/apps/chrome/ChromeBrowserInitializer;)Landroid/os/Handler;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeBrowserInitializer$1NativeInitTask;->val$initQueue:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->pop()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Runnable;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 228
    :cond_0
    return-void
.end method
