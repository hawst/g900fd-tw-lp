.class public Lcom/google/android/apps/chrome/infobar/SnapshotDownloadSuceededInfobar;
.super Lorg/chromium/chrome/browser/infobar/InfoBar;
.source "SnapshotDownloadSuceededInfobar.java"


# instance fields
.field private final mSnapshotUri:Landroid/net/Uri;

.field private final mTab:Lorg/chromium/chrome/browser/Tab;


# direct methods
.method public constructor <init>(Lorg/chromium/chrome/browser/Tab;Landroid/net/Uri;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 28
    sget v0, Lcom/google/android/apps/chrome/R$drawable;->infobar_downloading:I

    invoke-direct {p0, v1, v0, v1}, Lorg/chromium/chrome/browser/infobar/InfoBar;-><init>(Lorg/chromium/chrome/browser/infobar/InfoBarListeners$Dismiss;ILjava/lang/CharSequence;)V

    .line 29
    iput-object p1, p0, Lcom/google/android/apps/chrome/infobar/SnapshotDownloadSuceededInfobar;->mTab:Lorg/chromium/chrome/browser/Tab;

    .line 30
    iput-object p2, p0, Lcom/google/android/apps/chrome/infobar/SnapshotDownloadSuceededInfobar;->mSnapshotUri:Landroid/net/Uri;

    .line 31
    return-void
.end method


# virtual methods
.method public createContent(Lorg/chromium/chrome/browser/infobar/InfoBarLayout;)V
    .locals 2

    .prologue
    .line 35
    invoke-virtual {p1}, Lorg/chromium/chrome/browser/infobar/InfoBarLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 36
    sget v1, Lcom/google/android/apps/chrome/R$string;->snapshot_downloaded_infobar:I

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Lorg/chromium/chrome/browser/infobar/InfoBarLayout;->setMessage(Ljava/lang/CharSequence;)V

    .line 37
    sget v1, Lcom/google/android/apps/chrome/R$string;->snapshot_downloaded_infobar_button_open:I

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Lorg/chromium/chrome/browser/infobar/InfoBarLayout;->setButtons(Ljava/lang/String;Ljava/lang/String;)V

    .line 39
    return-void
.end method

.method public onButtonClicked(Z)V
    .locals 4

    .prologue
    .line 43
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/infobar/SnapshotDownloadSuceededInfobar;->dismissJavaOnlyInfoBar()V

    .line 44
    iget-object v0, p0, Lcom/google/android/apps/chrome/infobar/SnapshotDownloadSuceededInfobar;->mTab:Lorg/chromium/chrome/browser/Tab;

    new-instance v1, Lorg/chromium/content_public/browser/LoadUrlParams;

    iget-object v2, p0, Lcom/google/android/apps/chrome/infobar/SnapshotDownloadSuceededInfobar;->mSnapshotUri:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x2

    invoke-direct {v1, v2, v3}, Lorg/chromium/content_public/browser/LoadUrlParams;-><init>(Ljava/lang/String;I)V

    invoke-virtual {v0, v1}, Lorg/chromium/chrome/browser/Tab;->loadUrl(Lorg/chromium/content_public/browser/LoadUrlParams;)I

    .line 46
    return-void
.end method

.method public onCloseButtonClicked()V
    .locals 0

    .prologue
    .line 50
    invoke-super {p0}, Lorg/chromium/chrome/browser/infobar/InfoBar;->dismissJavaOnlyInfoBar()V

    .line 51
    return-void
.end method
