.class public Lcom/google/android/apps/chrome/passwordmanager/SamsungFingerprintAuthenticationHelper;
.super Ljava/lang/Object;
.source "SamsungFingerprintAuthenticationHelper.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getFingerprintPasswordProtectionString(Landroid/content/Context;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 83
    sget v0, Lcom/google/android/apps/chrome/R$string;->protect_with_fingerprint:I

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static isEnabled(Landroid/content/Context;)Z
    .locals 1

    .prologue
    .line 41
    const/4 v0, 0x0

    return v0
.end method

.method public static isSupported(Landroid/content/Context;)Z
    .locals 1

    .prologue
    .line 58
    const/4 v0, 0x0

    return v0
.end method
