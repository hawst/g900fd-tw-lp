.class public Lcom/google/android/apps/chrome/preferences/BasicsPreferences;
.super Lcom/google/android/apps/chrome/preferences/ChromeBasePreferenceFragment;
.source "BasicsPreferences.java"


# static fields
.field public static final PREF_CATEGORY_SYNC_ACCOUNT:Ljava/lang/String; = "sync_account"


# instance fields
.field private mSyncAccountPreferenceScreen:Landroid/preference/PreferenceScreen;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/google/android/apps/chrome/preferences/ChromeBasePreferenceFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 39
    invoke-super {p0, p1}, Lcom/google/android/apps/chrome/preferences/ChromeBasePreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    .line 40
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/BasicsPreferences;->updatePreferences()V

    .line 41
    return-void
.end method

.method public onResume()V
    .locals 0

    .prologue
    .line 113
    invoke-super {p0}, Lcom/google/android/apps/chrome/preferences/ChromeBasePreferenceFragment;->onResume()V

    .line 114
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/BasicsPreferences;->updateAccountHeader()V

    .line 115
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/BasicsPreferences;->updateSummaries()V

    .line 116
    return-void
.end method

.method public updateAccountHeader()V
    .locals 4

    .prologue
    .line 122
    invoke-static {}, Lorg/chromium/chrome/browser/signin/SigninManager;->isNewProfileManagementEnabled()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 138
    :cond_0
    :goto_0
    return-void

    .line 124
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/BasicsPreferences;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lorg/chromium/sync/signin/ChromeSigninController;->get(Landroid/content/Context;)Lorg/chromium/sync/signin/ChromeSigninController;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/sync/signin/ChromeSigninController;->getSignedInUser()Landroid/accounts/Account;

    move-result-object v0

    .line 125
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/BasicsPreferences;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v1

    .line 126
    if-eqz v0, :cond_2

    .line 127
    invoke-virtual {v1}, Landroid/preference/PreferenceScreen;->removeAll()V

    .line 128
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/BasicsPreferences;->getPreferenceManager()Landroid/preference/PreferenceManager;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/BasicsPreferences;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/preference/PreferenceManager;->createPreferenceScreen(Landroid/content/Context;)Landroid/preference/PreferenceScreen;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/chrome/preferences/BasicsPreferences;->mSyncAccountPreferenceScreen:Landroid/preference/PreferenceScreen;

    .line 130
    iget-object v2, p0, Lcom/google/android/apps/chrome/preferences/BasicsPreferences;->mSyncAccountPreferenceScreen:Landroid/preference/PreferenceScreen;

    const-string/jumbo v3, "sync_account"

    invoke-virtual {v2, v3}, Landroid/preference/PreferenceScreen;->setKey(Ljava/lang/String;)V

    .line 131
    iget-object v2, p0, Lcom/google/android/apps/chrome/preferences/BasicsPreferences;->mSyncAccountPreferenceScreen:Landroid/preference/PreferenceScreen;

    iget-object v0, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v2, v0}, Landroid/preference/PreferenceScreen;->setTitle(Ljava/lang/CharSequence;)V

    .line 132
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/BasicsPreferences;->mSyncAccountPreferenceScreen:Landroid/preference/PreferenceScreen;

    const-class v2, Lcom/google/android/apps/chrome/services/ui/GoogleServicesFragment;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/preference/PreferenceScreen;->setFragment(Ljava/lang/String;)V

    .line 133
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/BasicsPreferences;->mSyncAccountPreferenceScreen:Landroid/preference/PreferenceScreen;

    invoke-virtual {v1, v0}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 134
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/BasicsPreferences;->updatePreferences()V

    goto :goto_0

    .line 135
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/BasicsPreferences;->mSyncAccountPreferenceScreen:Landroid/preference/PreferenceScreen;

    if-eqz v0, :cond_0

    .line 136
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/BasicsPreferences;->mSyncAccountPreferenceScreen:Landroid/preference/PreferenceScreen;

    invoke-virtual {v1, v0}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    goto :goto_0
.end method

.method public updatePreferences()V
    .locals 2

    .prologue
    .line 47
    sget v0, Lcom/google/android/apps/chrome/R$xml;->basics_preferences:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/preferences/BasicsPreferences;->addPreferencesFromResource(I)V

    .line 48
    invoke-static {}, Lcom/google/android/apps/chrome/partnercustomizations/HomepageManager;->shouldShowHomepageSetting()Z

    move-result v0

    if-nez v0, :cond_0

    .line 49
    const-string/jumbo v0, "homepage"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/preferences/BasicsPreferences;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceScreen;

    .line 50
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/BasicsPreferences;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    .line 52
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/BasicsPreferences;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/chrome/utilities/FeatureUtilitiesInternal;->isDocumentModeEligible(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 53
    const-string/jumbo v0, "document_mode"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/preferences/BasicsPreferences;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceScreen;

    .line 54
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/BasicsPreferences;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    .line 56
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/BasicsPreferences;->updateSummaries()V

    .line 57
    return-void
.end method

.method public updateSummaries()V
    .locals 5

    .prologue
    .line 63
    const-string/jumbo v0, "autofill_settings"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/preferences/BasicsPreferences;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceScreen;

    .line 64
    invoke-static {}, Lorg/chromium/chrome/browser/autofill/PersonalDataManager;->isAutofillEnabled()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 65
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/BasicsPreferences;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/apps/chrome/R$string;->text_on:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->setSummary(Ljava/lang/CharSequence;)V

    .line 69
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/BasicsPreferences;->getActivity()Landroid/app/Activity;

    move-result-object v1

    sget v2, Lcom/google/android/apps/chrome/R$id;->autofill_settings:I

    int-to-long v2, v2

    invoke-static {v1, v2, v3, v0}, Lcom/google/android/apps/chrome/preferences/ManagedPreferencesUtils;->disableIfManagedByPolicy(Landroid/content/Context;JLandroid/preference/PreferenceScreen;)V

    .line 71
    const-string/jumbo v0, "manage_saved_passwords"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/preferences/BasicsPreferences;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceScreen;

    .line 73
    invoke-static {}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->getInstance()Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->isRememberPasswordsEnabled()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 74
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/BasicsPreferences;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/apps/chrome/R$string;->text_on:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->setSummary(Ljava/lang/CharSequence;)V

    .line 78
    :goto_1
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/BasicsPreferences;->getActivity()Landroid/app/Activity;

    move-result-object v1

    sget v2, Lcom/google/android/apps/chrome/R$id;->manage_saved_passwords:I

    int-to-long v2, v2

    invoke-static {v1, v2, v3, v0}, Lcom/google/android/apps/chrome/preferences/ManagedPreferencesUtils;->disableIfManagedByPolicy(Landroid/content/Context;JLandroid/preference/PreferenceScreen;)V

    .line 80
    const-string/jumbo v0, "document_mode"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/preferences/BasicsPreferences;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceScreen;

    .line 81
    if-eqz v0, :cond_0

    .line 82
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/BasicsPreferences;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->isOptedOutOfDocumentMode()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 83
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/BasicsPreferences;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/apps/chrome/R$string;->text_off:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->setSummary(Ljava/lang/CharSequence;)V

    .line 88
    :cond_0
    :goto_2
    const-string/jumbo v0, "homepage"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/preferences/BasicsPreferences;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceScreen;

    .line 89
    if-eqz v0, :cond_1

    .line 90
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/BasicsPreferences;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->getPrefHomepagePreference()Z

    move-result v1

    if-eqz v1, :cond_7

    .line 91
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/BasicsPreferences;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/apps/chrome/R$string;->text_on:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->setSummary(Ljava/lang/CharSequence;)V

    .line 96
    :cond_1
    :goto_3
    const-string/jumbo v0, "search_engine"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/preferences/BasicsPreferences;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceScreen;

    .line 97
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/BasicsPreferences;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->getPrefSearchEnginePreference()I

    move-result v2

    .line 100
    invoke-static {}, Lorg/chromium/chrome/browser/search_engines/TemplateUrlService;->getInstance()Lorg/chromium/chrome/browser/search_engines/TemplateUrlService;

    move-result-object v1

    invoke-virtual {v1}, Lorg/chromium/chrome/browser/search_engines/TemplateUrlService;->getLocalizedSearchEngines()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/chromium/chrome/browser/search_engines/TemplateUrlService$TemplateUrl;

    .line 101
    invoke-virtual {v1}, Lorg/chromium/chrome/browser/search_engines/TemplateUrlService$TemplateUrl;->getIndex()I

    move-result v4

    if-ne v2, v4, :cond_2

    .line 102
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/BasicsPreferences;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-static {v2, v1}, Lcom/google/android/apps/chrome/preferences/SearchEnginePreference;->getSearchEngineNameAndDomain(Landroid/content/Context;Lorg/chromium/chrome/browser/search_engines/TemplateUrlService$TemplateUrl;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->setSummary(Ljava/lang/CharSequence;)V

    .line 107
    :cond_3
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/BasicsPreferences;->getActivity()Landroid/app/Activity;

    move-result-object v1

    sget v2, Lcom/google/android/apps/chrome/R$id;->search_engine:I

    int-to-long v2, v2

    invoke-static {v1, v2, v3, v0}, Lcom/google/android/apps/chrome/preferences/ManagedPreferencesUtils;->disableIfManagedByPolicy(Landroid/content/Context;JLandroid/preference/PreferenceScreen;)V

    .line 109
    return-void

    .line 67
    :cond_4
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/BasicsPreferences;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/apps/chrome/R$string;->text_off:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->setSummary(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 76
    :cond_5
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/BasicsPreferences;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/apps/chrome/R$string;->text_off:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->setSummary(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 85
    :cond_6
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/BasicsPreferences;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/apps/chrome/R$string;->text_on:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->setSummary(Ljava/lang/CharSequence;)V

    goto/16 :goto_2

    .line 93
    :cond_7
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/BasicsPreferences;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/apps/chrome/R$string;->text_off:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->setSummary(Ljava/lang/CharSequence;)V

    goto/16 :goto_3
.end method
