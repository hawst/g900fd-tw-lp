.class public Lcom/google/android/apps/chrome/preferences/SeekBarPreference;
.super Landroid/preference/Preference;
.source "SeekBarPreference.java"

# interfaces
.implements Landroid/widget/SeekBar$OnSeekBarChangeListener;


# instance fields
.field protected mMax:F

.field protected mMin:F

.field protected mStep:F

.field mSummary:Ljava/lang/CharSequence;

.field private mSummaryView:Landroid/widget/TextView;

.field private mTrackingTouch:Z

.field private mValue:F


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3

    .prologue
    .line 41
    invoke-direct {p0, p1, p2}, Landroid/preference/Preference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 43
    sget-object v0, Lcom/google/android/apps/chrome/R$styleable;->SeekBarPreference:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 44
    sget v1, Lcom/google/android/apps/chrome/R$styleable;->SeekBarPreference_min:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v1

    iput v1, p0, Lcom/google/android/apps/chrome/preferences/SeekBarPreference;->mMin:F

    .line 45
    sget v1, Lcom/google/android/apps/chrome/R$styleable;->SeekBarPreference_max:I

    const/high16 v2, 0x42c80000    # 100.0f

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v1

    iput v1, p0, Lcom/google/android/apps/chrome/preferences/SeekBarPreference;->mMax:F

    .line 46
    sget v1, Lcom/google/android/apps/chrome/R$styleable;->SeekBarPreference_step:I

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v1

    iput v1, p0, Lcom/google/android/apps/chrome/preferences/SeekBarPreference;->mStep:F

    .line 47
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/SeekBarPreference;->notifyChanged()V

    .line 48
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 50
    sget v0, Lcom/google/android/apps/chrome/R$layout;->custom_preference:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/preferences/SeekBarPreference;->setLayoutResource(I)V

    .line 51
    sget v0, Lcom/google/android/apps/chrome/R$layout;->preference_seekbar:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/preferences/SeekBarPreference;->setWidgetLayoutResource(I)V

    .line 52
    return-void
.end method

.method private prefValueToSeekBarProgress(F)I
    .locals 2

    .prologue
    .line 104
    iget v0, p0, Lcom/google/android/apps/chrome/preferences/SeekBarPreference;->mMin:F

    sub-float v0, p1, v0

    iget v1, p0, Lcom/google/android/apps/chrome/preferences/SeekBarPreference;->mStep:F

    div-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    return v0
.end method

.method private seekBarProgressToPrefValue(I)F
    .locals 3

    .prologue
    .line 101
    iget v0, p0, Lcom/google/android/apps/chrome/preferences/SeekBarPreference;->mMin:F

    int-to-float v1, p1

    iget v2, p0, Lcom/google/android/apps/chrome/preferences/SeekBarPreference;->mStep:F

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    return v0
.end method

.method private setValue(FZ)V
    .locals 1

    .prologue
    .line 112
    iget v0, p0, Lcom/google/android/apps/chrome/preferences/SeekBarPreference;->mMax:F

    cmpl-float v0, p1, v0

    if-lez v0, :cond_0

    .line 113
    iget p1, p0, Lcom/google/android/apps/chrome/preferences/SeekBarPreference;->mMax:F

    .line 115
    :cond_0
    iget v0, p0, Lcom/google/android/apps/chrome/preferences/SeekBarPreference;->mMin:F

    cmpg-float v0, p1, v0

    if-gez v0, :cond_1

    .line 116
    iget p1, p0, Lcom/google/android/apps/chrome/preferences/SeekBarPreference;->mMin:F

    .line 118
    :cond_1
    iget v0, p0, Lcom/google/android/apps/chrome/preferences/SeekBarPreference;->mValue:F

    cmpl-float v0, p1, v0

    if-eqz v0, :cond_2

    .line 119
    iput p1, p0, Lcom/google/android/apps/chrome/preferences/SeekBarPreference;->mValue:F

    .line 120
    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/preferences/SeekBarPreference;->persistFloat(F)Z

    .line 121
    if-eqz p2, :cond_2

    .line 122
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/SeekBarPreference;->notifyChanged()V

    .line 125
    :cond_2
    return-void
.end method


# virtual methods
.method public getSummary()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/SeekBarPreference;->mSummary:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public isTrackingTouch()Z
    .locals 1

    .prologue
    .line 161
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/preferences/SeekBarPreference;->mTrackingTouch:Z

    return v0
.end method

.method protected onBindView(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 56
    invoke-super {p0, p1}, Landroid/preference/Preference;->onBindView(Landroid/view/View;)V

    .line 57
    sget v0, Lcom/google/android/apps/chrome/R$id;->seekbar:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/SeekBar;

    .line 58
    invoke-virtual {v0, p0}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    .line 59
    iget v1, p0, Lcom/google/android/apps/chrome/preferences/SeekBarPreference;->mMax:F

    invoke-direct {p0, v1}, Lcom/google/android/apps/chrome/preferences/SeekBarPreference;->prefValueToSeekBarProgress(F)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setMax(I)V

    .line 60
    iget v1, p0, Lcom/google/android/apps/chrome/preferences/SeekBarPreference;->mValue:F

    invoke-direct {p0, v1}, Lcom/google/android/apps/chrome/preferences/SeekBarPreference;->prefValueToSeekBarProgress(F)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 61
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/SeekBarPreference;->isEnabled()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setEnabled(Z)V

    .line 62
    sget v0, Lcom/google/android/apps/chrome/R$id;->seekbar_amount:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/chrome/preferences/SeekBarPreference;->mSummaryView:Landroid/widget/TextView;

    .line 63
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/SeekBarPreference;->mSummaryView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/apps/chrome/preferences/SeekBarPreference;->mSummary:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 64
    return-void
.end method

.method protected onGetDefaultValue(Landroid/content/res/TypedArray;I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 74
    const/4 v0, 0x0

    invoke-virtual {p1, p2, v0}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    return-object v0
.end method

.method public onProgressChanged(Landroid/widget/SeekBar;IZ)V
    .locals 0

    .prologue
    .line 145
    if-eqz p3, :cond_0

    .line 146
    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/preferences/SeekBarPreference;->syncProgress(Landroid/widget/SeekBar;)V

    .line 148
    :cond_0
    return-void
.end method

.method protected onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 2

    .prologue
    .line 189
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-class v1, Lcom/google/android/apps/chrome/preferences/SeekBarPreference$SavedState;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 191
    invoke-super {p0, p1}, Landroid/preference/Preference;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 203
    :goto_0
    return-void

    .line 196
    :cond_0
    check-cast p1, Lcom/google/android/apps/chrome/preferences/SeekBarPreference$SavedState;

    .line 197
    invoke-virtual {p1}, Lcom/google/android/apps/chrome/preferences/SeekBarPreference$SavedState;->getSuperState()Landroid/os/Parcelable;

    move-result-object v0

    invoke-super {p0, v0}, Landroid/preference/Preference;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 198
    iget v0, p1, Lcom/google/android/apps/chrome/preferences/SeekBarPreference$SavedState;->mMin:F

    iput v0, p0, Lcom/google/android/apps/chrome/preferences/SeekBarPreference;->mMin:F

    .line 199
    iget v0, p1, Lcom/google/android/apps/chrome/preferences/SeekBarPreference$SavedState;->mMax:F

    iput v0, p0, Lcom/google/android/apps/chrome/preferences/SeekBarPreference;->mMax:F

    .line 200
    iget v0, p1, Lcom/google/android/apps/chrome/preferences/SeekBarPreference$SavedState;->mStep:F

    iput v0, p0, Lcom/google/android/apps/chrome/preferences/SeekBarPreference;->mStep:F

    .line 201
    iget v0, p1, Lcom/google/android/apps/chrome/preferences/SeekBarPreference$SavedState;->mValue:F

    iput v0, p0, Lcom/google/android/apps/chrome/preferences/SeekBarPreference;->mValue:F

    .line 202
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/SeekBarPreference;->notifyChanged()V

    goto :goto_0
.end method

.method protected onSaveInstanceState()Landroid/os/Parcelable;
    .locals 2

    .prologue
    .line 172
    invoke-super {p0}, Landroid/preference/Preference;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v0

    .line 173
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/SeekBarPreference;->isPersistent()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 184
    :goto_0
    return-object v0

    .line 179
    :cond_0
    new-instance v1, Lcom/google/android/apps/chrome/preferences/SeekBarPreference$SavedState;

    invoke-direct {v1, v0}, Lcom/google/android/apps/chrome/preferences/SeekBarPreference$SavedState;-><init>(Landroid/os/Parcelable;)V

    .line 180
    iget v0, p0, Lcom/google/android/apps/chrome/preferences/SeekBarPreference;->mMin:F

    iput v0, v1, Lcom/google/android/apps/chrome/preferences/SeekBarPreference$SavedState;->mMin:F

    .line 181
    iget v0, p0, Lcom/google/android/apps/chrome/preferences/SeekBarPreference;->mMax:F

    iput v0, v1, Lcom/google/android/apps/chrome/preferences/SeekBarPreference$SavedState;->mMax:F

    .line 182
    iget v0, p0, Lcom/google/android/apps/chrome/preferences/SeekBarPreference;->mStep:F

    iput v0, v1, Lcom/google/android/apps/chrome/preferences/SeekBarPreference$SavedState;->mStep:F

    .line 183
    iget v0, p0, Lcom/google/android/apps/chrome/preferences/SeekBarPreference;->mValue:F

    iput v0, v1, Lcom/google/android/apps/chrome/preferences/SeekBarPreference$SavedState;->mValue:F

    move-object v0, v1

    .line 184
    goto :goto_0
.end method

.method protected onSetInitialValue(ZLjava/lang/Object;)V
    .locals 2

    .prologue
    .line 68
    if-eqz p1, :cond_0

    iget v0, p0, Lcom/google/android/apps/chrome/preferences/SeekBarPreference;->mValue:F

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/preferences/SeekBarPreference;->getPersistedFloat(F)F

    move-result v0

    :goto_0
    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/chrome/preferences/SeekBarPreference;->setValue(FZ)V

    .line 70
    return-void

    .line 68
    :cond_0
    check-cast p2, Ljava/lang/Float;

    invoke-virtual {p2}, Ljava/lang/Float;->floatValue()F

    move-result v0

    goto :goto_0
.end method

.method public onStartTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 1

    .prologue
    .line 152
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/preferences/SeekBarPreference;->mTrackingTouch:Z

    .line 153
    return-void
.end method

.method public onStopTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 1

    .prologue
    .line 157
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/preferences/SeekBarPreference;->mTrackingTouch:Z

    .line 158
    return-void
.end method

.method public setSummary(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/SeekBarPreference;->mSummary:Ljava/lang/CharSequence;

    invoke-static {p1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 90
    :cond_0
    :goto_0
    return-void

    .line 88
    :cond_1
    iput-object p1, p0, Lcom/google/android/apps/chrome/preferences/SeekBarPreference;->mSummary:Ljava/lang/CharSequence;

    .line 89
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/SeekBarPreference;->mSummaryView:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/SeekBarPreference;->mSummaryView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method syncProgress(Landroid/widget/SeekBar;)V
    .locals 2

    .prologue
    .line 132
    invoke-virtual {p1}, Landroid/widget/SeekBar;->getProgress()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/preferences/SeekBarPreference;->seekBarProgressToPrefValue(I)F

    move-result v0

    .line 133
    iget v1, p0, Lcom/google/android/apps/chrome/preferences/SeekBarPreference;->mValue:F

    cmpl-float v1, v0, v1

    if-eqz v1, :cond_0

    .line 134
    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/apps/chrome/preferences/SeekBarPreference;->callChangeListener(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 135
    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/chrome/preferences/SeekBarPreference;->setValue(FZ)V

    .line 140
    :cond_0
    :goto_0
    return-void

    .line 137
    :cond_1
    iget v0, p0, Lcom/google/android/apps/chrome/preferences/SeekBarPreference;->mValue:F

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/preferences/SeekBarPreference;->prefValueToSeekBarProgress(F)I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/widget/SeekBar;->setProgress(I)V

    goto :goto_0
.end method
