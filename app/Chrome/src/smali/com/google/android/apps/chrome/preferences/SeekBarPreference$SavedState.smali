.class Lcom/google/android/apps/chrome/preferences/SeekBarPreference$SavedState;
.super Landroid/preference/Preference$BaseSavedState;
.source "SeekBarPreference.java"


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field mMax:F

.field mMin:F

.field mStep:F

.field mValue:F


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 240
    new-instance v0, Lcom/google/android/apps/chrome/preferences/SeekBarPreference$SavedState$1;

    invoke-direct {v0}, Lcom/google/android/apps/chrome/preferences/SeekBarPreference$SavedState$1;-><init>()V

    sput-object v0, Lcom/google/android/apps/chrome/preferences/SeekBarPreference$SavedState;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 216
    invoke-direct {p0, p1}, Landroid/preference/Preference$BaseSavedState;-><init>(Landroid/os/Parcel;)V

    .line 218
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/preferences/SeekBarPreference$SavedState;->mMin:F

    .line 219
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/preferences/SeekBarPreference$SavedState;->mMax:F

    .line 220
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/preferences/SeekBarPreference$SavedState;->mStep:F

    .line 221
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/preferences/SeekBarPreference$SavedState;->mValue:F

    .line 222
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcelable;)V
    .locals 0

    .prologue
    .line 236
    invoke-direct {p0, p1}, Landroid/preference/Preference$BaseSavedState;-><init>(Landroid/os/Parcelable;)V

    .line 237
    return-void
.end method


# virtual methods
.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 226
    invoke-super {p0, p1, p2}, Landroid/preference/Preference$BaseSavedState;->writeToParcel(Landroid/os/Parcel;I)V

    .line 229
    iget v0, p0, Lcom/google/android/apps/chrome/preferences/SeekBarPreference$SavedState;->mMin:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 230
    iget v0, p0, Lcom/google/android/apps/chrome/preferences/SeekBarPreference$SavedState;->mMax:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 231
    iget v0, p0, Lcom/google/android/apps/chrome/preferences/SeekBarPreference$SavedState;->mStep:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 232
    iget v0, p0, Lcom/google/android/apps/chrome/preferences/SeekBarPreference$SavedState;->mValue:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 233
    return-void
.end method
