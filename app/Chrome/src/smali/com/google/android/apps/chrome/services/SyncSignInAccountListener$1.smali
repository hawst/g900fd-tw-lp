.class Lcom/google/android/apps/chrome/services/SyncSignInAccountListener$1;
.super Ljava/lang/Object;
.source "SyncSignInAccountListener.java"

# interfaces
.implements Lorg/chromium/chrome/browser/signin/SigninManager$SignInFlowObserver;


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/services/SyncSignInAccountListener;

.field final synthetic val$account:Landroid/accounts/Account;

.field final synthetic val$activity:Landroid/app/Activity;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 51
    const-class v0, Lcom/google/android/apps/chrome/services/SyncSignInAccountListener;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/services/SyncSignInAccountListener$1;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>(Lcom/google/android/apps/chrome/services/SyncSignInAccountListener;Landroid/app/Activity;Landroid/accounts/Account;)V
    .locals 0

    .prologue
    .line 51
    iput-object p1, p0, Lcom/google/android/apps/chrome/services/SyncSignInAccountListener$1;->this$0:Lcom/google/android/apps/chrome/services/SyncSignInAccountListener;

    iput-object p2, p0, Lcom/google/android/apps/chrome/services/SyncSignInAccountListener$1;->val$activity:Landroid/app/Activity;

    iput-object p3, p0, Lcom/google/android/apps/chrome/services/SyncSignInAccountListener$1;->val$account:Landroid/accounts/Account;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onSigninCancelled()V
    .locals 0

    .prologue
    .line 61
    return-void
.end method

.method public onSigninComplete()V
    .locals 3

    .prologue
    .line 54
    sget-boolean v0, Lcom/google/android/apps/chrome/services/SyncSignInAccountListener$1;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/services/SyncSignInAccountListener$1;->val$activity:Landroid/app/Activity;

    instance-of v0, v0, Lcom/google/android/apps/chrome/preferences/Preferences;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 55
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/services/SyncSignInAccountListener$1;->val$activity:Landroid/app/Activity;

    instance-of v0, v0, Lcom/google/android/apps/chrome/preferences/Preferences;

    if-eqz v0, :cond_1

    .line 56
    iget-object v1, p0, Lcom/google/android/apps/chrome/services/SyncSignInAccountListener$1;->this$0:Lcom/google/android/apps/chrome/services/SyncSignInAccountListener;

    iget-object v0, p0, Lcom/google/android/apps/chrome/services/SyncSignInAccountListener$1;->val$activity:Landroid/app/Activity;

    check-cast v0, Lcom/google/android/apps/chrome/preferences/Preferences;

    iget-object v2, p0, Lcom/google/android/apps/chrome/services/SyncSignInAccountListener$1;->val$account:Landroid/accounts/Account;

    # invokes: Lcom/google/android/apps/chrome/services/SyncSignInAccountListener;->doFinishSignIn(Lcom/google/android/apps/chrome/preferences/Preferences;Landroid/accounts/Account;)V
    invoke-static {v1, v0, v2}, Lcom/google/android/apps/chrome/services/SyncSignInAccountListener;->access$000(Lcom/google/android/apps/chrome/services/SyncSignInAccountListener;Lcom/google/android/apps/chrome/preferences/Preferences;Landroid/accounts/Account;)V

    .line 58
    :cond_1
    return-void
.end method
