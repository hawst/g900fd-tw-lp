.class public final Lcom/google/android/apps/chrome/document/DocumentDatabase$DocumentList;
.super Lcom/google/protobuf/nano/ExtendableMessageNano;
.source "DocumentDatabase.java"


# instance fields
.field public entries:[Lcom/google/android/apps/chrome/document/DocumentDatabase$DocumentEntry;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 124
    invoke-direct {p0}, Lcom/google/protobuf/nano/ExtendableMessageNano;-><init>()V

    .line 125
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/document/DocumentDatabase$DocumentList;->clear()Lcom/google/android/apps/chrome/document/DocumentDatabase$DocumentList;

    .line 126
    return-void
.end method


# virtual methods
.method public final clear()Lcom/google/android/apps/chrome/document/DocumentDatabase$DocumentList;
    .locals 1

    .prologue
    .line 129
    invoke-static {}, Lcom/google/android/apps/chrome/document/DocumentDatabase$DocumentEntry;->emptyArray()[Lcom/google/android/apps/chrome/document/DocumentDatabase$DocumentEntry;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentDatabase$DocumentList;->entries:[Lcom/google/android/apps/chrome/document/DocumentDatabase$DocumentEntry;

    .line 130
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentDatabase$DocumentList;->unknownFieldData:Ljava/util/List;

    .line 131
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/chrome/document/DocumentDatabase$DocumentList;->cachedSize:I

    .line 132
    return-object p0
.end method

.method protected final computeSerializedSize()I
    .locals 4

    .prologue
    .line 151
    invoke-super {p0}, Lcom/google/protobuf/nano/ExtendableMessageNano;->computeSerializedSize()I

    move-result v1

    .line 152
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentDatabase$DocumentList;->entries:[Lcom/google/android/apps/chrome/document/DocumentDatabase$DocumentEntry;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentDatabase$DocumentList;->entries:[Lcom/google/android/apps/chrome/document/DocumentDatabase$DocumentEntry;

    array-length v0, v0

    if-lez v0, :cond_1

    .line 153
    const/4 v0, 0x0

    :goto_0
    iget-object v2, p0, Lcom/google/android/apps/chrome/document/DocumentDatabase$DocumentList;->entries:[Lcom/google/android/apps/chrome/document/DocumentDatabase$DocumentEntry;

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 154
    iget-object v2, p0, Lcom/google/android/apps/chrome/document/DocumentDatabase$DocumentList;->entries:[Lcom/google/android/apps/chrome/document/DocumentDatabase$DocumentEntry;

    aget-object v2, v2, v0

    .line 155
    if-eqz v2, :cond_0

    .line 156
    const/4 v3, 0x1

    invoke-static {v3, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v2

    add-int/2addr v1, v2

    .line 153
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 161
    :cond_1
    return v1
.end method

.method public final mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/apps/chrome/document/DocumentDatabase$DocumentList;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 169
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->a()I

    move-result v0

    .line 170
    sparse-switch v0, :sswitch_data_0

    .line 174
    invoke-virtual {p0, p1, v0}, Lcom/google/android/apps/chrome/document/DocumentDatabase$DocumentList;->storeUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 175
    :sswitch_0
    return-object p0

    .line 180
    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->a(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v2

    .line 182
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentDatabase$DocumentList;->entries:[Lcom/google/android/apps/chrome/document/DocumentDatabase$DocumentEntry;

    if-nez v0, :cond_2

    move v0, v1

    .line 183
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/android/apps/chrome/document/DocumentDatabase$DocumentEntry;

    .line 185
    if-eqz v0, :cond_1

    .line 186
    iget-object v3, p0, Lcom/google/android/apps/chrome/document/DocumentDatabase$DocumentList;->entries:[Lcom/google/android/apps/chrome/document/DocumentDatabase$DocumentEntry;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 188
    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    .line 189
    new-instance v3, Lcom/google/android/apps/chrome/document/DocumentDatabase$DocumentEntry;

    invoke-direct {v3}, Lcom/google/android/apps/chrome/document/DocumentDatabase$DocumentEntry;-><init>()V

    aput-object v3, v2, v0

    .line 190
    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->a(Lcom/google/protobuf/nano/MessageNano;)V

    .line 191
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->a()I

    .line 188
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 182
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentDatabase$DocumentList;->entries:[Lcom/google/android/apps/chrome/document/DocumentDatabase$DocumentEntry;

    array-length v0, v0

    goto :goto_1

    .line 194
    :cond_3
    new-instance v3, Lcom/google/android/apps/chrome/document/DocumentDatabase$DocumentEntry;

    invoke-direct {v3}, Lcom/google/android/apps/chrome/document/DocumentDatabase$DocumentEntry;-><init>()V

    aput-object v3, v2, v0

    .line 195
    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->a(Lcom/google/protobuf/nano/MessageNano;)V

    .line 196
    iput-object v2, p0, Lcom/google/android/apps/chrome/document/DocumentDatabase$DocumentList;->entries:[Lcom/google/android/apps/chrome/document/DocumentDatabase$DocumentEntry;

    goto :goto_0

    .line 170
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
    .end sparse-switch
.end method

.method public final bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1

    .prologue
    .line 104
    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/document/DocumentDatabase$DocumentList;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/apps/chrome/document/DocumentDatabase$DocumentList;

    move-result-object v0

    return-object v0
.end method

.method public final writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 3

    .prologue
    .line 138
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentDatabase$DocumentList;->entries:[Lcom/google/android/apps/chrome/document/DocumentDatabase$DocumentEntry;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentDatabase$DocumentList;->entries:[Lcom/google/android/apps/chrome/document/DocumentDatabase$DocumentEntry;

    array-length v0, v0

    if-lez v0, :cond_1

    .line 139
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/chrome/document/DocumentDatabase$DocumentList;->entries:[Lcom/google/android/apps/chrome/document/DocumentDatabase$DocumentEntry;

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 140
    iget-object v1, p0, Lcom/google/android/apps/chrome/document/DocumentDatabase$DocumentList;->entries:[Lcom/google/android/apps/chrome/document/DocumentDatabase$DocumentEntry;

    aget-object v1, v1, v0

    .line 141
    if-eqz v1, :cond_0

    .line 142
    const/4 v2, 0x1

    invoke-virtual {p1, v2, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/MessageNano;)V

    .line 139
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 146
    :cond_1
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/ExtendableMessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 147
    return-void
.end method
