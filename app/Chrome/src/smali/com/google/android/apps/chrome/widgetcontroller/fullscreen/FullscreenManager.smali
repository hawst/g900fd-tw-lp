.class public abstract Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;
.super Ljava/lang/Object;
.source "FullscreenManager.java"


# static fields
.field public static final INVALID_TOKEN:I = -0x1


# instance fields
.field private final mHtmlApiHandler:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenHtmlApiHandler;

.field private final mModelSelector:Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

.field private mOverlayVideoMode:Z


# direct methods
.method public constructor <init>(Landroid/view/Window;Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;ZZ)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    iput-object p2, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->mModelSelector:Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

    .line 43
    new-instance v2, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenHtmlApiHandler;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->createApiDelegate()Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenHtmlApiHandler$FullscreenHtmlApiDelegate;

    move-result-object v3

    if-eqz p3, :cond_0

    if-eqz p4, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-direct {v2, p1, v3, v0}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenHtmlApiHandler;-><init>(Landroid/view/Window;Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenHtmlApiHandler$FullscreenHtmlApiDelegate;Z)V

    iput-object v2, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->mHtmlApiHandler:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenHtmlApiHandler;

    .line 45
    iput-boolean v1, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->mOverlayVideoMode:Z

    .line 46
    return-void

    :cond_0
    move v0, v1

    .line 43
    goto :goto_0
.end method


# virtual methods
.method protected abstract createApiDelegate()Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenHtmlApiHandler$FullscreenHtmlApiDelegate;
.end method

.method protected getHtmlApiHandler()Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenHtmlApiHandler;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->mHtmlApiHandler:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenHtmlApiHandler;

    return-object v0
.end method

.method public getPersistentFullscreenMode()Z
    .locals 1

    .prologue
    .line 161
    iget-object v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->mHtmlApiHandler:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenHtmlApiHandler;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenHtmlApiHandler;->getPersistentFullscreenMode()Z

    move-result v0

    return v0
.end method

.method protected getTabModelSelector()Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->mModelSelector:Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

    return-object v0
.end method

.method public abstract hideControlsPersistent(I)V
.end method

.method public isOverlayVideoMode()Z
    .locals 1

    .prologue
    .line 115
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->mOverlayVideoMode:Z

    return v0
.end method

.method public onContentViewScrollingStateChanged(Z)V
    .locals 0

    .prologue
    .line 185
    return-void
.end method

.method public onContentViewSystemUiVisibilityChange(I)V
    .locals 1

    .prologue
    .line 170
    iget-object v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->mHtmlApiHandler:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenHtmlApiHandler;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenHtmlApiHandler;->onContentViewSystemUiVisibilityChange(I)V

    .line 171
    return-void
.end method

.method public onWindowFocusChanged(Z)V
    .locals 1

    .prologue
    .line 178
    iget-object v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->mHtmlApiHandler:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenHtmlApiHandler;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenHtmlApiHandler;->onWindowFocusChanged(Z)V

    .line 179
    return-void
.end method

.method public setOverlayVideoMode(Z)V
    .locals 0

    .prologue
    .line 108
    iput-boolean p1, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->mOverlayVideoMode:Z

    .line 109
    return-void
.end method

.method public setPersistentFullscreenMode(Z)V
    .locals 3

    .prologue
    .line 146
    iget-object v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->mHtmlApiHandler:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenHtmlApiHandler;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenHtmlApiHandler;->setPersistentFullscreenMode(Z)V

    .line 148
    iget-object v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->mModelSelector:Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

    invoke-interface {v0}, Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;->getCurrentTab()Lorg/chromium/chrome/browser/Tab;

    move-result-object v0

    .line 149
    if-nez p1, :cond_0

    iget-boolean v1, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->mOverlayVideoMode:Z

    if-eqz v1, :cond_0

    .line 150
    invoke-static {}, Lorg/chromium/content/browser/ContentVideoView;->getContentVideoView()Lorg/chromium/content/browser/ContentVideoView;

    move-result-object v1

    .line 151
    if-eqz v1, :cond_0

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lorg/chromium/content/browser/ContentVideoView;->exitFullscreen(Z)V

    .line 153
    :cond_0
    if-eqz v0, :cond_1

    invoke-static {v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->fromTab(Lorg/chromium/chrome/browser/Tab;)Lcom/google/android/apps/chrome/tab/ChromeTab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->updateFullscreenEnabledState()V

    .line 154
    :cond_1
    return-void
.end method

.method public abstract setPositionsForTab(FF)V
.end method

.method public abstract setPositionsForTabToNonFullscreen()V
.end method

.method public abstract showControlsPersistentAndClearOldToken(I)I
.end method

.method public abstract showControlsTransient()V
.end method

.method public abstract updateContentViewChildrenState()V
.end method
