.class Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar$2;
.super Ljava/lang/Object;
.source "FindToolbar.java"

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;)V
    .locals 0

    .prologue
    .line 128
    iput-object p1, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar$2;->this$0:Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 0

    .prologue
    .line 162
    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 158
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 132
    iget-object v3, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar$2;->this$0:Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;

    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-lez v0, :cond_1

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->setPrevNextEnabled(Z)V

    .line 134
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar$2;->this$0:Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;

    # getter for: Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->mSettingFindTextProgrammatically:Z
    invoke-static {v0}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->access$300(Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 153
    :cond_0
    :goto_1
    return-void

    :cond_1
    move v0, v2

    .line 132
    goto :goto_0

    .line 138
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar$2;->this$0:Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;

    # getter for: Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->mCurrentTab:Lcom/google/android/apps/chrome/tab/ChromeTab;
    invoke-static {v0}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->access$400(Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;)Lcom/google/android/apps/chrome/tab/ChromeTab;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar$2;->this$0:Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;

    # getter for: Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->mCurrentTab:Lcom/google/android/apps/chrome/tab/ChromeTab;
    invoke-static {v0}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->access$400(Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;)Lcom/google/android/apps/chrome/tab/ChromeTab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->supportsFinding()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 140
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-lez v0, :cond_3

    .line 143
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar$2;->this$0:Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;

    # setter for: Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->mSearchKeyShouldTriggerSearch:Z
    invoke-static {v0, v2}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->access$202(Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;Z)Z

    .line 144
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar$2;->this$0:Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;

    # getter for: Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->mCurrentTab:Lcom/google/android/apps/chrome/tab/ChromeTab;
    invoke-static {v0}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->access$400(Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;)Lcom/google/android/apps/chrome/tab/ChromeTab;

    move-result-object v0

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3, v1, v2}, Lcom/google/android/apps/chrome/tab/ChromeTab;->startFinding(Ljava/lang/String;ZZ)V

    .line 150
    :goto_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar$2;->this$0:Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;

    # getter for: Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->mCurrentTab:Lcom/google/android/apps/chrome/tab/ChromeTab;
    invoke-static {v0}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->access$400(Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;)Lcom/google/android/apps/chrome/tab/ChromeTab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->isIncognito()Z

    move-result v0

    if-nez v0, :cond_0

    .line 151
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar$2;->this$0:Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    # setter for: Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->mLastUserSearch:Ljava/lang/String;
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->access$502(Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;Ljava/lang/String;)Ljava/lang/String;

    goto :goto_1

    .line 146
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar$2;->this$0:Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->clearResults()V

    .line 147
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar$2;->this$0:Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;

    # getter for: Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->mCurrentTab:Lcom/google/android/apps/chrome/tab/ChromeTab;
    invoke-static {v0}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->access$400(Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;)Lcom/google/android/apps/chrome/tab/ChromeTab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->stopFinding()V

    goto :goto_2
.end method
