.class public Lcom/google/android/apps/chrome/widget/ControlContainer;
.super Landroid/widget/FrameLayout;
.source "ControlContainer.java"


# instance fields
.field private final mNotificationHandler:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;

.field private mReadyForBitmapCapture:Z

.field private mResourceAdapter:Lcom/google/android/apps/chrome/compositor/resources/dynamics/ViewResourceAdapter;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 54
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 28
    new-instance v0, Lcom/google/android/apps/chrome/widget/ControlContainer$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/widget/ControlContainer$1;-><init>(Lcom/google/android/apps/chrome/widget/ControlContainer;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/widget/ControlContainer;->mNotificationHandler:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;

    .line 56
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/widget/ControlContainer;->createResourceAdapter()Lcom/google/android/apps/chrome/compositor/resources/dynamics/ViewResourceAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/widget/ControlContainer;->mResourceAdapter:Lcom/google/android/apps/chrome/compositor/resources/dynamics/ViewResourceAdapter;

    .line 57
    return-void
.end method

.method static synthetic access$002(Lcom/google/android/apps/chrome/widget/ControlContainer;Z)Z
    .locals 0

    .prologue
    .line 24
    iput-boolean p1, p0, Lcom/google/android/apps/chrome/widget/ControlContainer;->mReadyForBitmapCapture:Z

    return p1
.end method

.method static synthetic access$100(Lcom/google/android/apps/chrome/widget/ControlContainer;)Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/ControlContainer;->mNotificationHandler:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;

    return-object v0
.end method


# virtual methods
.method protected createResourceAdapter()Lcom/google/android/apps/chrome/compositor/resources/dynamics/ViewResourceAdapter;
    .locals 1

    .prologue
    .line 63
    new-instance v0, Lcom/google/android/apps/chrome/widget/ControlContainer$ControlContainerResourceAdapter;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/widget/ControlContainer$ControlContainerResourceAdapter;-><init>(Lcom/google/android/apps/chrome/widget/ControlContainer;)V

    return-object v0
.end method

.method public getResourceAdapter()Lcom/google/android/apps/chrome/compositor/resources/dynamics/ViewResourceAdapter;
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/ControlContainer;->mResourceAdapter:Lcom/google/android/apps/chrome/compositor/resources/dynamics/ViewResourceAdapter;

    return-object v0
.end method

.method public invalidateChildInParent([ILandroid/graphics/Rect;)Landroid/view/ViewParent;
    .locals 2

    .prologue
    .line 75
    invoke-super {p0, p1, p2}, Landroid/widget/FrameLayout;->invalidateChildInParent([ILandroid/graphics/Rect;)Landroid/view/ViewParent;

    move-result-object v0

    .line 76
    iget-boolean v1, p0, Lcom/google/android/apps/chrome/widget/ControlContainer;->mReadyForBitmapCapture:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/ControlContainer;->mResourceAdapter:Lcom/google/android/apps/chrome/compositor/resources/dynamics/ViewResourceAdapter;

    invoke-virtual {v1, p2}, Lcom/google/android/apps/chrome/compositor/resources/dynamics/ViewResourceAdapter;->invalidate(Landroid/graphics/Rect;)V

    .line 77
    :cond_0
    return-object v0
.end method

.method protected onAttachedToWindow()V
    .locals 3

    .prologue
    .line 82
    invoke-super {p0}, Landroid/widget/FrameLayout;->onAttachedToWindow()V

    .line 83
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/widget/ControlContainer;->mReadyForBitmapCapture:Z

    if-nez v0, :cond_0

    .line 84
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/widget/ControlContainer;->getContext()Landroid/content/Context;

    move-result-object v0

    const/16 v1, 0x19

    iget-object v2, p0, Lcom/google/android/apps/chrome/widget/ControlContainer;->mNotificationHandler:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->registerForNotification(Landroid/content/Context;ILcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;)V

    .line 87
    :cond_0
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 3

    .prologue
    .line 91
    invoke-super {p0}, Landroid/widget/FrameLayout;->onDetachedFromWindow()V

    .line 92
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/widget/ControlContainer;->getContext()Landroid/content/Context;

    move-result-object v0

    const/16 v1, 0x19

    iget-object v2, p0, Lcom/google/android/apps/chrome/widget/ControlContainer;->mNotificationHandler:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->unregisterForNotification(Landroid/content/Context;ILcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;)V

    .line 94
    return-void
.end method
