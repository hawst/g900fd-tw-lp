.class Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$RecentlyClosedTabsGroup;
.super Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$Group;
.source "RecentTabsRowAdapter.java"


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;)V
    .locals 0

    .prologue
    .line 449
    iput-object p1, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$RecentlyClosedTabsGroup;->this$0:Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;

    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$Group;-><init>(Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;)V

    return-void
.end method

.method private isHistoryLink(I)Z
    .locals 1

    .prologue
    .line 475
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$RecentlyClosedTabsGroup;->this$0:Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;

    # getter for: Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;->mRecentTabsManager:Lcom/google/android/apps/chrome/ntp/RecentTabsManager;
    invoke-static {v0}, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;->access$300(Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;)Lcom/google/android/apps/chrome/ntp/RecentTabsManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ntp/RecentTabsManager;->getRecentlyClosedTabs()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ne p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public configureChildView(ILcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$ViewHolder;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 488
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$RecentlyClosedTabsGroup;->isHistoryLink(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 489
    iget-object v0, p2, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$ViewHolder;->textView:Landroid/widget/TextView;

    sget v1, Lcom/google/android/apps/chrome/R$string;->show_full_history:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 490
    iget-object v0, p2, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$ViewHolder;->textView:Landroid/widget/TextView;

    sget v1, Lcom/google/android/apps/chrome/R$drawable;->history_favicon:I

    invoke-static {v0, v1, v2, v2, v2}, Lorg/chromium/base/ApiCompatibilityUtils;->setCompoundDrawablesRelativeWithIntrinsicBounds(Landroid/widget/TextView;IIII)V

    .line 498
    :goto_0
    return-void

    .line 493
    :cond_0
    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$RecentlyClosedTabsGroup;->getChild(I)Lorg/chromium/chrome/browser/RecentlyClosedBridge$RecentlyClosedTab;

    move-result-object v0

    .line 494
    iget-object v1, v0, Lorg/chromium/chrome/browser/RecentlyClosedBridge$RecentlyClosedTab;->title:Ljava/lang/String;

    iget-object v2, v0, Lorg/chromium/chrome/browser/RecentlyClosedBridge$RecentlyClosedTab;->url:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->getTitleForDisplay(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 495
    iget-object v2, p2, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$ViewHolder;->textView:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 496
    iget-object v1, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$RecentlyClosedTabsGroup;->this$0:Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;

    iget-object v0, v0, Lorg/chromium/chrome/browser/RecentlyClosedBridge$RecentlyClosedTab;->url:Ljava/lang/String;

    # invokes: Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;->loadLocalFavicon(Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$ViewHolder;Ljava/lang/String;)V
    invoke-static {v1, p2, v0}, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;->access$400(Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$ViewHolder;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public configureGroupView(Lcom/google/android/apps/chrome/ntp/RecentTabsGroupView;Z)V
    .locals 0

    .prologue
    .line 502
    invoke-virtual {p1, p2}, Lcom/google/android/apps/chrome/ntp/RecentTabsGroupView;->configureForRecentlyClosedTabs(Z)V

    .line 503
    return-void
.end method

.method public bridge synthetic getChild(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 449
    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$RecentlyClosedTabsGroup;->getChild(I)Lorg/chromium/chrome/browser/RecentlyClosedBridge$RecentlyClosedTab;

    move-result-object v0

    return-object v0
.end method

.method public getChild(I)Lorg/chromium/chrome/browser/RecentlyClosedBridge$RecentlyClosedTab;
    .locals 1

    .prologue
    .line 480
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$RecentlyClosedTabsGroup;->isHistoryLink(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 481
    const/4 v0, 0x0

    .line 483
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$RecentlyClosedTabsGroup;->this$0:Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;

    # getter for: Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;->mRecentTabsManager:Lcom/google/android/apps/chrome/ntp/RecentTabsManager;
    invoke-static {v0}, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;->access$300(Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;)Lcom/google/android/apps/chrome/ntp/RecentTabsManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ntp/RecentTabsManager;->getRecentlyClosedTabs()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/chromium/chrome/browser/RecentlyClosedBridge$RecentlyClosedTab;

    goto :goto_0
.end method

.method public getChildType()Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$ChildType;
    .locals 1

    .prologue
    .line 467
    sget-object v0, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$ChildType;->DEFAULT_CONTENT:Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$ChildType;

    return-object v0
.end method

.method public getChildrenCount()I
    .locals 1

    .prologue
    .line 462
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$RecentlyClosedTabsGroup;->this$0:Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;

    # getter for: Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;->mRecentTabsManager:Lcom/google/android/apps/chrome/ntp/RecentTabsManager;
    invoke-static {v0}, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;->access$300(Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;)Lcom/google/android/apps/chrome/ntp/RecentTabsManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ntp/RecentTabsManager;->getRecentlyClosedTabs()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public getGroupType()Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$GroupType;
    .locals 1

    .prologue
    .line 455
    sget-object v0, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$GroupType;->CONTENT:Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$GroupType;

    return-object v0
.end method

.method public isCollapsed()Z
    .locals 1

    .prologue
    .line 512
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$RecentlyClosedTabsGroup;->this$0:Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;

    # getter for: Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;->mRecentTabsManager:Lcom/google/android/apps/chrome/ntp/RecentTabsManager;
    invoke-static {v0}, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;->access$300(Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;)Lcom/google/android/apps/chrome/ntp/RecentTabsManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ntp/RecentTabsManager;->isRecentlyClosedTabsCollapsed()Z

    move-result v0

    return v0
.end method

.method public onChildClick(I)Z
    .locals 3

    .prologue
    .line 517
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$RecentlyClosedTabsGroup;->isHistoryLink(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 518
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$RecentlyClosedTabsGroup;->this$0:Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;

    # getter for: Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;->mRecentTabsManager:Lcom/google/android/apps/chrome/ntp/RecentTabsManager;
    invoke-static {v0}, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;->access$300(Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;)Lcom/google/android/apps/chrome/ntp/RecentTabsManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ntp/RecentTabsManager;->openHistoryPage()V

    .line 523
    :goto_0
    const/4 v0, 0x1

    return v0

    .line 520
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$RecentlyClosedTabsGroup;->this$0:Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;

    # getter for: Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;->mRecentTabsManager:Lcom/google/android/apps/chrome/ntp/RecentTabsManager;
    invoke-static {v0}, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;->access$300(Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;)Lcom/google/android/apps/chrome/ntp/RecentTabsManager;

    move-result-object v0

    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$RecentlyClosedTabsGroup;->getChild(I)Lorg/chromium/chrome/browser/RecentlyClosedBridge$RecentlyClosedTab;

    move-result-object v1

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/chrome/ntp/RecentTabsManager;->openRecentlyClosedTab(Lorg/chromium/chrome/browser/RecentlyClosedBridge$RecentlyClosedTab;I)V

    goto :goto_0
.end method

.method public onCreateContextMenuForChild(ILandroid/view/ContextMenu;Landroid/app/Activity;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 533
    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$RecentlyClosedTabsGroup;->getChild(I)Lorg/chromium/chrome/browser/RecentlyClosedBridge$RecentlyClosedTab;

    move-result-object v0

    .line 534
    if-nez v0, :cond_0

    .line 556
    :goto_0
    return-void

    .line 535
    :cond_0
    new-instance v1, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$RecentlyClosedTabsGroup$1;

    invoke-direct {v1, p0, v0}, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$RecentlyClosedTabsGroup$1;-><init>(Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$RecentlyClosedTabsGroup;Lorg/chromium/chrome/browser/RecentlyClosedBridge$RecentlyClosedTab;)V

    .line 552
    const/4 v0, 0x1

    sget v2, Lcom/google/android/apps/chrome/R$string;->contextmenu_open_in_new_tab:I

    invoke-interface {p2, v3, v0, v3, v2}, Landroid/view/ContextMenu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 554
    const/4 v0, 0x2

    sget v2, Lcom/google/android/apps/chrome/R$string;->remove_all:I

    invoke-interface {p2, v3, v0, v3, v2}, Landroid/view/ContextMenu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    goto :goto_0
.end method

.method public onCreateContextMenuForGroup(Landroid/view/ContextMenu;Landroid/app/Activity;)V
    .locals 0

    .prologue
    .line 528
    return-void
.end method

.method public setCollapsed(Z)V
    .locals 1

    .prologue
    .line 507
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$RecentlyClosedTabsGroup;->this$0:Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;

    # getter for: Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;->mRecentTabsManager:Lcom/google/android/apps/chrome/ntp/RecentTabsManager;
    invoke-static {v0}, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;->access$300(Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;)Lcom/google/android/apps/chrome/ntp/RecentTabsManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/ntp/RecentTabsManager;->setRecentlyClosedTabsCollapsed(Z)V

    .line 508
    return-void
.end method
