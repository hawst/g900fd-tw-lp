.class Lcom/google/android/apps/chrome/document/DocumentTabList$4;
.super Landroid/os/AsyncTask;
.source "DocumentTabList.java"


# instance fields
.field private mCurrentTabs:Landroid/util/SparseArray;

.field final synthetic this$0:Lcom/google/android/apps/chrome/document/DocumentTabList;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/document/DocumentTabList;)V
    .locals 0

    .prologue
    .line 808
    iput-object p1, p0, Lcom/google/android/apps/chrome/document/DocumentTabList$4;->this$0:Lcom/google/android/apps/chrome/document/DocumentTabList;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 808
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/document/DocumentTabList$4;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 8

    .prologue
    .line 819
    invoke-static {}, Lcom/google/android/apps/chrome/document/DocumentTabList;->getStateDirectory()Ljava/io/File;

    move-result-object v2

    .line 820
    invoke-virtual {v2}, Ljava/io/File;->list()[Ljava/lang/String;

    move-result-object v3

    .line 821
    array-length v4, v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_1

    aget-object v5, v3, v1

    .line 822
    invoke-static {v5}, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->parseStateDataFromFilename(Ljava/lang/String;)Landroid/util/Pair;

    move-result-object v6

    .line 824
    if-eqz v6, :cond_0

    .line 826
    iget-object v0, v6, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v7

    .line 827
    iget-object v0, v6, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 828
    iget-object v6, p0, Lcom/google/android/apps/chrome/document/DocumentTabList$4;->this$0:Lcom/google/android/apps/chrome/document/DocumentTabList;

    invoke-virtual {v6}, Lcom/google/android/apps/chrome/document/DocumentTabList;->isIncognito()Z

    move-result v6

    if-ne v0, v6, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentTabList$4;->mCurrentTabs:Landroid/util/SparseArray;

    invoke-virtual {v0, v7}, Landroid/util/SparseArray;->indexOfKey(I)I

    move-result v0

    if-gez v0, :cond_0

    .line 829
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v2, v5}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    move-result v0

    .line 830
    if-nez v0, :cond_0

    const-string/jumbo v0, "DocumentTabList"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string/jumbo v7, "Failed to delete: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v0, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 821
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 833
    :cond_1
    const/4 v0, 0x0

    return-object v0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 808
    check-cast p1, Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/document/DocumentTabList$4;->onPostExecute(Ljava/lang/Void;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/Void;)V
    .locals 2

    .prologue
    .line 838
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentTabList$4;->this$0:Lcom/google/android/apps/chrome/document/DocumentTabList;

    const/16 v1, 0xa

    # invokes: Lcom/google/android/apps/chrome/document/DocumentTabList;->setCurrentState(I)V
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/document/DocumentTabList;->access$100(Lcom/google/android/apps/chrome/document/DocumentTabList;I)V

    .line 839
    return-void
.end method

.method protected onPreExecute()V
    .locals 2

    .prologue
    .line 813
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentTabList$4;->this$0:Lcom/google/android/apps/chrome/document/DocumentTabList;

    const/16 v1, 0x9

    # invokes: Lcom/google/android/apps/chrome/document/DocumentTabList;->setCurrentState(I)V
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/document/DocumentTabList;->access$100(Lcom/google/android/apps/chrome/document/DocumentTabList;I)V

    .line 814
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentTabList$4;->this$0:Lcom/google/android/apps/chrome/document/DocumentTabList;

    # getter for: Lcom/google/android/apps/chrome/document/DocumentTabList;->mDelegate:Lcom/google/android/apps/chrome/document/DocumentTabList$Delegate;
    invoke-static {v0}, Lcom/google/android/apps/chrome/document/DocumentTabList;->access$1200(Lcom/google/android/apps/chrome/document/DocumentTabList;)Lcom/google/android/apps/chrome/document/DocumentTabList$Delegate;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/document/DocumentTabList$4;->this$0:Lcom/google/android/apps/chrome/document/DocumentTabList;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/document/DocumentTabList;->isIncognito()Z

    move-result v1

    invoke-interface {v0, v1}, Lcom/google/android/apps/chrome/document/DocumentTabList$Delegate;->getTasksFromRecents(Z)Landroid/util/SparseArray;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentTabList$4;->mCurrentTabs:Landroid/util/SparseArray;

    .line 815
    return-void
.end method
