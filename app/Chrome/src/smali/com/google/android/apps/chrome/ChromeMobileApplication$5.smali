.class Lcom/google/android/apps/chrome/ChromeMobileApplication$5;
.super Ljava/lang/Object;
.source "ChromeMobileApplication.java"

# interfaces
.implements Lorg/chromium/base/ApplicationStatus$ApplicationStateListener;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/ChromeMobileApplication;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/ChromeMobileApplication;)V
    .locals 0

    .prologue
    .line 681
    iput-object p1, p0, Lcom/google/android/apps/chrome/ChromeMobileApplication$5;->this$0:Lcom/google/android/apps/chrome/ChromeMobileApplication;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onApplicationStateChange(I)V
    .locals 1

    .prologue
    .line 684
    const/4 v0, 0x1

    if-ne p1, v0, :cond_1

    .line 685
    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeMobileApplication$5;->this$0:Lcom/google/android/apps/chrome/ChromeMobileApplication;

    invoke-static {v0}, Lcom/google/android/apps/chrome/ChromeMobileApplication;->access$700(Lcom/google/android/apps/chrome/ChromeMobileApplication;)V

    .line 691
    :cond_0
    :goto_0
    return-void

    .line 686
    :cond_1
    const/4 v0, 0x3

    if-ne p1, v0, :cond_2

    .line 687
    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeMobileApplication$5;->this$0:Lcom/google/android/apps/chrome/ChromeMobileApplication;

    # invokes: Lcom/google/android/apps/chrome/ChromeMobileApplication;->onForegroundSessionEnd()V
    invoke-static {v0}, Lcom/google/android/apps/chrome/ChromeMobileApplication;->access$800(Lcom/google/android/apps/chrome/ChromeMobileApplication;)V

    goto :goto_0

    .line 688
    :cond_2
    const/4 v0, 0x4

    if-ne p1, v0, :cond_0

    .line 689
    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeMobileApplication$5;->this$0:Lcom/google/android/apps/chrome/ChromeMobileApplication;

    # invokes: Lcom/google/android/apps/chrome/ChromeMobileApplication;->onForegroundActivityDestroyed()V
    invoke-static {v0}, Lcom/google/android/apps/chrome/ChromeMobileApplication;->access$900(Lcom/google/android/apps/chrome/ChromeMobileApplication;)V

    goto :goto_0
.end method
