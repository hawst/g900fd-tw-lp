.class public Lcom/google/android/apps/chrome/toolbar/ToolbarManager;
.super Ljava/lang/Object;
.source "ToolbarManager.java"

# interfaces
.implements Lcom/google/android/apps/chrome/omnibox/UrlFocusChangeListener;
.implements Lcom/google/android/apps/chrome/toolbar/ToolbarTabController;


# static fields
.field static final synthetic $assertionsDisabled:Z

.field public static final MINIMUM_LOAD_PROGRESS:I = 0x5

.field private static final NOTIFICATIONS:[I


# instance fields
.field private mAppMenuButtonHelper:Lorg/chromium/chrome/browser/appmenu/AppMenuButtonHelper;

.field private mBookmarksBridge:Lorg/chromium/chrome/browser/BookmarksBridge;

.field private final mBookmarksObserver:Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkModelObserver;

.field private mCurrentProfile:Lorg/chromium/chrome/browser/profiles/Profile;

.field private mFullscreenFindInPageToken:I

.field private mFullscreenFocusToken:I

.field private mFullscreenManager:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;

.field private mFullscreenMenuToken:I

.field private final mLoadProgressSimulator:Lcom/google/android/apps/chrome/toolbar/ToolbarManager$LoadProgressSimulator;

.field private final mLocationBar:Lcom/google/android/apps/chrome/omnibox/LocationBar;

.field private mMenuDelegatePhone:Lcom/google/android/apps/chrome/toolbar/ToolbarManager$MenuDelegatePhone;

.field private mNativeLibraryReady:Z

.field private final mNotificationHandler:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;

.field private mPreselectedTabId:I

.field private mShowReaderModeIcon:Z

.field private mTabModelSelector:Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

.field private final mTabObserver:Lorg/chromium/chrome/browser/TabObserver;

.field private mTabRestoreCompleted:Z

.field private final mToolbarDelegate:Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;

.field private final mToolbarModel:Lcom/google/android/apps/chrome/toolbar/ToolbarModelImpl;

.field private final mToolbarView:Landroid/view/View;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 50
    const-class v0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->$assertionsDisabled:Z

    .line 545
    const/16 v0, 0x18

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->NOTIFICATIONS:[I

    return-void

    .line 50
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 545
    nop

    :array_0
    .array-data 4
        0x0
        0xf
        0x1
        0x2
        0x3
        0x5
        0x6
        0x16
        0x23
        0x8
        0x9
        0x1c
        0xb
        0xc
        0x19
        0x15
        0x12
        0x2a
        0x3b
        0x2d
        0x48
        0x46
        0x45
        0x4f
    .end array-data
.end method

.method public constructor <init>(Lcom/google/android/apps/chrome/toolbar/Toolbar;)V
    .locals 3

    .prologue
    const/4 v0, -0x1

    .line 106
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 91
    iput v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->mFullscreenFocusToken:I

    .line 92
    iput v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->mFullscreenFindInPageToken:I

    .line 93
    iput v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->mFullscreenMenuToken:I

    .line 95
    iput v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->mPreselectedTabId:I

    .line 572
    new-instance v0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager$7;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/toolbar/ToolbarManager$7;-><init>(Lcom/google/android/apps/chrome/toolbar/ToolbarManager;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->mNotificationHandler:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;

    .line 107
    new-instance v0, Lcom/google/android/apps/chrome/toolbar/ToolbarModelImpl;

    invoke-direct {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarModelImpl;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->mToolbarModel:Lcom/google/android/apps/chrome/toolbar/ToolbarModelImpl;

    .line 108
    invoke-interface {p1}, Lcom/google/android/apps/chrome/toolbar/Toolbar;->getView()Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->mToolbarView:Landroid/view/View;

    .line 109
    invoke-interface {p1}, Lcom/google/android/apps/chrome/toolbar/Toolbar;->getDelegate()Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->mToolbarDelegate:Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;

    .line 111
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->mToolbarView:Landroid/view/View;

    sget v1, Lcom/google/android/apps/chrome/R$id;->location_bar:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/omnibox/LocationBar;

    iput-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->mLocationBar:Lcom/google/android/apps/chrome/omnibox/LocationBar;

    .line 112
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->mLocationBar:Lcom/google/android/apps/chrome/omnibox/LocationBar;

    iget-object v1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->mToolbarDelegate:Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;

    iget-object v2, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->mToolbarModel:Lcom/google/android/apps/chrome/toolbar/ToolbarModelImpl;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->setToolbar(Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;Lcom/google/android/apps/chrome/toolbar/ToolbarDataProvider;)V

    .line 113
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->mLocationBar:Lcom/google/android/apps/chrome/omnibox/LocationBar;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->setUrlFocusChangeListener(Lcom/google/android/apps/chrome/omnibox/UrlFocusChangeListener;)V

    .line 115
    new-instance v0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/toolbar/ToolbarManager$1;-><init>(Lcom/google/android/apps/chrome/toolbar/ToolbarManager;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->mBookmarksObserver:Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkModelObserver;

    .line 122
    new-instance v0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager$2;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/toolbar/ToolbarManager$2;-><init>(Lcom/google/android/apps/chrome/toolbar/ToolbarManager;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->mTabObserver:Lorg/chromium/chrome/browser/TabObserver;

    .line 146
    new-instance v0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager$LoadProgressSimulator;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/toolbar/ToolbarManager$LoadProgressSimulator;-><init>(Lcom/google/android/apps/chrome/toolbar/ToolbarManager;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->mLoadProgressSimulator:Lcom/google/android/apps/chrome/toolbar/ToolbarManager$LoadProgressSimulator;

    .line 150
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->mToolbarView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    const/16 v1, 0x10

    new-instance v2, Lcom/google/android/apps/chrome/toolbar/ToolbarManager$3;

    invoke-direct {v2, p0}, Lcom/google/android/apps/chrome/toolbar/ToolbarManager$3;-><init>(Lcom/google/android/apps/chrome/toolbar/ToolbarManager;)V

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->registerForNotification(Landroid/content/Context;ILcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;)V

    .line 163
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/chrome/toolbar/ToolbarManager;)V
    .locals 0

    .prologue
    .line 50
    invoke-direct {p0}, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->updateBookmarkButtonStatus()V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/apps/chrome/toolbar/ToolbarManager;)Lcom/google/android/apps/chrome/toolbar/ToolbarModelImpl;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->mToolbarModel:Lcom/google/android/apps/chrome/toolbar/ToolbarModelImpl;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/google/android/apps/chrome/toolbar/ToolbarManager;)Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->mTabModelSelector:Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/google/android/apps/chrome/toolbar/ToolbarManager;)V
    .locals 0

    .prologue
    .line 50
    invoke-direct {p0}, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->refreshSelectedTab()V

    return-void
.end method

.method static synthetic access$1200(Lcom/google/android/apps/chrome/toolbar/ToolbarManager;)Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->mFullscreenManager:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/google/android/apps/chrome/toolbar/ToolbarManager;)I
    .locals 1

    .prologue
    .line 50
    iget v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->mFullscreenMenuToken:I

    return v0
.end method

.method static synthetic access$1302(Lcom/google/android/apps/chrome/toolbar/ToolbarManager;I)I
    .locals 0

    .prologue
    .line 50
    iput p1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->mFullscreenMenuToken:I

    return p1
.end method

.method static synthetic access$1400(Lcom/google/android/apps/chrome/toolbar/ToolbarManager;)V
    .locals 0

    .prologue
    .line 50
    invoke-direct {p0}, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->updateButtonStatus()V

    return-void
.end method

.method static synthetic access$1500(Lcom/google/android/apps/chrome/toolbar/ToolbarManager;)V
    .locals 0

    .prologue
    .line 50
    invoke-direct {p0}, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->updateTabCount()V

    return-void
.end method

.method static synthetic access$1602(Lcom/google/android/apps/chrome/toolbar/ToolbarManager;I)I
    .locals 0

    .prologue
    .line 50
    iput p1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->mPreselectedTabId:I

    return p1
.end method

.method static synthetic access$1700(Lcom/google/android/apps/chrome/toolbar/ToolbarManager;)I
    .locals 1

    .prologue
    .line 50
    iget v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->mFullscreenFindInPageToken:I

    return v0
.end method

.method static synthetic access$1702(Lcom/google/android/apps/chrome/toolbar/ToolbarManager;I)I
    .locals 0

    .prologue
    .line 50
    iput p1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->mFullscreenFindInPageToken:I

    return p1
.end method

.method static synthetic access$1800(Lcom/google/android/apps/chrome/toolbar/ToolbarManager;)V
    .locals 0

    .prologue
    .line 50
    invoke-direct {p0}, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->onTabCrash()V

    return-void
.end method

.method static synthetic access$1900(Lcom/google/android/apps/chrome/toolbar/ToolbarManager;ZZ)V
    .locals 0

    .prologue
    .line 50
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->updateTabLoadingState(ZZ)V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/apps/chrome/toolbar/ToolbarManager;)Lcom/google/android/apps/chrome/omnibox/LocationBar;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->mLocationBar:Lcom/google/android/apps/chrome/omnibox/LocationBar;

    return-object v0
.end method

.method static synthetic access$2000(Lcom/google/android/apps/chrome/toolbar/ToolbarManager;)V
    .locals 0

    .prologue
    .line 50
    invoke-direct {p0}, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->onPageLoadFinished()V

    return-void
.end method

.method static synthetic access$2100(Lcom/google/android/apps/chrome/toolbar/ToolbarManager;)V
    .locals 0

    .prologue
    .line 50
    invoke-direct {p0}, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->onPageLoadFailed()V

    return-void
.end method

.method static synthetic access$2200(Lcom/google/android/apps/chrome/toolbar/ToolbarManager;I)V
    .locals 0

    .prologue
    .line 50
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->updateLoadProgress(I)V

    return-void
.end method

.method static synthetic access$2300(Lcom/google/android/apps/chrome/toolbar/ToolbarManager;)Lcom/google/android/apps/chrome/toolbar/ToolbarManager$LoadProgressSimulator;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->mLoadProgressSimulator:Lcom/google/android/apps/chrome/toolbar/ToolbarManager$LoadProgressSimulator;

    return-object v0
.end method

.method static synthetic access$2400(Lcom/google/android/apps/chrome/toolbar/ToolbarManager;)V
    .locals 0

    .prologue
    .line 50
    invoke-direct {p0}, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->onReaderModeStatusChanged()V

    return-void
.end method

.method static synthetic access$2700(Lcom/google/android/apps/chrome/toolbar/ToolbarManager;I)V
    .locals 0

    .prologue
    .line 50
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->updateLoadProgressInternal(I)V

    return-void
.end method

.method static synthetic access$300(Lcom/google/android/apps/chrome/toolbar/ToolbarManager;)Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->mToolbarDelegate:Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;

    return-object v0
.end method

.method static synthetic access$402(Lcom/google/android/apps/chrome/toolbar/ToolbarManager;Z)Z
    .locals 0

    .prologue
    .line 50
    iput-boolean p1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->mTabRestoreCompleted:Z

    return p1
.end method

.method static synthetic access$500(Lcom/google/android/apps/chrome/toolbar/ToolbarManager;)V
    .locals 0

    .prologue
    .line 50
    invoke-direct {p0}, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->handleTabRestoreCompleted()V

    return-void
.end method

.method static synthetic access$600(Lcom/google/android/apps/chrome/toolbar/ToolbarManager;)Landroid/view/View;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->mToolbarView:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$700()[I
    .locals 1

    .prologue
    .line 50
    sget-object v0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->NOTIFICATIONS:[I

    return-object v0
.end method

.method static synthetic access$800(Lcom/google/android/apps/chrome/toolbar/ToolbarManager;)Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->mNotificationHandler:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;

    return-object v0
.end method

.method static synthetic access$900(Lcom/google/android/apps/chrome/toolbar/ToolbarManager;)Lorg/chromium/chrome/browser/BookmarksBridge;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->mBookmarksBridge:Lorg/chromium/chrome/browser/BookmarksBridge;

    return-object v0
.end method

.method static synthetic access$902(Lcom/google/android/apps/chrome/toolbar/ToolbarManager;Lorg/chromium/chrome/browser/BookmarksBridge;)Lorg/chromium/chrome/browser/BookmarksBridge;
    .locals 0

    .prologue
    .line 50
    iput-object p1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->mBookmarksBridge:Lorg/chromium/chrome/browser/BookmarksBridge;

    return-object p1
.end method

.method private handleTabRestoreCompleted()V
    .locals 1

    .prologue
    .line 235
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->mTabRestoreCompleted:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->mNativeLibraryReady:Z

    if-nez v0, :cond_1

    .line 238
    :cond_0
    :goto_0
    return-void

    .line 236
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->mToolbarDelegate:Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;->onStateRestored()V

    .line 237
    invoke-direct {p0}, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->updateTabCount()V

    goto :goto_0
.end method

.method private onNativeLibraryReady()V
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 220
    iput-boolean v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->mNativeLibraryReady:Z

    .line 221
    iget-object v1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->mToolbarDelegate:Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;->onNativeLibraryReady()V

    .line 223
    iget-object v1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->mToolbarView:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->NOTIFICATIONS:[I

    iget-object v3, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->mNotificationHandler:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;

    invoke-static {v1, v2, v3}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->registerForNotifications(Landroid/content/Context;[ILcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;)V

    .line 225
    invoke-direct {p0}, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->refreshSelectedTab()V

    .line 226
    invoke-direct {p0}, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->handleTabRestoreCompleted()V

    .line 228
    invoke-static {}, Lorg/chromium/base/CommandLine;->getInstance()Lorg/chromium/base/CommandLine;

    move-result-object v1

    const-string/jumbo v2, "enable-reader-mode-toolbar-icon"

    invoke-virtual {v1, v2}, Lorg/chromium/base/CommandLine;->hasSwitch(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->mToolbarView:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lorg/chromium/ui/base/DeviceFormFactor;->isTablet(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_0

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->mShowReaderModeIcon:Z

    .line 232
    return-void

    .line 228
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private onPageLoadFailed()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 490
    const/4 v0, 0x1

    invoke-direct {p0, v1, v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->updateTabLoadingState(ZZ)V

    .line 491
    invoke-direct {p0}, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->updateButtonStatus()V

    .line 492
    invoke-direct {p0, v1}, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->updateLoadProgressInternal(I)V

    .line 493
    return-void
.end method

.method private onPageLoadFinished()V
    .locals 4

    .prologue
    const/16 v3, 0x64

    const/4 v2, 0x0

    .line 474
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->mToolbarModel:Lcom/google/android/apps/chrome/toolbar/ToolbarModelImpl;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarModelImpl;->getTab()Lcom/google/android/apps/chrome/tab/ChromeTab;

    move-result-object v0

    .line 475
    const/4 v1, 0x1

    invoke-direct {p0, v2, v1}, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->updateTabLoadingState(ZZ)V

    .line 476
    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getProgress()I

    move-result v0

    .line 477
    if-eq v0, v3, :cond_0

    .line 480
    const/4 v1, 0x5

    if-le v0, v1, :cond_1

    .line 481
    invoke-direct {p0, v3}, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->updateLoadProgress(I)V

    .line 486
    :cond_0
    :goto_0
    invoke-direct {p0}, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->updateButtonStatus()V

    .line 487
    return-void

    .line 483
    :cond_1
    invoke-direct {p0, v2}, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->updateLoadProgressInternal(I)V

    goto :goto_0
.end method

.method private onReaderModeStatusChanged()V
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 526
    iget-boolean v1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->mShowReaderModeIcon:Z

    if-nez v1, :cond_0

    .line 543
    :goto_0
    return-void

    .line 531
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->mToolbarModel:Lcom/google/android/apps/chrome/toolbar/ToolbarModelImpl;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/toolbar/ToolbarModelImpl;->getTab()Lcom/google/android/apps/chrome/tab/ChromeTab;

    move-result-object v1

    .line 532
    if-eqz v1, :cond_3

    .line 533
    invoke-virtual {v1}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getReaderModeManager()Lcom/google/android/apps/chrome/dom_distiller/ReaderModeManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/dom_distiller/ReaderModeManager;->getReaderModeStatus()I

    move-result v2

    .line 534
    invoke-virtual {v1}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getUrl()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lorg/chromium/components/dom_distiller/core/DomDistillerUrlUtils;->isDistilledPage(Ljava/lang/String;)Z

    move-result v1

    .line 535
    if-eqz v2, :cond_1

    const/4 v3, 0x2

    if-eq v2, v3, :cond_1

    if-eqz v1, :cond_2

    :cond_1
    const/4 v0, 0x1

    :cond_2
    move v4, v1

    move v1, v0

    move v0, v4

    .line 542
    :goto_1
    iget-object v2, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->mToolbarDelegate:Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;

    invoke-virtual {v2, v1, v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;->updateReaderModeButton(ZZ)V

    goto :goto_0

    :cond_3
    move v1, v0

    goto :goto_1
.end method

.method private onTabCrash()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 468
    invoke-direct {p0, v0, v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->updateTabLoadingState(ZZ)V

    .line 469
    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->updateLoadProgressInternal(I)V

    .line 470
    invoke-direct {p0}, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->updateButtonStatus()V

    .line 471
    return-void
.end method

.method private refreshSelectedTab()V
    .locals 5

    .prologue
    .line 406
    const/4 v0, 0x0

    .line 407
    iget v1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->mPreselectedTabId:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    .line 408
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->mTabModelSelector:Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

    iget v1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->mPreselectedTabId:I

    invoke-interface {v0, v1}, Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;->getTabById(I)Lorg/chromium/chrome/browser/Tab;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->fromTab(Lorg/chromium/chrome/browser/Tab;)Lcom/google/android/apps/chrome/tab/ChromeTab;

    move-result-object v0

    .line 410
    :cond_0
    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->mTabModelSelector:Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

    invoke-interface {v0}, Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;->getCurrentTab()Lorg/chromium/chrome/browser/Tab;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->fromTab(Lorg/chromium/chrome/browser/Tab;)Lcom/google/android/apps/chrome/tab/ChromeTab;

    move-result-object v0

    .line 412
    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->mToolbarModel:Lcom/google/android/apps/chrome/toolbar/ToolbarModelImpl;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/toolbar/ToolbarModelImpl;->isIncognito()Z

    move-result v2

    .line 413
    iget-object v1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->mToolbarModel:Lcom/google/android/apps/chrome/toolbar/ToolbarModelImpl;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/toolbar/ToolbarModelImpl;->getTab()Lcom/google/android/apps/chrome/tab/ChromeTab;

    move-result-object v3

    .line 415
    if-eqz v0, :cond_8

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->isIncognito()Z

    move-result v1

    .line 417
    :goto_0
    iget-object v4, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->mToolbarModel:Lcom/google/android/apps/chrome/toolbar/ToolbarModelImpl;

    invoke-virtual {v4, v0, v1}, Lcom/google/android/apps/chrome/toolbar/ToolbarModelImpl;->setTab(Lcom/google/android/apps/chrome/tab/ChromeTab;Z)V

    .line 419
    invoke-direct {p0}, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->updateCurrentTabDisplayStatus()V

    .line 420
    if-ne v3, v0, :cond_2

    if-eq v2, v1, :cond_5

    .line 421
    :cond_2
    if-eq v3, v0, :cond_4

    .line 422
    if-eqz v3, :cond_3

    iget-object v2, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->mTabObserver:Lorg/chromium/chrome/browser/TabObserver;

    invoke-virtual {v3, v2}, Lorg/chromium/chrome/browser/Tab;->removeObserver(Lorg/chromium/chrome/browser/TabObserver;)V

    .line 423
    :cond_3
    if-eqz v0, :cond_4

    iget-object v2, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->mTabObserver:Lorg/chromium/chrome/browser/TabObserver;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/chrome/tab/ChromeTab;->addObserver(Lorg/chromium/chrome/browser/TabObserver;)V

    .line 426
    :cond_4
    if-eqz v1, :cond_9

    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->mToolbarView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v2, Lcom/google/android/apps/chrome/R$color;->incognito_primary_color:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    .line 429
    :goto_1
    iget-object v2, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->mToolbarModel:Lcom/google/android/apps/chrome/toolbar/ToolbarModelImpl;

    invoke-virtual {v2, v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarModelImpl;->setPrimaryColor(I)V

    .line 430
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->mToolbarDelegate:Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;->onTabOrModelChanged()V

    .line 433
    :cond_5
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->mTabModelSelector:Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

    invoke-interface {v0, v1}, Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;->getModel(Z)Lorg/chromium/chrome/browser/tabmodel/TabModel;

    move-result-object v0

    invoke-interface {v0}, Lorg/chromium/chrome/browser/tabmodel/TabModel;->getProfile()Lorg/chromium/chrome/browser/profiles/Profile;

    move-result-object v0

    .line 434
    iget-object v1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->mCurrentProfile:Lorg/chromium/chrome/browser/profiles/Profile;

    if-eq v1, v0, :cond_7

    .line 435
    iget-object v1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->mBookmarksBridge:Lorg/chromium/chrome/browser/BookmarksBridge;

    if-eqz v1, :cond_6

    iget-object v1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->mBookmarksBridge:Lorg/chromium/chrome/browser/BookmarksBridge;

    invoke-virtual {v1}, Lorg/chromium/chrome/browser/BookmarksBridge;->destroy()V

    .line 436
    :cond_6
    new-instance v1, Lorg/chromium/chrome/browser/BookmarksBridge;

    invoke-direct {v1, v0}, Lorg/chromium/chrome/browser/BookmarksBridge;-><init>(Lorg/chromium/chrome/browser/profiles/Profile;)V

    iput-object v1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->mBookmarksBridge:Lorg/chromium/chrome/browser/BookmarksBridge;

    .line 437
    iget-object v1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->mBookmarksBridge:Lorg/chromium/chrome/browser/BookmarksBridge;

    iget-object v2, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->mBookmarksObserver:Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkModelObserver;

    invoke-virtual {v1, v2}, Lorg/chromium/chrome/browser/BookmarksBridge;->addObserver(Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkModelObserver;)V

    .line 440
    iget-object v1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->mLocationBar:Lcom/google/android/apps/chrome/omnibox/LocationBar;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->setAutocompleteProfile(Lorg/chromium/chrome/browser/profiles/Profile;)V

    .line 441
    iput-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->mCurrentProfile:Lorg/chromium/chrome/browser/profiles/Profile;

    .line 443
    :cond_7
    invoke-direct {p0}, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->onReaderModeStatusChanged()V

    .line 444
    return-void

    .line 415
    :cond_8
    iget-object v1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->mTabModelSelector:Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

    invoke-interface {v1}, Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;->isIncognitoSelected()Z

    move-result v1

    goto :goto_0

    .line 426
    :cond_9
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->mToolbarView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v2, Lcom/google/android/apps/chrome/R$color;->default_primary_color:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    goto :goto_1
.end method

.method private setMenuHandler(Lorg/chromium/chrome/browser/appmenu/AppMenuHandler;)V
    .locals 2

    .prologue
    .line 245
    new-instance v0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager$5;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/toolbar/ToolbarManager$5;-><init>(Lcom/google/android/apps/chrome/toolbar/ToolbarManager;)V

    invoke-virtual {p1, v0}, Lorg/chromium/chrome/browser/appmenu/AppMenuHandler;->addObserver(Lorg/chromium/chrome/browser/appmenu/AppMenuObserver;)V

    .line 259
    new-instance v0, Lorg/chromium/chrome/browser/appmenu/AppMenuButtonHelper;

    iget-object v1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->mToolbarDelegate:Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;->getMenuButton()Landroid/view/View;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Lorg/chromium/chrome/browser/appmenu/AppMenuButtonHelper;-><init>(Landroid/view/View;Lorg/chromium/chrome/browser/appmenu/AppMenuHandler;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->mAppMenuButtonHelper:Lorg/chromium/chrome/browser/appmenu/AppMenuButtonHelper;

    .line 261
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->mAppMenuButtonHelper:Lorg/chromium/chrome/browser/appmenu/AppMenuButtonHelper;

    new-instance v1, Lcom/google/android/apps/chrome/toolbar/ToolbarManager$6;

    invoke-direct {v1, p0}, Lcom/google/android/apps/chrome/toolbar/ToolbarManager$6;-><init>(Lcom/google/android/apps/chrome/toolbar/ToolbarManager;)V

    invoke-virtual {v0, v1}, Lorg/chromium/chrome/browser/appmenu/AppMenuButtonHelper;->setOnAppMenuShownListener(Ljava/lang/Runnable;)V

    .line 267
    return-void
.end method

.method private updateBookmarkButtonStatus()V
    .locals 4

    .prologue
    .line 383
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->mToolbarModel:Lcom/google/android/apps/chrome/toolbar/ToolbarModelImpl;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarModelImpl;->getTab()Lcom/google/android/apps/chrome/tab/ChromeTab;

    move-result-object v0

    .line 384
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getBookmarkId()J

    move-result-wide v0

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 386
    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->mToolbarDelegate:Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;->updateBookmarkButtonVisibility(Z)V

    .line 387
    return-void

    .line 384
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private updateButtonStatus()V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 368
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->mToolbarModel:Lcom/google/android/apps/chrome/toolbar/ToolbarModelImpl;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarModelImpl;->getTab()Lcom/google/android/apps/chrome/tab/ChromeTab;

    move-result-object v4

    .line 369
    if-eqz v4, :cond_0

    invoke-virtual {v4}, Lcom/google/android/apps/chrome/tab/ChromeTab;->isShowingSadTab()Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    .line 371
    :goto_0
    iget-object v5, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->mToolbarDelegate:Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;

    if-eqz v4, :cond_1

    invoke-virtual {v4}, Lcom/google/android/apps/chrome/tab/ChromeTab;->canGoBack()Z

    move-result v3

    if-eqz v3, :cond_1

    move v3, v1

    :goto_1
    invoke-virtual {v5, v3}, Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;->updateBackButtonVisibility(Z)V

    .line 373
    iget-object v3, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->mToolbarDelegate:Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;

    if-eqz v4, :cond_2

    invoke-virtual {v4}, Lcom/google/android/apps/chrome/tab/ChromeTab;->canGoForward()Z

    move-result v4

    if-eqz v4, :cond_2

    :goto_2
    invoke-virtual {v3, v1}, Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;->updateForwardButtonVisibility(Z)V

    .line 375
    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->updateReloadState(Z)V

    .line 376
    invoke-direct {p0}, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->updateBookmarkButtonStatus()V

    .line 378
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->mToolbarDelegate:Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;->getMenuButton()Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->mToolbarDelegate:Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;->shouldShowMenuButton()Z

    move-result v1

    if-eqz v1, :cond_3

    :goto_3
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 380
    return-void

    :cond_0
    move v0, v2

    .line 369
    goto :goto_0

    :cond_1
    move v3, v2

    .line 371
    goto :goto_1

    :cond_2
    move v1, v2

    .line 373
    goto :goto_2

    .line 378
    :cond_3
    const/16 v2, 0x8

    goto :goto_3
.end method

.method private updateCurrentTabDisplayStatus()V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 447
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->mToolbarModel:Lcom/google/android/apps/chrome/toolbar/ToolbarModelImpl;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarModelImpl;->getTab()Lcom/google/android/apps/chrome/tab/ChromeTab;

    move-result-object v3

    .line 448
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->mLocationBar:Lcom/google/android/apps/chrome/omnibox/LocationBar;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->setUrlToPageUrl()V

    .line 449
    if-nez v3, :cond_0

    .line 450
    invoke-direct {p0, v2}, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->updateLoadProgressInternal(I)V

    .line 451
    invoke-direct {p0}, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->updateButtonStatus()V

    .line 465
    :goto_0
    return-void

    .line 454
    :cond_0
    invoke-virtual {v3}, Lcom/google/android/apps/chrome/tab/ChromeTab;->isLoading()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {v3}, Lcom/google/android/apps/chrome/tab/ChromeTab;->isShowingInterstitialPage()Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    .line 455
    :goto_1
    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->updateTabLoadingState(ZZ)V

    .line 457
    invoke-virtual {v3}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getProgress()I

    move-result v0

    const/16 v1, 0x64

    if-eq v0, v1, :cond_1

    invoke-virtual {v3}, Lcom/google/android/apps/chrome/tab/ChromeTab;->isShowingInterstitialPage()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 460
    :cond_1
    invoke-direct {p0, v2}, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->updateLoadProgressInternal(I)V

    .line 464
    :goto_2
    invoke-direct {p0}, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->updateButtonStatus()V

    goto :goto_0

    :cond_2
    move v0, v2

    .line 454
    goto :goto_1

    .line 462
    :cond_3
    invoke-virtual {v3}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getProgress()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->updateLoadProgress(I)V

    goto :goto_2
.end method

.method private updateLoadProgress(I)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 509
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->mLoadProgressSimulator:Lcom/google/android/apps/chrome/toolbar/ToolbarManager$LoadProgressSimulator;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarManager$LoadProgressSimulator;->cancel()V

    .line 510
    const/4 v0, 0x5

    invoke-static {p1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 511
    iget-object v2, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->mToolbarModel:Lcom/google/android/apps/chrome/toolbar/ToolbarModelImpl;

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/toolbar/ToolbarModelImpl;->getTab()Lcom/google/android/apps/chrome/tab/ChromeTab;

    move-result-object v2

    .line 512
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getUrl()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/tab/ChromeTab;->isIncognito()Z

    move-result v2

    invoke-static {v3, v2}, Lcom/google/android/apps/chrome/tab/NativePageFactory;->isNativePageUrl(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_0

    move v0, v1

    .line 516
    :cond_0
    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->updateLoadProgressInternal(I)V

    .line 517
    const/16 v2, 0x64

    if-eq v0, v2, :cond_1

    if-nez v0, :cond_2

    .line 518
    :cond_1
    invoke-direct {p0}, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->updateButtonStatus()V

    .line 523
    :goto_0
    return-void

    .line 521
    :cond_2
    invoke-direct {p0, v1}, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->updateReloadState(Z)V

    goto :goto_0
.end method

.method private updateLoadProgressInternal(I)V
    .locals 1

    .prologue
    .line 502
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->mToolbarModel:Lcom/google/android/apps/chrome/toolbar/ToolbarModelImpl;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarModelImpl;->getLoadProgress()I

    move-result v0

    if-ne p1, v0, :cond_1

    .line 506
    :cond_0
    :goto_0
    return-void

    .line 503
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->mToolbarModel:Lcom/google/android/apps/chrome/toolbar/ToolbarModelImpl;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/toolbar/ToolbarModelImpl;->setLoadProgress(I)V

    .line 504
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->mToolbarDelegate:Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;->setLoadProgress(I)V

    .line 505
    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->mLoadProgressSimulator:Lcom/google/android/apps/chrome/toolbar/ToolbarManager$LoadProgressSimulator;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarManager$LoadProgressSimulator;->cancel()V

    goto :goto_0
.end method

.method private updateReloadState(Z)V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 390
    iget-object v1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->mToolbarModel:Lcom/google/android/apps/chrome/toolbar/ToolbarModelImpl;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/toolbar/ToolbarModelImpl;->getTab()Lcom/google/android/apps/chrome/tab/ChromeTab;

    move-result-object v1

    .line 392
    if-nez p1, :cond_2

    .line 393
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/tab/ChromeTab;->isLoading()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/tab/ChromeTab;->isShowingInterstitialPage()Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    iget-boolean v1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->mNativeLibraryReady:Z

    if-nez v1, :cond_2

    :cond_1
    const/4 v0, 0x1

    .line 396
    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->mToolbarDelegate:Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;->updateReloadButtonVisibility(Z)V

    .line 397
    iget-object v1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->mMenuDelegatePhone:Lcom/google/android/apps/chrome/toolbar/ToolbarManager$MenuDelegatePhone;

    if-eqz v1, :cond_3

    .line 398
    iget-object v1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->mMenuDelegatePhone:Lcom/google/android/apps/chrome/toolbar/ToolbarManager$MenuDelegatePhone;

    invoke-interface {v1, v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarManager$MenuDelegatePhone;->updateReloadButtonState(Z)V

    .line 400
    :cond_3
    return-void
.end method

.method private updateTabCount()V
    .locals 2

    .prologue
    .line 359
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->mTabRestoreCompleted:Z

    if-nez v0, :cond_0

    .line 361
    :goto_0
    return-void

    .line 360
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->mToolbarDelegate:Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;

    iget-object v1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->mTabModelSelector:Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

    invoke-interface {v1}, Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;->getCurrentModel()Lorg/chromium/chrome/browser/tabmodel/TabModel;

    move-result-object v1

    invoke-interface {v1}, Lorg/chromium/chrome/browser/tabmodel/TabModel;->getCount()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;->updateTabCountVisuals(I)V

    goto :goto_0
.end method

.method private updateTabLoadingState(ZZ)V
    .locals 2

    .prologue
    .line 496
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->mToolbarModel:Lcom/google/android/apps/chrome/toolbar/ToolbarModelImpl;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarModelImpl;->getTab()Lcom/google/android/apps/chrome/tab/ChromeTab;

    move-result-object v0

    .line 497
    iget-object v1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->mLocationBar:Lcom/google/android/apps/chrome/omnibox/LocationBar;

    invoke-virtual {v1, p2}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->updateLoadingState(Z)V

    .line 498
    if-eqz p1, :cond_0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getProgress()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->updateLoadProgress(I)V

    .line 499
    :cond_0
    return-void
.end method


# virtual methods
.method public back()Z
    .locals 2

    .prologue
    .line 279
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->mToolbarModel:Lcom/google/android/apps/chrome/toolbar/ToolbarModelImpl;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarModelImpl;->getTab()Lcom/google/android/apps/chrome/tab/ChromeTab;

    move-result-object v0

    .line 280
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/Tab;->canGoBack()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 281
    invoke-virtual {v0}, Lorg/chromium/chrome/browser/Tab;->goBack()V

    .line 282
    invoke-direct {p0}, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->updateButtonStatus()V

    .line 283
    const/4 v0, 0x1

    .line 285
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public forward()Z
    .locals 2

    .prologue
    .line 290
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->mToolbarModel:Lcom/google/android/apps/chrome/toolbar/ToolbarModelImpl;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarModelImpl;->getTab()Lcom/google/android/apps/chrome/tab/ChromeTab;

    move-result-object v0

    .line 291
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/Tab;->canGoForward()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 292
    invoke-virtual {v0}, Lorg/chromium/chrome/browser/Tab;->goForward()V

    .line 293
    invoke-direct {p0}, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->updateButtonStatus()V

    .line 294
    const/4 v0, 0x1

    .line 296
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public initializeWithNative(Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;Lorg/chromium/chrome/browser/appmenu/AppMenuHandler;Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;)V
    .locals 3

    .prologue
    .line 176
    iput-object p1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->mTabModelSelector:Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

    .line 177
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->mToolbarModel:Lcom/google/android/apps/chrome/toolbar/ToolbarModelImpl;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarModelImpl;->initializeWithNative()V

    .line 179
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->mToolbarView:Landroid/view/View;

    new-instance v1, Lcom/google/android/apps/chrome/toolbar/ToolbarManager$4;

    invoke-direct {v1, p0}, Lcom/google/android/apps/chrome/toolbar/ToolbarManager$4;-><init>(Lcom/google/android/apps/chrome/toolbar/ToolbarManager;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->addOnAttachStateChangeListener(Landroid/view/View$OnAttachStateChangeListener;)V

    .line 199
    invoke-direct {p0, p2}, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->setMenuHandler(Lorg/chromium/chrome/browser/appmenu/AppMenuHandler;)V

    .line 201
    sget-boolean v0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    if-nez p3, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 202
    :cond_0
    iput-object p3, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->mFullscreenManager:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;

    .line 204
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->mNativeLibraryReady:Z

    .line 206
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->mToolbarDelegate:Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;

    iget-object v1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->mToolbarModel:Lcom/google/android/apps/chrome/toolbar/ToolbarModelImpl;

    iget-object v2, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->mAppMenuButtonHelper:Lorg/chromium/chrome/browser/appmenu/AppMenuButtonHelper;

    invoke-virtual {v0, v1, p0, v2}, Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;->initialize(Lcom/google/android/apps/chrome/toolbar/ToolbarDataProvider;Lcom/google/android/apps/chrome/toolbar/ToolbarTabController;Lorg/chromium/chrome/browser/appmenu/AppMenuButtonHelper;)V

    .line 208
    invoke-direct {p0}, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->onNativeLibraryReady()V

    .line 209
    return-void
.end method

.method public onAccessibilityStatusChanged(Z)V
    .locals 1

    .prologue
    .line 216
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->mToolbarDelegate:Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;->onAccessibilityStatusChanged(Z)V

    .line 217
    return-void
.end method

.method public onUrlFocusChange(Z)V
    .locals 2

    .prologue
    .line 332
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->mToolbarDelegate:Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;->onUrlFocusChange(Z)V

    .line 333
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->mFullscreenManager:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;

    if-nez v0, :cond_0

    .line 341
    :goto_0
    return-void

    .line 334
    :cond_0
    if-eqz p1, :cond_1

    .line 335
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->mFullscreenManager:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;

    iget v1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->mFullscreenFocusToken:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->showControlsPersistentAndClearOldToken(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->mFullscreenFocusToken:I

    goto :goto_0

    .line 338
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->mFullscreenManager:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;

    iget v1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->mFullscreenFocusToken:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->hideControlsPersistent(I)V

    .line 339
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->mFullscreenFocusToken:I

    goto :goto_0
.end method

.method public openHomepage()V
    .locals 4

    .prologue
    .line 315
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->mToolbarModel:Lcom/google/android/apps/chrome/toolbar/ToolbarModelImpl;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarModelImpl;->getTab()Lcom/google/android/apps/chrome/tab/ChromeTab;

    move-result-object v1

    .line 316
    sget-boolean v0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    if-nez v1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 317
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->mToolbarView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 318
    invoke-static {v0}, Lcom/google/android/apps/chrome/partnercustomizations/HomepageManager;->getHomepageUri(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 319
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 320
    const-string/jumbo v0, "chrome-native://newtab/"

    .line 322
    :cond_1
    new-instance v2, Lorg/chromium/content_public/browser/LoadUrlParams;

    const/high16 v3, 0x4000000

    invoke-direct {v2, v0, v3}, Lorg/chromium/content_public/browser/LoadUrlParams;-><init>(Ljava/lang/String;I)V

    invoke-virtual {v1, v2}, Lorg/chromium/chrome/browser/Tab;->loadUrl(Lorg/chromium/content_public/browser/LoadUrlParams;)I

    .line 324
    return-void
.end method

.method public setMenuDelegatePhone(Lcom/google/android/apps/chrome/toolbar/ToolbarManager$MenuDelegatePhone;)V
    .locals 0

    .prologue
    .line 274
    iput-object p1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->mMenuDelegatePhone:Lcom/google/android/apps/chrome/toolbar/ToolbarManager$MenuDelegatePhone;

    .line 275
    return-void
.end method

.method public stopOrReloadCurrentTab()V
    .locals 2

    .prologue
    .line 301
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->mToolbarModel:Lcom/google/android/apps/chrome/toolbar/ToolbarModelImpl;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarModelImpl;->getTab()Lcom/google/android/apps/chrome/tab/ChromeTab;

    move-result-object v0

    .line 302
    if-eqz v0, :cond_0

    .line 303
    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->isLoading()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 304
    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->stopLoading()V

    .line 310
    :cond_0
    :goto_0
    invoke-direct {p0}, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->updateButtonStatus()V

    .line 311
    return-void

    .line 306
    :cond_1
    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->reload()V

    .line 307
    invoke-static {}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->reload()V

    goto :goto_0
.end method

.method updatePrimaryColor(I)V
    .locals 1

    .prologue
    .line 348
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->mToolbarModel:Lcom/google/android/apps/chrome/toolbar/ToolbarModelImpl;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarModelImpl;->getPrimaryColor()I

    move-result v0

    if-eq v0, p1, :cond_0

    const/4 v0, 0x1

    .line 349
    :goto_0
    if-nez v0, :cond_1

    .line 353
    :goto_1
    return-void

    .line 348
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 351
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->mToolbarModel:Lcom/google/android/apps/chrome/toolbar/ToolbarModelImpl;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/toolbar/ToolbarModelImpl;->setPrimaryColor(I)V

    .line 352
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->mToolbarDelegate:Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;->onPrimaryColorChanged()V

    goto :goto_1
.end method
