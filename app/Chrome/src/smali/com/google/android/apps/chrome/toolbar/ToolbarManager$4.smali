.class Lcom/google/android/apps/chrome/toolbar/ToolbarManager$4;
.super Ljava/lang/Object;
.source "ToolbarManager.java"

# interfaces
.implements Landroid/view/View$OnAttachStateChangeListener;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarManager;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/toolbar/ToolbarManager;)V
    .locals 0

    .prologue
    .line 179
    iput-object p1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager$4;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onViewAttachedToWindow(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 195
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager$4;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarManager;

    # getter for: Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->mTabModelSelector:Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;
    invoke-static {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->access$1000(Lcom/google/android/apps/chrome/toolbar/ToolbarManager;)Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager$4;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarManager;

    # invokes: Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->refreshSelectedTab()V
    invoke-static {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->access$1100(Lcom/google/android/apps/chrome/toolbar/ToolbarManager;)V

    .line 196
    :cond_0
    return-void
.end method

.method public onViewDetachedFromWindow(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 182
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager$4;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarManager;

    # getter for: Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->mToolbarView:Landroid/view/View;
    invoke-static {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->access$600(Lcom/google/android/apps/chrome/toolbar/ToolbarManager;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    # getter for: Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->NOTIFICATIONS:[I
    invoke-static {}, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->access$700()[I

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager$4;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarManager;

    # getter for: Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->mNotificationHandler:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;
    invoke-static {v2}, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->access$800(Lcom/google/android/apps/chrome/toolbar/ToolbarManager;)Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->unregisterForNotifications(Landroid/content/Context;[ILcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;)V

    .line 184
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager$4;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarManager;

    # getter for: Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->mBookmarksBridge:Lorg/chromium/chrome/browser/BookmarksBridge;
    invoke-static {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->access$900(Lcom/google/android/apps/chrome/toolbar/ToolbarManager;)Lorg/chromium/chrome/browser/BookmarksBridge;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 185
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager$4;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarManager;

    # getter for: Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->mBookmarksBridge:Lorg/chromium/chrome/browser/BookmarksBridge;
    invoke-static {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->access$900(Lcom/google/android/apps/chrome/toolbar/ToolbarManager;)Lorg/chromium/chrome/browser/BookmarksBridge;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/BookmarksBridge;->destroy()V

    .line 186
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager$4;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarManager;

    const/4 v1, 0x0

    # setter for: Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->mBookmarksBridge:Lorg/chromium/chrome/browser/BookmarksBridge;
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->access$902(Lcom/google/android/apps/chrome/toolbar/ToolbarManager;Lorg/chromium/chrome/browser/BookmarksBridge;)Lorg/chromium/chrome/browser/BookmarksBridge;

    .line 188
    :cond_0
    return-void
.end method
