.class public Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl;
.super Lorg/chromium/chrome/browser/tabmodel/TabModelSelectorBase;
.source "TabModelSelectorImpl.java"

# interfaces
.implements Lorg/chromium/chrome/browser/tabmodel/TabModelDelegate;


# static fields
.field static final synthetic $assertionsDisabled:Z

.field private static final NOTIFICATIONS:[I


# instance fields
.field private mActiveState:Z

.field private final mActivity:Lcom/google/android/apps/chrome/ChromeActivity;

.field private final mHandler:Landroid/os/Handler;

.field private mInOverviewMode:Z

.field private final mNotificationHandler:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;

.field private final mOrderController:Lorg/chromium/chrome/browser/tabmodel/TabModelOrderController;

.field private final mSessionRestoreInProgress:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private mSnapshotDownloadBroadcastReceiver:Landroid/content/BroadcastReceiver;

.field private mTabContentManager:Lcom/google/android/apps/chrome/compositor/TabContentManager;

.field private final mTabSaver:Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;

.field private final mUma:Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorUma;

.field private mVisibleTab:Lcom/google/android/apps/chrome/tab/ChromeTab;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 45
    const-class v0, Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl;->$assertionsDisabled:Z

    .line 92
    const/16 v0, 0xd

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl;->NOTIFICATIONS:[I

    return-void

    .line 45
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 92
    nop

    :array_0
    .array-data 4
        0x0
        0x1
        0x11
        0x47
        0x10
        0x1b
        0x6
        0x2
        0x19
        0x3a
        0x1c
        0x9
        0x34
    .end array-data
.end method

.method public constructor <init>(Lcom/google/android/apps/chrome/ChromeActivity;ILorg/chromium/ui/base/WindowAndroid;)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 80
    invoke-direct {p0}, Lorg/chromium/chrome/browser/tabmodel/TabModelSelectorBase;-><init>()V

    .line 50
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0, v6}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl;->mSessionRestoreInProgress:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 56
    iput-boolean v5, p0, Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl;->mActiveState:Z

    .line 70
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl;->mHandler:Landroid/os/Handler;

    .line 108
    new-instance v0, Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl$1;-><init>(Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl;->mNotificationHandler:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;

    .line 81
    iput-object p1, p0, Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl;->mActivity:Lcom/google/android/apps/chrome/ChromeActivity;

    .line 82
    new-instance v0, Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorUma;

    iget-object v1, p0, Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl;->mActivity:Lcom/google/android/apps/chrome/ChromeActivity;

    invoke-direct {v0, v1}, Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorUma;-><init>(Landroid/app/Activity;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl;->mUma:Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorUma;

    .line 83
    new-instance v0, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;

    iget-object v1, p0, Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl;->mActivity:Lcom/google/android/apps/chrome/ChromeActivity;

    iget-object v2, p0, Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl;->mActivity:Lcom/google/android/apps/chrome/ChromeActivity;

    invoke-direct {v0, p0, p2, v1, v2}, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;-><init>(Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;ILandroid/content/Context;Lcom/google/android/apps/chrome/tabmodel/ChromeTabCreatorManager;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl;->mTabSaver:Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;

    .line 84
    new-instance v0, Lorg/chromium/chrome/browser/tabmodel/TabModelOrderController;

    invoke-direct {v0, p0}, Lorg/chromium/chrome/browser/tabmodel/TabModelOrderController;-><init>(Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl;->mOrderController:Lorg/chromium/chrome/browser/tabmodel/TabModelOrderController;

    .line 85
    new-instance v0, Lcom/google/android/apps/chrome/tabmodel/ChromeTabCreator;

    iget-object v1, p0, Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl;->mActivity:Lcom/google/android/apps/chrome/ChromeActivity;

    iget-object v3, p0, Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl;->mOrderController:Lorg/chromium/chrome/browser/tabmodel/TabModelOrderController;

    iget-object v4, p0, Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl;->mTabSaver:Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;

    move-object v2, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/chrome/tabmodel/ChromeTabCreator;-><init>(Lcom/google/android/apps/chrome/ChromeActivity;Lorg/chromium/ui/base/WindowAndroid;Lorg/chromium/chrome/browser/tabmodel/TabModelOrderController;Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;Z)V

    .line 87
    new-instance v1, Lcom/google/android/apps/chrome/tabmodel/ChromeTabCreator;

    iget-object v2, p0, Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl;->mActivity:Lcom/google/android/apps/chrome/ChromeActivity;

    iget-object v4, p0, Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl;->mOrderController:Lorg/chromium/chrome/browser/tabmodel/TabModelOrderController;

    iget-object v5, p0, Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl;->mTabSaver:Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;

    move-object v3, p3

    invoke-direct/range {v1 .. v6}, Lcom/google/android/apps/chrome/tabmodel/ChromeTabCreator;-><init>(Lcom/google/android/apps/chrome/ChromeActivity;Lorg/chromium/ui/base/WindowAndroid;Lorg/chromium/chrome/browser/tabmodel/TabModelOrderController;Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;Z)V

    .line 89
    iget-object v2, p0, Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl;->mActivity:Lcom/google/android/apps/chrome/ChromeActivity;

    invoke-virtual {v2, v0, v1}, Lcom/google/android/apps/chrome/ChromeActivity;->setTabCreators(Lcom/google/android/apps/chrome/tabmodel/ChromeTabCreator;Lcom/google/android/apps/chrome/tabmodel/ChromeTabCreator;)V

    .line 90
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl;)V
    .locals 0

    .prologue
    .line 45
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl;->handleOnOverviewModeShowStarted()V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl;)V
    .locals 1

    .prologue
    .line 45
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl;->mInOverviewMode:Z

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl;)V
    .locals 0

    .prologue
    .line 45
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl;->cacheCurrentTabBitmapIfNativePage()V

    return-void
.end method

.method static synthetic access$300(Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl;I)V
    .locals 0

    .prologue
    .line 45
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl;->handleOnPageLoadStopped(I)V

    return-void
.end method

.method static synthetic access$400(Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl;)Ljava/util/concurrent/atomic/AtomicBoolean;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl;->mSessionRestoreInProgress:Ljava/util/concurrent/atomic/AtomicBoolean;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl;)Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorUma;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl;->mUma:Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorUma;

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl;)Lcom/google/android/apps/chrome/compositor/TabContentManager;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl;->mTabContentManager:Lcom/google/android/apps/chrome/compositor/TabContentManager;

    return-object v0
.end method

.method static synthetic access$700(Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl;)V
    .locals 0

    .prologue
    .line 45
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl;->handleAutoLoginDisabled()V

    return-void
.end method

.method static synthetic access$800(Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl;)V
    .locals 0

    .prologue
    .line 45
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl;->notifyChanged()V

    return-void
.end method

.method private blockingNotifyModelSelected()V
    .locals 3

    .prologue
    .line 231
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 232
    const-string/jumbo v0, "incognito"

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl;->isIncognitoSelected()Z

    move-result v2

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 233
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl;->getCurrentTab()Lorg/chromium/chrome/browser/Tab;

    move-result-object v0

    .line 234
    const-string/jumbo v2, "tabId"

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/Tab;->getId()I

    move-result v0

    :goto_0
    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 235
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl;->mActivity:Lcom/google/android/apps/chrome/ChromeActivity;

    const/16 v2, 0xc

    invoke-static {v0, v2, v1}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->broadcastImmediateNotification(Landroid/content/Context;ILandroid/os/Bundle;)V

    .line 236
    return-void

    .line 234
    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method private cacheCurrentTabBitmapIfNativePage()V
    .locals 2

    .prologue
    .line 394
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl;->getCurrentTab()Lorg/chromium/chrome/browser/Tab;

    move-result-object v0

    .line 395
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/Tab;->isNativePage()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 396
    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl;->cacheTabBitmap(Lorg/chromium/chrome/browser/Tab;)V

    .line 398
    :cond_0
    return-void
.end method

.method private cacheTabBitmap(Lorg/chromium/chrome/browser/Tab;)V
    .locals 2

    .prologue
    .line 402
    if-nez p1, :cond_0

    .line 404
    :goto_0
    return-void

    .line 403
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl;->mTabContentManager:Lcom/google/android/apps/chrome/compositor/TabContentManager;

    invoke-static {p1}, Lcom/google/android/apps/chrome/tab/ChromeTab;->fromTab(Lorg/chromium/chrome/browser/Tab;)Lcom/google/android/apps/chrome/tab/ChromeTab;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/compositor/TabContentManager;->cacheTabThumbnail(Lorg/chromium/chrome/browser/Tab;)V

    goto :goto_0
.end method

.method private handleAutoLoginDisabled()V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 188
    move v0, v1

    :goto_0
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl;->getModels()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v0, v2, :cond_2

    .line 189
    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl;->getModelAt(I)Lorg/chromium/chrome/browser/tabmodel/TabModel;

    move-result-object v3

    move v2, v1

    .line 190
    :goto_1
    if-eqz v3, :cond_1

    invoke-interface {v3}, Lorg/chromium/chrome/browser/tabmodel/TabModel;->getCount()I

    move-result v4

    if-ge v2, v4, :cond_1

    .line 191
    invoke-interface {v3, v2}, Lorg/chromium/chrome/browser/tabmodel/TabModel;->getTabAt(I)Lorg/chromium/chrome/browser/Tab;

    move-result-object v4

    .line 192
    if-eqz v4, :cond_0

    invoke-virtual {v4}, Lorg/chromium/chrome/browser/Tab;->getInfoBarContainer()Lorg/chromium/chrome/browser/infobar/InfoBarContainer;

    move-result-object v5

    if-eqz v5, :cond_0

    .line 193
    invoke-virtual {v4}, Lorg/chromium/chrome/browser/Tab;->getInfoBarContainer()Lorg/chromium/chrome/browser/infobar/InfoBarContainer;

    move-result-object v4

    invoke-virtual {v4}, Lorg/chromium/chrome/browser/infobar/InfoBarContainer;->dismissAutoLoginInfoBars()V

    .line 190
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 188
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 197
    :cond_2
    return-void
.end method

.method private handleOnOverviewModeShowStarted()V
    .locals 1

    .prologue
    .line 176
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl;->mInOverviewMode:Z

    .line 177
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl;->cacheCurrentTabBitmapIfNativePage()V

    .line 178
    return-void
.end method

.method private handleOnPageLoadStopped(I)V
    .locals 2

    .prologue
    .line 181
    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl;->getTabById(I)Lorg/chromium/chrome/browser/Tab;

    move-result-object v0

    .line 182
    if-eqz v0, :cond_0

    .line 183
    iget-object v1, p0, Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl;->mTabSaver:Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;

    invoke-static {v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->fromTab(Lorg/chromium/chrome/browser/Tab;)Lcom/google/android/apps/chrome/tab/ChromeTab;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->addTabToSaveQueue(Lcom/google/android/apps/chrome/tab/ChromeTab;)V

    .line 185
    :cond_0
    return-void
.end method


# virtual methods
.method public clearEncryptedState()V
    .locals 1

    .prologue
    .line 315
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl;->mTabSaver:Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->clearEncryptedState()V

    .line 316
    return-void
.end method

.method public clearState()V
    .locals 1

    .prologue
    .line 311
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl;->mTabSaver:Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->clearState()V

    .line 312
    return-void
.end method

.method public commitAllTabClosures()V
    .locals 2

    .prologue
    .line 269
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl;->getModels()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 270
    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl;->getModelAt(I)Lorg/chromium/chrome/browser/tabmodel/TabModel;

    move-result-object v1

    invoke-interface {v1}, Lorg/chromium/chrome/browser/tabmodel/TabModel;->commitAllTabClosures()V

    .line 269
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 272
    :cond_0
    return-void
.end method

.method public destroy()V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 322
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl;->mTabSaver:Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->destroy()V

    .line 323
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl;->mUma:Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorUma;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorUma;->destroy()V

    .line 324
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl;->mSnapshotDownloadBroadcastReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v0, :cond_0

    .line 325
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl;->mActivity:Lcom/google/android/apps/chrome/ChromeActivity;

    iget-object v2, p0, Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl;->mSnapshotDownloadBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/chrome/ChromeActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 327
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl;->mActivity:Lcom/google/android/apps/chrome/ChromeActivity;

    sget-object v2, Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl;->NOTIFICATIONS:[I

    iget-object v3, p0, Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl;->mNotificationHandler:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;

    invoke-static {v0, v2, v3}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->unregisterForNotifications(Landroid/content/Context;[ILcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;)V

    move v0, v1

    .line 330
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl;->getModels()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v0, v2, :cond_1

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl;->getModelAt(I)Lorg/chromium/chrome/browser/tabmodel/TabModel;

    move-result-object v2

    invoke-interface {v2}, Lorg/chromium/chrome/browser/tabmodel/TabModel;->destroy()V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 331
    :cond_1
    iput-boolean v1, p0, Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl;->mActiveState:Z

    .line 332
    return-void
.end method

.method public didChange()V
    .locals 0

    .prologue
    .line 423
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl;->notifyChanged()V

    .line 424
    return-void
.end method

.method public didCreateNewTab(Lorg/chromium/chrome/browser/Tab;)V
    .locals 0

    .prologue
    .line 418
    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl;->notifyNewTabCreated(Lorg/chromium/chrome/browser/Tab;)V

    .line 419
    return-void
.end method

.method public getModelAt(I)Lorg/chromium/chrome/browser/tabmodel/TabModel;
    .locals 1

    .prologue
    .line 240
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl;->mActiveState:Z

    if-eqz v0, :cond_0

    invoke-super {p0, p1}, Lorg/chromium/chrome/browser/tabmodel/TabModelSelectorBase;->getModelAt(I)Lorg/chromium/chrome/browser/tabmodel/TabModel;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lorg/chromium/chrome/browser/tabmodel/EmptyTabModel;->getInstance()Lorg/chromium/chrome/browser/tabmodel/EmptyTabModel;

    move-result-object v0

    goto :goto_0
.end method

.method public getRestoredTabCount()I
    .locals 1

    .prologue
    .line 344
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl;->mTabSaver:Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->getRestoredTabCount()I

    move-result v0

    return v0
.end method

.method public isInOverviewMode()Z
    .locals 1

    .prologue
    .line 408
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl;->mInOverviewMode:Z

    return v0
.end method

.method public isSessionRestoreInProgress()Z
    .locals 1

    .prologue
    .line 413
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl;->mSessionRestoreInProgress:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    return v0
.end method

.method public loadState()V
    .locals 1

    .prologue
    .line 284
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl;->mTabSaver:Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->loadState()I

    move-result v0

    .line 285
    if-ltz v0, :cond_0

    invoke-static {v0}, Lorg/chromium/chrome/browser/Tab;->incrementIdCounterTo(I)V

    .line 286
    :cond_0
    return-void
.end method

.method public onNativeLibraryReady(Lcom/google/android/apps/chrome/compositor/TabContentManager;)V
    .locals 10

    .prologue
    const/4 v9, 0x1

    const/4 v1, 0x0

    .line 205
    sget-boolean v0, Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl;->mActiveState:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    const-string/jumbo v1, "onNativeLibraryReady called twice!"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 206
    :cond_0
    iput-object p1, p0, Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl;->mTabContentManager:Lcom/google/android/apps/chrome/compositor/TabContentManager;

    .line 208
    new-instance v0, Lcom/google/android/apps/chrome/tabmodel/TabModelImpl;

    iget-object v2, p0, Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl;->mActivity:Lcom/google/android/apps/chrome/ChromeActivity;

    iget-object v3, p0, Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl;->mUma:Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorUma;

    iget-object v4, p0, Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl;->mOrderController:Lorg/chromium/chrome/browser/tabmodel/TabModelOrderController;

    iget-object v5, p0, Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl;->mTabContentManager:Lcom/google/android/apps/chrome/compositor/TabContentManager;

    iget-object v6, p0, Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl;->mTabSaver:Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;

    move-object v7, p0

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/chrome/tabmodel/TabModelImpl;-><init>(ZLcom/google/android/apps/chrome/ChromeActivity;Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorUma;Lorg/chromium/chrome/browser/tabmodel/TabModelOrderController;Lcom/google/android/apps/chrome/compositor/TabContentManager;Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;Lorg/chromium/chrome/browser/tabmodel/TabModelDelegate;)V

    .line 210
    new-instance v2, Lcom/google/android/apps/chrome/tabmodel/OffTheRecordTabModel;

    iget-object v3, p0, Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl;->mActivity:Lcom/google/android/apps/chrome/ChromeActivity;

    iget-object v4, p0, Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl;->mUma:Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorUma;

    iget-object v5, p0, Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl;->mOrderController:Lorg/chromium/chrome/browser/tabmodel/TabModelOrderController;

    iget-object v6, p0, Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl;->mTabContentManager:Lcom/google/android/apps/chrome/compositor/TabContentManager;

    iget-object v7, p0, Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl;->mTabSaver:Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;

    move-object v8, p0

    invoke-direct/range {v2 .. v8}, Lcom/google/android/apps/chrome/tabmodel/OffTheRecordTabModel;-><init>(Lcom/google/android/apps/chrome/ChromeActivity;Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorUma;Lorg/chromium/chrome/browser/tabmodel/TabModelOrderController;Lcom/google/android/apps/chrome/compositor/TabContentManager;Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;Lorg/chromium/chrome/browser/tabmodel/TabModelDelegate;)V

    .line 212
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl;->isIncognitoSelected()Z

    move-result v3

    const/4 v4, 0x2

    new-array v4, v4, [Lorg/chromium/chrome/browser/tabmodel/TabModel;

    aput-object v0, v4, v1

    aput-object v2, v4, v9

    invoke-virtual {p0, v3, v4}, Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl;->initialize(Z[Lorg/chromium/chrome/browser/tabmodel/TabModel;)V

    .line 213
    iget-object v3, p0, Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl;->mActivity:Lcom/google/android/apps/chrome/ChromeActivity;

    invoke-virtual {v3, v1}, Lcom/google/android/apps/chrome/ChromeActivity;->getTabCreator(Z)Lcom/google/android/apps/chrome/tabmodel/ChromeTabCreator;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl;->mTabContentManager:Lcom/google/android/apps/chrome/compositor/TabContentManager;

    invoke-virtual {v3, v0, v4}, Lcom/google/android/apps/chrome/tabmodel/ChromeTabCreator;->setTabModel(Lorg/chromium/chrome/browser/tabmodel/TabModel;Lcom/google/android/apps/chrome/compositor/TabContentManager;)V

    .line 214
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl;->mActivity:Lcom/google/android/apps/chrome/ChromeActivity;

    invoke-virtual {v0, v9}, Lcom/google/android/apps/chrome/ChromeActivity;->getTabCreator(Z)Lcom/google/android/apps/chrome/tabmodel/ChromeTabCreator;

    move-result-object v0

    iget-object v3, p0, Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl;->mTabContentManager:Lcom/google/android/apps/chrome/compositor/TabContentManager;

    invoke-virtual {v0, v2, v3}, Lcom/google/android/apps/chrome/tabmodel/ChromeTabCreator;->setTabModel(Lorg/chromium/chrome/browser/tabmodel/TabModel;Lcom/google/android/apps/chrome/compositor/TabContentManager;)V

    .line 216
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl;->mTabSaver:Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->setTabContentManager(Lcom/google/android/apps/chrome/compositor/TabContentManager;)V

    .line 218
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl;->mActivity:Lcom/google/android/apps/chrome/ChromeActivity;

    sget-object v2, Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl;->NOTIFICATIONS:[I

    iget-object v3, p0, Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl;->mNotificationHandler:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;

    invoke-static {v0, v2, v3}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->registerForNotifications(Landroid/content/Context;[ILcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;)V

    .line 220
    iput-boolean v9, p0, Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl;->mActiveState:Z

    .line 223
    new-instance v0, Lcom/google/android/apps/chrome/snapshot/SnapshotStateBroadcastReceiver;

    invoke-virtual {p0, v1}, Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl;->getModel(Z)Lorg/chromium/chrome/browser/tabmodel/TabModel;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl;->mHandler:Landroid/os/Handler;

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/chrome/snapshot/SnapshotStateBroadcastReceiver;-><init>(Lorg/chromium/chrome/browser/tabmodel/TabModel;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl;->mSnapshotDownloadBroadcastReceiver:Landroid/content/BroadcastReceiver;

    .line 225
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl;->mActivity:Lcom/google/android/apps/chrome/ChromeActivity;

    iget-object v1, p0, Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl;->mSnapshotDownloadBroadcastReceiver:Landroid/content/BroadcastReceiver;

    new-instance v2, Landroid/content/IntentFilter;

    const-string/jumbo v3, "com.google.android.apps.chrome.snapshot.ACTION_SNAPSHOT_STATE_UPDATE"

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/chrome/ChromeActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 227
    return-void
.end method

.method public openNewTab(Lorg/chromium/content_public/browser/LoadUrlParams;Lorg/chromium/chrome/browser/tabmodel/TabModel$TabLaunchType;Lorg/chromium/chrome/browser/Tab;Z)Lorg/chromium/chrome/browser/Tab;
    .locals 1

    .prologue
    .line 337
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl;->mActivity:Lcom/google/android/apps/chrome/ChromeActivity;

    invoke-virtual {v0, p4}, Lcom/google/android/apps/chrome/ChromeActivity;->getTabCreator(Z)Lcom/google/android/apps/chrome/tabmodel/ChromeTabCreator;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/apps/chrome/tabmodel/ChromeTabCreator;->createNewTab(Lorg/chromium/content_public/browser/LoadUrlParams;Lorg/chromium/chrome/browser/tabmodel/TabModel$TabLaunchType;Lorg/chromium/chrome/browser/Tab;)Lcom/google/android/apps/chrome/tab/ChromeTab;

    move-result-object v0

    return-object v0
.end method

.method public requestToShowTab(Lorg/chromium/chrome/browser/Tab;Lorg/chromium/chrome/browser/tabmodel/TabModel$TabSelectionType;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 349
    invoke-static {p1}, Lcom/google/android/apps/chrome/tab/ChromeTab;->fromTab(Lorg/chromium/chrome/browser/Tab;)Lcom/google/android/apps/chrome/tab/ChromeTab;

    move-result-object v1

    .line 351
    if-eqz v1, :cond_5

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getLaunchType()Lorg/chromium/chrome/browser/tabmodel/TabModel$TabLaunchType;

    move-result-object v0

    sget-object v2, Lorg/chromium/chrome/browser/tabmodel/TabModel$TabLaunchType;->FROM_EXTERNAL_APP:Lorg/chromium/chrome/browser/tabmodel/TabModel$TabLaunchType;

    if-ne v0, v2, :cond_5

    const/4 v0, 0x1

    .line 354
    :goto_0
    iget-object v2, p0, Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl;->mVisibleTab:Lcom/google/android/apps/chrome/tab/ChromeTab;

    if-eq v2, p1, :cond_0

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lorg/chromium/chrome/browser/Tab;->isNativePage()Z

    move-result v2

    if-nez v2, :cond_0

    .line 355
    invoke-static {p2}, Lorg/chromium/chrome/browser/tabmodel/TabModelBase;->startTabSwitchLatencyTiming(Lorg/chromium/chrome/browser/tabmodel/TabModel$TabSelectionType;)V

    .line 357
    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl;->mVisibleTab:Lcom/google/android/apps/chrome/tab/ChromeTab;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl;->mVisibleTab:Lcom/google/android/apps/chrome/tab/ChromeTab;

    if-eq v2, p1, :cond_3

    iget-object v2, p0, Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl;->mVisibleTab:Lcom/google/android/apps/chrome/tab/ChromeTab;

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/tab/ChromeTab;->canProvideThumbnail()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 361
    iget-object v2, p0, Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl;->mVisibleTab:Lcom/google/android/apps/chrome/tab/ChromeTab;

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/tab/ChromeTab;->isClosing()Z

    move-result v2

    if-nez v2, :cond_2

    if-eqz v0, :cond_1

    sget-object v0, Lorg/chromium/chrome/browser/tabmodel/TabModel$TabSelectionType;->FROM_NEW:Lorg/chromium/chrome/browser/tabmodel/TabModel$TabSelectionType;

    if-eq p2, v0, :cond_2

    .line 363
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl;->mVisibleTab:Lcom/google/android/apps/chrome/tab/ChromeTab;

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl;->cacheTabBitmap(Lorg/chromium/chrome/browser/Tab;)V

    .line 365
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl;->mVisibleTab:Lcom/google/android/apps/chrome/tab/ChromeTab;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->hide()V

    .line 366
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl;->mVisibleTab:Lcom/google/android/apps/chrome/tab/ChromeTab;

    invoke-virtual {v0, v3}, Lcom/google/android/apps/chrome/tab/ChromeTab;->setFullscreenManager(Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;)V

    .line 367
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl;->mTabSaver:Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;

    iget-object v2, p0, Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl;->mVisibleTab:Lcom/google/android/apps/chrome/tab/ChromeTab;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->addTabToSaveQueue(Lcom/google/android/apps/chrome/tab/ChromeTab;)V

    .line 368
    iput-object v3, p0, Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl;->mVisibleTab:Lcom/google/android/apps/chrome/tab/ChromeTab;

    .line 371
    :cond_3
    if-nez p1, :cond_6

    .line 391
    :cond_4
    :goto_1
    return-void

    .line 351
    :cond_5
    const/4 v0, 0x0

    goto :goto_0

    .line 375
    :cond_6
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl;->mVisibleTab:Lcom/google/android/apps/chrome/tab/ChromeTab;

    if-ne v0, p1, :cond_7

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl;->mVisibleTab:Lcom/google/android/apps/chrome/tab/ChromeTab;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->isHidden()Z

    move-result v0

    if-nez v0, :cond_7

    .line 377
    invoke-virtual {p1}, Lorg/chromium/chrome/browser/Tab;->loadIfNeeded()Z

    goto :goto_1

    .line 381
    :cond_7
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl;->mActivity:Lcom/google/android/apps/chrome/ChromeActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ChromeActivity;->getFullscreenManager()Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->setFullscreenManager(Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;)V

    .line 382
    iput-object v1, p0, Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl;->mVisibleTab:Lcom/google/android/apps/chrome/tab/ChromeTab;

    .line 387
    sget-object v0, Lorg/chromium/chrome/browser/tabmodel/TabModel$TabSelectionType;->FROM_EXIT:Lorg/chromium/chrome/browser/tabmodel/TabModel$TabSelectionType;

    if-eq p2, v0, :cond_4

    .line 388
    invoke-virtual {v1, p2}, Lcom/google/android/apps/chrome/tab/ChromeTab;->show(Lorg/chromium/chrome/browser/tabmodel/TabModel$TabSelectionType;)V

    .line 389
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl;->mUma:Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorUma;

    invoke-virtual {p1}, Lorg/chromium/chrome/browser/Tab;->getId()I

    move-result v2

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/tab/ChromeTab;->isBeingRestored()Z

    move-result v1

    invoke-virtual {v0, v2, v1}, Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorUma;->onShowTab(IZ)V

    goto :goto_1
.end method

.method public restoreTabs(Z)V
    .locals 1

    .prologue
    .line 295
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl;->mTabSaver:Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->restoreTabs(Z)V

    .line 296
    return-void
.end method

.method public saveState()V
    .locals 1

    .prologue
    .line 275
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl;->commitAllTabClosures()V

    .line 276
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl;->mTabSaver:Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->saveState()V

    .line 277
    return-void
.end method

.method public selectModel(Z)V
    .locals 2

    .prologue
    .line 245
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl;->getCurrentModel()Lorg/chromium/chrome/browser/tabmodel/TabModel;

    move-result-object v0

    .line 246
    invoke-super {p0, p1}, Lorg/chromium/chrome/browser/tabmodel/TabModelSelectorBase;->selectModel(Z)V

    .line 247
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl;->getCurrentModel()Lorg/chromium/chrome/browser/tabmodel/TabModel;

    move-result-object v1

    .line 248
    if-eq v0, v1, :cond_0

    .line 249
    invoke-interface {v1}, Lorg/chromium/chrome/browser/tabmodel/TabModel;->index()I

    move-result v0

    invoke-static {v1, v0}, Lorg/chromium/chrome/browser/tabmodel/TabModelUtils;->setIndex(Lorg/chromium/chrome/browser/tabmodel/TabModel;I)V

    .line 250
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl;->blockingNotifyModelSelected()V

    .line 255
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v1, Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl$2;

    invoke-direct {v1, p0}, Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl$2;-><init>(Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 262
    :cond_0
    return-void
.end method

.method public tryToRestoreTabState(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 306
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl;->isSessionRestoreInProgress()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 307
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl;->mTabSaver:Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->restoreTabState(Ljava/lang/String;)Z

    move-result v0

    goto :goto_0
.end method
