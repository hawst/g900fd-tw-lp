.class Lcom/google/android/apps/chrome/widget/tabstrip/TabStripManager$1;
.super Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;
.source "TabStripManager.java"


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/widget/tabstrip/TabStripManager;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/widget/tabstrip/TabStripManager;)V
    .locals 0

    .prologue
    .line 203
    iput-object p1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStripManager$1;->this$0:Lcom/google/android/apps/chrome/widget/tabstrip/TabStripManager;

    invoke-direct {p0}, Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3

    .prologue
    .line 206
    iget v0, p1, Landroid/os/Message;->what:I

    sparse-switch v0, :sswitch_data_0

    .line 217
    :goto_0
    return-void

    .line 208
    :sswitch_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStripManager$1;->this$0:Lcom/google/android/apps/chrome/widget/tabstrip/TabStripManager;

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v1

    const-string/jumbo v2, "incognito"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    # invokes: Lcom/google/android/apps/chrome/widget/tabstrip/TabStripManager;->handleTabModelSelection(Z)V
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStripManager;->access$000(Lcom/google/android/apps/chrome/widget/tabstrip/TabStripManager;Z)V

    goto :goto_0

    .line 211
    :sswitch_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStripManager$1;->this$0:Lcom/google/android/apps/chrome/widget/tabstrip/TabStripManager;

    const/4 v1, 0x1

    # invokes: Lcom/google/android/apps/chrome/widget/tabstrip/TabStripManager;->setFadeRequired(Z)V
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStripManager;->access$100(Lcom/google/android/apps/chrome/widget/tabstrip/TabStripManager;Z)V

    goto :goto_0

    .line 214
    :sswitch_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStripManager$1;->this$0:Lcom/google/android/apps/chrome/widget/tabstrip/TabStripManager;

    const/4 v1, 0x0

    # invokes: Lcom/google/android/apps/chrome/widget/tabstrip/TabStripManager;->setFadeRequired(Z)V
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStripManager;->access$100(Lcom/google/android/apps/chrome/widget/tabstrip/TabStripManager;Z)V

    goto :goto_0

    .line 206
    nop

    :sswitch_data_0
    .sparse-switch
        0xc -> :sswitch_0
        0x11 -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method
