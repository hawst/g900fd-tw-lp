.class Lcom/google/android/apps/chrome/icing/IcingController$2;
.super Lcom/google/android/apps/chrome/icing/ConnectedTask;
.source "IcingController.java"


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/icing/IcingController;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/icing/IcingController;Lcom/google/android/apps/chrome/icing/GoogleApiIcingClient;Ljava/util/concurrent/ScheduledThreadPoolExecutor;)V
    .locals 0

    .prologue
    .line 99
    iput-object p1, p0, Lcom/google/android/apps/chrome/icing/IcingController$2;->this$0:Lcom/google/android/apps/chrome/icing/IcingController;

    invoke-direct {p0, p2, p3}, Lcom/google/android/apps/chrome/icing/ConnectedTask;-><init>(Lcom/google/android/apps/chrome/icing/GoogleApiIcingClient;Ljava/util/concurrent/ScheduledThreadPoolExecutor;)V

    return-void
.end method


# virtual methods
.method protected doWhenConnected(Lcom/google/android/apps/chrome/icing/GoogleApiIcingClient;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 107
    iget-object v0, p0, Lcom/google/android/apps/chrome/icing/IcingController$2;->this$0:Lcom/google/android/apps/chrome/icing/IcingController;

    # getter for: Lcom/google/android/apps/chrome/icing/IcingController;->mUsageReportingEnabled:Ljava/util/concurrent/atomic/AtomicBoolean;
    invoke-static {v0}, Lcom/google/android/apps/chrome/icing/IcingController;->access$200(Lcom/google/android/apps/chrome/icing/IcingController;)Ljava/util/concurrent/atomic/AtomicBoolean;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 108
    iget-object v0, p0, Lcom/google/android/apps/chrome/icing/IcingController$2;->this$0:Lcom/google/android/apps/chrome/icing/IcingController;

    # getter for: Lcom/google/android/apps/chrome/icing/IcingController;->mUsageReportingTaskActive:Ljava/util/concurrent/atomic/AtomicBoolean;
    invoke-static {v0}, Lcom/google/android/apps/chrome/icing/IcingController;->access$300(Lcom/google/android/apps/chrome/icing/IcingController;)Ljava/util/concurrent/atomic/AtomicBoolean;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 109
    iget-object v0, p0, Lcom/google/android/apps/chrome/icing/IcingController$2;->this$0:Lcom/google/android/apps/chrome/icing/IcingController;

    # getter for: Lcom/google/android/apps/chrome/icing/IcingController;->mPrefs:Landroid/content/SharedPreferences;
    invoke-static {v0}, Lcom/google/android/apps/chrome/icing/IcingController;->access$000(Lcom/google/android/apps/chrome/icing/IcingController;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string/jumbo v1, "com.google.android.apps.chrome.icing.IcingController.DATA_CLEARED"

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Lcom/google/android/apps/chrome/icing/GoogleApiIcingClient;->clearData()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 110
    iget-object v0, p0, Lcom/google/android/apps/chrome/icing/IcingController$2;->this$0:Lcom/google/android/apps/chrome/icing/IcingController;

    # getter for: Lcom/google/android/apps/chrome/icing/IcingController;->mPrefs:Landroid/content/SharedPreferences;
    invoke-static {v0}, Lcom/google/android/apps/chrome/icing/IcingController;->access$000(Lcom/google/android/apps/chrome/icing/IcingController;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string/jumbo v1, "com.google.android.apps.chrome.icing.IcingController.VERSION"

    const/4 v2, 0x2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 111
    iget-object v0, p0, Lcom/google/android/apps/chrome/icing/IcingController$2;->this$0:Lcom/google/android/apps/chrome/icing/IcingController;

    # getter for: Lcom/google/android/apps/chrome/icing/IcingController;->mPrefs:Landroid/content/SharedPreferences;
    invoke-static {v0}, Lcom/google/android/apps/chrome/icing/IcingController;->access$000(Lcom/google/android/apps/chrome/icing/IcingController;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string/jumbo v1, "com.google.android.apps.chrome.icing.IcingController.DATA_CLEARED"

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 113
    :cond_0
    return-void
.end method

.method protected getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 102
    const-string/jumbo v0, "ClearData"

    return-object v0
.end method
