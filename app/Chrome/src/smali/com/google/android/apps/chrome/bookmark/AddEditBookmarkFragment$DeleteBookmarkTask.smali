.class Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment$DeleteBookmarkTask;
.super Lcom/google/android/apps/chrome/bookmark/AsyncTaskFragment$FragmentAsyncTask;
.source "AddEditBookmarkFragment.java"


# instance fields
.field private final mBookmarkId:J

.field private final mContext:Landroid/content/Context;

.field final synthetic this$0:Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;J)V
    .locals 2

    .prologue
    .line 641
    iput-object p1, p0, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment$DeleteBookmarkTask;->this$0:Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;

    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/bookmark/AsyncTaskFragment$FragmentAsyncTask;-><init>(Lcom/google/android/apps/chrome/bookmark/AsyncTaskFragment;)V

    .line 642
    invoke-virtual {p1}, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment$DeleteBookmarkTask;->mContext:Landroid/content/Context;

    .line 643
    iput-wide p2, p0, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment$DeleteBookmarkTask;->mBookmarkId:J

    .line 644
    return-void
.end method


# virtual methods
.method protected onTaskFinished()V
    .locals 1

    .prologue
    .line 656
    iget-object v0, p0, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment$DeleteBookmarkTask;->this$0:Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;

    # getter for: Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;->mActionListener:Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment$OnActionListener;
    invoke-static {v0}, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;->access$800(Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;)Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment$OnActionListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment$OnActionListener;->onRemove()V

    .line 657
    return-void
.end method

.method protected runBackgroundTask()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 648
    iget-object v0, p0, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment$DeleteBookmarkTask;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->getBookmarksUri(Landroid/content/Context;)Landroid/net/Uri;

    move-result-object v0

    .line 649
    iget-object v1, p0, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment$DeleteBookmarkTask;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    iget-wide v2, p0, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment$DeleteBookmarkTask;->mBookmarkId:J

    invoke-static {v0, v2, v3}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v1, v0, v4, v4}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 652
    return-void
.end method

.method protected setDependentUIEnabled(Z)V
    .locals 1

    .prologue
    .line 661
    iget-object v0, p0, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment$DeleteBookmarkTask;->this$0:Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;

    # getter for: Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;->mOkButton:Landroid/widget/Button;
    invoke-static {v0}, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;->access$200(Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 662
    iget-object v0, p0, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment$DeleteBookmarkTask;->this$0:Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;

    # getter for: Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;->mRemoveButton:Landroid/widget/Button;
    invoke-static {v0}, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;->access$700(Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 663
    iget-object v0, p0, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment$DeleteBookmarkTask;->this$0:Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;

    # getter for: Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;->mCancelButton:Landroid/widget/Button;
    invoke-static {v0}, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;->access$900(Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 664
    iget-object v0, p0, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment$DeleteBookmarkTask;->this$0:Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;

    # getter for: Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;->mActionListener:Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment$OnActionListener;
    invoke-static {v0}, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;->access$800(Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;)Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment$OnActionListener;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment$OnActionListener;->setBackEnabled(Z)V

    .line 665
    return-void
.end method
