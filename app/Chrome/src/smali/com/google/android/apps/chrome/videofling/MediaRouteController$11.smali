.class Lcom/google/android/apps/chrome/videofling/MediaRouteController$11;
.super Landroid/content/BroadcastReceiver;
.source "MediaRouteController.java"


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/videofling/MediaRouteController;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/videofling/MediaRouteController;)V
    .locals 0

    .prologue
    .line 921
    iput-object p1, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController$11;->this$0:Lcom/google/android/apps/chrome/videofling/MediaRouteController;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2

    .prologue
    .line 924
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController$11;->this$0:Lcom/google/android/apps/chrome/videofling/MediaRouteController;

    # getter for: Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mDebug:Z
    invoke-static {v0}, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->access$400(Lcom/google/android/apps/chrome/videofling/MediaRouteController;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 925
    const-string/jumbo v0, "Got a session broadcast intent from the MRP: "

    invoke-static {v0, p2}, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->dumpIntentToLog(Ljava/lang/String;Landroid/content/Intent;)V

    .line 928
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController$11;->this$0:Lcom/google/android/apps/chrome/videofling/MediaRouteController;

    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    # invokes: Lcom/google/android/apps/chrome/videofling/MediaRouteController;->processSessionStatusBundle(Landroid/os/Bundle;)V
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->access$1400(Lcom/google/android/apps/chrome/videofling/MediaRouteController;Landroid/os/Bundle;)V

    .line 929
    return-void
.end method
