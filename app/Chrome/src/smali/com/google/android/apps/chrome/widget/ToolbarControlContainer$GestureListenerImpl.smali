.class Lcom/google/android/apps/chrome/widget/ToolbarControlContainer$GestureListenerImpl;
.super Landroid/view/GestureDetector$SimpleOnGestureListener;
.source "ToolbarControlContainer.java"


# instance fields
.field private final mMotionStartPoint:Landroid/graphics/PointF;

.field final synthetic this$0:Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;)V
    .locals 1

    .prologue
    .line 167
    iput-object p1, p0, Lcom/google/android/apps/chrome/widget/ToolbarControlContainer$GestureListenerImpl;->this$0:Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;

    invoke-direct {p0}, Landroid/view/GestureDetector$SimpleOnGestureListener;-><init>()V

    .line 168
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/widget/ToolbarControlContainer$GestureListenerImpl;->mMotionStartPoint:Landroid/graphics/PointF;

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;Lcom/google/android/apps/chrome/widget/ToolbarControlContainer$1;)V
    .locals 0

    .prologue
    .line 167
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/widget/ToolbarControlContainer$GestureListenerImpl;-><init>(Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;)V

    return-void
.end method


# virtual methods
.method public onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 223
    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/ToolbarControlContainer$GestureListenerImpl;->this$0:Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;

    # getter for: Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;->mSwipeHandler:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeHandler;
    invoke-static {v1}, Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;->access$500(Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;)Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeHandler;

    move-result-object v1

    if-nez v1, :cond_1

    .line 235
    :cond_0
    :goto_0
    return v0

    .line 224
    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/ToolbarControlContainer$GestureListenerImpl;->this$0:Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;

    # getter for: Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;->mForwardingDirection:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;
    invoke-static {v1}, Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;->access$800(Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;)Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;->UNKNOWN:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;

    if-eq v1, v2, :cond_0

    .line 225
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawX()F

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/ToolbarControlContainer$GestureListenerImpl;->this$0:Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;

    # getter for: Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;->mPxToDp:F
    invoke-static {v1}, Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;->access$700(Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;)F

    move-result v1

    mul-float/2addr v1, v0

    .line 226
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawY()F

    move-result v0

    iget-object v2, p0, Lcom/google/android/apps/chrome/widget/ToolbarControlContainer$GestureListenerImpl;->this$0:Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;

    # getter for: Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;->mPxToDp:F
    invoke-static {v2}, Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;->access$700(Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;)F

    move-result v2

    mul-float/2addr v2, v0

    .line 227
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawX()F

    move-result v0

    iget-object v3, p0, Lcom/google/android/apps/chrome/widget/ToolbarControlContainer$GestureListenerImpl;->mMotionStartPoint:Landroid/graphics/PointF;

    iget v3, v3, Landroid/graphics/PointF;->x:F

    sub-float/2addr v0, v3

    iget-object v3, p0, Lcom/google/android/apps/chrome/widget/ToolbarControlContainer$GestureListenerImpl;->this$0:Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;

    # getter for: Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;->mPxToDp:F
    invoke-static {v3}, Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;->access$700(Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;)F

    move-result v3

    mul-float/2addr v3, v0

    .line 228
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawY()F

    move-result v0

    iget-object v4, p0, Lcom/google/android/apps/chrome/widget/ToolbarControlContainer$GestureListenerImpl;->mMotionStartPoint:Landroid/graphics/PointF;

    iget v4, v4, Landroid/graphics/PointF;->y:F

    sub-float/2addr v0, v4

    iget-object v4, p0, Lcom/google/android/apps/chrome/widget/ToolbarControlContainer$GestureListenerImpl;->this$0:Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;

    # getter for: Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;->mPxToDp:F
    invoke-static {v4}, Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;->access$700(Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;)F

    move-result v4

    mul-float/2addr v4, v0

    .line 229
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/ToolbarControlContainer$GestureListenerImpl;->this$0:Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;

    # getter for: Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;->mPxToDp:F
    invoke-static {v0}, Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;->access$700(Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;)F

    move-result v0

    mul-float v5, p3, v0

    .line 230
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/ToolbarControlContainer$GestureListenerImpl;->this$0:Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;

    # getter for: Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;->mPxToDp:F
    invoke-static {v0}, Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;->access$700(Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;)F

    move-result v0

    mul-float v6, p4, v0

    .line 232
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/ToolbarControlContainer$GestureListenerImpl;->this$0:Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;

    # getter for: Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;->mSwipeHandler:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeHandler;
    invoke-static {v0}, Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;->access$500(Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;)Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeHandler;

    move-result-object v0

    invoke-interface/range {v0 .. v6}, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeHandler;->swipeFlingOccurred(FFFFFF)V

    .line 233
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public onScroll(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 8

    .prologue
    const/high16 v7, 0x41200000    # 10.0f

    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 172
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/ToolbarControlContainer$GestureListenerImpl;->this$0:Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;

    # getter for: Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;->mSwipeHandler:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeHandler;
    invoke-static {v0}, Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;->access$500(Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;)Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeHandler;

    move-result-object v0

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/ToolbarControlContainer$GestureListenerImpl;->this$0:Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;

    # invokes: Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;->isOnTabStrip(Landroid/view/MotionEvent;)Z
    invoke-static {v0, p1}, Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;->access$600(Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    move v0, v4

    .line 218
    :goto_0
    return v0

    .line 174
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/ToolbarControlContainer$GestureListenerImpl;->this$0:Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;

    # getter for: Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;->mToolbar:Lcom/google/android/apps/chrome/toolbar/Toolbar;
    invoke-static {v0}, Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;->access$300(Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;)Lcom/google/android/apps/chrome/toolbar/Toolbar;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/chrome/toolbar/Toolbar;->getDelegate()Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;->getMenuButtonHelper()Lorg/chromium/chrome/browser/appmenu/AppMenuButtonHelper;

    move-result-object v0

    .line 175
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/appmenu/AppMenuButtonHelper;->isAppMenuActive()Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v4

    .line 176
    goto :goto_0

    .line 179
    :cond_2
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawX()F

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/ToolbarControlContainer$GestureListenerImpl;->this$0:Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;

    # getter for: Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;->mPxToDp:F
    invoke-static {v1}, Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;->access$700(Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;)F

    move-result v1

    mul-float/2addr v1, v0

    .line 180
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawY()F

    move-result v0

    iget-object v2, p0, Lcom/google/android/apps/chrome/widget/ToolbarControlContainer$GestureListenerImpl;->this$0:Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;

    # getter for: Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;->mPxToDp:F
    invoke-static {v2}, Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;->access$700(Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;)F

    move-result v2

    mul-float/2addr v2, v0

    .line 182
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/ToolbarControlContainer$GestureListenerImpl;->this$0:Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;

    # getter for: Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;->mForwardingDirection:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;
    invoke-static {v0}, Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;->access$800(Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;)Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;

    move-result-object v0

    sget-object v5, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;->UNKNOWN:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;

    if-ne v0, v5, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/ToolbarControlContainer$GestureListenerImpl;->this$0:Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;

    # getter for: Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;->mToolbar:Lcom/google/android/apps/chrome/toolbar/Toolbar;
    invoke-static {v0}, Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;->access$300(Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;)Lcom/google/android/apps/chrome/toolbar/Toolbar;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/chrome/toolbar/Toolbar;->getDelegate()Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;->urlHasFocus()Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/ToolbarControlContainer$GestureListenerImpl;->this$0:Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;

    # getter for: Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;->mFindInPageShowing:Z
    invoke-static {v0}, Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;->access$000(Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;)Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/ToolbarControlContainer$GestureListenerImpl;->this$0:Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v5, p0, Lcom/google/android/apps/chrome/widget/ToolbarControlContainer$GestureListenerImpl;->this$0:Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;

    invoke-static {v0, v5}, Lorg/chromium/ui/UiUtils;->isKeyboardShowing(Landroid/content/Context;Landroid/view/View;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 186
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawX()F

    move-result v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawX()F

    move-result v5

    sub-float/2addr v0, v5

    iget-object v5, p0, Lcom/google/android/apps/chrome/widget/ToolbarControlContainer$GestureListenerImpl;->this$0:Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;

    # getter for: Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;->mPxToDp:F
    invoke-static {v5}, Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;->access$700(Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;)F

    move-result v5

    mul-float/2addr v0, v5

    .line 187
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawY()F

    move-result v5

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result v6

    sub-float/2addr v5, v6

    iget-object v6, p0, Lcom/google/android/apps/chrome/widget/ToolbarControlContainer$GestureListenerImpl;->this$0:Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;

    # getter for: Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;->mPxToDp:F
    invoke-static {v6}, Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;->access$700(Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;)F

    move-result v6

    mul-float/2addr v5, v6

    .line 188
    const/high16 v6, 0x40a00000    # 5.0f

    cmpl-float v5, v5, v6

    if-lez v5, :cond_4

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v5

    cmpg-float v5, v5, v7

    if-gez v5, :cond_4

    cmpg-float v5, p4, v3

    if-gez v5, :cond_4

    iget-object v5, p0, Lcom/google/android/apps/chrome/widget/ToolbarControlContainer$GestureListenerImpl;->this$0:Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawX()F

    move-result v6

    # invokes: Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;->isOverMenuButton(F)Z
    invoke-static {v5, v6}, Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;->access$900(Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;F)Z

    move-result v5

    if-nez v5, :cond_4

    .line 191
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/ToolbarControlContainer$GestureListenerImpl;->this$0:Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;

    # getter for: Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;->mSwipeHandler:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeHandler;
    invoke-static {v0}, Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;->access$500(Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;)Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeHandler;

    move-result-object v0

    sget-object v3, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;->DOWN:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;

    invoke-interface {v0, v3}, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeHandler;->isSwipeEnabled(Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 192
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/ToolbarControlContainer$GestureListenerImpl;->this$0:Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;

    sget-object v3, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;->DOWN:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;

    # setter for: Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;->mForwardingDirection:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;
    invoke-static {v0, v3}, Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;->access$802(Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;)Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;

    .line 193
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/ToolbarControlContainer$GestureListenerImpl;->this$0:Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;

    # getter for: Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;->mSwipeHandler:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeHandler;
    invoke-static {v0}, Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;->access$500(Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;)Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeHandler;

    move-result-object v0

    sget-object v3, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;->DOWN:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;

    invoke-interface {v0, v3, v1, v2}, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeHandler;->swipeStarted(Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;FF)V

    .line 194
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/ToolbarControlContainer$GestureListenerImpl;->mMotionStartPoint:Landroid/graphics/PointF;

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawX()F

    move-result v3

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawY()F

    move-result v5

    invoke-virtual {v0, v3, v5}, Landroid/graphics/PointF;->set(FF)V

    .line 208
    :cond_3
    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/ToolbarControlContainer$GestureListenerImpl;->this$0:Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;

    # getter for: Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;->mForwardingDirection:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;
    invoke-static {v0}, Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;->access$800(Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;)Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;

    move-result-object v0

    sget-object v3, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;->UNKNOWN:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;

    if-eq v0, v3, :cond_6

    .line 209
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawX()F

    move-result v0

    iget-object v3, p0, Lcom/google/android/apps/chrome/widget/ToolbarControlContainer$GestureListenerImpl;->mMotionStartPoint:Landroid/graphics/PointF;

    iget v3, v3, Landroid/graphics/PointF;->x:F

    sub-float/2addr v0, v3

    iget-object v3, p0, Lcom/google/android/apps/chrome/widget/ToolbarControlContainer$GestureListenerImpl;->this$0:Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;

    # getter for: Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;->mPxToDp:F
    invoke-static {v3}, Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;->access$700(Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;)F

    move-result v3

    mul-float v5, v0, v3

    .line 210
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawY()F

    move-result v0

    iget-object v3, p0, Lcom/google/android/apps/chrome/widget/ToolbarControlContainer$GestureListenerImpl;->mMotionStartPoint:Landroid/graphics/PointF;

    iget v3, v3, Landroid/graphics/PointF;->y:F

    sub-float/2addr v0, v3

    iget-object v3, p0, Lcom/google/android/apps/chrome/widget/ToolbarControlContainer$GestureListenerImpl;->this$0:Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;

    # getter for: Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;->mPxToDp:F
    invoke-static {v3}, Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;->access$700(Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;)F

    move-result v3

    mul-float v6, v0, v3

    .line 211
    neg-float v0, p3

    iget-object v3, p0, Lcom/google/android/apps/chrome/widget/ToolbarControlContainer$GestureListenerImpl;->this$0:Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;

    # getter for: Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;->mPxToDp:F
    invoke-static {v3}, Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;->access$700(Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;)F

    move-result v3

    mul-float/2addr v3, v0

    .line 212
    neg-float v0, p4

    iget-object v4, p0, Lcom/google/android/apps/chrome/widget/ToolbarControlContainer$GestureListenerImpl;->this$0:Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;

    # getter for: Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;->mPxToDp:F
    invoke-static {v4}, Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;->access$700(Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;)F

    move-result v4

    mul-float/2addr v4, v0

    .line 214
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/ToolbarControlContainer$GestureListenerImpl;->this$0:Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;

    # getter for: Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;->mSwipeHandler:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeHandler;
    invoke-static {v0}, Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;->access$500(Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;)Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeHandler;

    move-result-object v0

    invoke-interface/range {v0 .. v6}, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeHandler;->swipeUpdated(FFFFFF)V

    .line 215
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 196
    :cond_4
    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v5

    cmpl-float v5, v5, v7

    if-lez v5, :cond_3

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v5

    iget-object v6, p0, Lcom/google/android/apps/chrome/widget/ToolbarControlContainer$GestureListenerImpl;->this$0:Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;

    invoke-virtual {v6}, Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;->getBottom()I

    move-result v6

    int-to-float v6, v6

    cmpg-float v5, v5, v6

    if-gez v5, :cond_3

    .line 197
    cmpl-float v0, v0, v3

    if-lez v0, :cond_5

    sget-object v0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;->RIGHT:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;

    .line 199
    :goto_2
    iget-object v5, p0, Lcom/google/android/apps/chrome/widget/ToolbarControlContainer$GestureListenerImpl;->this$0:Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;

    # getter for: Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;->mSwipeHandler:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeHandler;
    invoke-static {v5}, Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;->access$500(Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;)Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeHandler;

    move-result-object v5

    invoke-interface {v5, v0}, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeHandler;->isSwipeEnabled(Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 200
    iget-object v5, p0, Lcom/google/android/apps/chrome/widget/ToolbarControlContainer$GestureListenerImpl;->this$0:Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;

    # setter for: Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;->mForwardingDirection:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;
    invoke-static {v5, v0}, Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;->access$802(Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;)Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;

    .line 201
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/ToolbarControlContainer$GestureListenerImpl;->this$0:Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;

    # getter for: Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;->mSwipeHandler:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeHandler;
    invoke-static {v0}, Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;->access$500(Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;)Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeHandler;

    move-result-object v0

    iget-object v5, p0, Lcom/google/android/apps/chrome/widget/ToolbarControlContainer$GestureListenerImpl;->this$0:Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;

    # getter for: Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;->mForwardingDirection:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;
    invoke-static {v5}, Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;->access$800(Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;)Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;

    move-result-object v5

    invoke-interface {v0, v5, v1, v2}, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeHandler;->swipeStarted(Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;FF)V

    .line 202
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/ToolbarControlContainer$GestureListenerImpl;->mMotionStartPoint:Landroid/graphics/PointF;

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawX()F

    move-result v5

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawY()F

    move-result v6

    invoke-virtual {v0, v5, v6}, Landroid/graphics/PointF;->set(FF)V

    move p3, v3

    .line 203
    goto/16 :goto_1

    .line 197
    :cond_5
    sget-object v0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;->LEFT:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;

    goto :goto_2

    :cond_6
    move v0, v4

    .line 218
    goto/16 :goto_0
.end method
