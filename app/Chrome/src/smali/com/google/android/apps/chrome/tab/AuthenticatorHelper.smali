.class Lcom/google/android/apps/chrome/tab/AuthenticatorHelper;
.super Ljava/lang/Object;
.source "AuthenticatorHelper.java"


# static fields
.field static final DATA_KEY_NAME:Ljava/lang/String; = "data"

.field static final ERROR_AUTHENTICATOR_NOT_INSTALLED:I = 0x1

.field static final ERROR_KEY_NAME:Ljava/lang/String; = "errorCode"

.field static final ERROR_SUCCESS:I = 0x0

.field static final ERROR_UNKNOWN_ERROR:I = 0x3

.field static final ERROR_USER_CANCELED:I = 0x2

.field static final INTENT_URL_KEY_NAME:Ljava/lang/String; = "intentURL"

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mAction:Ljava/lang/String;

.field private mIntentCallback:Lorg/chromium/ui/base/WindowAndroid$IntentCallback;

.field private mIntentUrl:Ljava/lang/String;

.field private mOriginalTabUrl:Ljava/lang/String;

.field private final mTab:Lcom/google/android/apps/chrome/tab/ChromeTab;

.field private mTestDelegate:Lcom/google/android/apps/chrome/tab/AuthenticatorHelper$TestDelegate;

.field private final mWindowAndroid:Lorg/chromium/ui/base/WindowAndroid;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 28
    const-class v0, Lcom/google/android/apps/chrome/tab/AuthenticatorHelper;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/chrome/tab/AuthenticatorHelper;->TAG:Ljava/lang/String;

    return-void
.end method

.method constructor <init>(Lcom/google/android/apps/chrome/tab/ChromeTab;Lorg/chromium/ui/base/WindowAndroid;)V
    .locals 1

    .prologue
    .line 75
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 59
    const-string/jumbo v0, "com.google.android.apps.authenticator.AUTHENTICATE"

    iput-object v0, p0, Lcom/google/android/apps/chrome/tab/AuthenticatorHelper;->mAction:Ljava/lang/String;

    .line 76
    iput-object p1, p0, Lcom/google/android/apps/chrome/tab/AuthenticatorHelper;->mTab:Lcom/google/android/apps/chrome/tab/ChromeTab;

    .line 77
    iput-object p2, p0, Lcom/google/android/apps/chrome/tab/AuthenticatorHelper;->mWindowAndroid:Lorg/chromium/ui/base/WindowAndroid;

    .line 78
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/chrome/tab/AuthenticatorHelper;I)V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/tab/AuthenticatorHelper;->sendErrorToPage(I)V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/apps/chrome/tab/AuthenticatorHelper;)Lcom/google/android/apps/chrome/tab/ChromeTab;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/AuthenticatorHelper;->mTab:Lcom/google/android/apps/chrome/tab/ChromeTab;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/apps/chrome/tab/AuthenticatorHelper;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/AuthenticatorHelper;->mOriginalTabUrl:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/apps/chrome/tab/AuthenticatorHelper;ILjava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 27
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/chrome/tab/AuthenticatorHelper;->buildJsonMessage(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/apps/chrome/tab/AuthenticatorHelper;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/tab/AuthenticatorHelper;->sendMessageToPage(Ljava/lang/String;)V

    return-void
.end method

.method private buildJsonMessage(ILjava/lang/String;)Ljava/lang/String;
    .locals 6

    .prologue
    .line 161
    new-instance v0, Ljava/io/StringWriter;

    invoke-direct {v0}, Ljava/io/StringWriter;-><init>()V

    .line 162
    new-instance v1, Landroid/util/JsonWriter;

    invoke-direct {v1, v0}, Landroid/util/JsonWriter;-><init>(Ljava/io/Writer;)V

    .line 164
    :try_start_0
    invoke-virtual {v1}, Landroid/util/JsonWriter;->beginObject()Landroid/util/JsonWriter;

    .line 165
    const-string/jumbo v2, "intentURL"

    invoke-virtual {v1, v2}, Landroid/util/JsonWriter;->name(Ljava/lang/String;)Landroid/util/JsonWriter;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/chrome/tab/AuthenticatorHelper;->mIntentUrl:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/util/JsonWriter;->value(Ljava/lang/String;)Landroid/util/JsonWriter;

    .line 166
    const-string/jumbo v2, "errorCode"

    invoke-virtual {v1, v2}, Landroid/util/JsonWriter;->name(Ljava/lang/String;)Landroid/util/JsonWriter;

    move-result-object v2

    int-to-long v4, p1

    invoke-virtual {v2, v4, v5}, Landroid/util/JsonWriter;->value(J)Landroid/util/JsonWriter;

    .line 167
    if-eqz p2, :cond_0

    .line 168
    const-string/jumbo v2, "data"

    invoke-virtual {v1, v2}, Landroid/util/JsonWriter;->name(Ljava/lang/String;)Landroid/util/JsonWriter;

    move-result-object v2

    invoke-virtual {v2, p2}, Landroid/util/JsonWriter;->value(Ljava/lang/String;)Landroid/util/JsonWriter;

    .line 170
    :cond_0
    invoke-virtual {v1}, Landroid/util/JsonWriter;->endObject()Landroid/util/JsonWriter;

    .line 171
    invoke-virtual {v1}, Landroid/util/JsonWriter;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 176
    invoke-virtual {v0}, Ljava/io/StringWriter;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    .line 173
    :catch_0
    move-exception v0

    sget-object v0, Lcom/google/android/apps/chrome/tab/AuthenticatorHelper;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "Failed to serialize message"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 174
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private sendErrorToPage(I)V
    .locals 1

    .prologue
    .line 196
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/chrome/tab/AuthenticatorHelper;->buildJsonMessage(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/tab/AuthenticatorHelper;->sendMessageToPage(Ljava/lang/String;)V

    .line 197
    return-void
.end method

.method private sendMessageToPage(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 184
    const-string/jumbo v0, "\\"

    const-string/jumbo v1, "\\\\"

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 185
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 186
    const-string/jumbo v2, "window.postMessage(\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 187
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 188
    const-string/jumbo v0, "\', \'*\');"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 189
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/AuthenticatorHelper;->mTab:Lcom/google/android/apps/chrome/tab/ChromeTab;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getContentViewCore()Lorg/chromium/content/browser/ContentViewCore;

    move-result-object v0

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lorg/chromium/content/browser/ContentViewCore;->evaluateJavaScript(Ljava/lang/String;Lorg/chromium/content_public/browser/JavaScriptCallback;)V

    .line 190
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/AuthenticatorHelper;->mTestDelegate:Lcom/google/android/apps/chrome/tab/AuthenticatorHelper$TestDelegate;

    if-eqz v0, :cond_0

    .line 191
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/AuthenticatorHelper;->mTestDelegate:Lcom/google/android/apps/chrome/tab/AuthenticatorHelper$TestDelegate;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/tab/AuthenticatorHelper$TestDelegate;->messageSentToPage()V

    .line 193
    :cond_0
    return-void
.end method


# virtual methods
.method public handleAuthenticatorUrl(Ljava/lang/String;)Z
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 91
    const/4 v2, 0x1

    :try_start_0
    invoke-static {p1, v2}, Landroid/content/Intent;->parseUri(Ljava/lang/String;I)Landroid/content/Intent;
    :try_end_0
    .catch Ljava/net/URISyntaxException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 99
    const-string/jumbo v3, "android.intent.category.BROWSABLE"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 100
    invoke-virtual {v2, v6}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 101
    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0xf

    if-lt v3, v4, :cond_0

    .line 102
    invoke-virtual {v2}, Landroid/content/Intent;->getSelector()Landroid/content/Intent;

    move-result-object v3

    .line 103
    if-eqz v3, :cond_0

    .line 104
    const-string/jumbo v4, "android.intent.category.BROWSABLE"

    invoke-virtual {v3, v4}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 105
    invoke-virtual {v3, v6}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 109
    :cond_0
    iget-object v3, p0, Lcom/google/android/apps/chrome/tab/AuthenticatorHelper;->mAction:Ljava/lang/String;

    invoke-virtual {v2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 157
    :goto_0
    return v0

    .line 92
    :catch_0
    move-exception v1

    .line 93
    const-string/jumbo v2, "Chrome"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "Bad URI "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, ": "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/net/URISyntaxException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 111
    :cond_1
    iget-object v3, p0, Lcom/google/android/apps/chrome/tab/AuthenticatorHelper;->mTab:Lcom/google/android/apps/chrome/tab/ChromeTab;

    invoke-virtual {v3}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getUrl()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/apps/chrome/tab/AuthenticatorHelper;->mOriginalTabUrl:Ljava/lang/String;

    .line 112
    iput-object p1, p0, Lcom/google/android/apps/chrome/tab/AuthenticatorHelper;->mIntentUrl:Ljava/lang/String;

    .line 115
    iget-object v3, p0, Lcom/google/android/apps/chrome/tab/AuthenticatorHelper;->mWindowAndroid:Lorg/chromium/ui/base/WindowAndroid;

    invoke-virtual {v3, v2}, Lorg/chromium/ui/base/WindowAndroid;->canResolveActivity(Landroid/content/Intent;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 116
    invoke-direct {p0, v1}, Lcom/google/android/apps/chrome/tab/AuthenticatorHelper;->sendErrorToPage(I)V

    move v0, v1

    .line 117
    goto :goto_0

    .line 121
    :cond_2
    iget-object v3, p0, Lcom/google/android/apps/chrome/tab/AuthenticatorHelper;->mIntentCallback:Lorg/chromium/ui/base/WindowAndroid$IntentCallback;

    if-eqz v3, :cond_3

    .line 122
    iget-object v3, p0, Lcom/google/android/apps/chrome/tab/AuthenticatorHelper;->mWindowAndroid:Lorg/chromium/ui/base/WindowAndroid;

    iget-object v4, p0, Lcom/google/android/apps/chrome/tab/AuthenticatorHelper;->mIntentCallback:Lorg/chromium/ui/base/WindowAndroid$IntentCallback;

    invoke-virtual {v3, v4}, Lorg/chromium/ui/base/WindowAndroid;->removeIntentCallback(Lorg/chromium/ui/base/WindowAndroid$IntentCallback;)Z

    .line 125
    :cond_3
    new-instance v3, Lcom/google/android/apps/chrome/tab/AuthenticatorHelper$1;

    invoke-direct {v3, p0}, Lcom/google/android/apps/chrome/tab/AuthenticatorHelper$1;-><init>(Lcom/google/android/apps/chrome/tab/AuthenticatorHelper;)V

    iput-object v3, p0, Lcom/google/android/apps/chrome/tab/AuthenticatorHelper;->mIntentCallback:Lorg/chromium/ui/base/WindowAndroid$IntentCallback;

    .line 151
    const-string/jumbo v3, "referrer"

    iget-object v4, p0, Lcom/google/android/apps/chrome/tab/AuthenticatorHelper;->mOriginalTabUrl:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 152
    iget-object v3, p0, Lcom/google/android/apps/chrome/tab/AuthenticatorHelper;->mWindowAndroid:Lorg/chromium/ui/base/WindowAndroid;

    iget-object v4, p0, Lcom/google/android/apps/chrome/tab/AuthenticatorHelper;->mIntentCallback:Lorg/chromium/ui/base/WindowAndroid$IntentCallback;

    sget v5, Lcom/google/android/apps/chrome/R$string;->failed_to_start_authenticator:I

    invoke-virtual {v3, v2, v4, v5}, Lorg/chromium/ui/base/WindowAndroid;->showIntent(Landroid/content/Intent;Lorg/chromium/ui/base/WindowAndroid$IntentCallback;I)Z

    move-result v2

    if-nez v2, :cond_4

    .line 154
    iput-object v6, p0, Lcom/google/android/apps/chrome/tab/AuthenticatorHelper;->mIntentCallback:Lorg/chromium/ui/base/WindowAndroid$IntentCallback;

    goto :goto_0

    :cond_4
    move v0, v1

    .line 157
    goto :goto_0
.end method

.method setAction(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 201
    iput-object p1, p0, Lcom/google/android/apps/chrome/tab/AuthenticatorHelper;->mAction:Ljava/lang/String;

    .line 202
    return-void
.end method

.method setTestDelegate(Lcom/google/android/apps/chrome/tab/AuthenticatorHelper$TestDelegate;)V
    .locals 0

    .prologue
    .line 206
    iput-object p1, p0, Lcom/google/android/apps/chrome/tab/AuthenticatorHelper;->mTestDelegate:Lcom/google/android/apps/chrome/tab/AuthenticatorHelper$TestDelegate;

    .line 207
    return-void
.end method
