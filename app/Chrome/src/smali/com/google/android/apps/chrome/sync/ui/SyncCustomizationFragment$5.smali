.class Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment$5;
.super Ljava/lang/Object;
.source "SyncCustomizationFragment.java"

# interfaces
.implements Landroid/accounts/AccountManagerCallback;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;

.field final synthetic val$passphrase:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 456
    iput-object p1, p0, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment$5;->this$0:Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;

    iput-object p2, p0, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment$5;->val$passphrase:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run(Landroid/accounts/AccountManagerFuture;)V
    .locals 4

    .prologue
    .line 459
    const/4 v1, 0x0

    .line 461
    :try_start_0
    invoke-interface {p1}, Landroid/accounts/AccountManagerFuture;->getResult()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    .line 462
    const-string/jumbo v2, "booleanResult"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/accounts/OperationCanceledException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/accounts/AuthenticatorException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2

    move-result v0

    .line 473
    :goto_0
    const-string/jumbo v1, "SyncCustomizationFragment"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "GAIA password valid: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 474
    if-eqz v0, :cond_0

    .line 475
    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment$5;->this$0:Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;

    iget-object v1, p0, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment$5;->val$passphrase:Ljava/lang/String;

    const/4 v2, 0x1

    # invokes: Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;->configureEncryption(Ljava/lang/String;Z)V
    invoke-static {v0, v1, v2}, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;->access$900(Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;Ljava/lang/String;Z)V

    .line 479
    :goto_1
    return-void

    .line 463
    :catch_0
    move-exception v0

    .line 466
    const-string/jumbo v2, "SyncCustomizationFragment"

    const-string/jumbo v3, "unable to verify password"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move v0, v1

    .line 471
    goto :goto_0

    .line 467
    :catch_1
    move-exception v0

    .line 468
    const-string/jumbo v2, "SyncCustomizationFragment"

    const-string/jumbo v3, "unable to verify password"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move v0, v1

    .line 471
    goto :goto_0

    .line 469
    :catch_2
    move-exception v0

    .line 470
    const-string/jumbo v2, "SyncCustomizationFragment"

    const-string/jumbo v3, "unable to verify password"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move v0, v1

    goto :goto_0

    .line 477
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment$5;->this$0:Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;

    # invokes: Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;->notifyInvalidPassphrase()V
    invoke-static {v0}, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;->access$1000(Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;)V

    goto :goto_1
.end method
