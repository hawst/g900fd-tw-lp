.class public Lcom/google/android/apps/chrome/tab/ChromeTab;
.super Lorg/chromium/chrome/browser/Tab;
.source "ChromeTab.java"

# interfaces
.implements Landroid/view/View$OnSystemUiVisibilityChangeListener;
.implements Landroid/view/ViewGroup$OnHierarchyChangeListener;


# static fields
.field static final synthetic $assertionsDisabled:Z

.field public static final BLOCKED_BY_ADMINISTRATOR:I = -0x16

.field private static final CONTEXTUAL_SEARCH_NOTIFICATIONS:[I

.field static final KEY_CHECKER:J = 0x0L

.field public static final NTP_TAB_ID:I = -0x2

.field public static final PAGESPEED_PASSTHROUGH_HEADER:Ljava/lang/String; = "X-PSA-Client-Options: v=1,m=1\nCache-Control: no-cache"

.field public static final SAVED_STATE_FILE_PREFIX:Ljava/lang/String; = "tab"

.field public static final SAVED_STATE_FILE_PREFIX_INCOGNITO:Ljava/lang/String; = "cryptonito"

.field static sChannelNameOverrideForTest:Ljava/lang/String;


# instance fields
.field private final mActivity:Lcom/google/android/apps/chrome/ChromeActivity;

.field private mAppAssociatedWith:Ljava/lang/String;

.field private mBackgroundContentViewHelper:Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;

.field private mBaseUrl:Ljava/lang/String;

.field private final mCloseContentsRunnable:Ljava/lang/Runnable;

.field private mContextualSearchGestureListener:Lorg/chromium/content_public/browser/GestureStateListener;

.field private final mContextualSearchNotificationHandler:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;

.field private mCreationTime:J

.field private mDownloadListener:Lcom/google/android/apps/chrome/tab/ChromeDownloadListener;

.field private mFindMatchRectsListener:Lcom/google/android/apps/chrome/tab/ChromeTab$FindMatchRectsListener;

.field private mFindResultListener:Lcom/google/android/apps/chrome/tab/ChromeTab$FindResultListener;

.field private mFrozenContentsState:Lcom/google/android/apps/chrome/tab/ChromeTab$ContentsState;

.field private mFullscreenHungRendererToken:I

.field private mFullscreenManager:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;

.field private mGestureStateListener:Lorg/chromium/content_public/browser/GestureStateListener;

.field private mHandler:Landroid/os/Handler;

.field private mInterceptNavigationDelegate:Lcom/google/android/apps/chrome/tab/ChromeTab$InterceptNavigationDelegateImpl;

.field private mIsBeingRestored:Z

.field private mIsFullscreenWaitingForLoad:Z

.field private mIsHidden:Z

.field private mIsImeShowing:Z

.field private mIsLoading:Z

.field private mIsNativePageCommitPending:Z

.field private mIsShowingErrorPage:Z

.field private mIsTabStateDirty:Z

.field private mIsTitleDirectionRtl:Z

.field private mLastPageLoadHasSpdyProxyPassthroughHeaders:Z

.field private mLaunchType:Lorg/chromium/chrome/browser/tabmodel/TabModel$TabLaunchType;

.field private mLoadProgressAtViewSwapInTime:I

.field private mNativeChromeTab:J

.field private mNeedsReload:Z

.field private mPendingLoadParams:Lorg/chromium/content_public/browser/LoadUrlParams;

.field private mPreviousFullscreenContentOffsetY:F

.field private mPreviousFullscreenOverdrawBottomHeight:F

.field private mPreviousFullscreenTopControlsOffsetY:F

.field private mReaderModeManager:Lcom/google/android/apps/chrome/dom_distiller/ReaderModeManager;

.field private mSadTabView:Landroid/view/View;

.field private mShouldPreserve:Z

.field private mSnapshotDownloadErrorInfoBar:Lorg/chromium/chrome/browser/infobar/MessageInfoBar;

.field private mSnapshotDownloadingInfoBar:Lcom/google/android/apps/chrome/infobar/SnapshotDownloadingInfobar;

.field private mSnapshotHandlingFinished:Z

.field private mSnapshotId:Ljava/lang/String;

.field private final mTabObserver:Lorg/chromium/chrome/browser/TabObserver;

.field private mTabRedirectHandler:Lcom/google/android/apps/chrome/tab/TabRedirectHandler;

.field private final mTabUma:Lcom/google/android/apps/chrome/tab/TabUma;

.field private mThumbnailCapturedForLoad:Z

.field private mThumbnailHeight:I

.field private final mThumbnailRunnable:Ljava/lang/Runnable;

.field private mThumbnailWidth:I

.field private mTimestampMillis:J

.field private mTitle:Ljava/lang/String;

.field private mTransitionPageHelper:Lcom/google/android/apps/chrome/tab/TransitionPageHelper;

.field private mUrl:Ljava/lang/String;

.field private mUsedSpdyProxy:Z

.field private mUsedSpdyProxyWithPassthrough:Z

.field private mWebContentsObserver:Lorg/chromium/content/browser/WebContentsObserverAndroid;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 145
    const-class v0, Lcom/google/android/apps/chrome/tab/ChromeTab;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/tab/ChromeTab;->$assertionsDisabled:Z

    .line 399
    const/4 v0, 0x3

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/google/android/apps/chrome/tab/ChromeTab;->CONTEXTUAL_SEARCH_NOTIFICATIONS:[I

    return-void

    .line 145
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 399
    :array_0
    .array-data 4
        0x4d
        0x4e
        0x2a
    .end array-data
.end method

.method protected constructor <init>(ILcom/google/android/apps/chrome/ChromeActivity;ZLorg/chromium/ui/base/WindowAndroid;ILcom/google/android/apps/chrome/tab/ChromeTab$TabState;Lcom/google/android/apps/chrome/tab/TabUma$TabCreationState;)V
    .locals 8

    .prologue
    .line 605
    sget-object v5, Lorg/chromium/chrome/browser/tabmodel/TabModel$TabLaunchType;->FROM_RESTORE:Lorg/chromium/chrome/browser/tabmodel/TabModel$TabLaunchType;

    move-object v0, p0

    move v1, p1

    move-object v2, p2

    move v3, p3

    move-object v4, p4

    move v6, p5

    move-object v7, p7

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/chrome/tab/ChromeTab;-><init>(ILcom/google/android/apps/chrome/ChromeActivity;ZLorg/chromium/ui/base/WindowAndroid;Lorg/chromium/chrome/browser/tabmodel/TabModel$TabLaunchType;ILcom/google/android/apps/chrome/tab/TabUma$TabCreationState;)V

    .line 607
    iget-object v0, p6, Lcom/google/android/apps/chrome/tab/ChromeTab$TabState;->contentsState:Lcom/google/android/apps/chrome/tab/ChromeTab$ContentsState;

    iput-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mFrozenContentsState:Lcom/google/android/apps/chrome/tab/ChromeTab$ContentsState;

    .line 608
    iget-wide v0, p6, Lcom/google/android/apps/chrome/tab/ChromeTab$TabState;->timestampMillis:J

    iput-wide v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mTimestampMillis:J

    .line 609
    iget-wide v0, p6, Lcom/google/android/apps/chrome/tab/ChromeTab$TabState;->syncId:J

    long-to-int v0, v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->setSyncId(I)V

    .line 610
    iget-boolean v0, p6, Lcom/google/android/apps/chrome/tab/ChromeTab$TabState;->shouldPreserve:Z

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->setShouldPreserve(Z)V

    .line 611
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mFrozenContentsState:Lcom/google/android/apps/chrome/tab/ChromeTab$ContentsState;

    invoke-static {v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getDisplayTitleFromState(Lcom/google/android/apps/chrome/tab/ChromeTab$ContentsState;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mTitle:Ljava/lang/String;

    .line 612
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mTitle:Ljava/lang/String;

    invoke-static {v0}, Lorg/chromium/ui/base/LocalizationUtils;->getFirstStrongCharacterDirection(Ljava/lang/String;)I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mIsTitleDirectionRtl:Z

    .line 614
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mFrozenContentsState:Lcom/google/android/apps/chrome/tab/ChromeTab$ContentsState;

    invoke-static {v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getVirtualUrlFromState(Lcom/google/android/apps/chrome/tab/ChromeTab$ContentsState;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mUrl:Ljava/lang/String;

    .line 615
    return-void

    .line 612
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected constructor <init>(ILcom/google/android/apps/chrome/ChromeActivity;ZLorg/chromium/ui/base/WindowAndroid;Lorg/chromium/chrome/browser/tabmodel/TabModel$TabLaunchType;ILcom/google/android/apps/chrome/tab/TabUma$TabCreationState;)V
    .locals 6

    .prologue
    .line 563
    move-object v0, p0

    move v1, p1

    move v2, p6

    move v3, p3

    move-object v4, p2

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lorg/chromium/chrome/browser/Tab;-><init>(IIZLandroid/content/Context;Lorg/chromium/ui/base/WindowAndroid;)V

    .line 189
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mNativeChromeTab:J

    .line 211
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mIsHidden:Z

    .line 217
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mCreationTime:J

    .line 282
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mIsTabStateDirty:Z

    .line 292
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mTimestampMillis:J

    .line 294
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mIsFullscreenWaitingForLoad:Z

    .line 295
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mIsLoading:Z

    .line 296
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mIsBeingRestored:Z

    .line 356
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mFindMatchRectsListener:Lcom/google/android/apps/chrome/tab/ChromeTab$FindMatchRectsListener;

    .line 373
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mSnapshotDownloadingInfoBar:Lcom/google/android/apps/chrome/infobar/SnapshotDownloadingInfobar;

    .line 408
    new-instance v0, Lcom/google/android/apps/chrome/tab/ChromeTab$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/tab/ChromeTab$1;-><init>(Lcom/google/android/apps/chrome/tab/ChromeTab;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mContextualSearchNotificationHandler:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;

    .line 422
    new-instance v0, Lcom/google/android/apps/chrome/tab/ChromeTab$2;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/tab/ChromeTab$2;-><init>(Lcom/google/android/apps/chrome/tab/ChromeTab;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mThumbnailRunnable:Ljava/lang/Runnable;

    .line 459
    new-instance v0, Lcom/google/android/apps/chrome/tab/ChromeTab$3;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/tab/ChromeTab$3;-><init>(Lcom/google/android/apps/chrome/tab/ChromeTab;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mCloseContentsRunnable:Ljava/lang/Runnable;

    .line 492
    const/high16 v0, 0x7fc00000    # NaNf

    iput v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mPreviousFullscreenTopControlsOffsetY:F

    .line 493
    const/high16 v0, 0x7fc00000    # NaNf

    iput v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mPreviousFullscreenContentOffsetY:F

    .line 494
    const/high16 v0, 0x7fc00000    # NaNf

    iput v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mPreviousFullscreenOverdrawBottomHeight:F

    .line 495
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mFullscreenHungRendererToken:I

    .line 3154
    new-instance v0, Lcom/google/android/apps/chrome/tab/ChromeTab$11;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/tab/ChromeTab$11;-><init>(Lcom/google/android/apps/chrome/tab/ChromeTab;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mTabObserver:Lorg/chromium/chrome/browser/TabObserver;

    .line 564
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mTabObserver:Lorg/chromium/chrome/browser/TabObserver;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->addObserver(Lorg/chromium/chrome/browser/TabObserver;)V

    .line 565
    iput-object p2, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mActivity:Lcom/google/android/apps/chrome/ChromeActivity;

    .line 566
    iput-object p5, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mLaunchType:Lorg/chromium/chrome/browser/tabmodel/TabModel$TabLaunchType;

    .line 567
    new-instance v0, Lcom/google/android/apps/chrome/tab/ChromeTab$4;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/tab/ChromeTab$4;-><init>(Lcom/google/android/apps/chrome/tab/ChromeTab;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mHandler:Landroid/os/Handler;

    .line 576
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->createContentViewClient()Lorg/chromium/content/browser/ContentViewClient;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->setContentViewClient(Lorg/chromium/content/browser/ContentViewClient;)V

    .line 577
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mActivity:Lcom/google/android/apps/chrome/ChromeActivity;

    if-eqz v0, :cond_1

    if-eqz p7, :cond_1

    .line 578
    new-instance v0, Lcom/google/android/apps/chrome/tab/TabUma;

    iget-object v1, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mActivity:Lcom/google/android/apps/chrome/ChromeActivity;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/ChromeActivity;->getTabModelSelector()Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

    move-result-object v1

    invoke-interface {v1, p3}, Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;->getModel(Z)Lorg/chromium/chrome/browser/tabmodel/TabModel;

    move-result-object v1

    invoke-direct {v0, p0, p7, v1}, Lcom/google/android/apps/chrome/tab/TabUma;-><init>(Lcom/google/android/apps/chrome/tab/ChromeTab;Lcom/google/android/apps/chrome/tab/TabUma$TabCreationState;Lorg/chromium/chrome/browser/tabmodel/TabModel;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mTabUma:Lcom/google/android/apps/chrome/tab/TabUma;

    .line 584
    :goto_0
    if-eqz p3, :cond_0

    .line 585
    invoke-static {}, Lorg/chromium/content/browser/crypto/CipherFactory;->getInstance()Lorg/chromium/content/browser/crypto/CipherFactory;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/content/browser/crypto/CipherFactory;->triggerKeyGeneration()V

    .line 588
    :cond_0
    new-instance v0, Lcom/google/android/apps/chrome/dom_distiller/ReaderModeManager;

    invoke-direct {v0, p0, p2}, Lcom/google/android/apps/chrome/dom_distiller/ReaderModeManager;-><init>(Lcom/google/android/apps/chrome/tab/ChromeTab;Lcom/google/android/apps/chrome/ChromeActivity;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mReaderModeManager:Lcom/google/android/apps/chrome/dom_distiller/ReaderModeManager;

    .line 589
    new-instance v1, Lcom/google/android/apps/chrome/tab/TabRedirectHandler;

    if-nez p2, :cond_2

    const/4 v0, 0x0

    :goto_1
    invoke-direct {v1, v0}, Lcom/google/android/apps/chrome/tab/TabRedirectHandler;-><init>(Ljava/lang/String;)V

    iput-object v1, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mTabRedirectHandler:Lcom/google/android/apps/chrome/tab/TabRedirectHandler;

    .line 591
    return-void

    .line 581
    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mTabUma:Lcom/google/android/apps/chrome/tab/TabUma;

    goto :goto_0

    .line 589
    :cond_2
    invoke-virtual {p2}, Lcom/google/android/apps/chrome/ChromeActivity;->getPackageName()Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method private constructor <init>(IZ)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    const/high16 v3, 0x7fc00000    # NaNf

    const/4 v2, 0x0

    .line 547
    invoke-direct {p0, p1, p2, v2, v2}, Lorg/chromium/chrome/browser/Tab;-><init>(IZLandroid/content/Context;Lorg/chromium/ui/base/WindowAndroid;)V

    .line 189
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mNativeChromeTab:J

    .line 211
    iput-boolean v5, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mIsHidden:Z

    .line 217
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mCreationTime:J

    .line 282
    iput-boolean v5, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mIsTabStateDirty:Z

    .line 292
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mTimestampMillis:J

    .line 294
    iput-boolean v4, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mIsFullscreenWaitingForLoad:Z

    .line 295
    iput-boolean v4, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mIsLoading:Z

    .line 296
    iput-boolean v4, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mIsBeingRestored:Z

    .line 356
    iput-object v2, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mFindMatchRectsListener:Lcom/google/android/apps/chrome/tab/ChromeTab$FindMatchRectsListener;

    .line 373
    iput-object v2, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mSnapshotDownloadingInfoBar:Lcom/google/android/apps/chrome/infobar/SnapshotDownloadingInfobar;

    .line 408
    new-instance v0, Lcom/google/android/apps/chrome/tab/ChromeTab$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/tab/ChromeTab$1;-><init>(Lcom/google/android/apps/chrome/tab/ChromeTab;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mContextualSearchNotificationHandler:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;

    .line 422
    new-instance v0, Lcom/google/android/apps/chrome/tab/ChromeTab$2;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/tab/ChromeTab$2;-><init>(Lcom/google/android/apps/chrome/tab/ChromeTab;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mThumbnailRunnable:Ljava/lang/Runnable;

    .line 459
    new-instance v0, Lcom/google/android/apps/chrome/tab/ChromeTab$3;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/tab/ChromeTab$3;-><init>(Lcom/google/android/apps/chrome/tab/ChromeTab;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mCloseContentsRunnable:Ljava/lang/Runnable;

    .line 492
    iput v3, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mPreviousFullscreenTopControlsOffsetY:F

    .line 493
    iput v3, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mPreviousFullscreenContentOffsetY:F

    .line 494
    iput v3, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mPreviousFullscreenOverdrawBottomHeight:F

    .line 495
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mFullscreenHungRendererToken:I

    .line 3154
    new-instance v0, Lcom/google/android/apps/chrome/tab/ChromeTab$11;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/tab/ChromeTab$11;-><init>(Lcom/google/android/apps/chrome/tab/ChromeTab;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mTabObserver:Lorg/chromium/chrome/browser/TabObserver;

    .line 548
    iput-object v2, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mActivity:Lcom/google/android/apps/chrome/ChromeActivity;

    .line 549
    iput-object v2, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mTabUma:Lcom/google/android/apps/chrome/tab/TabUma;

    .line 550
    new-instance v0, Lcom/google/android/apps/chrome/tab/TabRedirectHandler;

    invoke-direct {v0, v2}, Lcom/google/android/apps/chrome/tab/TabRedirectHandler;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mTabRedirectHandler:Lcom/google/android/apps/chrome/tab/TabRedirectHandler;

    .line 551
    return-void
.end method

.method static synthetic access$000(Ljava/nio/ByteBuffer;)V
    .locals 0

    .prologue
    .line 145
    invoke-static {p0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->nativeFreeContentsStateBuffer(Ljava/nio/ByteBuffer;)V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/apps/chrome/tab/ChromeTab;)Z
    .locals 1

    .prologue
    .line 145
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->isContextualSearchActive()Z

    move-result v0

    return v0
.end method

.method static synthetic access$1000(Lcom/google/android/apps/chrome/tab/ChromeTab;Landroid/graphics/Bitmap;)V
    .locals 0

    .prologue
    .line 145
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/tab/ChromeTab;->updateHistoryThumbnail(Landroid/graphics/Bitmap;)V

    return-void
.end method

.method static synthetic access$1100(Lcom/google/android/apps/chrome/tab/ChromeTab;)V
    .locals 0

    .prologue
    .line 145
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->enableFullscreenAfterLoad()V

    return-void
.end method

.method static synthetic access$1200(Lcom/google/android/apps/chrome/tab/ChromeTab;Lorg/chromium/content/browser/ContentViewCore;ZZZ)V
    .locals 0

    .prologue
    .line 145
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/google/android/apps/chrome/tab/ChromeTab;->swapContentViewCore(Lorg/chromium/content/browser/ContentViewCore;ZZZ)V

    return-void
.end method

.method static synthetic access$1300(Lcom/google/android/apps/chrome/tab/ChromeTab;)I
    .locals 1

    .prologue
    .line 145
    iget v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mLoadProgressAtViewSwapInTime:I

    return v0
.end method

.method static synthetic access$1302(Lcom/google/android/apps/chrome/tab/ChromeTab;I)I
    .locals 0

    .prologue
    .line 145
    iput p1, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mLoadProgressAtViewSwapInTime:I

    return p1
.end method

.method static synthetic access$1400(Lcom/google/android/apps/chrome/tab/ChromeTab;)Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;
    .locals 1

    .prologue
    .line 145
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mBackgroundContentViewHelper:Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;

    return-object v0
.end method

.method static synthetic access$1500(Lcom/google/android/apps/chrome/tab/ChromeTab;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 145
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$1600(Lcom/google/android/apps/chrome/tab/ChromeTab;)V
    .locals 0

    .prologue
    .line 145
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->notifyLoadProgress()V

    return-void
.end method

.method static synthetic access$1700(Lcom/google/android/apps/chrome/tab/ChromeTab;Ljava/lang/String;Ljava/lang/String;[BIZZ)V
    .locals 0

    .prologue
    .line 145
    invoke-direct/range {p0 .. p6}, Lcom/google/android/apps/chrome/tab/ChromeTab;->openNewTab(Ljava/lang/String;Ljava/lang/String;[BIZZ)V

    return-void
.end method

.method static synthetic access$1800(Lcom/google/android/apps/chrome/tab/ChromeTab;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 145
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1900(Lcom/google/android/apps/chrome/tab/ChromeTab;I)V
    .locals 0

    .prologue
    .line 145
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/tab/ChromeTab;->notifyPageLoad(I)V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/apps/chrome/tab/ChromeTab;Lorg/chromium/content/browser/ContentViewCore;)V
    .locals 0

    .prologue
    .line 145
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/tab/ChromeTab;->addContextualSearchListener(Lorg/chromium/content/browser/ContentViewCore;)V

    return-void
.end method

.method static synthetic access$2000(Lcom/google/android/apps/chrome/tab/ChromeTab;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 145
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$2100(Lcom/google/android/apps/chrome/tab/ChromeTab;)Z
    .locals 1

    .prologue
    .line 145
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->isCapturingAudio()Z

    move-result v0

    return v0
.end method

.method static synthetic access$2200(Lcom/google/android/apps/chrome/tab/ChromeTab;)Z
    .locals 1

    .prologue
    .line 145
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->isCapturingVideo()Z

    move-result v0

    return v0
.end method

.method static synthetic access$2300(Lcom/google/android/apps/chrome/tab/ChromeTab;)Z
    .locals 1

    .prologue
    .line 145
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mIsLoading:Z

    return v0
.end method

.method static synthetic access$2302(Lcom/google/android/apps/chrome/tab/ChromeTab;Z)Z
    .locals 0

    .prologue
    .line 145
    iput-boolean p1, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mIsLoading:Z

    return p1
.end method

.method static synthetic access$2400(Lcom/google/android/apps/chrome/tab/ChromeTab;)Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;
    .locals 1

    .prologue
    .line 145
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mFullscreenManager:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;

    return-object v0
.end method

.method static synthetic access$2500(Lcom/google/android/apps/chrome/tab/ChromeTab;)I
    .locals 1

    .prologue
    .line 145
    iget v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mFullscreenHungRendererToken:I

    return v0
.end method

.method static synthetic access$2502(Lcom/google/android/apps/chrome/tab/ChromeTab;I)I
    .locals 0

    .prologue
    .line 145
    iput p1, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mFullscreenHungRendererToken:I

    return p1
.end method

.method static synthetic access$2600(Lcom/google/android/apps/chrome/tab/ChromeTab;)Lcom/google/android/apps/chrome/tab/ChromeTab$FindResultListener;
    .locals 1

    .prologue
    .line 145
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mFindResultListener:Lcom/google/android/apps/chrome/tab/ChromeTab$FindResultListener;

    return-object v0
.end method

.method static synthetic access$2700(Lcom/google/android/apps/chrome/tab/ChromeTab;)Lcom/google/android/apps/chrome/tab/ChromeTab$FindMatchRectsListener;
    .locals 1

    .prologue
    .line 145
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mFindMatchRectsListener:Lcom/google/android/apps/chrome/tab/ChromeTab$FindMatchRectsListener;

    return-object v0
.end method

.method static synthetic access$2800(Lcom/google/android/apps/chrome/tab/ChromeTab;)Ljava/lang/Runnable;
    .locals 1

    .prologue
    .line 145
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mCloseContentsRunnable:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$2900(Lcom/google/android/apps/chrome/tab/ChromeTab;)Z
    .locals 1

    .prologue
    .line 145
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mUsedSpdyProxy:Z

    return v0
.end method

.method static synthetic access$300(Lcom/google/android/apps/chrome/tab/ChromeTab;Lorg/chromium/content/browser/ContentViewCore;)V
    .locals 0

    .prologue
    .line 145
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/tab/ChromeTab;->removeContextualSearchListener(Lorg/chromium/content/browser/ContentViewCore;)V

    return-void
.end method

.method static synthetic access$3000(Lcom/google/android/apps/chrome/tab/ChromeTab;)Z
    .locals 1

    .prologue
    .line 145
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mUsedSpdyProxyWithPassthrough:Z

    return v0
.end method

.method static synthetic access$3200(Lcom/google/android/apps/chrome/tab/ChromeTab;Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 145
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/tab/ChromeTab;->updateTitle(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$3300(Lcom/google/android/apps/chrome/tab/ChromeTab;)V
    .locals 0

    .prologue
    .line 145
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->notifyPageTitleChanged()V

    return-void
.end method

.method static synthetic access$3400(Lcom/google/android/apps/chrome/tab/ChromeTab;Z)V
    .locals 0

    .prologue
    .line 145
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/tab/ChromeTab;->notifyContextualActionBarStateChanged(Z)V

    return-void
.end method

.method static synthetic access$3500(Lcom/google/android/apps/chrome/tab/ChromeTab;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 145
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$3602(Lcom/google/android/apps/chrome/tab/ChromeTab;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 145
    iput-object p1, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mAppAssociatedWith:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$3702(Lcom/google/android/apps/chrome/tab/ChromeTab;Z)Z
    .locals 0

    .prologue
    .line 145
    iput-boolean p1, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mIsImeShowing:Z

    return p1
.end method

.method static synthetic access$3800(Lcom/google/android/apps/chrome/tab/ChromeTab;)Z
    .locals 1

    .prologue
    .line 145
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mNeedsReload:Z

    return v0
.end method

.method static synthetic access$3802(Lcom/google/android/apps/chrome/tab/ChromeTab;Z)Z
    .locals 0

    .prologue
    .line 145
    iput-boolean p1, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mNeedsReload:Z

    return p1
.end method

.method static synthetic access$3900(Lcom/google/android/apps/chrome/tab/ChromeTab;)Landroid/view/View;
    .locals 1

    .prologue
    .line 145
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mSadTabView:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$3902(Lcom/google/android/apps/chrome/tab/ChromeTab;Landroid/view/View;)Landroid/view/View;
    .locals 0

    .prologue
    .line 145
    iput-object p1, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mSadTabView:Landroid/view/View;

    return-object p1
.end method

.method static synthetic access$400(Lcom/google/android/apps/chrome/tab/ChromeTab;)Z
    .locals 1

    .prologue
    .line 145
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mThumbnailCapturedForLoad:Z

    return v0
.end method

.method static synthetic access$4000(Lcom/google/android/apps/chrome/tab/ChromeTab;)Lcom/google/android/apps/chrome/tab/TabUma;
    .locals 1

    .prologue
    .line 145
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mTabUma:Lcom/google/android/apps/chrome/tab/TabUma;

    return-object v0
.end method

.method static synthetic access$402(Lcom/google/android/apps/chrome/tab/ChromeTab;Z)Z
    .locals 0

    .prologue
    .line 145
    iput-boolean p1, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mThumbnailCapturedForLoad:Z

    return p1
.end method

.method static synthetic access$4100(Lcom/google/android/apps/chrome/tab/ChromeTab;)V
    .locals 0

    .prologue
    .line 145
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->cancelThumbnailCapture()V

    return-void
.end method

.method static synthetic access$4202(Lcom/google/android/apps/chrome/tab/ChromeTab;Z)Z
    .locals 0

    .prologue
    .line 145
    iput-boolean p1, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mIsBeingRestored:Z

    return p1
.end method

.method static synthetic access$4300(Lcom/google/android/apps/chrome/tab/ChromeTab;)V
    .locals 0

    .prologue
    .line 145
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->handleTabCrash()V

    return-void
.end method

.method static synthetic access$4400(Lcom/google/android/apps/chrome/tab/ChromeTab;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 145
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$4500(Lcom/google/android/apps/chrome/tab/ChromeTab;)V
    .locals 0

    .prologue
    .line 145
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->didFinishPageLoad()V

    return-void
.end method

.method static synthetic access$4600(Lcom/google/android/apps/chrome/tab/ChromeTab;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 145
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$4700(Lcom/google/android/apps/chrome/tab/ChromeTab;)V
    .locals 0

    .prologue
    .line 145
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->cancelEnableFullscreenLoadDelay()V

    return-void
.end method

.method static synthetic access$4800(Lcom/google/android/apps/chrome/tab/ChromeTab;I)V
    .locals 0

    .prologue
    .line 145
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/tab/ChromeTab;->notifyPageLoadFailed(I)V

    return-void
.end method

.method static synthetic access$4900(Lcom/google/android/apps/chrome/tab/ChromeTab;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 145
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/apps/chrome/tab/ChromeTab;)Z
    .locals 1

    .prologue
    .line 145
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->canUpdateHistoryThumbnail()Z

    move-result v0

    return v0
.end method

.method static synthetic access$5000(Lcom/google/android/apps/chrome/tab/ChromeTab;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 145
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$5102(Lcom/google/android/apps/chrome/tab/ChromeTab;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 145
    iput-object p1, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mBaseUrl:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$5200(Lcom/google/android/apps/chrome/tab/ChromeTab;Ljava/lang/String;Z)V
    .locals 0

    .prologue
    .line 145
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/chrome/tab/ChromeTab;->didStartPageLoad(Ljava/lang/String;Z)V

    return-void
.end method

.method static synthetic access$5302(Lcom/google/android/apps/chrome/tab/ChromeTab;Z)Z
    .locals 0

    .prologue
    .line 145
    iput-boolean p1, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mIsNativePageCommitPending:Z

    return p1
.end method

.method static synthetic access$5400(Lcom/google/android/apps/chrome/tab/ChromeTab;Ljava/lang/String;Z)Z
    .locals 1

    .prologue
    .line 145
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/chrome/tab/ChromeTab;->maybeShowNativePage(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method static synthetic access$5502(Lcom/google/android/apps/chrome/tab/ChromeTab;Z)Z
    .locals 0

    .prologue
    .line 145
    iput-boolean p1, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mIsTabStateDirty:Z

    return p1
.end method

.method static synthetic access$5600(Lcom/google/android/apps/chrome/tab/ChromeTab;)V
    .locals 0

    .prologue
    .line 145
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->notifyPageUrlChanged()V

    return-void
.end method

.method static synthetic access$5700(Lcom/google/android/apps/chrome/tab/ChromeTab;)J
    .locals 2

    .prologue
    .line 145
    iget-wide v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mNativeChromeTab:J

    return-wide v0
.end method

.method static synthetic access$5800(Lcom/google/android/apps/chrome/tab/ChromeTab;J)I
    .locals 1

    .prologue
    .line 145
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/chrome/tab/ChromeTab;->nativeGetKnoxCertificateFailure(J)I

    move-result v0

    return v0
.end method

.method static synthetic access$5900(Lcom/google/android/apps/chrome/tab/ChromeTab;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 145
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/android/apps/chrome/tab/ChromeTab;)Z
    .locals 1

    .prologue
    .line 145
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->shouldUpdateThumbnail()Z

    move-result v0

    return v0
.end method

.method static synthetic access$6000(Lcom/google/android/apps/chrome/tab/ChromeTab;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 145
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$6200(Lcom/google/android/apps/chrome/tab/ChromeTab;)V
    .locals 0

    .prologue
    .line 145
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->rescheduleThumbnailCapture()V

    return-void
.end method

.method static synthetic access$6302(Lcom/google/android/apps/chrome/tab/ChromeTab;Lcom/google/android/apps/chrome/infobar/SnapshotDownloadingInfobar;)Lcom/google/android/apps/chrome/infobar/SnapshotDownloadingInfobar;
    .locals 0

    .prologue
    .line 145
    iput-object p1, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mSnapshotDownloadingInfoBar:Lcom/google/android/apps/chrome/infobar/SnapshotDownloadingInfobar;

    return-object p1
.end method

.method static synthetic access$6402(Lcom/google/android/apps/chrome/tab/ChromeTab;Lorg/chromium/chrome/browser/infobar/MessageInfoBar;)Lorg/chromium/chrome/browser/infobar/MessageInfoBar;
    .locals 0

    .prologue
    .line 145
    iput-object p1, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mSnapshotDownloadErrorInfoBar:Lorg/chromium/chrome/browser/infobar/MessageInfoBar;

    return-object p1
.end method

.method static synthetic access$6500(Lcom/google/android/apps/chrome/tab/ChromeTab;)Lcom/google/android/apps/chrome/tab/TabRedirectHandler;
    .locals 1

    .prologue
    .line 145
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mTabRedirectHandler:Lcom/google/android/apps/chrome/tab/TabRedirectHandler;

    return-object v0
.end method

.method static synthetic access$6600(Lcom/google/android/apps/chrome/tab/ChromeTab;)V
    .locals 0

    .prologue
    .line 145
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->notifyTabContentViewChanged()V

    return-void
.end method

.method static synthetic access$6700(Lcom/google/android/apps/chrome/tab/ChromeTab;)Z
    .locals 1

    .prologue
    .line 145
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->updateTitle()Z

    move-result v0

    return v0
.end method

.method static synthetic access$6800(Lcom/google/android/apps/chrome/tab/ChromeTab;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 145
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mTitle:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$6900(Lcom/google/android/apps/chrome/tab/ChromeTab;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 145
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$700(Lcom/google/android/apps/chrome/tab/ChromeTab;)I
    .locals 1

    .prologue
    .line 145
    iget v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mThumbnailWidth:I

    return v0
.end method

.method static synthetic access$800(Lcom/google/android/apps/chrome/tab/ChromeTab;)I
    .locals 1

    .prologue
    .line 145
    iget v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mThumbnailHeight:I

    return v0
.end method

.method static synthetic access$900(Lcom/google/android/apps/chrome/tab/ChromeTab;)Lcom/google/android/apps/chrome/ChromeActivity;
    .locals 1

    .prologue
    .line 145
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mActivity:Lcom/google/android/apps/chrome/ChromeActivity;

    return-object v0
.end method

.method private addContextualSearchListener(Lorg/chromium/content/browser/ContentViewCore;)V
    .locals 1

    .prologue
    .line 2254
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchFieldTrial;->isEnabled(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2255
    invoke-static {}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchUma;->logPreferenceState()V

    .line 2257
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mContextualSearchGestureListener:Lorg/chromium/content_public/browser/GestureStateListener;

    if-nez v0, :cond_1

    if-eqz p1, :cond_1

    invoke-direct {p0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->isContextualSearchActive()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2258
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getContextualSearchManager()Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->getGestureStateListener()Lorg/chromium/content_public/browser/GestureStateListener;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mContextualSearchGestureListener:Lorg/chromium/content_public/browser/GestureStateListener;

    .line 2260
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mContextualSearchGestureListener:Lorg/chromium/content_public/browser/GestureStateListener;

    invoke-virtual {p1, v0}, Lorg/chromium/content/browser/ContentViewCore;->addGestureStateListener(Lorg/chromium/content_public/browser/GestureStateListener;)V

    .line 2262
    :cond_1
    return-void
.end method

.method private addInfoBar(Lorg/chromium/chrome/browser/infobar/InfoBar;)V
    .locals 1

    .prologue
    .line 2715
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getInfoBarContainer()Lorg/chromium/chrome/browser/infobar/InfoBarContainer;

    move-result-object v0

    invoke-virtual {v0, p1}, Lorg/chromium/chrome/browser/infobar/InfoBarContainer;->addInfoBar(Lorg/chromium/chrome/browser/infobar/InfoBar;)V

    .line 2716
    return-void
.end method

.method private canUpdateHistoryThumbnail()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2040
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getUrl()Ljava/lang/String;

    move-result-object v1

    .line 2041
    const-string/jumbo v2, "chrome://"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string/jumbo v2, "chrome-native://"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2045
    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->isReady()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mIsShowingErrorPage:Z

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->isHidden()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->isShowingSadTab()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->isShowingInterstitialPage()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getProgress()I

    move-result v1

    const/16 v2, 0x64

    if-ne v1, v2, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getWidth()I

    move-result v1

    if-lez v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getHeight()I

    move-result v1

    if-lez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method private cancelEnableFullscreenLoadDelay()V
    .locals 2

    .prologue
    .line 1550
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 1551
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mIsFullscreenWaitingForLoad:Z

    .line 1552
    return-void
.end method

.method private cancelThumbnailCapture()V
    .locals 2

    .prologue
    .line 1537
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mThumbnailRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 1538
    return-void
.end method

.method private createContentViewClient()Lorg/chromium/content/browser/ContentViewClient;
    .locals 1

    .prologue
    .line 1175
    new-instance v0, Lcom/google/android/apps/chrome/tab/ChromeTab$6;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/tab/ChromeTab$6;-><init>(Lcom/google/android/apps/chrome/tab/ChromeTab;)V

    return-object v0
.end method

.method public static createFrozenTabFromState(ILcom/google/android/apps/chrome/ChromeActivity;ZLorg/chromium/ui/base/WindowAndroid;ILcom/google/android/apps/chrome/tab/ChromeTab$TabState;)Lcom/google/android/apps/chrome/tab/ChromeTab;
    .locals 8

    .prologue
    .line 650
    new-instance v0, Lcom/google/android/apps/chrome/tab/ChromeTab;

    sget-object v7, Lcom/google/android/apps/chrome/tab/TabUma$TabCreationState;->FROZEN_ON_RESTORE:Lcom/google/android/apps/chrome/tab/TabUma$TabCreationState;

    move v1, p0

    move-object v2, p1

    move v3, p2

    move-object v4, p3

    move v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/chrome/tab/ChromeTab;-><init>(ILcom/google/android/apps/chrome/ChromeActivity;ZLorg/chromium/ui/base/WindowAndroid;ILcom/google/android/apps/chrome/tab/ChromeTab$TabState;Lcom/google/android/apps/chrome/tab/TabUma$TabCreationState;)V

    .line 652
    return-object v0
.end method

.method private createGestureStateListener()Lorg/chromium/content_public/browser/GestureStateListener;
    .locals 1

    .prologue
    .line 1761
    new-instance v0, Lcom/google/android/apps/chrome/tab/ChromeTab$8;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/tab/ChromeTab$8;-><init>(Lcom/google/android/apps/chrome/tab/ChromeTab;)V

    return-object v0
.end method

.method public static createHistoricalTabFromState(Lcom/google/android/apps/chrome/tab/ChromeTab$ContentsState;)V
    .locals 2

    .prologue
    .line 2016
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tab/ChromeTab$ContentsState;->buffer()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tab/ChromeTab$ContentsState;->version()I

    move-result v1

    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/tab/ChromeTab;->nativeCreateHistoricalTabFromState(Ljava/nio/ByteBuffer;I)V

    .line 2017
    return-void
.end method

.method public static createLiveTab(ILcom/google/android/apps/chrome/ChromeActivity;ZLorg/chromium/ui/base/WindowAndroid;Lorg/chromium/chrome/browser/tabmodel/TabModel$TabLaunchType;IZ)Lcom/google/android/apps/chrome/tab/ChromeTab;
    .locals 8

    .prologue
    .line 636
    new-instance v0, Lcom/google/android/apps/chrome/tab/ChromeTab;

    if-eqz p6, :cond_0

    sget-object v7, Lcom/google/android/apps/chrome/tab/TabUma$TabCreationState;->LIVE_IN_BACKGROUND:Lcom/google/android/apps/chrome/tab/TabUma$TabCreationState;

    :goto_0
    move v1, p0

    move-object v2, p1

    move v3, p2

    move-object v4, p3

    move-object v5, p4

    move v6, p5

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/chrome/tab/ChromeTab;-><init>(ILcom/google/android/apps/chrome/ChromeActivity;ZLorg/chromium/ui/base/WindowAndroid;Lorg/chromium/chrome/browser/tabmodel/TabModel$TabLaunchType;ILcom/google/android/apps/chrome/tab/TabUma$TabCreationState;)V

    .line 639
    return-object v0

    .line 636
    :cond_0
    sget-object v7, Lcom/google/android/apps/chrome/tab/TabUma$TabCreationState;->LIVE_IN_FOREGROUND:Lcom/google/android/apps/chrome/tab/TabUma$TabCreationState;

    goto :goto_0
.end method

.method public static createMockTab(IZ)Lcom/google/android/apps/chrome/tab/ChromeTab;
    .locals 1

    .prologue
    .line 626
    new-instance v0, Lcom/google/android/apps/chrome/tab/ChromeTab;

    invoke-direct {v0, p0, p1}, Lcom/google/android/apps/chrome/tab/ChromeTab;-><init>(IZ)V

    return-object v0
.end method

.method public static createTabForLazyLoad(Lcom/google/android/apps/chrome/ChromeActivity;ZLorg/chromium/ui/base/WindowAndroid;Lorg/chromium/chrome/browser/tabmodel/TabModel$TabLaunchType;ILorg/chromium/content_public/browser/LoadUrlParams;)Lcom/google/android/apps/chrome/tab/ChromeTab;
    .locals 8

    .prologue
    .line 664
    new-instance v0, Lcom/google/android/apps/chrome/tab/ChromeTab;

    const/4 v1, -0x1

    sget-object v7, Lcom/google/android/apps/chrome/tab/TabUma$TabCreationState;->FROZEN_FOR_LAZY_LOAD:Lcom/google/android/apps/chrome/tab/TabUma$TabCreationState;

    move-object v2, p0

    move v3, p1

    move-object v4, p2

    move-object v5, p3

    move v6, p4

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/chrome/tab/ChromeTab;-><init>(ILcom/google/android/apps/chrome/ChromeActivity;ZLorg/chromium/ui/base/WindowAndroid;Lorg/chromium/chrome/browser/tabmodel/TabModel$TabLaunchType;ILcom/google/android/apps/chrome/tab/TabUma$TabCreationState;)V

    .line 669
    iput-object p5, v0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mPendingLoadParams:Lorg/chromium/content_public/browser/LoadUrlParams;

    .line 670
    invoke-virtual {p5}, Lorg/chromium/content_public/browser/LoadUrlParams;->getUrl()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mUrl:Ljava/lang/String;

    .line 671
    return-object v0
.end method

.method private createWebContentsObserverAndroid(Lorg/chromium/content_public/browser/WebContents;)Lorg/chromium/content/browser/WebContentsObserverAndroid;
    .locals 1

    .prologue
    .line 1295
    new-instance v0, Lcom/google/android/apps/chrome/tab/ChromeTab$7;

    invoke-direct {v0, p0, p1}, Lcom/google/android/apps/chrome/tab/ChromeTab$7;-><init>(Lcom/google/android/apps/chrome/tab/ChromeTab;Lorg/chromium/content_public/browser/WebContents;)V

    return-object v0
.end method

.method private didFinishPageLoad()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 1506
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mBackgroundContentViewHelper:Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->hasPendingBackgroundPage()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1534
    :goto_0
    return-void

    .line 1507
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mTabUma:Lcom/google/android/apps/chrome/tab/TabUma;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mTabUma:Lcom/google/android/apps/chrome/tab/TabUma;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tab/TabUma;->onLoadFinished()V

    .line 1508
    :cond_1
    iput-boolean v1, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mIsLoading:Z

    .line 1509
    iput-boolean v1, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mIsBeingRestored:Z

    .line 1510
    iput-boolean v4, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mIsTabStateDirty:Z

    .line 1511
    iput v1, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mLoadProgressAtViewSwapInTime:I

    .line 1512
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->updateTitle()Z

    .line 1513
    const/16 v0, 0x9

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->notifyPageLoad(I)V

    .line 1515
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->rescheduleThumbnailCapture()V

    .line 1519
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mIsFullscreenWaitingForLoad:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v4}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mBackgroundContentViewHelper:Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->hasPendingBackgroundPage()Z

    move-result v0

    if-nez v0, :cond_2

    .line 1522
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mHandler:Landroid/os/Handler;

    const-wide/16 v2, 0xbb8

    invoke-virtual {v0, v4, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 1526
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->updateFullscreenEnabledState()V

    .line 1527
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->maybeSetDataReductionProxyUsed()V

    .line 1532
    sget-object v0, Lcom/google/android/apps/chrome/tab/ChromeTab;->CONTEXTUAL_SEARCH_NOTIFICATIONS:[I

    iget-object v1, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mContextualSearchNotificationHandler:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;

    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->registerForNotifications([ILcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;)V

    goto :goto_0
.end method

.method private didStartPageLoad(Ljava/lang/String;Z)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1472
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mBackgroundContentViewHelper:Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->isPageSwappingInProgress()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1473
    iput v2, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mLoadProgressAtViewSwapInTime:I

    .line 1475
    :cond_0
    invoke-static {p1}, Lorg/chromium/components/dom_distiller/core/DomDistillerUrlUtils;->isDistilledPage(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mIsFullscreenWaitingForLoad:Z

    .line 1476
    iput-boolean v1, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mIsLoading:Z

    .line 1477
    iput v2, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mLoadProgressAtViewSwapInTime:I

    .line 1478
    iput-boolean p2, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mIsShowingErrorPage:Z

    .line 1479
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mBaseUrl:Ljava/lang/String;

    .line 1480
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->removeSadTabIfPresent()V

    .line 1481
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getInfoBarContainer()Lorg/chromium/chrome/browser/infobar/InfoBarContainer;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/infobar/InfoBarContainer;->onPageStarted()V

    .line 1482
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->updateTitle()Z

    .line 1484
    iput-boolean v2, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mThumbnailCapturedForLoad:Z

    .line 1485
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->cancelThumbnailCapture()V

    .line 1487
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getContentViewCore()Lorg/chromium/content/browser/ContentViewCore;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1488
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getContentViewCore()Lorg/chromium/content/browser/ContentViewCore;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/content/browser/ContentViewCore;->stopCurrentAccessibilityNotifications()V

    .line 1490
    :cond_1
    const/16 v0, 0x8

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->notifyPageLoad(I)V

    .line 1492
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mFullscreenManager:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;

    if-eqz v0, :cond_2

    .line 1493
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mFullscreenManager:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;

    iget v1, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mFullscreenHungRendererToken:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->hideControlsPersistent(I)V

    .line 1494
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mFullscreenHungRendererToken:I

    .line 1496
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->updateFullscreenEnabledState()V

    .line 1500
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getContentViewCore()Lorg/chromium/content/browser/ContentViewCore;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->addContextualSearchListener(Lorg/chromium/content/browser/ContentViewCore;)V

    .line 1501
    return-void

    :cond_3
    move v0, v2

    .line 1475
    goto :goto_0
.end method

.method private enableFullscreenAfterLoad()V
    .locals 1

    .prologue
    .line 1588
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mIsFullscreenWaitingForLoad:Z

    if-nez v0, :cond_0

    .line 1592
    :goto_0
    return-void

    .line 1590
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mIsFullscreenWaitingForLoad:Z

    .line 1591
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->updateFullscreenEnabledState()V

    goto :goto_0
.end method

.method public static fromTab(Lorg/chromium/chrome/browser/Tab;)Lcom/google/android/apps/chrome/tab/ChromeTab;
    .locals 0

    .prologue
    .line 675
    check-cast p0, Lcom/google/android/apps/chrome/tab/ChromeTab;

    return-object p0
.end method

.method public static getDisplayTitleFromState(Lcom/google/android/apps/chrome/tab/ChromeTab$ContentsState;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 2703
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tab/ChromeTab$ContentsState;->buffer()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tab/ChromeTab$ContentsState;->version()I

    move-result v1

    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/tab/ChromeTab;->nativeGetDisplayTitleFromByteBuffer(Ljava/nio/ByteBuffer;I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getVirtualUrlFromState(Lcom/google/android/apps/chrome/tab/ChromeTab$ContentsState;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 2707
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tab/ChromeTab$ContentsState;->buffer()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tab/ChromeTab$ContentsState;->version()I

    move-result v1

    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/tab/ChromeTab;->nativeGetVirtualUrlFromByteBuffer(Ljava/nio/ByteBuffer;I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private handleTabCrash()V
    .locals 2

    .prologue
    .line 1705
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 1706
    invoke-static {v0}, Lcom/google/android/apps/chrome/crash/MinidumpUploadService;->createFindAndUploadLastCrashIntent(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v1

    .line 1707
    invoke-virtual {v0, v1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 1708
    invoke-static {}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->crashUploadAttempt()V

    .line 1709
    return-void
.end method

.method private initHistoryThumbnailSize(Landroid/app/Activity;)V
    .locals 2

    .prologue
    .line 2103
    invoke-virtual {p1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 2104
    sget v1, Lcom/google/android/apps/chrome/R$dimen;->most_visited_thumbnail_width:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mThumbnailWidth:I

    .line 2105
    sget v1, Lcom/google/android/apps/chrome/R$dimen;->most_visited_thumbnail_height:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mThumbnailHeight:I

    .line 2106
    return-void
.end method

.method private internalInit(Lcom/google/android/apps/chrome/compositor/TabContentManager;)V
    .locals 3

    .prologue
    .line 723
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mBackgroundContentViewHelper:Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;

    if-nez v0, :cond_0

    .line 724
    new-instance v0, Lcom/google/android/apps/chrome/tab/ChromeTab$5;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/tab/ChromeTab$5;-><init>(Lcom/google/android/apps/chrome/tab/ChromeTab;)V

    .line 746
    new-instance v1, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getWindowAndroid()Lorg/chromium/ui/base/WindowAndroid;

    move-result-object v2

    invoke-direct {v1, v2, p0, v0}, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;-><init>(Lorg/chromium/ui/base/WindowAndroid;Lorg/chromium/chrome/browser/Tab;Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper$BackgroundContentViewDelegate;)V

    iput-object v1, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mBackgroundContentViewHelper:Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;

    .line 750
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mTransitionPageHelper:Lcom/google/android/apps/chrome/tab/TransitionPageHelper;

    if-nez v0, :cond_1

    .line 751
    new-instance v0, Lcom/google/android/apps/chrome/tab/TransitionPageHelper;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getWindowAndroid()Lorg/chromium/ui/base/WindowAndroid;

    move-result-object v2

    invoke-direct {v0, v1, v2, p0}, Lcom/google/android/apps/chrome/tab/TransitionPageHelper;-><init>(Landroid/content/Context;Lorg/chromium/ui/base/WindowAndroid;Lorg/chromium/chrome/browser/Tab;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mTransitionPageHelper:Lcom/google/android/apps/chrome/tab/TransitionPageHelper;

    .line 755
    :cond_1
    invoke-super {p0}, Lorg/chromium/chrome/browser/Tab;->initialize()V

    .line 756
    if-eqz p1, :cond_2

    .line 757
    iget-wide v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mNativeChromeTab:J

    invoke-direct {p0, v0, v1, p1}, Lcom/google/android/apps/chrome/tab/ChromeTab;->nativeAttachToTabContentManager(JLcom/google/android/apps/chrome/compositor/TabContentManager;)V

    .line 759
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mActivity:Lcom/google/android/apps/chrome/ChromeActivity;

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->initHistoryThumbnailSize(Landroid/app/Activity;)V

    .line 760
    return-void
.end method

.method private isCapturingAudio()Z
    .locals 2

    .prologue
    .line 3226
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->isClosing()Z

    move-result v0

    if-nez v0, :cond_0

    iget-wide v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mNativeChromeTab:J

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/chrome/tab/ChromeTab;->nativeIsCapturingAudio(J)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isCapturingVideo()Z
    .locals 2

    .prologue
    .line 3233
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->isClosing()Z

    move-result v0

    if-nez v0, :cond_0

    iget-wide v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mNativeChromeTab:J

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/chrome/tab/ChromeTab;->nativeIsCapturingVideo(J)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isContextualSearchActive()Z
    .locals 1

    .prologue
    .line 2239
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->isIncognito()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getContextualSearchManager()Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->getInstance()Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->isContextualSearchDisabled()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lorg/chromium/chrome/browser/search_engines/TemplateUrlService;->getInstance()Lorg/chromium/chrome/browser/search_engines/TemplateUrlService;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/search_engines/TemplateUrlService;->isDefaultSearchEngineGoogle()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getContextualSearchManager()Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->isRunningInCompatibilityMode()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static isStableChannelBuild()Z
    .locals 2

    .prologue
    .line 2600
    const-string/jumbo v0, "stable"

    sget-object v1, Lcom/google/android/apps/chrome/tab/ChromeTab;->sChannelNameOverrideForTest:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2601
    const/4 v0, 0x1

    .line 2603
    :goto_0
    return v0

    :cond_0
    invoke-static {}, Lcom/google/android/apps/chrome/utilities/ChromeBuildInfo;->isStableBuild()Z

    move-result v0

    goto :goto_0
.end method

.method private maybeSetDataReductionProxyUsed()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 774
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getUrl()Ljava/lang/String;

    move-result-object v0

    .line 775
    if-eqz v0, :cond_1

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v0, v1}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "chrome://"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 787
    :cond_0
    :goto_0
    return-void

    .line 778
    :cond_1
    iput-boolean v3, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mUsedSpdyProxy:Z

    .line 779
    iput-boolean v3, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mUsedSpdyProxyWithPassthrough:Z

    .line 780
    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->isSpdyProxyEnabledForUrl(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 781
    iput-boolean v4, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mUsedSpdyProxy:Z

    .line 782
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mLastPageLoadHasSpdyProxyPassthroughHeaders:Z

    if-eqz v0, :cond_0

    .line 783
    iput-boolean v3, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mLastPageLoadHasSpdyProxyPassthroughHeaders:Z

    .line 784
    iput-boolean v4, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mUsedSpdyProxyWithPassthrough:Z

    goto :goto_0
.end method

.method private maybeShowNativePage(Ljava/lang/String;Z)Z
    .locals 6

    .prologue
    .line 3141
    if-eqz p2, :cond_0

    const/4 v2, 0x0

    .line 3142
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getNativePageFactory()Lcom/google/android/apps/chrome/tab/NativePageFactory;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mActivity:Lcom/google/android/apps/chrome/ChromeActivity;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/ChromeActivity;->getTabModelSelector()Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mActivity:Lcom/google/android/apps/chrome/ChromeActivity;

    move-object v1, p1

    move-object v3, p0

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/apps/chrome/tab/NativePageFactory;->createNativePageForURL(Ljava/lang/String;Lorg/chromium/chrome/browser/NativePage;Lcom/google/android/apps/chrome/tab/ChromeTab;Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;Landroid/app/Activity;)Lorg/chromium/chrome/browser/NativePage;

    move-result-object v0

    .line 3144
    if-eqz v0, :cond_1

    .line 3145
    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->showNativePage(Lorg/chromium/chrome/browser/NativePage;)V

    .line 3146
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->notifyPageTitleChanged()V

    .line 3147
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->onFaviconUpdated()V

    .line 3148
    const/4 v0, 0x1

    .line 3150
    :goto_1
    return v0

    .line 3141
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getNativePage()Lorg/chromium/chrome/browser/NativePage;

    move-result-object v2

    goto :goto_0

    .line 3150
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private native nativeActivateNearestFindResult(JFF)V
.end method

.method private native nativeAttachToTabContentManager(JLcom/google/android/apps/chrome/compositor/TabContentManager;)V
.end method

.method private native nativeCreateHistoricalTab(J)V
.end method

.method private static native nativeCreateHistoricalTabFromState(Ljava/nio/ByteBuffer;I)V
.end method

.method private static native nativeCreateSingleNavigationStateAsByteBuffer(Ljava/lang/String;Ljava/lang/String;IZ)Ljava/nio/ByteBuffer;
.end method

.method private native nativeDangerousDownloadValidated(JIZ)V
.end method

.method private static native nativeFreeContentsStateBuffer(Ljava/nio/ByteBuffer;)V
.end method

.method private native nativeGetBookmarkId(JZ)J
.end method

.method private native nativeGetContentsStateAsByteBuffer(J)Ljava/nio/ByteBuffer;
.end method

.method private static native nativeGetDisplayTitleFromByteBuffer(Ljava/nio/ByteBuffer;I)Ljava/lang/String;
.end method

.method private native nativeGetDownloadWarningText(JLjava/lang/String;)Ljava/lang/String;
.end method

.method private native nativeGetKnoxCertificateFailure(J)I
.end method

.method private native nativeGetPreviousFindText(J)Ljava/lang/String;
.end method

.method private static native nativeGetVirtualUrlFromByteBuffer(Ljava/nio/ByteBuffer;I)Ljava/lang/String;
.end method

.method private native nativeHasPrerenderedUrl(JLjava/lang/String;)Z
.end method

.method private native nativeInit()J
.end method

.method private native nativeIsCapturingAudio(J)Z
.end method

.method private native nativeIsCapturingVideo(J)Z
.end method

.method private native nativeIsDownloadDangerous(JLjava/lang/String;)Z
.end method

.method private native nativeIsInitialNavigation(J)Z
.end method

.method private native nativeRequestFindMatchRects(JI)V
.end method

.method private static native nativeRestoreContentsFromByteBuffer(Ljava/nio/ByteBuffer;IZ)J
.end method

.method private native nativeSearchByImageInNewTabAsync(J)V
.end method

.method private native nativeSetInterceptNavigationDelegate(JLorg/chromium/components/navigation_interception/InterceptNavigationDelegate;)V
.end method

.method private native nativeSetWindowSessionId(JI)V
.end method

.method private native nativeShouldUpdateThumbnail(JLjava/lang/String;)Z
.end method

.method private native nativeStartFinding(JLjava/lang/String;ZZ)V
.end method

.method private native nativeStopFinding(J)V
.end method

.method private native nativeUpdateThumbnail(JLandroid/graphics/Bitmap;Z)V
.end method

.method private native nativeUpdateTopControlsState(JIIZ)V
.end method

.method private notifyContextualActionBarStateChanged(Z)V
    .locals 3

    .prologue
    .line 2459
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 2460
    const-string/jumbo v1, "tabId"

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getId()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 2461
    const-string/jumbo v1, "shown"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2462
    iget-object v1, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mActivity:Lcom/google/android/apps/chrome/ChromeActivity;

    const/16 v2, 0x22

    invoke-static {v1, v2, v0}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->broadcastNotification(Landroid/content/Context;ILandroid/os/Bundle;)V

    .line 2464
    return-void
.end method

.method private notifyLoadProgress()V
    .locals 3

    .prologue
    .line 2430
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 2431
    const-string/jumbo v1, "tabId"

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getId()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 2432
    const-string/jumbo v1, "progress"

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getProgress()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 2433
    iget-object v1, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mActivity:Lcom/google/android/apps/chrome/ChromeActivity;

    const/16 v2, 0xb

    invoke-static {v1, v2, v0}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->broadcastNotification(Landroid/content/Context;ILandroid/os/Bundle;)V

    .line 2434
    return-void
.end method

.method private notifyPageLoad(I)V
    .locals 3

    .prologue
    .line 2409
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 2410
    const-string/jumbo v1, "tabId"

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getId()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 2415
    const-string/jumbo v1, "url"

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getUrl()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2416
    const-string/jumbo v1, "incognito"

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->isIncognito()Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2417
    iget-object v1, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mActivity:Lcom/google/android/apps/chrome/ChromeActivity;

    invoke-static {v1, p1, v0}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->broadcastNotification(Landroid/content/Context;ILandroid/os/Bundle;)V

    .line 2418
    return-void
.end method

.method private notifyPageLoadFailed(I)V
    .locals 3

    .prologue
    .line 2421
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 2422
    const-string/jumbo v1, "tabId"

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getId()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 2423
    const-string/jumbo v1, "errorCode"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 2424
    const-string/jumbo v1, "incognito"

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->isIncognito()Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2425
    iget-object v1, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mActivity:Lcom/google/android/apps/chrome/ChromeActivity;

    const/16 v2, 0x1c

    invoke-static {v1, v2, v0}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->broadcastNotification(Landroid/content/Context;ILandroid/os/Bundle;)V

    .line 2427
    return-void
.end method

.method private notifyPageTitleChanged()V
    .locals 2

    .prologue
    .line 2445
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getTabObservers()Lorg/chromium/base/ObserverList$RewindableIterator;

    move-result-object v1

    .line 2446
    :goto_0
    invoke-interface {v1}, Lorg/chromium/base/ObserverList$RewindableIterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2447
    invoke-interface {v1}, Lorg/chromium/base/ObserverList$RewindableIterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/chromium/chrome/browser/TabObserver;

    invoke-interface {v0, p0}, Lorg/chromium/chrome/browser/TabObserver;->onTitleUpdated(Lorg/chromium/chrome/browser/Tab;)V

    goto :goto_0

    .line 2449
    :cond_0
    return-void
.end method

.method private notifyPageUrlChanged()V
    .locals 2

    .prologue
    .line 2452
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getTabObservers()Lorg/chromium/base/ObserverList$RewindableIterator;

    move-result-object v1

    .line 2453
    :goto_0
    invoke-interface {v1}, Lorg/chromium/base/ObserverList$RewindableIterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2454
    invoke-interface {v1}, Lorg/chromium/base/ObserverList$RewindableIterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/chromium/chrome/browser/TabObserver;

    invoke-interface {v0, p0}, Lorg/chromium/chrome/browser/TabObserver;->onUrlUpdated(Lorg/chromium/chrome/browser/Tab;)V

    goto :goto_0

    .line 2456
    :cond_0
    return-void
.end method

.method private notifyTabContentViewChanged()V
    .locals 3

    .prologue
    .line 2402
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 2403
    const-string/jumbo v1, "tabId"

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getId()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 2404
    iget-object v1, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mActivity:Lcom/google/android/apps/chrome/ChromeActivity;

    const/16 v2, 0x16

    invoke-static {v1, v2, v0}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->broadcastNotification(Landroid/content/Context;ILandroid/os/Bundle;)V

    .line 2406
    return-void
.end method

.method private openNewTab(Ljava/lang/String;Ljava/lang/String;[BIZZ)V
    .locals 5

    .prologue
    .line 798
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->isClosing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 832
    :cond_0
    :goto_0
    return-void

    .line 800
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->isIncognito()Z

    move-result v1

    .line 801
    sget-object v0, Lorg/chromium/chrome/browser/tabmodel/TabModel$TabLaunchType;->FROM_LONGPRESS_FOREGROUND:Lorg/chromium/chrome/browser/tabmodel/TabModel$TabLaunchType;

    .line 802
    if-eqz p5, :cond_2

    move-object v2, p0

    .line 804
    :goto_1
    packed-switch p4, :pswitch_data_0

    .line 817
    :pswitch_0
    sget-boolean v3, Lcom/google/android/apps/chrome/tab/ChromeTab;->$assertionsDisabled:Z

    if-nez v3, :cond_3

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 802
    :cond_2
    const/4 v2, 0x0

    goto :goto_1

    .line 810
    :pswitch_1
    sget-object v0, Lorg/chromium/chrome/browser/tabmodel/TabModel$TabLaunchType;->FROM_LONGPRESS_BACKGROUND:Lorg/chromium/chrome/browser/tabmodel/TabModel$TabLaunchType;

    .line 822
    :cond_3
    :goto_2
    :pswitch_2
    iget-object v3, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mInterceptNavigationDelegate:Lcom/google/android/apps/chrome/tab/ChromeTab$InterceptNavigationDelegateImpl;

    invoke-virtual {v3, p1, v1}, Lcom/google/android/apps/chrome/tab/ChromeTab$InterceptNavigationDelegateImpl;->shouldIgnoreNewTab(Ljava/lang/String;Z)Z

    move-result v3

    if-nez v3, :cond_0

    .line 826
    new-instance v3, Lorg/chromium/content_public/browser/LoadUrlParams;

    invoke-direct {v3, p1}, Lorg/chromium/content_public/browser/LoadUrlParams;-><init>(Ljava/lang/String;)V

    .line 827
    invoke-virtual {v3, p2}, Lorg/chromium/content_public/browser/LoadUrlParams;->setVerbatimHeaders(Ljava/lang/String;)V

    .line 828
    invoke-virtual {v3, p3}, Lorg/chromium/content_public/browser/LoadUrlParams;->setPostData([B)V

    .line 829
    invoke-virtual {v3, p6}, Lorg/chromium/content_public/browser/LoadUrlParams;->setIsRendererInitiated(Z)V

    .line 830
    iget-object v4, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mActivity:Lcom/google/android/apps/chrome/ChromeActivity;

    invoke-virtual {v4}, Lcom/google/android/apps/chrome/ChromeActivity;->getTabModelSelector()Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

    move-result-object v4

    invoke-interface {v4, v3, v0, v2, v1}, Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;->openNewTab(Lorg/chromium/content_public/browser/LoadUrlParams;Lorg/chromium/chrome/browser/tabmodel/TabModel$TabLaunchType;Lorg/chromium/chrome/browser/Tab;Z)Lorg/chromium/chrome/browser/Tab;

    goto :goto_0

    .line 813
    :pswitch_3
    sget-boolean v3, Lcom/google/android/apps/chrome/tab/ChromeTab;->$assertionsDisabled:Z

    if-nez v3, :cond_4

    if-nez v1, :cond_4

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 814
    :cond_4
    const/4 v1, 0x1

    .line 815
    goto :goto_2

    .line 804
    nop

    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_2
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method public static readState(Ljava/io/FileInputStream;Z)Lcom/google/android/apps/chrome/tab/ChromeTab$TabState;
    .locals 1

    .prologue
    .line 2586
    invoke-static {p0, p1}, Lcom/google/android/apps/chrome/tab/ChromeTab;->readStateInternal(Ljava/io/FileInputStream;Z)Lcom/google/android/apps/chrome/tab/ChromeTab$TabState;

    move-result-object v0

    return-object v0
.end method

.method private static readStateInternal(Ljava/io/FileInputStream;Z)Lcom/google/android/apps/chrome/tab/ChromeTab$TabState;
    .locals 11

    .prologue
    const-wide/16 v4, 0x0

    const/4 v7, 0x0

    const/4 v0, 0x0

    .line 2608
    .line 2609
    if-eqz p1, :cond_6

    .line 2610
    invoke-static {}, Lorg/chromium/content/browser/crypto/CipherFactory;->getInstance()Lorg/chromium/content/browser/crypto/CipherFactory;

    move-result-object v1

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Lorg/chromium/content/browser/crypto/CipherFactory;->getCipher(I)Ljavax/crypto/Cipher;

    move-result-object v2

    .line 2611
    if-eqz v2, :cond_6

    .line 2612
    new-instance v1, Ljava/io/DataInputStream;

    new-instance v3, Ljavax/crypto/CipherInputStream;

    invoke-direct {v3, p0, v2}, Ljavax/crypto/CipherInputStream;-><init>(Ljava/io/InputStream;Ljavax/crypto/Cipher;)V

    invoke-direct {v1, v3}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    .line 2615
    :goto_0
    if-nez v1, :cond_5

    .line 2616
    new-instance v1, Ljava/io/DataInputStream;

    invoke-direct {v1, p0}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    move-object v8, v1

    .line 2619
    :goto_1
    if-eqz p1, :cond_0

    :try_start_0
    invoke-virtual {v8}, Ljava/io/DataInputStream;->readLong()J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v2

    cmp-long v1, v2, v4

    if-eqz v1, :cond_0

    .line 2621
    invoke-virtual {v8}, Ljava/io/DataInputStream;->close()V

    .line 2684
    :goto_2
    return-object v0

    .line 2623
    :cond_0
    :try_start_1
    new-instance v6, Lcom/google/android/apps/chrome/tab/ChromeTab$TabState;

    invoke-direct {v6}, Lcom/google/android/apps/chrome/tab/ChromeTab$TabState;-><init>()V

    .line 2624
    invoke-virtual {v8}, Ljava/io/DataInputStream;->readLong()J

    move-result-wide v0

    iput-wide v0, v6, Lcom/google/android/apps/chrome/tab/ChromeTab$TabState;->timestampMillis:J

    .line 2625
    invoke-virtual {v8}, Ljava/io/DataInputStream;->readInt()I

    move-result v9

    .line 2626
    if-eqz p1, :cond_3

    .line 2628
    new-array v0, v9, [B

    .line 2629
    invoke-virtual {v8, v0}, Ljava/io/DataInputStream;->readFully([B)V

    .line 2630
    new-instance v1, Lcom/google/android/apps/chrome/tab/ChromeTab$ContentsState;

    invoke-static {v9}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/google/android/apps/chrome/tab/ChromeTab$ContentsState;-><init>(Ljava/nio/ByteBuffer;)V

    iput-object v1, v6, Lcom/google/android/apps/chrome/tab/ChromeTab$TabState;->contentsState:Lcom/google/android/apps/chrome/tab/ChromeTab$ContentsState;

    .line 2631
    iget-object v1, v6, Lcom/google/android/apps/chrome/tab/ChromeTab$TabState;->contentsState:Lcom/google/android/apps/chrome/tab/ChromeTab$ContentsState;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/tab/ChromeTab$ContentsState;->buffer()Ljava/nio/ByteBuffer;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 2644
    :cond_1
    :goto_3
    invoke-virtual {v8}, Ljava/io/DataInputStream;->readInt()I

    move-result v0

    iput v0, v6, Lcom/google/android/apps/chrome/tab/ChromeTab$TabState;->parentId:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2646
    :try_start_2
    invoke-virtual {v8}, Ljava/io/DataInputStream;->readUTF()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v6, Lcom/google/android/apps/chrome/tab/ChromeTab$TabState;->openerAppId:Ljava/lang/String;

    .line 2647
    const-string/jumbo v0, ""

    iget-object v1, v6, Lcom/google/android/apps/chrome/tab/ChromeTab$TabState;->openerAppId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2648
    const/4 v0, 0x0

    iput-object v0, v6, Lcom/google/android/apps/chrome/tab/ChromeTab$TabState;->openerAppId:Ljava/lang/String;
    :try_end_2
    .catch Ljava/io/EOFException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 2654
    :cond_2
    :goto_4
    :try_start_3
    iget-object v0, v6, Lcom/google/android/apps/chrome/tab/ChromeTab$TabState;->contentsState:Lcom/google/android/apps/chrome/tab/ChromeTab$ContentsState;

    invoke-virtual {v8}, Ljava/io/DataInputStream;->readInt()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/tab/ChromeTab$ContentsState;->setVersion(I)V
    :try_end_3
    .catch Ljava/io/EOFException; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2666
    :goto_5
    :try_start_4
    invoke-virtual {v8}, Ljava/io/DataInputStream;->readLong()J

    move-result-wide v0

    iput-wide v0, v6, Lcom/google/android/apps/chrome/tab/ChromeTab$TabState;->syncId:J
    :try_end_4
    .catch Ljava/io/EOFException; {:try_start_4 .. :try_end_4} :catch_2
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 2674
    :goto_6
    :try_start_5
    invoke-virtual {v8}, Ljava/io/DataInputStream;->readBoolean()Z

    move-result v0

    iput-boolean v0, v6, Lcom/google/android/apps/chrome/tab/ChromeTab$TabState;->shouldPreserve:Z
    :try_end_5
    .catch Ljava/io/EOFException; {:try_start_5 .. :try_end_5} :catch_3
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 2681
    :goto_7
    :try_start_6
    iput-boolean p1, v6, Lcom/google/android/apps/chrome/tab/ChromeTab$TabState;->isIncognito:Z
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 2684
    invoke-virtual {v8}, Ljava/io/DataInputStream;->close()V

    move-object v0, v6

    goto :goto_2

    .line 2634
    :cond_3
    :try_start_7
    invoke-virtual {p0}, Ljava/io/FileInputStream;->getChannel()Ljava/nio/channels/FileChannel;

    move-result-object v0

    .line 2635
    new-instance v10, Lcom/google/android/apps/chrome/tab/ChromeTab$ContentsState;

    sget-object v1, Ljava/nio/channels/FileChannel$MapMode;->READ_ONLY:Ljava/nio/channels/FileChannel$MapMode;

    invoke-virtual {v0}, Ljava/nio/channels/FileChannel;->position()J

    move-result-wide v2

    int-to-long v4, v9

    invoke-virtual/range {v0 .. v5}, Ljava/nio/channels/FileChannel;->map(Ljava/nio/channels/FileChannel$MapMode;JJ)Ljava/nio/MappedByteBuffer;

    move-result-object v0

    invoke-direct {v10, v0}, Lcom/google/android/apps/chrome/tab/ChromeTab$ContentsState;-><init>(Ljava/nio/ByteBuffer;)V

    iput-object v10, v6, Lcom/google/android/apps/chrome/tab/ChromeTab$TabState;->contentsState:Lcom/google/android/apps/chrome/tab/ChromeTab$ContentsState;

    .line 2638
    int-to-long v0, v9

    invoke-virtual {p0, v0, v1}, Ljava/io/FileInputStream;->skip(J)J

    move-result-wide v0

    .line 2639
    int-to-long v2, v9

    cmp-long v2, v0, v2

    if-eqz v2, :cond_1

    .line 2640
    const-string/jumbo v2, "ChromeTab"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "Only skipped "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " bytes when "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " should\'ve been skipped. Tab restore may fail."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto :goto_3

    .line 2684
    :catchall_0
    move-exception v0

    invoke-virtual {v8}, Ljava/io/DataInputStream;->close()V

    throw v0

    .line 2651
    :catch_0
    move-exception v0

    :try_start_8
    const-string/jumbo v0, "ChromeTab"

    const-string/jumbo v1, "Failed to read opener app id state from tab state"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_4

    .line 2658
    :catch_1
    move-exception v0

    iget-object v1, v6, Lcom/google/android/apps/chrome/tab/ChromeTab$TabState;->contentsState:Lcom/google/android/apps/chrome/tab/ChromeTab$ContentsState;

    invoke-static {}, Lcom/google/android/apps/chrome/tab/ChromeTab;->isStableChannelBuild()Z

    move-result v0

    if-eqz v0, :cond_4

    move v0, v7

    :goto_8
    invoke-virtual {v1, v0}, Lcom/google/android/apps/chrome/tab/ChromeTab$ContentsState;->setVersion(I)V

    .line 2662
    const-string/jumbo v0, "ChromeTab"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "Failed to read saved state version id from tab state. Assuming version "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, v6, Lcom/google/android/apps/chrome/tab/ChromeTab$TabState;->contentsState:Lcom/google/android/apps/chrome/tab/ChromeTab$ContentsState;

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/tab/ChromeTab$ContentsState;->version()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_5

    .line 2658
    :cond_4
    const/4 v0, 0x1

    goto :goto_8

    .line 2668
    :catch_2
    move-exception v0

    const-wide/16 v0, 0x0

    iput-wide v0, v6, Lcom/google/android/apps/chrome/tab/ChromeTab$TabState;->syncId:J

    .line 2670
    const-string/jumbo v0, "ChromeTab"

    const-string/jumbo v1, "Failed to read syncId from tab state. Assuming syncId is: 0"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_6

    .line 2677
    :catch_3
    move-exception v0

    const/4 v0, 0x0

    iput-boolean v0, v6, Lcom/google/android/apps/chrome/tab/ChromeTab$TabState;->shouldPreserve:Z

    .line 2678
    const-string/jumbo v0, "ChromeTab"

    const-string/jumbo v1, "Failed to read shouldPreserve flag from tab state. Assuming shouldPreserve is false"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    goto/16 :goto_7

    :cond_5
    move-object v8, v1

    goto/16 :goto_1

    :cond_6
    move-object v1, v0

    goto/16 :goto_0
.end method

.method private removeContextualSearchListener(Lorg/chromium/content/browser/ContentViewCore;)V
    .locals 1

    .prologue
    .line 2269
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchFieldTrial;->isEnabled(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2270
    invoke-static {}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchUma;->logPreferenceState()V

    .line 2272
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mContextualSearchGestureListener:Lorg/chromium/content_public/browser/GestureStateListener;

    if-eqz v0, :cond_1

    if-eqz p1, :cond_1

    .line 2273
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mContextualSearchGestureListener:Lorg/chromium/content_public/browser/GestureStateListener;

    invoke-virtual {p1, v0}, Lorg/chromium/content/browser/ContentViewCore;->removeGestureStateListener(Lorg/chromium/content_public/browser/GestureStateListener;)V

    .line 2274
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mContextualSearchGestureListener:Lorg/chromium/content_public/browser/GestureStateListener;

    .line 2276
    :cond_1
    return-void
.end method

.method private removeSadTabIfPresent()V
    .locals 2

    .prologue
    .line 3031
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->isShowingSadTab()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getContentViewCore()Lorg/chromium/content/browser/ContentViewCore;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/content/browser/ContentViewCore;->getContainerView()Landroid/view/ViewGroup;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mSadTabView:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 3032
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mSadTabView:Landroid/view/View;

    .line 3033
    return-void
.end method

.method private rescheduleThumbnailCapture()V
    .locals 4

    .prologue
    .line 1541
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mThumbnailCapturedForLoad:Z

    if-eqz v0, :cond_1

    .line 1547
    :cond_0
    :goto_0
    return-void

    .line 1542
    :cond_1
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->cancelThumbnailCapture()V

    .line 1545
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getContentViewCore()Lorg/chromium/content/browser/ContentViewCore;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getContentViewCore()Lorg/chromium/content/browser/ContentViewCore;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/content/browser/ContentViewCore;->isScrollInProgress()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1546
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mThumbnailRunnable:Ljava/lang/Runnable;

    const-wide/16 v2, 0x15e

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method

.method private restoreIfNeeded()V
    .locals 1

    .prologue
    .line 2184
    invoke-static {}, Lorg/chromium/base/TraceEvent;->begin()V

    .line 2185
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->isFrozen()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mFrozenContentsState:Lcom/google/android/apps/chrome/tab/ChromeTab$ContentsState;

    if-eqz v0, :cond_1

    .line 2188
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->unfreezeContents()Z

    .line 2200
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->loadIfNecessary()V

    .line 2201
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mIsBeingRestored:Z

    .line 2202
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mTabUma:Lcom/google/android/apps/chrome/tab/TabUma;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mTabUma:Lcom/google/android/apps/chrome/tab/TabUma;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tab/TabUma;->onRestoreStarted()V

    .line 2203
    :cond_0
    invoke-static {}, Lorg/chromium/base/TraceEvent;->end()V

    .line 2204
    :goto_1
    return-void

    .line 2189
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mNeedsReload:Z

    if-eqz v0, :cond_2

    .line 2192
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mNeedsReload:Z

    .line 2193
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->requestRestoreLoad()V

    goto :goto_0

    .line 2196
    :cond_2
    invoke-static {}, Lorg/chromium/base/TraceEvent;->end()V

    goto :goto_1
.end method

.method public static saveState(Ljava/io/FileOutputStream;Lcom/google/android/apps/chrome/tab/ChromeTab$TabState;Z)V
    .locals 1

    .prologue
    .line 2537
    if-eqz p1, :cond_0

    iget-object v0, p1, Lcom/google/android/apps/chrome/tab/ChromeTab$TabState;->contentsState:Lcom/google/android/apps/chrome/tab/ChromeTab$ContentsState;

    if-nez v0, :cond_1

    .line 2541
    :cond_0
    :goto_0
    return-void

    .line 2540
    :cond_1
    invoke-static {p0, p1, p2}, Lcom/google/android/apps/chrome/tab/ChromeTab;->saveStateInternal(Ljava/io/FileOutputStream;Lcom/google/android/apps/chrome/tab/ChromeTab$TabState;Z)V

    goto :goto_0
.end method

.method private static saveStateInternal(Ljava/io/FileOutputStream;Lcom/google/android/apps/chrome/tab/ChromeTab$TabState;Z)V
    .locals 4

    .prologue
    .line 2546
    if-eqz p2, :cond_2

    .line 2547
    invoke-static {}, Lorg/chromium/content/browser/crypto/CipherFactory;->getInstance()Lorg/chromium/content/browser/crypto/CipherFactory;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lorg/chromium/content/browser/crypto/CipherFactory;->getCipher(I)Ljavax/crypto/Cipher;

    move-result-object v1

    .line 2548
    if-eqz v1, :cond_1

    .line 2549
    new-instance v0, Ljava/io/DataOutputStream;

    new-instance v2, Ljavax/crypto/CipherOutputStream;

    invoke-direct {v2, p0, v1}, Ljavax/crypto/CipherOutputStream;-><init>(Ljava/io/OutputStream;Ljavax/crypto/Cipher;)V

    invoke-direct {v0, v2}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    move-object v1, v0

    .line 2562
    :goto_0
    if-eqz p2, :cond_0

    .line 2563
    const-wide/16 v2, 0x0

    :try_start_0
    invoke-virtual {v1, v2, v3}, Ljava/io/DataOutputStream;->writeLong(J)V

    .line 2565
    :cond_0
    iget-wide v2, p1, Lcom/google/android/apps/chrome/tab/ChromeTab$TabState;->timestampMillis:J

    invoke-virtual {v1, v2, v3}, Ljava/io/DataOutputStream;->writeLong(J)V

    .line 2566
    iget-object v0, p1, Lcom/google/android/apps/chrome/tab/ChromeTab$TabState;->contentsState:Lcom/google/android/apps/chrome/tab/ChromeTab$ContentsState;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tab/ChromeTab$ContentsState;->buffer()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    .line 2567
    iget-object v0, p1, Lcom/google/android/apps/chrome/tab/ChromeTab$TabState;->contentsState:Lcom/google/android/apps/chrome/tab/ChromeTab$ContentsState;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tab/ChromeTab$ContentsState;->buffer()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v0

    invoke-virtual {v1, v0}, Ljava/io/DataOutputStream;->writeInt(I)V

    .line 2568
    if-eqz p2, :cond_3

    .line 2569
    iget-object v0, p1, Lcom/google/android/apps/chrome/tab/ChromeTab$TabState;->contentsState:Lcom/google/android/apps/chrome/tab/ChromeTab$ContentsState;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tab/ChromeTab$ContentsState;->buffer()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v0

    new-array v0, v0, [B

    .line 2570
    iget-object v2, p1, Lcom/google/android/apps/chrome/tab/ChromeTab$TabState;->contentsState:Lcom/google/android/apps/chrome/tab/ChromeTab$ContentsState;

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/tab/ChromeTab$ContentsState;->buffer()Ljava/nio/ByteBuffer;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/nio/ByteBuffer;->get([B)Ljava/nio/ByteBuffer;

    .line 2571
    invoke-virtual {v1, v0}, Ljava/io/DataOutputStream;->write([B)V

    .line 2575
    :goto_1
    iget v0, p1, Lcom/google/android/apps/chrome/tab/ChromeTab$TabState;->parentId:I

    invoke-virtual {v1, v0}, Ljava/io/DataOutputStream;->writeInt(I)V

    .line 2576
    iget-object v0, p1, Lcom/google/android/apps/chrome/tab/ChromeTab$TabState;->openerAppId:Ljava/lang/String;

    if-eqz v0, :cond_4

    iget-object v0, p1, Lcom/google/android/apps/chrome/tab/ChromeTab$TabState;->openerAppId:Ljava/lang/String;

    :goto_2
    invoke-virtual {v1, v0}, Ljava/io/DataOutputStream;->writeUTF(Ljava/lang/String;)V

    .line 2577
    iget-object v0, p1, Lcom/google/android/apps/chrome/tab/ChromeTab$TabState;->contentsState:Lcom/google/android/apps/chrome/tab/ChromeTab$ContentsState;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tab/ChromeTab$ContentsState;->version()I

    move-result v0

    invoke-virtual {v1, v0}, Ljava/io/DataOutputStream;->writeInt(I)V

    .line 2578
    iget-wide v2, p1, Lcom/google/android/apps/chrome/tab/ChromeTab$TabState;->syncId:J

    invoke-virtual {v1, v2, v3}, Ljava/io/DataOutputStream;->writeLong(J)V

    .line 2579
    iget-boolean v0, p1, Lcom/google/android/apps/chrome/tab/ChromeTab$TabState;->shouldPreserve:Z

    invoke-virtual {v1, v0}, Ljava/io/DataOutputStream;->writeBoolean(Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2581
    invoke-static {v1}, Lorg/chromium/chrome/browser/util/StreamUtil;->closeQuietly(Ljava/io/Closeable;)V

    .line 2582
    :cond_1
    return-void

    .line 2557
    :cond_2
    new-instance v0, Ljava/io/DataOutputStream;

    invoke-direct {v0, p0}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    move-object v1, v0

    goto :goto_0

    .line 2573
    :cond_3
    :try_start_1
    invoke-virtual {p0}, Ljava/io/FileOutputStream;->getChannel()Ljava/nio/channels/FileChannel;

    move-result-object v0

    iget-object v2, p1, Lcom/google/android/apps/chrome/tab/ChromeTab$TabState;->contentsState:Lcom/google/android/apps/chrome/tab/ChromeTab$ContentsState;

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/tab/ChromeTab$ContentsState;->buffer()Ljava/nio/ByteBuffer;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/nio/channels/FileChannel;->write(Ljava/nio/ByteBuffer;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 2581
    :catchall_0
    move-exception v0

    invoke-static {v1}, Lorg/chromium/chrome/browser/util/StreamUtil;->closeQuietly(Ljava/io/Closeable;)V

    throw v0

    .line 2576
    :cond_4
    :try_start_2
    const-string/jumbo v0, ""
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_2
.end method

.method private setInterceptNavigationDelegate(Lcom/google/android/apps/chrome/tab/ChromeTab$InterceptNavigationDelegateImpl;)V
    .locals 2

    .prologue
    .line 3036
    iput-object p1, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mInterceptNavigationDelegate:Lcom/google/android/apps/chrome/tab/ChromeTab$InterceptNavigationDelegateImpl;

    .line 3037
    iget-wide v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mNativeChromeTab:J

    invoke-direct {p0, v0, v1, p1}, Lcom/google/android/apps/chrome/tab/ChromeTab;->nativeSetInterceptNavigationDelegate(JLorg/chromium/components/navigation_interception/InterceptNavigationDelegate;)V

    .line 3038
    return-void
.end method

.method private shouldUpdateThumbnail()Z
    .locals 3

    .prologue
    .line 2025
    iget-wide v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mNativeChromeTab:J

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getUrl()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/apps/chrome/tab/ChromeTab;->nativeShouldUpdateThumbnail(JLjava/lang/String;)Z

    move-result v0

    return v0
.end method

.method private shouldWelcomePageLinkToTermsOfService()Z
    .locals 1

    .prologue
    .line 3003
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mActivity:Lcom/google/android/apps/chrome/ChromeActivity;

    invoke-static {v0}, Lorg/chromium/ui/base/DeviceFormFactor;->isTablet(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mActivity:Lcom/google/android/apps/chrome/ChromeActivity;

    invoke-static {v0}, Lorg/chromium/chrome/browser/util/FeatureUtilities;->canAllowSync(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private snapshotStateDownloading()V
    .locals 2

    .prologue
    .line 2361
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mSnapshotDownloadingInfoBar:Lcom/google/android/apps/chrome/infobar/SnapshotDownloadingInfobar;

    if-nez v0, :cond_0

    .line 2362
    new-instance v0, Lcom/google/android/apps/chrome/infobar/SnapshotDownloadingInfobar;

    new-instance v1, Lcom/google/android/apps/chrome/tab/ChromeTab$9;

    invoke-direct {v1, p0}, Lcom/google/android/apps/chrome/tab/ChromeTab$9;-><init>(Lcom/google/android/apps/chrome/tab/ChromeTab;)V

    invoke-direct {v0, v1}, Lcom/google/android/apps/chrome/infobar/SnapshotDownloadingInfobar;-><init>(Lorg/chromium/chrome/browser/infobar/InfoBarListeners$Dismiss;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mSnapshotDownloadingInfoBar:Lcom/google/android/apps/chrome/infobar/SnapshotDownloadingInfobar;

    .line 2372
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mSnapshotDownloadingInfoBar:Lcom/google/android/apps/chrome/infobar/SnapshotDownloadingInfobar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/infobar/SnapshotDownloadingInfobar;->setExpireOnNavigation(Z)V

    .line 2373
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mSnapshotDownloadingInfoBar:Lcom/google/android/apps/chrome/infobar/SnapshotDownloadingInfobar;

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->addInfoBar(Lorg/chromium/chrome/browser/infobar/InfoBar;)V

    .line 2375
    :cond_0
    return-void
.end method

.method private snapshotStateError()V
    .locals 4

    .prologue
    .line 2378
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mSnapshotDownloadingInfoBar:Lcom/google/android/apps/chrome/infobar/SnapshotDownloadingInfobar;

    if-eqz v0, :cond_0

    .line 2379
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mSnapshotDownloadingInfoBar:Lcom/google/android/apps/chrome/infobar/SnapshotDownloadingInfobar;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/infobar/SnapshotDownloadingInfobar;->dismissJavaOnlyInfoBar()V

    .line 2381
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mSnapshotDownloadErrorInfoBar:Lorg/chromium/chrome/browser/infobar/MessageInfoBar;

    if-nez v0, :cond_1

    .line 2382
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    sget v1, Lcom/google/android/apps/chrome/R$string;->snapshot_download_failed_infobar:I

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 2384
    new-instance v1, Lorg/chromium/chrome/browser/infobar/MessageInfoBar;

    new-instance v2, Lcom/google/android/apps/chrome/tab/ChromeTab$10;

    invoke-direct {v2, p0}, Lcom/google/android/apps/chrome/tab/ChromeTab$10;-><init>(Lcom/google/android/apps/chrome/tab/ChromeTab;)V

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3, v0}, Lorg/chromium/chrome/browser/infobar/MessageInfoBar;-><init>(Lorg/chromium/chrome/browser/infobar/InfoBarListeners$Dismiss;ILjava/lang/CharSequence;)V

    iput-object v1, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mSnapshotDownloadErrorInfoBar:Lorg/chromium/chrome/browser/infobar/MessageInfoBar;

    .line 2391
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mSnapshotDownloadErrorInfoBar:Lorg/chromium/chrome/browser/infobar/MessageInfoBar;

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->addInfoBar(Lorg/chromium/chrome/browser/infobar/InfoBar;)V

    .line 2393
    :cond_1
    return-void
.end method

.method private snapshotStateNeverReady()V
    .locals 1

    .prologue
    .line 2396
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mSnapshotDownloadingInfoBar:Lcom/google/android/apps/chrome/infobar/SnapshotDownloadingInfobar;

    if-eqz v0, :cond_0

    .line 2397
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mSnapshotDownloadingInfoBar:Lcom/google/android/apps/chrome/infobar/SnapshotDownloadingInfobar;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/infobar/SnapshotDownloadingInfobar;->dismissJavaOnlyInfoBar()V

    .line 2399
    :cond_0
    return-void
.end method

.method private snapshotStateReady(Landroid/net/Uri;)V
    .locals 2

    .prologue
    .line 2343
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mSnapshotDownloadingInfoBar:Lcom/google/android/apps/chrome/infobar/SnapshotDownloadingInfobar;

    if-eqz v0, :cond_0

    .line 2344
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mSnapshotDownloadingInfoBar:Lcom/google/android/apps/chrome/infobar/SnapshotDownloadingInfobar;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/infobar/SnapshotDownloadingInfobar;->dismissJavaOnlyInfoBar()V

    .line 2347
    :cond_0
    new-instance v0, Lcom/google/android/apps/chrome/infobar/SnapshotDownloadSuceededInfobar;

    invoke-direct {v0, p0, p1}, Lcom/google/android/apps/chrome/infobar/SnapshotDownloadSuceededInfobar;-><init>(Lorg/chromium/chrome/browser/Tab;Landroid/net/Uri;)V

    .line 2349
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/infobar/SnapshotDownloadSuceededInfobar;->setExpireOnNavigation(Z)V

    .line 2350
    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->addInfoBar(Lorg/chromium/chrome/browser/infobar/InfoBar;)V

    .line 2356
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mSnapshotId:Ljava/lang/String;

    .line 2357
    return-void
.end method

.method private updateHistoryThumbnail(Landroid/graphics/Bitmap;)V
    .locals 10

    .prologue
    const-wide/high16 v8, 0x3ff0000000000000L    # 1.0

    const/high16 v7, 0x3f800000    # 1.0f

    .line 2056
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->isIncognito()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2100
    :goto_0
    return-void

    .line 2062
    :cond_0
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    iget v1, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mThumbnailWidth:I

    if-ne v0, v1, :cond_1

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    iget v1, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mThumbnailHeight:I

    if-ne v0, v1, :cond_1

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getConfig()Landroid/graphics/Bitmap$Config;

    move-result-object v0

    sget-object v1, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    if-eq v0, v1, :cond_5

    .line 2066
    :cond_1
    const/4 v0, 0x2

    :try_start_0
    new-array v2, v0, [I

    const/4 v0, 0x0

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    aput v1, v2, v0

    const/4 v0, 0x1

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    aput v1, v2, v0

    .line 2071
    iget v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mThumbnailWidth:I

    int-to-float v0, v0

    const/4 v1, 0x0

    aget v1, v2, v1

    int-to-float v1, v1

    div-float/2addr v0, v1

    iget v1, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mThumbnailHeight:I

    int-to-float v1, v1

    const/4 v3, 0x1

    aget v3, v2, v3

    int-to-float v3, v3

    div-float/2addr v1, v3

    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    move-result v3

    .line 2074
    cmpg-float v0, v3, v7

    if-gez v0, :cond_3

    iget v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mThumbnailWidth:I

    float-to-double v4, v3

    invoke-static {v4, v5}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v4

    div-double v4, v8, v4

    double-to-int v1, v4

    mul-int/2addr v0, v1

    move v1, v0

    .line 2076
    :goto_1
    cmpg-float v0, v3, v7

    if-gez v0, :cond_4

    iget v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mThumbnailHeight:I

    float-to-double v4, v3

    invoke-static {v4, v5}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v4

    div-double v4, v8, v4

    double-to-int v3, v4

    mul-int/2addr v0, v3

    .line 2078
    :goto_2
    invoke-static {v2, v1, v0}, Lcom/google/android/apps/chrome/utilities/ChromeMathUtils;->scaleToFitTargetSize([III)F

    move-result v3

    .line 2080
    const/4 v4, 0x0

    aget v2, v2, v4

    sub-int v2, v1, v2

    int-to-float v2, v2

    const/high16 v4, 0x40000000    # 2.0f

    div-float/2addr v2, v4

    div-float/2addr v2, v3

    .line 2081
    sget-object v4, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v1, v0, v4}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 2083
    new-instance v1, Landroid/graphics/Canvas;

    invoke-direct {v1, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 2084
    invoke-virtual {v1, v3, v3}, Landroid/graphics/Canvas;->scale(FF)V

    .line 2085
    const/4 v4, 0x0

    new-instance v5, Landroid/graphics/Paint;

    const/4 v6, 0x2

    invoke-direct {v5, v6}, Landroid/graphics/Paint;-><init>(I)V

    invoke-virtual {v1, p1, v2, v4, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 2086
    cmpg-float v1, v3, v7

    if-gez v1, :cond_2

    .line 2087
    iget v1, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mThumbnailWidth:I

    iget v2, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mThumbnailHeight:I

    const/4 v3, 0x1

    invoke-static {v0, v1, v2, v3}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 2090
    :cond_2
    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->updateThumbnail(Landroid/graphics/Bitmap;)V

    .line 2091
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 2093
    :catch_0
    move-exception v0

    const-string/jumbo v0, "ChromeTab"

    const-string/jumbo v1, "OutOfMemoryError while updating the history thumbnail."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 2094
    const/16 v0, 0x31

    invoke-static {v0}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->broadcastImmediateNotification(I)V

    goto/16 :goto_0

    .line 2074
    :cond_3
    :try_start_1
    iget v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mThumbnailWidth:I

    move v1, v0

    goto :goto_1

    .line 2076
    :cond_4
    iget v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mThumbnailHeight:I
    :try_end_1
    .catch Ljava/lang/OutOfMemoryError; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_2

    .line 2098
    :cond_5
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/tab/ChromeTab;->updateThumbnail(Landroid/graphics/Bitmap;)V

    goto/16 :goto_0
.end method

.method private updateThumbnail(Landroid/graphics/Bitmap;)V
    .locals 4

    .prologue
    .line 2029
    iget-wide v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mNativeChromeTab:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 2030
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getContentViewCore()Lorg/chromium/content/browser/ContentViewCore;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getContentViewCore()Lorg/chromium/content/browser/ContentViewCore;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/content/browser/ContentViewCore;->computeVerticalScrollOffset()I

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    .line 2032
    :goto_0
    iget-wide v2, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mNativeChromeTab:J

    invoke-direct {p0, v2, v3, p1, v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->nativeUpdateThumbnail(JLandroid/graphics/Bitmap;Z)V

    .line 2033
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 2034
    const-string/jumbo v1, "url"

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getUrl()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2035
    iget-object v1, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mActivity:Lcom/google/android/apps/chrome/ChromeActivity;

    const/16 v2, 0x3d

    invoke-static {v1, v2, v0}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->broadcastNotification(Landroid/content/Context;ILandroid/os/Bundle;)V

    .line 2037
    :cond_0
    return-void

    .line 2030
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private updateTitle()Z
    .locals 1

    .prologue
    .line 1699
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->isFrozen()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-super {p0}, Lorg/chromium/chrome/browser/Tab;->getTitle()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->updateTitle(Ljava/lang/String;)Z

    move-result v0

    .line 1701
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private updateTitle(Ljava/lang/String;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 1688
    iget-object v2, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mTitle:Ljava/lang/String;

    invoke-static {v2, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 1689
    iput-boolean v1, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mIsTabStateDirty:Z

    .line 1690
    iput-object p1, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mTitle:Ljava/lang/String;

    .line 1691
    invoke-static {p1}, Lorg/chromium/ui/base/LocalizationUtils;->getFirstStrongCharacterDirection(Ljava/lang/String;)I

    move-result v2

    if-ne v2, v1, :cond_0

    move v0, v1

    :cond_0
    iput-boolean v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mIsTitleDirectionRtl:Z

    .line 1695
    :goto_0
    return v1

    :cond_1
    move v1, v0

    goto :goto_0
.end method


# virtual methods
.method public activateNearestFindResult(FF)V
    .locals 4

    .prologue
    .line 3095
    sget-boolean v0, Lcom/google/android/apps/chrome/tab/ChromeTab;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-wide v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mNativeChromeTab:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 3096
    :cond_0
    iget-wide v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mNativeChromeTab:J

    invoke-direct {p0, v0, v1, p1, p2}, Lcom/google/android/apps/chrome/tab/ChromeTab;->nativeActivateNearestFindResult(JFF)V

    .line 3097
    return-void
.end method

.method public associateWithApp(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2944
    iput-object p1, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mAppAssociatedWith:Ljava/lang/String;

    .line 2945
    return-void
.end method

.method public canProvideThumbnail()Z
    .locals 1

    .prologue
    .line 1883
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mNeedsReload:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected createAutoLoginProcessor()Lorg/chromium/chrome/browser/infobar/AutoLoginProcessor;
    .locals 2

    .prologue
    .line 1737
    new-instance v0, Lcom/google/android/apps/chrome/infobar/AutoLoginProcessorImpl;

    iget-object v1, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mActivity:Lcom/google/android/apps/chrome/ChromeActivity;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/ChromeActivity;->getTabModelSelector()Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/apps/chrome/infobar/AutoLoginProcessorImpl;-><init>(Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;)V

    return-object v0
.end method

.method protected createContextMenuPopulator()Lorg/chromium/chrome/browser/contextmenu/ContextMenuPopulator;
    .locals 3

    .prologue
    .line 1150
    new-instance v0, Lcom/google/android/apps/chrome/tab/ChromeTab$ChromeTabChromeContextMenuPopulator;

    new-instance v1, Lcom/google/android/apps/chrome/tab/ChromeTab$ChromeTabChromeContextMenuItemDelegate;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/google/android/apps/chrome/tab/ChromeTab$ChromeTabChromeContextMenuItemDelegate;-><init>(Lcom/google/android/apps/chrome/tab/ChromeTab;Lcom/google/android/apps/chrome/tab/ChromeTab$1;)V

    invoke-direct {v0, v1}, Lcom/google/android/apps/chrome/tab/ChromeTab$ChromeTabChromeContextMenuPopulator;-><init>(Lcom/google/android/apps/chrome/tab/ChromeTab$ChromeTabChromeContextMenuItemDelegate;)V

    return-object v0
.end method

.method public createHistoricalTab()V
    .locals 2

    .prologue
    .line 2003
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->isFrozen()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2004
    iget-wide v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mNativeChromeTab:J

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/chrome/tab/ChromeTab;->nativeCreateHistoricalTab(J)V

    .line 2008
    :cond_0
    :goto_0
    return-void

    .line 2005
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mFrozenContentsState:Lcom/google/android/apps/chrome/tab/ChromeTab$ContentsState;

    if-eqz v0, :cond_0

    .line 2006
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mFrozenContentsState:Lcom/google/android/apps/chrome/tab/ChromeTab$ContentsState;

    invoke-static {v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->createHistoricalTabFromState(Lcom/google/android/apps/chrome/tab/ChromeTab$ContentsState;)V

    goto :goto_0
.end method

.method protected createWebContentsDelegate()Lorg/chromium/chrome/browser/Tab$TabChromeWebContentsDelegateAndroid;
    .locals 1

    .prologue
    .line 836
    new-instance v0, Lcom/google/android/apps/chrome/tab/ChromeTab$TabChromeWebContentsDelegateAndroidImpl;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/tab/ChromeTab$TabChromeWebContentsDelegateAndroidImpl;-><init>(Lcom/google/android/apps/chrome/tab/ChromeTab;)V

    return-object v0
.end method

.method dangerousDownloadValidated(IZ)V
    .locals 4

    .prologue
    .line 3046
    sget-boolean v0, Lcom/google/android/apps/chrome/tab/ChromeTab;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-wide v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mNativeChromeTab:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 3047
    :cond_0
    iget-wide v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mNativeChromeTab:J

    invoke-direct {p0, v0, v1, p1, p2}, Lcom/google/android/apps/chrome/tab/ChromeTab;->nativeDangerousDownloadValidated(JIZ)V

    .line 3048
    return-void
.end method

.method public destroy()V
    .locals 3

    .prologue
    const/high16 v2, 0x7fc00000    # NaNf

    .line 1967
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->updateTitle()Z

    .line 1970
    invoke-super {p0}, Lorg/chromium/chrome/browser/Tab;->destroy()V

    .line 1971
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mNativeChromeTab:J

    .line 1973
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mNeedsReload:Z

    .line 1974
    iput v2, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mPreviousFullscreenTopControlsOffsetY:F

    .line 1975
    iput v2, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mPreviousFullscreenContentOffsetY:F

    .line 1976
    return-void
.end method

.method protected destroyContentViewCoreInternal(Lorg/chromium/content/browser/ContentViewCore;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1796
    invoke-super {p0, p1}, Lorg/chromium/chrome/browser/Tab;->destroyContentViewCoreInternal(Lorg/chromium/content/browser/ContentViewCore;)V

    .line 1798
    invoke-virtual {p1}, Lorg/chromium/content/browser/ContentViewCore;->getContainerView()Landroid/view/ViewGroup;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setOnHierarchyChangeListener(Landroid/view/ViewGroup$OnHierarchyChangeListener;)V

    .line 1799
    invoke-virtual {p1}, Lorg/chromium/content/browser/ContentViewCore;->getContainerView()Landroid/view/ViewGroup;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setOnSystemUiVisibilityChangeListener(Landroid/view/View$OnSystemUiVisibilityChangeListener;)V

    .line 1800
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mGestureStateListener:Lorg/chromium/content_public/browser/GestureStateListener;

    if-eqz v0, :cond_0

    .line 1801
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mGestureStateListener:Lorg/chromium/content_public/browser/GestureStateListener;

    invoke-virtual {p1, v0}, Lorg/chromium/content/browser/ContentViewCore;->removeGestureStateListener(Lorg/chromium/content_public/browser/GestureStateListener;)V

    .line 1803
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mContextualSearchGestureListener:Lorg/chromium/content_public/browser/GestureStateListener;

    if-eqz v0, :cond_1

    .line 1804
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mContextualSearchGestureListener:Lorg/chromium/content_public/browser/GestureStateListener;

    invoke-virtual {p1, v0}, Lorg/chromium/content/browser/ContentViewCore;->removeGestureStateListener(Lorg/chromium/content_public/browser/GestureStateListener;)V

    .line 1805
    iput-object v1, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mContextualSearchGestureListener:Lorg/chromium/content_public/browser/GestureStateListener;

    .line 1807
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mWebContentsObserver:Lorg/chromium/content/browser/WebContentsObserverAndroid;

    if-eqz v0, :cond_2

    .line 1808
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mWebContentsObserver:Lorg/chromium/content/browser/WebContentsObserverAndroid;

    invoke-virtual {v0}, Lorg/chromium/content/browser/WebContentsObserverAndroid;->detachFromWebContents()V

    .line 1810
    :cond_2
    iput-object v1, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mWebContentsObserver:Lorg/chromium/content/browser/WebContentsObserverAndroid;

    .line 1811
    sget-object v0, Lcom/google/android/apps/chrome/tab/ChromeTab;->CONTEXTUAL_SEARCH_NOTIFICATIONS:[I

    iget-object v1, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mContextualSearchNotificationHandler:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;

    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->unregisterForNotifications([ILcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;)V

    .line 1813
    return-void
.end method

.method public getAppAssociatedWith()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2914
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mAppAssociatedWith:Ljava/lang/String;

    return-object v0
.end method

.method public getAuthenticatorHelper()Lcom/google/android/apps/chrome/tab/AuthenticatorHelper;
    .locals 1

    .prologue
    .line 2807
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mInterceptNavigationDelegate:Lcom/google/android/apps/chrome/tab/ChromeTab$InterceptNavigationDelegateImpl;

    iget-object v0, v0, Lcom/google/android/apps/chrome/tab/ChromeTab$InterceptNavigationDelegateImpl;->mAuthenticatorHelper:Lcom/google/android/apps/chrome/tab/AuthenticatorHelper;

    return-object v0
.end method

.method public getBackgroundContentViewHelper()Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;
    .locals 1

    .prologue
    .line 1156
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mBackgroundContentViewHelper:Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;

    return-object v0
.end method

.method public getBaseUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1914
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mBaseUrl:Ljava/lang/String;

    return-object v0
.end method

.method public getBookmarkId()J
    .locals 3

    .prologue
    .line 3011
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->isFrozen()Z

    move-result v0

    if-eqz v0, :cond_0

    const-wide/16 v0, -0x1

    :goto_0
    return-wide v0

    :cond_0
    iget-wide v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mNativeChromeTab:J

    const/4 v2, 0x0

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/apps/chrome/tab/ChromeTab;->nativeGetBookmarkId(JZ)J

    move-result-wide v0

    goto :goto_0
.end method

.method public getChromeWebContentsDelegateAndroidForTest()Lorg/chromium/chrome/browser/ChromeWebContentsDelegateAndroid;
    .locals 1

    .prologue
    .line 2021
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getChromeWebContentsDelegateAndroid()Lorg/chromium/chrome/browser/Tab$TabChromeWebContentsDelegateAndroid;

    move-result-object v0

    return-object v0
.end method

.method public getContextualSearchManager()Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;
    .locals 1

    .prologue
    .line 2229
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mActivity:Lcom/google/android/apps/chrome/ChromeActivity;

    instance-of v0, v0, Lcom/google/android/apps/chrome/CompositorChromeActivity;

    if-eqz v0, :cond_0

    .line 2230
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mActivity:Lcom/google/android/apps/chrome/ChromeActivity;

    check-cast v0, Lcom/google/android/apps/chrome/CompositorChromeActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/CompositorChromeActivity;->getContextualSearchManager()Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;

    move-result-object v0

    .line 2232
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method getDownloadWarningText(Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 3041
    sget-boolean v0, Lcom/google/android/apps/chrome/tab/ChromeTab;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-wide v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mNativeChromeTab:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 3042
    :cond_0
    iget-wide v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mNativeChromeTab:J

    invoke-direct {p0, v0, v1, p1}, Lcom/google/android/apps/chrome/tab/ChromeTab;->nativeGetDownloadWarningText(JLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getFallbackTextureId()I
    .locals 1

    .prologue
    .line 2879
    const/4 v0, -0x1

    return v0
.end method

.method public getFullscreenOverdrawBottomHeightPix()F
    .locals 1

    .prologue
    .line 1715
    iget v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mPreviousFullscreenOverdrawBottomHeight:F

    return v0
.end method

.method public getInfoBars()Ljava/util/List;
    .locals 1

    .prologue
    .line 2723
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getInfoBarContainer()Lorg/chromium/chrome/browser/infobar/InfoBarContainer;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 2724
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getInfoBarContainer()Lorg/chromium/chrome/browser/infobar/InfoBarContainer;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/infobar/InfoBarContainer;->getInfoBars()Ljava/util/ArrayList;

    move-result-object v0

    goto :goto_0
.end method

.method public getLaunchType()Lorg/chromium/chrome/browser/tabmodel/TabModel$TabLaunchType;
    .locals 1

    .prologue
    .line 2711
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mLaunchType:Lorg/chromium/chrome/browser/tabmodel/TabModel$TabLaunchType;

    return-object v0
.end method

.method protected getNativePageFactory()Lcom/google/android/apps/chrome/tab/NativePageFactory;
    .locals 1

    .prologue
    .line 3124
    invoke-static {}, Lcom/google/android/apps/chrome/tab/NativePageFactory;->getInstance()Lcom/google/android/apps/chrome/tab/NativePageFactory;

    move-result-object v0

    return-object v0
.end method

.method protected getNativePtr()J
    .locals 2

    .prologue
    .line 792
    iget-wide v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mNativeChromeTab:J

    return-wide v0
.end method

.method public getPreviousFindText()Ljava/lang/String;
    .locals 4

    .prologue
    .line 3080
    sget-boolean v0, Lcom/google/android/apps/chrome/tab/ChromeTab;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-wide v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mNativeChromeTab:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 3081
    :cond_0
    iget-wide v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mNativeChromeTab:J

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/chrome/tab/ChromeTab;->nativeGetPreviousFindText(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getProgress()I
    .locals 2

    .prologue
    .line 1848
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mBackgroundContentViewHelper:Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->hasPendingBackgroundPage()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mBackgroundContentViewHelper:Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->isPageSwappingInProgress()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1850
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mBackgroundContentViewHelper:Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->getProgress()I

    move-result v0

    .line 1858
    :cond_1
    :goto_0
    return v0

    .line 1852
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getChromeWebContentsDelegateAndroid()Lorg/chromium/chrome/browser/Tab$TabChromeWebContentsDelegateAndroid;

    move-result-object v0

    .line 1853
    if-nez v0, :cond_3

    .line 1854
    const/4 v0, 0x0

    goto :goto_0

    .line 1856
    :cond_3
    invoke-virtual {v0}, Lorg/chromium/chrome/browser/ChromeWebContentsDelegateAndroid;->getMostRecentProgress()I

    move-result v0

    .line 1857
    iget v1, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mLoadProgressAtViewSwapInTime:I

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 1858
    iget-boolean v1, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mIsLoading:Z

    if-nez v1, :cond_1

    const/16 v0, 0x64

    goto :goto_0
.end method

.method public getReaderModeManager()Lcom/google/android/apps/chrome/dom_distiller/ReaderModeManager;
    .locals 1

    .prologue
    .line 3131
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mReaderModeManager:Lcom/google/android/apps/chrome/dom_distiller/ReaderModeManager;

    return-object v0
.end method

.method public getState()Lcom/google/android/apps/chrome/tab/ChromeTab$TabState;
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 2496
    iget-wide v2, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mNativeChromeTab:J

    const-wide/16 v4, 0x0

    cmp-long v0, v2, v4

    if-nez v0, :cond_0

    move-object v0, v1

    .line 2525
    :goto_0
    return-object v0

    .line 2497
    :cond_0
    new-instance v2, Lcom/google/android/apps/chrome/tab/ChromeTab$TabState;

    invoke-direct {v2}, Lcom/google/android/apps/chrome/tab/ChromeTab$TabState;-><init>()V

    .line 2499
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mFrozenContentsState:Lcom/google/android/apps/chrome/tab/ChromeTab$ContentsState;

    if-eqz v0, :cond_1

    .line 2500
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mFrozenContentsState:Lcom/google/android/apps/chrome/tab/ChromeTab$ContentsState;

    iput-object v0, v2, Lcom/google/android/apps/chrome/tab/ChromeTab$TabState;->contentsState:Lcom/google/android/apps/chrome/tab/ChromeTab$ContentsState;

    .line 2520
    :goto_1
    iget-wide v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mTimestampMillis:J

    iput-wide v0, v2, Lcom/google/android/apps/chrome/tab/ChromeTab$TabState;->timestampMillis:J

    .line 2521
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getParentId()I

    move-result v0

    iput v0, v2, Lcom/google/android/apps/chrome/tab/ChromeTab$TabState;->parentId:I

    .line 2522
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getAppAssociatedWith()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, Lcom/google/android/apps/chrome/tab/ChromeTab$TabState;->openerAppId:Ljava/lang/String;

    .line 2523
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getSyncId()I

    move-result v0

    int-to-long v0, v0

    iput-wide v0, v2, Lcom/google/android/apps/chrome/tab/ChromeTab$TabState;->syncId:J

    .line 2524
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->shouldPreserve()Z

    move-result v0

    iput-boolean v0, v2, Lcom/google/android/apps/chrome/tab/ChromeTab$TabState;->shouldPreserve:Z

    move-object v0, v2

    .line 2525
    goto :goto_0

    .line 2503
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mPendingLoadParams:Lorg/chromium/content_public/browser/LoadUrlParams;

    if-eqz v0, :cond_4

    .line 2504
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mPendingLoadParams:Lorg/chromium/content_public/browser/LoadUrlParams;

    invoke-virtual {v0}, Lorg/chromium/content_public/browser/LoadUrlParams;->getReferrer()Lorg/chromium/content_public/common/Referrer;

    move-result-object v4

    .line 2505
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mPendingLoadParams:Lorg/chromium/content_public/browser/LoadUrlParams;

    invoke-virtual {v0}, Lorg/chromium/content_public/browser/LoadUrlParams;->getUrl()Ljava/lang/String;

    move-result-object v5

    if-eqz v4, :cond_2

    invoke-virtual {v4}, Lorg/chromium/content_public/common/Referrer;->getUrl()Ljava/lang/String;

    move-result-object v0

    move-object v3, v0

    :goto_2
    if-eqz v4, :cond_3

    invoke-virtual {v4}, Lorg/chromium/content_public/common/Referrer;->getPolicy()I

    move-result v0

    :goto_3
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->isIncognito()Z

    move-result v4

    invoke-static {v5, v3, v0, v4}, Lcom/google/android/apps/chrome/tab/ChromeTab;->nativeCreateSingleNavigationStateAsByteBuffer(Ljava/lang/String;Ljava/lang/String;IZ)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 2515
    :goto_4
    if-nez v0, :cond_5

    move-object v0, v1

    goto :goto_0

    :cond_2
    move-object v3, v1

    .line 2505
    goto :goto_2

    :cond_3
    const/4 v0, 0x0

    goto :goto_3

    .line 2512
    :cond_4
    iget-wide v4, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mNativeChromeTab:J

    invoke-direct {p0, v4, v5}, Lcom/google/android/apps/chrome/tab/ChromeTab;->nativeGetContentsStateAsByteBuffer(J)Ljava/nio/ByteBuffer;

    move-result-object v0

    goto :goto_4

    .line 2516
    :cond_5
    new-instance v1, Lcom/google/android/apps/chrome/tab/ChromeTab$ContentsStateNative;

    invoke-direct {v1, v0}, Lcom/google/android/apps/chrome/tab/ChromeTab$ContentsStateNative;-><init>(Ljava/nio/ByteBuffer;)V

    iput-object v1, v2, Lcom/google/android/apps/chrome/tab/ChromeTab$TabState;->contentsState:Lcom/google/android/apps/chrome/tab/ChromeTab$ContentsState;

    .line 2517
    iget-object v0, v2, Lcom/google/android/apps/chrome/tab/ChromeTab$TabState;->contentsState:Lcom/google/android/apps/chrome/tab/ChromeTab$ContentsState;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/tab/ChromeTab$ContentsState;->setVersion(I)V

    goto :goto_1
.end method

.method public getTabRedirectHandler()Lcom/google/android/apps/chrome/tab/TabRedirectHandler;
    .locals 1

    .prologue
    .line 2827
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mTabRedirectHandler:Lcom/google/android/apps/chrome/tab/TabRedirectHandler;

    return-object v0
.end method

.method public getTabUma()Lcom/google/android/apps/chrome/tab/TabUma;
    .locals 1

    .prologue
    .line 2815
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mTabUma:Lcom/google/android/apps/chrome/tab/TabUma;

    return-object v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1903
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mTitle:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 1904
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->updateTitle()Z

    .line 1906
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mTitle:Ljava/lang/String;

    return-object v0
.end method

.method protected getTopControlsStateConstraints()I
    .locals 6

    .prologue
    const/4 v4, 0x3

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 1651
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mFullscreenManager:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;

    if-nez v0, :cond_1

    .line 1677
    :cond_0
    :goto_0
    return v1

    .line 1653
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getUrl()Ljava/lang/String;

    move-result-object v0

    .line 1654
    if-eqz v0, :cond_3

    const-string/jumbo v3, "chrome://"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_3

    const-string/jumbo v3, "chrome-native://"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    move v0, v1

    .line 1658
    :goto_1
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getSecurityLevel()I

    move-result v3

    .line 1659
    const/4 v5, 0x5

    if-eq v3, v5, :cond_4

    if-eq v3, v4, :cond_4

    move v3, v1

    :goto_2
    and-int/2addr v3, v0

    .line 1663
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mIsImeShowing:Z

    if-nez v0, :cond_5

    move v0, v1

    :goto_3
    and-int/2addr v3, v0

    .line 1664
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mIsFullscreenWaitingForLoad:Z

    if-nez v0, :cond_6

    move v0, v1

    :goto_4
    and-int/2addr v3, v0

    .line 1665
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mIsShowingErrorPage:Z

    if-nez v0, :cond_7

    move v0, v1

    :goto_5
    and-int/2addr v3, v0

    .line 1666
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lorg/chromium/chrome/browser/AccessibilityUtil;->isAccessibilityEnabled(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_8

    move v0, v1

    :goto_6
    and-int/2addr v0, v3

    .line 1669
    iget-object v3, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mFullscreenManager:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;

    invoke-virtual {v3}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->getPersistentFullscreenMode()Z

    move-result v3

    if-nez v3, :cond_2

    move v2, v1

    .line 1672
    :cond_2
    if-nez v2, :cond_9

    .line 1673
    const/4 v1, 0x2

    goto :goto_0

    :cond_3
    move v0, v2

    .line 1654
    goto :goto_1

    :cond_4
    move v3, v2

    .line 1659
    goto :goto_2

    :cond_5
    move v0, v2

    .line 1663
    goto :goto_3

    :cond_6
    move v0, v2

    .line 1664
    goto :goto_4

    :cond_7
    move v0, v2

    .line 1665
    goto :goto_5

    :cond_8
    move v0, v2

    .line 1666
    goto :goto_6

    .line 1674
    :cond_9
    if-eqz v0, :cond_0

    move v1, v4

    goto :goto_0
.end method

.method public getTransitionPageHelper()Lcom/google/android/apps/chrome/tab/TransitionPageHelper;
    .locals 1

    .prologue
    .line 1161
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mTransitionPageHelper:Lcom/google/android/apps/chrome/tab/TransitionPageHelper;

    return-object v0
.end method

.method public getUrl()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1888
    invoke-super {p0}, Lorg/chromium/chrome/browser/Tab;->getUrl()Ljava/lang/String;

    move-result-object v0

    .line 1892
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getContentViewCore()Lorg/chromium/content/browser/ContentViewCore;

    move-result-object v1

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getNativePage()Lorg/chromium/chrome/browser/NativePage;

    move-result-object v1

    if-nez v1, :cond_0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1893
    :cond_0
    iput-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mUrl:Ljava/lang/String;

    .line 1896
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mUrl:Ljava/lang/String;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mUrl:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_2
    const-string/jumbo v0, ""

    goto :goto_0
.end method

.method public getUserBookmarkId()J
    .locals 3

    .prologue
    .line 3019
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->isFrozen()Z

    move-result v0

    if-eqz v0, :cond_0

    const-wide/16 v0, -0x1

    :goto_0
    return-wide v0

    :cond_0
    iget-wide v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mNativeChromeTab:J

    const/4 v2, 0x1

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/apps/chrome/tab/ChromeTab;->nativeGetBookmarkId(JZ)J

    move-result-wide v0

    goto :goto_0
.end method

.method public getViewClientForTesting()Lorg/chromium/content/browser/ContentViewClient;
    .locals 1

    .prologue
    .line 1171
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getContentViewClient()Lorg/chromium/content/browser/ContentViewClient;

    move-result-object v0

    return-object v0
.end method

.method public goBack()V
    .locals 1

    .prologue
    .line 2729
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mBackgroundContentViewHelper:Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->recordBack()V

    .line 2730
    invoke-super {p0}, Lorg/chromium/chrome/browser/Tab;->goBack()V

    .line 2731
    return-void
.end method

.method public hasPrerenderedUrl(Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 2993
    iget-wide v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mNativeChromeTab:J

    invoke-direct {p0, v0, v1, p1}, Lcom/google/android/apps/chrome/tab/ChromeTab;->nativeHasPrerenderedUrl(JLjava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public hide()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2280
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mIsHidden:Z

    if-eqz v0, :cond_0

    .line 2299
    :goto_0
    return-void

    .line 2281
    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->cancelEnableFullscreenLoadDelay()V

    .line 2282
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mIsHidden:Z

    .line 2283
    iput-boolean v1, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mIsImeShowing:Z

    .line 2285
    invoke-super {p0}, Lorg/chromium/chrome/browser/Tab;->hide()V

    .line 2288
    invoke-static {}, Lcom/google/android/apps/chrome/ntp/NativePageAssassin;->getInstance()Lcom/google/android/apps/chrome/ntp/NativePageAssassin;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/apps/chrome/ntp/NativePageAssassin;->tabHidden(Lorg/chromium/chrome/browser/Tab;)V

    .line 2291
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mFullscreenManager:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;

    if-eqz v0, :cond_1

    .line 2292
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mFullscreenManager:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->setPersistentFullscreenMode(Z)V

    .line 2293
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mFullscreenManager:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;

    iget v1, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mFullscreenHungRendererToken:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->hideControlsPersistent(I)V

    .line 2294
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mFullscreenHungRendererToken:I

    .line 2295
    const/high16 v0, 0x7fc00000    # NaNf

    iput v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mPreviousFullscreenOverdrawBottomHeight:F

    .line 2298
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mTabRedirectHandler:Lcom/google/android/apps/chrome/tab/TabRedirectHandler;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tab/TabRedirectHandler;->clear()V

    goto :goto_0
.end method

.method public initialize(JLcom/google/android/apps/chrome/compositor/TabContentManager;Z)V
    .locals 5

    .prologue
    .line 688
    invoke-static {}, Lorg/chromium/base/TraceEvent;->begin()V

    .line 689
    invoke-direct {p0, p3}, Lcom/google/android/apps/chrome/tab/ChromeTab;->internalInit(Lcom/google/android/apps/chrome/compositor/TabContentManager;)V

    .line 691
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mFrozenContentsState:Lcom/google/android/apps/chrome/tab/ChromeTab$ContentsState;

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mPendingLoadParams:Lorg/chromium/content_public/browser/LoadUrlParams;

    if-nez v0, :cond_1

    .line 693
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-nez v0, :cond_0

    .line 694
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->isIncognito()Z

    move-result v0

    invoke-static {v0, p4}, Lorg/chromium/chrome/browser/ContentViewUtil;->createNativeWebContents(ZZ)J

    move-result-wide p1

    .line 697
    :cond_0
    invoke-virtual {p0, p1, p2}, Lcom/google/android/apps/chrome/tab/ChromeTab;->initContentViewCore(J)V

    .line 699
    :cond_1
    iget-wide v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mTimestampMillis:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mTimestampMillis:J

    .line 700
    :cond_2
    invoke-static {}, Lorg/chromium/base/TraceEvent;->end()V

    .line 701
    return-void
.end method

.method public initialize(Lorg/chromium/content/browser/ContentViewCore;Lcom/google/android/apps/chrome/compositor/TabContentManager;)V
    .locals 4

    .prologue
    .line 710
    invoke-static {}, Lorg/chromium/base/TraceEvent;->begin()V

    .line 711
    invoke-direct {p0, p2}, Lcom/google/android/apps/chrome/tab/ChromeTab;->internalInit(Lcom/google/android/apps/chrome/compositor/TabContentManager;)V

    .line 712
    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/tab/ChromeTab;->setContentViewCore(Lorg/chromium/content/browser/ContentViewCore;)V

    .line 713
    iget-wide v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mTimestampMillis:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mTimestampMillis:J

    .line 714
    :cond_0
    invoke-static {}, Lorg/chromium/base/TraceEvent;->end()V

    .line 715
    return-void
.end method

.method public initializeNative()V
    .locals 4

    .prologue
    .line 764
    sget-boolean v0, Lcom/google/android/apps/chrome/tab/ChromeTab;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-wide v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mNativeChromeTab:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 765
    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->nativeInit()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mNativeChromeTab:J

    .line 766
    return-void
.end method

.method public isBeingRestored()Z
    .locals 1

    .prologue
    .line 2221
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mIsBeingRestored:Z

    return v0
.end method

.method isDownloadDangerous(Ljava/lang/String;)Z
    .locals 4

    .prologue
    .line 3051
    sget-boolean v0, Lcom/google/android/apps/chrome/tab/ChromeTab;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-wide v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mNativeChromeTab:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 3052
    :cond_0
    iget-wide v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mNativeChromeTab:J

    invoke-direct {p0, v0, v1, p1}, Lcom/google/android/apps/chrome/tab/ChromeTab;->nativeIsDownloadDangerous(JLjava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public isFrozen()Z
    .locals 1

    .prologue
    .line 2802
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getNativePage()Lorg/chromium/chrome/browser/NativePage;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getContentViewCore()Lorg/chromium/content/browser/ContentViewCore;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isHidden()Z
    .locals 1

    .prologue
    .line 1844
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mIsHidden:Z

    return v0
.end method

.method public isInitialNavigation()Z
    .locals 2

    .prologue
    .line 1879
    iget-wide v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mNativeChromeTab:J

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/chrome/tab/ChromeTab;->nativeIsInitialNavigation(J)Z

    move-result v0

    return v0
.end method

.method public isLoading()Z
    .locals 1

    .prologue
    .line 1957
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mIsLoading:Z

    return v0
.end method

.method public isLoadingAndRenderingDone()Z
    .locals 2

    .prologue
    .line 1867
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->isReady()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getProgress()I

    move-result v0

    const/16 v1, 0x64

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isShowingSadTab()Z
    .locals 2

    .prologue
    .line 3026
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mSadTabView:Landroid/view/View;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getContentViewCore()Lorg/chromium/content/browser/ContentViewCore;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mSadTabView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getContentViewCore()Lorg/chromium/content/browser/ContentViewCore;

    move-result-object v1

    invoke-virtual {v1}, Lorg/chromium/content/browser/ContentViewCore;->getContainerView()Landroid/view/ViewGroup;

    move-result-object v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isShowingSnapshot()Z
    .locals 2

    .prologue
    .line 2987
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getUrl()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v0, v1}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    .line 2988
    const-string/jumbo v1, "file://"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string/jumbo v1, ".mht"

    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected isSpdyProxyEnabledForUrl(Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 2694
    invoke-static {}, Lorg/chromium/chrome/browser/net/spdyproxy/DataReductionProxySettings;->getInstance()Lorg/chromium/chrome/browser/net/spdyproxy/DataReductionProxySettings;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/net/spdyproxy/DataReductionProxySettings;->isDataReductionProxyEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {p1, v0}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "https://"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->isIncognito()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2697
    const/4 v0, 0x1

    .line 2699
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isTitleDirectionRtl()Z
    .locals 1

    .prologue
    .line 2903
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mIsTitleDirectionRtl:Z

    return v0
.end method

.method public loadIfNeeded()Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 2160
    iget-object v1, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mActivity:Lcom/google/android/apps/chrome/ChromeActivity;

    if-nez v1, :cond_0

    .line 2161
    const-string/jumbo v0, "ChromeTab"

    const-string/jumbo v1, "Tab couldn\'t be loaded because Activity was null."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2162
    const/4 v0, 0x0

    .line 2174
    :goto_0
    return v0

    .line 2165
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mPendingLoadParams:Lorg/chromium/content_public/browser/LoadUrlParams;

    if-eqz v1, :cond_2

    .line 2166
    sget-boolean v1, Lcom/google/android/apps/chrome/tab/ChromeTab;->$assertionsDisabled:Z

    if-nez v1, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->isFrozen()Z

    move-result v1

    if-nez v1, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 2167
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->isIncognito()Z

    move-result v1

    iget-boolean v2, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mIsHidden:Z

    invoke-static {v1, v2}, Lorg/chromium/chrome/browser/ContentViewUtil;->createNativeWebContents(ZZ)J

    move-result-wide v2

    invoke-virtual {p0, v2, v3}, Lcom/google/android/apps/chrome/tab/ChromeTab;->initContentViewCore(J)V

    .line 2168
    iget-object v1, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mPendingLoadParams:Lorg/chromium/content_public/browser/LoadUrlParams;

    invoke-virtual {p0, v1}, Lcom/google/android/apps/chrome/tab/ChromeTab;->loadUrl(Lorg/chromium/content_public/browser/LoadUrlParams;)I

    .line 2169
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mPendingLoadParams:Lorg/chromium/content_public/browser/LoadUrlParams;

    goto :goto_0

    .line 2173
    :cond_2
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->restoreIfNeeded()V

    goto :goto_0
.end method

.method public loadUrl(Lorg/chromium/content_public/browser/LoadUrlParams;)I
    .locals 4

    .prologue
    const/high16 v3, 0x2000000

    const/4 v2, 0x0

    .line 2747
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->removeSadTabIfPresent()V

    .line 2749
    invoke-static {}, Lorg/chromium/base/TraceEvent;->begin()V

    .line 2754
    iput-boolean v2, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mLastPageLoadHasSpdyProxyPassthroughHeaders:Z

    .line 2755
    invoke-virtual {p1}, Lorg/chromium/content_public/browser/LoadUrlParams;->getVerbatimHeaders()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "X-PSA-Client-Options: v=1,m=1\nCache-Control: no-cache"

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2756
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mLastPageLoadHasSpdyProxyPassthroughHeaders:Z

    .line 2761
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mIsNativePageCommitPending:Z

    if-nez v0, :cond_1

    .line 2762
    invoke-virtual {p1}, Lorg/chromium/content_public/browser/LoadUrlParams;->getUrl()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, v2}, Lcom/google/android/apps/chrome/tab/ChromeTab;->maybeShowNativePage(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mIsNativePageCommitPending:Z

    .line 2766
    :cond_1
    invoke-virtual {p1}, Lorg/chromium/content_public/browser/LoadUrlParams;->getTransitionType()I

    move-result v0

    and-int/2addr v0, v3

    if-ne v0, v3, :cond_2

    .line 2768
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mAppAssociatedWith:Ljava/lang/String;

    .line 2771
    :cond_2
    invoke-super {p0, p1}, Lorg/chromium/chrome/browser/Tab;->loadUrl(Lorg/chromium/content_public/browser/LoadUrlParams;)I

    move-result v0

    .line 2772
    invoke-static {}, Lorg/chromium/base/TraceEvent;->end()V

    .line 2774
    return v0
.end method

.method protected notifyTabCrashed(Z)V
    .locals 3

    .prologue
    .line 2438
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 2439
    const-string/jumbo v1, "tabId"

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getId()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 2440
    const-string/jumbo v1, "sadTabShown"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2441
    iget-object v1, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mActivity:Lcom/google/android/apps/chrome/ChromeActivity;

    const/4 v2, 0x6

    invoke-static {v1, v2, v0}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->broadcastNotification(Landroid/content/Context;ILandroid/os/Bundle;)V

    .line 2442
    return-void
.end method

.method public onActivityStart()V
    .locals 1

    .prologue
    .line 1983
    sget-object v0, Lorg/chromium/chrome/browser/tabmodel/TabModel$TabSelectionType;->FROM_USER:Lorg/chromium/chrome/browser/tabmodel/TabModel$TabSelectionType;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->show(Lorg/chromium/chrome/browser/tabmodel/TabModel$TabSelectionType;)V

    .line 1988
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->updateFullscreenEnabledState()V

    .line 1989
    return-void
.end method

.method public onActivityStop()V
    .locals 0

    .prologue
    .line 1995
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->hide()V

    .line 1996
    return-void
.end method

.method public onChildViewAdded(Landroid/view/View;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 1824
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mFullscreenManager:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;

    if-eqz v0, :cond_0

    .line 1825
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mFullscreenManager:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->updateContentViewChildrenState()V

    .line 1827
    :cond_0
    return-void
.end method

.method public onChildViewRemoved(Landroid/view/View;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 1817
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mFullscreenManager:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;

    if-eqz v0, :cond_0

    .line 1818
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mFullscreenManager:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->updateContentViewChildrenState()V

    .line 1820
    :cond_0
    return-void
.end method

.method public onFirstSearch()V
    .locals 1

    .prologue
    .line 2820
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/chrome/RevenueStats;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/chrome/RevenueStats;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/RevenueStats;->onFirstSearch()V

    .line 2821
    return-void
.end method

.method public onNavEntryChanged()V
    .locals 1

    .prologue
    .line 2468
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mIsTabStateDirty:Z

    .line 2469
    return-void
.end method

.method protected onOffsetsChanged(FFF)V
    .locals 1

    .prologue
    .line 1563
    iput p1, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mPreviousFullscreenTopControlsOffsetY:F

    .line 1564
    iput p2, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mPreviousFullscreenContentOffsetY:F

    .line 1565
    iput p3, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mPreviousFullscreenOverdrawBottomHeight:F

    .line 1567
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mFullscreenManager:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;

    if-nez v0, :cond_0

    .line 1574
    :goto_0
    return-void

    .line 1568
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->isShowingSadTab()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->isNativePage()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1569
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mFullscreenManager:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->setPositionsForTabToNonFullscreen()V

    .line 1573
    :goto_1
    invoke-static {}, Lorg/chromium/chrome/browser/tabmodel/TabModelBase;->setActualTabSwitchLatencyMetricRequired()V

    goto :goto_0

    .line 1571
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mFullscreenManager:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->setPositionsForTab(FF)V

    goto :goto_1
.end method

.method protected onOverrideUrlLoading()V
    .locals 4

    .prologue
    .line 2892
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getContentViewCore()Lorg/chromium/content/browser/ContentViewCore;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getContentViewCore()Lorg/chromium/content/browser/ContentViewCore;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/content/browser/ContentViewCore;->getWebContents()Lorg/chromium/content_public/browser/WebContents;

    move-result-object v0

    invoke-interface {v0}, Lorg/chromium/content_public/browser/WebContents;->getNavigationController()Lorg/chromium/content_public/browser/NavigationController;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lorg/chromium/content_public/browser/NavigationController;->canGoToOffset(I)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mCreationTime:J

    iget-object v2, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mActivity:Lcom/google/android/apps/chrome/ChromeActivity;

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/ChromeActivity;->getLastUserInteractionTime()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-lez v0, :cond_1

    .line 2895
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getChromeWebContentsDelegateAndroid()Lorg/chromium/chrome/browser/Tab$TabChromeWebContentsDelegateAndroid;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/Tab$TabChromeWebContentsDelegateAndroid;->closeContents()V

    .line 2897
    :cond_1
    return-void
.end method

.method public onSystemUiVisibilityChange(I)V
    .locals 1

    .prologue
    .line 1831
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mFullscreenManager:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;

    if-eqz v0, :cond_0

    .line 1832
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mFullscreenManager:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->onContentViewSystemUiVisibilityChange(I)V

    .line 1834
    :cond_0
    return-void
.end method

.method public openSnapshotDocument(Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;)V
    .locals 2

    .prologue
    .line 2963
    if-nez p1, :cond_1

    .line 2980
    :cond_0
    :goto_0
    return-void

    .line 2965
    :cond_1
    invoke-static {p1}, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;->isPrintedDocument(Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2966
    invoke-static {p1}, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;->getSnapshotViewableState(Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;)Lcom/google/android/apps/chrome/snapshot/SnapshotViewableState;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/chrome/snapshot/SnapshotViewableState;->READY:Lcom/google/android/apps/chrome/snapshot/SnapshotViewableState;

    if-ne v0, v1, :cond_0

    .line 2968
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mActivity:Lcom/google/android/apps/chrome/ChromeActivity;

    invoke-static {p1, v0}, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;->openDownloadedFileAsNewTaskActivity(Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;Landroid/content/Context;)V

    goto :goto_0

    .line 2973
    :cond_2
    invoke-virtual {p1}, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->getPageUri()Landroid/net/Uri;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2974
    new-instance v0, Lorg/chromium/content_public/browser/LoadUrlParams;

    invoke-virtual {p1}, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->getPageUri()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/chromium/content_public/browser/LoadUrlParams;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->loadUrl(Lorg/chromium/content_public/browser/LoadUrlParams;)I

    .line 2975
    invoke-virtual {p1}, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->getSnapshotId()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2976
    invoke-virtual {p1}, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->getSnapshotId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->setSnapshotId(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public processEnableFullscreenRunnableForTest()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 1581
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1582
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 1583
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->enableFullscreenAfterLoad()V

    .line 1585
    :cond_0
    return-void
.end method

.method public reload()V
    .locals 1

    .prologue
    .line 2735
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mBackgroundContentViewHelper:Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->recordReload()V

    .line 2736
    invoke-super {p0}, Lorg/chromium/chrome/browser/Tab;->reload()V

    .line 2737
    return-void
.end method

.method public reloadIgnoringCache()V
    .locals 1

    .prologue
    .line 2741
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mBackgroundContentViewHelper:Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->recordReload()V

    .line 2742
    invoke-super {p0}, Lorg/chromium/chrome/browser/Tab;->reloadIgnoringCache()V

    .line 2743
    return-void
.end method

.method public requestFindMatchRects(I)V
    .locals 4

    .prologue
    .line 3086
    sget-boolean v0, Lcom/google/android/apps/chrome/tab/ChromeTab;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-wide v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mNativeChromeTab:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 3087
    :cond_0
    iget-wide v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mNativeChromeTab:J

    invoke-direct {p0, v0, v1, p1}, Lcom/google/android/apps/chrome/tab/ChromeTab;->nativeRequestFindMatchRects(JI)V

    .line 3088
    return-void
.end method

.method public requestFocus()V
    .locals 1

    .prologue
    .line 2952
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getView()Landroid/view/View;

    move-result-object v0

    .line 2953
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    .line 2954
    :cond_0
    return-void
.end method

.method public setClosing(Z)V
    .locals 1

    .prologue
    .line 3111
    if-eqz p1, :cond_0

    .line 3112
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mBackgroundContentViewHelper:Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->recordTabClose()V

    .line 3113
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->cancelThumbnailCapture()V

    .line 3115
    :cond_0
    invoke-super {p0, p1}, Lorg/chromium/chrome/browser/Tab;->setClosing(Z)V

    .line 3116
    return-void
.end method

.method protected setContentViewCore(Lorg/chromium/content/browser/ContentViewCore;)V
    .locals 3

    .prologue
    .line 1742
    invoke-static {}, Lorg/chromium/base/TraceEvent;->begin()V

    .line 1743
    invoke-super {p0, p1}, Lorg/chromium/chrome/browser/Tab;->setContentViewCore(Lorg/chromium/content/browser/ContentViewCore;)V

    .line 1744
    invoke-virtual {p1}, Lorg/chromium/content/browser/ContentViewCore;->getWebContents()Lorg/chromium/content_public/browser/WebContents;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->createWebContentsObserverAndroid(Lorg/chromium/content_public/browser/WebContents;)Lorg/chromium/content/browser/WebContentsObserverAndroid;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mWebContentsObserver:Lorg/chromium/content/browser/WebContentsObserverAndroid;

    .line 1745
    invoke-virtual {p1}, Lorg/chromium/content/browser/ContentViewCore;->getContainerView()Landroid/view/ViewGroup;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/ViewGroup;->setOnSystemUiVisibilityChangeListener(Landroid/view/View$OnSystemUiVisibilityChangeListener;)V

    .line 1747
    invoke-virtual {p1}, Lorg/chromium/content/browser/ContentViewCore;->getContainerView()Landroid/view/ViewGroup;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/ViewGroup;->setOnHierarchyChangeListener(Landroid/view/ViewGroup$OnHierarchyChangeListener;)V

    .line 1748
    new-instance v0, Lcom/google/android/apps/chrome/tab/ChromeDownloadListener;

    iget-object v1, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mActivity:Lcom/google/android/apps/chrome/ChromeActivity;

    iget-object v2, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mActivity:Lcom/google/android/apps/chrome/ChromeActivity;

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/ChromeActivity;->getTabModelSelector()Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

    move-result-object v2

    invoke-direct {v0, v1, v2, p0}, Lcom/google/android/apps/chrome/tab/ChromeDownloadListener;-><init>(Landroid/content/Context;Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;Lcom/google/android/apps/chrome/tab/ChromeTab;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mDownloadListener:Lcom/google/android/apps/chrome/tab/ChromeDownloadListener;

    .line 1750
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mDownloadListener:Lcom/google/android/apps/chrome/tab/ChromeDownloadListener;

    invoke-virtual {p1, v0}, Lorg/chromium/content/browser/ContentViewCore;->setDownloadDelegate(Lorg/chromium/content/browser/ContentViewDownloadDelegate;)V

    .line 1751
    new-instance v0, Lcom/google/android/apps/chrome/tab/ChromeTab$InterceptNavigationDelegateImpl;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/chrome/tab/ChromeTab$InterceptNavigationDelegateImpl;-><init>(Lcom/google/android/apps/chrome/tab/ChromeTab;Lcom/google/android/apps/chrome/tab/ChromeTab$1;)V

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->setInterceptNavigationDelegate(Lcom/google/android/apps/chrome/tab/ChromeTab$InterceptNavigationDelegateImpl;)V

    .line 1753
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mGestureStateListener:Lorg/chromium/content_public/browser/GestureStateListener;

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->createGestureStateListener()Lorg/chromium/content_public/browser/GestureStateListener;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mGestureStateListener:Lorg/chromium/content_public/browser/GestureStateListener;

    .line 1754
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mGestureStateListener:Lorg/chromium/content_public/browser/GestureStateListener;

    invoke-virtual {p1, v0}, Lorg/chromium/content/browser/ContentViewCore;->addGestureStateListener(Lorg/chromium/content_public/browser/GestureStateListener;)V

    .line 1755
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/tab/ChromeTab;->addContextualSearchListener(Lorg/chromium/content/browser/ContentViewCore;)V

    .line 1757
    invoke-static {}, Lorg/chromium/base/TraceEvent;->end()V

    .line 1758
    return-void
.end method

.method public setFindMatchRectsListener(Lcom/google/android/apps/chrome/tab/ChromeTab$FindMatchRectsListener;)V
    .locals 0

    .prologue
    .line 3106
    iput-object p1, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mFindMatchRectsListener:Lcom/google/android/apps/chrome/tab/ChromeTab$FindMatchRectsListener;

    .line 3107
    return-void
.end method

.method public setFindResultListener(Lcom/google/android/apps/chrome/tab/ChromeTab$FindResultListener;)V
    .locals 0

    .prologue
    .line 3101
    iput-object p1, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mFindResultListener:Lcom/google/android/apps/chrome/tab/ChromeTab$FindResultListener;

    .line 3102
    return-void
.end method

.method public setFullscreenManager(Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;)V
    .locals 3

    .prologue
    .line 2782
    iput-object p1, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mFullscreenManager:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;

    .line 2783
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mFullscreenManager:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;

    if-eqz v0, :cond_1

    .line 2784
    iget v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mPreviousFullscreenTopControlsOffsetY:F

    invoke-static {v0}, Ljava/lang/Float;->isNaN(F)Z

    move-result v0

    if-nez v0, :cond_0

    iget v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mPreviousFullscreenContentOffsetY:F

    invoke-static {v0}, Ljava/lang/Float;->isNaN(F)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2786
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mFullscreenManager:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->setPositionsForTabToNonFullscreen()V

    .line 2791
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mFullscreenManager:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->showControlsTransient()V

    .line 2792
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->updateFullscreenEnabledState()V

    .line 2794
    :cond_1
    return-void

    .line 2788
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mFullscreenManager:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;

    iget v1, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mPreviousFullscreenTopControlsOffsetY:F

    iget v2, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mPreviousFullscreenContentOffsetY:F

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->setPositionsForTab(FF)V

    goto :goto_0
.end method

.method public setShouldPreserve(Z)V
    .locals 0

    .prologue
    .line 2931
    iput-boolean p1, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mShouldPreserve:Z

    .line 2932
    return-void
.end method

.method public setSnapshotId(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 2302
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mSnapshotHandlingFinished:Z

    if-eqz v0, :cond_0

    .line 2309
    :goto_0
    return-void

    .line 2303
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mSnapshotHandlingFinished:Z

    .line 2304
    iput-object p1, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mSnapshotId:Ljava/lang/String;

    .line 2306
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mActivity:Lcom/google/android/apps/chrome/ChromeActivity;

    invoke-static {v0, p1}, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;->createQueryStateIntent(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 2308
    iget-object v1, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mActivity:Lcom/google/android/apps/chrome/ChromeActivity;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/chrome/ChromeActivity;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    goto :goto_0
.end method

.method public setViewClientForTesting(Lorg/chromium/content/browser/ContentViewClient;)V
    .locals 0

    .prologue
    .line 1166
    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/tab/ChromeTab;->setContentViewClient(Lorg/chromium/content/browser/ContentViewClient;)V

    .line 1167
    return-void
.end method

.method public setWindowSessionId(I)V
    .locals 2

    .prologue
    .line 3120
    iget-wide v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mNativeChromeTab:J

    invoke-direct {p0, v0, v1, p1}, Lcom/google/android/apps/chrome/tab/ChromeTab;->nativeSetWindowSessionId(JI)V

    .line 3121
    return-void
.end method

.method protected shouldInterceptContextMenuDownload(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 1027
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mDownloadListener:Lcom/google/android/apps/chrome/tab/ChromeDownloadListener;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/tab/ChromeDownloadListener;->shouldInterceptContextMenuDownload(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public shouldPreserve()Z
    .locals 1

    .prologue
    .line 2923
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mShouldPreserve:Z

    return v0
.end method

.method public shouldStall()Z
    .locals 2

    .prologue
    .line 1874
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->isFrozen()Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mNeedsReload:Z

    if-eqz v0, :cond_1

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getUrl()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->isIncognito()Z

    move-result v1

    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/tab/NativePageFactory;->isNativePageUrl(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public shouldTabStateBePersisted()Z
    .locals 1

    .prologue
    .line 1724
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mIsTabStateDirty:Z

    return v0
.end method

.method public show(Lorg/chromium/chrome/browser/tabmodel/TabModel$TabSelectionType;)V
    .locals 4

    .prologue
    .line 2117
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mIsHidden:Z

    if-nez v0, :cond_0

    .line 2148
    :goto_0
    return-void

    .line 2118
    :cond_0
    invoke-static {}, Lorg/chromium/base/TraceEvent;->begin()V

    .line 2121
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mIsHidden:Z

    .line 2122
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->loadIfNeeded()Z

    .line 2123
    sget-boolean v0, Lcom/google/android/apps/chrome/tab/ChromeTab;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->isFrozen()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 2125
    :cond_1
    invoke-super {p0}, Lorg/chromium/chrome/browser/Tab;->show()V

    .line 2129
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getNativePage()Lorg/chromium/chrome/browser/NativePage;

    move-result-object v0

    instance-of v0, v0, Lorg/chromium/chrome/browser/FrozenNativePage;

    if-eqz v0, :cond_2

    .line 2130
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getUrl()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/chrome/tab/ChromeTab;->maybeShowNativePage(Ljava/lang/String;Z)Z

    .line 2132
    :cond_2
    invoke-static {}, Lcom/google/android/apps/chrome/ntp/NativePageAssassin;->getInstance()Lcom/google/android/apps/chrome/ntp/NativePageAssassin;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/apps/chrome/ntp/NativePageAssassin;->tabShown(Lorg/chromium/chrome/browser/Tab;)V

    .line 2136
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getProgress()I

    move-result v0

    const/16 v1, 0x64

    if-ge v0, v1, :cond_3

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->isShowingInterstitialPage()Z

    move-result v0

    if-nez v0, :cond_3

    .line 2137
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->notifyLoadProgress()V

    .line 2139
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mTabUma:Lcom/google/android/apps/chrome/tab/TabUma;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mTabUma:Lcom/google/android/apps/chrome/tab/TabUma;

    iget-wide v2, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mTimestampMillis:J

    invoke-virtual {v0, p1, v2, v3}, Lcom/google/android/apps/chrome/tab/TabUma;->onShow(Lorg/chromium/chrome/browser/tabmodel/TabModel$TabSelectionType;J)V

    .line 2140
    :cond_4
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mTimestampMillis:J

    .line 2141
    invoke-static {}, Lorg/chromium/base/TraceEvent;->end()V

    .line 2147
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->rescheduleThumbnailCapture()V

    goto :goto_0
.end method

.method protected showRenderedPage()V
    .locals 0

    .prologue
    .line 1467
    invoke-super {p0}, Lorg/chromium/chrome/browser/Tab;->showRenderedPage()V

    .line 1468
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->updateTitle()Z

    .line 1469
    return-void
.end method

.method simulateRendererKilledForTesting()V
    .locals 2

    .prologue
    .line 2212
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mWebContentsObserver:Lorg/chromium/content/browser/WebContentsObserverAndroid;

    if-eqz v0, :cond_0

    .line 2213
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mWebContentsObserver:Lorg/chromium/content/browser/WebContentsObserverAndroid;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lorg/chromium/content/browser/WebContentsObserverAndroid;->renderProcessGone(Z)V

    .line 2215
    :cond_0
    return-void
.end method

.method public snapshotStateQueryResult(Ljava/lang/String;Landroid/net/Uri;Lcom/google/android/apps/chrome/snapshot/SnapshotViewableState;)Z
    .locals 2

    .prologue
    .line 2321
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mSnapshotId:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mSnapshotId:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2322
    :cond_0
    const/4 v0, 0x0

    .line 2338
    :goto_0
    return v0

    .line 2324
    :cond_1
    sget-object v0, Lcom/google/android/apps/chrome/tab/ChromeTab$12;->$SwitchMap$com$google$android$apps$chrome$snapshot$SnapshotViewableState:[I

    invoke-virtual {p3}, Lcom/google/android/apps/chrome/snapshot/SnapshotViewableState;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2338
    :goto_1
    const/4 v0, 0x1

    goto :goto_0

    .line 2326
    :pswitch_0
    invoke-direct {p0, p2}, Lcom/google/android/apps/chrome/tab/ChromeTab;->snapshotStateReady(Landroid/net/Uri;)V

    goto :goto_1

    .line 2329
    :pswitch_1
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->snapshotStateDownloading()V

    goto :goto_1

    .line 2332
    :pswitch_2
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->snapshotStateError()V

    goto :goto_1

    .line 2335
    :pswitch_3
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->snapshotStateNeverReady()V

    goto :goto_1

    .line 2324
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public startFinding(Ljava/lang/String;ZZ)V
    .locals 7

    .prologue
    .line 3068
    sget-boolean v0, Lcom/google/android/apps/chrome/tab/ChromeTab;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-wide v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mNativeChromeTab:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 3069
    :cond_0
    iget-wide v2, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mNativeChromeTab:J

    move-object v1, p0

    move-object v4, p1

    move v5, p2

    move v6, p3

    invoke-direct/range {v1 .. v6}, Lcom/google/android/apps/chrome/tab/ChromeTab;->nativeStartFinding(JLjava/lang/String;ZZ)V

    .line 3070
    return-void
.end method

.method public stopFinding()V
    .locals 4

    .prologue
    .line 3074
    sget-boolean v0, Lcom/google/android/apps/chrome/tab/ChromeTab;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-wide v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mNativeChromeTab:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 3075
    :cond_0
    iget-wide v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mNativeChromeTab:J

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/chrome/tab/ChromeTab;->nativeStopFinding(J)V

    .line 3076
    return-void
.end method

.method public stopLoading()V
    .locals 1

    .prologue
    .line 1838
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->isLoading()Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x9

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->notifyPageLoad(I)V

    .line 1839
    :cond_0
    invoke-super {p0}, Lorg/chromium/chrome/browser/Tab;->stopLoading()V

    .line 1840
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mBackgroundContentViewHelper:Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->stopLoading()V

    .line 1841
    return-void
.end method

.method public supportsFinding()Z
    .locals 1

    .prologue
    .line 3059
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getContentViewCore()Lorg/chromium/content/browser/ContentViewCore;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public tabStateWasPersisted()V
    .locals 1

    .prologue
    .line 1732
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mIsTabStateDirty:Z

    .line 1733
    return-void
.end method

.method public triggerSearchByImage()V
    .locals 4

    .prologue
    .line 2475
    iget-wide v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mNativeChromeTab:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 2476
    iget-wide v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mNativeChromeTab:J

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/chrome/tab/ChromeTab;->nativeSearchByImageInNewTabAsync(J)V

    .line 2478
    :cond_0
    return-void
.end method

.method public unfreezeContents()Z
    .locals 8

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 1929
    invoke-static {}, Lorg/chromium/base/TraceEvent;->begin()V

    .line 1930
    sget-boolean v0, Lcom/google/android/apps/chrome/tab/ChromeTab;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mFrozenContentsState:Lcom/google/android/apps/chrome/tab/ChromeTab$ContentsState;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 1931
    :cond_0
    sget-boolean v0, Lcom/google/android/apps/chrome/tab/ChromeTab;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getContentViewCore()Lorg/chromium/content/browser/ContentViewCore;

    move-result-object v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 1934
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mFrozenContentsState:Lcom/google/android/apps/chrome/tab/ChromeTab$ContentsState;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tab/ChromeTab$ContentsState;->buffer()Ljava/nio/ByteBuffer;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mFrozenContentsState:Lcom/google/android/apps/chrome/tab/ChromeTab$ContentsState;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/tab/ChromeTab$ContentsState;->version()I

    move-result v1

    iget-boolean v2, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mIsHidden:Z

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/chrome/tab/ChromeTab;->nativeRestoreContentsFromByteBuffer(Ljava/nio/ByteBuffer;IZ)J

    move-result-wide v0

    .line 1936
    const-wide/16 v6, 0x0

    cmp-long v2, v0, v6

    if-nez v2, :cond_5

    .line 1940
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->isIncognito()Z

    move-result v0

    iget-boolean v1, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mIsHidden:Z

    invoke-static {v0, v1}, Lorg/chromium/chrome/browser/ContentViewUtil;->createNativeWebContents(ZZ)J

    move-result-wide v0

    move v2, v3

    .line 1944
    :goto_0
    const/4 v5, 0x0

    iput-object v5, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mFrozenContentsState:Lcom/google/android/apps/chrome/tab/ChromeTab$ContentsState;

    .line 1945
    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/chrome/tab/ChromeTab;->initContentViewCore(J)V

    .line 1947
    if-eqz v2, :cond_2

    .line 1948
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mUrl:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    const-string/jumbo v0, "chrome-native://newtab/"

    .line 1949
    :goto_1
    new-instance v1, Lorg/chromium/content_public/browser/LoadUrlParams;

    const/4 v5, 0x5

    invoke-direct {v1, v0, v5}, Lorg/chromium/content_public/browser/LoadUrlParams;-><init>(Ljava/lang/String;I)V

    invoke-virtual {p0, v1}, Lcom/google/android/apps/chrome/tab/ChromeTab;->loadUrl(Lorg/chromium/content_public/browser/LoadUrlParams;)I

    .line 1951
    :cond_2
    invoke-static {}, Lorg/chromium/base/TraceEvent;->end()V

    .line 1953
    if-nez v2, :cond_4

    :goto_2
    return v3

    .line 1948
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mUrl:Ljava/lang/String;

    goto :goto_1

    :cond_4
    move v3, v4

    .line 1953
    goto :goto_2

    :cond_5
    move v2, v4

    goto :goto_0
.end method

.method public updateFullscreenEnabledState()V
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 1598
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->isFrozen()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mFullscreenManager:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;

    if-nez v1, :cond_1

    .line 1606
    :cond_0
    :goto_0
    return-void

    .line 1600
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getTopControlsStateConstraints()I

    move-result v1

    const/4 v2, 0x3

    invoke-virtual {p0, v1, v2, v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->updateTopControlsState(IIZ)V

    .line 1602
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getContentViewCore()Lorg/chromium/content/browser/ContentViewCore;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1603
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getContentViewCore()Lorg/chromium/content/browser/ContentViewCore;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mFullscreenManager:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->getPersistentFullscreenMode()Z

    move-result v2

    if-nez v2, :cond_2

    :goto_1
    invoke-virtual {v1, v0}, Lorg/chromium/content/browser/ContentViewCore;->updateMultiTouchZoomSupport(Z)V

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method protected updateTopControlsState(IIZ)V
    .locals 7

    .prologue
    .line 1621
    iget-wide v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mNativeChromeTab:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 1623
    :goto_0
    return-void

    .line 1622
    :cond_0
    iget-wide v2, p0, Lcom/google/android/apps/chrome/tab/ChromeTab;->mNativeChromeTab:J

    move-object v1, p0

    move v4, p1

    move v5, p2

    move v6, p3

    invoke-direct/range {v1 .. v6}, Lcom/google/android/apps/chrome/tab/ChromeTab;->nativeUpdateTopControlsState(JIIZ)V

    goto :goto_0
.end method

.method public updateTopControlsState(IZ)V
    .locals 3

    .prologue
    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 1636
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getTopControlsStateConstraints()I

    move-result v0

    .line 1639
    if-ne v0, v2, :cond_0

    if-eq p1, v1, :cond_1

    :cond_0
    if-ne v0, v1, :cond_2

    if-ne p1, v2, :cond_2

    .line 1644
    :cond_1
    :goto_0
    return-void

    .line 1643
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getTopControlsStateConstraints()I

    move-result v0

    invoke-virtual {p0, v0, p1, p2}, Lcom/google/android/apps/chrome/tab/ChromeTab;->updateTopControlsState(IIZ)V

    goto :goto_0
.end method
