.class public Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;
.super Ljava/lang/Object;
.source "ToolbarDelegate.java"


# static fields
.field protected static final BACKGROUND_TRANSITION_DURATION_MS:I = 0x190


# instance fields
.field private mAppMenuButtonHelper:Lorg/chromium/chrome/browser/appmenu/AppMenuButtonHelper;

.field private mDefaultActionModeCallback:Lcom/google/android/apps/chrome/CustomSelectionActionModeCallback;

.field private mFirstDrawTimeMs:J

.field private mInvalidator:Lcom/google/android/apps/chrome/compositor/Invalidator;

.field protected mLocationBar:Lcom/google/android/apps/chrome/omnibox/LocationBar;

.field protected mMenuButton:Landroid/widget/ImageButton;

.field private mNativeLibraryReady:Z

.field private final mTempPosition:[I

.field private mToolbarDataProvider:Lcom/google/android/apps/chrome/toolbar/ToolbarDataProvider;

.field private mToolbarTabController:Lcom/google/android/apps/chrome/toolbar/ToolbarTabController;

.field private final mToolbarView:Landroid/view/View;

.field private mUrlHasFocus:Z


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    const/4 v0, 0x2

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;->mTempPosition:[I

    .line 53
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;->mDefaultActionModeCallback:Lcom/google/android/apps/chrome/CustomSelectionActionModeCallback;

    .line 62
    iput-object p1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;->mToolbarView:Landroid/view/View;

    .line 64
    sget v0, Lcom/google/android/apps/chrome/R$id;->menu_button:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;->mMenuButton:Landroid/widget/ImageButton;

    .line 65
    sget v0, Lcom/google/android/apps/chrome/R$id;->location_bar:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/omnibox/LocationBar;

    iput-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;->mLocationBar:Lcom/google/android/apps/chrome/omnibox/LocationBar;

    .line 68
    new-instance v0, Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate$1;-><init>(Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;->mToolbarDataProvider:Lcom/google/android/apps/chrome/toolbar/ToolbarDataProvider;

    .line 114
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;)Lorg/chromium/chrome/browser/appmenu/AppMenuButtonHelper;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;->mAppMenuButtonHelper:Lorg/chromium/chrome/browser/appmenu/AppMenuButtonHelper;

    return-object v0
.end method


# virtual methods
.method protected back()Z
    .locals 1

    .prologue
    .line 491
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;->mLocationBar:Lcom/google/android/apps/chrome/omnibox/LocationBar;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->hideSuggestions()V

    .line 492
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;->mToolbarTabController:Lcom/google/android/apps/chrome/toolbar/ToolbarTabController;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;->mToolbarTabController:Lcom/google/android/apps/chrome/toolbar/ToolbarTabController;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarTabController;->back()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public finishAnimations()V
    .locals 0

    .prologue
    .line 459
    return-void
.end method

.method protected forward()Z
    .locals 1

    .prologue
    .line 500
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;->mLocationBar:Lcom/google/android/apps/chrome/omnibox/LocationBar;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->hideSuggestions()V

    .line 501
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;->mToolbarTabController:Lcom/google/android/apps/chrome/toolbar/ToolbarTabController;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;->mToolbarTabController:Lcom/google/android/apps/chrome/toolbar/ToolbarTabController;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarTabController;->forward()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getCurrentView()Landroid/view/View;
    .locals 1

    .prologue
    .line 465
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;->mToolbarDataProvider:Lcom/google/android/apps/chrome/toolbar/ToolbarDataProvider;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarDataProvider;->getTab()Lorg/chromium/chrome/browser/Tab;

    move-result-object v0

    .line 466
    if-eqz v0, :cond_0

    .line 467
    invoke-virtual {v0}, Lorg/chromium/chrome/browser/Tab;->getView()Landroid/view/View;

    move-result-object v0

    .line 469
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getDefaultActionModeCallbackForTextEdit()Lcom/google/android/apps/chrome/CustomSelectionActionModeCallback;
    .locals 1

    .prologue
    .line 417
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;->mDefaultActionModeCallback:Lcom/google/android/apps/chrome/CustomSelectionActionModeCallback;

    return-object v0
.end method

.method public getFirstDrawTime()J
    .locals 2

    .prologue
    .line 433
    iget-wide v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;->mFirstDrawTimeMs:J

    return-wide v0
.end method

.method public getLocationBar()Lcom/google/android/apps/chrome/omnibox/LocationBar;
    .locals 1

    .prologue
    .line 483
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;->mLocationBar:Lcom/google/android/apps/chrome/omnibox/LocationBar;

    return-object v0
.end method

.method public getLocationBarContentRect(Landroid/graphics/Rect;)V
    .locals 3

    .prologue
    .line 369
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;->mLocationBar:Lcom/google/android/apps/chrome/omnibox/LocationBar;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->getContentRect(Landroid/graphics/Rect;)V

    .line 370
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;->mToolbarView:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;->mLocationBar:Lcom/google/android/apps/chrome/omnibox/LocationBar;

    iget-object v2, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;->mTempPosition:[I

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/chrome/utilities/ViewUtils;->getRelativeDrawPosition(Landroid/view/View;Landroid/view/View;[I)V

    .line 371
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;->mTempPosition:[I

    const/4 v1, 0x0

    aget v0, v0, v1

    iget-object v1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;->mTempPosition:[I

    const/4 v2, 0x1

    aget v1, v1, v2

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Rect;->offset(II)V

    .line 372
    return-void
.end method

.method protected getMenuButton()Landroid/view/View;
    .locals 1

    .prologue
    .line 150
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;->mMenuButton:Landroid/widget/ImageButton;

    return-object v0
.end method

.method public getMenuButtonHelper()Lorg/chromium/chrome/browser/appmenu/AppMenuButtonHelper;
    .locals 1

    .prologue
    .line 157
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;->mAppMenuButtonHelper:Lorg/chromium/chrome/browser/appmenu/AppMenuButtonHelper;

    return-object v0
.end method

.method protected getToolbarDataProvider()Lcom/google/android/apps/chrome/toolbar/ToolbarDataProvider;
    .locals 1

    .prologue
    .line 178
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;->mToolbarDataProvider:Lcom/google/android/apps/chrome/toolbar/ToolbarDataProvider;

    return-object v0
.end method

.method protected getToolbarView()Landroid/view/View;
    .locals 1

    .prologue
    .line 164
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;->mToolbarView:Landroid/view/View;

    return-object v0
.end method

.method protected handleFindToolbarStateChange(Z)V
    .locals 0

    .prologue
    .line 270
    return-void
.end method

.method public initialize(Lcom/google/android/apps/chrome/toolbar/ToolbarDataProvider;Lcom/google/android/apps/chrome/toolbar/ToolbarTabController;Lorg/chromium/chrome/browser/appmenu/AppMenuButtonHelper;)V
    .locals 2

    .prologue
    .line 125
    iput-object p1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;->mToolbarDataProvider:Lcom/google/android/apps/chrome/toolbar/ToolbarDataProvider;

    .line 126
    iput-object p2, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;->mToolbarTabController:Lcom/google/android/apps/chrome/toolbar/ToolbarTabController;

    .line 128
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;->mMenuButton:Landroid/widget/ImageButton;

    new-instance v1, Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate$2;

    invoke-direct {v1, p0}, Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate$2;-><init>(Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 134
    iput-object p3, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;->mAppMenuButtonHelper:Lorg/chromium/chrome/browser/appmenu/AppMenuButtonHelper;

    .line 135
    return-void
.end method

.method public isAnimatingForOverview()Z
    .locals 1

    .prologue
    .line 333
    const/4 v0, 0x0

    return v0
.end method

.method public isIncognito()Z
    .locals 1

    .prologue
    .line 476
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;->mToolbarDataProvider:Lcom/google/android/apps/chrome/toolbar/ToolbarDataProvider;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarDataProvider;->isIncognito()Z

    move-result v0

    return v0
.end method

.method protected isNativeLibraryReady()Z
    .locals 1

    .prologue
    .line 171
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;->mNativeLibraryReady:Z

    return v0
.end method

.method public isOverviewMode()Z
    .locals 1

    .prologue
    .line 326
    const/4 v0, 0x0

    return v0
.end method

.method protected onAccessibilityStatusChanged(Z)V
    .locals 0

    .prologue
    .line 276
    return-void
.end method

.method public onBookmarkUiVisibilityChange(Z)V
    .locals 0

    .prologue
    .line 263
    return-void
.end method

.method protected onDefaultSearchEngineChanged()V
    .locals 0

    .prologue
    .line 361
    return-void
.end method

.method protected onHomeButtonUpdate(Z)V
    .locals 0

    .prologue
    .line 289
    return-void
.end method

.method protected onNativeLibraryReady()V
    .locals 1

    .prologue
    .line 141
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;->mNativeLibraryReady:Z

    .line 143
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;->mLocationBar:Lcom/google/android/apps/chrome/omnibox/LocationBar;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->onNativeLibraryReady()V

    .line 144
    return-void
.end method

.method protected onNavigatedToDifferentPage()V
    .locals 0

    .prologue
    .line 447
    return-void
.end method

.method protected onOverviewTransitionFinished()V
    .locals 0

    .prologue
    .line 348
    return-void
.end method

.method protected onPrimaryColorChanged()V
    .locals 0

    .prologue
    .line 309
    return-void
.end method

.method protected onStateRestored()V
    .locals 0

    .prologue
    .line 282
    return-void
.end method

.method protected onTabContentViewChanged()V
    .locals 2

    .prologue
    .line 315
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;->getToolbarDataProvider()Lcom/google/android/apps/chrome/toolbar/ToolbarDataProvider;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarDataProvider;->getNewTabPageForCurrentTab()Lcom/google/android/apps/chrome/ntp/NewTabPage;

    move-result-object v0

    .line 316
    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;->mLocationBar:Lcom/google/android/apps/chrome/omnibox/LocationBar;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/ntp/NewTabPage;->setLocationBar(Lcom/google/android/apps/chrome/omnibox/LocationBar;)V

    .line 317
    :cond_0
    return-void
.end method

.method protected onTabOrModelChanged()V
    .locals 2

    .prologue
    .line 299
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;->getToolbarDataProvider()Lcom/google/android/apps/chrome/toolbar/ToolbarDataProvider;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarDataProvider;->getNewTabPageForCurrentTab()Lcom/google/android/apps/chrome/ntp/NewTabPage;

    move-result-object v0

    .line 300
    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;->mLocationBar:Lcom/google/android/apps/chrome/omnibox/LocationBar;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/ntp/NewTabPage;->setLocationBar(Lcom/google/android/apps/chrome/omnibox/LocationBar;)V

    .line 302
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;->mLocationBar:Lcom/google/android/apps/chrome/omnibox/LocationBar;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->updateMicButtonState()V

    .line 303
    return-void
.end method

.method protected onUrlFocusChange(Z)V
    .locals 0

    .prologue
    .line 394
    iput-boolean p1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;->mUrlHasFocus:Z

    .line 395
    return-void
.end method

.method protected openHomepage()V
    .locals 1

    .prologue
    .line 519
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;->mLocationBar:Lcom/google/android/apps/chrome/omnibox/LocationBar;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->hideSuggestions()V

    .line 520
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;->mToolbarTabController:Lcom/google/android/apps/chrome/toolbar/ToolbarTabController;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;->mToolbarTabController:Lcom/google/android/apps/chrome/toolbar/ToolbarTabController;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarTabController;->openHomepage()V

    .line 521
    :cond_0
    return-void
.end method

.method protected recordFirstDrawTime()V
    .locals 4

    .prologue
    .line 424
    iget-wide v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;->mFirstDrawTimeMs:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 425
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;->mFirstDrawTimeMs:J

    .line 427
    :cond_0
    return-void
.end method

.method public requestLayout()V
    .locals 1

    .prologue
    .line 440
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;->getToolbarView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->requestLayout()V

    .line 441
    return-void
.end method

.method public setBookmarkClickHandler(Landroid/view/View$OnClickListener;)V
    .locals 0

    .prologue
    .line 220
    return-void
.end method

.method public setDefaultActionModeCallbackForTextEdit(Lcom/google/android/apps/chrome/CustomSelectionActionModeCallback;)V
    .locals 1

    .prologue
    .line 409
    iput-object p1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;->mDefaultActionModeCallback:Lcom/google/android/apps/chrome/CustomSelectionActionModeCallback;

    .line 410
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;->mLocationBar:Lcom/google/android/apps/chrome/omnibox/LocationBar;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->getUrlBar()Lcom/google/android/apps/chrome/omnibox/UrlBar;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/omnibox/UrlBar;->setCustomSelectionActionModeCallback(Landroid/view/ActionMode$Callback;)V

    .line 411
    return-void
.end method

.method protected setLoadProgress(I)V
    .locals 0

    .prologue
    .line 454
    return-void
.end method

.method public setOnNewTabClickHandler(Landroid/view/View$OnClickListener;)V
    .locals 0

    .prologue
    .line 214
    return-void
.end method

.method public setOnOverviewClickHandler(Landroid/view/View$OnClickListener;)V
    .locals 0

    .prologue
    .line 208
    return-void
.end method

.method protected setOverviewMode(ZLandroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 342
    return-void
.end method

.method public setPaintInvalidator(Lcom/google/android/apps/chrome/compositor/Invalidator;)V
    .locals 0

    .prologue
    .line 188
    iput-object p1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;->mInvalidator:Lcom/google/android/apps/chrome/compositor/Invalidator;

    .line 189
    return-void
.end method

.method public setTextureCaptureMode(Z)V
    .locals 0

    .prologue
    .line 380
    return-void
.end method

.method protected shouldShowMenuButton()Z
    .locals 1

    .prologue
    .line 398
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;->mToolbarView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->hasPermanentMenuKey()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected stopOrReloadCurrentTab()V
    .locals 1

    .prologue
    .line 511
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;->mLocationBar:Lcom/google/android/apps/chrome/omnibox/LocationBar;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->hideSuggestions()V

    .line 512
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;->mToolbarTabController:Lcom/google/android/apps/chrome/toolbar/ToolbarTabController;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;->mToolbarTabController:Lcom/google/android/apps/chrome/toolbar/ToolbarTabController;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarTabController;->stopOrReloadCurrentTab()V

    .line 513
    :cond_0
    return-void
.end method

.method public triggerPaintInvalidate(Lcom/google/android/apps/chrome/compositor/Invalidator$Client;)V
    .locals 1

    .prologue
    .line 197
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;->mInvalidator:Lcom/google/android/apps/chrome/compositor/Invalidator;

    if-nez v0, :cond_0

    .line 198
    invoke-interface {p1}, Lcom/google/android/apps/chrome/compositor/Invalidator$Client;->doInvalidate()V

    .line 202
    :goto_0
    return-void

    .line 200
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;->mInvalidator:Lcom/google/android/apps/chrome/compositor/Invalidator;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/compositor/Invalidator;->invalidate(Lcom/google/android/apps/chrome/compositor/Invalidator$Client;)V

    goto :goto_0
.end method

.method protected updateBackButtonVisibility(Z)V
    .locals 0

    .prologue
    .line 227
    return-void
.end method

.method protected updateBookmarkButtonVisibility(Z)V
    .locals 0

    .prologue
    .line 256
    return-void
.end method

.method protected updateForwardButtonVisibility(Z)V
    .locals 0

    .prologue
    .line 234
    return-void
.end method

.method public updateReaderModeButton(ZZ)V
    .locals 0

    .prologue
    .line 249
    return-void
.end method

.method protected updateReloadButtonVisibility(Z)V
    .locals 0

    .prologue
    .line 241
    return-void
.end method

.method protected updateTabCountVisuals(I)V
    .locals 0

    .prologue
    .line 355
    return-void
.end method

.method public urlHasFocus()Z
    .locals 1

    .prologue
    .line 386
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;->mUrlHasFocus:Z

    return v0
.end method
