.class Lcom/google/android/apps/chrome/toolbar/ToolbarManager$LoadProgressSimulator$1;
.super Landroid/os/Handler;
.source "ToolbarManager.java"


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarManager$LoadProgressSimulator;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 705
    const-class v0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager$LoadProgressSimulator$1;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>(Lcom/google/android/apps/chrome/toolbar/ToolbarManager$LoadProgressSimulator;Landroid/os/Looper;)V
    .locals 0

    .prologue
    .line 705
    iput-object p1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager$LoadProgressSimulator$1;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarManager$LoadProgressSimulator;

    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 5

    .prologue
    const/16 v4, 0x64

    const/4 v3, 0x1

    .line 708
    sget-boolean v0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager$LoadProgressSimulator$1;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget v0, p1, Landroid/os/Message;->what:I

    if-eq v0, v3, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 709
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager$LoadProgressSimulator$1;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarManager$LoadProgressSimulator;

    iget-object v1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager$LoadProgressSimulator$1;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarManager$LoadProgressSimulator;

    const/16 v2, 0xa

    # += operator for: Lcom/google/android/apps/chrome/toolbar/ToolbarManager$LoadProgressSimulator;->mProgress:I
    invoke-static {v1, v2}, Lcom/google/android/apps/chrome/toolbar/ToolbarManager$LoadProgressSimulator;->access$2512(Lcom/google/android/apps/chrome/toolbar/ToolbarManager$LoadProgressSimulator;I)I

    move-result v1

    invoke-static {v4, v1}, Ljava/lang/Math;->min(II)I

    move-result v1

    # setter for: Lcom/google/android/apps/chrome/toolbar/ToolbarManager$LoadProgressSimulator;->mProgress:I
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/toolbar/ToolbarManager$LoadProgressSimulator;->access$2502(Lcom/google/android/apps/chrome/toolbar/ToolbarManager$LoadProgressSimulator;I)I

    .line 710
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager$LoadProgressSimulator$1;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarManager$LoadProgressSimulator;

    # getter for: Lcom/google/android/apps/chrome/toolbar/ToolbarManager$LoadProgressSimulator;->mToolbar:Lcom/google/android/apps/chrome/toolbar/ToolbarManager;
    invoke-static {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarManager$LoadProgressSimulator;->access$2600(Lcom/google/android/apps/chrome/toolbar/ToolbarManager$LoadProgressSimulator;)Lcom/google/android/apps/chrome/toolbar/ToolbarManager;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager$LoadProgressSimulator$1;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarManager$LoadProgressSimulator;

    # getter for: Lcom/google/android/apps/chrome/toolbar/ToolbarManager$LoadProgressSimulator;->mProgress:I
    invoke-static {v1}, Lcom/google/android/apps/chrome/toolbar/ToolbarManager$LoadProgressSimulator;->access$2500(Lcom/google/android/apps/chrome/toolbar/ToolbarManager$LoadProgressSimulator;)I

    move-result v1

    # invokes: Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->updateLoadProgressInternal(I)V
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->access$2700(Lcom/google/android/apps/chrome/toolbar/ToolbarManager;I)V

    .line 712
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager$LoadProgressSimulator$1;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarManager$LoadProgressSimulator;

    # getter for: Lcom/google/android/apps/chrome/toolbar/ToolbarManager$LoadProgressSimulator;->mProgress:I
    invoke-static {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarManager$LoadProgressSimulator;->access$2500(Lcom/google/android/apps/chrome/toolbar/ToolbarManager$LoadProgressSimulator;)I

    move-result v0

    if-lt v0, v4, :cond_1

    .line 714
    :goto_0
    return-void

    .line 713
    :cond_1
    const-wide/16 v0, 0xa

    invoke-virtual {p0, v3, v0, v1}, Lcom/google/android/apps/chrome/toolbar/ToolbarManager$LoadProgressSimulator$1;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0
.end method
