.class Lcom/google/android/apps/chrome/compositor/CompositorViewHolder$8;
.super Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;
.source "CompositorViewHolder.java"


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1059
    const-class v0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder$8;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>(Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;)V
    .locals 0

    .prologue
    .line 1059
    iput-object p1, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder$8;->this$0:Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;

    invoke-direct {p0}, Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 2

    .prologue
    .line 1062
    iget v0, p1, Landroid/os/Message;->what:I

    sparse-switch v0, :sswitch_data_0

    .line 1076
    sget-boolean v0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder$8;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 1064
    :sswitch_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder$8;->this$0:Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;

    # getter for: Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mFindToolbarShowing:Z
    invoke-static {v0}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->access$1200(Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1065
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder$8;->this$0:Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;

    const/4 v1, 0x0

    # setter for: Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mFindToolbarShowing:Z
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->access$1202(Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;Z)Z

    .line 1066
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder$8;->this$0:Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;

    iget-object v1, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder$8;->this$0:Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;

    # getter for: Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mFindToolbarShowing:Z
    invoke-static {v1}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->access$1200(Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;)Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->handleFindInPage(Z)V

    .line 1078
    :cond_0
    :goto_0
    return-void

    .line 1070
    :sswitch_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder$8;->this$0:Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;

    # getter for: Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mFindToolbarShowing:Z
    invoke-static {v0}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->access$1200(Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1071
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder$8;->this$0:Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;

    const/4 v1, 0x1

    # setter for: Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mFindToolbarShowing:Z
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->access$1202(Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;Z)Z

    .line 1072
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder$8;->this$0:Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;

    iget-object v1, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder$8;->this$0:Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;

    # getter for: Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mFindToolbarShowing:Z
    invoke-static {v1}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->access$1200(Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;)Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->handleFindInPage(Z)V

    goto :goto_0

    .line 1062
    nop

    :sswitch_data_0
    .sparse-switch
        0x2d -> :sswitch_0
        0x3b -> :sswitch_1
    .end sparse-switch
.end method
