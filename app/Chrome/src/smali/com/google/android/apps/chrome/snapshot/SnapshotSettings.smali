.class public final Lcom/google/android/apps/chrome/snapshot/SnapshotSettings;
.super Ljava/lang/Object;
.source "SnapshotSettings.java"


# static fields
.field static final APPLICATION_VERSION_KEY:Ljava/lang/String; = "chrome_to_mobile_application_version"

.field static final AUTH_TOKEN_KEY:Ljava/lang/String; = "chrome_to_mobile_authToken"

.field public static final CURRENT_VERSION:J = 0x3L

.field static final LAST_UPDATED_TIMESTAMP_KEY:Ljava/lang/String; = "chrome_to_mobile_last_updated_timestamp"

.field static final LEGACY_ENABLED_KEY:Ljava/lang/String; = "chrome_to_mobile_enabled"

.field static final NEEDS_UPDATING_KEY:Ljava/lang/String; = "chrome_to_mobile_needs_updating"

.field static final NEW_PROFILE_MANAGEMENT_ENABLED:Ljava/lang/String; = "chrome_to_mobile_new_profile_management_enabled"

.field static final OAUTH2_TOKEN_KEY:Ljava/lang/String; = "chrome_to_mobile_oAuth2Token"

.field static final PRINTER_ID_KEY:Ljava/lang/String; = "chrome_to_mobile_printerId"

.field static final VERSION_KEY:Ljava/lang/String; = "chrome_to_mobile_version"


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 161
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 162
    return-void
.end method

.method public static clearPrinterId(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 46
    invoke-static {p0}, Lcom/google/android/apps/chrome/snapshot/SnapshotSettings;->getPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string/jumbo v1, "chrome_to_mobile_printerId"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/chrome/snapshot/SnapshotSettings;->storeField(Landroid/content/SharedPreferences;Ljava/lang/String;Ljava/lang/String;)V

    .line 47
    return-void
.end method

.method public static getApplicationVersion(Landroid/content/Context;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 120
    invoke-static {p0}, Lcom/google/android/apps/chrome/snapshot/SnapshotSettings;->getPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string/jumbo v1, "chrome_to_mobile_application_version"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getAuthToken(Landroid/content/Context;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 75
    invoke-static {}, Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintContract;->isChromeToMobileOAuth2Enabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 76
    invoke-static {p0}, Lcom/google/android/apps/chrome/snapshot/SnapshotSettings;->getOAuth2Token(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 78
    :goto_0
    return-object v0

    :cond_0
    invoke-static {p0}, Lcom/google/android/apps/chrome/snapshot/SnapshotSettings;->getClientLoginAuthToken(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method static getClientLoginAuthToken(Landroid/content/Context;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 84
    invoke-static {p0}, Lcom/google/android/apps/chrome/snapshot/SnapshotSettings;->getPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string/jumbo v1, "chrome_to_mobile_authToken"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getLastUpdatedTimestamp(Landroid/content/Context;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 128
    invoke-static {p0}, Lcom/google/android/apps/chrome/snapshot/SnapshotSettings;->getPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string/jumbo v1, "chrome_to_mobile_last_updated_timestamp"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getOAuth2Token(Landroid/content/Context;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 88
    invoke-static {p0}, Lcom/google/android/apps/chrome/snapshot/SnapshotSettings;->getPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string/jumbo v1, "chrome_to_mobile_oAuth2Token"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static getPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;
    .locals 1

    .prologue
    .line 158
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    return-object v0
.end method

.method public static getPrinterId(Landroid/content/Context;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 50
    invoke-static {p0}, Lcom/google/android/apps/chrome/snapshot/SnapshotSettings;->getPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string/jumbo v1, "chrome_to_mobile_printerId"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getVersion(Landroid/content/Context;)J
    .locals 4

    .prologue
    .line 154
    invoke-static {p0}, Lcom/google/android/apps/chrome/snapshot/SnapshotSettings;->getPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string/jumbo v1, "chrome_to_mobile_version"

    const-wide/16 v2, -0x1

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public static hasPrinterId(Landroid/content/Context;)Z
    .locals 1

    .prologue
    .line 54
    invoke-static {p0}, Lcom/google/android/apps/chrome/snapshot/SnapshotSettings;->getPrinterId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isNewProfileManagementEnabled(Landroid/content/Context;)Z
    .locals 3

    .prologue
    .line 104
    invoke-static {p0}, Lcom/google/android/apps/chrome/snapshot/SnapshotSettings;->getPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string/jumbo v1, "chrome_to_mobile_new_profile_management_enabled"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static legacyIsEnabled(Landroid/content/Context;)Z
    .locals 3

    .prologue
    .line 96
    invoke-static {p0}, Lcom/google/android/apps/chrome/snapshot/SnapshotSettings;->getPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string/jumbo v1, "chrome_to_mobile_enabled"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static legacySetEnabled(Landroid/content/Context;Z)V
    .locals 2

    .prologue
    .line 92
    invoke-static {p0}, Lcom/google/android/apps/chrome/snapshot/SnapshotSettings;->getPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string/jumbo v1, "chrome_to_mobile_enabled"

    invoke-static {v0, v1, p1}, Lcom/google/android/apps/chrome/snapshot/SnapshotSettings;->storeField(Landroid/content/SharedPreferences;Ljava/lang/String;Z)V

    .line 93
    return-void
.end method

.method public static needsUpdating(Landroid/content/Context;)Z
    .locals 3

    .prologue
    .line 112
    invoke-static {p0}, Lcom/google/android/apps/chrome/snapshot/SnapshotSettings;->getPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string/jumbo v1, "chrome_to_mobile_needs_updating"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static setApplicationVersion(Landroid/content/Context;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 116
    invoke-static {p0}, Lcom/google/android/apps/chrome/snapshot/SnapshotSettings;->getPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string/jumbo v1, "chrome_to_mobile_application_version"

    invoke-static {v0, v1, p1}, Lcom/google/android/apps/chrome/snapshot/SnapshotSettings;->storeField(Landroid/content/SharedPreferences;Ljava/lang/String;Ljava/lang/String;)V

    .line 117
    return-void
.end method

.method public static setAuthToken(Landroid/content/Context;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 58
    invoke-static {}, Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintContract;->isChromeToMobileOAuth2Enabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 59
    invoke-static {p0, p1}, Lcom/google/android/apps/chrome/snapshot/SnapshotSettings;->setOAuth2Token(Landroid/content/Context;Ljava/lang/String;)V

    .line 63
    :goto_0
    return-void

    .line 61
    :cond_0
    invoke-static {p0, p1}, Lcom/google/android/apps/chrome/snapshot/SnapshotSettings;->setClientLoginAuthToken(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_0
.end method

.method static setClientLoginAuthToken(Landroid/content/Context;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 67
    invoke-static {p0}, Lcom/google/android/apps/chrome/snapshot/SnapshotSettings;->getPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string/jumbo v1, "chrome_to_mobile_authToken"

    invoke-static {v0, v1, p1}, Lcom/google/android/apps/chrome/snapshot/SnapshotSettings;->storeField(Landroid/content/SharedPreferences;Ljava/lang/String;Ljava/lang/String;)V

    .line 68
    return-void
.end method

.method public static setLastUpdatedTimestamp(Landroid/content/Context;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 124
    invoke-static {p0}, Lcom/google/android/apps/chrome/snapshot/SnapshotSettings;->getPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string/jumbo v1, "chrome_to_mobile_last_updated_timestamp"

    invoke-static {v0, v1, p1}, Lcom/google/android/apps/chrome/snapshot/SnapshotSettings;->storeField(Landroid/content/SharedPreferences;Ljava/lang/String;Ljava/lang/String;)V

    .line 125
    return-void
.end method

.method public static setNeedsUpdating(Landroid/content/Context;Z)V
    .locals 2

    .prologue
    .line 108
    invoke-static {p0}, Lcom/google/android/apps/chrome/snapshot/SnapshotSettings;->getPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string/jumbo v1, "chrome_to_mobile_needs_updating"

    invoke-static {v0, v1, p1}, Lcom/google/android/apps/chrome/snapshot/SnapshotSettings;->storeField(Landroid/content/SharedPreferences;Ljava/lang/String;Z)V

    .line 109
    return-void
.end method

.method public static setNewProfileManagementEnabled(Landroid/content/Context;Z)V
    .locals 2

    .prologue
    .line 100
    invoke-static {p0}, Lcom/google/android/apps/chrome/snapshot/SnapshotSettings;->getPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string/jumbo v1, "chrome_to_mobile_new_profile_management_enabled"

    invoke-static {v0, v1, p1}, Lcom/google/android/apps/chrome/snapshot/SnapshotSettings;->storeField(Landroid/content/SharedPreferences;Ljava/lang/String;Z)V

    .line 101
    return-void
.end method

.method public static setOAuth2Token(Landroid/content/Context;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 71
    invoke-static {p0}, Lcom/google/android/apps/chrome/snapshot/SnapshotSettings;->getPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string/jumbo v1, "chrome_to_mobile_oAuth2Token"

    invoke-static {v0, v1, p1}, Lcom/google/android/apps/chrome/snapshot/SnapshotSettings;->storeField(Landroid/content/SharedPreferences;Ljava/lang/String;Ljava/lang/String;)V

    .line 72
    return-void
.end method

.method public static setPrinterId(Landroid/content/Context;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 42
    invoke-static {p0}, Lcom/google/android/apps/chrome/snapshot/SnapshotSettings;->getPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string/jumbo v1, "chrome_to_mobile_printerId"

    invoke-static {v0, v1, p1}, Lcom/google/android/apps/chrome/snapshot/SnapshotSettings;->storeField(Landroid/content/SharedPreferences;Ljava/lang/String;Ljava/lang/String;)V

    .line 43
    return-void
.end method

.method public static setVersion(Landroid/content/Context;J)V
    .locals 3

    .prologue
    .line 132
    invoke-static {p0}, Lcom/google/android/apps/chrome/snapshot/SnapshotSettings;->getPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string/jumbo v1, "chrome_to_mobile_version"

    invoke-static {v0, v1, p1, p2}, Lcom/google/android/apps/chrome/snapshot/SnapshotSettings;->storeField(Landroid/content/SharedPreferences;Ljava/lang/String;J)V

    .line 133
    return-void
.end method

.method private static storeField(Landroid/content/SharedPreferences;Ljava/lang/String;J)V
    .locals 2

    .prologue
    .line 148
    invoke-interface {p0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 149
    invoke-interface {v0, p1, p2, p3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 150
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 151
    return-void
.end method

.method private static storeField(Landroid/content/SharedPreferences;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 136
    invoke-interface {p0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 137
    invoke-interface {v0, p1, p2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 138
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 139
    return-void
.end method

.method private static storeField(Landroid/content/SharedPreferences;Ljava/lang/String;Z)V
    .locals 1

    .prologue
    .line 142
    invoke-interface {p0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 143
    invoke-interface {v0, p1, p2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 144
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 145
    return-void
.end method
