.class Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog$4;
.super Landroid/animation/AnimatorListenerAdapter;
.source "EnhancedBookmarkDetailDialog.java"


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;

.field final synthetic val$editText:Landroid/widget/EditText;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;Landroid/widget/EditText;)V
    .locals 0

    .prologue
    .line 254
    iput-object p1, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog$4;->this$0:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;

    iput-object p2, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog$4;->val$editText:Landroid/widget/EditText;

    invoke-direct {p0}, Landroid/animation/AnimatorListenerAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, -0x1

    .line 265
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog$4;->this$0:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;

    # getter for: Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;->mImageView:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;->access$500(Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;)Landroid/widget/ImageView;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 266
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog$4;->this$0:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;

    # getter for: Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;->mMaskView:Landroid/view/View;
    invoke-static {v0}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;->access$800(Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;)Landroid/view/View;

    move-result-object v0

    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    iget-object v2, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog$4;->this$0:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;

    # getter for: Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;->mMaskView:Landroid/view/View;
    invoke-static {v2}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;->access$800(Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    iget v2, v2, Landroid/view/ViewGroup$LayoutParams;->width:I

    iget-object v3, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog$4;->this$0:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;

    # getter for: Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;->mActionBarLayout:Landroid/widget/RelativeLayout;
    invoke-static {v3}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;->access$700(Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;)Landroid/widget/RelativeLayout;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/RelativeLayout;->getHeight()I

    move-result v3

    invoke-direct {v1, v2, v3}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 268
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog$4;->this$0:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;

    # getter for: Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;->mScrollView:Landroid/widget/ScrollView;
    invoke-static {v0}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;->access$600(Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;)Landroid/widget/ScrollView;

    move-result-object v0

    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v1, v4, v4}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/widget/ScrollView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 270
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog$4;->this$0:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;

    # getter for: Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;->mScrollView:Landroid/widget/ScrollView;
    invoke-static {v0}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;->access$600(Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;)Landroid/widget/ScrollView;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ScrollView;->setTranslationY(F)V

    .line 271
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog$4;->this$0:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;

    # getter for: Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;->mActionBarLayout:Landroid/widget/RelativeLayout;
    invoke-static {v0}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;->access$700(Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;)Landroid/widget/RelativeLayout;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog$4;->this$0:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;

    # getter for: Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;->mEnhancedBookmarksModel:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarksModel;
    invoke-static {v1}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;->access$900(Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;)Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarksModel;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog$4;->this$0:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;

    # getter for: Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;->mBookmarkId:Lorg/chromium/components/bookmarks/BookmarkId;
    invoke-static {v2}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;->access$100(Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;)Lorg/chromium/components/bookmarks/BookmarkId;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarksModel;->getBookmarkById(Lorg/chromium/components/bookmarks/BookmarkId;)Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkItem;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkColor;->generateBackgroundColor(Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkItem;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setBackgroundColor(I)V

    .line 273
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog$4;->this$0:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;

    # getter for: Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;->mShadow:Landroid/view/View;
    invoke-static {v0}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;->access$1000(Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    .line 275
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog$4;->val$editText:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    .line 276
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog$4;->this$0:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const-string/jumbo v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 278
    iget-object v1, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog$4;->val$editText:Landroid/widget/EditText;

    invoke-virtual {v0, v1, v5}, Landroid/view/inputmethod/InputMethodManager;->showSoftInput(Landroid/view/View;I)Z

    .line 279
    return-void
.end method

.method public onAnimationStart(Landroid/animation/Animator;)V
    .locals 5

    .prologue
    .line 257
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog$4;->this$0:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;

    # getter for: Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;->mScrollView:Landroid/widget/ScrollView;
    invoke-static {v0}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;->access$600(Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;)Landroid/widget/ScrollView;

    move-result-object v0

    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    const/4 v2, -0x1

    iget-object v3, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog$4;->this$0:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;

    # getter for: Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;->mScrollView:Landroid/widget/ScrollView;
    invoke-static {v3}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;->access$600(Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;)Landroid/widget/ScrollView;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/ScrollView;->getHeight()I

    move-result v3

    iget-object v4, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog$4;->this$0:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;

    # getter for: Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;->mImageView:Landroid/widget/ImageView;
    invoke-static {v4}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;->access$500(Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;)Landroid/widget/ImageView;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/ImageView;->getHeight()I

    move-result v4

    add-int/2addr v3, v4

    iget-object v4, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog$4;->this$0:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;

    # getter for: Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;->mActionBarLayout:Landroid/widget/RelativeLayout;
    invoke-static {v4}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;->access$700(Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;)Landroid/widget/RelativeLayout;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/RelativeLayout;->getHeight()I

    move-result v4

    sub-int/2addr v3, v4

    invoke-direct {v1, v2, v3}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/widget/ScrollView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 261
    return-void
.end method
