.class Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager$InterceptNavigationDelegateImpl;
.super Ljava/lang/Object;
.source "ContextualSearchManager.java"

# interfaces
.implements Lorg/chromium/components/navigation_interception/InterceptNavigationDelegate;


# instance fields
.field final mUrlHandler:Lcom/google/android/apps/chrome/UrlHandler;

.field final synthetic this$0:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;)V
    .locals 2

    .prologue
    .line 1051
    iput-object p1, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager$InterceptNavigationDelegateImpl;->this$0:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1052
    new-instance v0, Lcom/google/android/apps/chrome/UrlHandler;

    iget-object v1, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager$InterceptNavigationDelegateImpl;->this$0:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;

    # getter for: Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mActivity:Lcom/google/android/apps/chrome/ChromeActivity;
    invoke-static {v1}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->access$1000(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;)Lcom/google/android/apps/chrome/ChromeActivity;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/apps/chrome/UrlHandler;-><init>(Landroid/app/Activity;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager$InterceptNavigationDelegateImpl;->mUrlHandler:Lcom/google/android/apps/chrome/UrlHandler;

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager$1;)V
    .locals 0

    .prologue
    .line 1051
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager$InterceptNavigationDelegateImpl;-><init>(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;)V

    return-void
.end method

.method private getReferrerUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1071
    iget-object v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager$InterceptNavigationDelegateImpl;->this$0:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;

    # getter for: Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mSearchContentViewCore:Lorg/chromium/content/browser/ContentViewCore;
    invoke-static {v0}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->access$300(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;)Lorg/chromium/content/browser/ContentViewCore;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager$InterceptNavigationDelegateImpl;->this$0:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;

    # getter for: Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mSearchContentViewCore:Lorg/chromium/content/browser/ContentViewCore;
    invoke-static {v0}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->access$300(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;)Lorg/chromium/content/browser/ContentViewCore;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/content/browser/ContentViewCore;->getWebContents()Lorg/chromium/content_public/browser/WebContents;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1072
    iget-object v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager$InterceptNavigationDelegateImpl;->this$0:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;

    # getter for: Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mSearchContentViewCore:Lorg/chromium/content/browser/ContentViewCore;
    invoke-static {v0}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->access$300(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;)Lorg/chromium/content/browser/ContentViewCore;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/content/browser/ContentViewCore;->getWebContents()Lorg/chromium/content_public/browser/WebContents;

    move-result-object v0

    invoke-interface {v0}, Lorg/chromium/content_public/browser/WebContents;->getNavigationController()Lorg/chromium/content_public/browser/NavigationController;

    move-result-object v0

    invoke-interface {v0}, Lorg/chromium/content_public/browser/NavigationController;->getOriginalUrlForVisibleNavigationEntry()Ljava/lang/String;

    move-result-object v0

    .line 1075
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public shouldIgnoreNavigation(Lorg/chromium/components/navigation_interception/NavigationParams;)Z
    .locals 7

    .prologue
    const/4 v3, 0x0

    .line 1055
    iget-object v0, p1, Lorg/chromium/components/navigation_interception/NavigationParams;->url:Ljava/lang/String;

    .line 1056
    iget-object v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager$InterceptNavigationDelegateImpl;->this$0:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;

    # getter for: Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mTabRedirectHandler:Lcom/google/android/apps/chrome/tab/TabRedirectHandler;
    invoke-static {v0}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->access$1100(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;)Lcom/google/android/apps/chrome/tab/TabRedirectHandler;

    move-result-object v0

    iget v1, p1, Lorg/chromium/components/navigation_interception/NavigationParams;->pageTransitionType:I

    iget-object v2, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager$InterceptNavigationDelegateImpl;->this$0:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;

    # getter for: Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mActivity:Lcom/google/android/apps/chrome/ChromeActivity;
    invoke-static {v2}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->access$1000(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;)Lcom/google/android/apps/chrome/ChromeActivity;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/ChromeActivity;->getLastUserInteractionTime()J

    move-result-wide v4

    invoke-virtual {v0, v1, v4, v5}, Lcom/google/android/apps/chrome/tab/TabRedirectHandler;->applyNewUrlLoading(IJ)V

    .line 1059
    iget-object v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager$InterceptNavigationDelegateImpl;->mUrlHandler:Lcom/google/android/apps/chrome/UrlHandler;

    iget-object v1, p1, Lorg/chromium/components/navigation_interception/NavigationParams;->url:Ljava/lang/String;

    invoke-direct {p0}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager$InterceptNavigationDelegateImpl;->getReferrerUrl()Ljava/lang/String;

    move-result-object v2

    iget v4, p1, Lorg/chromium/components/navigation_interception/NavigationParams;->pageTransitionType:I

    iget-boolean v5, p1, Lorg/chromium/components/navigation_interception/NavigationParams;->isRedirect:Z

    iget-object v6, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager$InterceptNavigationDelegateImpl;->this$0:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;

    # getter for: Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mTabRedirectHandler:Lcom/google/android/apps/chrome/tab/TabRedirectHandler;
    invoke-static {v6}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->access$1100(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;)Lcom/google/android/apps/chrome/tab/TabRedirectHandler;

    move-result-object v6

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/apps/chrome/UrlHandler;->shouldOverrideUrlLoading(Ljava/lang/String;Ljava/lang/String;ZIZLcom/google/android/apps/chrome/tab/TabRedirectHandler;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1062
    iget-object v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager$InterceptNavigationDelegateImpl;->this$0:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;

    # getter for: Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mLayoutDelegate:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchLayoutDelegate;
    invoke-static {v0}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->access$000(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;)Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchLayoutDelegate;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;->TAB_PROMOTION:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;

    const-wide/16 v2, 0x28

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchLayoutDelegate;->maximizePanelThenPromoteToTab(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;J)V

    .line 1065
    const/4 v3, 0x1

    .line 1067
    :cond_0
    return v3
.end method
