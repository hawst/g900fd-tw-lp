.class Lcom/google/android/apps/chrome/tab/AuthenticatorHelper$1;
.super Ljava/lang/Object;
.source "AuthenticatorHelper.java"

# interfaces
.implements Lorg/chromium/ui/base/WindowAndroid$IntentCallback;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/tab/AuthenticatorHelper;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/tab/AuthenticatorHelper;)V
    .locals 0

    .prologue
    .line 125
    iput-object p1, p0, Lcom/google/android/apps/chrome/tab/AuthenticatorHelper$1;->this$0:Lcom/google/android/apps/chrome/tab/AuthenticatorHelper;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onIntentCompleted(Lorg/chromium/ui/base/WindowAndroid;ILandroid/content/ContentResolver;Landroid/content/Intent;)V
    .locals 4

    .prologue
    .line 129
    if-nez p2, :cond_1

    .line 130
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/AuthenticatorHelper$1;->this$0:Lcom/google/android/apps/chrome/tab/AuthenticatorHelper;

    const/4 v1, 0x2

    # invokes: Lcom/google/android/apps/chrome/tab/AuthenticatorHelper;->sendErrorToPage(I)V
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/tab/AuthenticatorHelper;->access$000(Lcom/google/android/apps/chrome/tab/AuthenticatorHelper;I)V

    .line 148
    :cond_0
    :goto_0
    return-void

    .line 133
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/AuthenticatorHelper$1;->this$0:Lcom/google/android/apps/chrome/tab/AuthenticatorHelper;

    # getter for: Lcom/google/android/apps/chrome/tab/AuthenticatorHelper;->mOriginalTabUrl:Ljava/lang/String;
    invoke-static {v0}, Lcom/google/android/apps/chrome/tab/AuthenticatorHelper;->access$200(Lcom/google/android/apps/chrome/tab/AuthenticatorHelper;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/tab/AuthenticatorHelper$1;->this$0:Lcom/google/android/apps/chrome/tab/AuthenticatorHelper;

    # getter for: Lcom/google/android/apps/chrome/tab/AuthenticatorHelper;->mTab:Lcom/google/android/apps/chrome/tab/ChromeTab;
    invoke-static {v1}, Lcom/google/android/apps/chrome/tab/AuthenticatorHelper;->access$100(Lcom/google/android/apps/chrome/tab/AuthenticatorHelper;)Lcom/google/android/apps/chrome/tab/ChromeTab;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getUrl()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 139
    const/4 v0, 0x0

    .line 140
    if-eqz p4, :cond_2

    .line 141
    const-string/jumbo v0, "resultData"

    invoke-virtual {p4, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 143
    :cond_2
    if-nez v0, :cond_3

    .line 144
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/AuthenticatorHelper$1;->this$0:Lcom/google/android/apps/chrome/tab/AuthenticatorHelper;

    const/4 v1, 0x3

    # invokes: Lcom/google/android/apps/chrome/tab/AuthenticatorHelper;->sendErrorToPage(I)V
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/tab/AuthenticatorHelper;->access$000(Lcom/google/android/apps/chrome/tab/AuthenticatorHelper;I)V

    goto :goto_0

    .line 147
    :cond_3
    iget-object v1, p0, Lcom/google/android/apps/chrome/tab/AuthenticatorHelper$1;->this$0:Lcom/google/android/apps/chrome/tab/AuthenticatorHelper;

    iget-object v2, p0, Lcom/google/android/apps/chrome/tab/AuthenticatorHelper$1;->this$0:Lcom/google/android/apps/chrome/tab/AuthenticatorHelper;

    const/4 v3, 0x0

    # invokes: Lcom/google/android/apps/chrome/tab/AuthenticatorHelper;->buildJsonMessage(ILjava/lang/String;)Ljava/lang/String;
    invoke-static {v2, v3, v0}, Lcom/google/android/apps/chrome/tab/AuthenticatorHelper;->access$300(Lcom/google/android/apps/chrome/tab/AuthenticatorHelper;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    # invokes: Lcom/google/android/apps/chrome/tab/AuthenticatorHelper;->sendMessageToPage(Ljava/lang/String;)V
    invoke-static {v1, v0}, Lcom/google/android/apps/chrome/tab/AuthenticatorHelper;->access$400(Lcom/google/android/apps/chrome/tab/AuthenticatorHelper;Ljava/lang/String;)V

    goto :goto_0
.end method
