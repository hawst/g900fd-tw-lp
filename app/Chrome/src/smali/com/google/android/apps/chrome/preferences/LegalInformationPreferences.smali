.class public Lcom/google/android/apps/chrome/preferences/LegalInformationPreferences;
.super Lcom/google/android/apps/chrome/preferences/ChromeBasePreferenceFragment;
.source "LegalInformationPreferences.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Lcom/google/android/apps/chrome/preferences/ChromeBasePreferenceFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 18
    invoke-super {p0, p1}, Lcom/google/android/apps/chrome/preferences/ChromeBasePreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    .line 19
    sget v0, Lcom/google/android/apps/chrome/R$xml;->legal_information_preferences:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/preferences/LegalInformationPreferences;->addPreferencesFromResource(I)V

    .line 20
    return-void
.end method
