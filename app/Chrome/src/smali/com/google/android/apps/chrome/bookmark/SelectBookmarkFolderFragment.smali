.class public Lcom/google/android/apps/chrome/bookmark/SelectBookmarkFolderFragment;
.super Lcom/google/android/apps/chrome/bookmark/AsyncTaskFragment;
.source "SelectBookmarkFolderFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private mActionListener:Lcom/google/android/apps/chrome/bookmark/SelectBookmarkFolderFragment$OnActionListener;

.field private mAllowFolderAddition:Z

.field private mEmptyFoldersView:Landroid/widget/TextView;

.field private mFolderIdToSelect:J

.field private mFoldersAdapter:Lcom/google/android/apps/chrome/bookmark/SelectBookmarkFolderFragment$FolderListAdapter;

.field private mFoldersList:Landroid/widget/ListView;

.field private mIsFolder:Z

.field private mMaximumFolderIndentDepth:I

.field private mNewFolderButton:Landroid/widget/Button;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/google/android/apps/chrome/bookmark/AsyncTaskFragment;-><init>()V

    .line 65
    const/16 v0, 0x8

    iput v0, p0, Lcom/google/android/apps/chrome/bookmark/SelectBookmarkFolderFragment;->mMaximumFolderIndentDepth:I

    .line 346
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/chrome/bookmark/SelectBookmarkFolderFragment;)I
    .locals 1

    .prologue
    .line 35
    iget v0, p0, Lcom/google/android/apps/chrome/bookmark/SelectBookmarkFolderFragment;->mMaximumFolderIndentDepth:I

    return v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/chrome/bookmark/SelectBookmarkFolderFragment;Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;JZ)V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/apps/chrome/bookmark/SelectBookmarkFolderFragment;->handleLoadAllFolders(Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;JZ)V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/apps/chrome/bookmark/SelectBookmarkFolderFragment;)Landroid/widget/Button;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/google/android/apps/chrome/bookmark/SelectBookmarkFolderFragment;->mNewFolderButton:Landroid/widget/Button;

    return-object v0
.end method

.method private addFolderItem(Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;IJ)V
    .locals 3

    .prologue
    .line 218
    invoke-virtual {p1}, Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;->id()J

    move-result-wide v0

    cmp-long v0, v0, p3

    if-nez v0, :cond_1

    const/4 v0, 0x1

    .line 219
    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/chrome/bookmark/SelectBookmarkFolderFragment;->mFoldersAdapter:Lcom/google/android/apps/chrome/bookmark/SelectBookmarkFolderFragment$FolderListAdapter;

    new-instance v2, Lcom/google/android/apps/chrome/bookmark/SelectBookmarkFolderFragment$FolderListEntry;

    invoke-direct {v2, p1, p2, v0}, Lcom/google/android/apps/chrome/bookmark/SelectBookmarkFolderFragment$FolderListEntry;-><init>(Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;IZ)V

    invoke-virtual {v1, v2}, Lcom/google/android/apps/chrome/bookmark/SelectBookmarkFolderFragment$FolderListAdapter;->add(Ljava/lang/Object;)V

    .line 221
    invoke-virtual {p1}, Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;->id()J

    move-result-wide v0

    cmp-long v0, v0, p3

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/bookmark/SelectBookmarkFolderFragment;->mIsFolder:Z

    if-nez v0, :cond_2

    .line 222
    :cond_0
    invoke-virtual {p1}, Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;->children()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;

    .line 223
    add-int/lit8 v2, p2, 0x1

    invoke-direct {p0, v0, v2, p3, p4}, Lcom/google/android/apps/chrome/bookmark/SelectBookmarkFolderFragment;->addFolderItem(Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;IJ)V

    goto :goto_1

    .line 218
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 226
    :cond_2
    return-void
.end method

.method private handleLoadAllFolders(Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;JZ)V
    .locals 4

    .prologue
    .line 196
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/bookmark/SelectBookmarkFolderFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/bookmark/SelectBookmarkFolderFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 215
    :cond_0
    :goto_0
    return-void

    .line 198
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/bookmark/SelectBookmarkFolderFragment;->mFoldersAdapter:Lcom/google/android/apps/chrome/bookmark/SelectBookmarkFolderFragment$FolderListAdapter;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/bookmark/SelectBookmarkFolderFragment$FolderListAdapter;->clear()V

    .line 199
    if-nez p1, :cond_2

    .line 200
    iget-object v0, p0, Lcom/google/android/apps/chrome/bookmark/SelectBookmarkFolderFragment;->mEmptyFoldersView:Landroid/widget/TextView;

    sget v1, Lcom/google/android/apps/chrome/R$string;->bookmark_folder_tree_error:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    .line 202
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/bookmark/SelectBookmarkFolderFragment;->mEmptyFoldersView:Landroid/widget/TextView;

    sget v1, Lcom/google/android/apps/chrome/R$string;->no_bookmark_folders:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 205
    invoke-virtual {p1}, Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;->children()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_3
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;

    .line 206
    if-nez p4, :cond_4

    .line 207
    invoke-virtual {v0}, Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;->type()Lorg/chromium/chrome/browser/ChromeBrowserProvider$Type;

    move-result-object v2

    .line 208
    sget-object v3, Lorg/chromium/chrome/browser/ChromeBrowserProvider$Type;->BOOKMARK_BAR:Lorg/chromium/chrome/browser/ChromeBrowserProvider$Type;

    if-eq v2, v3, :cond_3

    sget-object v3, Lorg/chromium/chrome/browser/ChromeBrowserProvider$Type;->OTHER_NODE:Lorg/chromium/chrome/browser/ChromeBrowserProvider$Type;

    if-eq v2, v3, :cond_3

    .line 209
    :cond_4
    const/4 v2, 0x0

    invoke-direct {p0, v0, v2, p2, p3}, Lcom/google/android/apps/chrome/bookmark/SelectBookmarkFolderFragment;->addFolderItem(Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;IJ)V

    goto :goto_1
.end method

.method private loadAllFolders(J)V
    .locals 3

    .prologue
    .line 189
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/bookmark/SelectBookmarkFolderFragment;->isFragmentAsyncTaskRunning()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 192
    :goto_0
    return-void

    .line 190
    :cond_0
    new-instance v0, Lcom/google/android/apps/chrome/bookmark/SelectBookmarkFolderFragment$LoadAllFoldersTask;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/android/apps/chrome/bookmark/SelectBookmarkFolderFragment$LoadAllFoldersTask;-><init>(Lcom/google/android/apps/chrome/bookmark/SelectBookmarkFolderFragment;J)V

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/bookmark/SelectBookmarkFolderFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    sget v2, Lcom/google/android/apps/chrome/R$string;->loading_bookmark:I

    invoke-virtual {v1, v2}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/chrome/bookmark/SelectBookmarkFolderFragment;->runFragmentAsyncTask(Lcom/google/android/apps/chrome/bookmark/AsyncTaskFragment$FragmentAsyncTask;Ljava/lang/String;)Z

    goto :goto_0
.end method

.method public static newInstance(ZJZ)Lcom/google/android/apps/chrome/bookmark/SelectBookmarkFolderFragment;
    .locals 3

    .prologue
    .line 76
    new-instance v0, Lcom/google/android/apps/chrome/bookmark/SelectBookmarkFolderFragment;

    invoke-direct {v0}, Lcom/google/android/apps/chrome/bookmark/SelectBookmarkFolderFragment;-><init>()V

    .line 77
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 78
    const-string/jumbo v2, "allowAdd"

    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 79
    const-string/jumbo v2, "selectedFolder"

    invoke-virtual {v1, v2, p1, p2}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 80
    const-string/jumbo v2, "isFolder"

    invoke-virtual {v1, v2, p3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 81
    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/bookmark/SelectBookmarkFolderFragment;->setArguments(Landroid/os/Bundle;)V

    .line 82
    return-object v0
.end method


# virtual methods
.method public areFoldersLoaded()Z
    .locals 1

    .prologue
    .line 185
    iget-object v0, p0, Lcom/google/android/apps/chrome/bookmark/SelectBookmarkFolderFragment;->mFoldersAdapter:Lcom/google/android/apps/chrome/bookmark/SelectBookmarkFolderFragment$FolderListAdapter;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/bookmark/SelectBookmarkFolderFragment$FolderListAdapter;->getCount()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public executeFolderSelection(JLjava/lang/String;)V
    .locals 1

    .prologue
    .line 307
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/bookmark/SelectBookmarkFolderFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->popBackStackImmediate()Z

    .line 308
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/bookmark/SelectBookmarkFolderFragment;->getTargetFragment()Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;->setParentFolderInfo(JLjava/lang/String;)V

    .line 310
    return-void
.end method

.method protected getOnActionListenerForTest()Lcom/google/android/apps/chrome/bookmark/SelectBookmarkFolderFragment$OnActionListener;
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, Lcom/google/android/apps/chrome/bookmark/SelectBookmarkFolderFragment;->mActionListener:Lcom/google/android/apps/chrome/bookmark/SelectBookmarkFolderFragment$OnActionListener;

    return-object v0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 134
    invoke-super {p0, p1}, Lcom/google/android/apps/chrome/bookmark/AsyncTaskFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 136
    iget-object v0, p0, Lcom/google/android/apps/chrome/bookmark/SelectBookmarkFolderFragment;->mFoldersAdapter:Lcom/google/android/apps/chrome/bookmark/SelectBookmarkFolderFragment$FolderListAdapter;

    if-nez v0, :cond_2

    .line 137
    new-instance v0, Lcom/google/android/apps/chrome/bookmark/SelectBookmarkFolderFragment$FolderListAdapter;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/bookmark/SelectBookmarkFolderFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/chrome/bookmark/SelectBookmarkFolderFragment$FolderListAdapter;-><init>(Lcom/google/android/apps/chrome/bookmark/SelectBookmarkFolderFragment;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/bookmark/SelectBookmarkFolderFragment;->mFoldersAdapter:Lcom/google/android/apps/chrome/bookmark/SelectBookmarkFolderFragment$FolderListAdapter;

    .line 138
    iget-object v0, p0, Lcom/google/android/apps/chrome/bookmark/SelectBookmarkFolderFragment;->mFoldersList:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/google/android/apps/chrome/bookmark/SelectBookmarkFolderFragment;->mFoldersAdapter:Lcom/google/android/apps/chrome/bookmark/SelectBookmarkFolderFragment$FolderListAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 140
    const-wide/16 v0, -0x1

    .line 141
    if-nez p1, :cond_0

    .line 146
    iget-wide v0, p0, Lcom/google/android/apps/chrome/bookmark/SelectBookmarkFolderFragment;->mFolderIdToSelect:J

    .line 149
    :cond_0
    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/chrome/bookmark/SelectBookmarkFolderFragment;->loadAllFolders(J)V

    .line 156
    :cond_1
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/bookmark/SelectBookmarkFolderFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/chrome/R$integer;->select_bookmark_folder_max_depth_indent:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/bookmark/SelectBookmarkFolderFragment;->mMaximumFolderIndentDepth:I

    .line 158
    return-void

    .line 151
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/bookmark/SelectBookmarkFolderFragment;->areFoldersLoaded()Z

    move-result v0

    if-nez v0, :cond_1

    .line 152
    iget-wide v0, p0, Lcom/google/android/apps/chrome/bookmark/SelectBookmarkFolderFragment;->mFolderIdToSelect:J

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/chrome/bookmark/SelectBookmarkFolderFragment;->loadAllFolders(J)V

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 12

    .prologue
    .line 162
    iget-object v0, p0, Lcom/google/android/apps/chrome/bookmark/SelectBookmarkFolderFragment;->mActionListener:Lcom/google/android/apps/chrome/bookmark/SelectBookmarkFolderFragment$OnActionListener;

    if-nez v0, :cond_1

    .line 163
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "No OnResultListener specified -- onClick == NoOp"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 178
    :cond_0
    :goto_0
    return-void

    .line 166
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/bookmark/SelectBookmarkFolderFragment;->mNewFolderButton:Landroid/widget/Button;

    if-ne p1, v0, :cond_0

    .line 167
    const-wide/16 v2, -0x1

    .line 168
    const/4 v1, 0x0

    .line 169
    const/4 v0, 0x0

    move v4, v0

    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/bookmark/SelectBookmarkFolderFragment;->mFoldersAdapter:Lcom/google/android/apps/chrome/bookmark/SelectBookmarkFolderFragment$FolderListAdapter;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/bookmark/SelectBookmarkFolderFragment$FolderListAdapter;->getCount()I

    move-result v0

    if-ge v4, v0, :cond_2

    .line 170
    iget-object v0, p0, Lcom/google/android/apps/chrome/bookmark/SelectBookmarkFolderFragment;->mFoldersAdapter:Lcom/google/android/apps/chrome/bookmark/SelectBookmarkFolderFragment$FolderListAdapter;

    invoke-virtual {v0, v4}, Lcom/google/android/apps/chrome/bookmark/SelectBookmarkFolderFragment$FolderListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/bookmark/SelectBookmarkFolderFragment$FolderListEntry;

    .line 171
    iget-object v5, v0, Lcom/google/android/apps/chrome/bookmark/SelectBookmarkFolderFragment$FolderListEntry;->mFolder:Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;

    invoke-virtual {v5}, Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;->id()J

    move-result-wide v6

    iget-wide v8, p0, Lcom/google/android/apps/chrome/bookmark/SelectBookmarkFolderFragment;->mFolderIdToSelect:J

    cmp-long v5, v6, v8

    if-nez v5, :cond_3

    .line 172
    iget-object v1, v0, Lcom/google/android/apps/chrome/bookmark/SelectBookmarkFolderFragment$FolderListEntry;->mFolder:Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;

    invoke-virtual {v1}, Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;->id()J

    move-result-wide v2

    .line 173
    iget-object v0, v0, Lcom/google/android/apps/chrome/bookmark/SelectBookmarkFolderFragment$FolderListEntry;->mFolder:Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;->name()Ljava/lang/String;

    move-result-object v0

    move-wide v10, v2

    move-object v2, v0

    move-wide v0, v10

    .line 169
    :goto_2
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    move-wide v10, v0

    move-object v1, v2

    move-wide v2, v10

    goto :goto_1

    .line 176
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/bookmark/SelectBookmarkFolderFragment;->mActionListener:Lcom/google/android/apps/chrome/bookmark/SelectBookmarkFolderFragment$OnActionListener;

    invoke-interface {v0, v2, v3, v1}, Lcom/google/android/apps/chrome/bookmark/SelectBookmarkFolderFragment$OnActionListener;->triggerNewFolderCreation(JLjava/lang/String;)V

    goto :goto_0

    :cond_3
    move-wide v10, v2

    move-object v2, v1

    move-wide v0, v10

    goto :goto_2
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 104
    invoke-super {p0, p1}, Lcom/google/android/apps/chrome/bookmark/AsyncTaskFragment;->onCreate(Landroid/os/Bundle;)V

    .line 106
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/bookmark/SelectBookmarkFolderFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string/jumbo v1, "allowAdd"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/bookmark/SelectBookmarkFolderFragment;->mAllowFolderAddition:Z

    .line 107
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/bookmark/SelectBookmarkFolderFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string/jumbo v1, "selectedFolder"

    const-wide/16 v2, -0x1

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/chrome/bookmark/SelectBookmarkFolderFragment;->mFolderIdToSelect:J

    .line 109
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/bookmark/SelectBookmarkFolderFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string/jumbo v1, "isFolder"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/bookmark/SelectBookmarkFolderFragment;->mIsFolder:Z

    .line 110
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3

    .prologue
    .line 115
    sget v0, Lcom/google/android/apps/chrome/R$layout;->select_bookmark_folder:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 117
    sget v0, Lcom/google/android/apps/chrome/R$id;->new_folder_btn:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/android/apps/chrome/bookmark/SelectBookmarkFolderFragment;->mNewFolderButton:Landroid/widget/Button;

    .line 118
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/bookmark/SelectBookmarkFolderFragment;->mAllowFolderAddition:Z

    if-eqz v0, :cond_1

    .line 119
    iget-object v0, p0, Lcom/google/android/apps/chrome/bookmark/SelectBookmarkFolderFragment;->mNewFolderButton:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 124
    :goto_0
    sget v0, Lcom/google/android/apps/chrome/R$id;->bookmark_folder_list:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/google/android/apps/chrome/bookmark/SelectBookmarkFolderFragment;->mFoldersList:Landroid/widget/ListView;

    .line 125
    sget v0, Lcom/google/android/apps/chrome/R$id;->empty_folders:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/chrome/bookmark/SelectBookmarkFolderFragment;->mEmptyFoldersView:Landroid/widget/TextView;

    .line 126
    iget-object v0, p0, Lcom/google/android/apps/chrome/bookmark/SelectBookmarkFolderFragment;->mFoldersList:Landroid/widget/ListView;

    iget-object v2, p0, Lcom/google/android/apps/chrome/bookmark/SelectBookmarkFolderFragment;->mEmptyFoldersView:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setEmptyView(Landroid/view/View;)V

    .line 128
    iget-object v0, p0, Lcom/google/android/apps/chrome/bookmark/SelectBookmarkFolderFragment;->mFoldersAdapter:Lcom/google/android/apps/chrome/bookmark/SelectBookmarkFolderFragment$FolderListAdapter;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/bookmark/SelectBookmarkFolderFragment;->mFoldersList:Landroid/widget/ListView;

    iget-object v2, p0, Lcom/google/android/apps/chrome/bookmark/SelectBookmarkFolderFragment;->mFoldersAdapter:Lcom/google/android/apps/chrome/bookmark/SelectBookmarkFolderFragment$FolderListAdapter;

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 129
    :cond_0
    return-object v1

    .line 121
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/bookmark/SelectBookmarkFolderFragment;->mNewFolderButton:Landroid/widget/Button;

    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_0
.end method

.method public setOnActionListener(Lcom/google/android/apps/chrome/bookmark/SelectBookmarkFolderFragment$OnActionListener;)V
    .locals 0

    .prologue
    .line 99
    iput-object p1, p0, Lcom/google/android/apps/chrome/bookmark/SelectBookmarkFolderFragment;->mActionListener:Lcom/google/android/apps/chrome/bookmark/SelectBookmarkFolderFragment$OnActionListener;

    .line 100
    return-void
.end method
