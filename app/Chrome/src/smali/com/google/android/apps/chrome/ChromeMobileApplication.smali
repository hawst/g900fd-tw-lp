.class public Lcom/google/android/apps/chrome/ChromeMobileApplication;
.super Lorg/chromium/chrome/browser/ChromiumApplication;
.source "ChromeMobileApplication.java"


# static fields
.field static final synthetic $assertionsDisabled:Z

.field private static final CHROME_MANDATORY_PAKS:[Ljava/lang/String;


# instance fields
.field private final mBackgroundProcessing:Lcom/google/android/apps/chrome/ChromeMobileApplication$BackgroundProcessing;

.field private mChromeLifetimeController:Lcom/google/android/apps/chrome/ChromeLifetimeController;

.field private mDevToolsServer:Lorg/chromium/chrome/browser/DevToolsServer;

.field private mInitializedSharedClasses:Z

.field private mIsProcessInitialized:Z

.field private mIsStarted:Z

.field private final mLock:Ljava/lang/Object;

.field private mPKCS11AuthenticationManager:Lorg/chromium/chrome/browser/PKCS11AuthenticationManager;

.field private final mPowerBroadcastReceiver:Lcom/google/android/apps/chrome/PowerBroadcastReceiver;

.field private mPrintingController:Lorg/chromium/printing/PrintingController;

.field private final mUpdateInfoBarHelper:Lcom/google/android/apps/chrome/omaha/UpdateInfoBarHelper;

.field private mVariationsSession:Lcom/google/android/apps/chrome/variations/VariationsSession;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 92
    const-class v0, Lcom/google/android/apps/chrome/ChromeMobileApplication;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/ChromeMobileApplication;->$assertionsDisabled:Z

    .line 156
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/String;

    const-string/jumbo v3, "en-US.pak"

    aput-object v3, v0, v2

    const-string/jumbo v2, "resources.pak"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string/jumbo v2, "chrome_100_percent.pak"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string/jumbo v2, "icudtl.dat"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/chrome/ChromeMobileApplication;->CHROME_MANDATORY_PAKS:[Ljava/lang/String;

    return-void

    :cond_0
    move v0, v2

    .line 92
    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 197
    invoke-direct {p0}, Lorg/chromium/chrome/browser/ChromiumApplication;-><init>()V

    .line 101
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/ChromeMobileApplication;->mLock:Ljava/lang/Object;

    .line 181
    new-instance v0, Lcom/google/android/apps/chrome/ChromeMobileApplication$BackgroundProcessing;

    invoke-direct {v0, v1}, Lcom/google/android/apps/chrome/ChromeMobileApplication$BackgroundProcessing;-><init>(Lcom/google/android/apps/chrome/ChromeMobileApplication$1;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/ChromeMobileApplication;->mBackgroundProcessing:Lcom/google/android/apps/chrome/ChromeMobileApplication$BackgroundProcessing;

    .line 182
    new-instance v0, Lcom/google/android/apps/chrome/PowerBroadcastReceiver;

    invoke-direct {v0}, Lcom/google/android/apps/chrome/PowerBroadcastReceiver;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/ChromeMobileApplication;->mPowerBroadcastReceiver:Lcom/google/android/apps/chrome/PowerBroadcastReceiver;

    .line 183
    new-instance v0, Lcom/google/android/apps/chrome/omaha/UpdateInfoBarHelper;

    invoke-direct {v0}, Lcom/google/android/apps/chrome/omaha/UpdateInfoBarHelper;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/ChromeMobileApplication;->mUpdateInfoBarHelper:Lcom/google/android/apps/chrome/omaha/UpdateInfoBarHelper;

    .line 199
    invoke-static {v1}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->enable(Landroid/content/Context;)V

    .line 200
    return-void
.end method

.method static synthetic access$700(Lcom/google/android/apps/chrome/ChromeMobileApplication;)V
    .locals 1

    .prologue
    .line 92
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->enable(Landroid/content/Context;)V

    return-void
.end method

.method static synthetic access$800(Lcom/google/android/apps/chrome/ChromeMobileApplication;)V
    .locals 0

    .prologue
    .line 92
    invoke-direct {p0}, Lcom/google/android/apps/chrome/ChromeMobileApplication;->onForegroundSessionEnd()V

    return-void
.end method

.method static synthetic access$900(Lcom/google/android/apps/chrome/ChromeMobileApplication;)V
    .locals 0

    .prologue
    .line 92
    invoke-direct {p0}, Lcom/google/android/apps/chrome/ChromeMobileApplication;->onForegroundActivityDestroyed()V

    return-void
.end method

.method private createActivityStateListener()Lorg/chromium/base/ApplicationStatus$ActivityStateListener;
    .locals 1

    .prologue
    .line 668
    new-instance v0, Lcom/google/android/apps/chrome/ChromeMobileApplication$4;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/ChromeMobileApplication$4;-><init>(Lcom/google/android/apps/chrome/ChromeMobileApplication;)V

    return-object v0
.end method

.method private createApplicationStateListener()Lorg/chromium/base/ApplicationStatus$ApplicationStateListener;
    .locals 1

    .prologue
    .line 681
    new-instance v0, Lcom/google/android/apps/chrome/ChromeMobileApplication$5;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/ChromeMobileApplication$5;-><init>(Lcom/google/android/apps/chrome/ChromeMobileApplication;)V

    return-object v0
.end method

.method private createIntentForSettingsPage(Landroid/content/Context;Ljava/lang/String;I)Landroid/content/Intent;
    .locals 3

    .prologue
    .line 705
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 706
    const-class v1, Lcom/google/android/apps/chrome/preferences/Preferences;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Landroid/content/Intent;->setClassName(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    .line 707
    const/high16 v1, 0x30020000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 709
    if-eqz p2, :cond_0

    .line 710
    const-string/jumbo v1, ":android:show_fragment"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 711
    const-string/jumbo v1, ":android:show_fragment_title"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 712
    const-string/jumbo v1, "display_home_as_up"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 714
    :cond_0
    return-object v0
.end method

.method public static destroyProfile(Lorg/chromium/chrome/browser/profiles/Profile;)V
    .locals 0

    .prologue
    .line 403
    invoke-static {p0}, Lcom/google/android/apps/chrome/ChromeMobileApplication;->nativeDestroyProfile(Lorg/chromium/chrome/browser/profiles/Profile;)V

    .line 404
    return-void
.end method

.method public static flushPersistentData()V
    .locals 0

    .prologue
    .line 412
    invoke-static {}, Lorg/chromium/base/TraceEvent;->begin()V

    .line 413
    invoke-static {}, Lcom/google/android/apps/chrome/ChromeMobileApplication;->nativeFlushPersistentData()V

    .line 414
    invoke-static {}, Lorg/chromium/base/TraceEvent;->end()V

    .line 415
    return-void
.end method

.method private static getAlternativeCommandLinePath(Landroid/content/Context;)Ljava/io/File;
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 440
    new-instance v1, Ljava/io/File;

    const-string/jumbo v2, "/data/local/tmp"

    const-string/jumbo v3, "chrome-command-line"

    invoke-direct {v1, v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 442
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_1

    .line 459
    :cond_0
    :goto_0
    return-object v0

    .line 445
    :cond_1
    :try_start_0
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x11

    if-ge v2, v3, :cond_2

    invoke-static {p0}, Lcom/google/android/apps/chrome/ChromeMobileApplication;->getDebugAppPreJBMR1(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    .line 449
    :goto_1
    if-eqz v2, :cond_0

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 451
    const-string/jumbo v2, "ChromeMobileApplication"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "Using alternative command line file in "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, v1

    .line 453
    goto :goto_0

    .line 445
    :cond_2
    invoke-static {p0}, Lcom/google/android/apps/chrome/ChromeMobileApplication;->getDebugAppJBMR1(Landroid/content/Context;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    goto :goto_1

    .line 455
    :catch_0
    move-exception v1

    .line 456
    const-string/jumbo v1, "ChromeMobileApplication"

    const-string/jumbo v2, "Unable to detect alternative command line file"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private static getDebugAppJBMR1(Landroid/content/Context;)Ljava/lang/String;
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 430
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string/jumbo v3, "adb_enabled"

    invoke-static {v2, v3, v1}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    if-ne v2, v0, :cond_0

    .line 432
    :goto_0
    if-eqz v0, :cond_1

    .line 433
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v1, "debug_app"

    invoke-static {v0, v1}, Landroid/provider/Settings$Global;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 436
    :goto_1
    return-object v0

    :cond_0
    move v0, v1

    .line 430
    goto :goto_0

    .line 436
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private static getDebugAppPreJBMR1(Landroid/content/Context;)Ljava/lang/String;
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 419
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string/jumbo v3, "adb_enabled"

    invoke-static {v2, v3, v1}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    if-ne v2, v0, :cond_0

    .line 421
    :goto_0
    if-eqz v0, :cond_1

    .line 422
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v1, "debug_app"

    invoke-static {v0, v1}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 425
    :goto_1
    return-object v0

    :cond_0
    move v0, v1

    .line 419
    goto :goto_0

    .line 425
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private hasLocaleChanged(Ljava/lang/String;)Z
    .locals 3

    .prologue
    .line 558
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string/jumbo v1, "locale"

    const-string/jumbo v2, ""

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 561
    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 562
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 563
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 564
    const-string/jumbo v1, "locale"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 565
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 566
    const/4 v0, 0x1

    .line 568
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static initCommandLine(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 463
    invoke-static {}, Lorg/chromium/base/CommandLine;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_1

    .line 464
    invoke-static {p0}, Lcom/google/android/apps/chrome/ChromeMobileApplication;->getAlternativeCommandLinePath(Landroid/content/Context;)Ljava/io/File;

    move-result-object v0

    .line 465
    if-nez v0, :cond_0

    .line 466
    new-instance v0, Ljava/io/File;

    const-string/jumbo v1, "/data/local"

    const-string/jumbo v2, "chrome-command-line"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 469
    :cond_0
    invoke-virtual {v0}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lorg/chromium/base/CommandLine;->initFromFile(Ljava/lang/String;)V

    .line 471
    :cond_1
    return-void
.end method

.method public static initializeApplicationParameters()V
    .locals 1

    .prologue
    .line 245
    sget-object v0, Lcom/google/android/apps/chrome/ChromeMobileApplication;->CHROME_MANDATORY_PAKS:[Ljava/lang/String;

    invoke-static {v0}, Lorg/chromium/base/ResourceExtractor;->setMandatoryPaksToExtract([Ljava/lang/String;)V

    .line 246
    const-string/jumbo v0, "chrome"

    invoke-static {v0}, Lorg/chromium/base/PathUtils;->setPrivateDataDirectorySuffix(Ljava/lang/String;)V

    .line 247
    return-void
.end method

.method private initializeDataReductionProxySettings()V
    .locals 0

    .prologue
    .line 302
    invoke-static {}, Lorg/chromium/chrome/browser/net/spdyproxy/DataReductionProxySettings;->getInstance()Lorg/chromium/chrome/browser/net/spdyproxy/DataReductionProxySettings;

    .line 303
    return-void
.end method

.method private static native nativeChangeAppStatus(Z)V
.end method

.method private static native nativeDestroyProfile(Lorg/chromium/chrome/browser/profiles/Profile;)V
.end method

.method private static native nativeFlushPersistentData()V
.end method

.method private static native nativeRemoveSessionCookies()V
.end method

.method private onForegroundActivityDestroyed()V
    .locals 1

    .prologue
    .line 653
    invoke-static {}, Lorg/chromium/base/ApplicationStatus;->isEveryActivityDestroyed()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 654
    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeMobileApplication;->mBackgroundProcessing:Lcom/google/android/apps/chrome/ChromeMobileApplication$BackgroundProcessing;

    # invokes: Lcom/google/android/apps/chrome/ChromeMobileApplication$BackgroundProcessing;->onDestroy()V
    invoke-static {v0}, Lcom/google/android/apps/chrome/ChromeMobileApplication$BackgroundProcessing;->access$600(Lcom/google/android/apps/chrome/ChromeMobileApplication$BackgroundProcessing;)V

    .line 655
    const/16 v0, 0x27

    invoke-static {v0}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->broadcastImmediateNotification(I)V

    .line 657
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->disable(Landroid/content/Context;)V

    .line 658
    invoke-static {}, Lcom/google/android/apps/chrome/playlog/PlayLogTracker;->stopPlayLogger()V

    .line 659
    invoke-static {}, Lcom/google/android/apps/chrome/partnercustomizations/PartnerBrowserCustomizations;->destroy()V

    .line 661
    :cond_0
    return-void
.end method

.method private onForegroundSessionEnd()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 633
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/ChromeMobileApplication;->mIsStarted:Z

    if-nez v0, :cond_0

    .line 646
    :goto_0
    return-void

    .line 634
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeMobileApplication;->mBackgroundProcessing:Lcom/google/android/apps/chrome/ChromeMobileApplication$BackgroundProcessing;

    # invokes: Lcom/google/android/apps/chrome/ChromeMobileApplication$BackgroundProcessing;->suspendTimers()V
    invoke-static {v0}, Lcom/google/android/apps/chrome/ChromeMobileApplication$BackgroundProcessing;->access$500(Lcom/google/android/apps/chrome/ChromeMobileApplication$BackgroundProcessing;)V

    .line 635
    invoke-static {}, Lcom/google/android/apps/chrome/ChromeMobileApplication;->flushPersistentData()V

    .line 636
    iput-boolean v1, p0, Lcom/google/android/apps/chrome/ChromeMobileApplication;->mIsStarted:Z

    .line 637
    invoke-static {v1}, Lcom/google/android/apps/chrome/ChromeMobileApplication;->nativeChangeAppStatus(Z)V

    .line 640
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeMobileApplication;->mPowerBroadcastReceiver:Lcom/google/android/apps/chrome/PowerBroadcastReceiver;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/chrome/PowerBroadcastReceiver;->unregisterReceiver(Landroid/content/Context;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 645
    :goto_1
    invoke-static {}, Lorg/chromium/content/browser/ChildProcessLauncher;->onSentToBackground()V

    goto :goto_0

    :catch_0
    move-exception v0

    goto :goto_1
.end method

.method private onForegroundSessionStart()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 613
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->enable(Landroid/content/Context;)V

    .line 614
    invoke-static {}, Lorg/chromium/content/browser/ChildProcessLauncher;->onBroughtToForeground()V

    .line 615
    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeMobileApplication;->mBackgroundProcessing:Lcom/google/android/apps/chrome/ChromeMobileApplication$BackgroundProcessing;

    # invokes: Lcom/google/android/apps/chrome/ChromeMobileApplication$BackgroundProcessing;->startTimers()V
    invoke-static {v0}, Lcom/google/android/apps/chrome/ChromeMobileApplication$BackgroundProcessing;->access$400(Lcom/google/android/apps/chrome/ChromeMobileApplication$BackgroundProcessing;)V

    .line 616
    invoke-direct {p0}, Lcom/google/android/apps/chrome/ChromeMobileApplication;->updatePasswordEchoState()V

    .line 617
    invoke-direct {p0}, Lcom/google/android/apps/chrome/ChromeMobileApplication;->updateFontSize()V

    .line 618
    invoke-direct {p0}, Lcom/google/android/apps/chrome/ChromeMobileApplication;->updateAcceptLanguages()V

    .line 619
    invoke-static {v1}, Lcom/google/android/apps/chrome/ChromeMobileApplication;->nativeChangeAppStatus(Z)V

    .line 620
    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeMobileApplication;->mVariationsSession:Lcom/google/android/apps/chrome/variations/VariationsSession;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/variations/VariationsSession;->start()V

    .line 622
    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeMobileApplication;->mPowerBroadcastReceiver:Lcom/google/android/apps/chrome/PowerBroadcastReceiver;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/chrome/PowerBroadcastReceiver;->registerReceiver(Landroid/content/Context;)V

    .line 623
    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeMobileApplication;->mPowerBroadcastReceiver:Lcom/google/android/apps/chrome/PowerBroadcastReceiver;

    invoke-virtual {v0, p0, v1}, Lcom/google/android/apps/chrome/PowerBroadcastReceiver;->runActions(Landroid/content/Context;Z)V

    .line 624
    return-void
.end method

.method private removeSessionCookies()V
    .locals 6

    .prologue
    .line 378
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string/jumbo v1, "com.google.android.apps.chrome.ChromeMobileApplication.BOOT_TIMESTAMP"

    const-wide/16 v2, 0x0

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    .line 380
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v4

    sub-long/2addr v2, v4

    .line 381
    sub-long v0, v2, v0

    .line 384
    invoke-static {v0, v1}, Ljava/lang/Math;->abs(J)J

    move-result-wide v0

    const-wide/16 v4, 0x3e8

    cmp-long v0, v0, v4

    if-lez v0, :cond_0

    .line 385
    const-string/jumbo v0, "ChromeMobileApplication"

    const-string/jumbo v1, "Removing session cookies."

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 386
    invoke-static {}, Lcom/google/android/apps/chrome/ChromeMobileApplication;->nativeRemoveSessionCookies()V

    .line 388
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 389
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 390
    const-string/jumbo v1, "com.google.android.apps.chrome.ChromeMobileApplication.BOOT_TIMESTAMP"

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 391
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 393
    :cond_0
    return-void
.end method

.method public static reportStartupErrorAndExit(Lorg/chromium/base/library_loader/ProcessInitException;)V
    .locals 3

    .prologue
    .line 254
    invoke-static {}, Lorg/chromium/base/ApplicationStatus;->getLastTrackedFocusedActivity()Landroid/app/Activity;

    move-result-object v0

    .line 255
    invoke-static {v0}, Lorg/chromium/base/ApplicationStatus;->getStateForActivity(Landroid/app/Activity;)I

    move-result v1

    const/4 v2, 0x6

    if-eq v1, v2, :cond_0

    instance-of v1, v0, Landroid/support/v4/app/k;

    if-nez v1, :cond_1

    .line 298
    :cond_0
    :goto_0
    return-void

    .line 259
    :cond_1
    invoke-virtual {p0}, Lorg/chromium/base/library_loader/ProcessInitException;->getErrorCode()I

    move-result v1

    .line 261
    packed-switch v1, :pswitch_data_0

    .line 269
    sget v1, Lcom/google/android/apps/chrome/R$string;->native_startup_failed:I

    .line 271
    :goto_1
    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 273
    new-instance v2, Lcom/google/android/apps/chrome/ChromeMobileApplication$2;

    invoke-direct {v2, v1}, Lcom/google/android/apps/chrome/ChromeMobileApplication$2;-><init>(Ljava/lang/String;)V

    .line 296
    check-cast v0, Landroid/support/v4/app/k;

    invoke-virtual {v0}, Landroid/support/v4/app/k;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    const-string/jumbo v1, "InvalidStartupDialog"

    invoke-virtual {v2, v0, v1}, Landroid/support/v4/app/DialogFragment;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_0

    .line 263
    :pswitch_0
    sget v1, Lcom/google/android/apps/chrome/R$string;->os_version_missing_features:I

    goto :goto_1

    .line 266
    :pswitch_1
    sget v1, Lcom/google/android/apps/chrome/R$string;->incompatible_libraries:I

    goto :goto_1

    .line 261
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private updateAcceptLanguages()V
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 544
    invoke-static {}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->getInstance()Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;

    move-result-object v0

    .line 545
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v1

    .line 546
    invoke-direct {p0, v1}, Lcom/google/android/apps/chrome/ChromeMobileApplication;->hasLocaleChanged(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 547
    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->resetAcceptLanguages(Ljava/lang/String;)V

    .line 553
    const/4 v1, 0x0

    const/4 v3, 0x1

    move v4, v2

    move v5, v2

    move v6, v2

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->clearBrowsingData(Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences$OnClearBrowsingDataListener;ZZZZZ)V

    .line 555
    :cond_0
    return-void
.end method

.method private updateFontSize()V
    .locals 4

    .prologue
    .line 515
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ChromeMobileApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lorg/chromium/chrome/browser/accessibility/FontSizePrefs;->getInstance(Landroid/content/Context;)Lorg/chromium/chrome/browser/accessibility/FontSizePrefs;

    move-result-object v1

    .line 518
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string/jumbo v2, "text_scale"

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences;->getFloat(Ljava/lang/String;F)F

    move-result v0

    .line 521
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ChromeMobileApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    iget v2, v2, Landroid/content/res/Configuration;->fontScale:F

    mul-float/2addr v0, v2

    .line 523
    invoke-virtual {v1}, Lorg/chromium/chrome/browser/accessibility/FontSizePrefs;->getFontScaleFactor()F

    move-result v2

    sub-float v2, v0, v2

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    .line 524
    const v3, 0x3a83126f    # 0.001f

    cmpl-float v2, v2, v3

    if-ltz v2, :cond_0

    .line 525
    invoke-virtual {v1, v0}, Lorg/chromium/chrome/browser/accessibility/FontSizePrefs;->setFontScaleFactor(F)V

    .line 530
    :cond_0
    const v2, 0x3fa66666    # 1.3f

    cmpl-float v0, v0, v2

    if-ltz v0, :cond_2

    const/4 v0, 0x1

    .line 532
    :goto_0
    invoke-virtual {v1}, Lorg/chromium/chrome/browser/accessibility/FontSizePrefs;->getUserSetForceEnableZoom()Z

    move-result v2

    if-nez v2, :cond_1

    invoke-virtual {v1}, Lorg/chromium/chrome/browser/accessibility/FontSizePrefs;->getForceEnableZoom()Z

    move-result v2

    if-eq v2, v0, :cond_1

    .line 534
    invoke-virtual {v1, v0}, Lorg/chromium/chrome/browser/accessibility/FontSizePrefs;->setForceEnableZoom(Z)V

    .line 536
    :cond_1
    return-void

    .line 530
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private updatePasswordEchoState()V
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 779
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ChromeMobileApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string/jumbo v2, "show_password"

    invoke-static {v1, v2, v0}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    if-ne v1, v0, :cond_0

    .line 782
    :goto_0
    invoke-static {}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->getInstance()Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->getPasswordEchoEnabled()Z

    move-result v1

    if-ne v1, v0, :cond_1

    .line 785
    :goto_1
    return-void

    .line 779
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 784
    :cond_1
    invoke-static {}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->getInstance()Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->setPasswordEchoEnabled(Z)V

    goto :goto_1
.end method


# virtual methods
.method protected areParentalControlsEnabled()Z
    .locals 1

    .prologue
    .line 771
    invoke-static {}, Lcom/google/android/apps/chrome/partnercustomizations/PartnerBrowserCustomizations;->isIncognitoDisabled()Z

    move-result v0

    return v0
.end method

.method protected getPKCS11AuthenticationManager()Lorg/chromium/chrome/browser/PKCS11AuthenticationManager;
    .locals 2

    .prologue
    .line 789
    iget-object v1, p0, Lcom/google/android/apps/chrome/ChromeMobileApplication;->mLock:Ljava/lang/Object;

    monitor-enter v1

    .line 790
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeMobileApplication;->mPKCS11AuthenticationManager:Lorg/chromium/chrome/browser/PKCS11AuthenticationManager;

    if-nez v0, :cond_0

    .line 791
    new-instance v0, Lcom/google/android/apps/chrome/oemsupport/DelegatedPKCS11Manager;

    invoke-direct {v0}, Lcom/google/android/apps/chrome/oemsupport/DelegatedPKCS11Manager;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/ChromeMobileApplication;->mPKCS11AuthenticationManager:Lorg/chromium/chrome/browser/PKCS11AuthenticationManager;

    .line 793
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 794
    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeMobileApplication;->mPKCS11AuthenticationManager:Lorg/chromium/chrome/browser/PKCS11AuthenticationManager;

    return-object v0

    .line 793
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method getPowerBroadcastReceiver()Lcom/google/android/apps/chrome/PowerBroadcastReceiver;
    .locals 1

    .prologue
    .line 506
    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeMobileApplication;->mPowerBroadcastReceiver:Lcom/google/android/apps/chrome/PowerBroadcastReceiver;

    return-object v0
.end method

.method public getPrintingController()Lorg/chromium/printing/PrintingController;
    .locals 1

    .prologue
    .line 802
    invoke-static {}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->getInstance()Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->isPrintingEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeMobileApplication;->mPrintingController:Lorg/chromium/printing/PrintingController;

    .line 803
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getUpdateInfoBarHelper()Lcom/google/android/apps/chrome/omaha/UpdateInfoBarHelper;
    .locals 1

    .prologue
    .line 810
    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeMobileApplication;->mUpdateInfoBarHelper:Lcom/google/android/apps/chrome/omaha/UpdateInfoBarHelper;

    return-object v0
.end method

.method public initializeProcess()V
    .locals 1

    .prologue
    .line 576
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/ChromeMobileApplication;->mIsProcessInitialized:Z

    if-eqz v0, :cond_0

    .line 588
    :goto_0
    return-void

    .line 577
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/ChromeMobileApplication;->mIsProcessInitialized:Z

    .line 579
    sget-boolean v0, Lcom/google/android/apps/chrome/ChromeMobileApplication;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/ChromeMobileApplication;->mIsStarted:Z

    if-eqz v0, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 581
    :cond_1
    new-instance v0, Lcom/google/android/apps/chrome/variations/VariationsSession;

    invoke-direct {v0}, Lcom/google/android/apps/chrome/variations/VariationsSession;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/ChromeMobileApplication;->mVariationsSession:Lcom/google/android/apps/chrome/variations/VariationsSession;

    .line 582
    invoke-direct {p0}, Lcom/google/android/apps/chrome/ChromeMobileApplication;->initializeDataReductionProxySettings()V

    .line 583
    invoke-direct {p0}, Lcom/google/android/apps/chrome/ChromeMobileApplication;->removeSessionCookies()V

    .line 584
    invoke-direct {p0}, Lcom/google/android/apps/chrome/ChromeMobileApplication;->createApplicationStateListener()Lorg/chromium/base/ApplicationStatus$ApplicationStateListener;

    move-result-object v0

    invoke-static {v0}, Lorg/chromium/base/ApplicationStatus;->registerApplicationStateListener(Lorg/chromium/base/ApplicationStatus$ApplicationStateListener;)V

    .line 585
    new-instance v0, Lcom/google/android/apps/chrome/passwordmanager/PasswordAuthenticationDelegateImpl;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/passwordmanager/PasswordAuthenticationDelegateImpl;-><init>(Landroid/content/Context;)V

    invoke-static {v0}, Lorg/chromium/chrome/browser/password_manager/PasswordAuthenticationManager;->setDelegate(Lorg/chromium/chrome/browser/password_manager/PasswordAuthenticationManager$PasswordAuthenticationDelegate;)V

    .line 586
    new-instance v0, Lcom/google/android/apps/chrome/banner/PhoneskyDetailsDelegate;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/banner/PhoneskyDetailsDelegate;-><init>(Landroid/content/Context;)V

    invoke-static {v0}, Lorg/chromium/chrome/browser/banners/AppBannerManager;->setAppDetailsDelegate(Lorg/chromium/chrome/browser/banners/AppDetailsDelegate;)V

    .line 587
    new-instance v0, Lcom/google/android/apps/chrome/ChromeLifetimeController;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/ChromeLifetimeController;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/ChromeMobileApplication;->mChromeLifetimeController:Lcom/google/android/apps/chrome/ChromeLifetimeController;

    goto :goto_0
.end method

.method public initializeSharedClasses()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 478
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/ChromeMobileApplication;->mInitializedSharedClasses:Z

    if-eqz v0, :cond_1

    .line 499
    :cond_0
    :goto_0
    return-void

    .line 479
    :cond_1
    iput-boolean v2, p0, Lcom/google/android/apps/chrome/ChromeMobileApplication;->mInitializedSharedClasses:Z

    .line 481
    invoke-static {p0}, Lcom/google/android/apps/chrome/services/GoogleServicesManager;->get(Landroid/content/Context;)Lcom/google/android/apps/chrome/services/GoogleServicesManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/services/GoogleServicesManager;->onMainActivityStart()V

    .line 482
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ChromeMobileApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/chrome/RevenueStats;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/chrome/RevenueStats;

    .line 483
    const-string/jumbo v0, "com.google.android.apps.chrome.webapps.WebappManager.ACTION_START_WEBAPP"

    invoke-static {v0}, Lorg/chromium/chrome/browser/ShortcutHelper;->setFullScreenAction(Ljava/lang/String;)V

    .line 485
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ChromeMobileApplication;->getPKCS11AuthenticationManager()Lorg/chromium/chrome/browser/PKCS11AuthenticationManager;

    move-result-object v0

    invoke-interface {v0, p0}, Lorg/chromium/chrome/browser/PKCS11AuthenticationManager;->initialize(Landroid/content/Context;)V

    .line 487
    new-instance v0, Lorg/chromium/chrome/browser/DevToolsServer;

    const-string/jumbo v1, "chrome"

    invoke-direct {v0, v1}, Lorg/chromium/chrome/browser/DevToolsServer;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/ChromeMobileApplication;->mDevToolsServer:Lorg/chromium/chrome/browser/DevToolsServer;

    .line 488
    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeMobileApplication;->mDevToolsServer:Lorg/chromium/chrome/browser/DevToolsServer;

    sget-object v1, Lorg/chromium/chrome/browser/DevToolsServer$Security;->ALLOW_DEBUG_PERMISSION:Lorg/chromium/chrome/browser/DevToolsServer$Security;

    invoke-virtual {v0, v2, v1}, Lorg/chromium/chrome/browser/DevToolsServer;->setRemoteDebuggingEnabled(ZLorg/chromium/chrome/browser/DevToolsServer$Security;)V

    .line 491
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ChromeMobileApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/chrome/playlog/PlayLogTracker;->createPlayLogger(Landroid/content/Context;)V

    .line 493
    invoke-static {p0}, Lcom/google/android/apps/chrome/DownloadManagerService;->getDownloadManagerService(Landroid/content/Context;)Lcom/google/android/apps/chrome/DownloadManagerService;

    move-result-object v0

    invoke-static {v0}, Lorg/chromium/content/browser/DownloadController;->setDownloadNotificationService(Lorg/chromium/content/browser/DownloadController$DownloadNotificationService;)V

    .line 496
    invoke-static {}, Lorg/chromium/base/ApiCompatibilityUtils;->isPrintingSupported()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 497
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ChromeMobileApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lorg/chromium/chrome/browser/printing/PrintingControllerFactory;->create(Landroid/content/Context;)Lorg/chromium/printing/PrintingController;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/ChromeMobileApplication;->mPrintingController:Lorg/chromium/printing/PrintingController;

    goto :goto_0
.end method

.method public onCreate()V
    .locals 1

    .prologue
    .line 209
    invoke-static {}, Lorg/chromium/chrome/browser/UmaUtils;->recordMainEntryPointTime()V

    .line 210
    invoke-super {p0}, Lorg/chromium/chrome/browser/ChromiumApplication;->onCreate()V

    .line 211
    new-instance v0, Lcom/google/android/apps/chrome/ChromeMobileApplication$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/ChromeMobileApplication$1;-><init>(Lcom/google/android/apps/chrome/ChromeMobileApplication;)V

    invoke-static {v0}, Lorg/chromium/ui/UiUtils;->setKeyboardShowingDelegate(Lorg/chromium/ui/UiUtils$KeyboardShowingDelegate;)V

    .line 226
    invoke-static {}, Lcom/google/android/apps/chrome/ChromeMobileApplication;->initializeApplicationParameters()V

    .line 227
    invoke-static {p0}, Lcom/google/android/apps/chrome/utilities/ChromeBuildInfo;->init(Landroid/content/Context;)V

    .line 228
    invoke-direct {p0}, Lcom/google/android/apps/chrome/ChromeMobileApplication;->createActivityStateListener()Lorg/chromium/base/ApplicationStatus$ActivityStateListener;

    move-result-object v0

    invoke-static {v0}, Lorg/chromium/base/ApplicationStatus;->registerStateListenerForAllActivities(Lorg/chromium/base/ApplicationStatus$ActivityStateListener;)V

    .line 233
    invoke-static {p0}, Lorg/chromium/chrome/browser/invalidation/UniqueIdInvalidationClientNameGenerator;->doInitializeAndInstallGenerator(Landroid/content/Context;)V

    .line 238
    const/4 v0, 0x5

    invoke-static {v0}, Lcom/google/ipc/invalidation/external/client/android/service/AndroidLogger;->setMinimumAndroidLogLevel(I)V

    .line 239
    return-void
.end method

.method public onStartWithNative()V
    .locals 1

    .prologue
    .line 598
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/ChromeMobileApplication;->mIsStarted:Z

    if-eqz v0, :cond_0

    .line 604
    :goto_0
    return-void

    .line 599
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/ChromeMobileApplication;->mIsStarted:Z

    .line 601
    sget-boolean v0, Lcom/google/android/apps/chrome/ChromeMobileApplication;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/ChromeMobileApplication;->mIsProcessInitialized:Z

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 603
    :cond_1
    invoke-direct {p0}, Lcom/google/android/apps/chrome/ChromeMobileApplication;->onForegroundSessionStart()V

    goto :goto_0
.end method

.method protected openClearBrowsingData(Lorg/chromium/chrome/browser/Tab;)V
    .locals 5

    .prologue
    .line 755
    invoke-virtual {p1}, Lorg/chromium/chrome/browser/Tab;->getWindowAndroid()Lorg/chromium/ui/base/WindowAndroid;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/ui/base/WindowAndroid;->getActivity()Ljava/lang/ref/WeakReference;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    .line 756
    if-nez v0, :cond_0

    .line 757
    const-string/jumbo v0, "ChromeMobileApplication"

    const-string/jumbo v1, "Attempting to open clear browsing data for a tab without a valid activity"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 767
    :goto_0
    return-void

    .line 761
    :cond_0
    const-class v1, Lcom/google/android/apps/chrome/preferences/PrivacyPreferences;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    sget v2, Lcom/google/android/apps/chrome/R$string;->prefs_privacy:I

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/apps/chrome/ChromeMobileApplication;->createIntentForSettingsPage(Landroid/content/Context;Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v1

    .line 763
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 764
    const-string/jumbo v3, "ShowClearBrowsingData"

    const/4 v4, 0x1

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 765
    const-string/jumbo v3, ":android:show_fragment_args"

    invoke-virtual {v1, v3, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    .line 766
    invoke-virtual {v0, v1}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method public openProtectedContentSettings()V
    .locals 3

    .prologue
    .line 719
    sget-boolean v0, Lcom/google/android/apps/chrome/ChromeMobileApplication;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-ge v0, v1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    const-string/jumbo v1, "Protected Content support is KITKAT and above."

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 720
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ChromeMobileApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 721
    const-class v1, Lcom/google/android/apps/chrome/preferences/ProtectedContentPreferences;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    sget v2, Lcom/google/android/apps/chrome/R$string;->prefs_content_settings:I

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/apps/chrome/ChromeMobileApplication;->createIntentForSettingsPage(Landroid/content/Context;Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v1

    .line 723
    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 724
    return-void
.end method

.method protected showAutofillSettings()V
    .locals 3

    .prologue
    .line 740
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ChromeMobileApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 741
    const-class v1, Lcom/google/android/apps/chrome/preferences/autofill/AutofillPreferences;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    sget v2, Lcom/google/android/apps/chrome/R$string;->prefs_autofill:I

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/apps/chrome/ChromeMobileApplication;->createIntentForSettingsPage(Landroid/content/Context;Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v1

    .line 743
    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 744
    return-void
.end method

.method protected showSyncSettings()V
    .locals 3

    .prologue
    .line 728
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ChromeMobileApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    .line 731
    invoke-static {v1}, Lorg/chromium/sync/signin/ChromeSigninController;->get(Landroid/content/Context;)Lorg/chromium/sync/signin/ChromeSigninController;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/sync/signin/ChromeSigninController;->isSignedIn()Z

    move-result v0

    if-eqz v0, :cond_0

    const-class v0, Lcom/google/android/apps/chrome/services/ui/GoogleServicesFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    :goto_0
    sget v2, Lcom/google/android/apps/chrome/R$string;->sign_in_sync:I

    invoke-direct {p0, v1, v0, v2}, Lcom/google/android/apps/chrome/ChromeMobileApplication;->createIntentForSettingsPage(Landroid/content/Context;Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    .line 735
    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 736
    return-void

    .line 731
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected showTermsOfServiceDialog()V
    .locals 1

    .prologue
    .line 748
    invoke-static {}, Lorg/chromium/base/ApplicationStatus;->getLastTrackedFocusedActivity()Landroid/app/Activity;

    move-result-object v0

    .line 750
    if-eqz v0, :cond_0

    invoke-static {v0}, Lcom/google/android/apps/chrome/firstrun/FirstRunUtil;->showToSDialog(Landroid/content/Context;)V

    .line 751
    :cond_0
    return-void
.end method

.method public startBrowserProcessesAndLoadLibrariesSync(Landroid/content/Context;Z)V
    .locals 1

    .prologue
    .line 362
    invoke-static {}, Lorg/chromium/base/ThreadUtils;->assertOnUiThread()V

    .line 363
    invoke-static {p1}, Lcom/google/android/apps/chrome/ChromeMobileApplication;->initCommandLine(Landroid/content/Context;)V

    .line 364
    const/4 v0, 0x1

    invoke-static {p0, v0}, Lorg/chromium/base/library_loader/LibraryLoader;->ensureInitialized(Landroid/content/Context;Z)V

    .line 365
    invoke-virtual {p0, p2}, Lcom/google/android/apps/chrome/ChromeMobileApplication;->startChromeBrowserProcessesSync(Z)V

    .line 366
    const/16 v0, 0x25

    invoke-static {v0}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->broadcastNotification(I)V

    .line 368
    return-void
.end method

.method public startChromeBrowserProcessesAsync(Lorg/chromium/content/browser/BrowserStartupController$StartupCallback;)V
    .locals 2

    .prologue
    .line 348
    sget-boolean v0, Lcom/google/android/apps/chrome/ChromeMobileApplication;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    invoke-static {}, Lorg/chromium/base/ThreadUtils;->runningOnUiThread()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    const-string/jumbo v1, "Tried to start the browser on the wrong thread"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 349
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ChromeMobileApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 350
    invoke-static {v0}, Lorg/chromium/content/browser/BrowserStartupController;->get(Landroid/content/Context;)Lorg/chromium/content/browser/BrowserStartupController;

    move-result-object v0

    invoke-virtual {v0, p1}, Lorg/chromium/content/browser/BrowserStartupController;->startBrowserProcessesAsync(Lorg/chromium/content/browser/BrowserStartupController$StartupCallback;)V

    .line 351
    return-void
.end method

.method public startChromeBrowserProcessesSync(Z)V
    .locals 2

    .prologue
    .line 316
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ChromeMobileApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 317
    new-instance v1, Lcom/google/android/apps/chrome/ChromeMobileApplication$3;

    invoke-direct {v1, p0, v0, p1}, Lcom/google/android/apps/chrome/ChromeMobileApplication$3;-><init>(Lcom/google/android/apps/chrome/ChromeMobileApplication;Landroid/content/Context;Z)V

    invoke-static {v1}, Lorg/chromium/base/ThreadUtils;->runOnUiThreadBlockingNoException(Ljava/util/concurrent/Callable;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 332
    if-eqz v0, :cond_0

    .line 333
    new-instance v1, Lorg/chromium/base/library_loader/ProcessInitException;

    invoke-direct {v1, v0}, Lorg/chromium/base/library_loader/ProcessInitException;-><init>(I)V

    throw v1

    .line 335
    :cond_0
    return-void
.end method
