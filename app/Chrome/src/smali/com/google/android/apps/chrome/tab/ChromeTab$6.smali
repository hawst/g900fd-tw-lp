.class Lcom/google/android/apps/chrome/tab/ChromeTab$6;
.super Lorg/chromium/content/browser/ContentViewClient;
.source "ChromeTab.java"


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/tab/ChromeTab;)V
    .locals 0

    .prologue
    .line 1175
    iput-object p1, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$6;->this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;

    invoke-direct {p0}, Lorg/chromium/content/browser/ContentViewClient;-><init>()V

    return-void
.end method


# virtual methods
.method public doesPerformWebSearch()Z
    .locals 1

    .prologue
    .line 1238
    const/4 v0, 0x1

    return v0
.end method

.method public getContentVideoViewClient()Lorg/chromium/content/browser/ContentVideoViewClient;
    .locals 2

    .prologue
    .line 1264
    new-instance v0, Lcom/google/android/apps/chrome/tab/ChromeTab$6$1;

    iget-object v1, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$6;->this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;

    # getter for: Lcom/google/android/apps/chrome/tab/ChromeTab;->mActivity:Lcom/google/android/apps/chrome/ChromeActivity;
    invoke-static {v1}, Lcom/google/android/apps/chrome/tab/ChromeTab;->access$900(Lcom/google/android/apps/chrome/tab/ChromeTab;)Lcom/google/android/apps/chrome/ChromeActivity;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/chrome/tab/ChromeTab$6$1;-><init>(Lcom/google/android/apps/chrome/tab/ChromeTab$6;Landroid/app/Activity;)V

    return-object v0
.end method

.method public bridge synthetic getSelectActionModeCallback(Landroid/content/Context;Lorg/chromium/content/browser/SelectActionModeCallback$ActionHandler;Z)Landroid/view/ActionMode$Callback;
    .locals 1

    .prologue
    .line 1175
    invoke-virtual {p0, p1, p2, p3}, Lcom/google/android/apps/chrome/tab/ChromeTab$6;->getSelectActionModeCallback(Landroid/content/Context;Lorg/chromium/content/browser/SelectActionModeCallback$ActionHandler;Z)Lorg/chromium/content/browser/SelectActionModeCallback;

    move-result-object v0

    return-object v0
.end method

.method public getSelectActionModeCallback(Landroid/content/Context;Lorg/chromium/content/browser/SelectActionModeCallback$ActionHandler;Z)Lorg/chromium/content/browser/SelectActionModeCallback;
    .locals 1

    .prologue
    .line 1259
    new-instance v0, Lcom/google/android/apps/chrome/tab/ChromeSelectActionModeCallback;

    invoke-direct {v0, p1, p2, p3}, Lcom/google/android/apps/chrome/tab/ChromeSelectActionModeCallback;-><init>(Landroid/content/Context;Lorg/chromium/content/browser/SelectActionModeCallback$ActionHandler;Z)V

    return-object v0
.end method

.method public onBackgroundColorChanged(I)V
    .locals 3

    .prologue
    .line 1183
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 1184
    const-string/jumbo v1, "tabId"

    iget-object v2, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$6;->this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getId()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1185
    const-string/jumbo v1, "color"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1186
    iget-object v1, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$6;->this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;

    # getter for: Lcom/google/android/apps/chrome/tab/ChromeTab;->mActivity:Lcom/google/android/apps/chrome/ChromeActivity;
    invoke-static {v1}, Lcom/google/android/apps/chrome/tab/ChromeTab;->access$900(Lcom/google/android/apps/chrome/tab/ChromeTab;)Lcom/google/android/apps/chrome/ChromeActivity;

    move-result-object v1

    const/16 v2, 0x3f

    invoke-static {v1, v2, v0}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->broadcastImmediateNotification(Landroid/content/Context;ILandroid/os/Bundle;)V

    .line 1188
    return-void
.end method

.method public onContextualActionBarHidden()V
    .locals 2

    .prologue
    .line 1203
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$6;->this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;

    const/4 v1, 0x0

    # invokes: Lcom/google/android/apps/chrome/tab/ChromeTab;->notifyContextualActionBarStateChanged(Z)V
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/tab/ChromeTab;->access$3400(Lcom/google/android/apps/chrome/tab/ChromeTab;Z)V

    .line 1204
    return-void
.end method

.method public onContextualActionBarShown()V
    .locals 2

    .prologue
    .line 1198
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$6;->this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;

    const/4 v1, 0x1

    # invokes: Lcom/google/android/apps/chrome/tab/ChromeTab;->notifyContextualActionBarStateChanged(Z)V
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/tab/ChromeTab;->access$3400(Lcom/google/android/apps/chrome/tab/ChromeTab;Z)V

    .line 1199
    return-void
.end method

.method public onImeEvent()V
    .locals 2

    .prologue
    .line 1246
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$6;->this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;

    const/4 v1, 0x0

    # setter for: Lcom/google/android/apps/chrome/tab/ChromeTab;->mAppAssociatedWith:Ljava/lang/String;
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/tab/ChromeTab;->access$3602(Lcom/google/android/apps/chrome/tab/ChromeTab;Ljava/lang/String;)Ljava/lang/String;

    .line 1247
    return-void
.end method

.method public onImeStateChangeRequested(Z)V
    .locals 1

    .prologue
    .line 1251
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$6;->this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;

    # getter for: Lcom/google/android/apps/chrome/tab/ChromeTab;->mFullscreenManager:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;
    invoke-static {v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->access$2400(Lcom/google/android/apps/chrome/tab/ChromeTab;)Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;

    move-result-object v0

    if-nez v0, :cond_0

    .line 1254
    :goto_0
    return-void

    .line 1252
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$6;->this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;

    # setter for: Lcom/google/android/apps/chrome/tab/ChromeTab;->mIsImeShowing:Z
    invoke-static {v0, p1}, Lcom/google/android/apps/chrome/tab/ChromeTab;->access$3702(Lcom/google/android/apps/chrome/tab/ChromeTab;Z)Z

    .line 1253
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$6;->this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->updateFullscreenEnabledState()V

    goto :goto_0
.end method

.method public onOffsetsForFullscreenChanged(FFF)V
    .locals 1

    .prologue
    .line 1193
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$6;->this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/apps/chrome/tab/ChromeTab;->onOffsetsChanged(FFF)V

    .line 1194
    return-void
.end method

.method public onSelectionChanged(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 1222
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$6;->this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;

    # invokes: Lcom/google/android/apps/chrome/tab/ChromeTab;->isContextualSearchActive()Z
    invoke-static {v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->access$100(Lcom/google/android/apps/chrome/tab/ChromeTab;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1223
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$6;->this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getContextualSearchManager()Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->handleSelectionChanged(Ljava/lang/String;)V

    .line 1224
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$6;->this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;

    const/4 v1, 0x3

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/chrome/tab/ChromeTab;->updateTopControlsState(IZ)V

    .line 1226
    :cond_0
    return-void
.end method

.method public onSelectionEvent(IFF)V
    .locals 1

    .prologue
    .line 1231
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$6;->this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;

    # invokes: Lcom/google/android/apps/chrome/tab/ChromeTab;->isContextualSearchActive()Z
    invoke-static {v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->access$100(Lcom/google/android/apps/chrome/tab/ChromeTab;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1232
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$6;->this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getContextualSearchManager()Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->handleSelectionEvent(IFF)V

    .line 1234
    :cond_0
    return-void
.end method

.method public onUpdateTitle(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1178
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$6;->this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;

    # invokes: Lcom/google/android/apps/chrome/tab/ChromeTab;->updateTitle(Ljava/lang/String;)Z
    invoke-static {v0, p1}, Lcom/google/android/apps/chrome/tab/ChromeTab;->access$3200(Lcom/google/android/apps/chrome/tab/ChromeTab;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$6;->this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;

    # invokes: Lcom/google/android/apps/chrome/tab/ChromeTab;->notifyPageTitleChanged()V
    invoke-static {v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->access$3300(Lcom/google/android/apps/chrome/tab/ChromeTab;)V

    .line 1179
    :cond_0
    return-void
.end method

.method public performWebSearch(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 1208
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1217
    :goto_0
    return-void

    .line 1209
    :cond_0
    invoke-static {}, Lorg/chromium/chrome/browser/search_engines/TemplateUrlService;->getInstance()Lorg/chromium/chrome/browser/search_engines/TemplateUrlService;

    move-result-object v0

    invoke-virtual {v0, p1}, Lorg/chromium/chrome/browser/search_engines/TemplateUrlService;->getUrlForSearchQuery(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1210
    iget-object v1, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$6;->this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;

    # invokes: Lcom/google/android/apps/chrome/tab/ChromeTab;->getApplicationContext()Landroid/content/Context;
    invoke-static {v1}, Lcom/google/android/apps/chrome/tab/ChromeTab;->access$3500(Lcom/google/android/apps/chrome/tab/ChromeTab;)Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$6;->this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/tab/ChromeTab;->isIncognito()Z

    move-result v2

    invoke-static {v1, v0, v2}, Lcom/google/android/apps/chrome/omnibox/GeolocationHeader;->getGeoHeader(Landroid/content/Context;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v1

    .line 1213
    new-instance v2, Lorg/chromium/content_public/browser/LoadUrlParams;

    invoke-direct {v2, v0}, Lorg/chromium/content_public/browser/LoadUrlParams;-><init>(Ljava/lang/String;)V

    .line 1214
    invoke-virtual {v2, v1}, Lorg/chromium/content_public/browser/LoadUrlParams;->setVerbatimHeaders(Ljava/lang/String;)V

    .line 1215
    const/4 v0, 0x5

    invoke-virtual {v2, v0}, Lorg/chromium/content_public/browser/LoadUrlParams;->setTransitionType(I)V

    .line 1216
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$6;->this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/chrome/tab/ChromeTab;->loadUrl(Lorg/chromium/content_public/browser/LoadUrlParams;)I

    goto :goto_0
.end method
