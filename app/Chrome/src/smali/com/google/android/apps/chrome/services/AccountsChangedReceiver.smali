.class public Lcom/google/android/apps/chrome/services/AccountsChangedReceiver;
.super Landroid/content/BroadcastReceiver;
.source "AccountsChangedReceiver.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3

    .prologue
    .line 29
    const-string/jumbo v0, "android.accounts.LOGIN_ACCOUNTS_CHANGED"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 30
    invoke-static {}, Lorg/chromium/base/ApplicationStatus;->hasVisibleActivities()Z

    move-result v0

    .line 31
    if-eqz v0, :cond_1

    .line 32
    new-instance v1, Lcom/google/android/apps/chrome/services/AccountsChangedReceiver$1;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/chrome/services/AccountsChangedReceiver$1;-><init>(Lcom/google/android/apps/chrome/services/AccountsChangedReceiver;Landroid/content/Context;)V

    .line 52
    :try_start_0
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/ChromeMobileApplication;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/ChromeMobileApplication;->startChromeBrowserProcessesAsync(Lorg/chromium/content/browser/BrowserStartupController$StartupCallback;)V
    :try_end_0
    .catch Lorg/chromium/base/library_loader/ProcessInitException; {:try_start_0 .. :try_end_0} :catch_0

    .line 63
    :cond_0
    :goto_0
    return-void

    .line 54
    :catch_0
    move-exception v0

    .line 55
    const-string/jumbo v1, "AccountsChangeReceiver"

    const-string/jumbo v2, "Unable to load native library."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 56
    const/4 v0, -0x1

    invoke-static {v0}, Ljava/lang/System;->exit(I)V

    goto :goto_0

    .line 60
    :cond_1
    invoke-static {p1}, Lcom/google/android/apps/chrome/services/GoogleServicesManager;->markAccountsChangedPref(Landroid/content/Context;)V

    goto :goto_0
.end method
