.class public Lcom/google/android/apps/chrome/services/AccountAdder;
.super Ljava/lang/Object;
.source "AccountAdder.java"


# static fields
.field public static final ADD_ACCOUNT_RESULT:I = 0x66


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static createAddAccountIntent()Landroid/content/Intent;
    .locals 5

    .prologue
    .line 25
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "android.settings.ADD_ACCOUNT_SETTINGS"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 28
    const-string/jumbo v1, "account_types"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string/jumbo v4, "com.google"

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 30
    return-object v0
.end method


# virtual methods
.method public addAccount(Landroid/app/Activity;I)V
    .locals 1

    .prologue
    .line 38
    invoke-static {}, Lcom/google/android/apps/chrome/services/AccountAdder;->createAddAccountIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p1, v0, p2}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 39
    return-void
.end method

.method public addAccount(Landroid/app/Fragment;I)V
    .locals 1

    .prologue
    .line 34
    invoke-static {}, Lcom/google/android/apps/chrome/services/AccountAdder;->createAddAccountIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p1, v0, p2}, Landroid/app/Fragment;->startActivityForResult(Landroid/content/Intent;I)V

    .line 35
    return-void
.end method
