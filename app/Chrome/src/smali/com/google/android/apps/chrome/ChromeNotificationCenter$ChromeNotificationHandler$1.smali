.class Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler$1;
.super Landroid/os/Handler;
.source "ChromeNotificationCenter.java"


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;)V
    .locals 0

    .prologue
    .line 883
    iput-object p1, p0, Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler$1;->this$0:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 886
    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler$1;->this$0:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;

    # getter for: Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;->mWeakContext:Ljava/lang/ref/WeakReference;
    invoke-static {v0}, Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;->access$800(Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;)Ljava/lang/ref/WeakReference;

    move-result-object v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler$1;->this$0:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;

    # getter for: Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;->mWeakContext:Ljava/lang/ref/WeakReference;
    invoke-static {v0}, Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;->access$800(Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;)Ljava/lang/ref/WeakReference;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    .line 887
    :goto_0
    iget v1, p1, Landroid/os/Message;->what:I

    const/16 v4, 0x27

    if-ne v1, v4, :cond_5

    move v4, v3

    .line 888
    :goto_1
    if-nez v0, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler$1;->this$0:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;

    # getter for: Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;->mIsApplicationContext:Z
    invoke-static {v1}, Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;->access$900(Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;)Z

    move-result v1

    if-eqz v1, :cond_6

    :cond_0
    move v1, v3

    .line 889
    :goto_2
    # invokes: Lcom/google/android/apps/chrome/ChromeNotificationCenter;->getBroadcaster(Landroid/content/Context;)Lcom/google/android/apps/chrome/third_party/Broadcaster;
    invoke-static {v0}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->access$300(Landroid/content/Context;)Lcom/google/android/apps/chrome/third_party/Broadcaster;

    move-result-object v0

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler$1;->this$0:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;

    # getter for: Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;->mIsApplicationContext:Z
    invoke-static {v0}, Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;->access$900(Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->getInstance()Lcom/google/android/apps/chrome/ChromeNotificationCenter;

    move-result-object v0

    # getter for: Lcom/google/android/apps/chrome/ChromeNotificationCenter;->mApplicationBroadcasterDestroyed:Z
    invoke-static {v0}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->access$400(Lcom/google/android/apps/chrome/ChromeNotificationCenter;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 892
    :cond_1
    :goto_3
    if-eqz v1, :cond_8

    if-nez v4, :cond_2

    if-eqz v3, :cond_8

    .line 893
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler$1;->this$0:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;->handleMessage(Landroid/os/Message;)V

    .line 898
    :cond_3
    :goto_4
    return-void

    .line 886
    :cond_4
    const/4 v0, 0x0

    goto :goto_0

    :cond_5
    move v4, v2

    .line 887
    goto :goto_1

    :cond_6
    move v1, v2

    .line 888
    goto :goto_2

    :cond_7
    move v3, v2

    .line 889
    goto :goto_3

    .line 894
    :cond_8
    if-nez v4, :cond_3

    .line 895
    const-string/jumbo v1, "ChromeNotificationCenter"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v0, "Message sent after notification center destroyed: "

    invoke-direct {v2, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    # invokes: Lcom/google/android/apps/chrome/ChromeNotificationCenter;->getNotificationTypeMap()Ljava/util/Map;
    invoke-static {}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->access$600()Ljava/util/Map;

    move-result-object v0

    iget v3, p1, Landroid/os/Message;->what:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/reflect/Field;

    invoke-virtual {v0}, Ljava/lang/reflect/Field;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_4
.end method
