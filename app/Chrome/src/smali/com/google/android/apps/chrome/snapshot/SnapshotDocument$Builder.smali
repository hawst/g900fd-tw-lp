.class Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;
.super Ljava/lang/Object;
.source "SnapshotDocument.java"


# instance fields
.field private mTempCreateTime:J

.field private mTempDownloadId:J

.field private mTempDownloadUri:Landroid/net/Uri;

.field private mTempId:I

.field private mTempJobId:Ljava/lang/String;

.field private mTempLocalUri:Landroid/net/Uri;

.field private mTempMimeType:Ljava/lang/String;

.field private mTempPageUrlDownloadId:J

.field private mTempPageUrlDownloadUri:Landroid/net/Uri;

.field private mTempPageUrlJobId:Ljava/lang/String;

.field private mTempPageUrlMimeType:Ljava/lang/String;

.field private mTempPageUrlState:Lcom/google/android/apps/chrome/snapshot/PageUrlJobState;

.field private mTempPrinterId:Ljava/lang/String;

.field private mTempSnapshotId:Ljava/lang/String;

.field private mTempState:Lcom/google/android/apps/chrome/snapshot/SnapshotJobState;

.field private mTempTitle:Ljava/lang/String;

.field private mTempUri:Landroid/net/Uri;

.field final synthetic this$0:Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;)V
    .locals 2

    .prologue
    .line 331
    iput-object p1, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;->this$0:Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 332
    # getter for: Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mId:I
    invoke-static {p1}, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->access$200(Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;->mTempId:I

    .line 333
    # getter for: Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mCreateTime:J
    invoke-static {p1}, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->access$300(Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;->mTempCreateTime:J

    .line 334
    # getter for: Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mSnapshotId:Ljava/lang/String;
    invoke-static {p1}, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->access$400(Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;->mTempSnapshotId:Ljava/lang/String;

    .line 335
    # getter for: Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mPrinterId:Ljava/lang/String;
    invoke-static {p1}, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->access$500(Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;->mTempPrinterId:Ljava/lang/String;

    .line 336
    # getter for: Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mJobId:Ljava/lang/String;
    invoke-static {p1}, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->access$600(Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;->mTempJobId:Ljava/lang/String;

    .line 337
    # getter for: Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mPageUrlJobId:Ljava/lang/String;
    invoke-static {p1}, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->access$700(Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;->mTempPageUrlJobId:Ljava/lang/String;

    .line 338
    # getter for: Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mTitle:Ljava/lang/String;
    invoke-static {p1}, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->access$800(Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;->mTempTitle:Ljava/lang/String;

    .line 339
    # getter for: Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mUri:Landroid/net/Uri;
    invoke-static {p1}, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->access$900(Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;->mTempUri:Landroid/net/Uri;

    .line 340
    # getter for: Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mDownloadUri:Landroid/net/Uri;
    invoke-static {p1}, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->access$1000(Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;->mTempDownloadUri:Landroid/net/Uri;

    .line 341
    # getter for: Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mPageUrlDownloadUri:Landroid/net/Uri;
    invoke-static {p1}, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->access$1100(Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;->mTempPageUrlDownloadUri:Landroid/net/Uri;

    .line 342
    # getter for: Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mMimeType:Ljava/lang/String;
    invoke-static {p1}, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->access$1200(Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;->mTempMimeType:Ljava/lang/String;

    .line 343
    # getter for: Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mPageUrlMimeType:Ljava/lang/String;
    invoke-static {p1}, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->access$1300(Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;->mTempPageUrlMimeType:Ljava/lang/String;

    .line 344
    # getter for: Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mLocalUri:Landroid/net/Uri;
    invoke-static {p1}, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->access$1400(Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;->mTempLocalUri:Landroid/net/Uri;

    .line 345
    # getter for: Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mDownloadId:J
    invoke-static {p1}, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->access$1500(Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;->mTempDownloadId:J

    .line 346
    # getter for: Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mPageUrlDownloadId:J
    invoke-static {p1}, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->access$1600(Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;->mTempPageUrlDownloadId:J

    .line 347
    # getter for: Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mState:Lcom/google/android/apps/chrome/snapshot/SnapshotJobState;
    invoke-static {p1}, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->access$1700(Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;)Lcom/google/android/apps/chrome/snapshot/SnapshotJobState;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;->mTempState:Lcom/google/android/apps/chrome/snapshot/SnapshotJobState;

    .line 348
    # getter for: Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mPageUrlState:Lcom/google/android/apps/chrome/snapshot/PageUrlJobState;
    invoke-static {p1}, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->access$1800(Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;)Lcom/google/android/apps/chrome/snapshot/PageUrlJobState;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;->mTempPageUrlState:Lcom/google/android/apps/chrome/snapshot/PageUrlJobState;

    .line 349
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$1;)V
    .locals 0

    .prologue
    .line 295
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;-><init>(Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;)V

    return-void
.end method


# virtual methods
.method build()Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;
    .locals 24

    .prologue
    .line 506
    new-instance v3, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;

    move-object/from16 v0, p0

    iget v4, v0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;->mTempId:I

    move-object/from16 v0, p0

    iget-wide v5, v0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;->mTempCreateTime:J

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;->mTempSnapshotId:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;->mTempPrinterId:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;->mTempJobId:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;->mTempPageUrlJobId:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;->mTempTitle:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;->mTempUri:Landroid/net/Uri;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;->mTempDownloadUri:Landroid/net/Uri;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;->mTempPageUrlDownloadUri:Landroid/net/Uri;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;->mTempMimeType:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;->mTempPageUrlMimeType:Ljava/lang/String;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;->mTempLocalUri:Landroid/net/Uri;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;->mTempDownloadId:J

    move-wide/from16 v18, v0

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;->mTempPageUrlDownloadId:J

    move-wide/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;->mTempState:Lcom/google/android/apps/chrome/snapshot/SnapshotJobState;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;->mTempPageUrlState:Lcom/google/android/apps/chrome/snapshot/PageUrlJobState;

    move-object/from16 v23, v0

    invoke-direct/range {v3 .. v23}, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;-><init>(IJLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;Landroid/net/Uri;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;JJLcom/google/android/apps/chrome/snapshot/SnapshotJobState;Lcom/google/android/apps/chrome/snapshot/PageUrlJobState;)V

    return-object v3
.end method

.method withCreateTimeIfNewer(J)Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;
    .locals 5

    .prologue
    .line 361
    iget-wide v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;->mTempCreateTime:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;->mTempCreateTime:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;->mTempCreateTime:J

    cmp-long v0, p1, v0

    if-gez v0, :cond_1

    .line 363
    :cond_0
    iput-wide p1, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;->mTempCreateTime:J

    .line 365
    :cond_1
    return-object p0
.end method

.method withDownloadId(J)Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;
    .locals 3

    .prologue
    .line 458
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-eqz v0, :cond_0

    .line 459
    iput-wide p1, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;->mTempDownloadId:J

    .line 461
    :cond_0
    return-object p0
.end method

.method withDownloadUri(Landroid/net/Uri;)Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;
    .locals 0

    .prologue
    .line 418
    if-eqz p1, :cond_0

    .line 419
    iput-object p1, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;->mTempDownloadUri:Landroid/net/Uri;

    .line 421
    :cond_0
    return-object p0
.end method

.method withId(I)Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;
    .locals 0

    .prologue
    .line 353
    if-eqz p1, :cond_0

    .line 354
    iput p1, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;->mTempId:I

    .line 356
    :cond_0
    return-object p0
.end method

.method withJobId(Ljava/lang/String;)Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;
    .locals 0

    .prologue
    .line 386
    if-eqz p1, :cond_0

    .line 387
    iput-object p1, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;->mTempJobId:Ljava/lang/String;

    .line 389
    :cond_0
    return-object p0
.end method

.method withLocalUri(Landroid/net/Uri;)Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;
    .locals 0

    .prologue
    .line 450
    if-eqz p1, :cond_0

    .line 451
    iput-object p1, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;->mTempLocalUri:Landroid/net/Uri;

    .line 453
    :cond_0
    return-object p0
.end method

.method withMimeType(Ljava/lang/String;)Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;
    .locals 0

    .prologue
    .line 434
    if-eqz p1, :cond_0

    .line 435
    iput-object p1, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;->mTempMimeType:Ljava/lang/String;

    .line 437
    :cond_0
    return-object p0
.end method

.method withPageUrlDownloadId(J)Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;
    .locals 3

    .prologue
    .line 466
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-eqz v0, :cond_0

    .line 467
    iput-wide p1, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;->mTempPageUrlDownloadId:J

    .line 469
    :cond_0
    return-object p0
.end method

.method withPageUrlDownloadUri(Landroid/net/Uri;)Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;
    .locals 0

    .prologue
    .line 426
    if-eqz p1, :cond_0

    .line 427
    iput-object p1, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;->mTempPageUrlDownloadUri:Landroid/net/Uri;

    .line 429
    :cond_0
    return-object p0
.end method

.method withPageUrlJobId(Ljava/lang/String;)Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;
    .locals 0

    .prologue
    .line 394
    if-eqz p1, :cond_0

    .line 395
    iput-object p1, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;->mTempPageUrlJobId:Ljava/lang/String;

    .line 397
    :cond_0
    return-object p0
.end method

.method withPageUrlMimeType(Ljava/lang/String;)Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;
    .locals 0

    .prologue
    .line 442
    if-eqz p1, :cond_0

    .line 443
    iput-object p1, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;->mTempPageUrlMimeType:Ljava/lang/String;

    .line 445
    :cond_0
    return-object p0
.end method

.method withPageUrlState(Lcom/google/android/apps/chrome/snapshot/PageUrlJobState;)Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;
    .locals 0

    .prologue
    .line 490
    if-eqz p1, :cond_0

    .line 491
    iput-object p1, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;->mTempPageUrlState:Lcom/google/android/apps/chrome/snapshot/PageUrlJobState;

    .line 493
    :cond_0
    return-object p0
.end method

.method withPageUrlStateIfNotSet(Lcom/google/android/apps/chrome/snapshot/PageUrlJobState;)Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;
    .locals 1

    .prologue
    .line 498
    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;->mTempPageUrlState:Lcom/google/android/apps/chrome/snapshot/PageUrlJobState;

    if-nez v0, :cond_0

    if-eqz p1, :cond_0

    .line 499
    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;->withPageUrlState(Lcom/google/android/apps/chrome/snapshot/PageUrlJobState;)Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;

    move-result-object p0

    .line 501
    :cond_0
    return-object p0
.end method

.method withPrinterId(Ljava/lang/String;)Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;
    .locals 0

    .prologue
    .line 378
    if-eqz p1, :cond_0

    .line 379
    iput-object p1, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;->mTempPrinterId:Ljava/lang/String;

    .line 381
    :cond_0
    return-object p0
.end method

.method withSnapshotId(Ljava/lang/String;)Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;
    .locals 0

    .prologue
    .line 370
    if-eqz p1, :cond_0

    .line 371
    iput-object p1, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;->mTempSnapshotId:Ljava/lang/String;

    .line 373
    :cond_0
    return-object p0
.end method

.method withState(Lcom/google/android/apps/chrome/snapshot/SnapshotJobState;)Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;
    .locals 0

    .prologue
    .line 474
    if-eqz p1, :cond_0

    .line 475
    iput-object p1, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;->mTempState:Lcom/google/android/apps/chrome/snapshot/SnapshotJobState;

    .line 477
    :cond_0
    return-object p0
.end method

.method withStateIfNotSet(Lcom/google/android/apps/chrome/snapshot/SnapshotJobState;)Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;
    .locals 1

    .prologue
    .line 482
    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;->mTempState:Lcom/google/android/apps/chrome/snapshot/SnapshotJobState;

    if-nez v0, :cond_0

    if-eqz p1, :cond_0

    .line 483
    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;->withState(Lcom/google/android/apps/chrome/snapshot/SnapshotJobState;)Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;

    move-result-object p0

    .line 485
    :cond_0
    return-object p0
.end method

.method withTitle(Ljava/lang/String;)Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;
    .locals 0

    .prologue
    .line 402
    if-eqz p1, :cond_0

    .line 403
    iput-object p1, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;->mTempTitle:Ljava/lang/String;

    .line 405
    :cond_0
    return-object p0
.end method

.method withUri(Landroid/net/Uri;)Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;
    .locals 0

    .prologue
    .line 410
    if-eqz p1, :cond_0

    .line 411
    iput-object p1, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;->mTempUri:Landroid/net/Uri;

    .line 413
    :cond_0
    return-object p0
.end method
