.class public Lcom/google/android/apps/chrome/document/ssb/SearchLauncher;
.super Ljava/lang/Object;
.source "SearchLauncher.java"


# static fields
.field static final SEARCH_INTENT_PACKAGE:Ljava/lang/String; = "com.google.android.googlequicksearchbox"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static fireIntent(Landroid/content/Context;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 57
    invoke-static {}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->recordDocumentActivityFiredSearchIntent()V

    .line 58
    const-string/jumbo v0, "com.google.android.googlequicksearchbox.TEXT_ASSIST"

    invoke-static {p0, p1, v0}, Lcom/google/android/apps/chrome/document/ssb/SearchLauncher;->getSearchIntent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 59
    return-void
.end method

.method public static fireVoiceIntent(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 65
    invoke-static {}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->recordDocumentActivityFiredSearchIntent()V

    .line 66
    const/4 v0, 0x0

    const-string/jumbo v1, "android.intent.action.VOICE_ASSIST"

    invoke-static {p0, v0, v1}, Lcom/google/android/apps/chrome/document/ssb/SearchLauncher;->getSearchIntent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 67
    return-void
.end method

.method private static getSearchIntent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
    .locals 6

    .prologue
    const/4 v2, 0x1

    .line 86
    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3}, Landroid/content/Intent;-><init>()V

    .line 87
    invoke-virtual {v3, p2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 88
    const-string/jumbo v0, "com.google.android.googlequicksearchbox"

    invoke-virtual {v3, v0}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 89
    const-string/jumbo v0, "android.intent.extra.ASSIST_PACKAGE"

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 91
    invoke-static {}, Lorg/chromium/base/library_loader/LibraryLoader;->isInitialized()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-static {p0}, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->get(Landroid/content/Context;)Lorg/chromium/chrome/browser/sync/ProfileSyncService;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->isSyncInitialized()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-static {p0}, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->get(Landroid/content/Context;)Lorg/chromium/chrome/browser/sync/ProfileSyncService;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->isEncryptEverythingEnabled()Z

    move-result v0

    if-nez v0, :cond_2

    move v1, v2

    .line 95
    :goto_0
    if-eqz v1, :cond_3

    const-string/jumbo v0, ""

    invoke-static {p0, p1, v0}, Lcom/google/android/apps/chrome/icing/SsbContextHelper;->getContextStubForSsb(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/c/f;

    move-result-object v0

    .line 99
    :goto_1
    if-eqz p1, :cond_0

    if-eqz v1, :cond_0

    .line 100
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 101
    const-string/jumbo v4, "com.google.search.assist.URI"

    invoke-virtual {v1, v4, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 103
    const-string/jumbo v4, "android.intent.extra.ASSIST_CONTEXT"

    invoke-virtual {v3, v4, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    .line 106
    :cond_0
    new-instance v1, Lcom/google/android/c/k;

    invoke-direct {v1}, Lcom/google/android/c/k;-><init>()V

    .line 107
    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "content://"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-class v5, Lcom/google/android/apps/chrome/document/DocumentSuggestionProvider;

    invoke-virtual {v5}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    iput-object v4, v1, Lcom/google/android/c/k;->a:Ljava/lang/String;

    .line 110
    const-string/jumbo v4, "chrome"

    iput-object v4, v1, Lcom/google/android/c/k;->b:Ljava/lang/String;

    .line 111
    iput-object v1, v0, Lcom/google/android/c/f;->c:Lcom/google/android/c/k;

    .line 112
    const-string/jumbo v1, "com.google.android.ssb.extra.SSB_CONTEXT"

    invoke-static {v0}, Lcom/google/protobuf/nano/MessageNano;->toByteArray(Lcom/google/protobuf/nano/MessageNano;)[B

    move-result-object v0

    invoke-virtual {v3, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    .line 115
    if-eqz p1, :cond_1

    const-string/jumbo v0, "chrome-native://"

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string/jumbo v0, "chrome://"

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 117
    const-string/jumbo v0, "android.intent.extra.TEXT"

    invoke-virtual {v3, v0, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 118
    const-string/jumbo v0, "select_query"

    invoke-virtual {v3, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 121
    :cond_1
    const-string/jumbo v0, "com.google.android.googlequicksearchbox.extra.request_elapsed_realtime_millis"

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    invoke-virtual {v3, v0, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 122
    return-object v3

    .line 91
    :cond_2
    const/4 v0, 0x0

    move v1, v0

    goto/16 :goto_0

    .line 95
    :cond_3
    new-instance v0, Lcom/google/android/c/f;

    invoke-direct {v0}, Lcom/google/android/c/f;-><init>()V

    goto/16 :goto_1
.end method

.method public static isGSAAboveMinVersion(Landroid/content/Context;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 132
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 135
    :try_start_0
    const-string/jumbo v2, "com.google.android.googlequicksearchbox"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 139
    if-nez v1, :cond_1

    .line 140
    :cond_0
    :goto_0
    return v0

    :cond_1
    iget v1, v1, Landroid/content/pm/PackageInfo;->versionCode:I

    const v2, 0x11e65256

    if-lt v1, v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    .line 137
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public static isGSAAvailable(Landroid/content/Context;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 75
    invoke-static {}, Lorg/chromium/base/CommandLine;->getInstance()Lorg/chromium/base/CommandLine;

    move-result-object v1

    const-string/jumbo v2, "enable-e200-suggestions"

    invoke-virtual {v1, v2}, Lorg/chromium/base/CommandLine;->hasSwitch(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 82
    :cond_0
    :goto_0
    return v0

    .line 78
    :cond_1
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 79
    const-string/jumbo v2, ""

    const-string/jumbo v3, "com.google.android.googlequicksearchbox.TEXT_ASSIST"

    invoke-static {p0, v2, v3}, Lcom/google/android/apps/chrome/document/ssb/SearchLauncher;->getSearchIntent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Landroid/content/pm/PackageManager;->resolveActivity(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;

    move-result-object v1

    .line 81
    if-eqz v1, :cond_0

    iget-object v1, v1, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    if-eqz v1, :cond_0

    .line 82
    const/4 v0, 0x1

    goto :goto_0
.end method
