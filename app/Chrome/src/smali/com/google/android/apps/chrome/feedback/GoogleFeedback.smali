.class public final Lcom/google/android/apps/chrome/feedback/GoogleFeedback;
.super Ljava/lang/Object;
.source "GoogleFeedback.java"


# static fields
.field private static sCurrentScreenshot:Landroid/graphics/Bitmap;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 34
    const/4 v0, 0x0

    sput-object v0, Lcom/google/android/apps/chrome/feedback/GoogleFeedback;->sCurrentScreenshot:Landroid/graphics/Bitmap;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 133
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 134
    return-void
.end method

.method public static cleanup()V
    .locals 1

    .prologue
    .line 107
    const/4 v0, 0x0

    sput-object v0, Lcom/google/android/apps/chrome/feedback/GoogleFeedback;->sCurrentScreenshot:Landroid/graphics/Bitmap;

    .line 108
    return-void
.end method

.method private static createDefaultReportingExtras()Landroid/os/Bundle;
    .locals 3

    .prologue
    .line 117
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 118
    const-string/jumbo v1, "Data reduction proxy enabled"

    invoke-static {}, Lorg/chromium/chrome/browser/net/spdyproxy/DataReductionProxySettings;->getInstance()Lorg/chromium/chrome/browser/net/spdyproxy/DataReductionProxySettings;

    move-result-object v2

    invoke-virtual {v2}, Lorg/chromium/chrome/browser/net/spdyproxy/DataReductionProxySettings;->isDataReductionProxyEnabled()Z

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 121
    return-object v0
.end method

.method public static getCurrentScreenshotForTest()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 129
    sget-object v0, Lcom/google/android/apps/chrome/feedback/GoogleFeedback;->sCurrentScreenshot:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public static launchWithExtras(Landroid/content/Context;Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 96
    invoke-static {p0}, Lcom/google/android/apps/chrome/feedback/FeedbackReporterFactory;->createFeedbackReporter(Landroid/content/Context;)Lcom/google/android/apps/chrome/feedback/FeedbackReporter;

    move-result-object v0

    .line 98
    invoke-static {}, Lcom/google/android/apps/chrome/feedback/GoogleFeedback;->createDefaultReportingExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 99
    if-eqz p1, :cond_0

    invoke-virtual {v1, p1}, Landroid/os/Bundle;->putAll(Landroid/os/Bundle;)V

    .line 100
    :cond_0
    invoke-static {}, Lcom/google/android/apps/chrome/feedback/GoogleFeedback;->getCurrentScreenshotForTest()Landroid/graphics/Bitmap;

    move-result-object v2

    invoke-interface {v0, v2, v1}, Lcom/google/android/apps/chrome/feedback/FeedbackReporter;->reportFeedback(Landroid/graphics/Bitmap;Landroid/os/Bundle;)V

    .line 101
    return-void
.end method

.method public static prepareWithActivity(Landroid/app/Activity;)V
    .locals 3

    .prologue
    .line 44
    if-nez p0, :cond_0

    .line 45
    const/4 v0, 0x0

    sput-object v0, Lcom/google/android/apps/chrome/feedback/GoogleFeedback;->sCurrentScreenshot:Landroid/graphics/Bitmap;

    .line 51
    :goto_0
    return-void

    .line 48
    :cond_0
    invoke-virtual {p0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getRootView()Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x258

    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, v1, v2}, Lorg/chromium/ui/UiUtils;->generateScaledScreenshot(Landroid/view/View;ILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/chrome/feedback/GoogleFeedback;->sCurrentScreenshot:Landroid/graphics/Bitmap;

    goto :goto_0
.end method
