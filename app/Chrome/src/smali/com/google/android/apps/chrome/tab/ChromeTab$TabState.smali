.class public Lcom/google/android/apps/chrome/tab/ChromeTab$TabState;
.super Ljava/lang/Object;
.source "ChromeTab.java"


# instance fields
.field public contentsState:Lcom/google/android/apps/chrome/tab/ChromeTab$ContentsState;

.field public isIncognito:Z

.field public openerAppId:Ljava/lang/String;

.field public parentId:I

.field public shouldPreserve:Z

.field public syncId:J

.field public timestampMillis:J


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 2484
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2485
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$TabState;->timestampMillis:J

    .line 2487
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$TabState;->parentId:I

    return-void
.end method
