.class public Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;
.super Ljava/lang/Object;
.source "SnapshotDocument.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static CREATOR:Landroid/os/Parcelable$Creator; = null

.field public static final NO_ID:I = -0x1


# instance fields
.field private mCreateTime:J

.field private mDownloadId:J

.field private mDownloadUri:Landroid/net/Uri;

.field private mId:I

.field private mJobId:Ljava/lang/String;

.field private mLocalUri:Landroid/net/Uri;

.field private mMimeType:Ljava/lang/String;

.field private mPageUrlDownloadId:J

.field private mPageUrlDownloadUri:Landroid/net/Uri;

.field private mPageUrlJobId:Ljava/lang/String;

.field private mPageUrlMimeType:Ljava/lang/String;

.field private mPageUrlState:Lcom/google/android/apps/chrome/snapshot/PageUrlJobState;

.field private mPrinterId:Ljava/lang/String;

.field private mSnapshotId:Ljava/lang/String;

.field private mState:Lcom/google/android/apps/chrome/snapshot/SnapshotJobState;

.field private mTitle:Ljava/lang/String;

.field private mUri:Landroid/net/Uri;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 19
    new-instance v0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$1;

    invoke-direct {v0}, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$1;-><init>()V

    sput-object v0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 186
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 187
    return-void
.end method

.method constructor <init>(IJLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;Landroid/net/Uri;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;JJLcom/google/android/apps/chrome/snapshot/SnapshotJobState;Lcom/google/android/apps/chrome/snapshot/PageUrlJobState;)V
    .locals 3

    .prologue
    .line 155
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 156
    iput p1, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mId:I

    .line 157
    iput-wide p2, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mCreateTime:J

    .line 158
    iput-object p4, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mSnapshotId:Ljava/lang/String;

    .line 159
    iput-object p5, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mPrinterId:Ljava/lang/String;

    .line 160
    iput-object p6, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mJobId:Ljava/lang/String;

    .line 161
    iput-object p7, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mPageUrlJobId:Ljava/lang/String;

    .line 162
    iput-object p8, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mTitle:Ljava/lang/String;

    .line 163
    iput-object p9, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mUri:Landroid/net/Uri;

    .line 164
    iput-object p10, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mDownloadUri:Landroid/net/Uri;

    .line 165
    iput-object p11, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mPageUrlDownloadUri:Landroid/net/Uri;

    .line 166
    iput-object p12, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mMimeType:Ljava/lang/String;

    .line 167
    move-object/from16 v0, p13

    iput-object v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mPageUrlMimeType:Ljava/lang/String;

    .line 168
    move-object/from16 v0, p14

    iput-object v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mLocalUri:Landroid/net/Uri;

    .line 169
    move-wide/from16 v0, p15

    iput-wide v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mDownloadId:J

    .line 170
    move-wide/from16 v0, p17

    iput-wide v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mPageUrlDownloadId:J

    .line 171
    move-object/from16 v0, p19

    iput-object v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mState:Lcom/google/android/apps/chrome/snapshot/SnapshotJobState;

    .line 172
    move-object/from16 v0, p20

    iput-object v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mPageUrlState:Lcom/google/android/apps/chrome/snapshot/PageUrlJobState;

    .line 173
    return-void
.end method

.method constructor <init>(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;Landroid/net/Uri;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;JJLcom/google/android/apps/chrome/snapshot/SnapshotJobState;Lcom/google/android/apps/chrome/snapshot/PageUrlJobState;)V
    .locals 22

    .prologue
    .line 181
    const/4 v2, -0x1

    move-object/from16 v1, p0

    move-wide/from16 v3, p1

    move-object/from16 v5, p3

    move-object/from16 v6, p4

    move-object/from16 v7, p5

    move-object/from16 v8, p6

    move-object/from16 v9, p7

    move-object/from16 v10, p8

    move-object/from16 v11, p9

    move-object/from16 v12, p10

    move-object/from16 v13, p11

    move-object/from16 v14, p12

    move-object/from16 v15, p13

    move-wide/from16 v16, p14

    move-wide/from16 v18, p16

    move-object/from16 v20, p18

    move-object/from16 v21, p19

    invoke-direct/range {v1 .. v21}, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;-><init>(IJLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;Landroid/net/Uri;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;JJLcom/google/android/apps/chrome/snapshot/SnapshotJobState;Lcom/google/android/apps/chrome/snapshot/PageUrlJobState;)V

    .line 184
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 120
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 121
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mId:I

    .line 122
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mCreateTime:J

    .line 123
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mSnapshotId:Ljava/lang/String;

    .line 124
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mPrinterId:Ljava/lang/String;

    .line 125
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mJobId:Ljava/lang/String;

    .line 126
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mPageUrlJobId:Ljava/lang/String;

    .line 127
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mTitle:Ljava/lang/String;

    .line 128
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 129
    if-nez v0, :cond_0

    move-object v0, v1

    :goto_0
    iput-object v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mUri:Landroid/net/Uri;

    .line 130
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 131
    if-nez v0, :cond_1

    move-object v0, v1

    :goto_1
    iput-object v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mDownloadUri:Landroid/net/Uri;

    .line 132
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 133
    if-nez v0, :cond_2

    move-object v0, v1

    :goto_2
    iput-object v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mPageUrlDownloadUri:Landroid/net/Uri;

    .line 135
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mMimeType:Ljava/lang/String;

    .line 136
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mPageUrlMimeType:Ljava/lang/String;

    .line 137
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 138
    if-nez v0, :cond_3

    move-object v0, v1

    :goto_3
    iput-object v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mLocalUri:Landroid/net/Uri;

    .line 139
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mDownloadId:J

    .line 140
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mPageUrlDownloadId:J

    .line 141
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 142
    if-nez v0, :cond_4

    move-object v0, v1

    :goto_4
    iput-object v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mState:Lcom/google/android/apps/chrome/snapshot/SnapshotJobState;

    .line 143
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 144
    if-nez v0, :cond_5

    :goto_5
    iput-object v1, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mPageUrlState:Lcom/google/android/apps/chrome/snapshot/PageUrlJobState;

    .line 146
    return-void

    .line 129
    :cond_0
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    goto :goto_0

    .line 131
    :cond_1
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    goto :goto_1

    .line 133
    :cond_2
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    goto :goto_2

    .line 138
    :cond_3
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    goto :goto_3

    .line 142
    :cond_4
    invoke-static {v0}, Lcom/google/android/apps/chrome/snapshot/SnapshotJobState;->fromValue(Ljava/lang/String;)Lcom/google/android/apps/chrome/snapshot/SnapshotJobState;

    move-result-object v0

    goto :goto_4

    .line 144
    :cond_5
    invoke-static {v0}, Lcom/google/android/apps/chrome/snapshot/PageUrlJobState;->fromValue(Ljava/lang/String;)Lcom/google/android/apps/chrome/snapshot/PageUrlJobState;

    move-result-object v1

    goto :goto_5
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$1;)V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method static synthetic access$1000(Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 17
    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mDownloadUri:Landroid/net/Uri;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 17
    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mPageUrlDownloadUri:Landroid/net/Uri;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 17
    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mMimeType:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 17
    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mPageUrlMimeType:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 17
    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mLocalUri:Landroid/net/Uri;

    return-object v0
.end method

.method static synthetic access$1500(Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;)J
    .locals 2

    .prologue
    .line 17
    iget-wide v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mDownloadId:J

    return-wide v0
.end method

.method static synthetic access$1600(Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;)J
    .locals 2

    .prologue
    .line 17
    iget-wide v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mPageUrlDownloadId:J

    return-wide v0
.end method

.method static synthetic access$1700(Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;)Lcom/google/android/apps/chrome/snapshot/SnapshotJobState;
    .locals 1

    .prologue
    .line 17
    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mState:Lcom/google/android/apps/chrome/snapshot/SnapshotJobState;

    return-object v0
.end method

.method static synthetic access$1800(Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;)Lcom/google/android/apps/chrome/snapshot/PageUrlJobState;
    .locals 1

    .prologue
    .line 17
    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mPageUrlState:Lcom/google/android/apps/chrome/snapshot/PageUrlJobState;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;)I
    .locals 1

    .prologue
    .line 17
    iget v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mId:I

    return v0
.end method

.method static synthetic access$300(Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;)J
    .locals 2

    .prologue
    .line 17
    iget-wide v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mCreateTime:J

    return-wide v0
.end method

.method static synthetic access$400(Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 17
    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mSnapshotId:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 17
    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mPrinterId:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 17
    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mJobId:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$700(Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 17
    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mPageUrlJobId:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$800(Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 17
    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mTitle:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$900(Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 17
    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mUri:Landroid/net/Uri;

    return-object v0
.end method

.method static createBuilder()Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;
    .locals 1

    .prologue
    .line 292
    new-instance v0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;

    invoke-direct {v0}, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;-><init>()V

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->builder()Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method builder()Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;
    .locals 2

    .prologue
    .line 288
    new-instance v0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;-><init>(Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$1;)V

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 263
    const/4 v0, 0x0

    return v0
.end method

.method public getCreateTime()J
    .locals 2

    .prologue
    .line 198
    iget-wide v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mCreateTime:J

    return-wide v0
.end method

.method public getDownloadId()J
    .locals 2

    .prologue
    .line 246
    iget-wide v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mDownloadId:J

    return-wide v0
.end method

.method public getDownloadUri()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 226
    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mDownloadUri:Landroid/net/Uri;

    return-object v0
.end method

.method public getId()I
    .locals 1

    .prologue
    .line 194
    iget v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mId:I

    return v0
.end method

.method public getJobId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 210
    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mJobId:Ljava/lang/String;

    return-object v0
.end method

.method public getLocalUri()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 242
    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mLocalUri:Landroid/net/Uri;

    return-object v0
.end method

.method public getMimeType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 234
    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mMimeType:Ljava/lang/String;

    return-object v0
.end method

.method public getPageUri()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 222
    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mUri:Landroid/net/Uri;

    return-object v0
.end method

.method public getPageUrlDownloadId()J
    .locals 2

    .prologue
    .line 250
    iget-wide v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mPageUrlDownloadId:J

    return-wide v0
.end method

.method public getPageUrlDownloadUri()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 230
    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mPageUrlDownloadUri:Landroid/net/Uri;

    return-object v0
.end method

.method public getPageUrlJobId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 214
    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mPageUrlJobId:Ljava/lang/String;

    return-object v0
.end method

.method public getPageUrlMimeType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 238
    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mPageUrlMimeType:Ljava/lang/String;

    return-object v0
.end method

.method public getPageUrlState()Lcom/google/android/apps/chrome/snapshot/PageUrlJobState;
    .locals 1

    .prologue
    .line 258
    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mPageUrlState:Lcom/google/android/apps/chrome/snapshot/PageUrlJobState;

    return-object v0
.end method

.method public getPrinterId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 206
    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mPrinterId:Ljava/lang/String;

    return-object v0
.end method

.method public getSnapshotId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 202
    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mSnapshotId:Ljava/lang/String;

    return-object v0
.end method

.method public getState()Lcom/google/android/apps/chrome/snapshot/SnapshotJobState;
    .locals 1

    .prologue
    .line 254
    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mState:Lcom/google/android/apps/chrome/snapshot/SnapshotJobState;

    return-object v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 218
    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mTitle:Ljava/lang/String;

    return-object v0
.end method

.method public setId(I)V
    .locals 0

    .prologue
    .line 190
    iput p1, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mId:I

    .line 191
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    .prologue
    const/16 v4, 0x27

    .line 518
    new-instance v0, Ljava/lang/StringBuilder;

    const-string/jumbo v1, "SnapshotDocument{mId="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mId:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", mCreateTime="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mCreateTime:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", mSnapshotId=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mSnapshotId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", mPrinterId=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mPrinterId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", mJobId=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mJobId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", mPageUrlJobId=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mPageUrlJobId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", mTitle=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mTitle:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", mUri="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mUri:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", mDownloadUri="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mDownloadUri:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", mPageUrlDownloadUri="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mPageUrlDownloadUri:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", mMimeType=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mMimeType:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", mPageUrlMimeType=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mPageUrlMimeType:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", mLocalUri="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mLocalUri:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", mDownloadId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mDownloadId:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", mPageUrlDownloadId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mPageUrlDownloadId:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", mState=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mState:Lcom/google/android/apps/chrome/snapshot/SnapshotJobState;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", mPageUrlState=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mPageUrlState:Lcom/google/android/apps/chrome/snapshot/PageUrlJobState;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 268
    iget v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mId:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 269
    iget-wide v2, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mCreateTime:J

    invoke-virtual {p1, v2, v3}, Landroid/os/Parcel;->writeLong(J)V

    .line 270
    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mSnapshotId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 271
    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mPrinterId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 272
    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mJobId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 273
    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mPageUrlJobId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 274
    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mTitle:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 275
    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mUri:Landroid/net/Uri;

    if-nez v0, :cond_0

    move-object v0, v1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 276
    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mDownloadUri:Landroid/net/Uri;

    if-nez v0, :cond_1

    move-object v0, v1

    :goto_1
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 277
    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mPageUrlDownloadUri:Landroid/net/Uri;

    if-nez v0, :cond_2

    move-object v0, v1

    :goto_2
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 278
    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mMimeType:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 279
    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mPageUrlMimeType:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 280
    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mLocalUri:Landroid/net/Uri;

    if-nez v0, :cond_3

    move-object v0, v1

    :goto_3
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 281
    iget-wide v2, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mDownloadId:J

    invoke-virtual {p1, v2, v3}, Landroid/os/Parcel;->writeLong(J)V

    .line 282
    iget-wide v2, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mPageUrlDownloadId:J

    invoke-virtual {p1, v2, v3}, Landroid/os/Parcel;->writeLong(J)V

    .line 283
    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mState:Lcom/google/android/apps/chrome/snapshot/SnapshotJobState;

    if-nez v0, :cond_4

    move-object v0, v1

    :goto_4
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 284
    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mPageUrlState:Lcom/google/android/apps/chrome/snapshot/PageUrlJobState;

    if-nez v0, :cond_5

    :goto_5
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 285
    return-void

    .line 275
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mUri:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 276
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mDownloadUri:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 277
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mPageUrlDownloadUri:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    .line 280
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mLocalUri:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_3

    .line 283
    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mState:Lcom/google/android/apps/chrome/snapshot/SnapshotJobState;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/snapshot/SnapshotJobState;->toValue()Ljava/lang/String;

    move-result-object v0

    goto :goto_4

    .line 284
    :cond_5
    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->mPageUrlState:Lcom/google/android/apps/chrome/snapshot/PageUrlJobState;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/snapshot/PageUrlJobState;->toValue()Ljava/lang/String;

    move-result-object v1

    goto :goto_5
.end method
