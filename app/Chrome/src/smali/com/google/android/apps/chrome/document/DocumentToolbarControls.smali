.class public Lcom/google/android/apps/chrome/document/DocumentToolbarControls;
.super Ljava/lang/Object;
.source "DocumentToolbarControls.java"

# interfaces
.implements Lcom/google/android/apps/chrome/document/DocumentToolbarHelper$ToolbarControls;


# instance fields
.field private final mProgressBar:Landroid/widget/ProgressBar;

.field private final mTab:Lorg/chromium/chrome/browser/Tab;

.field private final mTabObserver:Lorg/chromium/chrome/browser/TabObserver;

.field private final mTitleBar:Lcom/google/android/apps/chrome/document/TitleBar;

.field private final mTitleBarListener:Lcom/google/android/apps/chrome/document/TitleBar$TitleBarListener;


# direct methods
.method public constructor <init>(Landroid/view/View;Lcom/google/android/apps/chrome/ChromeActivity;Lorg/chromium/chrome/browser/appmenu/AppMenuHandler;Lcom/google/android/apps/chrome/document/TitleBar$DocumentDelegate;Lcom/google/android/apps/chrome/document/TitleBar$TitleBarListener;)V
    .locals 2

    .prologue
    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    invoke-virtual {p2}, Lcom/google/android/apps/chrome/ChromeActivity;->getCurrentTab()Lcom/google/android/apps/chrome/tab/ChromeTab;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentToolbarControls;->mTab:Lorg/chromium/chrome/browser/Tab;

    .line 46
    sget v0, Lcom/google/android/apps/chrome/R$id;->titlebar:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/document/TitleBar;

    iput-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentToolbarControls;->mTitleBar:Lcom/google/android/apps/chrome/document/TitleBar;

    .line 47
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentToolbarControls;->mTitleBar:Lcom/google/android/apps/chrome/document/TitleBar;

    iget-object v1, p0, Lcom/google/android/apps/chrome/document/DocumentToolbarControls;->mTab:Lorg/chromium/chrome/browser/Tab;

    invoke-virtual {v1}, Lorg/chromium/chrome/browser/Tab;->getUrl()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p4, p3, v1}, Lcom/google/android/apps/chrome/document/TitleBar;->initialize(Lcom/google/android/apps/chrome/document/TitleBar$DocumentDelegate;Lorg/chromium/chrome/browser/appmenu/AppMenuHandler;Ljava/lang/String;)V

    .line 51
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentToolbarControls;->mTitleBar:Lcom/google/android/apps/chrome/document/TitleBar;

    invoke-virtual {v0, p5}, Lcom/google/android/apps/chrome/document/TitleBar;->addListener(Lcom/google/android/apps/chrome/document/TitleBar$TitleBarListener;)V

    .line 53
    new-instance v0, Lcom/google/android/apps/chrome/document/DocumentToolbarControls$1;

    invoke-direct {v0, p0, p2}, Lcom/google/android/apps/chrome/document/DocumentToolbarControls$1;-><init>(Lcom/google/android/apps/chrome/document/DocumentToolbarControls;Lcom/google/android/apps/chrome/ChromeActivity;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentToolbarControls;->mTitleBarListener:Lcom/google/android/apps/chrome/document/TitleBar$TitleBarListener;

    .line 71
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentToolbarControls;->mTitleBar:Lcom/google/android/apps/chrome/document/TitleBar;

    iget-object v1, p0, Lcom/google/android/apps/chrome/document/DocumentToolbarControls;->mTitleBarListener:Lcom/google/android/apps/chrome/document/TitleBar$TitleBarListener;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/document/TitleBar;->addListener(Lcom/google/android/apps/chrome/document/TitleBar$TitleBarListener;)V

    .line 73
    sget v0, Lcom/google/android/apps/chrome/R$id;->progress:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentToolbarControls;->mProgressBar:Landroid/widget/ProgressBar;

    .line 74
    invoke-direct {p0}, Lcom/google/android/apps/chrome/document/DocumentToolbarControls;->initializeProgressBar()V

    .line 76
    new-instance v0, Lcom/google/android/apps/chrome/document/DocumentToolbarControls$2;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/document/DocumentToolbarControls$2;-><init>(Lcom/google/android/apps/chrome/document/DocumentToolbarControls;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentToolbarControls;->mTabObserver:Lorg/chromium/chrome/browser/TabObserver;

    .line 103
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentToolbarControls;->mTab:Lorg/chromium/chrome/browser/Tab;

    iget-object v1, p0, Lcom/google/android/apps/chrome/document/DocumentToolbarControls;->mTabObserver:Lorg/chromium/chrome/browser/TabObserver;

    invoke-virtual {v0, v1}, Lorg/chromium/chrome/browser/Tab;->addObserver(Lorg/chromium/chrome/browser/TabObserver;)V

    .line 104
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/chrome/document/DocumentToolbarControls;)Lorg/chromium/chrome/browser/Tab;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentToolbarControls;->mTab:Lorg/chromium/chrome/browser/Tab;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/chrome/document/DocumentToolbarControls;)Lcom/google/android/apps/chrome/document/TitleBar;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentToolbarControls;->mTitleBar:Lcom/google/android/apps/chrome/document/TitleBar;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/apps/chrome/document/DocumentToolbarControls;)Landroid/widget/ProgressBar;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentToolbarControls;->mProgressBar:Landroid/widget/ProgressBar;

    return-object v0
.end method

.method private initializeProgressBar()V
    .locals 4

    .prologue
    .line 133
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentToolbarControls;->mProgressBar:Landroid/widget/ProgressBar;

    invoke-virtual {v0}, Landroid/widget/ProgressBar;->getProgressDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 134
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentToolbarControls;->mProgressBar:Landroid/widget/ProgressBar;

    invoke-virtual {v0}, Landroid/widget/ProgressBar;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    .line 136
    iget-object v2, p0, Lcom/google/android/apps/chrome/document/DocumentToolbarControls;->mProgressBar:Landroid/widget/ProgressBar;

    invoke-virtual {v2}, Landroid/widget/ProgressBar;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/google/android/apps/chrome/R$dimen;->document_titlebar_height:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v2

    .line 138
    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    sub-int v1, v2, v1

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->topMargin:I

    .line 139
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentToolbarControls;->mProgressBar:Landroid/widget/ProgressBar;

    invoke-virtual {v0}, Landroid/widget/ProgressBar;->requestLayout()V

    .line 140
    return-void
.end method


# virtual methods
.method public destroy()V
    .locals 2

    .prologue
    .line 123
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentToolbarControls;->mTab:Lorg/chromium/chrome/browser/Tab;

    iget-object v1, p0, Lcom/google/android/apps/chrome/document/DocumentToolbarControls;->mTabObserver:Lorg/chromium/chrome/browser/TabObserver;

    invoke-virtual {v0, v1}, Lorg/chromium/chrome/browser/Tab;->removeObserver(Lorg/chromium/chrome/browser/TabObserver;)V

    .line 124
    return-void
.end method

.method public focusUrlBar()V
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentToolbarControls;->mTitleBar:Lcom/google/android/apps/chrome/document/TitleBar;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/document/TitleBar;->animateTransitionToSearch()V

    .line 114
    return-void
.end method

.method public getMenuAnchor()Landroid/view/View;
    .locals 1

    .prologue
    .line 108
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentToolbarControls;->mTitleBar:Lcom/google/android/apps/chrome/document/TitleBar;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/document/TitleBar;->getMenuAnchor()Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 128
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentToolbarControls;->mTitleBar:Lcom/google/android/apps/chrome/document/TitleBar;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/document/TitleBar;->animateTransitionFromSearch()V

    .line 129
    return-void
.end method

.method public setThemeColor(I)V
    .locals 1

    .prologue
    .line 118
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentToolbarControls;->mTitleBar:Lcom/google/android/apps/chrome/document/TitleBar;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/document/TitleBar;->updateTheme(I)V

    .line 119
    return-void
.end method
