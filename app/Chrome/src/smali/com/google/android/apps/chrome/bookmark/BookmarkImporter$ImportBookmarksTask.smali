.class Lcom/google/android/apps/chrome/bookmark/BookmarkImporter$ImportBookmarksTask;
.super Landroid/os/AsyncTask;
.source "BookmarkImporter.java"


# instance fields
.field private final mBookmarksImportedListener:Lcom/google/android/apps/chrome/bookmark/BookmarkImporter$OnBookmarksImportedListener;

.field final synthetic this$0:Lcom/google/android/apps/chrome/bookmark/BookmarkImporter;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/bookmark/BookmarkImporter;Lcom/google/android/apps/chrome/bookmark/BookmarkImporter$OnBookmarksImportedListener;)V
    .locals 0

    .prologue
    .line 114
    iput-object p1, p0, Lcom/google/android/apps/chrome/bookmark/BookmarkImporter$ImportBookmarksTask;->this$0:Lcom/google/android/apps/chrome/bookmark/BookmarkImporter;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 115
    iput-object p2, p0, Lcom/google/android/apps/chrome/bookmark/BookmarkImporter$ImportBookmarksTask;->mBookmarksImportedListener:Lcom/google/android/apps/chrome/bookmark/BookmarkImporter$OnBookmarksImportedListener;

    .line 116
    return-void
.end method

.method private alreadyExists(Lcom/google/android/apps/chrome/bookmark/BookmarkImporter$Bookmark;)Z
    .locals 8

    .prologue
    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 237
    iget-boolean v0, p1, Lcom/google/android/apps/chrome/bookmark/BookmarkImporter$Bookmark;->isFolder:Z

    if-eqz v0, :cond_1

    .line 247
    :cond_0
    :goto_0
    return v7

    .line 239
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/bookmark/BookmarkImporter$ImportBookmarksTask;->this$0:Lcom/google/android/apps/chrome/bookmark/BookmarkImporter;

    iget-object v0, v0, Lcom/google/android/apps/chrome/bookmark/BookmarkImporter;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/bookmark/BookmarkImporter$ImportBookmarksTask;->this$0:Lcom/google/android/apps/chrome/bookmark/BookmarkImporter;

    iget-object v1, v1, Lcom/google/android/apps/chrome/bookmark/BookmarkImporter;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->getBookmarksApiUri(Landroid/content/Context;)Landroid/net/Uri;

    move-result-object v1

    # getter for: Lcom/google/android/apps/chrome/bookmark/BookmarkImporter;->EXISTS_PROJECTION:[Ljava/lang/String;
    invoke-static {}, Lcom/google/android/apps/chrome/bookmark/BookmarkImporter;->access$100()[Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    # getter for: Lcom/google/android/apps/chrome/bookmark/BookmarkImporter;->SELECT_IS_BOOKMARK:Ljava/lang/String;
    invoke-static {}, Lcom/google/android/apps/chrome/bookmark/BookmarkImporter;->access$200()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, " AND url=?"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    new-array v4, v6, [Ljava/lang/String;

    iget-object v5, p1, Lcom/google/android/apps/chrome/bookmark/BookmarkImporter$Bookmark;->url:Ljava/lang/String;

    aput-object v5, v4, v7

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 242
    if-eqz v1, :cond_0

    .line 243
    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_2

    move v0, v6

    .line 244
    :goto_1
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    move v7, v0

    .line 245
    goto :goto_0

    :cond_2
    move v0, v7

    .line 243
    goto :goto_1
.end method

.method private createRootFolderBookmark()Lcom/google/android/apps/chrome/bookmark/BookmarkImporter$Bookmark;
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 267
    new-instance v0, Lcom/google/android/apps/chrome/bookmark/BookmarkImporter$Bookmark;

    invoke-direct {v0}, Lcom/google/android/apps/chrome/bookmark/BookmarkImporter$Bookmark;-><init>()V

    .line 268
    iput-wide v4, v0, Lcom/google/android/apps/chrome/bookmark/BookmarkImporter$Bookmark;->id:J

    .line 269
    iget-object v1, p0, Lcom/google/android/apps/chrome/bookmark/BookmarkImporter$ImportBookmarksTask;->this$0:Lcom/google/android/apps/chrome/bookmark/BookmarkImporter;

    iget-object v1, v1, Lcom/google/android/apps/chrome/bookmark/BookmarkImporter;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lorg/chromium/chrome/browser/ChromeBrowserProviderClient;->getMobileBookmarksFolderId(Landroid/content/Context;)J

    move-result-wide v2

    iput-wide v2, v0, Lcom/google/android/apps/chrome/bookmark/BookmarkImporter$Bookmark;->nativeId:J

    .line 270
    iput-wide v4, v0, Lcom/google/android/apps/chrome/bookmark/BookmarkImporter$Bookmark;->parentId:J

    .line 271
    iput-object v0, v0, Lcom/google/android/apps/chrome/bookmark/BookmarkImporter$Bookmark;->parent:Lcom/google/android/apps/chrome/bookmark/BookmarkImporter$Bookmark;

    .line 272
    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/android/apps/chrome/bookmark/BookmarkImporter$Bookmark;->isFolder:Z

    .line 273
    return-object v0
.end method

.method private getBookmarkValues(Lcom/google/android/apps/chrome/bookmark/BookmarkImporter$Bookmark;)Landroid/content/ContentValues;
    .locals 6

    .prologue
    .line 214
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 215
    const-string/jumbo v1, "bookmark"

    # getter for: Lcom/google/android/apps/chrome/bookmark/BookmarkImporter;->VALUE_IS_BOOKMARK:Ljava/lang/Integer;
    invoke-static {}, Lcom/google/android/apps/chrome/bookmark/BookmarkImporter;->access$000()Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 216
    const-string/jumbo v1, "url"

    iget-object v2, p1, Lcom/google/android/apps/chrome/bookmark/BookmarkImporter$Bookmark;->url:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 217
    const-string/jumbo v1, "title"

    iget-object v2, p1, Lcom/google/android/apps/chrome/bookmark/BookmarkImporter$Bookmark;->title:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 218
    const-string/jumbo v1, "parentId"

    iget-object v2, p1, Lcom/google/android/apps/chrome/bookmark/BookmarkImporter$Bookmark;->parent:Lcom/google/android/apps/chrome/bookmark/BookmarkImporter$Bookmark;

    iget-wide v2, v2, Lcom/google/android/apps/chrome/bookmark/BookmarkImporter$Bookmark;->nativeId:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 219
    iget-object v1, p1, Lcom/google/android/apps/chrome/bookmark/BookmarkImporter$Bookmark;->created:Ljava/lang/Long;

    if-eqz v1, :cond_0

    const-string/jumbo v1, "created"

    iget-object v2, p1, Lcom/google/android/apps/chrome/bookmark/BookmarkImporter$Bookmark;->created:Ljava/lang/Long;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 220
    :cond_0
    iget-object v1, p1, Lcom/google/android/apps/chrome/bookmark/BookmarkImporter$Bookmark;->lastVisit:Ljava/lang/Long;

    if-eqz v1, :cond_1

    const-string/jumbo v1, "date"

    iget-object v2, p1, Lcom/google/android/apps/chrome/bookmark/BookmarkImporter$Bookmark;->lastVisit:Ljava/lang/Long;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 221
    :cond_1
    iget-object v1, p1, Lcom/google/android/apps/chrome/bookmark/BookmarkImporter$Bookmark;->visits:Ljava/lang/Long;

    if-eqz v1, :cond_2

    .line 224
    iget-object v1, p1, Lcom/google/android/apps/chrome/bookmark/BookmarkImporter$Bookmark;->created:Ljava/lang/Long;

    if-eqz v1, :cond_2

    iget-object v1, p1, Lcom/google/android/apps/chrome/bookmark/BookmarkImporter$Bookmark;->lastVisit:Ljava/lang/Long;

    if-eqz v1, :cond_2

    iget-object v1, p1, Lcom/google/android/apps/chrome/bookmark/BookmarkImporter$Bookmark;->visits:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    const-wide/16 v4, 0x2

    cmp-long v1, v2, v4

    if-lez v1, :cond_2

    iget-object v1, p1, Lcom/google/android/apps/chrome/bookmark/BookmarkImporter$Bookmark;->lastVisit:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    iget-object v1, p1, Lcom/google/android/apps/chrome/bookmark/BookmarkImporter$Bookmark;->created:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    sub-long/2addr v2, v4

    iget-object v1, p1, Lcom/google/android/apps/chrome/bookmark/BookmarkImporter$Bookmark;->visits:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    cmp-long v1, v2, v4

    if-lez v1, :cond_2

    .line 228
    const-string/jumbo v1, "visits"

    iget-object v2, p1, Lcom/google/android/apps/chrome/bookmark/BookmarkImporter$Bookmark;->visits:Ljava/lang/Long;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 231
    :cond_2
    iget-object v1, p1, Lcom/google/android/apps/chrome/bookmark/BookmarkImporter$Bookmark;->favicon:[B

    if-eqz v1, :cond_3

    const-string/jumbo v1, "favicon"

    iget-object v2, p1, Lcom/google/android/apps/chrome/bookmark/BookmarkImporter$Bookmark;->favicon:[B

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 232
    :cond_3
    return-object v0
.end method

.method private importBookmarkHierarchy(Lcom/google/android/apps/chrome/bookmark/BookmarkImporter$Bookmark;Lcom/google/android/apps/chrome/bookmark/BookmarkImporter$ImportResults;)V
    .locals 9

    .prologue
    const-wide/16 v4, 0x0

    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 278
    iget-boolean v0, p1, Lcom/google/android/apps/chrome/bookmark/BookmarkImporter$Bookmark;->processed:Z

    if-eqz v0, :cond_1

    .line 336
    :cond_0
    :goto_0
    return-void

    .line 279
    :cond_1
    iput-boolean v6, p1, Lcom/google/android/apps/chrome/bookmark/BookmarkImporter$Bookmark;->processed:Z

    .line 281
    iget-boolean v0, p1, Lcom/google/android/apps/chrome/bookmark/BookmarkImporter$Bookmark;->isFolder:Z

    if-eqz v0, :cond_5

    .line 282
    iget-wide v0, p1, Lcom/google/android/apps/chrome/bookmark/BookmarkImporter$Bookmark;->id:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_2

    .line 283
    iget-object v0, p0, Lcom/google/android/apps/chrome/bookmark/BookmarkImporter$ImportBookmarksTask;->this$0:Lcom/google/android/apps/chrome/bookmark/BookmarkImporter;

    iget-object v0, v0, Lcom/google/android/apps/chrome/bookmark/BookmarkImporter;->mContext:Landroid/content/Context;

    iget-object v1, p1, Lcom/google/android/apps/chrome/bookmark/BookmarkImporter$Bookmark;->title:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/android/apps/chrome/bookmark/BookmarkImporter$Bookmark;->parent:Lcom/google/android/apps/chrome/bookmark/BookmarkImporter$Bookmark;

    iget-wide v2, v2, Lcom/google/android/apps/chrome/bookmark/BookmarkImporter$Bookmark;->nativeId:J

    invoke-static {v0, v1, v2, v3}, Lorg/chromium/chrome/browser/ChromeBrowserProviderClient;->createBookmarksFolderOnce(Landroid/content/Context;Ljava/lang/String;J)J

    move-result-wide v0

    iput-wide v0, p1, Lcom/google/android/apps/chrome/bookmark/BookmarkImporter$Bookmark;->nativeId:J

    .line 285
    iget v0, p2, Lcom/google/android/apps/chrome/bookmark/BookmarkImporter$ImportResults;->numImported:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p2, Lcom/google/android/apps/chrome/bookmark/BookmarkImporter$ImportResults;->numImported:I

    .line 288
    :cond_2
    iget-wide v0, p1, Lcom/google/android/apps/chrome/bookmark/BookmarkImporter$Bookmark;->nativeId:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_3

    iget-wide v0, p1, Lcom/google/android/apps/chrome/bookmark/BookmarkImporter$Bookmark;->id:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_3

    .line 290
    const-string/jumbo v0, "BookmarkImporter"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "Error creating the folder \'"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p1, Lcom/google/android/apps/chrome/bookmark/BookmarkImporter$Bookmark;->title:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "\'. Skipping entries."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 295
    :cond_3
    iget-object v0, p1, Lcom/google/android/apps/chrome/bookmark/BookmarkImporter$Bookmark;->entries:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/bookmark/BookmarkImporter$Bookmark;

    .line 296
    iget-object v2, v0, Lcom/google/android/apps/chrome/bookmark/BookmarkImporter$Bookmark;->parent:Lcom/google/android/apps/chrome/bookmark/BookmarkImporter$Bookmark;

    if-eq v2, p1, :cond_4

    .line 297
    const-string/jumbo v0, "BookmarkImporter"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "Hierarchy error in bookmark \'"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p1, Lcom/google/android/apps/chrome/bookmark/BookmarkImporter$Bookmark;->title:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "\'. Skipping."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 301
    :cond_4
    invoke-direct {p0, v0, p2}, Lcom/google/android/apps/chrome/bookmark/BookmarkImporter$ImportBookmarksTask;->importBookmarkHierarchy(Lcom/google/android/apps/chrome/bookmark/BookmarkImporter$Bookmark;Lcom/google/android/apps/chrome/bookmark/BookmarkImporter$ImportResults;)V

    goto :goto_1

    .line 304
    :cond_5
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/bookmark/BookmarkImporter$ImportBookmarksTask;->sanitizeBookmarkDates(Lcom/google/android/apps/chrome/bookmark/BookmarkImporter$Bookmark;)V

    .line 305
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/bookmark/BookmarkImporter$ImportBookmarksTask;->getBookmarkValues(Lcom/google/android/apps/chrome/bookmark/BookmarkImporter$Bookmark;)Landroid/content/ContentValues;

    move-result-object v8

    .line 308
    const/4 v0, 0x1

    :try_start_0
    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    iget-object v1, p1, Lcom/google/android/apps/chrome/bookmark/BookmarkImporter$Bookmark;->url:Ljava/lang/String;

    aput-object v1, v4, v0

    .line 309
    iget-object v0, p0, Lcom/google/android/apps/chrome/bookmark/BookmarkImporter$ImportBookmarksTask;->this$0:Lcom/google/android/apps/chrome/bookmark/BookmarkImporter;

    iget-object v0, v0, Lcom/google/android/apps/chrome/bookmark/BookmarkImporter;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->getBookmarksApiUri(Landroid/content/Context;)Landroid/net/Uri;

    move-result-object v1

    .line 310
    iget-object v0, p0, Lcom/google/android/apps/chrome/bookmark/BookmarkImporter$ImportBookmarksTask;->this$0:Lcom/google/android/apps/chrome/bookmark/BookmarkImporter;

    iget-object v0, v0, Lcom/google/android/apps/chrome/bookmark/BookmarkImporter;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v2, 0x0

    const-string/jumbo v3, "url=?"

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    .line 312
    if-eqz v2, :cond_7

    invoke-interface {v2}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_7

    move v0, v6

    .line 313
    :goto_2
    if-eqz v2, :cond_6

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 315
    :cond_6
    if-eqz v0, :cond_8

    .line 317
    iget-object v0, p0, Lcom/google/android/apps/chrome/bookmark/BookmarkImporter$ImportBookmarksTask;->this$0:Lcom/google/android/apps/chrome/bookmark/BookmarkImporter;

    iget-object v0, v0, Lcom/google/android/apps/chrome/bookmark/BookmarkImporter;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v2, "url=?"

    invoke-virtual {v0, v1, v8, v2, v4}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_9

    .line 319
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "Couldn\'t update the existing history information"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 331
    :catch_0
    move-exception v0

    .line 332
    const-string/jumbo v1, "BookmarkImporter"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "Error inserting bookmark "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p1, Lcom/google/android/apps/chrome/bookmark/BookmarkImporter$Bookmark;->title:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ": "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_7
    move v0, v7

    .line 312
    goto :goto_2

    .line 324
    :cond_8
    :try_start_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/bookmark/BookmarkImporter$ImportBookmarksTask;->this$0:Lcom/google/android/apps/chrome/bookmark/BookmarkImporter;

    iget-object v0, v0, Lcom/google/android/apps/chrome/bookmark/BookmarkImporter;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-virtual {v0, v1, v8}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v0

    if-nez v0, :cond_9

    .line 326
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "Couldn\'t insert the bookmark"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 330
    :cond_9
    iget v0, p2, Lcom/google/android/apps/chrome/bookmark/BookmarkImporter$ImportResults;->numImported:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p2, Lcom/google/android/apps/chrome/bookmark/BookmarkImporter$ImportResults;->numImported:I
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0
.end method

.method private importFromIterator(Lcom/google/android/apps/chrome/bookmark/BookmarkImporter$BookmarkIterator;)Lcom/google/android/apps/chrome/bookmark/BookmarkImporter$ImportResults;
    .locals 10

    .prologue
    const/4 v1, 0x0

    .line 151
    if-nez p1, :cond_1

    move-object v0, v1

    .line 209
    :cond_0
    :goto_0
    return-object v0

    .line 154
    :cond_1
    :try_start_0
    new-instance v3, Ljava/util/LinkedHashMap;

    invoke-direct {v3}, Ljava/util/LinkedHashMap;-><init>()V

    .line 155
    new-instance v4, Ljava/util/HashSet;

    invoke-direct {v4}, Ljava/util/HashSet;-><init>()V

    .line 159
    invoke-direct {p0}, Lcom/google/android/apps/chrome/bookmark/BookmarkImporter$ImportBookmarksTask;->createRootFolderBookmark()Lcom/google/android/apps/chrome/bookmark/BookmarkImporter$Bookmark;

    move-result-object v5

    .line 160
    const-wide/16 v6, 0x0

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v3, v0, v5}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 162
    const/4 v0, 0x0

    move v2, v0

    .line 163
    :cond_2
    :goto_1
    invoke-interface {p1}, Lcom/google/android/apps/chrome/bookmark/BookmarkImporter$BookmarkIterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 164
    invoke-interface {p1}, Lcom/google/android/apps/chrome/bookmark/BookmarkImporter$BookmarkIterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/bookmark/BookmarkImporter$Bookmark;

    .line 165
    if-nez v0, :cond_3

    .line 166
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    .line 167
    goto :goto_1

    .line 171
    :cond_3
    iget-wide v6, v0, Lcom/google/android/apps/chrome/bookmark/BookmarkImporter$Bookmark;->id:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/util/LinkedHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 172
    const-string/jumbo v6, "BookmarkImporter"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string/jumbo v8, "Duplicate bookmark id: "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v8, v0, Lcom/google/android/apps/chrome/bookmark/BookmarkImporter$Bookmark;->id:J

    invoke-virtual {v7, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v7, ". Dropping bookmark."

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v6, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 174
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    .line 175
    goto :goto_1

    .line 179
    :cond_4
    iget-boolean v6, v0, Lcom/google/android/apps/chrome/bookmark/BookmarkImporter$Bookmark;->isFolder:Z

    if-nez v6, :cond_5

    iget-object v6, v0, Lcom/google/android/apps/chrome/bookmark/BookmarkImporter$Bookmark;->url:Ljava/lang/String;

    invoke-virtual {v4, v6}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 180
    const-string/jumbo v6, "BookmarkImporter"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string/jumbo v8, "More than one bookmark pointing to "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v0, v0, Lcom/google/android/apps/chrome/bookmark/BookmarkImporter$Bookmark;->url:Ljava/lang/String;

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v7, ". Keeping only the first one for consistency with Chromium."

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v6, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 207
    :catch_0
    move-exception v0

    .line 208
    const-string/jumbo v2, "BookmarkImporter"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "Unexpected exception while importing bookmarks: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, v1

    .line 209
    goto/16 :goto_0

    .line 186
    :cond_5
    :try_start_1
    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/bookmark/BookmarkImporter$ImportBookmarksTask;->alreadyExists(Lcom/google/android/apps/chrome/bookmark/BookmarkImporter$Bookmark;)Z

    move-result v6

    if-nez v6, :cond_2

    .line 188
    iget-wide v6, v0, Lcom/google/android/apps/chrome/bookmark/BookmarkImporter$Bookmark;->id:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v3, v6, v0}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 189
    iget-object v0, v0, Lcom/google/android/apps/chrome/bookmark/BookmarkImporter$Bookmark;->url:Ljava/lang/String;

    invoke-virtual {v4, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 191
    :cond_6
    invoke-interface {p1}, Lcom/google/android/apps/chrome/bookmark/BookmarkImporter$BookmarkIterator;->close()V

    .line 194
    new-instance v0, Lcom/google/android/apps/chrome/bookmark/BookmarkImporter$ImportResults;

    invoke-direct {v0}, Lcom/google/android/apps/chrome/bookmark/BookmarkImporter$ImportResults;-><init>()V

    .line 195
    iget-wide v6, v5, Lcom/google/android/apps/chrome/bookmark/BookmarkImporter$Bookmark;->nativeId:J

    iput-wide v6, v0, Lcom/google/android/apps/chrome/bookmark/BookmarkImporter$ImportResults;->rootFolderId:J

    .line 196
    invoke-virtual {v3}, Ljava/util/LinkedHashMap;->size()I

    move-result v4

    add-int/2addr v4, v2

    add-int/lit8 v4, v4, -0x1

    iput v4, v0, Lcom/google/android/apps/chrome/bookmark/BookmarkImporter$ImportResults;->newBookmarks:I

    .line 197
    iget v4, v0, Lcom/google/android/apps/chrome/bookmark/BookmarkImporter$ImportResults;->newBookmarks:I

    if-eqz v4, :cond_0

    .line 200
    invoke-virtual {v3}, Ljava/util/LinkedHashMap;->size()I

    move-result v4

    const/4 v6, 0x1

    if-ne v4, v6, :cond_7

    if-lez v2, :cond_7

    move-object v0, v1

    goto/16 :goto_0

    .line 203
    :cond_7
    invoke-direct {p0, v3}, Lcom/google/android/apps/chrome/bookmark/BookmarkImporter$ImportBookmarksTask;->recreateFolderHierarchy(Ljava/util/LinkedHashMap;)V

    .line 204
    invoke-direct {p0, v5, v0}, Lcom/google/android/apps/chrome/bookmark/BookmarkImporter$ImportBookmarksTask;->importBookmarkHierarchy(Lcom/google/android/apps/chrome/bookmark/BookmarkImporter$Bookmark;Lcom/google/android/apps/chrome/bookmark/BookmarkImporter$ImportResults;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0
.end method

.method private recreateFolderHierarchy(Ljava/util/LinkedHashMap;)V
    .locals 10

    .prologue
    const-wide/16 v8, 0x0

    .line 251
    invoke-virtual {p1}, Ljava/util/LinkedHashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/bookmark/BookmarkImporter$Bookmark;

    .line 252
    iget-wide v4, v0, Lcom/google/android/apps/chrome/bookmark/BookmarkImporter$Bookmark;->id:J

    cmp-long v1, v4, v8

    if-eqz v1, :cond_0

    .line 255
    iget-wide v4, v0, Lcom/google/android/apps/chrome/bookmark/BookmarkImporter$Bookmark;->parentId:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/util/LinkedHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-wide v4, v0, Lcom/google/android/apps/chrome/bookmark/BookmarkImporter$Bookmark;->parentId:J

    iget-wide v6, v0, Lcom/google/android/apps/chrome/bookmark/BookmarkImporter$Bookmark;->id:J

    cmp-long v1, v4, v6

    if-nez v1, :cond_2

    .line 256
    :cond_1
    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/chrome/bookmark/BookmarkImporter$Bookmark;

    iput-object v1, v0, Lcom/google/android/apps/chrome/bookmark/BookmarkImporter$Bookmark;->parent:Lcom/google/android/apps/chrome/bookmark/BookmarkImporter$Bookmark;

    .line 257
    iget-object v1, v0, Lcom/google/android/apps/chrome/bookmark/BookmarkImporter$Bookmark;->parent:Lcom/google/android/apps/chrome/bookmark/BookmarkImporter$Bookmark;

    iget-object v1, v1, Lcom/google/android/apps/chrome/bookmark/BookmarkImporter$Bookmark;->entries:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 261
    :cond_2
    iget-wide v4, v0, Lcom/google/android/apps/chrome/bookmark/BookmarkImporter$Bookmark;->parentId:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/chrome/bookmark/BookmarkImporter$Bookmark;

    iput-object v1, v0, Lcom/google/android/apps/chrome/bookmark/BookmarkImporter$Bookmark;->parent:Lcom/google/android/apps/chrome/bookmark/BookmarkImporter$Bookmark;

    .line 262
    iget-object v1, v0, Lcom/google/android/apps/chrome/bookmark/BookmarkImporter$Bookmark;->parent:Lcom/google/android/apps/chrome/bookmark/BookmarkImporter$Bookmark;

    iget-object v1, v1, Lcom/google/android/apps/chrome/bookmark/BookmarkImporter$Bookmark;->entries:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 264
    :cond_3
    return-void
.end method

.method private sanitizeBookmarkDates(Lcom/google/android/apps/chrome/bookmark/BookmarkImporter$Bookmark;)V
    .locals 4

    .prologue
    .line 341
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 342
    iget-object v2, p1, Lcom/google/android/apps/chrome/bookmark/BookmarkImporter$Bookmark;->created:Ljava/lang/Long;

    if-eqz v2, :cond_0

    iget-object v2, p1, Lcom/google/android/apps/chrome/bookmark/BookmarkImporter$Bookmark;->created:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    cmp-long v2, v2, v0

    if-lez v2, :cond_0

    .line 343
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    iput-object v2, p1, Lcom/google/android/apps/chrome/bookmark/BookmarkImporter$Bookmark;->created:Ljava/lang/Long;

    .line 346
    :cond_0
    iget-object v2, p1, Lcom/google/android/apps/chrome/bookmark/BookmarkImporter$Bookmark;->lastVisit:Ljava/lang/Long;

    if-eqz v2, :cond_1

    iget-object v2, p1, Lcom/google/android/apps/chrome/bookmark/BookmarkImporter$Bookmark;->lastVisit:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    cmp-long v2, v2, v0

    if-lez v2, :cond_1

    .line 347
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p1, Lcom/google/android/apps/chrome/bookmark/BookmarkImporter$Bookmark;->lastVisit:Ljava/lang/Long;

    .line 350
    :cond_1
    iget-object v0, p1, Lcom/google/android/apps/chrome/bookmark/BookmarkImporter$Bookmark;->created:Ljava/lang/Long;

    if-eqz v0, :cond_2

    iget-object v0, p1, Lcom/google/android/apps/chrome/bookmark/BookmarkImporter$Bookmark;->lastVisit:Ljava/lang/Long;

    if-eqz v0, :cond_2

    iget-object v0, p1, Lcom/google/android/apps/chrome/bookmark/BookmarkImporter$Bookmark;->created:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    iget-object v2, p1, Lcom/google/android/apps/chrome/bookmark/BookmarkImporter$Bookmark;->lastVisit:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-lez v0, :cond_2

    .line 352
    iget-object v0, p1, Lcom/google/android/apps/chrome/bookmark/BookmarkImporter$Bookmark;->lastVisit:Ljava/lang/Long;

    iput-object v0, p1, Lcom/google/android/apps/chrome/bookmark/BookmarkImporter$Bookmark;->created:Ljava/lang/Long;

    .line 356
    :cond_2
    iget-object v0, p1, Lcom/google/android/apps/chrome/bookmark/BookmarkImporter$Bookmark;->lastVisit:Ljava/lang/Long;

    if-eqz v0, :cond_3

    iget-object v0, p1, Lcom/google/android/apps/chrome/bookmark/BookmarkImporter$Bookmark;->created:Ljava/lang/Long;

    if-eqz v0, :cond_3

    iget-object v0, p1, Lcom/google/android/apps/chrome/bookmark/BookmarkImporter$Bookmark;->visits:Ljava/lang/Long;

    if-eqz v0, :cond_3

    .line 357
    iget-object v0, p1, Lcom/google/android/apps/chrome/bookmark/BookmarkImporter$Bookmark;->lastVisit:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    iget-object v2, p1, Lcom/google/android/apps/chrome/bookmark/BookmarkImporter$Bookmark;->created:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    sub-long/2addr v0, v2

    const-wide/16 v2, 0x1

    add-long/2addr v0, v2

    .line 358
    iget-object v2, p1, Lcom/google/android/apps/chrome/bookmark/BookmarkImporter$Bookmark;->visits:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    cmp-long v2, v2, v0

    if-lez v2, :cond_3

    .line 359
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p1, Lcom/google/android/apps/chrome/bookmark/BookmarkImporter$Bookmark;->visits:Ljava/lang/Long;

    .line 362
    :cond_3
    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Lcom/google/android/apps/chrome/bookmark/BookmarkImporter$ImportResults;
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 120
    :try_start_0
    iget-object v1, p0, Lcom/google/android/apps/chrome/bookmark/BookmarkImporter$ImportBookmarksTask;->this$0:Lcom/google/android/apps/chrome/bookmark/BookmarkImporter;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/bookmark/BookmarkImporter;->availableBookmarks()[Lcom/google/android/apps/chrome/bookmark/BookmarkImporter$BookmarkIterator;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    .line 129
    if-nez v3, :cond_1

    .line 130
    const-string/jumbo v1, "BookmarkImporter"

    const-string/jumbo v2, "No bookmark iterators found."

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 139
    :cond_0
    :goto_0
    return-object v0

    .line 123
    :catch_0
    move-exception v1

    .line 124
    const-string/jumbo v2, "BookmarkImporter"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "Unexpected exception while requesting available bookmarks: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 134
    :cond_1
    array-length v4, v3

    const/4 v1, 0x0

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_0

    aget-object v1, v3, v2

    .line 135
    invoke-direct {p0, v1}, Lcom/google/android/apps/chrome/bookmark/BookmarkImporter$ImportBookmarksTask;->importFromIterator(Lcom/google/android/apps/chrome/bookmark/BookmarkImporter$BookmarkIterator;)Lcom/google/android/apps/chrome/bookmark/BookmarkImporter$ImportResults;

    move-result-object v1

    .line 136
    if-eqz v1, :cond_2

    move-object v0, v1

    goto :goto_0

    .line 134
    :cond_2
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_1
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 111
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/bookmark/BookmarkImporter$ImportBookmarksTask;->doInBackground([Ljava/lang/Void;)Lcom/google/android/apps/chrome/bookmark/BookmarkImporter$ImportResults;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Lcom/google/android/apps/chrome/bookmark/BookmarkImporter$ImportResults;)V
    .locals 1

    .prologue
    .line 144
    iget-object v0, p0, Lcom/google/android/apps/chrome/bookmark/BookmarkImporter$ImportBookmarksTask;->mBookmarksImportedListener:Lcom/google/android/apps/chrome/bookmark/BookmarkImporter$OnBookmarksImportedListener;

    if-eqz v0, :cond_0

    .line 145
    iget-object v0, p0, Lcom/google/android/apps/chrome/bookmark/BookmarkImporter$ImportBookmarksTask;->mBookmarksImportedListener:Lcom/google/android/apps/chrome/bookmark/BookmarkImporter$OnBookmarksImportedListener;

    invoke-interface {v0, p1}, Lcom/google/android/apps/chrome/bookmark/BookmarkImporter$OnBookmarksImportedListener;->onBookmarksImported(Lcom/google/android/apps/chrome/bookmark/BookmarkImporter$ImportResults;)V

    .line 147
    :cond_0
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 111
    check-cast p1, Lcom/google/android/apps/chrome/bookmark/BookmarkImporter$ImportResults;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/bookmark/BookmarkImporter$ImportBookmarksTask;->onPostExecute(Lcom/google/android/apps/chrome/bookmark/BookmarkImporter$ImportResults;)V

    return-void
.end method
