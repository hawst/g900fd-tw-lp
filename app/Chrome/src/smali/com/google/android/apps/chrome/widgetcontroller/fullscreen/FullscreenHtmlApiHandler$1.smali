.class Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenHtmlApiHandler$1;
.super Landroid/os/Handler;
.source "FullscreenHtmlApiHandler.java"


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenHtmlApiHandler;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 122
    const-class v0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenHtmlApiHandler;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenHtmlApiHandler$1;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>(Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenHtmlApiHandler;)V
    .locals 0

    .prologue
    .line 122
    iput-object p1, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenHtmlApiHandler$1;->this$0:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenHtmlApiHandler;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4

    .prologue
    .line 125
    if-nez p1, :cond_1

    .line 184
    :cond_0
    :goto_0
    return-void

    .line 126
    :cond_1
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 181
    sget-boolean v0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenHtmlApiHandler$1;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "Unexpected message for ID: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p1, Landroid/os/Message;->what:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 128
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenHtmlApiHandler$1;->this$0:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenHtmlApiHandler;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenHtmlApiHandler;->hideNotificationBubble()V

    goto :goto_0

    .line 132
    :pswitch_1
    sget-boolean v0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenHtmlApiHandler$1;->$assertionsDisabled:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenHtmlApiHandler$1;->this$0:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenHtmlApiHandler;

    # getter for: Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenHtmlApiHandler;->mIsPersistentMode:Z
    invoke-static {v0}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenHtmlApiHandler;->access$000(Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenHtmlApiHandler;)Z

    move-result v0

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    const-string/jumbo v1, "Calling after we exited fullscreen"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 133
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenHtmlApiHandler$1;->this$0:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenHtmlApiHandler;

    # getter for: Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenHtmlApiHandler;->mContentViewCoreInFullscreen:Lorg/chromium/content/browser/ContentViewCore;
    invoke-static {v0}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenHtmlApiHandler;->access$100(Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenHtmlApiHandler;)Lorg/chromium/content/browser/ContentViewCore;

    move-result-object v0

    .line 134
    if-eqz v0, :cond_0

    .line 135
    invoke-virtual {v0}, Lorg/chromium/content/browser/ContentViewCore;->getContainerView()Landroid/view/ViewGroup;

    move-result-object v0

    .line 136
    invoke-virtual {v0}, Landroid/view/View;->getSystemUiVisibility()I

    move-result v1

    .line 137
    and-int/lit8 v2, v1, 0x4

    const/4 v3, 0x4

    if-eq v2, v3, :cond_0

    .line 141
    or-int/lit8 v1, v1, 0x4

    .line 142
    or-int/lit8 v1, v1, 0x1

    .line 143
    invoke-virtual {v0, v1}, Landroid/view/View;->setSystemUiVisibility(I)V

    .line 150
    new-instance v1, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenHtmlApiHandler$1$1;

    invoke-direct {v1, p0, v0}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenHtmlApiHandler$1$1;-><init>(Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenHtmlApiHandler$1;Landroid/view/View;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->addOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    goto :goto_0

    .line 169
    :pswitch_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenHtmlApiHandler$1;->this$0:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenHtmlApiHandler;

    # getter for: Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenHtmlApiHandler;->mIsPersistentMode:Z
    invoke-static {v0}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenHtmlApiHandler;->access$000(Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenHtmlApiHandler;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 170
    iget-object v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenHtmlApiHandler$1;->this$0:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenHtmlApiHandler;

    # getter for: Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenHtmlApiHandler;->mContentViewCoreInFullscreen:Lorg/chromium/content/browser/ContentViewCore;
    invoke-static {v0}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenHtmlApiHandler;->access$100(Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenHtmlApiHandler;)Lorg/chromium/content/browser/ContentViewCore;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 171
    iget-object v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenHtmlApiHandler$1;->this$0:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenHtmlApiHandler;

    # getter for: Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenHtmlApiHandler;->mContentViewCoreInFullscreen:Lorg/chromium/content/browser/ContentViewCore;
    invoke-static {v0}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenHtmlApiHandler;->access$100(Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenHtmlApiHandler;)Lorg/chromium/content/browser/ContentViewCore;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/content/browser/ContentViewCore;->getContainerView()Landroid/view/ViewGroup;

    move-result-object v0

    .line 172
    invoke-virtual {v0}, Landroid/view/View;->getSystemUiVisibility()I

    move-result v1

    .line 173
    and-int/lit16 v2, v1, 0x400

    if-eqz v2, :cond_0

    .line 176
    and-int/lit16 v1, v1, -0x401

    .line 177
    invoke-virtual {v0, v1}, Landroid/view/View;->setSystemUiVisibility(I)V

    goto/16 :goto_0

    .line 126
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
