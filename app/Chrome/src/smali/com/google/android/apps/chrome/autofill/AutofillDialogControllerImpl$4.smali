.class Lcom/google/android/apps/chrome/autofill/AutofillDialogControllerImpl$4;
.super Ljava/lang/Object;
.source "AutofillDialogControllerImpl.java"

# interfaces
.implements Ljava/util/concurrent/Callable;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/autofill/AutofillDialogControllerImpl;

.field final synthetic val$address:Lcom/google/android/gms/wallet/Address;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/autofill/AutofillDialogControllerImpl;Lcom/google/android/gms/wallet/Address;)V
    .locals 0

    .prologue
    .line 333
    iput-object p1, p0, Lcom/google/android/apps/chrome/autofill/AutofillDialogControllerImpl$4;->this$0:Lcom/google/android/apps/chrome/autofill/AutofillDialogControllerImpl;

    iput-object p2, p0, Lcom/google/android/apps/chrome/autofill/AutofillDialogControllerImpl$4;->val$address:Lcom/google/android/gms/wallet/Address;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic call()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 333
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/autofill/AutofillDialogControllerImpl$4;->call()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public call()Ljava/lang/String;
    .locals 4

    .prologue
    .line 336
    invoke-static {}, Lorg/chromium/chrome/browser/autofill/PersonalDataManager;->getInstance()Lorg/chromium/chrome/browser/autofill/PersonalDataManager;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/autofill/PersonalDataManager;->getProfiles()Ljava/util/List;

    move-result-object v2

    .line 338
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 339
    iget-object v3, p0, Lcom/google/android/apps/chrome/autofill/AutofillDialogControllerImpl$4;->val$address:Lcom/google/android/gms/wallet/Address;

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/chromium/chrome/browser/autofill/PersonalDataManager$AutofillProfile;

    # invokes: Lcom/google/android/apps/chrome/autofill/AutofillDialogControllerImpl;->areEqual(Lcom/google/android/gms/wallet/Address;Lorg/chromium/chrome/browser/autofill/PersonalDataManager$AutofillProfile;)Z
    invoke-static {v3, v0}, Lcom/google/android/apps/chrome/autofill/AutofillDialogControllerImpl;->access$200(Lcom/google/android/gms/wallet/Address;Lorg/chromium/chrome/browser/autofill/PersonalDataManager$AutofillProfile;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/chromium/chrome/browser/autofill/PersonalDataManager$AutofillProfile;

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/autofill/PersonalDataManager$AutofillProfile;->getGUID()Ljava/lang/String;

    move-result-object v0

    .line 341
    :goto_1
    return-object v0

    .line 338
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 341
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method
