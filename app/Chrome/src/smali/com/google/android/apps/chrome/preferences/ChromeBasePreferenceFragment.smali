.class public Lcom/google/android/apps/chrome/preferences/ChromeBasePreferenceFragment;
.super Landroid/preference/PreferenceFragment;
.source "ChromeBasePreferenceFragment.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Landroid/preference/PreferenceFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public addSwitchToActionBar(Landroid/widget/Switch;)V
    .locals 1

    .prologue
    .line 44
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/ChromeBasePreferenceFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/google/android/apps/chrome/preferences/ChromeBaseFragment;->addSwitchToActionBar(Landroid/app/Activity;Landroid/widget/Switch;)V

    .line 45
    return-void
.end method

.method public closeEditor()V
    .locals 3

    .prologue
    .line 21
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/ChromeBasePreferenceFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/ChromeBasePreferenceFragment;->getView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/ChromeBasePreferenceFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/chrome/preferences/ChromeBaseFragment;->closeEditor(Landroid/app/Activity;Landroid/view/View;Landroid/app/FragmentManager;)V

    .line 22
    return-void
.end method

.method public onDestroy()V
    .locals 3

    .prologue
    .line 26
    invoke-super {p0}, Landroid/preference/PreferenceFragment;->onDestroy()V

    .line 27
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/ChromeBasePreferenceFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    .line 28
    instance-of v0, v1, Lcom/google/android/apps/chrome/preferences/Preferences;

    if-eqz v0, :cond_0

    move-object v0, v1

    .line 29
    check-cast v0, Lcom/google/android/apps/chrome/preferences/Preferences;

    .line 30
    invoke-virtual {v0, p0}, Lcom/google/android/apps/chrome/preferences/Preferences;->popFragmentList(Landroid/app/Fragment;)V

    .line 31
    invoke-virtual {v1}, Landroid/app/Activity;->invalidateOptionsMenu()V

    .line 32
    invoke-virtual {v0}, Lcom/google/android/apps/chrome/preferences/Preferences;->getCurrentFragment()Landroid/app/Fragment;

    move-result-object v0

    .line 33
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    const-class v2, Lcom/google/android/apps/chrome/preferences/BasicsPreferences;

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 35
    check-cast v0, Lcom/google/android/apps/chrome/preferences/BasicsPreferences;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/preferences/BasicsPreferences;->updateSummaries()V

    .line 38
    :cond_0
    return-void
.end method
