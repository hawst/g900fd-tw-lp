.class Lcom/google/android/apps/chrome/toolbar/ToolbarTablet$5;
.super Lcom/google/android/apps/chrome/toolbar/KeyboardNavigationListener;
.source "ToolbarTablet.java"


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;)V
    .locals 0

    .prologue
    .line 185
    iput-object p1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet$5;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;

    invoke-direct {p0}, Lcom/google/android/apps/chrome/toolbar/KeyboardNavigationListener;-><init>()V

    return-void
.end method


# virtual methods
.method public getNextFocusBackward()Landroid/view/View;
    .locals 2

    .prologue
    .line 193
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet$5;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;

    sget v1, Lcom/google/android/apps/chrome/R$id;->url_bar:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;->findViewById(I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public getNextFocusForward()Landroid/view/View;
    .locals 1

    .prologue
    .line 188
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet$5;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;->getDelegate()Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;->getCurrentView()Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method protected handleEnterKeyPress()Z
    .locals 1

    .prologue
    .line 198
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet$5;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;->getDelegate()Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;->getMenuButtonHelper()Lorg/chromium/chrome/browser/appmenu/AppMenuButtonHelper;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/appmenu/AppMenuButtonHelper;->onEnterKeyPress()Z

    move-result v0

    return v0
.end method
