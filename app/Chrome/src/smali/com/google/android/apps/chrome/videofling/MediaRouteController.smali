.class public Lcom/google/android/apps/chrome/videofling/MediaRouteController;
.super Landroid/support/v7/media/g;
.source "MediaRouteController.java"

# interfaces
.implements Lcom/google/android/apps/chrome/third_party/ChromeMediaRouteControllerDialog$DisconnectListener;
.implements Lcom/google/android/apps/chrome/videofling/MediaUrlResolver$Delegate;
.implements Lorg/chromium/base/ApplicationStatus$ApplicationStateListener;


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private mConnectionFailureNotifier:Ljava/lang/Runnable;

.field private mConnectionFailureNotifierQueued:Z

.field private mContext:Landroid/content/Context;

.field protected mCurrentItemId:Ljava/lang/String;

.field private mCurrentRoute:Landroid/support/v7/media/MediaRouter$RouteInfo;

.field private mCurrentSessionId:Ljava/lang/String;

.field private mCurrentTab:Lorg/chromium/chrome/browser/Tab;

.field private mDebug:Z

.field private mDeviceId:Ljava/lang/String;

.field private mDiscoveryStarted:Z

.field private mHandler:Landroid/os/Handler;

.field private mIntentCategory:Ljava/lang/String;

.field private mIsPrepared:Z

.field private mLastKnownStreamPosition:I

.field private final mListeners:Ljava/util/Set;

.field private mLocalVideoCookies:Ljava/lang/String;

.field private mLocalVideoUri:Landroid/net/Uri;

.field private mMediaRouteSelector:Landroid/support/v7/media/e;

.field private mMediaRouter:Landroid/support/v7/media/MediaRouter;

.field private mMediaStatusBroadcastReceiver:Landroid/content/BroadcastReceiver;

.field private mMediaStatusUpdateIntent:Landroid/app/PendingIntent;

.field private mMediaUrlResolver:Lcom/google/android/apps/chrome/videofling/MediaUrlResolver;

.field private mNativePlayerId:I

.field private mOldItemId:Ljava/lang/String;

.field private mPlaybackState:Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;

.field private mPreferredTitle:Ljava/lang/String;

.field private mReconnecting:Z

.field private mSeeking:Z

.field private mSessionStatusBroadcastReceiver:Landroid/content/BroadcastReceiver;

.field private mSessionStatusUpdateIntent:Landroid/app/PendingIntent;

.field private mStartPositionMillis:J

.field private mStreamDuration:I

.field private mStreamPositionTimestamp:I

.field private mVideoUriToStart:Landroid/net/Uri;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 84
    const-class v0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 203
    invoke-direct {p0}, Landroid/support/v7/media/g;-><init>()V

    .line 137
    const-string/jumbo v0, "com.google.android.apps.chrome"

    iput-object v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mIntentCategory:Ljava/lang/String;

    .line 160
    sget-object v0, Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;->FINISHED:Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;

    iput-object v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mPlaybackState:Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;

    .line 162
    iput-boolean v1, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mReconnecting:Z

    .line 165
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mNativePlayerId:I

    .line 167
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mMediaRouter:Landroid/support/v7/media/MediaRouter;

    .line 170
    iput-boolean v1, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mDiscoveryStarted:Z

    .line 177
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArraySet;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mListeners:Ljava/util/Set;

    .line 180
    iput-boolean v1, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mConnectionFailureNotifierQueued:Z

    .line 182
    iput-boolean v1, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mIsPrepared:Z

    .line 187
    new-instance v0, Lcom/google/android/apps/chrome/videofling/MediaRouteController$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/videofling/MediaRouteController$1;-><init>(Lcom/google/android/apps/chrome/videofling/MediaRouteController;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mConnectionFailureNotifier:Ljava/lang/Runnable;

    .line 204
    invoke-static {}, Lorg/chromium/base/ApplicationStatus;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mContext:Landroid/content/Context;

    .line 206
    sget-boolean v0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mContext:Landroid/content/Context;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 208
    :cond_0
    invoke-static {}, Lorg/chromium/base/CommandLine;->getInstance()Lorg/chromium/base/CommandLine;

    move-result-object v0

    const-string/jumbo v1, "enable-cast-debug"

    invoke-virtual {v0, v1}, Lorg/chromium/base/CommandLine;->hasSwitch(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mDebug:Z

    .line 210
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mHandler:Landroid/os/Handler;

    .line 212
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mIntentCategory:Ljava/lang/String;

    .line 213
    return-void
.end method

.method static synthetic access$002(Lcom/google/android/apps/chrome/videofling/MediaRouteController;Z)Z
    .locals 0

    .prologue
    .line 84
    iput-boolean p1, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mConnectionFailureNotifierQueued:Z

    return p1
.end method

.method static synthetic access$100(Lcom/google/android/apps/chrome/videofling/MediaRouteController;)Z
    .locals 1

    .prologue
    .line 84
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mReconnecting:Z

    return v0
.end method

.method static synthetic access$1000(Lcom/google/android/apps/chrome/videofling/MediaRouteController;)I
    .locals 1

    .prologue
    .line 84
    iget v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mNativePlayerId:I

    return v0
.end method

.method static synthetic access$102(Lcom/google/android/apps/chrome/videofling/MediaRouteController;Z)Z
    .locals 0

    .prologue
    .line 84
    iput-boolean p1, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mReconnecting:Z

    return p1
.end method

.method static synthetic access$1100(Lcom/google/android/apps/chrome/videofling/MediaRouteController;)V
    .locals 0

    .prologue
    .line 84
    invoke-direct {p0}, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->recordTimeRemainingUma()V

    return-void
.end method

.method static synthetic access$1200(Lcom/google/android/apps/chrome/videofling/MediaRouteController;Z)V
    .locals 0

    .prologue
    .line 84
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->disconnect(Z)V

    return-void
.end method

.method static synthetic access$1302(Lcom/google/android/apps/chrome/videofling/MediaRouteController;Z)Z
    .locals 0

    .prologue
    .line 84
    iput-boolean p1, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mIsPrepared:Z

    return p1
.end method

.method static synthetic access$1400(Lcom/google/android/apps/chrome/videofling/MediaRouteController;Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 84
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->processSessionStatusBundle(Landroid/os/Bundle;)V

    return-void
.end method

.method static synthetic access$1500(Lcom/google/android/apps/chrome/videofling/MediaRouteController;Landroid/net/Uri;Ljava/lang/String;J)V
    .locals 1

    .prologue
    .line 84
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->startPlayback(Landroid/net/Uri;Ljava/lang/String;J)V

    return-void
.end method

.method static synthetic access$1600(Landroid/os/Bundle;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 84
    invoke-static {p0}, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->bundleToString(Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1700(Lcom/google/android/apps/chrome/videofling/MediaRouteController;I)V
    .locals 0

    .prologue
    .line 84
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->sendErrorToListeners(I)V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/apps/chrome/videofling/MediaRouteController;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mCurrentSessionId:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$202(Lcom/google/android/apps/chrome/videofling/MediaRouteController;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 84
    iput-object p1, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mCurrentSessionId:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$300(Lcom/google/android/apps/chrome/videofling/MediaRouteController;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/apps/chrome/videofling/MediaRouteController;)Z
    .locals 1

    .prologue
    .line 84
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mDebug:Z

    return v0
.end method

.method static synthetic access$500(Lcom/google/android/apps/chrome/videofling/MediaRouteController;)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mVideoUriToStart:Landroid/net/Uri;

    return-object v0
.end method

.method static synthetic access$502(Lcom/google/android/apps/chrome/videofling/MediaRouteController;Landroid/net/Uri;)Landroid/net/Uri;
    .locals 0

    .prologue
    .line 84
    iput-object p1, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mVideoUriToStart:Landroid/net/Uri;

    return-object p1
.end method

.method static synthetic access$600(Lcom/google/android/apps/chrome/videofling/MediaRouteController;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mPreferredTitle:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$602(Lcom/google/android/apps/chrome/videofling/MediaRouteController;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 84
    iput-object p1, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mPreferredTitle:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$700(Lcom/google/android/apps/chrome/videofling/MediaRouteController;)J
    .locals 2

    .prologue
    .line 84
    iget-wide v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mStartPositionMillis:J

    return-wide v0
.end method

.method static synthetic access$702(Lcom/google/android/apps/chrome/videofling/MediaRouteController;J)J
    .locals 1

    .prologue
    .line 84
    iput-wide p1, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mStartPositionMillis:J

    return-wide p1
.end method

.method static synthetic access$800(Lcom/google/android/apps/chrome/videofling/MediaRouteController;)Ljava/util/Set;
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mListeners:Ljava/util/Set;

    return-object v0
.end method

.method static synthetic access$900(Lcom/google/android/apps/chrome/videofling/MediaRouteController;)Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mPlaybackState:Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;

    return-object v0
.end method

.method public static buildDefaultMediaRouteSelector()Landroid/support/v7/media/e;
    .locals 2

    .prologue
    .line 280
    const-string/jumbo v0, "37F83649"

    .line 282
    new-instance v1, Landroid/support/v7/media/f;

    invoke-direct {v1}, Landroid/support/v7/media/f;-><init>()V

    invoke-static {v0}, Lcom/google/android/gms/cast/CastMediaControlIntent;->categoryForRemotePlayback(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/support/v7/media/f;->a(Ljava/lang/String;)Landroid/support/v7/media/f;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/media/f;->a()Landroid/support/v7/media/e;

    move-result-object v0

    return-object v0
.end method

.method private static bundleToString(Landroid/os/Bundle;)Ljava/lang/String;
    .locals 6

    .prologue
    .line 1384
    if-nez p0, :cond_0

    const-string/jumbo v0, ""

    .line 1395
    :goto_0
    return-object v0

    .line 1386
    :cond_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 1387
    const-string/jumbo v0, "["

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1388
    invoke-virtual {p0}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1389
    invoke-virtual {p0, v0}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    .line 1390
    if-nez v1, :cond_2

    const-string/jumbo v2, "null"

    .line 1391
    :goto_2
    instance-of v5, v1, Landroid/os/Bundle;

    if-eqz v5, :cond_1

    check-cast v1, Landroid/os/Bundle;

    invoke-static {v1}, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->bundleToString(Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v2

    .line 1392
    :cond_1
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 1390
    :cond_2
    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_2

    .line 1394
    :cond_3
    const-string/jumbo v0, "]"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1395
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private canPlayMedia(Landroid/net/Uri;[Lorg/apache/http/Header;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 1500
    sget-object v1, Landroid/net/Uri;->EMPTY:Landroid/net/Uri;

    invoke-virtual {v1, p1}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1509
    :cond_0
    :goto_0
    return v0

    .line 1505
    :cond_1
    if-eqz p2, :cond_2

    array-length v1, p2

    if-nez v1, :cond_3

    :cond_2
    invoke-static {p1}, Lcom/google/android/apps/chrome/videofling/RemoteMediaUtils;->isEnhancedMedia(Landroid/net/Uri;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1506
    iget-boolean v1, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mDebug:Z

    if-eqz v1, :cond_0

    const-string/jumbo v1, "MediaRouteController"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "HLS stream without CORs header: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1509
    :cond_3
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private disconnect(Z)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 761
    if-eqz p1, :cond_0

    .line 762
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->clearStreamState()V

    .line 763
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->clearMediaRoute()V

    .line 766
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mSessionStatusBroadcastReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v0, :cond_1

    .line 767
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mSessionStatusBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 768
    iput-object v3, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mSessionStatusBroadcastReceiver:Landroid/content/BroadcastReceiver;

    .line 770
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mMediaStatusBroadcastReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v0, :cond_2

    .line 771
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mMediaStatusBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 772
    iput-object v3, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mMediaStatusBroadcastReceiver:Landroid/content/BroadcastReceiver;

    .line 774
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mConnectionFailureNotifier:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 775
    iput-boolean v2, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mConnectionFailureNotifierQueued:Z

    .line 777
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mDiscoveryStarted:Z

    if-eqz v0, :cond_3

    .line 778
    iput-boolean v2, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mDiscoveryStarted:Z

    .line 779
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mMediaRouter:Landroid/support/v7/media/MediaRouter;

    if-eqz v0, :cond_3

    .line 780
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mMediaRouter:Landroid/support/v7/media/MediaRouter;

    invoke-virtual {v0, p0}, Landroid/support/v7/media/MediaRouter;->a(Landroid/support/v7/media/g;)V

    .line 783
    :cond_3
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->removeAllListeners()V

    .line 784
    return-void
.end method

.method protected static dumpIntentToLog(Ljava/lang/String;Landroid/content/Intent;)V
    .locals 3

    .prologue
    .line 1380
    const-string/jumbo v0, "MediaRouteController"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " extras: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->bundleToString(Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1381
    return-void
.end method

.method private installBroadcastReceivers()V
    .locals 3

    .prologue
    .line 920
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mSessionStatusBroadcastReceiver:Landroid/content/BroadcastReceiver;

    if-nez v0, :cond_0

    .line 921
    new-instance v0, Lcom/google/android/apps/chrome/videofling/MediaRouteController$11;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/videofling/MediaRouteController$11;-><init>(Lcom/google/android/apps/chrome/videofling/MediaRouteController;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mSessionStatusBroadcastReceiver:Landroid/content/BroadcastReceiver;

    .line 931
    new-instance v0, Landroid/content/IntentFilter;

    const-string/jumbo v1, "com.google.android.apps.chrome.videofling.RECEIVE_SESSION_STATUS_UPDATE"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 933
    iget-object v1, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mIntentCategory:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addCategory(Ljava/lang/String;)V

    .line 934
    iget-object v1, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mSessionStatusBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 938
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mMediaStatusBroadcastReceiver:Landroid/content/BroadcastReceiver;

    if-nez v0, :cond_1

    .line 939
    new-instance v0, Lcom/google/android/apps/chrome/videofling/MediaRouteController$12;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/videofling/MediaRouteController$12;-><init>(Lcom/google/android/apps/chrome/videofling/MediaRouteController;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mMediaStatusBroadcastReceiver:Landroid/content/BroadcastReceiver;

    .line 947
    new-instance v0, Landroid/content/IntentFilter;

    const-string/jumbo v1, "com.google.android.apps.chrome.videofling.RECEIVE_MEDIA_STATUS_UPDATE"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 949
    iget-object v1, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mIntentCategory:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addCategory(Ljava/lang/String;)V

    .line 950
    iget-object v1, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mMediaStatusBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 952
    :cond_1
    return-void
.end method

.method private static isAtEndOfVideo(II)Z
    .locals 2

    .prologue
    .line 1354
    sub-int v0, p1, p0

    const/16 v1, 0x1f4

    if-ge v0, v1, :cond_0

    if-lez p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private playMedia()V
    .locals 4

    .prologue
    .line 1513
    const/4 v0, 0x0

    .line 1514
    iget-object v1, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mCurrentTab:Lorg/chromium/chrome/browser/Tab;

    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mCurrentTab:Lorg/chromium/chrome/browser/Tab;

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/Tab;->getTitle()Ljava/lang/String;

    move-result-object v0

    .line 1516
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mLocalVideoUri:Landroid/net/Uri;

    iget-wide v2, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mStartPositionMillis:J

    invoke-direct {p0, v1, v0, v2, v3}, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->playUri(Landroid/net/Uri;Ljava/lang/String;J)V

    .line 1517
    return-void
.end method

.method private playUri(Landroid/net/Uri;Ljava/lang/String;J)V
    .locals 9

    .prologue
    .line 494
    invoke-static {}, Lorg/chromium/base/library_loader/LibraryLoader;->isInitialized()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 495
    invoke-static {p1}, Lcom/google/android/apps/chrome/videofling/RemoteMediaUtils;->getMediaType(Landroid/net/Uri;)I

    move-result v0

    invoke-static {v0}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->castMediaType(I)V

    .line 498
    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->installBroadcastReceivers()V

    .line 501
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mReconnecting:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mCurrentSessionId:Ljava/lang/String;

    if-eqz v0, :cond_2

    :cond_1
    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/google/android/apps/chrome/videofling/RemotePlaybackSettings;->getUriPlaying(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 547
    :goto_0
    return-void

    .line 508
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mCurrentSessionId:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 509
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mDebug:Z

    if-eqz v0, :cond_3

    const-string/jumbo v0, "MediaRouteController"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "Playing a new url: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 511
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mContext:Landroid/content/Context;

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/videofling/RemotePlaybackSettings;->setUriPlaying(Landroid/content/Context;Ljava/lang/String;)V

    .line 514
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mCurrentItemId:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mOldItemId:Ljava/lang/String;

    .line 517
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->clearItemState()V

    .line 518
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->startPlayback(Landroid/net/Uri;Ljava/lang/String;J)V

    goto :goto_0

    .line 522
    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mContext:Landroid/content/Context;

    const-string/jumbo v1, "37F83649"

    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/videofling/RemotePlaybackSettings;->setPlayerInUse(Landroid/content/Context;Ljava/lang/String;)V

    .line 523
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mDebug:Z

    if-eqz v0, :cond_5

    .line 524
    const-string/jumbo v0, "MediaRouteController"

    const-string/jumbo v1, "Sending stream to app: 37F83649"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 525
    const-string/jumbo v0, "MediaRouteController"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "Url: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 528
    :cond_5
    const/4 v6, 0x1

    const/4 v7, 0x0

    new-instance v0, Lcom/google/android/apps/chrome/videofling/MediaRouteController$5;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-wide v4, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/chrome/videofling/MediaRouteController$5;-><init>(Lcom/google/android/apps/chrome/videofling/MediaRouteController;Landroid/net/Uri;Ljava/lang/String;J)V

    invoke-direct {p0, v6, v7, v0}, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->startSession(ZLjava/lang/String;Lcom/google/android/apps/chrome/videofling/MediaRouteController$ResultBundleHandler;)V

    goto :goto_0
.end method

.method private processSessionStatusBundle(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    .line 1043
    if-nez p1, :cond_2

    .line 1044
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mDebug:Z

    if-eqz v0, :cond_0

    const-string/jumbo v0, "MediaRouteController"

    const-string/jumbo v1, "Got a null status bundle in a session update"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1045
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->release()V

    .line 1090
    :cond_1
    :goto_0
    return-void

    .line 1049
    :cond_2
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mDebug:Z

    if-eqz v0, :cond_3

    const-string/jumbo v0, "MediaRouteController"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "processSessionStatusBundle: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p1}, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->bundleToString(Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1051
    :cond_3
    const-string/jumbo v0, "android.media.intent.extra.SESSION_ID"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1052
    const-string/jumbo v1, "android.media.intent.extra.SESSION_STATUS"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    invoke-static {v1}, Landroid/support/v7/media/x;->a(Landroid/os/Bundle;)Landroid/support/v7/media/x;

    move-result-object v1

    .line 1054
    invoke-virtual {v1}, Landroid/support/v7/media/x;->a()I

    move-result v1

    .line 1056
    iget-object v2, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mCurrentSessionId:Ljava/lang/String;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mCurrentSessionId:Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1061
    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 1064
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mCurrentSessionId:Ljava/lang/String;

    new-instance v1, Lcom/google/android/apps/chrome/videofling/MediaRouteController$13;

    invoke-direct {v1, p0}, Lcom/google/android/apps/chrome/videofling/MediaRouteController$13;-><init>(Lcom/google/android/apps/chrome/videofling/MediaRouteController;)V

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->syncStatus(Ljava/lang/String;Lcom/google/android/apps/chrome/videofling/MediaRouteController$ResultBundleHandler;)V

    goto :goto_0

    .line 1083
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mListeners:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/videofling/MediaRouteController$Listener;

    .line 1084
    iget-object v2, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mPlaybackState:Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;

    sget-object v3, Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;->INVALIDATED:Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;

    iget-object v4, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mCurrentTab:Lorg/chromium/chrome/browser/Tab;

    iget v5, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mNativePlayerId:I

    invoke-interface {v0, v2, v3, v4, v5}, Lcom/google/android/apps/chrome/videofling/MediaRouteController$Listener;->onPlaybackStateChanged(Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;Lorg/chromium/chrome/browser/Tab;I)V

    goto :goto_1

    .line 1088
    :cond_4
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mCurrentSessionId:Ljava/lang/String;

    .line 1089
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->release()V

    goto :goto_0

    .line 1061
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method private recordTimeRemainingUma()V
    .locals 3

    .prologue
    .line 1373
    invoke-static {}, Lorg/chromium/base/library_loader/LibraryLoader;->isInitialized()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1374
    iget v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mStreamDuration:I

    iget v1, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mStreamDuration:I

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->getPosition()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->castEndedTimeRemaining(II)V

    .line 1377
    :cond_0
    return-void
.end method

.method private selectDevice(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 379
    if-nez p1, :cond_1

    .line 380
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->release()V

    .line 397
    :cond_0
    :goto_0
    return-void

    .line 384
    :cond_1
    iput-object p1, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mDeviceId:Ljava/lang/String;

    .line 386
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mDebug:Z

    if-eqz v0, :cond_2

    const-string/jumbo v0, "MediaRouteController"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "Trying to select "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mDeviceId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 389
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mMediaRouter:Landroid/support/v7/media/MediaRouter;

    if-eqz v0, :cond_0

    .line 390
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mMediaRouter:Landroid/support/v7/media/MediaRouter;

    invoke-static {}, Landroid/support/v7/media/MediaRouter;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/media/MediaRouter$RouteInfo;

    .line 391
    invoke-virtual {v0}, Landroid/support/v7/media/MediaRouter$RouteInfo;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 392
    invoke-virtual {v0}, Landroid/support/v7/media/MediaRouter$RouteInfo;->n()V

    goto :goto_0
.end method

.method private sendErrorToListeners(I)V
    .locals 5

    .prologue
    .line 1230
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mContext:Landroid/content/Context;

    sget v1, Lcom/google/android/apps/chrome/R$string;->cast_error_playing_video:I

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mCurrentRoute:Landroid/support/v7/media/MediaRouter$RouteInfo;

    invoke-virtual {v4}, Landroid/support/v7/media/MediaRouter$RouteInfo;->b()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 1233
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mListeners:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/videofling/MediaRouteController$Listener;

    .line 1234
    invoke-interface {v0, p1, v1}, Lcom/google/android/apps/chrome/videofling/MediaRouteController$Listener;->onError(ILjava/lang/String;)V

    goto :goto_0

    .line 1236
    :cond_0
    return-void
.end method

.method private setPrepared()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 1334
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mIsPrepared:Z

    if-nez v0, :cond_2

    .line 1335
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mListeners:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/videofling/MediaRouteController$Listener;

    .line 1336
    invoke-interface {v0, p0}, Lcom/google/android/apps/chrome/videofling/MediaRouteController$Listener;->onPrepared(Lcom/google/android/apps/chrome/videofling/MediaRouteController;)V

    goto :goto_0

    .line 1338
    :cond_0
    invoke-static {}, Lorg/chromium/base/library_loader/LibraryLoader;->isInitialized()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {v2}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->castDefaultPlayerResult(Z)V

    .line 1339
    :cond_1
    iput-boolean v2, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mIsPrepared:Z

    .line 1341
    :cond_2
    return-void
.end method

.method private shouldReconnect()Z
    .locals 8

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 344
    invoke-static {}, Lorg/chromium/base/CommandLine;->getInstance()Lorg/chromium/base/CommandLine;

    move-result-object v2

    const-string/jumbo v3, "disable-cast-reconnection"

    invoke-virtual {v2, v3}, Lorg/chromium/base/CommandLine;->hasSwitch(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 345
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mDebug:Z

    if-eqz v0, :cond_0

    const-string/jumbo v0, "MediaRouteController"

    const-string/jumbo v2, "Cast reconnection disabled"

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 369
    :cond_0
    :goto_0
    return v1

    .line 349
    :cond_1
    iget-object v2, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/google/android/apps/chrome/videofling/RemotePlaybackSettings;->getShouldReconnectToRemote(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 350
    iget-object v2, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/google/android/apps/chrome/videofling/RemotePlaybackSettings;->getLastVideoState(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    .line 351
    if-eqz v2, :cond_3

    .line 352
    invoke-static {v2}, Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;->valueOf(Ljava/lang/String;)Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;

    move-result-object v2

    .line 353
    sget-object v3, Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;->PLAYING:Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;

    if-eq v2, v3, :cond_2

    sget-object v3, Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;->LOADING:Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;

    if-ne v2, v3, :cond_4

    .line 357
    :cond_2
    iget-object v2, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/google/android/apps/chrome/videofling/RemotePlaybackSettings;->getRemainingTime(Landroid/content/Context;)J

    move-result-wide v2

    .line 358
    iget-object v4, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mContext:Landroid/content/Context;

    invoke-static {v4}, Lcom/google/android/apps/chrome/videofling/RemotePlaybackSettings;->getLastPlayedTime(Landroid/content/Context;)J

    move-result-wide v4

    .line 359
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    .line 360
    add-long/2addr v2, v4

    cmp-long v2, v6, v2

    if-gez v2, :cond_5

    :goto_1
    move v1, v0

    .line 368
    :cond_3
    :goto_2
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mDebug:Z

    if-eqz v0, :cond_0

    const-string/jumbo v0, "MediaRouteController"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "shouldReconnect returning: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 363
    :cond_4
    sget-object v3, Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;->PAUSED:Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;

    if-ne v2, v3, :cond_3

    move v1, v0

    .line 364
    goto :goto_2

    :cond_5
    move v0, v1

    goto :goto_1
.end method

.method private showMessageToast(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1520
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mContext:Landroid/content/Context;

    const/4 v1, 0x0

    invoke-static {v0, p1, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    .line 1521
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 1522
    return-void
.end method

.method private startPlayback(Landroid/net/Uri;Ljava/lang/String;J)V
    .locals 3

    .prologue
    .line 574
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mIsPrepared:Z

    .line 575
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "android.media.intent.action.PLAY"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 576
    const-string/jumbo v1, "android.media.intent.category.REMOTE_PLAYBACK"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 577
    const-string/jumbo v1, "video/mp4"

    invoke-virtual {v0, p1, v1}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    .line 578
    const-string/jumbo v1, "android.media.intent.extra.SESSION_ID"

    iget-object v2, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mCurrentSessionId:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 579
    const-string/jumbo v1, "android.media.intent.extra.ITEM_STATUS_UPDATE_RECEIVER"

    iget-object v2, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mMediaStatusUpdateIntent:Landroid/app/PendingIntent;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 581
    const-string/jumbo v1, "android.media.intent.extra.ITEM_POSITION"

    invoke-virtual {v0, v1, p3, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 583
    if-eqz p2, :cond_0

    .line 584
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 585
    const-string/jumbo v2, "android.media.metadata.TITLE"

    invoke-virtual {v1, v2, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 586
    const-string/jumbo v2, "android.media.intent.extra.ITEM_METADATA"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    .line 589
    :cond_0
    new-instance v1, Lcom/google/android/apps/chrome/videofling/MediaRouteController$6;

    invoke-direct {v1, p0}, Lcom/google/android/apps/chrome/videofling/MediaRouteController$6;-><init>(Lcom/google/android/apps/chrome/videofling/MediaRouteController;)V

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->sendIntentToRoute(Landroid/content/Intent;Lcom/google/android/apps/chrome/videofling/MediaRouteController$ResultBundleHandler;)V

    .line 602
    return-void
.end method

.method private startSession(ZLjava/lang/String;Lcom/google/android/apps/chrome/videofling/MediaRouteController$ResultBundleHandler;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 557
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "android.media.intent.action.START_SESSION"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 558
    const-string/jumbo v1, "android.media.intent.category.REMOTE_PLAYBACK"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 560
    const-string/jumbo v1, "com.google.android.gms.cast.EXTRA_CAST_STOP_APPLICATION_WHEN_SESSION_ENDS"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 561
    const-string/jumbo v1, "android.media.intent.extra.SESSION_STATUS_UPDATE_RECEIVER"

    iget-object v2, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mSessionStatusUpdateIntent:Landroid/app/PendingIntent;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 563
    const-string/jumbo v1, "com.google.android.gms.cast.EXTRA_CAST_APPLICATION_ID"

    const-string/jumbo v2, "37F83649"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 564
    const-string/jumbo v1, "com.google.android.gms.cast.EXTRA_CAST_RELAUNCH_APPLICATION"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 565
    if-eqz p2, :cond_0

    const-string/jumbo v1, "android.media.intent.extra.SESSION_ID"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 567
    :cond_0
    iget-boolean v1, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mDebug:Z

    if-eqz v1, :cond_1

    const-string/jumbo v1, "com.google.android.gms.cast.EXTRA_DEBUG_LOGGING_ENABLED"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 569
    :cond_1
    invoke-virtual {p0, v0, p3}, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->sendIntentToRoute(Landroid/content/Intent;Lcom/google/android/apps/chrome/videofling/MediaRouteController$ResultBundleHandler;)V

    .line 570
    return-void
.end method

.method private stopAndDisconnect(Lorg/chromium/chrome/browser/Tab;)V
    .locals 3

    .prologue
    .line 701
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mMediaRouter:Landroid/support/v7/media/MediaRouter;

    if-nez v0, :cond_0

    .line 751
    :goto_0
    return-void

    .line 702
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mCurrentSessionId:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 705
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->disconnect(Z)V

    goto :goto_0

    .line 709
    :cond_1
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "android.media.intent.action.STOP"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 710
    const-string/jumbo v1, "android.media.intent.category.REMOTE_PLAYBACK"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 711
    const-string/jumbo v1, "android.media.intent.extra.SESSION_ID"

    iget-object v2, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mCurrentSessionId:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 713
    new-instance v1, Lcom/google/android/apps/chrome/videofling/MediaRouteController$8;

    invoke-direct {v1, p0}, Lcom/google/android/apps/chrome/videofling/MediaRouteController$8;-><init>(Lcom/google/android/apps/chrome/videofling/MediaRouteController;)V

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->sendIntentToRoute(Landroid/content/Intent;Lcom/google/android/apps/chrome/videofling/MediaRouteController$ResultBundleHandler;)V

    .line 723
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "android.media.intent.action.END_SESSION"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 724
    const-string/jumbo v1, "android.media.intent.category.REMOTE_PLAYBACK"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 725
    const-string/jumbo v1, "android.media.intent.extra.SESSION_ID"

    iget-object v2, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mCurrentSessionId:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 727
    new-instance v1, Lcom/google/android/apps/chrome/videofling/MediaRouteController$9;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/chrome/videofling/MediaRouteController$9;-><init>(Lcom/google/android/apps/chrome/videofling/MediaRouteController;Lorg/chromium/chrome/browser/Tab;)V

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->sendIntentToRoute(Landroid/content/Intent;Lcom/google/android/apps/chrome/videofling/MediaRouteController$ResultBundleHandler;)V

    goto :goto_0
.end method

.method private syncStatus(Ljava/lang/String;Lcom/google/android/apps/chrome/videofling/MediaRouteController$ResultBundleHandler;)V
    .locals 3

    .prologue
    .line 1026
    if-nez p1, :cond_0

    .line 1033
    :goto_0
    return-void

    .line 1027
    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "com.google.android.gms.cast.ACTION_SYNC_STATUS"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1028
    invoke-static {}, Lcom/google/android/gms/cast/CastMediaControlIntent;->categoryForRemotePlayback()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 1029
    const-string/jumbo v1, "android.media.intent.extra.SESSION_ID"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1030
    const-string/jumbo v1, "android.media.intent.extra.ITEM_STATUS_UPDATE_RECEIVER"

    iget-object v2, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mMediaStatusUpdateIntent:Landroid/app/PendingIntent;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1032
    invoke-virtual {p0, v0, p2}, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->sendIntentToRoute(Landroid/content/Intent;Lcom/google/android/apps/chrome/videofling/MediaRouteController$ResultBundleHandler;)V

    goto :goto_0
.end method

.method private updateDuration(I)V
    .locals 2

    .prologue
    .line 1239
    iput p1, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mStreamDuration:I

    .line 1241
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mListeners:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/videofling/MediaRouteController$Listener;

    .line 1242
    invoke-interface {v0, p1}, Lcom/google/android/apps/chrome/videofling/MediaRouteController$Listener;->onDurationUpdated(I)V

    goto :goto_0

    .line 1244
    :cond_0
    return-void
.end method

.method private updatePosition()V
    .locals 3

    .prologue
    .line 1344
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mListeners:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/videofling/MediaRouteController$Listener;

    .line 1345
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->getPosition()I

    move-result v2

    invoke-interface {v0, v2}, Lcom/google/android/apps/chrome/videofling/MediaRouteController$Listener;->onPositionChanged(I)V

    goto :goto_0

    .line 1347
    :cond_0
    return-void
.end method


# virtual methods
.method public addListener(Lcom/google/android/apps/chrome/videofling/MediaRouteController$Listener;)V
    .locals 1

    .prologue
    .line 299
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mListeners:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 300
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mListeners:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 302
    :cond_0
    return-void
.end method

.method public buildMediaRouteSelector()Landroid/support/v7/media/e;
    .locals 1

    .prologue
    .line 273
    invoke-static {}, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->buildDefaultMediaRouteSelector()Landroid/support/v7/media/e;

    move-result-object v0

    return-object v0
.end method

.method protected clearItemState()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 1002
    iput-object v2, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mCurrentItemId:Ljava/lang/String;

    .line 1003
    iput v1, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mStreamPositionTimestamp:I

    .line 1004
    iput v1, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mLastKnownStreamPosition:I

    .line 1005
    iput v1, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mStreamDuration:I

    .line 1006
    sget-object v0, Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;->FINISHED:Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;

    iput-object v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mPlaybackState:Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;

    .line 1007
    iput-boolean v1, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mSeeking:Z

    .line 1008
    invoke-virtual {p0, v2}, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->updateTitle(Ljava/lang/String;)V

    .line 1009
    return-void
.end method

.method protected clearMediaRoute()V
    .locals 2

    .prologue
    .line 790
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mMediaRouter:Landroid/support/v7/media/MediaRouter;

    if-eqz v0, :cond_0

    .line 791
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mMediaRouter:Landroid/support/v7/media/MediaRouter;

    invoke-static {}, Landroid/support/v7/media/MediaRouter;->b()Landroid/support/v7/media/MediaRouter$RouteInfo;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/media/MediaRouter$RouteInfo;->n()V

    .line 792
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mMediaRouter:Landroid/support/v7/media/MediaRouter;

    invoke-static {}, Landroid/support/v7/media/MediaRouter;->b()Landroid/support/v7/media/MediaRouter$RouteInfo;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->registerRoute(Landroid/support/v7/media/MediaRouter$RouteInfo;)V

    .line 793
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mContext:Landroid/content/Context;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/videofling/RemotePlaybackSettings;->setDeviceId(Landroid/content/Context;Ljava/lang/String;)V

    .line 795
    :cond_0
    return-void
.end method

.method protected clearStreamState()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 986
    iput-object v2, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mVideoUriToStart:Landroid/net/Uri;

    .line 987
    iput-object v2, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mLocalVideoUri:Landroid/net/Uri;

    .line 988
    iput-object v2, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mCurrentSessionId:Ljava/lang/String;

    .line 989
    iput-object v2, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mOldItemId:Ljava/lang/String;

    .line 990
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->clearItemState()V

    .line 992
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mContext:Landroid/content/Context;

    if-eqz v0, :cond_0

    .line 993
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mContext:Landroid/content/Context;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/videofling/RemotePlaybackSettings;->setShouldReconnectToRemote(Landroid/content/Context;Z)V

    .line 994
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mContext:Landroid/content/Context;

    invoke-static {v0, v2}, Lcom/google/android/apps/chrome/videofling/RemotePlaybackSettings;->setUriPlaying(Landroid/content/Context;Ljava/lang/String;)V

    .line 996
    :cond_0
    return-void
.end method

.method public currentRouteSupportsDomain(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 422
    invoke-static {p1}, Lcom/google/android/apps/chrome/videofling/RemoteMediaUtils;->isYouTubeUrl(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public currentRouteSupportsRemotePlayback()Z
    .locals 2

    .prologue
    .line 414
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mCurrentRoute:Landroid/support/v7/media/MediaRouter$RouteInfo;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mCurrentRoute:Landroid/support/v7/media/MediaRouter$RouteInfo;

    const-string/jumbo v1, "android.media.intent.category.REMOTE_PLAYBACK"

    invoke-virtual {v0, v1}, Landroid/support/v7/media/MediaRouter$RouteInfo;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected getContext()Landroid/content/Context;
    .locals 1

    .prologue
    .line 1361
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method public getCookies()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1482
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mLocalVideoCookies:Ljava/lang/String;

    return-object v0
.end method

.method public getDuration()I
    .locals 1

    .prologue
    .line 632
    iget v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mStreamDuration:I

    return v0
.end method

.method protected getListeners()Ljava/util/Set;
    .locals 1

    .prologue
    .line 1369
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mListeners:Ljava/util/Set;

    return-object v0
.end method

.method public getNativePlayerId()I
    .locals 1

    .prologue
    .line 1432
    iget v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mNativePlayerId:I

    return v0
.end method

.method public getOwnerTab()Lorg/chromium/chrome/browser/Tab;
    .locals 1

    .prologue
    .line 1418
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mCurrentTab:Lorg/chromium/chrome/browser/Tab;

    return-object v0
.end method

.method getPlayerState()Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;
    .locals 1

    .prologue
    .line 1526
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mPlaybackState:Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;

    return-object v0
.end method

.method public getPosition()I
    .locals 6

    .prologue
    .line 614
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mPlaybackState:Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;

    sget-object v1, Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;->PLAYING:Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;

    if-eq v0, v1, :cond_1

    const/4 v0, 0x1

    .line 615
    :goto_0
    iget v1, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mStreamPositionTimestamp:I

    if-eqz v1, :cond_2

    iget-boolean v1, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mSeeking:Z

    if-nez v1, :cond_2

    if-nez v0, :cond_2

    iget v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mLastKnownStreamPosition:I

    iget v1, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mStreamDuration:I

    if-ge v0, v1, :cond_2

    .line 618
    iget v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mLastKnownStreamPosition:I

    int-to-long v0, v0

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    iget v4, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mStreamPositionTimestamp:I

    int-to-long v4, v4

    sub-long/2addr v2, v4

    add-long/2addr v0, v2

    .line 620
    iget v2, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mStreamDuration:I

    int-to-long v2, v2

    cmp-long v2, v0, v2

    if-lez v2, :cond_0

    .line 621
    iget v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mStreamDuration:I

    int-to-long v0, v0

    .line 623
    :cond_0
    long-to-int v0, v0

    .line 625
    :goto_1
    return v0

    .line 614
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 625
    :cond_2
    iget v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mLastKnownStreamPosition:I

    goto :goto_1
.end method

.method public getRouteName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 801
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mCurrentRoute:Landroid/support/v7/media/MediaRouter$RouteInfo;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mCurrentRoute:Landroid/support/v7/media/MediaRouter$RouteInfo;

    invoke-virtual {v0}, Landroid/support/v7/media/MediaRouter$RouteInfo;->b()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getUri()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 1472
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mLocalVideoUri:Landroid/net/Uri;

    return-object v0
.end method

.method public getUserAgent()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1477
    invoke-static {}, Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerController;->nativeGetBrowserUserAgent()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public initialize(Ljava/lang/String;)Z
    .locals 4

    .prologue
    const/high16 v3, 0x8000000

    const/4 v0, 0x0

    .line 228
    :try_start_0
    iget-object v1, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mContext:Landroid/content/Context;

    invoke-static {v1}, Landroid/support/v7/media/MediaRouter;->a(Landroid/content/Context;)Landroid/support/v7/media/MediaRouter;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mMediaRouter:Landroid/support/v7/media/MediaRouter;

    .line 229
    iget-object v1, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mMediaRouter:Landroid/support/v7/media/MediaRouter;

    invoke-static {}, Landroid/support/v7/media/MediaRouter;->c()Landroid/support/v7/media/MediaRouter$RouteInfo;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->registerRoute(Landroid/support/v7/media/MediaRouter$RouteInfo;)V
    :try_end_0
    .catch Ljava/lang/NoSuchMethodError; {:try_start_0 .. :try_end_0} :catch_0

    .line 235
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->buildMediaRouteSelector()Landroid/support/v7/media/e;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mMediaRouteSelector:Landroid/support/v7/media/e;

    .line 237
    invoke-static {p0}, Lorg/chromium/base/ApplicationStatus;->registerApplicationStateListener(Lorg/chromium/base/ApplicationStatus$ApplicationStateListener;)V

    .line 239
    iget-object v1, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mSessionStatusUpdateIntent:Landroid/app/PendingIntent;

    if-nez v1, :cond_0

    .line 240
    new-instance v1, Landroid/content/Intent;

    const-string/jumbo v2, "com.google.android.apps.chrome.videofling.RECEIVE_SESSION_STATUS_UPDATE"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 241
    iget-object v2, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mIntentCategory:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 242
    iget-object v2, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mContext:Landroid/content/Context;

    invoke-static {v2, v0, v1, v3}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mSessionStatusUpdateIntent:Landroid/app/PendingIntent;

    .line 246
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mMediaStatusUpdateIntent:Landroid/app/PendingIntent;

    if-nez v1, :cond_1

    .line 247
    new-instance v1, Landroid/content/Intent;

    const-string/jumbo v2, "com.google.android.apps.chrome.videofling.RECEIVE_MEDIA_STATUS_UPDATE"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 248
    iget-object v2, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mIntentCategory:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 249
    iget-object v2, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mContext:Landroid/content/Context;

    invoke-static {v2, v0, v1, v3}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mMediaStatusUpdateIntent:Landroid/app/PendingIntent;

    .line 253
    :cond_1
    const/4 v0, 0x1

    :goto_0
    return v0

    .line 230
    :catch_0
    move-exception v1

    .line 231
    const-string/jumbo v2, "MediaRouteController"

    invoke-virtual {v1}, Ljava/lang/NoSuchMethodError;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public isBeingCast()Z
    .locals 2

    .prologue
    .line 646
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mPlaybackState:Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;

    sget-object v1, Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;->INVALIDATED:Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mPlaybackState:Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;

    sget-object v1, Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;->ERROR:Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mPlaybackState:Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;

    sget-object v1, Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;->FINISHED:Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isPlaying()Z
    .locals 2

    .prologue
    .line 639
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mPlaybackState:Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;

    sget-object v1, Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;->PLAYING:Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mPlaybackState:Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;

    sget-object v1, Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;->LOADING:Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isRemotePlaybackAvailable()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 403
    iget-object v2, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mMediaRouter:Landroid/support/v7/media/MediaRouter;

    if-nez v2, :cond_1

    .line 405
    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v2, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mMediaRouter:Landroid/support/v7/media/MediaRouter;

    invoke-static {}, Landroid/support/v7/media/MediaRouter;->c()Landroid/support/v7/media/MediaRouter$RouteInfo;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v7/media/MediaRouter$RouteInfo;->h()I

    move-result v2

    if-eq v2, v1, :cond_2

    iget-object v2, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mMediaRouter:Landroid/support/v7/media/MediaRouter;

    iget-object v2, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mMediaRouteSelector:Landroid/support/v7/media/e;

    invoke-static {v2, v1}, Landroid/support/v7/media/MediaRouter;->a(Landroid/support/v7/media/e;I)Z

    move-result v2

    if-eqz v2, :cond_0

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method protected onActivitiesDestroyed()V
    .locals 1

    .prologue
    .line 958
    invoke-static {p0}, Lorg/chromium/base/ApplicationStatus;->unregisterApplicationStateListener(Lorg/chromium/base/ApplicationStatus$ApplicationStateListener;)V

    .line 961
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->disconnect(Z)V

    .line 962
    return-void
.end method

.method public onApplicationStateChange(I)V
    .locals 0

    .prologue
    .line 909
    packed-switch p1, :pswitch_data_0

    .line 913
    :goto_0
    return-void

    .line 912
    :pswitch_0
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->onActivitiesDestroyed()V

    goto :goto_0

    .line 909
    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_0
    .end packed-switch
.end method

.method public onDisconnect()V
    .locals 0

    .prologue
    .line 1403
    invoke-direct {p0}, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->recordTimeRemainingUma()V

    .line 1404
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->release()V

    .line 1405
    return-void
.end method

.method public onRouteAdded(Landroid/support/v7/media/MediaRouter;Landroid/support/v7/media/MediaRouter$RouteInfo;)V
    .locals 3

    .prologue
    .line 810
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mDebug:Z

    if-eqz v0, :cond_0

    const-string/jumbo v0, "MediaRouteController"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "Added route "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 811
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mDeviceId:Ljava/lang/String;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mDeviceId:Ljava/lang/String;

    invoke-virtual {p2}, Landroid/support/v7/media/MediaRouter$RouteInfo;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 813
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mDebug:Z

    if-eqz v0, :cond_1

    const-string/jumbo v0, "MediaRouteController"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "Selecting Added Device "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/support/v7/media/MediaRouter$RouteInfo;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 814
    :cond_1
    invoke-virtual {p2}, Landroid/support/v7/media/MediaRouter$RouteInfo;->n()V

    .line 816
    :cond_2
    return-void
.end method

.method public onRouteChanged(Landroid/support/v7/media/MediaRouter;Landroid/support/v7/media/MediaRouter$RouteInfo;)V
    .locals 4

    .prologue
    .line 875
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mCurrentRoute:Landroid/support/v7/media/MediaRouter$RouteInfo;

    invoke-virtual {p2, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 894
    :cond_0
    :goto_0
    return-void

    .line 877
    :cond_1
    invoke-virtual {p2}, Landroid/support/v7/media/MediaRouter$RouteInfo;->e()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 879
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mConnectionFailureNotifierQueued:Z

    if-nez v0, :cond_0

    .line 880
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mConnectionFailureNotifierQueued:Z

    .line 881
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mConnectionFailureNotifier:Ljava/lang/Runnable;

    const-wide/16 v2, 0x2710

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .line 888
    :cond_2
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mConnectionFailureNotifierQueued:Z

    if-eqz v0, :cond_0

    .line 890
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mConnectionFailureNotifier:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 891
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mConnectionFailureNotifierQueued:Z

    goto :goto_0
.end method

.method public onRouteSelected(Landroid/support/v7/media/MediaRouter;Landroid/support/v7/media/MediaRouter$RouteInfo;)V
    .locals 5

    .prologue
    const/4 v3, 0x0

    .line 828
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mDebug:Z

    if-eqz v0, :cond_0

    const-string/jumbo v0, "MediaRouteController"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "Selected route "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 829
    :cond_0
    invoke-virtual {p2}, Landroid/support/v7/media/MediaRouter$RouteInfo;->f()Z

    move-result v0

    if-nez v0, :cond_2

    .line 870
    :cond_1
    :goto_0
    return-void

    .line 831
    :cond_2
    invoke-direct {p0}, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->installBroadcastReceivers()V

    .line 833
    invoke-static {}, Lorg/chromium/base/library_loader/LibraryLoader;->isInitialized()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 834
    invoke-static {v3}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->remotePlaybackDeviceSelected(I)V

    .line 836
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mListeners:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 837
    invoke-virtual {p2}, Landroid/support/v7/media/MediaRouter$RouteInfo;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->showCastError(Ljava/lang/String;)V

    .line 838
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->release()V

    goto :goto_0

    .line 842
    :cond_4
    invoke-virtual {p0, p2}, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->registerRoute(Landroid/support/v7/media/MediaRouter$RouteInfo;)V

    .line 843
    invoke-direct {p0}, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->shouldReconnect()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 844
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/apps/chrome/videofling/RemotePlaybackSettings;->getSessionId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/chrome/videofling/MediaRouteController$10;

    invoke-direct {v1, p0}, Lcom/google/android/apps/chrome/videofling/MediaRouteController$10;-><init>(Lcom/google/android/apps/chrome/videofling/MediaRouteController;)V

    invoke-direct {p0, v3, v0, v1}, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->startSession(ZLjava/lang/String;Lcom/google/android/apps/chrome/videofling/MediaRouteController$ResultBundleHandler;)V

    .line 867
    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mListeners:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/videofling/MediaRouteController$Listener;

    .line 868
    invoke-virtual {p2}, Landroid/support/v7/media/MediaRouter$RouteInfo;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->getOwnerTab()Lorg/chromium/chrome/browser/Tab;

    move-result-object v3

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->getNativePlayerId()I

    move-result v4

    invoke-interface {v0, v2, v3, v4, p0}, Lcom/google/android/apps/chrome/videofling/MediaRouteController$Listener;->onRouteSelected(Ljava/lang/String;Lorg/chromium/chrome/browser/Tab;ILcom/google/android/apps/chrome/videofling/MediaRouteController;)V

    goto :goto_2

    .line 863
    :cond_5
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->clearStreamState()V

    .line 864
    iput-boolean v3, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mReconnecting:Z

    goto :goto_1
.end method

.method public onRouteUnselected(Landroid/support/v7/media/MediaRouter;Landroid/support/v7/media/MediaRouter$RouteInfo;)V
    .locals 3

    .prologue
    .line 903
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mDebug:Z

    if-eqz v0, :cond_0

    const-string/jumbo v0, "MediaRouteController"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "Unselected route "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 904
    :cond_0
    invoke-virtual {p2}, Landroid/support/v7/media/MediaRouter$RouteInfo;->a()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mCurrentRoute:Landroid/support/v7/media/MediaRouter$RouteInfo;

    invoke-virtual {v1}, Landroid/support/v7/media/MediaRouter$RouteInfo;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->clearStreamState()V

    .line 905
    :cond_1
    return-void
.end method

.method public pause()V
    .locals 3

    .prologue
    .line 456
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mCurrentItemId:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 480
    :goto_0
    return-void

    .line 458
    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "android.media.intent.action.PAUSE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 459
    const-string/jumbo v1, "android.media.intent.category.REMOTE_PLAYBACK"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 460
    const-string/jumbo v1, "android.media.intent.extra.SESSION_ID"

    iget-object v2, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mCurrentSessionId:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 461
    new-instance v1, Lcom/google/android/apps/chrome/videofling/MediaRouteController$4;

    invoke-direct {v1, p0}, Lcom/google/android/apps/chrome/videofling/MediaRouteController$4;-><init>(Lcom/google/android/apps/chrome/videofling/MediaRouteController;)V

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->sendIntentToRoute(Landroid/content/Intent;Lcom/google/android/apps/chrome/videofling/MediaRouteController$ResultBundleHandler;)V

    .line 478
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->getPosition()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mLastKnownStreamPosition:I

    .line 479
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->updateState(I)V

    goto :goto_0
.end method

.method public prepareAsync(Ljava/lang/String;J)V
    .locals 4

    .prologue
    .line 1455
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mDebug:Z

    if-eqz v0, :cond_0

    const-string/jumbo v0, "MediaRouteController"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "prepareAsync called, mLocalVideoUri = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mLocalVideoUri:Landroid/net/Uri;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1456
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mLocalVideoUri:Landroid/net/Uri;

    if-nez v0, :cond_1

    .line 1468
    :goto_0
    return-void

    .line 1458
    :cond_1
    invoke-static {}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->castPlayRequested()V

    .line 1461
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mMediaUrlResolver:Lcom/google/android/apps/chrome/videofling/MediaUrlResolver;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mMediaUrlResolver:Lcom/google/android/apps/chrome/videofling/MediaUrlResolver;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/videofling/MediaUrlResolver;->cancel(Z)Z

    .line 1465
    :cond_2
    new-instance v0, Lcom/google/android/apps/chrome/videofling/MediaUrlResolver;

    iget-object v1, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1, p0}, Lcom/google/android/apps/chrome/videofling/MediaUrlResolver;-><init>(Landroid/content/Context;Lcom/google/android/apps/chrome/videofling/MediaUrlResolver$Delegate;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mMediaUrlResolver:Lcom/google/android/apps/chrome/videofling/MediaUrlResolver;

    .line 1466
    iput-wide p2, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mStartPositionMillis:J

    .line 1467
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mMediaUrlResolver:Lcom/google/android/apps/chrome/videofling/MediaUrlResolver;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/videofling/MediaUrlResolver;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0
.end method

.method public prepareMediaRoute()V
    .locals 0

    .prologue
    .line 290
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->startDiscovery()V

    .line 291
    return-void
.end method

.method protected processMediaStatusBundle(Landroid/os/Bundle;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 1098
    if-nez p1, :cond_1

    .line 1157
    :cond_0
    :goto_0
    return-void

    .line 1100
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mDebug:Z

    if-eqz v0, :cond_2

    const-string/jumbo v0, "MediaRouteController"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "processMediaStatusBundle: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p1}, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->bundleToString(Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1102
    :cond_2
    const-string/jumbo v0, "android.media.intent.extra.ITEM_ID"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1103
    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mCurrentItemId:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1106
    const-string/jumbo v0, "android.media.intent.extra.ITEM_METADATA"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1107
    const-string/jumbo v0, "android.media.intent.extra.ITEM_METADATA"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    .line 1109
    const-string/jumbo v2, "android.media.metadata.TITLE"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->updateTitle(Ljava/lang/String;)V

    .line 1113
    :cond_3
    const-string/jumbo v0, "android.media.intent.extra.ITEM_STATUS"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1114
    const-string/jumbo v0, "android.media.intent.extra.ITEM_STATUS"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    .line 1116
    invoke-static {v0}, Landroid/support/v7/media/a;->a(Landroid/os/Bundle;)Landroid/support/v7/media/a;

    move-result-object v2

    .line 1118
    iget-boolean v3, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mDebug:Z

    if-eqz v3, :cond_4

    const-string/jumbo v3, "MediaRouteController"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "Received item status: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->bundleToString(Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1120
    :cond_4
    invoke-virtual {v2}, Landroid/support/v7/media/a;->b()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->updateState(I)V

    .line 1122
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mPlaybackState:Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;

    sget-object v3, Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;->PAUSED:Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;

    if-eq v0, v3, :cond_5

    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mPlaybackState:Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;

    sget-object v3, Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;->PLAYING:Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;

    if-eq v0, v3, :cond_5

    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mPlaybackState:Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;

    sget-object v3, Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;->LOADING:Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;

    if-ne v0, v3, :cond_6

    .line 1126
    :cond_5
    iput-object v1, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mCurrentItemId:Ljava/lang/String;

    .line 1128
    invoke-virtual {v2}, Landroid/support/v7/media/a;->d()J

    move-result-wide v0

    long-to-int v0, v0

    .line 1130
    invoke-static {v0, v6}, Ljava/lang/Math;->max(II)I

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->updateDuration(I)V

    .line 1133
    invoke-virtual {v2}, Landroid/support/v7/media/a;->c()J

    move-result-wide v0

    long-to-int v0, v0

    iput v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mLastKnownStreamPosition:I

    .line 1134
    invoke-virtual {v2}, Landroid/support/v7/media/a;->a()J

    move-result-wide v0

    long-to-int v0, v0

    iput v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mStreamPositionTimestamp:I

    .line 1135
    invoke-direct {p0}, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->updatePosition()V

    .line 1137
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mSeeking:Z

    if-eqz v0, :cond_6

    .line 1138
    iput-boolean v6, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mSeeking:Z

    .line 1139
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mListeners:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/videofling/MediaRouteController$Listener;

    .line 1140
    invoke-interface {v0}, Lcom/google/android/apps/chrome/videofling/MediaRouteController$Listener;->onSeekCompleted()V

    goto :goto_1

    .line 1145
    :cond_6
    invoke-virtual {v2}, Landroid/support/v7/media/a;->e()Landroid/os/Bundle;

    move-result-object v0

    .line 1146
    iget-boolean v1, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mDebug:Z

    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    .line 1147
    const-string/jumbo v1, "android.media.status.extra.HTTP_STATUS_CODE"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 1148
    const-string/jumbo v1, "android.media.status.extra.HTTP_STATUS_CODE"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    .line 1149
    const-string/jumbo v2, "MediaRouteController"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "HTTP status: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1151
    :cond_7
    const-string/jumbo v1, "android.media.status.extra.HTTP_RESPONSE_HEADERS"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1152
    const-string/jumbo v1, "android.media.status.extra.HTTP_RESPONSE_HEADERS"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    .line 1153
    const-string/jumbo v1, "MediaRouteController"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "HTTP headers: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method

.method public reconnectAnyExistingRoute()V
    .locals 4

    .prologue
    .line 322
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/apps/chrome/videofling/RemotePlaybackSettings;->getDeviceId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 323
    iget-object v1, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mMediaRouter:Landroid/support/v7/media/MediaRouter;

    invoke-static {}, Landroid/support/v7/media/MediaRouter;->b()Landroid/support/v7/media/MediaRouter$RouteInfo;

    move-result-object v1

    .line 324
    if-eqz v0, :cond_0

    invoke-virtual {v1}, Landroid/support/v7/media/MediaRouter$RouteInfo;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->shouldReconnect()Z

    move-result v1

    if-nez v1, :cond_1

    .line 325
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mContext:Landroid/content/Context;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/videofling/RemotePlaybackSettings;->setShouldReconnectToRemote(Landroid/content/Context;Z)V

    .line 341
    :goto_0
    return-void

    .line 328
    :cond_1
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mReconnecting:Z

    .line 329
    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->selectDevice(Ljava/lang/String;)V

    .line 330
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/chrome/videofling/MediaRouteController$2;

    invoke-direct {v1, p0}, Lcom/google/android/apps/chrome/videofling/MediaRouteController$2;-><init>(Lcom/google/android/apps/chrome/videofling/MediaRouteController;)V

    const-wide/16 v2, 0x2710

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method

.method protected final registerRoute(Landroid/support/v7/media/MediaRouter$RouteInfo;)V
    .locals 3

    .prologue
    .line 1012
    iput-object p1, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mCurrentRoute:Landroid/support/v7/media/MediaRouter$RouteInfo;

    .line 1014
    if-eqz p1, :cond_2

    .line 1015
    invoke-virtual {p1}, Landroid/support/v7/media/MediaRouter$RouteInfo;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mDeviceId:Ljava/lang/String;

    .line 1016
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mDebug:Z

    if-eqz v0, :cond_0

    const-string/jumbo v0, "MediaRouteController"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "Selected route "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mDeviceId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1017
    :cond_0
    invoke-virtual {p1}, Landroid/support/v7/media/MediaRouter$RouteInfo;->g()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1018
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mDeviceId:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/videofling/RemotePlaybackSettings;->setDeviceId(Landroid/content/Context;Ljava/lang/String;)V

    .line 1023
    :cond_1
    :goto_0
    return-void

    .line 1021
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mContext:Landroid/content/Context;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/videofling/RemotePlaybackSettings;->setDeviceId(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public release()V
    .locals 4

    .prologue
    .line 687
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->getOwnerTab()Lorg/chromium/chrome/browser/Tab;

    move-result-object v1

    .line 688
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mListeners:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/videofling/MediaRouteController$Listener;

    .line 689
    iget v3, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mNativePlayerId:I

    invoke-interface {v0, v1, v3, p0}, Lcom/google/android/apps/chrome/videofling/MediaRouteController$Listener;->onRouteUnselected(Lorg/chromium/chrome/browser/Tab;ILcom/google/android/apps/chrome/videofling/MediaRouteController;)V

    goto :goto_0

    .line 691
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->setOwnerTab(Lorg/chromium/chrome/browser/Tab;)V

    .line 693
    invoke-direct {p0, v1}, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->stopAndDisconnect(Lorg/chromium/chrome/browser/Tab;)V

    .line 694
    return-void
.end method

.method protected removeAllListeners()V
    .locals 1

    .prologue
    .line 314
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mListeners:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 315
    return-void
.end method

.method public removeListener(Lcom/google/android/apps/chrome/videofling/MediaRouteController$Listener;)V
    .locals 1

    .prologue
    .line 310
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mListeners:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 311
    return-void
.end method

.method public resume()V
    .locals 3

    .prologue
    .line 429
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mCurrentItemId:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 450
    :goto_0
    return-void

    .line 431
    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "android.media.intent.action.RESUME"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 432
    const-string/jumbo v1, "android.media.intent.category.REMOTE_PLAYBACK"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 433
    const-string/jumbo v1, "android.media.intent.extra.SESSION_ID"

    iget-object v2, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mCurrentSessionId:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 434
    new-instance v1, Lcom/google/android/apps/chrome/videofling/MediaRouteController$3;

    invoke-direct {v1, p0}, Lcom/google/android/apps/chrome/videofling/MediaRouteController$3;-><init>(Lcom/google/android/apps/chrome/videofling/MediaRouteController;)V

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->sendIntentToRoute(Landroid/content/Intent;Lcom/google/android/apps/chrome/videofling/MediaRouteController$ResultBundleHandler;)V

    .line 449
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->updateState(I)V

    goto :goto_0
.end method

.method public routeIsDefaultRoute()Z
    .locals 1

    .prologue
    .line 805
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mCurrentRoute:Landroid/support/v7/media/MediaRouter$RouteInfo;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mCurrentRoute:Landroid/support/v7/media/MediaRouter$RouteInfo;

    invoke-virtual {v0}, Landroid/support/v7/media/MediaRouter$RouteInfo;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public seekTo(I)V
    .locals 4

    .prologue
    .line 656
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->getPosition()I

    move-result v0

    if-ne p1, v0, :cond_0

    .line 681
    :goto_0
    return-void

    .line 660
    :cond_0
    iput p1, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mLastKnownStreamPosition:I

    .line 661
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mSeeking:Z

    .line 662
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "android.media.intent.action.SEEK"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 663
    const-string/jumbo v1, "android.media.intent.category.REMOTE_PLAYBACK"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 664
    const-string/jumbo v1, "android.media.intent.extra.SESSION_ID"

    iget-object v2, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mCurrentSessionId:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 665
    const-string/jumbo v1, "android.media.intent.extra.ITEM_ID"

    iget-object v2, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mCurrentItemId:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 666
    const-string/jumbo v1, "android.media.intent.extra.ITEM_POSITION"

    int-to-long v2, p1

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 667
    new-instance v1, Lcom/google/android/apps/chrome/videofling/MediaRouteController$7;

    invoke-direct {v1, p0}, Lcom/google/android/apps/chrome/videofling/MediaRouteController$7;-><init>(Lcom/google/android/apps/chrome/videofling/MediaRouteController;)V

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->sendIntentToRoute(Landroid/content/Intent;Lcom/google/android/apps/chrome/videofling/MediaRouteController$ResultBundleHandler;)V

    goto :goto_0
.end method

.method protected sendControlIntent(Landroid/content/Intent;Lcom/google/android/apps/chrome/videofling/MediaRouteController$ResultBundleHandler;)V
    .locals 3

    .prologue
    .line 1191
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mDebug:Z

    if-eqz v0, :cond_0

    .line 1192
    const-string/jumbo v0, "MediaRouteController"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "Sending intent to "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mCurrentRoute:Landroid/support/v7/media/MediaRouter$RouteInfo;

    invoke-virtual {v2}, Landroid/support/v7/media/MediaRouter$RouteInfo;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mCurrentRoute:Landroid/support/v7/media/MediaRouter$RouteInfo;

    invoke-virtual {v2}, Landroid/support/v7/media/MediaRouter$RouteInfo;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1194
    const-string/jumbo v0, "sendControlIntent "

    invoke-static {v0, p1}, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->dumpIntentToLog(Ljava/lang/String;Landroid/content/Intent;)V

    .line 1196
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mCurrentRoute:Landroid/support/v7/media/MediaRouter$RouteInfo;

    invoke-virtual {v0}, Landroid/support/v7/media/MediaRouter$RouteInfo;->g()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1197
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mDebug:Z

    if-eqz v0, :cond_1

    const-string/jumbo v0, "MediaRouteController"

    const-string/jumbo v1, "Route is default, not sending"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1227
    :cond_1
    :goto_0
    return-void

    .line 1201
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mCurrentRoute:Landroid/support/v7/media/MediaRouter$RouteInfo;

    new-instance v1, Lcom/google/android/apps/chrome/videofling/MediaRouteController$14;

    invoke-direct {v1, p0, p2, p1}, Lcom/google/android/apps/chrome/videofling/MediaRouteController$14;-><init>(Lcom/google/android/apps/chrome/videofling/MediaRouteController;Lcom/google/android/apps/chrome/videofling/MediaRouteController$ResultBundleHandler;Landroid/content/Intent;)V

    invoke-virtual {v0, p1, v1}, Landroid/support/v7/media/MediaRouter$RouteInfo;->a(Landroid/content/Intent;Landroid/support/v7/media/i;)V

    goto :goto_0
.end method

.method protected sendIntentToRoute(Landroid/content/Intent;Lcom/google/android/apps/chrome/videofling/MediaRouteController$ResultBundleHandler;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1168
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mCurrentRoute:Landroid/support/v7/media/MediaRouter$RouteInfo;

    if-nez v0, :cond_2

    .line 1169
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mDebug:Z

    if-eqz v0, :cond_0

    .line 1170
    const-string/jumbo v0, "sendIntentToRoute "

    invoke-static {v0, p1}, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->dumpIntentToLog(Ljava/lang/String;Landroid/content/Intent;)V

    .line 1171
    const-string/jumbo v0, "MediaRouteController"

    const-string/jumbo v1, "The current route is null."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1173
    :cond_0
    if-eqz p2, :cond_1

    invoke-interface {p2, v3, v3}, Lcom/google/android/apps/chrome/videofling/MediaRouteController$ResultBundleHandler;->onError(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 1187
    :cond_1
    :goto_0
    return-void

    .line 1177
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mCurrentRoute:Landroid/support/v7/media/MediaRouter$RouteInfo;

    invoke-virtual {v0, p1}, Landroid/support/v7/media/MediaRouter$RouteInfo;->a(Landroid/content/Intent;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 1178
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mDebug:Z

    if-eqz v0, :cond_3

    .line 1179
    const-string/jumbo v0, "sendIntentToRoute "

    invoke-static {v0, p1}, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->dumpIntentToLog(Ljava/lang/String;Landroid/content/Intent;)V

    .line 1180
    const-string/jumbo v0, "MediaRouteController"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "The intent is not supported by the route: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mCurrentRoute:Landroid/support/v7/media/MediaRouter$RouteInfo;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1182
    :cond_3
    if-eqz p2, :cond_1

    invoke-interface {p2, v3, v3}, Lcom/google/android/apps/chrome/videofling/MediaRouteController$ResultBundleHandler;->onError(Ljava/lang/String;Landroid/os/Bundle;)V

    goto :goto_0

    .line 1186
    :cond_4
    invoke-virtual {p0, p1, p2}, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->sendControlIntent(Landroid/content/Intent;Lcom/google/android/apps/chrome/videofling/MediaRouteController$ResultBundleHandler;)V

    goto :goto_0
.end method

.method public setDataSource(Landroid/net/Uri;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 1443
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mDebug:Z

    if-eqz v0, :cond_0

    const-string/jumbo v0, "MediaRouteController"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "setDataSource called, uri = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1444
    :cond_0
    iput-object p1, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mLocalVideoUri:Landroid/net/Uri;

    .line 1445
    iput-object p2, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mLocalVideoCookies:Ljava/lang/String;

    .line 1446
    return-void
.end method

.method public setNativePlayerId(I)V
    .locals 0

    .prologue
    .line 1425
    iput p1, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mNativePlayerId:I

    .line 1426
    return-void
.end method

.method public setOwnerTab(Lorg/chromium/chrome/browser/Tab;)V
    .locals 0

    .prologue
    .line 1411
    iput-object p1, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mCurrentTab:Lorg/chromium/chrome/browser/Tab;

    .line 1412
    return-void
.end method

.method public setRemoteVolume(I)V
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 262
    iget-object v1, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mCurrentRoute:Landroid/support/v7/media/MediaRouter$RouteInfo;

    invoke-virtual {v1}, Landroid/support/v7/media/MediaRouter$RouteInfo;->j()I

    move-result v1

    if-ne v1, v0, :cond_1

    .line 264
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->currentRouteSupportsRemotePlayback()Z

    move-result v1

    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    .line 265
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mCurrentRoute:Landroid/support/v7/media/MediaRouter$RouteInfo;

    invoke-virtual {v0, p1}, Landroid/support/v7/media/MediaRouter$RouteInfo;->b(I)V

    .line 267
    :cond_0
    return-void

    .line 262
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setUri(Landroid/net/Uri;[Lorg/apache/http/Header;)V
    .locals 2

    .prologue
    .line 1487
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->canPlayMedia(Landroid/net/Uri;[Lorg/apache/http/Header;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1488
    iput-object p1, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mLocalVideoUri:Landroid/net/Uri;

    .line 1489
    invoke-direct {p0}, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->playMedia()V

    .line 1497
    :goto_0
    return-void

    .line 1492
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mLocalVideoUri:Landroid/net/Uri;

    .line 1493
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mContext:Landroid/content/Context;

    sget v1, Lcom/google/android/apps/chrome/R$string;->cast_permission_error_playing_video:I

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->showMessageToast(Ljava/lang/String;)V

    .line 1496
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->release()V

    goto :goto_0
.end method

.method protected showCastError(Ljava/lang/String;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 819
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mContext:Landroid/content/Context;

    sget v2, Lcom/google/android/apps/chrome/R$string;->cast_error_playing_video:I

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    aput-object p1, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    .line 823
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 824
    return-void
.end method

.method public startDiscovery()V
    .locals 3

    .prologue
    .line 968
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mDiscoveryStarted:Z

    if-eqz v0, :cond_0

    .line 980
    :goto_0
    return-void

    .line 970
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mDiscoveryStarted:Z

    .line 974
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mContext:Landroid/content/Context;

    invoke-static {v0}, Landroid/support/v7/media/MediaRouter;->a(Landroid/content/Context;)Landroid/support/v7/media/MediaRouter;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->buildMediaRouteSelector()Landroid/support/v7/media/e;

    move-result-object v1

    const/4 v2, 0x5

    invoke-virtual {v0, v1, p0, v2}, Landroid/support/v7/media/MediaRouter;->a(Landroid/support/v7/media/e;Landroid/support/v7/media/g;I)V
    :try_end_0
    .catch Ljava/lang/NoSuchMethodError; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 977
    :catch_0
    move-exception v0

    .line 978
    const-string/jumbo v1, "MediaRouteController"

    invoke-virtual {v0}, Ljava/lang/NoSuchMethodError;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method protected updateState(I)V
    .locals 8

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 1253
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mDebug:Z

    if-eqz v0, :cond_0

    .line 1254
    const-string/jumbo v0, "MediaRouteController"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "updateState oldState: "

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mPlaybackState:Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v4, " newState: "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1257
    :cond_0
    iget-object v4, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mPlaybackState:Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;

    .line 1259
    sget-object v0, Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;->STOPPED:Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;

    .line 1260
    packed-switch p1, :pswitch_data_0

    move-object v1, v0

    .line 1288
    :goto_0
    iput-object v1, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mPlaybackState:Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;

    .line 1295
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mListeners:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/videofling/MediaRouteController$Listener;

    .line 1296
    iget-object v6, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mCurrentTab:Lorg/chromium/chrome/browser/Tab;

    iget v7, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mNativePlayerId:I

    invoke-interface {v0, v4, v1, v6, v7}, Lcom/google/android/apps/chrome/videofling/MediaRouteController$Listener;->onPlaybackStateChanged(Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;Lorg/chromium/chrome/browser/Tab;I)V

    goto :goto_1

    .line 1262
    :pswitch_0
    sget-object v0, Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;->LOADING:Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;

    move-object v1, v0

    .line 1263
    goto :goto_0

    .line 1265
    :pswitch_1
    sget-object v0, Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;->FINISHED:Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;

    move-object v1, v0

    .line 1266
    goto :goto_0

    .line 1268
    :pswitch_2
    sget-object v0, Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;->ERROR:Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;

    move-object v1, v0

    .line 1269
    goto :goto_0

    .line 1271
    :pswitch_3
    sget-object v0, Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;->FINISHED:Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;

    move-object v1, v0

    .line 1272
    goto :goto_0

    .line 1274
    :pswitch_4
    sget-object v0, Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;->INVALIDATED:Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;

    move-object v1, v0

    .line 1275
    goto :goto_0

    .line 1277
    :pswitch_5
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->getPosition()I

    move-result v0

    iget v1, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mStreamDuration:I

    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->isAtEndOfVideo(II)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1278
    sget-object v0, Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;->FINISHED:Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;

    move-object v1, v0

    goto :goto_0

    .line 1280
    :cond_1
    sget-object v0, Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;->PAUSED:Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;

    move-object v1, v0

    .line 1282
    goto :goto_0

    .line 1284
    :pswitch_6
    sget-object v0, Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;->PAUSED:Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;

    move-object v1, v0

    .line 1285
    goto :goto_0

    .line 1287
    :pswitch_7
    sget-object v0, Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;->PLAYING:Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;

    move-object v1, v0

    goto :goto_0

    .line 1299
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mPlaybackState:Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;

    if-eq v4, v0, :cond_3

    .line 1301
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mPlaybackState:Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;->name()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/videofling/RemotePlaybackSettings;->setLastVideoState(Landroid/content/Context;Ljava/lang/String;)V

    .line 1303
    sget-object v0, Lcom/google/android/apps/chrome/videofling/MediaRouteController$15;->$SwitchMap$com$google$android$apps$chrome$videofling$RemoteVideoInfo$PlayerState:[I

    iget-object v1, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mPlaybackState:Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_1

    .line 1326
    :cond_3
    :goto_2
    return-void

    .line 1305
    :pswitch_8
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mContext:Landroid/content/Context;

    iget v1, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mStreamDuration:I

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->getPosition()I

    move-result v4

    sub-int/2addr v1, v4

    int-to-long v4, v1

    invoke-static {v0, v4, v5}, Lcom/google/android/apps/chrome/videofling/RemotePlaybackSettings;->setRemainingTime(Landroid/content/Context;J)V

    .line 1307
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mContext:Landroid/content/Context;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-static {v0, v4, v5}, Lcom/google/android/apps/chrome/videofling/RemotePlaybackSettings;->setLastPlayedTime(Landroid/content/Context;J)V

    .line 1308
    iget-object v1, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mContext:Landroid/content/Context;

    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mCurrentRoute:Landroid/support/v7/media/MediaRouter$RouteInfo;

    invoke-virtual {v0}, Landroid/support/v7/media/MediaRouter$RouteInfo;->g()Z

    move-result v0

    if-nez v0, :cond_4

    move v0, v2

    :goto_3
    invoke-static {v1, v0}, Lcom/google/android/apps/chrome/videofling/RemotePlaybackSettings;->setShouldReconnectToRemote(Landroid/content/Context;Z)V

    .line 1310
    invoke-direct {p0}, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->setPrepared()V

    goto :goto_2

    :cond_4
    move v0, v3

    .line 1308
    goto :goto_3

    .line 1313
    :pswitch_9
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mCurrentRoute:Landroid/support/v7/media/MediaRouter$RouteInfo;

    invoke-virtual {v1}, Landroid/support/v7/media/MediaRouter$RouteInfo;->g()Z

    move-result v1

    if-nez v1, :cond_5

    :goto_4
    invoke-static {v0, v2}, Lcom/google/android/apps/chrome/videofling/RemotePlaybackSettings;->setShouldReconnectToRemote(Landroid/content/Context;Z)V

    .line 1315
    invoke-direct {p0}, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->setPrepared()V

    goto :goto_2

    :cond_5
    move v2, v3

    .line 1313
    goto :goto_4

    .line 1318
    :pswitch_a
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->release()V

    goto :goto_2

    .line 1321
    :pswitch_b
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->clearItemState()V

    goto :goto_2

    .line 1324
    :pswitch_c
    invoke-direct {p0, v2}, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->sendErrorToListeners(I)V

    .line 1325
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->release()V

    goto :goto_2

    .line 1260
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_6
        :pswitch_7
        :pswitch_5
        :pswitch_0
        :pswitch_3
        :pswitch_1
        :pswitch_4
        :pswitch_2
    .end packed-switch

    .line 1303
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
    .end packed-switch
.end method

.method protected updateTitle(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1247
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mListeners:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/videofling/MediaRouteController$Listener;

    .line 1248
    invoke-interface {v0, p1}, Lcom/google/android/apps/chrome/videofling/MediaRouteController$Listener;->onTitleChanged(Ljava/lang/String;)V

    goto :goto_0

    .line 1250
    :cond_0
    return-void
.end method
