.class Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItemsContainer;
.super Landroid/widget/GridView;
.source "EnhancedBookmarkItemsContainer.java"

# interfaces
.implements Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkUIObserver;


# instance fields
.field private mBookmarkModelObserver:Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkModelObserver;

.field private mDelegate:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDelegate;

.field private final mItemsAdapter:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItemsAdapter;

.field private mItemsGridScrollListener:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItemsContainer$ItemsGridScrollListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 68
    invoke-direct {p0, p1, p2}, Landroid/widget/GridView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 36
    new-instance v0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItemsAdapter;

    invoke-direct {v0}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItemsAdapter;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItemsContainer;->mItemsAdapter:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItemsAdapter;

    .line 39
    new-instance v0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItemsContainer$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItemsContainer$1;-><init>(Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItemsContainer;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItemsContainer;->mBookmarkModelObserver:Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkModelObserver;

    .line 69
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItemsContainer;)Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDelegate;
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItemsContainer;->mDelegate:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDelegate;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItemsContainer;)Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItemsAdapter;
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItemsContainer;->mItemsAdapter:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItemsAdapter;

    return-object v0
.end method


# virtual methods
.method protected computeVerticalScrollOffset()I
    .locals 2

    .prologue
    .line 106
    invoke-super {p0}, Landroid/widget/GridView;->computeVerticalScrollOffset()I

    move-result v0

    .line 107
    iget-object v1, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItemsContainer;->mItemsGridScrollListener:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItemsContainer$ItemsGridScrollListener;

    if-eqz v1, :cond_0

    .line 108
    iget-object v1, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItemsContainer;->mItemsGridScrollListener:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItemsContainer$ItemsGridScrollListener;

    invoke-interface {v1, v0}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItemsContainer$ItemsGridScrollListener;->onVerticalScrollOffsetChanged(I)V

    .line 110
    :cond_0
    return v0
.end method

.method public onAllBookmarksModeSet()V
    .locals 3

    .prologue
    .line 141
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItemsContainer;->mItemsAdapter:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItemsAdapter;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItemsContainer;->mDelegate:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDelegate;

    invoke-interface {v2}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDelegate;->getModel()Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarksModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarksModel;->getAllBookmarkIDsOrderedByCreationDate()Ljava/util/List;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItemsAdapter;->setBookmarks(Ljava/util/List;Ljava/util/List;)V

    .line 143
    return-void
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 131
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItemsContainer;->mDelegate:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDelegate;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDelegate;->getModel()Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarksModel;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItemsContainer;->mBookmarkModelObserver:Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkModelObserver;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarksModel;->removeModelObserver(Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkModelObserver;)V

    .line 132
    return-void
.end method

.method public onEmptyModeSet()V
    .locals 3

    .prologue
    .line 136
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItemsContainer;->mItemsAdapter:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItemsAdapter;

    const/4 v1, 0x0

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItemsAdapter;->setBookmarks(Ljava/util/List;Ljava/util/List;)V

    .line 137
    return-void
.end method

.method public onEnhancedBookmarkDelegateInitialized(Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDelegate;)V
    .locals 2

    .prologue
    .line 124
    iput-object p1, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItemsContainer;->mDelegate:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDelegate;

    .line 125
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItemsContainer;->mItemsAdapter:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItemsAdapter;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItemsAdapter;->setEnhancedBookmarkDelegate(Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDelegate;)V

    .line 126
    invoke-interface {p1}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDelegate;->getModel()Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarksModel;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItemsContainer;->mBookmarkModelObserver:Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkModelObserver;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarksModel;->addModelObserver(Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkModelObserver;)V

    .line 127
    return-void
.end method

.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 73
    invoke-super {p0}, Landroid/widget/GridView;->onFinishInflate()V

    .line 75
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItemsContainer;->mItemsAdapter:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItemsAdapter;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItemsContainer;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 76
    new-instance v0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItemsContainer$2;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItemsContainer$2;-><init>(Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItemsContainer;)V

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItemsContainer;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 94
    return-void
.end method

.method public onFolderModeSet(Lorg/chromium/components/bookmarks/BookmarkId;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 152
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItemsContainer;->mItemsAdapter:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItemsAdapter;

    iget-object v1, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItemsContainer;->mDelegate:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDelegate;

    invoke-interface {v1}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDelegate;->getModel()Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarksModel;

    move-result-object v1

    invoke-virtual {v1, p1, v4, v3}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarksModel;->getChildIDs(Lorg/chromium/components/bookmarks/BookmarkId;ZZ)Ljava/util/List;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItemsContainer;->mDelegate:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDelegate;

    invoke-interface {v2}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDelegate;->getModel()Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarksModel;

    move-result-object v2

    invoke-virtual {v2, p1, v3, v4}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarksModel;->getChildIDs(Lorg/chromium/components/bookmarks/BookmarkId;ZZ)Ljava/util/List;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItemsAdapter;->setBookmarks(Ljava/util/List;Ljava/util/List;)V

    .line 154
    return-void
.end method

.method protected onMeasure(II)V
    .locals 2

    .prologue
    .line 98
    invoke-super {p0, p1, p2}, Landroid/widget/GridView;->onMeasure(II)V

    .line 101
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItemsContainer;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItemsAdapter;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItemsContainer;->getNumColumns()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItemsAdapter;->setNumColumns(I)V

    .line 102
    return-void
.end method

.method public onUncategorizedModeSet()V
    .locals 3

    .prologue
    .line 147
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItemsContainer;->mItemsAdapter:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItemsAdapter;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItemsContainer;->mDelegate:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDelegate;

    invoke-interface {v2}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDelegate;->getModel()Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarksModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarksModel;->getUncategorizedBookmarkIDs()Ljava/util/List;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItemsAdapter;->setBookmarks(Ljava/util/List;Ljava/util/List;)V

    .line 148
    return-void
.end method

.method setOnScrollListener(Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItemsContainer$ItemsGridScrollListener;)V
    .locals 0

    .prologue
    .line 117
    iput-object p1, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItemsContainer;->mItemsGridScrollListener:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItemsContainer$ItemsGridScrollListener;

    .line 118
    return-void
.end method
