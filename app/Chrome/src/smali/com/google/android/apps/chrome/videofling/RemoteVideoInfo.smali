.class public Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo;
.super Ljava/lang/Object;
.source "RemoteVideoInfo.java"


# instance fields
.field public currentTimeMillis:I

.field public durationMillis:I

.field public errorMessage:Ljava/lang/String;

.field public state:Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;

.field public title:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo;)V
    .locals 6

    .prologue
    .line 60
    iget-object v1, p1, Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo;->title:Ljava/lang/String;

    iget v2, p1, Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo;->durationMillis:I

    iget-object v3, p1, Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo;->state:Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;

    iget v4, p1, Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo;->currentTimeMillis:I

    iget-object v5, p1, Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo;->errorMessage:Ljava/lang/String;

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo;-><init>(Ljava/lang/String;ILcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;ILjava/lang/String;)V

    .line 62
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;ILcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;ILjava/lang/String;)V
    .locals 0

    .prologue
    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    iput-object p1, p0, Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo;->title:Ljava/lang/String;

    .line 53
    iput p2, p0, Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo;->durationMillis:I

    .line 54
    iput-object p3, p0, Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo;->state:Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;

    .line 55
    iput p4, p0, Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo;->currentTimeMillis:I

    .line 56
    iput-object p5, p0, Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo;->errorMessage:Ljava/lang/String;

    .line 57
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 66
    if-ne p1, p0, :cond_1

    .line 74
    :cond_0
    :goto_0
    return v0

    .line 69
    :cond_1
    instance-of v2, p1, Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo;

    if-nez v2, :cond_2

    move v0, v1

    .line 70
    goto :goto_0

    .line 73
    :cond_2
    check-cast p1, Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo;

    .line 74
    iget v2, p0, Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo;->durationMillis:I

    iget v3, p1, Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo;->durationMillis:I

    if-ne v2, v3, :cond_3

    iget v2, p0, Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo;->currentTimeMillis:I

    iget v3, p1, Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo;->currentTimeMillis:I

    if-ne v2, v3, :cond_3

    iget-object v2, p0, Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo;->state:Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;

    iget-object v3, p1, Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo;->state:Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;

    if-ne v2, v3, :cond_3

    iget-object v2, p0, Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo;->title:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo;->title:Ljava/lang/String;

    invoke-static {v2, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo;->errorMessage:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo;->errorMessage:Ljava/lang/String;

    invoke-static {v2, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 83
    iget v0, p0, Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo;->durationMillis:I

    .line 84
    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo;->currentTimeMillis:I

    add-int/2addr v0, v2

    .line 85
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo;->title:Ljava/lang/String;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/2addr v0, v2

    .line 86
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo;->state:Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 87
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo;->errorMessage:Ljava/lang/String;

    if-nez v2, :cond_2

    :goto_2
    add-int/2addr v0, v1

    .line 88
    return v0

    .line 85
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo;->title:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    .line 86
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo;->state:Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;->hashCode()I

    move-result v0

    goto :goto_1

    .line 87
    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo;->errorMessage:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_2
.end method
