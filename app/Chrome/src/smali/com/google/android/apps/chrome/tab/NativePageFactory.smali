.class public Lcom/google/android/apps/chrome/tab/NativePageFactory;
.super Ljava/lang/Object;
.source "NativePageFactory.java"


# static fields
.field static final synthetic $assertionsDisabled:Z

.field public static final CHROME_NATIVE_SCHEME:Ljava/lang/String; = "chrome-native"

.field private static final sNativePageFactory:Lcom/google/android/apps/chrome/tab/NativePageFactory;


# instance fields
.field private mNativePageBuilder:Lcom/google/android/apps/chrome/tab/NativePageFactory$NativePageBuilder;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 25
    const-class v0, Lcom/google/android/apps/chrome/tab/NativePageFactory;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/tab/NativePageFactory;->$assertionsDisabled:Z

    .line 29
    new-instance v0, Lcom/google/android/apps/chrome/tab/NativePageFactory;

    new-instance v1, Lcom/google/android/apps/chrome/tab/NativePageFactory$NativePageBuilder;

    invoke-direct {v1}, Lcom/google/android/apps/chrome/tab/NativePageFactory$NativePageBuilder;-><init>()V

    invoke-direct {v0, v1}, Lcom/google/android/apps/chrome/tab/NativePageFactory;-><init>(Lcom/google/android/apps/chrome/tab/NativePageFactory$NativePageBuilder;)V

    sput-object v0, Lcom/google/android/apps/chrome/tab/NativePageFactory;->sNativePageFactory:Lcom/google/android/apps/chrome/tab/NativePageFactory;

    return-void

    .line 25
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lcom/google/android/apps/chrome/tab/NativePageFactory$NativePageBuilder;)V
    .locals 0

    .prologue
    .line 91
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 92
    iput-object p1, p0, Lcom/google/android/apps/chrome/tab/NativePageFactory;->mNativePageBuilder:Lcom/google/android/apps/chrome/tab/NativePageFactory$NativePageBuilder;

    .line 93
    return-void
.end method

.method public static getInstance()Lcom/google/android/apps/chrome/tab/NativePageFactory;
    .locals 1

    .prologue
    .line 34
    sget-object v0, Lcom/google/android/apps/chrome/tab/NativePageFactory;->sNativePageFactory:Lcom/google/android/apps/chrome/tab/NativePageFactory;

    return-object v0
.end method

.method public static isNativePageUrl(Ljava/lang/String;Z)Z
    .locals 2

    .prologue
    .line 150
    const/4 v0, 0x0

    invoke-static {p0, v0, p1}, Lcom/google/android/apps/chrome/tab/NativePageFactory;->nativePageType(Ljava/lang/String;Lorg/chromium/chrome/browser/NativePage;Z)Lcom/google/android/apps/chrome/tab/NativePageFactory$NativePageType;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/chrome/tab/NativePageFactory$NativePageType;->NONE:Lcom/google/android/apps/chrome/tab/NativePageFactory$NativePageType;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static nativePageType(Ljava/lang/String;Lorg/chromium/chrome/browser/NativePage;Z)Lcom/google/android/apps/chrome/tab/NativePageFactory$NativePageType;
    .locals 3

    .prologue
    .line 68
    if-nez p0, :cond_0

    sget-object v0, Lcom/google/android/apps/chrome/tab/NativePageFactory$NativePageType;->NONE:Lcom/google/android/apps/chrome/tab/NativePageFactory$NativePageType;

    .line 87
    :goto_0
    return-object v0

    .line 70
    :cond_0
    invoke-static {p0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 71
    const-string/jumbo v1, "chrome-native"

    invoke-virtual {v0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 72
    sget-object v0, Lcom/google/android/apps/chrome/tab/NativePageFactory$NativePageType;->NONE:Lcom/google/android/apps/chrome/tab/NativePageFactory$NativePageType;

    goto :goto_0

    .line 75
    :cond_1
    invoke-virtual {v0}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v0

    .line 76
    if-eqz p1, :cond_2

    invoke-interface {p1}, Lorg/chromium/chrome/browser/NativePage;->getHost()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 77
    sget-object v0, Lcom/google/android/apps/chrome/tab/NativePageFactory$NativePageType;->CANDIDATE:Lcom/google/android/apps/chrome/tab/NativePageFactory$NativePageType;

    goto :goto_0

    .line 80
    :cond_2
    const-string/jumbo v1, "newtab"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 81
    sget-object v0, Lcom/google/android/apps/chrome/tab/NativePageFactory$NativePageType;->NTP:Lcom/google/android/apps/chrome/tab/NativePageFactory$NativePageType;

    goto :goto_0

    .line 82
    :cond_3
    const-string/jumbo v1, "bookmarks"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 83
    sget-object v0, Lcom/google/android/apps/chrome/tab/NativePageFactory$NativePageType;->BOOKMARKS:Lcom/google/android/apps/chrome/tab/NativePageFactory$NativePageType;

    goto :goto_0

    .line 84
    :cond_4
    const-string/jumbo v1, "recent-tabs"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    if-nez p2, :cond_5

    .line 85
    sget-object v0, Lcom/google/android/apps/chrome/tab/NativePageFactory$NativePageType;->RECENT_TABS:Lcom/google/android/apps/chrome/tab/NativePageFactory$NativePageType;

    goto :goto_0

    .line 87
    :cond_5
    sget-object v0, Lcom/google/android/apps/chrome/tab/NativePageFactory$NativePageType;->NONE:Lcom/google/android/apps/chrome/tab/NativePageFactory$NativePageType;

    goto :goto_0
.end method


# virtual methods
.method public createNativePageForURL(Ljava/lang/String;Lorg/chromium/chrome/browser/NativePage;Lcom/google/android/apps/chrome/tab/ChromeTab;Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;Landroid/app/Activity;)Lorg/chromium/chrome/browser/NativePage;
    .locals 7

    .prologue
    .line 109
    invoke-virtual {p3}, Lcom/google/android/apps/chrome/tab/ChromeTab;->isIncognito()Z

    move-result v6

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/apps/chrome/tab/NativePageFactory;->createNativePageForURL(Ljava/lang/String;Lorg/chromium/chrome/browser/NativePage;Lcom/google/android/apps/chrome/tab/ChromeTab;Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;Landroid/app/Activity;Z)Lorg/chromium/chrome/browser/NativePage;

    move-result-object v0

    return-object v0
.end method

.method createNativePageForURL(Ljava/lang/String;Lorg/chromium/chrome/browser/NativePage;Lcom/google/android/apps/chrome/tab/ChromeTab;Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;Landroid/app/Activity;Z)Lorg/chromium/chrome/browser/NativePage;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 118
    sget-object v1, Lcom/google/android/apps/chrome/tab/NativePageFactory$1;->$SwitchMap$com$google$android$apps$chrome$tab$NativePageFactory$NativePageType:[I

    invoke-static {p1, p2, p6}, Lcom/google/android/apps/chrome/tab/NativePageFactory;->nativePageType(Ljava/lang/String;Lorg/chromium/chrome/browser/NativePage;Z)Lcom/google/android/apps/chrome/tab/NativePageFactory$NativePageType;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/tab/NativePageFactory$NativePageType;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 134
    sget-boolean v1, Lcom/google/android/apps/chrome/tab/NativePageFactory;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :pswitch_0
    move-object p2, v0

    .line 138
    :goto_0
    return-object p2

    .line 125
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/NativePageFactory;->mNativePageBuilder:Lcom/google/android/apps/chrome/tab/NativePageFactory$NativePageBuilder;

    invoke-virtual {v0, p5, p3, p4}, Lcom/google/android/apps/chrome/tab/NativePageFactory$NativePageBuilder;->buildNewTabPage(Landroid/app/Activity;Lcom/google/android/apps/chrome/tab/ChromeTab;Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;)Lorg/chromium/chrome/browser/NativePage;

    move-result-object p2

    .line 137
    :goto_1
    :pswitch_2
    invoke-interface {p2, p1}, Lorg/chromium/chrome/browser/NativePage;->updateForUrl(Ljava/lang/String;)V

    goto :goto_0

    .line 128
    :pswitch_3
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/NativePageFactory;->mNativePageBuilder:Lcom/google/android/apps/chrome/tab/NativePageFactory$NativePageBuilder;

    invoke-virtual {v0, p5, p3, p4}, Lcom/google/android/apps/chrome/tab/NativePageFactory$NativePageBuilder;->buildBookmarksPage(Landroid/app/Activity;Lorg/chromium/chrome/browser/Tab;Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;)Lorg/chromium/chrome/browser/NativePage;

    move-result-object p2

    goto :goto_1

    .line 131
    :pswitch_4
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/NativePageFactory;->mNativePageBuilder:Lcom/google/android/apps/chrome/tab/NativePageFactory$NativePageBuilder;

    invoke-virtual {v0, p5, p3}, Lcom/google/android/apps/chrome/tab/NativePageFactory$NativePageBuilder;->buildRecentTabsPage(Landroid/app/Activity;Lcom/google/android/apps/chrome/tab/ChromeTab;)Lorg/chromium/chrome/browser/NativePage;

    move-result-object p2

    goto :goto_1

    :cond_0
    move-object p2, v0

    .line 135
    goto :goto_0

    .line 118
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method
