.class public interface abstract Lcom/google/android/apps/chrome/videofling/TransportControl$Listener;
.super Ljava/lang/Object;
.source "TransportControl.java"


# virtual methods
.method public abstract onMute()V
.end method

.method public abstract onPause()V
.end method

.method public abstract onPlay()V
.end method

.method public abstract onSeek(I)V
.end method

.method public abstract onSelect(Landroid/content/Context;)V
.end method

.method public abstract onStop()V
.end method

.method public abstract onUnmute()V
.end method
