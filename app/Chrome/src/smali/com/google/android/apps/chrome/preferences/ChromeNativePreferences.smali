.class public final Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;
.super Ljava/lang/Object;
.source "ChromeNativePreferences.java"


# static fields
.field public static final EXCEPTION_SETTING_ALLOW:Ljava/lang/String; = "allow"

.field public static final EXCEPTION_SETTING_BLOCK:Ljava/lang/String; = "block"

.field public static final EXCEPTION_SETTING_DEFAULT:Ljava/lang/String; = "default"

.field public static final SUPERVISED_USER_FILTERING_ALLOW:I = 0x0

.field public static final SUPERVISED_USER_FILTERING_BLOCK:I = 0x2

.field public static final SUPERVISED_USER_FILTERING_WARN:I = 0x1

.field private static sInstance:Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;

.field private static sProfilePathValue:Ljava/lang/String;


# instance fields
.field private mAcceptCookiesEnabled:Z

.field private mAcceptCookiesManaged:Z

.field private mAllowLocationEnabled:Z

.field private mAllowLocationManaged:Z

.field private mAllowPopupsEnabled:Z

.field private mAllowPopupsManaged:Z

.field private mAutologinEnabled:Z

.field private mClearBrowsingDataListener:Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences$OnClearBrowsingDataListener;

.field private mCrashReportManaged:Z

.field private mDefaultSupervisedUserFilteringBehavior:I

.field private mDoNotTrackEnabled:Z

.field private mFirstRunEulaAccepted:Z

.field private mForceSafeSearch:Z

.field private mIncognitoModeEnabled:Z

.field private mIncognitoModeManaged:Z

.field private mJavaScriptEnabled:Z

.field private mJavaScriptManaged:Z

.field private mNetworkPredictionManaged:Z

.field private mNetworkPredictionOptions:I

.field private mPasswordEchoEnabled:Z

.field private mPrintingEnabled:Z

.field private mProtectedMediaIdentifierEnabled:Z

.field private mRememberPasswordsEnabled:Z

.field private mRememberPasswordsManaged:Z

.field private mResolveNavigationErrorEnabled:Z

.field private mResolveNavigationErrorManaged:Z

.field private mSearchSuggestEnabled:Z

.field private mSearchSuggestManaged:Z

.field private mTranslateEnabled:Z

.field private mTranslateManaged:Z

.field private mVoiceAndVideoCallingEnabled:Z


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 163
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 164
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->update()V

    .line 165
    return-void
.end method

.method private browsingDataCleared()V
    .locals 1

    .prologue
    .line 556
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->mClearBrowsingDataListener:Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences$OnClearBrowsingDataListener;

    if-eqz v0, :cond_0

    .line 557
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->mClearBrowsingDataListener:Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences$OnClearBrowsingDataListener;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences$OnClearBrowsingDataListener;->onBrowsingDataCleared()V

    .line 558
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->mClearBrowsingDataListener:Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences$OnClearBrowsingDataListener;

    .line 560
    :cond_0
    return-void
.end method

.method private static createAboutVersionStrings(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences$AboutVersionStrings;
    .locals 6

    .prologue
    .line 159
    new-instance v0, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences$AboutVersionStrings;

    const/4 v5, 0x0

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences$AboutVersionStrings;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences$1;)V

    return-object v0
.end method

.method public static getDefaultCountryCodeAtInstall(Landroid/content/Context;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 190
    invoke-static {}, Lorg/chromium/base/CommandLine;->getInstance()Lorg/chromium/base/CommandLine;

    move-result-object v0

    .line 191
    const-string/jumbo v1, "default-country-code"

    invoke-virtual {v0, v1}, Lorg/chromium/base/CommandLine;->hasSwitch(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string/jumbo v1, "default-country-code"

    invoke-virtual {v0, v1}, Lorg/chromium/base/CommandLine;->getSwitchValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget-object v0, v0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v0}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static getInstance()Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;
    .locals 1

    .prologue
    .line 173
    sget-object v0, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->sInstance:Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;

    invoke-direct {v0}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;-><init>()V

    sput-object v0, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->sInstance:Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;

    .line 174
    :cond_0
    sget-object v0, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->sInstance:Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;

    return-object v0
.end method

.method private static insertPopupExceptionToList(Ljava/util/ArrayList;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 667
    new-instance v0, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences$PopupException;

    const/4 v1, 0x0

    invoke-direct {v0, p1, p2, p3, v1}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences$PopupException;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences$1;)V

    .line 668
    invoke-virtual {p0, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 669
    return-void
.end method

.method public static isInitialized()Z
    .locals 1

    .prologue
    .line 181
    sget-object v0, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->sInstance:Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private native nativeCanPredictNetworkActions()Z
.end method

.method private native nativeClearBrowsingData(ZZZZZ)V
.end method

.method private native nativeGet()V
.end method

.method private native nativeGetAboutVersionStrings()Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences$AboutVersionStrings;
.end method

.method private native nativeGetContextualSearchPreference()Ljava/lang/String;
.end method

.method private native nativeGetNetworkPredictionEnabledUserPrefValue()Z
.end method

.method private native nativeGetPopupExceptions(Ljava/lang/Object;)V
.end method

.method private native nativeGetSupervisedUserCustodianEmail()Ljava/lang/String;
.end method

.method private native nativeGetSupervisedUserCustodianName()Ljava/lang/String;
.end method

.method private native nativeGetSupervisedUserCustodianProfileImageURL()Ljava/lang/String;
.end method

.method private native nativeGetSupervisedUserSecondCustodianEmail()Ljava/lang/String;
.end method

.method private native nativeGetSupervisedUserSecondCustodianName()Ljava/lang/String;
.end method

.method private native nativeGetSupervisedUserSecondCustodianProfileImageURL()Ljava/lang/String;
.end method

.method private native nativeGetSyncLastAccountName()Ljava/lang/String;
.end method

.method private native nativeNetworkPredictionEnabledHasUserSetting()Z
.end method

.method private native nativeNetworkPredictionOptionsHasUserSetting()Z
.end method

.method private native nativeRemovePopupException(Ljava/lang/String;)V
.end method

.method private native nativeResetAcceptLanguages(Ljava/lang/String;)V
.end method

.method private native nativeResetTranslateDefaults()V
.end method

.method private native nativeSetAllowCookiesEnabled(Z)V
.end method

.method private native nativeSetAllowLocationEnabled(Z)V
.end method

.method private native nativeSetAllowPopupsEnabled(Z)V
.end method

.method private native nativeSetAutologinEnabled(Z)V
.end method

.method private native nativeSetContextualSearchPreference(Ljava/lang/String;)V
.end method

.method private native nativeSetCrashReporting(Z)V
.end method

.method private native nativeSetDoNotTrackEnabled(Z)V
.end method

.method private native nativeSetEulaAccepted()V
.end method

.method private native nativeSetJavaScriptEnabled(Z)V
.end method

.method private native nativeSetNetworkPredictionOptions(I)V
.end method

.method private native nativeSetPasswordEchoEnabled(Z)V
.end method

.method private native nativeSetPathValuesForAboutChrome()V
.end method

.method private native nativeSetPopupException(Ljava/lang/String;Z)V
.end method

.method private native nativeSetProtectedMediaIdentifierEnabled(Z)V
.end method

.method private native nativeSetRememberPasswordsEnabled(Z)V
.end method

.method private native nativeSetResolveNavigationErrorEnabled(Z)V
.end method

.method private native nativeSetSearchSuggestEnabled(Z)V
.end method

.method private native nativeSetTranslateEnabled(Z)V
.end method

.method public static setProfilePathValue(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 218
    sput-object p0, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->sProfilePathValue:Ljava/lang/String;

    .line 219
    return-void
.end method


# virtual methods
.method public final canPredictNetworkActions()Z
    .locals 1

    .prologue
    .line 480
    invoke-direct {p0}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->nativeCanPredictNetworkActions()Z

    move-result v0

    return v0
.end method

.method public final clearBrowsingData(Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences$OnClearBrowsingDataListener;ZZZZZ)V
    .locals 6

    .prologue
    .line 549
    iput-object p1, p0, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->mClearBrowsingDataListener:Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences$OnClearBrowsingDataListener;

    move-object v0, p0

    move v1, p2

    move v2, p3

    move v3, p4

    move v4, p5

    move v5, p6

    .line 550
    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->nativeClearBrowsingData(ZZZZZ)V

    .line 552
    return-void
.end method

.method public final getAboutVersionStrings()Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences$AboutVersionStrings;
    .locals 1

    .prologue
    .line 676
    invoke-direct {p0}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->nativeGetAboutVersionStrings()Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences$AboutVersionStrings;

    move-result-object v0

    return-object v0
.end method

.method public final getContextualSearchPreference()Ljava/lang/String;
    .locals 1

    .prologue
    .line 390
    invoke-direct {p0}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->nativeGetContextualSearchPreference()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final getDefaultSupervisedUserFilteringBehavior()I
    .locals 1

    .prologue
    .line 708
    iget v0, p0, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->mDefaultSupervisedUserFilteringBehavior:I

    return v0
.end method

.method public final getNetworkPredictionEnabledUserPrefValue()Z
    .locals 1

    .prologue
    .line 449
    invoke-direct {p0}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->nativeGetNetworkPredictionEnabledUserPrefValue()Z

    move-result v0

    return v0
.end method

.method public final getNetworkPredictionOptions()Lcom/google/android/apps/chrome/preferences/NetworkPredictionOptions;
    .locals 1

    .prologue
    .line 456
    iget v0, p0, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->mNetworkPredictionOptions:I

    invoke-static {v0}, Lcom/google/android/apps/chrome/preferences/NetworkPredictionOptions;->intToEnum(I)Lcom/google/android/apps/chrome/preferences/NetworkPredictionOptions;

    move-result-object v0

    return-object v0
.end method

.method public final getPasswordEchoEnabled()Z
    .locals 1

    .prologue
    .line 271
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->mPasswordEchoEnabled:Z

    return v0
.end method

.method public final getPopupExceptions()Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 658
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 659
    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->nativeGetPopupExceptions(Ljava/lang/Object;)V

    .line 660
    return-object v0
.end method

.method public final getProfilePathValue()Ljava/lang/String;
    .locals 1

    .prologue
    .line 210
    sget-object v0, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->sProfilePathValue:Ljava/lang/String;

    return-object v0
.end method

.method public final getSupervisedUserCustodianEmail()Ljava/lang/String;
    .locals 1

    .prologue
    .line 716
    invoke-direct {p0}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->nativeGetSupervisedUserCustodianEmail()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final getSupervisedUserSecondCustodianEmail()Ljava/lang/String;
    .locals 1

    .prologue
    .line 728
    invoke-direct {p0}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->nativeGetSupervisedUserSecondCustodianEmail()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final getSyncLastAccountName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 339
    invoke-direct {p0}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->nativeGetSyncLastAccountName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final isAcceptCookiesEnabled()Z
    .locals 1

    .prologue
    .line 225
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->mAcceptCookiesEnabled:Z

    return v0
.end method

.method public final isAcceptCookiesManaged()Z
    .locals 1

    .prologue
    .line 232
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->mAcceptCookiesManaged:Z

    return v0
.end method

.method public final isAllowLocationEnabled()Z
    .locals 1

    .prologue
    .line 253
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->mAllowLocationEnabled:Z

    return v0
.end method

.method public final isAllowLocationManaged()Z
    .locals 1

    .prologue
    .line 260
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->mAllowLocationManaged:Z

    return v0
.end method

.method public final isAutologinEnabled()Z
    .locals 1

    .prologue
    .line 354
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->mAutologinEnabled:Z

    return v0
.end method

.method public final isContextualSearchDisabled()Z
    .locals 2

    .prologue
    .line 409
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->getContextualSearchPreference()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "false"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final isContextualSearchUninitialized()Z
    .locals 1

    .prologue
    .line 417
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->getContextualSearchPreference()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    return v0
.end method

.method public final isCrashReportManaged()Z
    .locals 1

    .prologue
    .line 501
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->mCrashReportManaged:Z

    return v0
.end method

.method public final isDoNotTrackEnabled()Z
    .locals 1

    .prologue
    .line 267
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->mDoNotTrackEnabled:Z

    return v0
.end method

.method public final isFirstRunEulaAccepted()Z
    .locals 1

    .prologue
    .line 278
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->mFirstRunEulaAccepted:Z

    return v0
.end method

.method public final isForceSafeSearch()Z
    .locals 1

    .prologue
    .line 701
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->mForceSafeSearch:Z

    return v0
.end method

.method public final isIncognitoModeEnabled()Z
    .locals 1

    .prologue
    .line 616
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->mIncognitoModeEnabled:Z

    return v0
.end method

.method public final isIncognitoModeManaged()Z
    .locals 1

    .prologue
    .line 623
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->mIncognitoModeManaged:Z

    return v0
.end method

.method public final isNetworkPredictionManaged()Z
    .locals 1

    .prologue
    .line 471
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->mNetworkPredictionManaged:Z

    return v0
.end method

.method public final isPopupsManaged()Z
    .locals 1

    .prologue
    .line 599
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->mAllowPopupsManaged:Z

    return v0
.end method

.method public final isPrintingEnabled()Z
    .locals 1

    .prologue
    .line 630
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->mPrintingEnabled:Z

    return v0
.end method

.method public final isProtectedMediaIdentifierEnabled()Z
    .locals 1

    .prologue
    .line 508
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->mProtectedMediaIdentifierEnabled:Z

    return v0
.end method

.method public final isRememberPasswordsEnabled()Z
    .locals 1

    .prologue
    .line 239
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->mRememberPasswordsEnabled:Z

    return v0
.end method

.method public final isRememberPasswordsManaged()Z
    .locals 1

    .prologue
    .line 246
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->mRememberPasswordsManaged:Z

    return v0
.end method

.method public final isResolveNavigationErrorEnabled()Z
    .locals 1

    .prologue
    .line 487
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->mResolveNavigationErrorEnabled:Z

    return v0
.end method

.method public final isResolveNavigationErrorManaged()Z
    .locals 1

    .prologue
    .line 494
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->mResolveNavigationErrorManaged:Z

    return v0
.end method

.method public final isSearchSuggestEnabled()Z
    .locals 1

    .prologue
    .line 368
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->mSearchSuggestEnabled:Z

    return v0
.end method

.method public final isSearchSuggestManaged()Z
    .locals 1

    .prologue
    .line 383
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->mSearchSuggestManaged:Z

    return v0
.end method

.method public final isTranslateEnabled()Z
    .locals 1

    .prologue
    .line 515
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->mTranslateEnabled:Z

    return v0
.end method

.method public final isTranslateManaged()Z
    .locals 1

    .prologue
    .line 522
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->mTranslateManaged:Z

    return v0
.end method

.method public final javaScriptEnabled()Z
    .locals 1

    .prologue
    .line 286
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->mJavaScriptEnabled:Z

    return v0
.end method

.method public final javaScriptManaged()Z
    .locals 1

    .prologue
    .line 293
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->mJavaScriptManaged:Z

    return v0
.end method

.method public final networkPredictionEnabledHasUserSetting()Z
    .locals 1

    .prologue
    .line 433
    invoke-direct {p0}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->nativeNetworkPredictionEnabledHasUserSetting()Z

    move-result v0

    return v0
.end method

.method public final networkPredictionOptionsHasUserSetting()Z
    .locals 1

    .prologue
    .line 441
    invoke-direct {p0}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->nativeNetworkPredictionOptionsHasUserSetting()Z

    move-result v0

    return v0
.end method

.method public final popupsEnabled()Z
    .locals 1

    .prologue
    .line 592
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->mAllowPopupsEnabled:Z

    return v0
.end method

.method public final removePopupException(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 649
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->nativeRemovePopupException(Ljava/lang/String;)V

    .line 650
    return-void
.end method

.method public final resetAcceptLanguages(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 694
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->nativeResetAcceptLanguages(Ljava/lang/String;)V

    .line 695
    return-void
.end method

.method public final resetTranslateDefaults()V
    .locals 0

    .prologue
    .line 323
    invoke-direct {p0}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->nativeResetTranslateDefaults()V

    .line 324
    return-void
.end method

.method public final setAllowCookiesEnabled(Z)V
    .locals 0

    .prologue
    .line 563
    iput-boolean p1, p0, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->mAcceptCookiesEnabled:Z

    .line 564
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->nativeSetAllowCookiesEnabled(Z)V

    .line 565
    return-void
.end method

.method public final setAllowLocationEnabled(Z)V
    .locals 1

    .prologue
    .line 578
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->mAllowLocationEnabled:Z

    if-ne p1, v0, :cond_0

    .line 581
    :goto_0
    return-void

    .line 579
    :cond_0
    iput-boolean p1, p0, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->mAllowLocationEnabled:Z

    .line 580
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->nativeSetAllowLocationEnabled(Z)V

    goto :goto_0
.end method

.method public final setAllowPopupsEnabled(Z)V
    .locals 0

    .prologue
    .line 608
    iput-boolean p1, p0, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->mAllowPopupsEnabled:Z

    .line 609
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->nativeSetAllowPopupsEnabled(Z)V

    .line 610
    return-void
.end method

.method public final setAutologinEnabled(Z)V
    .locals 0

    .prologue
    .line 346
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->nativeSetAutologinEnabled(Z)V

    .line 347
    iput-boolean p1, p0, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->mAutologinEnabled:Z

    .line 348
    return-void
.end method

.method public final setContextualSearchPreference(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 398
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->nativeSetContextualSearchPreference(Ljava/lang/String;)V

    .line 399
    const-string/jumbo v0, "true"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x4e

    .line 402
    :goto_0
    invoke-static {v0}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->broadcastNotification(I)V

    .line 403
    return-void

    .line 399
    :cond_0
    const/16 v0, 0x4d

    goto :goto_0
.end method

.method public final setContextualSearchState(Z)V
    .locals 1

    .prologue
    .line 424
    if-eqz p1, :cond_0

    const-string/jumbo v0, "true"

    :goto_0
    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->setContextualSearchPreference(Ljava/lang/String;)V

    .line 426
    return-void

    .line 424
    :cond_0
    const-string/jumbo v0, "false"

    goto :goto_0
.end method

.method public final setCrashReporting(Z)V
    .locals 0

    .prologue
    .line 361
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->nativeSetCrashReporting(Z)V

    .line 362
    return-void
.end method

.method public final setDoNotTrackEnabled(Z)V
    .locals 0

    .prologue
    .line 568
    iput-boolean p1, p0, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->mDoNotTrackEnabled:Z

    .line 569
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->nativeSetDoNotTrackEnabled(Z)V

    .line 570
    return-void
.end method

.method public final setEulaAccepted()V
    .locals 0

    .prologue
    .line 316
    invoke-direct {p0}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->nativeSetEulaAccepted()V

    .line 317
    return-void
.end method

.method public final setJavaScriptEnabled(Z)V
    .locals 1

    .prologue
    .line 331
    iput-boolean p1, p0, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->mJavaScriptEnabled:Z

    .line 332
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->mJavaScriptEnabled:Z

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->nativeSetJavaScriptEnabled(Z)V

    .line 333
    return-void
.end method

.method public final setNetworkPredictionOptions(Lcom/google/android/apps/chrome/preferences/NetworkPredictionOptions;)V
    .locals 1

    .prologue
    .line 463
    invoke-virtual {p1}, Lcom/google/android/apps/chrome/preferences/NetworkPredictionOptions;->enumToInt()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->mNetworkPredictionOptions:I

    .line 464
    iget v0, p0, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->mNetworkPredictionOptions:I

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->nativeSetNetworkPredictionOptions(I)V

    .line 465
    return-void
.end method

.method public final setPasswordEchoEnabled(Z)V
    .locals 0

    .prologue
    .line 584
    iput-boolean p1, p0, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->mPasswordEchoEnabled:Z

    .line 585
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->nativeSetPasswordEchoEnabled(Z)V

    .line 586
    return-void
.end method

.method public final setPathValuesForAboutChrome()V
    .locals 1

    .prologue
    .line 683
    sget-object v0, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->sProfilePathValue:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 684
    invoke-direct {p0}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->nativeSetPathValuesForAboutChrome()V

    .line 686
    :cond_0
    return-void
.end method

.method public final setPopupException(Ljava/lang/String;Z)V
    .locals 0

    .prologue
    .line 640
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->nativeSetPopupException(Ljava/lang/String;Z)V

    .line 641
    return-void
.end method

.method public final setProtectedMediaIdentifierEnabled(Z)V
    .locals 0

    .prologue
    .line 300
    iput-boolean p1, p0, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->mProtectedMediaIdentifierEnabled:Z

    .line 301
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->nativeSetProtectedMediaIdentifierEnabled(Z)V

    .line 302
    return-void
.end method

.method public final setRememberPasswordsEnabled(Z)V
    .locals 0

    .prologue
    .line 573
    iput-boolean p1, p0, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->mRememberPasswordsEnabled:Z

    .line 574
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->nativeSetRememberPasswordsEnabled(Z)V

    .line 575
    return-void
.end method

.method public final setResolveNavigationErrorEnabled(Z)V
    .locals 0

    .prologue
    .line 529
    iput-boolean p1, p0, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->mResolveNavigationErrorEnabled:Z

    .line 530
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->nativeSetResolveNavigationErrorEnabled(Z)V

    .line 531
    return-void
.end method

.method public final setSearchSuggestEnabled(Z)V
    .locals 0

    .prologue
    .line 375
    iput-boolean p1, p0, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->mSearchSuggestEnabled:Z

    .line 376
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->nativeSetSearchSuggestEnabled(Z)V

    .line 377
    return-void
.end method

.method public final setTranslateEnabled(Z)V
    .locals 0

    .prologue
    .line 308
    iput-boolean p1, p0, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->mTranslateEnabled:Z

    .line 309
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->nativeSetTranslateEnabled(Z)V

    .line 310
    return-void
.end method

.method public final update()V
    .locals 1

    .prologue
    .line 201
    invoke-static {}, Lorg/chromium/base/ThreadUtils;->assertOnUiThread()V

    .line 202
    invoke-static {}, Lorg/chromium/chrome/browser/search_engines/TemplateUrlService;->getInstance()Lorg/chromium/chrome/browser/search_engines/TemplateUrlService;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/search_engines/TemplateUrlService;->load()V

    .line 203
    invoke-direct {p0}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->nativeGet()V

    .line 204
    return-void
.end method
