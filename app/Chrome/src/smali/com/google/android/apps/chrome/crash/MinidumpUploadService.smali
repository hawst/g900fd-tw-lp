.class public Lcom/google/android/apps/chrome/crash/MinidumpUploadService;
.super Landroid/app/IntentService;
.source "MinidumpUploadService.java"


# static fields
.field static final ACTION_FIND_ALL:Ljava/lang/String; = "com.google.android.apps.chrome.crash.ACTION_FIND_ALL"

.field static final ACTION_UPLOAD:Ljava/lang/String; = "com.google.android.apps.chrome.crash.ACTION_UPLOAD"

.field static final FILE_TO_UPLOAD_KEY:Ljava/lang/String; = "minidump_file"

.field static final MAX_TRIES_ALLOWED:I = 0x3

.field static final UPLOAD_LOG_KEY:Ljava/lang/String; = "upload_log"


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 48
    const-string/jumbo v0, "MinidumpUploadService"

    invoke-direct {p0, v0}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    .line 49
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/crash/MinidumpUploadService;->setIntentRedelivery(Z)V

    .line 50
    return-void
.end method

.method static createFindAndUploadAllCrashesIntent(Landroid/content/Context;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 113
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/apps/chrome/crash/MinidumpUploadService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 114
    const-string/jumbo v1, "com.google.android.apps.chrome.crash.ACTION_FIND_ALL"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 115
    return-object v0
.end method

.method public static createFindAndUploadLastCrashIntent(Landroid/content/Context;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 73
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/apps/chrome/crash/MinidumpUploadService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 74
    const-string/jumbo v1, "com.google.android.apps.chrome.crash.ACTION_FIND_LAST"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 75
    return-object v0
.end method

.method public static createUploadIntent(Landroid/content/Context;Ljava/io/File;Ljava/io/File;)Landroid/content/Intent;
    .locals 3

    .prologue
    .line 137
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/apps/chrome/crash/MinidumpUploadService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 138
    const-string/jumbo v1, "com.google.android.apps.chrome.crash.ACTION_UPLOAD"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 139
    const-string/jumbo v1, "minidump_file"

    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 140
    const-string/jumbo v1, "upload_log"

    invoke-virtual {p2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 141
    return-object v0
.end method

.method private handleFindAndUploadAllCrashes()V
    .locals 6

    .prologue
    .line 119
    new-instance v0, Lcom/google/android/apps/chrome/crash/CrashFileManager;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/crash/MinidumpUploadService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/apps/chrome/crash/CrashFileManager;-><init>(Ljava/io/File;)V

    .line 120
    invoke-virtual {v0}, Lcom/google/android/apps/chrome/crash/CrashFileManager;->getAllMinidumpFiles()[Ljava/io/File;

    move-result-object v1

    .line 121
    invoke-virtual {v0}, Lcom/google/android/apps/chrome/crash/CrashFileManager;->getCrashUploadLogFile()Ljava/io/File;

    move-result-object v2

    .line 122
    const-string/jumbo v0, "MinidumpUploadService"

    const-string/jumbo v3, "Attempting to upload accumulated crash dumps."

    invoke-static {v0, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 123
    array-length v3, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_0

    aget-object v4, v1, v0

    .line 124
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/crash/MinidumpUploadService;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5, v4, v2}, Lcom/google/android/apps/chrome/crash/MinidumpUploadService;->createUploadIntent(Landroid/content/Context;Ljava/io/File;Ljava/io/File;)Landroid/content/Intent;

    move-result-object v4

    .line 125
    invoke-virtual {p0, v4}, Lcom/google/android/apps/chrome/crash/MinidumpUploadService;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 123
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 127
    :cond_0
    return-void
.end method

.method private handleFindAndUploadLastCrash(Landroid/content/Intent;)V
    .locals 3

    .prologue
    .line 92
    new-instance v0, Lcom/google/android/apps/chrome/crash/CrashFileManager;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/crash/MinidumpUploadService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/apps/chrome/crash/CrashFileManager;-><init>(Ljava/io/File;)V

    .line 93
    invoke-virtual {v0}, Lcom/google/android/apps/chrome/crash/CrashFileManager;->getAllMinidumpFilesSorted()[Ljava/io/File;

    move-result-object v1

    .line 94
    array-length v2, v1

    if-nez v2, :cond_0

    .line 96
    const-string/jumbo v0, "MinidumpUploadService"

    const-string/jumbo v1, "Could not find any crash dumps to upload"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 103
    :goto_0
    return-void

    .line 99
    :cond_0
    const/4 v2, 0x0

    aget-object v1, v1, v2

    .line 100
    invoke-virtual {v0}, Lcom/google/android/apps/chrome/crash/CrashFileManager;->getCrashUploadLogFile()Ljava/io/File;

    move-result-object v0

    .line 101
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/crash/MinidumpUploadService;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2, v1, v0}, Lcom/google/android/apps/chrome/crash/MinidumpUploadService;->createUploadIntent(Landroid/content/Context;Ljava/io/File;Ljava/io/File;)Landroid/content/Intent;

    move-result-object v0

    .line 102
    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/crash/MinidumpUploadService;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    goto :goto_0
.end method

.method private handleUploadCrash(Landroid/content/Intent;)V
    .locals 6

    .prologue
    const/4 v5, 0x3

    .line 145
    const-string/jumbo v0, "minidump_file"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 146
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 147
    :cond_0
    const-string/jumbo v0, "MinidumpUploadService"

    const-string/jumbo v1, "Cannot upload crash data since minidump is absent."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 188
    :goto_0
    return-void

    .line 150
    :cond_1
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 151
    invoke-virtual {v1}, Ljava/io/File;->isFile()Z

    move-result v2

    if-nez v2, :cond_2

    .line 152
    const-string/jumbo v1, "MinidumpUploadService"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "Cannot upload crash data since specified minidump "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v2, "is not present."

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 156
    :cond_2
    invoke-static {v0}, Lcom/google/android/apps/chrome/crash/CrashFileManager;->readAttemptNumber(Ljava/lang/String;)I

    move-result v2

    .line 157
    const/4 v3, -0x1

    if-eq v2, v3, :cond_3

    if-lt v2, v5, :cond_4

    .line 158
    :cond_3
    invoke-static {p0}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->incrementBreakpadUploadFailCount()V

    .line 159
    const-string/jumbo v1, "MinidumpUploadService"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "Giving up on trying to upload "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v3, " after "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v2, " attempts"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 164
    :cond_4
    const-string/jumbo v3, "upload_log"

    invoke-virtual {p1, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 165
    new-instance v4, Ljava/io/File;

    invoke-direct {v4, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 168
    invoke-virtual {p0, v1, v4}, Lcom/google/android/apps/chrome/crash/MinidumpUploadService;->createMinidumpUploadCallable(Ljava/io/File;Ljava/io/File;)Lcom/google/android/apps/chrome/crash/MinidumpUploadCallable;

    move-result-object v3

    .line 170
    invoke-virtual {v3}, Lcom/google/android/apps/chrome/crash/MinidumpUploadCallable;->call()Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    .line 172
    if-eqz v3, :cond_5

    .line 173
    invoke-static {p0}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->incrementBreakpadUploadSuccessCount()V

    goto/16 :goto_0

    .line 179
    :cond_5
    invoke-static {v1}, Lcom/google/android/apps/chrome/crash/CrashFileManager;->tryIncrementAttemptNumber(Ljava/io/File;)Ljava/lang/String;

    move-result-object v1

    .line 180
    add-int/lit8 v2, v2, 0x1

    .line 181
    if-eqz v1, :cond_6

    if-ge v2, v5, :cond_6

    .line 183
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/crash/MinidumpUploadService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/chrome/crash/MinidumpUploadRetry;->scheduleRetry(Landroid/content/Context;)V

    goto/16 :goto_0

    .line 185
    :cond_6
    const-string/jumbo v1, "MinidumpUploadService"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "Failed to rename minidump "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method

.method public static storeBreakpadUploadAttemptsInUma(Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 79
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->getBreakpadUploadSuccessCount()I

    move-result v0

    :goto_0
    if-lez v0, :cond_0

    .line 80
    invoke-static {}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->crashUploadAttemptSuccess()V

    .line 79
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 83
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->getBreakpadUploadFailCount()I

    move-result v0

    :goto_1
    if-lez v0, :cond_1

    .line 84
    invoke-static {}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->crashUploadAttemptFail()V

    .line 83
    add-int/lit8 v0, v0, -0x1

    goto :goto_1

    .line 87
    :cond_1
    invoke-virtual {p0, v1}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->setBreakpadUploadSuccessCount(I)V

    .line 88
    invoke-virtual {p0, v1}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->setBreakpadUploadFailCount(I)V

    .line 89
    return-void
.end method

.method public static tryUploadAllCrashDumps(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 215
    invoke-static {p0}, Lcom/google/android/apps/chrome/crash/MinidumpUploadService;->createFindAndUploadAllCrashesIntent(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    .line 216
    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 217
    return-void
.end method


# virtual methods
.method createMinidumpUploadCallable(Ljava/io/File;Ljava/io/File;)Lcom/google/android/apps/chrome/crash/MinidumpUploadCallable;
    .locals 2

    .prologue
    .line 201
    new-instance v0, Lcom/google/android/apps/chrome/crash/MinidumpUploadCallable;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/crash/MinidumpUploadService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, p1, p2, v1}, Lcom/google/android/apps/chrome/crash/MinidumpUploadCallable;-><init>(Ljava/io/File;Ljava/io/File;Landroid/content/Context;)V

    return-object v0
.end method

.method protected onHandleIntent(Landroid/content/Intent;)V
    .locals 3

    .prologue
    .line 54
    const-string/jumbo v0, "com.google.android.apps.chrome.crash.ACTION_FIND_LAST"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 55
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/crash/MinidumpUploadService;->handleFindAndUploadLastCrash(Landroid/content/Intent;)V

    .line 63
    :goto_0
    return-void

    .line 56
    :cond_0
    const-string/jumbo v0, "com.google.android.apps.chrome.crash.ACTION_FIND_ALL"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 57
    invoke-direct {p0}, Lcom/google/android/apps/chrome/crash/MinidumpUploadService;->handleFindAndUploadAllCrashes()V

    goto :goto_0

    .line 58
    :cond_1
    const-string/jumbo v0, "com.google.android.apps.chrome.crash.ACTION_UPLOAD"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 59
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/crash/MinidumpUploadService;->handleUploadCrash(Landroid/content/Intent;)V

    goto :goto_0

    .line 61
    :cond_2
    const-string/jumbo v0, "MinidumpUploadService"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "Got unknown action from intent: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
