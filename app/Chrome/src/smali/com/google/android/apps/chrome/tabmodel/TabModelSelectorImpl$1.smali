.class Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl$1;
.super Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;
.source "TabModelSelectorImpl.java"


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 108
    const-class v0, Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl$1;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>(Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl;)V
    .locals 0

    .prologue
    .line 108
    iput-object p1, p0, Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl$1;->this$0:Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl;

    invoke-direct {p0}, Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 111
    iget v0, p1, Landroid/os/Message;->what:I

    sparse-switch v0, :sswitch_data_0

    .line 166
    sget-boolean v0, Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl$1;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "Unexpected notification received for ID: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p1, Landroid/os/Message;->what:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 113
    :sswitch_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl$1;->this$0:Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl;

    # invokes: Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl;->handleOnOverviewModeShowStarted()V
    invoke-static {v0}, Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl;->access$000(Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl;)V

    .line 168
    :cond_0
    :goto_0
    return-void

    .line 116
    :sswitch_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl$1;->this$0:Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl;

    invoke-static {v0}, Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl;->access$100(Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl;)V

    goto :goto_0

    .line 123
    :sswitch_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl$1;->this$0:Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl;

    # invokes: Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl;->cacheCurrentTabBitmapIfNativePage()V
    invoke-static {v0}, Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl;->access$200(Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl;)V

    goto :goto_0

    .line 126
    :sswitch_3
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl$1;->this$0:Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl;

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v1

    const-string/jumbo v2, "tabId"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    # invokes: Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl;->handleOnPageLoadStopped(I)V
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl;->access$300(Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl;I)V

    goto :goto_0

    .line 129
    :sswitch_4
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl$1;->this$0:Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl;

    # getter for: Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl;->mSessionRestoreInProgress:Ljava/util/concurrent/atomic/AtomicBoolean;
    invoke-static {v0}, Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl;->access$400(Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl;)Ljava/util/concurrent/atomic/AtomicBoolean;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->getAndSet(Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 132
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl$1;->this$0:Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl;->getModel(Z)Lorg/chromium/chrome/browser/tabmodel/TabModel;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/tabmodel/TabModelImpl;

    .line 134
    if-eqz v0, :cond_1

    .line 135
    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabmodel/TabModelImpl;->broadcastSessionRestoreComplete()V

    goto :goto_0

    .line 137
    :cond_1
    sget-boolean v0, Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl$1;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    const-string/jumbo v1, "Normal tab model is null after tab state loaded."

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 142
    :sswitch_5
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl$1;->this$0:Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl;

    # getter for: Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl;->mUma:Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorUma;
    invoke-static {v0}, Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl;->access$500(Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl;)Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorUma;

    move-result-object v0

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v1

    const-string/jumbo v2, "tabId"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorUma;->onTabCrashed(I)V

    goto :goto_0

    .line 147
    :sswitch_6
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl$1;->this$0:Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl;->getCurrentModel()Lorg/chromium/chrome/browser/tabmodel/TabModel;

    move-result-object v0

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v1

    const-string/jumbo v2, "tabId"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    invoke-static {v0, v1}, Lorg/chromium/chrome/browser/tabmodel/TabModelUtils;->getTabById(Lorg/chromium/chrome/browser/tabmodel/TabList;I)Lorg/chromium/chrome/browser/Tab;

    move-result-object v0

    .line 149
    if-eqz v0, :cond_0

    .line 150
    iget-object v1, p0, Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl$1;->this$0:Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl;

    # getter for: Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl;->mTabContentManager:Lcom/google/android/apps/chrome/compositor/TabContentManager;
    invoke-static {v1}, Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl;->access$600(Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl;)Lcom/google/android/apps/chrome/compositor/TabContentManager;

    move-result-object v1

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/Tab;->getId()I

    move-result v2

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/Tab;->getUrl()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lcom/google/android/apps/chrome/compositor/TabContentManager;->invalidateIfChanged(ILjava/lang/String;)V

    goto/16 :goto_0

    .line 154
    :sswitch_7
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl$1;->this$0:Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl;

    # invokes: Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl;->handleAutoLoginDisabled()V
    invoke-static {v0}, Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl;->access$700(Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl;)V

    goto/16 :goto_0

    .line 157
    :sswitch_8
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl$1;->this$0:Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl;

    # getter for: Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl;->mUma:Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorUma;
    invoke-static {v0}, Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl;->access$500(Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl;)Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorUma;

    move-result-object v0

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v1

    const-string/jumbo v2, "tabId"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorUma;->onPageLoadFailed(I)V

    goto/16 :goto_0

    .line 160
    :sswitch_9
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl$1;->this$0:Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl;

    # getter for: Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl;->mUma:Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorUma;
    invoke-static {v0}, Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl;->access$500(Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl;)Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorUma;

    move-result-object v0

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v1

    const-string/jumbo v2, "tabId"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorUma;->onPageLoadFinished(I)V

    goto/16 :goto_0

    .line 163
    :sswitch_a
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl$1;->this$0:Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl;

    # getter for: Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl;->mUma:Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorUma;
    invoke-static {v0}, Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl;->access$500(Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl;)Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorUma;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorUma;->onTabsViewShown()V

    goto/16 :goto_0

    .line 111
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x1 -> :sswitch_1
        0x2 -> :sswitch_6
        0x6 -> :sswitch_5
        0x9 -> :sswitch_9
        0x10 -> :sswitch_4
        0x11 -> :sswitch_2
        0x19 -> :sswitch_6
        0x1b -> :sswitch_3
        0x1c -> :sswitch_8
        0x34 -> :sswitch_a
        0x3a -> :sswitch_7
        0x47 -> :sswitch_2
    .end sparse-switch
.end method
