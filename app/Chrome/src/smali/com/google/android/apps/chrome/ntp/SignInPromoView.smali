.class public Lcom/google/android/apps/chrome/ntp/SignInPromoView;
.super Landroid/widget/FrameLayout;
.source "SignInPromoView.java"


# instance fields
.field private mAccountFirstRunView:Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;

.field private mActivity:Landroid/app/Activity;

.field private mFadeAnimation:Landroid/animation/Animator;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 46
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 47
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/chrome/ntp/SignInPromoView;)Landroid/app/Activity;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/SignInPromoView;->mActivity:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic access$102(Lcom/google/android/apps/chrome/ntp/SignInPromoView;Landroid/animation/Animator;)Landroid/animation/Animator;
    .locals 0

    .prologue
    .line 32
    iput-object p1, p0, Lcom/google/android/apps/chrome/ntp/SignInPromoView;->mFadeAnimation:Landroid/animation/Animator;

    return-object p1
.end method

.method static synthetic access$200(Lcom/google/android/apps/chrome/ntp/SignInPromoView;)V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Lcom/google/android/apps/chrome/ntp/SignInPromoView;->removeSignInView()V

    return-void
.end method

.method private removeSignInView()V
    .locals 1

    .prologue
    .line 133
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/SignInPromoView;->mAccountFirstRunView:Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/ntp/SignInPromoView;->removeView(Landroid/view/View;)V

    .line 134
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/chrome/ntp/SignInPromoView;->mAccountFirstRunView:Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;

    .line 135
    return-void
.end method

.method private updateSyncPromoNotSignedIn()V
    .locals 3

    .prologue
    .line 59
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/SignInPromoView;->mFadeAnimation:Landroid/animation/Animator;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/SignInPromoView;->mFadeAnimation:Landroid/animation/Animator;

    invoke-virtual {v0}, Landroid/animation/Animator;->cancel()V

    .line 60
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/SignInPromoView;->mAccountFirstRunView:Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;

    if-eqz v0, :cond_1

    .line 110
    :goto_0
    return-void

    .line 62
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ntp/SignInPromoView;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 63
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x15

    if-ge v1, v2, :cond_2

    .line 66
    new-instance v0, Landroid/view/ContextThemeWrapper;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ntp/SignInPromoView;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x103006e

    invoke-direct {v0, v1, v2}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    .line 69
    :cond_2
    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Lcom/google/android/apps/chrome/R$layout;->fre_choose_account:I

    invoke-virtual {v0, v1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 70
    new-instance v1, Lcom/google/android/apps/chrome/firstrun/ProfileDataCache;

    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/SignInPromoView;->mActivity:Landroid/app/Activity;

    invoke-static {}, Lorg/chromium/chrome/browser/profiles/Profile;->getLastUsedProfile()Lorg/chromium/chrome/browser/profiles/Profile;

    move-result-object v2

    invoke-direct {v1, v0, v2}, Lcom/google/android/apps/chrome/firstrun/ProfileDataCache;-><init>(Landroid/app/Activity;Lorg/chromium/chrome/browser/profiles/Profile;)V

    .line 72
    sget v0, Lcom/google/android/apps/chrome/R$id;->fre_account_layout:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/ntp/SignInPromoView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;

    iput-object v0, p0, Lcom/google/android/apps/chrome/ntp/SignInPromoView;->mAccountFirstRunView:Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;

    .line 73
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/SignInPromoView;->mAccountFirstRunView:Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->init(Lcom/google/android/apps/chrome/firstrun/ProfileDataCache;)V

    .line 74
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/SignInPromoView;->mAccountFirstRunView:Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    const/4 v1, -0x2

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 75
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/SignInPromoView;->mAccountFirstRunView:Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->configureForRecentTabsPage()V

    .line 76
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/SignInPromoView;->mAccountFirstRunView:Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->setCanCancel(Z)V

    .line 77
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/SignInPromoView;->mAccountFirstRunView:Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;

    new-instance v1, Lcom/google/android/apps/chrome/ntp/SignInPromoView$1;

    invoke-direct {v1, p0}, Lcom/google/android/apps/chrome/ntp/SignInPromoView$1;-><init>(Lcom/google/android/apps/chrome/ntp/SignInPromoView;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->setListener(Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView$Listener;)V

    goto :goto_0
.end method

.method private updateSyncPromoSignedIn(Z)V
    .locals 5

    .prologue
    .line 113
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/SignInPromoView;->mAccountFirstRunView:Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/SignInPromoView;->mFadeAnimation:Landroid/animation/Animator;

    if-eqz v0, :cond_1

    .line 130
    :cond_0
    :goto_0
    return-void

    .line 114
    :cond_1
    if-eqz p1, :cond_2

    .line 115
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/SignInPromoView;->mAccountFirstRunView:Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->switchToSignedMode()V

    .line 117
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/SignInPromoView;->mAccountFirstRunView:Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;

    sget-object v1, Landroid/view/View;->ALPHA:Landroid/util/Property;

    const/4 v2, 0x1

    new-array v2, v2, [F

    const/4 v3, 0x0

    const/4 v4, 0x0

    aput v4, v2, v3

    invoke-static {v0, v1, v2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/ntp/SignInPromoView;->mFadeAnimation:Landroid/animation/Animator;

    .line 118
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/SignInPromoView;->mFadeAnimation:Landroid/animation/Animator;

    const-wide/16 v2, 0x12c

    invoke-virtual {v0, v2, v3}, Landroid/animation/Animator;->setDuration(J)Landroid/animation/Animator;

    .line 119
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/SignInPromoView;->mFadeAnimation:Landroid/animation/Animator;

    new-instance v1, Lcom/google/android/apps/chrome/ntp/SignInPromoView$2;

    invoke-direct {v1, p0}, Lcom/google/android/apps/chrome/ntp/SignInPromoView$2;-><init>(Lcom/google/android/apps/chrome/ntp/SignInPromoView;)V

    invoke-virtual {v0, v1}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 126
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/SignInPromoView;->mFadeAnimation:Landroid/animation/Animator;

    invoke-virtual {v0}, Landroid/animation/Animator;->start()V

    goto :goto_0

    .line 128
    :cond_2
    invoke-direct {p0}, Lcom/google/android/apps/chrome/ntp/SignInPromoView;->removeSignInView()V

    goto :goto_0
.end method


# virtual methods
.method public initialize(Landroid/app/Activity;)V
    .locals 0

    .prologue
    .line 55
    iput-object p1, p0, Lcom/google/android/apps/chrome/ntp/SignInPromoView;->mActivity:Landroid/app/Activity;

    .line 56
    return-void
.end method

.method public updateSyncPromoState(ZZ)V
    .locals 0

    .prologue
    .line 144
    if-eqz p1, :cond_0

    .line 145
    invoke-direct {p0, p2}, Lcom/google/android/apps/chrome/ntp/SignInPromoView;->updateSyncPromoSignedIn(Z)V

    .line 149
    :goto_0
    return-void

    .line 147
    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/chrome/ntp/SignInPromoView;->updateSyncPromoNotSignedIn()V

    goto :goto_0
.end method
