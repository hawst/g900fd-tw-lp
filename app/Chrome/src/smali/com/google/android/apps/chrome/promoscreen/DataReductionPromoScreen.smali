.class public Lcom/google/android/apps/chrome/promoscreen/DataReductionPromoScreen;
.super Landroid/app/Dialog;
.source "DataReductionPromoScreen.java"

# interfaces
.implements Landroid/content/DialogInterface$OnDismissListener;
.implements Landroid/view/View$OnClickListener;


# static fields
.field static final synthetic $assertionsDisabled:Z

.field public static final ACTION_INDEX_BOUNDARY:I = 0x4

.field public static final FROM_PROMO:Ljava/lang/String; = "FromPromo"


# instance fields
.field private mState:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 33
    const-class v0, Lcom/google/android/apps/chrome/promoscreen/DataReductionPromoScreen;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/promoscreen/DataReductionPromoScreen;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    const/4 v2, -0x1

    .line 84
    sget v0, Lcom/google/android/apps/chrome/R$style;->DataReductionPromoScreenDialog:I

    invoke-direct {p0, p1, v0}, Landroid/app/Dialog;-><init>(Landroid/content/Context;I)V

    .line 85
    invoke-static {p1}, Lcom/google/android/apps/chrome/promoscreen/DataReductionPromoScreen;->getContentView(Landroid/content/Context;)Landroid/view/View;

    move-result-object v0

    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v1, v2, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/chrome/promoscreen/DataReductionPromoScreen;->setContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 87
    return-void
.end method

.method private addListenerOnButton()V
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 108
    const/4 v1, 0x3

    new-array v1, v1, [I

    sget v2, Lcom/google/android/apps/chrome/R$id;->no_thanks_button:I

    aput v2, v1, v0

    const/4 v2, 0x1

    sget v3, Lcom/google/android/apps/chrome/R$id;->enable_button_front:I

    aput v3, v1, v2

    const/4 v2, 0x2

    sget v3, Lcom/google/android/apps/chrome/R$id;->close_button_front:I

    aput v3, v1, v2

    .line 114
    array-length v2, v1

    :goto_0
    if-ge v0, v2, :cond_0

    aget v3, v1, v0

    .line 115
    invoke-virtual {p0, v3}, Lcom/google/android/apps/chrome/promoscreen/DataReductionPromoScreen;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 114
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 117
    :cond_0
    return-void
.end method

.method private static getContentView(Landroid/content/Context;)Landroid/view/View;
    .locals 3

    .prologue
    .line 48
    const-string/jumbo v0, "layout_inflater"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 50
    sget v1, Lcom/google/android/apps/chrome/R$layout;->data_reduction_promo_screen:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method private getTouchDelegateRunnable(Landroid/content/Context;Landroid/view/View;Landroid/view/View;)Ljava/lang/Runnable;
    .locals 1

    .prologue
    .line 121
    new-instance v0, Lcom/google/android/apps/chrome/promoscreen/DataReductionPromoScreen$1;

    invoke-direct {v0, p0, p1, p3, p2}, Lcom/google/android/apps/chrome/promoscreen/DataReductionPromoScreen$1;-><init>(Lcom/google/android/apps/chrome/promoscreen/DataReductionPromoScreen;Landroid/content/Context;Landroid/view/View;Landroid/view/View;)V

    return-object v0
.end method

.method private handleEnableButtonPressed()V
    .locals 4

    .prologue
    .line 160
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/promoscreen/DataReductionPromoScreen;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 161
    if-nez v0, :cond_0

    .line 174
    :goto_0
    return-void

    .line 162
    :cond_0
    new-instance v1, Landroid/content/Intent;

    const-string/jumbo v2, "android.intent.action.VIEW"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 163
    const-class v2, Lcom/google/android/apps/chrome/preferences/Preferences;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->setClassName(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    .line 164
    const/high16 v2, 0x20020000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 166
    const-string/jumbo v2, ":android:show_fragment"

    const-class v3, Lcom/google/android/apps/chrome/preferences/bandwidth/BandwidthReductionPreferences;

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 168
    const-string/jumbo v2, ":android:show_fragment_title"

    sget v3, Lcom/google/android/apps/chrome/R$string;->reduce_data_usage_title:I

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 170
    const-string/jumbo v2, "display_home_as_up"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 171
    const-string/jumbo v2, "FromPromo"

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 172
    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 173
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/promoscreen/DataReductionPromoScreen;->dismiss()V

    goto :goto_0
.end method

.method public static launchDataReductionPromo(Landroid/app/Activity;)V
    .locals 1

    .prologue
    .line 60
    invoke-static {}, Lorg/chromium/chrome/browser/net/spdyproxy/DataReductionProxySettings;->getInstance()Lorg/chromium/chrome/browser/net/spdyproxy/DataReductionProxySettings;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/net/spdyproxy/DataReductionProxySettings;->isDataReductionProxyPromoAllowed()Z

    move-result v0

    if-nez v0, :cond_1

    .line 76
    :cond_0
    :goto_0
    return-void

    .line 64
    :cond_1
    invoke-static {}, Lorg/chromium/chrome/browser/net/spdyproxy/DataReductionProxySettings;->getInstance()Lorg/chromium/chrome/browser/net/spdyproxy/DataReductionProxySettings;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/net/spdyproxy/DataReductionProxySettings;->isDataReductionProxyManaged()Z

    move-result v0

    if-nez v0, :cond_0

    .line 65
    invoke-static {}, Lorg/chromium/chrome/browser/net/spdyproxy/DataReductionProxySettings;->getInstance()Lorg/chromium/chrome/browser/net/spdyproxy/DataReductionProxySettings;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/net/spdyproxy/DataReductionProxySettings;->isDataReductionProxyEnabled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 66
    invoke-static {p0}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;

    move-result-object v0

    .line 68
    invoke-virtual {v0}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->getDisplayedDataReductionPromo()Z

    move-result v0

    if-nez v0, :cond_0

    .line 71
    invoke-static {p0}, Lcom/google/android/apps/chrome/third_party/samsung/MultiWindowUtils;->isMultiWindow(Landroid/app/Activity;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 73
    new-instance v0, Lcom/google/android/apps/chrome/promoscreen/DataReductionPromoScreen;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/promoscreen/DataReductionPromoScreen;-><init>(Landroid/content/Context;)V

    .line 74
    invoke-virtual {v0, v0}, Lcom/google/android/apps/chrome/promoscreen/DataReductionPromoScreen;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 75
    invoke-virtual {v0}, Lcom/google/android/apps/chrome/promoscreen/DataReductionPromoScreen;->show()V

    goto :goto_0
.end method


# virtual methods
.method public dismiss()V
    .locals 2

    .prologue
    const/4 v1, 0x4

    .line 182
    iget v0, p0, Lcom/google/android/apps/chrome/promoscreen/DataReductionPromoScreen;->mState:I

    if-ge v0, v1, :cond_0

    .line 183
    iget v0, p0, Lcom/google/android/apps/chrome/promoscreen/DataReductionPromoScreen;->mState:I

    invoke-static {v0}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->dataReductionProxyPromoAction(I)V

    .line 184
    iput v1, p0, Lcom/google/android/apps/chrome/promoscreen/DataReductionPromoScreen;->mState:I

    .line 186
    :cond_0
    invoke-super {p0}, Landroid/app/Dialog;->dismiss()V

    .line 187
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 140
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    .line 142
    sget v1, Lcom/google/android/apps/chrome/R$id;->no_thanks_button:I

    if-ne v0, v1, :cond_1

    .line 143
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/promoscreen/DataReductionPromoScreen;->dismiss()V

    .line 152
    :cond_0
    :goto_0
    return-void

    .line 144
    :cond_1
    sget v1, Lcom/google/android/apps/chrome/R$id;->enable_button_front:I

    if-ne v0, v1, :cond_2

    .line 145
    const/4 v0, 0x2

    iput v0, p0, Lcom/google/android/apps/chrome/promoscreen/DataReductionPromoScreen;->mState:I

    .line 146
    invoke-direct {p0}, Lcom/google/android/apps/chrome/promoscreen/DataReductionPromoScreen;->handleEnableButtonPressed()V

    goto :goto_0

    .line 147
    :cond_2
    sget v1, Lcom/google/android/apps/chrome/R$id;->close_button_front:I

    if-ne v0, v1, :cond_3

    .line 148
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/promoscreen/DataReductionPromoScreen;->dismiss()V

    goto :goto_0

    .line 150
    :cond_3
    sget-boolean v0, Lcom/google/android/apps/chrome/promoscreen/DataReductionPromoScreen;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    const-string/jumbo v1, "Unhandled onClick event"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    const/4 v1, -0x1

    .line 91
    invoke-super {p0, p1}, Landroid/app/Dialog;->onCreate(Landroid/os/Bundle;)V

    .line 94
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/promoscreen/DataReductionPromoScreen;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v1, v1}, Landroid/view/Window;->setLayout(II)V

    .line 97
    sget v0, Lcom/google/android/apps/chrome/R$id;->close_button_front:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/promoscreen/DataReductionPromoScreen;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 98
    invoke-virtual {v1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 99
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/promoscreen/DataReductionPromoScreen;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {p0, v2, v0, v1}, Lcom/google/android/apps/chrome/promoscreen/DataReductionPromoScreen;->getTouchDelegateRunnable(Landroid/content/Context;Landroid/view/View;Landroid/view/View;)Ljava/lang/Runnable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    .line 101
    invoke-direct {p0}, Lcom/google/android/apps/chrome/promoscreen/DataReductionPromoScreen;->addListenerOnButton()V

    .line 103
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/chrome/promoscreen/DataReductionPromoScreen;->mState:I

    .line 104
    invoke-static {}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->dataReductionProxyPromoDisplayed()V

    .line 105
    return-void
.end method

.method public onDismiss(Landroid/content/DialogInterface;)V
    .locals 2

    .prologue
    .line 156
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/promoscreen/DataReductionPromoScreen;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->setDisplayedDataReductionPromo(Z)V

    .line 157
    return-void
.end method
