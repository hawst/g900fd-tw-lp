.class public Lcom/google/android/apps/chrome/sync/ui/PassphraseTypeDialogFragment;
.super Lcom/google/android/apps/chrome/preferences/ChromeBaseDialogFragment;
.source "PassphraseTypeDialogFragment.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;
.implements Landroid/widget/AdapterView$OnItemClickListener;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/google/android/apps/chrome/preferences/ChromeBaseDialogFragment;-><init>()V

    .line 108
    return-void
.end method

.method static synthetic access$600(Lcom/google/android/apps/chrome/sync/ui/PassphraseTypeDialogFragment;)Z
    .locals 1

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/google/android/apps/chrome/sync/ui/PassphraseTypeDialogFragment;->getEncryptEverythingAllowedFromArguments()Z

    move-result v0

    return v0
.end method

.method static create(Lorg/chromium/sync/internal_api/pub/SyncDecryptionPassphraseType;JZ)Lcom/google/android/apps/chrome/sync/ui/PassphraseTypeDialogFragment;
    .locals 3

    .prologue
    .line 174
    new-instance v0, Lcom/google/android/apps/chrome/sync/ui/PassphraseTypeDialogFragment;

    invoke-direct {v0}, Lcom/google/android/apps/chrome/sync/ui/PassphraseTypeDialogFragment;-><init>()V

    .line 175
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 176
    const-string/jumbo v2, "arg_current_type"

    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 177
    const-string/jumbo v2, "arg_passphrase_time"

    invoke-virtual {v1, v2, p1, p2}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 178
    const-string/jumbo v2, "arg_encrypt_everything_allowed"

    invoke-virtual {v1, v2, p3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 179
    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/sync/ui/PassphraseTypeDialogFragment;->setArguments(Landroid/os/Bundle;)V

    .line 180
    return-object v0
.end method

.method private createAdapter(Lorg/chromium/sync/internal_api/pub/SyncDecryptionPassphraseType;)Lcom/google/android/apps/chrome/sync/ui/PassphraseTypeDialogFragment$Adapter;
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 101
    new-instance v1, Lcom/google/android/apps/chrome/sync/ui/PassphraseTypeDialogFragment$PassphraseTypeUiElementContainer;

    invoke-direct {v1, v5}, Lcom/google/android/apps/chrome/sync/ui/PassphraseTypeDialogFragment$PassphraseTypeUiElementContainer;-><init>(Lcom/google/android/apps/chrome/sync/ui/PassphraseTypeDialogFragment$1;)V

    .line 102
    invoke-virtual {p1}, Lorg/chromium/sync/internal_api/pub/SyncDecryptionPassphraseType;->getVisibleTypes()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/chromium/sync/internal_api/pub/SyncDecryptionPassphraseType;

    .line 103
    new-instance v3, Lcom/google/android/apps/chrome/sync/ui/PassphraseTypeDialogFragment$PassphraseTypeUiElement;

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/sync/ui/PassphraseTypeDialogFragment;->textForPassphraseType(Lorg/chromium/sync/internal_api/pub/SyncDecryptionPassphraseType;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v0, v4, v5}, Lcom/google/android/apps/chrome/sync/ui/PassphraseTypeDialogFragment$PassphraseTypeUiElement;-><init>(Lorg/chromium/sync/internal_api/pub/SyncDecryptionPassphraseType;Ljava/lang/String;Lcom/google/android/apps/chrome/sync/ui/PassphraseTypeDialogFragment$1;)V

    invoke-virtual {v1, v3}, Lcom/google/android/apps/chrome/sync/ui/PassphraseTypeDialogFragment$PassphraseTypeUiElementContainer;->add(Lcom/google/android/apps/chrome/sync/ui/PassphraseTypeDialogFragment$PassphraseTypeUiElement;)V

    goto :goto_0

    .line 105
    :cond_0
    new-instance v0, Lcom/google/android/apps/chrome/sync/ui/PassphraseTypeDialogFragment$Adapter;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/sync/ui/PassphraseTypeDialogFragment$PassphraseTypeUiElementContainer;->getDisplayNames()[Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, p0, v1, v2, v5}, Lcom/google/android/apps/chrome/sync/ui/PassphraseTypeDialogFragment$Adapter;-><init>(Lcom/google/android/apps/chrome/sync/ui/PassphraseTypeDialogFragment;Lcom/google/android/apps/chrome/sync/ui/PassphraseTypeDialogFragment$PassphraseTypeUiElementContainer;[Ljava/lang/String;Lcom/google/android/apps/chrome/sync/ui/PassphraseTypeDialogFragment$1;)V

    return-object v0
.end method

.method private getEncryptEverythingAllowedFromArguments()Z
    .locals 2

    .prologue
    .line 246
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/sync/ui/PassphraseTypeDialogFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string/jumbo v1, "arg_encrypt_everything_allowed"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method private getPassphraseDateStringFromArguments()Ljava/lang/String;
    .locals 4

    .prologue
    .line 240
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/sync/ui/PassphraseTypeDialogFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string/jumbo v1, "arg_passphrase_time"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    .line 241
    const/4 v2, 0x2

    invoke-static {v2}, Ljava/text/DateFormat;->getDateInstance(I)Ljava/text/DateFormat;

    move-result-object v2

    .line 242
    new-instance v3, Ljava/util/Date;

    invoke-direct {v3, v0, v1}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v2, v3}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private textForPassphraseType(Lorg/chromium/sync/internal_api/pub/SyncDecryptionPassphraseType;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 82
    sget-object v0, Lcom/google/android/apps/chrome/sync/ui/PassphraseTypeDialogFragment$1;->$SwitchMap$org$chromium$sync$internal_api$pub$SyncDecryptionPassphraseType:[I

    invoke-virtual {p1}, Lorg/chromium/sync/internal_api/pub/SyncDecryptionPassphraseType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 96
    const-string/jumbo v0, ""

    :goto_0
    return-object v0

    .line 84
    :pswitch_0
    sget v0, Lcom/google/android/apps/chrome/R$string;->sync_passphrase_type_none:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/sync/ui/PassphraseTypeDialogFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 87
    :pswitch_1
    sget v0, Lcom/google/android/apps/chrome/R$string;->sync_passphrase_type_keystore:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/sync/ui/PassphraseTypeDialogFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 89
    :pswitch_2
    invoke-direct {p0}, Lcom/google/android/apps/chrome/sync/ui/PassphraseTypeDialogFragment;->getPassphraseDateStringFromArguments()Ljava/lang/String;

    move-result-object v0

    .line 90
    sget v1, Lcom/google/android/apps/chrome/R$string;->sync_passphrase_type_frozen:I

    invoke-virtual {p0, v1}, Lcom/google/android/apps/chrome/sync/ui/PassphraseTypeDialogFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 91
    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 93
    :pswitch_3
    sget v0, Lcom/google/android/apps/chrome/R$string;->sync_passphrase_type_custom:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/sync/ui/PassphraseTypeDialogFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 82
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method


# virtual methods
.method public getCurrentTypeFromArguments()Lorg/chromium/sync/internal_api/pub/SyncDecryptionPassphraseType;
    .locals 2

    .prologue
    .line 230
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/sync/ui/PassphraseTypeDialogFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string/jumbo v1, "arg_current_type"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lorg/chromium/sync/internal_api/pub/SyncDecryptionPassphraseType;

    .line 232
    if-nez v0, :cond_0

    .line 233
    const-string/jumbo v0, "PassphraseTypeDialogFragment"

    const-string/jumbo v1, "Unable to find argument with current type. Setting to INVALID."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 234
    sget-object v0, Lorg/chromium/sync/internal_api/pub/SyncDecryptionPassphraseType;->INVALID:Lorg/chromium/sync/internal_api/pub/SyncDecryptionPassphraseType;

    .line 236
    :cond_0
    return-object v0
.end method

.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 1

    .prologue
    .line 206
    const/4 v0, -0x2

    if-ne p2, v0, :cond_0

    .line 207
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/sync/ui/PassphraseTypeDialogFragment;->dismiss()V

    .line 209
    :cond_0
    return-void
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 4

    .prologue
    .line 185
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/sync/ui/PassphraseTypeDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    .line 186
    sget v1, Lcom/google/android/apps/chrome/R$layout;->sync_encryption_type:I

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 189
    sget v0, Lcom/google/android/apps/chrome/R$id;->list:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    .line 190
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/sync/ui/PassphraseTypeDialogFragment;->getCurrentTypeFromArguments()Lorg/chromium/sync/internal_api/pub/SyncDecryptionPassphraseType;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/google/android/apps/chrome/sync/ui/PassphraseTypeDialogFragment;->createAdapter(Lorg/chromium/sync/internal_api/pub/SyncDecryptionPassphraseType;)Lcom/google/android/apps/chrome/sync/ui/PassphraseTypeDialogFragment$Adapter;

    move-result-object v2

    .line 191
    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 192
    invoke-virtual {v0, p0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 193
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/sync/ui/PassphraseTypeDialogFragment;->getCurrentTypeFromArguments()Lorg/chromium/sync/internal_api/pub/SyncDecryptionPassphraseType;

    move-result-object v3

    .line 194
    invoke-virtual {v2, v3}, Lcom/google/android/apps/chrome/sync/ui/PassphraseTypeDialogFragment$Adapter;->getPositionForType(Lorg/chromium/sync/internal_api/pub/SyncDecryptionPassphraseType;)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setSelection(I)V

    .line 197
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/sync/ui/PassphraseTypeDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-direct {v0, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    sget v2, Lcom/google/android/apps/chrome/R$string;->cancel:I

    invoke-virtual {v0, v2, p0}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    sget v2, Lcom/google/android/apps/chrome/R$string;->sync_passphrase_type_title:I

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 4

    .prologue
    .line 213
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/sync/ui/PassphraseTypeDialogFragment;->getCurrentTypeFromArguments()Lorg/chromium/sync/internal_api/pub/SyncDecryptionPassphraseType;

    move-result-object v0

    .line 216
    long-to-int v1, p4

    invoke-static {v1}, Lorg/chromium/sync/internal_api/pub/SyncDecryptionPassphraseType;->fromInternalValue(I)Lorg/chromium/sync/internal_api/pub/SyncDecryptionPassphraseType;

    move-result-object v1

    .line 218
    invoke-direct {p0}, Lcom/google/android/apps/chrome/sync/ui/PassphraseTypeDialogFragment;->getEncryptEverythingAllowedFromArguments()Z

    move-result v2

    .line 219
    invoke-virtual {v0, v2}, Lorg/chromium/sync/internal_api/pub/SyncDecryptionPassphraseType;->getAllowedTypes(Z)Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 220
    invoke-virtual {v0}, Lorg/chromium/sync/internal_api/pub/SyncDecryptionPassphraseType;->internalValue()I

    move-result v0

    int-to-long v2, v0

    cmp-long v0, p4, v2

    if-eqz v0, :cond_0

    .line 221
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/sync/ui/PassphraseTypeDialogFragment;->getTargetFragment()Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/sync/ui/PassphraseTypeDialogFragment$Listener;

    .line 222
    invoke-interface {v0, v1}, Lcom/google/android/apps/chrome/sync/ui/PassphraseTypeDialogFragment$Listener;->onPassphraseTypeSelected(Lorg/chromium/sync/internal_api/pub/SyncDecryptionPassphraseType;)V

    .line 224
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/sync/ui/PassphraseTypeDialogFragment;->dismiss()V

    .line 226
    :cond_1
    return-void
.end method
