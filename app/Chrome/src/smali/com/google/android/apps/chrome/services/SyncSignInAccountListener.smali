.class public Lcom/google/android/apps/chrome/services/SyncSignInAccountListener;
.super Ljava/lang/Object;
.source "SyncSignInAccountListener.java"

# interfaces
.implements Lcom/google/android/apps/chrome/services/ui/AddGoogleAccountDialogFragment$Listener;
.implements Lcom/google/android/apps/chrome/sync/ui/SyncChooseAccountFragment$Listener;


# static fields
.field private static sAccountAdder:Lcom/google/android/apps/chrome/services/AccountAdder;

.field private static sSyncSignInListener:Lcom/google/android/apps/chrome/services/SyncSignInAccountListener;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 31
    new-instance v0, Lcom/google/android/apps/chrome/services/AccountAdder;

    invoke-direct {v0}, Lcom/google/android/apps/chrome/services/AccountAdder;-><init>()V

    sput-object v0, Lcom/google/android/apps/chrome/services/SyncSignInAccountListener;->sAccountAdder:Lcom/google/android/apps/chrome/services/AccountAdder;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/chrome/services/SyncSignInAccountListener;Lcom/google/android/apps/chrome/preferences/Preferences;Landroid/accounts/Account;)V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/chrome/services/SyncSignInAccountListener;->doFinishSignIn(Lcom/google/android/apps/chrome/preferences/Preferences;Landroid/accounts/Account;)V

    return-void
.end method

.method private doFinishSignIn(Lcom/google/android/apps/chrome/preferences/Preferences;Landroid/accounts/Account;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 68
    invoke-static {}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->getInstance()Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->update()V

    .line 73
    invoke-static {}, Lcom/google/android/apps/chrome/services/GoogleServicesStates;->create()Lcom/google/android/apps/chrome/services/GoogleServicesStates$GoogleServicesStatesBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/services/GoogleServicesStates$GoogleServicesStatesBuilder;->autoLogin(Z)Lcom/google/android/apps/chrome/services/GoogleServicesStates$GoogleServicesStatesBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/services/GoogleServicesStates$GoogleServicesStatesBuilder;->sync(Z)Lcom/google/android/apps/chrome/services/GoogleServicesStates$GoogleServicesStatesBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/services/GoogleServicesStates$GoogleServicesStatesBuilder;->chromeToMobile(Z)Lcom/google/android/apps/chrome/services/GoogleServicesStates$GoogleServicesStatesBuilder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/services/GoogleServicesStates$GoogleServicesStatesBuilder;->build()Lcom/google/android/apps/chrome/services/GoogleServicesStates;

    move-result-object v0

    .line 78
    invoke-static {p1}, Lcom/google/android/apps/chrome/services/GoogleServicesManager;->get(Landroid/content/Context;)Lcom/google/android/apps/chrome/services/GoogleServicesManager;

    move-result-object v1

    invoke-virtual {v1, v0, p2}, Lcom/google/android/apps/chrome/services/GoogleServicesManager;->setStates(Lcom/google/android/apps/chrome/services/GoogleServicesStates;Landroid/accounts/Account;)V

    .line 80
    invoke-static {p1}, Lorg/chromium/chrome/browser/signin/SigninManager;->get(Landroid/content/Context;)Lorg/chromium/chrome/browser/signin/SigninManager;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/signin/SigninManager;->logInSignedInUser()V

    .line 81
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v1, Lcom/google/android/apps/chrome/services/SyncSignInAccountListener$2;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/chrome/services/SyncSignInAccountListener$2;-><init>(Lcom/google/android/apps/chrome/services/SyncSignInAccountListener;Lcom/google/android/apps/chrome/preferences/Preferences;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 98
    return-void
.end method

.method public static get()Lcom/google/android/apps/chrome/services/SyncSignInAccountListener;
    .locals 1

    .prologue
    .line 34
    sget-object v0, Lcom/google/android/apps/chrome/services/SyncSignInAccountListener;->sSyncSignInListener:Lcom/google/android/apps/chrome/services/SyncSignInAccountListener;

    if-nez v0, :cond_0

    .line 35
    new-instance v0, Lcom/google/android/apps/chrome/services/SyncSignInAccountListener;

    invoke-direct {v0}, Lcom/google/android/apps/chrome/services/SyncSignInAccountListener;-><init>()V

    sput-object v0, Lcom/google/android/apps/chrome/services/SyncSignInAccountListener;->sSyncSignInListener:Lcom/google/android/apps/chrome/services/SyncSignInAccountListener;

    .line 37
    :cond_0
    sget-object v0, Lcom/google/android/apps/chrome/services/SyncSignInAccountListener;->sSyncSignInListener:Lcom/google/android/apps/chrome/services/SyncSignInAccountListener;

    return-object v0
.end method

.method public static overrideAccountAdderForTests(Lcom/google/android/apps/chrome/services/AccountAdder;)V
    .locals 0

    .prologue
    .line 102
    sput-object p0, Lcom/google/android/apps/chrome/services/SyncSignInAccountListener;->sAccountAdder:Lcom/google/android/apps/chrome/services/AccountAdder;

    .line 103
    return-void
.end method


# virtual methods
.method public onAccountSelected(Landroid/app/Activity;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 45
    invoke-static {p2}, Lorg/chromium/sync/signin/AccountManagerHelper;->createAccountFromName(Ljava/lang/String;)Landroid/accounts/Account;

    move-result-object v0

    .line 49
    invoke-static {p1}, Lorg/chromium/chrome/browser/signin/SigninManager;->get(Landroid/content/Context;)Lorg/chromium/chrome/browser/signin/SigninManager;

    move-result-object v1

    .line 50
    const/4 v2, 0x0

    new-instance v3, Lcom/google/android/apps/chrome/services/SyncSignInAccountListener$1;

    invoke-direct {v3, p0, p1, v0}, Lcom/google/android/apps/chrome/services/SyncSignInAccountListener$1;-><init>(Lcom/google/android/apps/chrome/services/SyncSignInAccountListener;Landroid/app/Activity;Landroid/accounts/Account;)V

    invoke-virtual {v1, p1, v0, v2, v3}, Lorg/chromium/chrome/browser/signin/SigninManager;->startSignIn(Landroid/app/Activity;Landroid/accounts/Account;ZLorg/chromium/chrome/browser/signin/SigninManager$SignInFlowObserver;)V

    .line 63
    return-void
.end method

.method public onAddAccount(Landroid/app/Activity;)V
    .locals 2

    .prologue
    .line 110
    sget-object v0, Lcom/google/android/apps/chrome/services/SyncSignInAccountListener;->sAccountAdder:Lcom/google/android/apps/chrome/services/AccountAdder;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, Lcom/google/android/apps/chrome/services/AccountAdder;->addAccount(Landroid/app/Activity;I)V

    .line 111
    return-void
.end method
