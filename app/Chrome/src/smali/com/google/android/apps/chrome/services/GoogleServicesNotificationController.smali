.class public Lcom/google/android/apps/chrome/services/GoogleServicesNotificationController;
.super Ljava/lang/Object;
.source "GoogleServicesNotificationController.java"

# interfaces
.implements Lorg/chromium/chrome/browser/sync/ProfileSyncService$SyncStateChangedListener;
.implements Lorg/chromium/sync/signin/ChromeSigninController$Listener;


# static fields
.field static final synthetic $assertionsDisabled:Z

.field private static final LOCK:Ljava/lang/Object;

.field public static final SIGNED_IN_TO_CHROME_ID:I = 0x3

.field private static sInstance:Lcom/google/android/apps/chrome/services/GoogleServicesNotificationController;


# instance fields
.field private final mApplicationContext:Landroid/content/Context;

.field private mNotificationManager:Lcom/google/android/apps/chrome/services/NotificationManagerWrapper;

.field private final mProfileSyncService:Lorg/chromium/chrome/browser/sync/ProfileSyncService;

.field private final mSyncStatusHelper:Lorg/chromium/sync/notifier/SyncStatusHelper;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 36
    const-class v0, Lcom/google/android/apps/chrome/services/GoogleServicesNotificationController;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/services/GoogleServicesNotificationController;->$assertionsDisabled:Z

    .line 63
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/google/android/apps/chrome/services/GoogleServicesNotificationController;->LOCK:Ljava/lang/Object;

    return-void

    .line 36
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private constructor <init>(Landroid/content/Context;Lorg/chromium/chrome/browser/sync/ProfileSyncService;Lorg/chromium/sync/notifier/SyncStatusHelper;)V
    .locals 3

    .prologue
    .line 66
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 67
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/services/GoogleServicesNotificationController;->mApplicationContext:Landroid/content/Context;

    .line 68
    iput-object p2, p0, Lcom/google/android/apps/chrome/services/GoogleServicesNotificationController;->mProfileSyncService:Lorg/chromium/chrome/browser/sync/ProfileSyncService;

    .line 69
    iput-object p3, p0, Lcom/google/android/apps/chrome/services/GoogleServicesNotificationController;->mSyncStatusHelper:Lorg/chromium/sync/notifier/SyncStatusHelper;

    .line 70
    new-instance v1, Lcom/google/android/apps/chrome/services/DefaultNotificationManagerWrapper;

    iget-object v0, p0, Lcom/google/android/apps/chrome/services/GoogleServicesNotificationController;->mApplicationContext:Landroid/content/Context;

    const-string/jumbo v2, "notification"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    invoke-direct {v1, v0}, Lcom/google/android/apps/chrome/services/DefaultNotificationManagerWrapper;-><init>(Landroid/app/NotificationManager;)V

    iput-object v1, p0, Lcom/google/android/apps/chrome/services/GoogleServicesNotificationController;->mNotificationManager:Lcom/google/android/apps/chrome/services/NotificationManagerWrapper;

    .line 74
    return-void
.end method

.method private cancelNotification(I)V
    .locals 1

    .prologue
    .line 239
    iget-object v0, p0, Lcom/google/android/apps/chrome/services/GoogleServicesNotificationController;->mNotificationManager:Lcom/google/android/apps/chrome/services/NotificationManagerWrapper;

    invoke-interface {v0, p1}, Lcom/google/android/apps/chrome/services/NotificationManagerWrapper;->cancel(I)V

    .line 240
    return-void
.end method

.method public static createNewInstance(Landroid/content/Context;Lorg/chromium/chrome/browser/sync/ProfileSyncService;Lorg/chromium/sync/notifier/SyncStatusHelper;)Lcom/google/android/apps/chrome/services/GoogleServicesNotificationController;
    .locals 4

    .prologue
    .line 78
    sget-object v1, Lcom/google/android/apps/chrome/services/GoogleServicesNotificationController;->LOCK:Ljava/lang/Object;

    monitor-enter v1

    .line 79
    :try_start_0
    sget-object v0, Lcom/google/android/apps/chrome/services/GoogleServicesNotificationController;->sInstance:Lcom/google/android/apps/chrome/services/GoogleServicesNotificationController;

    if-nez v0, :cond_0

    .line 80
    new-instance v0, Lcom/google/android/apps/chrome/services/GoogleServicesNotificationController;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/android/apps/chrome/services/GoogleServicesNotificationController;-><init>(Landroid/content/Context;Lorg/chromium/chrome/browser/sync/ProfileSyncService;Lorg/chromium/sync/notifier/SyncStatusHelper;)V

    sput-object v0, Lcom/google/android/apps/chrome/services/GoogleServicesNotificationController;->sInstance:Lcom/google/android/apps/chrome/services/GoogleServicesNotificationController;

    .line 86
    :goto_0
    sget-object v0, Lcom/google/android/apps/chrome/services/GoogleServicesNotificationController;->sInstance:Lcom/google/android/apps/chrome/services/GoogleServicesNotificationController;

    monitor-exit v1

    return-object v0

    .line 83
    :cond_0
    const-string/jumbo v0, "GoogleServicesNotificationController"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "GoogleServicesNotificationController already created. Currently on thread: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 87
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private createPasswordIntent()Landroid/content/Intent;
    .locals 4

    .prologue
    .line 228
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "android.intent.action.MAIN"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 229
    new-instance v1, Landroid/content/ComponentName;

    iget-object v2, p0, Lcom/google/android/apps/chrome/services/GoogleServicesNotificationController;->mApplicationContext:Landroid/content/Context;

    const-class v3, Lcom/google/android/apps/chrome/sync/ui/PassphraseActivity;

    invoke-direct {v1, v2, v3}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 230
    const-string/jumbo v1, "android.intent.category.LAUNCHER"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 232
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 234
    const/high16 v1, 0x4000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 235
    return-object v0
.end method

.method private createSettingsIntent()Landroid/content/Intent;
    .locals 4

    .prologue
    .line 209
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "android.intent.action.MAIN"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 210
    new-instance v1, Landroid/content/ComponentName;

    iget-object v2, p0, Lcom/google/android/apps/chrome/services/GoogleServicesNotificationController;->mApplicationContext:Landroid/content/Context;

    const-class v3, Lcom/google/android/apps/chrome/preferences/Preferences;

    invoke-direct {v1, v2, v3}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 211
    const-string/jumbo v1, ":android:show_fragment"

    const-class v2, Lcom/google/android/apps/chrome/services/ui/GoogleServicesFragment;

    invoke-virtual {v2}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 213
    const-string/jumbo v1, "display_home_as_up"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 214
    const-string/jumbo v1, "android.intent.category.LAUNCHER"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 216
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 218
    const/high16 v1, 0x4000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 219
    return-object v0
.end method

.method private formatMessageParts(Ljava/lang/Integer;Ljava/lang/Integer;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 124
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/google/android/apps/chrome/services/GoogleServicesNotificationController;->mApplicationContext:Landroid/content/Context;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ": "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/services/GoogleServicesNotificationController;->mApplicationContext:Landroid/content/Context;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getInstance()Lcom/google/android/apps/chrome/services/GoogleServicesNotificationController;
    .locals 2

    .prologue
    .line 91
    sget-object v1, Lcom/google/android/apps/chrome/services/GoogleServicesNotificationController;->LOCK:Ljava/lang/Object;

    monitor-enter v1

    .line 92
    :try_start_0
    sget-object v0, Lcom/google/android/apps/chrome/services/GoogleServicesNotificationController;->sInstance:Lcom/google/android/apps/chrome/services/GoogleServicesNotificationController;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    .line 93
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private shouldSyncAuthErrorBeShown()Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 164
    sget-object v1, Lcom/google/android/apps/chrome/services/GoogleServicesNotificationController$1;->$SwitchMap$org$chromium$chrome$browser$sync$GoogleServiceAuthError$State:[I

    iget-object v2, p0, Lcom/google/android/apps/chrome/services/GoogleServicesNotificationController;->mProfileSyncService:Lorg/chromium/chrome/browser/sync/ProfileSyncService;

    invoke-virtual {v2}, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->getAuthError()Lorg/chromium/chrome/browser/sync/GoogleServiceAuthError$State;

    move-result-object v2

    invoke-virtual {v2}, Lorg/chromium/chrome/browser/sync/GoogleServiceAuthError$State;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 179
    const-string/jumbo v1, "GoogleServicesNotificationController"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "Not showing unknown Auth Error: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/google/android/apps/chrome/services/GoogleServicesNotificationController;->mProfileSyncService:Lorg/chromium/chrome/browser/sync/ProfileSyncService;

    invoke-virtual {v3}, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->getAuthError()Lorg/chromium/chrome/browser/sync/GoogleServiceAuthError$State;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 180
    :goto_0
    :pswitch_0
    return v0

    .line 177
    :pswitch_1
    const/4 v0, 0x1

    goto :goto_0

    .line 164
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method private showNotification(Ljava/lang/String;Ljava/lang/String;Landroid/content/Intent;I)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v2, 0x0

    .line 186
    iget-object v0, p0, Lcom/google/android/apps/chrome/services/GoogleServicesNotificationController;->mApplicationContext:Landroid/content/Context;

    sget v1, Lcom/google/android/apps/chrome/R$string;->app_name:I

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 187
    iget-object v1, p0, Lcom/google/android/apps/chrome/services/GoogleServicesNotificationController;->mApplicationContext:Landroid/content/Context;

    invoke-static {v1, v2, p3, v2}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    .line 188
    new-instance v2, Landroid/support/v4/app/L;

    iget-object v3, p0, Lcom/google/android/apps/chrome/services/GoogleServicesNotificationController;->mApplicationContext:Landroid/content/Context;

    invoke-direct {v2, v3}, Landroid/support/v4/app/L;-><init>(Landroid/content/Context;)V

    invoke-virtual {v2, v4}, Landroid/support/v4/app/L;->b(Z)Landroid/support/v4/app/L;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/support/v4/app/L;->a(Landroid/app/PendingIntent;)Landroid/support/v4/app/L;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/support/v4/app/L;->a(Ljava/lang/CharSequence;)Landroid/support/v4/app/L;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/support/v4/app/L;->b(Ljava/lang/CharSequence;)Landroid/support/v4/app/L;

    move-result-object v0

    sget v1, Lcom/google/android/apps/chrome/R$drawable;->ic_stat_notify:I

    invoke-virtual {v0, v1}, Landroid/support/v4/app/L;->a(I)Landroid/support/v4/app/L;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/support/v4/app/L;->d(Ljava/lang/CharSequence;)Landroid/support/v4/app/L;

    move-result-object v0

    .line 195
    invoke-static {v0, v4}, Lcom/google/android/apps/chrome/utilities/UnreleasedApiCompatibilityUtils;->setLocalOnly(Landroid/support/v4/app/L;Z)Landroid/support/v4/app/L;

    move-result-object v0

    .line 197
    new-instance v1, Landroid/support/v4/app/K;

    invoke-direct {v1, v0}, Landroid/support/v4/app/K;-><init>(Landroid/support/v4/app/L;)V

    invoke-virtual {v1, p2}, Landroid/support/v4/app/K;->a(Ljava/lang/CharSequence;)Landroid/support/v4/app/K;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/K;->a()Landroid/app/Notification;

    move-result-object v0

    .line 199
    iget-object v1, p0, Lcom/google/android/apps/chrome/services/GoogleServicesNotificationController;->mNotificationManager:Lcom/google/android/apps/chrome/services/NotificationManagerWrapper;

    invoke-interface {v1, p4, v0}, Lcom/google/android/apps/chrome/services/NotificationManagerWrapper;->notify(ILandroid/app/Notification;)V

    .line 200
    return-void
.end method

.method private updateNotification()V
    .locals 2

    .prologue
    .line 111
    sget-boolean v0, Lcom/google/android/apps/chrome/services/GoogleServicesNotificationController;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v1

    if-eq v0, v1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 112
    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/chrome/services/GoogleServicesNotificationController;->updateSyncStatusNotification()V

    .line 113
    return-void
.end method

.method private updateSingleNotification(ILjava/lang/String;Landroid/content/Intent;)V
    .locals 0

    .prologue
    .line 116
    if-nez p2, :cond_0

    .line 117
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/services/GoogleServicesNotificationController;->cancelNotification(I)V

    .line 121
    :goto_0
    return-void

    .line 119
    :cond_0
    invoke-direct {p0, p2, p2, p3, p1}, Lcom/google/android/apps/chrome/services/GoogleServicesNotificationController;->showNotification(Ljava/lang/String;Ljava/lang/String;Landroid/content/Intent;I)V

    goto :goto_0
.end method

.method private updateSyncStatusNotification()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 134
    iget-object v0, p0, Lcom/google/android/apps/chrome/services/GoogleServicesNotificationController;->mSyncStatusHelper:Lorg/chromium/sync/notifier/SyncStatusHelper;

    invoke-virtual {v0}, Lorg/chromium/sync/notifier/SyncStatusHelper;->isSyncEnabled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 135
    invoke-direct {p0, v3}, Lcom/google/android/apps/chrome/services/GoogleServicesNotificationController;->cancelNotification(I)V

    .line 161
    :goto_0
    return-void

    .line 137
    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/chrome/services/GoogleServicesNotificationController;->shouldSyncAuthErrorBeShown()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 138
    iget-object v0, p0, Lcom/google/android/apps/chrome/services/GoogleServicesNotificationController;->mProfileSyncService:Lorg/chromium/chrome/browser/sync/ProfileSyncService;

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->getAuthError()Lorg/chromium/chrome/browser/sync/GoogleServiceAuthError$State;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/sync/GoogleServiceAuthError$State;->getMessage()I

    move-result v1

    .line 139
    invoke-direct {p0}, Lcom/google/android/apps/chrome/services/GoogleServicesNotificationController;->createSettingsIntent()Landroid/content/Intent;

    move-result-object v0

    .line 159
    :goto_1
    sget v2, Lcom/google/android/apps/chrome/R$string;->sign_in_sync:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-direct {p0, v2, v1}, Lcom/google/android/apps/chrome/services/GoogleServicesNotificationController;->formatMessageParts(Ljava/lang/Integer;Ljava/lang/Integer;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v3, v1, v0}, Lcom/google/android/apps/chrome/services/GoogleServicesNotificationController;->updateSingleNotification(ILjava/lang/String;Landroid/content/Intent;)V

    goto :goto_0

    .line 141
    :cond_1
    sget-object v0, Lcom/google/android/apps/chrome/services/GoogleServicesNotificationController$1;->$SwitchMap$org$chromium$sync$internal_api$pub$SyncDecryptionPassphraseType:[I

    iget-object v1, p0, Lcom/google/android/apps/chrome/services/GoogleServicesNotificationController;->mProfileSyncService:Lorg/chromium/chrome/browser/sync/ProfileSyncService;

    invoke-virtual {v1}, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->getSyncDecryptionPassphraseTypeIfRequired()Lorg/chromium/sync/internal_api/pub/SyncDecryptionPassphraseType;

    move-result-object v1

    invoke-virtual {v1}, Lorg/chromium/sync/internal_api/pub/SyncDecryptionPassphraseType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 154
    invoke-direct {p0, v3}, Lcom/google/android/apps/chrome/services/GoogleServicesNotificationController;->cancelNotification(I)V

    goto :goto_0

    .line 143
    :pswitch_0
    sget v1, Lcom/google/android/apps/chrome/R$string;->sync_need_password:I

    .line 144
    invoke-direct {p0}, Lcom/google/android/apps/chrome/services/GoogleServicesNotificationController;->createPasswordIntent()Landroid/content/Intent;

    move-result-object v0

    goto :goto_1

    .line 148
    :pswitch_1
    sget v1, Lcom/google/android/apps/chrome/R$string;->sync_need_passphrase:I

    .line 149
    invoke-direct {p0}, Lcom/google/android/apps/chrome/services/GoogleServicesNotificationController;->createPasswordIntent()Landroid/content/Intent;

    move-result-object v0

    goto :goto_1

    .line 141
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public displayAndroidMasterSyncDisabledNotification()V
    .locals 3

    .prologue
    .line 280
    sget v0, Lcom/google/android/apps/chrome/R$string;->sign_in_sync:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sget v1, Lcom/google/android/apps/chrome/R$string;->sync_android_master_sync_disabled:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/chrome/services/GoogleServicesNotificationController;->formatMessageParts(Ljava/lang/Integer;Ljava/lang/Integer;)Ljava/lang/String;

    move-result-object v0

    .line 282
    new-instance v1, Landroid/content/Intent;

    const-string/jumbo v2, "android.settings.SYNC_SETTINGS"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v2

    invoke-direct {p0, v0, v0, v1, v2}, Lcom/google/android/apps/chrome/services/GoogleServicesNotificationController;->showNotification(Ljava/lang/String;Ljava/lang/String;Landroid/content/Intent;I)V

    .line 284
    return-void
.end method

.method public onClearSignedInUser()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 291
    const/4 v0, 0x3

    invoke-direct {p0, v0, v1, v1}, Lcom/google/android/apps/chrome/services/GoogleServicesNotificationController;->updateSingleNotification(ILjava/lang/String;Landroid/content/Intent;)V

    .line 292
    return-void
.end method

.method public overrideNotificationManagerForTests(Lcom/google/android/apps/chrome/services/NotificationManagerWrapper;)V
    .locals 0

    .prologue
    .line 99
    iput-object p1, p0, Lcom/google/android/apps/chrome/services/GoogleServicesNotificationController;->mNotificationManager:Lcom/google/android/apps/chrome/services/NotificationManagerWrapper;

    .line 100
    return-void
.end method

.method public showOneOffNotification(ILjava/lang/String;)V
    .locals 1

    .prologue
    .line 250
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    invoke-virtual {p0, p1, p2, p2, v0}, Lcom/google/android/apps/chrome/services/GoogleServicesNotificationController;->showOneOffNotification(ILjava/lang/String;Ljava/lang/String;Landroid/content/Intent;)V

    .line 251
    return-void
.end method

.method public showOneOffNotification(ILjava/lang/String;Ljava/lang/String;Landroid/content/Intent;)V
    .locals 3

    .prologue
    .line 265
    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    .line 266
    const-string/jumbo v0, "GoogleServicesNotificationController"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "Unique ID "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " might override current notifications."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 268
    :cond_0
    invoke-direct {p0, p2, p3, p4, p1}, Lcom/google/android/apps/chrome/services/GoogleServicesNotificationController;->showNotification(Ljava/lang/String;Ljava/lang/String;Landroid/content/Intent;I)V

    .line 269
    return-void
.end method

.method public syncStateChanged()V
    .locals 0

    .prologue
    .line 276
    invoke-direct {p0}, Lcom/google/android/apps/chrome/services/GoogleServicesNotificationController;->updateNotification()V

    .line 277
    return-void
.end method
