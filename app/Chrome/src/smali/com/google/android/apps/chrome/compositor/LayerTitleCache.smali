.class public Lcom/google/android/apps/chrome/compositor/LayerTitleCache;
.super Ljava/lang/Object;
.source "LayerTitleCache.java"

# interfaces
.implements Lcom/google/android/apps/chrome/tabs/TitleCache;


# instance fields
.field private mContext:Landroid/content/Context;

.field private mNativeLayerTitleCache:J


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 4

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    iput-object p1, p0, Lcom/google/android/apps/chrome/compositor/LayerTitleCache;->mContext:Landroid/content/Context;

    .line 35
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/LayerTitleCache;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 36
    sget v1, Lcom/google/android/apps/chrome/R$dimen;->border_texture_title_fade:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    .line 37
    sget v2, Lcom/google/android/apps/chrome/R$dimen;->tab_title_favicon_start_padding:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 39
    sget v3, Lcom/google/android/apps/chrome/R$dimen;->tab_title_favicon_end_padding:I

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 41
    invoke-direct {p0, v1, v2, v0}, Lcom/google/android/apps/chrome/compositor/LayerTitleCache;->nativeInit(III)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/chrome/compositor/LayerTitleCache;->mNativeLayerTitleCache:J

    .line 43
    return-void
.end method

.method private getNativePtr()J
    .locals 2

    .prologue
    .line 67
    iget-wide v0, p0, Lcom/google/android/apps/chrome/compositor/LayerTitleCache;->mNativeLayerTitleCache:J

    return-wide v0
.end method

.method private native nativeClearExcept(JI)V
.end method

.method private static native nativeDestroy(J)V
.end method

.method private native nativeInit(III)J
.end method

.method private native nativeUpdateImmutableResources(JLandroid/graphics/Bitmap;Landroid/graphics/Bitmap;)V
.end method

.method private native nativeUpdateLayer(JILandroid/graphics/Bitmap;Landroid/graphics/Bitmap;ZZ)V
.end method

.method private updateImmutableResources()V
    .locals 5

    .prologue
    .line 91
    iget-wide v0, p0, Lcom/google/android/apps/chrome/compositor/LayerTitleCache;->mNativeLayerTitleCache:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 100
    :goto_0
    return-void

    .line 92
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/LayerTitleCache;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 93
    sget v1, Lcom/google/android/apps/chrome/R$drawable;->spinner:I

    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/compositor/resources/BitmapResources;->load(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 94
    sget v2, Lcom/google/android/apps/chrome/R$drawable;->spinner_white:I

    invoke-static {v0, v2}, Lcom/google/android/apps/chrome/compositor/resources/BitmapResources;->load(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 95
    new-instance v2, Lcom/google/android/apps/chrome/tabs/TitleBitmapFactory;

    iget-object v3, p0, Lcom/google/android/apps/chrome/compositor/LayerTitleCache;->mContext:Landroid/content/Context;

    const/4 v4, 0x0

    invoke-direct {v2, v3, v4}, Lcom/google/android/apps/chrome/tabs/TitleBitmapFactory;-><init>(Landroid/content/Context;Z)V

    .line 96
    iget-object v3, p0, Lcom/google/android/apps/chrome/compositor/LayerTitleCache;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v3, v1}, Lcom/google/android/apps/chrome/tabs/TitleBitmapFactory;->getFaviconBitmap(Landroid/content/Context;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 97
    iget-object v3, p0, Lcom/google/android/apps/chrome/compositor/LayerTitleCache;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v3, v0}, Lcom/google/android/apps/chrome/tabs/TitleBitmapFactory;->getFaviconBitmap(Landroid/content/Context;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 99
    iget-wide v2, p0, Lcom/google/android/apps/chrome/compositor/LayerTitleCache;->mNativeLayerTitleCache:J

    invoke-direct {p0, v2, v3, v1, v0}, Lcom/google/android/apps/chrome/compositor/LayerTitleCache;->nativeUpdateImmutableResources(JLandroid/graphics/Bitmap;Landroid/graphics/Bitmap;)V

    goto :goto_0
.end method


# virtual methods
.method public clearExcept(I)V
    .locals 4

    .prologue
    .line 86
    iget-wide v0, p0, Lcom/google/android/apps/chrome/compositor/LayerTitleCache;->mNativeLayerTitleCache:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 88
    :goto_0
    return-void

    .line 87
    :cond_0
    iget-wide v0, p0, Lcom/google/android/apps/chrome/compositor/LayerTitleCache;->mNativeLayerTitleCache:J

    invoke-direct {p0, v0, v1, p1}, Lcom/google/android/apps/chrome/compositor/LayerTitleCache;->nativeClearExcept(JI)V

    goto :goto_0
.end method

.method public put(ILandroid/graphics/Bitmap;Landroid/graphics/Bitmap;ZZ)V
    .locals 9

    .prologue
    .line 73
    iget-wide v0, p0, Lcom/google/android/apps/chrome/compositor/LayerTitleCache;->mNativeLayerTitleCache:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 76
    :goto_0
    return-void

    .line 74
    :cond_0
    iget-wide v2, p0, Lcom/google/android/apps/chrome/compositor/LayerTitleCache;->mNativeLayerTitleCache:J

    move-object v1, p0

    move v4, p1

    move-object v5, p2

    move-object v6, p3

    move v7, p4

    move v8, p5

    invoke-direct/range {v1 .. v8}, Lcom/google/android/apps/chrome/compositor/LayerTitleCache;->nativeUpdateLayer(JILandroid/graphics/Bitmap;Landroid/graphics/Bitmap;ZZ)V

    goto :goto_0
.end method

.method public remove(I)V
    .locals 9

    .prologue
    const/4 v5, 0x0

    const/4 v7, 0x0

    .line 80
    iget-wide v0, p0, Lcom/google/android/apps/chrome/compositor/LayerTitleCache;->mNativeLayerTitleCache:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 82
    :goto_0
    return-void

    .line 81
    :cond_0
    iget-wide v2, p0, Lcom/google/android/apps/chrome/compositor/LayerTitleCache;->mNativeLayerTitleCache:J

    move-object v1, p0

    move v4, p1

    move-object v6, v5

    move v8, v7

    invoke-direct/range {v1 .. v8}, Lcom/google/android/apps/chrome/compositor/LayerTitleCache;->nativeUpdateLayer(JILandroid/graphics/Bitmap;Landroid/graphics/Bitmap;ZZ)V

    goto :goto_0
.end method

.method public shutDown()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 60
    iget-wide v0, p0, Lcom/google/android/apps/chrome/compositor/LayerTitleCache;->mNativeLayerTitleCache:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 63
    :goto_0
    return-void

    .line 61
    :cond_0
    iget-wide v0, p0, Lcom/google/android/apps/chrome/compositor/LayerTitleCache;->mNativeLayerTitleCache:J

    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/compositor/LayerTitleCache;->nativeDestroy(J)V

    .line 62
    iput-wide v2, p0, Lcom/google/android/apps/chrome/compositor/LayerTitleCache;->mNativeLayerTitleCache:J

    goto :goto_0
.end method

.method public updateImmutableResources(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 50
    invoke-static {}, Lorg/chromium/base/CommandLine;->getInstance()Lorg/chromium/base/CommandLine;

    move-result-object v0

    const-string/jumbo v1, "disable-compositor-tab-strip"

    invoke-virtual {v0, v1}, Lorg/chromium/base/CommandLine;->hasSwitch(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/LayerTitleCache;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lorg/chromium/ui/base/DeviceFormFactor;->isTablet(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 52
    invoke-direct {p0}, Lcom/google/android/apps/chrome/compositor/LayerTitleCache;->updateImmutableResources()V

    .line 54
    :cond_0
    return-void
.end method
