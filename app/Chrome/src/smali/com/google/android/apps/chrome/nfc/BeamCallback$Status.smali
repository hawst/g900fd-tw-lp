.class Lcom/google/android/apps/chrome/nfc/BeamCallback$Status;
.super Ljava/lang/Object;
.source "BeamCallback.java"


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field public final errorStrID:Ljava/lang/Integer;

.field public final result:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 37
    const-class v0, Lcom/google/android/apps/chrome/nfc/BeamCallback;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/nfc/BeamCallback$Status;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>(Ljava/lang/Integer;)V
    .locals 1

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    sget-boolean v0, Lcom/google/android/apps/chrome/nfc/BeamCallback$Status;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 43
    :cond_0
    iput-object p1, p0, Lcom/google/android/apps/chrome/nfc/BeamCallback$Status;->errorStrID:Ljava/lang/Integer;

    .line 44
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/chrome/nfc/BeamCallback$Status;->result:Ljava/lang/String;

    .line 45
    return-void
.end method

.method constructor <init>(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    sget-boolean v0, Lcom/google/android/apps/chrome/nfc/BeamCallback$Status;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 49
    :cond_0
    iput-object p1, p0, Lcom/google/android/apps/chrome/nfc/BeamCallback$Status;->result:Ljava/lang/String;

    .line 50
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/chrome/nfc/BeamCallback$Status;->errorStrID:Ljava/lang/Integer;

    .line 51
    return-void
.end method
