.class public Lcom/google/android/apps/chrome/services/ui/SignOutDialogFragment;
.super Lcom/google/android/apps/chrome/preferences/ChromeBaseDialogFragment;
.source "SignOutDialogFragment.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Lcom/google/android/apps/chrome/preferences/ChromeBaseDialogFragment;-><init>()V

    .line 26
    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 1

    .prologue
    .line 73
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 74
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/services/ui/SignOutDialogFragment;->getTargetFragment()Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/services/ui/SignOutDialogFragment$Listener;

    .line 75
    invoke-interface {v0}, Lcom/google/android/apps/chrome/services/ui/SignOutDialogFragment$Listener;->onSignedOut()V

    .line 77
    :cond_0
    return-void
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 7

    .prologue
    .line 43
    invoke-static {}, Lorg/chromium/chrome/browser/signin/SigninManager;->isNewProfileManagementEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 44
    sget v3, Lcom/google/android/apps/chrome/R$string;->signout_title_new:I

    .line 45
    sget v2, Lcom/google/android/apps/chrome/R$string;->signout_message_new:I

    .line 46
    sget v1, Lcom/google/android/apps/chrome/R$string;->signout_managed_account_message_new:I

    .line 47
    sget v0, Lcom/google/android/apps/chrome/R$string;->ok:I

    .line 55
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/services/ui/SignOutDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-static {v4}, Lorg/chromium/chrome/browser/signin/SigninManager;->get(Landroid/content/Context;)Lorg/chromium/chrome/browser/signin/SigninManager;

    move-result-object v4

    invoke-virtual {v4}, Lorg/chromium/chrome/browser/signin/SigninManager;->getManagementDomain()Ljava/lang/String;

    move-result-object v4

    .line 57
    if-nez v4, :cond_1

    .line 58
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/services/ui/SignOutDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 63
    :goto_1
    new-instance v2, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/services/ui/SignOutDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-direct {v2, v4}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    invoke-virtual {v2, v0, p0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    sget v2, Lcom/google/android/apps/chrome/R$string;->cancel:I

    invoke-virtual {v0, v2, p0}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0

    .line 49
    :cond_0
    sget v3, Lcom/google/android/apps/chrome/R$string;->signout_title:I

    .line 50
    sget v2, Lcom/google/android/apps/chrome/R$string;->signout_message:I

    .line 51
    sget v1, Lcom/google/android/apps/chrome/R$string;->signout_managed_account_message:I

    .line 52
    sget v0, Lcom/google/android/apps/chrome/R$string;->signout_signout:I

    goto :goto_0

    .line 60
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/services/ui/SignOutDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v4, v5, v6

    invoke-virtual {v2, v1, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto :goto_1
.end method

.method public onDismiss(Landroid/content/DialogInterface;)V
    .locals 1

    .prologue
    .line 81
    invoke-super {p0, p1}, Lcom/google/android/apps/chrome/preferences/ChromeBaseDialogFragment;->onDismiss(Landroid/content/DialogInterface;)V

    .line 82
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/services/ui/SignOutDialogFragment;->getTargetFragment()Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/services/ui/SignOutDialogFragment$Listener;

    .line 83
    invoke-interface {v0}, Lcom/google/android/apps/chrome/services/ui/SignOutDialogFragment$Listener;->onDismissedSignOut()V

    .line 84
    return-void
.end method
