.class Lcom/google/android/apps/chrome/bookmark/SelectBookmarkFolderFragment$FolderListAdapter$1;
.super Ljava/lang/Object;
.source "SelectBookmarkFolderFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic this$1:Lcom/google/android/apps/chrome/bookmark/SelectBookmarkFolderFragment$FolderListAdapter;

.field final synthetic val$entry:Lcom/google/android/apps/chrome/bookmark/SelectBookmarkFolderFragment$FolderListEntry;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/bookmark/SelectBookmarkFolderFragment$FolderListAdapter;Lcom/google/android/apps/chrome/bookmark/SelectBookmarkFolderFragment$FolderListEntry;)V
    .locals 0

    .prologue
    .line 281
    iput-object p1, p0, Lcom/google/android/apps/chrome/bookmark/SelectBookmarkFolderFragment$FolderListAdapter$1;->this$1:Lcom/google/android/apps/chrome/bookmark/SelectBookmarkFolderFragment$FolderListAdapter;

    iput-object p2, p0, Lcom/google/android/apps/chrome/bookmark/SelectBookmarkFolderFragment$FolderListAdapter$1;->val$entry:Lcom/google/android/apps/chrome/bookmark/SelectBookmarkFolderFragment$FolderListEntry;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    .line 284
    iget-object v0, p0, Lcom/google/android/apps/chrome/bookmark/SelectBookmarkFolderFragment$FolderListAdapter$1;->this$1:Lcom/google/android/apps/chrome/bookmark/SelectBookmarkFolderFragment$FolderListAdapter;

    iget-object v0, v0, Lcom/google/android/apps/chrome/bookmark/SelectBookmarkFolderFragment$FolderListAdapter;->this$0:Lcom/google/android/apps/chrome/bookmark/SelectBookmarkFolderFragment;

    iget-object v1, p0, Lcom/google/android/apps/chrome/bookmark/SelectBookmarkFolderFragment$FolderListAdapter$1;->val$entry:Lcom/google/android/apps/chrome/bookmark/SelectBookmarkFolderFragment$FolderListEntry;

    iget-object v1, v1, Lcom/google/android/apps/chrome/bookmark/SelectBookmarkFolderFragment$FolderListEntry;->mFolder:Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;

    invoke-virtual {v1}, Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;->id()J

    move-result-wide v2

    iget-object v1, p0, Lcom/google/android/apps/chrome/bookmark/SelectBookmarkFolderFragment$FolderListAdapter$1;->val$entry:Lcom/google/android/apps/chrome/bookmark/SelectBookmarkFolderFragment$FolderListEntry;

    iget-object v1, v1, Lcom/google/android/apps/chrome/bookmark/SelectBookmarkFolderFragment$FolderListEntry;->mFolder:Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;

    invoke-virtual {v1}, Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v3, v1}, Lcom/google/android/apps/chrome/bookmark/SelectBookmarkFolderFragment;->executeFolderSelection(JLjava/lang/String;)V

    .line 285
    return-void
.end method
