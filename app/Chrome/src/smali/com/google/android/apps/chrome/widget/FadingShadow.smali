.class public Lcom/google/android/apps/chrome/widget/FadingShadow;
.super Ljava/lang/Object;
.source "FadingShadow.java"


# static fields
.field public static final POSITION_BOTTOM:I = 0x1

.field public static final POSITION_TOP:I


# instance fields
.field private mShadowMatrix:Landroid/graphics/Matrix;

.field private mShadowPaint:Landroid/graphics/Paint;

.field private mShadowShader:Landroid/graphics/Shader;


# direct methods
.method public constructor <init>(I)V
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/widget/FadingShadow;->mShadowMatrix:Landroid/graphics/Matrix;

    .line 33
    new-instance v0, Landroid/graphics/LinearGradient;

    const/high16 v4, 0x3f800000    # 1.0f

    const/4 v6, 0x0

    sget-object v7, Landroid/graphics/Shader$TileMode;->CLAMP:Landroid/graphics/Shader$TileMode;

    move v2, v1

    move v3, v1

    move v5, p1

    invoke-direct/range {v0 .. v7}, Landroid/graphics/LinearGradient;-><init>(FFFFIILandroid/graphics/Shader$TileMode;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/widget/FadingShadow;->mShadowShader:Landroid/graphics/Shader;

    .line 34
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/widget/FadingShadow;->mShadowPaint:Landroid/graphics/Paint;

    .line 35
    return-void
.end method


# virtual methods
.method public drawShadow(Landroid/view/View;Landroid/graphics/Canvas;IFF)V
    .locals 7

    .prologue
    const/high16 v5, 0x3f800000    # 1.0f

    .line 50
    const/4 v0, 0x0

    invoke-static {v5, p5}, Ljava/lang/Math;->min(FF)F

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    move-result v0

    mul-float/2addr v0, p4

    .line 51
    cmpg-float v1, v0, v5

    if-gez v1, :cond_1

    .line 72
    :cond_0
    :goto_0
    return-void

    .line 53
    :cond_1
    invoke-virtual {p1}, Landroid/view/View;->getScrollX()I

    move-result v1

    .line 54
    invoke-virtual {p1}, Landroid/view/View;->getRight()I

    move-result v2

    add-int v3, v1, v2

    .line 56
    const/4 v2, 0x1

    if-ne p3, v2, :cond_2

    .line 57
    invoke-virtual {p1}, Landroid/view/View;->getScrollY()I

    move-result v2

    invoke-virtual {p1}, Landroid/view/View;->getBottom()I

    move-result v4

    add-int/2addr v2, v4

    invoke-virtual {p1}, Landroid/view/View;->getTop()I

    move-result v4

    sub-int v4, v2, v4

    .line 58
    iget-object v2, p0, Lcom/google/android/apps/chrome/widget/FadingShadow;->mShadowMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v2, v5, v0}, Landroid/graphics/Matrix;->setScale(FF)V

    .line 59
    iget-object v2, p0, Lcom/google/android/apps/chrome/widget/FadingShadow;->mShadowMatrix:Landroid/graphics/Matrix;

    const/high16 v5, 0x43340000    # 180.0f

    invoke-virtual {v2, v5}, Landroid/graphics/Matrix;->postRotate(F)Z

    .line 60
    iget-object v2, p0, Lcom/google/android/apps/chrome/widget/FadingShadow;->mShadowMatrix:Landroid/graphics/Matrix;

    int-to-float v5, v1

    int-to-float v6, v4

    invoke-virtual {v2, v5, v6}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 61
    iget-object v2, p0, Lcom/google/android/apps/chrome/widget/FadingShadow;->mShadowShader:Landroid/graphics/Shader;

    iget-object v5, p0, Lcom/google/android/apps/chrome/widget/FadingShadow;->mShadowMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v2, v5}, Landroid/graphics/Shader;->setLocalMatrix(Landroid/graphics/Matrix;)V

    .line 62
    iget-object v2, p0, Lcom/google/android/apps/chrome/widget/FadingShadow;->mShadowPaint:Landroid/graphics/Paint;

    iget-object v5, p0, Lcom/google/android/apps/chrome/widget/FadingShadow;->mShadowShader:Landroid/graphics/Shader;

    invoke-virtual {v2, v5}, Landroid/graphics/Paint;->setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;

    .line 63
    int-to-float v1, v1

    int-to-float v2, v4

    sub-float/2addr v2, v0

    int-to-float v3, v3

    int-to-float v4, v4

    iget-object v5, p0, Lcom/google/android/apps/chrome/widget/FadingShadow;->mShadowPaint:Landroid/graphics/Paint;

    move-object v0, p2

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    goto :goto_0

    .line 64
    :cond_2
    if-nez p3, :cond_0

    .line 65
    invoke-virtual {p1}, Landroid/view/View;->getScrollY()I

    move-result v4

    .line 66
    iget-object v2, p0, Lcom/google/android/apps/chrome/widget/FadingShadow;->mShadowMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v2, v5, v0}, Landroid/graphics/Matrix;->setScale(FF)V

    .line 67
    iget-object v2, p0, Lcom/google/android/apps/chrome/widget/FadingShadow;->mShadowMatrix:Landroid/graphics/Matrix;

    int-to-float v5, v1

    int-to-float v6, v4

    invoke-virtual {v2, v5, v6}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 68
    iget-object v2, p0, Lcom/google/android/apps/chrome/widget/FadingShadow;->mShadowShader:Landroid/graphics/Shader;

    iget-object v5, p0, Lcom/google/android/apps/chrome/widget/FadingShadow;->mShadowMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v2, v5}, Landroid/graphics/Shader;->setLocalMatrix(Landroid/graphics/Matrix;)V

    .line 69
    iget-object v2, p0, Lcom/google/android/apps/chrome/widget/FadingShadow;->mShadowPaint:Landroid/graphics/Paint;

    iget-object v5, p0, Lcom/google/android/apps/chrome/widget/FadingShadow;->mShadowShader:Landroid/graphics/Shader;

    invoke-virtual {v2, v5}, Landroid/graphics/Paint;->setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;

    .line 70
    int-to-float v1, v1

    int-to-float v2, v4

    int-to-float v3, v3

    int-to-float v4, v4

    add-float/2addr v4, v0

    iget-object v5, p0, Lcom/google/android/apps/chrome/widget/FadingShadow;->mShadowPaint:Landroid/graphics/Paint;

    move-object v0, p2

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    goto :goto_0
.end method
