.class final Lcom/google/android/apps/chrome/partnercustomizations/PartnerBrowserCustomizations$1;
.super Landroid/os/AsyncTask;
.source "PartnerBrowserCustomizations.java"


# instance fields
.field private mDisablePartnerBookmarksShim:Z

.field private mUpdateNativePreferences:Z

.field final synthetic val$context:Landroid/content/Context;


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 115
    iput-object p1, p0, Lcom/google/android/apps/chrome/partnercustomizations/PartnerBrowserCustomizations$1;->val$context:Landroid/content/Context;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method private onFinalized()V
    .locals 2

    .prologue
    .line 215
    const/4 v0, 0x1

    # setter for: Lcom/google/android/apps/chrome/partnercustomizations/PartnerBrowserCustomizations;->sIsInitialized:Z
    invoke-static {v0}, Lcom/google/android/apps/chrome/partnercustomizations/PartnerBrowserCustomizations;->access$502(Z)Z

    .line 217
    # getter for: Lcom/google/android/apps/chrome/partnercustomizations/PartnerBrowserCustomizations;->sInitializeAsyncCallbacks:Ljava/util/List;
    invoke-static {}, Lcom/google/android/apps/chrome/partnercustomizations/PartnerBrowserCustomizations;->access$600()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Runnable;

    .line 218
    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    goto :goto_0

    .line 220
    :cond_0
    # getter for: Lcom/google/android/apps/chrome/partnercustomizations/PartnerBrowserCustomizations;->sInitializeAsyncCallbacks:Ljava/util/List;
    invoke-static {}, Lcom/google/android/apps/chrome/partnercustomizations/PartnerBrowserCustomizations;->access$600()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 225
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/partnercustomizations/PartnerBrowserCustomizations$1;->mUpdateNativePreferences:Z

    if-eqz v0, :cond_1

    invoke-static {}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->isInitialized()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 226
    invoke-static {}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->getInstance()Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->update()V

    .line 230
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/partnercustomizations/PartnerBrowserCustomizations$1;->mDisablePartnerBookmarksShim:Z

    if-eqz v0, :cond_2

    .line 231
    invoke-static {}, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader;->disablePartnerBookmarksEditing()V

    .line 234
    :cond_2
    const/16 v0, 0x48

    invoke-static {v0}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->broadcastNotification(I)V

    .line 236
    return-void
.end method

.method private refreshBookmarksEditingDisabled()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 156
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/partnercustomizations/PartnerBrowserCustomizations$1;->val$context:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 157
    const-string/jumbo v1, "disablebookmarksediting"

    invoke-static {v1}, Lcom/google/android/apps/chrome/partnercustomizations/PartnerBrowserCustomizations;->buildQueryUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 160
    if-eqz v1, :cond_1

    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Landroid/database/Cursor;->getColumnCount()I

    move-result v0

    if-ne v0, v6, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/partnercustomizations/PartnerBrowserCustomizations$1;->isCancelled()Z

    move-result v0

    if-nez v0, :cond_1

    .line 162
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-ne v0, v6, :cond_3

    move v0, v6

    .line 163
    :goto_0
    # getter for: Lcom/google/android/apps/chrome/partnercustomizations/PartnerBrowserCustomizations;->sBookmarksEditingDisabled:Z
    invoke-static {}, Lcom/google/android/apps/chrome/partnercustomizations/PartnerBrowserCustomizations;->access$200()Z

    move-result v2

    if-eq v0, v2, :cond_0

    .line 164
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/google/android/apps/chrome/partnercustomizations/PartnerBrowserCustomizations$1;->mDisablePartnerBookmarksShim:Z

    .line 166
    :cond_0
    # setter for: Lcom/google/android/apps/chrome/partnercustomizations/PartnerBrowserCustomizations;->sBookmarksEditingDisabled:Z
    invoke-static {v0}, Lcom/google/android/apps/chrome/partnercustomizations/PartnerBrowserCustomizations;->access$202(Z)Z

    .line 168
    :cond_1
    if-eqz v1, :cond_2

    invoke-interface {v1}, Landroid/database/Cursor;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 172
    :cond_2
    :goto_1
    return-void

    :cond_3
    move v0, v7

    .line 162
    goto :goto_0

    .line 169
    :catch_0
    move-exception v0

    .line 170
    const-string/jumbo v1, "PartnerBrowserProvider"

    const-string/jumbo v2, "Partner disable bookmarks editing read failed : "

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method private refreshHomepage()V
    .locals 6

    .prologue
    .line 121
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/partnercustomizations/PartnerBrowserCustomizations$1;->val$context:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 122
    const-string/jumbo v1, "homepage"

    invoke-static {v1}, Lcom/google/android/apps/chrome/partnercustomizations/PartnerBrowserCustomizations;->buildQueryUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 124
    if-eqz v0, :cond_0

    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Landroid/database/Cursor;->getColumnCount()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/partnercustomizations/PartnerBrowserCustomizations$1;->isCancelled()Z

    move-result v1

    if-nez v1, :cond_0

    .line 126
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    # setter for: Lcom/google/android/apps/chrome/partnercustomizations/PartnerBrowserCustomizations;->sHomepage:Ljava/lang/String;
    invoke-static {v1}, Lcom/google/android/apps/chrome/partnercustomizations/PartnerBrowserCustomizations;->access$002(Ljava/lang/String;)Ljava/lang/String;

    .line 128
    :cond_0
    if-eqz v0, :cond_1

    invoke-interface {v0}, Landroid/database/Cursor;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 132
    :cond_1
    :goto_0
    return-void

    .line 129
    :catch_0
    move-exception v0

    .line 130
    const-string/jumbo v1, "PartnerBrowserProvider"

    const-string/jumbo v2, "Partner homepage provider URL read failed : "

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method private refreshIncognitoModeDisabled()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 136
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/partnercustomizations/PartnerBrowserCustomizations$1;->val$context:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 137
    const-string/jumbo v1, "disableincognitomode"

    invoke-static {v1}, Lcom/google/android/apps/chrome/partnercustomizations/PartnerBrowserCustomizations;->buildQueryUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 140
    if-eqz v1, :cond_1

    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Landroid/database/Cursor;->getColumnCount()I

    move-result v0

    if-ne v0, v6, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/partnercustomizations/PartnerBrowserCustomizations$1;->isCancelled()Z

    move-result v0

    if-nez v0, :cond_1

    .line 142
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-ne v0, v6, :cond_3

    move v0, v6

    .line 143
    :goto_0
    # getter for: Lcom/google/android/apps/chrome/partnercustomizations/PartnerBrowserCustomizations;->sIncognitoModeDisabled:Z
    invoke-static {}, Lcom/google/android/apps/chrome/partnercustomizations/PartnerBrowserCustomizations;->access$100()Z

    move-result v2

    if-eq v0, v2, :cond_0

    .line 144
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/google/android/apps/chrome/partnercustomizations/PartnerBrowserCustomizations$1;->mUpdateNativePreferences:Z

    .line 146
    :cond_0
    # setter for: Lcom/google/android/apps/chrome/partnercustomizations/PartnerBrowserCustomizations;->sIncognitoModeDisabled:Z
    invoke-static {v0}, Lcom/google/android/apps/chrome/partnercustomizations/PartnerBrowserCustomizations;->access$102(Z)Z

    .line 148
    :cond_1
    if-eqz v1, :cond_2

    invoke-interface {v1}, Landroid/database/Cursor;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 152
    :cond_2
    :goto_1
    return-void

    :cond_3
    move v0, v7

    .line 142
    goto :goto_0

    .line 149
    :catch_0
    move-exception v0

    .line 150
    const-string/jumbo v1, "PartnerBrowserProvider"

    const-string/jumbo v2, "Partner disable incognito mode read failed : "

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method


# virtual methods
.method protected final bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 115
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/partnercustomizations/PartnerBrowserCustomizations$1;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected final varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 177
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/partnercustomizations/PartnerBrowserCustomizations$1;->val$context:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    # getter for: Lcom/google/android/apps/chrome/partnercustomizations/PartnerBrowserCustomizations;->sProviderAuthority:Ljava/lang/String;
    invoke-static {}, Lcom/google/android/apps/chrome/partnercustomizations/PartnerBrowserCustomizations;->access$300()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->resolveContentProvider(Ljava/lang/String;I)Landroid/content/pm/ProviderInfo;

    move-result-object v0

    .line 179
    if-nez v0, :cond_1

    .line 201
    :cond_0
    :goto_0
    return-object v4

    .line 181
    :cond_1
    iget-object v1, v0, Landroid/content/pm/ProviderInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget v1, v1, Landroid/content/pm/ApplicationInfo;->flags:I

    and-int/lit8 v1, v1, 0x1

    if-nez v1, :cond_2

    # getter for: Lcom/google/android/apps/chrome/partnercustomizations/PartnerBrowserCustomizations;->sIgnoreBrowserProviderSystemPackageCheck:Z
    invoke-static {}, Lcom/google/android/apps/chrome/partnercustomizations/PartnerBrowserCustomizations;->access$400()Z

    move-result v1

    if-nez v1, :cond_2

    .line 183
    const-string/jumbo v1, "TAG"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "Browser Cutomizations content provider package, "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v0, v0, Landroid/content/pm/ProviderInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v2, ", is not a system package. This can be a malicious attepment from a thrid party app, "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v2, "so skip reading the browser content provider."

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 198
    :catch_0
    move-exception v0

    .line 199
    const-string/jumbo v1, "PartnerBrowserProvider"

    const-string/jumbo v2, "Fetching partner customizations failed"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 190
    :cond_2
    :try_start_1
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/partnercustomizations/PartnerBrowserCustomizations$1;->isCancelled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 191
    invoke-direct {p0}, Lcom/google/android/apps/chrome/partnercustomizations/PartnerBrowserCustomizations$1;->refreshIncognitoModeDisabled()V

    .line 193
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/partnercustomizations/PartnerBrowserCustomizations$1;->isCancelled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 194
    invoke-direct {p0}, Lcom/google/android/apps/chrome/partnercustomizations/PartnerBrowserCustomizations$1;->refreshBookmarksEditingDisabled()V

    .line 196
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/partnercustomizations/PartnerBrowserCustomizations$1;->isCancelled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 197
    invoke-direct {p0}, Lcom/google/android/apps/chrome/partnercustomizations/PartnerBrowserCustomizations$1;->refreshHomepage()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method

.method protected final synthetic onCancelled(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 115
    check-cast p1, Ljava/lang/Void;

    invoke-direct {p0}, Lcom/google/android/apps/chrome/partnercustomizations/PartnerBrowserCustomizations$1;->onFinalized()V

    return-void
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 115
    check-cast p1, Ljava/lang/Void;

    invoke-direct {p0}, Lcom/google/android/apps/chrome/partnercustomizations/PartnerBrowserCustomizations$1;->onFinalized()V

    return-void
.end method
