.class Lcom/google/android/apps/chrome/ntp/NewTabPageView$10;
.super Ljava/lang/Object;
.source "NewTabPageView.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/ntp/NewTabPageView;

.field final synthetic val$mSnapScrollRunnable:Ljava/lang/Runnable;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/ntp/NewTabPageView;Ljava/lang/Runnable;)V
    .locals 0

    .prologue
    .line 379
    iput-object p1, p0, Lcom/google/android/apps/chrome/ntp/NewTabPageView$10;->this$0:Lcom/google/android/apps/chrome/ntp/NewTabPageView;

    iput-object p2, p0, Lcom/google/android/apps/chrome/ntp/NewTabPageView$10;->val$mSnapScrollRunnable:Ljava/lang/Runnable;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v4, 0x0

    .line 382
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/NewTabPageView$10;->this$0:Lcom/google/android/apps/chrome/ntp/NewTabPageView;

    # getter for: Lcom/google/android/apps/chrome/ntp/NewTabPageView;->mScrollView:Lcom/google/android/apps/chrome/ntp/NewTabScrollView;
    invoke-static {v0}, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->access$400(Lcom/google/android/apps/chrome/ntp/NewTabPageView;)Lcom/google/android/apps/chrome/ntp/NewTabScrollView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ntp/NewTabScrollView;->getHandler()Landroid/os/Handler;

    move-result-object v0

    if-nez v0, :cond_0

    .line 392
    :goto_0
    return v4

    .line 383
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/NewTabPageView$10;->this$0:Lcom/google/android/apps/chrome/ntp/NewTabPageView;

    # getter for: Lcom/google/android/apps/chrome/ntp/NewTabPageView;->mScrollView:Lcom/google/android/apps/chrome/ntp/NewTabScrollView;
    invoke-static {v0}, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->access$400(Lcom/google/android/apps/chrome/ntp/NewTabPageView;)Lcom/google/android/apps/chrome/ntp/NewTabScrollView;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/ntp/NewTabPageView$10;->val$mSnapScrollRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/ntp/NewTabScrollView;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 385
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v0

    const/4 v1, 0x3

    if-eq v0, v1, :cond_1

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v0

    if-ne v0, v2, :cond_2

    .line 387
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/NewTabPageView$10;->this$0:Lcom/google/android/apps/chrome/ntp/NewTabPageView;

    # setter for: Lcom/google/android/apps/chrome/ntp/NewTabPageView;->mPendingSnapScroll:Z
    invoke-static {v0, v2}, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->access$302(Lcom/google/android/apps/chrome/ntp/NewTabPageView;Z)Z

    .line 388
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/NewTabPageView$10;->this$0:Lcom/google/android/apps/chrome/ntp/NewTabPageView;

    # getter for: Lcom/google/android/apps/chrome/ntp/NewTabPageView;->mScrollView:Lcom/google/android/apps/chrome/ntp/NewTabScrollView;
    invoke-static {v0}, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->access$400(Lcom/google/android/apps/chrome/ntp/NewTabPageView;)Lcom/google/android/apps/chrome/ntp/NewTabScrollView;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/ntp/NewTabPageView$10;->val$mSnapScrollRunnable:Ljava/lang/Runnable;

    const-wide/16 v2, 0x1e

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/apps/chrome/ntp/NewTabScrollView;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .line 390
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/NewTabPageView$10;->this$0:Lcom/google/android/apps/chrome/ntp/NewTabPageView;

    # setter for: Lcom/google/android/apps/chrome/ntp/NewTabPageView;->mPendingSnapScroll:Z
    invoke-static {v0, v4}, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->access$302(Lcom/google/android/apps/chrome/ntp/NewTabPageView;Z)Z

    goto :goto_0
.end method
